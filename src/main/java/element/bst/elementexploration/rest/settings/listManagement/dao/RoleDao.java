package element.bst.elementexploration.rest.settings.listManagement.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.Role;

public interface RoleDao extends GenericDao<Role, Long>{

}
