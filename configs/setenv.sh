
JAVA_OPTS="-Dorg.apache.catalina.security.SecurityListener.UMASK=`umask` -server -Xms1024m -Xmx2048m -Xmn512m -XX:+CMSClassUnloadingEnabled  -XX:HeapDumpPath=/root/apache-tomcat-8.5.9/logs -XX:+HeapDumpOnOutOfMemoryError -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:SurvivorRatio=8 -XX:+UseCompressedOops -Djava.security.auth.login.config=$CATALINA_HOME/webapps/policyEnforcement/WEB-INF/classes/login.config -Dorg.jboss.logging.provider=jdk -Dorg.kie.demo=false -Dorg.kie.example=false -Derrai.bus.enable_sse_support=false -Dorg.uberfire.nio.git.daemon.enabled=false -Dorg.uberfire.nio.git.ssh.enabled=false"

JAVA_OPTS="$JAVA_OPTS -Dserver.name=$HOSTNAME -Djava.security.egd=file:/dev/./urandom"
JAVA_OPTS="$JAVA_OPTS -Dorg.kie.server.id=policy-server -Dorg.kie.server.location=http://localhost:7070/policy-server/services/rest/server -Dorg.kie.server.controller=http://localhost:7070/policyEnforcement/rest/controller"
