package element.bst.elementexploration.rest.logging.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.logging.enumType.LogLevels;

@Component("apiLogConfigurer")
public class ApiLogConfigurer {

	List<ApiLogger> apiLoggers = new ArrayList<>();
	public ApiLogConfigurer(){
		
	}
		
	public List<ApiLogger> getApiLoggers() {
		return apiLoggers;
	}

	@PostConstruct
	public void initDatabase(){
		configure();
	}
	private void configure(){

		/*InvestigationController*/
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCaseSeedStatusSubmittedToAcknowledged", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from submitted to acknowledged"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusAcknowledgedToAccepted", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from acknowledged to accepted"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusAcknowledgedToPaused", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from acknowledged to paused"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusAcknowledgedToRejected", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from acknowledged to rejected"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusPauseToAccepted", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from paused to accepted"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusPauseToRejected", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from paused to rejected"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusAcceptedToPaused", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from accepted to paused"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusAcceptedToResolved", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from accepted to resolved"));
		apiLoggers.add(new ApiLogger("InvestigationController", "changeCurrentStatusAcceptedToCanceled", Collections.singletonList("caseId"), LogLevels.INFO, "Changed case status", "{} changed case {} status from accepted to cancelled"));	
		apiLoggers.add(new ApiLogger("InvestigationController", "caseReassignment", Arrays.asList(new String[] {"caseId", "newUserId"}), LogLevels.INFO, "Reassigned a case", "{} reassigned case {} to {}"));
		apiLoggers.add(new ApiLogger("InvestigationController", "caseForwarding", Arrays.asList(new String[] {"caseId", "newUserId"}), LogLevels.INFO, "Forwarded a case", "{} forwarded case {} to {}"));
		apiLoggers.add(new ApiLogger("InvestigationController", "addCaseToFocus", Collections.singletonList("caseId"), LogLevels.INFO, "Marked a case as focused", "{} marked case {} as focused"));
		apiLoggers.add(new ApiLogger("InvestigationController", "deleteCaseFromFocus", Collections.singletonList("caseId"), LogLevels.WARNING, "Removed a case from focused", "{} removed case {} from focused"));
		
		/*UserController*/		
		apiLoggers.add(new ApiLogger("UserController", "handleUpdateUser", Collections.emptyList(), LogLevels.INFO, "Updated Profile", "{} updated their profile"));
				
		/*AdminstrationController*/
		apiLoggers.add(new ApiLogger("AdminstrationController", "activateUser", Collections.singletonList("userId"), LogLevels.INFO, "Activated a user", "{} activated user {}"));
		apiLoggers.add(new ApiLogger("AdminstrationController", "deactivateUser", Collections.singletonList("userId"), LogLevels.WARNING, "Deactivated a user", "{} deactivated user {}"));
		apiLoggers.add(new ApiLogger("AdminstrationController", "handleUpdateUserByAdmin", Collections.singletonList("userId"), LogLevels.INFO, "Updated a user", "{} updated user {}"));
		
		/*UserGroupsController*/		
		apiLoggers.add(new ApiLogger("UserGroupsController", "updateGroup", Collections.singletonList("groupId"), LogLevels.INFO, "Updated a group", "{} updated group {}"));
		apiLoggers.add(new ApiLogger("UserGroupsController", "activateGroup", Collections.singletonList("groupId"), LogLevels.INFO, "Activated a group", "{} activated group {}"));
		apiLoggers.add(new ApiLogger("UserGroupsController", "deactivateGroup", Collections.singletonList("groupId"), LogLevels.INFO, "Deactivated a group", "{} deactivated group {}"));
		apiLoggers.add(new ApiLogger("UserGroupsController", "addUsersGroups", Collections.emptyList(), LogLevels.INFO, "Added a user to group", "{} added user {} to group {}"));
		apiLoggers.add(new ApiLogger("UserGroupsController", "removeUsersGroups", Collections.emptyList(), LogLevels.WARNING, "Removed a user from group", "{} removed user {} from group {}"));
				
		/*CalendarEventController*/
		//addEventParticipants added in code
		//removeEventParticipants added in code
		apiLoggers.add(new ApiLogger("CalendarEventController", "updateEvent", Collections.singletonList("eventId"), LogLevels.INFO, "Updated an event", "{} updated event {}"));
		
		/*NotificationController*/		
		apiLoggers.add(new ApiLogger("NotificationController", "checkInNotifications", Collections.emptyList(), LogLevels.INFO, "Checkedin notifications", "{} checkedin notifications"));
		
		/*ContactsController*/		
		apiLoggers.add(new ApiLogger("ContactsController", "removeContact", Collections.singletonList("contactId"), LogLevels.WARNING, "Removed a contact", "{} removed {} from contacts"));
		apiLoggers.add(new ApiLogger("ContactsController", "addContact", Collections.singletonList("contactId"), LogLevels.INFO, "Added a contact", "{} added {} to contacts"));
		
		/*ElementAuthenticationController*/
		//apiLoggers.add(new ApiLogger("ElementAuthenticationController", "validateEmailPassword", Collections.singletonList("emailAddress"), LogLevels.INFO, "Logged in", "{} logged in to system at {}"));
		apiLoggers.add(new ApiLogger("ElementAuthenticationsController", "validateEmailPassword", Collections.singletonList("emailAddress"), LogLevels.INFO, "Logged in", "{} logged in to system "));
		apiLoggers.add(new ApiLogger("ElementAuthenticationsController", "validateEmailPassword1", Collections.singletonList("emailAddress"), LogLevels.INFO, "Logged in", "{} logged in to system "));
		
		/*CaseController*/		
		//createCaseSeed added in code**
		//uploadJsonFileToDatabase added in code**
		apiLoggers.add(new ApiLogger("CaseController", "updateBstCaseSeedById", Collections.singletonList("caseId"), LogLevels.INFO, "Updated a case", "{} updated case {}"));
		//createCaseFromSurvey added in code**
		//uploadDocumentDetails added in code
		apiLoggers.add(new ApiLogger("CaseController", "upDateDocumentDetails", Collections.singletonList("docId"), LogLevels.INFO, "Updated case document", "{} updated document {} of case {}"));
		//deleteCaseSeedDocument added in code
			
		/*DocumentController*/	
		//uploadDocumentDetails added in code
		apiLoggers.add(new ApiLogger("DocumentController", "updateDocument",Collections.singletonList("docId"), LogLevels.INFO, "Updated document metadata", "{} updated metadata of document {}"));		
		apiLoggers.add(new ApiLogger("DocumentController", "updateDocumentContent", Collections.singletonList("docId"), LogLevels.INFO, "Updated document content", "{} updated contents of document {}"));
		apiLoggers.add(new ApiLogger("DocumentController", "softDeleteDocument", Collections.singletonList("docId"), LogLevels.WARNING, "Deleted document", "{} deleted document {}"));		
		apiLoggers.add(new ApiLogger("DocumentController", "shareDocumentWithCaseseed", Collections.emptyList(), LogLevels.INFO, "Shared a document", "{} shared document {} with case {}"));		
		apiLoggers.add(new ApiLogger("DocumentController", "unshareDocumentFromCaseseed", Collections.emptyList(), LogLevels.WARNING, "Unshared a document", "{} unshared document {} from case {}"));		
		apiLoggers.add(new ApiLogger("DocumentController", "shareDocumetWithUser", Collections.emptyList(), LogLevels.INFO, "Shared a document", "{} shared document {} with user {}"));		
		apiLoggers.add(new ApiLogger("DocumentController", "unShareDocumetFromUser", Collections.emptyList(), LogLevels.WARNING, "Unshared a document", "{} unshared document {} from user {}"));		
		apiLoggers.add(new ApiLogger("DocumentController", "commentDocument", Collections.emptyList(), LogLevels.INFO, "Commented on a document", "{} commented on document {}"));
		//editComment added in code
		apiLoggers.add(new ApiLogger("DocumentController", "documentDissemination", Collections.emptyList(), LogLevels.INFO, "Disseminated a document", "{} disseminated document {} to case {}"));
	}
}
