package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.AnswerTopEvents;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface AnswerTopEventsDao extends GenericDao<AnswerTopEvents, Long>{
	
	public List<AnswerTopEvents> getTopEventsByAnswerId(Long answerId);

}
