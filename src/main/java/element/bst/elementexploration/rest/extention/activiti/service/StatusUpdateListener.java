package element.bst.elementexploration.rest.extention.activiti.service;

import java.text.ParseException;

import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseAnalystMappingService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Component("statusUpdateListener")
public class StatusUpdateListener implements TaskListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private CaseAnalystMappingService caseAnalystMappingService;

	@Autowired
	private TaskService taskService;

	@Override
	public void notify(DelegateTask delegateTask) {
		String statusFrom = (String) taskService.getVariable(delegateTask.getId(), "statusFrom");
		String statusTo = (String) taskService.getVariable(delegateTask.getId(), "statusTo");
		String caseId = (String) taskService.getVariable(delegateTask.getId(), "caseId");
		String userId = (String) taskService.getVariable(delegateTask.getId(), "userId");
		String statusComment = (String) taskService.getVariable(delegateTask.getId(), "statusComment");
		String notificationDate = (String) taskService.getVariable(delegateTask.getId(), "notificationDate");
		String reassign = (String) taskService.getVariable(delegateTask.getId(), "reassign");
		String forward = (String) taskService.getVariable(delegateTask.getId(), "forward");
		CaseAnalystMapping caseAnalystMapping = null;
		try {
			if (!statusTo.equals("7") && reassign == null && forward == null) {
				caseAnalystMapping = caseAnalystMappingService.updateCaseStatusBpm(new Long(caseId), new Long(userId),
						statusComment, notificationDate, new Integer(statusFrom), new Integer(statusTo));
				delegateTask.getExecution().setVariable("userId",
						caseAnalystMapping == null ? null : caseAnalystMapping.getUser().getUserId().toString());
				delegateTask.getExecution().setVariable("statusFrom",
						StatusEnum.getStatusEnumByIndex(new Integer(statusFrom)).getName());
				delegateTask.getExecution().setVariable("statusTo",
						StatusEnum.getStatusEnumByIndex(new Integer(statusTo)).getName());
			}
			if (reassign == null && forward == null)
				delegateTask.getExecution().setVariable("statusTo",
						StatusEnum.getStatusEnumByIndex(new Integer(statusTo)).getName());

		} catch (NumberFormatException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, StatusUpdateListener.class);

		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, StatusUpdateListener.class);
		}

	}

}
