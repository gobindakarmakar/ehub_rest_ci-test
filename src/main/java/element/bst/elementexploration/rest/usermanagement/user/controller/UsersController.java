package element.bst.elementexploration.rest.usermanagement.user.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.activiti.app.conf.Bootstrapper;
import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.AssigneeDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.DateCountStatusDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.FinalUsersListDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.usermanagement.user.dto.LogonsDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.ProfileDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDtoWithRolesAndGroups;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersListsDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersRegistrationDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.FilterModelDto;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Users API" }, description = "Manages users of the application")
@RestController
@RequestMapping("/api/usersNew")
public class UsersController extends BaseController {

	@Autowired
	private UsersService usersService;

	@Autowired
	SettingsService settingsService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	Bootstrapper bootstrapper;
	
	/*@Autowired
	private UserRolesService userRolesService;*/

	@ApiOperation("Register a user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_REGISTRATION_DISABLED),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Username or email already exists"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/registerByUser", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleRegistrationByUser(@Valid @RequestBody UsersRegistrationDto userRegistrationDto,
			BindingResult results, HttpServletRequest request) {
		Users user = new Users();
		BeanUtils.copyProperties(userRegistrationDto, user);
		// Load properties file.
		Properties prop = usersService.getProperties();

		// Check if registration by user is enabled.
		if (prop != null
				&& ElementConstants.ENABLED_USER_REGISTRATION == Integer.valueOf(prop.getProperty("enable", "0"))) {
			if (!results.hasErrors()) {
				UsersDto existingUser = usersService.getUserByEmailOrUsername(user.getScreenName());
				if (existingUser == null) {
					UsersDto checkUserByEmailAddress = usersService.getUserByEmailOrUsername(user.getEmailAddress());
					if (checkUserByEmailAddress == null) {
						user.setCreatedDate(new Date());
						user.setModifiedDate(null);
						user.setPassword(passwordEncoder.encode(user.getPassword()));
						// user.setStatus(prop.getProperty("status", "0"));
						user = usersService.save(user);

						String url = new String(request.getScheme() + "://" + request.getLocalName() + ":"
								+ request.getLocalPort() + "/");

						// Send email verification link.
						if (ElementConstants.ENABLED_USER_REGISTRATION == Integer
								.valueOf(prop.getProperty("emailVerification", "0"))) {
							usersService.sendEmailVerification(user.getUserId(), user.getScreenName(), url, "token",
									user.getEmailAddress());
						}
						eventPublisher.publishEvent(new LoggingEvent(user, LoggingEventType.USER_REGISTRATION,
								user.getUserId(), new Date()));
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_CREATION_SUCCESSFUL),
								HttpStatus.OK);
					} else {
						return new ResponseEntity<>(
								new ResponseMessage(
										ElementConstants.EMAILADDRESS_ALREADY_EXISTS + user.getEmailAddress()),
								HttpStatus.BAD_REQUEST);
					}
				} else {
					return new ResponseEntity<>(
							new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + user.getScreenName()),
							HttpStatus.BAD_REQUEST);
				}
			} else {
				throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_REGISTRATION_DISABLED),
					HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Register a List of users")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_REGISTRATION_DISABLED),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Username or email already exists"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/registerByUsers", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleRegistrationByUsersList(
			@Valid @RequestBody ArrayList<UsersRegistrationDto> userRegistrationDtos, BindingResult results,
			HttpServletRequest request) {
		Users user = new Users();

		if (userRegistrationDtos.size() <= 0) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND), HttpStatus.OK);
		}

		for (UsersRegistrationDto userRegistrationDto : userRegistrationDtos) {
			BeanUtils.copyProperties(userRegistrationDto, user);

			// Load properties file.
			Properties prop = usersService.getProperties();

			// Check if registration by user is enabled.
			if (prop != null
					&& ElementConstants.ENABLED_USER_REGISTRATION == Integer.valueOf(prop.getProperty("enable", "0"))) {
				if (!results.hasErrors()) {
					UsersDto existingUser = usersService.getUserByEmailOrUsername(user.getScreenName());
					if (existingUser == null) {
						UsersDto checkUserByEmailAddress = usersService
								.getUserByEmailOrUsername(user.getEmailAddress());
						if (checkUserByEmailAddress == null) {
							user.setCreatedDate(new Date());
							user.setModifiedDate(null);
							user.setPassword(passwordEncoder.encode(user.getPassword()));
							// user.setStatus(prop.getProperty("status", "0"));
							user = usersService.save(user);

							String url = new String(request.getScheme() + "://" + request.getLocalName() + ":"
									+ request.getLocalPort() + "/");

							// Send email verification link.
							if (ElementConstants.ENABLED_USER_REGISTRATION == Integer
									.valueOf(prop.getProperty("emailVerification", "0"))) {
								usersService.sendEmailVerification(user.getUserId(), user.getScreenName(), url, "token",
										user.getEmailAddress());
							}
							eventPublisher.publishEvent(new LoggingEvent(user, LoggingEventType.USER_REGISTRATION,
									user.getUserId(), new Date()));

						} else {
							return new ResponseEntity<>(
									new ResponseMessage(
											ElementConstants.EMAILADDRESS_ALREADY_EXISTS + user.getEmailAddress()),
									HttpStatus.BAD_REQUEST);
						}
					} else {
						return new ResponseEntity<>(
								new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + user.getScreenName()),
								HttpStatus.BAD_REQUEST);
					}
				} else {
					throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_REGISTRATION_DISABLED),
						HttpStatus.UNAUTHORIZED);
			}
		}
		return new ResponseEntity<>(
				new ResponseMessage(
						"Total " + userRegistrationDtos.size() + " " + ElementConstants.USERS_CREATION_SUCCESSFUL),
				HttpStatus.OK);
	}

	@ApiOperation("Gets all Users")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersListsDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserListing", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserList(@RequestParam("token") String token,
			@RequestParam(required = false) String status, @RequestParam(required = false) Long groupId,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {
		List<UsersDto> userList = usersService.getUserList(groupId, status, pageNumber, recordsPerPage, orderBy,
				orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = usersService.countUserList(groupId, status);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(userList != null ? userList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		UsersListsDto userListDto = new UsersListsDto();
		userListDto.setResult(userList);
		userListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result", userList);
		 * jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userListDto, HttpStatus.OK);
	}

	@ApiOperation("Search all Users")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersListsDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/fullTextSearch", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserListFullTextSearch(@RequestParam("token") String token,
			@RequestParam("keyword") String keyword, @RequestParam(required = false) String status,
			@RequestParam(required = false) Long groupId, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, HttpServletRequest request) {

		if (StringUtils.isEmpty(keyword) || keyword.length() < 3) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_STRING_LENTH_INVALID),
					HttpStatus.OK);
		}
		List<UsersDto> userList = usersService.getUserListFullTextSearch(keyword, status, groupId, pageNumber,
				recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = usersService.countUserListFullTextSearch(keyword, status, groupId);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(userList != null ? userList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		UsersListsDto userListDto = new UsersListsDto();
		userListDto.setResult(userList);
		userListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result", userList);
		 * jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userListDto, HttpStatus.OK);
	}

	@ApiOperation("Gets user profile")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserProfile", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserProfile(@RequestParam("token") String token, HttpServletRequest request)
			throws Exception {
		//UsersDto userDto = new UsersDto();
		Long userId = getCurrentUserId();
		/*Users user = usersService.find(userId);
		BeanUtils.copyProperties(user, userDto);*/
		
		return new ResponseEntity<>(usersService.getUserProfileById(userId, getCurrentUserId()), HttpStatus.OK);
	}

	@ApiOperation("Updates user profile")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.CHANGE_PASSWORD_FAIL_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateProfile", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleUpdateUser(@RequestParam("token") String token,
			@Valid @RequestBody ProfileDto updatedUser, BindingResult results, HttpServletRequest request)
			throws Exception {

		if (!results.hasErrors()) {
			Long userId = getCurrentUserId();
			Users user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			UsersDto checkUserByUserName = usersService.checkUserNameOrEmailExists(updatedUser.getScreenName(),
					updatedUser.getUserId());
			if (checkUserByUserName == null) {
				UsersDto checkUserByEmailAddress = usersService
						.checkUserNameOrEmailExists(updatedUser.getEmailAddress(), updatedUser.getUserId());
				if (checkUserByEmailAddress == null) {
					// Set user updated record
					user.setEmailAddress(updatedUser.getEmailAddress());
					user.setFirstName(updatedUser.getFirstName());
					user.setLastName(updatedUser.getLastName());
					user.setMiddleName(updatedUser.getMiddleName());
					user.setScreenName(updatedUser.getScreenName());
					user.setDob(updatedUser.getDob());
					// user.setCountry(updatedUser.getCountry());
					user.setModifiedDate(new Date());
					if (updatedUser.getOldPassword() != null && updatedUser.getNewPassword() != null
							&& updatedUser.getRetypePassword() != null) {
						if ((updatedUser.getNewPassword().equals(updatedUser.getRetypePassword()))
								&& (passwordEncoder.matches(updatedUser.getOldPassword(), user.getPassword()))) {
							user.setPassword(passwordEncoder.encode(updatedUser.getNewPassword()));
						} else {
							return new ResponseEntity<>(new ResponseMessage(ElementConstants.CHANGE_PASSWORD_FAIL_MSG),
									HttpStatus.CONFLICT);
						}
					}
					if (settingsService
							.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
									ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
							.equalsIgnoreCase("Off")) {
						usersService.saveOrUpdate(user);
					}
					

					return new ResponseEntity<>(
							new ResponseMessage(ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG), HttpStatus.OK);
				} else {
					return new ResponseEntity<>(
							new ResponseMessage(
									ElementConstants.EMAILADDRESS_ALREADY_EXISTS + updatedUser.getEmailAddress()),
							HttpStatus.CONFLICT);
				}
			} else {
				return new ResponseEntity<>(
						new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + updatedUser.getScreenName()),
						HttpStatus.CONFLICT);
			}
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Gets user profile")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUsersByGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUsersByGroup(@RequestParam("token") String token, HttpServletRequest request,
			@RequestParam Long groupId) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(groupsService.getUsersByGroup(groupId), HttpStatus.OK);
	}

	@ApiOperation("Updates user Image")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_PROFILE_PICTURE_UPDATED_SUCCESSFULLY_MSG)})
	@PostMapping(value = "/uploadUserImage", produces = { "application/json; charset=UTF-8" }, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> uploadUserImage(@RequestParam("token") String token, @RequestParam Long userId,
			@RequestParam MultipartFile userImage, HttpServletRequest request) throws Exception {
		Users user = usersService.find(userId);
		GenericReturnObject message = new GenericReturnObject();
		user = usersService.prepareUserFromFirebase(user);
		boolean isUploaded=false;
		if(user.getUserId().equals(getCurrentUserId()))
			isUploaded=usersService.uploadUserImage(user,userImage,null);
		else
			isUploaded=usersService.uploadUserImage(user,userImage,getCurrentUserId());
		if (isUploaded) {
			user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			message.setData(user);
			String fullName= user.getFirstName();
			if(null!=user.getLastName())
				fullName= fullName+" "+ user.getLastName();
			fullName=fullName+ " "+ user.getLastName();
			message.setResponseMessage(ElementConstants.USER_PROFILE_PICTURE_UPDATED_SUCCESSFULLY_MSG + fullName);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		message.setResponseMessage(ElementConstants.USER_NOT_FOUND_MSG);
		message.setStatus(ElementConstants.ERROR_STATUS);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.OK);

	}
	
	@ApiOperation("Gets user profile By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersRegistrationDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserProfileById", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserProfileById(@RequestParam("token") String token,@RequestParam Long userId, HttpServletRequest request)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		message.setData(usersService.getUserProfileById(userId, getCurrentUserId()));
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		message.setResponseMessage(ElementConstants.FETCH_USER_SUCCESSFUL);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Deletes user Image")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_PROFILE_PICTURE_REMOVED_SUCCESSFULLY_MSG) })
	@DeleteMapping(value = "/deleteUserImage", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteUserImage(@RequestParam("token") String token, @RequestParam Long userId,
			HttpServletRequest request) throws Exception {
		Users user = usersService.find(userId);
		GenericReturnObject message = new GenericReturnObject();
		user = usersService.prepareUserFromFirebase(user);
		
		boolean isDeleted = false;
		if (user.getUserId().equals(getCurrentUserId()))
			isDeleted = usersService.deleteUserImage(user, null);
		else
			isDeleted = usersService.deleteUserImage(user, getCurrentUserId());
		if (isDeleted) {
			message.setData(user);
			String fullName = user.getFirstName();
			if (null != user.getLastName())
				fullName = fullName + " " + user.getLastName();
			fullName = fullName + " " + user.getLastName();
			message.setResponseMessage(ElementConstants.USER_PROFILE_PICTURE_REMOVED_SUCCESSFULLY_MSG + fullName);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);

		}
		message.setResponseMessage(ElementConstants.USER_NOT_FOUND_MSG);
		message.setStatus(ElementConstants.ERROR_STATUS);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.OK);

	}

	@ApiOperation("Gets Users Table data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersListsDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getUsers", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUsers(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) String orderBy,
			@RequestParam(required = true) Boolean isAllRequired, @RequestBody(required = false) FilterModelDto filterDto) {
		
		Long totalResults = 0L;
		Map<List<AssigneeDto>, Long> result = usersService.getUsers(filterDto, isAllRequired, pageNumber, recordsPerPage,totalResults, orderIn, orderBy);
		List<AssigneeDto> userList = new ArrayList<AssigneeDto>();
		
		for(Entry<List<AssigneeDto>, Long> entry : result.entrySet()){
			userList = entry.getKey();
			if(!isAllRequired){
				totalResults = entry.getValue();
			}
		}
		
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		if(isAllRequired){
			totalResults = usersService.countUserList(null, null);
		}
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(userList != null ? userList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		FinalUsersListDto userListDto = new FinalUsersListDto();
		userListDto.setResult(userList);
		userListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result", userList);
		 * jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userListDto, HttpStatus.OK);
	}

	@ApiOperation("Deactivate a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_DEACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/deactivateUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deactivateUser(@RequestParam("token") String token,
			@RequestParam("userId") Long userId, HttpServletRequest request) throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		Long adminUserId = getCurrentUserId();
		Users userBeforeChange = usersService.find(userId);
		userBeforeChange = usersService.prepareUserFromFirebase(userBeforeChange);
		if (userBeforeChange != null) {
			boolean status = usersService.deactivateUser(userBeforeChange, adminUserId);
			if (status) {
				UsersDtoWithRolesAndGroups usersDtoWithRolesAndGroups = usersService.getUserProfileById(userId,
						getCurrentUserId());
				message.setData(usersDtoWithRolesAndGroups);
				message.setResponseMessage(ElementConstants.USER_DEACTIVATED_SUCCESSFULLY_MSG);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
			} else {
				message.setResponseMessage(ElementConstants.USER_DEACTIVATED_FAIL_MSG);
				message.setStatus(ElementConstants.ERROR_STATUS);
			}
		} else {
			message.setResponseMessage(ElementConstants.USER_NOT_FOUND_MSG);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Performs quick search operation on first name , middle name, last name and screen name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersRegistrationDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/quickSearch", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> quickSearch(@RequestParam("token") String token,@RequestParam String searchKey,@RequestParam boolean isScreeNameRequired, HttpServletRequest request)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<UsersDtoWithRolesAndGroups> usersList = usersService.quickSearch(getCurrentUserId(), searchKey,isScreeNameRequired);
		message.setData(usersList);
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		message.setResponseMessage(ElementConstants.FETCH_USER_SUCCESSFUL);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	

	@ApiOperation("Performs quick search operation on first name , middle name, last name and screen name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersRegistrationDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getCountByStatusWise", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCountByStatusWise(@RequestParam("token") String token, HttpServletRequest request)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<DateCountStatusDto> usersList = usersService.getCountByStatusWise(getCurrentUserId());
		message.setData(usersList);
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		message.setResponseMessage(ElementConstants.FETCH_USER_SUCCESSFUL);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@ApiOperation("gives the logon users data by hour, days and months")
	@ApiResponses(value = { @ApiResponse(code = 200, /*response = UsersRegistrationDto.class,*/ message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getLogonsByHourCount", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getlogonCounts(@RequestParam("token") String token,
			@RequestParam(required = false) String period, @RequestParam(required = false) String from,
			@RequestParam(required = false) String to, HttpServletRequest request)
			throws Exception {
		Map<Object, Long> logonData = usersService.getLogonsByHourCount(period, from, to);
		if(logonData != null){
			return new ResponseEntity<>(logonData, HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.NO_DATA_FOUND_MSG), HttpStatus.OK);
		}
		
	}
	
	@ApiOperation("gives the failed logon users data by hour, days and months")
	@ApiResponses(value = { @ApiResponse(code = 200, /*response = UsersRegistrationDto.class,*/ message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getFailedLogonsCount", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFailedLogonsCount(@RequestParam("token") String token,
			@RequestParam(required = false) String period, @RequestParam(required = false) String from,
			@RequestParam(required = false) String to, HttpServletRequest request)
			throws Exception {
		Map<Object, Long> logonData = usersService.getFailedLogonsCount(period, from, to);
		if(logonData != null){
			return new ResponseEntity<>(logonData, HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.NO_DATA_FOUND_MSG), HttpStatus.OK);
		}
		
	}
	
	@ApiOperation("gives the logon users List")
	@ApiResponses(value = { @ApiResponse(code = 200, /*response = UsersRegistrationDto.class,*/ message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getLogonList", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getlogonUsersList(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) String orderBy,
			@RequestBody(required = false) FilterModelDto filterDto)
			throws Exception {
		Long totalResults = 0L;
		Map<List<LogonsDto>, Long> logonDataList = usersService.getLogonList(filterDto, pageNumber, recordsPerPage,totalResults, orderIn, orderBy);
		List<LogonsDto> logonDtoList = new ArrayList<LogonsDto>();
		
		for(Entry<List<LogonsDto>, Long> entry : logonDataList.entrySet()){
			logonDtoList = entry.getKey();
			//if(!isAllRequired){
				totalResults = entry.getValue();
			//}
		}
		
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		/*if(isAllRequired){
			totalResults = usersService.countUserList(null, null);
		}*/
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(logonDtoList != null ? logonDtoList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		FinalUsersListDto logonListDto = new FinalUsersListDto();
		logonListDto.setResult(logonDtoList);
		logonListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result", userList);
		 * jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(logonListDto, HttpStatus.OK);
	}
	
	@ApiOperation("gives the top ten logon users by hour, days and months")
	@ApiResponses(value = { @ApiResponse(code = 200, /*response = UsersRegistrationDto.class,*/ message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getTopTenLogonFailureUsers", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTopTenLogonFailuresUsers(@RequestParam("token") String token,
			@RequestParam(required = false) String period, @RequestParam(required = false) String from,
			@RequestParam(required = false) String to, HttpServletRequest request)
			throws Exception {
		Map<String, Long> logonData = usersService.getTopTenLogonFailuresUsers(period, from, to);
		if(logonData != null){
			return new ResponseEntity<>(logonData, HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.NO_DATA_FOUND_MSG), HttpStatus.OK);
		}
		
	}
	
	@ApiOperation("gives the bytestream array of the users list for csv export")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getUsersCSV" , produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertsCSV(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) Long feedId, @RequestParam(required = true) Boolean isAllRequired,
			@RequestBody(required = false) FilterModelDto filterDto) throws Exception {

		Long totalResults = 0L;
		Map<List<AssigneeDto>, Long> result = usersService.getUsers(filterDto, isAllRequired, pageNumber, recordsPerPage,totalResults, orderIn, orderBy);
		List<AssigneeDto> userList = new ArrayList<AssigneeDto>();
		
		for(Entry<List<AssigneeDto>, Long> entry : result.entrySet()){
			userList = entry.getKey();
			if(!isAllRequired){
				totalResults = entry.getValue();
			}
		}
		byte[] fileData=null;
	
		try {
		if (null != userList && userList.size()>0) {
			fileData = usersService.prepareCSV(userList).toByteArray();
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		    return new ResponseEntity<>(fileData, headers, HttpStatus.OK);
		}
		else {
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		    return new ResponseEntity<>("No Record Found", headers,HttpStatus.OK);
		}
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>("No Record Found", HttpStatus.OK);
		}
		//return new ResponseEntity<>(fileData, HttpStatus.OK);
	}
}
