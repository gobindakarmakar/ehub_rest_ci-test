package element.bst.elementexploration.rest.extention.menuitem.service;

import java.net.URISyntaxException;

import element.bst.elementexploration.rest.extention.menuitem.domain.UserMenu;
import element.bst.elementexploration.rest.extention.menuitem.dto.MenuFinalDto;
import element.bst.elementexploration.rest.extention.menuitem.dto.UserMenuFinalDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Jalagari Paul
 *
 */
public interface MenuItemService extends GenericService<UserMenu, Long> {

	boolean updateUserMenu(UserMenuFinalDto userMenuFinalDto, Long userId) throws Exception;

	MenuFinalDto getAllUserMenu(Long userId) throws Exception;

	String getAllUserMenus() throws Exception ;
}
