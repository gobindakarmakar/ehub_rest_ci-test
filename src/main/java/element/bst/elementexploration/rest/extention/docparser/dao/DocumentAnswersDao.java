package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentAnswersDao extends GenericDao<DocumentAnswers, Long>{

	DocumentAnswers getDcoumentAnswerByQuestionId(Long questionId);
	DocumentAnswers getDcoumentAnswerByQuestionId(Long questionId,Long docId);
	List<DocumentAnswers> getListOfTableAnswers(Long questionId, Long docId);
	List<DocumentAnswers> allDocumentAnswersbyLevel1Code(DocumentAnswers documentAnswer);
	

}
