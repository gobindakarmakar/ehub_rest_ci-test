package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

public class RequestFilterDto {
	
	private String filterType;
	
	private String subType;
	
	private String entityColumn;
	
	private String keyword;

	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getEntityColumn() {
		return entityColumn;
	}

	public void setEntityColumn(String entityColumn) {
		this.entityColumn = entityColumn;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
}
