package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerAddressDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerAddress;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("customerAddressDao")
public class CustomerAddressDaoImpl extends GenericDaoImpl<CustomerAddress, Long> implements CustomerAddressDao {

	public CustomerAddressDaoImpl() {
		super(CustomerAddress.class);
	}

}
