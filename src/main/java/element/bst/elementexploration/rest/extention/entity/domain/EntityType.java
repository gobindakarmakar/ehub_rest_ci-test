package element.bst.elementexploration.rest.extention.entity.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "entity_types")
public class EntityType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long type_id;
	
	@Column(name = "type")
	private String entityType;
	
	@OneToMany(mappedBy = "type", cascade = CascadeType.ALL, targetEntity = EntityRequirements.class, fetch = FetchType.LAZY)
	private List<EntityRequirements> requirementsList;

	public EntityType() {

	}

	public EntityType(String entityType) {
		super();
		this.entityType = entityType;

	}

	public EntityType(long type_id, String entityType, List<EntityRequirements> requirementsList) {
		super();
		this.type_id = type_id;
		this.entityType = entityType;
		this.requirementsList = requirementsList;
	}

	public long getType_id() {
		return type_id;
	}

	public void setType_id(long type_id) {
		this.type_id = type_id;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public List<EntityRequirements> getRequirementsList() {
		return requirementsList;
	}

	public void setRequirementsList(List<EntityRequirements> requirementsList) {
		this.requirementsList = requirementsList;
	}

}
