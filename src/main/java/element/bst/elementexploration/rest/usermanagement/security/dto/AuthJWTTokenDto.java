package element.bst.elementexploration.rest.usermanagement.security.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek Maurya
 */

public class AuthJWTTokenDto implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Authentication JWT Token", required = true)
	@NotEmpty(message = "Authentication Token can not be empty or blank.")
	private String idToken;

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	
	
	
}
