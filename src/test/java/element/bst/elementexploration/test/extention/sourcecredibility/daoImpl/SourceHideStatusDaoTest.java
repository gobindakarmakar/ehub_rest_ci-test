package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourcesHideStatusDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;

/**
 * @author suresh
 *
 */
public class SourceHideStatusDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourcesHideStatusDaoImpl sourcesHideStatusDao;

	List<SourcesHideStatus> sourcesHideStatusList= new ArrayList<SourcesHideStatus>();
	SourcesHideStatus sourcesHideStatus = null;
	@Before
	public void setupMock() {
		sourcesHideStatus = new SourcesHideStatus();
		sourcesHideStatus.setClassificationId(1L);
		sourcesHideStatusList.add(sourcesHideStatus);
		MockitoAnnotations.initMocks(this);
	}

	/*@Test*/
	public void getSourceHideStatus() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sourcesHideStatusList);
		Mockito.when(sourcesHideStatusDao.findAll()).thenReturn(sourcesHideStatusList);
		List<SourcesHideStatus> sourcesHideStatusSaved = sourcesHideStatusDao.getSourceHideStatusByClassification(45L);
		assertEquals(sourcesHideStatusSaved.size(), sourcesHideStatusList.size());
	}

	@SuppressWarnings("unchecked")
	//@Test(expected = FailedToExecuteQueryException.class)
	public void getSourceHideStatus_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		sourcesHideStatusDao.getSourceHideStatusByClassification(45L);
	}

}
