package element.bst.elementexploration.rest.extention.menuitem.dao;

import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Jalagari Paul
 *
 */
public interface ModulesDao extends GenericDao<Modules, Long> {

}
