package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface AccountDao extends GenericDao<Account, Long> 
{
	
	public boolean checkExistenceOfAccount(List<TxDto> accountIds);
	
	public List<Account> fetchAccounts(String partyIdentfier);

	public List<Account> fetchMulAccounts(List<String> customerNumbers);
	
	Account fetchAccount(String accountNumber);
	
	public List<Account> fetchLuxuryAccounts(List<String> luxuryItems);
	
	public Account fetchLuxuryAccount(String accountNumber);
	
	
}
