package element.bst.elementexploration.rest.extention.menuitem.dao;

import element.bst.elementexploration.rest.extention.menuitem.domain.UserMenu;
import element.bst.elementexploration.rest.extention.menuitem.dto.UserMenuFinalDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Jalagari Paul
 *
 */
public interface MenuItemDao extends GenericDao<UserMenu, Long> {

	UserMenu getExistingUserMenu(UserMenuFinalDto userMenuFinalDto, Long userId) throws Exception;

	int getMaxClickCountOfUserByGroup(Long groupId, Long idOfuser) throws Exception;

}
