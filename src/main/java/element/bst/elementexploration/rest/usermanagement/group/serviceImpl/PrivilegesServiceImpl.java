package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.PrivilegesDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.DefaultPermissionsDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.ListPermissionsDtoWithParent;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;

/**
 * @author Jalagari Paul
 *
 */
@Service("privilegesService")
@Transactional("transactionManager")
public class PrivilegesServiceImpl extends GenericServiceImpl<Permissions, Long> implements PrivilegesService {
	
	
	@Autowired
	PrivilegesDao privilegesDao;
	

	public PrivilegesServiceImpl() {
	}

	@Autowired
	public PrivilegesServiceImpl(GenericDao<Permissions, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public Permissions getPermissionOfItem(String item) throws Exception {
		Permissions permission = new Permissions();
		List<Permissions> filteredPermission = new ArrayList<>();
		filteredPermission = findAll().stream().filter(p -> p.getItemName().equalsIgnoreCase(item))
				.collect(Collectors.toList());
		if (filteredPermission.size() > 0)
			permission = filteredPermission.get(0);
		return permission;
	}

	@Override
	public Permissions findPermission(Permissions permissions) throws Exception {
		Permissions permission = new Permissions();
		List<Permissions> filteredPermission = new ArrayList<>();
		filteredPermission = findAll().stream().filter(p -> p.getItemName().equalsIgnoreCase(permissions.getItemName()))
				.collect(Collectors.toList());
		if (permissions.getPermissionId() != null)
			filteredPermission = filteredPermission.stream()
					.filter(p -> p.getPermissionId().equals(permissions.getPermissionId()))
					.collect(Collectors.toList());
		if (filteredPermission.size() > 0)
			permission = filteredPermission.get(0);
		return permission;
	}

	@Override
	public List<PermissionsWithIdDto> findAllPermissions() throws Exception {
		List<PermissionsWithIdDto> allPermissions=privilegesDao.findAllPermissions();
		return allPermissions;
	}

	@Override
	public PermissionsWithIdDto findPermissionById(Long permissionId) throws Exception {
		return privilegesDao.findPermissionById(permissionId);
	}

	@Override
	public void loadDefaultPreviliges(String permissionsFileName) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource(permissionsFileName);
		File file = new File(resource.toURI());
		ListPermissionsDtoWithParent jsonRead = mapper.readValue(file, ListPermissionsDtoWithParent.class);
		if (jsonRead != null) {
			List<DefaultPermissionsDto> permissionsList = jsonRead.getPermissionsList();
			if (permissionsList != null && permissionsList.size() > 0) {
				for (DefaultPermissionsDto eachPermission : permissionsList) {
					PermissionsWithIdDto permissionFromDb = null;
					try {
						permissionFromDb = findPermissionById(eachPermission.getPermissionId());
					} catch (Exception e) {
						if (permissionFromDb == null || permissionFromDb.getPermissionId() == null) {
							boolean isPrivilegeSaved=privilegesDao.savePrivilege(eachPermission);
							if(!isPrivilegeSaved) {
								System.out.println("Failed to save permission: "+eachPermission.getItemName());
							}
						}
					}
				}
			}
		}
	}

}
