package element.bst.elementexploration.rest.extention.alert.feedmanagement.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedGroupsManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedGroupSevice;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("feedGroupService")
@Transactional("transactionManager")
public class FeedGroupServiceImpl extends GenericServiceImpl<FeedGroups, Long> implements FeedGroupSevice {

	@Autowired
	public FeedGroupServiceImpl(GenericDao<FeedGroups, Long> genericDao) {
		super(genericDao);
	}
	
	@Autowired
	FeedGroupsManagementDao feedGroupsManagementDao;
	
	public FeedGroupServiceImpl() {
		
	}

	@Override
	public List<FeedGroups> getFeedGroupsByFeedId(Long feed_management_id) {
		
		return feedGroupsManagementDao.getFeedGroupsByFeedId(feed_management_id);
	}

	@Override
	@Transactional("transactionManager")
	public StringBuilder deleteFeedgroupList(List<FeedGroups> dbgroups, StringBuilder dbGroupsString) {
		for (FeedGroups del : dbgroups) {
			dbGroupsString.append(del.getGroupId().getName() + ", ");
			delete(del);
			
		}		
	return dbGroupsString;
	}

	

}
