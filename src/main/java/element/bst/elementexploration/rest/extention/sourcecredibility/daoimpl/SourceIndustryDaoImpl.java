package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceIndustryDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("sourceIndustryDao")
public class SourceIndustryDaoImpl extends GenericDaoImpl<SourceIndustry, Long> implements SourceIndustryDao {

	public SourceIndustryDaoImpl() {
		super(SourceIndustry.class);
	}

	@Override
	public SourceIndustry fetchIndustry(String name) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceIndustry s where s.industryName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", name);
			SourceIndustry sourcesIndustry = (SourceIndustry) query.getSingleResult();
			if (sourcesIndustry != null)
				return sourcesIndustry;
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourceIndustryDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SourceIndustry> fetchIndustryListExceptAll() {
		List<SourceIndustry> inddustryList = new ArrayList<SourceIndustry>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceIndustry s where s.industryName <>:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", "All");
			inddustryList = (List<SourceIndustry>) query.getResultList();
			return inddustryList;
		} catch (Exception e) {
			return inddustryList;
		}

	}

}
