package element.bst.elementexploration.rest.casemanagement.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

public interface CaseDao extends GenericDao<Case, Long> {

	boolean isCaseAccessibleOrOwner(Long caseId, Long userId);

	List<CaseDto> fullTextSearchForCaseSeed(String caseSearchKeyword, Integer pageNumber, Integer recordsPerPage,
			Date creationDate, Date modifiedDate, long userId, String type,Integer status,Integer priority,String orderBy,String orderIn);
	long countFullTextSearchForCaseSeed(String caseSearchKeyword,long userId,Date creationDate, Date modifiedDate,String type,Integer status,Integer priority);

	List<CaseDto> getAllCasesByUser(long userId, Integer pageNumber, Integer recordsPerPage,String type,Integer status,Integer priority,String orderBy,String orderIn);
	
	Long countAllCasesByUser(long userId,String type,Integer status,Integer priority);

	public boolean isAnalystRejectedCaseBefore(Long analystId, Long caseId);

	public Long assignCaseToAnalyst(Users user, Case bstCaseSeed);
	
	List<CaseDto> getAllIncomingtrayBasedOnUserId(Long analystId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn);
	
	long countgetAllIncomingTray(Long userId);
	
	List<CaseDto> fullTextSearchForIncomingtray(Long userId,String caseSearchKeyword,Integer pageNumber,Integer recordsPerPage,Date creationDate,Date modifiedDate, String orderBy, String orderIn) throws ParseException;
	
	long countFullTextSearchForIncomingtray(Long userId,String caseSearchKeyword,Date creationDate,Date modifiedDate) throws ParseException;
	
	List<CaseDto> getAllMyCasesBasedOnUserId(Long userId,String orderBy,String orderIn,Integer pageNumber,Integer recordsPerPage);
	
	long countgetAllMyCases(Long userId);
	
	List<CaseDto> fullTextSearchForMyCases(Long userId,String caseSearchKeyword,Integer pageNumber,Integer recordsPerPage,Date creationDate,Date modifiedDate, String orderBy, String orderIn) throws ParseException;
	
	long countFullTextSearchForMyCases(Long userId,String caseSearchKeyword,Date creationDate,Date modifiedDate) throws ParseException;

	List<CaseDto> getCaseList(CaseVo caseVo,Long userId);

	Long getCaseListCount(CaseVo caseVo, Long userId);

	List<CaseAggregator> caseAggregator(Long userId, CaseAggregatorDto caseAggregatorDto);

	List<Long> getCasesByUser(Long currentUserId);

	List<Long> getMyCaseIds(Long userId,CaseVo caseVo,Integer limit);
	
	List<CaseUnderwritingDto> getCaseListForUnderwriting(Integer recordsPerPage, Integer pageNumber, List<Long> docIds);

	Long caseListCountForUnderwriting(List<Long> docIds);

	List<CaseUnderwritingDto> searchCaseList(String keyword);

	Long searchCaseListCount(String keyword);

	Case getCaseByDocId(Long docId);
	
}
