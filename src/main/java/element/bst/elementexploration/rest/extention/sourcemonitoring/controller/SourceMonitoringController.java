package element.bst.elementexploration.rest.extention.sourcemonitoring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcemonitoring.service.SourceMonitoringService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Source Monitoring API" })
@RestController
@RequestMapping("/api/sourceMonitoring")
public class SourceMonitoringController extends BaseController {

	@Autowired
	private SourceMonitoringService sourceMonitoringService;

	@ApiOperation("Gets Metadata")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getMetaData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getMetaData(@RequestParam String token) throws Exception {

		return new ResponseEntity<>(sourceMonitoringService.getMetaData(null, null), HttpStatus.OK);
	}

	@ApiOperation("Gets ErrorData data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getErrorData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getErrorData(@RequestParam String token, @RequestParam(required = false) String dateLimit) throws Exception {
		return new ResponseEntity<>(sourceMonitoringService.getErrorData(dateLimit), HttpStatus.OK);
	}
	
	@ApiOperation("Gets sparkline Data By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getSparklineDataBySourceAndDatelimit", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSparklineDataBySourceAndDatelimit(@RequestParam String token, @RequestParam(required = false) String dateLimit,
			@RequestParam(required = false) String source_id) throws Exception {
		return new ResponseEntity<>(sourceMonitoringService.getSparklineDataById(dateLimit, source_id), HttpStatus.OK);
	}
}
