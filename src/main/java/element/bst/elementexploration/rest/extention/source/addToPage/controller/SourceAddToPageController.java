package element.bst.elementexploration.rest.extention.source.addToPage.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocumentUploadResponse;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.extention.source.addToPage.dto.SourceAddToPageDto;
import element.bst.elementexploration.rest.extention.source.addToPage.service.SourceAddToPageService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hanuman
 *
 */
@Api(tags = { "Source Add To Page API" })
@RestController
@RequestMapping("/api/sourceAddToPage")
public class SourceAddToPageController extends BaseController{

	@Autowired
	SourceAddToPageService sourceAddToPageService;

	@ApiOperation("Get All Source Add To Page")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllSourceAddToPage", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllEntityComplexStructure(HttpServletRequest request, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(sourceAddToPageService.getAllSourceAddToPage(),HttpStatus.OK);
	}


	@ApiOperation("Get Source Add To Page")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getSourceAddToPage", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntityComplexStructure(HttpServletRequest request,@RequestParam("token") String token,@RequestParam(required = true) String entityId,@RequestParam(required = true) String sourceName) throws Exception {
		SourceAddToPageDto sourceAddToPageDto=sourceAddToPageService.getSourceAddToPage(entityId, sourceName);
		if(sourceAddToPageDto==null) {
			return new ResponseEntity<>(new ResponseMessage("Source Add To Page Is Not Found"), HttpStatus.NOT_FOUND);
		}else {

			return new ResponseEntity<>(sourceAddToPageDto,HttpStatus.OK);
		}
	}

	@ApiOperation("Save Source Add To Page")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveSourcesAddToPage", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveEntityComplexStructure(HttpServletRequest request, @RequestParam String token,
			@RequestBody List<SourceAddToPageDto> sourceAddToPageDtos) throws Exception {
		long userIdTemp = getCurrentUserId();
		List<SourceAddToPageDto> sourceAddToPageDto = sourceAddToPageService.saveSourcesAddToPage(sourceAddToPageDtos,userIdTemp);
		if(sourceAddToPageDto!=null) {
			return new ResponseEntity<>(sourceAddToPageDto,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(new ResponseMessage("Source Add To Page is Already Exists"), HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation("Upload Add To Page Document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocumentUploadResponse.class, message = "File Uploaded Successfully"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.INVALID_DOC_FLAG_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/uploadAddToPageDocument", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadAddToPageDocument(@RequestParam MultipartFile uploadFile,
			@RequestParam String fileTitle, @RequestParam("token") String token,
			@RequestParam(required = false) Integer docFlag,
			@RequestParam(required = true) String entityId ,@RequestParam(required = true) String sourceName,
			@RequestParam(required = true) Boolean isAddToPage,HttpServletRequest request, HttpServletResponse response) throws DocNotFoundException, Exception {

		if (docFlag != null && (docFlag < 0 || docFlag > 255)) {
			throw new BadRequestException(ElementConstants.INVALID_DOC_FLAG_MSG);
		}
		long userIdTemp = getCurrentUserId();
		JSONObject jsonResponse = sourceAddToPageService.uploadAddToPageDocument(uploadFile, fileTitle, userIdTemp, docFlag,entityId,sourceName,null,isAddToPage);

		return new ResponseEntity<>(jsonResponse.toString(), HttpStatus.OK);
	}



	@ApiOperation("Delete Source Add To Page")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@DeleteMapping(value = "/deleteSourceAddToPage")
	public ResponseEntity<?> deleteSourceAddToPage(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = true) String entityId,@RequestParam(required = true) String sourceName) throws Exception {
		long userIdTemp = getCurrentUserId();
		boolean entityComplexStructureDto = sourceAddToPageService.deleteSourceAddToPage(entityId, sourceName,userIdTemp);
		if(entityComplexStructureDto) {
			return new ResponseEntity<>(new ResponseMessage("success"), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(new ResponseMessage("failed"),HttpStatus.NOT_FOUND);
		}
	}

}