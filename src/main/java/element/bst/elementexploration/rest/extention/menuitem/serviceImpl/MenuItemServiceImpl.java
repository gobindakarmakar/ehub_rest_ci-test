package element.bst.elementexploration.rest.extention.menuitem.serviceImpl;

import java.io.File;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.MenuItemDao;
import element.bst.elementexploration.rest.extention.menuitem.dao.ModulesGroupDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.extention.menuitem.domain.UserMenu;
import element.bst.elementexploration.rest.extention.menuitem.dto.GroupFinalDto;
import element.bst.elementexploration.rest.extention.menuitem.dto.MenuFinalDto;
import element.bst.elementexploration.rest.extention.menuitem.dto.ModuleFinalDto;
import element.bst.elementexploration.rest.extention.menuitem.dto.UserMenuFinalDto;
import element.bst.elementexploration.rest.extention.menuitem.dto.UserMenuResponseDto;
import element.bst.elementexploration.rest.extention.menuitem.enumType.MenuSizeEnum;
import element.bst.elementexploration.rest.extention.menuitem.service.MenuItemService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Jalagari Paul
 *
 */
@Service("menuItemService")
@Transactional("transactionManager")
public class MenuItemServiceImpl extends GenericServiceImpl<UserMenu, Long> implements MenuItemService {

	@Autowired
	MenuItemDao menuItemDao;

	@Autowired
	ModulesGroupDao modulesGroupDao;

	@Autowired
	MenuItemService menuItemService;

	public MenuItemServiceImpl() {
	}

	@Autowired
	public MenuItemServiceImpl(GenericDao<UserMenu, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public boolean updateUserMenu(UserMenuFinalDto userMenuFinalDto, Long userId) throws Exception {
		if (userMenuFinalDto != null) {
			UserMenu existingUserMenu = menuItemDao.getExistingUserMenu(userMenuFinalDto, userId);
			if (existingUserMenu != null) {
				int existingCount = existingUserMenu.getClicksCount();
				// calculate size
				int count = menuItemDao.getMaxClickCountOfUserByGroup(userMenuFinalDto.getGroupId(),
						userMenuFinalDto.getUserId());
				MenuSizeEnum size = calculateMenuItemSize(existingCount + 1, count);
				existingUserMenu.setMenuItemSize(size.toString());
				existingUserMenu.setClicksCount(existingCount + 1);
				update(existingUserMenu);
				if(userMenuFinalDto.getGroupId()!=null) {
					List<UserMenu> groupMenu= findAll().stream().filter(menu -> menu.getModuleGroup().getModuleGroupId().equals(userMenuFinalDto.getGroupId()))
							.filter(menu -> menu.getUserId().equals(userId)).collect(Collectors.toList());
					if(groupMenu!=null && groupMenu.size()>0) {
						for(UserMenu menu:groupMenu) {		
							int groupMaxCount = menuItemDao.getMaxClickCountOfUserByGroup(userMenuFinalDto.getGroupId(),
									userMenuFinalDto.getUserId());
							MenuSizeEnum sizeNew = calculateMenuItemSize(menu.getClicksCount(), groupMaxCount);
							menu.setMenuItemSize(sizeNew.toString());
							update(menu);
						}
					}
				}
				return true;
			}
		}

		return false;
	}

	private MenuSizeEnum calculateMenuItemSize(int existingCount, int maxCount) {
		double sizePercent = 0.0;
		if (maxCount != 0 && existingCount == 0) {
			sizePercent = 0.0;
		} else if (maxCount == 0 && existingCount != 0) {
			sizePercent = 100.0;
		} else {
			sizePercent = (existingCount * 100) / maxCount;
		}
		if (sizePercent > 80.0)
			return MenuSizeEnum.LARGE;
		else if (sizePercent >= 50.0 && sizePercent <= 80.0)
			return MenuSizeEnum.MEDIUM;
		else
			return MenuSizeEnum.SMALL;
	}

	@Override
	public MenuFinalDto getAllUserMenu(Long userId) throws Exception {
		MenuFinalDto finalMenu = new MenuFinalDto();
		List<UserMenuResponseDto> userMenuResponse = new ArrayList<>();
		Map<ModulesGroup, List<UserMenu>> groupData = new HashMap<>();
		List<ModuleFinalDto> modules = new ArrayList<>();
		List<UserMenu> userMenuList = findAll().stream().filter(menu -> menu.getUserId().equals(userId))
				.collect(Collectors.toList());
		groupData = userMenuList.stream().collect(Collectors.groupingBy(UserMenu::getModuleGroup));
		List<GroupFinalDto> finalGroups = new ArrayList<>();
		for (UserMenu menu : userMenuList) {
			UserMenuResponseDto userMenuResponseDto = new UserMenuResponseDto();
			ModuleFinalDto moduleFinalDto = new ModuleFinalDto();
			BeanUtils.copyProperties(menu, userMenuResponseDto);
			BeanUtils.copyProperties(menu.getModule(), moduleFinalDto);
			moduleFinalDto.setClicksCount(menu.getClicksCount());
			moduleFinalDto.setDisabled(menu.getDisabled());
			moduleFinalDto.setMenuItemSize(menu.getMenuItemSize());
			moduleFinalDto.setUserId(menu.getUserId());
			moduleFinalDto.setModuleGroupId(menu.getModuleGroup().getModuleGroupId());
			modules.add(moduleFinalDto);
			userMenuResponse.add(userMenuResponseDto);
		}
		Set<ModulesGroup> groupsSet = groupData.keySet();
		List<ModuleFinalDto> eachModulesList = null;
		Iterator<ModulesGroup> groupIterator = groupsSet.iterator();
		while (groupIterator.hasNext()) {
			ModulesGroup moduleGroup = groupIterator.next();
			GroupFinalDto groupDto = new GroupFinalDto();
			groupDto.setModuleGroupColor(moduleGroup.getModuleGroupColor());
			groupDto.setModuleGroupIcon(moduleGroup.getModuleGroupIcon());
			groupDto.setModuleGroupId(moduleGroup.getModuleGroupId());
			groupDto.setModuleGroupName(moduleGroup.getModuleGroupName());
			eachModulesList = new ArrayList<>();
			for (ModuleFinalDto eachModule : modules) {
				if (moduleGroup.getModuleGroupId().equals(eachModule.getModuleGroupId())) {
					eachModulesList.add(eachModule);
				}
			}
			if (eachModulesList != null)
				groupDto.setModules(eachModulesList);
			finalGroups.add(groupDto);
		}
		finalMenu.setModuleGroups(finalGroups);
		return finalMenu;
	}

	@Override
	public String getAllUserMenus() throws Exception {
		Object object = new Object();
		try{
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("newSubMenu.json");
			File file = new File(resource.toURI());
			JSONParser parser = new JSONParser();
			object = parser.parse(new FileReader(file));
		}catch(Exception e){
			e.printStackTrace();
		}
		return object.toString();
	}
}
