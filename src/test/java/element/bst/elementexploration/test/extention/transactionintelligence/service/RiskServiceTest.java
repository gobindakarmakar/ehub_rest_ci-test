package element.bst.elementexploration.test.extention.transactionintelligence.service;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfoDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ScenariosResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.TxDataServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RiskServiceTest {

	@Mock
	TxDataDao txDataDaoMock;

	@Mock
	AlertDao alertDaoMock;

	@InjectMocks
	private TxDataServiceImpl txDataService;

	RiskCountAndRatioDto riskCountAndRatioDto;

	RiskCountAndRatioDto productRisk;

	RiskCountAndRatioDto geoGraphicRisk;

	RiskCountAndRatioDto politicalRisk;

	RiskCountAndRatioDto corruptionRisk;

	List<RiskCountAndRatioDto> list;

	ScenariosResponseDto scenariosResponseDto;

	private Set<String> scenario;

	List<RiskCountAndRatioDto> riskRatioChartList;

	List<AlertDashBoardRespDto> alertDashBoardRespDtoList;

	CounterPartyInfoDto counterPartyInfoDto;

	List<RiskCountAndRatioDto> list2 = new ArrayList<>();

	List<AlertedCounterPartyDto> alertedCounterPartyDtoList = new ArrayList<AlertedCounterPartyDto>();

	@Before
	public void setupMock() {
		scenario = new TreeSet<>();
		scenario.add("Large reportable wired transaction");
		scenario.add("Large reportable cash transaction");

		riskCountAndRatioDto = new RiskCountAndRatioDto("CustomerRisk", new Double(1000), 10L, new Double(10), 100L);
		riskCountAndRatioDto.setAlertAmount(new Double(100));

		geoGraphicRisk = new RiskCountAndRatioDto("GeoGraphicRisk", new Double(1000), 10L, new Double(10), 100L);
		geoGraphicRisk.setAlertAmount(new Double(100));

		politicalRisk = new RiskCountAndRatioDto("CorruptionRisk", new Double(1000), 10L, new Double(10), 100L);
		politicalRisk.setAlertAmount(new Double(100));

		corruptionRisk = new RiskCountAndRatioDto("PoliticalRisk", new Double(1000), 10L, new Double(10), 100L);
		corruptionRisk.setAlertAmount(new Double(100));

		productRisk = new RiskCountAndRatioDto("ProductRisk", new Double(1000), 10L, new Double(10), 100L);
		productRisk.setAlertAmount(new Double(100));

		list = new ArrayList<>();
		RiskCountAndRatioDto riskCountAndRatioDto1 = new RiskCountAndRatioDto("Investing corporation", new Double(1000),
				10L);
		RiskCountAndRatioDto riskCountAndRatioDto2 = new RiskCountAndRatioDto("Real estate agency", new Double(100),
				5L);
		list.add(riskCountAndRatioDto1);
		list.add(riskCountAndRatioDto2);

		scenariosResponseDto = new ScenariosResponseDto();
		List<RiskCountAndRatioDto> list1 = new ArrayList<>();

		RiskCountAndRatioDto riskCountAndRatioDto3 = new RiskCountAndRatioDto("Investing corporation", new Double(1000),
				10L);
		riskCountAndRatioDto3.setScenario(scenario);
		list1.add(riskCountAndRatioDto3);
		RiskCountAndRatioDto riskCountAndRatioDto4 = new RiskCountAndRatioDto("Large reportable wired transaction",
				new Double(100), 5L);
		RiskCountAndRatioDto riskCountAndRatioDto5 = new RiskCountAndRatioDto("Large reportable cash transaction",
				new Double(100), 5L);
		list2.add(riskCountAndRatioDto4);
		list2.add(riskCountAndRatioDto5);
		scenariosResponseDto.setRiskCountAndRatioDtos(list1);
		scenariosResponseDto.setScenariosList(list2);

		riskRatioChartList = new ArrayList<>();
		riskRatioChartList.add(riskCountAndRatioDto);
		riskRatioChartList.add(geoGraphicRisk);
		riskRatioChartList.add(corruptionRisk);
		riskRatioChartList.add(politicalRisk);
		riskRatioChartList.add(productRisk);

		alertDashBoardRespDtoList = new ArrayList<>();
		AlertDashBoardRespDto alertDashBoardRespDto = new AlertDashBoardRespDto(1L, "test", new Date(),
				new Double(1000), "focal Entity id", "search name", CustomerType.IND, "scenario", new Date(), 33L, 20);
		AlertDashBoardRespDto alertDashBoardRespDtoNew = new AlertDashBoardRespDto(2L, "test", new Date(),
				new Double(1000), "focal Entity id", "search name", CustomerType.IND, "scenario", new Date(), 33L, 20);
		alertDashBoardRespDtoList.add(alertDashBoardRespDto);
		alertDashBoardRespDtoList.add(alertDashBoardRespDtoNew);

		AlertedCounterPartyDto alertedCounterPartyDto = new AlertedCounterPartyDto(1L, new Date(), "Internal", "test",
				"", "UK", 50000D, 2L, 1L, "2345.23", "45678.23");
		AlertedCounterPartyDto alertedCounterPartyDtonew = new AlertedCounterPartyDto(2L, new Date(), "Internal",
				"testJunit", "", "US", 10000D, 3L, 2L, "345.23", "5678.23");
		alertedCounterPartyDtoList.add(alertedCounterPartyDto);
		alertedCounterPartyDtoList.add(alertedCounterPartyDtonew);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getCustomerRiskTest() throws ParseException {
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(),Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(riskCountAndRatioDto);
		RiskCountAndRatioDto riskCountAndRatioDto = txDataService.getCustomerRisk(new Date().toString(),
				new Date().toString(), new FilterDto());
		assertEquals("CustomerRisk", riskCountAndRatioDto.getType());
	}

	@Test
	public void getCustomerRiskAggregatestest() {
		Mockito.when(txDataDaoMock.getCustomerRiskAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(list);
		List<RiskCountAndRatioDto> riskCountAndRatioDtoList = txDataService
				.getCustomerRiskAggregates(new Date().toString(), new Date().toString(), new FilterDto());
		assertEquals(2, riskCountAndRatioDtoList.size());
	}

	@Test
	public void getGeoGraphicRiskTest() throws ParseException {
		RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();
		Mockito.when(txDataDaoMock.getRiskAlerts(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(riskCountAndRatioDto);
		RiskCountAndRatioDto riskCountAndRatioDtoResult = txDataService.getGeoGraphicRisk(new Date().toString(),
				new Date().toString(), new FilterDto());
		assertEquals("GeoGraphicRisk", riskCountAndRatioDtoResult.getType());
	}

	@Test
	public void getGeoGraphicRiskAggregatesTest() {
		Mockito.when(txDataDaoMock.getRiskAlertsAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.any(FilterDto.class))).thenReturn(riskCountAndRatioDto);
		List<RiskCountAndRatioDto> riskCountAndRatioDtoresultList = txDataService
				.getGeoGraphicRiskAggregates(new Date().toString(), new Date().toString(), new FilterDto());
		assertEquals(3, riskCountAndRatioDtoresultList.size());
	}

	@Test
	public void getCorruptionRiskAggregatesTest() {
		Mockito.when(txDataDaoMock.getCorruptionRiskAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.any(FilterDto.class))).thenReturn(list);
		List<RiskCountAndRatioDto> riskCountAndRatioDtoresultList = txDataService
				.getCorruptionRiskAggregates(new Date().toString(), new Date().toString(), new FilterDto());
		assertEquals(2, riskCountAndRatioDtoresultList.size());
	}

	@Test
	public void getPoliticalRiskAggregatesTest() {
		Mockito.when(txDataDaoMock.getCorruptionRiskAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.any(FilterDto.class))).thenReturn(list);

		List<RiskCountAndRatioDto> riskCountAndRatioDtoresultList = txDataService
				.getPoliticalRiskAggregates(new Date().toString(), new Date().toString(), new FilterDto());
		assertEquals(2, riskCountAndRatioDtoresultList.size());
	}

	@Test
	public void getCorruptionRiskTest() throws ParseException {

		Mockito.when(txDataDaoMock.getRiskAlerts(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(corruptionRisk);
		RiskCountAndRatioDto riskCountAndRatioDtoResult = txDataService.getCorruptionRisk(new Date().toString(),
				new Date().toString(), new FilterDto());
		assertEquals("CorruptionRisk", riskCountAndRatioDtoResult.getType());
	}

	@Test
	public void getPoliticalRiskTest() throws ParseException {
		Mockito.when(txDataDaoMock.getRiskAlerts(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(politicalRisk);

		RiskCountAndRatioDto riskCountAndRatioDtoResult = txDataService.getPoliticalRisk(new Date().toString(),
				new Date().toString(), new FilterDto());
		assertEquals("PoliticalRisk", riskCountAndRatioDtoResult.getType());
	}

	@Test
	public void getScenariosByTypeTest() throws ParseException {
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(list.get(0));
		Mockito.when(
				alertDaoMock.getGroupByScenario(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(list2);
		ScenariosResponseDto ScenariosResponseDtoResult = txDataService.getScenariosByType(new Date().toString(),
				new Date().toString(), "", "",new FilterDto());
		assertEquals(2, ScenariosResponseDtoResult.getScenariosList().size());
	}

	@Test
	public void getProductRiskTest() throws ParseException {
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(productRisk);

		RiskCountAndRatioDto riskCountAndRatioDtoResult = txDataService.getProductRisk(new Date().toString(),
				new Date().toString(), new FilterDto());
		assertEquals("ProductRisk", riskCountAndRatioDtoResult.getType());
	}

	@Test
	public void getProductRiskAggregates() {
		Mockito.when(alertDaoMock.getProductRiskAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.any(FilterDto.class))).thenReturn(list);

		List<RiskCountAndRatioDto> riskCountAndRatioDtoresultList = txDataService
				.getProductRiskAggregates(new Date().toString(), new Date().toString(), new FilterDto());
		assertEquals(2, riskCountAndRatioDtoresultList.size());
	}

	@Test
	public void getRiskRatioChart_whenRiskIsTrueTest() throws ParseException {
		FilterDto filterDto = new FilterDto();
		filterDto.setProductRisk(true);
		filterDto.setCustomerRiskType(true);
		filterDto.setGeographyRiskType(true);
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(riskCountAndRatioDto);
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(productRisk);
		Mockito.when(txDataDaoMock.getRiskAlerts(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(riskCountAndRatioDto);

		List<RiskCountAndRatioDto> finalRiskCountAndRatioDto = txDataService.getRiskRatioChart(new Date().toString(),
				new Date().toString(), filterDto);
		assertEquals(3, finalRiskCountAndRatioDto.size());
	}

	@Test
	public void getRiskRatioChart_whenRiskIsFalseTest() throws ParseException {
		FilterDto filterDto = new FilterDto();
		filterDto.setProductRisk(false);
		filterDto.setCustomerRiskType(false);
		filterDto.setGeographyRiskType(false);
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(riskCountAndRatioDto);
		Mockito.when(
				txDataDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(productRisk);
		Mockito.when(txDataDaoMock.getRiskAlerts(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(riskCountAndRatioDto);

		List<RiskCountAndRatioDto> finalRiskCountAndRatioDto = txDataService.getRiskRatioChart(new Date().toString(),
				new Date().toString(), filterDto);
		assertEquals(3, finalRiskCountAndRatioDto.size());
	}

	@Test
	public void getRiskAlertsByScenarios() {
		Mockito.when(alertDaoMock.getRiskAlertsByScenarios(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(alertDashBoardRespDtoList);

		List<AlertDashBoardRespDto> alertDashBoardRespDtoResultList = txDataService
				.getRiskAlertsByScenarios(new Date().toString(), new Date().toString(), new FilterDto());
		assertEquals(2, alertDashBoardRespDtoResultList.size());
	}

	@Test
	public void viewAll_ProductRiskTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> ProductRiskList = txDataService.viewAll(new Date().toString(), new Date().toString(),
				1, 10, "ProductRisk", new FilterDto());
		assertEquals(2, ProductRiskList.size());
	}

	@Test
	public void viewAll_CustomerRiskTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> customerRiskList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "CustomerRisk", new FilterDto());
		assertEquals(2, customerRiskList.size());
	}

	@Test
	public void viewAll_ActivityTypeTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> ActivityTypeList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "ActivityType", new FilterDto());
		assertEquals(2, ActivityTypeList.size());
	}

	@Test
	public void viewAll_GeoGraphicRiskTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> GeoGraphicRiskList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "GeoGraphicRisk", new FilterDto());
		assertEquals(2, GeoGraphicRiskList.size());
	}

	@Test
	public void viewAll_PoliticalRiskTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> PoliticalRiskList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "PoliticalRisk", new FilterDto());
		assertEquals(2, PoliticalRiskList.size());
	}

	@Test
	public void viewAll_CorruptionRiskTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> CorruptionRiskList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "CorruptionRisk", new FilterDto());
		assertEquals(2, CorruptionRiskList.size());
	}

	@Test
	public void viewAll_BanksTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> BanksList = txDataService.viewAll(new Date().toString(), new Date().toString(), 1,
				10, "Banks", new FilterDto());
		assertEquals(2, BanksList.size());
	}

	@Test
	public void viewAll_GeoGraphicTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> GeoGraphicList = txDataService.viewAll(new Date().toString(), new Date().toString(),
				1, 10, "GeoGraphic", new FilterDto());
		assertEquals(2, GeoGraphicList.size());
	}

	@Test
	public void viewAll_BankLocationsTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> BankLocationsList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "BankLocations", new FilterDto());
		assertEquals(2, BankLocationsList.size());
	}

	@Test
	public void viewAll_CorporateStructureTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> CorporateStructureList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "CorporateStructure", new FilterDto());
		assertEquals(2, CorporateStructureList.size());
	}

	@Test
	public void viewAll_ShareholdersTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> ShareholdersList = txDataService.viewAll(new Date().toString(),
				new Date().toString(), 1, 10, "Shareholders", new FilterDto());
		assertEquals(2, ShareholdersList.size());
	}

	@Test
	public void viewAll_GeographyTest() {
		Mockito.when(txDataDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(list);
		List<RiskCountAndRatioDto> GeographyList = txDataService.viewAll(new Date().toString(), new Date().toString(),
				1, 10, "Geography", new FilterDto());
		assertEquals(2, GeographyList.size());
	}

	@Test
	public void counterPartyInfo() {
		Mockito.when(txDataDaoMock.getCustomerTransactions(Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(),
				Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.any(FilterDto.class)))
				.thenReturn(alertedCounterPartyDtoList);

		CounterPartyInfoDto counterPartyInfoDtoresult = txDataService.counterPartyInfo(new Date().toString(),
				new Date().toString(), 1L, new FilterDto());
		assertEquals(2, counterPartyInfoDtoresult.getCustomer().size());
	}
}
