package element.bst.elementexploration.rest.logging.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.logging.dao.AuditTrailDao;
import element.bst.elementexploration.rest.logging.domain.AuditTrail;

/**
 * 
 * @author Amit Patel
 *
 */
@Repository("auditTrailDao")
public class AuditTrailDaoImpl extends GenericDaoImpl<AuditTrail, Long> implements AuditTrailDao{

	public AuditTrailDaoImpl() {
		super(AuditTrail.class);
	}
}
