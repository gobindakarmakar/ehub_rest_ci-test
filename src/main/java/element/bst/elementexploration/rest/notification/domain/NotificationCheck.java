package element.bst.elementexploration.rest.notification.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "bst_notification_check")
@Access(AccessType.FIELD)
public class NotificationCheck implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7385694767611867559L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "timestamp")
	private LocalDateTime timestamp;

	@Column(name = "type")
	private int type;

	/**
	 * 
	 */
	public NotificationCheck() {
		super();
	}

	public NotificationCheck(Long userId, LocalDateTime timestamp, int type) {
		super();
		this.timestamp = timestamp;
		this.userId = userId;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
