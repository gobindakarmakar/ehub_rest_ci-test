package element.bst.elementexploration.rest.generic.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.util.FilterColumnDto;
import element.bst.elementexploration.rest.util.FilterModelDto;
import element.bst.elementexploration.rest.util.RequestFilterDto;

/**
 * 
 * @author Amit Patel
 *
 */
@Service("genericService")
public class GenericServiceImpl<E, I> implements GenericService<E, I>{

	
	protected GenericDao<E, I>  genericDao;
	
	public GenericServiceImpl() {
		
	}
	public GenericServiceImpl(GenericDao<E, I> genericDao) {
		super();
		this.genericDao = genericDao;
	}

	@Override
	@Transactional(transactionManager = "transactionManager", readOnly=true)
	public E find(I id) {
		return genericDao.find(id);
	}

	@Override
	@Transactional("transactionManager")
	public E save(E e) {
		return genericDao.create(e);
	}
	
	@Override
	@Transactional("transactionManager")
	public Iterable<E> saveAll(Iterable<E> e) {
		return genericDao.saveAll(e);
	}

	@Override
	@Transactional("transactionManager")
	public void saveOrUpdate(E e) {
		genericDao.saveOrUpdate(e);
	}

	@Override
	@Transactional("transactionManager")
	public void delete(E e) {
		genericDao.delete(e);
	}
	
	@Override
	@Transactional(transactionManager = "transactionManager", readOnly=true)
	public List<E> findAll() {		
		return genericDao.findAll();
	}
	
	@Override
	@Transactional("transactionManager")
	public void update(E e) {
		genericDao.update(e);
	}
	
	@Override
	@Transactional("transactionManager")
	public void flush() {
		genericDao.flush();
	}

	@SuppressWarnings("null")
	@Override
	@Transactional("transactionManager")
	public Map<List<E>, Long> filterData(E e, FilterModelDto dto, Integer pageNumber, Integer recordsPerPage,Long totalResults, String orderIn,
			String orderBy) {
		String filterString = dto.getFilterModel();
		JSONObject filterJson = new JSONObject(filterString);
		JSONArray jarray = filterJson.names();
		
		List<FilterColumnDto> columnsList = new ArrayList<FilterColumnDto>();
		try{
			if(jarray != null){
			//Loop the main JSONObject for getting columns list
			for (int i = 0; i < jarray.length(); i++) {
				FilterColumnDto columndto = new FilterColumnDto();
				JSONObject columnObject = filterJson.getJSONObject(jarray.getString(i));
				columndto.setColumn(jarray.getString(i));
				if(columnObject.has("operator"))
					columndto.setOperator(columnObject.getString("operator"));
				
				//extract each filter condition 1 info and add to the respective column filter
				RequestFilterDto condition1 = new RequestFilterDto();
				JSONObject condition1Object =  new JSONObject();
				if(columnObject.has("condition1")){
					condition1Object = columnObject.getJSONObject("condition1");
					if(condition1Object.has("filterType"))
						condition1.setFilterType(condition1Object.getString("filterType"));
					if(condition1Object.has("type"))
						condition1.setSubType(condition1Object.getString("type"));
					if(condition1Object.has("filter") ){
						if(condition1Object.get("filter") instanceof Integer){
							String filter1[] = new String[5];
							filter1[0] = String.valueOf(condition1Object.getInt("filter"));
							if(condition1Object.has("filterTo") && condition1Object.get("filterTo") instanceof Integer){
								filter1[1] = String.valueOf(condition1Object.getInt("filterTo"));
							}
							condition1.setKeyword(filter1);
						}else{
							if(condition1Object.getString("filter").contains("#")){
								String filter1[] = condition1Object.getString("filter").split("#");
								condition1.setKeyword(filter1);
							}else{
								String filter1[] = new String[1];
								filter1[0]=condition1Object.getString("filter");
								condition1.setKeyword(filter1);
							}
						}
					}
					condition1.setEntityColumn(jarray.getString(i));
				}
				
				//extract each filter condition 2 info and add to the respective column filter
				RequestFilterDto condition2 = new RequestFilterDto();
				JSONObject condition2Object =  new JSONObject();
				if(columnObject.has("condition2")){
					condition2Object = columnObject.getJSONObject("condition2");
					if(condition2Object.has("filterType"))
						condition2.setFilterType(condition2Object.getString("filterType"));
					if(condition2Object.has("type"))
						condition2.setSubType(condition2Object.getString("type"));
					if(condition2Object.has("filter")){
						if(condition2Object.get("filter") instanceof Integer){
							String filter2[] = new String[1];
							filter2[0] = String.valueOf(condition2Object.getInt("filter"));
							if(condition2Object.has("filterTo") && condition2Object.get("filterTo") instanceof Integer){
								filter2[1] = String.valueOf(condition2Object.getInt("filterTo"));
							}
							condition2.setKeyword(filter2);
						}else{
							if(condition2Object.getString("filter").contains("#")){
								String filter2[] = condition2Object.getString("filter").split("#");
								condition2.setKeyword(filter2);
							}else{
								String filter2[] = new String[1];
								filter2[0]=condition2Object.getString("filter");
								condition2.setKeyword(filter2);
							}
						}
					}
					condition2.setEntityColumn(jarray.getString(i));
				}
				
				//add each condition filter request to this column filter
				columndto.setCondition1(condition1);
				columndto.setCondition2(condition2);
				
				//add this column filter to the list 
				columnsList.add(columndto);
			}
			}
		}catch(Exception  e1){
			e1.printStackTrace();
		}
		
		return genericDao.filterData(e, columnsList, pageNumber, recordsPerPage, totalResults, orderIn, orderBy);
	}
	

}
