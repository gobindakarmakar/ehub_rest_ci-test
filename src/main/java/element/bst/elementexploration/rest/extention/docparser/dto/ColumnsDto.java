package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author rambabu
 *
 */
public class ColumnsDto implements Serializable
{

	private static final long serialVersionUID = 1L;
	@JsonProperty("columData")
	private ColumDataDto columDataDto;
	
	public ColumnsDto() {
		super();		
	}

	public ColumDataDto getColumDataDto() {
		return columDataDto;
	}

	public void setColumDataDto(ColumDataDto columDataDto) {
		this.columDataDto = columDataDto;
	}
	
	

	
}
