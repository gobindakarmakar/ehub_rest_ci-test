package element.bst.elementexploration.rest.extention.transactionintelligence.enums;

/**
 * @author suresh
 *
 */
public enum EntityType {

	TRANSACTION,
	PERSON,
	ORGANIZATION
} 
