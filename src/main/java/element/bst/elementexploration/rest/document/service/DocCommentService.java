package element.bst.elementexploration.rest.document.service;

import element.bst.elementexploration.rest.document.domain.DocComment;
import element.bst.elementexploration.rest.document.dto.DocCommentVo;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface DocCommentService extends GenericService<DocComment, Long>{

	void editDocmentComment(Long commentId, long userId, DocCommentVo docCommentDto);
}
