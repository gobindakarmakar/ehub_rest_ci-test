package element.bst.elementexploration.test.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({ @PropertySource(value = "classpath:ehubrest.properties"),
	@PropertySource(value = "classpath:serviceurls.properties"),
	@PropertySource(value = "classpath:user.properties")/*, @PropertySource(value = "classpath:kieServer.properties"),
	@PropertySource(value = "classpath:activiti-app.properties")*/ })
public class PropertiesConfig {

}
