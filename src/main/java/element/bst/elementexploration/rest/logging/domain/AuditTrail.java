package element.bst.elementexploration.rest.logging.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Audit Trail and View Trail
 * 
 * @author Amit Patel
 *
 */
@Entity
@Table(name = "bst_audit_trail")
@Access(AccessType.FIELD)
public class AuditTrail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Primary Key. Trail Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "trailId")
	private Long trailId;

	@Column(name = "timestamp")
	private Date timestamp;

	@Column(name = "ipAddress")
	private String ipAddress;
	
	@Column(name = "requestDetails", columnDefinition="LONGTEXT")
	private String requestDetails;
	
	@Column(name = "requestBody", columnDefinition="LONGTEXT")
	private String requestBody;
	
	@Column(name = "responseBody", columnDefinition="LONGTEXT")
	private String responseBody;
	
	@Column(name = "responseStatus")
	private int responseStatus;
	
	@Column(name = "userId")
	private Long userId;
	
	@Column(name = "level")
	private String level;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "exceptionClass")
	private String exceptionClass;

	public AuditTrail(Date timestamp, String ipAddress, HttpServletRequest httpServletRequest, String requestBody,
			String responseBody, int responseStatus, Long userId, String level, String type, String exceptionClass) {
		super();
		this.timestamp = timestamp;
		this.ipAddress = ipAddress;
		this.requestDetails = addHttpRequestInfo(httpServletRequest);
		this.requestBody = requestBody;
		this.responseBody = responseBody;
		this.responseStatus = responseStatus;
		this.userId = userId;
		this.level = level;
		this.type = type;
		this.exceptionClass = exceptionClass;
	}

	public Long getTrailId() {
		return trailId;
	}

	public void setTrailId(Long trailId) {
		this.trailId = trailId;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getRequestDetails() {
		return requestDetails;
	}

	public void setRequestDetails(String requestDetails) {
		this.requestDetails = requestDetails;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public int getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(int responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExceptionClass() {
		return exceptionClass;
	}

	public void setExceptionClass(String exceptionClass) {
		this.exceptionClass = exceptionClass;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	/**
	 * Get HttpRequest Info
	 * 
	 * @param httpServletRequest
	 * @return
	 */
	private static String addHttpRequestInfo(HttpServletRequest httpServletRequest) {
		StringBuffer requestInfo = new StringBuffer();
		requestInfo.append("Http Method =" + httpServletRequest.getMethod());
		requestInfo.append(" ; ");
		requestInfo.append("Http Request =" + httpServletRequest.getRequestURI());
		requestInfo.append(" ; ");
		requestInfo.append("Http Paramenters: ");
		if (httpServletRequest.getParameterNames() != null) {
			Enumeration<String> parameterList = httpServletRequest.getParameterNames();
			while (parameterList.hasMoreElements()) {
				String key = parameterList.nextElement();
				if(!"token".equalsIgnoreCase(key))
					requestInfo.append(key + "=" + httpServletRequest.getParameter(key));
			}
		}
		
		return requestInfo.toString();
	}
	
}
