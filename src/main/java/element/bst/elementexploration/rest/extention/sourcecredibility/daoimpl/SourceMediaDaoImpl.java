package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceMediaDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("sourceMediaDao")
public class SourceMediaDaoImpl extends GenericDaoImpl<SourceMedia, Long> implements SourceMediaDao {

	public SourceMediaDaoImpl() {
		super(SourceMedia.class);
	}

	@Override
	public SourceMedia fetchMedia(String name) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceMedia s where s.mediaName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", name);
			SourceMedia sourcesMedia = (SourceMedia) query.getSingleResult();
			if (sourcesMedia != null)
				return sourcesMedia;
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourceMediaDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SourceMedia> fetchMediaListExceptAll() {
		List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceMedia s where s.mediaName <>:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", "All");
			mediaList = (List<SourceMedia>) query.getResultList();
			return mediaList;
		} catch (Exception e) {
			return mediaList;
		}
	}

}
