package element.bst.elementexploration.rest.settings.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class SystemSettingsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	// private String name;
	@JsonProperty("General Settings")
	private List<SettingsDto> generalSettingsList;
	@JsonProperty("Mail Settings")
	private List<SettingsDto> mailSettingsList;
	@JsonProperty("List Management Settings")
	private List<SettingsDto> listManagementSettingsList;
	
	@JsonProperty("Alert Management Settings")
	private List<SettingsDto> alertManagementSettingsList;
	
	@JsonProperty("User Management Regulation")
	private List<SettingsDto> userManagementSettingsList;

	public List<SettingsDto> getGeneralSettingsList() {
		return generalSettingsList;
	}

	public void setGeneralSettingsList(List<SettingsDto> generalSettingsList) {
		this.generalSettingsList = generalSettingsList;
	}

	public List<SettingsDto> getMailSettingsList() {
		return mailSettingsList;
	}

	public void setMailSettingsList(List<SettingsDto> mailSettingsList) {
		this.mailSettingsList = mailSettingsList;
	}

	public List<SettingsDto> getListManagementSettingsList() {
		return listManagementSettingsList;
	}

	public void setListManagementSettingsList(List<SettingsDto> listManagementSettingsList) {
		this.listManagementSettingsList = listManagementSettingsList;
	}

	public List<SettingsDto> getAlertManagementSettingsList() {
		return alertManagementSettingsList;
	}

	public void setAlertManagementSettingsList(List<SettingsDto> alertManagementSettingsList) {
		this.alertManagementSettingsList = alertManagementSettingsList;
	}

	public List<SettingsDto> getUserManagementSettingsList() {
		return userManagementSettingsList;
	}

	public void setUserManagementSettingsList(List<SettingsDto> userManagementSettingsList) {
		this.userManagementSettingsList = userManagementSettingsList;
	}

	
}
