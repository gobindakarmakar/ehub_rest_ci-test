package element.bst.elementexploration.rest.recentsearch.dto;

import java.util.Date;

public class RecentSearchDto {
	
	private String search;
		
	private Long userId;
	
	private Date createdDate;

	public RecentSearchDto(String search, Long userId, Date createdDate) {
		super();
		this.search = search;
		this.userId = userId;
		this.createdDate = createdDate;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	

}
