package element.bst.elementexploration.rest.extention.staging.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.staging.domain.OrgStaging;
import element.bst.elementexploration.rest.extention.staging.service.StagingService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("stagingService")
public class StagingServiceImpl implements StagingService {

	@Value("${bigdata_org_url}")
	private String BIG_DATA__ORG_URL;

	@Override
	public String getOrgStaging(String status) throws Exception {
		String statusEncode = java.net.URLEncoder.encode(status, "UTF-8").replaceAll("\\+", "%20");
		String url = BIG_DATA__ORG_URL + "org/staging?status=" + statusEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String saveOrgStaging(OrgStaging orgStaging) throws Exception {
		String url = BIG_DATA__ORG_URL + "org/staging";
		ObjectMapper mapper = new ObjectMapper();
		String data = mapper.writeValueAsString(orgStaging);
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, data);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

}
