package element.bst.elementexploration.rest.extention.significantnews.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rambabu
 *
 */
public class SignificantnewsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@JsonProperty(required = true)
	private String entityId;
	@JsonProperty(required = true)
	private String entityName;
	@JsonProperty(required = true)
	private String publishedDate;
	@JsonProperty(required = true)
	private String title;
	@JsonProperty(required = true)
	private String url;
	@JsonProperty(required = true)
	private String newsClass;
	private Long userId;
	private Boolean isSignificantNews;
	private String sentiment;
	private String comment;

	public SignificantnewsDto() {
		super();
	}

	public SignificantnewsDto(Long id, String entityId, String entityName, String publishedDate, String title,
			String url, String newsClass, Long userId, Boolean isSignificantNews, String sentiment, String comment) {
		super();
		this.id = id;
		this.entityId = entityId;
		this.entityName = entityName;
		this.publishedDate = publishedDate;
		this.title = title;
		this.url = url;
		this.newsClass = newsClass;
		this.userId = userId;
		this.isSignificantNews = isSignificantNews;
		this.sentiment = sentiment;
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNewsClass() {
		return newsClass;
	}

	public void setNewsClass(String newsClass) {
		this.newsClass = newsClass;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getIsSignificantNews() {
		return isSignificantNews;
	}

	public void setIsSignificantNews(Boolean isSignificantNews) {
		this.isSignificantNews = isSignificantNews;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
