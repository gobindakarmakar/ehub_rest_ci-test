package element.bst.elementexploration.test.extention.sourcecredibility.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceMediaDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceMediaDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl.SourceMediaServiceImpl;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SourceMediaServiceTest {

	@Mock
	SourceMediaDao sourceMediaDao;

	@InjectMocks
	SourceMediaServiceImpl sourceMediaService;

	List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
	SourceMedia sourceMediaOne = null;
	SourceMediaDto sourceMediaDto = null;

	@Before
	public void setupMock() {
		sourceMediaOne = new SourceMedia("Excel");
		sourceMediaDto = new SourceMediaDto("PPT");
		SourceMedia sourceMediaTwo = new SourceMedia("Movies");
		mediaList.add(sourceMediaOne);
		mediaList.add(sourceMediaTwo);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getClassifications() {
		Mockito.when(sourceMediaDao.findAll()).thenReturn(mediaList);
		List<SourceMediaDto> mediaCheck = sourceMediaService.getSourceMedia();
		Assert.assertEquals(mediaCheck.size(), mediaList.size());
	}

	@Test
	public void saveSourceMedia() throws Exception {
		Mockito.when(sourceMediaDao.create(Mockito.anyObject())).thenReturn(sourceMediaOne);
		Mockito.when(sourceMediaDao.findAll()).thenReturn(mediaList);
		List<SourceMediaDto> mediaCheck = sourceMediaService.saveSourceMedia(sourceMediaDto);
		Assert.assertEquals(mediaCheck.size(), mediaList.size());
	}

}
