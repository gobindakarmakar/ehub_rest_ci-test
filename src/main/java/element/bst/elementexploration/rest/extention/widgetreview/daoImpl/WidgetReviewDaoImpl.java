package element.bst.elementexploration.rest.extention.widgetreview.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.widgetreview.dao.WidgetReviewDao;
import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author rambabu
 *
 */
@Repository("widgetReviewDao")
public class WidgetReviewDaoImpl extends GenericDaoImpl<WidgetReview, Long> implements WidgetReviewDao {

	public WidgetReviewDaoImpl() {
		super(WidgetReview.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WidgetReview> getWidgetReviewsByEntityId(String entityId) {
		List<WidgetReview> WidgetReviewList = new ArrayList<WidgetReview>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from WidgetReview wr where wr.entityId =:entityId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			WidgetReviewList = (List<WidgetReview>) query.getResultList();
			return WidgetReviewList;
		} catch (Exception e) {
			return WidgetReviewList;
		}
	}

	@Override
	public WidgetReview getWidgetWithEntityIdAndWidgetId(String entityId, Long widgetId) {
		WidgetReview widgetReview = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from WidgetReview wr where wr.entityId =:entityId and wr.widget.id =:widgetId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("widgetId", widgetId);
			widgetReview = (WidgetReview) query.getSingleResult();
			return widgetReview;
		} catch (Exception e) {
			return widgetReview;
		}
	}

}
