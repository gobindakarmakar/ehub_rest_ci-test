package element.bst.elementexploration.test.document.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.document.daoImpl.DocCommentDaoImpl;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DocCommentDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@InjectMocks
	DocCommentDaoImpl docCommentDao;

	List<DocCommentDTO> docCommentDtoList = new ArrayList<DocCommentDTO>();

	@Before
	public void doSetup() {
		docCommentDtoList.add(new DocCommentDTO(1L, "test", "test cases", new Date(), new Date(), "testing"));
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void listDocCommentTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(docCommentDtoList);
		List<DocCommentDTO> docCommentDtoListNew = docCommentDao.listDocComment(1L, 1L, 1, 15, "desc");
		assertEquals(1, docCommentDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void listDocComment_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		docCommentDao.listDocComment(1L, 1L, 1, 10, "desc");
	}

	@Test
	public void countDocCommentTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2);
		long count = docCommentDao.countDocComment(1L, 1L);
		assertEquals(2l, count);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countDocComment_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		docCommentDao.countDocComment(1L, 1L);
	}

}
