package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceJurisdictionDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourceJurisdictionService extends GenericService<SourceJurisdiction, Long> {

	List<SourceJurisdictionDto> saveSourceJurisdiction(SourceJurisdictionDto jurisdictionDto) throws Exception;

	List<SourceJurisdictionDto> getSourceJurisdiction();
	
	public SourceJurisdiction fetchJurisdiction(String name);

	SourceJurisdiction create(SourceJurisdiction jurisdictionNew);

	SourceJurisdiction findByListItemId(Long listItemId);
	
	int updateDBCollation(String schemaName);
	
	List<String> getNonUTF8Tables(String schemaName);
	
	int updateTableCollation(String tableName);
	
	int updateTables(String dbUrl);

}
