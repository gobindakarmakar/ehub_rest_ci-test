package element.bst.elementexploration.rest.extention.entity.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.entity.dao.EntityTypeDao;
import element.bst.elementexploration.rest.extention.entity.domain.EntityType;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("entityTypeDao")
public class EntityTypeDaoImpl extends GenericDaoImpl<EntityType, Long> implements EntityTypeDao {

	public EntityTypeDaoImpl() {
		super(EntityType.class);
	}

}
