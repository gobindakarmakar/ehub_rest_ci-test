package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class PossibleAnswersDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String possibleAnswer;
	private String answerPosition;
	private String answerPositionOnThePage;
	private String answerUUID;
	private Boolean deleted;

	public PossibleAnswersDto() {
	}

	public PossibleAnswersDto(Long id, String possibleAnswer, String answerPosition, String answerPositionOnThePage,
			String answerUUID, Boolean deleted) {
		super();
		this.id = id;
		this.possibleAnswer = possibleAnswer;
		this.answerPosition = answerPosition;
		this.answerPositionOnThePage = answerPositionOnThePage;
		this.answerUUID = answerUUID;
		this.deleted = deleted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPossibleAnswer() {
		return possibleAnswer;
	}

	public void setPossibleAnswer(String possibleAnswer) {
		this.possibleAnswer = possibleAnswer;
	}

	public String getAnswerPosition() {
		return answerPosition;
	}

	public void setAnswerPosition(String answerPosition) {
		this.answerPosition = answerPosition;
	}

	public String getAnswerPositionOnThePage() {
		return answerPositionOnThePage;
	}

	public void setAnswerPositionOnThePage(String answerPositionOnThePage) {
		this.answerPositionOnThePage = answerPositionOnThePage;
	}

	public String getAnswerUUID() {
		return answerUUID;
	}

	public void setAnswerUUID(String answerUUID) {
		this.answerUUID = answerUUID;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
