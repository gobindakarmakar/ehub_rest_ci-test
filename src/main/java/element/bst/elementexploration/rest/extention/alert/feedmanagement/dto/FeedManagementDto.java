package element.bst.elementexploration.rest.extention.alert.feedmanagement.dto;

import java.io.Serializable;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author hanuman
 *
 */
public class FeedManagementDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long feed_management_id;
	
	@ApiModelProperty(value = "Alert Id", required = true)
	@NotEmpty(message = "feed name can not be blank.")
	private String feedName;
	
	private String color;
	private Integer type;
	private Integer source;
	private List<FeedGroups> groupLevels;
	private Integer assignedAlertsCount=0;

	private Boolean isReviewerRequired = false;
	private List<ListItem> reviewer;
	
	public Long getFeed_management_id() {
		return feed_management_id;
	}
	public void setFeed_management_id(Long feed_management_id) {
		this.feed_management_id = feed_management_id;
	}
	public String getFeedName() {
		return feedName;
	}
	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getSource() {
		return source;
	}
	public List<FeedGroups> getGroupLevels() {
		return groupLevels;
	}
	public void setGroupLevels(List<FeedGroups> groupLevels) {
		this.groupLevels = groupLevels;
	}
	public void setSource(Integer source) {
		this.source = source;
	}
	public Integer getAssignedAlertsCount() {
		return assignedAlertsCount;
	}
	public void setAssignedAlertsCount(Integer assignedAlertsCount) {
		this.assignedAlertsCount = assignedAlertsCount;
	}
	public Boolean getIsReviewerRequired() {
		return isReviewerRequired;
	}
	public void setIsReviewerRequired(Boolean isReviewerRequired) {
		this.isReviewerRequired = isReviewerRequired;
	}
	public List<ListItem> getReviewer() {
		return reviewer;
	}
	public void setReviewer(List<ListItem> reviewer) {
		this.reviewer = reviewer;
	}
	
}
