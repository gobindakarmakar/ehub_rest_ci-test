package element.bst.elementexploration.rest.logging.service;

import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.util.LogRequest;

public interface AuditLogService extends GenericService<AuditLog, Long>{

	List<AuditLog> getLogs(LogRequest logRequest);
	
	List<AuditLog> getLogsByTypeId(String type,Long typeId);

	Long countLogs(LogRequest logRequest);

	List<LogAggregator> logAggregator(LogRequest logRequest);

	AuditLog mailLogging(DelegateExecution execution, Exception e);

	List<String> getTypes();

	AuditLog auditLogForGeneratedReport(String entityId,String entityName,long userId);

	AuditLog auditLogForOverviewAPI(String entityId,String entityName,long userId);

	Date getLatestStatusChangeOfAlert(Long alertId);

	List<AuditLog> getLogsByTypeAndSubtype(String type, String subType);

	List<AuditLog> getLogsByTitleAndTypeId(String title, Long typeId);
}
