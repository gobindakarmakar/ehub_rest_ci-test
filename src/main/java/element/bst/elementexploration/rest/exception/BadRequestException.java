package element.bst.elementexploration.rest.exception;

public class BadRequestException extends RuntimeException{

    /**
     * 
     */
    private static final long serialVersionUID = -5972445803860979255L;
    private  String message;;
    
    public static final String CASE_SEED_NOT_FOUND_MSG="Error while fetching data";
	
	public BadRequestException(String message) {
		super();
		this.message = message;
	}



	public BadRequestException() {

		super();
		message = CASE_SEED_NOT_FOUND_MSG;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return message;
	}

}
