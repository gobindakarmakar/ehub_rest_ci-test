package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CounterPartyService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;

@Service("counterPartyService")
public class CounterPartyServiceImpl implements CounterPartyService {


	@Autowired
	private TxDataDao txDao;
	
	@Autowired
	private TxDataService txDataService;
	
	@Autowired
	private  AlertDao alertDao;
	
	@Autowired
	private  AccountDao accountDao;
	

	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getActivity(String fromDate, String toDate,FilterDto filterDto) throws ParseException {
		filterDto.setActivityType(true);
		RiskCountAndRatioDto response=txDao.getCustomerRisk(fromDate, toDate,null, filterDto);
		response.setType("ActivityType");
		return response;
	}
	
	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getGeoGraphicAggregates(String fromDate, String toDate,FilterDto filterDto,boolean isBankCountry) throws ParseException {
		filterDto.setCounterPartyCountry(true);
		List<RiskCountAndRatioDto> response=txDao.getCounterPartyGeoAggregates(fromDate, toDate,filterDto,isBankCountry );
		return response;
	}
	
	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getBanks(String fromDate, String toDate,FilterDto filterDto) throws ParseException {
		filterDto.setBank(true);
		RiskCountAndRatioDto response=txDao.getCustomerRisk(fromDate, toDate,null,filterDto);
		response.setType("Banks");
		return response;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getBankAggregates(String fromDate, String toDate, FilterDto filterDto) {
		filterDto.setBank(true);
		List<RiskCountAndRatioDto> response=txDao.getBankAggregates(fromDate, toDate, filterDto);
		return response;
	}

	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getGeoGraphic(String fromDate, String toDate, FilterDto filterDto,boolean isBankCountry) throws ParseException {
		filterDto.setCounterPartyCountry(true);
		if(isBankCountry)
			filterDto.setBank(true);
		RiskCountAndRatioDto response=txDao.getCustomerRisk(fromDate, toDate,null, filterDto);
		response.setType("GeoGraphic");
		return response;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getCouterPartyAggregates(String fromDate, String toDate) {
		return txDao.getCustomerRiskAggregates(fromDate, toDate, null);
	}

	@Override
	@Transactional("transactionManager")
	public Map<Integer, List<RiskCountAndRatioDto>> getBankByType(String fromDate, String toDate, boolean falg,String type) {
		Map<Integer, List<RiskCountAndRatioDto>> map=new HashMap<>();
		RiskCountAndRatioDto response=txDao.getBankByType(fromDate, toDate, falg,type);
		 List<RiskCountAndRatioDto> list=new ArrayList<>();
		 List<String> scenarios=new ArrayList<>();		
		List<RiskCountAndRatioDto> responseList=txDao.getBankAggregates(fromDate,toDate,falg,true,type);
		for (RiskCountAndRatioDto riskCountAndRatioDto : responseList) {	
			scenarios.add(riskCountAndRatioDto.getType());
		}
		response.setType(type);
		//response.setScenario(scenarios);
		list.add(response);
		map.put(new Integer(1),list);
		map.put(new Integer(2),responseList);
		
		return map;
	}

	/*@Override
	@Transactional("transactionManager")
	public Map<Integer, List<RiskCountAndRatioDto>> getCouterPartyByType(String fromDate, String toDate, String type) throws ParseException {
		Map<Integer, List<RiskCountAndRatioDto>> map=txDataService.getScenariosByType(fromDate,toDate,type,null);
		map.get(1).get(0).setType(type);;
		return map;
	}*/

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getViewAll(String fromDate, String toDate, String type,Integer pageNumber,
			Integer recordsPerPage,FilterDto filterDto) {
		
		List<RiskCountAndRatioDto> transactionList = new ArrayList<>();
		List<RiskCountAndRatioDto> alertList = new ArrayList<>();
		if ("Activity".equalsIgnoreCase(type)) {
			transactionList = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,filterDto,null);
			alertList = txDao.viewAll(fromDate, toDate, false,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,filterDto,null);
			for (RiskCountAndRatioDto riskCountAndRatioDto : alertList) {
				for (RiskCountAndRatioDto transaction : transactionList) {
					int value = (riskCountAndRatioDto.getBusinessDate()).compareTo(transaction.getBusinessDate());
					if (value == 0)
						transaction.setAlertAmount(riskCountAndRatioDto.getAlertAmount());
				}

			}
		}
		
		if ("Banks".equalsIgnoreCase(type)) {
			transactionList = txDao.getViewAllForBanks(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
			alertList = txDao.getViewAllForBanks(fromDate, toDate, false,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
			for (RiskCountAndRatioDto riskCountAndRatioDto : alertList) {
				for (RiskCountAndRatioDto transaction : transactionList) {
					int value = (riskCountAndRatioDto.getBusinessDate()).compareTo(transaction.getBusinessDate());
					if (value == 0)
						transaction.setAlertAmount(riskCountAndRatioDto.getAlertAmount());

				}

			}
		}
		
		if("GeoGraphic".equalsIgnoreCase(type) || "BankLocations".equalsIgnoreCase(type)){

			transactionList = txDao.getViewAllCountry(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
			alertList = txDao.getViewAllCountry(fromDate, toDate, false,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
			for (RiskCountAndRatioDto riskCountAndRatioDto : alertList) {
				for (RiskCountAndRatioDto transaction : transactionList) {
					int value = (riskCountAndRatioDto.getBusinessDate()).compareTo(transaction.getBusinessDate());
					if (value == 0)
						transaction.setAlertAmount(riskCountAndRatioDto.getAlertAmount());

				}

			}
		
		}
		
		return transactionList;
	}

	@Override
	@Transactional("transactionManager")
	public List<CounterPartyNotiDto> getAlertByScenarios(String fromDate, String toDate, String type,
			List<String> scenarios,boolean isCountry) {		
		List<CounterPartyNotiDto> customerDetailsList = alertDao.getAlertByScenarios(fromDate, toDate, type, scenarios,isCountry);		
		return customerDetailsList;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, List<RiskCountAndRatioDto>> getTopCounterParties(String fromDate, String toDate) {
		 Map<String, List<RiskCountAndRatioDto>> map=new HashMap<String, List<RiskCountAndRatioDto>>();
		 List<RiskCountAndRatioDto> topActivities=txDao.getTopCustomerRisks(fromDate, toDate);
		 List<RiskCountAndRatioDto> topCountries=alertDao.getTopCounterParties(fromDate,toDate);
		 List<RiskCountAndRatioDto> topBanks=alertDao.getTopBanks(fromDate,toDate);
		 map.put("TopActivities", topActivities);
		 map.put("TopCountries", topCountries);
		 map.put("TopBanks", topBanks);


		return map;
	}

	@Override
	@Transactional("transactionManager")
	public Long getViewAllCount(String fromDate, String toDate, String type) {
		
		long count = 0;

		if ("Activity".equalsIgnoreCase(type) || "ProductRisk".equalsIgnoreCase(type)) {
			//count = txDao.getViewAllRiskCount(fromDate, toDate, true,null);

		}else if ("Banks".equalsIgnoreCase(type) || "GeoGraphic".equalsIgnoreCase(type) || "BankLocations".equalsIgnoreCase(type)) {
			count = txDao.getViewAllCountBanks(fromDate, toDate, true);
			
		}else if("GeoGraphic".equalsIgnoreCase(type) || "BankLocations".equalsIgnoreCase(type)){
			count = txDao.getViewAllContry(fromDate, toDate, true);

		}

		return count;

	}

	@Override
	@Transactional("transactionManager")
	public boolean addBankNames() {
		List<TransactionsData> list=txDao.findAll();
		for (TransactionsData transactionsData : list) {
			transactionsData.setBenficiaryBankName(accountDao.fetchAccount((transactionsData.getBeneficiaryAccountId())).getBankName());
		}
		return true;
	}

}
