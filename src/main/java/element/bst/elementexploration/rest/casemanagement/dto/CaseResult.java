package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Case Result")
public class CaseResult implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = -4141902005108846343L;
    @ApiModelProperty(value="Case List")
    private List<CaseDto> result;
    
    @ApiModelProperty(value="Page Information")
    private PaginationInformation paginationInformation;
    
    public CaseResult() {
	// TODO Auto-generated constructor stub
    }

    public List<CaseDto> getResult() {
        return result;
    }

    public void setResult(List<CaseDto> result) {
        this.result = result;
    }

    public PaginationInformation getPaginationInformation() {
        return paginationInformation;
    }

    public void setPaginationInformation(PaginationInformation paginationInformation) {
        this.paginationInformation = paginationInformation;
    }

  
}
