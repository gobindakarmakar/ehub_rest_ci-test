package element.bst.elementexploration.rest.settings.listManagement.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;

public interface ListItemService extends GenericService<ListItem, Long> {

	List<ListItemDto> getListItemByListTypeAndsearchKey(String listType,String searchKey) throws Exception;

	ListItemDto saveOrUpdateListItem(ListItemDto listItemDto)
			throws Exception;
	
	ListItemDto getListItemByID(Long listItemId) throws Exception;
	
	List<ListItemDto> getListItemByListType(String listType) throws Exception;
	
	boolean deleteListItem(Long listItemId);
	
	ListItemDto getListItemByListTypeAndDisplayName(String listType,String displayName) throws Exception;
	
	ListItemDto getListItemByListTypeAndCode(String listType,String code) throws Exception;

	ListItemDto getListItemByListTypeAndLikeDisplayName(String listType, String displayName);
	
}
