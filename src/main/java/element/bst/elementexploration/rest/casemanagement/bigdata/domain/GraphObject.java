package element.bst.elementexploration.rest.casemanagement.bigdata.domain;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class GraphObject {

	// This stores the deserialized JSON data
	private LinkedTreeMap<String, Object> data;

	@SuppressWarnings("unchecked")
	private GraphObject(String jsonString) {
		this.data = (LinkedTreeMap<String, Object>) (new Gson().fromJson(jsonString, Object.class));
	}

	public static GraphObject create(String jsonString) {
		return new GraphObject(jsonString);
	}

	/**
	 * @return the data
	 */
	public LinkedTreeMap<String, Object> getData() {
		return data;
	}

	/**
	 * @param data
	 * the data to set
	 */
	public void setData(LinkedTreeMap<String, Object> data) {
		this.data = data;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<LinkedTreeMap<String, String>> getVertexes() {
		return (ArrayList<LinkedTreeMap<String, String>>) data.get("vertices");
	}

	@SuppressWarnings("unchecked")
	public ArrayList<LinkedTreeMap<String, String>> getEdges() {
		return (ArrayList<LinkedTreeMap<String, String>>) data.get("edges");
	}

	public String toString() {
		return new Gson().toJson(data);
	}

	public String toJson() {
		return this.toString();
	}

}
