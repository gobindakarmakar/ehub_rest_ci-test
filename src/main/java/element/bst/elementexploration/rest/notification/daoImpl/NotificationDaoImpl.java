package element.bst.elementexploration.rest.notification.daoImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.notification.dao.NotificationDao;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationDTO;
import element.bst.elementexploration.rest.usermanagement.user.daoImpl.UserDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("notificationDao")
public class NotificationDaoImpl extends GenericDaoImpl<NotificationAlert, Long> implements NotificationDao {

	public NotificationDaoImpl() {
		super(NotificationAlert.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationAlertDTO> retrieveCaseNotificationAlertBasedOnLastCheckIn(Long caseId, Long userId,
			LocalDateTime lastCheckIn, Integer type, Integer level, LocalDateTime dateTo) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		LocalDateTime dateNow = null;
		if (dateTo == null)
			dateNow = LocalDateTime.now();
		else
			dateNow = dateTo;
		List<NotificationAlertDTO> notificationList = new ArrayList<NotificationAlertDTO>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.type, bstna.createdOn, bstna.deliverOn, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			if (lastCheckIn != null) {
				hql.append("and bstna.deliverOn BETWEEN ");
				hql.append("'" + lastCheckIn.format(formatter) + "'");
				hql.append(" and '" + dateNow.format(formatter) + "'");
			} else {
				hql.append("and bstna.deliverOn < :dateNow ");
			}
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");
			if (null != level)
				hql.append("and bstna.level = :level ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			if (null != level)
				query.setParameter("level", level);
			if (lastCheckIn == null)
				query.setParameter("dateNow", dateNow);
			notificationList = (List<NotificationAlertDTO>) query.getResultList();
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException(
					"Failed to retrieve BstNotificationAlert record. Reason : " + e.getMessage());
		}
		return notificationList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationAlertDTO> getUserNotificationList(Long userId, LocalDateTime dateFromDt,
			LocalDateTime dateToDt, Long caseId, Integer type, Integer pageNumber, Integer recordsPerPage,
			String orderBy, LocalDateTime lastCheckIn, String orderIn) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

		List<NotificationAlertDTO> notificationList = new ArrayList<NotificationAlertDTO>();

		try {

			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.type, bstna.createdOn, bstna.deliverOn, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");

			LocalDateTime now = LocalDateTime.now();
			if (dateFromDt != null && dateToDt != null) {
				hql.append("and bstna.deliverOn BETWEEN ");
				hql.append("'" + dateFromDt.format(formatter) + "'");
				hql.append(" and '" + dateToDt.format(formatter) + "'");
			} else if (dateFromDt != null && dateToDt == null) {
				hql.append("and bstna.deliverOn >= '" + dateFromDt.format(formatter) + "' ");
			} else if (dateFromDt == null && dateToDt != null) {
				hql.append("and bstna.deliverOn < '" + dateToDt.format(formatter) + "' ");
			} else {
				if (lastCheckIn != null) {
					hql.append("and bstna.deliverOn BETWEEN ");
					hql.append("'" + lastCheckIn.format(formatter) + "'");
					hql.append(" and '" + now.format(formatter) + "'");
				} else {
					hql.append("and bstna.deliverOn < '" + now.format(formatter) + "' ");
				}
			}

			hql.append(" order by ")
			   .append("bstna.")
			   .append(resolveCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hql.append(" ");
					hql.append(orderIn);
				}
			}
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			notificationList = (List<NotificationAlertDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, NotificationDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return notificationList;
	}

	@Override
	public NotificationAlertDTO retrieveNotificationDetail(Long notifId, Long userId) {

		NotificationAlertDTO response = null;

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.type, bstna.createdOn, bstna.deliverOn, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			hql.append("and bstna.notificationId = :notifId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setParameter("notifId", notifId);
			response = (NotificationAlertDTO) query.getSingleResult();
		} catch (NoResultException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			response = null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return response;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Long countUserNotificationList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId, Integer type, LocalDateTime lastCheckIn) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		List<NotificationDTO> notificationList = new ArrayList<NotificationDTO>();
		
		try {

			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.type, bstna.createdOn, bstna.deliverOn, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");
			
			LocalDateTime now = LocalDateTime.now();
			if(dateFromDt != null && dateToDt != null) {
				hql.append("and bstna.deliverOn BETWEEN ");
				hql.append("'" + dateFromDt.format(formatter) + "'");
				hql.append(" and '" + dateToDt.format(formatter) + "'");
			}else if(dateFromDt != null && dateToDt == null) {
				hql.append("and bstna.deliverOn >= '" + dateFromDt.format(formatter) + "' ");
			}else if(dateFromDt == null && dateToDt != null) {
				hql.append("and bstna.deliverOn < '" + dateToDt.format(formatter) + "' ");
			}else {
				if(lastCheckIn != null) {
					hql.append("and bstna.deliverOn BETWEEN ");
					hql.append("'" + lastCheckIn.format(formatter) + "'");
					hql.append(" and '" + now.format(formatter) + "'");
				}else {
					hql.append("and bstna.deliverOn < '" + now.format(formatter) + "' ");
				}
			}
			
			Query<?> query =  this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			notificationList = (List<NotificationDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, this.getClass());
			throw new FailedToExecuteQueryException();
		}
		
		return notificationList == null ? 0L : notificationList.size();	
		
	}
	
	@Override
	public boolean isEventOwner(Long eventId, Long userId) {

		NotificationAlert found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<NotificationAlert> query = builder.createQuery(NotificationAlert.class);
			Root<NotificationAlert> event = query.from(NotificationAlert.class);
			query.select(event);
			query.where(builder.and(builder.equal(event.get("notificationId"), eventId),
					builder.equal(event.get("createdBy"), userId)));
			found = this.getCurrentSession().createQuery(query).getSingleResult();
			if (found != null)
				return true;
		} catch (NoResultException e) {
			return false;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to execute request. Reason : " + e.getMessage());
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventDTO> getUserEventList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

		List<EventDTO> eventList = new ArrayList<EventDTO>();

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.EventDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.createdOn, bstna.scheduledFrom, bstna.scheduledTo, bstna.type, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");

			if (dateFromDt != null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom BETWEEN ");
				hql.append("'" + dateFromDt.format(formatter) + "'");
				hql.append(" and '" + dateToDt.format(formatter) + "'");
			} else if (dateFromDt != null && dateToDt == null) {
				hql.append("and bstna.scheduledFrom >= '" + dateFromDt.format(formatter) + "' ");
			} else if (dateFromDt == null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom < '" + dateToDt.format(formatter) + "' ");
			} else {
				hql.append("and bstna.scheduledFrom is not null ");
			}

			hql.append(" order by ")
			   .append("bstna.")
			   .append(resolveCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hql.append(" ");
					hql.append(orderIn);
				}
			}

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			eventList = (List<EventDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return eventList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserEventList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId, Integer type) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		List<EventDTO> eventList = new ArrayList<EventDTO>();

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.EventDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.createdOn, bstna.scheduledFrom, bstna.scheduledTo, bstna.type, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");

			if (dateFromDt != null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom BETWEEN ");
				hql.append("'" + dateFromDt.format(formatter) + "'");
				hql.append(" and '" + dateToDt.format(formatter) + "'");
			} else if (dateFromDt != null && dateToDt == null) {
				hql.append("and bstna.scheduledFrom >= '" + dateFromDt.format(formatter) + "' ");
			} else if (dateFromDt == null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom < '" + dateToDt.format(formatter) + "' ");
			} else {
				hql.append("and bstna.scheduledFrom is not null ");
			}

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			eventList = (List<EventDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return eventList == null ? 0L : eventList.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventDTO> getEventListByCreator(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt,
			Long caseId, Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		List<EventDTO> eventList = new ArrayList<EventDTO>();

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.EventDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.createdOn, bstna.scheduledFrom, bstna.scheduledTo, bstna.type, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("where bstna.createdBy = :userId ");
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");

			if (dateFromDt != null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom BETWEEN ");
				hql.append("'" + dateFromDt.format(formatter) + "'");
				hql.append(" and '" + dateToDt.format(formatter) + "'");
			} else if (dateFromDt != null && dateToDt == null) {
				hql.append("and bstna.scheduledFrom >= '" + dateFromDt.format(formatter) + "' ");
			} else if (dateFromDt == null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom < '" + dateToDt.format(formatter) + "' ");
			} else {
				hql.append("and bstna.scheduledFrom is not null ");
			}

			hql.append(" order by ")
			   .append("bstna.")
			   .append(resolveCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hql.append(" ");
					hql.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			eventList = (List<EventDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return eventList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countEventListByCreator(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId, Integer type) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		List<EventDTO> eventList = new ArrayList<EventDTO>();

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.EventDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.createdOn, bstna.scheduledFrom, bstna.scheduledTo, bstna.type, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("where bstna.createdBy = :userId ");
			if (null != caseId)
				hql.append("and bstna.caseId = :caseId ");
			if (null != type)
				hql.append("and bstna.type = :type ");

			if (dateFromDt != null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom BETWEEN ");
				hql.append("'" + dateFromDt.format(formatter) + "'");
				hql.append(" and '" + dateToDt.format(formatter) + "'");
			} else if (dateFromDt != null && dateToDt == null) {
				hql.append("and bstna.scheduledFrom >= '" + dateFromDt.format(formatter) + "' ");
			} else if (dateFromDt == null && dateToDt != null) {
				hql.append("and bstna.scheduledFrom < '" + dateToDt.format(formatter) + "' ");
			} else {
				hql.append("and bstna.scheduledFrom is not null ");
			}

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (null != caseId)
				query.setParameter("caseId", caseId);
			if (null != type)
				query.setParameter("type", type);
			eventList = (List<EventDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return eventList == null ? 0L : eventList.size();
	}

	@Override
	public EventDetailDTO retrieveEventDetail(Long eventId, Long userId) {
		EventDetailDTO response = null;

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.EventDetailDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.createdOn, bstna.scheduledFrom, bstna.scheduledTo, bstna.type, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("left outer join NotificationMapping bstnm on bstnm.userId = :userId or bstna.createdBy = :creator ");
			hql.append("where bstna.notificationId = bstnm.notifId ");
			hql.append("and bstna.notificationId = :eventId");
			hql.append(" and bstna.scheduledFrom is not null ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setParameter("eventId", eventId);
			query.setParameter("creator", userId);
			response = (EventDetailDTO) query.getSingleResult();
		} catch (NoResultException e) {
			response = findEventByOwner(eventId, userId);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return response;
	}

	private EventDetailDTO findEventByOwner(Long eventId, Long userId) {
		EventDetailDTO response = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct new element.bst.elementexploration.rest.notification.dto.EventDetailDTO(bstna.notificationId, bstna.subject, ");
			hql.append("bstna.body, bstna.createdOn, bstna.scheduledFrom, bstna.scheduledTo, bstna.type, bstna.level, bstna.createdBy, bstna.caseId, bstna.flag)");
			hql.append("from NotificationAlert bstna ");
			hql.append("where bstna.createdBy = :creator ");
			hql.append("and bstna.notificationId = :eventId");
			hql.append(" and bstna.scheduledFrom is not null ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("eventId", eventId);
			query.setParameter("creator", userId);
			response = (EventDetailDTO) query.getSingleResult();
		} catch (NoResultException e) {
			response = null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to execute request. Reason : " + e.getMessage());
		}
		return response;
	}
	private String resolveCaseColumnName(String column) {
		if (column == null)
			return "createdOn";

		switch (column.toLowerCase()) {
		case "level":
			return "level";
		default:
			return "createdOn";
		}
	}
}
