package element.bst.elementexploration.rest.usermanagement.security.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserToken;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author ramababu
 *
 */
public class ElementSecurityServiceTest {
	
	@Mock
	ElementsSecurityService elementSecurityServiceMock;
	
	@Mock
	ListItem listItem;
	
	public Users user;
	
	public UsersToken userToken = new UsersToken();
	
	public String token;
	
	@Before
    public void setupMock() {
		token=UUID.randomUUID().toString();
		user=new Users("analyst","Analyst","Analyst",new Date(),listItem);	
		userToken.setCreatedOn(new Date());
		userToken.setExpireOn(new Date());
		userToken.setId(2L);
		userToken.setUserId(user);
		userToken.setToken(token);
       MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void validateEmailPasswordTest() throws Exception{
		Mockito.when(elementSecurityServiceMock.validateEmailPassword(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(token);
		String validToken = elementSecurityServiceMock.validateEmailPassword(Mockito.anyString(),Mockito.anyString(),Mockito.anyString());
		assertEquals(token,validToken);
	}
	
	
	@Test
	public void getUserUserFromTokenTest(){
		Mockito.when(elementSecurityServiceMock.getUserUserFromToken(Mockito.anyString())).thenReturn(user);
		Users validUser = elementSecurityServiceMock.getUserUserFromToken(Mockito.anyString());
		assertEquals("analyst",validUser.getScreenName());
	}
	
	@Test
	public void getUserTokenTest(){
		Mockito.when(elementSecurityServiceMock.getUserToken(Mockito.anyLong())).thenReturn(userToken);
		UsersToken validUserToken = elementSecurityServiceMock.getUserToken(Mockito.anyLong());
		assertEquals(token,validUserToken.getToken());
	}
	
	@Test
	public void saveTokenTest(){
		Mockito.when(elementSecurityServiceMock.saveToken(Mockito.any(Users.class))).thenReturn(token);
		String validToken = elementSecurityServiceMock.saveToken(Mockito.any(Users.class));
		assertEquals(token,validToken);
	}
	
	@Test
	public void validateTemporaryPasswordTest(){
		Mockito.when(elementSecurityServiceMock.validateTemporaryPassword(Mockito.anyLong(),Mockito.anyString(),Mockito.anyString())).thenReturn(true);
		boolean validPassword = elementSecurityServiceMock.validateTemporaryPassword(Mockito.anyLong(),Mockito.anyString(),Mockito.anyString());
		assertEquals(true,validPassword);
	}
	
	/*@Test
	public void sendEmailForPasswordResetTest(){
		Mockito.when(elementSecurityServiceMock.sendEmailForPasswordReset(Mockito.anyLong(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(true);
		boolean validPassword = elementSecurityServiceMock.sendEmailForPasswordReset(Mockito.anyLong(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString());
		assertEquals(true,validPassword);
	}*/
	
	

}
