package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableColumns;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentTableColumnsDao extends GenericDao<DocumentTableColumns, Long>{

	public List<DocumentTableColumns> getAllColumnsByTableId(Long id);
	
	public List<DocumentTableColumns> getAllChildColumsByParentId(Long parentId);

	
}
