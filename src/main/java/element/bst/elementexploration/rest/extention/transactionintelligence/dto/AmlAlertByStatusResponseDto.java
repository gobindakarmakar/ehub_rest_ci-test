package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 
 * @author Rambabu
 *
 */
@ApiModel("AML alert by status")
public class AmlAlertByStatusResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Aml alerts by status")	
	private List<AlertStatusDto> amlAlertByStatus;

	public AmlAlertByStatusResponseDto() {
		super();
		
	}

	public List<AlertStatusDto> getAmlAlertByStatus() {
		return amlAlertByStatus;
	}

	public void setAmlAlertByStatus(List<AlertStatusDto> amlAlertByStatus) {
		this.amlAlertByStatus = amlAlertByStatus;
	}
	

}
