package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceFilterDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourcesDao extends GenericDao<Sources, Long> {

	List<Sources> getSources(Integer pageNumber, Integer recordsPerPage,Boolean visible,
			Long classificationId,String orderBy,String orderIn, List<SourceFilterDto> sourceFilterDto,Long subClassifcationId);

	boolean checkSourceExist(String identifier, String url);

	boolean checkSourceLink(String url,boolean isIndex,Long classificationId);
	
	public List<Sources> fetchClassifctaionSources(String classiifcationName);
	
	public List<SourcesDto> getAllSources();

	String getSourceCategoryBySourceName(String source);

	boolean checkSourceExistWithSourceNameAndEntityId(String identifier, String sourceName);

	boolean checkSourceName(String sourceName, boolean isIndex, Long classificationId);

}
