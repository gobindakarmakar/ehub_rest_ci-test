package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfoDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ScenariosResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerDetailsService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Risk API" },description="Manages risky transactions data")
@RestController
@RequestMapping("/api/risk")
public class RiskController extends BaseController {

	@Autowired
	private TxDataService txDataService;

	@Autowired
	private CustomerDetailsService customerDetailsService;

	@ApiOperation("Get total transaction and alert amount by customer risk factor")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCustomerRisk/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerRisk(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		filterDto.setCustomerRiskType(true);
		return new ResponseEntity<>(txDataService.getCustomerRisk(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count for each customer risk type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class,responseContainer="List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCustomerRiskAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerRiskAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getCustomerRiskAggregates(fromDate, toDate, filterDto),
				HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count by geographic risk")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getGeoGraphicRisk/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeoGraphicRiskAlerts(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getGeoGraphicRisk(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count for each geographic risk type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getGeoGraphicRiskAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeoGraphicRiskAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getGeoGraphicRiskAggregates(fromDate, toDate, filterDto),
				HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count for each corruption risk type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCorruptionRiskAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorruptionRiskAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getCorruptionRiskAggregates(fromDate, toDate, filterDto),
				HttpStatus.OK);
	}

	@ApiOperation("Get total transaction and alert amount/count for each political risk type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getPoliticalRiskAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPoliticalRiskAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getPoliticalRiskAggregates(fromDate, toDate, filterDto),
				HttpStatus.OK);
	}

	@ApiOperation("Get total transaction and alert amount/count by corruption risk")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCorruptionRisk/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorruptionRisk(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getCorruptionRisk(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count by political risk")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getPoliticalRisk/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPoliticalRisk(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getPoliticalRisk(fromDate, toDate, filterDto), HttpStatus.OK);

	}
	
	@ApiOperation("Get scenarios by type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ScenariosResponseDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getScenariosByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getScenariosByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam String type,@RequestParam(required=false) String filterType, @RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getScenariosByType(fromDate, toDate, type,filterType, filterDto),
				HttpStatus.OK);

	}

/*	@PostMapping(value = "/getCustomerRiskByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerRiskByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}
	
	@PostMapping(value = "/getProductRiskByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getProductRiskByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		filterDto.setProductRisk(true);
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}

	@PostMapping(value = "/getGeoGraphicRiskByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeoGraphicRiskByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		filterDto.setGeographyRiskType(true);
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}

	@PostMapping(value = "/getCorruptionByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorruptionByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		filterDto.setCorruptionRisk(true);
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);
	}

	@PostMapping(value = "/getPoliticalRiskByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPoliticalRiskByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		filterDto.setPoliticalRisk(true);
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}*/

	@ApiOperation("Get total transaction and alert amount/count by product risk")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getProductRisk/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getProductRisk(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getProductRisk(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count for each product risk type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getProductRiskAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getProductRiskAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getProductRiskAggregates(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get ratio of each risk type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/riskRatioChart/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskRatioChart(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getRiskRatioChart(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get alerts by scenario type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertDashBoardRespDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getAlertsByScenario/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskAlertsByScenarios(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getRiskAlertsByScenarios(fromDate, toDate, filterDto), HttpStatus.OK);

	}

	@ApiOperation("Get All tranasction and alert amount/count by diffrent risk factors")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getViewAll/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getViewAllRisk(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "recordsPerPage", required = false) Integer recordsPerPage,
			@RequestParam("type") String type, HttpServletRequest request, @RequestBody(required=false) FilterDto filterDto)
			throws ParseException {

		List<RiskCountAndRatioDto> list = txDataService.viewAll(fromDate, toDate, pageNumber, recordsPerPage,
				type, filterDto);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = txDataService.viewAllCount(fromDate, toDate, type, filterDto);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", list);
		jsonObject.put("paginationInformation", information);*/
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setResult(list);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);

	}

	@ApiOperation("Get counter party information")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CounterPartyInfoDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/counterPartyInfo/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> counterPartyInfo(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam("id") Long customerId, @RequestBody(required=false) FilterDto filterDto, HttpServletRequest request)
			throws ParseException {

		return new ResponseEntity<>(txDataService.counterPartyInfo(fromDate, toDate, customerId, filterDto),
				HttpStatus.OK);
	}

	@ApiOperation("Get transactions by customer id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCounterParties/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerTransactions(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam("id") Long customerId, HttpServletRequest request,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "recordsPerPage", required = false) Integer recordsPerPage,
			@RequestParam(value = "isDetected") boolean isDetected,@RequestBody(required=false) FilterDto filterDto) throws ParseException {

		List<AlertedCounterPartyDto> list = txDataService.getCustomerTransactions(fromDate, toDate, customerId,
				pageNumber, recordsPerPage, isDetected,filterDto);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = txDataService.getCustomerTransactionsCount(fromDate, toDate, customerId, isDetected,filterDto);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", list);
		jsonObject.put("paginationInformation", information);*/
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setResult(list);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);

	}

	@ApiOperation("Get Customer information")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CustomerDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@GetMapping(value = "/customerInfo", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> customerInfo(@RequestParam("token") String token,
			@RequestParam("id") Long customerId,HttpServletRequest request)
			throws ParseException {

		return new ResponseEntity<>(customerDetailsService.customerInfo(customerId), HttpStatus.OK);
	}


}
