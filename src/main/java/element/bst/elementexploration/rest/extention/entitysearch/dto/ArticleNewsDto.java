package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArticleNewsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("article_title")
	private String articleTitle;

	@JsonProperty("classification")
	private String classification;

	@JsonProperty("search_query")
	private String searchQuery;

	@JsonProperty("entity_id")
	private String entityId;

	@JsonProperty("article")
	private String article;

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

}
