package element.bst.elementexploration.rest.usermanagement.security.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;

/**
 * 
 * @author Prateek Maurya
 *
 */
public interface LoginHistoryDao extends GenericDao<LoginHistory, Long>{

	public LoginHistory getFailedLoginByUsername(String username);

	public List<LoginHistory> getlogonsByUsernames(List<String> usernamesList, Date from, Date to);

	public Map<Long, List<LoginHistory>> getFailedlogonsByUserIdList(List<Long> userIdList);
}
