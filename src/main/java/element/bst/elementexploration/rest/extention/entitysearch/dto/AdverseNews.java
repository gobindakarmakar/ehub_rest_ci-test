package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Suresh
 *
 */
public class AdverseNews implements Serializable {

	private static final long serialVersionUID = 1L;

	private String source;

	private String url;

	private String title;

	@JsonProperty("class")
	private String classBing;

	private String published;

	private String text;

	@JsonProperty("entities")
	private List<EntityDto> entityDto;

	@JsonProperty("queue")
	private String queue = "Low";

	private Boolean isSignificant;

	private Boolean isArticleNews;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getClassBing() {
		return classBing;
	}

	public void setClassBing(String classBing) {
		this.classBing = classBing;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<EntityDto> getEntityDto() {
		return entityDto;
	}

	public void setEntityDto(List<EntityDto> entityDto) {
		this.entityDto = entityDto;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public Boolean getIsSignificant() {
		return isSignificant;
	}

	public void setIsSignificant(Boolean isSignificant) {
		this.isSignificant = isSignificant;
	}

	public Boolean getIsArticleNews() {
		return isArticleNews;
	}

	public void setIsArticleNews(Boolean isArticleNews) {
		this.isArticleNews = isArticleNews;
	}

}
