package element.bst.elementexploration.test.extention.sourcecredibility.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourcesDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl.SourcesServiceImpl;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SourcesServiceTest {
	
	
	@Mock
	SourcesDao sourcesDao;
	
	@Mock
	ApplicationEventPublisher eventPublisher;
	
	@InjectMocks
	SourcesServiceImpl sourcesService;
	
	List<Sources> sourcesList= new ArrayList<Sources>();
	SourcesDto sourcesDto = null;
	Sources sourcesDB = null;
	
	@Before
    public void setupMock() {
		sourcesDto = new SourcesDto();
		sourcesDB = new Sources();
		sourcesDB.setSourceId(1L);
		sourcesDB.setSourceName("test");
		sourcesDB.setSourceDisplayName("test");
		sourcesDto.setSourceDisplayName("test");
		sourcesDto.setSourceId(1L);
		sourcesDto.setSourceUrl("http://google.com");
		sourcesDB.setSourceUrl("http://google.com");
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void saveSource() {
		Mockito.when(sourcesDao.create(Mockito.anyObject())).thenReturn(sourcesDB);
		Mockito.when(sourcesDao.checkSourceName(Mockito.anyString(),Mockito.anyBoolean(),Mockito.anyLong())).thenReturn(false);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		boolean  sourcesCheck =  sourcesService.saveSource(sourcesDto,1L);
		Assert.assertEquals(sourcesCheck,true);
	}
	
	@Test
	public void updateSource() throws Exception
	{
		Mockito.when(sourcesDao.find(Mockito.anyLong())).thenReturn(sourcesDB);
		Mockito.doNothing().when(sourcesDao).saveOrUpdate(Mockito.any(Sources.class));
		sourcesService.updateSource(sourcesDto, 1L);
	}
	

}
