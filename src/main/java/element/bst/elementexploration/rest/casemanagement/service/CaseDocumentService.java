package element.bst.elementexploration.rest.casemanagement.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocDetailsDTO;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface CaseDocumentService extends GenericService<CaseDocDetails, Long>{
	
	 Long uploadCaseDocument(MultipartFile uploadDoc, Long seedId, String fileTitle, String remarks,long userId, Integer docFlag) throws Exception;
	 
	 void updateCaseDocument(String fileTitle, Long docId, String remarks,long userId);
	 
	 CaseDocDetailsDTO getCaseDocumentDetailsById(Long docId);
	
	 void deleteCaseDocumentById(Long docId, Long userId);
	 
	 byte[] downloadCaseDocument(String docUUID) throws Exception;
	 
	 List<CaseDocDetailsDTO> getAllDocumentsForCase(Long caseSeedId,Integer docFlag, String orderBy, Integer pageNumber, Integer recordsPerPage);
	  
	 Long countAllDocumentsForCase(Long caseSeedId,Integer docFlag);
	  
	 Long getCaseIdFromDoc(long docId);

	 List<DocAggregatorDto> docAggregator(Long caseId,String fromDate,String toDate) throws ParseException;

	void updateDocumentContent(MultipartFile uploadDoc, Long userId, Long docId) throws IOException;
	
	byte[] downloadAnnualReturnsDocument(String url) throws Exception;
	
}
