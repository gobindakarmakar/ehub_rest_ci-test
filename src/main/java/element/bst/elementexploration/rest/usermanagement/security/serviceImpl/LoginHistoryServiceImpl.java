package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.LoginHistoryDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.security.service.LoginHistoryService;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Prateek Maurya
 *
 */
@Service("loginHistoryService")
public class LoginHistoryServiceImpl extends GenericServiceImpl<LoginHistory, Long>implements LoginHistoryService{

	/**
	 * Allowed Failed Login Attemtpts - Read from configuration file
	 */
	@Value("${allowedFailedLoginAttempts}")
	private Integer ALLOWED_FAILED_LOGIN_ATTEMPTS;
	
	/**
	 * Blocked Failed Login Wait Duration - Read from configuration file
	 */
	@Value("${blockedFailedLoginWaitDuration}")
	private Integer BLOCKED_FAILED_LOGIN_WAIT_DURATION;
	
	@Autowired
	public LoginHistoryServiceImpl(GenericDao<LoginHistory, Long> genericDao) {
		super(genericDao);		
	}
	
	@Autowired
	private LoginHistoryDao loginHistoryDao;
	
	@Autowired
	ElementsSecurityService elementsSecurityService;
	/**
	 * Check if Account is locked
	 * 
	 * @param username
	 * 
	 * @return result true: locked
	 */
	@Override
	@Transactional("transactionManager")
	public boolean isAccountLocked(String username) {		
		boolean result = false;
		LoginHistory failedLogin = loginHistoryDao.getFailedLoginByUsername(username);
		
		if (failedLogin != null) {
			//long lastFailedLoginTimestamp = failedLogin.getLastFailedLoginTime().getTime();
			long lastFailedLoginTimestamp = failedLogin.getLoginTime().getTime();
			long currentTimestamp = (new Date()).getTime();			
			if ( failedLogin.getFailedLoginAttempt() >= this.getAllowedFailedLoginAttempt() &&
					currentTimestamp < (lastFailedLoginTimestamp + getBlockedFailedLoginWaitDurationInMilliseconds())) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * Release Account locked
	 * 
	 * @param username
	 * @param ipAddress
	 * 
	 * @return result true: successful
	 */
	@Override
	@Transactional("transactionManager")
	public boolean releaseAccountLocked(String username, String ipAddress) {
		LoginHistory failedLogin = loginHistoryDao.getFailedLoginByUsername(username);
		if (failedLogin != null) {
			failedLogin.setFailedLoginAttempt(0);
			failedLogin.setLastSuccessfulLoginIpAddress(ipAddress);
			failedLogin.setLastSuccessfulLoginTime(new Date());
			loginHistoryDao.saveOrUpdate(failedLogin);
		}
		return true;
	}

	/**
	 * Process Failed Authentication
	 * 
	 * @param username
	 * @param screenName
	 * @param ipAddress
	 * 
	 */
	@Override
	@Transactional("transactionManager")
	public void processFailedAuthentication(String username, String screenName, String ipAddress) {
		ElementLogger.log(ElementLoggerLevel.INFO, "Process Failed Authentication. username: " + username + "; ipAddress: " + ipAddress, this.getClass());

		if (this.getCurrentLoginAttempt(username, screenName, ipAddress) >= getAllowedFailedLoginAttempt()) {
			ElementLogger.log(ElementLoggerLevel.WARNING, "Exceeded failed login attempt. username: " + username + "; ipAddress: " + ipAddress, this.getClass());
			//processExceededFailedLoginAttempts(username, password, ipAddress, unknownUser); // code to send an email
		}		
	}

	@Override
	@Transactional("transactionManager")
	public LoginHistory getFailedLoginByUsername(String username) {
		return loginHistoryDao.getFailedLoginByUsername(username);
	}
	
	/**
	 * Get Allowed Failed Login Attempts
	 * @return count for allowed Failed Login Attempt
	 */
	private int getAllowedFailedLoginAttempt() {
		int allowedFailedLoginAttempts = 0;
		allowedFailedLoginAttempts = ALLOWED_FAILED_LOGIN_ATTEMPTS;
		return allowedFailedLoginAttempts;
	}
	
	/**
	 * Get Blocked Failed Login Wait Duration
	 * @return wait duration
	 */
	private int getBlockedFailedLoginWaitDuration() {
		int blockedFailedLoginWaitDuration = 0;
		blockedFailedLoginWaitDuration = BLOCKED_FAILED_LOGIN_WAIT_DURATION;
		return blockedFailedLoginWaitDuration;
	}
	
	/**
	 * Get Blocked Failed Login Wait Duration
	 * @return wait duration in milliseconds
	 */
	private long getBlockedFailedLoginWaitDurationInMilliseconds() {
		return getBlockedFailedLoginWaitDuration() * 60 * 60 * (long)1000; //Convert Hours to Milliseconds
	}
	
	/**
	 * Get current login attempt
	 * 
	 * @param username
	 * @return
	 */
	@Transactional("transactionManager")
	@Override
	public int getCurrentLoginAttempt(String username, String screenName, String ipAddress) {
		int currentLoginAttempt = 0;
		LoginHistory failedLogin = loginHistoryDao.getFailedLoginByUsername(username);
		if (failedLogin != null) {
			if (failedLogin.getFailedLoginAttempt() != null) {
				currentLoginAttempt = failedLogin.getFailedLoginAttempt().intValue() + 1;				
				failedLogin.setLastFailedLoginIpAddress(ipAddress);
				failedLogin.setLastFailedLoginTime(new Date());
				failedLogin.setFailedLoginAttempt(currentLoginAttempt);
				loginHistoryDao.saveOrUpdate(failedLogin);
			}
		}
		else {
			currentLoginAttempt = 1;
			LoginHistory newFailedLogin = new LoginHistory();
			newFailedLogin.setUsername(username);
			newFailedLogin.setScreenName(screenName);
			newFailedLogin.setLastFailedLoginIpAddress(ipAddress);
			newFailedLogin.setLastFailedLoginTime(new Date());
			newFailedLogin.setFailedLoginAttempt(currentLoginAttempt);
			loginHistoryDao.saveOrUpdate(newFailedLogin);
		}
		return currentLoginAttempt;
	}

	@Override
	@Transactional("transactionManager")
	public boolean releaseAccountLockedNew(String username, String ipAddress, HttpServletRequest request,
			String loginSource, UsersDto user) {
		/*LoginHistory loginHistory = loginHistoryDao.getFailedLoginByUsername(username);
		if (loginHistory != null) {
			loginHistory.setFailedLoginAttempt(0);
			loginHistory.setLastSuccessfulLoginIpAddress(ipAddress);
			loginHistory.setLastSuccessfulLoginTime(new Date());
			
			loginHistory.setLoginStatus("Success");
			loginHistory.setLoginSource(loginSource);
			loginHistory.setLoginClientMedia(request.getHeader("User-Agent"));
			loginHistory.setLoginIpAddress(ipAddress);
			loginHistory.setLoginTime(new Date());
			loginHistory.setLoginDescription(ElementConstants.Active_USER_LOGIN);
			loginHistory.setLoginClientLocation(null);
			
			
			save(loginHistory);
		}else{
			LoginHistory loginHistoryNew = new LoginHistory();
			loginHistoryNew.setFailedLoginAttempt(0);
			loginHistoryNew.setLastSuccessfulLoginIpAddress(ipAddress);
			loginHistoryNew.setLastSuccessfulLoginTime(new Date());
			
			loginHistoryNew.setLoginStatus(ElementConstants.SUCCESS_STATUS);
			loginHistoryNew.setUsername(username);
			loginHistoryNew.setScreenName(username);
			loginHistoryNew.setLoginSource(loginSource);
			loginHistoryNew.setLoginClientMedia(request.getHeader("User-Agent"));
			loginHistoryNew.setLoginIpAddress(ipAddress);
			loginHistoryNew.setLoginTime(new Date());
			loginHistoryNew.setLoginDescription(ElementConstants.Active_USER_LOGIN);
			loginHistoryNew.setLoginClientLocation(null);
			loginHistoryNew.setUserStatus(user.getStatusId().getDisplayName());
			
			save(loginHistoryNew);
		//}*/
			elementsSecurityService.logUserLogins(user, request, true);
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public void processFailedAuthenticationNew(String username, String screenName, String ipAddress,
			HttpServletRequest request, String loginSource,  UsersDto user) {
		ElementLogger.log(ElementLoggerLevel.INFO, "Process Failed Authentication. username: " + username + "; ipAddress: " + ipAddress, this.getClass());

		//int currentLoginAttempt = this.getCurrentLoginAttemptNew(username, screenName, ipAddress, request, loginSource );
		if (this.getCurrentLoginAttemptNew(username, screenName, ipAddress, request, loginSource, user ) >= getAllowedFailedLoginAttempt()) {
			elementsSecurityService.logUserLogins(user, request, false);
			ElementLogger.log(ElementLoggerLevel.WARNING, "Exceeded failed login attempt. username: " + username + "; ipAddress: " + ipAddress, this.getClass());
			//processExceededFailedLoginAttempts(username, password, ipAddress, unknownUser); // code to send an email
		}
		
	}
	
	@Transactional("transactionManager")
	private int getCurrentLoginAttemptNew(String username, String screenName, String ipAddress,
			HttpServletRequest request, String loginSource, UsersDto user) {
		
		int currentLoginAttempt = 0;
		LoginHistory loginHistory = loginHistoryDao.getFailedLoginByUsername(username);
		if (loginHistory != null) {
			if (loginHistory.getFailedLoginAttempt() != null) {
				currentLoginAttempt = loginHistory.getFailedLoginAttempt().intValue() + 1;				
				/*loginHistory.setLastFailedLoginIpAddress(ipAddress);
				loginHistory.setLastFailedLoginTime(new Date());
				loginHistory.setFailedLoginAttempt(currentLoginAttempt);*/
				
				/*loginHistory.setLoginStatus("failed");
				loginHistory.setLoginSource(loginSource);
				loginHistory.setLoginClientMedia(request.getHeader("User-Agent"));
				loginHistory.setLoginIpAddress(ipAddress);
				loginHistory.setLoginTime(new Date());
				loginHistory.setLoginDescription(ElementConstants.Active_USER_LOGIN);
				loginHistory.setLoginClientLocation(null);*/
				
				//loginHistoryDao.create(loginHistory);
			}
		}
		else {
			currentLoginAttempt = 1;
		}
			LoginHistory newFailedLogin = new LoginHistory();
			newFailedLogin.setUsername(username);
			newFailedLogin.setScreenName(screenName);
			newFailedLogin.setLastFailedLoginIpAddress(ipAddress);
			newFailedLogin.setLastFailedLoginTime(new Date());
			newFailedLogin.setFailedLoginAttempt(currentLoginAttempt);
			
			newFailedLogin.setLoginStatus("failed");
			newFailedLogin.setLoginSource(loginSource);
			newFailedLogin.setLoginClientMedia(request.getHeader("User-Agent"));
			newFailedLogin.setLoginIpAddress(ipAddress);
			newFailedLogin.setLoginTime(new Date());
			newFailedLogin.setLoginDescription("tried to login with incorrect email/password");
			newFailedLogin.setLoginClientLocation(null);
			newFailedLogin.setUserStatus(user.getStatusId().getDisplayName());
			//loginHistoryDao.create(newFailedLogin);
		
		return currentLoginAttempt;
	}

	@Override
	@Transactional("transactionManager")
	public List<LoginHistory> getlogonsByUsernames(List<String> usernamesList, Date from, Date to) {
		
		return loginHistoryDao.getlogonsByUsernames(usernamesList,from, to );
	}

	@Override
	@Transactional("transactionManager")
	public Map<Long, List<LoginHistory>> getFailedlogonsByUserIdList(List<Long> userIdList) {
		
		return loginHistoryDao.getFailedlogonsByUserIdList(userIdList);
	}

}
