package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Transactions list aggregate response")
public class TxsListAggRespDto implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="List of Transactions Data ")
	List<TransactionsDataNotifDto> transactionsDataList;
	@ApiModelProperty(value="List of Alerts and amount aggregator  ")
	List<TransactionAmountAndAlertCountDto> txAlertCountAmountAgg;
	public List<TransactionsDataNotifDto> getTransactionsDataList() {
		return transactionsDataList;
	}
	public void setTransactionsDataList(List<TransactionsDataNotifDto> transactionsDataList) {
		this.transactionsDataList = transactionsDataList;
	}
	public List<TransactionAmountAndAlertCountDto> getTxAlertCountAmountAgg() {
		return txAlertCountAmountAgg;
	}
	public void setTxAlertCountAmountAgg(List<TransactionAmountAndAlertCountDto> txAlertCountAmountAgg) {
		this.txAlertCountAmountAgg = txAlertCountAmountAgg;
	}
	
	

}
