package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TxByDate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long count;
	private double sum;
	@JsonProperty("point")
	private Date businessDate;

	public TxByDate(Long count, double sum, Date businessDate) {
		super();
		this.count = count;
		this.sum = sum;
		this.businessDate = businessDate;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	@Override
	public String toString() {
		return "TxByDate [count=" + count + ", sum=" + sum + ", businessDate=" + businessDate + "]";
	}

}
