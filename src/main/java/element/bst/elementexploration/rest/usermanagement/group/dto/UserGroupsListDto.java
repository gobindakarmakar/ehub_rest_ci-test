package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModelProperty;


/**
 * @author Paul Jalagari
 *
 */
public class UserGroupsListDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "List of groups")
	private List<Groups> result;

	@ApiModelProperty(value = "Page information")
	private PaginationInformation paginationInformation;

	public List<Groups> getResult() {
		return result;
	}

	public void setResult(List<Groups> result) {
		this.result = result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}

}
