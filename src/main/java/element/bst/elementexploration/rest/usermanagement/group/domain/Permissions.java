package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@Entity
@Table(name = "bst_um_permissions")
public class Permissions implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Privilege ID")
	@Id
	/*@Column(name = "permissionId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)*/
	@GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "element.bst.elementexploration.rest.util.UseExistingIdOtherwiseGenerateUsingIdentity")
	@GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
	@Column(unique = true, nullable = false)
	private Long permissionId;

	@ApiModelProperty(value = "Privilege Name")
	@Column(name = "itemName", columnDefinition = "LONGTEXT")
	private String itemName;
	
	@ApiModelProperty(value = "Status of permission")
	@Column(name = "active")
	private Boolean active;

	//@JsonIgnore
	@ApiModelProperty(value = "Parent Permission ID")
	@ManyToOne(cascade = { CascadeType.MERGE },fetch = FetchType.LAZY)
	@JoinColumn(name = "parentPermissionId")
	@Access(AccessType.PROPERTY)
	private Permissions parentPermissionId;
	
	@ApiModelProperty(value = "Created On")
	@Column(name = "createdOn")
	private Date createdOn;
	
/*	@ApiModelProperty(value = "permissionToChild")
	@Column(name = "permissionToChild")
	private Long permissionToChild;*/

	@ApiModelProperty(value = "Permission List")
	@OneToMany(mappedBy = "parentPermissionId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<Permissions> permissions;

	@ApiModelProperty(value = "Permission List")
	@OneToMany(mappedBy = "permissionId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<GroupPermission> groupPermission;

	@ApiModelProperty(value = "Domains List")
	@OneToMany(mappedBy = "permissionId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<Domains> domains;

	@ApiModelProperty(value = "Modules List")
	@OneToMany(mappedBy = "permissionId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<Modules> modules;

	@ApiModelProperty(value = "Modules Group List")
	@OneToMany(mappedBy = "permissionId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<ModulesGroup> modulesGroups;

	public Permissions() {
	}

	public Permissions(String itemName) {
		this.active=true;
		this.createdOn=new Date();
		this.itemName = itemName;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Permissions getParentPermissionId() {
		return parentPermissionId;
	}

	public void setParentPermissionId(Permissions parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}

	public List<Permissions> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permissions> permissions) {
		this.permissions = permissions;
	}

	public List<GroupPermission> getGroupPermission() {
		return groupPermission;
	}

	public void setGroupPermission(List<GroupPermission> groupPermission) {
		this.groupPermission = groupPermission;
	}

	public List<Domains> getDomains() {
		return domains;
	}

	public void setDomains(List<Domains> domains) {
		this.domains = domains;
	}

	public List<Modules> getModules() {
		return modules;
	}

	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}

	public List<ModulesGroup> getModulesGroups() {
		return modulesGroups;
	}

	public void setModulesGroups(List<ModulesGroup> modulesGroups) {
		this.modulesGroups = modulesGroups;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/*public Long getPermissionToChild() {
		return permissionToChild;
	}

	public void setPermissionToChild(Long permissionToChild) {
		this.permissionToChild = permissionToChild;
	}*/
	
	
	
	

}
