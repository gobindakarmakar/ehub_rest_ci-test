package element.bst.elementexploration.rest.logging.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Amit Patel
 *
 */
@Entity
@Table(name = "bst_audit_log")
@Access(AccessType.FIELD)
public class AuditLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("Id of audit log")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "logId")
	private Long logId;

	@ApiModelProperty("Audit log created date")
	@JsonProperty("timestamp")
	@Column(name = "created_on")
	private Date createdOn;

	@ApiModelProperty("Log level of audit log")
	@Column(name = "level")
	@Enumerated(EnumType.ORDINAL)
	private LogLevels level;

	@ApiModelProperty("Type of audit log")
	@Column(name = "type")
	private String type;
	
	@ApiModelProperty("Type Id for audit log")
	@Column(name = "type_id")
	private Long typeId;

	@ApiModelProperty("Flag of audit log")
	@Column(name = "flag")
	private Integer flag;

	@ApiModelProperty("Title of audit log")
	@Column
	private String title;

	@ApiModelProperty("Description of audit log")
	@Column(columnDefinition = "LONGTEXT")
	private String description;

	@ApiModelProperty("Log created for user")
	@Column(name = "userId")
	private Long userId;

	@ApiModelProperty("Case id")
	@Column(name = "caseId")
	private Long caseId;

	@ApiModelProperty("Document id ")
	@Column(name = "documentId")
	private Long documentId;

	@ApiModelProperty("Entity id ")
	@Column(name = "entityId")
	private String entityId;  //change1

	@ApiModelProperty("Log name")
	@Column(name = "name")
	private String name;

	@ApiModelProperty("Answer id ")
	@Column(name = "answerId")
	private Long answerId;

	@ApiModelProperty("Template id ")
	@Column(name = "templateId")
	private Long templateId;

	@ApiModelProperty("Story id ")
	@Column(name = "storyId")
	private String storyId;

	@ApiModelProperty("Question id ")
	@Column(name = "questionId")
	private Long questionId;

	@ApiModelProperty("Possible answer id ")
	@Column(name = "possibleAnswerId")
	private Long possibleAnswerId;

	@ApiModelProperty("Significant news id ")
	@Column(name = "significantnewsId")
	private Long significantnewsId;

	@ApiModelProperty("Source id ")
	@Column(name = "sourceId")
	private Long sourceId;
	
	@ApiModelProperty("Sub Type")
	@Column(name = "subType")
	private String subType; //for (alert status KPI

	public AuditLog() {

	}

	public AuditLog(Date createdOn, LogLevels level, String type, Integer flag, String title) {
		super();
		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.flag = flag;
		this.title = title;
	}
	public AuditLog(Date createdOn, LogLevels level,String title,Long userId) {
		super();
		this.createdOn = createdOn;
		this.level = level;
		this.userId=userId;
		//this.type = type;
		//this.flag = flag;
		this.title = title;
	}


	public AuditLog(Date createdOn, LogLevels level, String type, Integer flag, String title, String description,
			Long userId, Long caseId, Long documentId, String entityId, String name) { //change 2 Long to String
		super();
		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.flag = flag;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.caseId = caseId;
		this.documentId = documentId;
		this.entityId = entityId;
		this.name = name;
	}

	public AuditLog(Date createdOn, LogLevels level, String type, Integer flag, String title, String description,
			Long userId, Long caseId, Long documentId, String entityId, String name, Long answerId) { //change 2
		super();

		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.flag = flag;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.caseId = caseId;
		this.documentId = documentId;
		this.entityId = entityId;
		this.name = name;
		this.answerId = answerId;
	}

	public AuditLog(Date createdOn, LogLevels level, String type, Integer flag, String title, String description,
			Long userId, Long caseId, Long documentId, String entityId, String name, Long answerId, Long templateId, //change 2
			Long questionId, Long possibleAnswerId) {
		super();

		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.flag = flag;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.caseId = caseId;
		this.documentId = documentId;
		this.entityId = entityId;
		this.name = name;
		this.answerId = answerId;
		this.templateId = templateId;
		this.questionId = questionId;
		this.possibleAnswerId = possibleAnswerId;
	}

	public AuditLog(Date createdOn, LogLevels level, String type, Integer flag, String title, String description,
			Long userId, Long caseId, Long documentId, String entityId, String name, Long answerId, String storyId) { //change 2
		super();

		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.flag = flag;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.caseId = caseId;
		this.documentId = documentId;
		this.entityId = entityId;
		this.name = name;
		this.answerId = answerId;
		this.storyId = storyId;
	}

	public AuditLog(Date createdOn, LogLevels level, String type, Integer flag, String title, String description,
			Long userId, String name, Long significantnewsId) {
		super();
		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.flag = flag;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.name = name;
		this.significantnewsId = significantnewsId;

	}

	public AuditLog(Date createdOn, LogLevels level, String type, String title, String description, Long userId,
			String name) {
		super();
		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.name = name;
	}

	public AuditLog(Date createdOn, LogLevels level, String type, String title, String description, Long userId,
			Long sourceId, String name) {
		super();
		this.createdOn = createdOn;
		this.level = level;
		this.type = type;
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.sourceId = sourceId;
		this.name = name;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public LogLevels getLevel() {
		return level;
	}

	public void setLevel(LogLevels level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getEntityId() { //change 2
		return entityId;
	}

	public void setEntityId(String entityId) { //change 2
		this.entityId = entityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getStoryId() {
		return storyId;
	}

	public void setStoryId(String storyId) {
		this.storyId = storyId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getPossibleAnswerId() {
		return possibleAnswerId;
	}

	public void setPossibleAnswerId(Long possibleAnswerId) {
		this.possibleAnswerId = possibleAnswerId;
	}

	public Long getSignificantnewsId() {
		return significantnewsId;
	}

	public void setSignificantnewsId(Long significantnewsId) {
		this.significantnewsId = significantnewsId;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
