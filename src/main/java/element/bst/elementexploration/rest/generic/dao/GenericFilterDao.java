package element.bst.elementexploration.rest.generic.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.dto.GenericFilterDto;

public interface GenericFilterDao {
	
	List<?> getFilterDataForList(GenericFilterDto dto);
	
	<E> void getSingle(GenericFilterDto dto);


}
