package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

/**
 * @author suresh
 *
 */
@ApiModel("Paragraph")
public class ParagraphDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String contentType;
	private String content;
	private String style;
	private String numberFont;
	private int fontSize;
	private String colour;
	private boolean isBold;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getNumberFont() {
		return numberFont;
	}

	public void setNumberFont(String numberFont) {
		this.numberFont = numberFont;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public boolean getIsBold() {
		return isBold;
	}

	public void setIsBold(boolean isBold) {
		this.isBold = isBold;
	}

}
