package element.bst.elementexploration.rest.extention.riskscore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.riskscore.service.RiskScoreService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Risk score API" },description="Manages riskscore")
@RestController
@RequestMapping("/api/riskScore")
public class RiskScoreController extends BaseController {

	@Autowired
	private RiskScoreService riskScoreService;
	
	@ApiOperation("Gets all risk models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllRiskModels(@RequestParam String token)throws Exception {
		return new ResponseEntity<>(riskScoreService.getAllRiskModels(), HttpStatus.OK);
	}
	
	@ApiOperation("Creates risk model")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/risk", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createRiskModel(@RequestParam String token,@RequestBody String jsonString)throws Exception {
		return new ResponseEntity<>(riskScoreService.createRiskModel(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Updates risk model")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PutMapping(value = "/risk", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> setModelId(@RequestParam String token,@RequestBody String jsonString)throws Exception {
		return new ResponseEntity<>(riskScoreService.setModelId(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets risk attribute")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk/attributeValues", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskAttributes(@RequestParam String token)throws Exception {
		return new ResponseEntity<>(riskScoreService.getRiskAttributes(), HttpStatus.OK);
	}
	
	@ApiOperation("Inserts attribute values")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/risk/attributeValues", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> insertRiskAttributes(@RequestParam String token,@RequestBody String jsonString)throws Exception {
		return new ResponseEntity<>(riskScoreService.insertRiskAttributes(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets default risk model")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk/defaultModel", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDefaultRiskModel(@RequestParam String token)throws Exception {
		return new ResponseEntity<>(riskScoreService.getDefaultRiskModel(), HttpStatus.OK);
	}
	
	@ApiOperation("Creates default risk model")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/risk/defaultModel", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createDefaultRiskModel(@RequestParam String token,@RequestBody String jsonString)throws Exception {
		return new ResponseEntity<>(riskScoreService.createDefaultRiskModel(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Checks provided name already exists or not, returns next usable counter for New Domain name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/risk/nameExists", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> checkNameExists(@RequestParam String token,@RequestBody String jsonString)throws Exception {
		return new ResponseEntity<>(riskScoreService.checkNameExists(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets risk relation")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk/relationshipValues", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskRelation(@RequestParam String token)throws Exception {
		return new ResponseEntity<>(riskScoreService.getRiskRelation(), HttpStatus.OK);
	}
	
	@ApiOperation("Inserts risk relation")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/risk/relationshipValues", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> insertRiskrelation(@RequestParam String token,@RequestBody String jsonString)throws Exception {
		return new ResponseEntity<>(riskScoreService.insertRiskrelation(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets OSINT sanctions list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk/sanctions-list", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSanctionsList(@RequestParam String token)throws Exception {
		return new ResponseEntity<>(riskScoreService.getSanctionsList(), HttpStatus.OK);
	}
	
	@ApiOperation("Deletes risk relation")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@DeleteMapping(value = "/risk/{modelId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteRiskModel(@RequestParam String token,@PathVariable String modelId)throws Exception {
		return new ResponseEntity<>(riskScoreService.deleteRiskModel(modelId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets risk model by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk/{modelId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskModel(@RequestParam String token,@PathVariable String modelId)throws Exception {
		return new ResponseEntity<>(riskScoreService.getRiskModel(modelId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets risk attributes insights")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/risk/attributeinsights", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskAttributesInsights(@RequestParam String token,@RequestParam String entityType)throws Exception {
		return new ResponseEntity<>(riskScoreService.getRiskAttributesInsights(entityType), HttpStatus.OK);
	}
	
	@ApiOperation("Gets riskscore of an entity by its ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/score/{entityId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskScoreByEntityId(@RequestParam String token,@PathVariable(required = false) String entityId)throws Exception {
		return new ResponseEntity<>(riskScoreService.getRiskScoreByEntityId(entityId), HttpStatus.OK);
	}
}
