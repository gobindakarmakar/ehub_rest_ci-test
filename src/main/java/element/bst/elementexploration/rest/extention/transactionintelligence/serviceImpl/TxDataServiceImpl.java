package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CountryDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionMasterView;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonReturnObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyGraphDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfoDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.InputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.NotificationDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.OutputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ScenariosResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAndAlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataNotifDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxProductTypeDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxsListAggRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.UpdateTransactionsDataDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AccountService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerDetailsService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerRelationService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.ShareholderService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.DaysCalculationUtility;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Service("txDataService")
public class TxDataServiceImpl extends GenericServiceImpl<TransactionsData, Long> implements TxDataService {

	private Random random = null;

	public TxDataServiceImpl() {
		this.random = new Random();
	}

	@Autowired
	public TxDataServiceImpl(GenericDao<TransactionsData, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	TxDataDao txDao;

	@Autowired
	AlertService alertService;

	@Autowired
	AccountDao accountDao;

	@Autowired
	CustomerDetailsDao customerDetailsDao;

	@Autowired
	CustomerDetailsService customerDetailsService;

	@Autowired
	CustomerRelationService customerRelationService;

	@Autowired
	AccountService accountService;

	@Autowired
	CountryDao countryDao;

	@Autowired
	private AlertDao alertDao;

	@Autowired
	DaysCalculationUtility daysCalulationUtility;

	@Autowired
	ShareholderService shareholderService;

	Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

	@Override
	@Transactional("transactionManager")
	public void saveTxData(MultipartFile multipartFile1, long userId) throws IOException, ParseException {

		BufferedReader br1 = new BufferedReader(new InputStreamReader(multipartFile1.getInputStream()));

		String line1 = null;

		int i = 1;
		try {
			while ((line1 = br1.readLine()) != null) {
				if (i != 1) {
					String[] data = null;
					if (line1.contains(","))
						data = line1.split(",");
					if (line1.contains(";"))
						data = line1.split(";");

					TransactionsData txData = new TransactionsData();
					txData.setOriginatorAccountId(data[0]);
					// System.out.println(data[0]);
					txData.setBeneficiaryAccountId(data[1]);
					txData.setTransProductType(data[2]);
					txData.setTransactionChannel(data[3]);
					txData.setAmount(Double.parseDouble(data[4]));
					txData.setCurrency(data[5]);
					DateFormat dateFormatIn = null;
					DateFormat dateFormatOut = null;
					String dateTo = data[6];
					if (dateTo.contains("-")) {
						dateFormatIn = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
						dateFormatOut = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
					}
					if (dateTo.contains("/")) {
						dateFormatIn = new SimpleDateFormat("dd/mm/yyyy hh:mm");
						dateFormatOut = new SimpleDateFormat("dd/mm/yyyy hh:mm");
					}

					Date dateIn = dateFormatIn.parse(data[6]);
					String dateOut = dateFormatOut.format(dateIn);
					txData.setBusinessDate(dateFormatOut.parse(dateOut));
					// System.out.println(txData.getBusinessDate());
					txData.setCustomText(data[7]);
					txData.setBenficiaryBankName(data[8]);
					if (data.length > 9)
						txData.setCountryOfTransaction(data[9]);
					if (data.length > 10)
						txData.setBeneficiaryName(data[10]);
					if (data.length > 11)
						txData.setOriginatorName(data[11]);
					if (data.length > 12)
						txData.setCountryOfBeneficiary(data[12]);
					if (data.length > 13)
						txData.setAtmAddress(data[13]);
					if (data.length > 14)
						txData.setMerchant(data[14]);
					if (data.length == 16)
						txData.setMerchantWebsite(data[15]);

					if (txData.getBeneficiaryAccountId() != null) {
						Account beneficiaryAccount = accountDao.fetchAccount(txData.getBeneficiaryAccountId());
						if (beneficiaryAccount != null) {
							CustomerDetails beneficiaryCustomer = beneficiaryAccount.getCustomerDetails();
							if (beneficiaryCustomer != null) {
								Long beneficiaryCustomerId = beneficiaryCustomer.getId();
								txData.setBeneficiaryCutomerId(beneficiaryCustomerId);
								if (txData.getCountryOfBeneficiary() == null
										|| "".equals(txData.getCountryOfBeneficiary())) {
									txData.setCountryOfBeneficiary(beneficiaryCustomer.getResidentCountry());
								}
							}

						}
					}
					if (txData.getOriginatorAccountId() != null) {
						Account originatorAccount = accountDao.fetchAccount(txData.getOriginatorAccountId());
						if (originatorAccount != null) {
							CustomerDetails orginatorCustomer = originatorAccount.getCustomerDetails();
							if (orginatorCustomer != null) {
								Long orginatorCustomerId = orginatorCustomer.getId();
								txData.setOriginatorCutomerId(orginatorCustomerId);
							}
						}
					}
					TransactionsData transactionsData = txDao.create(txData);// saving
					alertService.createAlert(transactionsData, userId);

				}
				i++;
			}

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
	}

	@Async
	@Transactional("transactionManager")
	public Boolean generateAlert(List<TxDto> listTx) {
		boolean value = accountDao.checkExistenceOfAccount(listTx);

		if (value == true) // if account exists,then only tx audited
		{
			if (listTx.size() > 1) {
				TransactionsData data = null;
				for (TxDto d : listTx) {
					data = new TransactionsData();
					if (d.getPartyRole().equalsIgnoreCase("ORG")) {
						data.setOriginatorAccountId(d.getPartyIdentifier());
					} else {
						data.setBeneficiaryAccountId(d.getPartyIdentifier());
					}
					data.setCurrency(listTx.get(0).getCurrency());
					data.setAmount(Double.parseDouble(listTx.get(0).getAmount()));
					data.setCustomText(listTx.get(0).getCustomText());
					data.setBusinessDate(listTx.get(0).getBusinessDate());
					data.setTransProductType(listTx.get(0).getTransProductType());
					data.setTransactionChannel(listTx.get(0).getTransactionChannel());
				}
				txDao.create(data);// saving
				alertService.createAlert(data, 1);

			}
			if (listTx.size() == 1) {
				TransactionsData data = new TransactionsData();
				if (listTx.get(0).getPartyRole().equalsIgnoreCase("REM")) {
					data.setOriginatorAccountId(listTx.get(0).getPartyIdentifier());
				}
				if (listTx.get(0).getPartyRole().equalsIgnoreCase("ORG")) {
					data.setOriginatorAccountId(listTx.get(0).getPartyIdentifier());
				}

				if (listTx.get(0).getPartyRole().equalsIgnoreCase("BENE")) {
					data.setBeneficiaryAccountId(listTx.get(0).getPartyIdentifier());
				}
				data.setCurrency(listTx.get(0).getCurrency());
				data.setCustomText(listTx.get(0).getCustomText());
				data.setAmount(Double.parseDouble(listTx.get(0).getAmount()));
				data.setBusinessDate(listTx.get(0).getBusinessDate());
				data.setTransProductType(listTx.get(0).getTransProductType());
				data.setTransactionChannel(listTx.get(0).getTransactionChannel());
				txDao.create(data);// saving
				alertService.createAlert(data, 1);

			}

		}
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public ResponseDto fetchAllTxsBasedOnEntity(List<String> entityIds, String transactionType, String minAmount,
			String locations, String minTxs, long alertId, long alertTransactionId)
			throws IllegalAccessException, InvocationTargetException {
		ResponseDto respDto = new ResponseDto();
		List<TxEntityRespDto> listDto = new ArrayList<TxEntityRespDto>();
		List<Account> accountlist = new ArrayList<Account>();
		for (String entity : entityIds) {
			// checking Customer Table with focalEntityId as customerNumber
			CustomerDetails customerDetails = customerDetailsDao.getCustomerDetails(entity, locations);

			if (customerDetails != null) {
				accountlist = accountDao.fetchAccounts(customerDetails.getCustomerNumber());

				if (!accountlist.isEmpty()) {
					for (Account account : accountlist) {
						List<TransactionsData> data = txDao.getTransaction(account.getAccountNumber(), transactionType,
								minAmount, minTxs);
						for (TransactionsData transactionsData : data) {
							if (transactionsData != null) {
								TxEntityRespDto dto = new TxEntityRespDto();

								// checking originator
								if (transactionsData.getOriginatorAccountId() != null
										&& !"".equals(transactionsData.getOriginatorAccountId())) {
									BeanUtils.copyProperties(dto, transactionsData);
									// setting beneficiary country
									if (transactionsData.getBeneficiaryAccountId() != null) {
										// System.out.println(transactionsData.getCountryOfBeneficiary()+"*************");
										if ((transactionsData.getCountryOfBeneficiary() != "")
												|| (transactionsData.getCountryOfBeneficiary() != null))
											dto.setCountryBene(transactionsData.getCountryOfBeneficiary());
										else {
											if (transactionsData.getBeneficiaryCutomerId() != null) {
												CustomerDetails customerDetailsFromTransaction = customerDetailsDao
														.find(transactionsData.getBeneficiaryCutomerId());
												if (customerDetailsFromTransaction != null) {
													dto.setCountryBene(customerDetails.getResidentCountry());
												}
											}
										}
										// name
										if (transactionsData.getBeneficiaryName() == ""
												|| transactionsData.getBeneficiaryName() == null)
											dto.setBeneficiaryName(transactionsData.getMerchant());
										else
											dto.setBeneficiaryName(transactionsData.getBeneficiaryName());
										dto.setAmountBene(transactionsData.getAmount());
										dto.setCurrencyBen(transactionsData.getCurrency());
										dto.setBeneId(transactionsData.getId());
										dto.setDateBene(transactionsData.getBusinessDate());
										dto.setDebitOrCredit("D");
									}
									// setting originator details
									dto.setCountryOrg(transactionsData.getCountryOfTransaction());
									dto.setAmountOrig(transactionsData.getAmount());
									dto.setOriginatorName(transactionsData.getOriginatorName());
									dto.setCurrencyOrg(transactionsData.getCurrency());
									dto.setOrgId(transactionsData.getId());
									dto.setDateOrg(transactionsData.getBusinessDate());
									dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());
									dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId()));

									/*
									 * if
									 * (transactionsData.getOriginatorAccountId().equals(account.getAccountNumber())
									 * ) { // setting beneficiary country & name if
									 * (transactionsData.getBeneficiaryAccountId() != null) { Account accountBen =
									 * accountDao.fetchAccount(transactionsData.getBeneficiaryAccountId()); //
									 * dto.setCountryBene(customerDetails.getResidentCountry()); if
									 * (account.getCustomerDetails() != null) {
									 * dto.setCountryBene(accountBen.getCustomerDetails().getResidentCountry());
									 * dto.setBeneficiaryName(accountBen.getCustomerDetails().getDisplayName()); }
									 * dto.setAmountBene(transactionsData.getAmount());
									 * dto.setCurrencyBen(transactionsData.getCurrency());
									 * dto.setBeneId(transactionsData.getId());
									 * dto.setDateBene(transactionsData.getBusinessDate());
									 * dto.setDebitOrCredit("D"); } // setting originator details
									 * dto.setCountryOrg(customerDetails.getResidentCountry());
									 * dto.setAmountOrig(transactionsData.getAmount());
									 * dto.setOriginatorName(customerDetails.getDisplayName());
									 * dto.setCurrencyOrg(transactionsData.getCurrency());
									 * dto.setOrgId(transactionsData.getId());
									 * dto.setDateOrg(transactionsData.getBusinessDate());
									 * BeanUtils.copyProperties(dto, transactionsData);
									 * dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());
									 * dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId())
									 * );
									 */
								} else {
									// setting originator details
									BeanUtils.copyProperties(dto, transactionsData);
									if (transactionsData.getBeneficiaryName() == ""
											|| transactionsData.getBeneficiaryName() == null)
										dto.setBeneficiaryName(transactionsData.getMerchant());
									else
										dto.setBeneficiaryName(transactionsData.getBeneficiaryName());
									// dto.setCountryBene(transactionsData.getCountryOfBeneficiary());
									if (transactionsData.getBeneficiaryAccountId() != null) {
										if ((transactionsData.getCountryOfBeneficiary() != "")
												|| (transactionsData.getCountryOfBeneficiary() != null))
											dto.setCountryBene(transactionsData.getCountryOfBeneficiary());
										else {
											if (transactionsData.getBeneficiaryCutomerId() != null) {
												CustomerDetails customerDetailsFromTransaction = customerDetailsDao
														.find(transactionsData.getBeneficiaryCutomerId());
												if (customerDetailsFromTransaction != null) {
													dto.setCountryBene(customerDetails.getResidentCountry());
												}
											}
										}
										dto.setAmountBene(transactionsData.getAmount());
										dto.setCurrencyBen(transactionsData.getCurrency());
										dto.setBeneId(transactionsData.getId());
										dto.setDateBene(transactionsData.getBusinessDate());
										dto.setDebitOrCredit("C");
										dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId()));
										dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());

										dto.setCountryOrg(transactionsData.getCountryOfTransaction());
										dto.setAmountOrig(transactionsData.getAmount());
										if (transactionsData.getOriginatorName() == ""
												|| transactionsData.getOriginatorName() == null)
											dto.setOriginatorName(transactionsData.getMerchant());
										else
											dto.setOriginatorName(transactionsData.getOriginatorName());
										// dto.setOriginatorName(transactionsData.getOriginatorName());
										dto.setCurrencyOrg(transactionsData.getCurrency());
										dto.setOrgId(transactionsData.getId());
										dto.setDateOrg(transactionsData.getBusinessDate());

										/*
										 * dto.setCountryOrg(customerDetails.getResidentCountry());
										 * dto.setAmountOrig(transactionsData.getAmount());
										 * dto.setOriginatorName(customerDetails.getDisplayName());
										 * dto.setCurrencyOrg(transactionsData.getCurrency());
										 * dto.setOrgId(transactionsData.getId());
										 * dto.setDateOrg(transactionsData.getBusinessDate());
										 * BeanUtils.copyProperties(dto, transactionsData);
										 * dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId())
										 * ); dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());
										 */

									}
									/*
									 * // checking beneficiary if (transactionsData.getBeneficiaryAccountId() !=
									 * null) { if(transactionsData.getOriginatorAccountId()!=null) {
									 * dto.setCountryOrg(transactionsData.getCountryOfTransaction());
									 * dto.setOriginatorName(transactionsData.getOriginatorName());
									 * dto.setAmountOrig(transactionsData.getAmount());
									 * dto.setCurrencyOrg(transactionsData.getCurrency());
									 * dto.setOrgId(transactionsData.getId());
									 * dto.setDateOrg(transactionsData.getBusinessDate());
									 * dto.setDebitOrCredit("C"); } // setting beneficiarydetails
									 * dto.setBeneficiaryName(transactionsData.getBeneficiaryName());
									 * dto.setCountryBene(transactionsData.getCountryOfBeneficiary());
									 * dto.setAmountBene(transactionsData.getAmount());
									 * dto.setCurrencyBen(transactionsData.getCurrency());
									 * dto.setBeneId(transactionsData.getId());
									 * dto.setDateBene(transactionsData.getBusinessDate());
									 * BeanUtils.copyProperties(dto, transactionsData);
									 * dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId())
									 * ); dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());
									 * } else { dto.setCountryOrg(transactionsData.getCountryOfTransaction());
									 * dto.setAmountOrig(transactionsData.getAmount());
									 * dto.setOriginatorName(transactionsData.getOriginatorName());
									 * dto.setCurrencyOrg(transactionsData.getCurrency());
									 * dto.setOrgId(transactionsData.getId());
									 * dto.setDateOrg(transactionsData.getBusinessDate());
									 * BeanUtils.copyProperties(dto, transactionsData);
									 * dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId())
									 * ); dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());
									 * 
									 * dto.setBeneficiaryName(customerDetails.getDisplayName());
									 * dto.setCountryBene(customerDetails.getResidentCountry());
									 * dto.setAmountBene(transactionsData.getAmount());
									 * dto.setCurrencyBen(transactionsData.getCurrency());
									 * dto.setBeneId(transactionsData.getId());
									 * dto.setDateBene(transactionsData.getBusinessDate());
									 * BeanUtils.copyProperties(dto, transactionsData);
									 * dto.setAlert(alertDao.findAlertByAlertTransactionId(transactionsData.getId())
									 * ); dto.setPrimaryCustomerIdentifier(account.getPrimaryCustomerIdentifier());
									 * }
									 */
								} // single transaction not found close if
								listDto.add(dto);
							} // transactions for each
						} // for each account
					} // account close

				} // customer close
			} // main for each multiple close

			// System.out.println("befor filtering size :" + listDto.size());

			/*
			 * //checking total have to remove Double inputAmount = 0.0; Double outputAmount
			 * =0.0; for (TxEntityRespDto txEntity : listDto) {
			 * 
			 * if(txEntity.getPrimaryCustomerIdentifier()== entityIds.get(0) &&
			 * txEntity.get) { txEntity.setAlertedTransaction(true); } }
			 */

			// checking isAlertedTransaction with alertId
			if (alertTransactionId != 0.0) {
				for (TxEntityRespDto txEntity : listDto) {

					if (txEntity.getId() == alertTransactionId) {
						txEntity.setAlertedTransaction(true);
					}
				}
			}

			// applying filter locations
			List<String> listLocations = new ArrayList<String>();
			if (locations != null) {
				listLocations = Arrays.asList(locations.split("\\*s,\\*s"));
				// System.out.println(listLocations.toString());

				Iterator<TxEntityRespDto> it = listDto.iterator();
				while (it.hasNext()) {
					TxEntityRespDto objToRemove = it.next();
					a: for (String loc : listLocations) // checking list of loc with
														// tx counterparties country
					{
						Country country = countryDao.getCountry(loc);
						if (country != null) {
							for (Account accFound : accountlist) {
								if (accFound.getAccountNumber().equals(objToRemove.getOriginatorAccountId()))// found
																												// in
																												// originator
								{
									if (objToRemove.getCountryBene() != null) {
										if (!objToRemove.getCountryBene().equals(country.getIso2Code())) {
											it.remove();// remove transaction
											break a;
										}
									}
								} else {
									if (objToRemove.getCountryOrg() != null) {
										if (!objToRemove.getCountryOrg().equals(country.getIso2Code())) {
											it.remove();// remove transaction
											break a;
										}
									}

								}

							}
						}
					}
				}

			}
		}
		respDto.setListTxDtos(listDto);
		return respDto;
	}

	@Override
	@Transactional("transactionManager")
	public InputStatsResponseDto inputStats(List<String> entityIds, String counterPartyMin, String counterPartyMax,
			String minTxs, String minAmount, String location, String transactionType)
			throws IllegalAccessException, InvocationTargetException {

		List<String> locations = null;
		if (location != null && location != "")
			locations = Arrays.asList(location.split(","));

		InputStatsResponseDto inputStatsDto = new InputStatsResponseDto();
		TotalDto totalDto = txDao.getTotalAggregate(entityIds, minAmount, locations, transactionType);
		List<CounterPartyDto> counterDtos = txDao.getCounterPartiesAggregate(entityIds, counterPartyMin,
				counterPartyMax, minTxs, locations, transactionType, minAmount);
		for (CounterPartyDto counterPartyDto : counterDtos) {
			if (!"Unknown".equalsIgnoreCase(counterPartyDto.getCustomerNumer()))
				counterPartyDto.setPoint(customerDetailsDao.fetchCustomerDetails(counterPartyDto.getCustomerNumer()));
			if ("Unknown".equalsIgnoreCase(counterPartyDto.getCustomerNumer())) {
				CustomerDetails point = new CustomerDetails();
				counterPartyDto.setPoint(point);
			}
		}
		List<CountryAggDto> countryaggregate = txDao.getCountryAggregate(entityIds, locations, minTxs, transactionType,
				minAmount);
		for (CountryAggDto testDtoCountry : countryaggregate) {
			if (!"Unknown".equalsIgnoreCase(testDtoCountry.getCountryCode()))
				testDtoCountry.setCountry(countryDao.findByName(testDtoCountry.getCountryCode()));
			if ("Unknown".equalsIgnoreCase(testDtoCountry.getCountryCode())) {
				Country countryPoint = new Country();
				countryPoint.setBaselAMLIndex2017(0.0);
				testDtoCountry.setCountry(countryPoint);
			}
		}
		inputStatsDto.setTotalDto(totalDto);
		inputStatsDto.setCounterPartyDto(counterDtos);
		inputStatsDto.setCountry(countryaggregate);

		return inputStatsDto;
	}

	@Override
	@Transactional("transactionManager")
	public OutputStatsResponseDto outputStats(List<String> entityIds, String counterPartyMin, String counterPartyMax,
			String minTxs, String minAmount, String location, String transactionType) {

		List<String> locations = null;
		if (location != null && location != "")
			locations = Arrays.asList(location.split(","));
		OutputStatsResponseDto outputStatsDto = new OutputStatsResponseDto();
		TotalDto totalDto = txDao.getTotalAggregateForOutputStats(entityIds, minAmount, locations, transactionType);
		List<CounterPartyDto> counterDtos = txDao.getCounterPartiesAggregateForOutputStats(entityIds, counterPartyMin,
				counterPartyMax, minTxs, locations, transactionType, minAmount);
		for (CounterPartyDto counterPartyDto : counterDtos) {
			if (!"Unknown".equalsIgnoreCase(counterPartyDto.getCustomerNumer()))
				counterPartyDto.setPoint(customerDetailsDao.fetchCustomerDetails(counterPartyDto.getCustomerNumer()));
			if ("Unknown".equalsIgnoreCase(counterPartyDto.getCustomerNumer())) {
				CustomerDetails point = new CustomerDetails();
				counterPartyDto.setPoint(point);
			}
		}
		List<CountryAggDto> countryaggregate = txDao.getCountryAggregateForOutputStats(entityIds, locations, minTxs,
				transactionType, minAmount);
		for (CountryAggDto testDtoCountry : countryaggregate) {
			if (!"Unknown".equalsIgnoreCase(testDtoCountry.getCountryCode()))
				testDtoCountry.setCountry(countryDao.findByName(testDtoCountry.getCountryCode()));
			if ("Unknown".equalsIgnoreCase(testDtoCountry.getCountryCode())) {
				Country countryPoint = new Country();
				countryPoint.setBaselAMLIndex2017(0.0);
				testDtoCountry.setCountry(countryPoint);
			}
		}
		outputStatsDto.setTotalDto(totalDto);
		outputStatsDto.setCounterPartyDto(counterDtos);
		outputStatsDto.setCountry(countryaggregate);

		return outputStatsDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<TransactionsData> getAllTransactions() {
		List<TransactionsData> transactionList = txDao.getAllTransactions();
		return transactionList;
	}

	@Override
	@Transactional("transactionManager")
	public NotificationDto fetchTxsBwDates(String fromDate, String toDate, String productType, FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException {

		NotificationDto notificationDto = new NotificationDto();

		// counterparties
		List<CounterPartyNotiDto> counterPartiesList = txDao.fetchCounterParties(fromDate, toDate, filterDto);
		notificationDto.setCounterpartiesNotifDtos(counterPartiesList);

		// topFiveTransactions
		List<CounterPartyNotiDto> topFiveTransactionsList = txDao.fetchTopFiveTransactions(fromDate, toDate, filterDto);
		notificationDto.setTopFiveTransactions(topFiveTransactionsList);

		notificationDto.setCorporateStructureDto(txDao.fetchCorporateStructure(fromDate, toDate, filterDto));
		List<TxProductTypeDto> txproductType = txDao.fetchSumTxsProductType(fromDate, toDate, filterDto);
		// rounding amount
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		for (TxProductTypeDto typeObject : txproductType) {
			typeObject.setAmountTotal(Double.valueOf(decimalFormat.format(typeObject.getAmountTotal())));
		}
		List<TxProductTypeDto> alertproductType = txDao.fetchAlertTransactionProductCount(fromDate, toDate, filterDto);
		for (TxProductTypeDto alertTypeObject : alertproductType) {
			alertTypeObject.setAmountTotal(Double.valueOf(decimalFormat.format(alertTypeObject.getAmountTotal())));
		}
		notificationDto.setAlertProductTypeGroupByAndCountList(alertproductType);
		List<AlertStatusDto> alertStatusDto = alertService.fetchGroupByAlertStatus(fromDate, toDate, false, filterDto);

		notificationDto.setTransactionsProductTypeGroupByAndCountList(txproductType);
		notificationDto.setAlertStatusDto(alertStatusDto);
		return notificationDto;
	}

	@Override
	@Transactional("transactionManager")
	public TxsListAggRespDto fetchTxsTotalListBwDates(String fromDate, String toDate, String productType,
			Integer recordsPerPage, Integer pageNumber, Boolean input, Boolean output, FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		TxsListAggRespDto txAgg = new TxsListAggRespDto();
		// list of txs for view all
		List<TransactionsDataNotifDto> dataNotifDtos = txDao.fetchTxsBwDatesCustomized(fromDate, toDate,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, null, input, output, filterDto);
		List<AlertNotiDto> alertNotiDtos = alertDao.fetchAlertsBwDatesCustomized(fromDate, toDate, null, input, output,
				filterDto);
		// setting alert amount to txs on respective date
		for (TransactionsDataNotifDto txl : dataNotifDtos) {
			for (AlertNotiDto alertNotiDto : alertNotiDtos) {
				int val = txl.getTxDate().compareTo(alertNotiDto.getAlertBusinessDate());
				if (val == 0) {
					txl.setAlertAmount(alertNotiDto.getAlertAmount());
				}
			}
		}
		List<TransactionAmountAndAlertCountDto> agg = txDao.getTotalAmountAndAlertCountByProductType(fromDate, toDate,
				false, recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, false, false, null, filterDto);
		txAgg.setTransactionsDataList(dataNotifDtos);
		txAgg.setTxAlertCountAmountAgg(agg);
		return txAgg;
	}

	@Override
	@Transactional("transactionManager")
	public List<TransactionAmountAndAlertCountDto> getTotalAmountAndAlertCountByProductType(String fromDate,
			String toDate, boolean groupBy, boolean input, boolean output, String type, FilterDto filterDto)
			throws ParseException {
		// List<TransactionAmountAndAlertCountDto> inputAndOutput =
		// getInputAndOutputCount(fromDate, toDate);
		List<TransactionAmountAndAlertCountDto> response = new ArrayList<TransactionAmountAndAlertCountDto>();

		List<TransactionAmountAndAlertCountDto> responseInput = new ArrayList<TransactionAmountAndAlertCountDto>();
		List<TransactionAmountAndAlertCountDto> responseOutput = new ArrayList<TransactionAmountAndAlertCountDto>();

		try {
			response = txDao.getTotalAmountAndAlertCountByProductType(fromDate, toDate, groupBy, 0, 0, input, output,
					type, filterDto);

			responseInput = txDao.getTotalAmountAndAlertCountByProductType(fromDate, toDate, true, 0, 0, true, false,
					null, filterDto);

			responseOutput = txDao.getTotalAmountAndAlertCountByProductType(fromDate, toDate, true, 0, 0, false, true,
					null, filterDto);

			if (groupBy)

			{
				for (TransactionAmountAndAlertCountDto responseDto : response) {

					for (TransactionAmountAndAlertCountDto inputAndOutputDto : responseInput) {
						if (responseDto.getTransactionProductType()
								.equals(inputAndOutputDto.getTransactionProductType())) {
							responseDto.setInputCount(inputAndOutputDto.getTransactionsCount());
							// responseDto.setOutputCount(inputAndOutputDto.getOutputCount());
						}
					}
				}
				for (TransactionAmountAndAlertCountDto responseDto : response) {

					for (TransactionAmountAndAlertCountDto inputAndOutputDto : responseOutput) {
						if (responseDto.getTransactionProductType()
								.equals(inputAndOutputDto.getTransactionProductType())) {
							// responseDto.setInputCount(inputAndOutputDto.getInputCount());
							responseDto.setOutputCount(inputAndOutputDto.getTransactionsCount());
						}
					}
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return response;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getAlertsBetweenDates(String fromDate, String toDate, String name,
			Integer pageNumber, Integer recordsPerPage, String orderIn, FilterDto filterDto, boolean isTimeStamp) {
		List<AlertDashBoardRespDto> alertDashBoardRespDtos = txDao.getAlertsBetweenDates(fromDate, toDate, name,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderIn, filterDto,
				isTimeStamp);

		/*
		 * for (AlertDashBoardRespDto alertDashBoardRespDto : alertDashBoardRespDtos) {
		 * //System.out.println(alertDashBoardRespDto.getAlertCreatedDate());
		 * 
		 * }
		 */
		return alertDashBoardRespDtos;
	}

	@Override
	@Transactional("transactionManager")
	public TransactionAndAlertAggregator getTransactionAndAlertAggregates(String fromDate, String toDate,
			String granularity, Boolean isGroupByType, boolean input, boolean output, String type, FilterDto filterDto)
			throws ParseException {
		TransactionAndAlertAggregator aggregator = new TransactionAndAlertAggregator();
		try {
			aggregator.setTransactionAggregator(txDao.getTransactionAggregates(fromDate, toDate, granularity, false,
					type, input, output, filterDto));
			aggregator.setAlertAggregator(
					alertDao.getAlertAggregates(fromDate, toDate, granularity, false, type, input, output, filterDto));
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());

		}
		return aggregator;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, TransactionAndAlertAggregator> getTransactionAndAlertAggregatesByProducType(String fromDate,
			String toDate, String granularity, boolean isGroupByType, boolean input, boolean output, String type,
			FilterDto filterDto) throws ParseException {
		Map<String, TransactionAndAlertAggregator> aggregatorMap = new HashMap<String, TransactionAndAlertAggregator>();
		List<String> transTypes = txDao.getTransProductTypes();
		try {
			if (type != null) {
				TransactionAndAlertAggregator aggregator = new TransactionAndAlertAggregator();
				aggregator.setTransactionAggregator(txDao.getTransactionAggregatesProductType(fromDate, toDate,
						granularity, true, type, input, output, filterDto));
				aggregator.setAlertAggregator(alertDao.getAlertAggregatesProductType(fromDate, toDate, granularity,
						true, type, input, output, filterDto));
				aggregatorMap.put(type, aggregator);
			} else {
				for (String transType : transTypes) {
					TransactionAndAlertAggregator aggregator = new TransactionAndAlertAggregator();
					aggregator.setTransactionAggregator(txDao.getTransactionAggregatesProductType(fromDate, toDate,
							granularity, true, transType, input, output, filterDto));
					aggregator.setAlertAggregator(alertDao.getAlertAggregatesProductType(fromDate, toDate, granularity,
							true, transType, input, output, filterDto));
					aggregatorMap.put(transType, aggregator);
				}
			}

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return aggregatorMap;
	}

	@Override
	@Transactional("transactionManager")
	public Long getAlertCountBetweenDates(String fromDate, String toDate, String name, FilterDto filterDto,
			boolean isTimeStamp) {
		return txDao.getAlertCountBetweenDates(fromDate, toDate, name, filterDto, isTimeStamp);
	}

	@Override
	@Transactional("transactionManager")
	public long fetchTxsBwDatesCustomizedCount(String fromDate, String toDate, String productType, boolean input,
			boolean output, FilterDto filterDto) throws ParseException {
		return txDao.fetchTxsBwDatesCustomizedCount(fromDate, toDate, productType, input, output, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public TxsListAggRespDto fetchTxByTypeListViewAll(String fromDate, String toDate, String productType,
			Integer recordsPerPage, Integer pageNumber, boolean input, boolean output, FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		TxsListAggRespDto txAgg = new TxsListAggRespDto();
		// list of txs for view all
		List<TransactionsDataNotifDto> dataNotifDtos = txDao.fetchTxsBwDatesCustomized(fromDate, toDate,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, productType, input, output,
				filterDto);
		List<AlertNotiDto> alertNotiDtos = alertDao.fetchAlertsBwDatesCustomized(fromDate, toDate, productType, input,
				output, filterDto);
		// setting alert amount to txs on respective date
		for (TransactionsDataNotifDto txl : dataNotifDtos) {
			System.out.println();
			for (AlertNotiDto alertNotiDto : alertNotiDtos) {
				int val = txl.getTxDate().compareTo(alertNotiDto.getAlertBusinessDate());
				if (val == 0) {
					txl.setAlertAmount(alertNotiDto.getAlertAmount());
				}
			}
		}
		txAgg.setTransactionsDataList(dataNotifDtos);
		return txAgg;
	}

	@Override
	@Transactional("transactionManager")
	public List<AssociatedEntityRespDto> fetchAssociatedEntity(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {

		List<AssociatedEntityRespDto> associatedEntityRespDtos = txDao.fetchAssociatedEntity(fromDate, toDate,
				filterDto);

		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		for (AssociatedEntityRespDto associatedEntityRespDto : associatedEntityRespDtos) {
			associatedEntityRespDto
					.setAmount(Double.valueOf(decimalFormat.format(associatedEntityRespDto.getAmount())));
		}

		try {
			for (AssociatedEntityRespDto associatedEntity : associatedEntityRespDtos) {
				CustomerDetails customerDetails = customerDetailsDao.find(associatedEntity.getCustomerId());
				if (customerDetails != null)
					associatedEntity.setCustomerName(customerDetails.getDisplayName());
			}

			Iterator<AssociatedEntityRespDto> iterator = associatedEntityRespDtos.iterator();
			while (iterator.hasNext()) {
				AssociatedEntityRespDto entityRespDto = iterator.next();
				if (entityRespDto.getCustomerId() == 0)
					iterator.remove();
			}

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return associatedEntityRespDtos;
	}

	@Transactional("transactionManager")
	public RiskCountAndRatioDto getCustomerRisk(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		filterDto.setCustomerRiskType(true);
		RiskCountAndRatioDto response = txDao.getCustomerRisk(fromDate, toDate, null, filterDto);
		response.setType("CustomerRisk");
		return response;

	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getCustomerRiskAggregates(String fromDate, String toDate, FilterDto filterDto) {
		filterDto.setCustomerRiskType(true);
		return txDao.getCustomerRiskAggregates(fromDate, toDate, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getGeoGraphicRisk(String fromDate, String toDate, FilterDto filterDto) {
		filterDto.setGeographyRiskType(true);
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getRiskAlerts(fromDate, toDate, null, filterDto);
		riskCountAndRatioDto.setType("GeoGraphicRisk");
		return riskCountAndRatioDto;
	}

	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getCorruptionRisk(String fromDate, String toDate, FilterDto filterDto) {
		String queryforCondition = "(t.transparencyRisk>0 or t.worldBankRisk>0 or t.wefRisk>0)";
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getRiskAlerts(fromDate, toDate, queryforCondition, filterDto);
		riskCountAndRatioDto.setType("CorruptionRisk");
		return riskCountAndRatioDto;
	}

	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getPoliticalRisk(String fromDate, String toDate, FilterDto filterDto) {
		String queryforCondition = "(t.freedomHouserisk>0 or t.worldJusticeRisk>0 or t.wefRisk>0)";
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getRiskAlerts(fromDate, toDate, queryforCondition, filterDto);
		riskCountAndRatioDto.setType("PoliticalRisk");
		return riskCountAndRatioDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getGeoGraphicRiskAggregates(String fromDate, String toDate, FilterDto filterDto) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();

		for (int i = 1; i <= 3; i++) {
			RiskCountAndRatioDto transactionAmountAndAlertCountDto = new RiskCountAndRatioDto();

			if (i == 1) {
				String value = "t.fatfRisk>0";
				transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate, toDate, value, filterDto);
				if (transactionAmountAndAlertCountDto != null)

					transactionAmountAndAlertCountDto.setType("FATF High Risk Countries");

			} else if (i == 2) {
				String value = "t.taskJusticeRisk>0";
				transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate, toDate, value, filterDto);
				if (transactionAmountAndAlertCountDto != null)

					transactionAmountAndAlertCountDto
							.setType("Tax Justice Network High Risk Countries (Finacial Secracy Index)");

			} else {
				String value = " t.internationalNarcoticsRisk>0";
				transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate, toDate, value, filterDto);
				if (transactionAmountAndAlertCountDto != null)

					transactionAmountAndAlertCountDto.setType("International Narcotics Control High Risk Countries");

			}
			if (transactionAmountAndAlertCountDto != null)
				list.add(transactionAmountAndAlertCountDto);
		}
		list.sort(Comparator.comparingDouble(RiskCountAndRatioDto::getAlertAmount).reversed());

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getCorruptionRiskAggregates(String fromDate, String toDate, FilterDto filterDto) {
		String queryforCondition = "(t.transparencyRisk>0 or t.worldBankRisk>0 or t.wefRisk>0)";
		List<RiskCountAndRatioDto> list = txDao.getCorruptionRiskAggregates(fromDate, toDate, queryforCondition,
				filterDto);

		/*
		 * for (int i = 1; i <= 3; i++) { RiskCountAndRatioDto
		 * transactionAmountAndAlertCountDto = new RiskCountAndRatioDto();
		 * 
		 * if (i == 1) { String value = "t.transparencyRisk>0";
		 * transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate,
		 * toDate, value,filterDto); if (transactionAmountAndAlertCountDto != null)
		 * 
		 * transactionAmountAndAlertCountDto.
		 * setType("Transparancy International High Risk Countries");
		 * 
		 * } else if (i == 2) { String value = "t.worldBankRisk>0";
		 * transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate,
		 * toDate, value,filterDto); if (transactionAmountAndAlertCountDto != null)
		 * transactionAmountAndAlertCountDto.setType("World Bank High Risk Countries");
		 * 
		 * } else { String value = "t.wefRisk>0"; transactionAmountAndAlertCountDto =
		 * txDao.getRiskAlertsAggregates(fromDate, toDate, value,filterDto); if
		 * (transactionAmountAndAlertCountDto != null)
		 * transactionAmountAndAlertCountDto.
		 * setType("WEF Global Competitiveness High Risk Countries");
		 * 
		 * } list.add(transactionAmountAndAlertCountDto); }
		 */
		list.sort(Comparator.comparingDouble(RiskCountAndRatioDto::getAlertAmount).reversed());

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getPoliticalRiskAggregates(String fromDate, String toDate, FilterDto filterDto) {
		String queryforCondition = "(t.freedomHouserisk>0 or t.worldJusticeRisk>0 or t.wefRisk>0)";
		List<RiskCountAndRatioDto> list = txDao.getCorruptionRiskAggregates(fromDate, toDate, queryforCondition,
				filterDto);

		/*
		 * for (int i = 1; i <= 3; i++) { RiskCountAndRatioDto
		 * transactionAmountAndAlertCountDto = new RiskCountAndRatioDto();
		 * 
		 * if (i == 1) { String value = "t.freedomHouserisk>0";
		 * transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate,
		 * toDate, value,filterDto); transactionAmountAndAlertCountDto.
		 * setType("Freedom House - Freedom in the World High Risk Countries");
		 * 
		 * } else if (i == 2) { String value = "t.worldJusticeRisk>0";
		 * transactionAmountAndAlertCountDto = txDao.getRiskAlertsAggregates(fromDate,
		 * toDate, value,filterDto); if (transactionAmountAndAlertCountDto != null)
		 * transactionAmountAndAlertCountDto.setType("World Justice Project");
		 * 
		 * } else { String value = "t.wefRisk>0"; transactionAmountAndAlertCountDto =
		 * txDao.getRiskAlertsAggregates(fromDate, toDate, value,filterDto); if
		 * (transactionAmountAndAlertCountDto != null)
		 * transactionAmountAndAlertCountDto.
		 * setType("WEF Global Competitiveness High Risk Countries");
		 * 
		 * } list.add(transactionAmountAndAlertCountDto); }
		 */
		list.sort(Comparator.comparingDouble(RiskCountAndRatioDto::getAlertAmount).reversed());

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public ScenariosResponseDto getScenariosByType(String fromDate, String toDate, String type, String filterType,
			FilterDto filterDto) throws ParseException {
		// Map<Integer, List<RiskCountAndRatioDto>> map = new HashMap<Integer,
		// List<RiskCountAndRatioDto>>();
		ScenariosResponseDto response = new ScenariosResponseDto();
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getCustomerRisk(fromDate, toDate, filterType, filterDto);
		List<RiskCountAndRatioDto> listScenarios = alertDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(type);
		for (RiskCountAndRatioDto dto : listScenarios) {
			scenarios.add(dto.getType());
		}
		riskCountAndRatioDto.setScenario(scenarios);
		list.add(riskCountAndRatioDto);
		response.setRiskCountAndRatioDtos(list);
		response.setScenariosList(listScenarios);
		return response;
	}

	@Override
	@Transactional("transactionManager")
	public Map<Integer, List<RiskCountAndRatioDto>> getCorruptionRiskByType(String fromDate, String toDate, String type,
			FilterDto filterDto) throws ParseException {
		Map<Integer, List<RiskCountAndRatioDto>> map = new HashMap<Integer, List<RiskCountAndRatioDto>>();
		Set<String> scenarios = new HashSet<>();
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getCustomerRisk(fromDate, toDate, null, filterDto);
		List<RiskCountAndRatioDto> listScenarios = alertDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getCorruptionRiskType());
		for (RiskCountAndRatioDto scenarioDto : listScenarios) {
			scenarios.add(scenarioDto.getType());
		}
		list.add(riskCountAndRatioDto);
		map.put(new Integer(1), list);
		map.put(new Integer(2), listScenarios);

		return map;
	}

	@Override
	@Transactional("transactionManager")
	public Map<Integer, List<RiskCountAndRatioDto>> getPoliticalRsikByType(String fromDate, String toDate, String type,
			FilterDto filterDto) throws ParseException {
		Map<Integer, List<RiskCountAndRatioDto>> map = new HashMap<Integer, List<RiskCountAndRatioDto>>();
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getCustomerRisk(fromDate, toDate, null, filterDto);
		List<RiskCountAndRatioDto> listScenarios = alertDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getPoliticalRiskType());
		for (RiskCountAndRatioDto scenariosDto : listScenarios) {
			scenarios.add(scenariosDto.getType());
		}
		riskCountAndRatioDto.setScenario(scenarios);
		list.add(riskCountAndRatioDto);
		map.put(new Integer(1), list);
		map.put(new Integer(2), listScenarios);

		return map;
	}

	@Override
	@Transactional("transactionManager")
	public Map<Integer, List<RiskCountAndRatioDto>> getGeoGraphicRiskByType(String fromDate, String toDate, String type,
			FilterDto filterDto) throws ParseException {
		Map<Integer, List<RiskCountAndRatioDto>> map = new HashMap<Integer, List<RiskCountAndRatioDto>>();
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getCustomerRisk(fromDate, toDate, null, filterDto);
		List<RiskCountAndRatioDto> listScenarios = alertDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getGeoRiskType());
		for (RiskCountAndRatioDto scenariosDto : listScenarios) {
			scenarios.add(scenariosDto.getType());
		}
		riskCountAndRatioDto.setScenario(scenarios);
		list.add(riskCountAndRatioDto);
		map.put(new Integer(1), list);
		map.put(new Integer(2), listScenarios);

		return map;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, AlertComparisonReturnObject> alertComparisonTransactions(String fromDate, String toDate,
			FilterDto filterDto) throws ParseException {
		Map<String, AlertComparisonReturnObject> comparisonResponse = new HashMap<String, AlertComparisonReturnObject>();
		List<AlertComparisonDto> presentAlerts = new ArrayList<>();
		List<AlertComparisonDto> pastAlerts = new ArrayList<>();
		String originlalToDate = toDate;
		Long differenceBetweenDates = daysCalulationUtility.getDifferenceBetweenDates(fromDate, toDate);
		Date previousAlertDate = daysCalulationUtility.getPreviousDate(fromDate, differenceBetweenDates);
		presentAlerts = txDao.alertComparisonTransactions(fromDate, toDate, null, filterDto, originlalToDate);
		pastAlerts = txDao.alertComparisonTransactions(fromDate, null, previousAlertDate, filterDto, originlalToDate);
		// getting all outputTypes into List<String>
		Set<String> alloutputTypes = new HashSet<String>();
		for (AlertComparisonDto presentAlertProductType : presentAlerts) {
			alloutputTypes.add(presentAlertProductType.getProductType());
		}
		for (AlertComparisonDto presentAlertProductType : pastAlerts) {
			alloutputTypes.add(presentAlertProductType.getProductType());
		}

		for (String stringProduct : alloutputTypes) {
			AlertComparisonReturnObject alertComparisonReturnObject = new AlertComparisonReturnObject();
			comparisonResponse.put(stringProduct, null);
			for (AlertComparisonDto comparisonResponsePresent : presentAlerts) {

				if (stringProduct.equals(comparisonResponsePresent.getProductType()))
					alertComparisonReturnObject.setPresent(comparisonResponsePresent);
			}
			for (AlertComparisonDto comparisonResponsePast : pastAlerts) {
				if (stringProduct.equals(comparisonResponsePast.getProductType()))
					alertComparisonReturnObject.setPast(comparisonResponsePast);
			}
			if (comparisonResponse.containsKey(stringProduct))
				comparisonResponse.put(stringProduct, alertComparisonReturnObject);
		}
		return comparisonResponse;
	}

	@Override
	@Transactional("transactionManager")
	public RiskCountAndRatioDto getProductRisk(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		filterDto.setProductRisk(true);
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getCustomerRisk(fromDate, toDate, null, filterDto);
		riskCountAndRatioDto.setType("ProductRisk");
		return riskCountAndRatioDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getProductRiskAggregates(String fromDate, String toDate, FilterDto filterDto) {
		return alertDao.getProductRiskAggregates(fromDate, toDate, null, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getRiskRatioChart(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		RiskCountAndRatioDto customerRisk = new RiskCountAndRatioDto();
		RiskCountAndRatioDto productRisk = new RiskCountAndRatioDto();
		RiskCountAndRatioDto geoGraphicRisk = new RiskCountAndRatioDto();
		if (filterDto.isCustomerRiskType()) {
			customerRisk = getCustomerRisk(fromDate, toDate, filterDto);
		} else {
			customerRisk = getCustomerRisk(fromDate, toDate, filterDto);
			filterDto.setCustomerRiskType(false);
		}
		if (filterDto.isProductRisk()) {
			productRisk = getProductRisk(fromDate, toDate, filterDto);
		} else {
			productRisk = getProductRisk(fromDate, toDate, filterDto);
			filterDto.setProductRisk(false);
		}
		if (filterDto.isGeographyRiskType()) {
			geoGraphicRisk = getGeoGraphicRisk(fromDate, toDate, filterDto);
		} else {
			geoGraphicRisk = getGeoGraphicRisk(fromDate, toDate, filterDto);
			filterDto.setGeographyRiskType(false);
		}
		list.add(customerRisk);
		list.add(productRisk);
		list.add(geoGraphicRisk);
		return list;
	}

	@Override
	@Transactional("transactionManager")
	public CounterPartyGraphDto counterPartyLocationsPlot(String fromDate, String toDate, String type,
			FilterDto filterDto) throws ParseException, IllegalAccessException, InvocationTargetException {

		String queryCondition = null;
		if ("CorruptionRisk".equalsIgnoreCase(type))
			queryCondition = "(t.transparencyRisk>0 or t.worldBankRisk>0 or t.wefRisk>0)";
		if ("PoliticalRisk".equalsIgnoreCase(type))
			queryCondition = "(t.freedomHouserisk>0 or t.worldJusticeRisk>0 or t.wefRisk>0)";

		CounterPartyGraphDto counterPartyGraphDto = new CounterPartyGraphDto();
		// counterparties input
		List<CounterPartyNotiDto> counterPartiesListInput = txDao.counterPartyLocationsPlot(fromDate, toDate, true,
				false, queryCondition, filterDto);
		for (CounterPartyNotiDto counterPartyNotiDto : counterPartiesListInput) {
			Country country = countryDao.find(counterPartyNotiDto.getCustomerId());
			if (country != null) {
				CountryDto countryDto = new CountryDto();
				BeanUtils.copyProperties(countryDto, country);
				counterPartyNotiDto.setCountry(countryDto);
			}
		}

		for (CounterPartyNotiDto counterPartyNotiDto : counterPartiesListInput) {
			if ((counterPartyNotiDto.getCountry().getBaselAMLIndex2017() >= 0)
					&& (counterPartyNotiDto.getCountry().getBaselAMLIndex2017() <= 40)) {
				counterPartyNotiDto.setRisk("low-risk");
				counterPartyNotiDto.setColour("#9AA639");
			}
			if ((counterPartyNotiDto.getCountry().getBaselAMLIndex2017() > 40)
					&& (counterPartyNotiDto.getCountry().getBaselAMLIndex2017() <= 70)) {
				counterPartyNotiDto.setRisk("medium-risk");
				counterPartyNotiDto.setColour("#3D6193");
			}
			if ((counterPartyNotiDto.getCountry().getBaselAMLIndex2017() > 70)) {
				counterPartyNotiDto.setRisk("high-risk");
				counterPartyNotiDto.setColour("#C4263D");
			}
		}

		List<CounterPartyNotiDto> counterPartiesListOutput = txDao.counterPartyLocationsPlot(fromDate, toDate, false,
				true, queryCondition, filterDto);
		for (CounterPartyNotiDto counterPartyNotiDto : counterPartiesListOutput) {
			Country country = countryDao.find(counterPartyNotiDto.getCustomerId());
			if (country != null) {
				CountryDto countryDto = new CountryDto();
				BeanUtils.copyProperties(countryDto, country);
				counterPartyNotiDto.setCountry(countryDto);
			}
		}
		for (CounterPartyNotiDto counterPartyNotiDto : counterPartiesListOutput) {
			if ((counterPartyNotiDto.getCountry().getBaselAMLIndex2017() >= 0)
					&& (counterPartyNotiDto.getCountry().getBaselAMLIndex2017() <= 40)) {
				counterPartyNotiDto.setRisk("low-risk");
				counterPartyNotiDto.setColour("#9AA639");
			}
			if ((counterPartyNotiDto.getCountry().getBaselAMLIndex2017() > 40)
					&& (counterPartyNotiDto.getCountry().getBaselAMLIndex2017() <= 70)) {
				counterPartyNotiDto.setRisk("medium-risk");
				counterPartyNotiDto.setColour("#3D6193");
			}
			if ((counterPartyNotiDto.getCountry().getBaselAMLIndex2017() > 70)) {
				counterPartyNotiDto.setRisk("high-risk");
				counterPartyNotiDto.setColour("#C4263D");
			}
		}
		counterPartyGraphDto.setInputCounterpartyList(counterPartiesListInput);
		counterPartyGraphDto.setOutputCounterpartyList(counterPartiesListOutput);
		return counterPartyGraphDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<TransactionAmountAndAlertCountDto> amlAlertAggregation(String fromDate, String toDate,
			String productType, Integer recordsPerPage, Integer pageNumber, Boolean input, Boolean output,
			FilterDto filterDto) throws ParseException {
		List<TransactionAmountAndAlertCountDto> aggregation = txDao.getTotalAmountAndAlertCountByProductType(null, null,
				false, recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, false, false, null, filterDto);
		return aggregation;
	}

	@Override
	@Transactional("transactionManager")
	public Map<Integer, List<RiskCountAndRatioDto>> getProductRiskByType(String fromDate, String toDate, String type,
			FilterDto filterDto)
			throws ParseException {/*
									 * 
									 * Map<Integer, List<RiskCountAndRatioDto>> map = new HashMap<Integer,
									 * List<RiskCountAndRatioDto>>(); RiskCountAndRatioDto riskCountAndRatioDto =
									 * new RiskCountAndRatioDto(); List<RiskCountAndRatioDto> list = new
									 * ArrayList<>(); List<String> scenarios = new ArrayList<String>();
									 * 
									 * List<RiskCountAndRatioDto> resultList =
									 * alertDao.getProductRiskAggregates(fromDate, toDate, null,null); for
									 * (RiskCountAndRatioDto riskCountAndRatio : resultList) { if
									 * (riskCountAndRatio.getType().equals(type)) riskCountAndRatioDto =
									 * riskCountAndRatio; } List<RiskCountAndRatioDto> groupByScenario =
									 * alertDao.getProductRiskAggregates(fromDate, toDate, type,null); for
									 * (RiskCountAndRatioDto groupByScenarioDto : groupByScenario) {
									 * scenarios.add(groupByScenarioDto.getType()); }
									 * riskCountAndRatioDto.setType(type);
									 * //riskCountAndRatioDto.setScenario(scenarios);
									 * list.add(riskCountAndRatioDto); map.put(new Integer(1), list); map.put(new
									 * Integer(2), alertDao.getProductRiskAggregates(fromDate, toDate, type,null));
									 * return map;
									 */

		Map<Integer, List<RiskCountAndRatioDto>> map = new HashMap<Integer, List<RiskCountAndRatioDto>>();
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		RiskCountAndRatioDto riskCountAndRatioDto = txDao.getCustomerRisk(fromDate, toDate, null, filterDto);
		List<RiskCountAndRatioDto> listScenarios = alertDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getProductRiskType());
		for (RiskCountAndRatioDto dto : listScenarios) {
			scenarios.add(dto.getType());
		}
		riskCountAndRatioDto.setScenario(scenarios);
		list.add(riskCountAndRatioDto);
		map.put(new Integer(1), list);
		map.put(new Integer(2), listScenarios);
		return map;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getRiskCount(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		RiskCountAndRatioDto customerRisk = getCustomerRisk(fromDate, toDate, filterDto);
		RiskCountAndRatioDto productRisk = getProductRisk(fromDate, toDate, filterDto);
		RiskCountAndRatioDto geoGraphicRisk = getGeoGraphicRisk(fromDate, toDate, filterDto);
		list.add(customerRisk);
		list.add(productRisk);
		list.add(geoGraphicRisk);
		return list;
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getRiskAlertsByScenarios(String fromDate, String toDate, FilterDto filterDto) {

		return alertDao.getRiskAlertsByScenarios(fromDate, toDate, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getProductByScenarios(String fromDate, String toDate, FilterDto filterDto) {
		return alertDao.getRiskAlertsByScenarios(fromDate, toDate, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getGeoGraphicByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type) {
		List<AlertDashBoardRespDto> list = new ArrayList<>();
		String value = null;

		if (type.equals("FATF High Risk Countries")) {
			value = "c.fatfRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		} else if (type.equals("Tax Justice Network High Risk Countries (Finacial Secracy Index)")) {
			value = "c.taskJusticeNetworkHighRisk_FinancialSecracyIndex";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		} else if (type.equals("International Narcotics Control High Risk Countries")) {
			value = "c.internationalNarcoticsControlRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		}

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getPoliticalByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type) {
		List<AlertDashBoardRespDto> list = new ArrayList<>();
		String value = null;

		if (type.equals("Freedom House - Freedom in the World High Risk Countries")) {
			value = "c.freedomHouseRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		} else if (type.equals("World Justice Project")) {
			value = "c.worldJusticeRiskWorld_CriminalJusticeRiskRating";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		} else if (type.equals("WEF Global Competitiveness High Risk Countries")) {
			value = "c.wefRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		}

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getCorruptionByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type) {
		List<AlertDashBoardRespDto> list = new ArrayList<>();
		String value = null;

		if (type.equals("Transparancy International High Risk Countries")) {
			value = "c.transparencyInternationalRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		} else if (type.equals("World Bank High Risk Countries")) {
			value = "c.worldBankRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		} else if (type.equals("WEF Global Competitiveness High Risk Countries")) {
			value = "c.wefRisk";
			list = alertDao.getRiskByScenarios(fromDate, toDate, scenarios, value);

		}

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> viewAll(String fromDate, String toDate, Integer pageNumber,
			Integer recordsPerPage, String type, FilterDto filterDto) {

		List<RiskCountAndRatioDto> list = new ArrayList<>();
		if ("CustomerRisk".equalsIgnoreCase(type) || "ProductRisk".equalsIgnoreCase(type)
				|| "ActivityType".equalsIgnoreCase(type)) {
			if ("CustomerRisk".equalsIgnoreCase(type))
				filterDto.setCustomerRiskType(true);
			if ("ProductRisk".equalsIgnoreCase(type))
				filterDto.setPoliticalRisk(true);
			if ("ActivityType".equalsIgnoreCase(type))
				filterDto.setActivityType(true);
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto, null);
		} else if ("GeoGraphicRisk".equalsIgnoreCase(type)) {
			filterDto.setGeographyRiskType(true);

			String queryforCondition = " and (t.fatfRisk>0 or t.taskJusticeRisk>0 or t.internationalNarcoticsRisk>0)";
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto,
					queryforCondition);
		} else if ("PoliticalRisk".equalsIgnoreCase(type)) {

			String queryforCondition = " and (t.freedomHouserisk>0 or t.worldJusticeRisk>0 or t.wefRisk>0)";
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto,
					queryforCondition);
		} else if ("CorruptionRisk".equalsIgnoreCase(type)) {

			String queryforCondition = " and (t.transparencyRisk>0 or t.worldBankRisk>0 or t.wefRisk>0)";
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto,
					queryforCondition);
		}

		if ("Banks".equalsIgnoreCase(type)) {
			filterDto.setBank(true);
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto, null);
		}

		if ("GeoGraphic".equalsIgnoreCase(type) || "BankLocations".equalsIgnoreCase(type)) {
			if ("BankLocations".equalsIgnoreCase(type))
				filterDto.setBank(true);
			filterDto.setCounterPartyCountry(true);
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto, null);
		}

		if ("CorporateStructure".equalsIgnoreCase(type) || "Shareholders".equalsIgnoreCase(type)
				|| "Geography".equalsIgnoreCase(type)) {
			if ("CorporateStructure".equalsIgnoreCase(type))
				filterDto.setCorporateStructure(true);
			if ("Shareholders".equalsIgnoreCase(type))
				filterDto.setShareholder(true);
			if ("Geography".equalsIgnoreCase(type))
				filterDto.setCorporateGeography(true);
			list = txDao.viewAll(fromDate, toDate, true,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto, null);
		}

		return list;
	}

	@Override
	@Transactional("transactionManager")
	public long viewAllCount(String fromDate, String toDate, String type, FilterDto filterDto) {
		long count = 0;

		if ("CustomerRisk".equalsIgnoreCase(type) || "ProductRisk".equalsIgnoreCase(type)
				|| "ActivityType".equalsIgnoreCase(type)) {
			if ("CustomerRisk".equalsIgnoreCase(type))
				filterDto.setCustomerRiskType(true);
			if ("ProductRisk".equalsIgnoreCase(type))
				filterDto.setPoliticalRisk(true);
			if ("ActivityType".equalsIgnoreCase(type))
				filterDto.setActivityType(true);
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, null);
		} else if ("GeoGraphicRisk".equalsIgnoreCase(type)) {
			String queryforCondition = " and (t.fatfRisk>0 or t.taskJusticeRisk>0 or t.internationalNarcoticsRisk>0)";
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, queryforCondition);
		} else if ("PoliticalRisk".equalsIgnoreCase(type)) {
			String queryforCondition = "and (t.freedomHouserisk>0 or t.worldJusticeRisk>0 or t.wefRisk>0)";
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, queryforCondition);
		} else if ("CorruptionRisk".equalsIgnoreCase(type)) {
			String queryforCondition = "and (t.transparencyRisk>0 or t.worldBankRisk>0 or t.wefRisk>0)";
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, queryforCondition);
		} else if ("Banks".equalsIgnoreCase(type)) {
			filterDto.setBank(true);
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, null);
		} else if ("GeoGraphic".equalsIgnoreCase(type) || "BankLocations".equalsIgnoreCase(type)) {
			if ("BankLocations".equalsIgnoreCase(type))
				filterDto.setBank(true);
			filterDto.setCounterPartyCountry(true);
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, null);
		} else if ("CorporateStructure".equalsIgnoreCase(type) || "Shareholders".equalsIgnoreCase(type)
				|| "Geography".equalsIgnoreCase(type)) {
			if ("CorporateStructure".equalsIgnoreCase(type))
				filterDto.setCorporateStructure(true);
			if ("Shareholders".equalsIgnoreCase(type))
				filterDto.setShareholder(true);
			if ("Geography".equalsIgnoreCase(type))
				filterDto.setCorporateGeography(true);
			count = txDao.viewAllCount(fromDate, toDate, true, filterDto, null);
		}
		return count;
	}

	@Override
	@Transactional("transactionManager")
	public List<RiskCountAndRatioDto> getScenarioAggregates(String fromDate, String toDate, String type) {

		List<RiskCountAndRatioDto> list = new ArrayList<>();
		String value = null;

		if (type.equals("Transparancy International High Risk Countries")) {
			value = "c.transparencyInternationalRisk";
			list = txDao.getScenarioAggregates(fromDate, toDate, value, null, type);

		} else if (type.equals("World Bank High Risk Countries")) {
			value = "c.worldBankRisk";
			list = txDao.getScenarioAggregates(fromDate, toDate, value, null, type);

		} else if (type.equals("WEF Global Competitiveness High Risk Countries")) {
			value = "c.wefRisk";
			list = txDao.getScenarioAggregates(fromDate, toDate, value, null, type);

		} else if (type.equals("Freedom House - Freedom in the World High Risk Countries")) {
			value = "c.freedomHouseRisk";
			list = txDao.getScenarioAggregates(fromDate, toDate, value, null, type);

		} else if (type.equals("World Justice Project")) {
			value = "c.worldJusticeRiskWorld_CriminalJusticeRiskRating";
			list = txDao.getScenarioAggregates(fromDate, toDate, value, null, type);

		}

		return list;
	}

	/*
	 * @Override
	 * 
	 * @Transactional("transactionManager") public CustomerCountryPartyDto
	 * getCounterParties(String fromDate, String toDate, Long customerId) throws
	 * ParseException {
	 * 
	 * CustomerCountryPartyDto response=new CustomerCountryPartyDto();
	 * InputStatsResponseDto responseDto=new InputStatsResponseDto();
	 * 
	 * List<String> inputList=new ArrayList<>();
	 * inputList.add(customerDetailsDao.find(customerId).getCustomerNumber());
	 * List<AlertedCounterPartyDto>
	 * list=txDao.getAlertedTransaction(fromDate,toDate,inputList);
	 * 
	 * List<CounterPartyDto> counterDtos =
	 * txDao.getCounterPartiesAggregate(inputList, null, null, null, null, null,
	 * null); for (CounterPartyDto counterPartyDto : counterDtos) {
	 * counterPartyDto.setPoint(customerDetailsDao.fetchCustomerDetails(
	 * counterPartyDto.getCustomerNumer())); }
	 * 
	 * List<CounterPartyDto> counterDtosOutPut =
	 * txDao.getCounterPartiesAggregateForOutputStats(inputList, null, null, null,
	 * null, null, null); for (CounterPartyDto counterPartyDto : counterDtosOutPut)
	 * { counterPartyDto.setPoint(customerDetailsDao.fetchCustomerDetails(
	 * counterPartyDto.getCustomerNumer())); }
	 * 
	 * for (CounterPartyDto counterPartyDto : counterDtosOutPut) {
	 * 
	 * for (CounterPartyDto counterDto : counterDtos) { if
	 * (counterDto.getCustomerNumer().equals(counterPartyDto.getCustomerNumer()) )
	 * counterDto.setSum(counterDto.getSum() + counterPartyDto.getSum()); } }
	 * 
	 * List<CountryAggDto> countryaggregate = txDao.getCountryAggregate(inputList,
	 * null, null, null, null); for (CountryAggDto testDtoCountry :
	 * countryaggregate) {
	 * testDtoCountry.setCountry(countryDao.findByName(testDtoCountry.
	 * getCountryCode ())); }
	 * 
	 * List<CountryAggDto> countryaggregateOutPut =
	 * txDao.getCountryAggregateForOutputStats(inputList, null, null, null, null);
	 * for (CountryAggDto testDtoCountry : countryaggregateOutPut) {
	 * testDtoCountry.setCountry(countryDao.findByName(testDtoCountry.
	 * getCountryCode ())); }
	 * 
	 * for (CountryAggDto countryinput : countryaggregate) {
	 * 
	 * for (CountryAggDto countryoutput : countryaggregateOutPut) { if
	 * (countryinput.getCountryCode().equals(countryoutput.getCountryCode()))
	 * countryinput.setSum(countryinput.getSum() + countryoutput.getSum()); } }
	 * responseDto.setCounterPartyDto(counterDtos);
	 * responseDto.setCountry(countryaggregate);
	 * 
	 * response.setAlertedCounterPartyDto(list);
	 * response.setCustomerDetails(customerDetailsDao.find(customerId));
	 * response.setInputStatsResponseDto(responseDto);
	 * 
	 * return response;
	 * 
	 * }
	 */

	@Override
	@Transactional("transactionManager")
	public List<TopAmlCustomersDto> amlTopCustomers(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		List<TopAmlCustomersDto> amlCustomersList = txDao.amlTopCustomers(fromDate, toDate, filterDto);
		for (TopAmlCustomersDto topAmlCustomersDto : amlCustomersList) {
			CustomerDetails customerDetails = customerDetailsDao
					.fetchCustomerDetails(topAmlCustomersDto.getCustomerName());
			if (customerDetails != null)
				topAmlCustomersDto.setCustomerName(customerDetails.getDisplayName());
		}
		// sorting based on count
		amlCustomersList.sort(Comparator.comparingLong(TopAmlCustomersDto::getCount).reversed());

		return amlCustomersList;
	}

	@Override
	@Transactional("transactionManager")
	public List<TransactionAmountAndAlertCountDto> amlTransferByMonth(FilterDto filterDto) throws ParseException {
		return alertDao.getAlertAggregates(null, null, "monthly", false, null, false, false, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public List<TransactionAmountAndAlertCountDto> getInputAndOutputCount(String fromDate, String toDate)
			throws ParseException {
		List<TransactionAmountAndAlertCountDto> inputCount = txDao.getInputAndOutput(fromDate, toDate, true,
				"c.id=t.beneficiaryCutomerId");
		List<TransactionAmountAndAlertCountDto> outputCount = txDao.getInputAndOutput(fromDate, toDate, true,
				"c.id=t.originatorCutomerId");
		for (TransactionAmountAndAlertCountDto outputDto : outputCount) {
			for (TransactionAmountAndAlertCountDto inputDto : inputCount) {
				if (inputDto.getTransactionProductType().equals(outputDto.getTransactionProductType()))
					inputDto.setOutputCount(outputDto.getInputCount());

			}
		}
		return inputCount;
	}

	@Override
	@Transactional("transactionManager")
	public CounterPartyInfoDto counterPartyInfo(String fromDate, String toDate, Long customerId, FilterDto filterDto) {
		Map<Long, CounterPartyInfo> map = new HashMap<>();
		// Map<String, List<CounterPartyInfo>> response = new HashMap<>();

		CounterPartyInfoDto response = new CounterPartyInfoDto();
		List<AlertedCounterPartyDto> customerAggregates = txDao.getCustomerTransactions(fromDate, toDate, customerId,
				true, false, true, filterDto);
		List<AlertedCounterPartyDto> customerAggregatesOut = txDao.getCustomerTransactions(fromDate, toDate, customerId,
				true, true, false, filterDto);
		customerAggregates.addAll(customerAggregatesOut);

		for (AlertedCounterPartyDto customerAggregate : customerAggregates) {
			if (customerAggregate.getCounterPartyId() != null) {
				if (map.containsKey(customerAggregate.getCounterPartyId())) {
					CounterPartyInfo dto = map.get(customerAggregate.getCounterPartyId());
					dto.setAmount(dto.getAmount() + customerAggregate.getAmount());
				} else {
					CounterPartyInfo counterPartyInfo = new CounterPartyInfo(customerAggregate.getCounterPartyId(),
							customerAggregate.getCounterParty(), customerAggregate.getAmount());
					map.put(customerAggregate.getCounterPartyId(), counterPartyInfo);
				}
			}
		}
		response.setCustomer(new ArrayList<CounterPartyInfo>(map.values()));
		map.clear();

		for (AlertedCounterPartyDto customerAggregate : customerAggregates) {
			if (map.containsKey(customerAggregate.getCounterPartyCountryId())) {
				CounterPartyInfo dto = map.get(customerAggregate.getCounterPartyCountryId());
				dto.setAmount(dto.getAmount() + customerAggregate.getAmount());
			} else {
				CounterPartyInfo counterPartyInfo = new CounterPartyInfo(customerAggregate.getCountry(),
						customerAggregate.getCounterPartyCountryId(), customerAggregate.getLatitude(),
						customerAggregate.getLongitude(), customerAggregate.getAmount());
				map.put(customerAggregate.getCounterPartyCountryId(), counterPartyInfo);
			}
		}
		response.setCountry(new ArrayList<CounterPartyInfo>(map.values()));

		/*
		 * List<CounterPartyInfo> counterAggregates =
		 * txDao.counterPartyCountryAggregate(fromDate, toDate, customerId,
		 * true,filterDto); List<CounterPartyInfo> customerAggregatesOut =
		 * txDao.counterPartyCountryAggregateOut(fromDate, toDate, customerId, false);
		 * List<CounterPartyInfo> counterAggregatesOut =
		 * txDao.counterPartyCountryAggregateOut(fromDate, toDate, customerId, true);
		 */

		/*
		 * map.put("Customer", (customerAggregates.isEmpty() || customerAggregates ==
		 * null) ? customerAggregatesOut : customerAggregates); map.put("country",
		 * (counterAggregates.isEmpty() || counterAggregates == null) ?
		 * counterAggregatesOut : counterAggregates);
		 */

		return response;
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertedCounterPartyDto> getCustomerTransactions(String fromDate, String toDate, Long customerId,
			Integer pageNumber, Integer recordsPerPage, boolean isDetected, FilterDto filterDto) {
		List<AlertedCounterPartyDto> response = new ArrayList<>();
		List<AlertedCounterPartyDto> outTransactions = txDao.getCustomerTransactions(fromDate, toDate, customerId,
				isDetected, true, false, filterDto);
		List<AlertedCounterPartyDto> inTransactions = txDao.getCustomerTransactions(fromDate, toDate, customerId,
				isDetected, false, true, filterDto);
		outTransactions.addAll(inTransactions);
		pageNumber = pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber;
		recordsPerPage = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int fromIndex = ((pageNumber - 1) * (recordsPerPage));
		int toIndex = fromIndex + recordsPerPage;

		if (fromIndex >= 0 && toIndex < outTransactions.size())
			response = outTransactions.subList(fromIndex, toIndex);
		if (fromIndex >= 0 && toIndex > outTransactions.size())
			response = outTransactions.subList(fromIndex, outTransactions.size());

		return response;
	}

	@Override
	@Transactional("transactionManager")
	public Long getCustomerTransactionsCount(String fromDate, String toDate, Long customerId, boolean isDetected,
			FilterDto filterDto) {
		// Long outCount = txDao.getCustomerTransactionsCount(fromDate, toDate,
		// customerId, isDetected,true,false,filterDto);
		// Long inCount = txDao.getCustomerTransactionsCount(fromDate, toDate,
		// customerId, isDetected,false,true,filterDto);
		List<AlertedCounterPartyDto> outTransactions = txDao.getCustomerTransactions(fromDate, toDate, customerId,
				isDetected, true, false, filterDto);
		List<AlertedCounterPartyDto> inTransactions = txDao.getCustomerTransactions(fromDate, toDate, customerId,
				isDetected, false, true, filterDto);
		outTransactions.addAll(inTransactions);
		return (long) outTransactions.size();
	}

	@Override
	@Transactional("transactionManager")
	@Async
	public Boolean updateTransactions() {
		@SuppressWarnings("unused")
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		long beginTime = Timestamp.valueOf("2017-01-01 00:00:00").getTime();
		long endTime = Timestamp.valueOf("2018-02-28 00:58:00").getTime();

		List<TransactionsData> transactionsData = txDao.findAll();
		for (TransactionsData transactionToUpdate : transactionsData) {
			Date randomDate = new Date(getRandomTimeBetweenTwoDates(beginTime, endTime));
			transactionToUpdate.setBusinessDate(randomDate);
			txDao.saveOrUpdate(transactionToUpdate);
			List<Alert> alertList = alertDao.fetchAlertSummaryList(transactionToUpdate.getId());
			for (Alert alert : alertList) {
				Alert alertUpdate = alertDao.find(alert.getId());
				alertUpdate.setAlertBusinessDate(randomDate);
				alertDao.saveOrUpdate(alertUpdate);
			}
		}

		return true;
	}

	private long getRandomTimeBetweenTwoDates(long beginTime, long endTime) {
		long diff = endTime - beginTime + 1;
		return beginTime + (long) (Math.random() * diff);
	}

	@Override
	@Transactional("transactionManager")
	@Async
	public Boolean processAllFiles(List<MultipartFile> multipartFilesList, Long userId)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException, Exception {
		List<MultipartFile> files = multipartFilesList;
		try {
			for (MultipartFile multipartFile : files) {
				if (!multipartFile.isEmpty()) {
					if (multipartFile.getName().equals("multipartFileCustomer"))
						customerDetailsService.uploadCustomerDetailsFile(multipartFile);
					if (multipartFile.getName().equals("multipartFileCustomerRelationShipDetailsFile"))
						customerRelationService.fetchCustomerRelationDetails(multipartFile);
					if (multipartFile.getName().equals("multipartFileCustomerAddress"))
						customerDetailsService.uploadCustomerAddressFile(multipartFile);
					if (multipartFile.getName().equals("multipartFileShareholders"))
						shareholderService.uoploadShareholders(multipartFile);
					if (multipartFile.getName().equals("multipartFileAccount"))
						accountService.fetchAccountDetails(multipartFile);
					if (multipartFile.getName().equals("multipartFileTransaction"))
						saveTxData(multipartFile, userId);
					if (multipartFile.getName().equals("multipartFileAlert"))
						alertService.fetchAlerts(multipartFile, userId);
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public Boolean alterDatabse() {
		List<String> outputTypes = new ArrayList<String>();
		outputTypes.add("Cash in");
		outputTypes.add("Cash out");
		outputTypes.add("Ach in");
		outputTypes.add("Ach out");
		outputTypes.add("Wire in");
		outputTypes.add("Wire out");
		outputTypes.add("Quick Transfer in");
		outputTypes.add("Quick Transfer out");
		outputTypes.add("Cheque In");
		outputTypes.add("Cheque Out");
		outputTypes.add("Currency Exchange");
		outputTypes.add("Involving Banking Depoisit");
		outputTypes.add("Withdrawing Banking Deposit ");
		outputTypes.add("Paying Banking Deposit Rates");
		outputTypes.add("Providing Bank Loan");
		outputTypes.add("Paying Bank Loan Interrest Rates");
		outputTypes.add("Memorial order");
		outputTypes.add("Grouped cash in");
		outputTypes.add("Grouped cash out");
		outputTypes.add("Grouped Memorial Order");
		outputTypes.add("Grouped Debit Order");
		outputTypes.add("Grouped Credit Order");

		List<TransactionsData> transactionsDatas = txDao.findAll();
		for (TransactionsData transactionsData : transactionsDatas) {
			transactionsData.setTransProductType(getRandomTypes(outputTypes));
			txDao.saveOrUpdate(transactionsData);
		}

		return true;
	}

	private String getRandomTypes(List<String> types) {
		String type = null;
		int index = random.nextInt(types.size());
		type = types.get(index);
		return type;
	}

	@Override
	@Async
	@Transactional("transactionManager")
	public Boolean auditColumns() {
		List<String> inputTypes = new ArrayList<String>();
		inputTypes.add("Cash in");
		inputTypes.add("Ach in");
		inputTypes.add("Wire in");
		inputTypes.add("Quick Transfer in");
		inputTypes.add("Cheque In");
		inputTypes.add("Involving Banking Depoisit");
		inputTypes.add("Grouped cash in");
		inputTypes.add("Grouped Credit Order");
		List<String> outputTypes = new ArrayList<String>();
		outputTypes.add("Cash out");
		outputTypes.add("Ach out");
		outputTypes.add("Wire out");
		outputTypes.add("Quick Transfer out");
		outputTypes.add("Withdrawing Banking Deposit ");
		outputTypes.add("Paying Banking Deposit Rates");
		outputTypes.add("Providing Bank Loan");
		outputTypes.add("Paying Bank Loan Interrest Rates");
		outputTypes.add("Memorial order");
		outputTypes.add("Grouped cash out");
		outputTypes.add("Grouped Memorial Order");
		outputTypes.add("Grouped Debit Order");
		outputTypes.add("Cheque Out");
		List<String> currencyType = new ArrayList<String>();
		currencyType.add("Currency Exchange");

		List<TransactionsData> list = txDao.findAll();
		for (TransactionsData transactionsData : list) {
			// for input productType
			for (String string : inputTypes) {
				if (transactionsData.getTransProductType().equals(string)) {
					/*
					 * transactionsData.setAccountNumberOfCustomer(transactionsData.
					 * getBeneficiaryAccountId()); CustomerDetails customerDetails =
					 * customerDetailsDao.find(transactionsData.getBeneficiaryCutomerId());
					 * if(customerDetails!=null)
					 * transactionsData.setNameOfCustomer(customerDetails.getDisplayName());
					 * CustomerDetails customerDetailsCounter =
					 * customerDetailsDao.find(transactionsData.getOriginatorCutomerId());
					 * transactionsData.setNameOfCounterParty(customerDetailsCounter.getDisplayName(
					 * )); transactionsData.setCountryOfCounterParty(customerDetailsCounter.
					 * getResidentCountry()); Account account =
					 * accountDao.fetchAccount(transactionsData.getOriginatorAccountId());
					 * transactionsData.setAccoutOfCounterParty(transactionsData.
					 * getOriginatorAccountId());
					 * transactionsData.setNameOfCounterPartyBank(account.getBankName());
					 * transactionsData.setCountryOfCounterPartyBank(customerDetailsCounter.
					 * getResidentCountry());
					 */
					txDao.saveOrUpdate(transactionsData);
				} // outer if for checking input close
			} // input for close

			// for output productType
			for (String string : outputTypes) {
				if (transactionsData.getTransProductType().equals(string)) {
					/*
					 * transactionsData.setAccountNumberOfCustomer(transactionsData.
					 * getOriginatorAccountId()); CustomerDetails customerDetails =
					 * customerDetailsDao.find(transactionsData.getOriginatorCutomerId());
					 * if(customerDetails!=null)
					 * transactionsData.setNameOfCustomer(customerDetails.getDisplayName());
					 * CustomerDetails customerDetailsCounter =
					 * customerDetailsDao.find(transactionsData.getBeneficiaryCutomerId());
					 * transactionsData.setNameOfCounterParty(customerDetailsCounter.getDisplayName(
					 * )); transactionsData.setCountryOfCounterParty(customerDetailsCounter.
					 * getResidentCountry()); Account account =
					 * accountDao.fetchAccount(transactionsData.getBeneficiaryAccountId());
					 * transactionsData.setAccoutOfCounterParty(transactionsData.
					 * getBeneficiaryAccountId());
					 * transactionsData.setNameOfCounterPartyBank(account.getBankName());
					 * transactionsData.setCountryOfCounterPartyBank(customerDetailsCounter.
					 * getResidentCountry());
					 */
					txDao.saveOrUpdate(transactionsData);
				} // outer if for checking input close
			} // output for closes

			for (String string : currencyType) {
				if (transactionsData.getTransProductType().equals(string)) {
					/*
					 * transactionsData.setAccountNumberOfCustomer(transactionsData.
					 * getOriginatorAccountId()); CustomerDetails customerDetails =
					 * customerDetailsDao.find(transactionsData.getOriginatorCutomerId());
					 * if(customerDetails!=null)
					 * transactionsData.setNameOfCustomer(customerDetails.getDisplayName());
					 */
					txDao.saveOrUpdate(transactionsData);
				} // outer if for checking input close
			} // output for closes

		} // main for

		return true;
	}

	@Override
	@Transactional("transactionManager")
	public Boolean updatePassport() {
		List<TransactionsData> list = txDao.findAll();
		for (TransactionsData transactionsData : list) {
			// transactionsData.setPassportNumber(passportNumber());
			txDao.saveOrUpdate(transactionsData);
		}
		return true;
	}

	String passportNumber() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (this.random.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	@Override
	@Transactional("transactionManager")
	public List<TransactionMasterView> getMasterDataFromView() {
		return txDao.getMasterDataFromView();
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> fetchAlertsByNumber(String customerNumber) {
		return txDao.fetchAlertsByNumber(customerNumber);
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> fetchAlerts() {
		return txDao.fetchAlerts();
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertDashBoardRespDto> getEntityBasedAlerts(String fromDate, String toDate, Integer pageNumber,
			Integer recordsPerPage, String orderIn, String entityType) {
		List<AlertDashBoardRespDto> alertDashBoardRespDtos = txDao.getEntityBasedAlerts(fromDate, toDate,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderIn, entityType);
		return alertDashBoardRespDtos;

	}

	@Override
	@Transactional("transactionManager")
	public long getEntityBasedAlertsCount(String fromDate, String toDate, String entityType) {
		return txDao.getEntityBasedAlertsCount(fromDate, toDate, entityType);
	}
/*
	@Override
	@Transactional("transactionManager")
	public void updateTransactions(UpdateTransactionsDataDto transactionsDataDto) throws Exception {
		if (transactionsDataDto.getId() != 0) {
			TransactionsData transactionsData = txDao.find(transactionsDataDto.getId());
			if (null != transactionsData) {
				if (null != transactionsDataDto.getAmount()) {
					transactionsData.setAmount(transactionsDataDto.getAmount());
				}
				if (null != transactionsDataDto.getBeneficiaryAccountId()) {
					transactionsData.setBeneficiaryAccountId(transactionsDataDto.getBeneficiaryAccountId());
				}
				if (null != transactionsDataDto.getBeneficiaryCutomerId()) {
					transactionsData.setBeneficiaryCutomerId(transactionsDataDto.getBeneficiaryCutomerId());
				}
				if (null != transactionsDataDto.getBusinessDate()) {
					transactionsData.setBusinessDate(transactionsDataDto.getBusinessDate());
				}
				if (null != transactionsDataDto.getCurrency()) {
					transactionsData.setCurrency(transactionsDataDto.getCurrency());
				}
				if (null != transactionsDataDto.getTransProductType()) {
					transactionsData.setTransProductType(transactionsDataDto.getTransProductType());
				}
				if (null != transactionsDataDto.getTransactionChannel()) {
					transactionsData.setTransactionChannel(transactionsDataDto.getTransactionChannel());
				}
				if (null != transactionsDataDto.getCustomText()) {
					transactionsData.setCustomText(transactionsDataDto.getCustomText());
				}
				if (null != transactionsDataDto.getOriginatorAccountId()) {
					transactionsData.setOriginatorAccountId(transactionsDataDto.getOriginatorAccountId());
				}
				if (null != transactionsDataDto.getOriginatorCutomerId()) {
					transactionsData.setOriginatorCutomerId(transactionsDataDto.getOriginatorCutomerId());
				}
				if (null != transactionsDataDto.getTransactionChannel()) {
					transactionsData.setTransactionChannel(transactionsDataDto.getTransactionChannel());
				}
				if (null != transactionsDataDto.getBeneficiaryName()) {
					transactionsData.setBeneficiaryName(transactionsDataDto.getBeneficiaryName());
				}
				if (null != transactionsDataDto.getBeneficiaryBankName()) {
					transactionsData.setBeneficiaryName(transactionsDataDto.getBeneficiaryBankName());
				}
				if (null != transactionsDataDto.getCountryOfBeneficiary()) {
					transactionsData.setCountryOfBeneficiary(transactionsDataDto.getCountryOfBeneficiary());
				}
				if (null != transactionsDataDto.getCountryOfTransaction()) {
					transactionsData.setCountryOfBeneficiary(transactionsDataDto.getCountryOfTransaction());
				}
				if (null != transactionsDataDto.getAtmAddress()) {
					transactionsData.setAtmAddress(transactionsDataDto.getAtmAddress());
				}
				if (null != transactionsDataDto.getMerchant()) {
					transactionsData.setMerchant(transactionsDataDto.getMerchant());
				}
				if (null != transactionsDataDto.getMerchantWebsite()) {
					transactionsData.setMerchantWebsite(transactionsDataDto.getMerchantWebsite());
				}
				txDao.update(transactionsData);
			}
		} else {
			throw new Exception("Transactional Id cannot be empty");
		}

	}*/

	@Override
	@Transactional("transactionManager")
	public void updateTransactions(TxEntityRespDto txEntityRespDto) throws Exception {

		if (txEntityRespDto.getId() != 0) {
			TransactionsData transactionsData = txDao.find(txEntityRespDto.getId());
			if (null != transactionsData) {
				transactionsData.setAmount(txEntityRespDto.getAmount());
				if (null != txEntityRespDto.getBeneficiaryAccountId()) {
					transactionsData.setBeneficiaryAccountId(txEntityRespDto.getBeneficiaryAccountId());
				}
				if (null != txEntityRespDto.getBusinessDate()) {
					transactionsData.setBusinessDate(txEntityRespDto.getBusinessDate());
				}
				if (null != txEntityRespDto.getCurrency()) {
					transactionsData.setCurrency(txEntityRespDto.getCurrency());
				}
				if (null != txEntityRespDto.getTransProductType()) {
					transactionsData.setTransProductType(txEntityRespDto.getTransProductType());
				}
				if (null != txEntityRespDto.getTransactionChannel()) {
					transactionsData.setTransactionChannel(txEntityRespDto.getTransactionChannel());
				}
				if (null != txEntityRespDto.getCustomText()) {
					transactionsData.setCustomText(txEntityRespDto.getCustomText());
				}
				if (null != txEntityRespDto.getOriginatorAccountId()) {
					transactionsData.setOriginatorAccountId(txEntityRespDto.getOriginatorAccountId());
				}
				if (null != txEntityRespDto.getOriginatorName()) {
					transactionsData.setOriginatorName(txEntityRespDto.getOriginatorName());
				}
				if (null != txEntityRespDto.getBeneficiaryName()) {
					transactionsData.setBeneficiaryName(txEntityRespDto.getBeneficiaryName());
				}
				if (null != txEntityRespDto.getCountryBene()) {
					transactionsData.setCountryOfBeneficiary(txEntityRespDto.getCountryBene());
				}
				if (null != txEntityRespDto.getMerchant()) {
					transactionsData.setMerchant(txEntityRespDto.getMerchant());
				}
				if (null != txEntityRespDto.getMerchantWebsite()) {
					transactionsData.setMerchantWebsite(txEntityRespDto.getMerchantWebsite());
				}
				txDao.update(transactionsData);
			}
		} else {
			throw new Exception("Transactional Id cannot be empty");
		}

	}

	@Override
	@Transactional("transactionManager")
	public void deleteTransaction(Long id) throws Exception {
		TransactionsData transactionsData = txDao.find(id);
		if (null != transactionsData) {
			txDao.delete(transactionsData);
		} else {
			throw new Exception("Transaction not found!");
		}
	}

	@Override
	@Transactional("transactionManager")
	public boolean updateFetchAlerts(AlertDashBoardRespDto alertDashBoardRespDto, long userId) throws Exception {

		boolean isFetchedAlertUpdated = false;
		Long alertId = (long) 0;
		Long transactionId = (long) 0;
		TransactionsData transactionsData = null;
		Alert alert = null;

		// CustomerDetails customerDetails = null;
		TransactionMasterView transactionMasterView = txDao.updateFetchAlerts(alertDashBoardRespDto);

		if (null != transactionMasterView) {
			alertId = transactionMasterView.getAlertId();
			transactionId = transactionMasterView.getAlertTransactionId();

		} else {
			throw new RuntimeException("Transaction Master View Data doesnot exists for alertId");
		}

		if (null != alertId) {
			alert = alertService.find(alertId);
		} else {
			throw new RuntimeException("AlertId doesn't exist");
		}
		if (null != transactionId) {
			transactionsData = txDao.find(transactionId);
		} else {
			throw new RuntimeException("TransactionId doesn't exist");
		}
		if (null != alert) {
			if (null != alertDashBoardRespDto.getSearchName()) {
				alert.setSearchName(alertDashBoardRespDto.getSearchName());
			}
			if (null != alertDashBoardRespDto.getScenario()) {
				alert.setScenario(alertDashBoardRespDto.getScenario());
			}
			alertDao.saveOrUpdate(alert);
		} else {
			throw new RuntimeException("Alert is empty");
		}
		if (null != transactionsData) {
			if (null != alertDashBoardRespDto.getTotalAmount()) {
				transactionsData.setAmount(alertDashBoardRespDto.getTotalAmount());
			}
			txDao.saveOrUpdate(transactionsData);
			if(alertDashBoardRespDto.getTotalAmount()<25000)
			{
				alertDao.delete(alert);
			}
			
		} else {
			throw new RuntimeException("Transactions is empty");
		}
		isFetchedAlertUpdated = true;
		return isFetchedAlertUpdated;
	}


}
