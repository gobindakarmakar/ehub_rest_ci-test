package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
/**
 * @author rambabu
 *
 */
@ApiModel("Table rows data")
public class TableRowDataDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String question;
	private String answer;
	public TableRowDataDto(String question, String answer) {
		super();
		this.question = question;
		this.answer = answer;
	}
	public TableRowDataDto() {
		super();
		
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	

}
