package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@Entity
@Table(name = "bst_um_group_alert")
public class GroupAlert implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long groupAlertId;

	@ApiModelProperty(value = "GroupId")
	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "usergroupId", referencedColumnName = "id")
	private Groups groupId;

	// @JsonIgnore
	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "listTypeId")
	private ListItem listTypeId;

	@ApiModelProperty(value = "type")
	private String type;

	@ApiModelProperty(value = "geoLocationSetting")
	private String geoLocationSetting;

	public Long getGroupAlertId() {
		return groupAlertId;
	}

	public void setGroupAlertId(Long groupAlertId) {
		this.groupAlertId = groupAlertId;
	}

	public Groups getGroupId() {
		return groupId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}

	public ListItem getListTypeId() {
		return listTypeId;
	}

	public void setListTypeId(ListItem listTypeId) {
		this.listTypeId = listTypeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGeoLocationSetting() {
		return geoLocationSetting;
	}

	public void setGeoLocationSetting(String geoLocationSetting) {
		this.geoLocationSetting = geoLocationSetting;
	}

}
