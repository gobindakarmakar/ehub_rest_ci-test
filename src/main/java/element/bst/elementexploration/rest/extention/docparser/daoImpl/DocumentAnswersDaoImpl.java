package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentAnswersDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("documentAnswersDao")
public class DocumentAnswersDaoImpl extends GenericDaoImpl<DocumentAnswers, Long> implements DocumentAnswersDao {

	public DocumentAnswersDaoImpl() {
		super(DocumentAnswers.class);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public DocumentAnswers getDcoumentAnswerByQuestionId(Long questionId) {
		
		DocumentAnswers documentAnswers=new DocumentAnswers();
		try{
			Query<?> query=this.getCurrentSession().createQuery("from DocumentAnswers da where da.documentQuestions.id="+questionId+" order by da.updatedTime desc"); 
			List<DocumentAnswers> documentAnswersList=(List<DocumentAnswers>) query.getResultList();			
			return documentAnswersList.get(0);
		}catch (Exception e) {
			return documentAnswers;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public DocumentAnswers getDcoumentAnswerByQuestionId(Long questionId, Long docId) {
		DocumentAnswers documentAnswers=null;
		try{
			Query<?> query=this.getCurrentSession().createQuery("from DocumentAnswers da where da.documentQuestions.id="+questionId+" and da.documetnId="+docId+ "order by da.updatedTime desc"); 
			List<DocumentAnswers> documentAnswer = (List<DocumentAnswers>) query.getResultList();	
			
			return documentAnswer.get(0);
		}catch (Exception e) {
			return documentAnswers;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentAnswers> getListOfTableAnswers(Long questionId, Long docId) {
		List<DocumentAnswers> documentAnswers=new ArrayList<DocumentAnswers>();
		try{
			Query<?> query=this.getCurrentSession().createQuery("from DocumentAnswers da where da.documentQuestions.id="+questionId+" and da.documetnId="+docId); 
			documentAnswers = (List<DocumentAnswers>) query.getResultList();	
			
		return documentAnswers;
		}catch (Exception e) {
			return documentAnswers;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentAnswers> allDocumentAnswersbyLevel1Code(DocumentAnswers documentAnswer) {
		List<DocumentAnswers> documentAnswers=new ArrayList<DocumentAnswers>();
		try{
			Query<?> query=this.getCurrentSession().createQuery("select da from DocumentAnswers da where da.documentQuestions.templateId="+documentAnswer.getDocumentQuestions().getTemplateId()+" and da.documentQuestions.level1Code='"+documentAnswer.getDocumentQuestions().getLevel1Code()+"'and da.documetnId="+documentAnswer.getDocumetnId()); 
			documentAnswers = (List<DocumentAnswers>) query.getResultList();			
		return documentAnswers;
		}catch (Exception e) {
			return documentAnswers;
		}
	}


}
