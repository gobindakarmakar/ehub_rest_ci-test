package element.bst.elementexploration.rest.extention.alertmanagement.controller;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertCommentsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertListDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertUpdateDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AssociatedAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.BatchScreeningResultDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.MyAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.PastDueAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.user.dto.AssigneeDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.util.FilterModelDto;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Prateek
 *
 */

@Api(tags = { "Alert Management API" })
@RestController
@RequestMapping("/api/alertManagement")
public class AlertManagementController extends BaseController{

	@Autowired
	private AlertManagementService alertManagementService;
	
	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	SourceJurisdictionService sourceJurisdictionService;
	
	/*@ApiOperation("Save or Update Alerts ")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Alerts not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateAlerts", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateAlerts(HttpServletRequest request, @RequestParam String token, 
			@RequestBody AlertsDto alertDto) throws Exception{
		long userId = getCurrentUserId();
		AlertsDto alertDtos = alertManagementService.saveOrUpdateAlerts(alertDto, userId);
		
		if(alertDtos==null) {
			return new ResponseEntity<>(new ResponseMessage("Alert Not Updated"), HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>(alertDtos,HttpStatus.OK);
		}
	}*/
	
	@ApiOperation("Screening Alerts")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Alerts not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value="/screeningAlerts", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> screeningAlerts(HttpServletRequest request,@RequestParam MultipartFile uploadFile, @RequestParam String token, @RequestParam double confidence) throws Exception{
		
		if(uploadFile.isEmpty()) 
			throw new NoDataFoundException(ElementConstants.FILE_NOT_FOUND);
		String response = alertManagementService.getScreeningResultId(uploadFile,confidence);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
	/*@ApiOperation("Create Alerts ")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Alerts not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value="/createAlerts", consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> createAlerts(HttpServletRequest request, @RequestParam String token,@RequestBody CreateAlertDto alertData) throws Exception{
		String resp=alertManagementService.createAlerts(alertData);
		return new ResponseEntity<>(resp,HttpStatus.OK); 
	}*/
	@SuppressWarnings("unused")
	@ApiOperation("Save or Update Alerts ")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Alerts not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateAlerts", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateAlerts(HttpServletRequest request, @RequestParam String token, 
			@RequestBody List<AlertsDto> alertDtoList, @RequestParam(required = false) String bulkType) throws Exception{
		
		
		long userId = getCurrentUserId();
		List<AlertsDto> returnDtoList = new ArrayList<AlertsDto>();
		List<String> failedUpdates = new ArrayList<String>();
		Integer groupLevel = null;
		AssigneeDto assignee = null;
		ListItemDto statuse = null;

		//Entity Identification
		AlertsDto alertsDto = alertDtoList.get(0);
		if(alertsDto.getAlertId() != null 
				&& null != alertsDto.getStatuse() 
				&& (alertsDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_APPROVED_STATUS)
						|| alertsDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_REJECTED_STATUS))
				&& null != alertsDto.getClassification() && (alertsDto.getClassification().size() > 0)
				&& !StringUtils.isEmpty(alertsDto.getSource()))
		{
			AlertsDto alertDtos = alertManagementService.saveOrUpdateAlerts(alertsDto, userId);
			
			if (alertDtos != null) {
				returnDtoList.add(alertDtos);
			} else {
				failedUpdates.add("Entity Identification failed");
			}
			AlertUpdateDto updateDto = new AlertUpdateDto();
			updateDto.setResult(returnDtoList);
			updateDto.setFailedUpdates(failedUpdates);
			return new ResponseEntity<>(updateDto,HttpStatus.OK);
		}
		else  //normal saveOrUpdate
		{	
			for(AlertsDto alertDto : alertDtoList){
					AlertsDto alertDtos = alertManagementService.saveOrUpdateAlerts(alertDto, userId);
					groupLevel= alertDtos.getGroupLevel();
					assignee = alertDtos.getAsignee();
					statuse = alertDtos.getStatuse();
					if(alertDtos != null){
						returnDtoList.add(alertDtos);
					}else{
						if(null != alertDto.getEntityName())
							failedUpdates.add(alertDto.getEntityName());
					}
			}
			if(bulkType !=null && bulkType !=""){
				StringBuilder description = new StringBuilder("Bulk Operation - ");
				if(bulkType.equalsIgnoreCase("groupLevel")){
					String dtoGroupLevel = groupsService.find(groupLevel.longValue()).getName();
					description.append("Level changed to ");
					description.append(dtoGroupLevel + " for "+ returnDtoList.size() + " of "+ alertDtoList.size()+ " Alerts");
				}
				if(bulkType.equalsIgnoreCase("Assignee")){
					String dtoAssignee = assignee.getFirstName() + " " + assignee.getLastName();
					description.append("Assignee changed to ");
					description.append(dtoAssignee + " for "+ returnDtoList.size() + " of "+ alertDtoList.size()+ " Alerts");
				}
				if(bulkType.equalsIgnoreCase("status")){
					String dtoStatus = statuse.getDisplayName();
					description.append("status changed to ");
					description.append(dtoStatus + " for "+ returnDtoList.size() + " of "+ alertDtoList.size()+ " Alerts");
				}
				
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
				log.setDescription(description.toString());
				log.setName("");
				log.setSubType(ElementConstants.ALERT_BULK_SUBTYPE);
				log.setUserId(userId);
				auditLogService.save(log);
			}
			AlertUpdateDto updateDto = new AlertUpdateDto();
			updateDto.setResult(returnDtoList);
			updateDto.setFailedUpdates(failedUpdates);
			return new ResponseEntity<>(updateDto,HttpStatus.OK);
			}
	}
	
	/*@ApiOperation("Delete Alert")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "alert not found.") })
	@DeleteMapping(value = "/deleteAlert", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteListItem(@RequestParam(required=true) Long alertId, @RequestParam("token") String token,
			HttpServletRequest request){
		long userId = getCurrentUserId();
		boolean isAlertDeleted = alertManagementService.deleteAlert(alertId, userId);
		return new ResponseEntity<>(isAlertDeleted, HttpStatus.OK);
	}*/
	
	@ApiOperation("Get All Feed Items")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getAlerts", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlerts(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) Long feedId,@RequestParam(required = true) Boolean isAllRequired,
			 @RequestBody(required = false) FilterModelDto filterDto) throws Exception {
		
			Long totalResults = 0L;
			Map<List<AlertsDto>, Long> result = alertManagementService.getAlerts(filterDto, isAllRequired, pageNumber, recordsPerPage,totalResults, orderIn, orderBy, feedId,getCurrentUserId());
			List<AlertsDto> alertDtos = new ArrayList<>();
			
			for(Entry<List<AlertsDto>, Long> entry : result.entrySet()){
				alertDtos = entry.getKey();
				alertDtos.stream().filter(s -> null != s.getFeedGroups()).forEach(p -> Hibernate.initialize(p.getFeedGroups()));
				alertDtos.stream().forEach(p -> p.getFeedGroups().stream().filter(s -> null != s.getGroupId()).forEach(f -> Hibernate.initialize(f.getGroupId())));
				if(!isAllRequired){
					totalResults = entry.getValue();
				}
			}

			PaginationInformation information = new PaginationInformation();

			if (pageNumber != null && recordsPerPage != null) {
				int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
				int index = pageNumber != null ? pageNumber : 1;
				if(isAllRequired){
					totalResults = alertManagementService.getAlertsCount();
				}
				information.setTotalResults(totalResults);
				information.setTitle(request.getRequestURI());
				information.setKind("list");
				information.setCount(alertDtos != null ? alertDtos.size() : 0);
				information.setIndex(index);
				int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
				information.setStartIndex(nextIndex);
				information.setInputEncoding("utf-8");
				information.setOutputEncoding("utf-8");
			}

			AlertListDto alertListDto = new AlertListDto();
			alertListDto.setResult(alertDtos);
			alertListDto.setPaginationInformation(information);
			return new ResponseEntity<>(alertListDto, HttpStatus.OK);

	}
	
	@ApiOperation("Get All Feed Items")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserAlert", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserAlert(HttpServletRequest request, @RequestParam String token) throws Exception {
		Long userId = getCurrentUserId();
		List<AlertsDto> alertDtos = alertManagementService.getUserAlert(userId);
		return new ResponseEntity<>(alertDtos,HttpStatus.OK);
	}
	
	@ApiOperation("Save or Update Alert Comments ")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Comment not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateAlertComments", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateAlertComments( HttpServletRequest request, @RequestParam String token, 
			@RequestBody AlertCommentsDto alertCommentDto) throws Exception{
		long userId = getCurrentUserId();
		AlertCommentsDto alertCommentDtos = alertManagementService.saveOrUpdateAlertComments(alertCommentDto, userId);
		if(alertCommentDtos==null) {
			return new ResponseEntity<>(new ResponseMessage("comment unsuccessful"), HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>(alertCommentDtos,HttpStatus.OK);
		}
	}
	
	@ApiOperation("Get All Feed Items")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAlertCommentsByAlertId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertCommentsByAlertId(@RequestParam(required=true) Long alertId, HttpServletRequest request, @RequestParam String token) throws Exception {
		
		List<AlertCommentsDto> alertDtos = alertManagementService.getAlertCommentsByAlertId(alertId);
		return new ResponseEntity<>(alertDtos,HttpStatus.OK);
	}
	
	

	@ApiOperation("Gets My alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getMyAlerts", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getMyAlerts(HttpServletRequest request, @RequestParam String token) throws Exception {
		Long userId = getCurrentUserId();
		List<MyAlertsDto> myAlertsDto = alertManagementService.getMyAlerts(userId);
		return new ResponseEntity<>(myAlertsDto,HttpStatus.OK);
	}
	
	@ApiOperation("Gets Associated alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAssociatedAlerts", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAssociatedAlerts(HttpServletRequest request, @RequestParam String token) throws Exception {
		Long userId = getCurrentUserId();
		List<AssociatedAlertsDto> myAlertsDto = alertManagementService.getAssociatedAlerts(userId);
		return new ResponseEntity<>(myAlertsDto,HttpStatus.OK);
	}
	
	
	@ApiOperation("Gets Past Due alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getPastDueAlerts", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPastDueAlerts(HttpServletRequest request, @RequestParam String token) throws Exception {
		Long userId = getCurrentUserId();
		PastDueAlertsDto pastDueAlertsDto = alertManagementService.getPastDueAlerts(userId);
		return new ResponseEntity<>(pastDueAlertsDto,HttpStatus.OK);
	}
	
	
	@ApiOperation("Delete Comment")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "alert not found.") })
	@DeleteMapping(value = "/deleteComment", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteComment(@RequestParam Long alertId,@RequestParam Long commentId, @RequestParam("token") String token,
			HttpServletRequest request){
		long userId = getCurrentUserId();
		boolean isAlertDeleted = alertManagementService.deleteComment(alertId,commentId, userId);
		return new ResponseEntity<>(isAlertDeleted, HttpStatus.OK);
	}
	
	@ApiOperation("Get All Feed Items")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getAlertsCSV" , produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertsCSV(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) Long feedId, @RequestParam(required = true) Boolean isAllRequired,
			@RequestBody(required = false) FilterModelDto filterDto) throws Exception {

		Long totalResults = 0L;
		Map<List<AlertsDto>, Long> result = alertManagementService.getAlerts(filterDto, isAllRequired, pageNumber, recordsPerPage,totalResults, orderIn, orderBy, feedId,getCurrentUserId());
		List<AlertsDto> alertDtos = new ArrayList<>();
		
		for(Entry<List<AlertsDto>, Long> entry : result.entrySet()){
			alertDtos = entry.getKey();
			if(!isAllRequired){
				totalResults = entry.getValue();
			}
		}
		byte[] fileData=null;
	
		try {
		if (null != alertDtos && alertDtos.size()>0) {
			fileData = alertManagementService.convertToCSV(alertDtos).toByteArray();
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		    return new ResponseEntity<>(fileData, headers, HttpStatus.OK);
		}
		else {
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		    return new ResponseEntity<>("No Record Found", headers,HttpStatus.OK);
		}
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>("No Record Found", HttpStatus.OK);
		}
		//return new ResponseEntity<>(fileData, HttpStatus.OK);
	}
	
	@ApiOperation("Screening Alerts Result")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Alerts not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value="/screeningAlertsResult", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> screeningAlertsResult(@RequestParam String token, @RequestParam String id) throws Exception{
		if(id.isEmpty() || id == null)
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.ID_NOT_FOUND),HttpStatus.BAD_REQUEST);
		BatchScreeningResultDto dto = alertManagementService.getScreeningResults(id);
		if(dto == null)
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG),HttpStatus.BAD_GATEWAY);
		return new ResponseEntity<>(dto,HttpStatus.OK);	
	}
	
	/*@ApiOperation("Get Alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAlertsFromServer", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertsFromServer(@RequestParam(required = true) String name,
			@RequestParam(required = false) String givenName, @RequestParam(required = true) String jurisdiction,
			@RequestParam(required = false) String screeningType, @RequestParam(required = false) String familyName,
			@RequestParam(required = false) String birthDate, @RequestParam(required = true) boolean isWrapper,
			HttpServletRequest request, @RequestParam String token) throws Exception {

		return new ResponseEntity<>(alertManagementService.getAlertsFromServer(name, givenName, jurisdiction,
				screeningType, familyName, birthDate, isWrapper), HttpStatus.OK);
	}*/
	
	@ApiOperation("Create New Alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
	@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/createNewAlerts", consumes = {"application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertsFromServer( @RequestParam String token,@RequestParam(required=false) String requestId) throws Exception {
		
		GenericReturnObject message = new GenericReturnObject();
		
		Long existingcount = alertManagementService.getAlertsCount();
		Boolean isAlertsCreated = alertManagementService.createNewAlerts(requestId);
		if(isAlertsCreated){
			Long newCount = alertManagementService.getAlertsCount();
			message.setData(newCount-existingcount);
			message.setResponseMessage(ElementConstants.ALERTS_CREATION_SUCCESSFUL);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message,HttpStatus.OK);
		}else{
			message.setResponseMessage(ElementConstants.ALERTS_CREATION_FAILED);
			message.setStatus(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			return new ResponseEntity<>(message,HttpStatus.BAD_GATEWAY);
		}
		
	}
	
	@ApiOperation("On Demand Screening API")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/ODSScreening", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getODSScreeningResults(@RequestParam String token, @RequestBody String input) throws Exception {
		JSONObject json = new JSONObject(input);
		if(json.has("entity")){
			JSONObject entity = json.getJSONObject("entity");
			if(entity.has("name")){
				entity.put("name", URLDecoder.decode(entity.getString("name"), "UTF-8"));
			}
		}
		return new ResponseEntity<>(alertManagementService.getODSScreeningResults(json.toString()), HttpStatus.OK);
		
	}
	
	@ApiOperation("Post Screening Data API")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.INTERNAL_SERVER_ERROR) })
	@PostMapping(value = "/postSSBScreeningData", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> postSSBScreeningData(@RequestParam String token, @RequestBody String input, @RequestParam String id) throws Exception {	
		return new ResponseEntity<>(alertManagementService.postSSBData(input, id), HttpStatus.OK);
	}
	
	
	@ApiOperation("Get SSB Screening Data API")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.INTERNAL_SERVER_ERROR) })
	@GetMapping(value = "/getSSBScreeningData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSSBScreeningData(@RequestParam String id, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(alertManagementService.getSSBScreeningData(id), HttpStatus.OK);
	}
	
	@ApiOperation("Get Data from Url")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
	@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.INTERNAL_SERVER_ERROR) })
	@GetMapping(value = "/getDataFromUrl", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDataFromUrl(@RequestParam String url, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(alertManagementService.getDataFromUrl(url), HttpStatus.OK);
	}
	
	/*@ApiOperation("Get Sub Graph Vla API")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
	@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.INTERNAL_SERVER_ERROR) })
	@GetMapping(value = "/getSubGraphVla", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSubGraphVla(@RequestParam String entityId, @RequestParam String clientId, 
			@RequestParam int levels, @RequestParam String field,@RequestParam String parentId, @RequestParam String token) throws Exception {	
		return new ResponseEntity<>(alertManagementService.getSubGraphVla(entityId, clientId, levels, field, parentId), HttpStatus.OK);
	}*/
	
	
	@ApiOperation("Get Sub Graph Vla API")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
	@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.INTERNAL_SERVER_ERROR) })
	@GetMapping(value = "/getSubGraphVla", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSubGraphVla(@RequestParam String entityId, 
			@RequestParam int levels, @RequestParam String field,@RequestParam String parentId, @RequestParam String request_id,@RequestParam String token) throws Exception {	
		return new ResponseEntity<>(alertManagementService.getSubGraphVla(entityId, levels, field, parentId, request_id), HttpStatus.OK);
	}
	
}
