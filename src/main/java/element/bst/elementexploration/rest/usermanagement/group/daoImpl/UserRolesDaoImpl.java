package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserRolesDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author Paul Jalagari
 *
 */
@Repository("userRolesDao")
public class UserRolesDaoImpl extends GenericDaoImpl<UserRoles, Long> implements UserRolesDao {
	
	public UserRolesDaoImpl() {
		super(UserRoles.class);
	}

	@Override
	public Map<Long, Long> rolesWithUserCount() {
		
		List<Object[]> queryResult = new ArrayList<Object[]>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = (CriteriaQuery<Object[]>) builder.createQuery(Object[].class);
		Root<UserRoles> result = (Root<UserRoles>) criteriaQuery.from(UserRoles.class);
		criteriaQuery.multiselect(result.get("roleId"), builder.count(result));
		criteriaQuery.groupBy(result.get("roleId"));
		Query<Object[]> query = this.getCurrentSession().createQuery(criteriaQuery);
		
		queryResult = query.getResultList();
		Map<Long, Long> resultMap = new HashMap<Long, Long>();
		for(Object[] groupCount : queryResult){
			if(groupCount[0] != null &&  groupCount[1] != null){
				Roles role = (Roles) groupCount[0];
				resultMap.put(role.getRoleId(), Long.valueOf(groupCount[1].toString()));
			}
		}
		return resultMap;
	}

	@Override
	public Map<Long, Long> getActiveUserNRoleCount(Long statusId) {
		List<Object[]> queryResult = new ArrayList<Object[]>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = (CriteriaQuery<Object[]>) builder.createQuery(Object[].class);
		Root<UserRoles> result = (Root<UserRoles>) criteriaQuery.from(UserRoles.class);
		criteriaQuery.multiselect(result.get("roleId"), builder.count(result));

		Join<UserRoles, Users> userJoin = result.join("userId");
		criteriaQuery.where(builder.equal(userJoin.get("statusId"), statusId));
		
		criteriaQuery.groupBy(result.get("roleId"));
		Query<Object[]> query = this.getCurrentSession().createQuery(criteriaQuery);
		
		queryResult = query.getResultList();
		Map<Long, Long> resultMap = new HashMap<Long, Long>();
		for(Object[] groupCount : queryResult){
			if(groupCount[0] != null &&  groupCount[1] != null){
				Roles role = (Roles) groupCount[0];
				resultMap.put(role.getRoleId(), Long.valueOf(groupCount[1].toString()));
			}
		}
		return resultMap;
	}

	@Override
	public Object getStatuswiseUsersByRoleId(Long roleId) {
		List<Object[]> queryResult = new ArrayList<Object[]>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = (CriteriaQuery<Object[]>) builder.createQuery(Object[].class);
		Root<UserRoles> result = (Root<UserRoles>) criteriaQuery.from(UserRoles.class);
		//criteriaQuery.multiselect(result.get("roleId"), builder.count(result));

		Join<UserRoles, Users> userJoin = result.join("userId");
		criteriaQuery.multiselect(userJoin.get("statusId").get("displayName"), builder.count(userJoin));
		//criteriaQuery.where(builder.equal(userJoin.get("statusId"), statusId));
		criteriaQuery.where(builder.equal(result.get("roleId"), roleId));
		
		criteriaQuery.groupBy(userJoin.get("statusId"));
		Query<Object[]> query = this.getCurrentSession().createQuery(criteriaQuery);
		
		queryResult = query.getResultList();
		Map<String, Long> resultMap = new HashMap<String, Long>();
		for(Object[] groupCount : queryResult){
			if(groupCount[0] != null &&  groupCount[1] != null){
				resultMap.put(groupCount[0].toString(), Long.valueOf(groupCount[1].toString()));
			}
		}
		return resultMap;
	}

	@Override
	public UserRoles getUserRoleByRoleAndUserId(Long roleId, Long userId) {
		UserRoles userRole = null;
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<UserRoles> criteriaQuery = (CriteriaQuery<UserRoles>) builder.createQuery(UserRoles.class);
		Root<UserRoles> result = (Root<UserRoles>) criteriaQuery.from(UserRoles.class);
		criteriaQuery.select(result);
		if (roleId != null && userId != null) {

			try {
				criteriaQuery.where(builder.and(builder.equal(result.get("roleId"), roleId),
						builder.equal(result.get("userId"), userId)));
				Query<UserRoles> query = this.getCurrentSession().createQuery(criteriaQuery);

				userRole = (UserRoles) query.getSingleResult();
			} catch (NoResultException ex) {

			}
		}
		return userRole;
	}

	@Override
	public Map<Long, Long> getUsersCountByRole(Long roleId) {
		
		List<Object[]> queryResult = new ArrayList<Object[]>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = (CriteriaQuery<Object[]>) builder.createQuery(Object[].class);
		Root<UserRoles> result = (Root<UserRoles>) criteriaQuery.from(UserRoles.class);
		criteriaQuery.multiselect(result.get("roleId"), builder.count(result));
		criteriaQuery.groupBy(result.get("roleId"));
		criteriaQuery.where(builder.equal(result.get("roleId"),roleId));
		Query<Object[]> query = this.getCurrentSession().createQuery(criteriaQuery);
		
		queryResult = query.getResultList();
		Map<Long, Long> resultMap = new HashMap<Long, Long>();
		for(Object[] groupCount : queryResult){
			if(groupCount[0] != null &&  groupCount[1] != null){
				Roles role = (Roles) groupCount[0];
				resultMap.put(role.getRoleId(), Long.valueOf(groupCount[1].toString()));
			}
		}
		return resultMap;
	}

	@Override
	public boolean isUserRolesExist(Long userId, Long roleId) {
		
		UserRoles userRoles = getUserRoleByRoleAndUserId(roleId,userId);
		if(userRoles != null)
			return true;
		else
		    return false;
	}
	
	
}
