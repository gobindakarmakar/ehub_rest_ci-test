package element.bst.elementexploration.rest.usermanagement.security.daoImpl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.CountrySettingDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
/**
 * 
 * @author Amit Patel
 *
 */
@Repository("countrySettingDao")
public class CountrySettingDaoImpl extends GenericDaoImpl<CountrySetting, Long> implements CountrySettingDao{

	public CountrySettingDaoImpl(){
        super(CountrySetting.class);
    }

	@Override
	public CountrySetting getCountrySetting(String countryIso) {
		try {			
			String hql = "from CountrySetting where countryIso = :countryIso";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("countryIso", countryIso);
			@SuppressWarnings("unchecked")
			List<CountrySetting> countrySettings = (List<CountrySetting>) query.getResultList();
			if (countrySettings != null) {
				return countrySettings.get(0);
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "country setting", e, CountrySettingDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not find country - " + e.getMessage());
		}
		return new CountrySetting();
	}

}
