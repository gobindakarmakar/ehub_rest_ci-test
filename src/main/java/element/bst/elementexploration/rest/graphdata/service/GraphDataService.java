package element.bst.elementexploration.rest.graphdata.service;

public interface GraphDataService {

	String getDataFromBigDataServer(Long caseId, Integer limit) throws Exception;

	String getRiskDataByCaseId(Long caseId) throws Exception;

	String shortestPath(String vertexid1, String vertexid2) throws Exception;

	String entityGraph(String type, String name, String limit) throws Exception;
}
