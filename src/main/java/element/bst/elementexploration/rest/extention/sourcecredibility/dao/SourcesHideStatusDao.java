package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourcesHideStatusDao extends GenericDao<SourcesHideStatus, Long> {

	//SourcesHideStatus getSourceHideStatus(Long sourceId, Long classifcationId);

	List<SourcesHideStatus> getSourceHideStatusByClassification(Long classificationId);

}
