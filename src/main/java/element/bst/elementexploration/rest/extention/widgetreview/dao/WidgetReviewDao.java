package element.bst.elementexploration.rest.extention.widgetreview.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author rambabu
 *
 */
public interface WidgetReviewDao extends GenericDao<WidgetReview, Long> {

	List<WidgetReview> getWidgetReviewsByEntityId(String entityId);

	WidgetReview getWidgetWithEntityIdAndWidgetId(String entityId, Long widgetId);

}
