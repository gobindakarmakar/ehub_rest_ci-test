package element.bst.elementexploration.rest.settings.listManagement.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.settings.listManagement.domain.Role;
import element.bst.elementexploration.rest.settings.listManagement.dto.RoleDto;


public interface RoleService extends GenericService<Role, Long> {
	List<RoleDto> getRoles();
}
