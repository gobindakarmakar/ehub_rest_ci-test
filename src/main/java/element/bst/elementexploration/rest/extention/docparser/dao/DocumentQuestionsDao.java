package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentQuestionsDao extends GenericDao<DocumentQuestions, Long>{
	
	public List<DocumentQuestions> getDocumentQuestionsByTemplateId(Long templateId,boolean isPdf);

	public List<DocumentQuestions> getTableQuestionsById(Long tableId);
	
	public int findQuestions(long docId);

	public List<DocumentQuestions> getAllQuestions(long docId,boolean question);

	public List<DocumentQuestions> getDocumentTableQuestionsById(Long tableId);
	
	public List<DocumentQuestions> getOSINTQuestionsByTemplateId(Long templateId);
	
	public int getElements(Long templateId);
	
	

}
