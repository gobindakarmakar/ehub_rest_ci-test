package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_ISO_CODES")
public class DocumentISOCodes implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String question;
	private String normalizedQuestion;
	private String primaryIsoCode;
	private List<IsoCodes> isoCodes;
	private String answerType;
	private String yesScore;
	private String noScore;
	private Date updatedTime;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "QUESTION")
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
	@Column(name = "NORMALIZED_QUESTION")
	public String getNormalizedQuestion() {
		return normalizedQuestion;
	}

	public void setNormalizedQuestion(String normalizedQuestion) {
		this.normalizedQuestion = normalizedQuestion;
	}
	@Column(name = "PRIMARY_ISO_CODE")
	public String getPrimaryIsoCode() {
		return primaryIsoCode;
	}

	public void setPrimaryIsoCode(String primaryIsoCode) {
		this.primaryIsoCode = primaryIsoCode;
	}
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentISOCodes",fetch=FetchType.EAGER)
	public List<IsoCodes> getIsoCodes() {
		return isoCodes;
	}

	public void setIsoCodes(List<IsoCodes> isoCodes) {
		this.isoCodes = isoCodes;
	}
	@Column(name="ANSWER_TYPE")
	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}
	@Column(name = "YES_SCORE")
	public String getYesScore() {
		return yesScore;
	}

	public void setYesScore(String yesScore) {
		this.yesScore = yesScore;
	}
	@Column(name = "NO_SCORE")
	public String getNoScore() {
		return noScore;
	}

	public void setNoScore(String noScore) {
		this.noScore = noScore;
	}
	@Column(name = "UPDATED_TIME")
	@UpdateTimestamp
	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}


	
	
	

}
