package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourceCredibilityDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;

/**
 * @author suresh
 *
 */
public class SourceCredibilityDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourceCredibilityDaoImpl sourceCredibilityDao;

	SourceCredibility sourceCredibility = null;

	@Before
	public void setupMock() {
		sourceCredibility = new SourceCredibility();
		sourceCredibility.setSourceCredibilityId(1L);
		sourceCredibility.setCredibility(CredibilityEnums.HIGH);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findSubClassificationCredibility() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sourceCredibility);
		SourceCredibility sourceCredibilityFind = sourceCredibilityDao.findSubClassificationCredibility(2L, 65L, 8L);
		assertEquals(sourceCredibility.getCredibility(), sourceCredibilityFind.getCredibility());
	}

}
