package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;

/**
 * @author Paul Jalagari
 *
 */
public interface RolesDao extends GenericDao<Roles, Long>{

	Roles findRole(Roles role) throws Exception;
	
	List<Roles> getRolesOfUser(Long userId) throws Exception;

	Long getRolesCount();

	Roles findRoleByName(String roleName)throws Exception;

	Roles getRoleByIdOrName(Long roleId, String roleName);

    void deleteRoleById(Long roleId);

}
