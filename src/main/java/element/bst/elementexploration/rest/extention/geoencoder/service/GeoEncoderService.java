package element.bst.elementexploration.rest.extention.geoencoder.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.geoencoder.dto.GoogleResponse;

/**
 * @author suresh
 *
 */
public interface GeoEncoderService {

	public List<GoogleResponse> getLatitudeAndLongitude(String address) throws Exception;

	//boolean saveCountiesMasterData(MultipartFile file) throws IOException, org.json.simple.parser.ParseException;

}
