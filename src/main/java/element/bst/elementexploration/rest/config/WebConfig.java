package element.bst.elementexploration.rest.config;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.EndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.HealthIndicatorAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.ManagementServerProperties;
import org.springframework.boot.actuate.autoconfigure.MetricExportAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricsChannelAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricsDropwizardAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.PublicMetricsAutoConfiguration;
import org.springframework.boot.actuate.endpoint.BeansEndpoint;
import org.springframework.boot.actuate.endpoint.ConfigurationPropertiesReportEndpoint;
import org.springframework.boot.actuate.endpoint.DumpEndpoint;
import org.springframework.boot.actuate.endpoint.EnvironmentEndpoint;
import org.springframework.boot.actuate.endpoint.HealthEndpoint;
import org.springframework.boot.actuate.endpoint.InfoEndpoint;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.boot.actuate.endpoint.RequestMappingEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter;
import org.springframework.boot.actuate.endpoint.mvc.EnvironmentMvcEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.HealthMvcEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.MetricsMvcEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.MvcEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author Amit Patel
 *
 */

@ComponentScan({ "element.bst.elementexploration.rest" })
@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
@EnableSwagger2
@Import({ DataAccessConfig.class, WebSecurityConfig.class,EndpointAutoConfiguration.class, HealthIndicatorAutoConfiguration.class, MetricExportAutoConfiguration.class,
	MetricFilterAutoConfiguration.class, MetricsChannelAutoConfiguration.class, MetricsDropwizardAutoConfiguration.class,
	MetricRepositoryAutoConfiguration.class, PublicMetricsAutoConfiguration.class 
})

//use for local machine
@PropertySources({ @PropertySource(value = "classpath:ehubrest.properties"),
		@PropertySource(value = "classpath:serviceurls.properties"),
		@PropertySource(value = "classpath:user.properties"), @PropertySource(value = "classpath:kieServer.properties"),
		@PropertySource(value = "classpath:activiti-app.properties"),@PropertySource(value = "classpath:application.properties"),
		@PropertySource(value = "classpath:bpm.properties"),@PropertySource(value = "classpath:permissions.properties")})



// Use for server
/*@PropertySources({

		@PropertySource(value = "file:///${app.conf.dir}/ehubrest.properties"),

		@PropertySource(value = "file:///${app.conf.dir}/kieServer.properties"),

		@PropertySource(value = "file:///${app.conf.dir}/serviceurls.properties"),

		@PropertySource(value = "file:///${app.conf.dir}/user.properties"),

		@PropertySource(value = "file:///${app.conf.dir}/activiti-app.properties"),

		@PropertySource(value = "file:///${app.conf.dir}/bpm.properties") })*/

public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configuration) {

		configuration.enable();
	}
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedMethods("PUT", "DELETE", "POST", "GET").allowedOrigins("*");
	}
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() throws IOException {

		PropertySourcesPlaceholderConfigurer config = new PropertySourcesPlaceholderConfigurer();
		// Use for server
		//config.setLocations(new PathMatchingResourcePatternResolver()
				//.getResources("file:///" + System.getProperty("app.conf.dir") + "/log.properties"));
		// User for local machine
		config.setLocations(new PathMatchingResourcePatternResolver().getResources("classpath:log.properties"));
		return config;
	}

	@Bean(name = "multipartResolver")
	public MultipartResolver getMultipartResolver() {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(104857600);
		commonsMultipartResolver.setMaxInMemorySize(209715200);
		return commonsMultipartResolver;

	}

	/* Swagger Configuration */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.any())
				.paths(paths()).build().tags(new Tag("Case API", "Manages cases"),
						new Tag("Contacts API", "Manages user's contacts"),
						new Tag("Document API", "Manages documents"),
						new Tag("Adverse News API", "Manages adverse news data"),
						new Tag("Datacuration API", "Manages datacuration information"),
						new Tag("Document Parser API", "Manages insurance form data"),
						new Tag("Entity search API", "Manages entity data"));
	}

	// Describe your apis
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Element Exploration APIs")
				.description("This page lists all the rest apis for Element Exploration Application.")
				.version("1.0-SNAPSHOT").build();
	}

	// Only select apis that matches the given Predicates.
	private Predicate<String> paths() {
		// Match all paths except /error
		return Predicates.and(PathSelectors.regex("/.*"), Predicates.not(PathSelectors.regex("/error.*")));
	}

	/* Mail configuration */
	/*@Bean("mailSender")
	public JavaMailSender getMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		mailSender.setUsername("");
		mailSender.setPassword("");

		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.starttls.enable", "true");
		javaMailProperties.put("mail.smtp.auth", "true");
		javaMailProperties.put("mail.transport.protocol", "smtp");
		javaMailProperties.put("mail.debug", "true");

		mailSender.setJavaMailProperties(javaMailProperties);
		return mailSender;
	}*/

	/*@Bean
	public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {
		FreeMarkerConfigurationFactoryBean fmConfigFactoryBean = new FreeMarkerConfigurationFactoryBean();
		fmConfigFactoryBean.setTemplateLoaderPath("/templates/");
		return fmConfigFactoryBean;
	}*/
	
	@Bean
	public ViewResolver internalResourceViewResolver() {
	    InternalResourceViewResolver bean = new InternalResourceViewResolver();
	   // bean.setViewClass(JstlView.class);
	    bean.setPrefix("/");
	    bean.setSuffix(".html");
	    return bean;
	}
	
	    @Bean(name="taskExecutor")
	    public TaskExecutor threadPoolTaskExecutor() {
	        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	        executor.setCorePoolSize(20);
	        executor.setMaxPoolSize(20);
	        executor.setThreadNamePrefix("default_task_executor_thread");
	        executor.initialize();
	        return executor;
	    } 
	    
	    @Bean("executor")
		public ThreadPoolExecutor getTreadPool() {
	        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(30);
	        return executor;
		}
	    
	  ///////////////Health Check API configuration//////////////////////////////////////////////////

	    @Bean
		@Autowired
		public EndpointHandlerMapping endpointHandlerMapping(Collection<? extends MvcEndpoint> endpoints) {
			return new EndpointHandlerMapping(endpoints);
		}

		@Bean
		@Autowired
		public HealthMvcEndpoint healthMvcEndpoint(HealthEndpoint delegate) {
			return new HealthMvcEndpoint(delegate, false);
		}

		@Bean
		@Autowired
		public MetricsMvcEndpoint metricsMvcEndpoint(MetricsEndpoint delegate) {
			return new MetricsMvcEndpoint(delegate);
		}

		@Bean
		@Autowired
		public EnvironmentMvcEndpoint environmentMvcEndpoint(EnvironmentEndpoint delegate) {
			return new EnvironmentMvcEndpoint(delegate);
		}

		@Bean
		@Autowired
		public EndpointMvcAdapter infoMvcEndPoint(InfoEndpoint delegate) {
			return new EndpointMvcAdapter(delegate);
		}

		@Bean
		@Autowired
		public EndpointMvcAdapter beansEndPoint(BeansEndpoint delegate) {
			return new EndpointMvcAdapter(delegate);
		}

		@Bean
		@Autowired
		public EndpointMvcAdapter dumpEndPoint(DumpEndpoint delegate) {
			return new EndpointMvcAdapter(delegate);
		}

		@Bean
		@Autowired
		public EndpointMvcAdapter configurationPropertiesEndPoint(ConfigurationPropertiesReportEndpoint delegate) {
			return new EndpointMvcAdapter(delegate);
		}

		@Bean
		@Autowired
		public EndpointMvcAdapter requestMappingEndPoint(RequestMappingEndpoint delegate) {
			return new EndpointMvcAdapter(delegate);
		}

	    
	   

}