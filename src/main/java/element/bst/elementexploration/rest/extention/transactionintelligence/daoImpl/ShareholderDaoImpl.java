package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.ShareholderDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Shareholders;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.FilterUtility;

/**
 * @author Paul Jalagari
 * 
 */
@Repository("shareholderDao")
public class ShareholderDaoImpl extends GenericDaoImpl<Shareholders, Long> implements ShareholderDao {

	private Random random ; 
	
	public ShareholderDaoImpl() {
		super(Shareholders.class);
		this.random = new Random();
	}

	@Autowired
	CustomerDetailsDao customerDetailsDao;
	
	@Autowired
	FilterUtility filterUtility;

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public CorporateStructureDto getCorporateStructure(String fromDate, String toDate,FilterDto filterDto) {
		CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<Object[]> objArray=new ArrayList<>();
		List<Object[]> transactionArray=new ArrayList<>();

		Double alertAmount =0.0;
		Double transactionAmount=0.0;
		try {
			/*StringBuilder hql = new StringBuilder();

			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id)) ");
			hql.append(
					"from Alert a Right Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and t.businessDate>=:fromDate and t.businessDate<=:toDate and c.corporateStructure is not null ");

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select sum(t.amount)");
			hql1.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql1.append(
					" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and c.corporateStructure is not null ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			corporateStructureDto = (CorporateStructureDto) query.getSingleResult();
			Double alertedAmount = (Double) query1.getSingleResult();
			corporateStructureDto.setAlertAmount(alertedAmount);
			return corporateStructureDto;*/
			StringBuilder hql = new StringBuilder();
			StringBuilder hql1 = new StringBuilder();
			StringBuilder hql2 = new StringBuilder();
			//for general query(hql)
			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			hql.append("Round(sum(t.amount),2),count(distinct t.alertTransactionId),(count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id))");
			hql.append(" from TransactionMasterView t where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate and t.benificiaryCorporateStructure"
					+ " is not null and t.benificiaryCustomerType='CORP' ");
			if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			corporateStructureDto = (CorporateStructureDto) query.getSingleResult();
			//For alertAmount (hql1)
            hql1.append(queryForAlertAmount("CorporateStructure"));
            if(filterDto!=null)
            hql1.append(filterUtility.filterColumns(filterDto));
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query1, filterDto, fromDate, toDate);
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			objArray = (List<Object[]>) query1.getResultList();
			for (Object[] object : objArray) {
				alertAmount=alertAmount+Double.valueOf(object[0].toString());
			}
			//For transaction amount (hql2)
			hql2.append(queryForTransactionAmount("CorporateStructure"));
			hql2.append(filterUtility.filterColumns(filterDto));			
			Query<?> query2 = this.getCurrentSession().createQuery(hql2.toString());
			query2.setParameter("fromDate", formatter.parse(fromDate));
			query2.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!= null)
				filterUtility.queryReturn(query2, filterDto, fromDate, toDate);
			transactionArray =  (List<Object[]>) query2.getResultList();
			for (Object[] objects : transactionArray) {
				transactionAmount=transactionAmount+Double.valueOf(objects[0].toString());
			}
			corporateStructureDto.setAlertAmount(Math.round(alertAmount*100.0)/100.0);
			corporateStructureDto.setTransactionAmount(Math.round(transactionAmount*100.0)/100.0);
			return corporateStructureDto;
		} catch (NoResultException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure. Reason : " + e.getMessage());
		}
	}

	// ok
/*	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCorporateStructureAggregates(String fromDate, String toDate,
			List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.corporateStructure,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.corporateStructure");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure aggregates. Reason : " + e.getMessage());
		}
	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCorporateStructureAggregates(String fromDate, String toDate,
			FilterDto filterDto,String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Map<String,CorporateStructureDto> map=new HashMap<String,CorporateStructureDto>();
		List<Object[]> objArray=new ArrayList<>();
		try {
			/*StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" t.benificiaryCorporateStructure,Round(sum(t.amount),2),count(distinct t.alertTransactionId)) ");
			hql.append(
					" from TransactionMasterView t where t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			if(groupBy!=null)
			hql.append(" group by "+groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;*/
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct Round(t.amount,2),t.alertTransactionId,t.benificiaryCorporateStructure from TransactionMasterView "
					+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
			if("CorporateStructure".equalsIgnoreCase(type))
					hql.append(" and t.benificiaryCorporateStructure is not null and t.benificiaryCustomerType='CORP' ");
			if("Shareholder".equalsIgnoreCase(type))
				hql.append(" and t.shareHolderName is not null ");
			if("Geography".equalsIgnoreCase(type))
				hql.append(" and t.benificiaryCountry is not null ");
			if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			//if(filterDto==null||((!filterDto.isAlertByStatusGreaterThan30Days())&&(!filterDto.isAlertsByPeriod10to20Days())&&(!filterDto.isAlertsByPeriod20to30Days())&&(!filterDto.isAlertsByPeriodgreaterthan30Days()))){

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			//}
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] object : objArray) {
				if(map.containsKey(object[2].toString())){
					CorporateStructureDto dto=map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount()+1);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[0].toString()));
				}else {
					CorporateStructureDto dto=new CorporateStructureDto(object[2].toString(),Double.valueOf(object[0].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, CorporateStructureDto> entry : map.entrySet()) {
	            list.add(entry.getValue());
	    }
			 list.sort(Comparator.comparingDouble(CorporateStructureDto :: getAlertAmount).reversed());
			return list;
		
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure aggregates. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCorporateStructuteByType(String fromDate, String toDate, String type,
			List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.corporateStructure,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and c.corporateStructure=:type");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.corporateStructure");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to corporate structure by type. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAlertEntitiesGeography(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append("t.amount,");
			hql.append(type);
			hql.append(
					" ) from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate group by :type");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("type", type);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Alert Entities Geography. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAlertEntitiesFilter(String fromDate, String toDate, String type,
			List<String> countries, String groupBy) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(groupBy);
			hql.append(",sum(t.amount)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and ");
			/*
			 * hql.append(type); hql.append("="); hql.append(country);
			 */
			/*
			 * if (countries != null && !countries.isEmpty())
			 * hql.append(countries);
			 */
			hql.append(type);
			hql.append(" IN (:countries) ");
			hql.append("group by :groupBy");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("countries", countries);
			query.setParameter("groupBy", groupBy);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Alert Entities Filter. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAlertEntitiesFilter(String fromDate, String toDate, String type,
			List<String> scenarios, String filter, String country, String groupBy) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append("sum(t.amount),c.displayName) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate where ");
			hql.append(filter);
			hql.append("=:country");
			// hql.append(country);
			hql.append(" and ");
			hql.append(type);
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" IN (:scenarios) ");
			hql.append("group by :groupBy");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("scenarios", scenarios);
			query.setParameter("groupBy", groupBy);
			query.setParameter("country", country);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Alert Entities filter. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAlertEntitiesFilter(String fromDate, String toDate, String type,
			List<String> scenarios, String oldfilter, String country, String newFilter, Long customerId,
			String groupBy) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerCounterPartyDto( ");
			hql.append(
					"cd.displayName,a.scenario,t.beneficiaryAccountId,t.amount,c.iso2Code,cd.residentCountry,a.alertBusinessDate,acc.bankName) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			hql.append("Join Country c on cd.residentCountry=c.iso2Code ");
			hql.append("Join Account acc on t.beneficiaryAccountId=acc.accountNumber");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate where ");
			hql.append(oldfilter);
			hql.append("=:country");
			hql.append(" and ");
			hql.append(newFilter);
			hql.append("=:customer");
			hql.append(" and ");
			hql.append(type);
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" IN (:scenarios) ");
			hql.append("group by :groupBy");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("scenarios", scenarios);
			query.setParameter("groupBy", groupBy);
			query.setParameter("country", country);
			query.setParameter("customer", customerId);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Alert Entities filter. Reason : " + e.getMessage());
		}
	}

	// ok
	@Override
	public void insertShareHolders() {
		for (int i = 0; i <= 35; i++) {
			Shareholders shareholders = new Shareholders();
			shareholders.setNumberOfShares(getRandomShareholders());
			List<CustomerDetails> customerDetails = customerDetailsDao.findCorporateCustomers();
			CustomerDetails randomCustomer = getRandomShareholdersName(customerDetails);
			shareholders.setShareHolderName(randomCustomer.getDisplayName());
			shareholders.setCustomerNumber(randomCustomer.getCustomerNumber());
			this.getCurrentSession().save(shareholders);

		}

	}

	// ok
	private Integer getRandomShareholders() {
		int Low = 1;
		int High = 100;
		int result = this.random.nextInt(High - Low) + Low;
		return (Integer) (result);
	}

	// ok
	private CustomerDetails getRandomShareholdersName(List<CustomerDetails> customerList) {

		CustomerDetails corp = null;
		int index = this.random.nextInt(customerList.size());
		corp = customerList.get(index);
		return corp;
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCorporateByType(String fromDate, String toDate, String type, List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);

			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate by Type. Reason : " + e.getMessage());
		}
		return list;

	}

	// ok
/*	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCorporateStructuteByScenarios(String fromDate, String toDate,
			List<String> scenarios, String type) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try {
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			queryBuilder.append(" c.id,c.displayName,a.alertBusinessDate,t.amount) ");
			queryBuilder.append(" from Alert a,TransactionsData t,CustomerDetails c where a.transactionId=t.id and ");
			queryBuilder.append(
					" t.beneficiaryCutomerId=c.id and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			queryBuilder.append("and a.scenario in (:scenarios) ");
			queryBuilder.append(" and c.corporateStructure=:type");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to corporate structure by scenarios. Reason : " + e.getMessage());
		}
	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCorporateStructuteByScenarios(String fromDate, String toDate,FilterDto filterDto,String type) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		StringBuilder hql = new StringBuilder();
		try {
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			hql.append(" t.benificiaryCustomerId,t.benificiaryCustomerName,t.alertBusinessDate,t.amount) ");
			hql.append(" from TransactionMasterView t where t.alertTransactionId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
			if("CorporateStructure".equalsIgnoreCase(type))
				hql.append(" and t.benificiaryCustomerType='CORP' and t.benificiaryCorporateStructure is not null ");
			if("Shareholder".equalsIgnoreCase(type))
				hql.append(" and t.shareHolderName is not null ");
			if("Geography".equalsIgnoreCase(type))
				hql.append(" and t.benificiaryCountry is not null ");
			if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			//if(filterDto==null||((!filterDto.isAlertByStatusGreaterThan30Days())&&(!filterDto.isAlertsByPeriod10to20Days())&&(!filterDto.isAlertsByPeriod20to30Days())&&(!filterDto.isAlertsByPeriodgreaterthan30Days()))){

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			//}
			/*query.setParameter("type", type);
			query.setParameter("scenarios", scenarios);*/
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to corporate structure by scenarios. Reason : " + e.getMessage());
		}
	}

	// ok
	/*@Override
	public CorporateStructureDto getAlertEntitiesGeography(String fromDate, String toDate) {
		CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();

			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id)) ");
			hql.append(
					"from Alert a Right Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(" and t.businessDate>=:fromDate and t.businessDate<=:toDate and c.residentCountry is not null ");

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select sum(t.amount)");
			hql1.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql1.append(
					" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and c.residentCountry is not null ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			corporateStructureDto = (CorporateStructureDto) query.getSingleResult();
			Double alertedAmount = (Double) query1.getSingleResult();
			corporateStructureDto.setAlertAmount(alertedAmount);
			return corporateStructureDto;
		} catch (NoResultException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure. Reason : " + e.getMessage());
		}
	}*/
	
	@Override
	public CorporateStructureDto getAlertEntitiesGeography(String fromDate, String toDate,FilterDto filterDto) {
		CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();

			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append("Round(sum(t.amount),2),count(distinct t.alertTransactionId),(count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id))");
			hql.append(" from TransactionMasterView t where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate");
			
			hql.append(filterUtility.filterColumns(filterDto));

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select sum(t.amount)");
			hql1.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql1.append(
					" and DATE(a.alertBusinessDate)>=:fromDate and DATE(a.alertBusinessDate)<=:toDate and c.residentCountry is not null ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			corporateStructureDto = (CorporateStructureDto) query.getSingleResult();
			Double alertedAmount = (Double) query1.getSingleResult();
			corporateStructureDto.setAlertAmount(alertedAmount);
			return corporateStructureDto;
		} catch (NoResultException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCountryAggregates(String fromDate, String toDate, List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.residentCountry,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.residentCountry");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Country aggregates. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCountryByType(String fromDate, String toDate, String type, List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.residentCountry=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate by Type. Reason : " + e.getMessage());
		}
		return list;

	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAlertEntitiesBusinessType(String fromDate, String toDate) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" t.amount,c.industry) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("group by c.industry");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			/*
			 * if (scenarios != null && !scenarios.isEmpty())
			 * query.setParameter("scenarios", scenarios);
			 */
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to alert entities business types. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getBusinessAggregates(String fromDate, String toDate, List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.industry,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.industry");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get Business Aggregates. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getBusinessByType(String fromDate, String toDate, String type, List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(" and c.industry=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to business by Type. Reason : " + e.getMessage());
		}
		return list;

	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCustomerByCountryScenario(String fromDate, String toDate, String country,
			List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.id,c.displayName,a.alertBusinessDate,sum(t.amount)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("where c.residentCountry = :country ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.id");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("country", country);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get customer by country scenario. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCustomerByBusinessScenario(String fromDate, String toDate, String industry,
			List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.id,c.displayName,a.alertBusinessDate,sum(t.amount)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("where c.industry = :industry ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.id");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("industry", industry);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get customer by business scenario. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getShareholderCorporateStructure(String fromDate, String toDate,
			String groupBy) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		list = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" cd.corporateStructure,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(
					" and cd.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("group by " + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			// query.setParameter("groupBy", groupBy);

			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException(
					"Failed to get Shareholder corporate structure. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getShareholderCountry(String fromDate, String toDate, String groupBy) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" count(a.id),t.amount,cd.residentCountry) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(
					" and cd.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("group by " + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			// query.setParameter("groupBy", groupBy);

			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get shareholder country. Reason : " + e.getMessage());
		}
	}

	// ok
	/*@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getShareholderAggregates(String fromDate, String toDate,
			List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		list = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id))");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by cd.residentCountry");
			System.out.println(hql.toString());
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Shareholders aggregates. Reason : " + e.getMessage());
		}
	}*/

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getShareholderAggregates(String fromDate, String toDate,
			FilterDto filterDto) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		list = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id))");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("group by cd.residentCountry");
			//System.out.println(hql.toString());
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Shareholders aggregates. Reason : " + e.getMessage());
		}
	}
	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getCustomerByShareholderScenario(String fromDate, String toDate, String country,
			List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" cd.id,cd.displayName,a.alertBusinessDate,sum(t.amount)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(
					" and cd.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("where cd.residentCountry ='" + country + "' ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by cd.id");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			// query.setParameter("country", country);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get Shareholder scenario. Reason : " + e.getMessage());
		}
	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getShareholderByType(String fromDate, String toDate, String type, List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		list = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(" and cd.corporateStructure='" + type
					+ "' and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			// query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			//System.out.println(hql.toString());
			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Shareholder by Type. Reason : " + e.getMessage());
		}
		return list;

	}

/*	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate,
			boolean isTransaction, String filter, Integer pageNumber, Integer recordsPerPage) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType
					+ " TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id and "
					+ filter);
			hql.append(" is not null and " + betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to view all corporate structure. Reason : " + e.getMessage());
		}

	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate,
			boolean isTransaction, String filter, Integer pageNumber, Integer recordsPerPage,FilterDto filterDto) {
		List<Object[]> objArray = new ArrayList<Object[]>();
//		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		Map<String,CorporateStructureDto> map=new HashMap<String,CorporateStructureDto>();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " distinct t.id,t.transactionBusinessDate,round(t.amount)";
				betweenDates = " DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate and t.id is not null";

			} else {
				selectColumns = " distinct t.alertTransactionId,t.alertBusinessDate,round(t.amount)";
				betweenDates = " DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate and t.alertTransactionId is not null ";

			}
			hql.append("select" + selectColumns);
					hql.append(" from TransactionMasterView t where "+betweenDates);
					if(filterDto!=null)
					hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			//if(filterDto==null||((!filterDto.isAlertByStatusGreaterThan30Days())&&(!filterDto.isAlertsByPeriod10to20Days())&&(!filterDto.isAlertsByPeriod20to30Days())&&(!filterDto.isAlertsByPeriodgreaterthan30Days()))){
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			//}
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			objArray =  (List<Object[]>) query.getResultList();
			for (Object[] objects : objArray) {
				if(map.containsKey(objects[1].toString())){
					CorporateStructureDto dto=map.get(objects[1].toString());
					if(isTransaction){
						dto.setTransactionAmount(dto.getTransactionAmount()+Double.valueOf(objects[2].toString()));
					}else {
						dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(objects[2].toString()));
					}
					
				}else {
					CorporateStructureDto dto=new CorporateStructureDto();
					if(isTransaction){
						 dto=new CorporateStructureDto(formatter.parse(objects[1].toString()),Double.valueOf(objects[2].toString()));
					}else {
						 dto=new CorporateStructureDto(Double.valueOf(objects[2].toString()),formatter.parse(objects[1].toString()));
					}
					map.put(objects[1].toString(),dto);
				}
			}
			List<CorporateStructureDto> list=new ArrayList<>(map.values());
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to view all corporate structure. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getViewAll(String fromDate, String toDate, boolean isTransaction,
			Integer pageNumber, Integer recordsPerPage, String queryforCondition) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " and t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType
					+ " TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			hql.append("join Country c on c.iso2Code=cd.residentCountry and ");
			hql.append(queryforCondition + betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get view all. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public long getViewAllCorporateStructureCount(String fromDate, String toDate, boolean isTransaction) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,sum(t.amount)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " sum(t.amount),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType
					+ " TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			hql.append(" and cd.corporateStructure is not null and " + betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CorporateStructureDto>) query.getResultList();
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate structure count. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public long getViewAllCount(String fromDate, String toDate, boolean isTransaction, String queryforCondition) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				if (queryforCondition != null && !queryforCondition.isEmpty())
					joinType = joinType + queryforCondition;
				groupBy = " group by t.businessDate";
				betweenDates = " and t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				/*
				 * if (queryforCondition != null &&
				 * !queryforCondition.isEmpty())
				 * joinType=joinType+queryforCondition;
				 */
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType
					+ " TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			if (queryforCondition != null && !queryforCondition.isEmpty())
				hql.append(queryforCondition);
			hql.append("join Country c on c.iso2Code=cd.residentCountry and ");
			hql.append(betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CorporateStructureDto>) query.getResultList();
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get view all count. Reason : " + e.getMessage());
		}

	}

	/*@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAssociatedAlerts(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" count(a.id),sum(t.amount),a.scenario ) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			hql.append("where cd.corporateStructure = :type ");
			hql.append("group by a.scenario");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get Associated alerts. Reason : " + e.getMessage());
		}
	}*/
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAssociatedAlerts(String fromDate, String toDate, FilterDto filterDto) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" count(distinct t.alertTransactionId),Round(sum(t.amount),2),t.alertScenario ) ");
			hql.append(
					" from TransactionMasterView t where DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			//hql.append("where cd.corporateStructure = :type ");
			hql.append(" group by t.alertScenario");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			//if(filterDto==null||((!filterDto.isAlertByStatusGreaterThan30Days())&&(!filterDto.isAlertsByPeriod10to20Days())&&(!filterDto.isAlertsByPeriod20to30Days())&&(!filterDto.isAlertsByPeriodgreaterthan30Days()))){
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			//}
			//query.setParameter("type", type);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get Associated alerts. Reason : " + e.getMessage());
		}
	}

/*	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate,
			boolean isTransaction, String filter, String queryforCondition, Integer pageNumber,
			Integer recordsPerPage) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " and t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType
					+ " TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			if (queryforCondition != null && !queryforCondition.isEmpty())
				hql.append(queryforCondition);
			hql.append(" Join Country c on c.iso2Code=cd.residentCountry ");
			hql.append(betweenDates + groupBy);
			System.out.println(hql.toString());
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to view all corporate structure. Reason : " + e.getMessage());
		}

	}
*/
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate,
			boolean isTransaction, String filter, String queryforCondition, Integer pageNumber,
			Integer recordsPerPage) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " and t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType
					+ " TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			if (queryforCondition != null && !queryforCondition.isEmpty())
				hql.append(queryforCondition);
			hql.append(" Join Country c on c.iso2Code=cd.residentCountry ");
			hql.append(betweenDates + groupBy);
			//System.out.println(hql.toString());
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to view all corporate structure. Reason : " + e.getMessage());
		}

	}

	
	/*@Override
	public List<CorporateStructureDto> getTopFiveAlerts(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> corporateStructureDtoList = new ArrayList<CorporateStructureDto>();
		String sql = null;
		try{
		if ("CorporateStructure".equalsIgnoreCase(type)) {
			sql = " select input.bid,input.amt,input.cnt,input.typ from ( SELECT t.beneficiaryCutomerId as bid,sum(t.AMOUNT) as amt,count(a.ID) as cnt,cd.CORPORATE_STRUCTURE as typ "
					+ "FROM ALERT a join TRANSACTIONS t on a.TRANSACTION_ID= t.ID join CUSTOMER_DETAILS cd on t.beneficiaryCutomerId=cd.ID "
					+ " where t.BUSINESS_DATE>='" + fromDate + "' and t.BUSINESS_DATE<='" + toDate
					+ "' and cd.CORPORATE_STRUCTURE is not null "
					+ "group by t.beneficiaryCutomerId) as input order by input.amt desc";
			System.out.println(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> list = this.getCurrentSession().createNativeQuery(sql).setMaxResults(5).getResultList();
			for (Object[] objects : list) {
				CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
				corporateStructureDto.setCustomerId(Long.valueOf(objects[0].toString()));
				corporateStructureDto.setAlertAmount(Double.valueOf(objects[1].toString()));
				corporateStructureDto.setAlertCount(Long.valueOf(objects[2].toString()));
				corporateStructureDto.setType(objects[3].toString());
				corporateStructureDtoList.add(corporateStructureDto);
			}

		} else if ("Shareholders".equalsIgnoreCase(type)) {
			sql = " select input.bid,input.amt,input.cnt,input.typ from ( SELECT t.beneficiaryCutomerId as bid,sum(t.AMOUNT) as amt,count(a.ID) as cnt,cd.CORPORATE_STRUCTURE as typ "
					+ "FROM ALERT a join TRANSACTIONS t on a.TRANSACTION_ID= t.ID join CUSTOMER_DETAILS cd on t.beneficiaryCutomerId=cd.ID "
					+ "join SHAREHOLDERS sh on cd.ID=sh.customerId " + "where t.BUSINESS_DATE>='" + fromDate
					+ "' and t.BUSINESS_DATE<='" + toDate
					+ "' group by t.beneficiaryCutomerId) as input order by input.amt desc";
			System.out.println(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> list = this.getCurrentSession().createNativeQuery(sql).setMaxResults(5).getResultList();
			for (Object[] objects : list) {
				CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
				corporateStructureDto.setCustomerId(Long.valueOf(objects[0].toString()));
				corporateStructureDto.setAlertAmount(Double.valueOf(objects[1].toString()));
				corporateStructureDto.setAlertCount(Long.valueOf(objects[2].toString()));
				corporateStructureDto.setType(objects[3].toString());
				corporateStructureDtoList.add(corporateStructureDto);
			}

		} else if ("Geography".equalsIgnoreCase(type)) {
			sql = " select input.bid,input.amt,input.cnt,input.typ from ( SELECT t.beneficiaryCutomerId as bid,sum(t.AMOUNT) as amt,count(a.ID) as cnt,cd.RESIDENT_COUNTRY as typ "
					+ "FROM ALERT a join TRANSACTIONS t on a.TRANSACTION_ID= t.ID join CUSTOMER_DETAILS cd on t.beneficiaryCutomerId=cd.ID "
					+ "where t.BUSINESS_DATE>='" + fromDate + "' and t.BUSINESS_DATE<='" + toDate
					+ "' and cd.RESIDENT_COUNTRY is not null "
					+ "group by t.beneficiaryCutomerId) as input order by input.amt desc";
			System.out.println(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> list = this.getCurrentSession().createNativeQuery(sql).setMaxResults(5).getResultList();
			for (Object[] objects : list) {
				CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
				corporateStructureDto.setCustomerId(Long.valueOf(objects[0].toString()));
				corporateStructureDto.setAlertAmount(Double.valueOf(objects[1].toString()));
				corporateStructureDto.setAlertCount(Long.valueOf(objects[2].toString()));
				corporateStructureDto.setType(objects[3].toString());
				corporateStructureDtoList.add(corporateStructureDto);
			}

		}
		return corporateStructureDtoList;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get top five alerts. Reason : " + e.getMessage());
		}

	}*/

	@Override
	public List<CorporateStructureDto> getTopFiveAlerts(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> corporateStructureDtoList = new ArrayList<CorporateStructureDto>();
		String sql = null;
		try{
		if ("CorporateStructure".equalsIgnoreCase(type)) {
			sql = " select input.bid,input.amt,input.cnt,input.typ from ( SELECT t.beneficiaryCutomerId as bid,sum(t.AMOUNT) as amt,count(a.ID) as cnt,cd.CORPORATE_STRUCTURE as typ "
					+ "FROM ALERT a join TRANSACTIONS t on a.TRANSACTION_ID= t.ID join CUSTOMER_DETAILS cd on t.beneficiaryCutomerId=cd.ID "
					+ " where t.BUSINESS_DATE>='" + fromDate + "' and t.BUSINESS_DATE<='" + toDate
					+ "' and cd.CORPORATE_STRUCTURE is not null "
					+ "group by t.beneficiaryCutomerId) as input order by input.amt desc";
			System.out.println(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> list = this.getCurrentSession().createNativeQuery(sql).setMaxResults(5).getResultList();
			for (Object[] objects : list) {
				CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
				corporateStructureDto.setCustomerId(Long.valueOf(objects[0].toString()));
				corporateStructureDto.setAlertAmount(Double.valueOf(objects[1].toString()));
				corporateStructureDto.setAlertCount(Long.valueOf(objects[2].toString()));
				corporateStructureDto.setType(objects[3].toString());
				corporateStructureDtoList.add(corporateStructureDto);
			}

		} else if ("Shareholders".equalsIgnoreCase(type)) {
			sql = " select input.bid,input.amt,input.cnt,input.typ from ( SELECT t.beneficiaryCutomerId as bid,sum(t.AMOUNT) as amt,count(a.ID) as cnt,cd.CORPORATE_STRUCTURE as typ "
					+ "FROM ALERT a join TRANSACTIONS t on a.TRANSACTION_ID= t.ID join CUSTOMER_DETAILS cd on t.beneficiaryCutomerId=cd.ID "
					+ "join SHAREHOLDERS sh on cd.ID=sh.customerId " + "where t.BUSINESS_DATE>='" + fromDate
					+ "' and t.BUSINESS_DATE<='" + toDate
					+ "' group by t.beneficiaryCutomerId) as input order by input.amt desc";
			//System.out.println(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> list = this.getCurrentSession().createNativeQuery(sql).setMaxResults(5).getResultList();
			for (Object[] objects : list) {
				CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
				corporateStructureDto.setCustomerId(Long.valueOf(objects[0].toString()));
				corporateStructureDto.setAlertAmount(Double.valueOf(objects[1].toString()));
				corporateStructureDto.setAlertCount(Long.valueOf(objects[2].toString()));
				corporateStructureDto.setType(objects[3].toString());
				corporateStructureDtoList.add(corporateStructureDto);
			}

		} else if ("Geography".equalsIgnoreCase(type)) {
			sql = " select input.bid,input.amt,input.cnt,input.typ from ( SELECT t.beneficiaryCutomerId as bid,sum(t.AMOUNT) as amt,count(a.ID) as cnt,cd.RESIDENT_COUNTRY as typ "
					+ "FROM ALERT a join TRANSACTIONS t on a.TRANSACTION_ID= t.ID join CUSTOMER_DETAILS cd on t.beneficiaryCutomerId=cd.ID "
					+ "where t.BUSINESS_DATE>='" + fromDate + "' and t.BUSINESS_DATE<='" + toDate
					+ "' and cd.RESIDENT_COUNTRY is not null "
					+ "group by t.beneficiaryCutomerId) as input order by input.amt desc";
			System.out.println(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> list = this.getCurrentSession().createNativeQuery(sql).setMaxResults(5).getResultList();
			for (Object[] objects : list) {
				CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
				corporateStructureDto.setCustomerId(Long.valueOf(objects[0].toString()));
				corporateStructureDto.setAlertAmount(Double.valueOf(objects[1].toString()));
				corporateStructureDto.setAlertCount(Long.valueOf(objects[2].toString()));
				corporateStructureDto.setType(objects[3].toString());
				corporateStructureDtoList.add(corporateStructureDto);
			}

		}
		return corporateStructureDtoList;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get top five alerts. Reason : " + e.getMessage());
		}

	}
	/*@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getGeographyAggregates(String fromDate, String toDate, List<String> scenarios) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" c.country,c.latitude,c.longitude,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Country c on cd.residentCountry=c.iso2Code ");
			hql.append(
					" and cd.residentCountry is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios) ");
			hql.append("group by c.country,c.latitude,c.longitude");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to geography aggregates. Reason : " + e.getMessage());
		}
	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getGeographyAggregates(String fromDate, String toDate, FilterDto filterDto) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<Object[]> listObjArray=new ArrayList<Object[]>();
		Double amount= 0.0;
		try {
			StringBuilder hql = new StringBuilder();
			/*hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" t.benificiaryCountry,t.latitude,t.longitude,Round(sum(t.amount),2),count(distinct t.alertTransactionId)) ");
			hql.append(
					" from TransactionMasterView t where t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			
			hql.append(" group by t.benificiaryCountry,t.latitude,t.longitude");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CorporateStructureDto>) query.getResultList();
			StringBuilder hql1 = new StringBuilder();
			hql1.append("select distinct Round(t.amount),t.alertTransactionId,t.benificiaryCorporateStructure from TransactionMasterView "
					+ "t where t.alertId is not null and t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate");
			hql1.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query1 = this.getCurrentSession().createQuery(hql.toString());
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			objArray = (List<Object[]>) query1.getResultList();
			for (Object[] object : objArray) {
				if(map.containsKey(object[2].toString())){
					CorporateStructureDto dto=map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount()+1);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[0].toString()));
				}else {
					CorporateStructureDto dto=new CorporateStructureDto(object[2].toString(),Double.valueOf(object[0].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, CorporateStructureDto> entry : map.entrySet()) {
	            list.add(entry.getValue());
	    }
			return list;
		
		
		*/
			
			Map<String,CorporateStructureDto> map=new HashMap<String,CorporateStructureDto>();
			hql.append("select distinct t.amount,t.alertTransactionId,t.benificiaryCountry,t.latitude,t.longitude from TransactionMasterView t");
			hql.append(" where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate and t.benificiaryCountry is not null ");
			if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		//	if(filterDto==null||((!filterDto.isAlertByStatusGreaterThan30Days())&&(!filterDto.isAlertsByPeriod10to20Days())&&(!filterDto.isAlertsByPeriod20to30Days())&&(!filterDto.isAlertsByPeriodgreaterthan30Days()))){
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			//}
			listObjArray = (List<Object[]>) query.getResultList();
			for (Object[] object : listObjArray) {
				if(map.containsKey(object[2].toString())){
					CorporateStructureDto dto=map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount()+1);
					amount=dto.getAlertAmount()+Double.valueOf(object[0].toString());
					dto.setAlertAmount(Math.round(amount*100.0)/100.0);
				}else {
					CorporateStructureDto dto=new CorporateStructureDto(object[2].toString(),object[3].toString(),object[4].toString(),Double.valueOf(object[0].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, CorporateStructureDto> entry : map.entrySet()) {
	            list.add(entry.getValue());
	    }
			 list.sort(Comparator.comparingDouble(CorporateStructureDto :: getAlertAmount).reversed());
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to geography aggregates. Reason : " + e.getMessage());
		}
	}

	/*@Override
	public CorporateStructureDto getCorporateStructure(String fromDate, String toDate, String type) {
		CorporateStructureDto count = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String condition = null;
		try {
			StringBuilder hql = new StringBuilder();
			condition = " and c.corporateStructure is not null ";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id)) ");
			hql.append("from Alert a Right Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c"
					+ " on t.beneficiaryCutomerId=c.id" + condition);
			hql.append(" and t.businessDate>=:fromDate and t.businessDate<=:toDate ");
			if (type != null)
				hql.append(" and c.corporateStructure=:type ");

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select sum(t.amount)");
			hql1.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql1.append(condition + " and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql1.append(" and c.corporateStructure=:type ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null)
				query.setParameter("type", type);

			count = (CorporateStructureDto) query.getSingleResult();
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			if (type != null)
				query1.setParameter("type", type);

			count = (CorporateStructureDto) query.getSingleResult();
			Double alertedAmount = (Double) query1.getSingleResult();
			count.setAlertAmount(alertedAmount);
			count.setType("CorporateStructure");
			return count;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get Corporate Structure. Reason : " + e.getMessage());
		}

	}*/
/////////////////////////used getCorporateStructure instead of above
	
	/*@SuppressWarnings("unchecked")
	@Override
	public List<String> getCorporateStructureByType(String fromDate, String toDate, String type,
			List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);

			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException(
					"Failed to get corporate Structure by type. Reason : " + e.getMessage());
		}
		return list;

	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCorporateStructureByType(String fromDate, String toDate, String type,
			List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);

			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException(
					"Failed to get corporate Structure by type. Reason : " + e.getMessage());
		}
		return list;

	}

	/*@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAggregates(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String groupByColumn = null;
		try {
			StringBuilder hql = new StringBuilder();
			if (type != null)
				groupByColumn = "a.scenario";
			else
				groupByColumn = "c.corporateStructure";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(groupByColumn + " ,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql.append(" and c.corporateStructure=:type ");
			hql.append("group by " + groupByColumn);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null && !type.isEmpty())
				query.setParameter("type", type);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get aggregates. Reason : " + e.getMessage());
		}

	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getAggregates(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String groupByColumn = null;
		try {
			StringBuilder hql = new StringBuilder();
			if (type != null)
				groupByColumn = "a.scenario";
			else
				groupByColumn = "c.corporateStructure";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(groupByColumn + " ,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(
					" and c.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql.append(" and c.corporateStructure=:type ");
			hql.append("group by " + groupByColumn);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null && !type.isEmpty())
				query.setParameter("type", type);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get aggregates. Reason : " + e.getMessage());
		}

	}

	// ok
	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getGeographyByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try {
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			queryBuilder.append(" c.id,c.displayName,a.alertBusinessDate,t.amount) ");
			queryBuilder.append(" from Alert a,TransactionsData t,CustomerDetails c,Country ct where a.transactionId=t.id and c.residentCountry=ct.iso2Code and ");
			queryBuilder.append(
					" t.beneficiaryCutomerId=c.id and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			queryBuilder.append("and a.scenario in (:scenarios) ");
			queryBuilder.append(" and ct.country=:type");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			query.setParameter("scenarios", scenarios);
			System.out.println(queryBuilder.toString());
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get geography by scenarios. Reason : " + e.getMessage());
		}
	}

	@Override
	public CorporateStructureDto getShareholder(String fromDate, String toDate, String type) {
		CorporateStructureDto count = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		// String condition = null;
		try {
			StringBuilder hql = new StringBuilder();
			// condition = " and cd.corporateStructure is not null ";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id)) ");
			hql.append("from Alert a Right Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd"
					+ " on t.beneficiaryCutomerId=cd.id");
			hql.append(" and t.businessDate>=:fromDate and t.businessDate<=:toDate ");
			if (type != null)
				hql.append(" and cd.corporateStructure=:type ");

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select sum(t.amount)");
			hql1.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql1.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql1.append(" and cd.corporateStructure=:type ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null)
				query.setParameter("type", type);

			count = (CorporateStructureDto) query.getSingleResult();
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			if (type != null)
				query1.setParameter("type", type);

			count = (CorporateStructureDto) query.getSingleResult();
			Double alertedAmount = (Double) query1.getSingleResult();
			count.setAlertAmount(alertedAmount);
			count.setType(type);
			return count;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Shareholders. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getShareholderAggregate(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String groupByColumn = null;
		try {
			StringBuilder hql = new StringBuilder();
			if (type != null)
				groupByColumn = "a.scenario";
			else
				groupByColumn = "cd.corporateStructure";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(groupByColumn + " ,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Shareholders sh on cd.id=sh.customerId ");
			hql.append(
					" and cd.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql.append(" and cd.corporateStructure=:type ");
			hql.append("group by " + groupByColumn);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null && !type.isEmpty())
				query.setParameter("type", type);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Shareholder aggregate. Reason : " + e.getMessage());
		}

	}

	@Override
	public CorporateStructureDto getGeography(String fromDate, String toDate, String type) {
		CorporateStructureDto count = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		// String condition = null;
		try {
			StringBuilder hql = new StringBuilder();
			// condition = " and cd.corporateStructure is not null ";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto( ");
			hql.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id)) ");
			hql.append("from Alert a Right Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd "
					+ " on t.beneficiaryCutomerId=cd.id Join Country ct on cd.residentCountry=ct.iso2Code ");
			hql.append(" and t.businessDate>=:fromDate and t.businessDate<=:toDate ");
			if (type != null)
				hql.append(" and ct.country=:type ");

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select sum(t.amount)");
			hql1.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Country ct on cd.residentCountry=ct.iso2Code ");
			hql1.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql1.append(" and ct.country=:type ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null)
				query.setParameter("type", type);

			count = (CorporateStructureDto) query.getSingleResult();
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			if (type != null)
				query1.setParameter("type", type);

			count = (CorporateStructureDto) query.getSingleResult();
			Double alertedAmount = (Double) query1.getSingleResult();
			count.setAlertAmount(alertedAmount);
			count.setType("Geography");
			return count;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Geography aggregates. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getGeographyAggregate(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String groupByColumn = null;
		try {
			StringBuilder hql = new StringBuilder();
			if (type != null)
				groupByColumn = "a.scenario";
			else
				groupByColumn = "cd.residentCountry";
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(groupByColumn + " ,sum(t.amount),count(a.id)) ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id Join Country c on cd.residentCountry=c.iso2Code ");
			hql.append(
					" and cd.corporateStructure is not null and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (type != null)
				hql.append(" and c.country=:type ");
			hql.append("group by " + groupByColumn);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (type != null && !type.isEmpty())
				query.setParameter("type", type);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Geography aggregate. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getGeographyByType(String fromDate, String toDate, String type, List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id Join Country ct on c.residentCountry=ct.iso2Code ");
			hql.append(
					" and ct.country=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);

			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException(
					"Failed to corporate Structure by type. Reason : " + e.getMessage());
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getShareholderByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<CorporateStructureDto> list = new ArrayList<CorporateStructureDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try {
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			queryBuilder.append(" c.id,c.displayName,a.alertBusinessDate,t.amount) ");
			queryBuilder.append(
					" from Alert a,TransactionsData t,CustomerDetails c,Shareholders sh where a.transactionId=t.id and c.id=sh.customerId and ");
			queryBuilder.append(
					" t.beneficiaryCutomerId=c.id and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			queryBuilder.append("and a.scenario in (:scenarios) ");
			queryBuilder.append(" and c.corporateStructure=:type");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			query.setParameter("scenarios", scenarios);
			list = (List<CorporateStructureDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get shareholdres by scenarios. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> getGroupByScenario(String fromDate, String toDate, FilterDto filterDto) {
		List<CorporateStructureDto> list=new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try{
			StringBuilder hql=new StringBuilder();
			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			hql.append(" t.alertScenario,sum(t.amount),count(distinct t.alertTransactionId))");
			hql.append(" from TransactionMasterView t");
			hql.append(" where DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			hql.append(" group by t.alertScenario");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			//if(filterDto==null||((!filterDto.isAlertByStatusGreaterThan30Days())&&(!filterDto.isAlertsByPeriod10to20Days())&&(!filterDto.isAlertsByPeriod20to30Days())&&(!filterDto.isAlertsByPeriodgreaterthan30Days()))){
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			//}
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			list=(List<CorporateStructureDto>) query.getResultList();			
		}catch(Exception e){
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get group by scenario. Reason : " + e.getMessage());
		}
		return list;
	}
	
	public String queryForAlertAmount(String type){
		StringBuilder hql=new StringBuilder();
		hql.append("select distinct round(t.amount,2),t.alertTransactionId from TransactionMasterView "
				+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
		if("CorporateStructure".equalsIgnoreCase(type))
			hql.append(" and t.benificiaryCorporateStructure is not null and t.benificiaryCustomerType='CORP' ");
	if("Shareholder".equalsIgnoreCase(type))
		hql.append(" and t.shareHolderName is not null ");
	if("Geography".equalsIgnoreCase(type))
		hql.append(" and t.benificiaryCountry is not null ");
		return hql.toString();
		
	}
	
	
	public String queryForTransactionAmount(String type){
		StringBuilder hql=new StringBuilder();				
		hql.append(" select distinct t.amount,t.id from TransactionMasterView t where t.id is not null"
				+ " and DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");	
		if("CorporateStructure".equalsIgnoreCase(type))
			hql.append(" and t.benificiaryCorporateStructure is not null and t.benificiaryCustomerType='CORP' ");
	if("Shareholder".equalsIgnoreCase(type))
		hql.append(" and t.shareHolderName is not null ");
	if("Geography".equalsIgnoreCase(type))
		hql.append(" and t.benificiaryCountry is not null ");
		return hql.toString();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public CorporateStructureDto getShareHolder(String fromDate, String toDate, FilterDto filterDto) {
		CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<Object[]> objArray=new ArrayList<>();
		List<Object[]> transactionArray=new ArrayList<>();

		Double alertAmount =0.0;
		Double transactionAmount=0.0;
		try {
			StringBuilder hql = new StringBuilder();
			StringBuilder hql1 = new StringBuilder();
			StringBuilder hql2 = new StringBuilder();
			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			hql.append("Round(sum(t.amount),2),count(distinct t.alertTransactionId),(count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id))");
			hql.append(" from TransactionMasterView t where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate "
					+ " and t.shareHolderName is not null ");
			if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			corporateStructureDto = (CorporateStructureDto) query.getSingleResult();
			//For alertAmount (hql1)
            hql1.append(queryForAlertAmount("Shareholder"));
            if(filterDto!=null)
            hql1.append(filterUtility.filterColumns(filterDto));
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query1, filterDto, fromDate, toDate);
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			objArray = (List<Object[]>) query1.getResultList();
			for (Object[] object : objArray) {
				alertAmount=alertAmount+Double.valueOf(object[0].toString());
			}
			hql2.append(queryForTransactionAmount("Shareholder"));
			hql2.append(filterUtility.filterColumns(filterDto));			
			Query<?> query2 = this.getCurrentSession().createQuery(hql2.toString());
			query2.setParameter("fromDate", formatter.parse(fromDate));
			query2.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!= null)
				filterUtility.queryReturn(query2, filterDto, fromDate, toDate);
			transactionArray =  (List<Object[]>) query2.getResultList();
			for (Object[] objects : transactionArray) {
				transactionAmount=transactionAmount+Double.valueOf(objects[0].toString());
			}
			corporateStructureDto.setAlertAmount(Math.round(alertAmount*100.0)/100.0);
			corporateStructureDto.setTransactionAmount(Math.round(transactionAmount*100.0)/100.0);
			return corporateStructureDto;
		} catch (NoResultException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public CorporateStructureDto getGeographyAlerts(String fromDate, String toDate, FilterDto filterDto) {
		CorporateStructureDto corporateStructureDto = new CorporateStructureDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<Object[]> objArray=new ArrayList<>();
		List<Object[]> transactionArray=new ArrayList<>();

		Double alertAmount =0.0;
		Double transactionAmount=0.0;
		try {
			StringBuilder hql = new StringBuilder();
			StringBuilder hql1 = new StringBuilder();
			StringBuilder hql2 = new StringBuilder();
			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
			hql.append("Round(sum(t.amount),2),count(distinct t.alertTransactionId),(count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id))");
			hql.append(" from TransactionMasterView t where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate and t.benificiaryCountry"
					+ " is not null ");
			if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			corporateStructureDto = (CorporateStructureDto) query.getSingleResult();
            hql1.append(queryForAlertAmount("Geography"));
            if(filterDto!=null)
            hql1.append(filterUtility.filterColumns(filterDto));
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());
			if(filterDto!=null)
			filterUtility.queryReturn(query1, filterDto, fromDate, toDate);
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			objArray = (List<Object[]>) query1.getResultList();
			for (Object[] object : objArray) {
				alertAmount=alertAmount+Double.valueOf(object[0].toString());
			}
			//For transaction amount (hql2)
			hql2.append(queryForTransactionAmount("Geography"));
			hql2.append(filterUtility.filterColumns(filterDto));			
			Query<?> query2 = this.getCurrentSession().createQuery(hql2.toString());
			query2.setParameter("fromDate", formatter.parse(fromDate));
			query2.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!= null)
				filterUtility.queryReturn(query2, filterDto, fromDate, toDate);
			transactionArray =  (List<Object[]>) query2.getResultList();
			for (Object[] objects : transactionArray) {
				transactionAmount=transactionAmount+Double.valueOf(objects[0].toString());
			}
			corporateStructureDto.setAlertAmount(Math.round(alertAmount*100.0)/100.0);
			corporateStructureDto.setTransactionAmount(Math.round(transactionAmount*100.0)/100.0);
			return corporateStructureDto;
		} catch (NoResultException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Corporate Structure. Reason : " + e.getMessage());
		}
	}

}
