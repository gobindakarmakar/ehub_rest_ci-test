package element.bst.elementexploration.rest.notification.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Notifications count")
public class NotificationCountDto {
	
	@ApiModelProperty("Notification count")
	private Integer notificationCount;
	
	@ApiModelProperty("Notification last check in time")
	private String lastCheckInTimestamp;

	public NotificationCountDto() {
		super();
	}

	public Integer getNotificationCount() {
		return notificationCount;
	}

	public void setNotificationCount(Integer notificationCount) {
		this.notificationCount = notificationCount;
	}

	public String getLastCheckInTimestamp() {
		return lastCheckInTimestamp;
	}

	public void setLastCheckInTimestamp(String lastCheckInTimestamp) {
		this.lastCheckInTimestamp = lastCheckInTimestamp;
	}

}
