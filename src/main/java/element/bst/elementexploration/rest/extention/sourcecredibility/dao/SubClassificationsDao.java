package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SubClassificationsDao extends GenericDao<SubClassifications, Long> {

	 SubClassifications fetchSubClassification(String subClasssifcationName);
	
}
