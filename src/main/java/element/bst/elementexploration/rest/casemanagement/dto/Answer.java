package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Answer")
public class Answer implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "ID of the question",required=true)
	private String questionId;
	@ApiModelProperty(value = "Title of the question",required=true)
	private String title;
	@ApiModelProperty(value = "Response of the question",required=true)
	private String response;
	@ApiModelProperty(value = "Risk of the question",required=true)
	private int risk;
	public Answer() {
		super();		
	}
	public Answer(String questionId, String title, String response) {
		super();
		this.questionId = questionId;
		this.title = title;
		this.response = response;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getRisk() {
		return risk;
	}
	public void setRisk(int risk) {
		this.risk = risk;
	}
	@Override
	public String toString() {
		return "Answer [questionId=" + questionId + ", title=" + title + ", response=" + response + ", risk=" + risk
				+ "]";
	}
}
