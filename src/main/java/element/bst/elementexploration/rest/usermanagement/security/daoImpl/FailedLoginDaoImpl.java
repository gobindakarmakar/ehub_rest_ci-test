package element.bst.elementexploration.rest.usermanagement.security.daoImpl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.FailedLoginDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.FailedLogin;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Amit Patel
 *
 */
@Repository("failedLoginDao")
public class FailedLoginDaoImpl extends GenericDaoImpl<FailedLogin, Long> implements FailedLoginDao{

	public FailedLoginDaoImpl() {
        super(FailedLogin.class);
    }

	@Override
	public FailedLogin getFailedLoginByUsername(String username) {
		FailedLogin failedLogin = null;
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<FailedLogin> query = builder.createQuery(FailedLogin.class);
			Root<FailedLogin> login = query.from(FailedLogin.class);
			query.select(login);
			query.where(builder.or(builder.equal(login.get("username"), username), builder.equal(login.get("screenName"), username)));
			List<FailedLogin> failedLogins = this.getCurrentSession().createQuery(query).getResultList();
			if(failedLogins != null && !failedLogins.isEmpty()){
				failedLogin =  failedLogins.get(0);
			}
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, FailedLoginDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return failedLogin;
	}

}
