package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.RoleGroupDto;

/**
 * @author Paul Jalagari
 *
 */
public interface RoleGroupService extends GenericService<RoleGroup, Long> {

	RoleGroup saveOrUpdateRoleGroup(RoleGroupDto groupPermissiondto, Long currentUserId);

	RoleGroup saveOrUpdateRoleGroup(Roles role, Groups group, Long roleGroupId, Long currentUserId) throws Exception;

	List<RoleGroup> getRolesByGroup(List<Long> groupIds);

	void initializeGroups(List<RoleGroup> filteredList);
	
	public boolean checkRoleExistsForGroup(Long groupId, Long roleId);
	
	public boolean deleteExistingRolesForGroup(Long groupId,Long currentUserId)throws Exception;

}
