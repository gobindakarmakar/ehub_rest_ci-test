package element.bst.elementexploration.test.document.daoImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.document.daoImpl.DocumentDaoImpl;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@InjectMocks
	DocumentDaoImpl docuemntDao;

	public List<DocumentVaultDTO> documentVaultDtoList = new ArrayList<DocumentVaultDTO>();
	public List<DocumentVault> documentVaultList = new ArrayList<DocumentVault>();

	@Before
	public void doSetup() {
		documentVaultDtoList.add(new DocumentVaultDTO(1L, "title", "test", 2L, "pdf", new Date(), 1L, 1L, new Date(),
				"test", "analyst"));
		documentVaultList.add(new DocumentVault(1L, "test", "unit test", "code testing", "pdf", 2L, new Date()));
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void isDocShareableTest() {
		List<BigInteger> list = new ArrayList<BigInteger>();
		list.add(new BigInteger("2"));
		list.add(new BigInteger("1"));
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(list);
		boolean status = docuemntDao.isDocShareable(1L, 1L);
		assertEquals(true, status);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void isDocShareable_whenExceptionTest() {
		List<BigInteger> list = new ArrayList<BigInteger>();
		list.add(new BigInteger("2"));
		list.add(new BigInteger("1"));
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(HibernateException.class);
		boolean status = docuemntDao.isDocShareable(1L, 1L);
		assertEquals(false, status);
	}

	@Test
	public void myDocumentsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.myDocuments(1L, 1L, 3, 1, 11, "title", "desc","",null,false);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@Test
	public void myDocuments_whenOrderByAndOrderInIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.myDocuments(1L, 1L, 3, 1, 11, null, null,"",null,false);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void myDocuments_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.myDocuments(1L, 1L, 3, 1, 11, null, null,"",null,false);
		assertEquals(0, documentVaultDtoListNew.size());
	}

	@Test
	public void countMyDocumentsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3l);
		long count = docuemntDao.countMyDocuments(1L, 1L, 3,"",null);
		assertEquals(3l, count);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countMyDocuments_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		long count = docuemntDao.countMyDocuments(1L, 1L, 3,"",null);
		assertEquals(0l, count);
	}

	@Test
	public void fullTextSearchMyDocumentsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.fullTextSearchMyDocuments(1L, 1L, "test", 3, 1, 11,
				"title", "desc","",null);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@Test
	public void fullTextSearchMyDocuments_whenOrderByAndOrderInIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.fullTextSearchMyDocuments(1L, 1L, "test", 3, 1, 11,
				null, null,"",null);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fullTextSearchMyDocuments_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.fullTextSearchMyDocuments(1L, 1L, "test", 3, 1, 11,
				"title", "desc","",null);
		assertEquals(0, documentVaultDtoListNew.size());
	}

	@Test
	public void countFullTextSearchMyDocumentsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3l);
		long count = docuemntDao.countFullTextSearchMyDocuments(1L, 1L, "test", 3,"",null);
		assertEquals(3l, count);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countFullTextSearchMyDocuments_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		long count = docuemntDao.countFullTextSearchMyDocuments(1L, 1L, "test", 3,"",null);
		assertEquals(0l, count);
	}

	@Test
	public void createHqlQueryForMyDocumentCountTest() {
		String query = docuemntDao.createHqlQueryForMyDocuments(1L,"",null);
		Assert.assertNotNull(query);
	}

	@Test
	public void createHqlQueryForMyDocumentCount_whenCaseIdNullTest() {
		String query = docuemntDao.createHqlQueryForMyDocuments(null,"",null);
		Assert.assertNotNull(query);
	}

	@Test
	public void createHqlQueryForMyDocumentsTest() {
		String query = docuemntDao.createHqlQueryForMyDocuments(1L,"",null);
		Assert.assertNotNull(query);
	}

	@Test
	public void createHqlQueryForMyDocuments_whenCaseIdNullTest() {
		String query = docuemntDao.createHqlQueryForMyDocuments(null,"",null);
		Assert.assertNotNull(query);
	}

	@Test
	public void getAllMySharedDocumentTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.getAllMySharedDocument(1L, 1L, 1, 11, "title",
				"desc", 10);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAllMySharedDocument_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.getAllMySharedDocument(1L, 1L, 1, 11, "title",
				"desc", 10);
		assertEquals(0, documentVaultDtoListNew.size());
	}

	@Test
	public void countGetAllMySharedDocumentTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3l);
		long count = docuemntDao.countGetAllMySharedDocument(1L, 1L, 3);
		assertEquals(3l, count);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countGetAllMySharedDocument_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		long count = docuemntDao.countGetAllMySharedDocument(1L, 1L, 3);
		assertEquals(0L, count);
	}

	@Test
	public void fullTextSearchDocumentTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.fullTextSearchDocument(1L, "test", 1, 10, "title",
				"desc", 1L, 8);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@Test
	public void fullTextSearchDocument_whenOrderByAndOrderInIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultDtoList);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.fullTextSearchDocument(1L, "test", 1, 10, null,
				null, 1L, 8);
		assertEquals(1, documentVaultDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fullTextSearchDocument_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<DocumentVaultDTO> documentVaultDtoListNew = docuemntDao.fullTextSearchDocument(1L, "test", 1, 10, "title",
				"desc", 1L, 8);
		assertEquals(0, documentVaultDtoListNew.size());
	}

	@Test
	public void countFullTextSearchDocumentTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3l);
		long count = docuemntDao.countFullTextSearchDocument(1L, "test", 1L, 3);
		assertEquals(3l, count);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countFullTextSearchDocument_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		long count = docuemntDao.countFullTextSearchDocument(1L, "test", 1L, 3);
		assertEquals(0L, count);
	}

	@Test
	public void getAllMySharedDocumentyTest() {
		String query = docuemntDao.getAllMySharedDocument(1L);
		Assert.assertNotNull(query);
	}

	@Test
	public void getAllMySharedDocument_whenCaseIdNullTest() {
		String query = docuemntDao.getAllMySharedDocument(null);
		Assert.assertNotNull(query);
	}

	@Test
	public void getAllMySharedDocumentCountTest() {
		String query = docuemntDao.getAllMySharedDocumentCount(1L);
		Assert.assertNotNull(query);
	}

	@Test
	public void getAllMySharedDocumentCount_whenCaseIdNullTest() {
		String query = docuemntDao.getAllMySharedDocumentCount(null);
		Assert.assertNotNull(query);
	}

	@Test
	public void isDocCommentableTest() {
		List<BigInteger> list = new ArrayList<BigInteger>();
		list.add(new BigInteger("2"));
		list.add(new BigInteger("1"));
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(list);
		boolean status = docuemntDao.isDocCommentable(1L, 1L);
		assertEquals(true, status);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void isDocCommentable_whenExceptionTest() {
		List<BigInteger> list = new ArrayList<BigInteger>();
		list.add(new BigInteger("2"));
		list.add(new BigInteger("1"));
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(HibernateException.class);
		boolean status = docuemntDao.isDocCommentable(1L, 1L);
		assertEquals(false, status);
	}

	@Test
	public void searchUnderwritingDocumentsIdsByNameTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentVaultList);
		List<Long> docIds = docuemntDao.searchUnderwritingDocumentsIdsByName("test");
		assertEquals(1, docIds.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void searchUnderwritingDocumentsIdsByName_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		docuemntDao.searchUnderwritingDocumentsIdsByName("test");
	}
}
