package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentContents;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentContentsDao extends GenericDao<DocumentContents, Long>{

	public List<DocumentContents> getDocumentContentById(Long docId);
	
	public List<DocumentContents> getDocumentContentByParentId(Long parentId);

	public List<DocumentContents> getParagraphsAndSubParagraphsByTemplateId(Long templateId);
	
	public List<DocumentContents> getTablesByTemplateId(Long templateId);
	
}
