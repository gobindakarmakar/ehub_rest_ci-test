package element.bst.elementexploration.rest.extention.sourcecredibility.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_jurisdiction")
public class SourceJurisdiction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long jurisdictionId;

	@Column(name = " jurisdictionName")
	private String jurisdictionName;

	@Column(name = "jurisdictionOriginalName")
	private String jurisdictionOriginalName;

	@Column(name = " selected")
	private Boolean selected;

	@Column(name = " listItemId")
	private Long listItemId;
	
	public SourceJurisdiction() {
	}

	public SourceJurisdiction(String jurisdictionName) {
		super();
		this.jurisdictionName = jurisdictionName;
	}

	public SourceJurisdiction(String jurisdictionName, Boolean selected) {
		super();
		this.jurisdictionName = jurisdictionName;
		this.selected = selected;
	}

	public SourceJurisdiction(String jurisdictionName, String jurisdictionOriginalName, Boolean selected) {
		super();
		this.jurisdictionName = jurisdictionName;
		this.jurisdictionOriginalName = jurisdictionOriginalName;
		this.selected = selected;
	}

	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	public String getJurisdictionName() {
		return jurisdictionName;
	}

	public void setJurisdictionName(String jurisdictionName) {
		this.jurisdictionName = jurisdictionName;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getJurisdictionOriginalName() {
		return jurisdictionOriginalName;
	}

	public void setJurisdictionOriginalName(String jurisdictionOriginalName) {
		this.jurisdictionOriginalName = jurisdictionOriginalName;
	}

	public Long getListItemId() {
		return listItemId;
	}

	public void setListItemId(Long listItemId) {
		this.listItemId = listItemId;
	}

}
