package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.util.List;

public class AlertMetaDataDto {
	Double confidence;
	String value;
	String record_information;
	List<EntriesDto> entries;
	public Double getConfidence() {
		return confidence;
	}
	public void setConfidence(Double confidence) {
		this.confidence = confidence;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRecord_information() {
		return record_information;
	}
	public void setRecord_information(String record_information) {
		this.record_information = record_information;
	}
	public List<EntriesDto> getEntries() {
		return entries;
	}
	public void setEntries(List<EntriesDto> entries) {
		this.entries = entries;
	}
	
	

}
