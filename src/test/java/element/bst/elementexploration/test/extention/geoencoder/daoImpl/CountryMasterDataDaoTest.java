package element.bst.elementexploration.test.extention.geoencoder.daoImpl;

import static org.junit.Assert.assertEquals;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.geoencoder.daoImpl.CountryMasterDataDaoImpl;
import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;

@RunWith(MockitoJUnitRunner.class)
public class CountryMasterDataDaoTest {
	
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@InjectMocks
	CountryMasterDataDaoImpl countryMasterDataDaoImpl;
	
	public CountryMasterData countryMasterData;
	
	@Before
	public void setupMock() {
		countryMasterData = new CountryMasterData(1L, "india", "latitude", "longitude", "analyst");
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void findCountryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(countryMasterData);
		CountryMasterData newResult = countryMasterDataDaoImpl.findCountry("india");
		assertEquals(countryMasterData.getUserId(), newResult.getUserId());
	}
	
	@Test
	public void findCountry_whenNoResultExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(null);
		CountryMasterData newResult = countryMasterDataDaoImpl.findCountry("india");
		assertEquals(null, newResult);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void findCountry_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		countryMasterDataDaoImpl.findCountry("india");
	}

}
