package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionNode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("base_amount")
	private double amount;
	
	private Date date;
	
	@JsonProperty("search-name")
	private String searchName;
	
	private String id;
	
	private String type;
	
	private String channel;
	
	private String location;
	
	private String direction;
	
	@JsonProperty("main_party")
	private String mainParty;
	
	@JsonProperty("activity_currency")
	private String currency;
	
	@JsonProperty("activity_amount")
	private double activityAmount;

	public TransactionNode(double amount, Date date, String searchName, String id, String type,
			String channel, String location, String direction, String mainParty, String currency,
			double activityAmount) {
		super();
		this.amount = amount;
		this.date = date;
		this.searchName = searchName;
		this.id = id;
		this.type = type;
		this.channel = channel;
		this.location = location;
		this.direction = direction;
		this.mainParty = mainParty;
		this.currency = currency;
		this.activityAmount = activityAmount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getMainParty() {
		return mainParty;
	}

	public void setMainParty(String mainParty) {
		this.mainParty = mainParty;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getActivityAmount() {
		return activityAmount;
	}

	public void setActivityAmount(double activityAmount) {
		this.activityAmount = activityAmount;
	}
}
