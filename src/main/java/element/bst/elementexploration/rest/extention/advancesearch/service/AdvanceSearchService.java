package element.bst.elementexploration.rest.extention.advancesearch.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import element.bst.elementexploration.rest.extention.advancesearch.dto.CustomOfficer;
import element.bst.elementexploration.rest.extention.advancesearch.dto.CustomShareHolder;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityListDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityScreeningDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.IdentifiersRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ProfileDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ShareHolderRequestDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;

/**
 * 
 * @author Viswanath Reddy G
 *
 */
public interface AdvanceSearchService {

	String getNewSearchResults(String newsSearchRequest) throws Exception;

	String getEntityByName(String name) throws Exception;

	String getHierarchyData(HierarchyDto hierarchyDto, Long userId, String apiKey) throws Exception;

	// String getOrgParentData(EntityRequestDto input)throws Exception;

	EntityListDataDto getOrgDataOfEntity(EntityRequestDto entityRequestDto) throws Exception;

	List<ProfileDataDto> getProfileData(EntityRequestDto entityRequestDto) throws Exception;

	List<ShareHolderRequestDto> getShareHoldersData(List<ShareHolderRequestDto> jsonString) throws Exception;

	List<IdentifiersRequestDto> getOrgData(CopyOnWriteArrayList<IdentifiersRequestDto> identifiers);

	String getShareHoloders(String organizationId, Integer level, String fields)
			throws UnsupportedEncodingException, Exception;

	Boolean ownershipStructure(String identifier, HierarchyDto jsonString, Integer maxlevel, Integer lowRange,
			Integer highRange, String path, Integer noOfSubsidiaries, Integer shareholder_level,
			String organisationName, String juridiction, Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate,boolean isRollBack, List<SourcesDto> sourcesDtos, String source,String sourceType) throws JSONException, Exception;

	String readCorporateStructure(String path) throws JSONException, Exception;

	boolean deleteFiles(String path) throws IOException;

	String getMultisourceData(String query, String jurisdiction, String website, Long userId) throws Exception;

	String fetchLinkData(String identifier, String jurisdiction, String fields, String graph, Long userId,String sourceType) throws Exception;

	String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level,String clientId,String sourceType)
			throws UnsupportedEncodingException;

	String getEntityScreening(String organizationName, String country, String entityType, String website, String dob, String identifier, String requestId)
			throws Exception;

	String getAnnualReturnsLink(String documentsLink) throws Exception;

	String selectEntityUrl(String selectEntityUrl) throws Exception;

	void getMergedResults(JSONArray mergedPeps, JSONArray peps,String mainIdentifier,boolean isPep);

	String getCountriesOfOperation(String identifier, JSONObject mainJsonObject, String mainJurisdiction, String mainCountry, Map<String, SourcesDto> map) throws Exception;

	String getEntitisScreening(String mainEntityId, String requestId)
			throws Exception;

	String getRequestIdOfScreening(List<EntityScreeningDto> entityScreeningDtos, String mainEntityId, String startDate,
			String endDate) throws Exception;

	String addCustomShareHolder(List<CustomShareHolder> customShareHolder, String mainIdentifier, Long userId, Boolean isSubsidiare) throws Exception;

	String deleteCustomEntity(String identifier, String field)throws Exception;

	String getScreenShotData(String entityId,String sourceName,String url) throws Exception;

	String customKeyManager(CustomOfficer customOfficer, Long currentUserId) throws Exception;

	public JSONObject getKeyManagementSource(JSONArray sources);

	String getScreeningStatus() throws Exception;
	
	String getCompanyVLAData(String identifier, String requestId) throws Exception;

	String selectEntity(String identifier, String requestId, String sourceType) throws Exception;
}
