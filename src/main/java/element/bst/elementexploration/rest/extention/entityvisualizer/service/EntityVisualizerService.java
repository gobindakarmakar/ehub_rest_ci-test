package element.bst.elementexploration.rest.extention.entityvisualizer.service;

public interface EntityVisualizerService {

	String getEntData(String payload) throws Exception;

}
