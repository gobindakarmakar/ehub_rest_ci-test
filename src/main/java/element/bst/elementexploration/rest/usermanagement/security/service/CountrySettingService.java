package element.bst.elementexploration.rest.usermanagement.security.service;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;
/**
 * 
 * @author Amit Patel
 *
 */
public interface CountrySettingService extends GenericService<CountrySetting, Long>{
	
	CountrySetting getCountrySetting(String countryIso);

}
