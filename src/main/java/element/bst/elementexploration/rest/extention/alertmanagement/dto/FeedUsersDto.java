package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import io.swagger.annotations.ApiModel;

@ApiModel("Feed Users")
public class FeedUsersDto implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Long feedId;
	
	private List<UsersDto> feedUsers;

	public Long getFeedId() {
		return feedId;
	}

	public void setFeedId(Long feedId) {
		this.feedId = feedId;
	}

	public List<UsersDto> getFeedUsers() {
		return feedUsers;
	}

	public void setFeedUsers(List<UsersDto> feedUsers) {
		this.feedUsers = feedUsers;
	}
	
	

}
