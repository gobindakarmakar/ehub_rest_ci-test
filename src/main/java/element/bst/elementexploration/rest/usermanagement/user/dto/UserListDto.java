package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
public class UserListDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Users list")
	private List<UsersDto> result;

	@ApiModelProperty("Page information")
	private PaginationInformation paginationInformation;

	public List<UsersDto> getResult() {
		return result;
	}

	public void setResult(List<UsersDto> result) {
		this.result = result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}

}
