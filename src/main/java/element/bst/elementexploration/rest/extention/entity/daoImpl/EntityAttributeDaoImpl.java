package element.bst.elementexploration.rest.extention.entity.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.entity.dao.EntityAttributeDao;
import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("entityAttributeDao")
public class EntityAttributeDaoImpl extends GenericDaoImpl<EntityAttributes, Long> implements EntityAttributeDao {

	public EntityAttributeDaoImpl() {
		super(EntityAttributes.class);
	}

	@Override
	public EntityAttributes get(String entityId, String sourceSchema) {
		EntityAttributes entityAttributes = null;
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select e from EntityAttributes e where e.entityId=:entityId ");
			if(sourceSchema!=null)
				queryBuilder.append(" and e.sourceSchema=:sourceSchema");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("entityId", entityId);
			if(sourceSchema!=null)
				query.setParameter("sourceSchema", sourceSchema);
			entityAttributes = (EntityAttributes) query.getSingleResult();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return entityAttributes;
	}
	
	@Override
	public List<EntityAttributes> getAttributeList(String entityId, String sourceSchema) {
		List<EntityAttributes> entityAttributes = new ArrayList<EntityAttributes>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select e from EntityAttributes e where e.entityId=:entityId ");
			if(sourceSchema!=null)
				queryBuilder.append(" and e.sourceSchema=:sourceSchema");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("entityId", entityId);
			if(sourceSchema!=null)
				query.setParameter("sourceSchema", sourceSchema);
			entityAttributes = (List<EntityAttributes>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return entityAttributes;
	}

}
