package element.bst.elementexploration.rest.extention.gridview.serviceImpl;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.extention.gridview.dao.GridViewDao;
import element.bst.elementexploration.rest.extention.gridview.domain.GridView;
import element.bst.elementexploration.rest.extention.gridview.dto.GridViewDto;
import element.bst.elementexploration.rest.extention.gridview.service.GridviewService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.group.dto.DefaultPermissionsDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.ListPermissionsDtoWithParent;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author Jalagari Paul
 *
 */
@Service("gridViewService")
@Transactional("transactionManager")
public class GridViewServiceImpl extends GenericServiceImpl<GridView, Long> implements GridviewService {

	@Autowired
	GridViewDao gridViewDao;

	@Autowired
	GridviewService gridviewService;


	public GridViewServiceImpl() {

	}

	@Autowired
	public GridViewServiceImpl(GenericDao<GridView, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public boolean saveOrUpdateGridView(GridViewDto gridViewDto, Long userId) throws Exception {
		List<GridView> gridViewsListExisting = gridViewDao.findAll();
		List<GridView> gridViewsList = new ArrayList<>();
		if (gridViewDto != null) {
			if (gridViewDto.isDefaultView()) {
				boolean updated = false;
				if (gridViewDto.getGridViewId() != null) {
					// old grid
					GridView existingView = gridViewDao.find(gridViewDto.getGridViewId());
					if (existingView != null) {
						existingView.setCreatedBy(userId);
						if (gridViewDto.isDefaultView())
							existingView.setDefaultView(gridViewDto.isDefaultView());
						if (gridViewDto.getGridTableName() != null)
							existingView.setGridTableName(gridViewDto.getGridTableName());
						if (gridViewDto.getGridViewMetaData() != null)
							existingView.setGridViewMetaData(gridViewDto.getGridViewMetaData());
						if (gridViewDto.getGridViewName() != null)
							existingView.setGridViewName(gridViewDto.getGridViewName());
						gridViewDao.saveOrUpdate(existingView);
						updated = true;
					}

				} else {
					// new grid
					GridView gridView = new GridView();
					BeanUtils.copyProperties(gridViewDto, gridView);
					gridView.setCreatedBy(userId);
					gridViewDao.saveOrUpdate(gridView);
					updated = true;
				}
				// for both save and update make others default view false
				if (updated) {
					if (gridViewsListExisting != null && gridViewsListExisting.size() > 0) {
						gridViewsList = gridViewsListExisting.stream()
								.filter(grid -> (!grid.getGridViewId().equals(gridViewDto.getGridViewId())))
								.filter(grid -> grid.isDefaultView())
								.filter(grid -> grid.getCreatedBy().equals(userId))
								.filter(grid -> grid.getGridTableName().equals(gridViewDto.getGridTableName()))
								.collect(Collectors.toList());
					}
					if (gridViewsList != null && gridViewsList.size() > 0) {
						for (GridView gridView : gridViewsList) {
							gridView.setDefaultView(false);
							gridViewDao.saveOrUpdate(gridView);
						}
					}
					return true;
				}
			} else {
				if (gridViewDto.getGridViewId() != null) {
					GridView existingView = gridViewDao.find(gridViewDto.getGridViewId());
					if (existingView != null) {
						existingView.setCreatedBy(userId);
						existingView.setDefaultView(gridViewDto.isDefaultView());
						if (gridViewDto.getGridTableName() != null)
							existingView.setGridTableName(gridViewDto.getGridTableName());
						if (gridViewDto.getGridViewMetaData() != null)
							existingView.setGridViewMetaData(gridViewDto.getGridViewMetaData());
						if (gridViewDto.getGridViewName() != null)
							existingView.setGridViewName(gridViewDto.getGridViewName());
						gridViewDao.saveOrUpdate(existingView);
						return true;
					}
				} else {
					GridView gridView = new GridView();
					BeanUtils.copyProperties(gridViewDto, gridView);
					gridView.setCreatedBy(userId);
					gridViewDao.saveOrUpdate(gridView);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<GridViewDto> getAllGridViewsByUser(Long userId, String tableName) throws Exception {
		List<GridViewDto> gridViewDtos = new ArrayList<>();
		List<GridView> gridViewsList = gridViewDao.findAll();
		List<GridView> gridViewsListByUser = new ArrayList<>();
		try {
			if (gridViewsList != null) {
				gridViewsListByUser = gridViewsList.stream().filter(gridView -> gridView.getCreatedBy().equals(userId))
						.filter(gridView -> gridView.getGridTableName().equals(tableName)).collect(Collectors.toList());
				if (gridViewsListByUser != null) {
					gridViewDtos = gridViewsListByUser.stream()
							.map((gridViewDto) -> new EntityConverter<GridView, GridViewDto>(GridView.class,
									GridViewDto.class).toT2(gridViewDto, new GridViewDto()))
							.collect(Collectors.toList());
				}
			}
			return gridViewDtos;
		} catch (Exception e) {
			e.printStackTrace();
			return gridViewDtos;
		}

	}

	@Override
	public boolean deleteGridView(Long gridViewId, Long userId) throws Exception {
		if (gridViewId != null) {
			return gridViewDao.deleteGridView(gridViewId, userId);
		}
		return false;
	}

	@Override
	public List<GridView> findGridViewByNameAndUser(String gridViewName,String tableName, Long userId) {
		List<GridView> gridViewsList = gridViewDao.findAll();
		List<GridView> gridViews = new ArrayList<>();
		gridViews = gridViewsList.stream().filter(gridView -> gridView.getCreatedBy().equals(userId))
				.filter(gridView -> gridView.getGridViewName().equals(gridViewName)).collect(Collectors.toList());
		gridViews=gridViews.stream().filter(view -> view.getGridTableName().equalsIgnoreCase(tableName)).collect(Collectors.toList());
		return gridViews;
	}

	@Override
	public List<GridView> findGridViewById(Long gridViewId) {
		List<GridView> gridViewsList = gridViewDao.findAll();
		List<GridView> gridViews = new ArrayList<>();
		gridViews = gridViewsList.stream().filter(gridView -> gridView.getGridViewId().equals(gridViewId))
				.collect(Collectors.toList());
		return gridViews;
	}

	@Override
	public void loadDefaultGridView(String gridViewFileName , Long userId) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource(gridViewFileName);
		File file = new File(resource.toURI());
		GridViewDto jsonRead = mapper.readValue(file, GridViewDto.class);
		if (jsonRead != null) {
			List<GridView> gridViewsList = gridViewDao.findAll();
			gridViewsList.stream().filter(g -> g.getGridViewName().equals("default")).collect(Collectors.toList());
			if (gridViewsList == null || gridViewsList.size() == 0) {
				if (jsonRead != null) {
					GridView gridView = new GridView();
					gridView.setCreatedBy(userId);
					gridView.setDefaultView(jsonRead.isDefaultView());
					gridView.setGridTableName(jsonRead.getGridTableName());
					gridView.setGridViewMetaData(jsonRead.getGridViewMetaData());
					gridView.setGridViewName(jsonRead.getGridViewName());
					save(gridView);
				}

			}
		}
	}
}
