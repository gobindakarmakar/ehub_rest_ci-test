package element.bst.elementexploration.test.contacts.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.contacts.dao.ContactsDao;
import element.bst.elementexploration.rest.contacts.domain.Contact;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.contacts.serviceImpl.ContactsServiceImpl;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;

@RunWith(MockitoJUnitRunner.class)
public class ContactsServiceTest {
	
	@Mock
	ContactsDao contactsDaoMock;
	
	@Mock
	UserDao userDaoMock;
	
	@InjectMocks
	ContactsServiceImpl contactsServiceImpl;
	
	public List<ContactDTO> contactDtoList;
	
	public User user;
	
	public Contact contact;
	
	@Before
    public void setupMock() {
		contactDtoList = new ArrayList<ContactDTO>();
		ContactDTO contact1 = new ContactDTO(1L,"admin","admin","admin@test.com");
		contactDtoList.add(contact1);
		ContactDTO contact2 = new ContactDTO(1L,"analyst","analyst","analyst@test.com");
		contactDtoList.add(contact2);
		user = new User("ehub", "Junit", "Mockito", new Date(), "status");
		contact = new Contact(1L, 2L, 3L);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getUserContactListTest(){
		Mockito.when(contactsDaoMock.getUserContactList(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(contactDtoList);
		List<ContactDTO> contactList = contactsServiceImpl.getUserContactList(2L, 5 , 5 , "JUnit", "Mockito", "test cases");
		assertEquals(2,contactList.size());
	}
	
	@Test
	public void getUserContactList_WhenRecordsPerPageIsNullTest(){
		Mockito.when(contactsDaoMock.getUserContactList(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(contactDtoList);
		List<ContactDTO> contactList = contactsServiceImpl.getUserContactList(2L, 5 , 5 , "JUnit", "Mockito", "test cases");
		assertEquals(2,contactList.size());
	}
	
	@Test
	public void removeContactTest() {
		Mockito.doNothing().when(contactsDaoMock).removeContact(Mockito.anyLong(), Mockito.anyLong());
		contactsServiceImpl.removeContact(2L, 2L);
	}
	
	@Test
	public void addContactTest() {
		contact = null;
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(contactsDaoMock.getUserContact(Mockito.anyLong(), Mockito.anyLong())).thenReturn(null);
		Mockito.when(contactsDaoMock.create(Mockito.any(Contact.class))).thenReturn(contact);
		boolean status = contactsServiceImpl.addContact(1L, 2L);
		assertEquals(true, status);
	}
	
	@Test
	public void addContact_whenGetUserContactIsNull() {
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(contactsDaoMock.getUserContact(Mockito.anyLong(), Mockito.anyLong())).thenReturn(contact);
		boolean status = contactsServiceImpl.addContact(1L, 2L);
		assertEquals(false, status);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = NoDataFoundException.class)
	public void addContactTest_ThrowsExceptionWhenUserIsNullTest(){
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenThrow(NoDataFoundException.class);
		contactsServiceImpl.addContact(1L, 2L);
	}
	
	@Test
	public void countUserContactListTest(){
		Mockito.when(contactsDaoMock.countUserContactList(Mockito.anyLong(),Mockito.anyString())).thenReturn(2L);
		long count = contactsServiceImpl.countUserContactList(2L, "Junit");
		assertEquals(2L,count);
	}
}
