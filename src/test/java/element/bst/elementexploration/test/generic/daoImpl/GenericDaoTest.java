package element.bst.elementexploration.test.generic.daoImpl;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author ramababu
 *
 */
public class GenericDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<Object> root;

	@InjectMocks
	GenericDaoImpl<Object, Long> genericDao = new GenericDaoImpl<Object, Long>(Object.class);

	List<Object> objectList = new ArrayList<Object>();

	Object object = new Object();

	@Before
	public void setupMock() {
		objectList.add(new Object());
		objectList.add(new Object());
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getCurrentSessionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Session resultSession = genericDao.getCurrentSession();
		Assert.assertNotNull(resultSession);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findAllTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Object.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<Object> objectListNew = genericDao.findAll();
		assertEquals(2, objectListNew.size());
	}

	@Test
	public void findTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.get(Object.class, Serializable.class)).thenReturn(object);
		Object obj = genericDao.find(1L);
		assertEquals(null, obj);
	}

	@Test
	public void createTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.save(Mockito.any(Object.class))).thenReturn(1L);
		Mockito.when(session.get(Object.class, Serializable.class)).thenReturn(object);
		Object obj = genericDao.create(object);
		assertEquals(null, obj);
	}

	@Test(expected = Exception.class)
	public void updateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.doThrow(Exception.class).when(session).update(Mockito.any(Object.class));
		genericDao.update(object);
	}

	@Test(expected = Exception.class)
	public void saveOrUpdateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.doThrow(Exception.class).when(session).saveOrUpdate(Mockito.any(Object.class));
		genericDao.saveOrUpdate(object);
	}

	@Test
	public void createTests() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.save(Mockito.any(Object.class))).thenReturn(1L);
		Mockito.when(session.get(Object.class, Serializable.class)).thenReturn(object);
		List<Object> objList = genericDao.saveAll(objectList);
		assertEquals(objectList.size(), objList.size());
	}

	@Test(expected = Exception.class)
	public void deleteTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.doThrow(Exception.class).when(session).delete(Mockito.any(Object.class));
		genericDao.delete(object);
	}

	@Test(expected = Exception.class)
	public void flushTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.doThrow(Exception.class).when(session).flush();
		genericDao.flush();
	}

}
