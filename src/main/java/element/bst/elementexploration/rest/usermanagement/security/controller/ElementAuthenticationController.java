package element.bst.elementexploration.rest.usermanagement.security.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupService;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserInfo;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserSetting;
import element.bst.elementexploration.rest.usermanagement.security.dto.ResetPasswordDto;
import element.bst.elementexploration.rest.usermanagement.security.service.CountrySettingService;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementSecurityService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.GeoIpCountryMapping;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Amit Patel
 *
 */
@Api(tags = { "Element authentication API" }, description = "Manages authentication")
@RestController
@RequestMapping("/api/security")
public class ElementAuthenticationController extends BaseController {

	@Autowired
	private ElementSecurityService elementSecurityService;

	@Autowired
	private GeoIpCountryMapping geoIpCountryMapping;

	@Autowired
	private CountrySettingService countrySettingService;

	@Autowired
	private UserService userService;

	@Autowired
	private GroupService groupService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	SettingsService settingsService;

	@Value("${fe_base_url}")
	private String FE_BASE_URL;
	

	/**
	 * API to validate email and password.
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("Validates email and password of the user")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "User account deactivated/locked or username/password is incorrect"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not get user from admin group") })
	@PostMapping(value = "/validateEmailPassword", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> validateEmailPassword(String emailAddress, String password, HttpServletRequest request)
			throws Exception {
		// Audit View Trail, Get Before Change
		UserDto userBeforeChange = userService.getUserByEmailOrUsername(emailAddress);

		if (null != userBeforeChange) {
			if (userBeforeChange.getStatus().equals("1")) {
				String ip = request.getHeader("CALLER-IP");
				if (ip == null || "".equals(ip) || "".equals(ip.trim())) {
					ip = request.getRemoteAddr();
				}
				String response = elementSecurityService.validateEmailPassword(emailAddress, password, ip);

				UserSetting userSetting = geoIpCountryMapping.getCountryMapping(ip);
				ElementLogger.log(ElementLoggerLevel.INFO,
						"IP Address = " + ip + " userSetting=" + userSetting.getCountry(), this.getClass());
				if (userSetting != null) {
					try {
						CountrySetting countrySetting = countrySettingService
								.getCountrySetting(userSetting.getIsoCode());
						request.getSession().setAttribute("countrySetting", countrySetting);
					} catch (Exception e) {
						ElementLogger.log(ElementLoggerLevel.INFO, "Country Setting not found", this.getClass());
					}
				}
				// To Update user last login date and ip address
				userService.setUserLastLoginDateAndIp(userBeforeChange, ip);
				UserDto userAfterChange = userService.getUserByEmailOrUsername(emailAddress);
				JSONObject jObject = new JSONObject(response);
				UserInfo userInfo = new UserInfo();
				userInfo.setEmail(userAfterChange.getEmailAddress());
				userInfo.setToken(jObject.getString("token"));

				// old code lasst login date as String
				/*if (null != userBeforeChange) {
					userInfo.setUserId((Long) userBeforeChange.getUserId());
					userInfo.setFullName(userBeforeChange.getFirstName() + " " + userBeforeChange.getLastName());
					userInfo.setLastLoginDate((userBeforeChange.getLastLoginDate() != null)
							? userBeforeChange.getLastLoginDate().toString()
							: userAfterChange.getLastLoginDate().toString());
					userInfo.setLang("en");
				}*/
				// New code last login date as date(timestamp)
				if (null != userBeforeChange) {
					userInfo.setUserId((Long) userBeforeChange.getUserId());
					userInfo.setFullName(userBeforeChange.getFirstName() + " " + userBeforeChange.getLastName());
					userInfo.setLastLoginDate((userBeforeChange.getLastLoginDate() != null)
							? userBeforeChange.getLastLoginDate()
							: userAfterChange.getLastLoginDate());
					userInfo.setLang("en");
				}
				if (groupService.doesUserbelongtoGroup(ElementConstants.DATAENTRY_GROUP_NAME,
						userBeforeChange.getUserId()))
					userInfo.setDataEntryUser(true);
				if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userBeforeChange.getUserId()))
					userInfo.setAdminUser(true);
				if (groupService.doesUserbelongtoGroup(ElementConstants.ANALYST_GROUP_NAME,
						userBeforeChange.getUserId()))
					userInfo.setAnalystUser(true);
				userInfo.setDefaultUrl(settingsService.getDeaultUrl());
				request.getSession().setAttribute("userInfo", userInfo);
				return new ResponseEntity<>(userInfo, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new ResponseMessage("User account deactivated."), HttpStatus.UNAUTHORIZED);
			}
		} else
			return new ResponseEntity<>(new ResponseMessage("Either username or password is incorrect."),
					HttpStatus.UNAUTHORIZED);
	}

	@ApiOperation("Forgot password request")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "User account deactivated/locked or username/password is incorrect"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not get user from admin group") })
	@PostMapping(value = "/forgotPassword", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> forgotPassword(@RequestParam(required = false) String username,
			@RequestParam(required = false) String emailAddress, HttpServletRequest request) throws Exception {
		UserDto user = null;
		if (emailAddress != null)
			user = userService.getUserByEmailOrUsername(emailAddress);
		if (username != null)
			user = userService.getUserByEmailOrUsername(username);
		if (emailAddress != null && null == user)
			return new ResponseEntity<>(new ResponseMessage("E-mail address has not been defined for the user."),
					HttpStatus.UNAUTHORIZED);
		if (username != null && null == user)
			return new ResponseEntity<>(new ResponseMessage("User doesn`t exist"), HttpStatus.UNAUTHORIZED);
		if (null != user && StringUtils.isEmpty(user.getEmailAddress())) {
			return new ResponseEntity<>(new ResponseMessage("E-mail address has not been defined for the user."),
					HttpStatus.UNAUTHORIZED);
		}
		if (elementSecurityService.sendEmailForPasswordReset(user, request))
			return new ResponseEntity<>(
					new ResponseMessage("Check your email inbox for an email with the subject 'Reset your password'"),
					HttpStatus.OK);

		return new ResponseEntity<>(
				new ResponseMessage("Could not send email, Please contact admin to configure mail settings"),
				HttpStatus.BAD_REQUEST);

	}

	@ApiOperation("Validates the temporary password")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 302, response = ResponseMessage.class, message = "could not find the authentication token") })
	@GetMapping(value = "/validateTemporaryPassword/{userIDAndTempPassword}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> validateTemporaryPassword(@PathVariable String userIDAndTempPassword,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		/*String url = new String(
				request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/");*/
		String url = FE_BASE_URL  + "/login?";
		if (elementSecurityService.validateTemporaryPassword(userIDAndTempPassword, request)) {
			Long userId = null;
			String[] userIdAndPass = userIDAndTempPassword.split("-");
			if (userIdAndPass.length > 0)
				userId = Long.valueOf(userIdAndPass[0].trim());
			if(userId!=null)
				url = url + "loginId=" + userId.toString();
			responseHeaders.add("Location", url);

		} else {
			url = url + "error=could not find the authentication token";
			responseHeaders.add("Location", url);
		}
		return new ResponseEntity<>(responseHeaders, HttpStatus.FOUND);

	}

	@ApiOperation("Resets the password")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResetPasswordDto.class, message = "Password changed successfully"),
			@ApiResponse(code = 404, response = ResetPasswordDto.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResetPasswordDto.class, message = "User doesn`t exist") })
	@PostMapping(value = "/resetPassword", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> resetPassword(@RequestParam Long userId, @RequestParam String newPassword,
			@RequestParam String reTypePassword, HttpServletRequest request) throws Exception {
		ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
		if (!newPassword.equals(reTypePassword)) {
			resetPasswordDto.setMessage("Passwords do not match");
			return new ResponseEntity<>(resetPasswordDto, HttpStatus.BAD_REQUEST);
		}
		User user = userService.find(userId);
		if (null != user) {
			if (!StringUtils.isEmpty(user.getTemporaryPassword()) && null != user.getTemporaryPassword()) {
				user.setPassword(passwordEncoder.encode(newPassword));
				user.setTemporaryPassword(null);
				user.setTemporaryPasswordExpiresOn(null);
				userService.saveOrUpdate(user);
				resetPasswordDto.setScreenName(user.getScreenName());
				resetPasswordDto.setMessage("Password changed successfully");
				resetPasswordDto.setValidated(true);
				return new ResponseEntity<>(resetPasswordDto, HttpStatus.OK);
			} else {
				resetPasswordDto.setMessage("Your temporary password has expired, please create a new one");
				return new ResponseEntity<>(resetPasswordDto, HttpStatus.UNAUTHORIZED);
			}
		}
		resetPasswordDto.setMessage("User doesn`t exist");
		return new ResponseEntity<>(resetPasswordDto, HttpStatus.UNAUTHORIZED);

	}
}
