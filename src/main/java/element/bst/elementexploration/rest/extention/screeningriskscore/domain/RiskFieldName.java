package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class RiskFieldName implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
