package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplateMappingService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
/**
 * @author Rambabu
 *
 */
@Service("documentTemplateMappingService")
public class DocumentTemplateMappingServiceImpl extends GenericServiceImpl<DocumentTemplateMapping,Long > implements DocumentTemplateMappingService{

	@Autowired
	public DocumentTemplateMappingServiceImpl(GenericDao<DocumentTemplateMapping, Long> genericDao) {
		super(genericDao);
		
	}
	
	

}
