package element.bst.elementexploration.rest.investigation.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.ExclusiveGateway;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.ParallelGateway;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.ServiceTask;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.dto.TransitionsDto;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseAnalystMappingService;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.activiti.service.TransitionService;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Investigation API" }, description = "Case management apis")
@RestController
@RequestMapping("/api/investigation")
public class InvestigationController extends BaseController {

	@Autowired
	private CaseService caseService;

	@Autowired
	private CaseAnalystMappingService caseAnalystMappingService;

	@Autowired
	UserService userService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private TransitionService transitionService;

	@ApiOperation("Change Status Form Submitted To Acknowledge")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Acknowledged."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusSubmittedToAcknowledged", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCaseSeedStatusSubmittedToAcknowledged(@RequestParam Long caseId,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();

		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, null, null, StatusEnum.SUBMITTED.getIndex(),
				StatusEnum.ACKNOWLEDGE.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Acknowledged."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Acknowledge To Accepted")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Accepted."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusAcknowledgedToAccepted", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusAcknowledgedToAccepted(@RequestParam Long caseId,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();
		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, null, null, StatusEnum.ACKNOWLEDGE.getIndex(),
				StatusEnum.ACCEPTED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Accepted."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Acknowledge To Paused")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Paused."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusAcknowledgedToPaused", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusAcknowledgedToPaused(@RequestParam Long caseId,
			@RequestParam String statusComment, @RequestParam String notificationDate,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();
		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, statusComment, notificationDate,
				StatusEnum.ACKNOWLEDGE.getIndex(), StatusEnum.PAUSED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Paused."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Acknowledge To Rejected")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Rejected."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusAcknowledgedToRejected", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusAcknowledgedToRejected(@RequestParam Long caseId,
			@RequestParam String statusComment, @RequestParam String notificationDate,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();
		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, statusComment, notificationDate,
				StatusEnum.ACKNOWLEDGE.getIndex(), StatusEnum.REJECTED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Rejected."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Paused To Accepted")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Accepted."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusPauseToAccepted", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusPauseToAccepted(@RequestParam Long caseId,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();

		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, null, null, StatusEnum.PAUSED.getIndex(),
				StatusEnum.ACCEPTED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Accepted."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Paused To Rejected")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Rejected."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusPauseToRejected", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusPauseToRejected(@RequestParam Long caseId,
			@RequestParam String statusComment, @RequestParam String notificationDate,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();

		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, statusComment, notificationDate,
				StatusEnum.PAUSED.getIndex(), StatusEnum.REJECTED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status change to Rejected."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Accepted To Paused")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Paused."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusAcceptedToPaused", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusAcceptedToPaused(@RequestParam Long caseId,
			@RequestParam String statusComment, @RequestParam String notificationDate,
			@RequestParam("token") String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();

		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, statusComment, notificationDate,
				StatusEnum.ACCEPTED.getIndex(), StatusEnum.PAUSED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status change to Paused."), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Accepted To Resolved")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Resolved."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusAcceptedToResolved", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusAcceptedToResolved(@RequestParam Long caseId,
			@RequestParam String statusComment, @RequestParam("token") String token, HttpServletRequest request)
			throws ParseException {

		Long userIdTemp = getCurrentUserId();

		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, null, null, StatusEnum.ACCEPTED.getIndex(),
				StatusEnum.RESOLVED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Resolved"), HttpStatus.OK);
	}

	@ApiOperation("Change Status Form Accepted To Canceled")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed to Cancelled."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCurrentStatusAcceptedToCanceled", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCurrentStatusAcceptedToCanceled(@RequestParam Long caseId,
			@RequestParam String statusComment, @RequestParam("token") String token, HttpServletRequest request)
			throws ParseException {

		Long userIdTemp = getCurrentUserId();

		caseAnalystMappingService.updateCaseStatus(caseId, userIdTemp, null, null, StatusEnum.ACCEPTED.getIndex(),
				StatusEnum.CANCELED.getIndex());
		return new ResponseEntity<>(new ResponseMessage("Status changed to Cancelled."), HttpStatus.OK);
	}

	@ApiOperation("Reassign a case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Case reassigned successfully"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Case cannot be reassigned unless its accepted or paused."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Either user or case is not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/reassignCase", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> caseReassignment(@RequestParam Long caseId, @RequestParam Long newUserId,
			@RequestParam String statusComment, @RequestParam("token") String token, HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		Case caseobj = caseService.find(caseId);

		caseAnalystMappingService.startTask(caseId, userIdTemp, caseobj.getCurrentStatus(), 0, statusComment, null, newUserId, "1", null);

		// caseAnalystMappingService.caseReassignment(caseId, userIdTemp,
		// newUserId, statusComment);
		return new ResponseEntity<>(new ResponseMessage("Case reassigned successfully"), HttpStatus.OK);
	}

	@ApiOperation("Forward a case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Case forwarded successfully."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Case cannot be forwarded unless its acknowledged or submitted."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Either user or case is not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/forwardCase", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> caseForwarding(@RequestParam Long caseId, @RequestParam Long newUserId,
			@RequestParam String statusComment, @RequestParam("token") String token, HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		Case caseobj = caseService.find(caseId);
		caseAnalystMappingService.startTask(caseId, userIdTemp, caseobj.getCurrentStatus(), 0, statusComment, null, newUserId, null, "1");

		// caseAnalystMappingService.caseForwarding(caseId, userIdTemp,
		// newUserId, statusComment);
		return new ResponseEntity<>(new ResponseMessage("Case forwarded successfully."), HttpStatus.OK);
	}

	@ApiOperation("Cases For Incoming Tray")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/incomingTray", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseSeedsByAnalystIdAndStatus(@RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, @RequestParam String token, HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		List<CaseDto> caseSeed = caseService.getAllIncomingTrayBasedOnUserId(userIdTemp, pageNumber, recordsPerPage,
				orderBy, orderIn);

		long totalResults = caseService.countgetAllIncomingTray(userIdTemp);
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseSeed != null ? caseSeed.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseSeed);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseSeed);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Cases For Incoming Tray With Fulltext Search")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/searchIncomingTray", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fullTextSearchForIncomingtray(@RequestParam String caseSearchKeyword,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String creationDate, @RequestParam(required = false) String modifiedDate,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			@RequestParam String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();
		if (StringUtils.isEmpty(caseSearchKeyword) || caseSearchKeyword.length() < 5) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
					HttpStatus.OK);
		}
		List<CaseDto> caseSeedList = caseService.fullTextSearchForIncomingtray(userIdTemp, caseSearchKeyword,
				pageNumber, recordsPerPage, creationDate, modifiedDate, orderBy, orderIn);
		long totalResults = caseService.countFullTextSearchForIncomingtray(userIdTemp, caseSearchKeyword, creationDate,
				modifiedDate);
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseSeedList != null ? caseSeedList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseSeedList);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseSeedList);
		responseUtil.setPaginationInformation(information);

		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Get My Cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/getMyCases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllMyCases(@RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam String token,
			HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		List<CaseDto> caseSeedList = caseService.getAllMyCasesBasedOnUserId(userIdTemp, orderBy, orderIn, pageNumber,
				recordsPerPage);
		long totalResults = caseService.countgetAllMyCases(userIdTemp);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseSeedList != null ? caseSeedList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseSeedList);
		 * myCaseResult.put("paginationInformation", information);
		 */

		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseSeedList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Fulltext Search Get My Cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/searchMyCases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fullTextSearchForMyCases(@RequestParam String caseSearchKeyword,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String creationDate, @RequestParam(required = false) String modifiedDate,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			@RequestParam String token, HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();
		if (StringUtils.isEmpty(caseSearchKeyword) || caseSearchKeyword.length() < 5) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
					HttpStatus.OK);
		}
		List<CaseDto> caseSeedList = caseService.fullTextSearchForMyCases(userIdTemp, caseSearchKeyword, pageNumber,
				recordsPerPage, creationDate, modifiedDate, orderBy, orderIn);
		long totalResults = caseService.countFullTextSearchForMyCases(userIdTemp, caseSearchKeyword, creationDate,
				modifiedDate);
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseSeedList != null ? caseSeedList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseSeedList);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseSeedList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Mark as focused")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.CASE_ADDED_TO_FOCUS_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/markAsFocused", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addCaseToFocus(@RequestParam Long caseId, @RequestParam String token,
			HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		Case caseobj = caseService.find(caseId);
		boolean status = true; 
		/* caseAnalystMappingService.addStatusFocus(caseId,
								 userIdTemp);*/
		try {
			caseAnalystMappingService.startTask(caseId, userIdTemp, caseobj.getCurrentStatus(), StatusEnum.FOCUS.getIndex(), null, null, null,
					null, null);

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		if (status) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.CASE_ADDED_TO_FOCUS_MSG), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.CASE_ALREADY_ADDED_TO_FOCUS_MSG),
					HttpStatus.OK);
		}
	}

	@ApiOperation("Remove Case From Focus")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.CASE_DELETED_FROM_IN_FOCUS_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.COULD_NOT_DELETED_CASE_FROM_IN_FOCUS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@DeleteMapping(value = "/removeCaseFromFocused", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteCaseFromFocus(@RequestParam Long caseId, @RequestParam String token,
			HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		boolean status =caseAnalystMappingService.deleteCaseFromFocusById(caseId,
								 userIdTemp);
		List<Task> taskList = taskService.createTaskQuery().taskAssignee(userIdTemp.toString()).list();

		Task taskToPerform = null;
		for (Task task : taskList) {
			if (caseId.toString().equals(task.getDescription()) && task.getTaskDefinitionKey().equalsIgnoreCase("UNFOCUS"))
				taskToPerform = task;
		}
		Map<String, String> variables = new HashMap<String, String>();
		variables.put("caseId", caseId.toString());
		variables.put("userId", userIdTemp.toString());
		if(taskToPerform!=null)
			taskService.setVariables(taskToPerform.getId(), variables);
		if(status)
			taskService.complete(taskToPerform.getId());
		if (status) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.CASE_DELETED_FROM_IN_FOCUS_MSG),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.COULD_NOT_DELETED_CASE_FROM_IN_FOCUS_MSG),
					HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation("Get All Focused Cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllFocusedCases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllFocusCaseSeed(@RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam String token,
			HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		List<CaseDto> caseList = caseAnalystMappingService.getAllCasesInFocusById(userIdTemp, orderBy, orderIn,
				pageNumber, recordsPerPage);
		long totalResults = caseAnalystMappingService.countAllCasesInFocusById(userIdTemp);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseList != null ? caseList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseList);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Search All Focused Cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is dd-MM-yyyy"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/searchAllFocusedCases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllFocusCaseSeedFullTextSearch(@RequestParam String caseSearchKeyword,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String creationDate, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, @RequestParam String token, HttpServletRequest request)
			throws ParseException {
		Long userIdTemp = getCurrentUserId();
		if (StringUtils.isEmpty(caseSearchKeyword) || caseSearchKeyword.length() < 5) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
					HttpStatus.OK);
		}
		List<CaseDto> searchcaseList = caseAnalystMappingService.fullTextSearchForCasesInFocus(userIdTemp,
				caseSearchKeyword, pageNumber, recordsPerPage, creationDate, orderBy, orderIn);
		long totalResults = caseAnalystMappingService.countfullTextSearchForCaseInFocus(userIdTemp, caseSearchKeyword,
				creationDate);
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(searchcaseList != null ? searchcaseList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", searchcaseList);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(searchcaseList);
		responseUtil.setPaginationInformation(information);

		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Get case comment notifications")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseCommentNotificationDto.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "No data found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/getCaseCommentNotification", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseCommentNotification(@RequestParam Long caseId, @RequestParam String token,
			HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		CaseCommentNotificationDto caseCommentNotificationDto = caseAnalystMappingService
				.getCaseCommentNotificationById(userIdTemp, caseId);

		if (null != caseCommentNotificationDto) {
			return new ResponseEntity<>(caseCommentNotificationDto, HttpStatus.OK);
		} else {
			throw new NoDataFoundException();
		}
	}

	@ApiOperation("Related Cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Could not fetch any data from Big data server or Case not found.") })
	@GetMapping(value = "/relatedCases/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRelatedCases(@PathVariable Long caseId, @RequestParam String token,
			HttpServletRequest request) {
		return new ResponseEntity<>(caseService.getRelatedCasesById(caseId), HttpStatus.OK);
	}

	@ApiOperation("Hot topics")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Could not fetch any data from Big data server or Case not found.") })
	@GetMapping(value = "/hotTopics/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getHotTopicForCase(@PathVariable Long caseId, @RequestParam String token,
			HttpServletRequest request) {
		return new ResponseEntity<>(caseService.getHotTopicsByCaseId(caseId), HttpStatus.OK);
	}

	@ApiOperation("Case source")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Could not fetch any data from Big data server or Case not found.") })
	@GetMapping(value = "/caseSource", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseSource(@RequestParam Long caseId, @RequestParam(required = false) Integer limit,
			@RequestParam String token, HttpServletRequest request) {
		return new ResponseEntity<>(caseService.getCaseSourceById(caseId, limit), HttpStatus.OK);
	}

	@ApiOperation("Hot topics")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Could not fetch any data from Big data server or Case not found.") })
	@GetMapping(value = "/caseSummary", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseSummary(@RequestParam(required = false) Long caseId, @RequestParam String token,
			HttpServletRequest request) {
		return new ResponseEntity<>(caseService.getCaseSummaryById(caseId), HttpStatus.OK);
	}

	@GetMapping(value = "/analystMetrics", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAnalystMetrix(@RequestParam("token") String token,
			@RequestParam(required = false) String dateFrom, @RequestParam(required = false) String dateTo,
			@RequestParam(required = false) Boolean currentOnly, @RequestParam(required = false) String type,
			HttpServletRequest request) throws ParseException {
		Long userIdTemp = getCurrentUserId();
		return new ResponseEntity<>(
				caseService.getAnalystMetrics(dateFrom, dateTo, type, currentOnly, userIdTemp).toString(),
				HttpStatus.OK);

	}

	@ApiOperation("Get case list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@PostMapping(value = "/caseList", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseList(@RequestParam String token, @RequestBody CaseVo caseVo,
			HttpServletRequest request) throws Exception {
		Long userId = getCurrentUserId();

		List<CaseDto> caseList = caseService.getCaseList(caseVo, userId);
		if(caseVo.getAddRiskScore()==null || caseVo.getAddRiskScore())
			caseList=caseService.getRiskScores(caseList);
		Long totalResults = caseService.caseListCount(caseVo, userId);

		int pageSize = caseVo.getRecordsPerPage() == null ? ElementConstants.DEFAULT_PAGE_SIZE
				: caseVo.getRecordsPerPage();
		int index = caseVo.getPageNumber() != null ? caseVo.getPageNumber() : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseList != null ? caseList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseList);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Get case list fulltext search")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@PostMapping(value = "/caseListFullTextSearch", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseListFullTextSearch(@RequestParam String token, @RequestBody CaseVo caseVo,
			HttpServletRequest request) throws Exception {
		Long userId = getCurrentUserId();

		List<CaseDto> caseList = caseService.getCaseList(caseVo, userId);
		Long totalResults = caseService.caseListCount(caseVo, userId);

		int pageSize = caseVo.getRecordsPerPage() == null ? ElementConstants.DEFAULT_PAGE_SIZE
				: caseVo.getRecordsPerPage();
		int index = caseVo.getPageNumber() != null ? caseVo.getPageNumber() : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseList != null ? caseList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myCaseResult = new JSONObject();
		 * myCaseResult.put("result", caseList);
		 * myCaseResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(caseList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}
	
	@ApiOperation("Get case list for underwriting")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/caseListForUnderwriting", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseseedListForUnderwriting(@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String keyWord, HttpServletRequest request) {
		Long userId = getCurrentUserId();

		List<CaseUnderwritingDto> caseList = caseService.getCaseListForUnderwriting(recordsPerPage, pageNumber, keyWord);
		Long totalResults = caseService.caseListCountForUnderwriting(keyWord);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseList != null ? caseList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setResult(caseList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}


	@ApiOperation("Case Aggregator")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseAggregator.class, responseContainer = "List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseUtil.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@PostMapping(value = "/caseAggregator", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> caseAggregator(@RequestParam("token") String token,
			@Valid @RequestBody CaseAggregatorDto caseAggregatorDto, BindingResult results, HttpServletRequest request)
			throws ParseException {

		Long userId = getCurrentUserId();
		if (!results.hasErrors()) {
			List<CaseAggregator> caseAggregatorList = caseService.caseAggregator(userId, caseAggregatorDto);
			return new ResponseEntity<>(caseAggregatorList, HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}
	
	@ApiOperation("Get Transitions")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, responseContainer = "List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseUtil.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/getTransitions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTransitions(@RequestParam("token") String token, HttpServletRequest request,@RequestParam("caseId") Long caseId,
			@RequestParam Integer currentStatus) {

		List<TransitionsDto> transitionsDtoList=new ArrayList<TransitionsDto>();
		List<FlowElement> listOfTransitions =transitionService.getTransitions(caseId, currentStatus);
		for (FlowElement flowElement2 : listOfTransitions) {
			TransitionsDto transitionsDto=new TransitionsDto();
			if (currentStatus == 6) {
				if (flowElement2 instanceof UserTask){
					if("FORWARD".equalsIgnoreCase(flowElement2.getName()) || "REASSIGN".equalsIgnoreCase(flowElement2.getName())){
						transitionsDto.setName(StringUtils.capitalize(flowElement2.getName().toLowerCase()));
					}else{
						transitionsDto.setKey(StatusEnum.valueOf(flowElement2.getName()).getIndex());
						transitionsDto.setName(StringUtils.capitalize(StatusEnum.valueOf(flowElement2.getName()).getName()));
					}
					
				}
				transitionsDtoList.add(transitionsDto);

			} else if (currentStatus == 7) {
				List<String> statuses=transitionService.getTransitionsBytaskKey(null,caseId);
				for (String string : statuses) {
					TransitionsDto transitionsDto2=null;
					if("FORWARD".equalsIgnoreCase(string) || "REASSIGN".equalsIgnoreCase(string) || "UNFOCUS".equalsIgnoreCase(string)){
						transitionsDto2=new TransitionsDto();
						transitionsDto2.setName(StringUtils.capitalize(string.toLowerCase()));
					}else if(StatusEnum.valueOf(string).getIndex()!=7){
						transitionsDto2=new TransitionsDto();
						transitionsDto2.setKey(StatusEnum.valueOf(string).getIndex());
						transitionsDto2.setName(StringUtils.capitalize(StatusEnum.valueOf(string).getName()));
					}
					transitionsDtoList.add(transitionsDto2);
				}
				transitionsDto.setName(StringUtils.capitalize("Unfocus"));
				transitionsDtoList.add(transitionsDto);
				break;

			} else {
				if("FORWARD".equalsIgnoreCase(flowElement2.getName()) || "REASSIGN".equalsIgnoreCase(flowElement2.getName()) || "UNFOCUS".equalsIgnoreCase(flowElement2.getName())){
					transitionsDto.setName(StringUtils.capitalize(flowElement2.getName().toLowerCase()));
				}else{
					transitionsDto.setKey(StatusEnum.valueOf(flowElement2.getName()).getIndex());
					transitionsDto.setName(StringUtils.capitalize(StatusEnum.valueOf(flowElement2.getName()).getName()));
				}
				transitionsDtoList.add(transitionsDto);
			}

		}
		return new ResponseEntity<>(transitionsDtoList, HttpStatus.OK);

	}
	
	/*public List<FlowElement> find(FlowElement element, List<FlowElement> list) {
		if (element instanceof UserTask)
			list.add(element);
		if (element instanceof ExclusiveGateway) {
			ExclusiveGateway gateway = (ExclusiveGateway) element;
			for (SequenceFlow node : gateway.getOutgoingFlows()) {
				//if (!node.getId().equalsIgnoreCase("reStartCase"))
					find(node.getTargetFlowElement(), list);
			}
		}
		if (element instanceof ParallelGateway) {
			ParallelGateway parllelgateway = (ParallelGateway) element;
			for (SequenceFlow node : parllelgateway.getOutgoingFlows()) {
				//if (!node.getId().equalsIgnoreCase("reStartCase"))
					find(node.getTargetFlowElement(), list);
			}
		}
		if (element instanceof ServiceTask)
			list.add(element);
		if (element instanceof EndEvent)
			list.add(element);
		return list;

	}*/
	
	

	@ApiOperation("Get Incoming Transitions")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, responseContainer = "List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseUtil.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/getIncomingTransitions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getIncomingTransitions(@RequestParam("token") String token, HttpServletRequest request,@RequestParam("caseId") Long caseId,
			@RequestParam Integer currentStatus) {

		List<TransitionsDto> transitionsDtoList=new ArrayList<TransitionsDto>();
		
		Task task=taskService.createTaskQuery().taskDescription(caseId.toString()).singleResult();
		/*ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
				.processDefinitionKey("caseManagementWorkFlow").latestVersion().singleResult();*/
		BpmnModel bpm = repositoryService.getBpmnModel(task.getProcessDefinitionId());
		org.activiti.bpmn.model.Process process = bpm.getProcessById("caseManagementWorkFlow");
		FlowElement flowElement = process.getFlowElement(StatusEnum.getStatusEnumByIndex(currentStatus).getName());
		List<FlowElement> listOfTransitions = new ArrayList<>();
		if (flowElement instanceof UserTask) {
			UserTask userTask = (UserTask) flowElement;
			List<SequenceFlow> outgoingFlows = userTask.getIncomingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				List<FlowElement> element = findIncomingFolws(sequenceFlow.getSourceFlowElement(), listOfTransitions);
				System.out.println(element.size());
			}
		} else if (flowElement instanceof ServiceTask) {
			ServiceTask serviceTask = (ServiceTask) flowElement;
			List<SequenceFlow> outgoingFlows = serviceTask.getIncomingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				List<FlowElement> element = findIncomingFolws(sequenceFlow.getSourceFlowElement(), listOfTransitions);
				System.out.println(element.size());
			}

		}

		for (FlowElement flowElement2 : listOfTransitions) {
			TransitionsDto transitionsDto=new TransitionsDto();
				if("FORWARD".equalsIgnoreCase(flowElement2.getName()) || "REASSIGN".equalsIgnoreCase(flowElement2.getName())){
					transitionsDto.setName(flowElement2.getName());
				}else{
					transitionsDto.setKey(StatusEnum.valueOf(flowElement2.getName()).getIndex());
					transitionsDto.setName(StatusEnum.valueOf(flowElement2.getName()).getName());
				}
				transitionsDtoList.add(transitionsDto);
			}

		return new ResponseEntity<>(transitionsDtoList, HttpStatus.OK);

	}
	
	public List<FlowElement> findIncomingFolws(FlowElement element, List<FlowElement> list) {
		if (element instanceof UserTask)
			list.add(element);
		if (element instanceof ExclusiveGateway) {
			ExclusiveGateway gateway = (ExclusiveGateway) element;
			for (SequenceFlow node : gateway.getIncomingFlows()) {
				//if (!node.getId().equalsIgnoreCase("reStartCase"))
				findIncomingFolws(node.getSourceFlowElement(), list);
			}
		}
		if (element instanceof ParallelGateway) {
			ParallelGateway parllelgateway = (ParallelGateway) element;
			for (SequenceFlow node : parllelgateway.getIncomingFlows()) {
				//if (!node.getId().equalsIgnoreCase("reStartCase"))
				findIncomingFolws(node.getSourceFlowElement(), list);
			}
		}
		if (element instanceof ServiceTask)
			list.add(element);
		if (element instanceof EndEvent)
			list.add(element);
		return list;

	}

	@ApiOperation("Change Status")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Status changed successfully"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/changeCaseSeedStatus", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> changeCaseSeedStatus(@RequestParam Long caseId, @RequestParam Integer statusFrom,
			@RequestParam Integer statusTo, @RequestParam(required = false) String statusComment,
			@RequestParam(required = false) String notificationDate, @RequestParam("token") String token,
			HttpServletRequest request) throws ParseException {

		Long userIdTemp = getCurrentUserId();
		caseAnalystMappingService.startTask(caseId, userIdTemp, StatusEnum.getStatusEnumByIndex(statusFrom).getIndex(),
				StatusEnum.getStatusEnumByIndex(statusTo).getIndex(), statusComment, notificationDate, null, null,
				null);
		return new ResponseEntity<>(new ResponseMessage("Status changed successfully"), HttpStatus.OK);
	}
	
	/*@ApiOperation("Get Case List By Search")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@GetMapping(value = "/searchCaseList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchCaseList(@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String keyWord, HttpServletRequest request) {

		List<CaseUnderwritingDto> caseList = caseService.searchCaseList(recordsPerPage, pageNumber, keyWord);
		Long totalResults = caseService.searchCaseListCount(keyWord);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(caseList != null ? caseList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setResult(caseList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}*/
}
