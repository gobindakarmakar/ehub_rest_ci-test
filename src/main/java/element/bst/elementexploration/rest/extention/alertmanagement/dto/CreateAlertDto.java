package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.List;

public class CreateAlertDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2352422198064390571L;
	
	private List<ResponseDto> response;
	
	public List<ResponseDto> getResponse() {
		return response;
	}
	public void setResponse(List<ResponseDto> response) {
		this.response = response;
	}


	
	

}
