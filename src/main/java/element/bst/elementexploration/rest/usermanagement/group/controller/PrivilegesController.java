package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithOutObjectsDto;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Privilges API" })
@RestController
@RequestMapping("/api/privileges")
public class PrivilegesController extends BaseController {

	@Autowired
	PrivilegesService privilegesService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private AuditLogService auditLogService;

	@ApiOperation("Updates the case related document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.ROLE_ADDED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdatePrivilege", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdatePrivilege(@RequestBody @Valid PermissionsWithOutObjectsDto permissionsDto,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		Long userId = getCurrentUserId();
		
		if (!StringUtils.isBlank(permissionsDto.getItemName())) {
			Permissions paranetPermissionId=null;
			if(permissionsDto.getParentPermissionId()!=null)
			 paranetPermissionId = privilegesService.find(permissionsDto.getParentPermissionId());
				if ((permissionsDto.getPermissionId() == null)) {
					Permissions newPermission = new Permissions();
					newPermission.setPermissionId(permissionsDto.getPermissionId());
					newPermission.setItemName(permissionsDto.getItemName());
					newPermission.setActive(true);
					if (paranetPermissionId != null)
					newPermission.setParentPermissionId(paranetPermissionId);
					Permissions existingPermission = privilegesService.findPermission(newPermission);
					if (existingPermission == null) {
						newPermission.setCreatedOn(new Date());
						privilegesService.saveOrUpdate(newPermission);
						AuditLog log = new AuditLog(new Date(), LogLevels.INFO,ElementConstants.STRING_TYPE_PRIVILEGES ,null, 
								ElementConstants.PRIVILEGES_ADDED);
						Users admin = null;
						if(getCurrentUserId() != null){
							admin = usersService.find(getCurrentUserId());
							admin = usersService.prepareUserFromFirebase(admin);
						}
						log.setDescription(admin.getFirstName() + " " + admin.getLastName() + ElementConstants.PRIVILEGES_ADDED
								+ newPermission.getItemName());
						log.setUserId(userId);
						log.setName(newPermission.getItemName());
						auditLogService.save(log);
						return new ResponseEntity<>(
								new ResponseMessage(ElementConstants.PERMISSION_ADDED_SUCCESSFULLY_MSG), HttpStatus.OK);
					} else {

						existingPermission.setCreatedOn(new Date());
						existingPermission.setItemName(permissionsDto.getItemName());
						existingPermission.setActive(true);
						if (paranetPermissionId != null)
						existingPermission.setParentPermissionId(paranetPermissionId);
						privilegesService.saveOrUpdate(existingPermission);
						AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.STRING_TYPE_PRIVILEGES,null, 
								ElementConstants.PRIVILEGES_UPDATED);
						Users admin = null;
						if(getCurrentUserId() != null){
							admin = usersService.find(getCurrentUserId());
							admin = usersService.prepareUserFromFirebase(admin);
						}
						log.setDescription(admin.getFirstName() + " " + admin.getLastName() + ElementConstants.PRIVILEGES_UPDATED
								+ newPermission.getItemName());
						log.setUserId(userId);
						log.setName(newPermission.getItemName());
						auditLogService.save(log);

						return new ResponseEntity<>(
								new ResponseMessage(ElementConstants.PERMISSION_ADDED_SUCCESSFULLY_MSG), HttpStatus.OK);
					}
				}

				else {
					Permissions newPermission = new Permissions();
					newPermission.setPermissionId(permissionsDto.getPermissionId());
					newPermission.setItemName(permissionsDto.getItemName());
					newPermission.setActive(true);
					if (paranetPermissionId != null)
					newPermission.setParentPermissionId(paranetPermissionId);
					Permissions existingPermission = privilegesService.findPermission(newPermission);
					if (existingPermission != null) {
						existingPermission.setCreatedOn(new Date());
						existingPermission.setPermissionId(permissionsDto.getPermissionId());
						existingPermission.setItemName(permissionsDto.getItemName());
						existingPermission.setActive(true);
						if (paranetPermissionId != null)
						existingPermission.setParentPermissionId(paranetPermissionId);
						privilegesService.saveOrUpdate(existingPermission);
						AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.STRING_TYPE_PRIVILEGES,null, 
								ElementConstants.PRIVILEGES_UPDATED);
						Users admin = null;
						if(getCurrentUserId() != null){
							admin = usersService.find(getCurrentUserId());
							admin = usersService.prepareUserFromFirebase(admin);
						}
						log.setDescription(admin.getFirstName() + " " + admin.getLastName() + ElementConstants.PRIVILEGES_UPDATED
								+ newPermission.getItemName());
						log.setUserId(userId);
						log.setName(newPermission.getItemName());
						auditLogService.save(log);

						return new ResponseEntity<>(
								new ResponseMessage(ElementConstants.PERMISSION_ADDED_SUCCESSFULLY_MSG), HttpStatus.OK);

					}
				}

				return new ResponseEntity<>(new ResponseMessage(ElementConstants.PARENTPERMISSIONID_FAILED_MSG),
						HttpStatus.OK);

			//}
			
/*
		}
			
	
			return new ResponseEntity<>(
					new ResponseMessage(ElementConstants.PARENT_PERMISSION_ID_EMPTY), HttpStatus.OK);*/
		}
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.PERMISSION_NAME_EMPTY), HttpStatus.OK);
		

	}
	/*
	 * //Permissions parentPermissionId1 =
	 * privilegesService.find(permissionsDto.getParentPermissionId()); //if
	 * (parentPermissionId!= null) {
	 * 
	 * Permissions newPermission = new Permissions();
	 * newPermission.setPermissionId(permissionsDto.getPermissionId());
	 * newPermission.setItemName(permissionsDto.getItemName());
	 * newPermission.setActive(true);
	 * //newPermission.setParentPermissionId(parentPermissionId1);
	 * 
	 * Permissions existingPermission =
	 * privilegesService.findPermission(newPermission); if
	 * (existingPermission==null ) {
	 * 
	 * newPermission.setCreatedOn(new Date());
	 * privilegesService.saveOrUpdate(newPermission); AuditLog log = new
	 * AuditLog(new Date(), LogLevels.INFO, "PRIVILEGES", null,
	 * "Added Privileges"); Users admin = null; if (getCurrentUserId() != null)
	 * admin = usersService.find(getCurrentUserId());
	 * 
	 * log.setDescription(admin.getFirstName() + " " + admin.getLastName() +
	 * " updated group permission " + newPermission.getItemName());
	 * log.setUserId(userId); log.setName(newPermission.getItemName());
	 * auditLogService.save(log); System.out.println("added");
	 * 
	 * return new ResponseEntity<>(new
	 * ResponseMessage(ElementConstants.PERMISSION_ADDED_SUCCESSFULLY_MSG),
	 * HttpStatus.OK); } else { Permissions parentPermissionId =
	 * privilegesService.find(permissionsDto.getParentPermissionId());
	 * 
	 * if (parentPermissionId!= null){ existingPermission.setCreatedOn(new
	 * Date()); existingPermission.setActive(permissionsDto.getActive());
	 * existingPermission.setItemName(permissionsDto.getItemName());
	 * existingPermission.setParentPermissionId(parentPermissionId);
	 * privilegesService.saveOrUpdate(existingPermission); AuditLog log = new
	 * AuditLog(new Date(), LogLevels.INFO, "PRIVILEGES", null,
	 * "updated privoleges"); Users admin = null; if (getCurrentUserId() !=
	 * null) admin = usersService.find(getCurrentUserId());
	 * 
	 * log.setDescription(admin.getFirstName() + " " + admin.getLastName() +
	 * " updated group permission " + newPermission.getItemName());
	 * log.setUserId(userId); log.setName(newPermission.getItemName());
	 * auditLogService.save(log); System.out.println("updated"); return new
	 * ResponseEntity<>(new
	 * ResponseMessage(ElementConstants.PERMISSION_ADDED_SUCCESSFULLY_MSG),
	 * HttpStatus.OK); } return new ResponseEntity<>(new
	 * ResponseMessage("PatentPermissionID not provided"), HttpStatus.OK);
	 * 
	 * }
	 * 
	 * } return new ResponseEntity<>(new
	 * ResponseMessage("PatentPermissionID cannot be null"), HttpStatus.OK);
	 * 
	 * } } return new ResponseEntity<>(new
	 * ResponseMessage(ElementConstants.PERMISSION_NAME_EMPTY), HttpStatus.OK);
	 * 
	 * }
	 */

	@ApiOperation("Gets All Permissions")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllPermissions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllPermissions(@RequestParam String token) throws Exception {
		List<PermissionsWithIdDto> permisssions = new ArrayList<PermissionsWithIdDto>();
		permisssions = privilegesService.findAllPermissions();
		return new ResponseEntity<>(permisssions, HttpStatus.OK);
	}

}
