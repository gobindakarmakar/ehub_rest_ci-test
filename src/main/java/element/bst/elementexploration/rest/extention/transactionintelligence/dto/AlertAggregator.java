package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

public class AlertAggregator {

	private String period;
	
	private Long count;

	public AlertAggregator(String period, Long count) {
		super();
		this.period = period;
		this.count = count;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}
