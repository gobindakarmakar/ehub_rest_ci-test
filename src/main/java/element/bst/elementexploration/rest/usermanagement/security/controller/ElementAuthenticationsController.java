package element.bst.elementexploration.rest.usermanagement.security.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.extention.menuitem.domain.UserMenu;
import element.bst.elementexploration.rest.extention.menuitem.enumType.MenuSizeEnum;
import element.bst.elementexploration.rest.extention.menuitem.service.MenuItemService;
import element.bst.elementexploration.rest.extention.menuitem.service.ModulesService;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupPermissionService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserInfo;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserSetting;
import element.bst.elementexploration.rest.usermanagement.security.dto.AuthJWTTokenDto;
import element.bst.elementexploration.rest.usermanagement.security.dto.ResetPasswordDto;
import element.bst.elementexploration.rest.usermanagement.security.service.CountrySettingService;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.security.service.MailService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.EmailTypes;
import element.bst.elementexploration.rest.util.GeoIpCountryMapping;
import element.bst.elementexploration.rest.util.JWTDecoder;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import element.bst.elementexploration.rest.util.UserCreationSource;
import element.bst.elementexploration.rest.util.UserStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Element authentication API" }, description = "Manages authentication")
@RestController
@RequestMapping("/api/securityNew")
public class ElementAuthenticationsController extends BaseController {

	@Autowired
	private ElementsSecurityService elementsSecurityService;

	@Autowired
	private GeoIpCountryMapping geoIpCountryMapping;

	@Autowired
	private CountrySettingService countrySettingService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private GroupsService groupsService;

	@SuppressWarnings("unused")
	@Autowired
	private MailService mailService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	ModulesService modulesService;
	
	@Autowired
	MenuItemService menuItemService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	SettingsService settingsService;
	
	@Autowired
	AuditLogService auditLogService;

	@Autowired
	ServletContext servletContext;
	
	@Value("${fe_base_url}")
	private String FE_BASE_URL;

	@Value("${temporaryPassword.validity.mins}")
	private int TEMP_PSWD_VAL_MINS;
	
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private GroupPermissionService groupPermissionService;
	
	@Autowired
	private ListItemService listItemService;

	/**
	 * API to validate email and password.
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@ApiOperation("Validates email and password of the user")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Users account deactivated/locked or username/password is incorrect"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not get user from admin group") })
	@PostMapping(value = "/validateEmailPassword", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> validateEmailPassword(String emailAddress, String password, HttpServletRequest request)
			throws Exception {
		if (settingsService
				.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
						ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
				.equalsIgnoreCase("On")) {
			return validateEmailPassword1( emailAddress,  password,  request);
		}else{
			/* -------------------	Authentication without firebase start-------------- */

			// Audit View Trail, Get Before Change
			UsersDto userBeforeChange = usersService.getUserByEmailOrUsername(emailAddress);
			String loginSource = "Internal";
			Boolean isLoginSuccess = false;
			if (null != userBeforeChange && null != userBeforeChange.getStatusId()) {
				String userStatus = userBeforeChange.getStatusId().getDisplayName();
				if(userStatus.equalsIgnoreCase(UserStatus.Active.getStatus())){
					// if (userBeforeChange.getStatus().equals("1")) {
					String ip = request.getHeader("CALLER-IP");
					if (ip == null || "".equals(ip) || "".equals(ip.trim())) {
						ip = request.getRemoteAddr();

					}
					//String response = elementsSecurityService.validateEmailPassword(emailAddress, password, ip );
					
					String response = elementsSecurityService.validateEmailPasswordNew(emailAddress, password,ip, request, loginSource, userBeforeChange );

					UserSetting userSetting = geoIpCountryMapping.getCountryMapping(ip);
					ElementLogger.log(ElementLoggerLevel.INFO,
							"IP Address = " + ip + " userSetting=" + userSetting.getCountry(), this.getClass());
					if (userSetting != null) {
						try {
							CountrySetting countrySetting = countrySettingService.getCountrySetting(userSetting.getIsoCode());
							request.getSession().setAttribute("countrySetting", countrySetting);
						} catch (Exception e) {
							ElementLogger.log(ElementLoggerLevel.INFO, "Country Setting not found", this.getClass());
						}
					}
					// To Update user last login date and ip address
					usersService.setUserLastLoginDateAndIp(userBeforeChange, ip);
					UsersDto userAfterChange = usersService.getUserByEmailOrUsername(emailAddress);
					JSONObject jObject = new JSONObject(response);
					UserInfo userInfo = new UserInfo();
					userInfo.setEmail(userAfterChange.getEmailAddress());
					userInfo.setToken(jObject.getString("token"));
					
					/////// inserts modules for first time login//////
					List<Modules>  allModulesList = modulesService.findAll();
					List<UserMenu> allMenu=menuItemService.findAll();
					 
						if (allModulesList != null && allModulesList.size() > 0) {
							for (Modules module : allModulesList) {
									List<UserMenu> singleUserMenu = allMenu.stream()
											.filter(menu -> menu.getUserId().equals(userBeforeChange.getUserId()))
											.filter(menu -> menu.getModule().getModuleName().equals(module.getModuleName()))
											.collect(Collectors.toList());
									if (singleUserMenu == null || singleUserMenu.size() == 0) {
										UserMenu newMenu = new UserMenu();
										newMenu.setClicksCount(0);
										newMenu.setMenuItemSize(MenuSizeEnum.SMALL.toString());
										newMenu.setModule(module);
										newMenu.setModuleGroup(module.getModuleGroup());
										newMenu.setUserId(userBeforeChange.getUserId());
										menuItemService.save(newMenu);
									}
							}
						}
					//}
					// old code lasst login date as String
					/*
					 * if (null != userBeforeChange) { userInfo.setUserId((Long)
					 * userBeforeChange.getUserId());
					 * userInfo.setFullName(userBeforeChange.getFirstName() + " " +
					 * userBeforeChange.getLastName());
					 * userInfo.setLastLoginDate((userBeforeChange.getLastLoginDate() != null) ?
					 * userBeforeChange.getLastLoginDate().toString() :
					 * userAfterChange.getLastLoginDate().toString()); userInfo.setLang("en"); }
					 */
					// New code last login date as date(timestamp)
					if (null != userBeforeChange) {
						userInfo.setUserId((Long) userBeforeChange.getUserId());
						userInfo.setFullName(userBeforeChange.getFirstName() + " " + userBeforeChange.getLastName());
						userInfo.setLastLoginDate(
								(userBeforeChange.getLastLoginDate() != null) ? userBeforeChange.getLastLoginDate()
										: userAfterChange.getLastLoginDate());
						userInfo.setLang("en");
					}
					if (groupsService.doesUserbelongtoGroup(ElementConstants.DATAENTRY_GROUP_NAME,
							userBeforeChange.getUserId()))
						userInfo.setDataEntryUser(true);
					if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userBeforeChange.getUserId()))
						userInfo.setAdminUser(true);
					if (groupsService.doesUserbelongtoGroup(ElementConstants.ANALYST_GROUP_NAME, userBeforeChange.getUserId()))
						userInfo.setAnalystUser(true);
					userInfo.setDefaultUrl(settingsService.getDeaultUrl());
					List<Groups> userGroups = groupsService.getGroupsOfUser(userBeforeChange.getUserId(), null, null, null,
							null);
					List<UserRolesDto> userRoles = userRolesService.getRolesOfUserDto(userBeforeChange.getUserId());
					List<GroupPermission> allGroupPermissions=groupPermissionService.findAll();
					List<GroupPermission> groupPsermissionsList= new ArrayList<>();
					/*if (userRoles != null && userRoles.size() > 0) {
						for (UserRolesDto userRole : userRoles) {
							if (userRole.getRoleId() != null) {
								List<GroupPermission> filteredGroupPermissions = new ArrayList<GroupPermission>();
								filteredGroupPermissions = allGroupPermissions.stream()
										.filter(gp -> gp.getRoleId().getRoleId().equals(userRole.getRoleId())).collect(Collectors.toList());
								filteredGroupPermissions=groupPermissionService.getGroupPermissionByRoleId(userRole.getRoleId());
								
								if(filteredGroupPermissions!=null && filteredGroupPermissions.size()>0) {
									//filteredGroupPermissions.stream().forEach(p -> Hibernate.initialize(p.getPermissionId()));
									for(int k=0;k<filteredGroupPermissions.size();k++) {
									groupPsermissionsList.add(filteredGroupPermissions.get(0));
									}
								}
							}
						}
					}*/
					/*for(GroupPermission permission: groupPsermissionsList) {
						Permissions permissionId=permission.getPermissionId();
						
					}*/
					JSONArray groupsJson = new JSONArray(userGroups);
					JSONArray rolesJson = new JSONArray(userRoles);
					JSONArray permissionsJson = new JSONArray(groupPsermissionsList);
					userInfo.setUserGroups(groupsJson.toString());
					userInfo.setUserRoles(rolesJson.toString());
					userInfo.setUserPermissions(permissionsJson.toString());
					if(userBeforeChange.getUserImage()!=null)
						userInfo.setUserImage(userBeforeChange.getUserImage());
					request.getSession().setAttribute("userInfo", userInfo);
					
					/*AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, "Login",
							"", userBeforeChange.getUserId(), userBeforeChange.getFirstName()+" "
							+userBeforeChange.getLastName());
					log.setDescription(userBeforeChange.getFirstName()+" "
							+userBeforeChange.getLastName()+" logged in to the system");
					auditLogService.save(log);*/
					
					return new ResponseEntity<>(userInfo, HttpStatus.OK);
					/*
					 * } else { return new ResponseEntity<>(new
					 * ResponseMessage("Users account deactivated."), HttpStatus.UNAUTHORIZED); }
					 */
				}else if(!isLoginSuccess){
					elementsSecurityService.logUserLogins(userBeforeChange, request, isLoginSuccess);
					
					return new ResponseEntity<>(new ResponseMessage("tried to login into the system with "
					+userBeforeChange.getStatusId().getDisplayName()+" account"),
							HttpStatus.UNAUTHORIZED);
				}else{
					return new ResponseEntity<>(new ResponseMessage("Either username or password is incorrect."),
							HttpStatus.UNAUTHORIZED);
				}
			} else{
				return new ResponseEntity<>(new ResponseMessage("Either username or password is incorrect."),
						HttpStatus.UNAUTHORIZED);
			}
		
			/* -------------------	Authentication without firebase end-------------- */
		}
		
		
		/* -------------------	Authentication with firebase start-------------- */
		/*JSONObject response = elementsSecurityService.getFirebaseAccountInfoByToken(password);
		
		if(response != null && response.has("users")){
			UserInfo userInfo = new UserInfo();
			userInfo.setToken(password);
			
			userInfo.setLang("en");
			userInfo.setToken(password);
			userInfo.setDefaultUrl(settingsService.getDeaultUrl());
			org.json.JSONArray userArray = response.getJSONArray("users");
			if(userArray.length()>0){
				JSONObject userObject = (JSONObject) userArray.get(0);
				
				//check existing user with localId or create new user with localId
				if(userObject.has("localId")){
					Users user = usersService.getFirebaseUserBySourceId(userObject.get("localId").toString());
					if(user != null){
						user.setPassword(password);
						
					}else if(settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow 3rd Party synchronization-Users")
								.equalsIgnoreCase("On")){
					user = usersService.createFirebaseUserBySourceId(userObject);
					
					//if we get a role from the authentication provider check or create a role
					Roles role = null;
					if(userObject.has("role")){
						String externalRole = userObject.getString("role");
						role = rolesService.findRoleByName(externalRole);
						if(null == role && 
								settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow 3rd Party synchronization-Roles")
								.equalsIgnoreCase("On")){
							//create a role with the external role if role not created earlier
							Roles userRole = new Roles();
							userRole.setCreatedOn(new Date());
							userRole.setDescription("External user role.");
							userRole.setNotes("External User");
							userRole.setRoleName(externalRole);
							userRole.setSource(UserCreationSource.EXTERNAL.toString());
							userRole = rolesService.save(userRole);
							
							groupPermissionService.loadZeroPermissionsForRole(userRole);
							role = userRole;
						}
						UserRoles userRole = new UserRoles(role, user);
						userRolesService.save(userRole);
					}else{
						role = rolesService.findRoleByName(ElementConstants.ROLE_NAME_USER);
						UserRoles userRole = new UserRoles(role, user);
						userRolesService.save(userRole);
					}
					
					
					
					List<Modules> allModulesList = modulesService.findAll();
					List<UserMenu> allMenu = menuItemService.findAll();

					if (allModulesList != null && allModulesList.size() > 0) {
						for (Modules module : allModulesList) {

							
						//	if (singleUserMenu == null || singleUserMenu.size() == 0) {
								UserMenu newMenu = new UserMenu();
								newMenu.setClicksCount(0);
								newMenu.setMenuItemSize(MenuSizeEnum.SMALL.toString());
								newMenu.setModule(module);
								newMenu.setModuleGroup(module.getModuleGroup());
								newMenu.setUserId(user.getUserId());
								menuItemService.save(newMenu);
							//}
						}
					}
					
					userInfo.setUserId(user.getUserId());
					
					}else{
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_CREATION_DISABLED),
								HttpStatus.UNAUTHORIZED);
					}
				}
				
				
				if(userObject.has("email"))
					userInfo.setEmail(userObject.getString("email"));
				if(userObject.has("displayName")){
					userInfo.setFullName(userObject.getString("displayName"));
				}else if(userObject.has("email")){
					userInfo.setFullName(getDisplaNameFromEmail(userObject.getString("email")));
				}
					
				if(userObject.has("localId"))
					userInfo.setUserId(userObject.get("localId"));
				if(userObject.has("photoUrl"))
					userInfo.setUserImage(userObject.get("photoUrl"));
				
				JSONObject decodedJwtBody = JWTDecoder.getDecodedJWTBody(password);
				response.put("accessToken", password);
				response.put("authTime", Long.valueOf(decodedJwtBody.get("auth_time").toString()));
				response.put("expireTime", Long.valueOf(decodedJwtBody.get("exp").toString()));
				
				List<String> attributes = Collections.list(servletContext.getAttributeNames());
				if(!attributes.contains(password))	
					servletContext.setAttribute(password, response);
			}
			request.getSession().setAttribute("userInfo", userInfo);
			return new ResponseEntity<>(userInfo, HttpStatus.OK);
		}else{
			List<String> attributes = Collections.list(servletContext.getAttributeNames());
			if(attributes.contains(password))	
				servletContext.removeAttribute(password);
			return new ResponseEntity<>(new ResponseMessage("Either username or password is incorrect."),
					HttpStatus.UNAUTHORIZED);
		}*/
		/* -------------------	Authentication with firebase end-------------- */
	}
	
	

	private String getDisplaNameFromEmail(String email) {
		String []pair = null;
		pair = email.split("@");
		if(pair != null){
			return pair[0];
		}else{
			return null;
		}
		
	}

	@ApiOperation("Forgot password request")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Users account deactivated/locked or username/password is incorrect"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not get user from admin group") })
	@PostMapping(value = "/forgotPassword", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> forgotPassword(@RequestParam(required = false) String username,
			@RequestParam(required = false) String emailAddress, HttpServletRequest request) throws Exception {
		UsersDto user = null;
		if (emailAddress != null)
			user = usersService.getUserByEmailOrUsername(emailAddress);
		if (username != null)
			user = usersService.getUserByEmailOrUsername(username);
		if (emailAddress != null && null == user)
			return new ResponseEntity<>(new ResponseMessage("E-mail address has not been defined for the user."),
					HttpStatus.UNAUTHORIZED);
		if (username != null && null == user)
			return new ResponseEntity<>(new ResponseMessage("Users doesn`t exist"), HttpStatus.UNAUTHORIZED);
		if (null != user && StringUtils.isEmpty(user.getEmailAddress())) {
			return new ResponseEntity<>(new ResponseMessage("E-mail address has not been defined for the user."),
					HttpStatus.UNAUTHORIZED);
		}
		if (elementsSecurityService.sendEmailForPasswordReset(user, request))
			return new ResponseEntity<>(
					new ResponseMessage("Check your email inbox for an email with the subject 'Reset your password'"),
					HttpStatus.OK);

		return new ResponseEntity<>(
				new ResponseMessage("Could not send email, Please contact admin to configure mail settings"),
				HttpStatus.BAD_REQUEST);

	}

	@ApiOperation("Validates the temporary password")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 302, response = ResponseMessage.class, message = "could not find the authentication token") })
	@GetMapping(value = "/validateTemporaryPassword/{userIDAndTempPassword}/{type}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> validateTemporaryPassword(@PathVariable String userIDAndTempPassword,@PathVariable(required=false) String type,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		//String url = new String(
				//request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/");
		//request.getScheme() + "://" + request.getServerName() + "/");
		String url = FE_BASE_URL  + "/login?";
		if (elementsSecurityService.validateTemporaryPassword(userIDAndTempPassword,type, request)) {
			Long userId = null;
			String[] userIdAndPass = userIDAndTempPassword.split("-");
			if (userIdAndPass.length > 0)
				userId = Long.valueOf(userIdAndPass[0].trim());
			if (userId != null)
				url = url + "loginId=" + userId.toString();
			if(type!=null && type.equalsIgnoreCase(EmailTypes.REGISTRATION.toString()))
				url=url+"&setPass=true";
			responseHeaders.add("Location", url);

		} else {
			url = url + "error=could not find the authentication token";
			responseHeaders.add("Location", url);
		}
		return new ResponseEntity<>(responseHeaders, HttpStatus.FOUND);

	}

	@ApiOperation("Resets the password")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResetPasswordDto.class, message = "Password changed successfully"),
			@ApiResponse(code = 404, response = ResetPasswordDto.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResetPasswordDto.class, message = "Users doesn`t exist") })
	@PostMapping(value = "/resetPassword", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> resetPassword(@RequestParam Long userId, @RequestParam String newPassword,
			@RequestParam String reTypePassword, HttpServletRequest request) throws Exception {
		ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
		if (!newPassword.equals(reTypePassword)) {
			resetPasswordDto.setMessage("Passwords do not match");
			return new ResponseEntity<>(resetPasswordDto, HttpStatus.BAD_REQUEST);
		}
		Users user = usersService.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		if (null != user) {
			if ((settingsService.findSettingToggleValue(SystemSettingTypes.MAIL_SETTING.toString(),
					"Enable Authentication").equalsIgnoreCase("Off"))||(!StringUtils.isEmpty(user.getTemporaryPassword()) && null != user.getTemporaryPassword())) {
				user.setPassword(passwordEncoder.encode(newPassword));
				user.setTemporaryPassword(null);
				user.setTemporaryPasswordExpiresOn(null);
				if(settingsService.findSettingToggleValue(SystemSettingTypes.MAIL_SETTING.toString(),
					"Enable Authentication").equalsIgnoreCase("Off")){
					List<ListItem> activeStatuses=listItemService.findAll().stream().filter(o -> o.getDisplayName().equalsIgnoreCase("Active")).filter(o -> o.getListType().equalsIgnoreCase(ElementConstants.USER_STATUS_TYPE)).collect(Collectors.toList());
					if(activeStatuses!=null && activeStatuses.size()>0)
					user.setStatusId(activeStatuses.get(0));
				}
				if (settingsService
						.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
								ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
						.equalsIgnoreCase("Off")) {
					usersService.saveOrUpdate(user);
				}
				
				resetPasswordDto.setScreenName(user.getScreenName());
				resetPasswordDto.setMessage("Password changed successfully");
				resetPasswordDto.setValidated(true);
				return new ResponseEntity<>(resetPasswordDto, HttpStatus.OK);
			} else {
				resetPasswordDto.setMessage("Your temporary password has expired, please create a new one");
				return new ResponseEntity<>(resetPasswordDto, HttpStatus.UNAUTHORIZED);
			}
		}
		resetPasswordDto.setMessage("Users doesn`t exist");
		return new ResponseEntity<>(resetPasswordDto, HttpStatus.UNAUTHORIZED);

	}
	
	@SuppressWarnings("unused")
	@ApiOperation("generates 2FA verification code")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResetPasswordDto.class, message = "Password changed successfully"),
			@ApiResponse(code = 404, response = ResetPasswordDto.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResetPasswordDto.class, message = "Users doesn`t exist") })
	@PostMapping(value = "/generateTwoFAVerificationCode", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> generateTwoFAVerificationCode(@RequestBody AuthJWTTokenDto authJWTTokenDto,
			HttpServletRequest request) throws Exception {
		if (authJWTTokenDto != null && null != authJWTTokenDto.getIdToken()) {
			JSONObject response = elementsSecurityService.getFirebaseAccountInfoByToken(authJWTTokenDto.getIdToken());
			if (response != null && response.has("users")) {

				// generate temporary verification code
				String verificationCode = RandomStringUtils.randomAlphanumeric(10);

				org.json.JSONArray userArray = response.getJSONArray("users");
				if (userArray.length() > 0) {
					JSONObject userObject = (JSONObject) userArray.get(0);

					// check existing user with localId or create new user with
					// localId
					if (userObject.has("localId")) {
						Users user = usersService.getFirebaseUserBySourceId(userObject.get("localId").toString());
						if (user != null) {
							user.setPassword(userObject.getString("localId"));
							/*Boolean isCodeSent = elementsSecurityService.sendVerificationCodeSMTP(verificationCode,
									userObject.getString("email"));*/
							Boolean isCodeSent = elementsSecurityService.sendVerificationCodeSendgrid(verificationCode,
									userObject.getString("email"));
							if (isCodeSent) {
								user.setTemporaryPassword(verificationCode);
								Date expiry = DateUtils.addMinutes(new Date(), TEMP_PSWD_VAL_MINS);
								user.setTemporaryPasswordExpiresOn(expiry);
								usersService.saveOrUpdate(user);
							}else{
								return new ResponseEntity<>(new ResponseMessage("Unable to send the email!"),
										HttpStatus.UNAUTHORIZED);
							}
						} else if (settingsService
								.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow 3rd Party synchronization-Users")
								.equalsIgnoreCase("On") 
								&& settingsService
								.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow 3rd Party synchronization-Roles")
								.equalsIgnoreCase("On")) {

							if (userObject.has("email")) {
								/*Boolean isCodeSent = elementsSecurityService.sendVerificationCodeSMTP(verificationCode,
										userObject.getString("email"));*/
								Boolean isCodeSent = elementsSecurityService.sendVerificationCodeSendgrid(verificationCode,
										userObject.getString("email"));
								if (isCodeSent) {
									userObject.put("verificationCode", verificationCode);
								}else{
									return new ResponseEntity<>(new ResponseMessage("Unable to send the email!"),
											HttpStatus.UNAUTHORIZED);
								}
							}
							user = usersService.createFirebaseUserBySourceId(userObject);

							// if we get a role from the authentication provider
							// check or create a role
							Roles role = null;
							if (userObject.has("role")) {
								String externalRole = userObject.getString("role");
								role = rolesService.findRoleByName(externalRole);
								if (null == role
										&& settingsService
												.findSettingToggleValue(
														SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
														"Allow 3rd Party synchronization-Roles")
												.equalsIgnoreCase("On")) {
									// create a role with the external role if
									// role not created earlier
									Roles userRole = new Roles();
									userRole.setCreatedOn(new Date());
									userRole.setDescription("External user role.");
									userRole.setNotes("External User");
									userRole.setRoleName(externalRole);
									userRole.setSource(UserCreationSource.EXTERNAL.toString());
									userRole = rolesService.save(userRole);

									groupPermissionService.loadZeroPermissionsForRole(userRole);
									role = userRole;
								}
								UserRoles userRole = new UserRoles(role, user);
								userRolesService.save(userRole);
							} else {
								role = rolesService.findRoleByName(ElementConstants.ROLE_NAME_USER);
								UserRoles userRole = new UserRoles(role, user);
								userRolesService.save(userRole);
							}
							List<Modules> allModulesList = modulesService.findAll();
							List<UserMenu> allMenu = menuItemService.findAll();

							if (allModulesList != null && allModulesList.size() > 0) {
								for (Modules module : allModulesList) {
									// if (singleUserMenu == null ||
									// singleUserMenu.size() == 0) {
									UserMenu newMenu = new UserMenu();
									newMenu.setClicksCount(0);
									newMenu.setMenuItemSize(MenuSizeEnum.SMALL.toString());
									newMenu.setModule(module);
									newMenu.setModuleGroup(module.getModuleGroup());
									newMenu.setUserId(user.getUserId());
									menuItemService.save(newMenu);
									// }
								}
							}

						} else {
							return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_CREATION_DISABLED),
									HttpStatus.UNAUTHORIZED);
						}
						return new ResponseEntity<>(new ResponseMessage("Verification code sent to the your registered email ID"),
								HttpStatus.OK);
					} else {
						return new ResponseEntity<>(ElementConstants.USER_NOT_FOUND_MSG, HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<>(ElementConstants.USER_NOT_FOUND_MSG, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG),
						HttpStatus.OK);
			}

		} else {
			return new ResponseEntity<>(new ResponseMessage("token can not be null or empty"), HttpStatus.UNAUTHORIZED);
		}
	}
	
	@ApiOperation("Verifies the Two Factor Auth Code the password")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResetPasswordDto.class, message = "Password changed successfully"),
			@ApiResponse(code = 404, response = ResetPasswordDto.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResetPasswordDto.class, message = "Users doesn`t exist") })
	@PostMapping(value = "/verifyTwoFACode", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> verifyTwoFACode(@RequestParam String verifCode,@RequestParam String uuid,
			HttpServletRequest request) throws Exception {
		
		Users user = usersService.getFirebaseUserBySourceId(uuid);
		if(user != null){
			if(user.getTemporaryPassword().equals(verifCode) ){
				if(null != user.getTemporaryPassword() && null != user.getTemporaryPasswordExpiresOn() ){
					Long currentTime = new Date().getTime();
					Long expiryTime = user.getTemporaryPasswordExpiresOn().getTime();
					if(currentTime<expiryTime){
						user.setTemporaryPassword(null);
						user.setTemporaryPasswordExpiresOn(null);
						return new ResponseEntity<>(new ResponseMessage("Code verified"), HttpStatus.OK);
					}else{
						return new ResponseEntity<>(new ResponseMessage("the verification code you are trying is already expired!"), HttpStatus.UNAUTHORIZED);
					}
				}else{
					return new ResponseEntity<>(new ResponseMessage("the verification code not found!"), HttpStatus.UNAUTHORIZED);
				}
				
			}else{
				return new ResponseEntity<>(new ResponseMessage("verification does not match!"), HttpStatus.UNAUTHORIZED);
			}
		}else{
			return new ResponseEntity<>(ElementConstants.USER_NOT_FOUND_MSG, HttpStatus.UNAUTHORIZED);
		}

	}
	
	//Duplicate validate email password with 2fA implementation
	@ApiOperation("Validates email and password of the user")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Users account deactivated/locked or username/password is incorrect"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not get user from admin group") })
	@PostMapping(value = "/validateEmailPassword1", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> validateEmailPassword1(String emailAddress, String password, HttpServletRequest request)
			throws Exception {
		
		/* -------------------	Authentication with firebase start-------------- */
		JSONObject response = elementsSecurityService.getFirebaseAccountInfoByToken(password);
		
		if(response != null && response.has("users")){
			UserInfo userInfo = new UserInfo();
			userInfo.setToken(password);
			
			userInfo.setLang("en");
			userInfo.setToken(password);
			userInfo.setDefaultUrl(settingsService.getDeaultUrl());
			org.json.JSONArray userArray = response.getJSONArray("users");
			if(userArray.length()>0){
				JSONObject userObject = (JSONObject) userArray.get(0);
				
				//check existing user with localId or create new user with localId
				if(userObject.has("localId")){
					servletContext.setAttribute(userObject.getString("localId"), password);
					Users user = usersService.getFirebaseUserBySourceId(userObject.get("localId").toString());
					
					if(user != null){
						if(null != user.getLastLoginDate()){
							userInfo.setLastLoginDate(user.getLastLoginDate());
						}else{
							userInfo.setLastLoginDate(new Date());
						}
						user.setPassword(userObject.getString("localId"));
						user.setTemporaryPassword(null);
						usersService.saveOrUpdate(user);
						userInfo.setUserId(user.getUserId());
					}else if(settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow 3rd Party synchronization-Users")
								.equalsIgnoreCase("On")){
					//user = usersService.createFirebaseUserBySourceId(userObject);
					
					//if we get a role from the authentication provider check or create a role
					/*Roles role = null;
					if(userObject.has("role")){
						String externalRole = userObject.getString("role");
						role = rolesService.findRoleByName(externalRole);
						if(null == role && 
								settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow 3rd Party synchronization-Roles")
								.equalsIgnoreCase("On")){
							//create a role with the external role if role not created earlier
							Roles userRole = new Roles();
							userRole.setCreatedOn(new Date());
							userRole.setDescription("External user role.");
							userRole.setNotes("External User");
							userRole.setRoleName(externalRole);
							userRole.setSource(UserCreationSource.EXTERNAL.toString());
							userRole = rolesService.save(userRole);
							
							groupPermissionService.loadZeroPermissionsForRole(userRole);
							role = userRole;
						}
						UserRoles userRole = new UserRoles(role, user);
						userRolesService.save(userRole);
					}else{
						role = rolesService.findRoleByName(ElementConstants.ROLE_NAME_USER);
						UserRoles userRole = new UserRoles(role, user);
						userRolesService.save(userRole);
					}*/
					
					
					
					/*List<Modules> allModulesList = modulesService.findAll();
					List<UserMenu> allMenu = menuItemService.findAll();

					if (allModulesList != null && allModulesList.size() > 0) {
						for (Modules module : allModulesList) {

							
						//	if (singleUserMenu == null || singleUserMenu.size() == 0) {
								UserMenu newMenu = new UserMenu();
								newMenu.setClicksCount(0);
								newMenu.setMenuItemSize(MenuSizeEnum.SMALL.toString());
								newMenu.setModule(module);
								newMenu.setModuleGroup(module.getModuleGroup());
								newMenu.setUserId(user.getUserId());
								menuItemService.save(newMenu);
							//}
						}
					}
					*/
					userInfo.setUserId(user.getUserId());
					
					}else{
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND),
								HttpStatus.UNAUTHORIZED);
					}
				}else{
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND),
							HttpStatus.UNAUTHORIZED);
				}
				
				
				if(userObject.has("email"))
					userInfo.setEmail(userObject.getString("email"));
				if(userObject.has("displayName")){
					userInfo.setFullName(userObject.getString("displayName"));
				}else if(userObject.has("email")){
					userInfo.setFullName(getDisplaNameFromEmail(userObject.getString("email")));
				}
					
				/*if(userObject.has("localId"))
					userInfo.setUserId(userObject.get("localId"));*/
				/*if(userObject.has("photoUrl"))
					userInfo.setUserImage(userObject.get("photoUrl"));*/
				
				JSONObject decodedJwtBody = JWTDecoder.getDecodedJWTBody(password);
				response.put("accessToken", password);
				response.put("authTime", Long.valueOf(decodedJwtBody.get("auth_time").toString()));
				response.put("expireTime", Long.valueOf(decodedJwtBody.get("exp").toString()));
				
				List<String> attributes = Collections.list(servletContext.getAttributeNames());
				if(!attributes.contains(password))	{
					servletContext.setAttribute(password, response);
					
				}
					
				
			}
			request.getSession().setAttribute("userInfo", userInfo);
			/*if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
					ElementConstants.TWO_WAY_AUTHENTICATION_STRING).equalsIgnoreCase("On")) {
				if (response != null && response.has("users")) {
					org.json.JSONArray userArray1 = response.getJSONArray("users");
					if (userArray1.length() > 0) {
						JSONObject userObject = (JSONObject) userArray1.get(0);
						usersService.updateFirebaseUserBySourceId(userObject);
					}

				}
			}*/
			return new ResponseEntity<>(userInfo, HttpStatus.OK);
		}else{
			List<String> attributes = Collections.list(servletContext.getAttributeNames());
			if(attributes.contains(password))	
				servletContext.removeAttribute(password);
			/*if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
					ElementConstants.TWO_WAY_AUTHENTICATION_STRING).equalsIgnoreCase("On")) {
				if (response != null && response.has("users")) {
					org.json.JSONArray userArray1 = response.getJSONArray("users");
					if (userArray1.length() > 0) {
						JSONObject userObject = (JSONObject) userArray1.get(0);
						usersService.updateFirebaseUserBySourceId(userObject);
					}

				}
			}*/
			return new ResponseEntity<>(new ResponseMessage("Either username or password is incorrect."),
					HttpStatus.UNAUTHORIZED);
		}
		/* -------------------	Authentication with firebase end-------------- */
	}
	
	
}
