package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.ClassificationsDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.ClassificationsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author suresh
 *
 */
@Service("classificationsService")
public class ClassificationsServiceImpl extends GenericServiceImpl<Classifications, Long>
		implements ClassificationsService {

	@Autowired
	public ClassificationsServiceImpl(GenericDao<Classifications, Long> genericDao) {
		super(genericDao);
	}

	public ClassificationsServiceImpl() {
	}

	@Autowired
	ClassificationsDao classificationsDao;

	@Override
	@Transactional("transactionManager")
	public List<ClassificationsDto> getClassifications() {
		List<Classifications> classificationList = classificationsDao.findAll();
		List<ClassificationsDto> sourcesDtoList = new ArrayList<ClassificationsDto>();
		if (classificationList != null) {
			sourcesDtoList = classificationList.stream()
					.map((sourcesDto) -> new EntityConverter<Classifications, ClassificationsDto>(Classifications.class,
							ClassificationsDto.class).toT2(sourcesDto, new ClassificationsDto()))
					.collect(Collectors.toList());
		}
		return sourcesDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public Classifications updateSubClassifcationMedia(ClassificationsDto classifcationsDto) {
		Classifications classifications = classificationsDao.find(classifcationsDto.getClassificationId());
		if (classifications.getClassificationId() != null) {
			for (SubClassifications subClassifications : classifications.getSubClassifications()) {
				subClassifications.getDataAttributes().forEach(attribute -> {
					attribute.setSubClassifications(subClassifications);
				});
			}
		}
		return classifications;
	}

	@Override
	@Transactional("transactionManager")
	public Classifications fetchClassification(String name) {
		return classificationsDao.fetchClassification(name);
	}

	@Override
	@Transactional("transactionManager")
	public List<Classifications> findAllWithSubclassifications() {
		List<Classifications> classifications = classificationsDao.findAll();
		classifications.stream()
				.forEach(classification -> Hibernate.initialize(classification.getSubClassifications()));
		return classifications;
	}
}
