package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.DataAttributesDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.DataAttributes;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("dataAttributesDao")
public class DataAttributesDaoImpl extends GenericDaoImpl<DataAttributes, Long> implements DataAttributesDao {

	public DataAttributesDaoImpl() {
		super(DataAttributes.class);
	}

}
