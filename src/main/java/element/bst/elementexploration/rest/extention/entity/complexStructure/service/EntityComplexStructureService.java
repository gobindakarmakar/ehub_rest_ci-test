package element.bst.elementexploration.rest.extention.entity.complexStructure.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.entity.complexStructure.domain.EntityComplexStructure;
import element.bst.elementexploration.rest.extention.entity.complexStructure.dto.EntityComplexStructureDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author hanuman
 *
 */
public interface EntityComplexStructureService extends GenericService<EntityComplexStructure, Long> {

	List<EntityComplexStructureDto> getAllEntityComplexStructure();

	EntityComplexStructureDto getEntityComplexStructure(String entityId,String sorceEntity,String entityName);

	EntityComplexStructureDto saveOrUpdateEntityComplexStructure(String entityId,String entityName,String entitySource,boolean isComplexStructure,Long userIdTemp)
			throws Exception;

	EntityComplexStructureDto saveEntityComplexStructure(EntityComplexStructureDto entityComplexStructureDto)
			throws Exception;

}
