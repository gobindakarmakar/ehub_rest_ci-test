package element.bst.elementexploration.rest.document.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Document comment Vo")
public class DocCommentVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="commentId  of the DocComment requested")
	private Long commentId;
	
	@ApiModelProperty(value="docId of the DocComment requested")
	private Long docId;
	
	@ApiModelProperty(value="title of the DocComment requested",required=true)
	@NotEmpty(message="title can not be null")
	private String title;
	
	@ApiModelProperty(value="description of the DocComment requested",required=true)
	@NotEmpty(message="description can not be null")
	private String description;
	
	@ApiModelProperty(value="Modification Reason of the DocComment requested")
	private String modificationReason;

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModificationReason() {
		return modificationReason;
	}

	public void setModificationReason(String modificationReason) {
		this.modificationReason = modificationReason;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
