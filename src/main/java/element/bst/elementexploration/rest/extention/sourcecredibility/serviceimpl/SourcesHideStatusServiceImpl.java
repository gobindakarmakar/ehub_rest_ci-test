package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourcesHideStatusDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesHideStatusDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesHideStatusService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author suresh
 *
 */
@Service("sourceHideStatusService")
public class SourcesHideStatusServiceImpl extends GenericServiceImpl<SourcesHideStatus, Long>
		implements SourcesHideStatusService {

	@Autowired
	public SourcesHideStatusServiceImpl(GenericDao<SourcesHideStatus, Long> genericDao) {
		super(genericDao);
	}

	public SourcesHideStatusServiceImpl() {
	}

	@Autowired
	SourcesHideStatusDao sourcesHideStatusDao;

	/*@Override
	@Transactional("transactionManager")
	public SourcesHideStatusDto getSourceHideStatus(Long sourceId, Long classificationId) {
		SourcesHideStatusDto sourcesHideStatusDto = new SourcesHideStatusDto();
		SourcesHideStatus sourcesHideStatus = sourcesHideStatusDao.getSourceHideStatus(sourceId, classificationId);
		BeanUtils.copyProperties(sourcesHideStatus, sourcesHideStatusDto);
		return sourcesHideStatusDto;
	}*/

	@Override
	@Transactional("transactionManager")
	public List<SourcesHideStatusDto> getSourceHideStatusByClassification(Long classificationId) {
		List<SourcesHideStatus> listHideStatus = sourcesHideStatusDao.getSourceHideStatusByClassification(classificationId);
		List<SourcesHideStatusDto> listDto = new ArrayList<SourcesHideStatusDto>();
		if (listHideStatus != null) {
			listDto = listHideStatus.stream()
					.map((sourcesDto) -> new EntityConverter<SourcesHideStatus, SourcesHideStatusDto>(SourcesHideStatus.class,
							SourcesHideStatusDto.class).toT2(sourcesDto, new SourcesHideStatusDto()))
					.collect(Collectors.toList());
		}
		return listDto;
	}

}
