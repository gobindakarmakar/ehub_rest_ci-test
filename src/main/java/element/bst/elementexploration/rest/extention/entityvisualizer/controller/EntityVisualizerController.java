package element.bst.elementexploration.rest.extention.entityvisualizer.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.entityvisualizer.service.EntityVisualizerService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Entity visualizer API" },description="Manages entity visualizer data")
@RestController
@RequestMapping("/api/entityVisualizer")
public class EntityVisualizerController extends BaseController{

	@Autowired
	EntityVisualizerService entityVisualizerService;

	@ApiOperation("Gets queried entity data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/ent", consumes ={"application/json; charset=UTF-8" }, 
			produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntData(@RequestBody String payload, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(entityVisualizerService.getEntData(payload), HttpStatus.OK);
	}

}
