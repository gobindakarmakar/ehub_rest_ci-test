package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author Prateek
 *
 */
public class ALertsCSVDto implements Serializable{


	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "created date for Alert", required = false)
	private Date createdDate;
	
	@ApiModelProperty(value = "Alert Id", required = true)
	@NotEmpty(message = "alert id can not be blank.")
	private Long alertId;
	
	@ApiModelProperty(value = "Customer Id for Alert", required = false)
	private String customerId;
	
	@ApiModelProperty(value = "Watchlist for Alert", required = false)
	private String watchList;
	
	@ApiModelProperty(value = "Entity name for Alert", required = false)
	private String entityName;
	
	@ApiModelProperty(value = "Confidence Level for Alert", required = false)
	private Double confidenceLevel;
	
	@ApiModelProperty(value = "feed for Alert", required = false)
	private String feed;
	
	@ApiModelProperty(value = "group level for Alert", required = false)
	private String groupLevel;
	
	@ApiModelProperty(value = "Assignee for Alert", required = false)
	private String asignee;
	
	@ApiModelProperty(value = "Status of Alert", required = false)
	private String statuse;
	
	@ApiModelProperty(value = "Risk indicator for Alert", required = false)
	private String riskIndicators;

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getWatchList() {
		return watchList;
	}

	public void setWatchList(String watchList) {
		this.watchList = watchList;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Double getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(Double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public String getGroupLevel() {
		return groupLevel;
	}

	public void setGroupLevel(String groupLevel) {
		this.groupLevel = groupLevel;
	}

	public String getAsignee() {
		return asignee;
	}

	public void setAsignee(String asignee) {
		this.asignee = asignee;
	}

	public String getStatuse() {
		return statuse;
	}

	public void setStatuse(String statuse) {
		this.statuse = statuse;
	}

	public String getRiskIndicators() {
		return riskIndicators;
	}

	public void setRiskIndicators(String string) {
		this.riskIndicators = string;
	}
}
