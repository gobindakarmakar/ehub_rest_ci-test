package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_um_roles")
@Proxy(lazy=false)
public class Roles implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Role ID")
	@Id
	@Column(name = "roleId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long roleId;

	@ApiModelProperty(value = "Role Name")
	@Column(name = "roleName")
	private String roleName;
	
	@ApiModelProperty(value = "Icon for each role")
	@Column(name = "icon")
	private String icon;
	
	@ApiModelProperty(value = "Color for each role")
	@Column(name = "color")
	private String color;

	@ApiModelProperty(value = "Role Description")
	@Column(name = "description", columnDefinition = "LONGTEXT")
	private String description;

	@ApiModelProperty(value = "Role notes")
	@Column(name = "notes", columnDefinition = "LONGTEXT")
	private String notes;
	
	@ApiModelProperty(value = "Created On")
	@Column(name = "createdOn")
	private Date createdOn;
	
	@ApiModelProperty(value = "Modified On")
	@Column(name = "modifiedOn")
	private Date modifiedOn;

	@ApiModelProperty(value = "Source of role creation")
	@Column(name = "source")
	private String source;
	
	@ApiModelProperty(value = "Is Modifiable")
	@Column(name = "isModifiable", columnDefinition = "boolean default true", nullable = false)
	private Boolean isModifiable=true;
	
	@ApiModelProperty(value = "Is Removable")
	@Column(name = "isRemovable", columnDefinition = "boolean default true", nullable = false)
	private Boolean isRemovable=true;
	
	@JsonIgnore
	@ApiModelProperty(value = "User Roles")
	@OneToMany(mappedBy = "roleId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY,orphanRemoval=true)
	private List<UserRoles> userRoles;
	
	@JsonIgnore
	@ApiModelProperty(value = "Group Permissions")
	@OneToMany(mappedBy = "roleId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<GroupPermission> groupPermissions;
	
	@JsonIgnore
	@ApiModelProperty(value = "Role Groups")
	@OneToMany(mappedBy = "roleId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<RoleGroup> roleGroups;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<GroupPermission> getGroupPermissions() {
		return groupPermissions;
	}

	public void setGroupPermissions(List<GroupPermission> groupPermissions) {
		this.groupPermissions = groupPermissions;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getIsModifiable() {
		return isModifiable;
	}

	public void setIsModifiable(Boolean isModifiable) {
		this.isModifiable = isModifiable;
	}

	public List<RoleGroup> getRoleGroups() {
		return roleGroups;
	}

	public void setRoleGroups(List<RoleGroup> roleGroups) {
		this.roleGroups = roleGroups;
	}

	public Boolean getIsRemovable() {
		return isRemovable;
	}

	public void setIsRemovable(Boolean isRemovable) {
		this.isRemovable = isRemovable;
	}

	
		
}
