package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@ApiModel("Users list")
public class UsersListDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="Users list")
	private List<UserDto> result;

	@ApiModelProperty("Page information")
	private PaginationInformation paginationInformation;

	public List<UserDto> getResult() {
		return result;
	}

	public void setResult(List<UserDto> result) {
		this.result = result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}

}
