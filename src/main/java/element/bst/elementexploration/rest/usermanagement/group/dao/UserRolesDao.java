package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.Map;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;

/**
 * @author Paul Jalagari
 *
 */
public interface UserRolesDao extends GenericDao<UserRoles, Long> {

	Map<Long, Long> rolesWithUserCount();

	Map<Long, Long> getActiveUserNRoleCount(Long statusId);

	Object getStatuswiseUsersByRoleId(Long roleId);

	UserRoles getUserRoleByRoleAndUserId(Long roleId, Long userId);

	Map<Long, Long> getUsersCountByRole(Long roleId);

	boolean isUserRolesExist(Long userId, Long roleId);	

}
