package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonProperty;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;

/**
 * @author suresh
 *
 */
public class CounterPartyDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private String customerNumer;
	private CustomerDetails point;
	private long count;
	@JsonProperty("min")
	private double minimum;
	@JsonProperty("max")
	private double maximum;

	private double sum;
	@JsonProperty("avg")
	private double average;

	public CounterPartyDto() {
		super();
	}

	public CounterPartyDto(String customerNumer, CustomerDetails point, long count, double minimum, double maximum,
			double sum, double average) {
		super();
		this.customerNumer = customerNumer;
		this.point = point;
		this.count = count;
		this.minimum = minimum;
		this.maximum = maximum;
		this.sum = sum;
		this.average = average;
	}

	public CounterPartyDto(String customerNumer, long count, double minimum, double maximum, double sum,
			double average) {
		super();
		this.customerNumer = customerNumer;
		this.count = count;
		this.minimum = minimum;
		this.maximum = maximum;
		this.sum = sum;
		this.average = average;
	}

	public String getCustomerNumer() {
		return customerNumer;
	}

	public void setCustomerNumer(String customerNumer) {
		this.customerNumer = customerNumer;
	}

	public CustomerDetails getPoint() {
		return point;
	}

	public void setPoint(CustomerDetails point) {
		this.point = point;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public double getMinimum() {
		return minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public double getMaximum() {
		return maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

}
