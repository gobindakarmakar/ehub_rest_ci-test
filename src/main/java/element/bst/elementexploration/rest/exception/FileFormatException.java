package element.bst.elementexploration.rest.exception;

/**
 * @author 
 *
 */
public class FileFormatException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String message;
    private FileFormatException exp;
    public static final String jsonExp = "File type mismatch";

    /**
     * 
     */
    public FileFormatException() {
	super();
	message = jsonExp;
    }


    /**
     * @param message
     */
    public FileFormatException(String message) {
	super(message);
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
    
    public FileFormatException getExp(){
	return exp;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "JsonFormatEception [message=" + message +  "]";
    }


}
