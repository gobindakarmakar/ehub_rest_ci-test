package element.bst.elementexploration.rest.extention.advancesearch.daoimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.advancesearch.dao.EntityQuestionsDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityQuestions;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.dao.SettingsDao;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author rambabu
 *
 */
@Repository("entityQuestionsDao")
public class EntityQuestionsDaoImpl extends GenericDaoImpl<EntityQuestions, Long> implements EntityQuestionsDao {

	@Autowired
	SettingsService settingsService;

	public EntityQuestionsDaoImpl() {
		super(EntityQuestions.class);
	}

	@Autowired
	SettingsDao settingsDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<EntityQuestions> getEntityQuestionByEntityId(String entityId, Long surveyId) {
		List<Object[]> entityQuestionsList = new ArrayList<Object[]>();
		List<EntityQuestions> entityQuestionsListObj = new ArrayList<EntityQuestions>();
		List<Settings> settingsList = settingsDao.getMailSettings("GENERAL_SETTING");
		StringBuilder hql = new StringBuilder();
		if ((settingsList.stream().filter(se -> se.getName().equalsIgnoreCase("Allow checklist"))
				.filter(se -> se.getDefaultValue().equalsIgnoreCase("on")).findAny().orElse(null) != null)
				&& (settingsList.stream().filter(se -> se.getName().equalsIgnoreCase("Report Comments"))
						.filter(se -> se.getDefaultValue().equalsIgnoreCase("Showing the latest Comments")).findAny()
						.orElse(null) != null))

		{
			hql.append("select * from entity_questions eq where eq.created_by="
					+ "(SELECT eq1.created_by FROM entity_questions AS eq1 WHERE eq1.created_date ="
					+ " (select Max(eq2.created_date) from entity_questions as eq2 where eq2.entity_id =:entityId AND eq2.survey_id =:surveyId) limit 1) AND eq.entity_id = :entityId AND eq.survey_id = :surveyId");
		} else {
			hql.append("select * from entity_questions eq where eq.entity_id =:entityId and eq.survey_id = :surveyId");
		}

		try {
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("surveyId", surveyId);
			entityQuestionsList = (List<Object[]>) query.getResultList();
			for (int i = entityQuestionsList.size() - 1; i >= 0; i--) {

				EntityQuestions entityQuestions = new EntityQuestions();
				if (entityQuestionsList.get(i)[0] != null)
					entityQuestions.setId((Long.parseLong(entityQuestionsList.get(i)[0].toString())));
				if (entityQuestionsList.get(i)[1] != null)
					entityQuestions.setCreatedBy((Long.parseLong(entityQuestionsList.get(i)[1].toString())));
				if (entityQuestionsList.get(i)[2] != null)
					entityQuestions.setCreatedDate((Date) (entityQuestionsList.get(i)[2]));
				if (entityQuestionsList.get(i)[3] != null)
					entityQuestions.setEntityId(entityQuestionsList.get(i)[3].toString());
				if (entityQuestionsList.get(i)[4] != null)
					entityQuestions.setGroupName(entityQuestionsList.get(i)[4].toString());
				if (entityQuestionsList.get(i)[5] != null)
					entityQuestions.setQuestionAnswer(entityQuestionsList.get(i)[5].toString());
				if (entityQuestionsList.get(i)[6] != null)
					entityQuestions.setQuestionDivId(entityQuestionsList.get(i)[6].toString());
				if (entityQuestionsList.get(i)[7] != null)
					entityQuestions.setQuestionName(entityQuestionsList.get(i)[7].toString());
				if (entityQuestionsList.get(i)[8] != null)
					entityQuestions.setQuestionOptions(entityQuestionsList.get(i)[8].toString());
				if (entityQuestionsList.get(i)[9] != null)
					entityQuestions.setQuestionType(entityQuestionsList.get(i)[9].toString());
				if (entityQuestionsList.get(i)[10] != null)
					entityQuestions.setSchemaAttribute(entityQuestionsList.get(i)[10].toString());
				if (entityQuestionsList.get(i)[11] != null)
					entityQuestions.setSchemaAttributeValue(entityQuestionsList.get(i)[11].toString());
				if (entityQuestionsList.get(i)[12] != null)
					entityQuestions.setSurveyId(Long.parseLong((entityQuestionsList.get(i)[12]).toString()));
				entityQuestionsListObj.add(entityQuestions);

			}
			return entityQuestionsListObj;
		} catch (Exception e) {
			return entityQuestionsListObj;
		}

	}

	@Override
	public EntityQuestions getEntityQuestionByNameAndEntityId(String entityId, String questionDivId, Long userId,
			Long surveyId) {
		EntityQuestions entityQuestion = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"from EntityQuestions eq where eq.entityId =:entityId and eq.questionDivId =:questionDivId and eq.createdBy=:userId and eq.surveyId=:surveyId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("questionDivId", questionDivId);
			query.setParameter("userId", userId);
			query.setParameter("surveyId", surveyId);
			entityQuestion = (EntityQuestions) query.getSingleResult();
			return entityQuestion;
		} catch (Exception e) {
			return entityQuestion;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EntityQuestions> getEntityQuestionByEntityIdAndUserId(String entityId, Long userId, Long surveyId) {
		List<Object[]> entityQuestionsList = new ArrayList<Object[]>();
		List<EntityQuestions> entityQuestionsListObj = new ArrayList<EntityQuestions>();
		List<Settings> settingsList = settingsDao.getMailSettings("GENERAL_SETTING");
		StringBuilder hql = new StringBuilder();
		boolean isStream = false;
		if ((settingsList.stream().filter(se -> se.getName().equalsIgnoreCase("Allow checklist"))
				.filter(se -> se.getDefaultValue().equalsIgnoreCase("on")).findAny().orElse(null) != null)
				&& (settingsList.stream().filter(se -> se.getName().equalsIgnoreCase("Report Comments"))
						.filter(se -> se.getDefaultValue().equalsIgnoreCase("Showing the latest Comments")).findAny()
						.orElse(null) != null)) {
			hql.append("select * from entity_questions eq where eq.created_by="
					+ "(SELECT eq1.created_by FROM entity_questions AS eq1 WHERE eq1.created_date ="
					+ " (select Max(eq2.created_date) from entity_questions as eq2 where eq2.entity_id =:entityId AND eq2.survey_id =:surveyId) limit 1) AND eq.entity_id=:entityId AND eq.survey_id=:surveyId");
			isStream = true;
		} else {
			hql.append(
					"select * from entity_questions eq where eq.entity_id =:entityId and eq.created_by=:userId and eq.survey_id=:surveyId");
		}

		try {
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("surveyId", surveyId);
			if (!isStream)
				query.setParameter("userId", userId);
			entityQuestionsList = (List<Object[]>) query.getResultList();
			for (int i = entityQuestionsList.size() - 1; i >= 0; i--) {

				EntityQuestions entityQuestions = new EntityQuestions();
				if (entityQuestionsList.get(i)[0] != null)
					entityQuestions.setId((Long.parseLong(entityQuestionsList.get(i)[0].toString())));
				if (entityQuestionsList.get(i)[1] != null)
					entityQuestions.setCreatedBy((Long.parseLong(entityQuestionsList.get(i)[1].toString())));
				if (entityQuestionsList.get(i)[2] != null)
					entityQuestions.setCreatedDate((Date) (entityQuestionsList.get(i)[2]));
				if (entityQuestionsList.get(i)[3] != null)
					entityQuestions.setEntityId(entityQuestionsList.get(i)[3].toString());
				if (entityQuestionsList.get(i)[4] != null)
					entityQuestions.setGroupName(entityQuestionsList.get(i)[4].toString());
				if (entityQuestionsList.get(i)[5] != null)
					entityQuestions.setQuestionAnswer(entityQuestionsList.get(i)[5].toString());
				if (entityQuestionsList.get(i)[6] != null)
					entityQuestions.setQuestionDivId(entityQuestionsList.get(i)[6].toString());
				if (entityQuestionsList.get(i)[7] != null)
					entityQuestions.setQuestionName(entityQuestionsList.get(i)[7].toString());
				if (entityQuestionsList.get(i)[8] != null)
					entityQuestions.setQuestionOptions(entityQuestionsList.get(i)[8].toString());
				if (entityQuestionsList.get(i)[9] != null)
					entityQuestions.setQuestionType(entityQuestionsList.get(i)[9].toString());
				if (entityQuestionsList.get(i)[10] != null)
					entityQuestions.setSchemaAttribute(entityQuestionsList.get(i)[10].toString());
				if (entityQuestionsList.get(i)[11] != null)
					entityQuestions.setSchemaAttributeValue(entityQuestionsList.get(i)[11].toString());
				if (entityQuestionsList.get(i)[12] != null)
					entityQuestions.setSurveyId(Long.parseLong((entityQuestionsList.get(i)[12]).toString()));
				entityQuestionsListObj.add(entityQuestions);

			}
			return entityQuestionsListObj;
		} catch (Exception e) {
			return entityQuestionsListObj;
		}

	}

	@Override
	public void deleteExistingEntityAnswersByUser(String entityId, Long surveyId, Long userId) throws Exception {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"delete EntityQuestions eq where eq.entityId =:entityId and eq.createdBy=:userId and eq.surveyId=:surveyId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("userId", userId);
			query.setParameter("surveyId", surveyId);
			query.executeUpdate();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to delete earlier records. Reason : " + e.getMessage());
		}
	}

}
