package element.bst.elementexploration.rest.extention.widgetreview.dao;

import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author rambabu
 *
 */
public interface ComplianceWidgetDao extends GenericDao<ComplianceWidget, Long> {

}
