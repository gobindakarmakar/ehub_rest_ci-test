package element.bst.elementexploration.rest.util;

import java.io.Serializable;
import java.util.List;

public class ResponseUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4141902005108846343L;

	private List<?> result;

	private PaginationInformation paginationInformation;

	private List<String> types;

	public ResponseUtil() {
		// TODO Auto-generated constructor stub
	}

	public ResponseUtil(List<String> types) {
		this.types = types;
	}

	public List<?> getResult() {
		return result;
	}

	public void setResult(List<?> result) {
		this.result = result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}

}
