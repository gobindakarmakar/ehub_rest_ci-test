package element.bst.elementexploration.test.usermanagement.security.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;

import element.bst.elementexploration.rest.exception.AuthenticationFailedException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.security.dao.ElementsSecurityDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.security.service.FailedLoginService;
import element.bst.elementexploration.rest.usermanagement.security.service.LoginHistoryService;
import element.bst.elementexploration.rest.usermanagement.security.service.MailService;
import element.bst.elementexploration.rest.usermanagement.security.serviceImpl.ElementsSecurityServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ElementSecurityServiceTest {

	@Mock
	ElementsSecurityDao elementSecurityDao;
	
	@Mock
	UsersService userService;
	
	@Mock
	MailService mailService;
	
	@Mock
	Environment env;
	
	@Mock
	FailedLoginService failedLoginService;
	
	@Mock
	LoginHistoryService loginHistoryService;
	
	@Mock
	ElementsSecurityService elementSecurityServiceMock;
	
	@Mock
	PasswordEncoder passwordEncoder;
	
	@Mock
	ListItem listItem;

	@InjectMocks
	ElementsSecurityServiceImpl elementSecurityService;

	public Users user;

	public UsersToken userToken;

	@Before
	public void doSetup() {
		user = new Users("analyst", "Analyst", "Analyst", new Date(), listItem);
		userToken = new UsersToken();
		userToken.setUserId(user);
		userToken.setCreatedOn(new Date());
		userToken.setToken("token");
		userToken.setExpireOn(new Date());;
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getUserUserFromTokenTest() {
		Mockito.when(elementSecurityDao.isValidToken(Mockito.anyString())).thenReturn(true);
		Mockito.when(elementSecurityDao.getUserToken(Mockito.anyString())).thenReturn(userToken);
		Users validUser = elementSecurityService.getUserUserFromToken("token");
		Assert.assertNotNull(validUser.getScreenName());
	}

	@Test
	public void withNullTokenTest() {
		Users validUser = elementSecurityService.getUserUserFromToken(null);
		assertEquals(null, validUser);
	}

	@Test
	public void withInvalidTokenTest() {
		Mockito.when(elementSecurityDao.isValidToken(Mockito.anyString())).thenReturn(false);
		Users validUser = elementSecurityService.getUserUserFromToken("a1b2c3d4");
		assertEquals(null, validUser);
	}

	@Test
	public void withUserTokenNullTest() {
		Mockito.when(elementSecurityDao.isValidToken(Mockito.anyString())).thenReturn(true);
		Mockito.when(elementSecurityDao.getUserToken(Mockito.anyString())).thenReturn(null);
		Users validUser = elementSecurityService.getUserUserFromToken("token");
		assertEquals(null, validUser);
	}
	
	@Test
	public void getUserTokenTest(){
		Mockito.when(elementSecurityDao.isTokenExist(Mockito.anyLong())).thenReturn(userToken);
		UsersToken newUserToken = elementSecurityService.getUserToken(1L);
		Assert.assertNotNull(newUserToken.getUserId());
	}
	
	@Test(expected = AuthenticationFailedException.class)
	public void validateEmailPassword_ThrowExTest() throws Exception{
		Mockito.when(failedLoginService.isAccountLocked(Mockito.anyString())).thenReturn(false);
		Mockito.when(userService.getUserObjectByEmailOrUsername(Mockito.anyString())).thenReturn(user);
		//Mockito.doNothing().when(elementSecurityServiceMock).saveToken(Mockito.any(User.class));
		//Mockito.doNothing().when(failedLoginService).releaseAccountLocked(Mockito.anyString(),Mockito.anyString());
		String responseMessage = elementSecurityService.validateEmailPassword("analyst", "analyst", "10.10.1.0");
		Assert.assertNotNull(responseMessage);
	}
	
	@Test(expected = InsufficientDataException.class)
	public void whenEmailOrPasswordNull_validateEmailPassword_ThrowExTest() throws Exception{
		 elementSecurityService.validateEmailPassword(null, null, "10.10.1.0");		
	}
	
	@Test(expected = AuthenticationFailedException.class)
	public void whenUserAccountLock_validateEmailPassword_ThrowExTest() throws Exception{
		Mockito.when(loginHistoryService.isAccountLocked(Mockito.anyString())).thenReturn(true);
		 elementSecurityService.validateEmailPassword("analyst", "analyst", "10.10.1.0");
		
	}
	
	/*@Test(expected = AuthenticationFailedException.class)
	public void whenUserNotFound_validateEmailPassword_ThrowExTest() throws Exception{
		Mockito.when(failedLoginService.isAccountLocked(Mockito.anyString())).thenReturn(false);
		Mockito.when(userService.getUserObjectByEmailOrUsername(Mockito.anyString())).thenReturn(null);
		 elementSecurityService.validateEmailPassword("analyst", "analyst", "10.10.1.0");
		
	}*/
	
	
	@Test
	public void saveTokenTest(){
		Mockito.when(elementSecurityDao.isTokenExist(Mockito.anyLong())).thenReturn(userToken);
		Mockito.doNothing().when(elementSecurityDao).saveOrUpdate(Mockito.any(UsersToken.class));
		String token = elementSecurityService.saveToken(user);
		Assert.assertNotNull(token);
	}
	
	@Test
	public void whenTokenAleradyExist_SaveTokenTest(){
		Mockito.when(elementSecurityDao.isTokenExist(Mockito.anyString())).thenReturn(false);
		Mockito.doNothing().when(elementSecurityDao).saveOrUpdate(Mockito.any(UsersToken.class));
		String token = elementSecurityService.saveToken(user);
		Assert.assertNotNull(token);
	}
	
	@Test
	public void whenWhenTokenNull_SaveTokenTest(){
		Mockito.when(elementSecurityDao.isTokenExist(Mockito.anyLong())).thenReturn(null);
		Mockito.doNothing().when(elementSecurityDao).saveOrUpdate(Mockito.any(UsersToken.class));
		String token = elementSecurityService.saveToken(user);
		Assert.assertNotNull(token);
	}
	
	
	
	@Test
	public void validateTemporaryPasswordTest(){
		Mockito.when(userService.find(Mockito.anyLong())).thenReturn(user);
		user.setTemporaryPassword("analyst");
		user.setTemporaryPasswordExpiresOn(new Date(System.currentTimeMillis() + 3600 * 1000));
		Mockito.when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		Mockito.when(userService.prepareUserFromFirebase(Mockito.any(Users.class))).thenReturn(user);
		boolean status = elementSecurityService.validateTemporaryPassword(1L,"test", "analyst");
		assertEquals(true, status);
	}
	
	@Test
	public void whenUserNull_ValidateTemporaryPasswordTest(){
		Mockito.when(userService.find(Mockito.anyLong())).thenReturn(null);		
		boolean status = elementSecurityService.validateTemporaryPassword(1L,null, "analyst");
		assertEquals(false, status);
	}
	
	@Test
	public void whenTemporaryPasswordNotMatched_ValidateTemporaryPasswordTest(){
		Mockito.when(userService.find(Mockito.anyLong())).thenReturn(user);
		user.setTemporaryPassword("test");
		user.setTemporaryPasswordExpiresOn(new Date(System.currentTimeMillis() + 3600 * 1000));
		boolean status = elementSecurityService.validateTemporaryPassword(1L,null, "analyst");
		assertEquals(false, status);
	}
	
	/*@Test
	public void sendEmailForPasswordReset(){
		Mockito.when(userService.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(env.getProperty(Mockito.anyString())).thenReturn("admin@gmail.com");
		Mockito.doNothing().when(mailService).sendEmail(Mockito.any(Mail.class));
		boolean status = elementSecurityService.sendEmailForPasswordReset(1L, "analyst", "http://localhost:8080", "a1d2c3d4", "analyst@gmail.com");
		assertEquals(true, status);		
	}
	
	@Test
	public void whenUserIsNull_SendEmailForPasswordReset(){
		Mockito.when(userService.find(Mockito.anyLong())).thenReturn(null);
		Mockito.when(env.getProperty(Mockito.anyString())).thenReturn("admin@gmail.com");
		Mockito.doNothing().when(mailService).sendEmail(Mockito.any(Mail.class));
		boolean status = elementSecurityService.sendEmailForPasswordReset(1L, "analyst", "http://localhost:8080", "a1d2c3d4", "analyst@gmail.com");
		assertEquals(false, status);		
	}*/

}
