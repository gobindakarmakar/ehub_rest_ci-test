package element.bst.elementexploration.rest.logging.service;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.logging.domain.AuditTrail;

/**
 * 
 * @author Amit Patel
 *
 */
public interface AuditTrailService extends GenericService<AuditTrail, Long>{

}
