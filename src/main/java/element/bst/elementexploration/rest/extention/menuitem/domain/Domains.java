package element.bst.elementexploration.rest.extention.menuitem.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_domains")
public class Domains implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Domain ID")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "domainId")
	private Long domainId;

	@ApiModelProperty(value = "Name of the Domain")
	@Column(name = "domainName", columnDefinition = "LONGTEXT")
	private String domainName;

	@ApiModelProperty(value = "Title of the domain")
	@Column(name = "domainTitle", columnDefinition = "LONGTEXT")
	private String domainTitle;

	@ApiModelProperty(value = "Title Localization ID")
	@Column(name = "titleLocalizationID")
	private Integer titleLocalizationID;

	@ApiModelProperty(value = "Body of the domain")
	@Column(name = "domainBody", columnDefinition = "LONGTEXT")
	private String domainBody;

	@ApiModelProperty(value = "Body Localization ID")
	@Column(name = "bodyLocalizationID")
	private Integer bodyLocalizationID;

	@ApiModelProperty(value = "Image of the Domain")
	@Column(name = "domainMediaUrl", columnDefinition = "LONGTEXT")
	private String domainMediaUrl;

	@ApiModelProperty(value = "Code of the Domain (for Permissions)")
	@Column(name = "domainCode", columnDefinition = "LONGTEXT")
	private String domainCode;

	@ApiModelProperty(value = "Privilege ID")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "permissionId")
	private Permissions permissionId;

	/*
	 * @ApiModelProperty(value = "Modules List")
	 * 
	 * @OneToMany(mappedBy = "domainId", cascade = { CascadeType.ALL }, fetch =
	 * FetchType.EAGER) private List<Modules> modules;
	 */

	/*@ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name = "bst_menu_domains", joinColumns = { @JoinColumn(name = "domainId") }, inverseJoinColumns = {
			@JoinColumn(name = "menuId") })*/
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "bst_menu_domains", joinColumns = {
			@JoinColumn(name = "domainId",referencedColumnName = "domainId") }, inverseJoinColumns = { @JoinColumn(name = "menuId",referencedColumnName = "menuId") })
	private List<Modules> modules;

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/*
	 * public List<Modules> getModules() { return modules; }
	 * 
	 * public void setModules(List<Modules> modules) { this.modules = modules; }
	 */

	public String getDomainTitle() {
		return domainTitle;
	}

	public void setDomainTitle(String domainTitle) {
		this.domainTitle = domainTitle;
	}

	public String getDomainBody() {
		return domainBody;
	}

	public void setDomainBody(String domainBody) {
		this.domainBody = domainBody;
	}

	public Permissions getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Permissions permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getTitleLocalizationID() {
		return titleLocalizationID;
	}

	public void setTitleLocalizationID(Integer titleLocalizationID) {
		this.titleLocalizationID = titleLocalizationID;
	}

	public Integer getBodyLocalizationID() {
		return bodyLocalizationID;
	}

	public void setBodyLocalizationID(Integer bodyLocalizationID) {
		this.bodyLocalizationID = bodyLocalizationID;
	}

	public String getDomainMediaUrl() {
		return domainMediaUrl;
	}

	public void setDomainMediaUrl(String domainMediaUrl) {
		this.domainMediaUrl = domainMediaUrl;
	}

	public List<Modules> getModules() {
		return modules;
	}

	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}

	public String getDomainCode() {
		return domainCode;
	}

	public void setDomainCode(String domainCode) {
		this.domainCode = domainCode;
	}

	
}
