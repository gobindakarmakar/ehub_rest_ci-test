package element.bst.elementexploration.rest.casemanagement.daoImpl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("caseDao")
public class CaseDaoImpl extends GenericDaoImpl<Case, Long> implements CaseDao {
	public CaseDaoImpl() {
		super(Case.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isCaseAccessibleOrOwner(Long caseId, Long userId) {
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(
					"select count(*) as count from bst_case_analyst_mapping where fk_case_id= :caseId and fk_analyst_id= :userId and isnull(modified_on) and current_status!= :status ");
			builder.append("union ");
			builder.append(
					"select count(*) as count from bst_case where created_by = :userId and pk_case_id = :caseId");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(builder.toString());
			query.setParameter("caseId", caseId);
			query.setParameter("userId", userId);
			query.setParameter("status", StatusEnum.REJECTED.getIndex());
			// Checking if data found in Owner table or mapping table.
			List<BigInteger> list = (List<BigInteger>) query.getResultList();
			BigInteger bigInteger = new BigInteger("0");
			for (BigInteger bigInt : list) {
				if (bigInt.compareTo(bigInteger) != 0)
					return true;
			}

		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not execute query ");
		}
		return false;
	}

	@Override
	public boolean isAnalystRejectedCaseBefore(Long analystId, Long caseId) {
		boolean flag = false;
		try {
			String hql = "select count(*) from CaseAnalystMapping cam where cam.caseSeed.caseId = :caseId and cam.user.userId = :analystId and cam.currentStatus =:status";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("caseId", caseId);
			query.setParameter("analystId", analystId);
			query.setParameter("status", StatusEnum.REJECTED.getIndex());
			long count = ((Number)query.getSingleResult()).longValue();
			flag = count == 0 ? false : true;
		} catch (NoResultException e) {
			return false;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hiberante exception ", e, CaseDaoImpl.class);
		}
		return flag;
	}

	@Override
	public Long assignCaseToAnalyst(Users user, Case bstCaseSeed) {
		Long mappingId = null;
		CaseAnalystMapping analystMapping = new CaseAnalystMapping();
		analystMapping.setCaseSeed(bstCaseSeed);
		analystMapping.setUser(user);
		analystMapping.setCreatedOn(new Date());
		analystMapping.setCurrentStatus(StatusEnum.SUBMITTED.getIndex());
		analystMapping.setAssignedBy(bstCaseSeed.getCreatedBy());
		mappingId = (Long) getCurrentSession().save(analystMapping);
		return mappingId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> fullTextSearchForCaseSeed(String caseSearchKeyword, Integer pageNumber, Integer recordsPerPage,
			Date creationDate, Date modifiedDate, long userId, String type, Integer status, Integer priority,
			String orderBy, String orderIn) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		List<CaseDto> caseSeedList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ");
			builder.append(
					"bcs.description, bcs.remarks, bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ");
			builder.append(
					"bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) ");
			builder.append(" from Case bcs  ");
			builder.append("where bcs.createdBy.userId = :userId ");
			builder.append(" and (bcs.description LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR bcs.remarks LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR bcs.name LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%'");
			builder.append(")");

			if (creationDate != null) {
				builder.append(" and bcs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");

			}
			if (modifiedDate != null) {
				builder.append(" and bcs.modifiedOn >='");
				builder.append(sdf.format(modifiedDate));
				builder.append("'");

			}
			if (type != null) {
				builder.append(" and bcs.type = :type");
			}
			if (status != null) {
				builder.append(" and bcs.currentStatus = ");
				builder.append(status);
			}
			if (priority != null) {
				builder.append(" and bcs.priority = ");
				builder.append(priority);
			}
			builder.append(" order by ");
			builder.append(" bcs.");
			builder.append(resolveCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					builder.append(" ");
					builder.append(orderIn);
				}
			}

			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("userId", userId);
			if (type != null)
				query.setParameter("type", type);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseSeedList = (List<CaseDto>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseSeedList;
	}

	@Override
	public long countFullTextSearchForCaseSeed(String caseSearchKeyword, long userId, Date creationDate,
			Date modifiedDate, String type, Integer status, Integer priority) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			String hqlQuery;
			StringBuilder builder = new StringBuilder();
			builder.append("select count(*) from Case cs");
			builder.append(" where cs.createdBy.userId = :userId ");
			builder.append(" and (cs.description LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.remarks LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.name LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%'");
			builder.append(")");

			if (creationDate != null) {
				builder.append(" and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");
			}
			if (modifiedDate != null) {
				builder.append(" and cs.modifiedOn >='");
				builder.append(sdf.format(modifiedDate));
				builder.append("'");
			}

			if (type != null) {
				builder.append(" and cs.type = :type");
			}
			if (status != null) {
				builder.append(" and cs.currentStatus = ");
				builder.append(status);
			}
			if (priority != null) {
				builder.append(" and cs.priority = ");
				builder.append(priority);
			}
			hqlQuery = builder.toString();
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("userId", userId);
			if (type != null)
				query.setParameter("type", type);
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	private static String resolveCaseColumnName(String userColumn) {
		if (userColumn == null)
			return "createdOn";

		switch (userColumn.toLowerCase()) {
		case "name":
			return "name";
		case "priority":
			return "priority";
		case "health":
			return "health";
		case "currentstatus":
			return "currentStatus";
		case "directrisk":
			return "directRisk";
		case "indirectrisk":
			return "indirectRisk";
		case "transactionalrisk":
			return "transactionalRisk";
		default:
			return "createdOn";

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> getAllCasesByUser(long userId, Integer pageNumber, Integer recordsPerPage, String type,
			Integer status, Integer priority, String orderBy, String orderIn) {
		List<CaseDto> bstCaseSeedList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ");
			queryBuilder.append(
					"bcs.description, bcs.remarks, bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ");
			queryBuilder.append(
					"bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) ");
			queryBuilder.append(" from Case bcs  ");
			queryBuilder.append("where bcs.createdBy.userId = :userId ");
			if (type != null) {
				queryBuilder.append(" and bcs.type = :type");
			}
			if (status != null) {
				queryBuilder.append(" and bcs.currentStatus = ");
				queryBuilder.append(status);
			}
			if (priority != null) {
				queryBuilder.append(" and bcs.priority = ");
				queryBuilder.append(priority);
			}
			queryBuilder.append(" order by ");
			queryBuilder.append(" bcs.");
			queryBuilder.append(resolveCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}

			Query<?> queryResult = getCurrentSession().createQuery(queryBuilder.toString());
			queryResult.setParameter("userId", userId);
			if (type != null)
				queryResult.setParameter("type", type);
			queryResult.setFirstResult((pageNumber - 1) * (recordsPerPage));
			queryResult.setMaxResults(recordsPerPage);
			bstCaseSeedList = (List<CaseDto>) queryResult.getResultList();
		} catch (Exception ex) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, ex,
					CaseDaoImpl.class);
		}
		return bstCaseSeedList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> getAllIncomingtrayBasedOnUserId(Long analystId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		List<CaseDto> incomingTrayDtoList = new ArrayList<CaseDto>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ")
					.append("bcs.description, bcs.remarks, bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ")
					.append("bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) from Case bcs, ")
					.append("CaseAnalystMapping bcsam, Users bam ").append("where bcsam.modifiedOn is null ")
					.append("and (bcsam.currentStatus= :submitted or bcsam.currentStatus= :acknowledge)")
					.append(" and bam.userId = bcsam.user.userId").append(" and bam.userId = :analystId")
					.append(" and bcs.caseId=bcsam.caseSeed.caseId");

			if (orderBy != null) {
				hql.append(" order by ");
				if (resolveCaseColumnName(orderBy).equalsIgnoreCase("createdOn"))
					hql.append("bcsam.");
				else
					hql.append("bcs.");
				hql.append(resolveCaseColumnName(orderBy));
				if (orderBy != null && orderIn != null) {
					if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
						hql.append(" ");
						hql.append(orderIn);
					}
				}
			}

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("analystId", analystId);
			query.setParameter("submitted", StatusEnum.SUBMITTED.getIndex());
			query.setParameter("acknowledge", StatusEnum.ACKNOWLEDGE.getIndex());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			incomingTrayDtoList = (List<CaseDto>) query.getResultList();

		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return incomingTrayDtoList;
	}

	@Override
	public long countgetAllIncomingTray(Long userId) {

		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ")
					.append("bcs.description, bcs.remarks,bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ")
					.append("bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) from Case bcs, ")
					.append("CaseAnalystMapping bcsam, Users bam ").append("where bcsam.modifiedOn is null ")
					.append("and (bcsam.currentStatus= :submitted or bcsam.currentStatus= :acknowledge)")
					.append(" and bam.userId = bcsam.user.userId").append(" and bam.userId = :userId")
					.append(" and bcs.caseId=bcsam.caseSeed.caseId");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setParameter("submitted", StatusEnum.SUBMITTED.getIndex());
			query.setParameter("acknowledge", StatusEnum.ACKNOWLEDGE.getIndex());
			long count = query.getResultList() != null ? query.getResultList().size() : 0;
			return count;

		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> fullTextSearchForIncomingtray(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, Date creationDate, Date modifiedDate, String orderBy, String orderIn) throws ParseException {
		List<CaseDto> caseSeedList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			String hqlQuery;
			StringBuilder builder = new StringBuilder();
			builder.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(cs.caseId, cs.name, ")
					.append("cs.description, cs.remarks,cs.priority, cs.currentStatus, cs.health, cs.directRisk, cs.indirectRisk, ")
					.append("cs.transactionalRisk, cs.createdOn, cs.createdBy.userId, cs.modifiedOn, cs.modifiedBy, cs.type) from Case cs, CaseAnalystMapping csmap");
			builder.append(" where (cs.description LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.remarks LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.name LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%'");
			builder.append(
					") and cs.caseId = csmap.caseSeed.caseId and ((csmap.currentStatus=:submitted or csmap.currentStatus=:acknowledge) and (csmap.modifiedOn is null)) and csmap.user.userId='");
			builder.append(userId);
			if (creationDate != null) {
				builder.append("' and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");

				if ((orderBy != null) && (!orderBy.isEmpty())) {
					builder.append(" order by ");
					builder.append("cs.");
					builder.append(resolveCaseColumnName(orderBy));
				}
			} else if (modifiedDate != null) {
				builder.append("' and cs.modifiedOn >='");
				builder.append(sdf.format(modifiedDate));
				builder.append("'");

				if ((orderBy != null) && (!orderBy.isEmpty())) {
					builder.append(" order by ");
					builder.append("cs.");
					builder.append(resolveCaseColumnName(orderBy));
				}
			} else {
				builder.append("'");
				if ((orderBy != null) && (!orderBy.isEmpty())) {
					builder.append(" order by ");
					builder.append("cs.");
					builder.append(resolveCaseColumnName(orderBy));
				}
			}
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					builder.append(" ");
					builder.append(orderIn);
				}
			}
			hqlQuery = builder.toString();
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("submitted", StatusEnum.SUBMITTED.getIndex());
			query.setParameter("acknowledge", StatusEnum.ACKNOWLEDGE.getIndex());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseSeedList = (List<CaseDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return caseSeedList;
	}

	@Override
	public long countFullTextSearchForIncomingtray(Long userId, String caseSearchKeyword, Date creationDate,
			Date modifiedDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			String hqlQuery;
			StringBuilder builder = new StringBuilder();
			builder.append("select count(*) from Case cs, CaseAnalystMapping csmap");
			builder.append(" where (cs.description LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.remarks LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.name LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%'");
			builder.append(
					") and cs.caseId = csmap.caseSeed.caseId and ((csmap.currentStatus=:submitted or csmap.currentStatus=:acknowledge) and (csmap.modifiedOn is null)) and csmap.user.userId='");
			builder.append(userId);

			if (creationDate != null) {
				builder.append("' and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");
				hqlQuery = builder.toString();
			} else if (modifiedDate != null) {
				builder.append("' and cs.modifiedOn >='");
				builder.append(sdf.format(modifiedDate));
				builder.append("'");
				hqlQuery = builder.toString();
			} else {
				builder.append("'");
			}
			hqlQuery = builder.toString();
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("submitted", StatusEnum.SUBMITTED.getIndex());
			query.setParameter("acknowledge", StatusEnum.ACKNOWLEDGE.getIndex());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> getAllMyCasesBasedOnUserId(Long userId, String orderBy, String orderIn, Integer pageNumber,
			Integer recordsPerPage) {
		List<CaseDto> caseSeedList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ")
					.append("bcs.description, bcs.remarks,bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ")
					.append("bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) from Case bcs, ")
					.append("CaseAnalystMapping bcsam, Users bam ").append("where")
					.append("((bcsam.currentStatus= :resolved or bcsam.currentStatus= :cancelled or bcsam.currentStatus= :accepted or bcsam.currentStatus= :paused)and bcsam.modifiedOn is null) ")
					.append(" and bam.userId = bcsam.user.userId").append(" and bam.userId = :userId")
					.append(" and bcs.caseId=bcsam.caseSeed.caseId");

			if (orderBy != null) {
				queryBuilder.append(" order by ");
				if (resolveCaseColumnName(orderBy).equalsIgnoreCase("createdOn"))
					queryBuilder.append("bcsam.");
				else
					queryBuilder.append("bcs.");
				queryBuilder.append(resolveCaseColumnName(orderBy));
				if (orderIn != null) {
					if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
						queryBuilder.append(" ");
						queryBuilder.append(orderIn);
					}
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("userId", userId);
			query.setParameter("accepted", StatusEnum.ACCEPTED.getIndex());
			query.setParameter("paused", StatusEnum.PAUSED.getIndex());
			query.setParameter("resolved", StatusEnum.RESOLVED.getIndex());
			query.setParameter("cancelled", StatusEnum.CANCELED.getIndex());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseSeedList = (List<CaseDto>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseSeedList;
	}

	@Override
	public long countgetAllMyCases(Long userId) {

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select count(*) from Case bcs, ").append("CaseAnalystMapping bcsam, Users bam ").append("where")
					.append("((bcsam.currentStatus= :resolved or bcsam.currentStatus= :cancelled or bcsam.currentStatus= :accepted or bcsam.currentStatus= :paused)and bcsam.modifiedOn is null) ")
					.append(" and bam.userId = bcsam.user.userId").append(" and bam.userId = :userId")
					.append(" and bcs.caseId=bcsam.caseSeed.caseId");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setParameter("accepted", StatusEnum.ACCEPTED.getIndex());
			query.setParameter("paused", StatusEnum.PAUSED.getIndex());
			query.setParameter("resolved", StatusEnum.RESOLVED.getIndex());
			query.setParameter("cancelled", StatusEnum.CANCELED.getIndex());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> fullTextSearchForMyCases(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, Date creationDate, Date modifiedDate, String orderBy, String orderIn) throws ParseException {
		List<CaseDto> caseSeedList = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			StringBuilder builder = new StringBuilder();
			builder.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(cs.caseId, cs.name, ")
					.append("cs.description, cs.remarks,cs.priority, cs.currentStatus, cs.health, cs.directRisk, cs.indirectRisk, ")
					.append("cs.transactionalRisk, cs.createdOn, cs.createdBy.userId, cs.modifiedOn, cs.modifiedBy, cs.type) from Case cs, CaseAnalystMapping csmap")
					.append(" where (cs.description LIKE '%").append(caseSearchKeyword)
					.append("%' OR cs.remarks LIKE '%").append(caseSearchKeyword).append("%' OR cs.name LIKE '%")
					.append(caseSearchKeyword).append("%'")
					.append(") and cs.caseId = csmap.caseSeed.caseId and ((csmap.currentStatus= :accepted or csmap.currentStatus= :paused or csmap.currentStatus= :resolved or csmap.currentStatus= :canceled) and (csmap.modifiedOn is null)) and csmap.user.userId= :userId");

			if (creationDate != null) {
				builder.append(" and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");
				if ((orderBy != null) && (!orderBy.isEmpty())) {
					builder.append(" order by ");
					builder.append("cs.");
					builder.append(resolveCaseColumnName(orderBy));
				}
			} else if (modifiedDate != null) {
				builder.append(" and cs.modifiedOn >='");
				builder.append(sdf.format(modifiedDate));
				builder.append("'");
				if ((orderBy != null) && (!orderBy.isEmpty())) {
					builder.append(" order by ");
					builder.append("cs.");
					builder.append(resolveCaseColumnName(orderBy));
				}
			} else {
				if ((orderBy != null) && (!orderBy.isEmpty())) {
					builder.append(" order by ");
					builder.append("cs.");
					builder.append(resolveCaseColumnName(orderBy));
				}
			}
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					builder.append(" ");
					builder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("userId", userId);
			query.setParameter("accepted", StatusEnum.ACCEPTED.getIndex());
			query.setParameter("paused", StatusEnum.PAUSED.getIndex());
			query.setParameter("resolved", StatusEnum.RESOLVED.getIndex());
			query.setParameter("canceled", StatusEnum.CANCELED.getIndex());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseSeedList = (List<CaseDto>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return caseSeedList;
	}

	@Override
	public long countFullTextSearchForMyCases(Long userId, String caseSearchKeyword, Date creationDate,
			Date modifiedDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("select count(*) from Case cs, CaseAnalystMapping csmap");
			builder.append(" where (cs.description LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.remarks LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%' OR cs.name LIKE '%");
			builder.append(caseSearchKeyword);
			builder.append("%'");
			builder.append(
					") and cs.caseId = csmap.caseSeed.caseId and ((csmap.currentStatus= :accepted or csmap.currentStatus= :paused or csmap.currentStatus= :resolved or csmap.currentStatus= :canceled) and (csmap.modifiedOn is null)) and csmap.user.userId= :userId");
			if (creationDate != null) {
				builder.append(" and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");
			} else if (modifiedDate != null) {
				builder.append(" and cs.modifiedOn >='");
				builder.append(sdf.format(modifiedDate));
				builder.append("'");
			}
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("userId", userId);
			query.setParameter("accepted", StatusEnum.ACCEPTED.getIndex());
			query.setParameter("paused", StatusEnum.PAUSED.getIndex());
			query.setParameter("resolved", StatusEnum.RESOLVED.getIndex());
			query.setParameter("canceled", StatusEnum.CANCELED.getIndex());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countAllCasesByUser(long userId, String type, Integer status, Integer priority) {
		List<CaseDto> bstCaseSeedList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ");
			queryBuilder.append(
					"bcs.description, bcs.remarks,bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ");
			queryBuilder.append(
					"bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) ");
			queryBuilder.append(" from Case bcs  ");
			queryBuilder.append("where bcs.createdBy.userId = :userId ");
			if (type != null) {
				queryBuilder.append(" and bcs.type = :type");				
			}
			if (status != null) {
				queryBuilder.append(" and bcs.currentStatus = ");
				queryBuilder.append(status);
			}
			if (priority != null) {
				queryBuilder.append(" and bcs.priority = ");
				queryBuilder.append(priority);
			}

			Query<?> queryResult = getCurrentSession().createQuery(queryBuilder.toString());
			queryResult.setParameter("userId", userId);
			if (type != null)
				queryResult.setParameter("type", type);
			bstCaseSeedList = (List<CaseDto>) queryResult.getResultList();
		} catch (Exception ex) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, ex,
					CaseDaoImpl.class);
		}
		return (long) bstCaseSeedList.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> getCaseList(CaseVo caseVo, Long userId) {

		List<CaseDto> caseSeedList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ")
					.append("bcs.description, bcs.remarks,bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ")
					.append("bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type,bcs.decisionScoringEntityId,bcs.workflowName,bcs.workflowId,bcs.isLeadGeneration,bcs.subType,bcsam.assignedBy.firstName,bcs.multiSourceId) from Case bcs, ")
					.append("CaseAnalystMapping bcsam, Users bam ").append("where")
					.append(" bam.userId = bcsam.user.userId").append(" and bam.userId = :userId")
					.append(" and bcs.caseId=bcsam.caseSeed.caseId and (bcsam.modifiedOn is null)");
			
			if (!StringUtils.isEmpty(caseVo.getKeyword())) {
				queryBuilder.append(" and ( bcs.name like '%" + caseVo.getKeyword() + "%'");
				queryBuilder.append(" or bcs.description like '%" + caseVo.getKeyword() + "%'");
				queryBuilder.append(" or bcs.remarks like '%" + caseVo.getKeyword() + "%')");
			}

			if (caseVo.getStatus() != null && !caseVo.getStatus().isEmpty()) {
				queryBuilder.append(" and bcsam.currentStatus IN ( :statusList)");
			}

			if (caseVo.getType() != null) {
				queryBuilder.append(" and bcs.type = :type");
			}

			if (caseVo.getPriority() != null) {
				queryBuilder.append(" and bcs.priority = ");
				queryBuilder.append(caseVo.getPriority().ordinal());
			}
			if (caseVo.getCreationDate() != null) {
				queryBuilder.append(" and bcs.createdOn >='");
				queryBuilder.append(sdf.format(caseVo.getCreationDate()));
				queryBuilder.append("'");
			}
			if (caseVo.getModifiedDate() != null) {
				queryBuilder.append(" and bcs.modifiedOn >='");
				queryBuilder.append(sdf.format(caseVo.getModifiedDate()));
				queryBuilder.append("'");
			}
			queryBuilder.append(" order by ");
			if (ordeByCaseColumnName(caseVo.getOrderBy()).equalsIgnoreCase("createdOn"))
				queryBuilder.append("bcsam.");
			else
				queryBuilder.append("bcs.");
			queryBuilder.append(ordeByCaseColumnName(caseVo.getOrderBy()));
			if (caseVo.getOrderBy() != null && caseVo.getOrderIn() != null) {
				if("asc".equalsIgnoreCase(caseVo.getOrderIn()) || "desc".equalsIgnoreCase(caseVo.getOrderIn())){
					queryBuilder.append(" ");
					queryBuilder.append(caseVo.getOrderIn());
				}
			}

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("userId", userId);
			if (caseVo.getType() != null)
				query.setParameter("type", caseVo.getType());
			if (caseVo.getStatus() != null && !caseVo.getStatus().isEmpty())
				query.setParameter("statusList", caseVo.getStatus());
			query.setFirstResult((caseVo.getPageNumber() - 1) * (caseVo.getRecordsPerPage()));
			query.setMaxResults(caseVo.getRecordsPerPage());
			caseSeedList = (List<CaseDto>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseSeedList;
	}

	private String ordeByCaseColumnName(String oderByColumn) {
		if (oderByColumn == null)
			return "createdOn";

		switch (oderByColumn.toLowerCase()) {
		case "name":
			return "name";
		default:
			return "createdOn";

		}
	}

	@Override
	public Long getCaseListCount(CaseVo caseVo, Long userId) {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder hql = new StringBuilder();
			hql.append("select count(*) from Case bcs, ").append("CaseAnalystMapping bcsam, Users bam ").append("where")
					.append("  bam.userId = bcsam.user.userId").append(" and bam.userId = :userId")
					.append(" and bcs.caseId=bcsam.caseSeed.caseId and (bcsam.modifiedOn is null)");

			if (!StringUtils.isEmpty(caseVo.getKeyword())) {
				hql.append(" and ( bcs.name like '%" + caseVo.getKeyword() + "%'");
				hql.append(" or bcs.description like '%" + caseVo.getKeyword() + "%'");
				hql.append(" or bcs.remarks like '%" + caseVo.getKeyword() + "%')");
			}
			
			if (caseVo.getStatus() != null && !caseVo.getStatus().isEmpty()) {
				hql.append(" and bcsam.currentStatus IN ( :statusList)");
			}

			if (caseVo.getType() != null) {
				hql.append(" and bcs.type = :type");
			}

			if (caseVo.getPriority() != null) {
				hql.append(" and bcs.priority = ");
				hql.append(caseVo.getPriority().ordinal());
			}
			if (caseVo.getCreationDate() != null) {
				hql.append(" and bcs.createdOn >='");
				hql.append(sdf.format(caseVo.getCreationDate()));
				hql.append("'");
			} 
			if (caseVo.getModifiedDate() != null) {
				hql.append(" and bcs.modifiedOn >='");
				hql.append(sdf.format(caseVo.getModifiedDate()));
				hql.append("'");
			}
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (caseVo.getType() != null)
				query.setParameter("type", caseVo.getType());
			if (caseVo.getStatus() != null && !caseVo.getStatus().isEmpty())
				query.setParameter("statusList", caseVo.getStatus());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseAggregator> caseAggregator(Long userId, CaseAggregatorDto caseAggregatorDto) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		List<CaseAggregator> caseAggregatorList = new ArrayList<CaseAggregator>();
		try {
			StringBuilder hql = new StringBuilder();
			String builder = new String();

			hql.append("select new element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator( ");
			if ((caseAggregatorDto.getGranularity()).equalsIgnoreCase("daily"))
				builder = "DATE_FORMAT(cam.createdOn,'%Y-%m-%d')";
			if ((caseAggregatorDto.getGranularity()).equalsIgnoreCase("weekly"))
				builder = "DATE_FORMAT(cam.createdOn,'%X-%V')";
			if ((caseAggregatorDto.getGranularity()).equalsIgnoreCase("monthly"))
				builder = "DATE_FORMAT(cam.createdOn,'%Y-%m')";
			if ((caseAggregatorDto.getGranularity()).equalsIgnoreCase("yearly"))
				builder = "DATE_FORMAT(cam.createdOn,'%Y')";

			hql.append(builder);
			for (int i = 2; i <= 9; i++) {
				hql.append(" ,SUM(CASE WHEN cam.currentStatus= " + i + " THEN 1 ELSE 0 END) ");
			}
			hql.append(" )");

			hql.append("from Case c,CaseAnalystMapping cam where cam.user.userId=:userId  ");
			hql.append(" and c.caseId=cam.caseSeed.caseId");
			
			hql.append(" and cam.modifiedOn is null");

			if (caseAggregatorDto.getStatus() != null && !caseAggregatorDto.getStatus().isEmpty()) {
				hql.append(" and cam.currentStatus IN ( :statusList)");
			}
			
			hql.append(" and cam.createdOn BETWEEN ");
			hql.append("'" + sdf.format(caseAggregatorDto.getFromDate()) + "'");
			hql.append(" and '" + sdf.format(caseAggregatorDto.getToDate()) + "'");

			if (caseAggregatorDto.getType() != null) {
				hql.append(" and c.type = :type");
			}
			hql.append(" group by " + builder);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			if (!(caseAggregatorDto.getType() == null))
				query.setParameter("type", caseAggregatorDto.getType());
			if (!(caseAggregatorDto.getStatus() == null) && !caseAggregatorDto.getStatus().isEmpty())
				query.setParameter("statusList", caseAggregatorDto.getStatus());

			caseAggregatorList = (List<CaseAggregator>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}
		return caseAggregatorList;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getCasesByUser(Long currentUserId) {
		List<Long> list=new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		try {
			builder.append("select distinct c.caseId from Case c,CaseAnalystMapping cam where c.caseId=cam.caseSeed.caseId and "
					+ "(c.createdBy.userId=:currentUserId or (cam.user.userId=:currentUserId and cam.modifiedOn is null and cam.currentStatus!=:status))");
		
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("currentUserId", currentUserId);
			query.setParameter("status", StatusEnum.REJECTED.getIndex());
			 list =  (List<Long>) query.getResultList();
			

		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not execute query ");
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getMyCaseIds(Long userId,CaseVo caseVo,Integer limit) {

		List<Long> caseIds = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select c.caseId")
					.append(" from Case c, CaseAnalystMapping cam,Users u ")
					.append("where u.userId = cam.user.userId and u.userId = :userId and c.caseId=cam.caseSeed.caseId ")
					.append("and (cam.modifiedOn is null) and cam.currentStatus IN ( :statusList) ")
					.append(" order by cam.createdOn desc");

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("userId", userId);
			query.setParameter("statusList", caseVo.getStatus());
			if(limit!=null)
			query.setMaxResults(limit);
			caseIds = (List<Long>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseIds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseUnderwritingDto> getCaseListForUnderwriting(Integer recordsPerPage, Integer pageNumber,
			List<Long> docIds) {

		List<CaseUnderwritingDto> caseSeedList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto(bcs.caseId, bcs.name, ")
					.append("bcs.questionnaireDocId, bcs.currentStatus,bcs.createdOn,bcs.industry) from Case bcs ");

			if (!docIds.isEmpty()) {
				queryBuilder.append("where bcs.questionnaireDocId IN ( :docIds)");
			}
			queryBuilder.append(" order by ");
			queryBuilder.append("bcs.createdOn desc");

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if (!docIds.isEmpty()) 
				query.setParameter("docIds", docIds);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseSeedList = (List<CaseUnderwritingDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseSeedList;
	}

	@Override
	public Long caseListCountForUnderwriting(List<Long> docIds) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select count(*) from Case bcs ");

			if (!docIds.isEmpty()) {
				hql.append("where bcs.questionnaireDocId IN ( :docIds)");
			}
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if (!docIds.isEmpty()) 
				query.setParameter("docIds", docIds);
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CaseUnderwritingDto> searchCaseList(String keyword) {

		List<CaseUnderwritingDto> caseSeedList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto(bcs.caseId, bcs.name, ")
					.append("bcs.questionnaireDocId, bcs.currentStatus,bcs.createdOn) from Case bcs ");
			if(!StringUtils.isEmpty(keyword)){
				queryBuilder.append("where" + " (bcs.name like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or bcs.description like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or bcs.remarks like '%" + keyword.replace("\"", "") + "%')");
			}
			
			queryBuilder.append(" order by ");
			queryBuilder.append("bcs.createdOn desc");

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			caseSeedList = (List<CaseUnderwritingDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseSeedList;
	}

	@Override
	public Long searchCaseListCount(String keyword) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select count(*) from Case bcs ");
			if(!StringUtils.isEmpty(keyword)){
				hql.append("where" + " (bcs.name like '%" + keyword.replace("\"", "") + "%'");
				hql.append(" or bcs.description like '%" + keyword.replace("\"", "") + "%'");
				hql.append(" or bcs.remarks like '%" + keyword.replace("\"", "") + "%')");
			}
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@Override
	public Case getCaseByDocId(Long docId) {
		Case caseSeed=null;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("select cs from Case cs where cs.questionnaireDocId = :docId");			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("docId", docId);
			caseSeed=(Case) query.getSingleResult();
			return caseSeed;
		}catch (Exception e) {
			return caseSeed;
		}
		
	}
}
