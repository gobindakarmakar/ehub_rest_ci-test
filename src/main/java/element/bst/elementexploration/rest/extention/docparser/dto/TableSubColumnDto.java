package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
/**
 * @author rambabu
 *
 */
import java.util.List;

import io.swagger.annotations.ApiModel;


@ApiModel("Table sub columns")
public class TableSubColumnDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String subColumnName;

	private List<DocumentTableRowsDto> subColumnData;

	public TableSubColumnDto() {
		super();

	}

	public TableSubColumnDto(String subColumnName, List<DocumentTableRowsDto> subColumnData) {
		super();
		this.subColumnName = subColumnName;
		this.subColumnData = subColumnData;
	}

	public String getSubColumnName() {
		return subColumnName;
	}

	public void setSubColumnName(String subColumnName) {
		this.subColumnName = subColumnName;
	}

	public List<DocumentTableRowsDto> getSubColumnData() {
		return subColumnData;
	}

	public void setSubColumnData(List<DocumentTableRowsDto> subColumnData) {
		this.subColumnData = subColumnData;
	}

}
