package element.bst.elementexploration.rest.extention.widgetreview.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rambabu
 *
 */
public class ComplianceWidgetDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "widgetId")
	private Long id;

	private String widgetName;

	public ComplianceWidgetDto() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

}
