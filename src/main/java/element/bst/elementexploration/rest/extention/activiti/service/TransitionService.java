package element.bst.elementexploration.rest.extention.activiti.service;

import java.util.ArrayList;
import java.util.List;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.ExclusiveGateway;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.ParallelGateway;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.ServiceTask;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
/**
 * 
 * @author Viswanath Reddy G
 *
 */
@Service("transitionService")
public class TransitionService {
	
	@Value("${bpm_process_definition_key}")
	private String BPM_PROCESS_DEFINITION_KEY;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private TaskService taskService;

	public List<String> getTransitionsBytaskKey(String taskKey, Long caseId) {

		List<String> transitions = new ArrayList<String>();
		Task task = null;
		List<Task> list = taskService.createTaskQuery().taskDescription(caseId.toString()).list();
		for (Task task2 : list) {
			if (!"UNFOCUS".equalsIgnoreCase(task2.getName()))
				task = task2;
		}
		BpmnModel bpm = repositoryService.getBpmnModel(task.getProcessDefinitionId());
		org.activiti.bpmn.model.Process process = bpm.getProcessById(BPM_PROCESS_DEFINITION_KEY);
		FlowElement flowElement = process.getFlowElement(taskKey == null ? task.getTaskDefinitionKey() : taskKey);
		List<FlowElement> listOfTransitions = new ArrayList<>();
		if (flowElement instanceof UserTask) {
			UserTask userTask = (UserTask) flowElement;
			List<SequenceFlow> outgoingFlows = userTask.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				if(sequenceFlow.getTargetFlowElement() instanceof ServiceTask){
					ServiceTask serviceTask = (ServiceTask) sequenceFlow.getTargetFlowElement();
					if(serviceTask.getType() !=null && serviceTask.getType().equals("mail")){
						List<SequenceFlow> outgoingFlows2 =serviceTask.getOutgoingFlows();
						for (SequenceFlow sequenceFlow2 : outgoingFlows2) {
							listOfTransitions = find(sequenceFlow2.getTargetFlowElement(), listOfTransitions);

						}
					}else{
						listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);
					}
						
				}else{
					listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);

				}
			}
		} else if (flowElement instanceof ServiceTask) {
			ServiceTask serviceTask = (ServiceTask) flowElement;
			List<SequenceFlow> outgoingFlows = serviceTask.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				if(sequenceFlow.getTargetFlowElement() instanceof ServiceTask){
					ServiceTask serviceTask1 = (ServiceTask) sequenceFlow.getTargetFlowElement();
					if(serviceTask1.getType() !=null && serviceTask1.getType().equals("mail")){
						List<SequenceFlow> outgoingFlows2 =serviceTask.getOutgoingFlows();
						for (SequenceFlow sequenceFlow2 : outgoingFlows2) {
							listOfTransitions = find(sequenceFlow2.getTargetFlowElement(), listOfTransitions);

						}
					}else{
						listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);
					}
						
				}else{
					listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);

				}
			}

		}

		for (FlowElement flowElement2 : listOfTransitions) {
			if (taskKey == null ? task.getTaskDefinitionKey().equals("rejected") : taskKey.equals("rejected")) {
				if (flowElement2 instanceof UserTask)
					transitions.add(flowElement2.getName());
				System.out.println(flowElement2.getName());

			} else {
				System.out.println(flowElement2.getName());
				transitions.add(flowElement2.getName());
			}

		}
		return transitions;

	}
	
	public List<FlowElement> getTransitions(Long caseId, Integer currentStatus) {

		Task task = taskService.createTaskQuery().taskDescription(caseId.toString())
				.taskDefinitionKey(
						currentStatus == 7 ? "unFocus" : StatusEnum.getStatusEnumByIndex(currentStatus).getName())
				.singleResult();
		List<FlowElement> listOfTransitions = new ArrayList<>();
		if(task != null){
			BpmnModel bpm = repositoryService.getBpmnModel(task.getProcessDefinitionId());
			org.activiti.bpmn.model.Process process = bpm.getProcessById(BPM_PROCESS_DEFINITION_KEY);
			FlowElement flowElement = process.getFlowElement(StatusEnum.getStatusEnumByIndex(currentStatus).getName());
			if (flowElement instanceof UserTask) {
				UserTask userTask = (UserTask) flowElement;
				List<SequenceFlow> outgoingFlows = userTask.getOutgoingFlows();
				for (SequenceFlow sequenceFlow : outgoingFlows) {
					if(sequenceFlow.getTargetFlowElement() instanceof ServiceTask){
						ServiceTask serviceTask = (ServiceTask) sequenceFlow.getTargetFlowElement();
						if(serviceTask.getType() !=null && serviceTask.getType().equals("mail")){
							List<SequenceFlow> outgoingFlows2 =serviceTask.getOutgoingFlows();
							for (SequenceFlow sequenceFlow2 : outgoingFlows2) {
								listOfTransitions = find(sequenceFlow2.getTargetFlowElement(), listOfTransitions);

							}
						}else{
							listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);
						}
							
					}else{
						listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);

					}
				}
			} else if (flowElement instanceof ServiceTask) {
				ServiceTask serviceTask = (ServiceTask) flowElement;
				List<SequenceFlow> outgoingFlows = serviceTask.getOutgoingFlows();
				for (SequenceFlow sequenceFlow : outgoingFlows) {
					if(sequenceFlow.getTargetFlowElement() instanceof ServiceTask){
						ServiceTask serviceTask1 = (ServiceTask) sequenceFlow.getTargetFlowElement();
						if(serviceTask1.getType() !=null && serviceTask1.getType().equals("mail")){
							List<SequenceFlow> outgoingFlows2 =serviceTask.getOutgoingFlows();
							for (SequenceFlow sequenceFlow2 : outgoingFlows2) {
								listOfTransitions = find(sequenceFlow2.getTargetFlowElement(), listOfTransitions);

							}
						}else{
							listOfTransitions = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);
						}
							
					}else{
						listOfTransitions= find(sequenceFlow.getTargetFlowElement(), listOfTransitions);

					}
				}

			}
			return listOfTransitions;

		}else{
			return listOfTransitions;
		}

	}

	public List<FlowElement> find(FlowElement element, List<FlowElement> list) {
		if (element instanceof UserTask)
			list.add(element);
		if (element instanceof ExclusiveGateway) {
			ExclusiveGateway gateway = (ExclusiveGateway) element;
			for (SequenceFlow node : gateway.getOutgoingFlows()) {
				// if (!node.getId().equalsIgnoreCase("reStartCase"))
				find(node.getTargetFlowElement(), list);
			}
		}
		if (element instanceof ParallelGateway) {
			ParallelGateway parllelgateway = (ParallelGateway) element;
			for (SequenceFlow node : parllelgateway.getOutgoingFlows()) {
				// if (!node.getId().equalsIgnoreCase("reStartCase"))
				find(node.getTargetFlowElement(), list);
			}
		}
		if (element instanceof ServiceTask && ((ServiceTask) element).getType()==null)
			list.add(element);
		if(element instanceof ServiceTask && ((ServiceTask) element).getType()!=null){
			ServiceTask serviceTask=(ServiceTask) element;
			List<SequenceFlow> outgoingFlows =serviceTask.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				if(sequenceFlow.getTargetFlowElement() instanceof UserTask){
					list.add(sequenceFlow.getTargetFlowElement());
				} else if(sequenceFlow.getTargetFlowElement() instanceof ServiceTask){
					list.add(sequenceFlow.getTargetFlowElement());
				} else if(sequenceFlow.getTargetFlowElement()  instanceof ExclusiveGateway){
					find(sequenceFlow.getTargetFlowElement() ,list);
				} else if(sequenceFlow.getTargetFlowElement()  instanceof ParallelGateway){
					find(sequenceFlow.getTargetFlowElement() ,list);
				} else if(element instanceof EndEvent){
					list.add(element);
				}
			}
		}
			
		if (element instanceof EndEvent)
			list.add(element);
		return list;

	}

	public List<String> getIncomingTransitions(Long caseId) {

		List<String> transitions = new ArrayList<String>();
		Task task = taskService.createTaskQuery().taskDescription(caseId.toString()).singleResult();
		/*
		 * ProcessDefinition pd =
		 * repositoryService.createProcessDefinitionQuery()
		 * .processDefinitionKey("caseManagementWorkFlow").latestVersion().
		 * singleResult();
		 */
		BpmnModel bpm = repositoryService.getBpmnModel(task.getProcessDefinitionId());
		org.activiti.bpmn.model.Process process = bpm.getProcessById("caseManagementWorkFlow");
		FlowElement flowElement = process.getFlowElement(task.getName());
		List<FlowElement> listOfTransitions = new ArrayList<>();
		if (flowElement instanceof UserTask) {
			UserTask userTask = (UserTask) flowElement;
			List<SequenceFlow> outgoingFlows = userTask.getIncomingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				List<FlowElement> element = find(sequenceFlow.getSourceFlowElement(), listOfTransitions);
				System.out.println(element.size());
			}
		} else if (flowElement instanceof ServiceTask) {
			ServiceTask serviceTask = (ServiceTask) flowElement;
			List<SequenceFlow> outgoingFlows = serviceTask.getIncomingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				List<FlowElement> element = find(sequenceFlow.getSourceFlowElement(), listOfTransitions);
				System.out.println(element.size());
			}

		}

		for (FlowElement flowElement2 : listOfTransitions) {
			if (task.getName().equalsIgnoreCase("rejected")) {
				if (flowElement2 instanceof UserTask)
					transitions.add(flowElement2.getName());
				System.out.println(flowElement2.getName());

			} else {
				System.out.println(flowElement2.getName());
				transitions.add(flowElement2.getName());
			}

		}
		return transitions;

	}

	public List<FlowElement> findIncomingFolws(FlowElement element, List<FlowElement> list) {
		if (element instanceof UserTask)
			list.add(element);
		if (element instanceof ExclusiveGateway) {
			ExclusiveGateway gateway = (ExclusiveGateway) element;
			for (SequenceFlow node : gateway.getIncomingFlows()) {
				// if (!node.getId().equalsIgnoreCase("reStartCase"))
				findIncomingFolws(node.getSourceFlowElement(), list);
			}
		}
		if (element instanceof ParallelGateway) {
			ParallelGateway parllelgateway = (ParallelGateway) element;
			for (SequenceFlow node : parllelgateway.getIncomingFlows()) {
				// if (!node.getId().equalsIgnoreCase("reStartCase"))
				findIncomingFolws(node.getSourceFlowElement(), list);
			}
		}
		if (element instanceof ServiceTask)
			list.add(element);
		if (element instanceof EndEvent)
			list.add(element);
		return list;

	}

	
}
