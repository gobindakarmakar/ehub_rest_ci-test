package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

public class EntitySearchAggregatesDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<HitsDto> hits;
	private int total;
	private List<EntityAggregatesDto> aggregations;

	public List<HitsDto> getHits() {
		return hits;
	}

	public void setHits(List<HitsDto> hits) {
		this.hits = hits;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<EntityAggregatesDto> getAggregations() {
		return aggregations;
	}

	public void setAggregations(List<EntityAggregatesDto> aggregations) {
		this.aggregations = aggregations;
	}

}
