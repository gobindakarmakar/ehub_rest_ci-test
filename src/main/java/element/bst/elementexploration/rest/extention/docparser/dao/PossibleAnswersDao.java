package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface PossibleAnswersDao extends GenericDao<PossibleAnswers, Long>{

	List<PossibleAnswers> fetchPossibleAnswers(Long questionId);
	
	
}
