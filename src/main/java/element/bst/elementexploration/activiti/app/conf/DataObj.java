package element.bst.elementexploration.activiti.app.conf;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataObj implements Comparable< DataObj > {
	
	@JsonProperty("sourceId")
	String sourceId;
	
	@JsonProperty("sourceName")
	String sourceName;
	
	@JsonProperty("sourceUrl")
	String sourceUrl;

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	@Override
    public String toString() {
        return null;
    }
 
    @Override
    public int compareTo(DataObj o) {
        return this.getSourceName().compareTo(o.getSourceName());
    }
	

}
