package element.bst.elementexploration.rest.extention.adversenews.serviceImpl;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToUploadCaseSeedDocumentException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.adversenews.service.AdverseNewsService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("adverseNewsService")
@Transactional("transactionManager")
public class AdverseNewsServiceImpl implements AdverseNewsService {

	@Value("${bigdata_mip_upload_url}")
	private String BIG_DATA_MIP_UPLOAD_URL;

	/*@Value("${bigdata_entity_search_url}")
	private String BIG_DATA__ENTITY_SEARCH_URL;*/

	@Value("${bigdata_entity_news_url}")
	private String BIG_DATA_ENTITY_NEWS_URL;

	@Value("${bigdata_profile_url}")
	private String BIG_DATA_PROFILE_URL;

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENSION_URL;

	@Value("${workflow_extention_url_development}")
	private String WORKFLOW_EXTENSION_URL_DEVELOPMENT;

	@Override
	public String getEntities() throws Exception {
		String url = BIG_DATA_MIP_UPLOAD_URL + "entities";
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String postFiles(File[] files) throws Exception {
		String response = null;
		String url = BIG_DATA_MIP_UPLOAD_URL + "transactions";
		if (files[0] != null && files[0].length() > 0 && files[1] != null && files[1].length() > 0) {
			String serverResponse[] = ServiceCallHelper.postFileArrayToServerWithoutTImeOut(url, files);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200
					&& serverResponse[1].trim().length() > 0) {
				response = serverResponse[1];
			} else {
				throw new FailedToUploadCaseSeedDocumentException(
						ElementConstants.FAILED_TO_UPLOAD_FILES_TO_SERVER_MSG);
			}
		} else {
			throw new InsufficientDataException("File can not be empty.");
		}

		return response;
	}

	@Override
	public String getSummary(String type, String name) throws Exception {
		String nameEncode = java.net.URLEncoder.encode(name, "UTF-8").replaceAll("\\+", "%20");
		String url = BIG_DATA_MIP_UPLOAD_URL + "summary/" + type + "/" + nameEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getGraphById(String type, String name) throws Exception {
		String nameEncode = java.net.URLEncoder.encode(name, "UTF-8").replaceAll("\\+", "%20");
		String url = BIG_DATA_MIP_UPLOAD_URL + "graph/" + type + "/" + nameEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	/*@Override
	public String getAdversenNewsById(String type, String id, Double offset, Double limit) throws Exception {
		String idEncode = java.net.URLEncoder.encode(id, "UTF-8").replaceAll("\\+", "%20");
		String typeEncode = java.net.URLEncoder.encode(type, "UTF-8").replaceAll("\\+", "%20");
		String url = BIG_DATA__ENTITY_SEARCH_URL + "news/" + typeEncode + "/" + idEncode;
		StringBuilder builder = new StringBuilder(url);
		Boolean flag = false;
		if (offset != null) {
			builder.append("?offset=" + offset);
			flag = true;
		}
		if (limit != null && flag) {
			builder.append("&limit=" + limit);
		} else if (limit != null) {
			builder.append("?limit=" + limit);
			flag = true;
		}
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(builder.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}*/

	/*@Override

	public String getSearchResultList(String newsSearchRequest) throws Exception {
		String url = BIG_DATA_ENTITY_NEWS_URL + "v3/news/search";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, newsSearchRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}*/

	@Override
	public String getAdverseProfileById(String identifier) throws Exception {
		String url = BIG_DATA_PROFILE_URL + "profile/org/" + identifier;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getSearchMultiResourcesGraph(String newsSearchRequest) throws Exception {
		String url = WORKFLOW_EXTENSION_URL_DEVELOPMENT + "/graph/api/search/multisources";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, newsSearchRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getSearchEventResultList(String newsSearchRequest) throws Exception {
		String url = BIG_DATA_ENTITY_NEWS_URL + "v4/events/search";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, newsSearchRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getSearchEventAggregate(String newsSearchRequest) throws Exception {
		String url = BIG_DATA_ENTITY_NEWS_URL + "v4/events/aggregate";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, newsSearchRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

}
