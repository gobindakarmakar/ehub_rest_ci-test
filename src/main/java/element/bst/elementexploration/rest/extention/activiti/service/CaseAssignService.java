package element.bst.elementexploration.rest.extention.activiti.service;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
/**
 * 
 * @author Viswanath Reddy G
 *
 */
@Component("caseAssignService")
public class CaseAssignService implements JavaDelegate {

	@Autowired
	private CaseDao caseDao;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private CaseService caseService;

	private Expression assignee;

	public void setAssignee(Expression assignee) {
		this.assignee = assignee;
	}

	@Override
	public void execute(DelegateExecution execution) {
		String assigneeType = (String) assignee.getValue(execution);

		Users user = null;
		Case caseSeed = (Case) execution.getVariable("caseSeed");
		execution.setVariable("caseId", caseSeed.getCaseId().toString());

		if (assigneeType.equalsIgnoreCase("admin")) {
			user = new Users();
			Group adminGroup = groupDao.getGroup(ElementConstants.ADMIN_GROUP_NAME);
			List<UsersDto> userDto = usersDao.getUserList(adminGroup.getUserGroupId(), null, 0, 1, null, null);
			user = usersDao.find(userDto.get(0).getUserId());
			caseDao.assignCaseToAnalyst(user, caseSeed);

		} else if (assigneeType.equalsIgnoreCase("creator")) {
			user = usersDao.getAnalystByUserId(caseSeed.getCreatedBy().getUserId());
			if (user != null) {
				caseDao.assignCaseToAnalyst(user, caseSeed);
			} else {
				user = caseService.getNextAnalyst(caseSeed.getCaseId());
				caseDao.assignCaseToAnalyst(user, caseSeed);
			}
			// user = usersDao.find(caseSeed.getCreatedBy().getUserId());
		} else {
			user = caseService.getNextAnalyst(caseSeed.getCaseId());
			caseDao.assignCaseToAnalyst(user, caseSeed);
		}
		execution.setVariable("user", user.getFirstName());
		execution.setVariable("userId", user.getUserId().toString());
		execution.setVariable("caseId", caseSeed.getCaseId().toString());
		execution.setVariable("assignedUser", user.getEmailAddress());
		execution.setVariable("statusTo", StatusEnum.SUBMITTED.getName());
		execution.setVariable("caseName", caseSeed.getName());
	}

}
