package element.bst.elementexploration.rest.extention.tuna.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.tuna.service.TunaService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Tuna API" },description="Manages Tuna data")
@RestController
@RequestMapping(value = "/api/tuna")
public class TunaController extends BaseController {

	@Autowired
	TunaService tunaService;

	@ApiOperation("Gets company house data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getComapniesHouse/{id}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCompaniesHouse(@RequestParam("token") String token, HttpServletRequest request,
			@PathVariable("id") String id) throws Exception {
		return new ResponseEntity<>(tunaService.getCompaniesHouse(id), HttpStatus.OK);
	}

	@ApiOperation("Gets filing history of the company")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getFilingHistory/{company_number}/filing-history", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFilingHistory(@RequestParam("token") String token, HttpServletRequest request,
			@PathVariable String company_number)
			throws Exception {
		return new ResponseEntity<>(tunaService.getFilingHistory(company_number), HttpStatus.OK);
	}
	
	@ApiOperation("Gets officers of the company")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getCompanyOfficers/{company_number}/officers", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCompanyOfficers(@RequestParam("token") String token, HttpServletRequest request,
			@PathVariable String company_number)
			throws Exception {
		return new ResponseEntity<>(tunaService.getCompanyOfficers(company_number), HttpStatus.OK);
	}
	
	@ApiOperation("searches for companies")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/search/companies", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchCompanies(@RequestParam("token") String token, HttpServletRequest request,
			@RequestParam String q)
			throws Exception {
		return new ResponseEntity<>(tunaService.searchCompanies(q), HttpStatus.OK);
	}
	
	//for screening results
	/*@GetMapping(value = "/profile/org/{identifier}/watchlist", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrgSanctions(@RequestParam("token") String token, HttpServletRequest request,
			@PathVariable String identifier)
			throws Exception {
		return new ResponseEntity<>(tunaService.getOrgSanctions(identifier), HttpStatus.OK);
	}*/
	
	@ApiOperation("Gets info ops data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/profile/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getInfoOps(@RequestParam("token") String token, HttpServletRequest request,@RequestParam String orgOrPerson,@RequestParam(required=false) String filter,
			@PathVariable String identifier)
			throws Exception {
		return new ResponseEntity<>(tunaService.getInfoOps(identifier,orgOrPerson,filter), HttpStatus.OK);
	}
	
	@ApiOperation("Gets finance statement of the queried entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getFinanceStatement", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFinanceStatement(@RequestParam("token") String token, HttpServletRequest request,@RequestParam String source,@RequestParam String query,@RequestParam (required=false) String exchange,
			@RequestParam String apiKey)
			throws Exception {
		return new ResponseEntity<>(tunaService.getFinanceStatement(source,query,exchange,apiKey), HttpStatus.OK);
	}
	
	@ApiOperation("Gets persons of significant control in a company")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/company/{company_number}/persons-with-significant-control", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSignificantPersonsWithControl(@RequestParam("token") String token, HttpServletRequest request,@PathVariable String company_number)
			throws Exception {
		return new ResponseEntity<>(tunaService.getSignificantPersonsWithControl(company_number), HttpStatus.OK);
	}
}
