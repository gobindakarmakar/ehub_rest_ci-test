package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class CheckPdfTypeDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean scannedPdf = false;
	private boolean FillablePdf = false;
	private boolean normalPdf = false;

	public boolean isScannedPdf() {
		return scannedPdf;
	}

	public void setScannedPdf(boolean scannedPdf) {
		this.scannedPdf = scannedPdf;
	}

	public boolean isFillablePdf() {
		return FillablePdf;
	}

	public void setFillablePdf(boolean fillablePdf) {
		FillablePdf = fillablePdf;
	}

	public boolean isNormalPdf() {
		return normalPdf;
	}

	public void setNormalPdf(boolean normalPdf) {
		this.normalPdf = normalPdf;
	}

}
