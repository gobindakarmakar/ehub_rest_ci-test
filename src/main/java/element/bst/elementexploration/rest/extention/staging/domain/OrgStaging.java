package element.bst.elementexploration.rest.extention.staging.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class OrgStaging implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "ID of the source", required = true)
	@JsonProperty("_source-id")
	private String sourceId;
	
	@ApiModelProperty(value = "Specify if its manual", required = true)
	@JsonProperty("_is-manual")
	private String isManual;

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getIsManual() {
		return isManual;
	}

	public void setIsManual(String isManual) {
		this.isManual = isManual;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
