package element.bst.elementexploration.rest.extention.menuitem.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Jalagari Paul
 *
 */
public interface ModulesService extends GenericService<Modules, Long> {

	List<Modules> findAllWithModuleGroups() throws Exception;
	
	List<Modules> getDefaultModules() throws Exception;

}
