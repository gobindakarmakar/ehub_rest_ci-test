package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentStatements;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface DocumentStatementsService extends GenericService<DocumentStatements, Long>{
	

	
}
