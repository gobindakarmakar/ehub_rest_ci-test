package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;

/**
 * @author Paul Jalagari
 *
 */
public interface RolesGroupDao extends GenericDao<RoleGroup, Long> {

	List<RoleGroup> getRolesByGroup(List<Long> groupIds);

	void deleteExistingRolesForGroup(Long groupId);

}
