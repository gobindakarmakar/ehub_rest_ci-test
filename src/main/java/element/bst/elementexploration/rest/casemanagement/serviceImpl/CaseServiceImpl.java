package element.bst.elementexploration.rest.casemanagement.serviceImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.runtime.BatchExecutionCommandImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.kie.api.KieServices;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.opencsv.CSVReaderHeaderAware;

import element.bst.elementexploration.rest.casemanagement.bigdata.domain.CaseSource;
import element.bst.elementexploration.rest.casemanagement.bigdata.domain.GraphObject;
import element.bst.elementexploration.rest.casemanagement.bigdata.domain.Source;
import element.bst.elementexploration.rest.casemanagement.dao.CaseAnalystMappingDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.Answer;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.dto.DocumentDto;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplateMappingService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.search.service.SearchHistoryService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.LoggerUtil;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import net.sourceforge.tess4j.TesseractException;

@Service("caseService")
public class CaseServiceImpl extends GenericServiceImpl<Case, Long> implements CaseService {

	@Autowired
	private CaseDao caseDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private CaseAnalystMappingDao caseAnalystMappingDao;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@SuppressWarnings("unused")
	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private IdentityService identityService;

	@Autowired
	private DocumentParserService documentParserService;

	@Autowired
	DocumentService documentService;

	@Autowired
	DocumentTemplateMappingService documentTemplateMappingService;

	@Autowired
	private DocumentDao documentDao;
	
	@Autowired
	DocumentTemplatesService documentTemplatesService;
	
	@Autowired
	private SearchHistoryService searchHistoryService;
	
	@Autowired
	AdvanceSearchService advanceSearchService;

	@Value("${bigdata_create_seed_url}")
	private String BIG_DATA_BASE_URL;

	@Value("${kie_server_url}")
	private String KIE_SERVER_URL;

	@Value("${kie_server_username}")
	private String KIE_SERVER_USERNAME;

	@Value("${kie_server_password}")
	private String KIE_SERVER_PASSWORD;
	
	@Value("${bigdata_person_entity_create_url}")
	private String CREATE_PERSON_ENTITY_URL;
	
	@Value("${bigdata_org_entity_create_url}")
	private String CREATE_ORG_ENTITY_URL;
	
	@Value("${bigdata_profile_org_url}")
	private String BIGDATA_PROFILE_ORG_URL;
	
	@Value("${case_decisionscore_risk_url}")
	private String CASE_DECISIONSCORE_RISK_URL;

	@Value("${bigdata_entity_riskscore_url}")
	private String BIGDATA_ENTITY_RISKSCORE_URL;

	private final int NUMBER_OF_ATTEMPT_TO_BIG_DATA = 3;
	private final Charset UTF8_CHARSET = Charset.forName("UTF-8");
	
	@Value("${bpm_process_definition_key}")
	private String BPM_PROCESS_DEFINITION_KEY;
	
	@Value("${client_id}")
	private String CLIENT_ID;

	private Random random =null;
	
	public CaseServiceImpl() {
	 this.random = new Random();
	}

	@Autowired
	public CaseServiceImpl(GenericDao<Case, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public boolean isCaseAccessibleOrOwner(Long caseId, Long userId) throws InsufficientDataException {
		if (caseId == null || userId == null)
			throw new InsufficientDataException("Could not process request, caseId or userId cannot be null");
		if (!caseDao.isCaseAccessibleOrOwner(caseId, userId))
			throw new PermissionDeniedException();
		return true;

	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> fullTextSearchForCaseSeed(String caseSearchKeyword, Integer pageNumber, Integer recordsPerPage,
			String creationDate, String modifiedDate, long userId, String type, Integer status, Integer priority,
			String orderBy, String orderIn) throws ParseException {
		Date creationDateTemp = null;
		Date modificatonDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertToDateddMMyyyy(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (modifiedDate != null && modifiedDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(modifiedDate)) {
				modificatonDateTemp = LoggerUtil.convertToDateddMMyyyy(modifiedDate);
			} else {
				throw new DateFormatException();
			}
		}

		return caseDao.fullTextSearchForCaseSeed(caseSearchKeyword,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, creationDateTemp,
				modificatonDateTemp, userId, type, status, priority, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public long countFullTextSearchForCaseSeed(String caseSearchKeyword, long userId, String creationDate,
			String modifiedDate, String type, Integer status, Integer priority) throws ParseException {
		Date creationDateTemp = null;
		Date modificatonDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertJavaDateToSqlDate(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (modifiedDate != null && modifiedDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(modifiedDate)) {
				modificatonDateTemp = LoggerUtil.convertJavaDateToSqlDate(modifiedDate);
			} else {
				throw new DateFormatException();
			}
		}

		return caseDao.countFullTextSearchForCaseSeed(caseSearchKeyword, userId, creationDateTemp, modificatonDateTemp,
				type, status, priority);
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> getAllCasesByUser(long userId, Integer pageNumber, Integer recordsPerPage, String type,
			Integer status, Integer priority, String orderBy, String orderIn) {

		return caseDao.getAllCasesByUser(userId, pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, type, status, priority,
				orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public void mapUserWithCase(MultipartFile uploadFile, Long bstSeedId, Long userId, Long analystId)
			throws Exception {
		if (uploadFile == null || bstSeedId == null)
			throw new InsufficientDataException("Could not process your request, please provide mandatory data");
		if (!caseDao.isCaseAccessibleOrOwner(bstSeedId, userId))
			throw new PermissionDeniedException();
		Case caseSeed = caseDao.find(bstSeedId);
		if (caseSeed != null) {
			Users user = null;
			if (analystId != null)
				user = usersDao.getAnalystByUserId(analystId);
			Users analyst = null;
			boolean caseAssignment = false;
			if (user != null && !caseDao.isAnalystRejectedCaseBefore(analystId, bstSeedId)) {
				analyst = user;
				caseAssignment = true;
			} else {
				analyst = getNextAnalyst(bstSeedId);
				if (analyst != null)
					caseAssignment = true;
			}
			if (!caseAssignment)
				throw new FailedToExecuteQueryException(
						"Could not find analyst for given case or analyst has rejected the case");
			byte jsonFile[] = updateJsonFileWithKey(uploadFile, bstSeedId);

			//String key = null;
			//key = sendCaseToServer(BIG_DATA_BASE_URL, new String(jsonFile, UTF8_CHARSET));
			//caseDao.assignCaseToAnalyst(analyst, caseSeed);
			//BPM Code Started
			//identityService.setAuthenticatedUserId(userId.toString());
			//Map<String,Object> variables=new HashMap<String,Object>();
			//variables.put("analyst", analyst);
			//variables.put("caseSeed", caseSeed);
			//ProcessInstance instance=runtimeService.startProcessInstanceByKey(BPM_PROCESS_DEFINITION_KEY,variables);
			//ProcessInstance instance=runtimeService.startProcessInstanceByKey("caseModel",variables);
			//BPM Code ended

			List<String> keys = null;
			keys = sendCaseToServer(BIG_DATA_BASE_URL, new String(jsonFile, UTF8_CHARSET));
			caseDao.assignCaseToAnalyst(analyst, caseSeed);
			// BPM Code Started
			/*
			 * identityService.setAuthenticatedUserId(userId.toString());
			 * Map<String,Object> variables=new HashMap<String,Object>();
			 * //variables.put("analyst", analyst); variables.put("caseSeed",
			 * caseSeed); //ProcessInstance
			 * instance=runtimeService.startProcessInstanceByKey("caseModel",
			 * variables); //BPM Code ended
			 */
			caseSeed.setCurrentStatus(StatusEnum.SUBMITTED.getIndex());
			caseSeed.setModifiedOn(new Date());
			caseSeed.setCaseJsonFile(jsonFile);
			caseSeed.setCaseIdFromServer(keys.get(0));
			caseSeed.setDecisionScoringEntityId(keys.get(1));
			caseSeed.setDirectRisk(calculateRiskFromQuestioner(uploadFile));
			caseDao.saveOrUpdate(caseSeed);
			eventPublisher.publishEvent(
					new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.SEED_CASE, userId, new Date()));
			eventPublisher.publishEvent(new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.ASSIGN_CASE, userId,
					new Date(), analyst.getUserId()));
		} else {
			throw new NoDataFoundException(ElementConstants.CASE_SEED_NOT_FOUND);
		}
	}

	private List<String> sendCaseToServer(String url, String text) throws Exception {

		String serverResponse[] = null;
		String serverId = null;
		String entityId = null;
		List<String> keys = new ArrayList<>();
		int count = 0;
		do {
			serverResponse = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, text);
			count++;
		} while (serverResponse != null && Integer.parseInt(serverResponse[0]) != 200
				&& count < NUMBER_OF_ATTEMPT_TO_BIG_DATA);
		if (serverResponse != null && serverResponse[0] != null  && Integer.parseInt(serverResponse[0]) == 200 && serverResponse[1] != null
				&& serverResponse[1].trim().length() > 0) {
			JSONParser jsonObject = new JSONParser();
			JSONObject obj = (JSONObject) jsonObject.parse(serverResponse[1].trim());
			JSONObject data = (JSONObject) obj.get("data");
			serverId = (String) data.get("caseId");
			if(data.containsKey("rootEntityId"))
				entityId = (String) data.get("rootEntityId");
		} else {
			throw new Exception(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
		}
		keys.add(serverId);
		keys.add(entityId);
		return keys;
	}

	@SuppressWarnings("unchecked")
	private byte[] updateJsonFileWithKey(MultipartFile uploadFile, Long bstSeedId)
			throws IOException, ParseException, org.json.simple.parser.ParseException {

		if (uploadFile == null || uploadFile.getSize() <= 0)
			throw new FileNotFoundException("Could not find any json file");

		if (!ensureJsonFileFormat(uploadFile.getOriginalFilename()))
			throw new FileFormatException("Could not find json file, please ensure you have uploaded json file");

		JSONParser parser = new JSONParser();

		InputStream inputStreamObject = uploadFile.getInputStream();
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStreamObject, UTF8_CHARSET));
		JSONObject jsonObject = (JSONObject) parser.parse(streamReader);
		String rule = (String) jsonObject.get("rule");
		if (!StringUtils.isEmpty(rule))
			jsonObject.remove("rule");
		jsonObject.put(ElementConstants.CASE_SEED_SEED_JSON_FILE_ID, String.valueOf(bstSeedId));

		return jsonObject.toString().getBytes();
	}

	private boolean ensureJsonFileFormat(String jsonFileName) {
		return FilenameUtils.getExtension(jsonFileName).equalsIgnoreCase("json");
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> getAllIncomingTrayBasedOnUserId(Long analystId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		return caseDao.getAllIncomingtrayBasedOnUserId(analystId,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public long countgetAllIncomingTray(Long userId) {
		return caseDao.countgetAllIncomingTray(userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> fullTextSearchForIncomingtray(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, String creationDate, String modifiedDate, String orderBy, String orderIn)
			throws ParseException {
		Date creationDateTemp = null;
		Date modificatonDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertToDateddMMyyyy(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (modifiedDate != null && modifiedDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(modifiedDate)) {
				modificatonDateTemp = LoggerUtil.convertToDateddMMyyyy(modifiedDate);
			} else {
				throw new DateFormatException();
			}
		}

		return caseDao.fullTextSearchForIncomingtray(userId, caseSearchKeyword,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, creationDateTemp,
				modificatonDateTemp, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public long countFullTextSearchForIncomingtray(Long userId, String caseSearchKeyword, String creationDate,
			String modifiedDate) throws ParseException {
		Date creationDateTemp = null;
		Date modificatonDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertJavaDateToSqlDate(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (modifiedDate != null && modifiedDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(modifiedDate)) {
				modificatonDateTemp = LoggerUtil.convertJavaDateToSqlDate(modifiedDate);
			} else {
				throw new DateFormatException();
			}
		}

		return caseDao.countFullTextSearchForIncomingtray(userId, caseSearchKeyword, creationDateTemp,
				modificatonDateTemp);
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> getAllMyCasesBasedOnUserId(Long userId, String orderBy, String orderIn, Integer pageNumber,
			Integer recordsPerPage) {

		return caseDao.getAllMyCasesBasedOnUserId(userId, orderBy, orderIn,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
	}

	@Override
	@Transactional("transactionManager")
	public long countgetAllMyCases(Long userId) {
		return caseDao.countgetAllMyCases(userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> fullTextSearchForMyCases(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, String creationDate, String modifiedDate, String orderBy, String orderIn)
			throws ParseException {
		Date creationDateTemp = null;
		Date modificatonDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertJavaDateToSqlDate(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (modifiedDate != null && modifiedDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(modifiedDate)) {
				modificatonDateTemp = LoggerUtil.convertJavaDateToSqlDate(modifiedDate);
			} else {
				throw new DateFormatException();
			}
		}

		return caseDao.fullTextSearchForMyCases(userId, caseSearchKeyword,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, creationDateTemp,
				modificatonDateTemp, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public long countFullTextSearchForMyCases(Long userId, String caseSearchKeyword, String creationDate,
			String modifiedDate) throws ParseException {
		Date creationDateTemp = null;
		Date modificatonDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertJavaDateToSqlDate(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (modifiedDate != null && modifiedDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(modifiedDate)) {
				modificatonDateTemp = LoggerUtil.convertJavaDateToSqlDate(modifiedDate);
			} else {
				throw new DateFormatException();
			}
		}
		return caseDao.countFullTextSearchForMyCases(userId, caseSearchKeyword, creationDateTemp, modificatonDateTemp);
	}

	@Override
	@Transactional("transactionManager")
	public String getRelatedCasesById(Long caseId) {
		Case relatedCase = caseDao.find(caseId);
		if (relatedCase == null)
			throw new CaseSeedNotFoundException();
		String serverResponse[] = ServiceCallHelper
				.getDataFromServer(BIG_DATA_BASE_URL + "/" + relatedCase.getCaseIdFromServer() + "/similar");
		if (Integer.parseInt(serverResponse[0]) == 200) {
			return serverResponse[1];
		} else {
			throw new NoDataFoundException("Could not fetch any data from Big data server.");
		}

	}

	@Override
	@Transactional("transactionManager")
	public String getHotTopicsByCaseId(Long caseId) {
		Case hotTopicCase = caseDao.find(caseId);
		if (hotTopicCase == null)
			throw new CaseSeedNotFoundException();
		String serverResponse[] = ServiceCallHelper
				.getDataFromServer(BIG_DATA_BASE_URL + "/" + hotTopicCase.getCaseIdFromServer() + "/topics");
		if (Integer.parseInt(serverResponse[0]) == 200) {
			return serverResponse[1];
		} else {
			throw new NoDataFoundException("Could not fetch any data from Big data server.");
		}
	}

	@Override
	@Transactional("transactionManager")
	public CaseSource getCaseSourceById(Long caseId, Integer limit) {
		if (limit == null)
			limit = 1000;
		GraphObject graphObject = GraphObject.create(getAllDataFromBigDataServer(caseId, limit));
		ArrayList<LinkedTreeMap<String, String>> vertexes = graphObject.getVertexes();
		CaseSource caseSource = new CaseSource();
		ArrayList<Source> source = new ArrayList<Source>();
		if (vertexes == null) {
			caseSource.setWebsites(source);
			return caseSource;
		}

		for (LinkedTreeMap<String, String> vertex : vertexes) {
			// Check if labelV is website
			if (vertex.containsKey("labelV")) {
				if (vertex.get("labelV") != null && vertex.get("labelV").equals("website")) {
					source.add(new Source(vertex.containsKey("host") ? vertex.get("host") : null,
							vertex.containsKey("name") ? vertex.get("name") : null));
				}
			}
		}
		caseSource.setWebsites(source);
		return caseSource;
	}

	public String getAllDataFromBigDataServer(Long seedId, Integer limit) {
		// String jsonData = null;
		if (seedId == null || limit == null)
			throw new InsufficientDataException("Please provide mandatory fields");

		Case caseSeed = caseDao.find(seedId);
		if (caseSeed != null && caseSeed.getCaseIdFromServer() != null) {
			String url = null;
			url = BIG_DATA_BASE_URL + "/" + caseSeed.getCaseIdFromServer() + "/graph/" + limit;
			String serverResponse[] = ServiceCallHelper.getDataFromServer(url);
			if (Integer.parseInt(serverResponse[0]) == 200) {
				return serverResponse[1];
			} else {
				throw new NoDataFoundException("Could not fetch any data from Big data server.");
			}
		} else {
			throw new CaseSeedNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public String getCaseSummaryById(Long caseId) {
		String caseSeedId;
		Case caseSeed = null;
		if (caseId != null) {
			caseSeed = caseDao.find(caseId);
			if (caseSeed == null)
				throw new CaseSeedNotFoundException();
		}
		caseSeedId = caseId == null ? "all" : caseSeed.getCaseIdFromServer();
		String serverResponse[] = ServiceCallHelper.getDataFromServer(BIG_DATA_BASE_URL + "/" + caseSeedId);
		if (Integer.parseInt(serverResponse[0]) == 200) {
			return serverResponse[1];
		} else {
			throw new NoDataFoundException("Could not fetch any data from Big data server.");
		}
	}

	@Override
	@Transactional("transactionManager")
	public Long countAllCasesByUser(long userId, String type, Integer status, Integer priority) {
		return caseDao.countAllCasesByUser(userId, type, status, priority);
	}

	@Override
	@Transactional("transactionManager")
	public Users getNextAnalyst(Long caseId) {
		List<Users> analysts = usersDao.getAllAnalystWhoDidNotRejectedCaseBefore(caseId);
		Users analyst = null;
		if (!analysts.isEmpty()) {
			int index = new Random().nextInt(analysts.size());
			analyst = analysts.get(index);
		}
		return analyst;
	}

	@Override
	@Transactional("transactionManager")
	public JsonObject getAnalystMetrics(String dateFrom, String dateTo, String type, Boolean currentOnly,
			Long analystId) throws ParseException {
		Date FromDate = null;
		Date toDate = null;
		if (dateFrom != null && dateFrom.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(dateFrom)) {
				FromDate = LoggerUtil.convertToDateddMMyyyy(dateFrom);
			} else {
				throw new DateFormatException();
			}
		}
		if (dateTo != null && dateTo.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(dateTo)) {
				toDate = LoggerUtil.convertToDateddMMyyyy(dateTo);
			} else {
				throw new DateFormatException();
			}
		}
		JsonObject responseObject = new JsonObject();
		List<CaseAnalystMapping> caseList = caseAnalystMappingDao.getAnalystMetrics(FromDate, toDate, type, currentOnly,
				analystId);
		responseObject.addProperty(StatusEnum.FRESH.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.FRESH.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.SUBMITTED.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.SUBMITTED.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.ACKNOWLEDGE.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.ACKNOWLEDGE.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.ACCEPTED.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.ACCEPTED.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.PAUSED.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.PAUSED.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.REJECTED.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.REJECTED.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.FOCUS.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.FOCUS.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.RESOLVED.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.RESOLVED.getIndex() == caseSeed.getCurrentStatus()).count());
		responseObject.addProperty(StatusEnum.CANCELED.getName(), caseList.stream()
				.filter(caseSeed -> StatusEnum.CANCELED.getIndex() == caseSeed.getCurrentStatus()).count());
		return responseObject;
	}

	@Transactional("transactionManager")
	public List<CaseDto> getCaseList(CaseVo caseVo, Long userId) throws Exception {
		if (caseVo.getPageNumber() == null) {
			caseVo.setPageNumber(ElementConstants.DEFAULT_PAGE_NUMBER);
		}
		if (caseVo.getRecordsPerPage() == null) {
			caseVo.setRecordsPerPage(ElementConstants.DEFAULT_PAGE_SIZE);
		}
		return caseDao.getCaseList(caseVo, userId);
	}
	
	@Override
	@Transactional("transactionManager")
	public List<CaseDto> getRiskScores(List<CaseDto> caseList) {
		for (CaseDto caseDto : caseList) {
			String response;
			String entityId = null;
			try {
				if (caseDto.getMultiSourceId() != null) {
					entityId = caseDto.getMultiSourceId();
				} /*else {
					entityId = getMultiSourceId(caseDto.getName());
					Case caseSeed = caseDao.find(caseDto.getCaseId());
					if(caseSeed != null && entityId != null){
						caseSeed.setMultiSourceId(entityId);
						caseDao.saveOrUpdate(caseSeed);
					}
				}*/
				if (entityId != null) {
					caseDto.setMultiSourceId(entityId);
					response = getRiskScoresFromServer(caseDto.getCaseId().toString(), entityId);
					if (response != null) {
						org.json.JSONObject json = new org.json.JSONObject(response);
						/*
						 * if (json.has("vertices")) { org.json.JSONArray
						 * vertices = json.getJSONArray("vertices"); if
						 * (vertices.length() > 0) { org.json.JSONObject
						 * verticesJson = vertices.getJSONObject(0); if
						 * (verticesJson.has("riskScore"))
						 * caseDto.setRiskScore(verticesJson.getDouble(
						 * "riskScore")); if (verticesJson.has("dr"))
						 * caseDto.setDirectRisk(verticesJson.getDouble("dr" ));
						 * if (verticesJson.has("tr"))
						 * caseDto.setTransactionalRisk(verticesJson. getDouble(
						 * "tr")); if (verticesJson.has("ir"))
						 * caseDto.setIndirectRisk(verticesJson.getDouble(
						 * "ir")) ; } }
						 */

						if (json.has("latest")) {
							org.json.JSONObject latest = json.getJSONObject("latest");
							if (latest.has("entityRiskModel")) {
								org.json.JSONObject entityRiskModel = latest.getJSONObject("entityRiskModel");
								if (entityRiskModel.has("overall-score")) {
									caseDto.setDirectRisk(entityRiskModel.getDouble("overall-score"));
									caseDto.setRiskScore(0.0);
									caseDto.setTransactionalRisk(0.0);
									caseDto.setIndirectRisk(0.0);
								}
							}

						}

						caseDto.setCaseData(response);
					} else {
						caseDto.setRiskScore(0.0);
						caseDto.setDirectRisk(0.0);
						caseDto.setTransactionalRisk(0.0);
						caseDto.setIndirectRisk(0.0);
					}
				} else {
					caseDto.setRiskScore(0.0);
					caseDto.setDirectRisk(0.0);
					caseDto.setTransactionalRisk(0.0);
					caseDto.setIndirectRisk(0.0);
				}
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseServiceImpl.class);
			}
		}
		return caseList;
	}

	private String getRiskScoresFromServer(String caseId,String decisionScoringEntityId) throws Exception {
		String response = null;		
		String serverResponse[] = ServiceCallHelper.getDataFromServer(BIGDATA_ENTITY_RISKSCORE_URL+decisionScoringEntityId+"?client_id="+CLIENT_ID);
		if (Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response)) {
				return response;
			} else {
				return response;
				//throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		}else{
			return response;
			//throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public Long caseListCount(CaseVo caseVo, Long userId) {
		return caseDao.getCaseListCount(caseVo, userId);

	}

	@Override
	@Transactional("transactionManager")
	public List<CaseAggregator> caseAggregator(Long userId, CaseAggregatorDto caseAggregatorDto) {
		return caseDao.caseAggregator(userId, caseAggregatorDto);
	}

	@Override
	@Transactional("transactionManager")
	public List<Answer> calculateRisk(List<Answer> answers, Answer rule) {
		List<Answer> resultAnswers = new ArrayList<Answer>();
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(KIE_SERVER_URL, KIE_SERVER_USERNAME,
				KIE_SERVER_PASSWORD);
		Set<Class<?>> allClasses = new HashSet<Class<?>>();
		allClasses.add(Answer.class);
		config.addExtraClasses(allClasses);
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);
		RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
		for (Answer answer : answers) {
			List<GenericCommand<?>> commands = new ArrayList<GenericCommand<?>>();
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newInsert(answer, "Risk"));
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newFireAllRules("FireRules"));
			BatchExecutionCommandImpl batchCommand = new BatchExecutionCommandImpl(commands);
			batchCommand.setLookup("defaultKieSession");
			ServiceResponse<ExecutionResults> responseResult = ruleClient.executeCommandsWithResults(rule.getResponse(),
					batchCommand);
			resultAnswers.add((Answer) responseResult.getResult().getValue("Risk"));
		}
		return resultAnswers;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional("transactionManager")
	public void mapUserWithCase(String jsonString, Long bstSeedId, Long userId) throws Exception {
		if (jsonString == null || bstSeedId == null)
			throw new InsufficientDataException("Could not process your request, please provide mandatory data");
		if (!caseDao.isCaseAccessibleOrOwner(bstSeedId, userId))
			throw new PermissionDeniedException();
		Case caseSeed = caseDao.find(bstSeedId);
		if (caseSeed != null) {
			Users user = null;
			if (userId != null)
				user = usersDao.getAnalystByUserId(userId);
			Users analyst = null;
			boolean caseAssignment = false;
			if (user != null && !caseDao.isAnalystRejectedCaseBefore(userId, bstSeedId)) {
				analyst = user;
				caseAssignment = true;
			} else {
				analyst = getNextAnalyst(bstSeedId);
				if (analyst != null)
					caseAssignment = true;
			}
			if (!caseAssignment)
				throw new FailedToExecuteQueryException(
						"Could not find analyst for given case or analyst has rejected the case");
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
			jsonObject.put(ElementConstants.CASE_SEED_SEED_JSON_FILE_ID, String.valueOf(bstSeedId));
			byte jsonFile[] = jsonObject.toString().getBytes();

			List<String> keys = null;
			keys = sendCaseToServer(BIG_DATA_BASE_URL, new String(jsonFile, UTF8_CHARSET));
			if (userId != null)
				identityService.setAuthenticatedUserId(userId.toString());
			Map<String, Object> variables = new HashMap<String, Object>();
			// variables.put("analyst", analyst);
			variables.put("caseSeed", caseSeed);
			//caseDao.assignCaseToAnalyst(analyst, caseSeed);
			 ProcessInstance instance=runtimeService.startProcessInstanceByKey(BPM_PROCESS_DEFINITION_KEY,variables);
			caseSeed.setCurrentStatus(StatusEnum.SUBMITTED.getIndex());
			caseSeed.setModifiedOn(new Date());
			caseSeed.setCaseJsonFile(jsonFile);
			caseSeed.setCaseIdFromServer(keys.get(0));
			caseSeed.setDecisionScoringEntityId(keys.get(1));
			caseDao.saveOrUpdate(caseSeed);
			eventPublisher.publishEvent(
					new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.SEED_CASE, userId, new Date()));
			eventPublisher.publishEvent(new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.ASSIGN_CASE, userId,
					new Date(), analyst.getUserId()));
		} else {
			throw new NoDataFoundException(ElementConstants.CASE_SEED_NOT_FOUND);
		}
	}

	@SuppressWarnings("resource")
	@Override
	public List<Answer> readDataFromCsvFile(MultipartFile uploadFile) throws IOException {
		List<Answer> answers = new ArrayList<Answer>();
		String originalFile = new String(uploadFile.getOriginalFilename());
		String ext = FilenameUtils.getExtension(originalFile);
		String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
		File file = null;
		file = File.createTempFile(fileNameWithoutExtension, ext);
		uploadFile.transferTo(file);
		try (FileReader reader = new FileReader(file);) {
			Map<String, String> values = new CSVReaderHeaderAware(reader).readMap();
			if(values.containsValue("Corporate")){
				String directorList = values.get("DirectorList(Name/DateOfBirth/Nationality/Role/Address)");
			String supplierList = values
					.get("Suppliers List(Supplier Name/Date Of Incorporation/Place Of Incorporation/Address)");
			String customerList = values.get("Customer List(Name/Address)");
			String[] directorsArray = directorList.split(",");
			String[] supplierArray = supplierList.split(",");
			String[] customerArray = customerList.split(",");
			int directorcount = 0;
			for (String string : directorsArray) {
				String[] directorProperties = string.split("\\|");
				directorcount++;
				for (int j = 0; j < directorProperties.length; j++) {
					Answer director = new Answer();
					if (j == 0) {
						director.setQuestionId("directorName" + directorcount);
						director.setResponse(directorProperties[j]);
						director.setTitle("Director Name " + directorcount);
					}

					if (j == 1) {
						director.setResponse(directorProperties[1]);
						director.setQuestionId("directorDateOfBirth" + directorcount);
						director.setTitle("Director DateOfBirth " + directorcount);
					}
					if (j == 2) {
						director.setQuestionId("directorNationality" + directorcount);
						director.setResponse(directorProperties[j]);
						director.setTitle("Director Nationality " + directorcount);

					}
					if (j == 3) {
						director.setQuestionId("directorRole" + directorcount);
						director.setResponse(directorProperties[j]);
						director.setTitle("Director Role " + directorcount);

					}
					if (j == 4) {
						director.setQuestionId("directorAddress" + directorcount);
						director.setResponse(directorProperties[j]);
						director.setTitle("Director Address " + directorcount);

					}
					answers.add(director);

				}
			}

			int supplierCount = 0;
			for (String supplier : supplierArray) {
				String[] supplierProperties = supplier.split("\\|");
				supplierCount++;

				for (int j = 0; j < supplierProperties.length; j++) {
					Answer suppliers = new Answer();
					if (j == 0) {
						suppliers.setQuestionId("supplierName" + supplierCount);
						suppliers.setResponse(supplierProperties[j]);
						suppliers.setTitle("Supplier Name " + supplierCount);
					}

					if (j == 1) {
						suppliers.setResponse(supplierProperties[j]);
						suppliers.setQuestionId("supplierDateOfIncorporation" + supplierCount);
						suppliers.setTitle("Supplier DateOfIncorporation " + supplierCount);
					}
					if (j == 2) {
						suppliers.setQuestionId("supplierPlaceOfIncorporation" + supplierCount);
						suppliers.setResponse(supplierProperties[j]);
						suppliers.setTitle("Supplier PlaceOfIncorporation " + supplierCount);

					}
					if (j == 3) {
						suppliers.setQuestionId("supplierAddress" + supplierCount);
						suppliers.setResponse(supplierProperties[j]);
						suppliers.setTitle("Supplier Address " + supplierCount);

					}
					answers.add(suppliers);

				}
			}
			int coustomerCount = 0;
			for (String customer : customerArray) {
				coustomerCount++;
				String[] customerProperties = customer.split("\\|");
				for (int j = 0; j < customerProperties.length; j++) {
					Answer customers = new Answer();
					if (j == 0) {
						customers.setQuestionId("customerName" + coustomerCount);
						customers.setResponse(customerProperties[j]);
						customers.setTitle("Customer Name " + coustomerCount);
					}

					if (j == 1) {
						customers.setResponse(customerProperties[j]);
						customers.setQuestionId("customerAddress" + coustomerCount);
						customers.setTitle("Customer Address " + coustomerCount);
					}
					answers.add(customers);

				}
			}
			}

			Map<String, String> corporateMap = new HashMap<String, String>();
			corporateMap.put("Is Customer Individual Or Corporate", "customerType");
			corporateMap.put("Company Name", "companyName");
			corporateMap.put("Corporate ID", "corporateId");
			corporateMap.put("Years in Operation", "yearsInOperation");
			corporateMap.put("Number of Shareholders", "numberOfShareholders");
			corporateMap.put("Does company operate in high risk countries ?", "operatesRiskCountry");
			corporateMap.put("If yes, Please select country", "highRiskCountry");
			corporateMap.put("Website", "website");
			corporateMap.put("Date of Incorporation", "dateOfIncorporation");
			corporateMap.put("Remark", "remark");
			corporateMap.put("Description", "description");
			corporateMap.put("Priority [0/1/2]", "priority");
			corporateMap.put("Authorized Signatory", "authorizedSignatory");
			corporateMap.put("Ultimate Beneficial Owner", "beneficialOwner");
			corporateMap.put("Trading as name", "tradingAsName");
			corporateMap.put("Tax ID/Tax Number", "taxId");
			corporateMap.put("Address", "address");
			corporateMap.put("Business Address", "bussinessAddress");
			corporateMap.put("Head Quarter Address", "headQuarterAddress");
			corporateMap.put("Primary Type of Business", "primaryBusiness");
			corporateMap.put("Secondary Type of Business", "secondaryBusiness");
			corporateMap.put("Fax Number", "faxNumber");
			corporateMap.put("Countries of Operation", "countriesOfOperation");
			corporateMap.put("Parent Company", "parentCompany");
			corporateMap.put("Number of Owners", "numberOfOwners");
			corporateMap.put("Number of Directors", "numberOfDirectors");
			corporateMap.put("Number of Suppliers", "numberOfSuppliers");
			corporateMap.put("Number of Customers", "numberOfCustomers");
			corporateMap.put("Type", "type");
			corporateMap.put("Rule", "rule");

			Map<String, String> individualMap = new HashMap<String, String>();
			individualMap.put("Is Customer Individual Or Corporate", "customerType");
			individualMap.put("Remark", "remark");
			individualMap.put("Description", "description");
			individualMap.put("Priority [0/1/2]", "priority");
			individualMap.put("Gross yearly income", "grossYearlyIncome");
			individualMap.put("Address", "address");
			individualMap.put("Gender", "gender");
			individualMap.put("Date Of Birth", "dateOfBirth");
			individualMap.put("Age Group", "ageGroup");
			individualMap.put("Employment Status", "employmentStatus");
			individualMap.put("Country of residence", "countryOfResidence");
			individualMap.put("Country of citizenship", "countryOfCitizenship");
			individualMap.put("Phone Number", "phoneNumber");
			individualMap.put("Tax ID/Tax Number", "taxId");
			individualMap.put("Name Of Person", "name");
			individualMap.put("Email", "email");
			individualMap.put("Type", "type");
			individualMap.put("Rule", "rule");
			individualMap.put("Place of Birth", "placeOfBirth");


			values.forEach((k, v) -> {
				Answer answer = new Answer();
				if (values.containsValue("Individual")) {
					answer.setTitle(k);
					answer.setQuestionId(individualMap.get(k));
					answer.setResponse(v);
					answers.add(answer);

				} else {
					if (!k.equals("DirectorList(Name/DateOfBirth/Nationality/Role/Address)")
							&& !k.equals(
									"Suppliers List(Supplier Name/Date Of Incorporation/Place Of Incorporation/Address)")
							&& !k.equals("Customer List(Name/Address)")) {

						answer.setTitle(k);
						answer.setQuestionId(corporateMap.get(k));
						answer.setResponse(v);
						answers.add(answer);

					}
				}

			});

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseServiceImpl.class);
		}

		return answers;
	}

	@Override
	public String generateSeedJsonData(List<Answer> answers) {
		Map<String, String> queMap = answers.stream()
				.collect(Collectors.toMap(Answer::getQuestionId, Answer::getResponse));
		JsonObject seedJson = new JsonObject();

		if (!queMap.isEmpty()) {
			String customerType = null;
			seedJson.addProperty("type", "KYC");
			seedJson.addProperty("exploration-id", "123");
			if (queMap.containsKey("rule"))
				queMap.remove("rule");
			if (queMap.containsKey("customerType")) {
				customerType = queMap.get("customerType");
				seedJson.addProperty("customerType", customerType);
				queMap.remove("customerType");
			}
			if (queMap.containsKey("remark")) {
				seedJson.addProperty("remark", queMap.get("remark"));
				queMap.remove("remark");
			}
			if (queMap.containsKey("description")) {
				seedJson.addProperty("description", queMap.get("description"));
				queMap.remove("description");
			}
			if (queMap.containsKey("priority")) {
				seedJson.addProperty("priority", queMap.get("priority"));
				queMap.remove("priority");
			}
			if (customerType.equalsIgnoreCase("Individual")) {
				JsonObject person = new JsonObject();
				for (Map.Entry<String, String> entry : queMap.entrySet()) {
					person.addProperty(entry.getKey(), entry.getValue());
				}
				JsonArray personArray = new JsonArray();
				personArray.add(person);
				seedJson.add("persons", personArray);
				JsonArray companyArray = new JsonArray();
				seedJson.add("companies", companyArray);
			} else if (customerType.equalsIgnoreCase("Corporate")) {
				Integer numberOfDirectors = null, numberOfSuppliers = null, numberOfCustomers = null;
				if (queMap.containsKey("numberOfDirectors")) {
					if(queMap.get("numberOfDirectors")!=null && queMap.get("numberOfDirectors")!="" && queMap.get("numberOfDirectors").length()>0)
						numberOfDirectors = Integer.parseInt(queMap.get("numberOfDirectors"));
				}
				if (queMap.containsKey("numberOfSuppliers")) {
					if(queMap.get("numberOfSuppliers")!=null && queMap.get("numberOfSuppliers")!="" && queMap.get("numberOfSuppliers").length()>0)
						numberOfSuppliers = Integer.parseInt(queMap.get("numberOfSuppliers"));
				}
				if (queMap.containsKey("numberOfCustomers")) {
					if(queMap.get("numberOfCustomers")!=null && queMap.get("numberOfCustomers")!="" && queMap.get("numberOfCustomers").length()>0)
						numberOfCustomers = Integer.parseInt(queMap.get("numberOfCustomers"));
				}

				JsonArray personArray = new JsonArray();
				JsonArray companyArray = new JsonArray();
				if (queMap.containsKey("parentCompany")) {
					JsonObject company = new JsonObject();
					company.addProperty("name", queMap.get("parentCompany"));
					company.addProperty("type", "Parent");
					queMap.remove("parentCompany");
					companyArray.add(company);
				}
				if (numberOfDirectors != null) {
					for (int i = 1; i <= numberOfDirectors; i++) {
						JsonObject person = new JsonObject();
						if (!StringUtils.isEmpty(queMap.get("directorName" + i))) {
							if (queMap.containsKey("directorName" + i)) {
								person.addProperty("name", queMap.get("directorName" + i));
								queMap.remove("directorName" + i);
							}
							if (queMap.containsKey("directorDateOfBirth" + i)) {
								person.addProperty("dateOfBirth", queMap.get("directorDateOfBirth" + i));
								queMap.remove("directorDateOfBirth" + i);
							}
							if (queMap.containsKey("directorNationality" + i)) {
								person.addProperty("nationality", queMap.get("directorNationality" + i));
								queMap.remove("directorNationality" + i);
							}
							if (queMap.containsKey("directorRole" + i)) {
								person.addProperty("role", queMap.get("directorRole" + i));
								queMap.remove("directorRole" + i);
							}
							if (queMap.containsKey("directorAddress" + i)) {
								person.addProperty("address", queMap.get("directorAddress" + i));
								queMap.remove("directorAddress" + i);
							}
							person.addProperty("type", "Director");
							personArray.add(person);
						}
					}
				}

				if (numberOfSuppliers != null) {
					for (int i = 1; i <= numberOfSuppliers; i++) {
						JsonObject company = new JsonObject();
						if (!StringUtils.isEmpty(queMap.get("supplierName" + i))) {
							if (queMap.containsKey("supplierName" + i)) {
								company.addProperty("name", queMap.get("supplierName" + i));
								queMap.remove("supplierName" + i);
							}
							if (queMap.containsKey("supplierDateOfIncorporation" + i)) {
								company.addProperty("dateOfIncorporation",
										queMap.get("supplierDateOfIncorporation" + i));
								queMap.remove("supplierDateOfIncorporation" + i);
							}
							if (queMap.containsKey("supplierPlaceOfIncorporation" + i)) {
								company.addProperty("placeOfIncorporation",
										queMap.get("supplierPlaceOfIncorporation" + i));
								queMap.remove("supplierPlaceOfIncorporation" + i);
							}
							if (queMap.containsKey("supplierAddress" + i)) {
								company.addProperty("address", queMap.get("supplierAddress" + i));
								queMap.remove("supplierAddress" + i);
							}
							company.addProperty("type", "Supplier");
							companyArray.add(company);
						}
					}
				}

				if (numberOfCustomers != null) {
					for (int i = 1; i <= numberOfCustomers; i++) {
						JsonObject company = new JsonObject();
						if (!StringUtils.isEmpty(queMap.get("customerName" + i))) {
							if (queMap.containsKey("customerName" + i)) {
								company.addProperty("name", queMap.get("customerName" + i));
								queMap.remove("customerName" + i);
							}
							if (queMap.containsKey("customerAddress" + i)) {
								company.addProperty("address", queMap.get("customerAddress" + i));
								queMap.remove("customerAddress" + i);
							}
							company.addProperty("type", "Customer");
							companyArray.add(company);
						}
					}
				}
				JsonObject baseCompany = new JsonObject();
				for (Map.Entry<String, String> entry : queMap.entrySet()) {
					if ("companyName".equalsIgnoreCase(entry.getKey())) {
						baseCompany.addProperty("name", entry.getValue());
					} else {
						baseCompany.addProperty(entry.getKey(), entry.getValue());
					}
				}
				baseCompany.addProperty("type", "Current");
				companyArray.add(baseCompany);
				seedJson.add("persons", personArray);
				seedJson.add("companies", companyArray);
			}

		}
		return seedJson.toString();
	}

	@Override
	@Transactional("transactionManager")
	public List<Long> getCasesByUser(Long currentUserId) {
		return caseDao.getCasesByUser(currentUserId);
	}

	@SuppressWarnings("rawtypes")
	private double calculateRiskFromQuestioner(MultipartFile uploadFile)
			throws IOException, org.json.simple.parser.ParseException {

		if (uploadFile == null || uploadFile.getSize() <= 0)
			throw new FileNotFoundException("Could not find any json file");

		if (!ensureJsonFileFormat(uploadFile.getOriginalFilename()))
			throw new FileFormatException("Could not find json file, please ensure you have uploaded json file");

		JSONParser parser = new JSONParser();

		InputStream inputStreamObject = uploadFile.getInputStream();
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStreamObject, UTF8_CHARSET));
		JSONObject jsonObject = (JSONObject) parser.parse(streamReader);
		List<Answer> answers = new ArrayList<>();
		Answer rule = null;
		String ruleValue = (String) jsonObject.get("rule");
		if (!StringUtils.isEmpty(ruleValue))
			rule = new Answer("rule", "", ruleValue);
		if ("Corporate".equalsIgnoreCase(jsonObject.get("customerType").toString())) {
			JSONArray companies = (JSONArray) jsonObject.get("companies");
			for (int i = 0; i < companies.size(); i++) {
				JSONObject company = (JSONObject) companies.get(i);
				if ("Current".equalsIgnoreCase(company.get("type").toString())) {
					for (Iterator iterator = company.keySet().iterator(); iterator.hasNext();) {
						String key = (String) iterator.next();
						String value = (String) company.get(key);
						answers.add(new Answer(key, "", value));
					}
				}
			}
		}
		if ("Individual".equalsIgnoreCase(jsonObject.get("customerType").toString())) {
			JSONArray persons = (JSONArray) jsonObject.get("persons");
			for (int i = 0; i < persons.size(); i++) {
				JSONObject person = (JSONObject) persons.get(i);
				for (Iterator iterator = person.keySet().iterator(); iterator.hasNext();) {
					String key = (String) iterator.next();
					String value = (String) person.get(key);
					answers.add(new Answer(key, "", value));
				}
			}
		}

		double directRisk = 1;
		if (rule != null && rule.getResponse() != null) {
			List<Answer> resultAnswers = calculateRisk(answers, rule);
			for (Answer ans : resultAnswers) {
				if (ans.getRisk() != 0) {
					directRisk *= (1 - ans.getRisk() / 100.0);
				}
			}
		}
		directRisk = 1 - directRisk;
		return directRisk;
	}

	@Override
	@Transactional("transactionManager")
	public List<Long> getMyCaseIds(Long userId, CaseVo caseVo, Integer limit) {

		return caseDao.getMyCaseIds(userId, caseVo, limit);

	}

	@Override
	@Transactional("transactionManager")
	public void mapUserWithCaseTest(MultipartFile uploadFile, Long bstSeedId, Long userId, Long analystId)
			throws Exception {
		if (uploadFile == null || bstSeedId == null)
			throw new InsufficientDataException("Could not process your request, please provide mandatory data");
		if (!caseDao.isCaseAccessibleOrOwner(bstSeedId, userId))
			throw new PermissionDeniedException();
		Case caseSeed = caseDao.find(bstSeedId);
		if (caseSeed != null) {
			Users user = null;
			if (analystId != null)
				user = usersDao.getAnalystByUserId(analystId);
			Users analyst = null;
			boolean caseAssignment = false;
			if (user != null && !caseDao.isAnalystRejectedCaseBefore(analystId, bstSeedId)) {
				analyst = user;
				caseAssignment = true;
			} else {
				analyst = getNextAnalyst(bstSeedId);
				if (analyst != null)
					caseAssignment = true;
			}
			if (!caseAssignment)
				throw new FailedToExecuteQueryException(
						"Could not find analyst for given case or analyst has rejected the case");
			byte jsonFile[] = updateJsonFileWithKey(uploadFile, bstSeedId);

			List<String> keys = null;
			keys = sendCaseToServer(BIG_DATA_BASE_URL, new String(jsonFile, UTF8_CHARSET));
			identityService.setAuthenticatedUserId(userId.toString());
			Map<String, Object> variables = new HashMap<String, Object>();
			// variables.put("analyst", analyst);
			variables.put("caseSeed", caseSeed);
			 ProcessInstance instance=runtimeService.startProcessInstanceByKey(BPM_PROCESS_DEFINITION_KEY,variables);
			// caseDao.assignCaseToAnalyst(analyst, caseSeed);
			caseSeed.setCurrentStatus(StatusEnum.SUBMITTED.getIndex());
			caseSeed.setModifiedOn(new Date());
			caseSeed.setCaseJsonFile(jsonFile);
			caseSeed.setCaseIdFromServer(keys.get(0));
			caseSeed.setDecisionScoringEntityId(keys.get(1));
			caseSeed.setDirectRisk(calculateRiskFromQuestioner(uploadFile));
			caseDao.saveOrUpdate(caseSeed);
			eventPublisher.publishEvent(
					new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.SEED_CASE, userId, new Date()));
			eventPublisher.publishEvent(new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.ASSIGN_CASE, userId,
					new Date(), analyst.getUserId()));
		} else {
			throw new NoDataFoundException(ElementConstants.CASE_SEED_NOT_FOUND);
		}
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public CaseMapDocIdDto createCaseFromQuestionnaireFile(MultipartFile uploadFile, Long userId,Long docIdForNew,byte[] fileData) throws IOException, IllegalStateException, InvalidFormatException,TesseractException,InsufficientDataException, Exception {
		DocumentVault documentValut = new DocumentVault();
		String fileExtension = "";
		if(docIdForNew != null){
			try{
			documentValut = documentService.find(docIdForNew);	
			}catch (Exception e) {
				throw new NoDataFoundException("Document not found");
			}
			fileExtension = documentValut.getType();
		}
		else{
			fileExtension = FilenameUtils.getExtension(uploadFile.getOriginalFilename());
		}
		CaseMapDocIdDto caseMapDocIdDto = new CaseMapDocIdDto();
		Map<String, String> caseFields = new HashMap<>();
		if ((uploadFile != null && uploadFile.getSize() > 0) || docIdForNew != null) {
			DocumentTemplates matchedDocumentTemplate = documentParserService.compareFileWithDocumentTemplate(uploadFile,docIdForNew,userId,fileData);
			/*if(matchedDocumentTemplate ==null && fileExtension.equals("pdf")){
				matchedDocumentTemplate = documentTemplatesService.saveNewTemplate(uploadFile);
			}*/
			if (matchedDocumentTemplate != null) {				
				long docId = 0;
				if(docIdForNew != null){
					DocumentVault docuumentValutNew = new DocumentVault();
					BeanUtils.copyProperties(documentValut, docuumentValutNew);
					docuumentValutNew.setDocId(null);
					docuumentValutNew.setDocFlag(7);					
					docId = documentService.save(docuumentValutNew).getDocId();			
					
				}
				else{
					if(!uploadFile.isEmpty()){
				String originalFile = new String(uploadFile.getOriginalFilename());
				String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);		
				
				docId = documentService.uploadDocument(uploadFile, fileNameWithoutExtension, "", userId, 7,
						matchedDocumentTemplate.getId(),null,null,null,null,null);
				
					}
				}
				DocumentTemplateMapping documentTemplateMapping = new DocumentTemplateMapping();
				documentTemplateMapping.setDocumentId(docId);
				documentTemplateMapping.setTemplateId(matchedDocumentTemplate);				
				documentTemplateMappingService.save(documentTemplateMapping);				
				if (fileExtension.equalsIgnoreCase("docx"))
					caseMapDocIdDto = documentParserService.saveDocumetnAnswers(uploadFile, userId, docId,
							matchedDocumentTemplate.getId());
				else if (fileExtension.equalsIgnoreCase("csv"))
					caseMapDocIdDto = documentParserService.saveDocumetnAnswersCsv(uploadFile, userId, docId,
							matchedDocumentTemplate.getId());
				else if(fileExtension.equalsIgnoreCase("pdf"))
					caseMapDocIdDto = documentParserService.saveDocumetnAnswersPdf(uploadFile, userId, docId,matchedDocumentTemplate,fileData);
				DocumentDto documentDto = new DocumentDto();
				documentDto.setDocumentParsingStatus("completed");				
				documentService.updateDocumentById(documentDto, docId, userId);				
				//caseMapDocIdDto.setCaseFields(caseFields);
				caseMapDocIdDto.setDocId(docId);
				return caseMapDocIdDto;
			}
				else {
				throw new NoDataFoundException("Document is not matched with any template");
				//throw new NoDataFoundException("Document does not matched with any template");
			}
		} else {
			throw new InsufficientDataException("File can not be empty");
		}

	}

	@Override
	@Transactional("transactionManager")
	public String generateSeedJsonData(Map<String, String> fields) {
		JsonObject seedJson = new JsonObject();

		seedJson.addProperty("type", "KYC");
		seedJson.addProperty("exploration-id", "123");
		String customerType = null;
		if (fields.containsKey("customerType")) {
			customerType = fields.get("customerType");
			seedJson.addProperty("customerType", customerType);
			fields.remove("customerType");
		}

		if (customerType!=null && customerType.equalsIgnoreCase("Individual")) {
			JsonObject person = new JsonObject();
			for (Map.Entry<String, String> entry : fields.entrySet()) {
			person.addProperty(entry.getKey(), entry.getValue());
			}
			JsonArray personArray = new JsonArray();
			personArray.add(person);
			seedJson.add("persons", personArray);
			JsonArray companyArray = new JsonArray();
			seedJson.add("companies", companyArray);
			} else if (customerType.equalsIgnoreCase("Corporate")) {
			JsonObject baseCompany = new JsonObject();
			for (Map.Entry<String, String> entry : fields.entrySet()) {	
			baseCompany.addProperty(entry.getKey(), entry.getValue());	
			}
			JsonArray personArray = new JsonArray();
			seedJson.add("persons", personArray);
			JsonArray companyArray = new JsonArray();
			baseCompany.addProperty("type", "Current");
			companyArray.add(baseCompany);
			seedJson.add("persons", personArray);
			seedJson.add("companies", companyArray);
			}
		return seedJson.toString();
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseUnderwritingDto> getCaseListForUnderwriting(Integer recordsPerPage, Integer pageNumber,
			String keyword) {
		List<CaseUnderwritingDto> caseList = new ArrayList<>();
		if(!StringUtils.isEmpty(keyword)){
			List<Long> docIds = documentDao.searchUnderwritingDocumentsIdsByName(keyword);
			if(!docIds.isEmpty()){
			caseList = caseDao.getCaseListForUnderwriting(
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, docIds);
			}
		}else{
			caseList = caseDao.getCaseListForUnderwriting(
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, new ArrayList<>());
		}
		if (!caseList.isEmpty()) {
			for (CaseUnderwritingDto caseSeed : caseList) {
				if (caseSeed.getDocId() != null) {
					DocumentVault documentVault = documentDao.find(caseSeed.getDocId());
					if (documentVault != null) {
						caseSeed.setDocType(documentVault.getType());
						caseSeed.setFileName(documentVault.getDocName());
						caseSeed.setSize(documentVault.getSize());
					}
				}
				CaseAnalystMapping mapping = caseAnalystMappingDao.findByCaseId(caseSeed.getCaseId());
				if(mapping != null){
					caseSeed.setAssignedTo(mapping.getUser().getFirstName() + " " + mapping.getUser().getLastName());
					caseSeed.setAssigneeId(mapping.getUser().getUserId());
				}
			}
		}
		return caseList;
	}

	@Override
	@Transactional("transactionManager")
	public Long caseListCountForUnderwriting(String keyword) {
		if(!StringUtils.isEmpty(keyword)){
			List<Long> docIds = documentDao.searchUnderwritingDocumentsIdsByName(keyword);
			if(!docIds.isEmpty()){
				return caseDao.caseListCountForUnderwriting(docIds);
			}else{
				return 0L;
			}
		}else{
			return caseDao.caseListCountForUnderwriting(new ArrayList<>());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String createEntity(Map<String, String> fields, String type) throws org.json.simple.parser.ParseException {
		JSONObject jsonObject = new JSONObject();
		String url = null;
		if ("Corporate".equalsIgnoreCase(type)) {
			url = CREATE_ORG_ENTITY_URL;
			jsonObject.put("isDomiciledIn", "GB");
			jsonObject.put("@source-id", "website");
			if(fields.containsKey("name")){				
				jsonObject.put("vcard:organization-name", fields.get("name"));
			}
			if(fields.containsKey("phoneNumber")){	
				if(!fields.get("phoneNumber").isEmpty())
					jsonObject.put("tr-org:hasRegisteredPhoneNumber", fields.get("phoneNumber"));
			}
			if(fields.containsKey("website"))
			{				
				if(!fields.get("website").isEmpty())
					jsonObject.put("hasURL", fields.get("website"));
			}
			if(fields.containsKey("email")){	
				if(!fields.get("email").isEmpty())
					jsonObject.put("bst:email", fields.get("email"));
			}
			if(fields.containsKey("address")){
				if(!fields.get("address").isEmpty())
					jsonObject.put("mdaas:RegisteredAddress", fields.get("address"));
			}
		} else if ("Individual".equalsIgnoreCase(type)) {
			url = CREATE_PERSON_ENTITY_URL;
			jsonObject.put("@source-id", "website");
			if(fields.containsKey("name")){				
				jsonObject.put("vcard:hasName", fields.get("name"));
			}
			if(fields.containsKey("phoneNumber")){
				if(!fields.get("phoneNumber").isEmpty())
					jsonObject.put("vcard:hasTelephone", fields.get("phoneNumber"));
			}
			if(fields.containsKey("email")){	
				if(!fields.get("email").isEmpty())
					jsonObject.put("bst:email", fields.get("email"));
			}
			if(fields.containsKey("address")){		
				if(!fields.get("address").isEmpty())
					jsonObject.put("vcard:hasAddress", fields.get("address"));
			}
		}else{
			return null;
		}
		
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonObject.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				JSONParser jsonResponse = new JSONParser();
				JSONObject obj = (JSONObject) jsonResponse.parse(response.trim());
				String id = (String) obj.get("identifier");
				return id;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseUnderwritingDto> searchCaseList(String keyword) {
		List<CaseUnderwritingDto> caseList = new ArrayList<>();
		caseList = caseDao.searchCaseList(keyword);
		if (!caseList.isEmpty()) {
			for (CaseUnderwritingDto caseSeed : caseList) {
				CaseAnalystMapping mapping = caseAnalystMappingDao.findByCaseId(caseSeed.getCaseId());
				if (mapping != null) {
					caseSeed.setAssignedTo(mapping.getUser().getFirstName() + " " + mapping.getUser().getLastName());
					caseSeed.setAssigneeId(mapping.getUser().getUserId());
				}
			}
		}
		return caseList;
	}

	@Override
	@Transactional("transactionManager")
	public Long searchCaseListCount(String keyword) {
		return caseDao.searchCaseListCount(keyword);
	}

	@Override
	public String getIndustry(String entityId) throws org.json.simple.parser.ParseException, JsonParseException, JsonMappingException, URISyntaxException, IOException {
		String url = BIGDATA_PROFILE_ORG_URL +entityId;
		String finalIndustry=null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				JSONParser jsonResponse = new JSONParser();
				JSONObject obj = (JSONObject) jsonResponse.parse(response.trim());
				if( obj.containsKey("basic")){
					JSONObject basicObj = (JSONObject) obj.get("basic");
					if(basicObj.containsKey("bst:businessClassifier")){
						JSONArray businessClassifiers=(JSONArray) basicObj.get("bst:businessClassifier");
						if(businessClassifiers != null && businessClassifiers.size()>0){
							for (int i = 0; i < businessClassifiers.size(); i++) {
								JSONObject industryObj=(JSONObject) businessClassifiers.get(i);
								if(industryObj.containsKey("standard") && industryObj.get("standard").equals("ISIC")){
									if(industryObj.containsKey("code")){
										String indurty=(String) industryObj.get("code");
										Character industrychar=indurty.charAt(0);
										Map<String, String> jsonMap = readIndustryJson();
										 finalIndustry=jsonMap.get(industrychar.toString());
									}
									
								}
								
							}
						}
					}

				}
				return finalIndustry;
			} else {
				return finalIndustry;
			}
		} else {
			return finalIndustry;
		}
	}
	
	public Map<String, String> readIndustryJson() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader(); 
		URL resource = classLoader.getResource("industry.json"); 
		File file = new File(resource.toURI());
		ObjectMapper mapper = new ObjectMapper();

        // read JSON from a file
        Map<String, String> jsonMap = mapper.readValue(file,
                new TypeReference<Map<String, String>>(){});
		return jsonMap;
		
	}

	@Override
	@Transactional("transactionManager")
	public Long assignCaseToAnalyst(Users user, Case bstCaseSeed) {
		Long id = caseDao.assignCaseToAnalyst(user, bstCaseSeed);
		return id;
		
		
	}
	
	@Override
	public String getMultiSourceId(String caseName){
		String entityId = null;
		try{
			String entityData = advanceSearchService.getMultisourceData(caseName, "", null, null);
			if (entityData != null) {
				org.json.JSONObject entityJson = new org.json.JSONObject(entityData);
				
				if(entityJson.has("is-completed")) {
					while(!entityJson.getBoolean("is-completed")) {
						entityData = advanceSearchService.getMultisourceData(caseName, "", null, null);
						entityJson = new org.json.JSONObject(entityData);
					}
				}
					
				if (entityJson.has("results")) {
					org.json.JSONArray results = entityJson.getJSONArray("results");
					
					for (int i = 0; i < results.length(); i++) {
						org.json.JSONObject result = results.getJSONObject(i);
						if (result.has("identifier")) {
							entityId = result.getString("identifier");
						}
					}

				}
			}
		}catch (Exception e) {
			return entityId;
		}
		return entityId;
	}

}
