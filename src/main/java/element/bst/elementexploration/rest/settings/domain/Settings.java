package element.bst.elementexploration.rest.settings.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "SYS_SETTINGS")
public class Settings implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long settingId;
	@Column(name = "NAME")
	private String name;
	@Column(name = "TYPE")
	private String type;
	@Column(name = "VALIDATION")
	private String validation;
	@Column(name = "MAXIMUM_LENGTH")
	private Integer maximumLength;
	@Column(name = "REQUIRED")
	private boolean required;
	@Column(name = "DEFAULT_VALUE")
	@Lob
	private String defaultValue;
	@Column(name = "SYSTEM_SETTING_TYPE")
	private String systemSettingType;
	@Column(name = "CUSTOMER_LOGO_IMAGE")
	@Lob
	private String customerLogoImage;

	@Column(name = "SECTION")
	private String section;
	@OneToMany(mappedBy = "settings", cascade = CascadeType.ALL, targetEntity = Attributes.class, fetch = FetchType.LAZY)
	private List<Attributes> attributesList;

	@Column(name = "SELECTED_VALUE_MAX")
	private Integer selectedValueMax;
	
	@Column(name = "SLIDER_FROM")
	private Integer sliderFrom;
	
	@Column(name = "SLIDER_TO")
	private Integer sliderTo;
	
	public Settings() {
	}

	public Settings(long settingId, String name, String type, String validation, Integer maximumLength, boolean required,
			String defaultValue, String systemSettingType, String section, List<Attributes> attributesList, 
			Integer selectedValueMax, Integer sliderFrom, Integer sliderTo) {
		super();
		this.settingId = settingId;
		this.name = name;
		this.type = type;
		this.validation = validation;
		this.maximumLength = maximumLength;
		this.required = required;
		this.defaultValue = defaultValue;
		this.systemSettingType = systemSettingType;
		this.section = section;
		this.attributesList = attributesList;
		this.selectedValueMax = selectedValueMax;
		this.sliderFrom = sliderFrom;
		this.sliderTo = sliderTo;
	}

	public long getId() {
		return settingId;
	}

	public void setId(long id) {
		this.settingId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

	public Integer getMaximumLength() {
		return maximumLength;
	}

	public void setMaximumLength(Integer maximumLength) {
		this.maximumLength = maximumLength;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public List<Attributes> getAttributesList() {
		return attributesList;
	}

	public void setAttributesList(List<Attributes> attributesList) {
		this.attributesList = attributesList;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSystemSettingType() {
		return systemSettingType;
	}

	public void setSystemSettingType(String systemSettingType) {
		this.systemSettingType = systemSettingType;
	}

	public String getCustomerLogoImage() {
		return customerLogoImage;
	}

	public void setCustomerLogoImage(String customerLogoImage) {
		this.customerLogoImage = customerLogoImage;
	}
	

	public Integer getSelectedValueMax() {
		return selectedValueMax;
	}

	public void setSelectedValueMax(Integer selectedValueMax) {
		this.selectedValueMax = selectedValueMax;
	}

	public Integer getSliderFrom() {
		return sliderFrom;
	}

	public void setSliderFrom(Integer sliderFrom) {
		this.sliderFrom = sliderFrom;
	}

	public Integer getSliderTo() {
		return sliderTo;
	}

	public void setSliderTo(Integer sliderTo) {
		this.sliderTo = sliderTo;
	}

	@Override
	public String toString() {
		return "Settings [id=" + settingId + ", name=" + name + ", type=" + type + ", validation=" + validation
				+ ", maximumLength=" + maximumLength + ", required=" + required + ", defaultValue=" + defaultValue
				+ ", systemSettingType=" + systemSettingType + ", section=" + section + ", attributesList="
				+ attributesList + ", selectedValueMax=" + selectedValueMax + ", sliderFrom" + sliderFrom + ", sliderTo"+ sliderTo + "]";
	}

}
