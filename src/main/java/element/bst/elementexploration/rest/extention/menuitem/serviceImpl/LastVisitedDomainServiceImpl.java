package element.bst.elementexploration.rest.extention.menuitem.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.LastVisitedDomainDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.LastVisitedDomains;
import element.bst.elementexploration.rest.extention.menuitem.dto.LastVisitedDomainDto;
import element.bst.elementexploration.rest.extention.menuitem.service.LastVisitedDomainService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("lastVisitedDomainService")
@Transactional("transactionManager")
public class LastVisitedDomainServiceImpl extends GenericServiceImpl<LastVisitedDomains, Long>
		implements LastVisitedDomainService {

	@Autowired
	LastVisitedDomainDao lastVisitedDomainDao;

	@Autowired
	public LastVisitedDomainServiceImpl(GenericDao<LastVisitedDomains, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public LastVisitedDomains saveOrUpdateLastVisitedDomain(LastVisitedDomainDto lastVisitedDomainDto, Long userId)
			throws Exception {

		if (lastVisitedDomainDto != null) {
			LastVisitedDomains existingRecord = lastVisitedDomainDao
					.findLastVisitedDomainByUserId(lastVisitedDomainDto.getUserId());
			if (existingRecord != null) {
				if (null != lastVisitedDomainDto.getPermissionId()) {
					existingRecord.setPermissionId(lastVisitedDomainDto.getPermissionId());
				}
				if (null != lastVisitedDomainDto.getUserId()) {
					existingRecord.setUserId(lastVisitedDomainDto.getUserId());
				}
				if (null != lastVisitedDomainDto.getDomainName()) {
					existingRecord.setDomainName(lastVisitedDomainDto.getDomainName());
				}
				saveOrUpdate(existingRecord);
				return existingRecord;
			} else {
				LastVisitedDomains newRecord = new LastVisitedDomains();
				if (null != lastVisitedDomainDto.getDomainName()) {
					newRecord.setDomainName(lastVisitedDomainDto.getDomainName());
				}
				if (null != lastVisitedDomainDto.getPermissionId()) {
					newRecord.setPermissionId(lastVisitedDomainDto.getPermissionId());
				}
				if (null != lastVisitedDomainDto.getUserId()) {
					newRecord.setUserId(lastVisitedDomainDto.getUserId());
				}
				return save(newRecord);
			}
		}
		return null;

	}

	@Override
	public LastVisitedDomains findLastVisitedDomainByUserId(Long userId) {
		return lastVisitedDomainDao.findLastVisitedDomainByUserId(userId);
	}
}
