package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregateVo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDtoAscnd;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.DaysCalculationUtility;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.FilterUtility;

/**
 * @author suresh
 *
 */
@Repository("alertDao")
public class AlertDaoImpl extends GenericDaoImpl<Alert, Long> implements AlertDao {

	public AlertDaoImpl() {
		super(Alert.class);
	}

	@Autowired
	CustomerDetailsDao customerDeatilsDao;

	@Autowired
	private DaysCalculationUtility daysCalculationUtility;

	@Autowired
	private FilterUtility filterUtility;

	@SuppressWarnings("unchecked")
	@Override
	public List<Alert> fetchNonResolvedAlerts(Integer pageNumber, Integer recordsPerPage) {
		List<Alert> alertsList = new ArrayList<Alert>();
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("from Alert a where rowStatus=0  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		alertsList = (List<Alert>) query.getResultList();
		if (!alertsList.isEmpty())
			return alertsList;

		return alertsList;

	}

	@Override
	public Alert fetchAlertSummary(long alertId) {
		Alert alert = null;
		try {
			alert = (Alert) this.getCurrentSession().find(Alert.class, alertId);
		} catch (Exception e) {
			throw new NoDataFoundException();
		}
		return alert;
	}

	@Override
	public Alert loadAlertById(Long Id) {
		Alert alert = null;
		try {

			alert = this.getCurrentSession().load(Alert.class, Id);
		} catch (Exception e) {
			throw new NoDataFoundException();
		}
		return alert;
	}

	@Override
	public List<Alert> fetchCount(Long userId) {
		@SuppressWarnings("unchecked")
		List<Alert> alertlist = this.getCurrentSession().createQuery("from Alert where rowStatus=0  and a.entityType='TRANSACTION'").getResultList();
		return alertlist;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Alert> fetchfilterName(String name, String dateFrom, String dateTo, Integer pageNumber,
			Integer recordsPerPage) {

		StringBuilder hql = new StringBuilder();

		if (name != null && dateFrom == null && dateTo == null) {
			hql.append(
					"from Alert a where a.focalNtityDisplayId LIKE '%" + name + "%' or a.focalNtityDisplayName LIKE '%"
							+ name + "%' or  a.searchName LIKE '%" + name + "%' and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}
		if (dateFrom != null && dateTo != null && name == null) {
			hql.append("from Alert  a where DATE(a.alertBusinessDate)>= '" + dateFrom
					+ "' and DATE(a.alertBusinessDate)<='" + dateTo + "' and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}
		if ((name == null || dateFrom != null) && dateTo == null) {
			hql.append("from Alert  a where DATE(a.alertBusinessDate)>='" + dateFrom + "'  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (dateFrom == null && dateTo != null) {
			hql.append("from Alert  a where DATE(a.alertBusinessDate)<='" + dateFrom + "'  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (name != null && dateFrom != null && dateTo == null) {
			hql.append(
					"from Alert a where (a.focalNtityDisplayId LIKE '%" + name + "%' or a.focalNtityDisplayName LIKE '%"
							+ name + "%') and (DATE(a.alertBusinessDate)>='" + dateFrom + "')  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (name != null && dateFrom != null && dateTo != null) {
			hql.append(
					"from Alert a where (a.focalNtityDisplayId LIKE '%" + name + "%' or a.focalNtityDisplayName LIKE '%"
							+ name + "%' or  a.searchName LIKE '%" + name + "%') and (DATE(a.alertBusinessDate)>='"
							+ dateFrom + "' and DATE(a.alertBusinessDate)<='" + dateTo + "')   and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (name == null && dateFrom == null && dateTo == null) {
			hql.append("from Alert a where a.rowStatus=0 and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}
		//System.out.println(hql.toString());
		Query query = this.getCurrentSession().createQuery(hql.toString());
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		@SuppressWarnings("unchecked")
		List<Alert> listAlert = query.getResultList();
		//System.out.println(listAlert.size());

		return listAlert;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int fetchfilterNameCount(String name, String dateFrom, String dateTo, Integer pageNumber,
			Integer recordsPerPage) {

		StringBuilder hql = new StringBuilder();

		if (name != null && dateFrom == null && dateTo == null) {
			hql.append(
					"from Alert a where a.focalNtityDisplayId LIKE '%" + name + "%' or a.focalNtityDisplayName LIKE '%"
							+ name + "%' or  a.searchName LIKE '%" + name + "%'   and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}
		if (dateFrom != null && dateTo != null && name == null) {
			hql.append("from Alert  a where DATE(a.alertBusinessDate)>= '" + dateFrom
					+ "' and DATE(a.alertBusinessDate)<='" + dateTo + "'  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}
		if ((name == null || dateFrom != null) && dateTo == null) {
			hql.append("from Alert  a where DATE(a.alertBusinessDate)>='" + dateFrom + "'  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (dateFrom == null && dateTo != null) {
			hql.append("from Alert  a where DATE(a.alertBusinessDate)<='" + dateFrom + "'  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (name != null && dateFrom != null && dateTo == null) {
			hql.append(
					"from Alert a where (a.focalNtityDisplayId LIKE '%" + name + "%' or a.focalNtityDisplayName LIKE '%"
							+ name + "%') and (DATE(a.alertBusinessDate)>='" + dateFrom + "')  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (name != null && dateFrom != null && dateTo != null) {
			hql.append(
					"from Alert a where (a.focalNtityDisplayId LIKE '%" + name + "%' or a.focalNtityDisplayName LIKE '%"
							+ name + "%' or  a.searchName LIKE '%" + name + "%') and (DATE(a.alertBusinessDate)>='"
							+ dateFrom + "' and DATE(a.alertBusinessDate)<='" + dateTo + "')  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}

		if (name == null && dateFrom == null && dateTo == null) {
			hql.append("from Alert a where a.rowStatus=0  and a.entityType='TRANSACTION' ORDER BY a.id DESC");
		}
		//System.out.println(hql.toString());
		Query query = this.getCurrentSession().createQuery(hql.toString());
		@SuppressWarnings("unchecked")
		List<Alert> listAlert = query.getResultList();
		//System.out.println(listAlert.size());

		return listAlert.size();
	}

	@Override
	public List<Alert> findByCustomerAndDate(String customerNumber, Date date) {
		List<Alert> list = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Alert> query = builder.createQuery(Alert.class);
			Root<Alert> alert = query.from(Alert.class);
			query.select(alert);
			query.distinct(true);
			query.where(builder.and(builder.greaterThan(alert.get("alertBusinessDate"), date)),
					(builder.equal(alert.get("focalNtityDisplayId"), customerNumber)));
			list = (ArrayList<Alert>) this.getCurrentSession().createQuery(query).getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch accounts. Reason : " + e.getMessage());
		}
		return list;
	}

	/*
	 * //fetching alerts amount to trending in notification panel
	 * 
	 * @Override public List<AlertDashBoardRespDto> fetchAlertsBwDatesList(String
	 * dateFrom, String dateTo) throws ParseException {
	 * 
	 * List<AlertDashBoardRespDto> dashBoardList = new
	 * ArrayList<AlertDashBoardRespDto>(); Query<?> query =
	 * this.getCurrentSession().
	 * createQuery("from Alert a where a.alertBusinessDate >='" + dateFrom +
	 * "' and a.alertBusinessDate <='" + dateTo + "'");
	 * 
	 * @SuppressWarnings("unchecked") List<Alert> listAlert = (List<Alert>)
	 * query.getResultList(); for (Alert alert : listAlert) {
	 * 
	 * CustomerDetails customerDetails =
	 * customerDeatilsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId()); {
	 * 
	 * if (customerDetails != null) { AlertDashBoardRespDto alertDashBoardRespDto =
	 * new AlertDashBoardRespDto();
	 * alertDashBoardRespDto.setAlertBusinessDate(alert.getAlertBusinessDate()); if
	 * (customerDetails.getDisplayName() != null)
	 * alertDashBoardRespDto.setCustomerName(customerDetails.getDisplayName());
	 * 
	 * StringBuilder hql = new StringBuilder(); hql.
	 * append("select sum(t.amount) from TransactionsData t where t.beneficiaryCutomerId=:value or t.originatorCutomerId=:value"
	 * ); Query<?> query2 = this.getCurrentSession().createQuery(hql.toString());
	 * query2.setParameter("value",customerDetails.getId()); Double valueReturned1 =
	 * (Double) query2.getSingleResult();
	 * 
	 * if(valueReturned1!=null) {
	 * alertDashBoardRespDto.setTotalAmount(valueReturned1.intValue()); } else {
	 * alertDashBoardRespDto.setTotalAmount(0); }
	 * dashBoardList.add(alertDashBoardRespDto);
	 * 
	 * } }
	 * 
	 * } return dashBoardList; }
	 */

	/**
	 * calculatuing count and ration of alerts
	 * 
	 * @throws ParseException
	 */
	@Override
	public List<Alert> calculateAlertsGenRatio(List<Long> transactionIds, String fromDate, String toDate)
			throws ParseException {

		StringBuilder hql = new StringBuilder();
		hql.append("");
		/*
		 * List<Alert> alertSList = new ArrayList<Alert>(); if <<<<<<< HEAD
		 * (!transactionIds.isEmpty()) { StringBuilder hql = new StringBuilder();
		 * DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); Date utilFromDate
		 * = formatter.parse(fromDate); Date utilToDate = formatter.parse(toDate);
		 * 
		 * hql.
		 * append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertGenRatioDto("
		 * ); hql.
		 * append("count(a.id),(count(a.id)* 100 / (select count(*) From Alert)))" );
		 * 
		 * hql.append(
		 * "from Alert a where a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and a.transactionId IN (:transactionIds)"
		 * ); Query query = this.getCurrentSession().createQuery(hql.toString());
		 * query.setParameter("fromDate", utilFromDate); query.setParameter("toDate",
		 * utilToDate); query.setParameter("transactionIds", transactionIds); alertSList
		 * = (List<Alert>) query.getResultList(); } ======= (!transactionIds.isEmpty())
		 * { StringBuilder hql = new StringBuilder(); DateFormat formatter = new
		 * SimpleDateFormat("yyyy-MM-dd"); Date utilFromDate =
		 * formatter.parse(fromDate); Date utilToDate = formatter.parse(toDate);
		 * 
		 * hql.
		 * append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertGenRatioDto("
		 * ); hql.
		 * append("count(a.id),(count(a.id)* 100 / (select count(*) From Alert)))" );
		 * 
		 * hql.append(
		 * "from Alert a where a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and a.transactionId IN (:transactionIds)"
		 * ); Query query = this.getCurrentSession().createQuery(hql.toString());
		 * query.setParameter("fromDate", utilFromDate); query.setParameter("toDate",
		 * utilToDate); query.setParameter("transactionIds", transactionIds); alertSList
		 * = (List<Alert>) query.getResultList(); } >>>>>>>
		 * 888c96b85868bece95f9fa487a4aa04c12c92ff8
		 */
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Alert> overAllAlertsBwDates(String name, String dateFrom, String dateTo, String orderBy)
			throws ParseException {
		StringBuilder hql = new StringBuilder();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date utilFromDate = formatter.parse(dateFrom);
		Date utilToDate = formatter.parse(dateTo);
		hql.append("from Alert a where a.alertBusinessDate >=:fromDate and a.alertBusinessDate <=:toDate");
		@SuppressWarnings("rawtypes")
		Query query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("fromDate", utilFromDate);
		query.setParameter("toDate", utilToDate);
		List<Alert> listAlert = query.getResultList();
		return listAlert;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertStatusDto> fetchGroupByAlertStatus(String fromDate, String toDate, boolean periodBy,
			FilterDto filterDto) throws ParseException {
		List<AlertStatusDto> alertStatusDtolist = new ArrayList<AlertStatusDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		long days = daysCalculationUtility.getDifferenceBetweenDates(fromDate, toDate);
		if (days >= 30 && periodBy) {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct t.alertStatus,t.alertId from TransactionMasterView t ");
			hql.append(
					" where DATE(t.alertBusinessDate) >=:fromDate and DATE(t.alertBusinessDate) < DATE_ADD(:toDate,-30,DAY) ");

			/*
			 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
			 * (!filterDto.isAlertsByPeriod10to20Days()) &&
			 * (!filterDto.isAlertsByPeriod20to30Days()) &&
			 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
			 * append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
			 * ); }
			 */

			if (filterDto != null)
				hql.append(filterUtility.filterColumns(filterDto));

			// hql.append(" group by a.ALERT_kllSTATUS");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if (periodBy) {
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
			}

			if (filterDto != null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);

			/*
			 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
			 * (!filterDto.isAlertsByPeriod10to20Days()) &&
			 * (!filterDto.isAlertsByPeriod20to30Days()) &&
			 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
			 * query.setParameter("fromDate", formatter.parse(fromDate));
			 * query.setParameter("toDate", formatter.parse(toDate)); } }
			 */

			/*
			 * if (periodBy) { query.setParameter("fromDate", formatter.parse(fromDate));
			 * query.setParameter("toDate", formatter.parse(toDate));
			 * 
			 * }
			 */

			List<Object[]> objArray = (List<Object[]>) query.getResultList();

			Map<String, AlertStatusDto> map = new HashMap<String, AlertStatusDto>();
			for (Object[] object : objArray) {
				if (map.containsKey(object[0].toString())) {
					AlertStatusDto dto = map.get(object[0].toString());
					dto.setCount(dto.getCount() + 1);
				} else {
					AlertStatusDto dto = new AlertStatusDto(AlertStatus.valueOf(object[0].toString()), new Long(1));
					map.put(object[0].toString(), dto);
				}
			}
			for (Entry<String, AlertStatusDto> entry : map.entrySet()) {
				alertStatusDtolist.add(entry.getValue());
			}

			/*
			 * for (Object[] objects : list) { AlertStatusDto alertStatusDto = new
			 * AlertStatusDto();
			 * alertStatusDto.setAlertStatus(AlertStatus.valueOf(objects[0].toString()));
			 * alertStatusDto.setCount(Long.valueOf(objects[1].toString()));
			 * alertStatusDtolist.add(alertStatusDto); }
			 */
		} else if (!periodBy) {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct t.alertStatus,t.alertId from TransactionMasterView t ");
			hql.append(" where DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
			// hql.append(" group by t.alert_status");

			/*
			 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
			 * (!filterDto.isAlertsByPeriod10to20Days()) &&
			 * (!filterDto.isAlertsByPeriod20to30Days()) &&
			 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
			 * append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
			 * ); }
			 */

			if (filterDto != null)
				hql.append(filterUtility.filterColumns(filterDto));

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());

			/*
			 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
			 * (!filterDto.isAlertsByPeriod10to20Days()) &&
			 * (!filterDto.isAlertsByPeriod20to30Days()) &&
			 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
			 * query.setParameter("fromDate", formatter.parse(fromDate));
			 * query.setParameter("toDate", formatter.parse(toDate)); } }
			 */
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));

			if (filterDto != null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);

			List<Object[]> objArray = (List<Object[]>) query.getResultList();

			Map<String, AlertStatusDto> map = new HashMap<String, AlertStatusDto>();
			for (Object[] object : objArray) {
				if (map.containsKey(object[0].toString())) {
					AlertStatusDto dto = map.get(object[0].toString());
					dto.setCount(dto.getCount() + 1);
				} else {
					AlertStatusDto dto = new AlertStatusDto(AlertStatus.valueOf(object[0].toString()), new Long(1));
					map.put(object[0].toString(), dto);
				}
			}
			for (Entry<String, AlertStatusDto> entry : map.entrySet()) {
				alertStatusDtolist.add(entry.getValue());
			}

			/*
			 * for (Object[] objects : list) { AlertStatusDto alertStatusDto = new
			 * AlertStatusDto();
			 * alertStatusDto.setAlertStatus(AlertStatus.valueOf(objects[0].toString()));
			 * alertStatusDto.setCount(Long.valueOf(objects[1].toString()));
			 * alertStatusDtolist.add(alertStatusDto); }
			 */
		}
		return alertStatusDtolist;

	}

	@Override
	public List<AlertDashBoardRespDtoAscnd> fetchAlertsBwDatesOrderBy(String name, String dateFrom, String dateTo,
			Integer pageNumber, Integer recordsPerPage, String orderBy) throws ParseException {
		List<AlertDashBoardRespDtoAscnd> dashBoardList = new ArrayList<AlertDashBoardRespDtoAscnd>();
		Query<?> query = this.getCurrentSession().createQuery("from Alert a where a.alertBusinessDate >='" + dateFrom
				+ "' and a.alertBusinessDate <='" + dateTo + "'");
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		@SuppressWarnings("unchecked")
		List<Alert> listAlert = (List<Alert>) query.getResultList();
		for (Alert alert : listAlert) {

			CustomerDetails customerDetails = customerDeatilsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId());
			{

				if (customerDetails != null) {
					AlertDashBoardRespDtoAscnd alertDashBoardRespDto = new AlertDashBoardRespDtoAscnd();
					alertDashBoardRespDto.setAlertBusinessDate(alert.getAlertBusinessDate());
					if (customerDetails.getDisplayName() != null)
						alertDashBoardRespDto.setCustomerName(customerDetails.getDisplayName());

					StringBuilder hql = new StringBuilder();
					hql.append("select t.amount from TransactionsData t where t.id=:value");
					Query<?> query2 = this.getCurrentSession().createQuery(hql.toString());
					query2.setParameter("value", alert.getTransactionId());
					Double valueReturned1 = (Double) query2.getSingleResult();

					if (valueReturned1 != null) {
						alertDashBoardRespDto.setTotalAmount(valueReturned1.intValue());
					} else {
						alertDashBoardRespDto.setTotalAmount(0);
					}
					dashBoardList.add(alertDashBoardRespDto);

				}
			}

		}
		return dashBoardList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionAmountAndAlertCountDto> getAlertAggregates(String fromDate, String toDate,
			String granularity, Boolean isGroupByType, String transType, boolean input, boolean output,
			FilterDto filterDto) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<TransactionAmountAndAlertCountDto> alertList = new ArrayList<TransactionAmountAndAlertCountDto>();

		StringBuilder hql = new StringBuilder();
		String builder = new String();
		// hql.append("select new
		// element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto(
		// ");

		hql.append("select DISTINCT ");
		if ((granularity).equalsIgnoreCase("daily"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m-%d')";
		if ((granularity).equalsIgnoreCase("weekly"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%U')";
		if ((granularity).equalsIgnoreCase("monthly"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m')";
		if ((granularity).equalsIgnoreCase("yearly"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y')";
		if ((granularity).equalsIgnoreCase("hours"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%H')";
		if ((granularity).equalsIgnoreCase("minute"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%i')";

		hql.append(builder);
		hql.append(
				",t.alertTransactionId,t.amount from TransactionMasterView t where t.alertTransactionId is not null and ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
		 * append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
		 * ); }
		 */

		if (input)
			hql.append(" and t.benificiaryCustomerId is not null ");
		if (output)
			hql.append(" and t.originatorCustomerId is not null ");

		if (filterDto != null)
			hql.append(filterUtility.filterColumns(filterDto));

		Query<?> query = this.getCurrentSession().createQuery(hql.toString());

		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
		 * query.setParameter("fromDate", formatter.parse(fromDate));
		 * query.setParameter("toDate", formatter.parse(toDate)); } }
		 */
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		if (filterDto != null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);

		List<Object[]> objectArray = (List<Object[]>) query.getResultList();

		// for transactions amount,count,transaction product type

		Map<String, TransactionAmountAndAlertCountDto> mapWithoutDuplicatesTransactions = new HashMap<String, TransactionAmountAndAlertCountDto>();

		for (Object[] objects : objectArray) {

			if (mapWithoutDuplicatesTransactions.containsKey(objects[0])) {
				TransactionAmountAndAlertCountDto dto = mapWithoutDuplicatesTransactions.get(objects[0].toString());
				dto.setAlertCount(dto.getAlertCount() + 1);
				dto.setAlertedAmount(dto.getAlertedAmount() + Double.valueOf(objects[2].toString()));
				dto.setBusinessDate(dto.getBusinessDate());
			} else {
				TransactionAmountAndAlertCountDto dto = new TransactionAmountAndAlertCountDto(new Long(1),
						objects[0].toString(), Double.valueOf(objects[2].toString()));
				mapWithoutDuplicatesTransactions.put(objects[0].toString(), dto);
			}
		}

		for (Entry<String, TransactionAmountAndAlertCountDto> entry : mapWithoutDuplicatesTransactions.entrySet()) {
			alertList.add(entry.getValue());
		}

		return alertList;

		/*
		 * List<TransactionAmountAndAlertCountDto> alertAggregatorDtoList = new
		 * ArrayList<TransactionAmountAndAlertCountDto>(); try { StringBuilder hql = new
		 * StringBuilder(); StringBuilder subQuery = new StringBuilder();
		 * subQuery.append("select t.id from TransactionsData t "); if (fromDate !=
		 * null) subQuery.
		 * append("where  t.businessDate>=:fromDate and  t.businessDate<=:toDate");
		 * 
		 * String builder = new String();
		 * 
		 * hql.append(
		 * "select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto( "
		 * ); if ((granularity).equalsIgnoreCase("daily")) builder =
		 * "DATE_FORMAT(a.alertBusinessDate,'%Y-%m-%d')"; if
		 * ((granularity).equalsIgnoreCase("weekly")) builder =
		 * "DATE_FORMAT(a.alertBusinessDate,'%X-%V')"; if
		 * ((granularity).equalsIgnoreCase("monthly")) builder =
		 * "DATE_FORMAT(a.alertBusinessDate,'%Y-%m')"; if
		 * ((granularity).equalsIgnoreCase("yearly")) builder =
		 * "DATE_FORMAT(a.alertBusinessDate,'%Y')"; if
		 * ((granularity).equalsIgnoreCase("hours")) builder =
		 * "DATE_FORMAT(a.alertBusinessDate,'%H')"; if
		 * ((granularity).equalsIgnoreCase("minute")) builder =
		 * "DATE_FORMAT(a.alertBusinessDate,'%i')";
		 * 
		 * hql.append(builder);
		 * 
		 * hql.append(",sum(t.amount),count(a.id)"); if (isGroupByType)
		 * hql.append(",a.transactionProductType"); hql.append(" )");
		 * 
		 * hql.append("from Alert a ,TransactionsData t "); if (input || output)
		 * hql.append(",CustomerDetails c "); hql.append(" where a.transactionId in(" +
		 * subQuery.toString() + ") and a.transactionId=t.id "); if (transType != null)
		 * hql.append(" and a.transactionProductType=:transactionProductType "); if
		 * (input) hql.append(" and t.beneficiaryCutomerId = c.id"); if (output)
		 * hql.append(" and t.originatorCutomerId = c.id");
		 * 
		 * hql.append(" group by " + builder);
		 * 
		 * Query<?> query = this.getCurrentSession().createQuery(hql.toString()); if
		 * (fromDate != null) { query.setParameter("fromDate", sdf.parse(fromDate));
		 * query.setParameter("toDate", sdf.parse(toDate)); } if (transType != null)
		 * query.setParameter("transactionProductType", transType);
		 * 
		 * alertAggregatorDtoList = (List<TransactionAmountAndAlertCountDto>)
		 * query.getResultList(); } catch (HibernateException e) {
		 * ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e,
		 * AlertDaoImpl.class); throw new FailedToExecuteQueryException();
		 * 
		 * }
		 */

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionAmountAndAlertCountDto> getAlertAggregatesProductType(String fromDate, String toDate,
			String granularity, Boolean isGroupByType, String transType, boolean input, boolean output,
			FilterDto filterDto) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<TransactionAmountAndAlertCountDto> transactionList = new ArrayList<TransactionAmountAndAlertCountDto>();

		StringBuilder hql = new StringBuilder();
		String builder = new String();

		hql.append("select DISTINCT ");
		if ((granularity).equalsIgnoreCase("daily"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m-%d')";
		if ((granularity).equalsIgnoreCase("weekly"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%U')";
		if ((granularity).equalsIgnoreCase("monthly"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m')";
		if ((granularity).equalsIgnoreCase("yearly"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y')";
		if ((granularity).equalsIgnoreCase("hours"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%H')";
		if ((granularity).equalsIgnoreCase("minute"))
			builder = "DATE_FORMAT(t.transactionBusinessDate,'%i')";

		hql.append(builder);
		hql.append(
				",t.alertTransactionId,t.amount,t.alertTransactionType from TransactionMasterView t where t.alertTransactionId is not null and t.alertTransactionType ='"
						+ transType + "'");
		hql.append(" and DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
		 * append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
		 * ); }
		 */

		if (input)
			hql.append(" and t.benificiaryCustomerId is not null ");
		if (output)
			hql.append(" and t.originatorCustomerId is not null ");

		if (filterDto != null)
			hql.append(filterUtility.filterColumns(filterDto));

		Query<?> query = this.getCurrentSession().createQuery(hql.toString());

		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
		 * query.setParameter("fromDate", formatter.parse(fromDate));
		 * query.setParameter("toDate", formatter.parse(toDate)); } } else {
		 * query.setParameter("fromDate", formatter.parse(fromDate));
		 * query.setParameter("toDate", formatter.parse(toDate)); }
		 */
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		if (filterDto != null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);

		List<Object[]> objectArray = (List<Object[]>) query.getResultList();

		Map<String, TransactionAmountAndAlertCountDto> mapWithoutDuplicatesTransactions = new HashMap<String, TransactionAmountAndAlertCountDto>();

		for (Object[] objects : objectArray) {

			if (mapWithoutDuplicatesTransactions.containsKey(objects[0])) {
				TransactionAmountAndAlertCountDto dto = mapWithoutDuplicatesTransactions.get(objects[0].toString());
				dto.setAlertCount(dto.getAlertCount() + 1);
				dto.setTotalTransactionAmount(dto.getTotalTransactionAmount() + Double.valueOf(objects[2].toString()));
				dto.setTransactionProductType(dto.getTransactionProductType());
				dto.setBusinessDate(dto.getBusinessDate());
			} else {
				TransactionAmountAndAlertCountDto dto = new TransactionAmountAndAlertCountDto(new Long(1),
						objects[0].toString(), Double.valueOf(objects[2].toString()), objects[3].toString());
				mapWithoutDuplicatesTransactions.put(objects[0].toString(), dto);
			}
		}

		for (Entry<String, TransactionAmountAndAlertCountDto> entry : mapWithoutDuplicatesTransactions.entrySet()) {
			transactionList.add(entry.getValue());
		}

		return transactionList;

	}

	@Override
	public MonthlyTurnOverDto getMonthlyTurnOver(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		MonthlyTurnOverDto monthlyTurnOverDto = new MonthlyTurnOverDto();
		for (int i = 0; i < 4; i++) {

			StringBuilder hql = new StringBuilder();

			// hql.append("select count(a.id) from Alert a ,CustomerDetails c where
			// a.focalNtityDisplayId = c.customerNumber and ");

			hql.append(
					"select distinct t.alertTransactionId,t.benificiaryEstimatedAmount from TransactionMasterView t where t.alertTransactionId is not null and ");
			hql.append(" DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
			if (i == 0)
				hql.append(" and t.benificiaryEstimatedAmount <1000000");
			if (i == 1)
				hql.append(" and t.benificiaryEstimatedAmount >1000000 and t.benificiaryEstimatedAmount<5000000");
			if (i == 2)
				hql.append("and t.benificiaryEstimatedAmount >5000000 and t.benificiaryEstimatedAmount<10000000");
			if (i == 3)
				hql.append(" and t.benificiaryEstimatedAmount >10000000");

			/*
			 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
			 * (!filterDto.isAlertsByPeriod10to20Days()) &&
			 * (!filterDto.isAlertsByPeriod20to30Days()) &&
			 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
			 * append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
			 * ); }
			 */

			if (filterDto != null)
				hql.append(filterUtility.filterColumns(filterDto));

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());

			/*
			 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
			 * (!filterDto.isAlertsByPeriod10to20Days()) &&
			 * (!filterDto.isAlertsByPeriod20to30Days()) &&
			 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
			 * query.setParameter("fromDate", sdf.parse(fromDate));
			 * query.setParameter("toDate", sdf.parse(toDate)); } }
			 */

			if (filterDto != null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			@SuppressWarnings("unchecked")
			List<Object[]> count = (List<Object[]>) query.getResultList();
			if (i == 0)
				monthlyTurnOverDto.setBelowOneMillionUSD(count.size());
			if (i == 1)
				monthlyTurnOverDto.setBwOneToFiveMillionUSD(count.size());
			if (i == 2)
				monthlyTurnOverDto.setBwFiveToTenMillionUSD(count.size());
			if (i == 3)
				monthlyTurnOverDto.setAboveTenMillionUSD(count.size());
		}

		return monthlyTurnOverDto;

	}

	@Override
	public List<AlertNotiDto> fetchAlertsBwDatesCustomized(String fromDate, String toDate, String productType,
			boolean input, boolean output, FilterDto filterDto) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder hql = new StringBuilder();
		// hql.append("select new
		// element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertNotiDto(");
		hql.append("select distinct t.alertId,t.alertBusinessDate,t.amount ");
		hql.append(" from TransactionMasterView t where t.alertId is not null and ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		// hql.append(" and a.alertBusinessDate >=:fromDate and a.alertBusinessDate
		// <=:toDate");
		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
		 * append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
		 * ); }
		 */
		if (filterDto != null)
			hql.append(filterUtility.filterColumns(filterDto));

		if (input)
			hql.append(" and t.benificiaryCustomerId is not null ");
		if (output)
			hql.append(" and t.originatorCustomerId is not null ");
		if (productType != null)
			hql.append(" and t.tranactionProductType =:transProductType");

		// hql.append(" group By t.alertBusinessDate");

		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		if (productType != null)
			query.setParameter("transProductType", productType);

		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
		 * query.setParameter("fromDate", formatter.parse(fromDate));
		 * query.setParameter("toDate", formatter.parse(toDate)); } }
		 */
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));

		if (filterDto != null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		@SuppressWarnings("unchecked")
		List<Object[]> objArray = (List<Object[]>) query.getResultList();

		List<AlertNotiDto> alertNotiDtos = new ArrayList<AlertNotiDto>();

		Map<String, AlertNotiDto> map = new HashMap<String, AlertNotiDto>();

		for (Object[] object : objArray) {
			if (map.containsKey(object[1].toString())) {
				AlertNotiDto dto = map.get(object[1].toString());
				dto.setAlertAmount(dto.getAlertAmount() + Double.valueOf(object[2].toString()));
			} else {
				AlertNotiDto dto = new AlertNotiDto(formatter.parse(object[1].toString()),
						Double.valueOf(object[2].toString()));
				map.put(object[1].toString(), dto);
			}
		}
		for (Entry<String, AlertNotiDto> entry : map.entrySet()) {
			alertNotiDtos.add(entry.getValue());
		}

		return alertNotiDtos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCustomerRiskByType(String fromDate, String toDate, String type, List<String> scenarios) {

		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");
			hql.append(
					"from Alert a  Join TransactionsData t on t.id=a.transactionId Join CustomerDetails c on t.beneficiaryCutomerId=c.id ");
			hql.append(" and c.industry=:type and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			if (scenarios != null && !scenarios.isEmpty())
				query.setParameter("scenarios", scenarios);

			list = (List<String>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getScenarios(String fromDate, String toDate, String type) {
		List<String> list = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct a.scenario ");

			hql.append(
					"from Alert a Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			hql.append("join Country c on c.iso2Code=cd.residentCountry");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and ");

			hql.append(type + ">0 ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<String>) query.getResultList();
			return list;
		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertComparisonDto> alertComparisonNotification(String fromDate, String toDate, Date previousDate,
			FilterDto filterDto, String originalToDate) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<AlertComparisonDto> transactionList = new ArrayList<AlertComparisonDto>();

		StringBuilder hql = new StringBuilder();
		hql.append("select DISTINCT ");
		hql.append(
				" t.alertTransactionId,t.alertTransactionType from TransactionMasterView t where t.alertTransactionId is not null and ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) hql.
		 * append(" and  t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate "
		 * ); }
		 */

		if (filterDto != null)
			hql.append(filterUtility.filterColumns(filterDto));

		Query<?> query = this.getCurrentSession().createQuery(hql.toString());

		/*
		 * if(filterDto!=null) { if((!filterDto.isAlertsByPeriodgreaterthan30Days()) &&
		 * (!filterDto.isAlertsByPeriod10to20Days()) &&
		 * (!filterDto.isAlertsByPeriod20to30Days()) &&
		 * (!filterDto.isAlertByStatusGreaterThan30Days())) {
		 * query.setParameter("fromDate", formatter.parse(fromDate));
		 * query.setParameter("toDate", formatter.parse(toDate)); } }
		 */
		if (toDate == null) {
			query.setParameter("fromDate", previousDate);
			query.setParameter("toDate", formatter.parse(fromDate));
		} else {
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		}
		if (filterDto != null) {
			if (toDate == null) {
				query = filterUtility.queryReturn(query, filterDto, fromDate, originalToDate);
			} else

			{
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			}
		}

		List<Object[]> objectArray = (List<Object[]>) query.getResultList();

		Map<String, AlertComparisonDto> map = new HashMap<String, AlertComparisonDto>();

		for (Object[] object : objectArray) {
			if (map.containsKey(object[1].toString())) {
				AlertComparisonDto dto = map.get(object[1].toString());
				dto.setCount(dto.getCount() + 1);
				dto.setProductType(dto.getProductType());
			} else {
				AlertComparisonDto dto = new AlertComparisonDto(object[1].toString(), new Long(1));
				map.put(object[1].toString(), dto);
			}
		}
		for (Entry<String, AlertComparisonDto> entry : map.entrySet()) {
			transactionList.add(entry.getValue());
		}

		return transactionList;
		/*
		 * StringBuilder queryBuilder = new StringBuilder(); queryBuilder.
		 * append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto("
		 * ); queryBuilder.append("a.transactionProductType,count(a.id))");
		 * queryBuilder.
		 * append(" from Alert a , TransactionsData t where a.transactionId=t.id and ");
		 * queryBuilder.
		 * append("a.alertBusinessDate >=:fromDate and a.alertBusinessDate <=:toDate");
		 * queryBuilder.append(" Group By a.transactionProductType"); Query<?> query =
		 * this.getCurrentSession().createQuery(queryBuilder.toString());
		 * 
		 * if (toDate == null) { query.setParameter("fromDate", previousDate);
		 * query.setParameter("toDate", sdf.parse(fromDate)); } else {
		 * query.setParameter("fromDate", sdf.parse(fromDate));
		 * query.setParameter("toDate", sdf.parse(toDate)); }
		 * 
		 * @SuppressWarnings("unchecked") List<AlertComparisonDto> alertComparison =
		 * (List<AlertComparisonDto>) query.getResultList(); return alertComparison;
		 */
	}

	@Override
	public RiskCountAndRatioDto getProductRisk(String fromDate, String toDate) {
		RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append("sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id)) ");
			hql.append("from Alert a Right Join TransactionsData t on t.id=a.transactionId  ");
			hql.append(" and t.businessDate>=:fromDate and t.businessDate<=:toDate ");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			riskCountAndRatioDto = (RiskCountAndRatioDto) query.getSingleResult();

			StringBuilder hql1 = new StringBuilder();

			hql1.append("select  sum(t.amount) ");
			hql1.append("from Alert a Join TransactionsData t on t.id=a.transactionId  ");
			hql1.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate ");

			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			Double alertedAmount = (Double) query1.getSingleResult();
			riskCountAndRatioDto.setAlertAmount(alertedAmount);

			return riskCountAndRatioDto;
		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getProductRiskAggregates(String fromDate, String toDate, String type,
			FilterDto filterDto) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		List<Object[]> listObjArray = new ArrayList<Object[]>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			StringBuilder hql = new StringBuilder();
			Map<String,RiskCountAndRatioDto> map=new HashMap<String,RiskCountAndRatioDto>();
			hql.append("select distinct round(t.amount,2),t.alertTransactionId,t.alertTransactionType from TransactionMasterView t");
			hql.append(" where t.alertTransactionType is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			listObjArray = (List<Object[]>) query.getResultList();
			for (Object[] object : listObjArray) {
				if (map.containsKey(object[2].toString())) {
					RiskCountAndRatioDto dto = map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount() + 1);
					dto.setAlertAmount(dto.getAlertAmount() + Double.valueOf(object[0].toString()));
				} else {
					RiskCountAndRatioDto dto = new RiskCountAndRatioDto(object[2].toString(),
							Double.valueOf(object[0].toString()), new Long(1));
					map.put(object[2].toString(), dto);
				}
			}
			for (Entry<String, RiskCountAndRatioDto> entry : map.entrySet()) {
				list.add(entry.getValue());
			}
			list.sort(Comparator.comparingDouble(RiskCountAndRatioDto::getAlertAmount).reversed());

			return list;
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getTopProductRisk(String fromDate, String toDate) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select input.type,input.amt,input.alertcount from ");
			hql.append("(select t.TRANS_PRODUCT_TYPE as type,sum(t.amount) as amt,count(a.id) as alertcount ");
			hql.append("from ALERT a  Join TRANSACTIONS t on t.id=a.TRANSACTION_ID  ");
			hql.append(
					" and a.ALERT_BUSINESS_DATE>=:fromDate and a.ALERT_BUSINESS_DATE<=:toDate group by t.TRANS_PRODUCT_TYPE) as input "
							+ "order by input.amt ");

			Query<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			List<Object[]> objArray = (List<Object[]>) query.getResultList();

			for (int i = objArray.size() - 1; i >= 0; i--) {

				RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();
				riskCountAndRatioDto.setType((objArray.get(i)[0].toString()));
				riskCountAndRatioDto.setAlertAmount(Double.parseDouble(objArray.get(i)[1].toString()));
				riskCountAndRatioDto.setAlertCount(Long.parseLong(objArray.get(i)[2].toString()));
				list.add(riskCountAndRatioDto);

			}

			return list;
		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertDashBoardRespDto> getRiskAlertsByScenarios(String fromDate, String toDate, FilterDto filterDto) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<AlertDashBoardRespDto> list = new ArrayList<AlertDashBoardRespDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try {
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto(");
			queryBuilder.append(" t.benificiaryCustomerId,t.benificiaryCustomerName,t.alertBusinessDate,t.amount) ");
			queryBuilder.append(" from TransactionMasterView t where DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
			queryBuilder.append(" and t.alertTransactionId is not null");
			queryBuilder.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);

			list = (List<AlertDashBoardRespDto>) query.getResultList();
			return list;
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertDashBoardRespDto> getRiskByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<AlertDashBoardRespDto> list = new ArrayList<AlertDashBoardRespDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try {
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto(");
			queryBuilder.append(" cd.id,cd.displayName,a.alertBusinessDate,t.amount) ");
			queryBuilder.append(
					" from Alert a,TransactionsData t,CustomerDetails cd,Country c where a.transactionId=t.id and ");
			queryBuilder.append(
					" cd.customerNumber=a.focalNtityDisplayId and t.businessDate>=:fromDate and t.businessDate<=:toDate ");
			queryBuilder.append(" and c.iso2Code=cd.residentCountry and ");
			queryBuilder.append(type + ">0 ");

			queryBuilder.append("and a.scenario in (:scenarios) ");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("scenarios", scenarios);

			list = (List<AlertDashBoardRespDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	@Override
	public List<AlertScenarioDto> amlTopScenarios(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		List<AlertScenarioDto> alertScenarioDtos = new ArrayList<AlertScenarioDto>();

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder builder = new StringBuilder();

			/*
			 * builder.
			 * append("select scen,ct,am from (select a.SCENARIO scen,sum(t.AMOUNT) am ,count(a.ID) ct "
			 * +
			 * " from ALERT a  join TRANSACTIONS t on t.ID=a.TRANSACTION_ID and t.BUSINESS_DATE>='"
			 * + fromDate + "' and t.BUSINESS_DATE<='" + toDate + "' group by a.SCENARIO)" +
			 * "input order by ct DESC");
			 */
			builder.append(
					"select t.alertScenario,round(SUM(t.amount),2) as  am,count(t.alertTransactionId) from TransactionMasterView t ");
			builder.append(" where t.alertTransactionId is not null ");
			builder.append(" and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
			if (filterDto != null)
				builder.append(filterUtility.filterColumns(filterDto));
			builder.append(" group by t.alertScenario order by am DESC");
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));

			if (filterDto != null)
				builder.append(filterUtility.queryReturn(query, filterDto, fromDate, toDate));

			@SuppressWarnings("unchecked")
			List<Object[]> alertScenarios = (List<Object[]>) query.getResultList();
			for (Object[] objects : alertScenarios) {
				AlertScenarioDto alertScenarioDto = new AlertScenarioDto();
				alertScenarioDto.setScenario(objects[0].toString());
				alertScenarioDto.setCount(Long.valueOf(objects[2].toString()));
				alertScenarioDto.setAmount(Double.valueOf(objects[1].toString()));
				alertScenarioDtos.add(alertScenarioDto);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return alertScenarioDtos;
	}

	@Override
	public AlertByPeriodDto amlAlertByPeriod(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		AlertByPeriodDto alertPeriod = new AlertByPeriodDto();
		long days = daysCalculationUtility.getDifferenceBetweenDates(fromDate, toDate);
		try {
			if (days >= 30) {

				for (int i = 0; i < 3; i++) {
					StringBuilder hql = new StringBuilder();

					hql.append("select count(t.alertId) from TransactionMasterView t where t.alertId is not null and ");
					// hql.append(" t.alertBusinessDate >=:fromDate and t.alertBusinessDate
					// <=:toDate ");
					// hql.append("select COUNT(a.ID) from ALERT a ");
					if (i == 0)
						hql.append(
								" DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate) <DATE_ADD(:toDate,-30,DAY) ");
					if (i == 1)
						hql.append(
								" (DATE(t.alertBusinessDate) >DATE_ADD(:toDate,-20,DAY) and DATE(t.alertBusinessDate) <DATE_ADD(:toDate,-10,DAY))");
					if (i == 2)
						hql.append(
								" (DATE(t.alertBusinessDate) >DATE_ADD(:toDate,-10,DAY) and DATE(t.alertBusinessDate) >DATE_ADD(:toDate,0,DAY))");

					if (filterDto != null)
						hql.append(filterUtility.filterColumns(filterDto));

					Query<?> query = this.getCurrentSession().createQuery(hql.toString());
					if (filterDto != null)
						query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
					if (i == 0)
						query.setParameter("fromDate", formatter.parse(fromDate));
					query.setParameter("toDate", formatter.parse(toDate));

					// query.setParameter("toDate", toDate);
					Long count = (Long) query.getSingleResult();
					if (i == 0)
						alertPeriod.setGreaterThirtyDaysCount(count);
					if (i == 1)
						alertPeriod.setTenToTwentyCount(count);
					if (i == 2)
						alertPeriod.setTwentyToThirtyCount(count);
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return alertPeriod;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CounterPartyNotiDto> getAlertByScenarios(String fromDate, String toDate, String type,
			List<String> scenarios, boolean isCountry) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<CounterPartyNotiDto> list = new ArrayList<CounterPartyNotiDto>();

		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto( ");
			queryBuilder.append(" cd.displayName,cd.id,t.amount )");
			queryBuilder.append("from Alert a,TransactionsData t,CustomerDetails cd,Country c where ");
			queryBuilder.append("a.transactionId=t.id ");
			queryBuilder.append(" and t.beneficiaryCutomerId=cd.id and cd.residentCountry=c.iso2Code");
			queryBuilder.append(
					" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and a.scenario IN (:scenarios)");
			if (isCountry)
				queryBuilder.append(" and c.country=:type");
			else
				queryBuilder.append(" and t.benficiaryBankName=:type");

			// queryBuilder.append(" group by cd.displayName,cd.id");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("type", type);
			query.setParameter("scenarios", scenarios);
			list = (List<CounterPartyNotiDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch customer details. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getTopCounterParties(String fromDate, String toDate) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();

		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select input.countryName,input.amt,input.alertCount from (SELECT c.country as countryName ,sum(t.AMOUNT) amt, "
							+ " count(a.id) alertCount FROM ALERT a,"
							+ "TRANSACTIONS t,CUSTOMER_DETAILS cd,COUNTRY c where a.TRANSACTION_ID=t.id and t.beneficiaryCutomerId=cd.id and "
							+ "cd.RESIDENT_COUNTRY=c.IS02_CODE and a.ALERT_BUSINESS_DATE>=:fromDate and a.ALERT_BUSINESS_DATE<=:toDate "
							+ " group by c.COUNTRY) as input order by input.amt");

			Query<?> query = this.getCurrentSession().createNativeQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			List<Object[]> objArray = (List<Object[]>) query.getResultList();

			for (int i = objArray.size() - 1; i >= 0; i--) {

				RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();
				riskCountAndRatioDto.setType((objArray.get(i)[0].toString()));
				riskCountAndRatioDto.setAlertAmount(Double.parseDouble(objArray.get(i)[1].toString()));
				riskCountAndRatioDto.setAlertCount(Long.parseLong(objArray.get(i)[2].toString()));
				list.add(riskCountAndRatioDto);

			}
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch customer details. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getTopBanks(String fromDate, String toDate) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();

		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select input.bankname,input.amt ,input.alertCount from (SELECT ac.BANK_NAME as bankname ,sum(t.AMOUNT) amt "
							+ ",count(a.id) alertCount FROM ALERT a,"
							+ "CUSTOMER_DETAILS cd,ACCOUNT ac,TRANSACTIONS t where a.TRANSACTION_ID=t.id and "
							+ "t.BENEFICIARY_ACCOUNT_ID=ac.ACCOUNT_NUMBER "
							+ "and ac.CUSTOMER_ID=cd.id and a.ALERT_BUSINESS_DATE>=:fromDate and a.ALERT_BUSINESS_DATE<=:toDate "
							+ " group by ac.BANK_NAME) as input order by input.amt ");

			Query<?> query = this.getCurrentSession().createNativeQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			List<Object[]> objArray = (List<Object[]>) query.getResultList();

			for (int i = objArray.size() - 1; i >= 0; i--) {

				RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();
				riskCountAndRatioDto.setType((objArray.get(i)[0].toString()));
				riskCountAndRatioDto.setAlertAmount(Double.parseDouble(objArray.get(i)[1].toString()));
				riskCountAndRatioDto.setAlertCount(Long.parseLong(objArray.get(i)[2].toString()));
				list.add(riskCountAndRatioDto);

			}
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch customer details. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alert> fetchAlertSummaryList(Long transactionId) {
		List<Alert> alert = new ArrayList<Alert>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from Alert a where a.transactionId=:transactionId");
			Query<?> query = getCurrentSession().createQuery(hql.toString());
			query.setParameter("transactionId", transactionId);
			alert = (List<Alert>) query.getResultList();
		} catch (NoResultException e) {
			return alert;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return alert;
	}

	@Override
	public AlertScenarioDto getAlertScenarioById(Long id, String scenarioType) {
		AlertScenarioDto alertScenarioDto = new AlertScenarioDto();
		StringBuilder queryBuilder = new StringBuilder();
		if (scenarioType != null) {
			try {

				queryBuilder.append(
						"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto( ");
				queryBuilder
						.append(" a.id,a.scenario,t.amount,a.alertScenarioType,a.alertDescription,a.maximumAmount)");
				queryBuilder.append("from Alert a,TransactionsData t where ");
				queryBuilder.append("a.id=:id and a.alertScenarioType=:scenarioType and a.transactionId=t.id ");
				Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
				query.setParameter("id", id);
				query.setParameter("scenarioType", scenarioType);
				// System.out.println(queryBuilder.toString());
				alertScenarioDto = (AlertScenarioDto) query.getSingleResult();

			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				throw new FailedToExecuteQueryException("Failed to fetch alert details. Reason : " + e.getMessage());
			}
			return alertScenarioDto;
		}
		return alertScenarioDto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alert> getAlertsForCustomer(String fromDate, String toDate, String customerNumber) {
		List<Alert> alertList = new ArrayList<Alert>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(" from Alert a where a.focalNtityDisplayId=:customerNumber and a.entityType='TRANSACTION'");
			Query<?> query = getCurrentSession().createQuery(hql.toString());
			query.setParameter("customerNumber", customerNumber);
			alertList = (List<Alert>) query.getResultList();
			return alertList;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get alerts for customer. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getGroupByScenario(String fromDate, String toDate, FilterDto filterDto) {
		List<RiskCountAndRatioDto> list = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto(");
			hql.append(" t.alertScenario,sum(t.amount),count(distinct t.alertTransactionId))");
			hql.append(" from TransactionMasterView t");
			hql.append(" where DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			hql.append(" group by t.alertScenario");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			list = (List<RiskCountAndRatioDto>) query.getResultList();
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get alerts for customer. Reason : " + e.getMessage());
		}
		return list;
	}

	@Override
	public boolean findAlertByAlertTransactionId(Long alertTransactionId) {
		@SuppressWarnings("unchecked")
		List<Alert> alert = this.getCurrentSession()
				.createQuery("from Alert a where a.transactionId=" + alertTransactionId).getResultList();
		if (alert.size() > 0)
			return true;
		return false;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<AlertsDto> searchAlerts(String keyword) {
		List<AlertsDto> alertList=new ArrayList<AlertsDto>();
		try{
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto (")
					.append("a.focalNtityDisplayName,a.focalNtityDisplayId,a.scenario,a.alertBusinessDate,a.userId,")
					.append("a.comment,a.alertType,a.alertStatus,a.transactionProductType,a.transactionId,a.alertDescription,")
					.append("a.alertScenarioType,a.riskScore,a.maximumAmount) from Alert a ");
			if(!StringUtils.isEmpty(keyword)){
				queryBuilder.append("where" + " (a.focalNtityDisplayName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.focalNtityDisplayId like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.scenario like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.transactionProductType like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.alertDescription like '%" + keyword.replace("\"", "") + "%')");
			}
			
			queryBuilder.append(" order by ");
			queryBuilder.append("a.createdOn desc");

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			alertList = (List<AlertsDto>) query.getResultList();
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, AlertDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return alertList;
	}

	@Override
	public Long searchAlertsCount(String keyword) {
		try{
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select count(*) from Alert a ");
			if(!StringUtils.isEmpty(keyword)){
				queryBuilder.append("where" + " (a.focalNtityDisplayName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.focalNtityDisplayId like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.scenario like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.transactionProductType like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or a.alertDescription like '%" + keyword.replace("\"", "") + "%')");
			}
			

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, AlertDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AlertAggregator> alertsAggregates(AlertAggregateVo alertAggregateVo, Long userId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		List<AlertAggregator> alertAggregator = new ArrayList<AlertAggregator>();
		try {
			StringBuilder hql = new StringBuilder();
			String builder = new String();

			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator( ");
			if ((alertAggregateVo.getGranularity()).equalsIgnoreCase("daily"))
				builder = "DATE_FORMAT(a.alertBusinessDate,'%Y-%m-%d')";
			if ((alertAggregateVo.getGranularity()).equalsIgnoreCase("weekly"))
				builder = "DATE_FORMAT(a.alertBusinessDate,'%X-%V')";
			if ((alertAggregateVo.getGranularity()).equalsIgnoreCase("monthly"))
				builder = "DATE_FORMAT(a.alertBusinessDate,'%Y-%m')";
			if ((alertAggregateVo.getGranularity()).equalsIgnoreCase("yearly"))
				builder = "DATE_FORMAT(a.alertBusinessDate,'%Y')";

			hql.append(builder);
			hql.append(" ,count(a.id) ) from Alert a where ");
			hql.append(" a.alertBusinessDate BETWEEN ");
			hql.append("'" + sdf.format(alertAggregateVo.getFromDate()) + "'");
			hql.append(" and '" + sdf.format(alertAggregateVo.getToDate()) + "'");
			
			if(alertAggregateVo.getEntityType()!=null)
				hql.append(" and a.entityType=:entityType");
			hql.append(" group by " + builder);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if ((alertAggregateVo.getEntityType() != null))
				query.setParameter("entityType", EntityType.valueOf(alertAggregateVo.getEntityType()));

			alertAggregator = (List<AlertAggregator>) query.getResultList();

		}catch (Exception e) {

			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, AlertDaoImpl.class);
			throw new FailedToExecuteQueryException();

		
		}
		return alertAggregator;
	}

}