package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Upload file response")
public class UploadFileResponseDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Response message")	
	private String message;
	@ApiModelProperty(value="Flag")	
	private Boolean flag;
	public UploadFileResponseDto() {
		super();		
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getFlag() {
		return flag;
	}
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	

}
