package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.docparser.dao.PossibleAnswersDao;
import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.extention.docparser.service.PossibleAnswersService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;

/**
 * @author suresh
 *
 */
@Service("possibleAnswersService")
public class PossibleAnswersServiceImpl extends GenericServiceImpl<PossibleAnswers, Long> implements PossibleAnswersService {

	@Autowired
	public PossibleAnswersServiceImpl(GenericDao<PossibleAnswers, Long> genericDao) {
		super(genericDao);
	}
	
	@Autowired
	PossibleAnswersDao possibleAnswersDao;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	
	@Override
	@Transactional("transactionManager")
	public boolean deleteAnswerOptions(Long questionId,Long currentUserId) {
		PossibleAnswers possibleAnswers = possibleAnswersDao.find(questionId);
		if(possibleAnswers !=null)
		{
			possibleAnswers.setDeleted(true);
			possibleAnswersDao.saveOrUpdate(possibleAnswers);
			eventPublisher.publishEvent(new LoggingEvent(possibleAnswers.getId(), LoggingEventType.DELETE_POSSIBLE_ANSWER, currentUserId, new Date(), null));
			eventPublisher.publishEvent(new LoggingEvent(possibleAnswers.getDocumentQuestions().getTemplateId(), LoggingEventType.UPDATE_TEMPLATE, currentUserId, new Date(), null));
			return true;
		}
		else
		{
		return false;
		}
	}


	@Override
	@Transactional("transactionManager")
	public List<PossibleAnswers> fetchPossibleAnswers(Long questionId) {
		return possibleAnswersDao.fetchPossibleAnswers(questionId);
	}

	

}
