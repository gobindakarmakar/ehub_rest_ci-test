package element.bst.elementexploration.rest.casemanagement.serviceImpl;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.dao.CaseAnalystMappingDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseAnalystMappingService;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.extention.activiti.service.TransitionService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.LoggerUtil;

@Service("caseAnalystMappingService")
public class CaseAnalystMappingServiceImpl extends GenericServiceImpl<CaseAnalystMapping, Long>
		implements CaseAnalystMappingService {

	public CaseAnalystMappingServiceImpl(GenericDao<CaseAnalystMapping, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private CaseAnalystMappingDao caseAnalystMappingDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	
	@Autowired
	private CaseDao caseDao;
	
	@Autowired
	private CaseService caseService;
	
	@Autowired
	private TransitionService transitionService;
	
	@Autowired
	private TaskService taskService;
		
	@Override
	@Transactional("transactionManager")
	public void updateCaseStatus(Long caseId, Long userId, String statusComment, String notificationDate, int statusFrom, int statusTo) throws ParseException {
		Date notificationDateTemp = null;
		if (notificationDate != null && notificationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(notificationDate)) {
				notificationDateTemp = LoggerUtil.convertToDateddMMyyyy(notificationDate);
			} else {
				throw new DateFormatException();
			}
		}
		CaseAnalystMapping analystMapping = new CaseAnalystMapping();
		CaseAnalystMapping mapping = new CaseAnalystMapping();
		Users user = usersDao.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		Case caseSeed = caseDao.find(caseId);
		if(caseSeed != null){
			if (!caseSeed.getCurrentStatus().equals(new Integer(statusTo))) {	
				caseSeed.setModifiedOn(new Date());
				caseSeed.setModifiedBy(userId);
				analystMapping.setCaseSeed(caseSeed);
				analystMapping.setUser(user);
				analystMapping.setCreatedOn(new Date());
				analystMapping.setCurrentStatus(new Integer(statusTo));
				analystMapping.setAssignedBy(user);
				if(notificationDateTemp != null && statusComment != null){
					analystMapping.setStatusComments(statusComment);
					analystMapping.setNotificationDate(notificationDateTemp);
				}
				mapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(caseId, userId, statusFrom);
				if (mapping != null) {
					mapping.setModifiedOn(new Date());
					caseAnalystMappingDao.saveOrUpdate(mapping);
					caseAnalystMappingDao.createMapping(analystMapping);
					// Assign case to random analyst if case is rejected by current analyst.
					if(StatusEnum.REJECTED.getIndex() == statusTo){
						Users newAnalyst = caseService.getNextAnalyst(caseSeed.getCaseId());
						CaseAnalystMapping rejectedMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(caseId, userId, statusTo);						
						rejectedMapping.setModifiedOn(new Date());
						caseAnalystMappingDao.saveOrUpdate(rejectedMapping);
						CaseAnalystMapping focusedMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(caseId, userId,StatusEnum.FOCUS.getIndex() );
						if(focusedMapping != null){
							focusedMapping.setModifiedOn(new Date());
							caseAnalystMappingDao.saveOrUpdate(focusedMapping);
						}
						if(newAnalyst != null){
							CaseAnalystMapping randomAnalystMapping = new CaseAnalystMapping();
							randomAnalystMapping.setCaseSeed(caseSeed);
							randomAnalystMapping.setUser(newAnalyst);
							randomAnalystMapping.setCreatedOn(new Date());
							randomAnalystMapping.setCurrentStatus(new Integer(StatusEnum.SUBMITTED.getIndex()));
							randomAnalystMapping.setAssignedBy(user);
							caseAnalystMappingDao.createMapping(randomAnalystMapping);
						}
					}
				} else {
					throw new PermissionDeniedException();
				}
			}
		}else{
			throw new CaseSeedNotFoundException();
		}		
	}

	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping caseReassignment(Long caseId, Long currentUserId, Long NewUserId, String statusComment) throws InsufficientDataException {
		CaseAnalystMapping analystMapping = null;
		Users user = usersDao.getAnalystByUserId(NewUserId);
		Users currentUser=usersDao.find(currentUserId);
		Case caseSeed = caseDao.find(caseId);
		if (user == null || caseSeed == null)
			throw new NoDataFoundException("Either user or case is not found.");

		analystMapping = caseAnalystMappingDao.getCaseInStatusList(caseId, currentUserId, 
				Stream.of(StatusEnum.ACCEPTED.getIndex(), StatusEnum.PAUSED.getIndex()).collect(Collectors.toList()));
		if (analystMapping == null)
			throw new PermissionDeniedException("Case cannot be reassigned unless its accepted or paused.");
		CaseAnalystMapping newUseranalystMapping = new CaseAnalystMapping();

		if (StatusEnum.ACCEPTED.getIndex() == caseSeed.getCurrentStatus()
				|| StatusEnum.PAUSED.getIndex() == caseSeed.getCurrentStatus()) {
			newUseranalystMapping.setUser(user);
			newUseranalystMapping.setCaseSeed(caseSeed);
			newUseranalystMapping.setCreatedOn(new Date());
			newUseranalystMapping.setCurrentStatus(StatusEnum.ACCEPTED.getIndex());
			newUseranalystMapping.setAssignedBy(usersDao.find(currentUserId));
			newUseranalystMapping.setAssignedBy(currentUser);
			caseAnalystMappingDao.createMapping(newUseranalystMapping);
			analystMapping.setModifiedOn(new Date());
			analystMapping.setStatusComments(statusComment);
			caseAnalystMappingDao.saveOrUpdate(analystMapping);
			caseAnalystMappingDao.deleteCaseFromFocusById(caseId, currentUserId);
		}else{
			throw new PermissionDeniedException("Case cannot be reassigned unless its accepted or paused.");
		}
		return newUseranalystMapping;
	}

	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping caseForwarding(Long caseId, Long currentUserId, Long NewUserId, String statusComment) throws InsufficientDataException {
		CaseAnalystMapping analystMapping = null;
		Users user = usersDao.getAnalystByUserId(NewUserId);
		Users currentUser=usersDao.find(currentUserId);
		Case caseSeed = caseDao.find(caseId);
		if (user == null || caseSeed == null)
			throw new NoDataFoundException("Either user or case is not found.");

		analystMapping = caseAnalystMappingDao.getCaseInStatusList(caseId, currentUserId, 
				Stream.of(StatusEnum.ACKNOWLEDGE.getIndex(), StatusEnum.SUBMITTED.getIndex()).collect(Collectors.toList()));
		if (analystMapping == null)
			throw new PermissionDeniedException("Case cannot be forwarded unless its acknowledged or submitted.");
		CaseAnalystMapping newUseranalystMapping = new CaseAnalystMapping();

		if (StatusEnum.ACKNOWLEDGE.getIndex() == caseSeed.getCurrentStatus()
				|| StatusEnum.SUBMITTED.getIndex() == caseSeed.getCurrentStatus()) {
		newUseranalystMapping.setCurrentStatus(new Integer(StatusEnum.SUBMITTED.getIndex()));
		newUseranalystMapping.setUser(user);
		newUseranalystMapping.setCaseSeed(caseSeed);
		newUseranalystMapping.setCreatedOn(new Date());
		newUseranalystMapping.setAssignedBy(currentUser);
		caseAnalystMappingDao.createMapping(newUseranalystMapping);
		analystMapping.setModifiedOn(new Date());
		analystMapping.setStatusComments(statusComment);
		caseAnalystMappingDao.saveOrUpdate(analystMapping);
		caseAnalystMappingDao.deleteCaseFromFocusById(caseId, currentUserId);
		}else{
			throw new PermissionDeniedException("Case cannot be forwarded unless its acknowledged or submitted.");
		}
		return newUseranalystMapping;
	}

	@Override
	@Transactional("transactionManager")
	public boolean addStatusFocus(Long seedId, Long userId) {
		CaseAnalystMapping old = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(seedId, userId, StatusEnum.FOCUS.getIndex());
		CaseAnalystMapping caseAnalystMapping = null;
		if(old == null){
			caseAnalystMapping = caseAnalystMappingDao.getCaseInStatusList(seedId, userId, 
					Stream.of(StatusEnum.ACCEPTED.getIndex(), StatusEnum.PAUSED.getIndex()).collect(Collectors.toList()));
			if (caseAnalystMapping != null) {
				CaseAnalystMapping analystMapping = new CaseAnalystMapping();
				analystMapping.setCaseSeed(caseDao.find(seedId));
				Users user = usersDao.find(userId);
				user = usersService.prepareUserFromFirebase(user);
				analystMapping.setUser(user);
				analystMapping.setCreatedOn(new Date());
				analystMapping.setCurrentStatus(new Integer(StatusEnum.FOCUS.getIndex()));
				analystMapping.setAssignedBy(caseAnalystMapping.getAssignedBy());
				caseAnalystMappingDao.create(analystMapping);
				return true;
			}else
				throw new PermissionDeniedException();
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteCaseFromFocusById(Long seedId, Long userId) {
		return caseAnalystMappingDao.deleteCaseFromFocusById(seedId, userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> getAllCasesInFocusById(Long userId, String orderBy, String orderIn, Integer pageNumber,
			Integer recordsPerPage) {
		return caseAnalystMappingDao.getAllCasesInFocusById(userId, orderBy, orderIn,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
	}

	@Override
	@Transactional("transactionManager")
	public long countAllCasesInFocusById(Long userId) {
		return caseAnalystMappingDao.countAllCasesInFocusById(userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDto> fullTextSearchForCasesInFocus(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, String creationDate, String orderBy, String orderIn) throws ParseException {
		Date creationDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertJavaDateToSqlDate(creationDate);
			} else {
				throw new DateFormatException();
			}
		}
		return caseAnalystMappingDao.fullTextSearchForCasesInFocus(userId, caseSearchKeyword,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, creationDateTemp,
				orderBy, orderIn);

	}

	@Override
	@Transactional("transactionManager")
	public long countfullTextSearchForCaseInFocus(Long userId, String caseSearchKeyword, String creationDate)
			throws ParseException {
		Date creationDateTemp = null;
		if (creationDate != null && creationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(creationDate)) {
				creationDateTemp = LoggerUtil.convertJavaDateToSqlDate(creationDate);
			} else {
				throw new DateFormatException();
			}
		}

		return caseAnalystMappingDao.countfullTextSearchForCaseInFocus(userId, caseSearchKeyword,
				creationDateTemp);
	}

	@Override
	@Transactional("transactionManager")
	public CaseCommentNotificationDto getCaseCommentNotificationById(Long userId, Long caseId) {
		return caseAnalystMappingDao.getCaseCommentNotificationById(userId, caseId);
		
	}
	
	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping updateCaseStatusBpm(Long caseId, Long userId, String statusComment, String notificationDate, int statusFrom, int statusTo) throws ParseException {
		Date notificationDateTemp = null;
		if (notificationDate != null && notificationDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(notificationDate)) {
				notificationDateTemp = LoggerUtil.convertToDateddMMyyyy(notificationDate);
			} else {
				throw new DateFormatException();
			}
		}
		CaseAnalystMapping analystMapping = new CaseAnalystMapping();
		CaseAnalystMapping mapping = new CaseAnalystMapping();
		Users user = usersDao.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		Case caseSeed = caseDao.find(caseId);
		if(caseSeed != null){
			if (!caseSeed.getCurrentStatus().equals(new Integer(statusTo))) {	
				caseSeed.setModifiedOn(new Date());
				caseSeed.setModifiedBy(userId);
				analystMapping.setCaseSeed(caseSeed);
				analystMapping.setUser(user);
				analystMapping.setCreatedOn(new Date());
				analystMapping.setCurrentStatus(new Integer(statusTo));
				analystMapping.setAssignedBy(user);
				if(notificationDateTemp != null && statusComment != null){
					analystMapping.setStatusComments(statusComment);
					analystMapping.setNotificationDate(notificationDateTemp);
				}
				mapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(caseId, userId, statusFrom);
				if (mapping != null) {
					mapping.setModifiedOn(new Date());
					caseAnalystMappingDao.saveOrUpdate(mapping);
					caseAnalystMappingDao.createMapping(analystMapping);
					// Assign case to random analyst if case is rejected by current analyst.
					if(StatusEnum.REJECTED.getIndex() == statusTo){
						Users newAnalyst = caseService.getNextAnalyst(caseSeed.getCaseId());
						CaseAnalystMapping rejectedMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(caseId, userId, statusTo);						
						rejectedMapping.setModifiedOn(new Date());
						caseAnalystMappingDao.saveOrUpdate(rejectedMapping);
						CaseAnalystMapping focusedMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(caseId, userId,StatusEnum.FOCUS.getIndex() );
						if(focusedMapping != null){
							focusedMapping.setModifiedOn(new Date());
							caseAnalystMappingDao.saveOrUpdate(focusedMapping);
						}
						if(newAnalyst != null){
							List<String> transitions=transitionService.getTransitionsBytaskKey("rejected",caseId);
							CaseAnalystMapping randomAnalystMapping = new CaseAnalystMapping();
							randomAnalystMapping.setCaseSeed(caseSeed);
							randomAnalystMapping.setUser(newAnalyst);
							randomAnalystMapping.setCreatedOn(new Date());
							randomAnalystMapping.setCurrentStatus(new Integer(StatusEnum.valueOf(transitions.get(0)).getIndex()));
							randomAnalystMapping.setAssignedBy(user);
							caseAnalystMappingDao.createMapping(randomAnalystMapping);
							analystMapping=randomAnalystMapping;
						}
					}
				} else {
					throw new PermissionDeniedException();
				}
			}
		}else{
			throw new CaseSeedNotFoundException();
		}
		return analystMapping;		
	}
	
	@Override
	@Transactional("transactionManager")
	public void createMapping(CaseAnalystMapping analystMapping){
		caseAnalystMappingDao.createMapping(analystMapping);
	}

	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping getAnalystMappingBasedOnUserId(Long seedId, Long userId, int index) {
		// TODO Auto-generated method stub
		return caseAnalystMappingDao.getAnalystMappingBasedOnUserId(seedId, userId, index);
	}
	
	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping caseForwardingBpm(Long caseId, Long currentUserId, Long NewUserId, String statusComment) throws InsufficientDataException {
		CaseAnalystMapping analystMapping = null;
		Users user = usersDao.getAnalystByUserId(NewUserId);
		Case caseSeed = caseDao.find(caseId);
		if (user == null || caseSeed == null)
			throw new NoDataFoundException("Either user or case is not found.");

		analystMapping = caseAnalystMappingDao.findByCaseId(caseId, currentUserId);
		if (analystMapping == null)
			throw new PermissionDeniedException("Case cannot be forwarded unless its acknowledged or submitted.");
		CaseAnalystMapping newUseranalystMapping = new CaseAnalystMapping();

		try{
		List<String> list=transitionService.getTransitionsBytaskKey("forward", caseId);
		newUseranalystMapping.setCurrentStatus(new Integer(StatusEnum.valueOf(list.get(0)).getIndex()));
		newUseranalystMapping.setUser(user);
		newUseranalystMapping.setCaseSeed(caseSeed);
		newUseranalystMapping.setCreatedOn(new Date());
		newUseranalystMapping.setAssignedBy(usersDao.find(currentUserId));
		caseAnalystMappingDao.createMapping(newUseranalystMapping);
		analystMapping.setModifiedOn(new Date());
		analystMapping.setStatusComments(statusComment);
		caseAnalystMappingDao.saveOrUpdate(analystMapping);
		caseAnalystMappingDao.deleteCaseFromFocusById(caseId, currentUserId);
		Task task = taskService.createTaskQuery().taskName("UNFOCUS").taskDescription(caseId.toString()).singleResult();
		if(task!=null)
			taskService.complete(task.getId());
		}catch (Exception e) {
			throw new PermissionDeniedException("Case cannot be forwarded unless its acknowledged or submitted.");
		}
		return newUseranalystMapping;
	}
	
	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping caseReassignmentBpm(Long caseId, Long currentUserId, Long NewUserId, String statusComment) throws InsufficientDataException {
		CaseAnalystMapping analystMapping = null;
		Users user = usersDao.getAnalystByUserId(NewUserId);
		Case caseSeed = caseDao.find(caseId);
		if (user == null || caseSeed == null)
			throw new NoDataFoundException("Either user or case is not found.");

		analystMapping = caseAnalystMappingDao.findByCaseId(caseId, currentUserId);
		if (analystMapping == null)
			throw new PermissionDeniedException("Case cannot be reassigned unless its accepted or paused.");
		CaseAnalystMapping newUseranalystMapping = new CaseAnalystMapping();
		List<String> list=transitionService.getTransitionsBytaskKey("reassign", caseId);

		try{
			newUseranalystMapping.setUser(user);
			newUseranalystMapping.setCaseSeed(caseSeed);
			newUseranalystMapping.setCreatedOn(new Date());
			newUseranalystMapping.setCurrentStatus(new Integer(StatusEnum.valueOf(list.get(0)).getIndex()));
			newUseranalystMapping.setAssignedBy(usersDao.find(currentUserId));
			caseAnalystMappingDao.createMapping(newUseranalystMapping);
			analystMapping.setModifiedOn(new Date());
			analystMapping.setStatusComments(statusComment);
			caseAnalystMappingDao.saveOrUpdate(analystMapping);
			caseAnalystMappingDao.deleteCaseFromFocusById(caseId, currentUserId);
			Task task = taskService.createTaskQuery().taskName("UNFOCUS").taskDescription(caseId.toString()).singleResult();
			if(task!=null)
				taskService.complete(task.getId());
		}catch (Exception e) {
			throw new PermissionDeniedException("Case cannot be reassigned unless its accepted or paused.");
		}
		return newUseranalystMapping;
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean addStatusFocusBpm(Long seedId, Long userId) {
		CaseAnalystMapping old = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(seedId, userId, StatusEnum.FOCUS.getIndex());
		if(old!=null){
			Task task = taskService.createTaskQuery().taskName("UNFOCUS").taskDescription(seedId.toString()).singleResult();
			if(task!=null)
				taskService.complete(task.getId());
		}
			
		CaseAnalystMapping caseAnalystMapping = null;
		if(old == null){
			caseAnalystMapping = caseAnalystMappingDao.findByCaseId(seedId, userId);
			if (caseAnalystMapping != null) {
				CaseAnalystMapping analystMapping = new CaseAnalystMapping();
				analystMapping.setCaseSeed(caseDao.find(seedId));
				Users user = usersDao.find(userId);
				user = usersService.prepareUserFromFirebase(user);
				analystMapping.setUser(user);
				analystMapping.setCreatedOn(new Date());
				analystMapping.setCurrentStatus(new Integer(StatusEnum.FOCUS.getIndex()));
				analystMapping.setAssignedBy(caseAnalystMapping.getAssignedBy());
				caseAnalystMappingDao.create(analystMapping);
				return true;
			}else
				throw new PermissionDeniedException();
		}
		return false;
	}
	
	@Override
	@Transactional("transactionManager")
	public void startTask(Long caseId, Long userIdTemp, int statusFrom, int statusTo, String statusComment,
			String notificationDate, Long newUserId, String reassign, String forward) {
		Task taskToPerform = taskService.createTaskQuery().taskAssignee(userIdTemp.toString())
				.taskDescription(caseId.toString())
				.taskDefinitionKey(StatusEnum.getStatusEnumByIndex(statusFrom).getName()).singleResult();
		//Task taskToPerform = null;
		/*for (Task task : taskList) {
			if (caseId.toString().equals(task.getDescription()) && !task.getTaskDefinitionKey().equals("unFocus"))
				taskToPerform = task;
		}*/
		Map<String, String> variables = new HashMap<String, String>();
		variables.put("statusFrom", Integer.toString(statusFrom));
		variables.put("statusTo", Integer.toString(statusTo));
		variables.put("caseId", caseId.toString());
		variables.put("userId", userIdTemp.toString());
		variables.put("statusComment", statusComment);
		variables.put("notificationDate", notificationDate);
		variables.put("newUserId", newUserId == null ? null : newUserId.toString());
		variables.put("reassign", reassign);
		variables.put("forward", forward);

		taskService.setVariables(taskToPerform.getId(), variables);
		/*ProcessDefinition pd = repositoryService.createProcessDefinitionQuery()
				.processDefinitionKey("caseManagementWorkFlow").latestVersion().singleResult();*/

		// code for updating process definition key
		
		/*if (!pd.getId().equals(taskToPerform.getProcessDefinitionId())) {
			SetProcessDefinitionVersionCmd command = new SetProcessDefinitionVersionCmd(
					taskToPerform.getProcessInstanceId(), pd.getVersion());
			((ProcessEngineImpl) ProcessEngines.getDefaultProcessEngine()).getProcessEngineConfiguration()
					.getCommandExecutor().execute(command);
		}*/
		
		//ended

		// outcomes code

		/*BpmnModel bpm = repositoryService.getBpmnModel(pd.getId());
		org.activiti.bpmn.model.Process process = bpm.getProcessById(pd.getKey());
		List<UserTask> listOfUserTaks = process.findFlowElementsOfType(UserTask.class);
		Map<String, FlowElement> map = process.getFlowElementMap();

		map.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v.getName()));

		UserTask userTask = (UserTask) process.getFlowElement(taskToPerform.getTaskDefinitionKey());

		List<SequenceFlow> outgoingFlows = userTask.getOutgoingFlows();
		for (SequenceFlow sequenceFlow : outgoingFlows) {
			if (sequenceFlow.getTargetFlowElement() instanceof ExclusiveGateway) {
				ExclusiveGateway gateway = (ExclusiveGateway) sequenceFlow.getTargetFlowElement();

				for (SequenceFlow node : gateway.getOutgoingFlows()) {

					if (node.getTargetFlowElement() instanceof UserTask)
						System.out.println(node.getTargetRef());

				}
			} else if (sequenceFlow.getTargetFlowElement() instanceof ParallelGateway) {
				ParallelGateway gateway2 = (ParallelGateway) sequenceFlow.getTargetFlowElement();
				for (SequenceFlow node : gateway2.getOutgoingFlows()) {
					System.out.println(node.getTargetRef());
				}
			} else if (sequenceFlow.getTargetFlowElement() instanceof UserTask) {
				System.out.println(sequenceFlow.getTargetFlowElement().getName());

			}
		}*/
		
		/*BpmnModel bpm = repositoryService.getBpmnModel(pd.getId());
		org.activiti.bpmn.model.Process process = bpm.getProcessById(pd.getKey());
		FlowElement flowElement=null;
		if(statusTo==7)
			 flowElement = process.getFlowElement("reAssignOrFocused");

		else
			 flowElement = process.getFlowElement(StatusEnum.getStatusEnumByIndex(statusTo).getName());
		List<FlowElement> listOfTransitions = new ArrayList<>();
		if (flowElement instanceof UserTask) {
			UserTask userTask = (UserTask) flowElement;
			List<SequenceFlow> outgoingFlows = userTask.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				List<FlowElement> element = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);
				System.out.println(element.size());
			}
		}else if(flowElement instanceof ServiceTask){
			ServiceTask serviceTask = (ServiceTask) flowElement;
			List<SequenceFlow> outgoingFlows = serviceTask.getOutgoingFlows();
			for (SequenceFlow sequenceFlow : outgoingFlows) {
				List<FlowElement> element = find(sequenceFlow.getTargetFlowElement(), listOfTransitions);
				System.out.println(element.size());
			}
		
		}
		
		for (FlowElement flowElement2 : listOfTransitions) {
			System.out.println(flowElement2.getName());
		}*/
		// outcomes code ended
		if(newUserId!=null && caseId!=null){
			Users user = usersDao.getAnalystByUserId(newUserId);
			Case caseSeed = caseDao.find(caseId);
			if (user == null || caseSeed == null)
				throw new NoDataFoundException("Either user or case is not found.");
		}
		try {
			taskService.complete(taskToPerform.getId());

		} catch (Exception e) {
			throw new FailedToExecuteQueryException("Out going flow not found");
		}
	}

	@Override
	@Transactional("transactionManager")
	public CaseAnalystMapping findByCaseId(Long caseId) {
		return caseAnalystMappingDao.findByCaseId(caseId);
	}
}
