package element.bst.elementexploration.rest.extention.alert.feedmanagement.serviceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.app.service.exception.NotFoundException;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedGroupsManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedGroupsDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedGroupSevice;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedManagementService;
import element.bst.elementexploration.rest.extention.alertmanagement.dao.AlertManagementDao;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.FeedUsersDto;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.extention.alertmanagement.serviceImpl.AlertManagementServiceImpl;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.dao.GenericFilterDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.EntityConverter;
import element.bst.elementexploration.rest.util.RequestFilterDto;

/**
 * @author hanuman
 *
 */
@Service("feedManagementServiceImpl")
@Transactional("transactionManager")
public class FeedManagementServiceImpl extends GenericServiceImpl<FeedManagement, Long>
		implements FeedManagementService {

	@Autowired
	public FeedManagementServiceImpl(GenericDao<FeedManagement, Long> genericDao) {
		super(genericDao);
	}

	public FeedManagementServiceImpl() {

	}

	@Autowired
	FeedManagementService feedManagementService;

	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	FeedManagementDao feedManagementDao;
	
	@Autowired
	ListItemService listItemService; 

	@Autowired
	FeedGroupsManagementDao feedGroupsManagementDao;
	
	@Autowired
	FeedGroupSevice feedGroupsService;
	
	@Autowired
	UsersDao userDao;
	
	@Autowired
	AlertManagementDao alertManagementDao;
	
	@Autowired
	AlertManagementService alertService;

	@Autowired
	private AuditLogDao auditLogDao;
	
	//@Autowired
	//private GroupsService groupsService;
	
	@Autowired
	private UsersService usersService;
		
	//private GenericFilterDao genericFilterDao;

	@Override
	@Transactional("transactionManager")
	public List<FeedManagementDto> getAllFeedItems(RequestFilterDto filterDto, Boolean isAllRequired, Integer pageNumber, Integer recordsPerPage, String orderIn, String orderBy) {
		List<FeedManagement> feedManagementList = null;
		//FeedManagement feedClass = new FeedManagement();
		if(isAllRequired){
			feedManagementList = feedManagementService.findAll();
		}else{
			feedManagementList = feedManagementDao.getFeedsList(isAllRequired, pageNumber, recordsPerPage, orderIn, orderBy);
			// Apply the generic filter instead of using traditional method
			//feedManagementList = feedManagementDao.filterData(feedClass, filterDto, pageNumber, recordsPerPage, orderIn, orderBy);
		}
		List<FeedManagementDto> feedManagementDtosList = new ArrayList<FeedManagementDto>();
		
		/*
		 * for(FeedManagementDto singleDTO: feedManagementDtosList) {
		 * singleDTO.setGroupLevels(feedManagementDao.getGroups); }
		 */
			if (feedManagementList != null) {
				feedManagementList.stream().forEach(f -> Hibernate.initialize(f.getGroupLevels()));
				feedManagementList.stream().forEach(f -> f.getGroupLevels().stream().forEach(d -> Hibernate.initialize(d.getGroupId())));
				feedManagementDtosList = feedManagementList.stream()
						.map((feedManagementDto) -> new EntityConverter<FeedManagement, FeedManagementDto>(
								FeedManagement.class, FeedManagementDto.class).toT2(feedManagementDto,
										new FeedManagementDto()))
						.collect(Collectors.toList());
				
			}
			
			/*Map<Long,Integer> feedCounts=feedManagementDao.getCountForFeeds();
					for (FeedManagementDto singleDTO : feedManagementDtosList) {
						if (feedCounts.containsKey(singleDTO.getFeed_management_id()))
							singleDTO.setAssignedAlertsCount(feedCounts.get(singleDTO.getFeed_management_id()));
					}*/
			
		return feedManagementDtosList;
	}

	@Override
	@Transactional("transactionManager")
	public FeedManagementDto getFeedItemByID(Long feedManagementId) {
		FeedManagement feedManagement = feedManagementDao.find(feedManagementId);
		Hibernate.initialize(feedManagement.getGroupLevels());
		FeedManagementDto feedManagementDto = null;
		if (feedManagement != null) {
			feedManagementDto = new FeedManagementDto();
			BeanUtils.copyProperties(feedManagement, feedManagementDto);
		}
		return feedManagementDto;
	}

	@Override
	@Transactional("transactionManager")
	public FeedManagementDto getFeedManagementByFeedName(String feedName) {
		FeedManagement feedManagement = feedManagementDao.getFeedManagementByFeedName(feedName);
		FeedManagementDto feedManagementDto = null;
		if (feedManagement != null) {
			feedManagementDto = new FeedManagementDto();
			BeanUtils.copyProperties(feedManagement, feedManagementDto);
		}
		return feedManagementDto;
	}

	@Override
	@Transactional("transactionManager")
	public FeedManagementDto saveOrUpdateFeedItem(FeedManagementDto feedManagmentDto, long userIdTemp)
			throws Exception {

		String feedOperation = "";
		FeedManagement feedManagementCreated=new FeedManagement();
		Users author=userDao.find(userIdTemp);
		StringBuilder auditingString=new StringBuilder("");
		FeedManagement feedManagementCreating = new FeedManagement();
//		FeedManagementDto feedManagementDto = new FeedManagementDto();
		//FeedManagement feedManagement = feedManagementDao.getFeedManagementByFeedName(feedManagmentDto.getFeedName());
		FeedManagement feedManagement = null;
		if (feedManagmentDto != null && feedManagmentDto.getFeed_management_id() != null ) {
			feedManagement = feedManagementDao.find(feedManagmentDto.getFeed_management_id());
			if(feedManagement == null){
				if(null !=  feedManagmentDto.getColor())
					feedManagementCreating.setColor(feedManagmentDto.getColor());
				if(null != feedManagmentDto.getFeedName())
					feedManagementCreating.setFeedName(feedManagmentDto.getFeedName());
				if(null != feedManagmentDto.getSource())
					feedManagementCreating.setSource(feedManagmentDto.getSource());
				if(null != feedManagmentDto.getType())
					feedManagementCreating.setType(feedManagmentDto.getType());
				if(null != feedManagmentDto.getReviewer())
					feedManagementCreating.setReviewer(feedManagmentDto.getReviewer());
				if(null != feedManagmentDto.getIsReviewerRequired())
					feedManagementCreating.setIsReviewerRequired(feedManagmentDto.getIsReviewerRequired());

				feedManagement = new FeedManagement();
				BeanUtils.copyProperties(feedManagmentDto, feedManagement);
				 feedManagementCreated = feedManagementDao.create(feedManagementCreating);
				List<FeedGroups> feedGroupsListDTO = feedManagmentDto.getGroupLevels();
				if(null != feedManagmentDto.getGroupLevels()){
					for (FeedGroups dto : feedGroupsListDTO) {
						dto.setFeedId(feedManagementCreated);
						feedGroupsService.saveOrUpdate(dto);
					}
				}
				feedManagmentDto.setFeed_management_id(feedManagementCreated.getFeed_management_id());
				feedOperation = "CREATED Feed named "+ feedManagmentDto.getFeedName();
			}else{
				List<FeedGroups> dbgroups = new ArrayList<>();
				List<FeedGroups> feedGroupsListDTO = feedManagmentDto.getGroupLevels();
				// feedManagement= new FeedManagement();

				if (null != feedManagmentDto.getFeedName()) {
					if(null != feedManagement.getFeedName()){
						if(!feedManagement.getFeedName().equalsIgnoreCase(feedManagmentDto.getFeedName())){
							auditingString
							.append(" changed Feed Name from " + feedManagement.getFeedName() + " to " + feedManagmentDto.getFeedName()+ ", ");
						}
					}else{
						auditingString
						.append(" set Feed Name to " + feedManagmentDto.getFeedName()+ ", ");
					}
				}
				if (null != feedManagmentDto.getColor()) {
					if (null != feedManagement.getColor()) {
						if(!feedManagement.getColor().equals(feedManagmentDto.getColor()))
							auditingString.append(" changed feed color from " + feedManagement.getColor() + " to "
									+ feedManagmentDto.getColor() + ", ");
					}else {
						auditingString.append(" set feed color to " + feedManagmentDto.getColor() + ", ");
					}

				}
				if (null !=feedManagmentDto.getSource()) {
					if(null !=feedManagement.getSource()){
						if(!(feedManagement.getSource().compareTo(feedManagmentDto.getSource()) == 0)){
							try {
								String sourceNameEarlier = listItemService.getListItemByID(feedManagement.getSource().longValue())
										.getDisplayName();
								String sourceNameAfter = listItemService.getListItemByID(feedManagmentDto.getSource().longValue())
										.getDisplayName();
								auditingString.append(" changed feed source from " + sourceNameEarlier + " to " + sourceNameAfter+ ", ");
								}
								catch(Exception e) {
									e.printStackTrace();
								}
						}
					}else{
						//List<ListItemDto> listitem = listItemService.getListItemByListType("Feed Source");
						try {
						String sourceNameAfter = listItemService.getListItemByID(feedManagmentDto.getSource().longValue())
								.getDisplayName();
						auditingString.append(" set feed source to " + sourceNameAfter+ ", ");
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (null !=  feedManagmentDto.getType()) {
					if(null !=  feedManagement.getType()){
						if(!(feedManagement.getType().compareTo(feedManagmentDto.getType()) == 0)){
							try {
								String sourceNameEarlier = "";
								String sourceNameAfter ="";
								ListItemDto sourceNameErdto = listItemService.getListItemByID(feedManagement.getType().longValue());
								ListItemDto sourceNameAfterDto = listItemService.getListItemByID(feedManagmentDto.getType().longValue());
								if(null != sourceNameErdto && null != sourceNameErdto.getDisplayName()
										&& null != sourceNameAfterDto && null != sourceNameAfterDto.getDisplayName()){
									sourceNameEarlier = sourceNameErdto.getDisplayName();
									sourceNameAfter = sourceNameAfterDto.getDisplayName();
									auditingString.append(" changed feed type from " + sourceNameEarlier + " to " + sourceNameAfter+ ", ");
								}
								}
								catch(Exception e) {
									e.printStackTrace();
								}
						}
					}else{
//						List<ListItemDto>listitem=listItemService.getListItemByListType("Feed Classification");
						try {
						String sourceNameAfter = listItemService.getListItemByID(feedManagmentDto.getType().longValue())
								.getDisplayName();
						auditingString.append(" set feed type to " + sourceNameAfter+ ", ");
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
				
				}
				 
				if(null != feedManagmentDto.getColor())
					feedManagement.setColor(feedManagmentDto.getColor());
				if(null != feedManagmentDto.getSource())
					feedManagement.setSource(feedManagmentDto.getSource());
				if(null != feedManagmentDto.getType())
					feedManagement.setType(feedManagmentDto.getType());
				if(null != feedManagmentDto.getFeedName())
					feedManagement.setFeedName(feedManagmentDto.getFeedName());
				if(null != feedManagmentDto.getReviewer())
					feedManagement.setReviewer(feedManagmentDto.getReviewer());
				if(null != feedManagmentDto.getIsReviewerRequired())
					feedManagement.setIsReviewerRequired(feedManagmentDto.getIsReviewerRequired());
				// BeanUtils.copyProperties(feedManagmentDto, feedManagement);
				
				if(null != feedManagmentDto.getGroupLevels()){
					dbgroups = feedGroupsService.getFeedGroupsByFeedId(feedManagmentDto.getFeed_management_id());
					StringBuilder dbGroupsString = new StringBuilder("");
					StringBuilder dtoGroupsString = new StringBuilder("");
					dbGroupsString = feedGroupsService.deleteFeedgroupList(dbgroups,dbGroupsString);
					/*for (FeedGroups del : dbgroups) {
						dbGroupsString.append(del.getGroupId().getName() + ", ");
						feedGroupsService.delete(del);
						
					}*/
					//moved the below code to controller due to same session object reference
					/*for (FeedGroups dto : feedGroupsListDTO) {
						FeedGroups newdto = new FeedGroups();
						newdto.setFeedId(feedManagement);
						Groups newGroup = new Groups();
						Groups existingGroup = groupsService.find(dto.getGroupId().getId());
						if(null !=  existingGroup){
							BeanUtils.copyProperties(existingGroup, newGroup);
							newdto.setGroupId(newGroup);
							dtoGroupsString.append(newGroup.getName()+ ", ");
							feedGroupsService.save(newdto);
						}
					}*/
					feedManagementDao.saveOrUpdate(feedManagement);
					//audit string
					String db = dbGroupsString.toString().trim();
					String dbString= "";
					if(db.endsWith(",")){
						dbString = db.substring(0, db.lastIndexOf(","));
					}else{
						dbString = db;
					}
					//audit string
					String dto = dtoGroupsString.toString().trim();
					String dtoString= "";
					if(dto.endsWith(",")){
						dtoString = dto.substring(0, dto.lastIndexOf(","));
					}else{
						dtoString = dto;
					}
					if(!dbgroups.isEmpty()){
						if(!dbString.equalsIgnoreCase(dtoString))
							auditingString.append(" changed group level list from " + dbString + " to " + dtoString+ ", ");
					}else{
						auditingString.append(" set group level list to " + dtoString+ ", ");
					}
				}
				//moved the below code to controller due to same session object reference
				/*List<Alerts> alerts = alertService.getAlertsByfeed(feedManagement.getFeed_management_id());
				alerts.stream().forEach(alert -> Hibernate.initialize(alert.getFeed().getReviewer()));
				if(null != feedGroupsListDTO && feedGroupsListDTO.size()>0){
					FeedGroups maxRankGroup =  Collections.min(feedGroupsListDTO, Comparator.comparing(s -> s.getRank()));
					List<Groups> groupsList = feedGroupsListDTO.stream().map(e->e.getGroupId()).collect(Collectors.toList());
					List<Long> groupIdList = groupsList.stream().map(m -> m.getId()).collect(Collectors.toList());
					for(Alerts alert : alerts) {
						Long groupLevel = null;
						if(null != alert.getGroupLevel()){
							 groupLevel = Long.valueOf(alert.getGroupLevel());
							 if(null != groupLevel && !groupIdList.contains(groupLevel)) {
									alert.setGroupLevel(Integer.valueOf(maxRankGroup.getGroupId().getId().intValue()));
									alert.setReviewer(maxRankGroup.getGroupId().getId());
									alertService.saveOrUpdate(alert);
								}
						}
					}
				}else{
					for(Alerts alert : alerts) {
							alert.setGroupLevel(null);
							alertService.saveOrUpdate(alert);
					}
				}*/
				feedOperation = "UPDATED";
			}
			
		} else {
			if(null !=  feedManagmentDto.getColor())
				feedManagementCreating.setColor(feedManagmentDto.getColor());
			if(null != feedManagmentDto.getFeedName())
				feedManagementCreating.setFeedName(feedManagmentDto.getFeedName());
			if(null != feedManagmentDto.getSource())
				feedManagementCreating.setSource(feedManagmentDto.getSource());
			if(null != feedManagmentDto.getType())
				feedManagementCreating.setType(feedManagmentDto.getType());
			if(null != feedManagmentDto.getReviewer())
				feedManagementCreating.setReviewer(feedManagmentDto.getReviewer());
			if(null != feedManagmentDto.getIsReviewerRequired())
				feedManagementCreating.setIsReviewerRequired(feedManagmentDto.getIsReviewerRequired());

			feedManagement = new FeedManagement();
			BeanUtils.copyProperties(feedManagmentDto, feedManagement);
			 feedManagementCreated = feedManagementDao.create(feedManagementCreating);
			List<FeedGroups> feedGroupsListDTO = feedManagmentDto.getGroupLevels();
			if(null != feedManagmentDto.getGroupLevels()){
				for (FeedGroups dto : feedGroupsListDTO) {
					dto.setFeedId(feedManagementCreated);
					feedGroupsService.saveOrUpdate(dto);
				}
			}
			feedManagmentDto.setFeed_management_id(feedManagementCreated.getFeed_management_id());
			feedManagmentDto.setReviewer(feedManagementCreated.getReviewer());
			feedManagmentDto.setIsReviewerRequired(feedManagementCreated.getIsReviewerRequired());
			feedOperation = "CREATED Feed named "+ feedManagmentDto.getFeedName();
		} /*else {

			throw new Exception("feed name already exist!");
		}*/
	//	BeanUtils.copyProperties(feedManagement, feedManagmentDto);
		String finalString = "";
		if(!auditingString.toString().isEmpty()){
			String auditDesc = auditingString.toString().trim();
			if(auditDesc.endsWith(",")){
				finalString = auditDesc.substring(0, auditDesc.lastIndexOf(","));
			}
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Feed Management");
			Users user = userDao.find(userIdTemp);
			if (feedOperation.equals("UPDATED")) {
				log.setDescription(user.getFirstName() + " " + user.getLastName() + " " + finalString.toString() );
				log.setTypeId(feedManagmentDto.getFeed_management_id());
				log.setType("Feed Management");
			} else {
				log.setDescription(user.getFirstName() + " " + user.getLastName() + " " + feedOperation.toString() + " Feed ");
				log.setTypeId(feedManagementCreated.getFeed_management_id());
				log.setType("Feed Management");
			}
			log.setName(feedManagmentDto.getFeedName());
			log.setUserId(userIdTemp);
			//log.setType("Feed Management");
			//log.setTypeId(feedManagmentDto.getFeed_management_id());
		
			auditLogDao.create(log);
		}
		
		return feedManagmentDto;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteFeedItem(Long feedID, long userId) throws NotFoundException {
		
		boolean deleteStatus = false;		
		List<Long> feedIdList = new ArrayList<>();
		feedIdList.add(feedID);
		List<Alerts> associatedAlerts = null;
		FeedManagement feedManagement = null;
		StringBuilder auditingString=new StringBuilder("");
		List<Long> alertIdList = alertService.getAlertIdsByFeedIds(feedIdList);
		if(null != alertIdList && !alertIdList.isEmpty()){
			associatedAlerts = alertService.getAlertsByAlertIdList(alertIdList);				
			feedManagement = find(feedID);
			
		    if( null != feedManagement && (associatedAlerts.isEmpty() || null == associatedAlerts)){

		    	            delete(feedManagement);
							auditingString.append(" deleted ");
							Users user = userDao.find(userId);
							AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Feed management");
							log.setDescription(user.getFirstName() + " " + user.getLastName() + " " + auditingString.toString() );
							log.setName(feedManagement.getFeedName());
							log.setUserId(userId);
							log.setType("Feed Management");
							log.setTypeId(feedManagement.getFeed_management_id());
							auditLogDao.create(log);
							deleteStatus = true;
			}				
		} else {
			
				feedManagement = find(feedID);				
			    if( null != feedManagement){
	
			    	            delete(feedManagement);
								auditingString.append(" deleted ");
								Users user = userDao.find(userId);
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Feed management");
								log.setDescription(user.getFirstName() + " " + user.getLastName() + " " + auditingString.toString() );
								log.setName(feedManagement.getFeedName());
								log.setUserId(userId);
								log.setType("Feed Management");
								log.setTypeId(feedManagement.getFeed_management_id());
								auditLogDao.create(log);
								deleteStatus =  true;
			    }		   
	}
		return deleteStatus;	
}			     

	@Override
	@Transactional("transactionManager")
	public List<FeedGroupsDto> getGroupLevelByFeedId(Long feedManagementID) {
		List<FeedGroups> listGroupLevels = feedManagementDao.getgetGroupLevelByFeedId(feedManagementID);
		List<FeedGroupsDto> listGroupLevelsDto = new ArrayList<FeedGroupsDto>();
		if (listGroupLevels != null) {
			listGroupLevelsDto = listGroupLevels.stream()
					.map((GroupLevelsDto) -> new EntityConverter<FeedGroups, FeedGroupsDto>(FeedGroups.class,
							FeedGroupsDto.class).toT2(GroupLevelsDto, new FeedGroupsDto()))
					.collect(Collectors.toList());
		}
		return listGroupLevelsDto;
	}

	@Override
	@Transactional("transactionManager")
	public Long getFeedsCount() {
		return feedManagementDao.getFeedsCount();
	}

	@Override
	@Transactional("transactionManager")
	public List<FeedUsersDto> getUsersByFeed(Long feedManagementID, boolean isAllRequired) {
		List<FeedUsersDto> feedUsers = new ArrayList<>();
		Map<Long, List<Groups>> feedGroups = new HashMap<Long, List<Groups>>();
		if (isAllRequired && null == feedManagementID) {
			List<FeedManagement> feedList = feedManagementService.findAll();
		  if(null != feedList && feedList.size() >0){
			List<FeedGroups> groups = null;
			for (FeedManagement feed : feedList) {
				if(null != feed){
				  groups = feed.getGroupLevels();
				}
				List<Groups> groupIds = new ArrayList<>();
				if (groups != null && groups.size()>0) {
					for (FeedGroups group : groups) {
						if(null != group && null != group.getGroupId() && null != group.getGroupId().getId()){
						    groupIds.add(group.getGroupId());
						}
					}
					feedGroups.put(feed.getFeed_management_id(), groupIds);
				}
			}
		  }	
			//
		} else {
			if (feedManagementID != null) {
				FeedManagement feed = feedManagementService.find(feedManagementID);
				List<FeedGroups> feedGroupsList = null;
				if(null != feed ){
				   feedGroupsList = feed.getGroupLevels();
				}
				List<Groups> groupIds = new ArrayList<>();
				if (feedGroupsList != null && feedGroupsList.size()>0) {
					for (FeedGroups group : feedGroupsList) {
						
						if(null != group && null != group.getGroupId() && null != group.getGroupId().getId()){
							groupIds.add(group.getGroupId());
						}
					}
					
					if(null != feed && null != feed.getFeed_management_id()){
						feedGroups.put(feed.getFeed_management_id(), groupIds);
					}
				}
			}
		}
		if (feedGroups != null && feedGroups.size() > 0) {
			for (Map.Entry<Long, List<Groups>> entry : feedGroups.entrySet()) {
				FeedUsersDto feedUsersDto = new FeedUsersDto();
				List<UsersDto> usersDto = new ArrayList<>();
				Map<Long, UsersDto> IdUser = new HashMap<Long, UsersDto>();
				
				
				for (Groups group : entry.getValue()) {
					
					List<UsersDto> users = null;
					if(null != group && null != group.getId()){
						users = usersService.listUserByGroup(group.getId(), null, null, null, null, null);
					}
					if(null != users){
					for (UsersDto user : users) {
						if(null != user && null != user.getUserId()){
							IdUser.put(user.getUserId(), user);
						}
					}
				  }
				}

				for (Map.Entry<Long, UsersDto> userIds : IdUser.entrySet()) {
					if(null != userIds && null != userIds.getValue()){
						usersDto.add(userIds.getValue());
					}
				}
				feedUsersDto.setFeedId(entry.getKey());
				feedUsersDto.setFeedUsers(usersDto);
				feedUsers.add(feedUsersDto);
			}
		}
		return feedUsers;
	}

	@Override
	@Transactional("transactionManager")
	public FeedManagementDto getFeedByTypeAndSource(Long typeId, Long sourceId) {
		FeedManagementDto feedmanagementDto= new FeedManagementDto();
		FeedManagement feedmanagement=	feedManagementDao.getFeedByTypeAndSource(typeId, sourceId);
		if(feedmanagement!=null) {
			BeanUtils.copyProperties(feedmanagement, feedmanagementDto);
		}
		return feedmanagementDto;
	}
	
	@Override
	@Transactional("transactionManager")
	public List<FeedGroups> getFeedGroupsByGroupId(List<Long> groupIds) {
		List<FeedGroups> feedGroups = feedManagementDao.getFeedGroupsByGroupId(groupIds);
		return feedGroups;
	}

	@Override
	@Transactional("transactionManager")
	public List<FeedManagement> getFeedsByTypeIds(List<Long> classificationIds) {
		List<FeedManagement> feeds = feedManagementDao.getFeedsByTypeIds(classificationIds);
		if(null!=feeds&& feeds.size()>0){
			feeds.stream().forEach(s-> {
				List<FeedGroups>feedGroups=feedGroupsService.getFeedGroupsByFeedId(s.getFeed_management_id());
				s.setGroupLevels(feedGroups);
			});	
		}
		
		return feeds;
	}

	@Override
	@Transactional("transactionManager")
	public List<FeedManagementDto> getFeedsByUserId(Long userId) {
		List<FeedManagement> feeds = new ArrayList<FeedManagement>();
		List<FeedManagementDto> feedsDtoList = new ArrayList<FeedManagementDto>();
		Users currentUser = usersService.find(userId);
		try{
			if(currentUser != null ){
				List<Long> feedIds = new ArrayList<>();
				List<Groups> userGroups = groupsService.getGroupsOfUser(currentUser.getUserId());
				List<Long> groupIds = userGroups.stream().map(Groups::getId).collect(Collectors.toList());
				if(groupIds != null && !groupIds.isEmpty()){
					List<FeedGroups> feedGroups = feedManagementService.getFeedGroupsByGroupId(groupIds);
					feedGroups.stream().filter(s -> null != s.getGroupId()).forEach(p -> Hibernate.initialize(p.getGroupId()));
					feedIds = feedGroups.stream().map(FeedGroups::getFeedId).collect(Collectors.toList())
							.stream().map(FeedManagement::getFeed_management_id).collect(Collectors.toList());
					if(null != feedIds && feedIds.size() > 0){
						feeds = feedManagementService.getFeedsByFeedIds(feedIds);
						if(null != feeds && feeds.size() > 0){
							feeds.stream().filter(s -> null != s.getGroupLevels()).forEach(f -> Hibernate.initialize(f.getGroupLevels()));
							feeds.stream().forEach(f -> f.getGroupLevels().stream().filter(s -> null != s.getGroupId()).forEach(d -> Hibernate.initialize(d.getGroupId())));
							feedsDtoList = feeds.stream()
									.map(f -> new EntityConverter<FeedManagement,FeedManagementDto>(FeedManagement.class, FeedManagementDto.class)
											.toT2(f , new FeedManagementDto()))  
									.collect(Collectors.toList());
									
						}
					}
					
				}
			}
		}catch(Exception e){
			return feedsDtoList;
		}
		return feedsDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public List<FeedManagement> getFeedsByFeedIds(List<Long> feedIds) {
		return feedManagementDao.getFeedsByFeedIds(feedIds);
	}

	@Override
	public void updateFeedGroupsSupportingFeed(Long feedManagementid  , List<FeedGroups> feedGroupsListDTO) {
		FeedManagement feedManagement = feedManagementService.find(feedManagementid);
		if(null != feedGroupsListDTO && null != feedManagement){
			 for (FeedGroups dto : feedGroupsListDTO) {
					FeedGroups newdto = new FeedGroups();
					newdto.setFeedId(feedManagement);
					Groups newGroup = new Groups();
					Groups existingGroup = groupsService.find(dto.getGroupId().getId());
					if(null !=  existingGroup){
						BeanUtils.copyProperties(existingGroup, newGroup);
						newdto.setGroupId(newGroup);
						newdto.setRank(dto.getRank());
						//dtoGroupsString.append(newGroup.getName()+ ", ");
						feedGroupsService.saveOrUpdate(newdto);
					}
				}
			alertService.updateGroupLevelOnfeedChangeByRnak(feedManagement.getFeed_management_id(),feedGroupsListDTO);
	  }
	}

}
