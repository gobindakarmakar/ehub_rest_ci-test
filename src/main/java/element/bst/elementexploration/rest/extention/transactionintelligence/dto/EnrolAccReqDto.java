package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

public class EnrolAccReqDto implements Serializable
{

	private static final long serialVersionUID = 1L;
	private List<TxEntityRespDto> txList;
	private String originatorAccountNumber;

	public List<TxEntityRespDto> getTxList() {
		return txList;
	}

	public void setTxList(List<TxEntityRespDto> txList) {
		this.txList = txList;
	}

	public String getOriginatorAccountNumber() {
		return originatorAccountNumber;
	}

	public void setOriginatorAccountNumber(String originatorAccountNumber) {
		this.originatorAccountNumber = originatorAccountNumber;
	}

}
