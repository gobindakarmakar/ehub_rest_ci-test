package element.bst.elementexploration.rest.extention.entity.complexStructure.daoImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.entity.complexStructure.dao.EntityComplexStructureDao;
import element.bst.elementexploration.rest.extention.entity.complexStructure.domain.EntityComplexStructure;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author hanuman
 *
 */
@Repository("entityComplexStructureDao")
@Transactional("transactionManager")
public class EntityComplexStructureDaoImpl extends GenericDaoImpl<EntityComplexStructure, Long> implements 
EntityComplexStructureDao{

	public EntityComplexStructureDaoImpl() {
		super(EntityComplexStructure.class);
	}

	@Override
	public EntityComplexStructure getEntityComplexStructure(String entityId,String entitySource,String entityName) {
		EntityComplexStructure elementComplexStructure = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select ecs from EntityComplexStructure ecs where ecs.entityId = :entityId and ecs.entitySource = :entitySource and entityName = :entityName");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("entitySource", entitySource);
			query.setParameter("entityName", entityName);
			elementComplexStructure = (EntityComplexStructure) query.getSingleResult();
		} catch (Exception e) {
			return elementComplexStructure;
		}
		return elementComplexStructure;
	}

}
