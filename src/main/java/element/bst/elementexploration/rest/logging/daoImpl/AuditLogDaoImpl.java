package element.bst.elementexploration.rest.logging.daoImpl;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.util.LogRequest;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("auditLogDao")
public class AuditLogDaoImpl extends GenericDaoImpl<AuditLog, Long> implements AuditLogDao {

	public AuditLogDaoImpl() {
		super(AuditLog.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AuditLog> getLogs(LogRequest logRequest) {
		List<AuditLog> logsList = new ArrayList<>();

		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from AuditLog auditLog  ");
			String hql = new String(this.createQueryBuilder(logRequest));
			queryBuilder.append(hql);

			queryBuilder.append(" order by ").append("auditLog.")
					.append(resolveOrderColumnName(logRequest.getOrderBy()));
			if (logRequest.getOrderBy() != null && logRequest.getOrderIn() != null) {
				if ("asc".equalsIgnoreCase(logRequest.getOrderIn())
						|| "desc".equalsIgnoreCase(logRequest.getOrderIn())) {
					queryBuilder.append(" ");
					queryBuilder.append(logRequest.getOrderIn());
				} else {
					queryBuilder.append(" ");
					queryBuilder.append("desc");
				}
			} else {
				queryBuilder.append(" ");
				queryBuilder.append("desc");
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if (logRequest.getUserId() != null)
				query.setParameter("userId", logRequest.getUserId());
			if (logRequest.getCaseId() != null)
				query.setParameter("caseId", logRequest.getCaseId());
			if (logRequest.getDocId() != null)
				query.setParameter("documentId", logRequest.getDocId());
			if (logRequest.getEntityId() != null)
				query.setParameter("entityId", logRequest.getEntityId());
			if (logRequest.getType() != null && !logRequest.getType().equals("GENERAL"))
				query.setParameter("type", logRequest.getType());
			if (logRequest.getFlag() != null)
				query.setParameter("flag", logRequest.getFlag());
			if (logRequest.getLevel() != null)
				query.setParameter("level", logRequest.getLevel());
			query.setFirstResult((logRequest.getPageNumber() - 1) * (logRequest.getRecordsPerPage()));
			query.setMaxResults(logRequest.getRecordsPerPage());
			logsList = (List<AuditLog>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return logsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countLogs(LogRequest logRequest) {
		List<AuditLog> logsList = new ArrayList<>();

		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from AuditLog auditLog ");
			String hql = new String(this.createQueryBuilder(logRequest));
			queryBuilder.append(hql);

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if (logRequest.getUserId() != null)
				query.setParameter("userId", logRequest.getUserId());
			if (logRequest.getCaseId() != null)
				query.setParameter("caseId", logRequest.getCaseId());
			if (logRequest.getDocId() != null)
				query.setParameter("documentId", logRequest.getDocId());
			if (logRequest.getEntityId() != null)
				query.setParameter("entityId", logRequest.getEntityId());
			if (logRequest.getType() != null && !logRequest.getType().equals("GENERAL"))
				query.setParameter("type", logRequest.getType());
			if (logRequest.getFlag() != null)
				query.setParameter("flag", logRequest.getFlag());
			if (logRequest.getLevel() != null)
				query.setParameter("level", logRequest.getLevel());
			logsList = (List<AuditLog>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) logsList.size();
	}

	private String resolveOrderColumnName(String columnName) {
		if (columnName == null)
			return "createdOn";
		switch (columnName.toLowerCase()) {
		case "createdOn":
			return "createdOn";
		default:
			return "createdOn";
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogAggregator> logAggregator(LogRequest logRequest) {
		List<LogAggregator> logAggregatorList = new ArrayList<LogAggregator>();
		try {
			StringBuilder hql = new StringBuilder();
			String builder = new String();
			hql.append("select new element.bst.elementexploration.rest.logging.domain.LogAggregator( ");
			if (logRequest.getGranularity() != null) {
				if ((logRequest.getGranularity().name()).equalsIgnoreCase("daily"))
					builder = "DATE_FORMAT(auditLog.createdOn,'%Y-%m-%d')";
				if ((logRequest.getGranularity().name()).equalsIgnoreCase("weekly"))
					builder = "DATE_FORMAT(auditLog.createdOn,'%X-%V')";
				if ((logRequest.getGranularity().name()).equalsIgnoreCase("monthly"))
					builder = "DATE_FORMAT(auditLog.createdOn,'%Y-%m')";
				if ((logRequest.getGranularity().name()).equalsIgnoreCase("yearly"))
					builder = "DATE_FORMAT(auditLog.createdOn,'%Y')";
			} else {
				builder = "DATE_FORMAT(auditLog.createdOn,'%Y-%m')";
			}
			hql.append(builder);
			hql.append(" ,count(auditLog) )");
			hql.append("from AuditLog auditLog ");

			String queryBuilder = new String(this.createQueryBuilder(logRequest));
			hql.append(queryBuilder);

			hql.append(" group by " + builder);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if (logRequest.getUserId() != null)
				query.setParameter("userId", logRequest.getUserId());
			if (logRequest.getCaseId() != null)
				query.setParameter("caseId", logRequest.getCaseId());
			if (logRequest.getDocId() != null)
				query.setParameter("documentId", logRequest.getDocId());
			if (logRequest.getEntityId() != null)
				query.setParameter("entityId", logRequest.getEntityId());
			if (logRequest.getType() != null)
				query.setParameter("type", logRequest.getType());
			if (logRequest.getFlag() != null)
				query.setParameter("flag", logRequest.getFlag());
			if (logRequest.getLevel() != null)
				query.setParameter("level", logRequest.getLevel());
			logAggregatorList = (List<LogAggregator>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, AuditLogDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}
		return logAggregatorList;

	}

	public StringBuilder createQueryBuilder(LogRequest logRequest) {
		boolean isOptional = false;
		String clause = " where ";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		StringBuilder queryBuilder = new StringBuilder();

		if (logRequest.getUserId() != null) {
			queryBuilder.append(clause + "auditLog.userId = :userId ");
			isOptional = true;
		}
		if (!StringUtils.isEmpty(logRequest.getKeyword())) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + "( auditLog.name like '%" + logRequest.getKeyword() + "%'");
			queryBuilder.append(" or auditLog.title like '%" + logRequest.getKeyword() + "%'");
			queryBuilder.append(" or auditLog.description like '%" + logRequest.getKeyword() + "%' )");
			isOptional = true;
		}
		if (logRequest.getCaseId() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.caseId = :caseId");
			isOptional = true;
		}
		if (logRequest.getDocId() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.documentId = :documentId");
			isOptional = true;
		}
		if (logRequest.getEntityId() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.entityId = :entityId");
			isOptional = true;
		}
		if (logRequest.getType() != null) {
			if (isOptional)
				clause = " and";
			if(logRequest.getType().equals("GENERAL"))
				queryBuilder.append(clause + " auditLog.type is null");
			else
				queryBuilder.append(clause + " auditLog.type = :type");
			isOptional = true;
		}
		if (logRequest.getFlag() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.flag = :flag");
			isOptional = true;
		}
		if (logRequest.getLevel() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.level <= :level");
			isOptional = true;
		}
		if (logRequest.getDateFrom() != null && logRequest.getDateTo() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.createdOn BETWEEN ");
			queryBuilder.append("'" + logRequest.getDateFrom().format(formatter) + "'");
			queryBuilder.append(" and '" + logRequest.getDateTo().format(formatter) + "'");
			isOptional = true;
		} else if (logRequest.getDateFrom() != null && logRequest.getDateTo() == null) {
			if (isOptional)
				clause = " and";
			queryBuilder
					.append(clause + " auditLog.createdOn >= '" + logRequest.getDateFrom().format(formatter) + "' ");
			isOptional = true;
		} else if (logRequest.getDateFrom() == null && logRequest.getDateTo() != null) {
			if (isOptional)
				clause = " and";
			queryBuilder.append(clause + " auditLog.createdOn < '" + logRequest.getDateTo().format(formatter) + "' ");
			isOptional = true;
		}
		return queryBuilder;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTypes() {
		List<String> listTypes = new ArrayList<String>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from AuditLog auditLog group by auditLog.type ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			List<AuditLog> auditLogList = (List<AuditLog>) query.getResultList();
			if (auditLogList != null) {
				listTypes = auditLogList.stream().filter(value -> value.getType() != null)
						.map(obj -> new String(obj.getType())).collect(Collectors.toList());
				listTypes.add("GENERAL");
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, AuditLogDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return listTypes;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<AuditLog> getLogsByTypeId(String type, Long typeId) {
		
			List<AuditLog> logsList = new ArrayList<>();
			try {
			StringBuilder hql = new StringBuilder();
			hql.append("select ag from AuditLog ag where ag.type = :type and ag.typeId= :typeId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("type", type);
			query.setParameter("typeId", typeId);
			logsList = (List<AuditLog>) query.getResultList();

			} catch (Exception e) {
				e.printStackTrace();
				ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
			}
			return logsList;
	}

	@Override
	public Date getLatestStatusChangeOfAlert(Long alertId) {
		Date date =null;
		//AuditLog log = new AuditLog();
		try {
		StringBuilder hql = new StringBuilder();
		hql.append("select max(ag.createdOn) from AuditLog ag where ag.typeId = :alertId and ag.subType is not null and ag.type=:type");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("alertId", alertId);
		//query.setParameter("subType", ElementConstants.ALERT_STATUS_SUBTYPE);
		query.setParameter("type", ElementConstants.ALERT_MANAGEMENT_TYPE);
		query.setMaxResults(1);
		 date = (Date) query.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return date;
}

	@SuppressWarnings("unchecked")
	@Override
	public List<AuditLog> getLogsByTypeAndSubtype(String type, String subType) {
		List<AuditLog> logsList = new ArrayList<>();
		try {
		StringBuilder hql = new StringBuilder();
		hql.append("select ag from AuditLog ag where ag.type = :type and ag.subType= :subType");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("type", type);
		query.setParameter("subType", subType);
		logsList = (List<AuditLog>) query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return logsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AuditLog> getLogsByTitleAndTypeId(String title, Long typeId) {

		List<AuditLog> logsList = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select ag from AuditLog ag where ag.title = :title and ag.typeId= :typeId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("title", title);
			query.setParameter("typeId", typeId);
			logsList = (List<AuditLog>) query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return logsList;
	}
}
