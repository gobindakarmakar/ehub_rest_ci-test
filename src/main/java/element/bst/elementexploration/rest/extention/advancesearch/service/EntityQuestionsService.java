package element.bst.elementexploration.rest.extention.advancesearch.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityQuestions;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityQuestionsDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author rambabu
 *
 */
public interface EntityQuestionsService extends GenericService<EntityQuestions, Long> {

	boolean saveEntityQuestions(List<EntityQuestionsDto> entityQuestionDtoList, String surveyId,Long userId);

	List<EntityQuestionsDto> getEntityQuestionsByEntityId(String entityId,Long surveyId);
	
	List<EntityQuestionsDto> getEntityQuestionsByEntityIdAndUserId(String entityId,Long userId,Long surveyId);
	

}
