package element.bst.elementexploration.test.usermanagement.security.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.usermanagement.security.daoImpl.CountrySettingDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CountrySettingDaoTest {
	
	@Mock
	 SessionFactory sessionFactory;
	
	@Mock
	Session session;
	
	@SuppressWarnings("rawtypes")
	@Mock
	Query query;
	
	@InjectMocks
	CountrySettingDaoImpl countrySettingDao;
	
	List<CountrySetting> countrySettingsList = new ArrayList<CountrySetting>();
	
	@Before
	public void doSetup() {
		CountrySetting countrySetting = new CountrySetting();
		countrySetting.setCountryIso("USA");
		countrySetting.setCurrency("USD");
		countrySettingsList.add(countrySetting);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getCountrySettingTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(countrySettingsList);
		CountrySetting countrySettingNew = countrySettingDao.getCountrySetting("USA"); 
		assertEquals("USD",countrySettingNew.getCurrency());
	}
	
	@Test
	public void getCountrySettingNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(null);
		CountrySetting countrySettingNew = countrySettingDao.getCountrySetting("USA"); 
		assertEquals(null,countrySettingNew.getCurrency());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCountrySetting_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		countrySettingDao.getCountrySetting("USA"); 
		
	}
	
	

}
