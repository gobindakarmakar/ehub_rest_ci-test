package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentQuestionsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplateMappingDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplatesDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.extention.docparser.dto.CheckPdfTypeDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ContentDataDto;
import element.bst.elementexploration.rest.extention.docparser.dto.CoordinatesResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocParserContent;
import element.bst.elementexploration.rest.extention.docparser.dto.DocParserContentSave;
import element.bst.elementexploration.rest.extention.docparser.dto.DocParserSaveDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentContentDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.PossibleAnswersDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TemplatesRepsonseDto;
import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import element.bst.elementexploration.rest.extention.docparser.service.PossibleAnswersService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.PdfReaderNewTemplateUtil;
import element.bst.elementexploration.rest.util.PdfReaderUtil;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.PdfUtilities;



/**
 * @author Rambabu
 *
 */
@Service("documentTemplatesService")
public class DocumentTemplatesServiceImpl extends GenericServiceImpl<DocumentTemplates, Long>
		implements DocumentTemplatesService {

	@Autowired
	public DocumentTemplatesServiceImpl(GenericDao<DocumentTemplates, Long> genericDao) {
		super(genericDao);
	}
	

	  @Value("${isocode_question_url}")
	  private String ISOCODE_QUESTION_URL;
	  
	  @Value("${imageAllocationArea}")
		private String TEMPLATE_FILE_LOCATION;
	  
	  @Value("${tesseract_language}")
		private String TESSRACT_LANGUAGE;
	
	@Autowired
	DocumentTemplatesDao documentTemplatesDao;
	
	@Autowired
	DocumentQuestionsService documentsQuestionsService;
	
	@Autowired
	DocumentQuestionsDao documentQuestionsDao;
	
	@Autowired
	DocumentService documentService;
	
	@Autowired
	PdfReaderUtil pdfReaderUtil;
	
	@Autowired
	PdfReaderNewTemplateUtil pdfReaderNewTemplateUtil;
	
	@Autowired
	DocumentTemplateMappingDao documentTemplateMappingDao;
	
	@Autowired
	UserDao userDao;
	
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	DocumentParserService documentParserService;
	
	@Autowired
	PossibleAnswersService  possibleAnswersService;
	
	@Override
	@Transactional("transactionManager")
	public long saveNewPdfFillableTemplate(DocParserSaveDto responseDto, Long docIdUploaded, Long userId,Integer docFlag,byte[] byteArray) throws Exception {
		DocumentTemplates documentTemplate = null;
		//Map<String, String> responseMap= pdfReaderUtil.parsePdfFile(uploadFile);
		DocumentVault documentVault = documentService.find(docIdUploaded);
		if(documentVault!=null)
		{
			//documentVault.setDocFlag(7);
			//documentService.saveOrUpdate(documentVault);		
	/*	try
		{*/
		if(responseDto.getDocumentContentDto().size()>0)
		{
			 documentTemplate = new DocumentTemplates();
			documentTemplate.setTemplateFileType(documentVault.getType());
			documentTemplate.setTemplateName(documentVault.getDocName());
			documentTemplate.setInitialDocId(docIdUploaded);
			documentTemplate.setDocParserTemplate(true);
			documentTemplate.setCreatedBy(userId);
			documentTemplate.setModifiedBy(userId);
			//finding is fillable or scanned
			CheckPdfTypeDto checkPdfType = documentParserService.checkPdfType(byteArray);
			if(checkPdfType.isFillablePdf()==true)
				documentTemplate.setFillablePdf(true);//for fillable pdf
			else if (checkPdfType.isNormalPdf()==true)
				documentTemplate.setNormalPdf(true);
			else if(checkPdfType.isScannedPdf()==true)
				documentTemplate.setScannedPdf(true);
			else
			 throw new NoDataFoundException("PDF Cannot be parsed due to bad format");
			DocumentTemplates savedTemplate=documentTemplatesDao.create(documentTemplate);
			eventPublisher.publishEvent(new LoggingEvent(savedTemplate.getId(), LoggingEventType.CREATE_TEMPLATE, userId, new Date(), null));
			List<DocumentQuestions> questionList = new ArrayList<DocumentQuestions>();
			if(savedTemplate != null)
			{
				int i=0,j=0,k=0,l=0;
				long parentId = 0;
				List<DocParserContent> documentContentDtoList = responseDto.getDocumentContentDto();
				for(DocParserContent documentContent : documentContentDtoList)
				{
					DocParserContentSave content = documentContent.getContentData();
					if(parentId!=0)
					{
						DocumentQuestions documentQuestion=new DocumentQuestions();
						documentQuestion.setTemplateId(savedTemplate.getId());
						documentQuestion.setContentType(documentContent.getContentType());
						documentQuestion.setPageNumber(content.getPageNumber());
						if(documentContent.getContentType().equals("Question") || documentContent.getContentType().equals("Form Fields"))
							documentQuestion.setQuestionStatus(true);
						documentQuestion.setName(content.getSpecifiedQuestion());
						if(content.getSpecifiedQuestionPosition()!=null)
							documentQuestion.setQuestionPosition(content.getSpecifiedQuestionPosition());
						if(content.getSpecifiedQuestionPositionOnThePage()!=null)
							documentQuestion.setQuestionPositionOnThePage(content.getSpecifiedQuestionPositionOnThePage());
						if(content.getQuestionUUID()!=null)
							documentQuestion.setQuestionUUID(content.getQuestionUUID());
						documentQuestion.setAnswerType(content.getSpecifiedAnswerType());
						documentQuestion.setFormField(content.getFormField());
						documentQuestion.setSubType(content.getSubType());
						if(content.getSpecifiedAnswerType().toString().equals("CHECKBOX") || content.getSpecifiedAnswerType().toString().equals("YES_NO_CHECKBOX"))
						{
							List<PossibleAnswersDto> answersDtos = content.getPossibleAnswersDto();
							List<PossibleAnswers> possibleAnswersList = new ArrayList<PossibleAnswers>();
							for (PossibleAnswersDto possibleAnswer : answersDtos) 
							{
								PossibleAnswers answers = new PossibleAnswers();
								answers.setAnswerPosition(possibleAnswer.getAnswerPosition());
								answers.setDocumentQuestions(documentQuestion);
								answers.setPossibleAnswer(possibleAnswer.getPossibleAnswer());
								answers.setAnswerPositionOnThePage(possibleAnswer.getAnswerPositionOnThePage());
								answers.setAnswerUUID(possibleAnswer.getAnswerUUID());
								possibleAnswersList.add(answers);
							}
							documentQuestion.setPossibleAnswers(possibleAnswersList);
						}
						if(content.getAnswerPosition()!=null)
							documentQuestion.setAnswerPosition(content.getAnswerPosition());
						if(content.getSpecifiedAnswerPositionOnThePage()!=null)
							documentQuestion.setAnswerPositionOnThePage(content.getSpecifiedAnswerPositionOnThePage());
						if(content.getAnswerUUID()!=null)
							documentQuestion.setAnswerUUID(content.getAnswerUUID());
						documentQuestion.setShowInUI(true);
						String response = getIsoCodesJson(documentQuestion.getName());
						if (response != null) 
						{
							JSONObject jsonResponseObjectNew = new JSONObject(response);
							JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
							if (arrayObjectNew.length() > 0) {

								JSONObject normalObject = arrayObjectNew.getJSONObject(0);
								if (normalObject.has("controls")) {
									if (normalObject.getJSONObject("controls").has("level3")) {
										JSONObject level3Object = normalObject.getJSONObject("controls")
												.getJSONObject("level3");
										if (level3Object.has("code")) {
											documentQuestion.setPrimaryIsoCode(level3Object.getString("code"));
											documentQuestion.setHasIsoCode(true);
										}
										if (level3Object.has("title"))
											documentQuestion.setLevel3Title(level3Object.getString("title"));
										if (level3Object.has("title"))
											documentQuestion.setLevel3Description(level3Object.getString("title"));
									}
									// documentQuestion.setPrimaryIsoCode(getPrimaryIsoCode(response));
									if (normalObject.getJSONObject("controls").has("level1")) {
										JSONObject level1Object = normalObject.getJSONObject("controls")
												.getJSONObject("level1");
										if (level1Object.has("code"))
											documentQuestion.setLevel1Code(level1Object.getString("code"));
										if (level1Object.has("title"))
											documentQuestion.setLevel1Title(level1Object.getString("title"));										
									}
									documentQuestion
									.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
									if (normalObject.getJSONObject("controls").has("topic"))
										documentQuestion
												.setTopic(normalObject.getJSONObject("controls").getString("topic"));
								}
							
								
							} 
						}
						if(documentQuestion.getPrimaryIsoCode() == null)
							documentQuestion.setHasIsoCode(false);
						/*String[] answerCode = entryJson.getValue().split("&");
						documentQuestion.setPdfAnswerCode(answerCode[0]);
						
						if(answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
						{
							documentQuestion.setAnswerType(AnswerType.CHECKBOX);
							documentQuestion.setPossibleAnswer("YES/NO");
						}
						if(answerCode[1].trim().equalsIgnoreCase("TEXT"))
							documentQuestion.setAnswerType(AnswerType.TEXT);
						if(!answerCode[1].trim().equalsIgnoreCase("TEXT") && !answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
						{
							documentQuestion.setAnswerType(AnswerType.CHECKBOX);
							documentQuestion.setPossibleAnswer("YES/NO");
						}
						*/
						if(documentQuestion.getName().contains("Name") || documentQuestion.getName().contains("name")){
							i++;
							if(i==1){
								documentQuestion.setCaseField("name");
								documentQuestion.setRequiredForOSINT(true);
							}
						}
						if(documentQuestion.getName().equalsIgnoreCase("address")){
							j++;
							if(j==1){
								documentQuestion.setCaseField("address");
								documentQuestion.setRequiredForOSINT(true);
							}
						}
						if(documentQuestion.getName().equalsIgnoreCase("email")){
							l++;
							if(l==1){
								documentQuestion.setCaseField("email");
								documentQuestion.setRequiredForOSINT(true);
							}
						}
						if(documentQuestion.getName().contains("website") || documentQuestion.getName().contains("Website")){
							k++;
							if(k==1){
								documentQuestion.setCaseField("website");
								documentQuestion.setRequiredForOSINT(true);
							}
						}
						documentQuestion.setHasParentId(parentId);
						DocumentQuestions question = documentQuestionsDao.create(documentQuestion);
						questionList.add(question);
						parentId=0;
					}
					else
					{
					DocumentQuestions documentQuestion=new DocumentQuestions();
					documentQuestion.setTemplateId(savedTemplate.getId());
					documentQuestion.setContentType(documentContent.getContentType());
					documentQuestion.setPageNumber(content.getPageNumber());
					documentQuestion.setFormField(content.getFormField());
					documentQuestion.setElementName(content.getElementName());
					documentQuestion.setSubType(content.getSubType());
					if(documentContent.getContentType().equals("Question")  || documentContent.getContentType().equals("Form Fields"))
						documentQuestion.setQuestionStatus(true);
					documentQuestion.setName(content.getQuestion());
					if(content.getQuestionPosition()!=null)
						documentQuestion.setQuestionPosition(content.getQuestionPosition());
					if(content.getQuestionPositionOnThePage()!=null)
						documentQuestion.setQuestionPositionOnThePage(content.getQuestionPositionOnThePage());
					if(content.getQuestionUUID()!=null)
						documentQuestion.setQuestionUUID(content.getQuestionUUID());
					documentQuestion.setAnswerType(content.getAnswerType());
					if(content.getAnswerType()!=null)
					{
					if(content.getAnswerType().toString().equals("CHECKBOX") || content.getAnswerType().toString().equals("YES_NO_CHECKBOX"))
					{
						List<PossibleAnswersDto> answersDtos = content.getPossibleAnswersDto();
						List<PossibleAnswers> possibleAnswersList = new ArrayList<PossibleAnswers>();
						for (PossibleAnswersDto possibleAnswer : answersDtos) 
						{
							PossibleAnswers answers = new PossibleAnswers();
							answers.setAnswerPosition(possibleAnswer.getAnswerPosition());
							answers.setDocumentQuestions(documentQuestion);
							answers.setPossibleAnswer(possibleAnswer.getPossibleAnswer());
							answers.setAnswerPositionOnThePage(possibleAnswer.getAnswerPositionOnThePage());
							answers.setAnswerUUID(possibleAnswer.getAnswerUUID());
							possibleAnswersList.add(answers);
						}
						documentQuestion.setPossibleAnswers(possibleAnswersList);
					}
					}
					if(content.getAnswerPosition()!=null)
						documentQuestion.setAnswerPosition(content.getAnswerPosition());
					if(content.getAnswerPositionOnThePage()!=null)
						documentQuestion.setAnswerPositionOnThePage(content.getAnswerPositionOnThePage());
					if(content.getAnswerUUID()!=null)
						documentQuestion.setAnswerUUID(content.getAnswerUUID());
					documentQuestion.setShowInUI(true);
					String response = getIsoCodesJson(documentQuestion.getName());
					if (response != null) 
					{
						JSONObject jsonResponseObjectNew = new JSONObject(response);
						JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
						if (arrayObjectNew.length() > 0) {
							JSONObject normalObject = arrayObjectNew.getJSONObject(0);
							JSONObject level3Object = normalObject.getJSONObject("controls").getJSONObject("level3");
							documentQuestion.setPrimaryIsoCode(level3Object.getString("code"));
							documentQuestion.setHasIsoCode(true);
							documentQuestion.setLevel3Title(level3Object.getString("title"));
							documentQuestion.setLevel3Description(level3Object.getString("title"));
							JSONObject level1Object = normalObject.getJSONObject("controls").getJSONObject("level1");
							documentQuestion.setLevel1Code(level1Object.getString("code"));
							documentQuestion.setLevel1Title(level1Object.getString("title"));
							documentQuestion.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
							documentQuestion.setTopic(normalObject.getJSONObject("controls").getString("topic"));
							
						} 
					}
					if(documentQuestion.getPrimaryIsoCode() == null)
						documentQuestion.setHasIsoCode(false);
					/*String[] answerCode = entryJson.getValue().split("&");
					documentQuestion.setPdfAnswerCode(answerCode[0]);
					
					if(answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
					{
						documentQuestion.setAnswerType(AnswerType.CHECKBOX);
						documentQuestion.setPossibleAnswer("YES/NO");
					}
					if(answerCode[1].trim().equalsIgnoreCase("TEXT"))
						documentQuestion.setAnswerType(AnswerType.TEXT);
					if(!answerCode[1].trim().equalsIgnoreCase("TEXT") && !answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
					{
						documentQuestion.setAnswerType(AnswerType.CHECKBOX);
						documentQuestion.setPossibleAnswer("YES/NO");
					}
					*/
					if((documentQuestion.getName()!=null)&&(documentQuestion.getName().contains("Name") || documentQuestion.getName().contains("name"))){
						i++;
						if(i==1){
							documentQuestion.setCaseField("name");
							documentQuestion.setRequiredForOSINT(true);
						}
					}
					if(documentQuestion.getName()!=null&&documentQuestion.getName().equalsIgnoreCase("address")){
						j++;
						if(j==1){
							documentQuestion.setCaseField("address");
							documentQuestion.setRequiredForOSINT(true);
						}
					}
					if(documentQuestion.getName()!=null&&documentQuestion.getName().equalsIgnoreCase("email")){
						l++;
						if(l==1){
							documentQuestion.setCaseField("email");
							documentQuestion.setRequiredForOSINT(true);
						}
					}
					if((documentQuestion.getName()!=null)&&(documentQuestion.getName().contains("website") || documentQuestion.getName().contains("Website"))){
						k++;
						if(k==1){
							documentQuestion.setCaseField("website");
							documentQuestion.setRequiredForOSINT(true);
						}
					}
					DocumentQuestions question = documentQuestionsDao.create(documentQuestion);
					if(content.getSpecifiedQuestion()!=null && !content.getSpecifiedQuestion().equals(""))
						parentId = question.getId();
					questionList.add(question);
					}
				}
				List<DocumentQuestions> controlQuestion =  documentsQuestionsService.getListOfControlQuestion();
				for (DocumentQuestions documentQuestions2 : controlQuestion) {
					documentQuestions2.setTemplateId(savedTemplate.getId());
					documentQuestionsDao.create(documentQuestions2);
				}
				if(i==0) 
				{
					DocumentQuestions question1=questionList.get(0);
					question1.setCaseField("name");
					question1.setRequiredForOSINT(true);
					documentQuestionsDao.saveOrUpdate(question1);
				}
			}
			
			
		}
		/*}
		catch (Exception e) 
		{
			throw new NoDataFoundException("Failed Due to Internal Server Error");
		}*/
	}else
		throw new NoDataFoundException("Document not found");
		
		return documentTemplate.getId();
	}
	
	

	
	
	private String getIsoCodesJson(String question) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("text", question);
		jsonObject.put("size", 5);
		String response=null;
		String url = ISOCODE_QUESTION_URL + "iso/map";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonObject.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 404) 
			return null;
		 else
			 return response;

	}
	
	private StringBuilder getSecondaryIsoCode(String jsonResponse) {
		ArrayList<String> secondaryIsocodesList = new ArrayList<String>();
		StringBuilder builder = new StringBuilder();
		if (jsonResponse != null) {
			JSONObject jsonResponseObject = new JSONObject(jsonResponse);
			JSONArray array = jsonResponseObject.getJSONArray("hits");
			if (array.length() > 0) {
				for (int i = 1; i < array.length(); i++) {
					JSONObject resultObject = array.getJSONObject(i);
					JSONObject secondaryControlObject = resultObject.getJSONObject("controls").getJSONObject("level3");
					secondaryIsocodesList.add(secondaryControlObject.getString("code").toString());
					builder.append(secondaryControlObject.getString("code").toString());
					if (i != (array.length() - 1))
						builder.append(",");
				}
			}
		}
		return builder;
	}





	@SuppressWarnings("static-access")
	@Override
	@Transactional("transactionManager")
	public CoordinatesResponseDto getQuestionAndAnswerNewTemplate(CoordinatesResponseDto coordinatesDto, byte[] byteArrayFile) throws InvalidPasswordException, IOException, TesseractException
	{
		DocumentVault documentVault = documentService.find(coordinatesDto.getDocId());
		CoordinatesResponseDto coordinatesResponseDto = new CoordinatesResponseDto();
		if(documentVault.getDocStructureType()==1)
		{
		if(coordinatesDto.getQuestionCoordinates()!=null && !coordinatesDto.getQuestionCoordinates().equals(""))
		{
			coordinatesResponseDto.setQuestion(pdfReaderNewTemplateUtil.parsePdfFile(byteArrayFile,coordinatesDto,false));
			 String replaceExtra = coordinatesResponseDto.getQuestion().replaceAll("_"," ");
           	// String replaceExtraOne = replaceExtra.replaceAll("\\s+"," ");
           	 coordinatesResponseDto.setQuestion(replaceExtra.trim());
			coordinatesResponseDto.setQuestionCoordinates(coordinatesDto.getQuestionCoordinates());
			coordinatesResponseDto.setPageNumber(coordinatesDto.getPageNumber());
			coordinatesResponseDto.setQuestionPositionOnThePage(coordinatesDto.getQuestionPositionOnThePage());
			coordinatesResponseDto.setElementName(coordinatesDto.getElementName());
			if(coordinatesDto.getAnswerUUID()!=null)
				coordinatesResponseDto.setAnswerUUID(coordinatesDto.getAnswerUUID());
			coordinatesResponseDto.setQuestionUUID(coordinatesDto.getQuestionUUID());
			coordinatesResponseDto.setContentType(coordinatesDto.getContentType());
			if(coordinatesDto.getContentType().equals("Form Fields"))
			{
				coordinatesResponseDto.setQuestion(pdfReaderNewTemplateUtil.parsePdfFile(byteArrayFile,coordinatesDto,false));
				 String replaceExtra1 = coordinatesResponseDto.getQuestion().replaceAll("_"," ");
				 coordinatesResponseDto.setQuestion(replaceExtra1.trim());
				//settings answer
				 coordinatesDto.setAnswerCoordinates(coordinatesDto.getQuestionCoordinates());//for answer
				 org.json.simple.JSONObject jsonObject = getAnswerPositions(byteArrayFile,coordinatesDto);
				 if(!jsonObject.isEmpty())
				 {
				 coordinatesResponseDto.setAnswer(jsonObject.get("answer").toString());
				 coordinatesResponseDto.setAnswerType(jsonObject.get("type").toString());
				 }
				 if((coordinatesResponseDto.getAnswerType().toString().equals("CHECKBOX")) || (coordinatesResponseDto.getAnswerType().toString().equals("YES_NO_CHECKBOX")))
				 {
					 throw new NoDataFoundException("unable to recognise form field , please mark question and answer full position (or) you have marked checkbox which is not acceptable");
				 }
				 if(coordinatesResponseDto.getAnswer()==null || coordinatesResponseDto.getAnswer().trim().length()==0) {
					 coordinatesResponseDto.setFormField("NONE");
				 }
				 
				 if(coordinatesResponseDto.getAnswer()!=null ||	 coordinatesResponseDto.getAnswer().trim().length()>0)
				 {
						String urlPattern = "^(http(s{0,1})://|www)[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
						if(coordinatesResponseDto.getAnswer().matches(urlPattern))
							coordinatesResponseDto.setFormField("website");
						String emailPattern = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
						if(coordinatesResponseDto.getAnswer().matches(emailPattern))
							coordinatesResponseDto.setFormField("email");
						if(coordinatesResponseDto.getFormField()==null)
							 coordinatesResponseDto.setFormField("NONE");
				 }
				 
				/*
				coordinatesResponseDto.setQuestion(pdfReaderNewTemplateUtil.parsePdfFile(byteArrayFile,coordinatesDto,false));
				 String replaceExtra1 = coordinatesResponseDto.getQuestion().replaceAll("_"," ");
				 coordinatesResponseDto.setQuestion(replaceExtra1.trim());
				 //settings answer
				 coordinatesDto.setAnswerCoordinates(coordinatesDto.getQuestionCoordinates());//for answer
				 org.json.simple.JSONObject jsonObject = getAnswerPositions(byteArrayFile,coordinatesDto);
				 if(!jsonObject.isEmpty())
				 {
				 coordinatesResponseDto.setAnswer(jsonObject.get("answer").toString());
				 coordinatesResponseDto.setAnswerType(jsonObject.get("type").toString());
				 if((coordinatesResponseDto.getAnswerType().toString().equals("CHECKBOX")) || (coordinatesResponseDto.getAnswerType().toString().equals("YES_NO_CHECKBOX")))
				 {
					 throw new NoDataFoundException("unable to recognise form field , please mark question and answer full position (or) you have marked checkbox which is not acceptable");
				 }
				 }
				List<FormFieldsSubType> fieldsSubTypesList = formFieldsSubTypeService.findAll();
				String lowerCase =""; 
				//remove special characters
				if(coordinatesResponseDto.getQuestion()!=null)
				{
				String lowerCaseNew = coordinatesResponseDto.getQuestion().trim().toLowerCase();
				String removeCharc = lowerCaseNew.substring(lowerCaseNew.length() -1);
				Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
				Matcher m = p.matcher(removeCharc);
				if(m.find())
				{
					lowerCase = lowerCaseNew.substring(0,lowerCaseNew.length()-1);
				}
				}
				out:for (FormFieldsSubType formFieldsSubType : fieldsSubTypesList) 
				{
					if(lowerCase.contains("name"))
					{
						if(formFieldsSubType.getFormField().toLowerCase().contains("name"))
						{
							coordinatesResponseDto.setFormField(formFieldsSubType.getFormField());
							coordinatesResponseDto.setSubType(formFieldsSubType.getSubTypeAnswer());
							if(formFieldsSubType.getIcon()!=null)
								coordinatesResponseDto.setIcon(formFieldsSubType.getIcon());
							break out;
						}//if close for form fields
					}
						
				else if(Pattern.compile(Pattern.quote(lowerCase), Pattern.CASE_INSENSITIVE).matcher(formFieldsSubType.getFormField()).find())
					{
						String REGEX = "\\b"+formFieldsSubType.getFormField().toLowerCase()+"\\b";
						Pattern p = Pattern.compile(REGEX);
					      Matcher m = p.matcher(coordinatesResponseDto.getQuestion());
						coordinatesResponseDto.setFormField(formFieldsSubType.getFormField());
						coordinatesResponseDto.setSubType(formFieldsSubType.getSubTypeAnswer());
						if(formFieldsSubType.getIcon()!=null)
							coordinatesResponseDto.setIcon(formFieldsSubType.getIcon());
						break out;
					}//if close for form fields
					}
					
				 //saving new form field
				if(coordinatesResponseDto.getFormField()==null || coordinatesResponseDto.getFormField().equals(""))
				{
				FormFieldsSubType fieldsSubType = new FormFieldsSubType();
				if(coordinatesResponseDto.getQuestion()!=null)
				{
				fieldsSubType.setFormField(coordinatesResponseDto.getQuestion());
				fieldsSubType.setIcon("default");
				fieldsSubType.setSubType(coordinatesResponseDto.getQuestion());
				fieldsSubType.setSubTypeAnswer("Form");
				coordinatesResponseDto.setFormField(coordinatesResponseDto.getQuestion());
				coordinatesResponseDto.setSubType("Form");
				coordinatesResponseDto.setIcon("default");
				formFieldsSubTypeService.save(fieldsSubType);
				}
				}
				*/
				 
			}//form fields if close
			}//question coordinates if close
		if(coordinatesDto.getAnswerCoordinates()!=null && !coordinatesDto.getAnswerCoordinates().equals(""))
		{
			coordinatesResponseDto.setContentType(coordinatesDto.getContentType());
			coordinatesResponseDto.setPossibleAnswer(pdfReaderNewTemplateUtil.parsePdfFile(byteArrayFile,coordinatesDto,true));
			if(coordinatesResponseDto.getPossibleAnswer()!=null && !coordinatesResponseDto.getPossibleAnswer().equals(""))
			{
				String answerPossible = coordinatesResponseDto.getPossibleAnswer();
				answerPossible = answerPossible.substring(0, answerPossible.length()-1);
				coordinatesResponseDto.setPossibleAnswer(answerPossible.trim());
				
			}
			 org.json.simple.JSONObject jsonObject = getAnswerPositions(byteArrayFile,coordinatesDto);
			 if(!jsonObject.isEmpty())
			 {
			 if(jsonObject.get("answer").toString()!=null)
				 coordinatesResponseDto.setAnswer(jsonObject.get("answer").toString());
			 if(jsonObject.get("type").toString()!=null)
				 coordinatesResponseDto.setAnswerType(jsonObject.get("type").toString());
			if(	coordinatesResponseDto.getAnswerType()==null || coordinatesResponseDto.getAnswerType().equals(""))
				coordinatesResponseDto.setAnswerType("CHECKBOX");
			 }
			 
			coordinatesResponseDto.setAnswerCoordinates(coordinatesDto.getAnswerCoordinates());
			coordinatesResponseDto.setPageNumber(coordinatesDto.getPageNumber());
			coordinatesResponseDto.setAnswerPositionOnThePage(coordinatesDto.getAnswerPositionOnThePage());
			coordinatesResponseDto.setAnswerUUID(coordinatesDto.getAnswerUUID());
		}
		
		if(coordinatesResponseDto!=null)
		{
			String trimAnswer = coordinatesResponseDto.getPossibleAnswer();
			if(trimAnswer!=null && trimAnswer!="")
			{/*
				if(coordinatesResponseDto.getAnswerType().toString().equals("CHECKBOX") || coordinatesResponseDto.getAnswerType().toString().equals("YES_NO_CHECKBOX"))
				{
				if(trimAnswer.contains("Yes") || trimAnswer.contains("yes"))
				{
					coordinatesResponseDto.setPossibleAnswer("Yes");
				}
				else if(trimAnswer.contains("No") || trimAnswer.contains("no"))
				{
					coordinatesResponseDto.setPossibleAnswer("No");
				}
				}
						*/}
		}
	}//for fillable pdf
	else if(documentVault.getDocStructureType()==3 || documentVault.getDocStructureType()==2)//for scanned pdf
	{
	        //below is the different part
		File[] val = null;
		try
		{
	        File someFile = new File(TEMPLATE_FILE_LOCATION+"/scanned.pdf");
	        FileOutputStream fos = new FileOutputStream(someFile);
	        fos.write(byteArrayFile);
	        fos.flush();
	        fos.close();
	        if(documentVault.getDocStructureType()==3 || documentVault.getDocStructureType()==2)
	        	val = PdfUtilities.convertPdf2Png(someFile);
			List<String> items = null;
			if(coordinatesDto.getQuestionCoordinates()!=null && !coordinatesDto.getQuestionCoordinates().equals(""))
			     items = Arrays.asList(coordinatesDto.getQuestionCoordinates().split("\\s*,\\s*"));
			if(coordinatesDto.getAnswerCoordinates()!=null && !coordinatesDto.getAnswerCoordinates().equals(""))
				items = Arrays.asList(coordinatesDto.getAnswerCoordinates().split("\\s*,\\s*"));
			 	
				String text ="";
				if(documentVault.getDocStructureType()==3)
				{
					Integer a = (Integer.parseInt(items.get(0))*300)/72;
					Integer b = (Integer.parseInt(items.get(1))*300)/72;
					Integer c = (Integer.parseInt(items.get(2))*300)/72;
					Integer d = (Integer.parseInt(items.get(3))*300)/72;
					 Rectangle rectangle = new Rectangle(a,b,c,d); // doc parser coord
					ITesseract tesseract = new Tesseract();
					tesseract.setDatapath(TESSRACT_LANGUAGE);
					text = tesseract.doOCR(val[coordinatesDto.getPageNumber()-1], rectangle);
				}
				
				else if(documentVault.getDocStructureType()==2)
				  { 
					  PDDocument document =  PDDocument.load(byteArrayFile); 
					  PDFTextStripperByArea textStripper = new	  PDFTextStripperByArea();
					  Rectangle2D rect = new	  java.awt.geom.Rectangle2D.Float(Float.valueOf(items.get(0)),Float.valueOf(
				  items.get(1)),Float.valueOf(items.get(2)),Float.valueOf(items.get(3)));
				  textStripper.addRegion("region", rect);
				  PDPage docPage =  document.getPage(coordinatesDto.getPageNumber()-1);
				  textStripper.extractRegions(docPage); 
				  text =  textStripper.getTextForRegion("region");
				  }
				 
				if(coordinatesDto.getQuestionCoordinates()!=null && !coordinatesDto.getQuestionCoordinates().equals(""))
				{
					String replaceExtra1 = text.trim().replaceAll("_"," ");
					coordinatesResponseDto.setQuestion(replaceExtra1);
					coordinatesResponseDto.setQuestionCoordinates(coordinatesDto.getQuestionCoordinates());
					coordinatesResponseDto.setQuestionPositionOnThePage(coordinatesDto.getQuestionPositionOnThePage());
					coordinatesResponseDto.setQuestionUUID(coordinatesDto.getQuestionUUID());
				}
				if(coordinatesDto.getAnswerCoordinates()!=null && !coordinatesDto.getAnswerCoordinates().equals(""))
				{
					coordinatesResponseDto.setAnswer(text);
					String trimAnswer = coordinatesResponseDto.getAnswer();
					if(trimAnswer.contains("Yes") || trimAnswer.contains("yes"))
					{
						coordinatesResponseDto.setAnswer("Yes");
						coordinatesResponseDto.setAnswerType(AnswerType.CHECKBOX.toString());
					}
					else if((trimAnswer.contains("No") || trimAnswer.contains("no")) && trimAnswer.trim().length()<4)
					{
						coordinatesResponseDto.setAnswer("No");
						coordinatesResponseDto.setAnswerType(AnswerType.CHECKBOX.toString());
					}
					else
						coordinatesResponseDto.setAnswerType(AnswerType.TEXT.toString());
					coordinatesResponseDto.setAnswerCoordinates(coordinatesDto.getAnswerCoordinates());
					coordinatesResponseDto.setAnswerPositionOnThePage(coordinatesDto.getAnswerPositionOnThePage());
					coordinatesResponseDto.setAnswerUUID(coordinatesDto.getAnswerUUID());
				}
			coordinatesResponseDto.setPageNumber(coordinatesDto.getPageNumber());
			if(coordinatesDto.getElementName()!=null)
				coordinatesResponseDto.setElementName(coordinatesDto.getElementName());
			coordinatesResponseDto.setContentType(coordinatesDto.getContentType());
			//form field checking
			 if(coordinatesResponseDto.getAnswer()!=null)
			 {
				 if(coordinatesResponseDto.getAnswer().trim().length()>0)
				 {
					String urlPattern = "^(http(s{0,1})://|www)[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
					if(coordinatesResponseDto.getAnswer().matches(urlPattern))
						coordinatesResponseDto.setFormField("website");
					String emailPattern = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
					if(coordinatesResponseDto.getAnswer().matches(emailPattern))
						coordinatesResponseDto.setFormField("email");
					if(coordinatesResponseDto.getFormField()==null)
						 coordinatesResponseDto.setFormField("NONE");
			 }
			 }
		}
		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		finally {
			File old = new File(TEMPLATE_FILE_LOCATION+"/scanned.pdf");
			if(old!=null)
				old.delete();
			if(val!=null)
			{
			for(File file : val){
				file.delete();
			}
			}
		}
	}
	
		return coordinatesResponseDto;
	}
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	public  org.json.simple.JSONObject getAnswerPositions(byte[] byteArrayFile , CoordinatesResponseDto coordinatesResponseDto) throws InvalidPasswordException, IOException
    {
		 org.json.simple.JSONObject jsonObjectReturn = new  org.json.simple.JSONObject();
    	List< org.json.simple.JSONObject> jsonObjectsList = new ArrayList< org.json.simple.JSONObject>();
    	 //gettinng answers for respective page.
    	PDDocument document  = PDDocument.load(byteArrayFile);
        PDPage page = document.getPage(0);
        PDRectangle pdRectangle = page.getCropBox();
		//System.out.println(pdRectangle.getLowerLeftX()+" "+pdRectangle.getUpperRightY()+"&&&&&&&&&&&&");
		PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
		if (acroForm != null)
		{
			for (PDField form : acroForm.getFieldTree())
			{
				for (PDAnnotationWidget widget : form.getWidgets())
		        {
					COSDictionary widgetObject = widget.getCOSObject();
				    PDPageTree pages = document.getPages();
				    for (int i = 0; i < pages.getCount(); i++)
				    {
				        for (PDAnnotation annotation : pages.get(i).getAnnotations())
				        {
				            COSDictionary annotationObject = annotation.getCOSObject();
				            if (annotationObject.equals(widgetObject))
				            {
				            	 COSDictionary field = (COSDictionary) form.getCOSObject();
								 COSArray rectArray= (COSArray)field.getDictionaryObject("Rect");
								if(rectArray!=null)
								{
									COSBase base= rectArray.get(0);
								 PDRectangle mediaBox = new PDRectangle( rectArray ); 
								 //System.out.println(mediaBox.getHeight() +" "+ mediaBox.getWidth());
								 double upperRightY = pdRectangle.getHeight()-(mediaBox.getUpperRightY());
								 org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
									jsonObject.put("answer", form.getValueAsString());
									jsonObject.put("answerCode", form.getFullyQualifiedName());
									jsonObject.put("lowerleftX", mediaBox.getLowerLeftX());
									jsonObject.put("upperRightY", upperRightY);
									jsonObject.put("pageNumber", i+1);
									 String type = null;
										if (form instanceof PDCheckBox) {
											jsonObject.put("type", "CHECKBOX");
										} else if (form instanceof PDTextField) {
											jsonObject.put("type", "TEXT");
										}
										else if(form instanceof PDRadioButton)
										{
											jsonObject.put("type", "YES_NO_CHECKBOX");
										}
								//answersmap.put(val1, form.getFullyQualifiedName()+" "+form.getValueAsString() + " "+i+1);
								jsonObjectsList.add(jsonObject);
								//System.out.println("----------------------------");
								}//if close
								else
								{
									org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
									 double upperRightY = pdRectangle.getHeight()-(widget.getRectangle().getUpperRightY());
										jsonObject.put("answer", form.getValueAsString());
										jsonObject.put("lowerleftX", widget.getRectangle().getLowerLeftX());
										jsonObject.put("upperRightY", upperRightY);
										jsonObject.put("pageNumber", i+1);
										if(form instanceof PDTextField){
											jsonObject.put("type", "TEXT");
								        }else{
								        	jsonObject.put("type", "YES_NO_CHECKBOX");
								        }
										//jsonObject.put("type", "YES/NO CHECKBOX");
										jsonObjectsList.add(jsonObject);
								}
				            }
				        }
				    }//page close
		        }
			}//pdfield close for
		
		}//acroform if close
		//getAnswerPositions1(i);
		out:for( org.json.simple.JSONObject entry : jsonObjectsList)
		{
			int pageNumber = (int) entry.get("pageNumber");
			if(pageNumber==coordinatesResponseDto.getPageNumber())
			{
			//System.out.println(entry.get("answer") + " "+entry.get("lowerleftX") +" "+entry.get("upperRightY")+" "+entry.get("type"));
			double x = Double.parseDouble(entry.get("lowerleftX").toString());
			double y = (double) entry.get("upperRightY");
			List<String> items = Arrays.asList(coordinatesResponseDto.getAnswerCoordinates().split("\\s*,\\s*"));
			if(Double.valueOf(items.get(0))<=x && Double.valueOf(items.get(1))<=y)//change to x and y values
			{
				jsonObjectReturn.put("answer", entry.get("answer"));
				jsonObjectReturn.put("type", entry.get("type"));
				System.out.println(entry.get("answer") + " "+entry.get("lowerleftX") +" "+entry.get("upperRightY")+" "+entry.get("type") +" "+pageNumber);
				break out;
			}
			}
		}
    	return jsonObjectReturn;
    	}





	@Override
	@Transactional("transactionManager")
	public List<TemplatesRepsonseDto> getTemplatesPagination(Integer recordsPerPage, Integer pageNumber,boolean isPagination,String searchKey)
	{
		List<TemplatesRepsonseDto> repsonseDtosList = new ArrayList<TemplatesRepsonseDto>();
		List<DocumentTemplates> documentTemplatesList = documentTemplatesDao.getDocumentTemplatesList(recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,isPagination,searchKey);
		for (DocumentTemplates documentTemplates : documentTemplatesList)
		{
			TemplatesRepsonseDto templatesRepsonseDto = new TemplatesRepsonseDto();
			BeanUtils.copyProperties(documentTemplates, templatesRepsonseDto);
			//find no of docs parsed
			List<DocumentTemplateMapping> listTemplateMapings =  documentTemplateMappingDao.getDocumentTemplateMappingByTemplateId(documentTemplates.getId());
			if(!listTemplateMapings.isEmpty())
			{
				templatesRepsonseDto.setDocsParsed(listTemplateMapings.size());
				templatesRepsonseDto.setInitialDocId(listTemplateMapings.get(0).getDocumentId());
			}
			//no of elements
			templatesRepsonseDto.setNoOfElements(documentQuestionsDao.getElements(documentTemplates.getId()));
			//created by and Updated By
			User userObjectCreatedBy = userDao.find(documentTemplates.getCreatedBy());
			if(userObjectCreatedBy!=null)
			{
				templatesRepsonseDto.setCreatedById(userObjectCreatedBy.getUserId());
				templatesRepsonseDto.setCreatedBy(userObjectCreatedBy.getFirstName()!=null && !userObjectCreatedBy.getFirstName().equals("") ? userObjectCreatedBy.getFirstName() : "" );
			}
				User userObjectUpdated = userDao.find(documentTemplates.getCreatedBy());
			if(userObjectUpdated!=null)
			{
				templatesRepsonseDto.setModifiedById(userObjectUpdated.getUserId());
				templatesRepsonseDto.setModifiedBy(userObjectUpdated.getFirstName()!=null && !userObjectUpdated.getFirstName().equals("") ? userObjectUpdated.getFirstName() : "" );
			}
				repsonseDtosList.add(templatesRepsonseDto);
		}
		return repsonseDtosList;
	}


	@Override
	@Transactional("transactionManager")
	public long getTemplates(boolean isPagination,String searchKey) {
		List<DocumentTemplates> documentTemplatesList = documentTemplatesDao.getDocumentTemplatesList(null,null,isPagination,searchKey);
		return documentTemplatesList.size();
	}

	@Override
	@Transactional("transactionManager")
	public boolean updateTemplateName(TemplatesRepsonseDto repsonseDto,Long userId) 
	{
		DocumentTemplates documentTemplates = documentTemplatesDao.find(repsonseDto.getId());
		String oldTemplateName = null;
		if(documentTemplates!=null)
		{
			oldTemplateName = documentTemplates.getTemplateName();
			documentTemplates.setTemplateName(repsonseDto.getTemplateName());
			documentTemplates.setModifiedBy(userId);
		}
		if(oldTemplateName != null)
			eventPublisher.publishEvent(new LoggingEvent(documentTemplates.getId(), LoggingEventType.UPDATE_TEMPLATE_NAME, userId, new Date(), oldTemplateName,documentTemplates.getTemplateName()));
		
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public boolean updateTemplateQuestions(OverAllResponseDto repsonseDto, Long tempId, Long currentUserId) throws Exception 
	{
		DocumentTemplates documentTemplates = documentTemplatesDao.find(tempId);
		if(documentTemplates!=null)
		{
			List<DocumentContentDto> documentContentDtos = repsonseDto.getDocumentContentDto();
			//List<DocumentQuestions> questionList = documentQuestionsDao.getDocumentQuestionsByTemplateId(documentTemplates.getId(), true)
			if(!documentContentDtos.isEmpty())
			{			
				
				List<DocumentQuestions> documentQuestionsList = documentQuestionsDao.getDocumentQuestionsByTemplateId(documentTemplates.getId(),false);
				for (DocumentQuestions documentQuestions1 : documentQuestionsList) {
					if (!documentQuestions1.isOSINT()) {
						DocumentContentDto documentContent1 = documentContentDtos.stream().filter(content -> content
								.getContentDataDto().getQuestionId() != null).filter(content -> content
								.getContentDataDto().getQuestionId().equals(documentQuestions1.getId()) ).findAny()
								.orElse(null);
						if (documentContent1 == null) {
							documentQuestions1.setDeleteStatus(true);
							documentsQuestionsService.saveOrUpdate(documentQuestions1);
							eventPublisher.publishEvent(new LoggingEvent(documentQuestions1.getId(), LoggingEventType.DELETE_QUESTION_TO_TEMPLATE, currentUserId, new Date(), null));
							eventPublisher.publishEvent(new LoggingEvent(documentQuestions1.getTemplateId(), LoggingEventType.UPDATE_TEMPLATE, currentUserId, new Date(), null));
						}
					}
					
				}
				
				for (DocumentContentDto documentContent : documentContentDtos)
				{
				ContentDataDto contentData = documentContent.getContentDataDto();				
				//updating existing thing
				if(contentData.getQuestionId()!=null)
				{
					DocumentQuestions documentQuestion = documentQuestionsDao.find(contentData.getQuestionId());
				/*	if(contentData.getQuestionStatus()==false || contentData.getQuestionStatus()==null)
					{*/
					if(documentQuestion!=null && !documentQuestion.isOSINT())
					{
						documentQuestion.setName(contentData.getQuestion());
						documentQuestion.setElementName(contentData.getElementName());
						documentQuestion.setFormField(contentData.getFormField());
						documentQuestion.setQuestionPosition(contentData.getQuestionPosition());
						if(contentData.getQuestionPositionOnThePage()!=null)
							documentQuestion.setQuestionPositionOnThePage(contentData.getQuestionPositionOnThePage());
						if(contentData.getQuestionUUID()!=null)
							documentQuestion.setQuestionUUID(contentData.getQuestionUUID());
						if(contentData.getAnswerPosition()!=null)
							documentQuestion.setAnswerPosition(contentData.getAnswerPosition());
						if(documentQuestion.getAnswerType()!=null)
						{
							documentQuestion.setAnswerType(contentData.getAnswerType());
						if(documentQuestion.getAnswerType().toString().equals("TEXT"))
						{
							documentQuestion.setAnswerAtPosition(contentData.getAnswerPosition());
							documentQuestion.setAnswerPositionOnThePage(contentData.getAnswerPositionOnThePage());
							documentQuestion.setAnswerUUID(contentData.getAnswerUUID());
						}
						else
						{
						List<PossibleAnswersDto> possibleAnswersDtoList  = new ArrayList<PossibleAnswersDto>();
						if(contentData.getPossibleAnswersDto()!=null)
							possibleAnswersDtoList = contentData.getPossibleAnswersDto();
						else
							possibleAnswersDtoList =contentData.getSpecifiedPossibleAnswersDto();
						List<PossibleAnswers> possibleAnswers =  possibleAnswersService.fetchPossibleAnswers(contentData.getQuestionId());
						if(!possibleAnswersDtoList.isEmpty())
						{
										for (PossibleAnswers possibleAnswers2 : possibleAnswers) {
											PossibleAnswersDto possibleAnswerDto1 = possibleAnswersDtoList.stream().filter(possibleAns -> possibleAns.getId() != null)
													.filter(possibleAns -> possibleAns.getId()
															.equals(possibleAnswers2.getId()))
													.findAny().orElse(null);
											if (possibleAnswerDto1 == null) {
												possibleAnswers2.setDeleted(true);
												possibleAnswersService.saveOrUpdate(possibleAnswers2);
												eventPublisher.publishEvent(new LoggingEvent(possibleAnswers2.getId(), LoggingEventType.DELETE_POSSIBLE_ANSWER, currentUserId, new Date(), null));
												eventPublisher.publishEvent(new LoggingEvent(possibleAnswers2.getDocumentQuestions().getTemplateId(), LoggingEventType.UPDATE_TEMPLATE, currentUserId, new Date(), null));
											}
										}
							
							for (PossibleAnswersDto possibleAnswerToSave : possibleAnswersDtoList) 
							{
								if(possibleAnswerToSave.getId()!=null)
								{
									for (PossibleAnswers possibleAnswerFromDB : possibleAnswers)
									{
										if(possibleAnswerToSave.getId()==possibleAnswerFromDB.getId())
										{
											/*if(possibleAnswerToSave.getDeleted()==false || possibleAnswerToSave.getDeleted()==null)
											{*/
											possibleAnswerFromDB.setAnswerPosition(possibleAnswerToSave.getAnswerPosition());
											possibleAnswerFromDB.setAnswerPositionOnThePage(possibleAnswerToSave.getAnswerPositionOnThePage());
											possibleAnswerFromDB.setAnswerUUID(possibleAnswerToSave.getAnswerUUID());
											possibleAnswerFromDB.setPossibleAnswer(possibleAnswerToSave.getPossibleAnswer());
											/*}
											else
											
											{
												possibleAnswerFromDB.setDeleted(true);
											}*/
										}
									}
								}
								//for new possible answer
								else
								{
									PossibleAnswers answers = new PossibleAnswers();
									answers.setAnswerPosition(possibleAnswerToSave.getAnswerPosition());
									answers.setDocumentQuestions(documentQuestion);
									answers.setPossibleAnswer(possibleAnswerToSave.getPossibleAnswer());
									answers.setAnswerPositionOnThePage(possibleAnswerToSave.getAnswerPositionOnThePage());
									answers.setAnswerUUID(possibleAnswerToSave.getAnswerUUID());
									possibleAnswers.add(answers);
									possibleAnswersService.save(answers);
									eventPublisher.publishEvent(new LoggingEvent(answers.getId(), LoggingEventType.ADD_POSSIBLE_ANSWER, currentUserId, new Date(), null));
									eventPublisher.publishEvent(new LoggingEvent(answers.getDocumentQuestions().getTemplateId(), LoggingEventType.UPDATE_TEMPLATE, currentUserId, new Date(), null));
								}
								
							}
						}//checkbox answers update close
						}//else close
					}
					}
					/*}					
					else
					{
						documentQuestion.setDeleteStatus(true);
					}*/
					documentsQuestionsService.saveOrUpdate(documentQuestion);
				}//old question update end
				//for new Question
				else
				{
					int i=0,j=0,k=0,l=0;
					long parentId = 0;
					DocumentQuestions documentQuestion=new DocumentQuestions();
					documentQuestion.setTemplateId(tempId);
					documentQuestion.setContentType(documentContent.getContentType());
					documentQuestion.setElementName(contentData.getElementName());
					documentQuestion.setPageNumber(contentData.getPageNumber());
					documentQuestion.setFormField(contentData.getFormField());
					documentQuestion.setSubType(contentData.getSubType());
					if(documentContent.getContentType().equals("Question")  || documentContent.getContentType().equals("Form Fields"))
						documentQuestion.setQuestionStatus(true);
					documentQuestion.setName(contentData.getQuestion());
					if(contentData.getQuestionPosition()!=null)
						documentQuestion.setQuestionPosition(contentData.getQuestionPosition());
					if(contentData.getQuestionPositionOnThePage()!=null)
						documentQuestion.setQuestionPositionOnThePage(contentData.getQuestionPositionOnThePage());
					if(contentData.getQuestionUUID()!=null)
						documentQuestion.setQuestionUUID(contentData.getQuestionUUID());
					documentQuestion.setAnswerType(contentData.getAnswerType());
					if(contentData.getAnswerType()!=null)
					{
					if(contentData.getAnswerType().toString().equals("CHECKBOX") || contentData.getAnswerType().toString().equals("YES_NO_CHECKBOX"))
					{
						List<PossibleAnswersDto> answersDtos = contentData.getPossibleAnswersDto();
						List<PossibleAnswers> possibleAnswersList = new ArrayList<PossibleAnswers>();
						for (PossibleAnswersDto possibleAnswer : answersDtos) 
						{
							PossibleAnswers answers = new PossibleAnswers();
							answers.setAnswerPosition(possibleAnswer.getAnswerPosition());
							answers.setDocumentQuestions(documentQuestion);
							answers.setPossibleAnswer(possibleAnswer.getPossibleAnswer());
							answers.setAnswerPositionOnThePage(possibleAnswer.getAnswerPositionOnThePage());
							answers.setAnswerUUID(possibleAnswer.getAnswerUUID());
							possibleAnswersList.add(answers);
						}
						documentQuestion.setPossibleAnswers(possibleAnswersList);
					}
					}
					if(contentData.getAnswerPosition()!=null)
						documentQuestion.setAnswerPosition(contentData.getAnswerPosition());
					if(contentData.getAnswerPositionOnThePage()!=null)
						documentQuestion.setAnswerPositionOnThePage(contentData.getAnswerPositionOnThePage());
					if(contentData.getAnswerUUID()!=null)
						documentQuestion.setAnswerUUID(contentData.getAnswerUUID());
					documentQuestion.setShowInUI(true);
					String response = getIsoCodesJson(documentQuestion.getName());
					if (response != null) 
					{
						JSONObject jsonResponseObjectNew = new JSONObject(response);
						JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
						if (arrayObjectNew.length() > 0) {
							JSONObject normalObject = arrayObjectNew.getJSONObject(0);
							JSONObject level3Object = normalObject.getJSONObject("controls").getJSONObject("level3");
							documentQuestion.setPrimaryIsoCode(level3Object.getString("code"));
							documentQuestion.setHasIsoCode(true);
							documentQuestion.setLevel3Title(level3Object.getString("title"));
							documentQuestion.setLevel3Description(level3Object.getString("title"));
							JSONObject level1Object = normalObject.getJSONObject("controls").getJSONObject("level1");
							documentQuestion.setLevel1Code(level1Object.getString("code"));
							documentQuestion.setLevel1Title(level1Object.getString("title"));
							documentQuestion.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
							documentQuestion.setTopic(normalObject.getJSONObject("controls").getString("topic"));
							
						} 
					}
                    if(documentQuestion.getPrimaryIsoCode() == null)
                         documentQuestion.setHasIsoCode(false);
                    /*String[] answerCode = entryJson.getValue().split("&");
                    documentQuestion.setPdfAnswerCode(answerCode[0]);
                    
                    if(answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
                    {
                         documentQuestion.setAnswerType(AnswerType.CHECKBOX);
                         documentQuestion.setPossibleAnswer("YES/NO");
                    }
                    if(answerCode[1].trim().equalsIgnoreCase("TEXT"))
                         documentQuestion.setAnswerType(AnswerType.TEXT);
                    if(!answerCode[1].trim().equalsIgnoreCase("TEXT") && !answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
                    {
                         documentQuestion.setAnswerType(AnswerType.CHECKBOX);
                         documentQuestion.setPossibleAnswer("YES/NO");
                    }
                    */
                    if(documentQuestion.getName().contains("Name") || documentQuestion.getName().contains("name")){
                         i++;
                         if(i==1){
                              documentQuestion.setCaseField("name");
                              documentQuestion.setRequiredForOSINT(true);
                         }
                    }
                    if(documentQuestion.getName().equalsIgnoreCase("address")){
                         j++;
                         if(j==1){
                              documentQuestion.setCaseField("address");
                              documentQuestion.setRequiredForOSINT(true);
                         }
                    }
                    if(documentQuestion.getName().equalsIgnoreCase("email")){
                         l++;
                         if(l==1){
                              documentQuestion.setCaseField("email");
                              documentQuestion.setRequiredForOSINT(true);
                         }
                    }
                    if(documentQuestion.getName().contains("website") || documentQuestion.getName().contains("Website")){
                         k++;
                         if(k==1){
                              documentQuestion.setCaseField("website");
                              documentQuestion.setRequiredForOSINT(true);
                         }
                    }
					DocumentQuestions question = documentQuestionsDao.create(documentQuestion);
					if(contentData.getSpecifiedQuestion()!=null && !contentData.getSpecifiedQuestion().equals(""))
					{
						parentId = question.getId();

						DocumentQuestions documentQuestionNew=new DocumentQuestions();
						documentQuestionNew.setTemplateId(tempId);
						documentQuestionNew.setContentType(documentContent.getContentType());
						documentQuestionNew.setPageNumber(contentData.getPageNumber());
						if(documentContent.getContentType().equals("Question") || documentContent.getContentType().equals("Form Fields"))
							documentQuestionNew.setQuestionStatus(true);
						documentQuestionNew.setName(contentData.getSpecifiedQuestion());
						if(contentData.getSpecifiedQuestionPosition()!=null)
							documentQuestionNew.setQuestionPosition(contentData.getSpecifiedQuestionPosition());
						documentQuestionNew.setAnswerType(contentData.getSpecifiedAnswerType());
						documentQuestionNew.setFormField(contentData.getFormField());
						documentQuestionNew.setSubType(contentData.getSubType());
						if(contentData.getSpecifiedAnswerType().toString().equals("CHECKBOX")  || contentData.getSpecifiedAnswerType().toString().equals("YES_NO_CHECKBOX"))
						{
							List<PossibleAnswersDto> answersDtos = contentData.getPossibleAnswersDto();
							List<PossibleAnswers> possibleAnswersList = new ArrayList<PossibleAnswers>();
							for (PossibleAnswersDto possibleAnswer : answersDtos) 
							{
								PossibleAnswers answers = new PossibleAnswers();
								answers.setAnswerPosition(possibleAnswer.getAnswerPosition());
								answers.setDocumentQuestions(documentQuestionNew);
								answers.setPossibleAnswer(possibleAnswer.getPossibleAnswer());
								possibleAnswersList.add(answers);
							}
							documentQuestion.setPossibleAnswers(possibleAnswersList);
						}
						if(contentData.getAnswerPosition()!=null)
							documentQuestionNew.setAnswerPosition(contentData.getAnswerPosition());
						documentQuestion.setShowInUI(true);
						String response1 = getIsoCodesJson(documentQuestionNew.getName());
						if (response1 != null) 
						{
							JSONObject jsonResponseObjectNew = new JSONObject(response);
							JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
							if (arrayObjectNew.length() > 0) {
								JSONObject normalObject = arrayObjectNew.getJSONObject(0);
								JSONObject level3Object = normalObject.getJSONObject("controls").getJSONObject("level3");
								documentQuestion.setPrimaryIsoCode(level3Object.getString("code"));
								documentQuestion.setHasIsoCode(true);
								documentQuestion.setLevel3Title(level3Object.getString("title"));
								documentQuestion.setLevel3Description(level3Object.getString("title"));
								JSONObject level1Object = normalObject.getJSONObject("controls").getJSONObject("level1");
								documentQuestion.setLevel1Code(level1Object.getString("code"));
								documentQuestion.setLevel1Title(level1Object.getString("title"));
								documentQuestion.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
								documentQuestion.setTopic(normalObject.getJSONObject("controls").getString("topic"));
								
							} 
						}
						if(documentQuestionNew.getPrimaryIsoCode() == null)
							documentQuestionNew.setHasIsoCode(false);
						/*String[] answerCode = entryJson.getValue().split("&");
						documentQuestion.setPdfAnswerCode(answerCode[0]);
						
						if(answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
						{
							documentQuestion.setAnswerType(AnswerType.CHECKBOX);
							documentQuestion.setPossibleAnswer("YES/NO");
						}
						if(answerCode[1].trim().equalsIgnoreCase("TEXT"))
							documentQuestion.setAnswerType(AnswerType.TEXT);
						if(!answerCode[1].trim().equalsIgnoreCase("TEXT") && !answerCode[1].trim().equalsIgnoreCase("CHECKBOX"))
						{
							documentQuestion.setAnswerType(AnswerType.CHECKBOX);
							documentQuestion.setPossibleAnswer("YES/NO");
						}
						*/
						if(documentQuestionNew.getName().contains("Name") || documentQuestionNew.getName().contains("name")){
							i++;
							if(i==1){
								documentQuestionNew.setCaseField("name");
								documentQuestionNew.setRequiredForOSINT(true);
							}
						}
						if(documentQuestionNew.getName().equalsIgnoreCase("address")){
							j++;
							if(j==1){
								documentQuestionNew.setCaseField("address");
								documentQuestionNew.setRequiredForOSINT(true);
							}
						}
						if(documentQuestionNew.getName().equalsIgnoreCase("email")){
							l++;
							if(l==1){
								documentQuestionNew.setCaseField("email");
								documentQuestionNew.setRequiredForOSINT(true);
							}
						}
						if(documentQuestionNew.getName().contains("website") || documentQuestionNew.getName().contains("Website")){
							k++;
							if(k==1){
								documentQuestionNew.setCaseField("website");
								documentQuestionNew.setRequiredForOSINT(true);
							}
						}
						documentQuestionNew.setHasParentId(parentId);
						DocumentQuestions questionSave = documentQuestionsDao.create(documentQuestionNew);
						eventPublisher.publishEvent(new LoggingEvent(questionSave.getId(), LoggingEventType.ADD_NEW_QUESTION_TO_TEMPLATE, currentUserId, new Date(), null));
						eventPublisher.publishEvent(new LoggingEvent(questionSave.getTemplateId(), LoggingEventType.UPDATE_TEMPLATE, currentUserId, new Date(), null));
						parentId=0;
					}//parentId close
				
				}
				}//for close
			}
		}//templates if close
		else
		{
			throw new Exception("Template Not Found");
		}
		
		return true;
	}





	@Override
	@Transactional("transactionManager")
	public boolean deleteTemplate(Long templateId,Long userId) {
		DocumentTemplates documentTemplates = documentTemplatesDao.find(templateId);
		documentTemplates.setDeletedStatus(true);
		documentTemplatesDao.saveOrUpdate(documentTemplates);
		eventPublisher.publishEvent(new LoggingEvent(documentTemplates.getId(), LoggingEventType.DELETE_TEMPLATE, userId, new Date(), null));
		return true;
	}





	
	/*@Override
	@Transactional("transactionManager")
	public String saveTemplateFile(DocumentVault documentVault,Long userIdTemp, HttpServletResponse response) throws DocNotFoundException, Exception {
		File file = null;
		String filepath = null;
		if (documentVault != null) {
			if (documentVault.getDocFlag().equals(11)) {
				byte[] fileData = documentService.downloadDocument(documentVault.getDocId(), userIdTemp, response);
				String pathToFile =  TEMPLATE_FILE_LOCATION+ "/";
				String[] fileName = documentVault.getDocName().split("\\.");
				file = new File(pathToFile +fileName[0]+"_"+documentVault.getDocId()+"."+fileName[1] );				
				FileOutputStream fos = new FileOutputStream(file);
				if(fileData != null){
				fos.write(fileData);				
				filepath = file.getAbsolutePath();				 
				}
				fos.close();

			}
			else
				throw new BadRequestException("Document is not template");
		} else {
			throw new DocNotFoundException("Document not found");
		}
		return filepath;
	
		
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteTemplateFile(DocumentVault documentVault) throws DocNotFoundException, Exception {
		if(documentVault.getDocName() != null && documentVault.getDocName().contains("\\.")){
		String[] fileName = documentVault.getDocName().split("\\.");
		if(fileName.length == 2){
		String filePath = TEMPLATE_FILE_LOCATION+ "/"+fileName[0]+"_"+documentVault.getDocId()+fileName[1];
		File file = new File(filePath);
		boolean status = file.delete();		
		return status;
		}else
			throw new BadRequestException("Document name is not in correct format");
		
		}else
			throw new BadRequestException("Document name cann't be empty or not in correct format");		
		
	}*/
	
	

}
