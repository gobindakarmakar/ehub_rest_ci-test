package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Transactions by product types")
public class TxProductTypeDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Transaction product type")	
	private String transactionProductType;
	@ApiModelProperty(value="Total amount of transactions")
	private double amountTotal;
	@ApiModelProperty(value="Count of transaction product types")
	private long transactionProductTypeCount;
	

	public TxProductTypeDto() {
	}

	public TxProductTypeDto(String transactionProductType, double amountTotal, long transactionProductTypeCount) {
		super();
		this.transactionProductType = transactionProductType;
		this.amountTotal = amountTotal;
		this.transactionProductTypeCount = transactionProductTypeCount;
	}

	public String getTransactionProductType() {
		return transactionProductType;
	}

	public void setTransactionProductType(String transactionProductType) {
		this.transactionProductType = transactionProductType;
	}

	public double getAmountTotal() {
		return amountTotal;
	}

	public void setAmountTotal(double amountTotal) {
		this.amountTotal = amountTotal;
	}

	public long getTransactionProductTypeCount() {
		return transactionProductTypeCount;
	}

	public void setTransactionProductTypeCount(long transactionProductTypeCount) {
		this.transactionProductTypeCount = transactionProductTypeCount;
	}

}
