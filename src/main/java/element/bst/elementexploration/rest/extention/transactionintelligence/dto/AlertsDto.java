package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.util.Date;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;

public class AlertsDto {

	private String focalEntityDisplayName;
	
	private String focalEntityDisplayId;
	
	private String scenario;
	
	private Date alertBusinessDate;
	
	private Long userId;
	
	private String comment;
	
	private String alertType;
	
	private AlertStatus alertStatus;
	
	private String transactionProductType;
	
	private Long transactionId;
	
	private String alertDescription;
	
	private String alertScenarioType;
	
	private Integer riskScore;
	
	private Double maximumAmount;

	public AlertsDto(String focalEntityDisplayName, String focalEntityDisplayId, String scenario,
			Date alertBusinessDate, Long userId, String comment, String alertType, AlertStatus alertStatus,
			String transactionProductType, Long transactionId, String alertDescription, String alertScenarioType,
			Integer riskScore, Double maximumAmount) {
		super();
		this.focalEntityDisplayName = focalEntityDisplayName;
		this.focalEntityDisplayId = focalEntityDisplayId;
		this.scenario = scenario;
		this.alertBusinessDate = alertBusinessDate;
		this.userId = userId;
		this.comment = comment;
		this.alertType = alertType;
		this.alertStatus = alertStatus;
		this.transactionProductType = transactionProductType;
		this.transactionId = transactionId;
		this.alertDescription = alertDescription;
		this.alertScenarioType = alertScenarioType;
		this.riskScore = riskScore;
		this.maximumAmount = maximumAmount;
	}

	public String getFocalEntityDisplayName() {
		return focalEntityDisplayName;
	}

	public void setFocalEntityDisplayName(String focalEntityDisplayName) {
		this.focalEntityDisplayName = focalEntityDisplayName;
	}

	public String getFocalEntityDisplayId() {
		return focalEntityDisplayId;
	}

	public void setFocalEntityDisplayId(String focalEntityDisplayId) {
		this.focalEntityDisplayId = focalEntityDisplayId;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public AlertStatus getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(AlertStatus alertStatus) {
		this.alertStatus = alertStatus;
	}

	public String getTransactionProductType() {
		return transactionProductType;
	}

	public void setTransactionProductType(String transactionProductType) {
		this.transactionProductType = transactionProductType;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public String getAlertScenarioType() {
		return alertScenarioType;
	}

	public void setAlertScenarioType(String alertScenarioType) {
		this.alertScenarioType = alertScenarioType;
	}

	public Integer getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}

	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

}
