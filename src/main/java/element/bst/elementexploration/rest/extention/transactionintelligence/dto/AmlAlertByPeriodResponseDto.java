package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("AML alerts by period")
public class AmlAlertByPeriodResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Aml alerts by status")	
	private AlertByPeriodDto amlAlertByStatus;

	public AmlAlertByPeriodResponseDto() {
		super();
	}

	public AlertByPeriodDto getAmlAlertByStatus() {
		return amlAlertByStatus;
	}

	public void setAmlAlertByStatus(AlertByPeriodDto amlAlertByStatus) {
		this.amlAlertByStatus = amlAlertByStatus;
	}
	
	
}
