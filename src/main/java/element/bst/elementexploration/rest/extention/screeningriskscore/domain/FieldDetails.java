package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FieldDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private String fieldName;
    
    private Object value;
	
	private List<String> matches = new ArrayList<String>();
	
	

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}		

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public List<String> getMatches() {
		return matches;
	}

	public void setMatches(List<String> matches) {
		this.matches = matches;
	}

	

	

}
