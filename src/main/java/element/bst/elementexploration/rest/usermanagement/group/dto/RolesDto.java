package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@ApiModel("Role")
public class RolesDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Id of the role")
	private Long roleId;
	@NotBlank
	@ApiModelProperty(value = "Name of the role")
	private String roleName;

	@ApiModelProperty(value = "Description of the role")
	private String description;

	@ApiModelProperty(value = "Notes of the role")
	private String notes;
	
	@ApiModelProperty(value = "Icon for each role")
	private String icon;
	
	@ApiModelProperty(value = "Color for each role")
	private String color;
	
	@ApiModelProperty(value = "Active users count")
	private Long activeUsers;

	@ApiModelProperty(value = "Modified On")
	private Date modifiedOn;

	@ApiModelProperty(value = "Source of role creation")
	private String source;
	
	@ApiModelProperty(value = "Is Modifiable")
	private Boolean isModifiable=true;
	
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public RolesDto(String roleName, String description, String notes,Long roleId  ) {
		this.roleName = roleName;
		this.description = description;
		this.notes = notes;
		this.roleId=roleId;
	}

	public RolesDto() {
		// super();
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Long getActiveUsers() {
		return activeUsers;
	}

	public void setActiveUsers(Long activeUsers) {
		this.activeUsers = activeUsers;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getIsModifiable() {
		return isModifiable;
	}

	public void setIsModifiable(Boolean isModifiable) {
		this.isModifiable = isModifiable;
	}
	
	

}
