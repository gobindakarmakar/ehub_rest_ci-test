package element.bst.elementexploration.rest.extention.workflow.service;

public interface EntityResolveService {

	String resolve(String dir) throws Exception;

	String resolveWithThreshold(String dir, Double threshold) throws Exception;

	String resolveWithThresholdGenerateSchema(String dir, Double threshold, String generateSchema) throws Exception;

	String testService() throws Exception;

}
