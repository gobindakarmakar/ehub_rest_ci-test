package element.bst.elementexploration.rest.extention.alertmanagement.serviceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.activiti.app.service.exception.NotFoundException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedGroupsManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedGroupSevice;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedManagementService;
import element.bst.elementexploration.rest.extention.alertmanagement.dao.AlertManagementDao;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.AlertComments;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.ALertsCSVDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertCommentsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AssociatedAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.BatchScreeningResultDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.MyAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.PastDueAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.PastDueMonthCountDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.StatusCountDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.UserApprovedDto;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertCommentsService;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.dto.SettingsDto;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupsDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupAlertService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.AssigneeDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDtoWithRolesAndGroups;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.CsvToJsonUtil;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.EntityConverter;
import element.bst.elementexploration.rest.util.FilterModelDto;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * @author Prateek
 *
 */

@Service("alertManagementService")
@Transactional("transactionManager")
public class AlertManagementServiceImpl extends GenericServiceImpl<Alerts, Long> implements AlertManagementService {

	@Autowired
	public AlertManagementServiceImpl(GenericDao<Alerts, Long> genericDao) {
		super(genericDao);
	}

	public AlertManagementServiceImpl() {

	}

	@Autowired
	private AlertManagementService alertManagementService;

	@Autowired
	private AlertManagementDao alertManagementDao;

	@Autowired
	FeedManagementDao feedManagementDao;
	
	@Autowired
	FeedGroupsManagementDao feedGroupsManagementDao;

	@Autowired
	FeedManagementService feedManagementService;

	@Autowired
	private AuditLogDao auditLogDao;

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private GroupsDao groupsDao;

	@Autowired
	private ListItemService listItemService;

	@Autowired
	private DocumentService documentService;

	@Autowired
	private AlertCommentsService alertCommentsService;

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private SettingsService settingsService;
	
	@Autowired
	SourceJurisdictionService sourceJurisdictionService;

	@Autowired
	AuditLogService auditLogService;
	
	@Autowired
	UsersService usersService;
	
	@Autowired
	FeedGroupSevice feedGoupService;
	
	@Value("${alert_management_create_url}")
	private String AMS_CREATE_UPDATE_URL;
	

	@Value("${screening_alerts_url}")
	private String screeningAlertsUrl;
	
	@Value("${client_id}")
	private String CLIENT_ID;
	
	@Value("${bigdata_alerts_url}")
	private String BIGDATA_ALERTS_URL;
	
	@Value("${ssb_screening_data}")
	private String SSB_SCREENING_DATA_URL;
	
	@Value("${bigdata_multisource_url}")
	private String BIGDATA_MULTISOURCE_URL;
	
	@Autowired
	private GroupAlertService groupAlertService;
	
	/*@Override
	@Transactional("transactionManager")
	public String createAlerts(CreateAlertDto createDto) {
	//	Boolean created=null;
	
		StringBuilder file = new StringBuilder();
		String save="";
		String create="";
		Alerts created= new Alerts();
		try {
			//Alerts alertViewer = new Alerts();
			
			//List<Alerts> alertWithCustomerId= alertManagementDao.findAlertsByCustomerId(customeId);
			List<Alerts> alertWithCustomerId= new ArrayList<>();
			Map<String,List<String>> customerEntityMap= new HashMap<>(); //CustomerId and Entityname
			Map<String,List<Alerts>> entityNameALerts= new HashMap<>(); //CustomerId and Alerts
			List<String> customerIdList= new ArrayList<>();
			// commented are getting data from service url.
		//	String [] amsResponse=ServiceCallHelper.getDataFromServer(AMS_CREATE_UPDATE_URL);
		//	JSONObject amsJsonFromServer= new JSONObject(amsResponse[1]);
		//	if(amsResponse!=null && Integer.parseInt(amsResponse[0])==200) {
			//	amsJsonFromServer=new JSONObject(amsResponse[1]);
		//	}
			JSONObject amsJsonFromServer= new JSONObject();
			if(createDto!=null) {
				amsJsonFromServer=new JSONObject(createDto);
			}
			if(amsJsonFromServer.has("response")) {
				JSONArray response=amsJsonFromServer.getJSONArray("response");
				for (int i = 0; i < response.length(); i++) {
					Alerts alertViewer = new Alerts();
					JSONObject amsJson = response.getJSONObject(i);
					// Getting Data from DB
					if (amsJson.has("customerId")) {
						if(!customerIdList.contains(amsJson.getString("customerId"))) {
							customerIdList.add(amsJson.getString("customerId"));
							alertWithCustomerId= new ArrayList<>();
							alertWithCustomerId=alertManagementDao.findAlertsByCustomerId(amsJson.getString("customerId"));
							if (alertWithCustomerId != null && alertWithCustomerId.size()>0) {
								List<String> entityNameList = new ArrayList<>();
								for (Alerts alertName : alertWithCustomerId) {
									entityNameList.add(alertName.getEntityName());
								}
								customerEntityMap.put(amsJson.getString("customerId"), entityNameList);
								entityNameALerts.put(amsJson.getString("customerId"), alertWithCustomerId);
							}
						}
						alertViewer.setCustomerId(amsJson.getString("customerId"));
					}
					//Create and Update 
					if (amsJson.has("customerId") && amsJson.has("entityName") && amsJson.has("alertMetaData") && amsJson.getJSONArray("alertMetaData").length()>0 ) {
						if (customerEntityMap.containsKey(amsJson.getString("customerId"))) {
							List<String> checkEntityName = customerEntityMap.get(amsJson.getString("customerId"));
							if (checkEntityName.contains(amsJson.getString("entityName"))) {
								List<Alerts> alertListUpdate = entityNameALerts.get(amsJson.getString("customerId"));
								for (Alerts saveAlert : alertListUpdate) {
									if (amsJson.getString("entityName").equals(saveAlert.getEntityName())) {
										alertViewer = saveAlert;
										alertViewer.setCustomerId(amsJson.getString("customerId"));
										break;
									}
								}
								// Update
								save=saveAlerts(amsJson, alertViewer);
							} else {
								created=createAlerts(amsJson, alertViewer);
								if(created!=null) {
									create="Alert created ";
								}else {
									file.append("ERROR :Alert not created "+amsJson.getString("entityName"));
								}
							}
						} else {
							created=createAlerts(amsJson, alertViewer);
							if(created!=null) {
								create="Alert created ";
							}else {
								file.append("ERROR: Alert not created "+amsJson.getString("entityName"));
							}
						}
					}
				}
				if(!save.equals("")) {
					file.append(save);
				}
				if(!create.equals("")) {
					file.append(create);
				}
			}else {
				file.append("No data in JSON");
				throw new RuntimeException("Data not received");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return file.toString();
	}*/
	
	/*private String saveAlerts(JSONObject amsJson, Alerts alertViewer ) {
		String resp="";
		try {
			if (amsJson.has("entityName"))
				alertViewer.setEntityName(amsJson.getString("entityName"));
			if (amsJson.has("confidenceLevel"))
				alertViewer.setConfidenceLevel(amsJson.getDouble("confidenceLevel"));
			// gettting watchlist
		
			alertViewer.setCreatedDate(new Date());
			alertViewer.setUpdatedDate(new Date());
			
			ListItemDto FeedClassificationDto= new ListItemDto();
			ListItemDto FeedSourceDto = new ListItemDto();
			if(amsJson.has("feed_classification")) {
			
				 FeedClassificationDto = listItemService.getListItemByListTypeAndDisplayName(
						"Feed Classification", amsJson.getString("feed_classification"));
			}
			if(amsJson.has("feed_source")) {
			 FeedSourceDto = listItemService.getListItemByListTypeAndDisplayName("Feed Source",
					amsJson.getString("feed_source"));
			}
			
			// From List of type of Feed Classification and Feed Source and get
			// feedbyClassificationandSource

			FeedManagementDto feedManagementDto = new FeedManagementDto();
			FeedManagement feedManagement = new FeedManagement();
			feedManagementDto = feedManagementService.getFeedByTypeAndSource(
					FeedClassificationDto.getListItemId(), FeedSourceDto.getListItemId());
			// to set feedManagement
			// getFeedBytype&source(ListItemIdoftype,ListItemIdofSource)
			BeanUtils.copyProperties(feedManagementDto, feedManagement);
			if (feedManagement != null) {
				alertViewer.setFeed(feedManagement);
				List<FeedGroups> feedGroupsListDTO = feedManagement.getGroupLevels();
				FeedGroups maxRankGroup = Collections.min(feedGroupsListDTO,
						Comparator.comparing(s -> s.getRank()));
				alertViewer.setGroupLevel(maxRankGroup.getGroupId().getId().intValue());
			}
			alertViewer.setAssignee(0l);
			ListItemDto FeedStatusDto = listItemService.getListItemByListTypeAndDisplayName("Alert Status",
					"Open");
			alertViewer.setStatus(FeedStatusDto.getListItemId());
			
			alertViewer.setRiskIndicators(null);
			alertViewer.setComments(null);
			alertViewer.setPeriodicReview(true);
			if(amsJson.has("alertMetaData") && amsJson.get("alertMetaData")!=null && amsJson.getJSONArray("alertMetaData").length()>0) {
				if(amsJson.getJSONArray("alertMetaData").getJSONObject(0).getJSONArray("entries").getJSONObject(0).has("watchlist_id")) {
					JSONObject alertJson= amsJson.getJSONArray("alertMetaData").getJSONObject(0);
					JSONArray alertMetaDataArray= amsJson.getJSONArray("alertMetaData");
					for(int j=0;j<alertMetaDataArray.length();j++) {
						JSONObject entries=amsJson.getJSONArray("alertMetaData").getJSONObject(j).getJSONArray("entries").getJSONObject(0);
						entries.put("attribute_name", new JSONArray().put(new JSONObject().put("attribute_name",amsJson.getJSONArray("alertMetaData").getJSONObject(j).getJSONArray("entries").getJSONObject(0).getString("attribute_name"))));
					}
					alertViewer.setAlertMetaData(amsJson.getJSONArray("alertMetaData").toString());
					alertViewer.setWatchList(amsJson.getJSONArray("alertMetaData").getJSONObject(0).getJSONArray("entries").getJSONObject(0).getString("watchlist_id"));
					//createdAlert=alertManagementDao.create(alertViewer);
				}else {
					alertViewer.setAlertMetaData(amsJson.getJSONArray("alertMetaData").toString());
					alertViewer.setWatchList("");
				}
			}
			alertManagementDao.saveOrUpdate(alertViewer);
			resp="Alerts updated ";
		}
		catch(Exception e) {
			resp="ERROR:  Updating ALERT  "+alertViewer.getEntityName();
			e.printStackTrace();
		}
		return resp;
	}*/
	
	/*private Alerts createAlerts(JSONObject amsJson, Alerts alertViewer ) {
		Alerts created=null;
		try {

				if (amsJson.has("entityName"))
					alertViewer.setEntityName(amsJson.getString("entityName"));
				if (amsJson.has("confidenceLevel"))
					alertViewer.setConfidenceLevel(amsJson.getDouble("confidenceLevel"));
				
			
				alertViewer.setCreatedDate(new Date());
				alertViewer.setUpdatedDate(new Date());
				
				ListItemDto FeedClassificationDto= new ListItemDto();
				ListItemDto FeedSourceDto = new ListItemDto();
				if(amsJson.has("feed_classification")) {
					 FeedClassificationDto = listItemService.getListItemByListTypeAndDisplayName(
							"Feed Classification", amsJson.getString("feed_classification"));
				}
				if(amsJson.has("feed_source")) {
				 FeedSourceDto = listItemService.getListItemByListTypeAndDisplayName("Feed Source",
						amsJson.getString("feed_source"));
				}
				
				// From List of type of Feed Classification and Feed Source and get
				// feedbyClassificationandSource

				FeedManagementDto feedManagementDto = new FeedManagementDto();
				FeedManagement feedManagement = new FeedManagement();
				feedManagementDto = feedManagementService.getFeedByTypeAndSource(
						FeedClassificationDto.getListItemId(), FeedSourceDto.getListItemId());
				// to set feedManagement
				// getFeedBytype&source(ListItemIdoftype,ListItemIdofSource)
				BeanUtils.copyProperties(feedManagementDto, feedManagement);
				if (feedManagement != null) {
					alertViewer.setFeed(feedManagement);
					List<FeedGroups> feedGroupsListDTO = feedManagement.getGroupLevels();
					FeedGroups maxRankGroup = Collections.min(feedGroupsListDTO,
							Comparator.comparing(s -> s.getRank()));
					alertViewer.setGroupLevel(maxRankGroup.getGroupId().getId().intValue());
				}
				alertViewer.setAssignee(0l);
				ListItemDto FeedStatusDto = listItemService.getListItemByListTypeAndDisplayName("Alert Status",
						"Open");
				alertViewer.setStatus(FeedStatusDto.getListItemId());
			
				alertViewer.setRiskIndicators(null);
				alertViewer.setComments(null);
				alertViewer.setPeriodicReview(true);
				if(amsJson.has("alertMetaData") && amsJson.get("alertMetaData")!=null && amsJson.getJSONArray("alertMetaData").length()>0) {
					if(amsJson.getJSONArray("alertMetaData").getJSONObject(0).getJSONArray("entries").getJSONObject(0).has("watchlist_id")) {
						
						//createdAlert=alertManagementDao.create(alertViewer);
						
						JSONArray alertMetaDataArray= amsJson.getJSONArray("alertMetaData");
						for(int j=0;j<alertMetaDataArray.length();j++) {
							JSONObject entries=amsJson.getJSONArray("alertMetaData").getJSONObject(j).getJSONArray("entries").getJSONObject(0);
							entries.put("attribute_name", new JSONArray().put(new JSONObject().put("attribute_name",amsJson.getJSONArray("alertMetaData").getJSONObject(j).getJSONArray("entries").getJSONObject(0).getString("attribute_name"))));
						}
						alertViewer.setAlertMetaData(amsJson.getJSONArray("alertMetaData").toString());
						alertViewer.setWatchList(amsJson.getJSONArray("alertMetaData").getJSONObject(0).getJSONArray("entries").getJSONObject(0).getString("watchlist_id"));
					
					 * JSONObject
					 * entries=amsJson.getJSONArray("alertMetaData").getJSONObject(0).getJSONArray(
					 * "entries").getJSONObject(0);
					 * //if(alertJson.getString("value").equals("Burke Richard")) {
					 * //entries.put("attribute_name", alertJson.getString("value"));
					 * entries.put("attribute_name", new JSONArray().put(new
					 * JSONObject().put("attribute_name",amsJson.getJSONArray("alertMetaData").
					 * getJSONObject(0).getJSONArray("entries").getJSONObject(0).getString(
					 * "attribute_name"))));
					 
					}else {
						alertViewer.setAlertMetaData(amsJson.getJSONArray("alertMetaData").toString());
						alertViewer.setWatchList("");
					}
				}
				 created=alertManagementDao.create(alertViewer);
				System.out.println("---> "+  created.getAlertId());
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return created;
	}*/
	

	@Override
	@Transactional("transactionManager")
	public AlertsDto saveOrUpdateAlerts(AlertsDto alertDto, long userId) throws Exception {
		Alerts alertViewer = null;
		Users author = usersDao.find(userId);
		author = usersService.prepareUserFromFirebase(author);
		String status=null;
		StringBuilder alertOperation = new StringBuilder();
		boolean mainAlertDeleted=false;
		try{
			//Entity Identification for approve
			if(alertDto.getAlertId() != null && /*null != alertDto.getIsEntityIdentified() 
					&& alertDto.getIsEntityIdentified() == true 
					&&*/ null != alertDto.getStatuse() 
					&& (alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_APPROVED_STATUS)
							|| alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_REJECTED_STATUS))
					&& null != alertDto.getClassification() && (alertDto.getClassification().size() > 0)
					 /*&& alertDto.getIdentifiedEntityId() != null*/ 
					&& !StringUtils.isEmpty(alertDto.getSource()))
			{
				Alerts alert = alertManagementDao.find(alertDto.getAlertId());
				if(null != alert)
				{
					Long sourceId = null;
					//fetch the source
					ListItemDto statusDto = listItemService
							.getListItemByListTypeAndCode(ElementConstants.FEED_SOURCE_TYPE, alertDto.getSource());
					if(null != statusDto && null != statusDto.getListItemId()){
						sourceId = statusDto.getListItemId();
					}
					//classifications for the entity identified
					List<String> classificationNames =  alertDto.getClassification();
					List<ListItemDto> filteredFeedClassifications = new ArrayList<ListItemDto>();
					//fetch all feed classifications from list management
					List<ListItemDto> feedClassifications = listItemService.getListItemByListType(ElementConstants.FEED_CLASSIFICATION_STRING);
					//filter the feed classifications from list management with the given classifications of the identified entity
					feedClassifications.stream().forEach(s -> {
						for(String str : classificationNames){
							if(null != s.getDisplayName() 
									&& s.getDisplayName().equalsIgnoreCase(str) || s.getCode().equalsIgnoreCase(str)){
								filteredFeedClassifications.add(s);
							}
						}
					});
					//prepare the list of classification IDs
					List<Long> filteredFeedClassificationsIds = filteredFeedClassifications.stream().map(ListItemDto::getListItemId).collect(Collectors.toList());
					//fetch all the feed by the classifications for the given identified entity
					List<FeedManagement> feedList = feedManagementService.getFeedsByTypeIds(filteredFeedClassificationsIds);
					feedList.removeIf(p -> !p.getSource()
							.equals(statusDto.getListItemId().intValue()));
					//prepare list of statuses where review is required among all the feeds
					Set<Long> reviewRequiredStatuses = new HashSet<>();
					//prepare a list of new alerts in case of actual alerts to be created
					List<Alerts> alertTobeCreated = new ArrayList<>();
					for(FeedManagement feedobj : feedList){
						if(true == feedobj.getIsReviewerRequired()){
							List<ListItem> reviewsRequiredFor = feedobj.getReviewer();
							if(null != reviewsRequiredFor && !reviewsRequiredFor.isEmpty()){
								Set<Long> reviewstatusId = reviewsRequiredFor.stream().map(ListItem::getListItemId).collect(Collectors.toSet());
								reviewRequiredStatuses.addAll(reviewstatusId);
							}
						}
						//fetch the all group levels of the current feed
						List<FeedGroups> feedGroups =  feedGroupsManagementDao.getFeedGroupsByFeedId(feedobj.getFeed_management_id());
						//initialize the internal objects
						feedGroups.stream().filter(s -> null != s.getFeedId()).forEach(f -> Hibernate.initialize(f.getFeedId()));
						feedGroups.stream().filter(s -> null != s.getGroupId()).forEach(f -> Hibernate.initialize(f.getGroupId()));
						feedGroups.stream().filter(s -> null != s.getFeedId().getReviewer()).forEach(f -> f.getFeedId().getReviewer().stream().filter(s -> null != s).forEach(d -> Hibernate.initialize(d)));
						
						Integer groupLevel = null;
						if( null != feedGroups && feedGroups.size() > 0)
						{
							Groups entityIdentification = groupsService.getGroup(ElementConstants.ENTITY_IDENTIFICATION);
							if(null != entityIdentification && null != entityIdentification.getId() && feedGroups.size() >1)
								feedGroups.removeIf(f -> f.getGroupId().getId().equals(entityIdentification.getId()));
							if(feedGroups !=null && feedGroups.size() >0){
								FeedGroups minRankGroup =  Collections.min(feedGroups, Comparator.comparing(s -> s.getRank()));
								groupLevel  = minRankGroup.getGroupId().getId().intValue();
							}
						}	
						
						Alerts newAlert = new Alerts();
						List<FeedManagement> feedm = new ArrayList<FeedManagement>();
						feedm.add(feedobj);
						BeanUtils.copyProperties(alert, newAlert);
						newAlert.setAlertId(null);
						newAlert.setComments(null);
						newAlert.setFeed(feedm);
						newAlert.setGroupLevel(groupLevel);
						newAlert.setUserApproved(author.getUserId());
						newAlert.setReviewer(groupLevel.longValue());
						alertTobeCreated.add(newAlert);
					}
					if(null != reviewRequiredStatuses && !reviewRequiredStatuses.isEmpty()
							&& reviewRequiredStatuses.contains(alertDto.getStatuse().getListItemId())){
						if(null !=  alertDto.getStatuse().getDisplayName() && null!=alert.getUserApproved() && !alert.getUserApproved().equals(author.getUserId())
								&& null != alertDto.getIsEntityIdentified() && alertDto.getIsEntityIdentified() == true 
								&& alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_APPROVED_STATUS)
								&& null != alertDto.getReviewStatusId() && null != alertDto.getReviewStatusId().getDisplayName()
								&& ElementConstants.ALERT_REVIEW_CONFIRMED_STATUS.equalsIgnoreCase(alertDto.getReviewStatusId().getDisplayName())){
							//create individual alerts and status Identity approved
							List<AuditLog> logList = new ArrayList<>();
							logList =  auditLogService.getLogsByTypeId(ElementConstants.ALERT_MANAGEMENT_TYPE,alert.getAlertId());
							for(Alerts al : alertTobeCreated){
								al.setStatus(alertDto.getStatuse().getListItemId());
								alert.setStatus(alertDto.getStatuse().getListItemId());
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								
								if (null == alert.getAssignee()){
									alert.setAssignee(0L);
									al.setAssignee(0L);
								}
								if (null != alertDto.getIsEntityIdentified()){
									alert.setIsEntityIdentified(alertDto.getIsEntityIdentified());
									al.setIsEntityIdentified(alertDto.getIsEntityIdentified());
								}
								if (null != alertDto.getIdentifiedEntityId()){
									alert.setIdentifiedEntityId(alertDto.getIdentifiedEntityId());
									al.setIdentifiedEntityId(alertDto.getIdentifiedEntityId());
								}
								if (null != alertDto.getReviewStatusId()){
									alert.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
									al.setReviewStatusId(UncomfirmedStatus.getListItemId());
								}
								if(null != alertDto.getConfidenceLevel()){
									al.setConfidenceLevel(alertDto.getConfidenceLevel());
								}
								alertOperation.append(" changed Status to Identity Approved and Review Confirmed "+ ", ");
								
								/*save alert with the identified entity and the feed, group, reviewer by classification 
								status- Identy approved, review status - confirm*/
								al = save(al);
								
								for(AuditLog log : logList)
								{
								  AuditLog newlog = new AuditLog();
								  BeanUtils.copyProperties(log, newlog);
								  newlog.setLogId(null);
								  newlog.setTypeId(al.getAlertId());
								  auditLogService.save(newlog);
								}
								
								String alertOperationDesc = "";
							    if(null != alertOperation && !alertOperation.toString().isEmpty())
							       alertOperationDesc = alertOperation.toString().trim();
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
								log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + alertOperationDesc + " ");
								log.setName(al.getAlertId().toString());
								log.setUserId(userId);
								if(status!=null)
									log.setSubType(ElementConstants.ALERT_STATUS_SUBTYPE);
								log.setTypeId(al.getAlertId());
								if(status!=null)
								log.setSubType(status);
								auditLogDao.create(log);
							}
							delete(alert);
							mainAlertDeleted=true;
						}else if(null !=  alertDto.getStatuse().getDisplayName() && alert.getUserApproved()!=null && !alert.getUserApproved().equals(author.getUserId())
								&& alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_REJECTED_STATUS)
								&& null != alertDto.getReviewStatusId() && null != alertDto.getReviewStatusId().getDisplayName()
								&& ElementConstants.ALERT_REVIEW_CONFIRMED_STATUS.equalsIgnoreCase(alertDto.getReviewStatusId().getDisplayName())
								){
							boolean isStatusAllowedToChange = validateAlertStatusChange(alert.getStatus(), alertDto.getStatuse().getListItemId());
							if(isStatusAllowedToChange){
								//nothing, change the status to Identity Rejected
								alert.setStatus(alertDto.getStatuse().getListItemId());
								status = alertDto.getStatuse().getDisplayName();
								alert.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
								alertOperation.append(" changed Status to  Identity Rejected and Review Confirmed" + ", ");
							}else{
								throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
							}
							
						}else{
							//review not done
							if(null !=  alertDto.getStatuse().getDisplayName()
									&& alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_APPROVED_STATUS)
									&& null != alertDto.getIsEntityIdentified() && alertDto.getIsEntityIdentified() == true){
								if (null == alert.getAssignee())
									alert.setAssignee(0L);
								if (null != alertDto.getIsEntityIdentified())
									alert.setIsEntityIdentified(alertDto.getIsEntityIdentified());
								if (null != alertDto.getIdentifiedEntityId())
									alert.setIdentifiedEntityId(alertDto.getIdentifiedEntityId());
								if (null != alertDto.getReviewStatusId())
									alert.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
								alert.setUserApproved(author.getUserId());
								//set status identity approved-needed review
								ListItemDto approvedReviewStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_STATUS_TYPE,
										ElementConstants.ENTITY_APPROVED_NEEDED_REVIEW);
								
								boolean isStatusAllowedToChange = validateAlertStatusChange(alert.getStatus(), approvedReviewStatus.getListItemId());
								if(isStatusAllowedToChange){
									if(null != approvedReviewStatus)
										alert.setStatus(approvedReviewStatus.getListItemId());
									status = alertDto.getStatuse().getDisplayName();
									alertOperation.append(" changed Status to Identity Approved and Needed Review "+  ", ");
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
							}else if(null !=  alertDto.getStatuse().getDisplayName()
									&& alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_REJECTED_STATUS)){
								//status rejected-needed review
								ListItemDto approvedReviewStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_STATUS_TYPE,
										ElementConstants.ENTITY_REJECTED_NEEDED_REVIEW);
								boolean isStatusAllowedToChange = validateAlertStatusChange(alert.getStatus(), approvedReviewStatus.getListItemId());
								if(isStatusAllowedToChange){
									if(null != approvedReviewStatus)
										alert.setStatus(approvedReviewStatus.getListItemId());
									if (null != alertDto.getReviewStatusId())
										alert.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
									status = alertDto.getStatuse().getDisplayName();
									alert.setUserApproved(author.getUserId());
									alertOperation.append(" changed Status to Identity Rejected and Needed Review "+  ", ");
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
							}
						}
					}else{
						if(null !=  alertDto.getStatuse().getDisplayName()
								&& alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_APPROVED_STATUS)
								&& null != alertDto.getIsEntityIdentified() && alertDto.getIsEntityIdentified() == true){
							//create individual alerts and status Identity approved
							List<AuditLog> logList = new ArrayList<>();
						    logList =  auditLogService.getLogsByTypeId(ElementConstants.ALERT_MANAGEMENT_TYPE,alert.getAlertId());
							for(Alerts al : alertTobeCreated){
								al.setStatus(alertDto.getStatuse().getListItemId());
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								if (null == alert.getAssignee()){
									alert.setAssignee(0L);
									al.setAssignee(0L);
								}
								if (null != alertDto.getIsEntityIdentified()){
									alert.setIsEntityIdentified(alertDto.getIsEntityIdentified());
									al.setIsEntityIdentified(alertDto.getIsEntityIdentified());
								}
								if (null != alertDto.getIdentifiedEntityId()){
									alert.setIdentifiedEntityId(alertDto.getIdentifiedEntityId());
									al.setIdentifiedEntityId(alertDto.getIdentifiedEntityId());
								}
								if (null != alertDto.getReviewStatusId()){
									alert.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
									al.setReviewStatusId(UncomfirmedStatus.getListItemId());
								}
								if(null != alertDto.getConfidenceLevel()){
									al.setConfidenceLevel(alertDto.getConfidenceLevel());
								}
								alert.setUserApproved(author.getUserId());
								status = alertDto.getStatuse().getDisplayName();
								alertOperation.append(" changed Status to Identity Approved "+  ", ");	
								/*save alert with the identified entity and the feed, group, reviewer by classification 
								status- Identity approved, review status - confirm*/
                                al = save(al);	
								for(AuditLog log : logList)
								{
								  AuditLog newlog = new AuditLog();
								  BeanUtils.copyProperties(log, newlog);
								  newlog.setLogId(null);
								  newlog.setTypeId(al.getAlertId());
								  auditLogService.save(newlog);
								}
								
								String alertOperationDesc = "";
							    if(null != alertOperation && !alertOperation.toString().isEmpty())
							       alertOperationDesc = alertOperation.toString().trim();
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
								log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + alertOperationDesc + " ");
								log.setName(al.getAlertId().toString());
								log.setUserId(userId);
								if(status!=null)
									log.setSubType(ElementConstants.ALERT_STATUS_SUBTYPE);
								log.setTypeId(al.getAlertId());
								if(status!=null)
								log.setSubType(status);
								auditLogDao.create(log);
							}
							delete(alert);
							mainAlertDeleted=true;
						}else if((null !=  alertDto.getStatuse().getDisplayName()
								&& alertDto.getStatuse().getDisplayName().equalsIgnoreCase(ElementConstants.IDENTITY_REJECTED_STATUS))){
							
							boolean isStatusAllowedToChange = validateAlertStatusChange(alert.getStatus(), alertDto.getStatuse().getListItemId());
							if(isStatusAllowedToChange){
								//nothing, change the status to Identity Rejected
								alert.setStatus(alertDto.getStatuse().getListItemId());
								if (null != alertDto.getReviewStatusId())
									alert.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
								alert.setUserApproved(author.getUserId());
								alertOperation.append(" changed Status to Identity Rejected"+  ", ");
							}else{
								throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
							}
						}
					}
						if(!mainAlertDeleted) {
							alertManagementDao.saveOrUpdate(alert);
						}
					
							
						//Setting data in AlertsDto Object
						if(null != alert.getAlertId())
							alertDto.setAlertId(alert.getAlertId());
						if(null != alert.getAlertMetaData())
							alertDto.setAlertMetaData(alert.getAlertMetaData());
						
						// Finding Assignee if it exists
						if(null == alert.getAssignee() || 0 ==alert.getAssignee()) // Assignee does not exists
						{
							alertDto.setAsignee(null);
						}
						else   // Assignee exists so find
						{
							AssigneeDto assigneeDto = new AssigneeDto(); 
							Users user = usersDao.find(alert.getAssignee());
							
							if(null != user ){
								BeanUtils.copyProperties(user, assigneeDto);
								alertDto.setAsignee(assigneeDto);
							}else{
								alertDto.setAsignee(null);
							}
						}																																									
						
						if(null != alertDto.getClassification())
							alertDto.setClassification(alertDto.getClassification());
						if(null != alert.getComments())
						{
							Hibernate.initialize(alert.getComments());
							alertDto.setCommentsCount(alert.getComments().size());
						}
						
						if(null != alert.getConfidenceLevel())
							alertDto.setConfidenceLevel(alert.getConfidenceLevel());
						if(null != alert.getCreatedDate())
							alertDto.setCreatedDate(alert.getCreatedDate());
						if(null != alert.getCustomerId())
							alertDto.setCustomerId(alert.getCustomerId());
						if(null != alert.getEntityName())
							alertDto.setEntityName(alert.getEntityName());
						if(null != alert.getEntiType())
							alertDto.setEntiType(alert.getEntiType());
						if(null != alert.getFeed())
						{
							//Hibernate.initialize(alert.getFeed().getGroupLevels());
							alert.getFeed().stream().forEach(s-> Hibernate.initialize(s.getGroupLevels()));
							//Hibernate.initialize(alert.getFeed().getReviewer());
							alert.getFeed().stream().forEach(s-> Hibernate.initialize(s.getReviewer()));
							alertDto.setFeed(alert.getFeed());
							List<FeedGroups> feedGroups = new ArrayList<>();
							for(FeedManagement feed : alert.getFeed()){
								if(null != feed.getGroupLevels() && !feed.getGroupLevels().isEmpty()){
									List<FeedGroups> feedGroup = feed.getGroupLevels();
									feedGroup.stream().forEach(f -> Hibernate.initialize(f));
									feedGroup.stream().forEach(s -> Hibernate.initialize(s.getGroupId()));
									feedGroups.addAll(feedGroup);
								}
								
							}
							if(null != feedGroups)
								alertDto.setFeedGroups(feedGroups);
						}
						
						
						if(null != alert.getGroupLevel())
							alertDto.setGroupLevel(alert.getGroupLevel());
						if(null != alert.getIdentifiedEntityId())
							alertDto.setIdentifiedEntityId(alert.getIdentifiedEntityId());
						if(null != alert.getIsEntityIdentified())
							alertDto.setIsEntityIdentified(alert.getIsEntityIdentified());		
						if(null != alert.getPeriodicReview())
							alertDto.setPeriodicReview(alert.getPeriodicReview());
						
						if(null != alert.getReviewer())
						{
							Hibernate.initialize(alert.getReviewer());
						    alertDto.setReviewer(alert.getReviewer()); 
						}
						
						alertDto.setReviewStatusId(null);
						if(null != alert.getRiskIndicators())
							alertDto.setRiskIndicators(alert.getRiskIndicators());
						if(null != alertDto.getSource())
							alertDto.setSource(alertDto.getSource());
								
						if(null != listItemService.getListItemByID(alert.getStatus()))
							alertDto.setStatuse(listItemService.getListItemByID(alert.getStatus())); 
						alertDto.setTimeInStatus(null);
						alertDto.setTimeStatusDuration(null);
						if(null != alert.getUpdatedDate())
							alertDto.setUpdatedDate(alert.getUpdatedDate());
						if(null != alert.getWatchList())
							alertDto.setWatchList(alert.getWatchList());  
						
						/*if(null != alert.getEntityName())
							alertOperation = alertOperation.append("Entity "+alert.getEntityName()+" Identified");*/
					
						String alertOperationDesc = "";
					    if(null != alertOperation && !alertOperation.toString().isEmpty())
					       alertOperationDesc = alertOperation.toString().trim();
					    	
						AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
						log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + alertOperationDesc + " ");
						log.setName(alertDto.getAlertId().toString());
						log.setUserId(userId);
						if(status!=null)
							log.setSubType(ElementConstants.ALERT_STATUS_SUBTYPE);
						log.setTypeId(alertDto.getAlertId());
						if(status!=null)
						log.setSubType(status);
						auditLogDao.create(log);
						//alert.getFeed().stream().filter(f-> null != f.getType()).collect(Collectors.toList()));
						// manually adding hte classification name from feed
						List<String> classicationList= new ArrayList<>();
						for(FeedManagement feed: alert.getFeed()) {
							if(feed !=null) {
								ListItemDto dto= listItemService.getListItemByID(new Long(feed.getType()));
								classicationList.add(dto.getDisplayName());
							}
						}
						alertDto.setClassification(classicationList);
						UserApprovedDto userApproved= new UserApprovedDto();
						if(alert.getUserApproved()!=null) {
							Users user = usersDao.find(alert.getUserApproved());
							BeanUtils.copyProperties(user, userApproved);
						}
						
						alertDto.setUserApproved(userApproved);
						Hibernate.initialize(alertDto.getFeedGroups());
						alertDto.getFeedGroups().stream().filter(s -> null != s.getGroupId()).forEach(f -> Hibernate.initialize(f.getGroupId()));																						
					return alertDto;
				}
				
				}			         			
			else // Normal saveOrUpdate or Entity Identification for cancel
			{
			
		    	if (alertDto != null && alertDto.getAlertId() != null) {
				alertViewer = alertManagementDao.find(alertDto.getAlertId());
				if (alertViewer == null) {
					alertViewer = new Alerts();
					BeanUtils.copyProperties(alertDto, alertViewer);
					alertViewer = alertManagementDao.create(alertViewer);
					BeanUtils.copyProperties(alertViewer, alertDto);
					
					Users user = usersDao.find(alertViewer.getAssignee());
					AssigneeDto author1 = new AssigneeDto();
					if(user != null){
						BeanUtils.copyProperties(user, author1);
						alertDto.setAsignee(author1);
					}else{
						alertDto.setAsignee(null);
					}
					alertDto.setStatuse(listItemService.getListItemByID(alertViewer.getStatus()));
					alertOperation = alertOperation.append("CREATED");
				} else {
					//backup object for audit
					Alerts alertViewerdb = alertViewer;
					
					/*if(alertDto.getFeed() != null  && !alertDto.getFeed().getFeedName().equalsIgnoreCase(alertViewerdb.getFeed().getFeedName())){
						alertOperation.append(" changed feed from  "+ alertViewerdb.getFeed().getFeedName() + " to " + alertDto.getFeed().getFeedName() +", " );
					}*/
					if(alertDto.getGroupLevel() != null && !alertDto.getGroupLevel().equals(alertViewerdb.getGroupLevel())){
						String dtoGroupLevel = groupsDao.find(alertDto.getGroupLevel().longValue()).getName();
						String dbGroupLevel = groupsDao.find(alertViewerdb.getGroupLevel().longValue()).getName();
						alertOperation.append(" changed group level from  "+ dbGroupLevel + " to " + dtoGroupLevel+ ", ");
					}
					if( null != alertDto.getRiskIndicators()){
						if(null != alertViewerdb.getRiskIndicators() && !alertDto.getRiskIndicators().equals(alertViewerdb.getRiskIndicators())){
							alertOperation.append(" changed risk indicator from " + alertViewerdb.getRiskIndicators()+ " to " + alertDto.getRiskIndicators()+ ", ");
						}else{
							alertOperation.append(" set risk indicator to " + alertDto.getRiskIndicators()+ ", ");
						}
					}
					if(alertDto.getStatuse() != null && null != alertDto.getStatuse().getListItemId()  
							&& !alertDto.getStatuse().getListItemId().equals(alertViewerdb.getStatus())){
						String dtoStatus = listItemService.find(alertDto.getStatuse().getListItemId().longValue()).getDisplayName();
						status=dtoStatus;
						String dbStatus = listItemService.find(alertViewerdb.getStatus().longValue()).getDisplayName();
						status=dbStatus;
						alertOperation.append(" changed status from  "+ dbStatus + " to " + dtoStatus+ ", ");
					}
					if(alertDto.getReviewStatusId() != null && null != alertDto.getReviewStatusId().getListItemId() && null != alertViewerdb.getReviewStatusId() 
							&& !alertDto.getReviewStatusId().getListItemId().equals(alertViewerdb.getReviewStatusId())){
						String dtoStatus = listItemService.find(alertDto.getReviewStatusId().getListItemId()).getDisplayName();
						status=dtoStatus;
						String dbStatus = listItemService.find(alertViewerdb.getReviewStatusId()).getDisplayName();
						status=dbStatus;
						alertOperation.append(" changed review status from  "+ dbStatus + " to " + dtoStatus+ ", ");
					}
					if(alertDto.getReviewer() != null && !alertDto.getReviewer().equals(alertViewerdb.getReviewer())){
						String dtoGroupLevel = groupsDao.find(alertDto.getReviewer().longValue()).getName();
						String dbGroupLevel = groupsDao.find(alertViewerdb.getReviewer().longValue()).getName();
						alertOperation.append(" changed reviewer from  "+ dbGroupLevel + " to " + dtoGroupLevel+ ", ");
					}
					
					if(alertDto.getWatchList() != null && !alertDto.getWatchList().equalsIgnoreCase(alertViewerdb.getWatchList())){
						alertOperation.append(" changed watchlist from  "+ alertViewerdb.getWatchList() + " to " + alertDto.getWatchList()+ ", ");
					}
					if(alertDto.getPeriodicReview() != null && !alertDto.getPeriodicReview().equals(alertViewerdb.getPeriodicReview())){
						alertOperation.append(" changed periodic review to " + alertDto.getPeriodicReview()+ ", ");
					}
					
					if(null != alertDto.getAsignee() && null != alertDto.getAsignee().getUserId() 
							&& !alertDto.getAsignee().getUserId().equals(alertViewerdb.getAssignee())){
						Users dtoUserObj = usersDao.find(alertDto.getAsignee().getUserId());
						String dtoUser  = dtoUserObj.getFirstName()+" "+dtoUserObj.getLastName();
						Users dbUserObj = null;
						if(null != alertViewerdb.getAssignee() && !alertViewerdb.getAssignee().equals(0L)){
							dbUserObj = usersDao.find(alertViewerdb.getAssignee());
							//String dbUser  = dbUserObj.getFirstName()+" "+dbUserObj.getLastName();
							alertOperation.append( " assigned alert from "+dbUserObj.getFirstName() + " to " + dtoUser + ", ");
						}else{
							//String dbUser  = author.getFirstName()+" "+author.getLastName();
							alertOperation.append(" assigned alert from Unassigned to " + dtoUser + ", ");
						}
					}
					if(alertDto.getConfidenceLevel() != null &&  !alertDto.getConfidenceLevel().equals(alertViewerdb.getConfidenceLevel())){
						alertOperation.append(" changed confidence level from  "+ alertViewerdb.getConfidenceLevel() + " to " + alertDto.getConfidenceLevel()+ ", ");
					}
					if(alertDto.getCustomerId() != null && !alertDto.getCustomerId().equalsIgnoreCase(alertViewerdb.getCustomerId())){
						alertOperation.append(" changed costomer Id from  "+ alertViewerdb.getCustomerId() + " to " + alertDto.getCustomerId()+ ", ");
					}
					if(alertDto.getEntityName() !=  null && !alertDto.getEntityName().equalsIgnoreCase(alertViewerdb.getEntityName())){
						alertOperation.append(" changed Entity Name from  "+ alertViewerdb.getEntityName() + " to " + alertDto.getEntityName());
					}

                    if(alertDto.getIsEntityIdentified() !=  null && !alertDto.getIsEntityIdentified().equals(alertViewerdb.getIsEntityIdentified())){
						alertOperation.append(" changed IsEntityIdentified  from  "+ alertViewerdb.getIsEntityIdentified() + " to " + alertDto.getIsEntityIdentified());
					}
					if(alertDto.getIdentifiedEntityId() !=  null && !alertDto.getIdentifiedEntityId().equals(alertViewerdb.getIdentifiedEntityId())){
						alertOperation.append(" changed IdentifiedEntityId  from  "+ alertViewerdb.getIdentifiedEntityId() + " to " + alertDto.getIdentifiedEntityId());
					}
					if(alertDto.getEntiType() !=  null && !alertDto.getEntiType().equals(alertViewerdb.getEntiType())){
						alertOperation.append(" changed Entity Type  from  "+ alertViewerdb.getEntiType() + " to " + alertDto.getEntiType());
					}
					//List<AlertComments> dbComments = alertViewerdb.getComments();
					//List<AlertComments> dtoComments = alertDto.getComments();
					
					/*for(AlertComments dtoComment : dtoComments){
						for(AlertComments dbComment : dbComments){
							if(!dtoComment.getAlertId().equals(dbComment.getAlertId())){
								alertOperation.append(" "+author.getFirstName()+" "+author.getLastName()  + " commented on the alert ");
							}else{
								alertOperation.append(" "+author.getFirstName()+" "+author.getLastName()  + " edited the alert ");
							}
						}
					}*/
					//apply the persist data
					//BeanUtils.copyProperties(alertDto, alertViewer);
					
					//set the changes manually from dto to entity
					if(alertDto.getAlertId() != null){
						alertViewer.setAlertId(alertDto.getAlertId());
					}
					if(alertDto.getAlertMetaData() != null){
						alertViewer.setAlertMetaData(alertDto.getAlertMetaData());
					}
				
					if(null != alertDto.getAsignee() && null != alertDto.getAsignee().getUserId()){
						alertViewer.setAssignee(alertDto.getAsignee().getUserId());
					}else{
						alertViewer.setAssignee(new Long(0));
					}
					
					/*if(alertDto.getComments() != null){
						alertViewer.setComments(alertDto.getComments());
					}*/
					if(alertDto.getConfidenceLevel() != null){
						alertViewer.setConfidenceLevel(alertDto.getConfidenceLevel());
					}
					if(alertDto.getCreatedDate() != null){
						alertViewer.setCreatedDate(alertDto.getCreatedDate());
					}
					if(alertDto.getEntityName() != null){
						alertViewer.setEntityName(alertDto.getEntityName());
					}
					if(alertDto.getCustomerId() != null){
						alertViewer.setCustomerId(alertDto.getCustomerId());
					}
					if(alertDto.getFeed() != null){
						alertViewer.setFeed(alertDto.getFeed());
					}
					if(alertDto.getGroupLevel() != null){
						alertViewer.setGroupLevel(alertDto.getGroupLevel());
					}
					if(alertDto.getReviewer() != null){
						alertViewer.setReviewer(alertDto.getReviewer());
					}
					if(alertDto.getRiskIndicators() != null){
						alertViewer.setRiskIndicators(alertDto.getRiskIndicators());
					}
					if (alertDto.getStatuse() != null) {
						List<FeedManagement> feedList = alertViewer.getFeed();
						Set<Long> reviewRequiredStatuses = new HashSet<>();
						for (FeedManagement feedobj : feedList) {
							if (true == feedobj.getIsReviewerRequired()) {
								List<ListItem> reviewsRequiredFor = feedobj.getReviewer();
								if (null != reviewsRequiredFor && !reviewsRequiredFor.isEmpty()) {
									Set<Long> reviewstatusId = reviewsRequiredFor.stream()
											.map(ListItem::getListItemId).collect(Collectors.toSet());
									reviewRequiredStatuses.addAll(reviewstatusId);
								}
							}
						}
						if (null != reviewRequiredStatuses && !reviewRequiredStatuses.isEmpty()
								&& reviewRequiredStatuses.contains(alertDto.getStatuse().getListItemId())) {
							if (null != alertDto.getStatuse().getDisplayName()
									&& alertDto.getStatuse().getDisplayName()
											.equalsIgnoreCase(ElementConstants.ALERT_APPROVED_STATUS)
									&& null != alertDto.getReviewStatusId()
									&& null != alertDto.getReviewStatusId().getDisplayName()
									&& ElementConstants.ALERT_REVIEW_CONFIRMED_STATUS
											.equalsIgnoreCase(alertDto.getReviewStatusId().getDisplayName())
											&& alertViewer.getUserApproved()!=null && !alertViewer.getUserApproved().equals(author.getUserId())) {
								boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), alertDto.getStatuse().getListItemId());
								if(isStatusAllowedToChange){
									alertViewer.setStatus(alertDto.getStatuse().getListItemId());
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
							} else if (null != alertDto.getStatuse().getDisplayName()
									&& alertDto.getStatuse().getDisplayName()
											.equalsIgnoreCase(ElementConstants.ALERT_REJECTED_STATUS)
									&& null != alertDto.getReviewStatusId()
									&& null != alertDto.getReviewStatusId().getDisplayName()
									&& ElementConstants.ALERT_REVIEW_CONFIRMED_STATUS
											.equalsIgnoreCase(alertDto.getReviewStatusId().getDisplayName())
											&& alertViewer.getUserApproved()!=null && !alertViewer.getUserApproved().equals(author.getUserId())) {
								boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), alertDto.getStatuse().getListItemId());
								if(isStatusAllowedToChange){
									alertViewer.setStatus(alertDto.getStatuse().getListItemId());
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
							} else if (null != alertDto.getStatuse().getDisplayName()
									&& alertDto.getStatuse().getDisplayName()
											.equalsIgnoreCase(ElementConstants.ALERT_APPROVED_STATUS)
									&& null != alertDto.getReviewStatusId()
									&& null != alertDto.getReviewStatusId().getDisplayName()
									&& ElementConstants.ALERT_REVIEW_DECLINED_STATUS
											.equalsIgnoreCase(alertDto.getReviewStatusId().getDisplayName())
											&& alertViewer.getUserApproved()!=null && !alertViewer.getUserApproved().equals(author.getUserId())) {
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
							} else if (null != alertDto.getStatuse().getDisplayName()
									&& alertDto.getStatuse().getDisplayName()
											.equalsIgnoreCase(ElementConstants.ALERT_REJECTED_STATUS)
									&& null != alertDto.getReviewStatusId()
									&& null != alertDto.getReviewStatusId().getDisplayName()
									&& ElementConstants.ALERT_REVIEW_DECLINED_STATUS
											.equalsIgnoreCase(alertDto.getReviewStatusId().getDisplayName())
											&& alertViewer.getUserApproved()!=null && !alertViewer.getUserApproved().equals(author.getUserId())) {
								ListItemDto identityApprovedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_STATUS_TYPE,
										ElementConstants.IDENTITY_APPROVED_STATUS);
								boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), identityApprovedStatus.getListItemId());
								if(isStatusAllowedToChange){
									alertViewer.setStatus(identityApprovedStatus.getListItemId());
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
								 ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
											ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
							} else if (null != alertDto.getStatuse().getDisplayName() && alertDto.getStatuse()
									.getDisplayName().equalsIgnoreCase(ElementConstants.ALERT_APPROVED_STATUS) ) {
								ListItemDto approvedReviewStatus = listItemService
										.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_STATUS_TYPE,
												ElementConstants.APPROVED_NEEDED_REVIEW);
								boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), approvedReviewStatus.getListItemId());
								if(isStatusAllowedToChange){
									alertViewer.setStatus(approvedReviewStatus.getListItemId());
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
								alertViewer.setUserApproved(author.getUserId());
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
							} else if (null != alertDto.getStatuse().getDisplayName() && alertDto.getStatuse()
									.getDisplayName().equalsIgnoreCase(ElementConstants.ALERT_REJECTED_STATUS)) {
								// status rejected-needed review
								ListItemDto approvedReviewStatus = listItemService
										.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_STATUS_TYPE,
												ElementConstants.REJECTED_NEEDED_REVIEW);
								boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), approvedReviewStatus.getListItemId());
								if(isStatusAllowedToChange){
									alertViewer.setStatus(approvedReviewStatus.getListItemId());
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
								alertViewer.setUserApproved(author.getUserId());
							} else {
								boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), alertDto.getStatuse().getListItemId());
								if(isStatusAllowedToChange){
									alertViewer.setStatus(alertDto.getStatuse().getListItemId());
								}else{
									throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
								}
								ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
										ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
								alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
								alertViewer.setUserApproved(author.getUserId());
							}
						} else {
							boolean isStatusAllowedToChange = validateAlertStatusChange(alertViewer.getStatus(), alertDto.getStatuse().getListItemId());
							if(isStatusAllowedToChange){
								alertViewer.setStatus(alertDto.getStatuse().getListItemId());
							}else{
								throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
							}
							
							ListItemDto UncomfirmedStatus = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.ALERT_REVIEW_STATUS_TYPE,
									ElementConstants.ALERT_REVIEW_UNCONFIRMED_STATUS);
							alertViewer.setReviewStatusId(UncomfirmedStatus.getListItemId());
							alertViewer.setUserApproved(author.getUserId());
						}
					}
					/*if(alertDto.getReviewStatusId() != null ) {
							alertViewer.setReviewStatusId(alertDto.getReviewStatusId().getListItemId());
					}*/
					if(alertDto.getWatchList() != null){
						alertViewer.setWatchList(alertDto.getWatchList());
					}
					if(alertDto.getPeriodicReview() != null){
						alertViewer.setPeriodicReview(alertDto.getPeriodicReview());
					}
					if(alertDto.getIsEntityIdentified() != null){
						alertViewer.setIsEntityIdentified(alertDto.getIsEntityIdentified());
					}
					alertViewer.setUpdatedDate(new Date());
					Hibernate.initialize(alertViewer.getComments());
					if(alertDto.getIdentifiedEntityId()!=null)
						alertViewer.setIdentifiedEntityId(alertDto.getIdentifiedEntityId());
					
					/*if((alertDto.getIsEntityIdentified() == null || alertDto.getIsEntityIdentified() == false)  && null != alertDto.getFeed() && null != alertDto.getFeed().getType() && null !=alertDto.getFeed().getSource()) {
						//get Feed based on classification and screeningtype
						Long feedClassificationId= null;
						Long screeningTypeSourceId=null;
						List<ListItemDto> classificationTypes=listItemService.getListItemByListType(ElementConstants.FEED_CLASSIFICATION_STRING);
						if (classificationTypes != null && classificationTypes.size() > 0) {
							for (int i = 0; i < classificationTypes.size(); i++) {
								if (classificationTypes.get(i).getListItemId()
										.equals(Long.valueOf(alertDto.getFeed().getType())))
									feedClassificationId = classificationTypes.get(i).getListItemId();
							}
						}
						List<ListItemDto> sourceTypes=listItemService.getListItemByListType(ElementConstants.FEED_SOURCE_TYPE);
						if (sourceTypes != null && sourceTypes.size() > 0) {
							for (int i = 0; i < sourceTypes.size(); i++) {
								if (sourceTypes.get(i).getListItemId()
										.equals(Long.valueOf(alertDto.getFeed().getSource())))
									screeningTypeSourceId = sourceTypes.get(i).getListItemId();
							}
						}
						
						//sets alert feed and group level
						if (feedClassificationId != null && screeningTypeSourceId != null) {
							FeedManagementDto feedDto = feedManagementService
									.getFeedByTypeAndSource(feedClassificationId, screeningTypeSourceId);
							if (feedDto != null && feedDto.getFeed_management_id()!=null) {
								FeedManagement feed = new FeedManagement();
								BeanUtils.copyProperties(feedDto, feed);
								alertViewer.setFeed(feed);
								//Hibernate.initialize(feed.getReviewer());
								List<FeedGroups> feedGroupsListDTO = feed.getGroupLevels();
								if(null != feedGroupsListDTO){
									FeedGroups maxRankGroup =  Collections.min(feedGroupsListDTO, Comparator.comparing(s -> s.getRank()));
									List<Groups> groupsList = feedGroupsListDTO.stream().filter(s -> null != s.getGroupId()).map(e->e.getGroupId()).collect(Collectors.toList());
									List<Long> groupIdList = groupsList.stream().map(m -> m.getId()).collect(Collectors.toList());
										Long groupLevel = Long.valueOf(alertViewer.getGroupLevel());
										if(!groupIdList.contains(groupLevel)) {
											alertViewer.setGroupLevel(Integer.valueOf(maxRankGroup.getGroupId().getId().intValue()));
											alertViewer.setReviewer(maxRankGroup.getGroupId().getId());
										}
								}
							}
						}
					}*/
					
					alertManagementDao.saveOrUpdate(alertViewer);
					BeanUtils.copyProperties(alertViewer, alertDto);
					
					Users user = null;
					AssigneeDto author1 = new AssigneeDto();
					if(null != alertViewer.getAssignee()){
						user = usersDao.find(alertViewer.getAssignee());
					}
					if(user != null){
						BeanUtils.copyProperties(user, author1);
						alertDto.setAsignee(author1);
					}else{
						alertDto.setAsignee(null);
					}
					alertDto.setStatuse(listItemService.getListItemByID(alertViewer.getStatus()));
				}
			} else {
				alertViewer = new Alerts();
				BeanUtils.copyProperties(alertDto, alertViewer);
				alertViewer = alertManagementDao.create(alertViewer);
				BeanUtils.copyProperties(alertViewer, alertDto);
				
				Users user = usersDao.find(alertViewer.getAssignee());
				AssigneeDto author1 = new AssigneeDto();
				if(user != null){
					BeanUtils.copyProperties(user, author1);
					alertDto.setAsignee(author1);
				}else{
					alertDto.setAsignee(null);
				}
				alertDto.setStatuse(listItemService.getListItemByID(alertViewer.getStatus()));
				//alertOperation = alertOperation.append(alertDto.getFeed().getType() + " alert occurred " );
			}
		
			//User author = userDao.find(userId);
			String finalString = "";
			if(!alertOperation.toString().isEmpty()){
				String auditDesc = alertOperation.toString().trim();
				if(auditDesc.endsWith(",")){
					finalString = auditDesc.substring(0, auditDesc.lastIndexOf(","));
				}
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + finalString + " ");
				log.setName(alertDto.getAlertId().toString());
				log.setUserId(userId);
				if(status!=null)
					log.setSubType(ElementConstants.ALERT_STATUS_SUBTYPE);
				log.setTypeId(alertDto.getAlertId());
				if(status!=null)
					log.setSubType(status);
				auditLogDao.create(log);
			}
			//String finalString = alertOperation.toString().substring(0, alertOperation.toString().lastIndexOf(","));
			/*AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
			log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + alertOperation.toString() + " ");
			log.setName(alertDto.getAlertId().toString());
			log.setUserId(userId);
			if(status!=null)
				log.setSubType(ElementConstants.ALERT_STATUS_SUBTYPE);
			log.setTypeId(alertDto.getAlertId());
			if(status!=null)
			log.setSubType(status);
			auditLogDao.create(log);*/
			
			/*List<FeedManagement> feedList = feedManagementService.findAll();
			feedList.stream().filter(s -> null != s.getGroupLevels()).forEach(feed -> Hibernate.initialize(feed.getGroupLevels()));
			feedList.stream().filter(s -> null != s.getReviewer()).forEach(feed -> Hibernate.initialize(feed.getReviewer()));
			feedList.stream().forEach(f -> f.getGroupLevels().stream().filter(s -> null != s.getGroupId()).forEach(d -> Hibernate.initialize(d.getGroupId())));
			Map<Long, List<FeedGroups>> feedMap = feedList.stream()
					.collect(Collectors.toMap(FeedManagement::getFeed_management_id, FeedManagement::getGroupLevels));
			
				if (feedMap.containsKey(alertDto.getFeed().getFeed_management_id())) {
					alertDto.setFeedGroups(feedMap.get(alertDto.getFeed().getFeed_management_id()));
					
				}*/
			if(null != alertDto.getFeed())
			{
				//Hibernate.initialize(alert.getFeed().getGroupLevels());
				alertDto.getFeed().stream().forEach(s-> Hibernate.initialize(s.getGroupLevels()));
				//Hibernate.initialize(alert.getFeed().getReviewer());
				alertDto.getFeed().stream().forEach(s-> Hibernate.initialize(s.getReviewer()));
				alertDto.setFeed(alertDto.getFeed());
				List<FeedGroups> feedGroups = new ArrayList<>();
				for(FeedManagement feed : alertDto.getFeed()){
					if(null != feed.getGroupLevels() && !feed.getGroupLevels().isEmpty()){
						List<FeedGroups> feedGroup = feed.getGroupLevels();
						feedGroup.stream().forEach(f -> Hibernate.initialize(f));
						feedGroup.stream().forEach(s -> Hibernate.initialize(s.getGroupId()));
						feedGroups.addAll(feedGroup);
					}
					
				}
				if(null != feedGroups)
					alertDto.setFeedGroups(feedGroups);
			}
			// manually adding hte classification name from feed
			List<String> classicationList= new ArrayList<>();
			for(FeedManagement feed: alertViewer.getFeed()) {
				if(feed !=null) {
					ListItemDto dto= listItemService.getListItemByID(new Long(feed.getType()));
					classicationList.add(dto.getDisplayName());
				}
			}
			alertDto.setClassification(classicationList);
			//calculate Time in status
			UserApprovedDto userApproved= new UserApprovedDto();
			if(alertViewer.getUserApproved()!=null) {
				Users user = usersDao.find(alertViewer.getUserApproved());
				BeanUtils.copyProperties(user, userApproved);
			}
			alertDto.setUserApproved(userApproved);
			List<AlertsDto> dtoList = new ArrayList<>();
			dtoList.add(alertDto);
			dtoList = timeStatusUpdater(dtoList);
			for(AlertsDto dto : dtoList){
				alertDto = dto;
			}
			
			Hibernate.initialize(alertDto.getFeedGroups());
			alertDto.getFeedGroups().stream().filter(s -> null != s.getGroupId()).forEach(f -> Hibernate.initialize(f.getGroupId()));
			return alertDto;
		}	

		}catch(Exception ex){
		
			if(ex.getMessage().equalsIgnoreCase(ElementConstants.ALERT_STATUS_VALIDATION_FAILED)){
				throw new NoDataFoundException(ElementConstants.ALERT_STATUS_VALIDATION_FAILED);
			}else{
				ex.printStackTrace();
				return null;	
			}
		}
		return alertDto;
	}


	@Override
	@Transactional("transactionManager")
	public boolean deleteAlert(Long alertId, long userId) {
		Alerts alertViewer = null;
		if (alertId != null) {
			alertViewer = alertManagementDao.find(alertId);
			if (alertViewer != null) {
				alertManagementDao.delete(alertViewer);

				Users author = usersDao.find(userId);
				author = usersService.prepareUserFromFirebase(author);
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " Deleted an Alert ");
				log.setName(alertId.toString());
				log.setUserId(userId);
				auditLogDao.create(log);
				return true;
			} else {
				throw new NotFoundException("Alert not found!");
			}
		} else {
			throw new NotFoundException("Alert not found!");
		}
	}

	@Override
	@Transactional("transactionManager")
	public Map<List<AlertsDto>, Long> getAlerts(FilterModelDto filterDto, Boolean isAllRequired, Integer pageNumber,
			Integer recordsPerPage, Long totalResults, String orderIn, String orderBy, Long feedId,Long currentUserId) throws Exception {
		Map<List<AlertsDto>, Long> returnResult = new HashMap<>();
		Long totalCount = 0L;
		Map<List<Alerts>, Long> filteredResult = null;
		List<Alerts> alertList = null;
		Alerts alertClass = new Alerts();

		Users currentUser = usersService.find(currentUserId);
		try{
			if(currentUser != null ){
				List<Groups> userGroups = groupsService.getGroupsOfUser(currentUser.getUserId());
				List<Long> groupIds = userGroups.stream().map(Groups::getId).collect(Collectors.toList());
				JSONObject mainJson = new JSONObject();
				if(groupIds != null && !groupIds.isEmpty()){
						List<GroupAlert> groupAlerts = groupAlertService.getGroupAlertsByGroupIdList(groupIds);
					if(null !=groupAlerts && !groupAlerts.isEmpty()){
						List<GroupAlert> feedGroupAlertList = groupAlerts.stream()
								.filter(p ->  p.getType().equalsIgnoreCase(ElementConstants.FEED_CLASSIFICATION_TYPE))
								.collect(Collectors.toList());
						List<GroupAlert> jurisdictionGroupAlertList = groupAlerts.stream()
								.filter(p ->  p.getType().equalsIgnoreCase(ElementConstants.JURISDICTION_TYPE_STRING))
								.collect(Collectors.toList());
						
						List<Long> feedIds = new ArrayList<>();
						if(feedGroupAlertList != null && !feedGroupAlertList.isEmpty()){
							List<Long> classificationIds = feedGroupAlertList.stream().filter(p -> null != p.getListTypeId()).map(GroupAlert::getListTypeId).collect(Collectors.toList())
									.stream().map(ListItem::getListItemId).collect(Collectors.toList());
							if(classificationIds != null && !classificationIds.isEmpty()){
								List<FeedManagement> feedsByClassification = feedManagementService.getFeedsByTypeIds(classificationIds);
								feedIds = feedsByClassification.stream().map(FeedManagement::getFeed_management_id)
										.collect(Collectors.toList());
							}
							/*if(null == feedIds && feedIds.isEmpty()){
								List<FeedGroups> feedGroups = feedManagementService.getFeedGroupsByGroupId(groupIds);
								feedGroups.stream().filter(s -> null != s.getGroupId()).forEach(p -> Hibernate.initialize(p.getGroupId()));
								feedIds = feedGroups.stream().map(FeedGroups::getFeedId).collect(Collectors.toList())
										.stream().map(FeedManagement::getFeed_management_id).collect(Collectors.toList());
							}*/
						}/*else{
							List<FeedGroups> feedGroups = feedManagementService.getFeedGroupsByGroupId(groupIds);
							feedGroups.stream().filter(s -> null != s.getGroupId()).forEach(p -> Hibernate.initialize(p.getGroupId()));
							feedIds = feedGroups.stream().map(FeedGroups::getFeedId).collect(Collectors.toList())
									.stream().map(FeedManagement::getFeed_management_id).collect(Collectors.toList());
						}*/
						List<Long> jurisdictionIds = new ArrayList<>();
						if(jurisdictionGroupAlertList != null && !jurisdictionGroupAlertList.isEmpty()){
							List<GroupAlert> geoSettingOnList = jurisdictionGroupAlertList.stream()
									.filter(j -> null != j.getGeoLocationSetting())
									.filter(x -> x.getGeoLocationSetting().equalsIgnoreCase("On")).collect(Collectors.toList());
							if(geoSettingOnList != null && !geoSettingOnList.isEmpty() && geoSettingOnList.size() > 0){
								if(null != currentUser.getCountryId() && null != currentUser.getCountryId().getListItemId()){
									jurisdictionIds.add(currentUser.getCountryId().getListItemId());
								}
							}else{
								jurisdictionIds = jurisdictionGroupAlertList.stream().filter(p -> null != p.getListTypeId()).map(GroupAlert::getListTypeId).collect(Collectors.toList())
										.stream().map(ListItem::getListItemId).collect(Collectors.toList());
							}
						}
						
						if(feedIds != null && !feedIds.isEmpty()){
							StringBuilder  builder  = new StringBuilder();
							for (Long feed : feedIds){
								builder.append(feed).append("#");
							}
							String idString = builder.toString();
							idString = idString.substring(0, idString.length()-1);
							
							String[] defaultfilter = idString.split("#");
							List<String> listDefaultFilter = new ArrayList<String>(Arrays.asList(defaultfilter));
							
							JSONObject mainJson1 = new JSONObject(filterDto.getFilterModel());
							String idStringUser = "";
							if(null != mainJson1.names() &&  mainJson1.has("feed"))
							{
								
								idStringUser = mainJson1.getJSONObject("feed").getJSONObject("condition1").getString("filter");
							}
							String[] userFilter = idStringUser.split("#");
							List<String> listUserFilter = new ArrayList<String>(Arrays.asList(userFilter));
							StringBuilder  builder1  = new StringBuilder();
							if(listUserFilter.get(0).equals(""))
							{
							   idString = builder.toString();
							   JSONObject columnJson = new JSONObject();
							   JSONObject conditionJson = new JSONObject();
							   conditionJson.put("type", "multiSelect");
							   conditionJson.put("filterType", "text");
							   conditionJson.put("filter", idString);
								
								columnJson.put("condition1", conditionJson);
								mainJson1.put("feed", columnJson);
							    filterDto.setFilterModel(mainJson1.toString());
							}
							else
							{
								listUserFilter.retainAll(listDefaultFilter);
							
							if(listUserFilter.isEmpty())
							{
							    builder1.append(-1);
							}
							else
							{
							for (String feed : listUserFilter){
								builder1.append(feed).append("#");
							}
							}
							String idString3 = builder1.toString();
							
							JSONObject columnJson = new JSONObject();
							JSONObject conditionJson = new JSONObject();
							conditionJson.put("type", "multiSelect");
							conditionJson.put("filterType", "text");
							conditionJson.put("filter", idString3);
							
							columnJson.put("condition1", conditionJson);
							mainJson1.put("feed", columnJson);
						    filterDto.setFilterModel(mainJson1.toString());
							}
							
						    Iterator iterator = mainJson1.keys();
				            while(iterator.hasNext()){
				                String key = iterator.next().toString();
				                mainJson.put(key,mainJson1.optJSONObject(key));
				            }
							
						}
						
						
						if(jurisdictionIds != null && !jurisdictionIds.isEmpty()){
							StringBuilder  builder  = new StringBuilder();
							for (Long jurisdictionId : jurisdictionIds){
								builder.append(jurisdictionId).append("#");
							}
							String idString = builder.toString();
							idString = idString.substring(0, idString.length()-1);
							
							String[] defaultfilter = idString.split("#");
							List<String> listDefaultFilter = new ArrayList<String>(Arrays.asList(defaultfilter));
							
							JSONObject mainJson1 = new JSONObject(filterDto.getFilterModel());
							String idStringUser = "";
							if(null != mainJson1.names() &&  mainJson1.has("jurisdiction"))
							{
								
								idStringUser = mainJson1.getJSONObject("jurisdiction").getJSONObject("condition1").getString("filter");
							}
							String[] userFilter = idStringUser.split("#");
							List<String> listUserFilter = new ArrayList<String>(Arrays.asList(userFilter));
							StringBuilder  builder1  = new StringBuilder();
							if(listUserFilter.get(0).equals(""))
							{
							   idString = builder.toString();
							   JSONObject columnJson = new JSONObject();
							   JSONObject conditionJson = new JSONObject();
							   conditionJson.put("type", "multiSelect");
							   conditionJson.put("filterType", "text");
							   conditionJson.put("filter", idString);
								
								columnJson.put("condition1", conditionJson);
								mainJson1.put("jurisdiction", columnJson);
							    filterDto.setFilterModel(mainJson1.toString());
							}
							else
							{
								listUserFilter.retainAll(listDefaultFilter);
							
							if(listUserFilter.isEmpty())
							{
							    builder1.append(-1);
							}
							else
							{
							for (String feed : listUserFilter){
								builder1.append(feed).append("#");
							}
							}
							String idString3 = builder1.toString();
							
							JSONObject columnJson = new JSONObject();
							JSONObject conditionJson = new JSONObject();
							conditionJson.put("type", "multiSelect");
							conditionJson.put("filterType", "text");
							conditionJson.put("filter", idString3);
							
							columnJson.put("condition1", conditionJson);
							mainJson1.put("jurisdiction", columnJson);
						    filterDto.setFilterModel(mainJson1.toString());
							}
							
						    Iterator iterator = mainJson1.keys();
				            while(iterator.hasNext()){
				                String key = iterator.next().toString();
				                mainJson.put(key,mainJson1.optJSONObject(key));
				            }
							
					}
				 
				}
					 if(groupIds != null && !groupIds.isEmpty()){
						  StringBuilder  builder  = new StringBuilder();
							for (Long group : groupIds){
								builder.append(group).append("#");
							}
							String idString = builder.toString();
							idString = idString.substring(0, idString.length()-1);
							
							String[] defaultfilter = idString.split("#");
							List<String> listDefaultFilter = new ArrayList<String>(Arrays.asList(defaultfilter));
							
							JSONObject mainJson1 = new JSONObject(filterDto.getFilterModel());
							String idStringUser = "";
							if(null != mainJson1.names() &&  mainJson1.has("groupLevel"))
							{
								
								idStringUser = mainJson1.getJSONObject("groupLevel").getJSONObject("condition1").getString("filter");
							}
							String[] userFilter = idStringUser.split("#");
							List<String> listUserFilter = new ArrayList<String>(Arrays.asList(userFilter));
							StringBuilder  builder1  = new StringBuilder();
							if(listUserFilter.get(0).equals(""))
							{
							   idString = builder.toString();
							   JSONObject columnJson = new JSONObject();
							   JSONObject conditionJson = new JSONObject();
							   conditionJson.put("type", "multiSelect");
							   conditionJson.put("filterType", "text");
							   conditionJson.put("filter", idString);
								
								columnJson.put("condition1", conditionJson);
								mainJson1.put("groupLevel", columnJson);
							    filterDto.setFilterModel(mainJson1.toString());
							}
							else
							{
								listUserFilter.retainAll(listDefaultFilter);
							
							if(listUserFilter.isEmpty())
							{
							    builder1.append(-1);
							}
							else
							{
							for (String feed : listUserFilter){
								builder1.append(feed).append("#");
							}
							}
							String idString3 = builder1.toString();
							
							JSONObject columnJson = new JSONObject();
							JSONObject conditionJson = new JSONObject();
							conditionJson.put("type", "multiSelect");
							conditionJson.put("filterType", "text");
							conditionJson.put("filter", idString3);
							
							columnJson.put("condition1", conditionJson);
							mainJson1.put("groupLevel", columnJson);
						    filterDto.setFilterModel(mainJson1.toString());
							}
							
						    Iterator iterator = mainJson1.keys();
				            while(iterator.hasNext()){
				                String key = iterator.next().toString();
				                mainJson.put(key,mainJson1.optJSONObject(key));
				            }
					}	
					
					
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		if (isAllRequired) {
			alertList = alertManagementDao.findAll();
		} else {
			//alertList = alertManagementDao.getAlerts(pageNumber, recordsPerPage, orderIn, orderBy, feedId);
			filteredResult = alertManagementService.filterData(alertClass, filterDto, pageNumber, recordsPerPage, totalResults, orderIn, orderBy);
			for(Entry<List<Alerts>, Long> entry : filteredResult.entrySet()){
				alertList = entry.getKey();
				totalCount = entry.getValue();
			}
			
		}
		alertList.stream().forEach(s -> s.getFeed().stream().forEach(d -> Hibernate.initialize(d)));
		//Lazy initialization for List Item of Reviewer
		alertList.stream().forEach(s -> s.getFeed().stream().forEach(d -> Hibernate.initialize(d.getReviewer())));
		//alertList.stream().filter(s -> null != s.getFeed().stream().filter(f -> f.getReviewer()).forEach(alert -> Hibernate.initialize(alert.getReviewer()));
		alertList.stream().filter(s -> null != s.getComments()).forEach(alert -> Hibernate.initialize(alert.getComments()));
		List<AlertsDto> alertDtosList = new ArrayList<AlertsDto>();
		if (alertList != null) {
			alertDtosList = alertList.stream()
					.map((feedManagementDto) -> new EntityConverter<Alerts, AlertsDto>(Alerts.class, AlertsDto.class)
							.toT2(feedManagementDto, new AlertsDto()))
					.collect(Collectors.toList());
		}
		for (Alerts alert : alertList) {
			for (AlertsDto dto : alertDtosList) {
				if (dto.getAlertId() == alert.getAlertId()) {
					// set the comments count instead of all comments to minimize the response size
					dto.setCommentsCount(alert.getComments().size());
					dto.setEntiType(alert.getEntiType());
					// set the user dto to AlertDto for complete user details
					Users user = usersDao.find(alert.getAssignee());
					AssigneeDto author = new AssigneeDto();
					if (user != null) {
						BeanUtils.copyProperties(user, author);
						dto.setAsignee(author);
					} else {
						dto.setAsignee(null);
					}
					// manually adding hte classification name from feed
					List<String> classicationList= new ArrayList<>();
					for(FeedManagement feed: dto.getFeed()) {
						if(feed !=null) {
							ListItemDto listdto= listItemService.getListItemByID(new Long(feed.getType()));
							classicationList.add(listdto.getDisplayName());
						}
					}
					dto.setClassification(classicationList);
					// set user Approved for alerts
					UserApprovedDto userApproved=new UserApprovedDto();
					if(alert.getUserApproved()!=null) {
						Users userForApproved = usersDao.find(alert.getUserApproved());
						BeanUtils.copyProperties(userForApproved, userApproved);
						dto.setUserApproved(userApproved);
					}else {
						dto.setUserApproved(null);
					}
					// set the alert status object from the list management
					dto.setStatuse(listItemService.getListItemByID(alert.getStatus()));
					
					dto.setJurisdictions(listItemService.getListItemByID(alert.getJurisdiction()));
					// set the alert reviewStatus object from the list management
					dto.setReviewStatusId(listItemService.getListItemByID(alert.getReviewStatusId()));
					
				}
			}
		}
		List<FeedManagement> feedList = feedManagementService.findAll();
		feedList.stream().filter(s -> null != s.getGroupLevels()).forEach(feed -> Hibernate.initialize(feed.getGroupLevels()));
		Map<Long, List<FeedGroups>> feedMap = feedList.stream()
				.collect(Collectors.toMap(FeedManagement::getFeed_management_id, FeedManagement::getGroupLevels));

		for (AlertsDto alertDto : alertDtosList) {
			List<FeedManagement> feedListNew = alertDto.getFeed();
			
			//create a set of the groups found 
			Set<Long> groupsfound = new HashSet<>();
			
			//create a final list of feedgroups collected from all the feeds for this alert
			List<FeedGroups> finalFeedgroups = new ArrayList<>();
			
			for(int i=0 ; i< feedListNew.size() ; i++)
			{
				//form a final list of feedgroups by unique groups irrespective for all the feeds for the current alert
				feedMap.get(feedListNew.get(i).getFeed_management_id()).stream().filter(s -> null != s).forEach(p -> Hibernate.initialize(p));
				feedMap.get(feedListNew.get(i).getFeed_management_id()).stream().filter(s -> null != s.getGroupId()).forEach(p -> Hibernate.initialize(p.getGroupId()));
				List<FeedGroups> feedGroupsForCurrentFeed = feedListNew.get(i).getGroupLevels();
				if(null != feedGroupsForCurrentFeed && !feedGroupsForCurrentFeed.isEmpty()){
					for(FeedGroups fg : feedGroupsForCurrentFeed){
						if(!groupsfound.contains(fg.getGroupId().getId())){
							groupsfound.add(fg.getGroupId().getId());
							finalFeedgroups.add(fg);
						}
					}
				}
				
			//if (feedMap.containsKey(feedListNew.get(i).getFeed_management_id())) {
				//feedMap.get(feedListNew.get(i).getFeed_management_id()).stream().filter(s -> null != s).forEach(p -> Hibernate.initialize(p));
				//feedMap.get(feedListNew.get(i).getFeed_management_id()).stream().filter(s -> null != s.getGroupId()).forEach(p -> Hibernate.initialize(p.getGroupId()));
				//alertDto.setFeedGroups(feedMap.get(feedListNew.get(i).getFeed_management_id()));	
				
				//Filtering after Entity Identification
				//if(null != alertDto.getFeed().get(i).getFeedName() 
					//	&& null != alertDto.getFeed().get(i).getFeedName() 
						//&& !alertDto.getFeed().get(i).getFeedName().equals(ElementConstants.ENTITY_IDENTIFICATION)) {
				if(alertDto.getIsEntityIdentified()&& alertDto.getIdentifiedEntityId()!=null 
						&& !alertDto.getStatuse().getDisplayName().equals(ElementConstants.ALERT_NEW_STATUS)
						&& !alertDto.getStatuse().getDisplayName().equals(ElementConstants.IDENTITY_REJECTED_STATUS)
						&& !alertDto.getStatuse().getDisplayName().equals(ElementConstants.ENTITY_APPROVED_NEEDED_REVIEW)
						&& !alertDto.getStatuse().getDisplayName().equals(ElementConstants.ENTITY_REJECTED_NEEDED_REVIEW)) {
					String alertMetaData = alertDto.getAlertMetaData();
					JSONObject	results = null;
					JSONObject screen  = null;
					JSONArray  watchList = null;
					if(null != alertMetaData) {					
											
						JSONObject alertMetaDataJSON = new JSONObject(alertMetaData);
						if(null != alertMetaDataJSON && alertMetaDataJSON.has("results") )							
						     results = (JSONObject) alertMetaDataJSON.get("results");
						if(null != results && results.has("screening"))
							 screen = (JSONObject) results.get("screening");
						if(null != screen && screen.has("watchlists")) 							 						 
						     watchList = screen.getJSONArray("watchlists");
						 
						if(null != watchList && watchList.length() > 0) {
						
							JSONObject currentObject;
							JSONObject attributes = null;
							String identifiedEntityId = null;
							JSONArray  filteredWatchList = new JSONArray();							
							for(i=0; i< watchList.length(); i++){
							
								currentObject = (JSONObject) watchList.get(i);
								if(null != currentObject && currentObject.has("basic_info"))
							    	attributes = (JSONObject) currentObject.get("basic_info");
								
								if(null != attributes && attributes.has("id"))
									identifiedEntityId = attributes.getString("id");
								if(null != alertDto.getIdentifiedEntityId() && null != identifiedEntityId && alertDto.getIdentifiedEntityId().equals(identifiedEntityId))
								{
									if(filteredWatchList.length() == 0)
									    filteredWatchList.put(currentObject);
								}																		
							}
							
							if(null != filteredWatchList && filteredWatchList.length() == 1)
							{
								screen.put("watchlists", filteredWatchList); 
								results.put("screening", screen);
								alertMetaDataJSON.put("results", results);
								if(null != alertMetaDataJSON)
									alertDto.setAlertMetaData(alertMetaDataJSON.toString());	
							}																			
						}												
					  }													
					}//End of Filtering after Entity Identification																							    			
			//}
			}
			alertDto.setFeedGroups(finalFeedgroups);
			
		}
		alertDtosList = timeStatusUpdater(alertDtosList);
		//List<Groups> userGroups=groupsService.getGroupsOfUser(currentUserId);
		//alertDtosList.stream().filter(a -> a.getFeedGroups())
		returnResult.put(alertDtosList, totalCount);
		return returnResult;
	}

	@Override
	@Transactional("transactionManager")
	public Long getAlertsCount() {
		return alertManagementDao.getAlertsCount();
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertsDto> getUserAlert(Long userId) {
		List<Alerts> alertList = alertManagementDao.getUserAlertByUserId(userId);
		alertList.stream().filter(s -> null != s.getFeed()).forEach(alert -> Hibernate.initialize(alert.getFeed()));
		List<AlertsDto> alertDtosList = new ArrayList<AlertsDto>();
		if (alertList != null) {
			alertDtosList = alertList.stream()
					.map((feedManagementDto) -> new EntityConverter<Alerts, AlertsDto>(Alerts.class, AlertsDto.class)
							.toT2(feedManagementDto, new AlertsDto()))
					.collect(Collectors.toList());
		}
		List<FeedManagement> feedList = feedManagementService.findAll();
		feedList.stream().filter(s -> null != s.getGroupLevels()).forEach(feed -> Hibernate.initialize(feed.getGroupLevels()));
		Map<Long, List<FeedGroups>> feedMap = feedList.stream()
				.collect(Collectors.toMap(FeedManagement::getFeed_management_id, FeedManagement::getGroupLevels));

		for (AlertsDto alertDto : alertDtosList) {
			List<FeedManagement> feedManagementList = alertDto.getFeed();
			for(FeedManagement feed : feedManagementList)
			{
			if (feedMap.containsKey(feed.getFeed_management_id())) {
				alertDto.setFeedGroups(feedMap.get(feed.getFeed_management_id()));
			}
			}
		}
		return alertDtosList;
	}

	@Override
	@Transactional("transactionManager")
	public AlertCommentsDto saveOrUpdateAlertComments(AlertCommentsDto alertCommentDto, long userId) {

		/*Users user = usersDao.find(userId);
		UsersDto author = new UsersDto();
		BeanUtils.copyProperties(user, author);*/
		UsersDtoWithRolesAndGroups author = new UsersDtoWithRolesAndGroups();
		try {
			author = usersService.getUserProfileById(userId,
					null);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		AlertComments alertComments = null;
		StringBuilder auditOperation = new StringBuilder();
		if (alertCommentDto != null && alertCommentDto.getCommentId() != null) {
			alertComments = alertCommentsService.find(alertCommentDto.getCommentId());
			if (alertComments == null) {
				try {
					alertComments = new AlertComments();
					// BeanUtils.copyProperties(alertCommentDto, alertComments);
					if (alertCommentDto.getAlertId() != null)
						alertComments.setAlertId(alertCommentDto.getAlertId());

					/*if(alertCommentDto.getCommentId() != null)
						alertComments.setCommentId(alertCommentDto.getCommentId());*/
					if(alertCommentDto.getComments() != null)
						alertComments.setComments(alertCommentDto.getComments());
					auditOperation.append("commented on an alert");
					alertComments.setCreatedDate(new Date());
					alertComments.setUpdatedDate(new Date());
					alertComments.setUpdatedBy(userId);
					alertComments = alertCommentsService.save(alertComments);
					BeanUtils.copyProperties(alertComments, alertCommentDto);
					alertCommentDto.setCommentedBy(author);
					auditOperation.append("commented");
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				Hibernate.initialize(alertComments.getAlertId().getComments());

				if (alertCommentDto.getAlertId() != null)
					alertComments.setAlertId(alertCommentDto.getAlertId());
				if (alertCommentDto.getCommentId() != null)
					alertComments.setCommentId(alertCommentDto.getCommentId());
				if (alertCommentDto.getComments() != null)
					alertComments.setComments(alertCommentDto.getComments());
				auditOperation.append("commented on an alert");
				alertComments.setUpdatedDate(new Date());
				alertComments.setUpdatedBy(userId);

				// BeanUtils.copyProperties(alertCommentDto, alertComments);
				alertCommentsService.saveOrUpdate(alertComments);
				BeanUtils.copyProperties(alertComments, alertCommentDto);
				alertCommentDto.setCommentedBy(author);
			}
		} else {
			try {
				alertComments = new AlertComments();
				// BeanUtils.copyProperties(alertCommentDto, alertComments);
				if (alertCommentDto.getAlertId() != null)
					alertComments.setAlertId(alertCommentDto.getAlertId());
				if (alertCommentDto.getCommentId() != null)
					alertComments.setCommentId(alertCommentDto.getCommentId());
				if (alertCommentDto.getComments() != null)
					alertComments.setComments(alertCommentDto.getComments());
				auditOperation.append("commented on an alert");
				alertComments.setCreatedDate(new Date());
				alertComments.setUpdatedDate(new Date());
				alertComments.setUpdatedBy(userId);
				alertComments = alertCommentsService.save(alertComments);

				BeanUtils.copyProperties(alertComments, alertCommentDto);
				alertCommentDto.setCommentedBy(author);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
		log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + auditOperation.toString() + " ");
		log.setName(alertCommentDto.getAlertId().toString());
		log.setUserId(userId);
		log.setTypeId(alertCommentDto.getAlertId().getAlertId());
		//if(status!=null)
		//log.setSubType(status);
		auditLogDao.create(log);
		return alertCommentDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertCommentsDto> getAlertCommentsByAlertId(Long alertId) {
		Alerts alert = alertManagementService.find(alertId);
		Hibernate.initialize(alert.getComments());
		List<AlertComments> commentsList = alert.getComments();
		List<AlertCommentsDto> commentsDtoList = new ArrayList<AlertCommentsDto>();

		commentsDtoList = commentsList.stream()
				.map((commentsDto) -> new EntityConverter<AlertComments, AlertCommentsDto>(AlertComments.class,
						AlertCommentsDto.class).toT2(commentsDto, new AlertCommentsDto()))
				.collect(Collectors.toList());
		commentsDtoList.stream().forEach(comment -> comment.setAlertId(null));

		for (AlertComments comment : commentsList) {
			for (AlertCommentsDto commentsDto : commentsDtoList) {
				if (commentsDto.getCommentId() == comment.getCommentId()) {
					/*Users user = usersDao.find(comment.getUpdatedBy());
					UsersDto author = new UsersDto();

					Users user = usersDao.find(comment.getUpdatedBy());
					UsersDtoWithRolesAndGroups author = new UsersDtoWithRolesAndGroups();

					if (user != null) {
						BeanUtils.copyProperties(user, author);
					}
					commentsDto.setCommentedBy(author);*/

					try {
						UsersDtoWithRolesAndGroups usersDtoWithRolesAndGroups = usersService.getUserProfileById(comment.getUpdatedBy(),
								null);
						commentsDto.setCommentedBy(usersDtoWithRolesAndGroups);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*
					 * List<DocumentVaultDTO> docVault =
					 * documentService.getDocumentVaultByEntityIdAndDocflag(17,
					 * commentsDto.getCommentId()); commentsDto.setAttachment(docVault);
					 */
				}

			}
		}

		// bring attachment details for each comment from the document vault using doc
		// flag(17 for alert comments)
		for (AlertCommentsDto comment : commentsDtoList) {
			List<DocumentVaultDTO> docVault = documentService.getDocumentVaultByEntityIdAndDocflag(17,
					comment.getCommentId());
			comment.setAttachment(docVault);
		}
		return commentsDtoList;
	}

	@Override
	public List<MyAlertsDto> getMyAlerts(Long userId) {
		List<MyAlertsDto> myAlerts = new ArrayList<>();
		List<StatusCountDto> myStatusCount = new ArrayList<>();
		myStatusCount = alertManagementDao.getMyAlerts(userId);
		List<Alerts> alerts = alertManagementDao.findAll().stream().filter(s -> null != s.getAssignee()).filter(a -> a.getAssignee().equals(userId))
				.collect(Collectors.toList());
		Integer totalCount=0;
		for(StatusCountDto status:myStatusCount) {
			totalCount=totalCount+status.getCount();
		}
		if (null != alerts) {
			MyAlertsDto alert = new MyAlertsDto();
			alert.setStatusCountList(myStatusCount);
			alert.setTotalCount(totalCount);
			myAlerts.add(alert);
		}
		return myAlerts;
	}

	@Override
	public List<AssociatedAlertsDto> getAssociatedAlerts(Long userId) {
		List<AssociatedAlertsDto> associatedAlerts = new ArrayList<>();
		List<StatusCountDto> statusCounts = new ArrayList<>();
		List<Groups> userGroups = groupsService.getGroupsOfUser(userId);
		Set<Long> groupIds = userGroups.stream().map(g -> g.getId()).collect(Collectors.toSet());
		//for (Long id : groupIds) {
			/*List<Long> singleId = new ArrayList<>();
			singleId.add(id);
			Groups group = groupsService.find(id);*/
		try{
			Map<Long,Long> resultantGroupIds= new HashMap<>();
			if(null != groupIds && groupIds.size() ==0 ){
				return associatedAlerts;
			}
			statusCounts = alertManagementDao.getAlertsOfGroups(groupIds);
			if ( null != statusCounts && statusCounts.size() != 0) {
				for (StatusCountDto statusCount : statusCounts) {
					if(statusCount.getGroupId()!=null) {
						resultantGroupIds.put(statusCount.getGroupId(), statusCount.getGroupId());
					}
					
				}
				Groups eachGroup=null;
				
			if (resultantGroupIds != null && resultantGroupIds.size() > 0) {
				for (Long groupKey : resultantGroupIds.keySet()) {
					for (StatusCountDto statusCount : statusCounts) {
						eachGroup = groupsService.find(statusCount.getGroupId());
						if (eachGroup != null) {
							String groupName = eachGroup.getName();
							if (associatedAlerts != null && associatedAlerts.size() > 0) {
								if (associatedAlerts.stream().filter(s -> null != s.getGroupName()).filter(o -> o.getGroupName().equals(groupName))
										.findFirst().isPresent()) {
									List<AssociatedAlertsDto> filteredAlerts = associatedAlerts.stream().filter(s -> null != s.getGroupName())
											.filter(o -> o.getGroupName().equals(groupName))
											.collect(Collectors.toList());
									if (filteredAlerts != null && filteredAlerts.size() > 0) {
										AssociatedAlertsDto existingAssociatedAlert = filteredAlerts.get(0);
										if (existingAssociatedAlert != null) {
											String status=statusCount.getStatus();
											if (status != null) {
												if (!existingAssociatedAlert.getStatusCountList().stream().filter(s -> null != s.getStatus())
														.filter(s -> s.getStatus().equals(status)).findFirst()
														.isPresent())
													existingAssociatedAlert.getStatusCountList().add(statusCount);
											}
										}
									}
								}else {
									List<StatusCountDto> listStatusCounts = new ArrayList<>();
									listStatusCounts.add(statusCount);
									AssociatedAlertsDto alert = new AssociatedAlertsDto();
									alert.setStatusCountList(listStatusCounts);
									alert.setGroupName(groupName);
									associatedAlerts.add(alert);
								}
							} else {
								List<StatusCountDto> listStatusCounts = new ArrayList<>();
								listStatusCounts.add(statusCount);
								AssociatedAlertsDto alert = new AssociatedAlertsDto();
								alert.setStatusCountList(listStatusCounts);
								alert.setGroupName(groupName);
								associatedAlerts.add(alert);
							}
						}
					}
				}
			}
			if (associatedAlerts != null && associatedAlerts.size() > 0) {
				for (AssociatedAlertsDto eachDto : associatedAlerts) {
					List<StatusCountDto> finalStatuses = eachDto.getStatusCountList();
					if (finalStatuses != null && finalStatuses.size() > 0)
						eachDto.setTotalCount(finalStatuses.size());
					else
						eachDto.setTotalCount(0);
				}
			}
			
				/*for (StatusCountDto statusCount : statusCounts) {
					totalCount = totalCount + statusCount.getCount();
					 group=groupsService.find(statusCount.getGroupId());
				}
				AssociatedAlertsDto alert = new AssociatedAlertsDto();
				alert.setStatusCountList(statusCounts);
				alert.setTotalCount(totalCount);
				if(group!=null)
				alert.setGroupName(group.getName());
				associatedAlerts.add(alert);*/
			}
				
		}
		catch(Exception ex)
		{
			ElementLogger.log(ElementLoggerLevel.ERROR,"No associated alerts",ex, this.getClass());			
		}

		return associatedAlerts;
	}

	@Override
	public PastDueAlertsDto getPastDueAlerts(Long userId) {
		PastDueAlertsDto pastDueAlerts = new PastDueAlertsDto();
		String period = null;
		String finalPeriod = null;
		Integer rangeValue = 0;
		Integer finalRangeValue = 0;
		Date logDate = null;
		Integer finalDaysDifference = 0;
		Integer totalDueCount = 0;
		List<PastDueMonthCountDto> mounthAndCount=new ArrayList<>();
		List<Settings> rangeSetting = settingsService.findAll().stream().filter(s -> null != s.getName())
				.filter(setting -> setting.getName().equalsIgnoreCase("Time Status Medium Period (Long Term)"))
				.collect(Collectors.toList());
		if (rangeSetting != null && rangeSetting.size() > 0) {
			Settings range = rangeSetting.get(0);
			period = range.getDefaultValue();
			//rangeValue = range.getSliderTo();
		}
		List<Settings> periodSetting = settingsService.findAll().stream().filter(s -> null != s.getName())
				.filter(setting -> setting.getName().equalsIgnoreCase("Time Status Medium Slider (Long Term)"))
				.collect(Collectors.toList());
		if (periodSetting != null && periodSetting.size() > 0) {
			Settings periodValue = periodSetting.get(0);
			rangeValue = periodValue.getSliderTo();
		}
		if (period.equalsIgnoreCase("days")) {
			finalRangeValue = rangeValue;
		}
		if (period.equalsIgnoreCase("Years")) {
			finalRangeValue = rangeValue * 365;
		}
		if (period.equalsIgnoreCase("Months")) {
			finalRangeValue = Math.round(rangeValue * 30.417f);
		}
		if (period.equalsIgnoreCase("Weeks")) {
			finalRangeValue = rangeValue * 7;
		}
		List<Alerts> alerts = alertManagementDao.findAll().stream().filter(s -> null != s.getAssignee()).filter(a -> a.getAssignee().equals(userId))
				.collect(Collectors.toList());
		List<Alerts> filteredAlerts= new ArrayList<>();
		for (Alerts alert : alerts) {
			logDate = auditLogService.getLatestStatusChangeOfAlert(alert.getAlertId());

			Date rangeDate = DateUtils.addDays(new Date(), finalRangeValue);
			if (logDate != null) {

				Long days = (logDate.getTime() - new Date().getTime()) / 86400000;
				finalDaysDifference = Math.abs(days.intValue());
			}
			// ChronoUnit.DAYS.between(logDate, new Date());
			if (finalRangeValue < finalDaysDifference) {
				totalDueCount = totalDueCount + 1;
				filteredAlerts.add(alert);
			}
		}
		/*Map<Integer,Integer> monthCount= new HashMap<>();
		if(filteredAlerts!=null && filteredAlerts.size()>0) {
			for(Alerts filteredAlert:filteredAlerts) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(filteredAlert.getUpdatedDate());
				int month=cal.get(Calendar.MONTH);
			}
		}*/
	/*	List<Long> filteredAlertIds= new ArrayList<>();
		if(filteredAlerts!=null && filteredAlerts.size()>0) {
			for (Alerts filteredAlert : filteredAlerts) {
				filteredAlertIds.add(filteredAlert.getAlertId());
				if (filteredAlertIds != null && filteredAlertIds.size() > 0) {
					//mounthAndCount=alertManagementDao.getAlertsGroupByMonth(filteredAlertIds);
					Collections.sort(list);
				}
			}
		}*/
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int currentWeek= Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
		Map<Integer,Integer> previousWeekCount= new HashMap<>();
		Map<Integer,Integer> currentWeekCount= new HashMap<>();
		Map<Integer, Integer> monthCount = new HashMap<>();
		if (filteredAlerts != null && filteredAlerts.size() > 0) {
			for (Alerts filteredAlert : filteredAlerts) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(filteredAlert.getUpdatedDate());
				int month = cal.get(Calendar.MONTH);
				int year = cal.get(Calendar.YEAR);
				int week=cal.get(Calendar.WEEK_OF_YEAR);
				//month count
				if (monthCount.containsKey(month) && currentYear == year) {
					Integer value = monthCount.get(month);
					value = value + 1;
					monthCount.put(month, value);
				} else {
					monthCount.put(month, 1);
				}
				//current week count
				if (currentWeekCount.containsKey(week) && currentWeek == week) {
					
					Integer value = currentWeekCount.get(week);
					value = value + 1;
					currentWeekCount.put(week, value);
				}
				
				//previous week count
				if (week > 1 && currentWeek>1) {
					if (previousWeekCount.containsKey(week - 1) && currentWeek - 1 == week-1) {
						Integer value = currentWeekCount.get(week);
						value = value + 1;
						previousWeekCount.put(week-1, value);
					} 
				}

			}
		}
		Integer currentWeekCountValue=0;
		Integer previousWeekCountValue=0;
		String thanLastWeek=null;
		if(!currentWeekCount.isEmpty()) {
			Iterator<Map.Entry<Integer, Integer>> iterator = currentWeekCount.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<Integer, Integer> entry =  iterator.next();
				currentWeekCountValue=entry.getValue();
			}
		}
			
		if(!previousWeekCount.isEmpty()) {
			Iterator<Map.Entry<Integer, Integer>> iterator = previousWeekCount.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<Integer, Integer> entry =  iterator.next();
				previousWeekCountValue=entry.getValue();
			}
		}
			if(currentWeekCountValue==previousWeekCountValue) {
				thanLastWeek="Equals last week";
			}
			else if(currentWeekCountValue>previousWeekCountValue) {
				Double percentage= Double.valueOf((previousWeekCountValue*100)/currentWeekCountValue);
				thanLastWeek=percentage+"% more than last week";
			}else {
				Double percentage= Double.valueOf((currentWeekCountValue*100)/previousWeekCountValue);
				thanLastWeek=percentage+"% less than last week";
			}
		Iterator<Map.Entry<Integer, Integer>> iterator = monthCount.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<Integer, Integer>  entry = iterator.next(); 
			PastDueMonthCountDto pastCount = new PastDueMonthCountDto();
			pastCount.setMonth(entry.getKey());
			pastCount.setCount(entry.getValue());
			pastCount.setYear(currentYear);
			mounthAndCount.add(pastCount);
		}
		
		
		
		pastDueAlerts.setMonthCountList(mounthAndCount);
		pastDueAlerts.setLatestPastDueCount(totalDueCount);
		pastDueAlerts.setPercentageThanLastWeek(thanLastWeek);
		return pastDueAlerts;
	}

	@Override
	@Transactional("transactionManager")
	public List<Alerts> getAlertsByfeed(Long feedID) {
		
		return alertManagementDao.getAlertsByFeed(feedID);
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean deleteComment(Long alertId, Long commentId, long userId) {
		boolean status = alertManagementDao.deleteComment(alertId, commentId, userId);
		List<DocumentVaultDTO> commentDocumentsList = documentService.getDocumentVaultByEntityIdAndDocflag(17,
				commentId);
		if (commentDocumentsList != null && commentDocumentsList.size() > 0) {
			for (DocumentVaultDTO dto : commentDocumentsList) {
				documentService.softDeleteDocumentById(dto.getDocId(), userId);
			}
		}
		
		Users user = usersDao.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		//AlertComments comment=alertCommentsService.find(commentId);
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ALERT_MANAGEMENT_TYPE, null, ElementConstants.ALERT_MANAGEMENT_TYPE);
		log.setDescription(user.getFirstName() + " " + user.getLastName() + " deleted a comment ");
		log.setName(ElementConstants.ALERT_MANAGEMENT_TYPE);
		log.setUserId(userId);
		log.setType(ElementConstants.ALERT_MANAGEMENT_TYPE);
		log.setTypeId(alertId);
		auditLogDao.create(log);
		return status;
	}

	@Override
	@Transactional("transactionManager")
	public List<Alerts> findAllWithRisk() {
		return alertManagementDao.findAllWithRisk();
	}

	@Override
	public List<AlertsDto> timeStatusUpdater(List<AlertsDto> alertList) throws Exception {
		//List<Alerts> alertList = alertManagementService.findAll();
		
		List<SettingsDto> settings = settingsService.fetchSystemSettingsByType("ALERT_MANAGEMENT");
		List<SettingsDto> settingsSlider = settings.stream()
				.filter(setting -> setting.getType().equalsIgnoreCase("Slider")).collect(Collectors.toList());
		Map<String, Integer> nameSliderToMap = new HashMap<String, Integer>();
		settingsSlider.stream().forEach(setting -> {
			if (setting.getDefaultValue().equalsIgnoreCase("Days")) {
				nameSliderToMap.put(setting.getName(), setting.getSliderTo());
			}
			if (setting.getDefaultValue().equalsIgnoreCase("Months")) {
				int periodValue = setting.getSliderTo();
				periodValue = Math.round(periodValue * 30.417f);
				nameSliderToMap.put(setting.getName(), periodValue);

			}
			if (setting.getDefaultValue().equalsIgnoreCase("Years")) {
				int periodValue = setting.getSliderTo();
				periodValue = periodValue * 365;
				nameSliderToMap.put(setting.getName(), periodValue);
			}
			if (setting.getDefaultValue().equalsIgnoreCase("Weeks")) {
				int periodValue = setting.getSliderTo();
				periodValue = periodValue * 7;
				nameSliderToMap.put(setting.getName(), periodValue);
			}
		});
		for (AlertsDto alert : alertList) {
			Date logDate = null;
			//Date alertCreatedDate = alert.getCreatedDate();
			logDate = auditLogService.getLatestStatusChangeOfAlert(alert.getAlertId());
			if(logDate == null){
				logDate = alert.getCreatedDate();
			}
			Integer finalDaysDifference = 0;
			Integer shortTerm = nameSliderToMap.get("Time Status High Slider (Short Term)");
			Integer longTerm = nameSliderToMap.get("Time Status Medium Slider (Long Term)");
			
			
			if (logDate != null) {
				Long days = (logDate.getTime() - new Date().getTime()) / 86400000;
				finalDaysDifference = Math.abs(days.intValue());
			}
			
			if(null != shortTerm && finalDaysDifference <= shortTerm){
				alert.setTimeInStatus("Short Term");
				alert.setTimeStatusDuration(finalDaysDifference);
			}else if (null != longTerm && null != shortTerm 
					&& finalDaysDifference <= longTerm
					&& finalDaysDifference > shortTerm){
				alert.setTimeInStatus("Long Term");
				alert.setTimeStatusDuration(finalDaysDifference);
			}else if(null != longTerm && finalDaysDifference > longTerm){
				alert.setTimeInStatus("Past due");
				alert.setTimeStatusDuration(finalDaysDifference);
			}
		}
		return alertList;
	}
	
	public ByteArrayOutputStream convertToCSV(List<AlertsDto> alertDtos) {
		List<ALertsCSVDto> csvDto = new ArrayList<ALertsCSVDto>();
		List<Groups>allGroups=groupsService.findAll();
		
		for (AlertsDto alert : alertDtos) {
			ALertsCSVDto dto = new ALertsCSVDto();

			if (null != alert.getAlertId()) {
				dto.setAlertId(alert.getAlertId());
			}
			if (null != alert.getCreatedDate()) {
				dto.setCreatedDate(alert.getCreatedDate());
			}
			if (null != alert.getEntityName()) {
				dto.setEntityName(alert.getEntityName());
			} else {
				dto.setEntityName("");
			}
			if (null != alert.getCustomerId()) {
				dto.setCustomerId(alert.getCustomerId());
			} else {
				dto.setCustomerId("");
			}
			if (null != alert.getWatchList()) {
				dto.setWatchList(alert.getWatchList());
			} else {
				dto.setWatchList("");
			}
			if (null != alert.getConfidenceLevel()) {
				dto.setConfidenceLevel(alert.getConfidenceLevel());
			} else {
				dto.setConfidenceLevel(0.0);
			}
			List<FeedManagement> feedList = alert.getFeed();
			for(FeedManagement feed : feedList)
			{
			if (null != alert.getFeed() && alert.getFeed().size() > 0 && null != feed.getFeedName()) {
				dto.setFeed(feed.getFeedName());
			} else {
				dto.setFeed("");
			}
			}
			if (null != alert.getGroupLevel()) {
				for(Groups currentGroups:allGroups) {
					if(currentGroups.getId().compareTo(alert.getGroupLevel().longValue())==0) {
						dto.setGroupLevel(currentGroups.getName());
						break;
					}
				}
			} else {
				dto.setGroupLevel("");
			}
			if (null != alert.getAsignee() && null != alert.getAsignee().getFirstName()
					&& null != alert.getAsignee().getLastName()) {
				dto.setAsignee(alert.getAsignee().getFirstName() + " " + alert.getAsignee().getLastName());
			} else {
				dto.setAsignee("Unassigned");
			}
			if (null != alert.getStatuse() && null != alert.getStatuse().getDisplayName()) {
				dto.setStatuse(alert.getStatuse().getDisplayName());
			} else {
				dto.setStatuse("");
			}
			if (null != alert.getRiskIndicators()) {
				dto.setRiskIndicators(alert.getRiskIndicators());
			} else {
				dto.setRiskIndicators("");
			}
			csvDto.add(dto);
		}
		ByteArrayOutputStream bos1=prepareCSV(csvDto);
		//System.out.println(bos1.toByteArray().length);
		return bos1;
//		Object[] alertObjects = csvDto.toArray();
//		File csvFileName=new File("/home/ahextech/" + new Date().getDate() + "-"
//					+ new Date().getMonth() + "-152" + new Date().getYear() + ".csv");
//		try (OutputStream outputStream = new FileOutputStream(csvFileName)){
//				bos1.writeTo(outputStream);
//		} catch (Exception e) {
//			System.out.println("Error while generating csv from data. Error message : " + e.getMessage());
//			e.printStackTrace();
//		}
	}
	
	
	private ByteArrayOutputStream prepareCSV(List<ALertsCSVDto> csvDto) {
		StringBuilder sb = null;
		String csvFile = null;
		int count = 1;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			final String FILE_HEADER = "S.No,ALERT ID,CUSTOMER ID,WL ID,ENTITY NAME,CONFIDENCE LEVEL(%),CREATED,FEED,GROUP LEVEL,ASSIGNEE,STATUS,RISK INDICATOR";
			sb = new StringBuilder();
			sb.append(FILE_HEADER);
			sb.append(System.getProperty("line.separator"));
			for(ALertsCSVDto dto: csvDto) {
				sb.append(count++);
				sb.append(",");
				sb.append(dto.getAlertId());
				sb.append(",");
				sb.append(dto.getCustomerId());
				sb.append(",");
				sb.append(dto.getWatchList());
				sb.append(",");
				sb.append(dto.getEntityName());
				sb.append(",");
				sb.append(dto.getConfidenceLevel());
				sb.append(",");
				sb.append(dto.getCreatedDate());
				sb.append(",");
				sb.append(dto.getFeed());
				sb.append(",");
				sb.append(dto.getGroupLevel());
				sb.append(",");
				sb.append(dto.getAsignee());
				sb.append(",");
				sb.append(dto.getStatuse());
				sb.append(",");
				sb.append(dto.getRiskIndicators());
				sb.append(System.getProperty("line.separator"));
			}
			csvFile = sb.toString();
			bos.write(csvFile.getBytes());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return bos;
	}

	@Override
	public String getScreeningResultId(MultipartFile uploadFile, double confidence) throws Exception{
		String result = null;
		File responseFile=null;
		try {
			CSVParser records = null;
			
			if(!"csv".equalsIgnoreCase(FilenameUtils.getExtension(uploadFile.getOriginalFilename())))
				throw new NoDataFoundException(ElementConstants.ACCEPT_FILE_MSG);
			
			try {
				records = CSVFormat.EXCEL.withHeader().parse(new InputStreamReader(uploadFile.getInputStream(),StandardCharsets.UTF_8));
			}catch(IllegalArgumentException e) {
				throw new NoDataFoundException(ElementConstants.HEADER_MISSED);
			}
			
			Map<String, Integer> map= records.getHeaderMap();
			if(!map.containsKey("Entity Type"))
				throw new NoDataFoundException(ElementConstants.ENTITY_TYPE_MISSED);
			else if(!map.containsKey("Entity ID"))
				throw new NoDataFoundException(ElementConstants.ENTITY_ID_MISSED);
			else if(!map.containsKey("Entity Name"))
				throw new NoDataFoundException(ElementConstants.ENTITY_NAME_MISSED);
			else if(!map.containsKey("Birth Date/Registration Date"))
				throw new NoDataFoundException(ElementConstants.BIRTH_DATE_MISSED);
			else if(!map.containsKey("Birth Place/Country of Registration"))
				throw new NoDataFoundException(ElementConstants.BIRTH_PLACE_MISSED);
			else if(map.size() != 5 )
				throw new NoDataFoundException(ElementConstants.EXTRA_COLUMNS_EXISTED);
			
			List<CSVRecord> csvRecord= records.getRecords();
			if(csvRecord.isEmpty() || csvRecord.size()==0)
				throw new NoDataFoundException(ElementConstants.NO_RECORDS_FOUND);
			
			String jsonString=CsvToJsonUtil.convertCsvToJson(csvRecord,listItemService.getListItemByListType(ElementConstants.JURISDICTION_STRING), confidence);
			ElementLogger.log(ElementLoggerLevel.INFO,"screening payload :"+jsonString, this.getClass());
			responseFile = CsvToJsonUtil.convertJsontoCsv(jsonString);
			String[] response = ServiceCallHelper.postFileToServer(screeningAlertsUrl, responseFile);
			if("0".equals(response[0])) 
				throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			if("200".equals(response[0])) 
				result = response[1];	
		}
		finally {
			if(responseFile!=null && responseFile.exists())
				responseFile.delete();
		}
		if(result == null)
			throw new Exception(ElementConstants.INTERNAL_SERVER_ERROR);
		return result;
	}
	
	@Override
	public BatchScreeningResultDto getScreeningResults(String id) throws Exception{
		BatchScreeningResultDto batchScreeningResult = null;
		try {
			String[] response = ServiceCallHelper.getDataFromServer(screeningAlertsUrl+"/"+id);
			if("0".equals(response[0]))
				return batchScreeningResult;
				
			if("200".equals(response[0])) {
				JSONObject json = new JSONObject(response[1]);
				batchScreeningResult = new BatchScreeningResultDto();
				if(json.has("status"))
					batchScreeningResult.setStatus(json.getString("status"));
				if(json.has("workflow_status"))
					batchScreeningResult.setWorkflowStatus(json.getString("workflow_status"));
				if(json.has("stage"))
					batchScreeningResult.setStage(json.getString("stage"));
				if(json.has("message"))
					batchScreeningResult.setMessage(json.getString("message"));
				if(json.has("stats") && !json.isNull("stats")) {
					JSONObject stats = (JSONObject)json.getJSONObject("stats");
					if(stats.has("Validation") && !stats.isNull("Validation")) {
						JSONObject validation = stats.getJSONObject("Validation");
						if(validation.has("Bad"))
							batchScreeningResult.setBadRecords(validation.getInt("Bad"));
						if(validation.has("Total"))
							batchScreeningResult.setTotalEntities(validation.getInt("Total"));
						if(validation.has("Warnings"))
							batchScreeningResult.setWarnings(validation.getInt("Warnings"));
						
						batchScreeningResult.setValidEntity(batchScreeningResult.getTotalEntities()-batchScreeningResult.getBadRecords());
					}	
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.INTERNAL_SERVER_ERROR, e, this.getClass());
		}
		return batchScreeningResult;
	}
	
	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getAlertsFromServer(String name, String givenName, String jurisdiction, String screeningType,
			String familyName, String birthDate, boolean isWrapper) throws Exception {/*
		String url = BIGDATA_ALERTS_URL + "getAlert?name=" + encode(name) + "&givenName=" + encode(givenName)
				+ "&jurisdiction=" + encode(jurisdiction) + "&screening_type=" + encode(screeningType) + "&familyName="
				+ encode(familyName) + "&birthDate=" + encode(birthDate);
		String response = getDataFromServer(url);
		if (isWrapper) {
			return response;
		} else {
			if (response != null) {
				CreateAlertDto alertDto = new CreateAlertDto();
				List<ResponseDto> responseDtoList = new ArrayList<>();
				List<AlertMetaDataDto> metaDataListDto = new ArrayList<>();
				JSONObject responseJson = new JSONObject(response);
				if (responseJson != null && responseJson.has("Result")
						&& responseJson.get("Result") instanceof JSONObject) {
					JSONObject ResultJson = responseJson.getJSONObject("Result");
					// forming response dto
					ResponseDto responseDto = new ResponseDto();
					JSONArray resultsArray = null;
					JSONObject validResultObject = null;
					JSONArray watchListArray = null;
					// name
					if (ResultJson != null && ResultJson.has("name") && ResultJson.get("name") instanceof String) {
						responseDto.setEntityName(ResultJson.getString("name"));
					}
					// feed source
					if (ResultJson != null && ResultJson.has("screening_type")
							&& ResultJson.get("screening_type") instanceof String) {
						responseDto.setFeed_source(ResultJson.getString("screening_type"));
					}
					// customer ID
					if (CLIENT_ID != null)
						responseDto.setCustomerId(CLIENT_ID);
					if (ResultJson != null && ResultJson.has("results")
							&& ResultJson.get("results") instanceof JSONArray) {
						resultsArray = ResultJson.getJSONArray("results");
					}
					if (resultsArray != null && resultsArray.length() > 0) {
						for (int i = 0; i < resultsArray.length(); i++) {
							if (resultsArray.get(i) != null && resultsArray.get(i) instanceof JSONObject
									&& resultsArray.getJSONObject(i).has("valid")
									&& resultsArray.getJSONObject(i).get("valid") instanceof Boolean
									&& resultsArray.getJSONObject(i).getBoolean("valid")) {
								validResultObject = resultsArray.getJSONObject(i);
							}
						}
					}
					if (validResultObject != null && validResultObject.has("watchlist")
							&& validResultObject.get("watchlist") instanceof JSONArray) {
						watchListArray = validResultObject.getJSONArray("watchlist");
					}
					if (watchListArray != null && watchListArray.length() > 0) {
						String classification = null;
						// classification
						if (watchListArray.get(0) != null && watchListArray.get(0) instanceof JSONObject
								&& watchListArray.getJSONObject(0).has("classification")
								&& watchListArray.getJSONObject(0).get("classification") instanceof String)
							classification = watchListArray.getJSONObject(0).getString("classification");
						if (classification != null)
							responseDto.setFeed_classification(classification);
						for (int i = 0; i < watchListArray.length(); i++) {
							JSONArray entriesArray = null;
							if (watchListArray.get(i) != null && watchListArray.get(i) instanceof JSONObject
									&& watchListArray.getJSONObject(i).has("entries")
									&& watchListArray.getJSONObject(i).get("entries") instanceof JSONArray)
								entriesArray = watchListArray.getJSONObject(i).getJSONArray("entries");
							if (entriesArray != null && entriesArray.length() > 0) {
								for (int j = 0; j < entriesArray.length(); j++) {
									JSONArray attributesArray = null;
									if (entriesArray.get(j) != null && entriesArray.get(j) instanceof JSONObject
											&& entriesArray.getJSONObject(j).has("attributes")
											&& entriesArray.getJSONObject(j).get("attributes") instanceof JSONArray)
										attributesArray = entriesArray.getJSONObject(j).getJSONArray("attributes");
									if (attributesArray != null && attributesArray.length() > 0) {
										// eachMetaData
										AlertMetaDataDto eachMetaData = new AlertMetaDataDto();

										metaDataListDto.add(eachMetaData);
									}
								}
							}
						}
					}
				}
			}
			return null;
		}
	*/
		return null;}
	
	@Override
	public Boolean createNewAlerts(String requestId) throws Exception {



		try {
			if (BIGDATA_ALERTS_URL != null) {
				int totalPageForGetAlerts = 10;
				String alertsUrl = BIGDATA_ALERTS_URL + "getAlert";
				if (requestId != null)
					alertsUrl = alertsUrl + "?request_id=" + requestId + "&page=";
				else {
					alertsUrl = alertsUrl + "?page=";
				}
				// TEMP Change-1
				/*JSONParser parser = new JSONParser();
				ClassLoader classLoader = getClass().getClassLoader();
				File jsonfile = new File(classLoader.getResource("NewLatestAlert.json").getFile());
				Object jsonfiledata = parser.parse(new FileReader(jsonfile));*/
				// TEMP Change-1
				for (int x = 1; x <= totalPageForGetAlerts; x++) {

					String response = getDataFromServer(alertsUrl+x);
					// String response=jsonfiledata.toString();
					if (response != null) {

						JSONObject responseJSON = new JSONObject(response);
						if (responseJSON != null && responseJSON.has("results")
								&& responseJSON.get("results") instanceof JSONArray) {
							JSONArray resultJsonArray = responseJSON.getJSONArray("results");

							if (resultJsonArray != null && resultJsonArray.length() > 0) {
								for (int i = 0; i < resultJsonArray.length(); i++) {
									StringBuilder alertOperation = new StringBuilder();
									JSONObject requestJson = resultJsonArray.getJSONObject(i);
									Long detailId = null;
									List<String> classificationList = new ArrayList<>();
									if (requestJson != null && requestJson.has("detail_id")
											&& requestJson.get("detail_id") instanceof Integer) {
										detailId = Long.valueOf(requestJson.getInt("detail_id"));
										if (detailId != null && findBydetailId(detailId) == null) {
											JSONObject ResultObject = null;
											if (requestJson != null && requestJson.has("results")
													&& requestJson.get("results") instanceof JSONObject)
												ResultObject = requestJson.getJSONObject("results");
											Alerts alerts = new Alerts();
											alerts.setDetailId(detailId);

											alerts.setAlertMetaData(requestJson.toString());
											ListItemDto newTypeDto = listItemService
													.getListItemByListTypeAndDisplayName(
															ElementConstants.ALERT_STATUS_TYPE, "New");
											if (newTypeDto != null && newTypeDto.getListItemId() != null) {
												alerts.setStatus(newTypeDto.getListItemId());
											}
											if (requestJson != null && requestJson.has("entity_type"))
												alerts.setEntiType(requestJson.getString("entity_type"));
											if (requestJson != null && requestJson.has("entity_id"))
												alerts.setCustomerId(requestJson.getString("entity_id"));

											// alerts.setConfidenceLevel(4.0);
											alerts.setCreatedDate(new Date());
											alerts.setAssignee(0L);

											// the entity_id is represented as customer_id EL-7492
											/*
											 * if (requestJson != null && requestJson.has("customer_id") &&
											 * requestJson.get("customer_id") instanceof String)
											 * alerts.setCustomerId(requestJson.getString("customer_id"));
											 */
											if (requestJson != null && requestJson.has("name")
													&& requestJson.get("name") instanceof String)
												alerts.setEntityName(requestJson.getString("name"));
											if (requestJson != null && requestJson.has("jurisdiction")
													&& requestJson.getString("jurisdiction") instanceof String) {
												ListItemDto listItemDto = listItemService
														.getListItemByListTypeAndDisplayName(
																ElementConstants.JURISDICTIONS_STRING,
																requestJson.getString("jurisdiction"));
												if (listItemDto != null) {
													Long jurisdiction = listItemDto.getListItemId();
													alerts.setJurisdiction(jurisdiction);
												} else {
													listItemDto = listItemService.getListItemByListTypeAndCode(
															ElementConstants.JURISDICTIONS_STRING,
															requestJson.getString("jurisdiction"));
													if (listItemDto != null) {
														Long jurisdiction = listItemDto.getListItemId();
														alerts.setJurisdiction(jurisdiction);
													}
												}
											}

											if (ResultObject.has("screening")) {
												JSONObject screeningJsonObject = ResultObject
														.getJSONObject("screening");
												if (screeningJsonObject.has("watchlists")) {
													JSONArray watchlistJsonArray = screeningJsonObject
															.getJSONArray("watchlists");
													for (int j = 0; j < watchlistJsonArray.length(); j++) {
														JSONObject watchlistJsonObject = watchlistJsonArray
																.getJSONObject(j);
														if (watchlistJsonObject.has("entries")) {
															JSONArray entriesJsonArray = watchlistJsonObject
																	.getJSONArray("entries");
															for (int m = 0; m < entriesJsonArray.length(); m++) {
																JSONObject entriesJsonObject = entriesJsonArray
																		.getJSONObject(m);
																// classificationJsonArray.put(entriesJsonObject.getString("classification"));
																if (!classificationList.contains(entriesJsonObject
																		.getString("classification").toUpperCase())) {
																	if (!StringUtils.isBlank(entriesJsonObject
																			.getString("classification")
																			.toUpperCase())) {

																		classificationList.add(entriesJsonObject
																				.getString("classification")
																				.toUpperCase());
																	}
																}

															} 
														}
													}
												}
											}

											//
											List<String> duplicateClassificationList = new ArrayList<>(
													classificationList);

											Long sourceId = null;
											Long typeId = null;

											ListItemDto statusDto = listItemService.getListItemByListTypeAndCode(
													ElementConstants.FEED_SOURCE_TYPE,
													requestJson.getString("screening_type"));
											if (null != statusDto && null != statusDto.getListItemId()) {
												sourceId = statusDto.getListItemId();
											} else {
												ListItem listItem = new ListItem();
												listItem.setAllowDelete(true);
												listItem.setCode(requestJson.getString("screening_type"));
												listItem.setColorCode("ef5350");
												listItem.setDisplayName(requestJson.getString("screening_type"));
												listItem.setIcon("ban");
												listItem.setListType(ElementConstants.FEED_SOURCE_TYPE);
												listItem = listItemService.save(listItem);
												sourceId = listItem.getListItemId();
											}

											List<ListItemDto> feedClassifications = listItemService
													.getListItemByListType(ElementConstants.FEED_CLASSIFICATION_STRING);
											List<Long> filteredFeedClassificationsIds = new ArrayList<>();
											List<Long> groupId = new ArrayList<>();
											feedClassifications.stream().forEach(s -> {
												for (String str : duplicateClassificationList) {
													FeedManagement feed = new FeedManagement();
													if (null != s.getDisplayName()
															&& s.getDisplayName().equalsIgnoreCase(str)
															|| s.getCode().equalsIgnoreCase(str)) {
														// list exist check for feed if feed does not exist create feed.
														filteredFeedClassificationsIds.add(s.getListItemId());
														FeedManagementDto feedFromList = feedManagementService
																.getFeedByTypeAndSource(s.getListItemId(),
																		statusDto.getListItemId());
														if (null == feedFromList.getFeed_management_id()) {

															feed.setColor("a84c45");
															String feedName = str + " "
																	+ requestJson.getString("screening_type");
															feed.setFeedName(feedName);
															feed.setSource(statusDto.getListItemId().intValue());
															feed.setType(s.getListItemId().intValue());
															feed.setIsReviewerRequired(false);
															feed = feedManagementService.save(feed);

															Groups entityIdentification = groupsService
																	.getGroup(ElementConstants.ENTITY_IDENTIFICATION);
															if (!groupId.contains(entityIdentification.getId()))
																groupId.add(entityIdentification.getId());
															FeedGroups feedGroup = new FeedGroups();
															feedGroup.setFeedId(feed);
															feedGroup.setRank(0);
															feedGroup.setGroupId(entityIdentification);

															feedGoupService.saveOrUpdate(feedGroup);
														}
														classificationList.remove(str);
													}
												}
											});
											// Add newly created typeId

											// create typeId and Feeds

											for (int l = 0; l < classificationList.size(); l++) {
												ListItem listItem = new ListItem();
												listItem.setAllowDelete(true);
												listItem.setCode(classificationList.get(l));
												listItem.setColorCode("ef5350");
												listItem.setDisplayName(classificationList.get(l));
												listItem.setIcon("ban");
												listItem.setListType(ElementConstants.FEED_CLASSIFICATION_STRING);
												listItem = listItemService.save(listItem);

												typeId = listItem.getListItemId();
												FeedManagement feed = new FeedManagement();
												feed = new FeedManagement();
												feed.setColor("a84c45");
												String feedName = classificationList.get(l) + " "
														+ requestJson.getString("screening_type");
												feed.setFeedName(feedName);
												feed.setSource(sourceId.intValue());
												feed.setType(typeId.intValue());
												feed.setIsReviewerRequired(false);
												feed = feedManagementService.save(feed);

												Groups entityIdentification = groupsService
														.getGroup(ElementConstants.ENTITY_IDENTIFICATION);
												if (!groupId.contains(entityIdentification.getId()))
													groupId.add(entityIdentification.getId());
												FeedGroups feedGroup = new FeedGroups();
												feedGroup.setFeedId(feed);
												feedGroup.setRank(0);
												feedGroup.setGroupId(entityIdentification);

												feedGoupService.saveOrUpdate(feedGroup);

												filteredFeedClassificationsIds.add(typeId);
											}

											// get the newly created feed and also the existing one.
											if (null != filteredFeedClassificationsIds
													&& filteredFeedClassificationsIds.size() > 0) {
												List<FeedManagement> feedList = feedManagementService
														.getFeedsByTypeIds(filteredFeedClassificationsIds);
												feedList.stream().filter(s -> null != s.getFeed_management_id())
														.forEach(f -> Hibernate.initialize(f.getGroupLevels()));
												feedList.removeIf(p -> !p.getSource()
														.equals(statusDto.getListItemId().intValue()));
												alerts.setFeed(feedList);

												FeedManagement singlefeed = feedList.get(0);
												List<FeedGroups> feedGroups = singlefeed.getGroupLevels();
												if (feedGroups != null) {
													feedGroups.stream().filter(s -> null != s.getGroupId())
															.forEach(f -> Hibernate.initialize(f.getGroupId()));
													if (null != feedGroups) {
														FeedGroups maxRankGroup = Collections.min(feedGroups,
																Comparator.comparing(s -> s.getRank()));
														alerts.setGroupLevel(
																maxRankGroup.getGroupId().getId().intValue());
														alerts.setReviewer(maxRankGroup.getGroupId().getId());
													}
												} else if (groupId != null && groupId.size() > 0) {
													feedGroups = feedManagementService.getFeedGroupsByGroupId(groupId);
													feedGroups.stream().filter(s -> null != s.getGroupId())
															.forEach(f -> Hibernate.initialize(f.getGroupId()));
													if (null != feedGroups) {
														FeedGroups maxRankGroup = Collections.min(feedGroups,
																Comparator.comparing(s -> s.getRank()));
														alerts.setGroupLevel(
																maxRankGroup.getGroupId().getId().intValue());
														alerts.setReviewer(maxRankGroup.getGroupId().getId());
													}

												} else {
													Groups entityIdentification = groupsService
															.getGroup(ElementConstants.ENTITY_IDENTIFICATION);
													groupId.add(entityIdentification.getId());
													feedGroups = feedManagementService.getFeedGroupsByGroupId(groupId);
													feedGroups.stream().filter(s -> null != s.getGroupId())
															.forEach(f -> Hibernate.initialize(f.getGroupId()));
													if (null != feedGroups) {
														FeedGroups maxRankGroup = Collections.min(feedGroups,
																Comparator.comparing(s -> s.getRank()));
														alerts.setGroupLevel(
																maxRankGroup.getGroupId().getId().intValue());
														alerts.setReviewer(maxRankGroup.getGroupId().getId());
													}
												}

												ListItemDto listItemDto = listItemService
														.getListItemByListTypeAndDisplayName("Alert Status", "New");
												if (null != listItemDto)
													alerts.setStatus(listItemDto.getListItemId());

												alerts.setUpdatedDate(new Date());

												ListItemDto listItem = listItemService
														.getListItemByListTypeAndDisplayName(
																ElementConstants.ALERT_REVIEW_STATUS_TYPE,
																"Unconfirmed");
												if (null != listItem)
													alerts.setReviewStatusId(listItem.getListItemId());

												alerts.setPeriodicReview(false);
												alerts.setIsEntityIdentified(false);
												/*
												 * alerts.setEntiType(entiType); alerts.isEntityIdentified();
												 * alerts.setIdentifiedEntityId(identifiedEntityId);
												 * alerts.setJurisdiction(jurisdiction);
												 */
												alerts = alertManagementDao.create(alerts);

												alertOperation = alertOperation
														.append("a new screening alert was CREATED");

												// audit log on new alert creation
												// String finalString = "";
												if (!alertOperation.toString().isEmpty()) {
													/*
													 * String auditDesc = alertOperation.toString().trim();
													 * if(auditDesc.endsWith(",")){ finalString = auditDesc.substring(0,
													 * auditDesc.lastIndexOf(",")); }
													 */
													AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
															ElementConstants.ALERT_MANAGEMENT_TYPE, null,
															ElementConstants.ALERT_MANAGEMENT_TYPE);
													log.setDescription(alertOperation + " ");
													log.setName(alerts.getAlertId().toString());
													// log.setUserId(userId);
													// if(status!=null)
													// log.setSubType(ElementConstants.ALERT_STATUS_SUBTYPE);
													log.setTypeId(alerts.getAlertId());

													auditLogDao.create(log);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.INTERNAL_SERVER_ERROR, e, this.getClass());
			return false;
		}
		return true;
	
	}

	@Override
	public Alerts findBydetailId(Long detailId) {
		return alertManagementDao.findBydetailId(detailId);
	}
	
	@Override
	public String getODSScreeningResults(String input) throws Exception {
		String url = screeningAlertsUrl + "_rt";
		return postStringDataToServer(url, input);
	}
	
	private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public void updateGroupLevelOnfeedChangeByRnak(Long feed_management_id, List<FeedGroups> feedGroupsListDTO) {
		List<Long> feedIdList = new ArrayList<>();
		feedIdList.add(feed_management_id);
		List<Alerts> alerts = null;
		List<Long> alertIdList = getAlertIdsByFeedIds(feedIdList);
		if(null != alertIdList && !alertIdList.isEmpty()){
		     alerts = getAlertsByAlertIdList(alertIdList);	
		     if(null != alerts && !alerts.isEmpty()){
					alerts.stream().filter(s -> null != s.getFeed()).forEach(s -> Hibernate.initialize(s.getFeed()));
					alerts.stream().filter(alert -> null != alert.getFeed()).forEach(s -> Hibernate.initialize(s.getReviewer()));
				
				 
					if(null != feedGroupsListDTO && feedGroupsListDTO.size()>0){
						FeedGroups maxRankGroup =  Collections.min(feedGroupsListDTO, Comparator.comparing(s -> s.getRank()));
						List<Groups> groupsList = feedGroupsListDTO.stream().filter(s -> null != s.getGroupId()).map(e->e.getGroupId()).collect(Collectors.toList());
						List<Long> groupIdList = groupsList.stream().filter(s -> null != s.getId()).map(m -> m.getId()).collect(Collectors.toList());
						for(Alerts alert : alerts) {
							Long groupLevel = null;
							if(null != alert.getGroupLevel()){
								 groupLevel = Long.valueOf(alert.getGroupLevel());
								 if(null != groupLevel && !groupIdList.contains(groupLevel)) {
										alert.setGroupLevel(Integer.valueOf(maxRankGroup.getGroupId().getId().intValue()));
										alert.setReviewer(maxRankGroup.getGroupId().getId());
										saveOrUpdate(alert);
									}
							}
						}
					}else{
						for(Alerts alert : alerts) {
								alert.setGroupLevel(null);
								saveOrUpdate(alert);
						}
					}
				}
		}
	
	}
	
	@Override
	public String postSSBData(String requestBody, String id) throws Exception {
		if(id.isEmpty())
			throw new NoDataFoundException(ElementConstants.ID_NOT_FOUND); 
		String postSSDScreeningUrl = SSB_SCREENING_DATA_URL+id+"/"+id;
		return postStringDataToServer(postSSDScreeningUrl, requestBody);
	}
	
	@Override
	public String getSSBScreeningData(String id) throws Exception {
		if(id.isEmpty())
			throw new NoDataFoundException(ElementConstants.ID_NOT_FOUND); 
		String[] serverResponse = ServiceCallHelper.getDataFromServer(SSB_SCREENING_DATA_URL+id+"/"+id);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	@Override
	public String getDataFromUrl(String url) throws Exception {
		if(url.isEmpty())
			throw new NoDataFoundException(ElementConstants.ID_NOT_FOUND); 
		String[] serverResponse = ServiceCallHelper.getDataFromServer(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<Long> getAlertIdsByFeedIds(List<Long> comingIdList) {
		
		return alertManagementDao.getAlertIdsByFeedIds(comingIdList);
	}

	@Override
	public List<Alerts> getAlertsByAlertIdList(List<Long> alertIdList) {
		
		return alertManagementDao.getAlertsByAlertIdList(alertIdList);
	}
	
	@Override
	public String getSubGraphVla(String entityId, int levels, String field, String parentId, String requestId) throws Exception {
		String subGraphVlaUrl = BIGDATA_MULTISOURCE_URL+"/"+entityId+"/sub_graph_vla?client_id="+CLIENT_ID+"&levels="+levels+"&field="+field+"&parent_id="+parentId+"&request_id="+requestId;
		System.out.println("subGraphVlaUrl >> "+subGraphVlaUrl);
		String[] serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeout(subGraphVlaUrl);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public boolean validateAlertStatusChange(Long existingStatus, Long statusTobeChanged) throws Exception {
		boolean isStatusChangeAllowed = false;
		List<ListItemDto> allStatuses = listItemService.getListItemByListType(ElementConstants.ALERT_STATUS_TYPE);
		ListItemDto existingStatusObj = allStatuses.stream().filter(f -> f.getListItemId().equals(existingStatus)).findFirst().orElse(null);
		List<Long> statusesCanbeChangedObj = new ArrayList<Long>();
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource("alertStatusValidation.json");
		File file = new File(resource.toURI());
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(file));
		JSONObject jsonObject = new JSONObject(object.toString());
		
		JSONArray existingStatusArray = jsonObject.getJSONArray(existingStatusObj.getDisplayName().toLowerCase());
		for(int i=0; i< existingStatusArray.length(); i++){
			String status = existingStatusArray.getString(i);
			ListItemDto dto = allStatuses.stream()
					.filter(f -> f.getDisplayName().equalsIgnoreCase(status)).findAny().orElse(null);
			if(dto != null){
				statusesCanbeChangedObj.add(dto.getListItemId());
			}
		}
		if(statusesCanbeChangedObj.contains(statusTobeChanged)){
			isStatusChangeAllowed = true;
		}
		return isStatusChangeAllowed;

	}
}