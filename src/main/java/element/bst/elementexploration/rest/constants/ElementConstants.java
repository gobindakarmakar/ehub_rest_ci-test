package element.bst.elementexploration.rest.constants;

/**
 * Constant utility class for elementexploration.
 * @author Amit Patel
 *
 */
public class ElementConstants {

    public static final String USER_BASIC_INFO = "userBasicInfo";// will be
								 // saved in
								 // session.
    public static final String VALID_DATE_TYPE_MSG = "Check date formate(dd-MM-yyyy)";
    public static final String VALID_SESSION_CHECK_MSG = "No valid session found";
    public static final String CASE_SEED_SEARCH_KEYWORD_VALIDATION_MSG = "Search keyword cannot be less than 5";
    public static final String SEARCH_KEYWORD_VALIDATION_MSG = "Search keyword length should atleast be 5 characters.";
    public static final String SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG = "Search keyword length should atleast be 3 characters.";
    public static final Integer DEFAULT_PAGE_NUMBER = 1;
    public static final Integer DEFAULT_PAGE_SIZE = 10;
    public static final Double FILE_MAX_SIZE = 6.0;
    public static final String CASE_SEED_SEED_MSG = "check the json format";
    public static final String CASE_SEED_SEED_DELETE_MSG = "Case deleted successfully.";
    public static final String CASE_SEED_SEED_EMPTY_LIST_MSG = "No data found";
    public static final String CASE_SEED_SEED_JSON_FILE_ID = "caseId";
    public static final Integer CASE_SEED_CURRENT_FRESH_STATUS_ID = 1;
    public static final String CASE_SEED_SUCCESSFUL_MSG = "Json file uploaded successfully";
    public static final String CASE_SEED_UPDATE_MSG = "Case updated successfully.";
    public static final String HIBERNATE_EXCEPTION_MSG = "Hibernate exception : ";
    public static final String CASE_SEED_NOT_FOUND = "Case not found.";
    public static final String USER_NOT_FOUND = "user not found";
    public static final String USER_NOT_ADMIN = "The user is not from Admin group and not authorized to perform this operation.";
    public static final String TRANSACTION_MONITOR_EMPTY_LIST_MSG = "No data found";
    public static final String USER_CREATION_SUCCESSFUL = "User created/updated successfully!";
    public static final String USERS_CREATION_SUCCESSFUL = "Users created successfully!";
    public static final int ACTIVATED_USER_CODE = 1;
    public static final int ENABLED_USER_REGISTRATION = 1;
    public static final int ENABLED_EMAIL_VERIFICATION = 1;
    public static final String USER_REGISTRATION_DISABLED = "User registration is disabled!";
    public static final String USERNAME_ALREADY_EXISTS = "User already exists with username : ";
    public static final String EMAILADDRESS_ALREADY_EXISTS = "User with same email already exists :";
    public static final String CHECKIN_FAILED = "User check-in for notification failed";
    public static final String EXCEPTION_MSG = "Failed to run API due to exception encountered.";
    public static final String NOTIFICATION_DETAIL_NOT_FOUND = "Notification details not found.";
    public static final String NOTIFICATION_RECIPIENT_NOT_FOUND = "No recipient found for this notification.";
    public static final String EVENT_RECIPIENT_NOT_FOUND = "No recipient found for this event.";
    public static final String USER_NOT_OWNER = "Operation not permitted.";
    public static final String ADD_PARTICIPANTS_SUCESSFUL_MSG = "Participants added successfully.";
    public static final String REMOVE_PARTICIPANTS_SUCESSFUL_MSG = "Participants removed successfully.";
    public static final String NO_PARTICIPANTS_PROVIDED = "Participants not provided.";
    public static final String CALENDAR_EVENT_UPDATE_MSG = "Event updated successfully.";
    public static final String EVENT_NOT_FOUND = "Event not found.";
    public static final String REMOVE_CONTACT_SUCESSFUL_MSG = "Contact removed successfully.";
    public static final String ADD_CONTACT_SUCESSFUL_MSG = "Contact added successfully.";
    public static final String CONTACT_ALREADY_EXISTS_MSG = "Contact already exists.";
    public static final String CONTACT_NOT_FOUND_MSG = "Contact not found.";
    public static final String SEARCH_STRING_LENTH_INVALID = "Length of the search string should be at least 3 character.";
    public static final String NO_DATA_FOUND_MSG = "No data found";
    public static final String MISSING_REQUIRED_FIELDS_MSG = "One or more required fields are missing.";
    public static final String USER_NOT_FOUND_MSG = "User not found.";
    public static final String USER_DEACTIVATED_SUCCESSFULLY_MSG = "User deactivated successfully.";
    public static final String USER_ACTIVATED_SUCCESSFULLY_MSG = "User activated successfully.";
    public static final String GROUP_CREATED_SUCCESSFULLY_MSG = "Group created successfully.";
    public static final String GROUP_NOT_FOUND_MSG = "Group not found.";
    public static final String GROUP_ACTIVATED_SUCCESSFULLY_MSG = "Group activated successfully.";
    public static final String USER_DEACTIVATED_FAIL_MSG = "User deactivation failed.";
    public static final String GROUP_DEACTIVATED_SUCCESSFULLY_MSG = "Group deactivated successfully.";
    public static final String GROUP_UPDATED_SUCCESSFULLY_MSG = "Group updated successfully.";
    public static final String USER_ALREADY_IN_GROUP_MSG = "User already exists in this group.";
    public static final String USER_OR_GROUP_IS_INACTIVE_MSG = "Either user or group is inactive.";
    public static final String USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG = "User successfully added to the group.";
    public static final String USERS_ADDED_IN_GROUP_SUCCESSFULLY_MSG = "Users successfully added to the group.";
    public static final String USERS_ADDED_IN_GROUP_FAIL_MSG = "Failed to add Users to the group.";
    public static final String USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG = "User successfully removed from group.";
    public static final String USER_PROFILE_UPDATED_SUCCESSFULLY_MSG = "Profile picture was added/updated for ";
    public static final String USER_PROFILE_REMOVED_SUCCESSFULLY_MSG = "Profile removed successfully ";
    public static final String USER_PROFILE_PICTURE_UPDATED_SUCCESSFULLY_MSG = "Profile picture was added/updated for ";
    public static final String USER_PROFILE_PICTURE_REMOVED_SUCCESSFULLY_MSG = "Profile picture was removed for ";
    public static final String INVALID_JSON_FORMAT_MSG = "Invalid json format.";
    public static final String FAILED_TO_GET_DATA_FROM_SERVER_MSG = "Failed to get data from server.";
    public static final String FAILED_TO_DELETE_DATA_ON_SERVER_MSG = "Failed to delete data on server.";
    public static final String FAILED_TO_UPDATE_DATA_ON_SERVER_MSG = "Failed to update data on server.";
    public static final String SUCCESSFULLY_MARKED_AS_FAVOURITE_MSG = "Search history successfully marked as favorite.";
    public static final String COULD_NOT_MARK_AS_FAVOURITE_MSG = "Could not mark search history as favorite.";
    public static final String FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG = "Failed to upload file to the server.";
    public static final String FAILED_TO_UPLOAD_FILES_TO_SERVER_MSG = "Failed to upload files to the server.";
    public static final String FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG = "Failed to download file from server.";
    public static final String INVALID_FILE_TYPE_MSG = "Unsupported Media Type.";
    public static final String INVALID_FILE_SIZE_MSG = "The file size exceeds the limit allowed of %s and cannot be upload";
    public static final String DOCUMENT_METADATA_UPDATED_SUCCESSFULLY_MSG = "Document metadata updated successfully.";
    public static final String DOCUMENT_DELETED_SUCCESSFULLY_MSG = "Document deleted successfully.";
    public static final String DOCUMENT_UPLOADED_SUCCESSFULLY_MSG = "Document uploaded successfully.";
    public static final String DOCUMENT_UPDATED_SUCCESSFULLY_MSG = "Document updated successfully.";
    public static final String DOCUMENT_SHARED_SUCCESSFULLY_MSG = "Document shared successfully.";
    public static final String DOCUMENT_UNSHARED_SUCCESSFULLY_MSG = "Document unshared successfully.";
    public static final String CASE_DELETED_FROM_IN_FOCUS_MSG = "Case deleted from focus area.";
    public static final String COULD_NOT_DELETED_CASE_FROM_IN_FOCUS_MSG = "Could not delete case from focus area.";
    public static final String CASE_ADDED_TO_FOCUS_MSG = "Case added to focus area.";
    public static final String COULD_NOT_ADD_CASE_TO_FOCUS_MSG = "Could not add case to focus area.";
    public static final String COMMENT_UPDATED_MSG =  "Comment updated successfully.";
    public static final String DOCUMENT_DISSEMINATED_SUCCESSFULLY_MSG = "Document disseminated successfully.";
    public static final String SERACH_HISTORY_UPDATED_SUCCESSFULLY_MSG = "Search history successfully marked as favorite.";
    public static final String SEARCH_HISTORY_NOT_FOUND = "Search history not found.";
    public static final String INVALID_SEARCH_FLAG = "Invalid search flag value.";
    public static final String INVALID_DOC_FLAG_MSG = "Invalid docFlag value please provide value in range 0-255.";
    public static final String SEARCH_HISTORY_UPDATE_MSG = "Search history updated successfully.";
    public static final String SEARCH_HISTORY_NOT_FOUND_MSG = "Search history not found.";
    public static final String GET_LATITUDE_LONGITUDE_NOT_FOUND_MSG="You have exceeded your daily request quota for this API. We recommend registering for a key at the Google Developers"; 
    public static final String INTERNET_CONNECTION ="Please Check your Internet Connection and try again";
    public static final String DOCUMENT_ID_NULL ="document id passed as null ,please check";
    public static final String UPDATE_FAILED ="Internal server error";
    public static final String INDUSTRY_ADD_FAILED ="Industry name already exists";
    public static final String DOMAIN_ADD_FAILED ="Domain name already exists";
    public static final String JURISDICTION_ADD_FAILED ="Jurisdiction name already exists";
    public static final String MEDIA_ADD_FAILED ="Media name already exists";
    public static final String INTERNAL_SERVER_ERROR ="Internal server error";
    public static final String USER_CREATION_DISABLED ="User creation is disabled";
    public static final String USER_UPDATION_DISABLED ="User updation is disabled";
    public static final String ROLE_CREATION_DISABLED ="Role creation is disabled";
    public static final String ROLE_UPDATION_DISABLED ="Role updation is disabled";
    public static final String GROUP_CREATION_DISABLED ="Group creation is disabled";
    public static final String GROUP_UPDATION_DISABLED ="Group updation is disabled";
    public static final String FETCH_LAST_VISITED_DOMAINS_SUCCESSFUL ="Fetch last visited domains sucessful";
    public static final String LAST_VISITED_DOMAINS_FAILED ="Fetch last visited domains failed";
    public static final String FETCH_BASIC_SETTINGS_SUCCESSFUL="Fetch basic settings sucessful";
	
    //Exceptions
	public static final String PARSING_EXCEPTION_MSG="Parsing exception occured";
	
	public static final String FROM_EMAIL="";
	
    public static final String GROUP_NAME_EXIST = "Group with same name already exists.";
    
    public static final String ADMIN_GROUP_NAME = "Admin";
    
    public static final String ANALYST_GROUP_NAME = "Analyst";
    
    public static final Integer DEFAULT_PAGE_TO = 100;
    
    public static final String DOCUMENT_FORMAT="please check the format,only csv format allowed";

    public static final String CSV_DATA_COLUMNS_FORMAT="failed to upload ,please check order of columns";
    
    public static final String DATAENTRY_GROUP_NAME = "DataEntry";
    
    public static final String CASE_ALREADY_ADDED_TO_FOCUS_MSG = "Case already added to focus area.";
    
    public static final String CHANGE_PASSWORD_FAIL_MSG="Old password is wrong or new password and Retype password do not match";
    
    public static final String OWNERSHIP_ERROR="Failed to get ownership structure from the source";
    
    public static final String ROLE_NAME_EMPTY="Role cannot be null";
    public static final String ROLE_ADDED_SUCCESSFULLY_MSG="Role is added/updated successfully";
    public static final String ROLE_UPDATED_FAILED_MSG="Failed to add/update role";
    public static final String ROLE_WITH_ID_SPECIFIED_DOESNT_EXIST="Role with ID specified doesn't exist";
    public static final String ROLE_ALREADY_EXISTS_WITH_NAME="Role already exists with this name";
    public static final String ROLE_WITH_NAME_ID_DEOESNT_EXIST="Role with ID and Name combination doesn't exist";
    
    public static final String PERMISSION_NAME_EMPTY="Permission name cannot be null";
    public static final String PARENT_PERMISSION_ID_EMPTY="ParentpermissonID  cannot be null";
    public static final String PERMISSION_ADDED_SUCCESSFULLY_MSG="Permission is added/updated successfully";
    public static final String PERMISSION_UPDATED_FAILED_MSG="Failed to add/update Permission";
    public static final String PARENTPERMISSIONID_FAILED_MSG="provided parentpermission Id does not exists";

    
    public static final String ROLE_ASSIGN_SUCCESSFUL="Role is assigned successfully";
    public static final String ROLE_UNASSIGN_SUCCESSFUL="Role is un-assigned successfully";
    public static final String ROLE_UNASSIGN_ACTION="Un-assigned Role";
    public static final String ROLE_ASSIGN_ACTION="Assigned Role";
    public static final String ADD_TO_GROUP_ACTION="Added to the Group";
    public static final String REMOVED_FROM_GROUP_ACTION="Removed from group";
    public static final String ROLE_ASSIGN_FAIL="Role assignment failed";
    public static final String ROLE_NOT_FOUND="Role not found";
    
    public static final String ALERT_STATUS_SUBTYPE="Status"; 
    public static final String ALERT_BULK_SUBTYPE="Bulk Operation"; 
    public static final String ALERT_MANAGEMENT_TYPE="Alert Management";
    public static final String USER_REGISTRATION_TYPE="User Registration";
    
    public static final String USER_ROLE_NOT_FOUND="User Role not found";
    public static final String USER_ROLE_UPDATED="role of user updated";
    public static final String USER_ROLE_ADDED="role of user added";
    public static final String STRING_TYPE_USER_ROLE="USER ROLES";
    public static final String STRING_TYPE_USER_GROUP="USER GROUP";
    public static final String STRING_TYPE_GROUP_PERMISSIONS="GROUP PERMISSIONS";
    public static final String GROUP_PERMISSIONS_ADDED="group permissions added";
    public static final String GROUP_PERMISSIONS_UPDATED="group permissions updated";
    public static final String STRING_TYPE_PRIVILEGES="PRIVILEGES";
    public static final String PRIVILEGES_ADDED="privileges added";
    public static final String PRIVILEGES_UPDATED="privileges Updated";
    public static final String STRING_TYPE_ROLE="ROLES";
    public static final String ROLES_ADDED="Role added";
    public static final String ROLES_GROUP_ADDED="Role Group added";
    public static final String ROLES_UPDATED="roles updated";
    public static final String ROLES_UPDATE_ACTION="Roles Update";
    public static final String ROLES_ADD_ACTION="Roles Add";
    public static final String ROLES_GROUP_ADD_ACTION="Role GrFETCH_LAST_VISITED_DOMAINS_FAILoup Add";
    public static final String USER_REGISTRATION ="Added user";
    public static final String ROLES_ADDED_SUCCESSFULLY="Role added successfully";
    public static final String ROLES_UPDATED_SUCCESSFULLY="Role updated successfully";
    public static final String ROLE_ADDED_UPDATED_TO_GROUP_SUCCESSFULLY_MSG="Role(s) is added/updated to group successfully";
    public static final String ROLE_ADDED_UPDATED_TO_GROUP_FAILED_MSG="Role(s) addition/updation to group failed";




    
    public static final String GROUP_ID_NOT_FOUND="Group with ID not found";
    
    public static final String GROUP_ID_EMPTY="Group ID cannot be empty";
    public static final String ROLE_ID_EMPTY="Role ID cannot be empty";
    public static final String ROLE_ID_NOT_FOUND="Role with ID not found";
    public static final String PERMISSION_ID_EMPTY="Permission ID cannot be empty";
    public static final String PERMISSION_ID_NOT_FOUND="Permission with ID not found";
    public static final String GROUP_PERMISSION_ID_NOT_FOUND="Group permission with ID not found";
    
    public static final String GROUP_PERMISSION_UPDATE_SUCCESFUL="Group permissions add/update successful";
    public static final String FETCH_GROUP_PERMISSIONS_SUCCESSFUL="Fetch Group Permissions Successful";
    public static final String DELETE_GROUP_PERMISSIONS_SUCCESSFUL="Role Permissions Un-assigned Successfully";
    public static final String GROUP_PERMISSION_NOT_FOUND="Role permission not found";
    public static final String GROUP_PERMISSION_UPDATE_FAIL="Role permissions add/update failed";
    public static final String GROUP_PERMISSION_SOME_UPDATE_FAIL="Some Role permissions add/update failed";
    public static final String GROUP_PERMISSION_TYPE="Group permission";
    public static final String FETCH_GROUP_NAME_SUCCESSFUL="Fetch Group By Name Successful";
    public static final String FETCH_GROUP_NAME_FAIL="Fetch Group By Name Failed";

    public static final String ALERT_REVIEW_STATUS_TYPE="Alert Review Status";
    
    public static final String GROUP_PERMISSION_UNASSIGN_ACTION="Role permission unassign";
    public static final String UNASSINED_GROUP_PERMISSION="Unassigned Role Permission";
    public static final String GROUP_PERMISSION_ADD_ACTION="Added/updated Role permission";
    public static final String EMAIL_SENT_SUCCESSFULLY="Email sent successfully";
    public static final String EMAIL_SENDING_FAILED="Failed to send Email";
    public static final String EMAIL_SENT=" and e-mail sent to specified mail";
    
    public static final String TO_THE_USER="to the user";
    public static final String FROM_THE_USER="from the user";
    public static final String SUCCESS_STATUS="success";
    public static final String FAILED_STATUS="Failed";
    public static final String ERROR_STATUS="error";
    public static final String USER_MANAGEMENT_TYPE="USER MANAGEMENT";
    public static final String USER_STATUS_TYPE="User Status";
    public static final String FETCH_ROLES_SUCCESSFUL="Fetch Roles Successful";
    public static final String FETCH_ROLES_UNSUCCESSFUL="Fetch Roles Successful";
    public static final String FETCH_USER_SUCCESSFUL="Fetch User Successful";
    public static final String FETCH_GROUP_ALERT_SETTINGS_SUCCESSFUL="Fetch Group Alert Settings Successful";
    public static final String GROUP_ALERT_SETTINGS_NOT_FOUND="Group Alert Settings Not Found";    
    public static final String CHANGED="changed";
    public static final String Active_USER_LOGIN="User logged in successfully";
    public static final String PENDING_USER_LOGIN="This account is not active, please contact the admin";
    public static final String BLOCKED_USER_LOGIN="This account is blocked due to the security concerns";
    public static final String SUSPENDED_USER_LOGIN="This account is suspended temporarily, please contact the admin";
    public static final String DEACTIVATED_USER_LOGIN="This account is deactivated";
	public static final String LOGIN_ACTION = "Login";
	public static final String EMPTY = "empty";
	public static final String ROLE_MANAGEMENT_TYPE="Role Management";
	public static final String GROUP_MANAGEMENT_TYPE="Group Management";
	public static final String EMPTY_ROLEID_OR_ROLENAME = "Role Id/Role Name can not be null";
	public static final String EMPTY_ROLEID = "Role Id can not be null";
	public static final String DELETE_ROLE_SUCCESSFUL = "Role Deleted Successfully";
	public static final String DELETE_ROLE_UNSUCCESSFUL = "Role Deletion failed";
	public static final String ROLE_ASSIGN_USER_SUCCESSFUL = "Users Assigned to Role Successfully";
	public static final String ROLE_ASSIGN_USER_FAILED = "Users assignment to Role failed as it is already assigned";
	public static final String EMPTY_USER_IDs = "User IDs can not be Empty or null";
	public static final String REMOVE_USER_FROM_ROLE_SUCCESSFUL = "User removed from role successfully";
	public static final String REMOVE_USER_FROM_ROLE_UNSUCCESSFUL = "Failed to remove user from role";
	public static final String EMPTY_ROLEID_USERID = "Role and User id can not be empty or null";
	public static final String ROLE_USER_NOT_FOUND = "Role User Association not found!";
	public static final String FILE_CONVERSION_ERROR = "File conversion Error";
	public static final String FILE_NOT_FOUND = "File not found";
	public static final Object FIREBASE_SOURCE = "Firebase";
	public static final String USERID_LIST_CANNOT_BE_EMPTY = "User IDs list cannotbe empty";
	public static final String ADD_ALERT_GROUP_SETTINGS_SUCCESSFUL = "Successfully added alert group settings";
	public static final String ADD_ALERT_GROUP_SETTINGS_FAILED = "Failed to add alert group settings";
	public static final String FEED_CLASSIFICATION_TYPE="FEED_CLASSIFICATION_TYPE";
	public static final String JURISDICTION_TYPE_STRING="JURISDICTION_TYPE";
	public static final String ENTITY_IDENTIFICATION = "Entity Identification";
	public static final String ALERT_NEW_STATUS = "New";
	public static final String IDENTITY_APPROVED_STATUS = "Identity Approved";
	public static final String IDENTITY_REJECTED_STATUS = "Identity Rejected";
	public static final String ENTITY_APPROVED_NEEDED_REVIEW = "Identity Approved, Needed Review";
	public static final String ENTITY_REJECTED_NEEDED_REVIEW = "Identity Rejected, Needed Review";
	public static final String ALERT_APPROVED_STATUS = "Approved";
	public static final String ALERT_REJECTED_STATUS = "Rejected";
	public static final String APPROVED_NEEDED_REVIEW = "Approved, Needed Review";
	public static final String REJECTED_NEEDED_REVIEW = "Rejected, Needed Review";		
	public static final String FEED_CLASSIFICATION_STRING = "Feed Classification";
	public static final String FEED_SOURCE_TYPE = "Feed Source";
	public static final String ALERT_STATUS_TYPE = "Alert Status";
	public static final String ID_NOT_FOUND="id not found";
	public static final String ROLE_NAME_USER = "User";
	public static final String ROLE_NAME_ANALYST = "Analyst";
	public static final String ROLE_NAME_ADMIN = "Admin";
	public static final String GENERAL_SETTING = "GENERAL_SETTING";
	public static final String DELETE_GROUPS_SUCCESSFUL="Groups Deleted Successfully";
	public static final String DELETE_GROUPS_UNSUCCESSFUL="Groups Deletion failed";
	public static final String EMPTY_GROUPSID = "Groups Id can not be null";
	public static final String GROUPS_NOT_FOUND="Groups not found";
	public static final String GROUPS_MANAGEMENT_TYPE="Groups Management";
	public static final String USER_MANAGEMENT_REGULATION_STRING="USER_MANAGEMENT_REGULATION"; 
	public static final String TWO_WAY_AUTHENTICATION_STRING="2-way Authentication";
	public static final String ALERTS_CREATION_SUCCESSFUL = "Alerts Created Successfully";
	public static final String ALERTS_CREATION_FAILED = "Failed to create the alerts"; 
	public static final String EMPTY_VALUE="";
	public static final String ALERT_REVIEW_CONFIRMED_STATUS = "Confirmed";
	public static final String ALERT_REVIEW_UNCONFIRMED_STATUS = "UNConfirmed";
	public static final String ALERT_REVIEW_DECLINED_STATUS = "Declined";
	public static final String ALERT_STATUS_OPEN = "Open";
	public static final String GROUPS_DELETION_FAILED = "Failed to delete group";
	public static final String FILE_FORMAT_ERROR = "File format Error";

	public static final String FULL_NAME_NOT_FOUND="Full Name shouldn't be empty and must be less than 500";
	public static final String COUNTRY_NOT_FOUND = "Country shouldn't be empty";
	public static final String ENTITY_TYPE_ERROR = "Entity Type should be person /organization";
	public static final String JURISDICTION_STRING="Jurisdictions";
	public static final String ENTITY_NAME_NOT_FOUND="Entity name not found";
    public static final String DATE_FORMAT_ERROR = "Date should be yyyy-mm-dd in this format"; 
    public static final String JURISDICTIONS_STRING = "Jurisdictions";
	public static final String ALERT_STATUS_VALIDATION_FAILED = "Alert status validation failed!"; 
	
	public static final String ENTITY_TYPE_MISSED = "Entity Type missed in header";
	public static final String ENTITY_ID_MISSED = "Entity ID missed in header";
	public static final String ENTITY_NAME_MISSED = "Entity Name missed in header";
	public static final String BIRTH_DATE_MISSED = "Birth Date/Registration Date missed in header";
	public static final String BIRTH_PLACE_MISSED = "Birth Place/Country of Registration missed in header";
	public static final String EXTRA_COLUMNS_EXISTED = "5 columns only allowed for uploaded file";
	public static final String NO_RECORDS_FOUND = "No records found";
	public static final String ACCEPT_FILE_MSG = "Allowed .csv file only";
	public static final String HEADER_MISSED = "Header should have Entity Type, Entity ID, Entity Name, Birth Date/Registration Date and Birth Place/Country of Registration";	
    // Prevents instantiation
    private ElementConstants() {
    }

}

