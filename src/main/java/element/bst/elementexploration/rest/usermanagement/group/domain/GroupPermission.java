package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_um_role_permissions")
public class GroupPermission implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "rolePrivilegeId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(value = "Role Privilege ID")
	private Long groupPrivilegeId;

	@ApiModelProperty(value = "Privilege ID")
	@ManyToOne(cascade = { CascadeType.ALL },fetch = FetchType.LAZY)
	@JoinColumn(name = "permissionId")
	@Access(AccessType.PROPERTY)
	private Permissions permissionId;

	@ApiModelProperty(value = "Privilege Level")
	private Integer permissionLevel;

	@ApiModelProperty(value = "Role ID")
	@ManyToOne(cascade = { CascadeType.ALL },fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId")
	private Roles roleId;

/*	@JsonIgnore
	@ApiModelProperty(value = "GroupId")
	@ManyToOne( cascade = { CascadeType.ALL },fetch = FetchType.LAZY)
	@JoinColumn(name = "usergroupId",referencedColumnName="id")
	private Groups groupId;*/

	public Permissions getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Permissions permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(Integer permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public Roles getRoleId() {
		return roleId;
	}

	public void setRoleId(Roles roleId) {
		this.roleId = roleId;
	}


	/*public Groups getGroupId() {
		return groupId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}*/

	public Long getGroupPrivilegeId() {
		return groupPrivilegeId;
	}

	public void setGroupPrivilegeId(Long groupPrivilegeId) {
		this.groupPrivilegeId = groupPrivilegeId;
	}

	public GroupPermission(Permissions permissionId, Integer permissionLevel, Roles roleId, Groups groupId,Long groupPrivilegeId) {
		//super();
		this.permissionId = permissionId;
		this.permissionLevel = permissionLevel;
		this.roleId = roleId;
		//this.groupId = groupId;
		this.groupPrivilegeId=groupPrivilegeId;
	}
	public GroupPermission(){
		
	}
	

}
