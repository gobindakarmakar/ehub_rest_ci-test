package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * @author suresh
 *
 */
@ApiModel("Table")
public class TableDto implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	/*private  String contentType;
	@JsonProperty("content")
	private  List<ValueDto> contentTypeTableList;
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public List<ValueDto> getContentTypeTableList() {
		return contentTypeTableList;
	}
	public void setContentTypeTableList(List<ValueDto> contentTypeTableList) {
		this.contentTypeTableList = contentTypeTableList;
	}
	*/
	private List<RowsDto> rows;

	public List<RowsDto> getRows() {
		return rows;
	}

	public void setRows(List<RowsDto> rows) {
		this.rows = rows;
	}
	
	
}
