package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class GroupPermissionsDtoForPermissions implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Role Privilege ID")
	private Long groupPrivilegeId;

	@ApiModelProperty(value = "Privilege ID")
	private PermissionsDtoForPermissions permissionId;

	@ApiModelProperty(value = "Privilege Level")
	private Integer permissionLevel;

	@ApiModelProperty(value = "Role ID")
	private RolesDtoForPermissions roleId;

	// @JsonIgnore
	@ApiModelProperty(value = "GroupId")
	private GroupsDtoForPermissions groupId;

	public Long getGroupPrivilegeId() {
		return groupPrivilegeId;
	}

	public void setGroupPrivilegeId(Long groupPrivilegeId) {
		this.groupPrivilegeId = groupPrivilegeId;
	}

	public PermissionsDtoForPermissions getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(PermissionsDtoForPermissions permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(Integer permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public RolesDtoForPermissions getRoleId() {
		return roleId;
	}

	public void setRoleId(RolesDtoForPermissions roleId) {
		this.roleId = roleId;
	}

	public GroupsDtoForPermissions getGroupId() {
		return groupId;
	}

	public void setGroupId(GroupsDtoForPermissions groupId) {
		this.groupId = groupId;
	}

}
