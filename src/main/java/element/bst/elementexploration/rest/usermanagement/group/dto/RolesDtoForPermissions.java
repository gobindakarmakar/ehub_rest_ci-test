package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class RolesDtoForPermissions implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Role ID")
	private Long roleId;

	@ApiModelProperty(value = "Role Name")
	private String roleName;

	@ApiModelProperty(value = "Icon for each role")
	private String icon;

	@ApiModelProperty(value = "Color for each role")
	private String color;

	@ApiModelProperty(value = "Role Description")
	private String description;

	@ApiModelProperty(value = "Role notes")
	private String notes;

	@ApiModelProperty(value = "Created On")
	private Date createdOn;

	@ApiModelProperty(value = "Modified On")
	private Date modifiedOn;

	@ApiModelProperty(value = "Source of role creation")
	private String source;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
