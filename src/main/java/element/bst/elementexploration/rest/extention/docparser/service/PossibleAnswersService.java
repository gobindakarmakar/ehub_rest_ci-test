package element.bst.elementexploration.rest.extention.docparser.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface PossibleAnswersService extends GenericService<PossibleAnswers, Long>{
	
	public boolean deleteAnswerOptions(Long possibleAnswerId,Long currentUserId);
	List<PossibleAnswers> fetchPossibleAnswers(Long questionId);
}
