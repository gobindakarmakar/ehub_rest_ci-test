package element.bst.elementexploration.rest.extention.textsentiment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.extention.textsentiment.domain.TextSentiment;
import element.bst.elementexploration.rest.extention.textsentiment.service.TextSentimentService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Text sentiment API" },description="Manages text sentiment data")
@RestController
@RequestMapping("/api/textSentiment")
public class TextSentimentController extends BaseController{
	
	@Autowired
	TextSentimentService textSentimentService;

	@ApiOperation("Posts text sentiment data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = byte[].class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/sentiment", consumes = {"application/json; charset=UTF-8" }, 
			produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> textSentiment(@Valid @RequestBody TextSentiment textSentiment, BindingResult results,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		if (!results.hasErrors()) {
			return new ResponseEntity<>(textSentimentService.getTextSentiments(textSentiment), HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}
}
