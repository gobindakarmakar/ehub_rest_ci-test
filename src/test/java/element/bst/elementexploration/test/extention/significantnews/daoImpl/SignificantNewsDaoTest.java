package element.bst.elementexploration.test.extention.significantnews.daoImpl;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.significantnews.daoImpl.SignificantNewsDaoImpl;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;

@RunWith(MockitoJUnitRunner.class)
public class SignificantNewsDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@InjectMocks
	SignificantNewsDaoImpl significantNewsDaoImpl;
	
	public SignificantNews significantNews;
	
	@Before
	public void setupMock() {
		significantNews = new SignificantNews(1L, "2L", "significantNews", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "title", "/news", "newsClass", 2L,true,"test");
	
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getSignificantNewsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(significantNews);
		SignificantNews newResult = significantNewsDaoImpl.getSignificantNews("2L", "significantNews", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "title", "/news", "newsClass");
		assertEquals(significantNews.getId(), newResult.getId());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getSignificantNews_whenExceptionthrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		significantNewsDaoImpl.getSignificantNews("2L", "significantNews", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "title", "/news", "newsClass");
	}
	
	@Test
	public void deleteSignificantNewsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenReturn(1);
		int Result =  significantNewsDaoImpl.deleteSignificantNews("2L", "significantNews", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "title", "/news", "newsClass");
		assertEquals(1, Result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void deleteSignificantNews_whenExceptionthrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenThrow(HibernateException.class);
		significantNewsDaoImpl.deleteSignificantNews("2L", "significantNews", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "title", "/news", "newsClass");
	}
}
