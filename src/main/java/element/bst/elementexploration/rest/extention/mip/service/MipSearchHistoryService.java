package element.bst.elementexploration.rest.extention.mip.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.mip.domain.MipSearchHistory;
import element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface MipSearchHistoryService extends GenericService<MipSearchHistory, Long> {
	
	List<MipSearchHistoryDTO> getSearchHistoryList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countSearchHistoryList(Long userId);

	boolean isSearchHistoryOwner(Long searchId, Long currentUserId);
}
