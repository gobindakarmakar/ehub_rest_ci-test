package element.bst.elementexploration.test.usermanagement.user.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.usermanagement.user.daoImpl.UserDaoImpl;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDaoTest {
	
	@Mock
	 SessionFactory sessionFactory;
	
	@Mock
	Session session;
	
	@SuppressWarnings("rawtypes")
	@Mock
	Query query;
	
	@SuppressWarnings("rawtypes")
	@Mock
	NativeQuery nativeQuery;
	
	@Mock
	CriteriaBuilder builder;
	
	@Mock
	CriteriaQuery<Object> criteriaQuery;
	
	@Mock
	Root<User> root;
	
	@InjectMocks
	UserDaoImpl userDao;
	
	List<Long> list = new ArrayList<Long>();
	
	public List<UserDto> users = new ArrayList<UserDto>();
	
	public User user;
	
	public List<User> userList = new ArrayList<User>();
	
	@Before
	public void doSetup() {
		users.add(new UserDto(1L, "analyst@test.com", "analyst", "analyst", "",
				"analyst", new Date(), "UK", "test", new Date()));
		users.add(new UserDto(2L, "junit@test.com", "user", "user", "",
				"testuser", new Date(), "UK", "test", new Date()));
		user=new User("analyst","Analyst","Analyst",new Date(),"1");
		userList.add(user);
		
		list.add(1L);
		list.add(2L);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getUserListTest(){	
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		List<UserDto> userList = userDao.getUserList(1L, "test", 1, 11,"username", "desc");
		assertEquals(2,userList.size());
	}
	
	@Test
	public void getUserList_whenGroupIdisNullTest(){	
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		List<UserDto> userList = userDao.getUserList(null, "test", 1, 11,"username", "desc");
		assertEquals(2,userList.size());
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserList_ExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		List<UserDto> userList = userDao.getUserList(1L, "test", 1, 11,"username", "desc");
		assertEquals(0,userList.size());
	}
	
	@Test
	public void getUserListFullTextSearchTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		List<UserDto> userList = userDao.getUserListFullTextSearch("junit","test",1L, 1, 11,"username", "desc");
		assertEquals(2,userList.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserListFullTextSearch_ExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		List<UserDto> userList = userDao.getUserListFullTextSearch("junit","test",1L, 1, 11,"username", "desc");
		assertEquals(0,userList.size());
	}
	
	@Test
	public void countUserListTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		long count = userDao.countUserList(1L,"test");
		assertEquals(2l,count);
	}
	
	@Test
	public void countUserList_whenGroupIdIsNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		long count = userDao.countUserList(null,"test");
		assertEquals(2l,count);
	}
	

	@SuppressWarnings("unchecked")
	@Test
	public void countUserList_ExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		long count = userDao.countUserList(1L,"test");
		assertEquals(0l,count);
	}
	
	@Test
	public void countUserListFullTextSearchTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		long count = userDao.countUserListFullTextSearch("analyst","test",1L);
		assertEquals(2l,count);
	}
	

	@SuppressWarnings("unchecked")
	@Test
	public void countUserListFullTextSearch_ExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		long count = userDao.countUserListFullTextSearch("analyst","test",1L);
		assertEquals(0l,count);
	}
	

	@Test
	public void listUsersByGroupTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		List<UserDto> userList = userDao.listUsersByGroup(1L, 1, 1, 11, "username", "desc");
		assertEquals(2,userList.size());
	}
	

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void listUsersByGroup_WhenExceptionThrowsTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		userDao.listUsersByGroup(1L, 1, 1, 11, "username", "desc");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByEmailOrUsernameTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(user);
		User resultUser = userDao.getUserByEmailOrUsername("analyst@test.com");
		assertEquals("1",resultUser.getStatus());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByEmailOrUsername_whenNoResultExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		User resultUser = userDao.getUserByEmailOrUsername("analyst@test.com");
		assertEquals(null,resultUser);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getUserByEmailOrUsername_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		userDao.getUserByEmailOrUsername("analyst@test.com");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void checkUserNameOrEmailExistsTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(user);
		User resultUser = userDao.checkUserNameOrEmailExists("analyst@test.com", 2L);
		assertEquals("1",resultUser.getStatus());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void checkUserNameOrEmailExists_whenNoResultExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		User resultUser = userDao.checkUserNameOrEmailExists("analyst@test.com", 2L);
		assertEquals(null,resultUser);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void checkUserNameOrEmailExists_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		userDao.checkUserNameOrEmailExists("analyst@test.com", 2L);
	}
	
	@Test
	public void countUserByGroupTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(users);
		Long count = userDao.countUserByGroup(1L, 1);
		assertEquals(new Long("2"),count);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countUserByGroup_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		userDao.countUserByGroup(1L, 1);
	}
	/*
	@SuppressWarnings("unchecked")
	@Test
	public void getUsersInListTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(User.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(userList);
		List<User> newUserList  = userDao.getUsersInList(list);
		assertEquals(1, newUserList.size());
	}
	*/
	@Test
	public void getAnalystByUserIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(user);
		User foundUser = userDao.getAnalystByUserId(1L);
		assertEquals("1",foundUser.getStatus());
	}
	
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAnalystByUserId_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		userDao.getAnalystByUserId(1L);
	}
	
	@Test
	public void getAllAnalystWhoDidNotRejectedCaseBeforeTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(userList);
		List<User> newUserList  = userDao.getAllAnalystWhoDidNotRejectedCaseBefore(1L);
		assertEquals(1,newUserList.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAllAnalystWhoDidNotRejectedCaseBefore_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		userDao.getAllAnalystWhoDidNotRejectedCaseBefore(1L);
	}
	

}
