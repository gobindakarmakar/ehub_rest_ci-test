package element.bst.elementexploration.rest.exception;

public class DateFormatException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  String message;
	
	private DateFormatException exp;
	public static final String DATE_FORMAT_EXCEPTION="Please ensure date format is dd-MM-yyyy";
	
	public DateFormatException(String message) {
		super();
		this.message = message;
	}



	public DateFormatException() {

		super();
		message = DATE_FORMAT_EXCEPTION;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	
	/**
	 * @return the exp
	 */
	public DateFormatException getExp() {
		return exp;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DateFormatException [message=" + message + "]";
	}
}
