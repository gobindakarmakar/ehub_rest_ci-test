package element.bst.elementexploration.rest.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.BadRequestException;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
@Component
public class PdfReaderUtil {
	public static Map<String, String> parsePdfFile(MultipartFile uploadFile) throws BadRequestException {
		List<String> wordList = new ArrayList<String>();
		List<Map<Double, StringBuilder>> finalMap = new ArrayList<Map<Double, StringBuilder>>();
		Map<String, String> jsonResponseMap = new LinkedHashMap<String, String>();
		PDDocument document = null;
		try {
			document = PDDocument.load(uploadFile.getInputStream());
			PDFTextStripper stripper = new CustomTextStripper(document, wordList);
			stripper.setSortByPosition(true);

			for (int i = 1; i <= document.getNumberOfPages(); i++) {
				Map<Double, StringBuilder> map = new LinkedHashMap<Double, StringBuilder>();
				stripper.setStartPage(i);
				stripper.setEndPage(i);
				Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
				// getting page content as single character
				stripper.writeText(document, dummy);
				// grouping all words based on yaxis
				Double compare = 0.0;
				StringBuilder builder = new StringBuilder();
				for (String stringBuilder : wordList) {
					String[] array = stringBuilder.split("\\s+");
					String val = array[0];
					Double integer = Double.valueOf(val);
					compare = integer;
					if (map.containsKey(compare)) {
						map.put(integer, builder.append(array[1] + " "));
					} else {
						builder = new StringBuilder();
						map.put(integer, builder.append(array[1] + " "));
					}
				}
				// getting answers along with page no
				Map<Double, String> answersmap = getAnswerPositions(document);
				finalMap.add(map);
				// assigning answer with questions for respective page
				if(answersmap.size()!=0){
				for (Map.Entry<Double, String> entryAnswer : answersmap.entrySet()) {
					List<Double> belowAnswers = new ArrayList<Double>();
					Double answerYvalue = entryAnswer.getKey();
					String answer = entryAnswer.getValue();
					String pageAnswer = answer.substring(answer.lastIndexOf(" ") + 1);
					if (Integer.parseInt(pageAnswer) == i-1) {
						for (Map.Entry<Double, StringBuilder> entryQuestion : map.entrySet()) {

							if (entryQuestion.getKey() < answerYvalue)
								belowAnswers.add(entryQuestion.getKey());
						}
						StringBuilder questionFound = map.get(Collections.max(belowAnswers));
                   	 String replace = questionFound.toString();
                   	 String replaceExtra = replace.replaceAll("_"," ");
                   	 String replaceExtraOne = replaceExtra.replaceAll("\\s+"," ");
                   	 StringBuilder replaceExtraBuilder = new  StringBuilder();
                   	 replaceExtraBuilder.append(replaceExtraOne);
                   	 //String toInsert = questionFound.toString();
                   	 String toInsert = replaceExtraBuilder.toString();
						jsonResponseMap.put(toInsert, answersmap.get(answerYvalue));
					} // if answer close
				}}
				else{
					//throw new BadRequestException("Document is not fillable pdf");
				}
				wordList.clear();
				map = new LinkedHashMap<Double, StringBuilder>();
			} // page ending for loop close

		} catch (Exception e) {
			// TODO: handle exception
		}
		return jsonResponseMap;
	}

	public static Map<Double, String> getAnswerPositions(PDDocument document)
			throws InvalidPasswordException, IOException {
		Map<Double, String> answersmap = new LinkedHashMap<Double, String>();
		PDPage page = document.getPage(0);
        PDRectangle pdRectangle = page.getCropBox();
		// gettinng answers for respective page.
		//// PDPage page = document.getPage(0);
		//// PDRectangle pdRectangle = page.getCropBox();
		PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
		
		if (acroForm != null) {

			for (PDField form : acroForm.getFieldTree())
			{
				for (PDAnnotationWidget widget : form.getWidgets())
		        {
					COSDictionary widgetObject = widget.getCOSObject();
				    PDPageTree pages = document.getPages();
				    for (int i = 0; i < pages.getCount(); i++)
				    {
				        for (PDAnnotation annotation : pages.get(i).getAnnotations())
				        {
				            COSDictionary annotationObject = annotation.getCOSObject();
				            if (annotationObject.equals(widgetObject))
				            {
				            	 COSDictionary field = (COSDictionary) form.getCOSObject();
								 COSArray rectArray= (COSArray)field.getDictionaryObject("Rect");
								//map.put(form.getFullyQualifiedName(), form.getValueAsString());
								if(rectArray!=null)
								{
									COSBase base= rectArray.get(0);
								 PDRectangle mediaBox = new PDRectangle( rectArray ); 
								 //System.out.println(mediaBox.getHeight() +" "+ mediaBox.getWidth());
								 double val =  pdRectangle.getHeight()-(mediaBox.getUpperRightY());
								 double val1=  pdRectangle.getHeight()-(mediaBox.getLowerLeftY());
									//System.out.println("mediaBox: " + mediaBox.getLowerLeftX()  +"||" +val1);
									//System.out.println("mediaBox: " + mediaBox.getUpperRightX()  +"||" +val);
								//System.out.println(val1+ " "+ form.getAlternateFieldName()+"**"+form.getValueAsString() + "**" +"page number" +i);
								 String type = null;
									if (form instanceof PDCheckBox) {
										type = "CHECKBOX";
									} else if (form instanceof PDTextField) {
										type = "TEXT";
									}
									answersmap.put(val1, form.getFullyQualifiedName() +"&"+ type + "&" + form.getValueAsString()+" "+i);
								//answersmap.put(val1, form.getFullyQualifiedName()+" "+form.getValueAsString() + " "+i);
								//System.out.println("----------------------------");
								}//if close
				            }
				        }
				    }//page close
		        }
			}//pdfield close for
		} // acroform if close
			// getAnswerPositions1(i);
		//modifyAnswersPageNumber(answersmap);
		return answersmap;
	}

	public static void modifyAnswersPageNumber(Map<Double, String> map) {
		int i = 1;
		String temp = "";
		boolean value = true;
		for (Map.Entry<Double, String> entry : map.entrySet()) {
			String pageNumberString = entry.getValue();
			String last = pageNumberString.substring(pageNumberString.lastIndexOf(" ") + 1);
			if (temp.equals(last) || value) {
				if (value) {
					String replace = entry.getValue();
					String re = replace.replace(last, String.valueOf(i));
					map.replace(entry.getKey(), entry.getValue(), re);
					temp = last;
					value = false;
				}
				String replace = entry.getValue();
				String re = replace.replace(last, String.valueOf(i));
				map.replace(entry.getKey(), entry.getValue(), re);
			} else {
				String replace = entry.getValue();
				String re = replace.replaceAll(last, String.valueOf(i + 1));
				map.replace(entry.getKey(), entry.getValue(), re);
				temp = last;
				i++;
			}
		}

	}
}
