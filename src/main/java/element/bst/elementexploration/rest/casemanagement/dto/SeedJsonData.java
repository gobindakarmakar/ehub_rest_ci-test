package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeedJsonData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String type;
	
	private List<String> persons = new ArrayList<>();
	
	private List<String> companies = new ArrayList<>();
	
	private List<String> websites = new ArrayList<>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getPersons() {
		return persons;
	}

	public void setPersons(List<String> persons) {
		this.persons = persons;
	}

	public List<String> getCompanies() {
		return companies;
	}

	public void setCompanies(List<String> companies) {
		this.companies = companies;
	}

	public List<String> getWebsites() {
		return websites;
	}

	public void setWebsites(List<String> websites) {
		this.websites = websites;
	}

	

}
