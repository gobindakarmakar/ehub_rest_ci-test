package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("AlertScenarioDto")
public class AlertScenarioDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Alert id")
	private Long id;
	@ApiModelProperty(value="Alert scenario")
	private String scenario;
	@ApiModelProperty(value="Alert transaction amount")
	private double amount;
	@ApiModelProperty(value="Count")
	private long count;
	@ApiModelProperty(value="Alert scenario type")
	private String alertScenarioType;
	@ApiModelProperty(value="Alert description")
	private String alertDescription;
	@ApiModelProperty(value="Maximum amount")
	private Double maximumAmount;
	
	

	public AlertScenarioDto() {
	}

	public AlertScenarioDto(String scenario, double amount, long count) {
		super();
		this.scenario = scenario;
		this.amount = amount;
		this.count = count;
	}
	
	public AlertScenarioDto(Long id, String scenario, double amount, String alertScenarioType, String alertDescription,
			Double maximumAmount) {
		super();
		this.id = id;
		this.scenario = scenario;
		this.amount = amount;
		this.alertScenarioType = alertScenarioType;
		this.alertDescription = alertDescription;
		this.maximumAmount = maximumAmount;
	}
	
	
	

	public AlertScenarioDto(Long id, String scenario, String alertScenarioType, String alertDescription) {
		super();
		this.id = id;
		this.scenario = scenario;
		this.alertScenarioType = alertScenarioType;
		this.alertDescription = alertDescription;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlertScenarioType() {
		return alertScenarioType;
	}

	public void setAlertScenarioType(String alertScenarioType) {
		this.alertScenarioType = alertScenarioType;
	}

	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

		
}
