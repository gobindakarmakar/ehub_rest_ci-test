package element.bst.elementexploration.rest.logging.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class LogAggregator implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@ApiModelProperty("date")
	private String timestamp;
	
	@ApiModelProperty("Number of audit logs")
	private Long count;

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public LogAggregator( String timestamp,Long count) {
		super();
		this.count = count;
		this.timestamp = timestamp;
	}
	
	

}
