package element.bst.elementexploration.rest.usermanagement.security.service;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.security.domain.FailedLogin;

/**
 * 
 * @author Amit Patel
 *
 */
public interface FailedLoginService extends GenericService<FailedLogin, Long>{

	public boolean isAccountLocked(String username);
	
	public boolean releaseAccountLocked(String username, String ipAddress);
	
	public void processFailedAuthentication(String emailAddress, String screenName, String ipAddress);

	public FailedLogin getFailedLoginByUsername(String username);
	
	int getCurrentLoginAttempt(String username, String screenName, String ipAddress);
}
