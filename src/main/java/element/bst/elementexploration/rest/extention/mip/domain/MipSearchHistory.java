package element.bst.elementexploration.rest.extention.mip.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Type;

import io.swagger.annotations.ApiModelProperty;

@Table(name = "bst_mip_search_history")
@Entity
public class MipSearchHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Search ID")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "search_id")
	private Long searchId;

	@ApiModelProperty(value = "Name of the entity")
	@Column(name = "name")
	private String name;

	@ApiModelProperty(value = "Data of the entity")
	@Column(name = "data")
	@Type(type = "text")
	private String data;

	@ApiModelProperty(value = "Source page of the entity")
	@Column(name = "source_page")
	private String sourcePage;

	@ApiModelProperty(value = "Date of creation")
	@Column(name = "createdOn")
	private Date createdOn;

	@ApiModelProperty(value = "Date of updation")
	@Column(name = "updatedOn")
	private Date updatedOn;

	@ApiModelProperty(value = "ID of the user")
	@Column(name = "userId")
	private Long userId;

	public MipSearchHistory() {
		super();
	}

	public MipSearchHistory(Long searchId, String name, String data, String sourcePage, Date createdOn, Date updatedOn,
			Long userId) {
		super();
		this.searchId = searchId;
		this.name = name;
		this.data = data;
		this.sourcePage = sourcePage;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.userId = userId;
	}

	public Long getSearchId() {
		return searchId;
	}

	public void setSearchId(Long searchId) {
		this.searchId = searchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSourcePage() {
		return sourcePage;
	}

	public void setSourcePage(String sourcePage) {
		this.sourcePage = sourcePage;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
