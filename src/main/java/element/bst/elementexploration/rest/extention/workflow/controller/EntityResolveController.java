package element.bst.elementexploration.rest.extention.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.workflow.service.EntityResolveService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "Entity resolver API" },description="Manages entity resolver data")
@RestController
@RequestMapping("/api/workflow/er/entity")
public class EntityResolveController extends BaseController{

	@Autowired
	private EntityResolveService entityResolveService;

	@ApiOperation("Resolves entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/resolve/{dir}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> resolve(@RequestParam String token, @PathVariable String dir) throws Exception {
		return new ResponseEntity<>(entityResolveService.resolve(dir), HttpStatus.OK);
	}

	@ApiOperation("Resolves entity by threshold")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/resolve/{dir}/{threshold}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> resolveWithThreshold(@RequestParam String token, @PathVariable String dir,
			@PathVariable Double threshold) throws Exception {
		return new ResponseEntity<>(entityResolveService.resolveWithThreshold(dir, threshold), HttpStatus.OK);
	}

	@ApiOperation("Resolves with threshold generated schema")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/resolve/{dir}/{threshold}/{generateSchema}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> resolveWithThresholdGenerateSchema(@RequestParam String token, @PathVariable String dir,
			@PathVariable Double threshold, @PathVariable String generateSchema) throws Exception {
		return new ResponseEntity<>(
				entityResolveService.resolveWithThresholdGenerateSchema(dir, threshold, generateSchema), HttpStatus.OK);
	}

	@ApiOperation("test service")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/test", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> testService(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entityResolveService.testService(), HttpStatus.OK);
	}

}
