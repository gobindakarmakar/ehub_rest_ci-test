package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Country")
public class CountryDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Country name")
	private String country;
	@ApiModelProperty(value="ISO 2 code of country")
	private String iso2Code;
	@ApiModelProperty(value="ISO 3 code of country")
	private String is03Code;
	@ApiModelProperty(value="Latitude of the location")
	private String latitude;
	@ApiModelProperty(value="Longitude of the location")
	private String longitude;
	@ApiModelProperty(value="basel AML index 2017 ranking of the country")
	private double baselAMLIndex2017;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIso2Code() {
		return iso2Code;
	}

	public void setIso2Code(String iso2Code) {
		this.iso2Code = iso2Code;
	}

	public String getIs03Code() {
		return is03Code;
	}

	public void setIs03Code(String is03Code) {
		this.is03Code = is03Code;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public double getBaselAMLIndex2017() {
		return baselAMLIndex2017;
	}

	public void setBaselAMLIndex2017(double baselAMLIndex2017) {
		this.baselAMLIndex2017 = baselAMLIndex2017;
	}

}
