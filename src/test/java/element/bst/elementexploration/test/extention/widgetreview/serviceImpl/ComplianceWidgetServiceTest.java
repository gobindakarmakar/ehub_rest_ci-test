package element.bst.elementexploration.test.extention.widgetreview.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.widgetreview.dao.ComplianceWidgetDao;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.dto.ComplianceWidgetDto;
import element.bst.elementexploration.rest.extention.widgetreview.serviceImpl.ComplianceWidgetServiceImpl;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ComplianceWidgetServiceTest {
	
	@Mock
	ComplianceWidgetDao complianceWidgetDao;
	
	@InjectMocks
	ComplianceWidgetServiceImpl ComplianceWidgetServiceMock;
	
	List<ComplianceWidget> ComplianceWidgetList = new ArrayList<ComplianceWidget>();
	
	@Before
	public void setupMock() {
		ComplianceWidgetList.add(new ComplianceWidget(1L,"Company Information"));
		ComplianceWidgetList.add(new ComplianceWidget(2L,"Company Operations"));
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getAllComplianceWidgetTest(){
		Mockito.when(complianceWidgetDao.findAll()).thenReturn(ComplianceWidgetList);
		List<ComplianceWidgetDto> complianceWidgetDtoList = ComplianceWidgetServiceMock.getAllComplianceWidget();
		assertEquals(2,complianceWidgetDtoList.size());
	}
}
