package element.bst.elementexploration.rest.search.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.search.domain.SearchHistory;
import element.bst.elementexploration.rest.search.service.SearchHistoryService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Search history API" }, description = "Manages search history")
@RestController
@RequestMapping("/api/search")
public class SearchHistoryController extends BaseController {

	@Autowired
	private SearchHistoryService searchHistoryService;

	@PostMapping(value = "/getData", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getData(@RequestParam("token") String token, @RequestParam("searchFlag") String searchFlag,
			@RequestBody String search, HttpServletRequest request) throws Exception {

		Long userId = getCurrentUserId();
		String response = searchHistoryService.getSearchData(userId, search, searchFlag);
		//searchHistoryService.createSearchHistory(userId, search, searchFlag);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/setFavourite", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> markAsFavourite(@RequestParam("token") String token,
			@RequestParam(required = false) Long searchId, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		boolean result = searchHistoryService.setFavouriteSearch(userId, searchId);
		if (result)
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SERACH_HISTORY_UPDATED_SUCCESSFULLY_MSG),
					HttpStatus.OK);
		else
			throw new NoDataFoundException(ElementConstants.SEARCH_HISTORY_NOT_FOUND);
	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/recentSearches", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserNotificationlist(@RequestParam("token") String token,
			@RequestParam(required = false) String dateFrom, @RequestParam(required = false) String dateTo,
			@RequestParam(required = false) Boolean favourite, @RequestParam(required = false) String flag,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			HttpServletRequest request) throws ParseException {

		Long userId = getCurrentUserId();
		List<SearchHistory> searchList = searchHistoryService.getRecentSearches(userId, dateFrom, dateTo, favourite,
				flag, pageNumber, recordsPerPage);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = searchHistoryService.countRecentSearches(userId, dateFrom, dateTo, favourite, flag);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(searchList != null ? searchList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", searchList);
		jsonObject.put("paginationInformation", information);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}

	@GetMapping(value = "/graphData/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphData(@RequestParam("token") String token, @PathVariable("caseId") String caseId,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(searchHistoryService.getCaseGraphData(caseId), HttpStatus.OK);
	}

	@PostMapping(value = "/saveSearch", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSearch(@RequestParam("token") String token,
			@RequestParam("searchFlag") String searchFlag, @RequestBody String search, HttpServletRequest request)
			throws Exception {
		Long userId = getCurrentUserId();
		Long searchId = searchHistoryService.createSearchHistory(userId, search, searchFlag);
		return new ResponseEntity<>(searchId, HttpStatus.OK);
	}
}
