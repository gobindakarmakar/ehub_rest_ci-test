package element.bst.elementexploration.rest.extention.gridview.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_grid_view")
public class GridView implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "grid_view_id")
	private Long gridViewId;

	@ApiModelProperty(value = "Metadata of grid view")
	@Column(name = "grid_view_metadata", columnDefinition = "LONGTEXT")
	private String gridViewMetaData;

	@ApiModelProperty(value = "Grid Table Name")
	@Column(name = "grid_table_name")
	private String gridTableName;

	@ApiModelProperty(value = "Grid Table Name")
	@Column(name = "grid_view_name")
	@NotNull(message = "View name cannot be blank")
	private String gridViewName;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "is_default")
	@NotNull(message = "isDefault cannot be blank")
	private boolean isDefaultView;

	public Long getGridViewId() {
		return gridViewId;
	}

	public void setGridViewId(Long gridViewId) {
		this.gridViewId = gridViewId;
	}

	public String getGridViewMetaData() {
		return gridViewMetaData;
	}

	public void setGridViewMetaData(String gridViewMetaData) {
		this.gridViewMetaData = gridViewMetaData;
	}

	public String getGridTableName() {
		return gridTableName;
	}

	public void setGridTableName(String gridTableName) {
		this.gridTableName = gridTableName;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getGridViewName() {
		return gridViewName;
	}

	public void setGridViewName(String gridViewName) {
		this.gridViewName = gridViewName;
	}

	public boolean isDefaultView() {
		return isDefaultView;
	}

	public void setDefaultView(boolean isDefaultView) {
		this.isDefaultView = isDefaultView;
	}

}
