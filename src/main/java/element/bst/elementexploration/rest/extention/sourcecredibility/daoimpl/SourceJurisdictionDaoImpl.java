package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceJurisdictionDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("sourceJurisdictionDao")
public class SourceJurisdictionDaoImpl extends GenericDaoImpl<SourceJurisdiction, Long>
		implements SourceJurisdictionDao {

	public SourceJurisdictionDaoImpl() {
		super(SourceJurisdiction.class);
	}

	@Override
	public SourceJurisdiction fetchJurisdiction(String name) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceJurisdiction s where s.jurisdictionName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", name);
			SourceJurisdiction sourceJurisdiction = (SourceJurisdiction) query.getSingleResult();
			if (sourceJurisdiction != null)
				return sourceJurisdiction;
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SourceJurisdiction> fetchJurisdictionExceptAll() {
		List<SourceJurisdiction> sourceJurisdictionList = new ArrayList<SourceJurisdiction>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceJurisdiction s where s.jurisdictionName <>:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", "All");
			sourceJurisdictionList = (List<SourceJurisdiction>) query.getResultList();
			return sourceJurisdictionList;
		} catch (Exception e) {
			return sourceJurisdictionList;
		}

	}

	@Override
	public SourceJurisdiction findByListItemId(Long listItemId) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceJurisdiction s where s.listItemId =:listId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("listId", listItemId);
			SourceJurisdiction sourceJurisdiction = (SourceJurisdiction) query.getSingleResult();
			if (sourceJurisdiction != null)
				return sourceJurisdiction;
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			return null;
		}
		return null;
	}
	
	@Override
	@Transactional("transactionManager")
	public int updateDBCollation(String schemaName) {
		
		StringBuilder hql = new StringBuilder("ALTER DATABASE `"+schemaName+"`");
		hql.append(" CHARACTER SET utf8 COLLATE utf8_general_ci");	
		try {
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			return query.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional("transactionManager")
	public List<String> getNonUTF8Tables(String schemaName) {
		List<String> tableNames = null;
		try {
			StringBuilder hql = new StringBuilder("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES");
			hql.append(" WHERE TABLE_SCHEMA='"+schemaName+"'");
			hql.append(" AND TABLE_TYPE='BASE TABLE'");
			hql.append(" AND TABLE_COLLATION != 'utf8_general_ci'");
			
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			tableNames = (List<String>) query.getResultList();	
		}catch(Exception e) {
			e.printStackTrace();
		}
		return tableNames;
	}
	
	@Override
	@Transactional("transactionManager")
	public int updateTableCollation(String tableName) {
		StringBuilder hql = new StringBuilder("ALTER TABLE "+tableName);
		hql.append(" CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci");
		try {
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			return query.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
