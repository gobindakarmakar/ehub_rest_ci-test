package element.bst.elementexploration.rest.extention.entity.complexStructure.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author hanuman
 *
 */
public class EntityComplexStructureDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("Entity Complex Structure id")
	private Long enityComplexStructureId;

	public EntityComplexStructureDto() {
		super();

	}

	@ApiModelProperty(value = "Entity ID if the Entity")
	private String entityId;
	
	@ApiModelProperty(value = "Entity Name if the Entity")
	private String entityName;

	@ApiModelProperty(value = "Source if the Entity")
	private String entitySource;

	@ApiModelProperty(value = "Source if the Entity")
	private Boolean isComplexStructure;

	public Long getEnity_complex_structure_id() {
		return enityComplexStructureId;
	}

	public void setEnity_complex_structure_id(Long enityComplexStructureId) {
		this.enityComplexStructureId = enityComplexStructureId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntitySource() {
		return entitySource;
	}

	public void setEntitySource(String entitySource) {
		this.entitySource = entitySource;
	}

	public Boolean getIsComplexStructure() {
		return isComplexStructure;
	}

	public void setIsComplexStructure(Boolean isComplexStructure) {
		this.isComplexStructure = isComplexStructure;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

}
