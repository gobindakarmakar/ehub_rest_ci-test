package element.bst.elementexploration.rest.casemanagement.daoImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDocumentDao;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("caseDocumentDao")
public class CaseDocumentDaoImpl extends GenericDaoImpl<CaseDocDetails, Long> implements CaseDocumentDao {

	public CaseDocumentDaoImpl() {
		super(CaseDocDetails.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDocDetails> getAllDocumentsForCaseSeed(Long caseSeedId, Integer docFlag,String orderBy, Integer pageNumber, Integer recordsPerPage) {
		List<CaseDocDetails> caseSeedDocuments = new ArrayList<>();
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append("FROM CaseDocDetails WHERE caseSeed.caseId=:caseSeedId");
		if(docFlag != null){
			sqlString.append(" and docFlag=:docFlag");
		}
		if((orderBy != null) && (!orderBy.isEmpty())) {
			sqlString.append(" order by ");
			sqlString.append(orderBy);
		}	
			
		try {		   
		    Query<?> query = getCurrentSession().createQuery(sqlString.toString());
		    query.setParameter("caseSeedId", caseSeedId);
		    if(docFlag != null)
		    query.setParameter("docFlag", docFlag);
		    query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		    query.setMaxResults(recordsPerPage);
		    caseSeedDocuments = (List<CaseDocDetails>) query.getResultList();
		} catch (HibernateException e) {
		    ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
			    e, CaseDocumentDaoImpl.class);
		} 
		return caseSeedDocuments;		
	}

	@Override
	public Long getCaseIdFromDoc(long docId) {		
    	try{   	
    		String sqlQUery="SELECT caseSeed.caseId as caseId FROM CaseDocDetails where docId= :docId";
    		Query<?> query = this.getCurrentSession().createQuery(sqlQUery);
    		query.setParameter("docId", docId);
    		return (Long) query.getSingleResult();
    	}catch (NoResultException e) {
			return null;
		}catch(HibernateException e){
    		 ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ", e, CaseDocumentDaoImpl.class);
    		 throw new FailedToExecuteQueryException();
    	}		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countAllDocumentsForCaseSeed(Long caseSeedId,Integer docFlag) {
		List<CaseDocDetails> caseSeedDocuments = new ArrayList<>();
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append("FROM CaseDocDetails WHERE caseSeed.caseId=:caseSeedId");		
		if(docFlag != null){
			sqlString.append(" and docFlag=:docFlag");
		}
		try {		   
		    Query<?> query = getCurrentSession().createQuery(sqlString.toString());
		    query.setParameter("caseSeedId", caseSeedId);
		    if(docFlag != null)
			query.setParameter("docFlag", docFlag);
		    caseSeedDocuments = (List<CaseDocDetails>) query.getResultList();
		} catch (HibernateException e) {
		    ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
			    e, CaseDocumentDaoImpl.class);
		} 
		return (long) caseSeedDocuments.size();			
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocAggregatorDto> docAggregator(Long caseId, Date fromDate, Date toDate) {
		List<DocAggregatorDto> docAggregatorDtoList = new ArrayList<DocAggregatorDto>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select new element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto( ");
		queryBuilder.append(" docFlag,count(caseDocDetails) )");
		queryBuilder.append(" from CaseDocDetails caseDocDetails where caseDocDetails.caseSeed.caseId=:caseId");

		if (fromDate != null && toDate != null) {
			queryBuilder.append(" and caseDocDetails.uploadedOn BETWEEN ");
			queryBuilder.append("'" + sdf.format(fromDate) + "'");
			queryBuilder.append(" and '" + sdf.format(toDate) + "'");
		} else if (fromDate != null && toDate == null) {
			queryBuilder.append(" and caseDocDetails.uploadedOn >= '" + sdf.format(fromDate) + "' ");
		} else if (fromDate == null && toDate != null) {
			queryBuilder.append(" and caseDocDetails.uploadedOn < '" + sdf.format(toDate) + "' ");
		}

		queryBuilder.append(" Group By caseDocDetails.docFlag");
		try {
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("caseId", caseId);

			docAggregatorDtoList = (List<DocAggregatorDto>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ", e, CaseDocumentDaoImpl.class);
		}
		return docAggregatorDtoList;

	}

}
