package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Corporate Structure")
public class CorporateStructureDto implements Serializable {

	public CorporateStructureDto(Double alertAmount, Date date) {
		super();
		this.alertAmount = alertAmount;
		this.date = date;
	}

	public CorporateStructureDto(Date date, Double transactionAmount) {
		super();
		this.date = date;
		this.transactionAmount = transactionAmount;
	}

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Corporate structure of customer")
	private String corporateStructure;
	@ApiModelProperty(value = "Transaction amount")
	private Double transactionAmount;
	@ApiModelProperty(value = "Alert amount")
	private Double alertAmount;
	@ApiModelProperty(value = "Count of transactions")
	private Long transactionCount;
	@ApiModelProperty(value = "Count of alerts")
	private Long alertCount;
	@ApiModelProperty(value = "Alert ratio")
	private Double alertRatio;
	@ApiModelProperty(value = "Set of alert scenarios")
	private Set<String> scenarios;
	@ApiModelProperty(value = "List of alert scenarios")
	private List<String> scenario;
	@ApiModelProperty(value = "List of customer name")
	private List<String> customerName;
	@ApiModelProperty(value = "List of counter parties")
	private List<String> counterParty;
	@ApiModelProperty(value = "Type of transaction")
	private String type;
	@ApiModelProperty(value = "Customer id of the account holder")
	private Long customerId;
	@ApiModelProperty(value = "Transaction date")
	private Date date;
	@ApiModelProperty(value = "Name of the account holder")
	private String name;
	@ApiModelProperty(value = "Resident country of the account holder")
	private String residentCountry;
	@ApiModelProperty(value = "latitude of the account holder location")
	private String latitude;
	@ApiModelProperty(value = "Longitude of the account holder location")
	private String longitude;

	public CorporateStructureDto(String residentCountry, String latitude, String longitude, Double alertAmount,
			Long alertCount) {
		super();
		this.residentCountry = residentCountry;
		this.latitude = latitude;
		this.longitude = longitude;
		this.alertAmount = alertAmount;
		this.alertCount = alertCount;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getResidentCountry() {
		return residentCountry;
	}

	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CorporateStructureDto(Long customerId, String name, Date date, Double transactionAmount) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.date = date;
		this.transactionAmount = transactionAmount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(List<String> counterParty) {
		this.counterParty = counterParty;
	}

	public List<String> getCustomerName() {
		return customerName;
	}

	public void setCustomerName(List<String> customerName) {
		this.customerName = customerName;
	}

	public CorporateStructureDto() {
		super();
	}

	public CorporateStructureDto(Double alertAmount, Long alertCount) {
		super();
		this.alertAmount = alertAmount;
		this.alertCount = alertCount;
	}

	public CorporateStructureDto(String corporateStructure, Double alertAmount, Long alertCount) {
		super();
		this.corporateStructure = corporateStructure;
		this.alertAmount = alertAmount;
		this.alertCount = alertCount;
	}

	public CorporateStructureDto(Long transactionCount, Double transactionAmount, String type) {
		super();
		this.transactionCount = transactionCount;
		this.transactionAmount = transactionAmount;
		this.type = type;
	}

	public CorporateStructureDto(Double transactionAmount, Long alertCount, Double alertRatio, Long transactionCount) {
		super();
		this.transactionAmount = transactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionCount = transactionCount;
	}

	public CorporateStructureDto(String corporateStructure, Double transactionAmount, Long alertCount,
			Double alertRatio, Long transactionCount) {
		super();
		this.corporateStructure = corporateStructure;
		this.transactionAmount = transactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionCount = transactionCount;
	}

	public String getCorporateStructure() {
		return corporateStructure;
	}

	public void setCorporateStructure(String corporateStructure) {
		this.corporateStructure = corporateStructure;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Double getAlertAmount() {
		return alertAmount;
	}

	public void setAlertAmount(Double alertAmount) {
		this.alertAmount = alertAmount;
	}

	public Long getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(Long transactionCount) {
		this.transactionCount = transactionCount;
	}

	public Long getAlertCount() {
		return alertCount;
	}

	public void setAlertCount(Long alertCount) {
		this.alertCount = alertCount;
	}

	public Double getAlertRatio() {
		return alertRatio;
	}

	public void setAlertRatio(Double alertRatio) {
		this.alertRatio = alertRatio;
	}

	public Set<String> getScenarios() {
		return scenarios;
	}

	public void setScenarios(Set<String> scenarios) {
		this.scenarios = scenarios;
	}

	public List<String> getScenario() {
		return scenario;
	}

	public void setScenario(List<String> scenario) {
		this.scenario = scenario;
	}

	@Override
	public String toString() {
		return "CorporateStructureDto [corporateStructure=" + corporateStructure + ", transactionAmount="
				+ transactionAmount + ", alertAmount=" + alertAmount + ", transactionCount=" + transactionCount
				+ ", alertCount=" + alertCount + ", alertRatio=" + alertRatio + ", scenario=" + scenario
				+ ", counterParty=" + counterParty + "]";
	}

	public CorporateStructureDto(String type, Double transactionAmount) {
		super();
		this.transactionAmount = transactionAmount;
		this.type = type;
	}

	public CorporateStructureDto(Double transactionAmount, String type) {
		super();
		this.transactionAmount = transactionAmount;
		this.type = type;
	}

}
