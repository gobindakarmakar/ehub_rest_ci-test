package element.bst.elementexploration.rest.extention.docparser.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.extention.docparser.dto.Level1CodeDto;
import element.bst.elementexploration.rest.extention.docparser.service.IsoCategoryService;

public class IsoCategoryServiceTest {
	
	@Mock
	IsoCategoryService isoCategoryServiceMock;
	
	public Level1CodeDto level1CodeDto;
	
	
	@Before
    public void setupMock() {
		level1CodeDto = new Level1CodeDto();
		level1CodeDto.setlevel1Evaluation("A.1");
		MockitoAnnotations.initMocks(this);

	}
	
	@Test
	public void updateIsoCategoryEvaluationTest() {	
		Mockito.when(isoCategoryServiceMock.updateIsoCategoryEvaluation(Mockito.any(IsoCategory.class), Mockito.anyString(), Mockito.anyLong())).thenReturn(201);
		int status = isoCategoryServiceMock.updateIsoCategoryEvaluation(Mockito.any(IsoCategory.class), Mockito.anyString(), Mockito.anyLong());
		assertEquals(201,status);
	}
	
	@Test
	public void updateIsoCategoryEvaluationByAnswerTest() {	
		Mockito.when(isoCategoryServiceMock.updateIsoCategoryEvaluationByAnswer(Mockito.any(DocumentAnswers.class), Mockito.anyLong())).thenReturn(level1CodeDto);
		Level1CodeDto newLevel1CodeDto = isoCategoryServiceMock.updateIsoCategoryEvaluationByAnswer(Mockito.any(DocumentAnswers.class),Mockito.anyLong());
		assertEquals("A.1",newLevel1CodeDto.getlevel1Evaluation());
	}
	
	
		

}
