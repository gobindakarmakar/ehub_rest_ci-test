package element.bst.elementexploration.rest.extention.entity.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.entity.domain.EntityRequirements;
import element.bst.elementexploration.rest.extention.entity.service.EntityRequirementsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("entityRequirementsService")
@Transactional("transactionManager")
public class EntityRequirementsServiceImpl extends GenericServiceImpl<EntityRequirements, Long>
		implements EntityRequirementsService {

	@Autowired
	public EntityRequirementsServiceImpl(GenericDao<EntityRequirements, Long> genericDao) {
		super(genericDao);
	}

	public EntityRequirementsServiceImpl() {

	}

}
