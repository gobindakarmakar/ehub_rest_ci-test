package element.bst.elementexploration.rest.extention.entity.complexStructure.dao;
import element.bst.elementexploration.rest.extention.entity.complexStructure.domain.EntityComplexStructure;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author hanuman
 *
 */
public interface EntityComplexStructureDao extends GenericDao<EntityComplexStructure, Long> {
	EntityComplexStructure getEntityComplexStructure(String entityId, String entitySource,String entityName);

}