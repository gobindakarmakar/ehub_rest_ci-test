package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.advancesearch.dao.EntityOfficerInfoDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityOfficerInfo;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.EntityOfficerInfoService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Prateek Maurya
 *
 */
@Service("entityOfficerInfoService")
public class EntityOfficerInfoServiceImpl extends GenericServiceImpl<EntityOfficerInfo, Long> implements EntityOfficerInfoService{

	@Autowired
	EntityOfficerInfoDao entityOfficerInfoDao;
	
	public EntityOfficerInfoServiceImpl (GenericDao<EntityOfficerInfo, Long> genericDao){
		super(genericDao);
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean saveEntityOfficer(List<EntityOfficerInfoDto> entityOfficers){
		try {
			for(EntityOfficerInfoDto entityOfficerInfoDto : entityOfficers){
				EntityOfficerInfo existingOfficer = entityOfficerInfoDao.getOfficer(entityOfficerInfoDto);
				if(existingOfficer!=null && !existingOfficer.equals(null)){
					//EntityOfficerInfo entityOfficerInfo = new EntityOfficerInfo();
					//BeanUtils.copyProperties(entityOfficerInfo, entityOfficerInfoDto);
					entityOfficerInfoDao.delete(existingOfficer);
				}
					EntityOfficerInfo entityOfficerInfo = new EntityOfficerInfo();
					BeanUtils.copyProperties(entityOfficerInfo, entityOfficerInfoDto);
					entityOfficerInfoDao.create(entityOfficerInfo);
			}
			return true;
		} catch (Exception e) {
			return false;
		} 
	}

	@Override
	@Transactional("transactionManager")
	public List<EntityOfficerInfoDto> getEntityOfficerList(String entityId) {
		List<EntityOfficerInfo> entityList=entityOfficerInfoDao.getOfficerList(entityId);
		List<EntityOfficerInfoDto> entityDto = new ArrayList<EntityOfficerInfoDto>();
		try {
			for(EntityOfficerInfo entity:entityList) {
				EntityOfficerInfoDto dto= new EntityOfficerInfoDto();
				BeanUtils.copyProperties(dto, entity);
				entityDto.add(dto);
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		return entityDto;
	}
	
	public EntityOfficerInfoDto getOfficerFromLocal(EntityOfficerInfoDto dto) {
		EntityOfficerInfo entity=  entityOfficerInfoDao.getOfficer(dto);
		EntityOfficerInfoDto entityDto= new EntityOfficerInfoDto();
		try {
			BeanUtils.copyProperties(entityDto, entity);
		} catch (Exception e) {
			
			e.printStackTrace();
		} 
		return entityDto;
	}
}