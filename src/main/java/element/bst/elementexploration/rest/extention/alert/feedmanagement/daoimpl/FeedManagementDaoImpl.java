package element.bst.elementexploration.rest.extention.alert.feedmanagement.daoimpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;


@Repository("feedManagementDao")
@Transactional("transactionManager")
public class FeedManagementDaoImpl extends GenericDaoImpl<FeedManagement, Long> implements 
FeedManagementDao{

	
	public FeedManagementDaoImpl() {
		super(FeedManagement.class);
	}
	
	
	@Override
	public FeedManagement getFeedManagementByFeedName(String feedName) {
		FeedManagement feedManagement = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from FeedManagement fm where fm.feedName = :feedName");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("feedName", feedName);
			feedManagement = (FeedManagement) query.getSingleResult();
		} catch (Exception e) {
			return feedManagement;
		}
		return feedManagement;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FeedGroups> getAllFeedGroups() {
		List<FeedGroups> feedGroupList = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from FeedGroups ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			feedGroupList =  (List<FeedGroups>) query.getResultList();
		} catch (Exception e) {
			return feedGroupList;
		}
		return feedGroupList;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FeedGroups> getgetGroupLevelByFeedId(Long feedManagementID) {
		List<FeedGroups> feedManagement = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from FeedGroups fm where fm.feedId.feed_management_id = :feedId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("feedId", feedManagementID);
			feedManagement = (List<FeedGroups>) query.getResultList();
		} catch (Exception e) {
			return feedManagement;
		}
		return feedManagement;
	}


	@Override
	public FeedManagement createNewFeedManagement(FeedManagementDto feedManagmentDto) {
		FeedManagement created=null;
		try {
		NativeQuery<?> query= this.getCurrentSession().createNativeQuery("insert into bst_am_feed_management (color,feed_name,source,type)values(?2,?3,?4,?5);");
	//	query.setParameter(1, feedManagmentDto.getFeed_management_id());
		query.setParameter(2, feedManagmentDto.getColor());
		query.setParameter(3,feedManagmentDto.getFeedName());
		query.setParameter(4,feedManagmentDto.getSource());
		query.setParameter(5,feedManagmentDto.getType());
		query.executeUpdate();
		StringBuilder hql = new StringBuilder();
		hql.append("select fm from FeedManagement fm where fm.feed_management_id = :feedId");
		Query<?> query1 = this.getCurrentSession().createQuery(hql.toString());
		query1.setParameter("feedId", feedManagmentDto.getFeed_management_id());
		created=(FeedManagement) query1.getSingleResult();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return created;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FeedManagement> getFeedsList(Boolean isAllRequired, Integer pageNumber, Integer recordsPerPage,
			String orderIn, String orderBy) {
		
		List<FeedManagement> feedList = new ArrayList<FeedManagement>();
		try{
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from FeedManagement f");
			
			queryBuilder.append(" order by ").append("f.").append(resolveFeedColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			feedList = (List<FeedManagement>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return feedList;
	}
	private String resolveFeedColumnName(String groupColumn) {
		if (groupColumn == null)
			return "feedName";

		switch (groupColumn.toLowerCase()) {
		case "name":
			return "feedName";
		default:
			return "feedName";
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getFeedsCount() {
		List<FeedManagement> feedList = new ArrayList<FeedManagement>();
		try{
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from FeedManagement f");
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			feedList = (List<FeedManagement>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) feedList.size();
	}


	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, Integer> getCountForFeeds() {
		Map<Long, Integer> feedCount= new HashMap<Long, Integer>();
		try{
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select A.feed.feed_management_id,count(*) from Alerts A group by A.feed");
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			List<Object[]> feedCountlist = (List<Object[]>) query.getResultList();
			if (null != feedCountlist) {
				for (Object[] result : feedCountlist) {
					if (result[0] != null && result[1] != null)
						feedCount.put(Long.valueOf(result[0].toString()), Integer.valueOf(result[1].toString()));
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return feedCount;
	}


	@Override
	public FeedManagement getFeedByTypeAndSource(Long typeId, Long sourceId) {

		FeedManagement feedManagement = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from FeedManagement fm where fm.type = :typeId and fm.source=:sourceId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("typeId", typeId.intValue());
			query.setParameter("sourceId", sourceId.intValue());
			feedManagement = (FeedManagement) query.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			//e.printStackTrace();
			return feedManagement;
		}		
		return feedManagement;

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FeedGroups> getFeedGroupsByGroupId(List<Long> groupIds) {
		List<FeedGroups> feedGroupList = null;
		try {
			String hql = "from FeedGroups f where f.groupId.id in (:groupIds)";
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("groupIds", groupIds);
			feedGroupList = (List<FeedGroups>) query.getResultList();
		} catch (Exception e) {
			return feedGroupList;
		}
		return feedGroupList;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FeedManagement> getFeedsByTypeIds(List<Long> classificationIds) {
		List<Integer> typeIds = classificationIds.stream().map(p -> p.intValue()).collect(Collectors.toList());
		List<FeedManagement> feedGroupList = null;
		try {
			String hql = "from FeedManagement f where f.type in (:classificationIds)";
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("classificationIds", typeIds);
			feedGroupList = (List<FeedManagement>) query.getResultList();
		} catch (Exception e) {
			return feedGroupList;
		}
		return feedGroupList;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FeedManagement> getFeedsByFeedIds(List<Long> feedIds) {
		
		List<FeedManagement> feedList = null;
		try {
			String hql = "from FeedManagement f where f.feed_management_id in (:feedIds)";
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("feedIds", feedIds);
			feedList = (List<FeedManagement>) query.getResultList();
		} catch (Exception e) {
			return feedList;
		}
		return feedList;
	}

}
