package element.bst.elementexploration.rest.extention.advancesearch.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.advancesearch.dao.EntityOfficerInfoDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityOfficerInfo;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("entityOfficerInfoDao")
public class EntityOfficerInfoDaoImpl extends GenericDaoImpl<EntityOfficerInfo, Long> implements EntityOfficerInfoDao {

	public EntityOfficerInfoDaoImpl() {
		super(EntityOfficerInfo.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EntityOfficerInfo> getOfficerList(String entityId) {
		List<EntityOfficerInfo> list = new ArrayList<EntityOfficerInfo>();
		
			EntityOfficerInfo entityOfficerInfo = null;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select e from EntityOfficerInfo e where e.entityId=:entityId  ");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("entityId",entityId);
			try {
			list = (List<EntityOfficerInfo>) query.getResultList();
			}
			catch(NoResultException e) {
				entityOfficerInfo= new EntityOfficerInfo();
			}
			
			//list.add(entityOfficerInfo);
		
		return list;
	}

	@Override
	public EntityOfficerInfo getOfficer(EntityOfficerInfoDto entityOfficers) {
		
		EntityOfficerInfo entityOfficerInfo = new EntityOfficerInfo();
		EntityOfficerInfoDto entityOfficerInfoDto = new EntityOfficerInfoDto();
		try {
			String officerName = entityOfficers.getOfficerName();
			String entityId = entityOfficers.getEntityId();
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select e from EntityOfficerInfo e where e.officerName=:officerName");
			if(entityId!=null){
				queryBuilder.append(" and e.entityId=:entityId");
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("officerName", officerName);
			query.setParameter("entityId", entityId);
			entityOfficerInfo = (EntityOfficerInfo) query.getSingleResult();
		} catch (Exception e) {
			
		} 
		return entityOfficerInfo;
	}
	
}