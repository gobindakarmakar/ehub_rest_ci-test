package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * @author suresh
 *
 */
@ApiModel("Document answered values")
public class ValueDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<LinkedHashMap<String, BigInteger>> rowWiseAnsweredData;
	public List<LinkedHashMap<String, BigInteger>> getRowWiseAnsweredData() {
		return rowWiseAnsweredData;
	}
	public void setRowWiseAnsweredData(List<LinkedHashMap<String, BigInteger>> rowWiseAnsweredData) {
		this.rowWiseAnsweredData = rowWiseAnsweredData;
	}

	

}
