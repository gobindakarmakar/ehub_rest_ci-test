package element.bst.elementexploration.rest.extention.graph.service;

public interface GraphService {

	String getAllDataFineDataFromBigDataServer(String searchQuery, Integer depth) throws Exception;

	String getCyberSecurity(String jsonString) throws Exception;
}
