package element.bst.elementexploration.rest.casemanagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Amit Patel
 *
 */
@ApiModel("case")
@Entity
@Table(name = "bst_case")
@Access(AccessType.FIELD)
public class Case implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3382427138566374820L;

	@ApiModelProperty(value = "The ID of the case")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_case_id")
	private Long caseId;

	@ApiModelProperty(value = "name of the case", required = true)
	@Column(name = "name", length = 75)
	@NotEmpty(message = "Case name can not be blank.")
	@Size(max = 75, message = "Case name should not be more than 75 characters.")
	private String name;

	@ApiModelProperty(value = "description of the case")
	@Column(name = "description", length = 500)
	private String description;

	@ApiModelProperty(value = "remarks on the case")
	@Column(name = "remarks", length = 500)
	private String remarks;

	@ApiModelProperty(value = "priority of the case")
	@Column(name = "priority")
	@Enumerated(EnumType.ORDINAL)
	private PriorityEnums priority;

	@ApiModelProperty(value = "current status of the case")
	@Column(name = "current_status")
	private Integer currentStatus;

	@ApiModelProperty(value = "health of the case")
	@Column(name = "health")
	private Double health;

	@ApiModelProperty(value = "direct risk of the case")
	@Column(name = "direct_risk")
	private Double directRisk;

	@ApiModelProperty(value = "indirect risk of the case")
	@Column(name = "indirect_risk")
	private Double indirectRisk;

	@ApiModelProperty(value = "transactional risk of the case")
	@Column(name = "transactional_risk")
	private Double transactionalRisk;

	@ApiModelProperty(value = "creation date of the case")
	@Column(name = "created_on")
	private Date createdOn;

	@ApiModelProperty(value = "creator of the case")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	private Users createdBy;

	/// Added by Paul
	/*
	 * @ApiModelProperty(value = "creator of the case")
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "created_by") private Users createdByUser;
	 */
	/////

	@ApiModelProperty(value = "modification date of the case")
	@Column(name = "modified_on")
	private Date modifiedOn;

	@ApiModelProperty(value = "modifier of the case")
	@Column(name = "modified_by")
	private Long modifiedBy;

	@ApiModelProperty(value = "case id from server")
	@Column(name = "case_id_from_server")
	private String caseIdFromServer;

	@ApiModelProperty(value = "type of the case")
	@Column(name = "type")
	private String type;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "caseSeed", targetEntity = CaseDocDetails.class)
	private List<CaseDocDetails> docDetailsList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "caseSeed", fetch = FetchType.LAZY, targetEntity = CaseAnalystMapping.class)
	private List<CaseAnalystMapping> caseAnalystMappingsList;

	@Column(name = "json_file")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] caseJsonFile;

	@Column(name = "json_questionnaire")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] caseJsonQuestionnaire;

	@Column(name = "questionnaire_doc_id")
	private Long questionnaireDocId;

	@Column
	private String entityId;

	@Column
	private String decisionScoringEntityId;

	@Column
	private String industry;

	@ApiModelProperty(value = "sub type of the case")
	@Column(name = "sub_type")
	private String subType;

	@ApiModelProperty(value = "case work flow id")
	@Column(name = "workflow_id")
	private String workflowId;

	@ApiModelProperty(value = "case work flow name")
	@Column(name = "workflow_name")
	private String workflowName;

	@ApiModelProperty(value = "lead generation")
	@Column(name = "lead_generation")
	private Boolean isLeadGeneration;

	@ApiModelProperty(value = "case multisource id")
	@Column(name = "multisource_id")
	private String multiSourceId;

	/**
	 * 
	 */
	public Case() {
		super();
	}

	public Case(Long caseId, String name, String description, String remarks, PriorityEnums priority,
			Integer currentStatus, Double health, Double directRisk, Double indirectRisk, Double transactionalRisk,
			Date createdOn, Users createdBy, Date modifiedOn, Long modifiedBy, String caseIdFromServer, String type,
			List<CaseDocDetails> docDetailsList, List<CaseAnalystMapping> caseAnalystMappingsList, byte[] caseJsonFile,
			byte[] caseJsonQuestionnaire) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.description = description;
		this.remarks = remarks;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.health = health;
		this.directRisk = directRisk;
		this.indirectRisk = indirectRisk;
		this.transactionalRisk = transactionalRisk;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.caseIdFromServer = caseIdFromServer;
		this.type = type;
		this.docDetailsList = docDetailsList;
		this.caseAnalystMappingsList = caseAnalystMappingsList;
		this.caseJsonFile = caseJsonFile;
		this.caseJsonQuestionnaire = caseJsonQuestionnaire;
	}

	public Case(Long caseId, String name, String description, String remarks, PriorityEnums priority,
			Integer currentStatus, Double health, Double directRisk, Double indirectRisk, Double transactionalRisk,
			Date createdOn, Users createdBy, Date modifiedOn, Long modifiedBy, String caseIdFromServer, String type,
			List<CaseDocDetails> docDetailsList, List<CaseAnalystMapping> caseAnalystMappingsList, byte[] caseJsonFile,
			byte[] caseJsonQuestionnaire, Long questionnaireDocId, String entityId, String decisionScoringEntityId,
			String industry) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.description = description;
		this.remarks = remarks;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.health = health;
		this.directRisk = directRisk;
		this.indirectRisk = indirectRisk;
		this.transactionalRisk = transactionalRisk;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.caseIdFromServer = caseIdFromServer;
		this.type = type;
		this.docDetailsList = docDetailsList;
		this.caseAnalystMappingsList = caseAnalystMappingsList;
		this.caseJsonFile = caseJsonFile;
		this.caseJsonQuestionnaire = caseJsonQuestionnaire;
		this.questionnaireDocId = questionnaireDocId;
		this.entityId = entityId;
		this.decisionScoringEntityId = decisionScoringEntityId;
		this.industry = industry;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PriorityEnums getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnums priority) {
		this.priority = priority;
	}

	public Integer getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Integer currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Double getHealth() {
		return health;
	}

	public void setHealth(Double health) {
		this.health = health;
	}

	public Double getDirectRisk() {
		return directRisk;
	}

	public void setDirectRisk(Double directRisk) {
		this.directRisk = directRisk;
	}

	public Double getIndirectRisk() {
		return indirectRisk;
	}

	public void setIndirectRisk(Double indirectRisk) {
		this.indirectRisk = indirectRisk;
	}

	public Double getTransactionalRisk() {
		return transactionalRisk;
	}

	public void setTransactionalRisk(Double transactionalRisk) {
		this.transactionalRisk = transactionalRisk;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Users getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Users createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getCaseIdFromServer() {
		return caseIdFromServer;
	}

	public void setCaseIdFromServer(String caseIdFromServer) {
		this.caseIdFromServer = caseIdFromServer;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<CaseDocDetails> getDocDetailsList() {
		return docDetailsList;
	}

	public void setDocDetailsList(List<CaseDocDetails> docDetailsList) {
		this.docDetailsList = docDetailsList;
	}

	public List<CaseAnalystMapping> getCaseAnalystMappingsList() {
		return caseAnalystMappingsList;
	}

	public void setCaseAnalystMappingsList(List<CaseAnalystMapping> caseAnalystMappingsList) {
		this.caseAnalystMappingsList = caseAnalystMappingsList;
	}

	/**
	 * @return the caseJsonFile
	 */
	public byte[] getCaseJsonFile() {
		return caseJsonFile;
	}

	/**
	 * @param caseJsonFile
	 *            the caseJsonFile to set
	 */
	public void setCaseJsonFile(byte[] caseJsonFile) {
		this.caseJsonFile = caseJsonFile;
	}

	public byte[] getCaseJsonQuestionnaire() {
		return caseJsonQuestionnaire;
	}

	public void setCaseJsonQuestionnaire(byte[] caseJsonQuestionnaire) {
		this.caseJsonQuestionnaire = caseJsonQuestionnaire;
	}

	public Long getQuestionnaireDocId() {
		return questionnaireDocId;
	}

	public void setQuestionnaireDocId(Long questionnaireDocId) {
		this.questionnaireDocId = questionnaireDocId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getDecisionScoringEntityId() {
		return decisionScoringEntityId;
	}

	public void setDecisionScoringEntityId(String decisionScoringEntityId) {
		this.decisionScoringEntityId = decisionScoringEntityId;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Boolean getIsLeadGeneration() {
		return isLeadGeneration;
	}

	public void setIsLeadGeneration(Boolean isLeadGeneration) {
		this.isLeadGeneration = isLeadGeneration;
	}

	public String getMultiSourceId() {
		return multiSourceId;
	}

	public void setMultiSourceId(String multiSourceId) {
		this.multiSourceId = multiSourceId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
