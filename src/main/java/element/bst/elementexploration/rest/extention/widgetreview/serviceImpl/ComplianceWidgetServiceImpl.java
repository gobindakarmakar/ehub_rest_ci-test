package element.bst.elementexploration.rest.extention.widgetreview.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.widgetreview.dao.ComplianceWidgetDao;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.dto.ComplianceWidgetDto;
import element.bst.elementexploration.rest.extention.widgetreview.service.ComplianceWidgetService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author rambabu
 *
 */
@Service("complianceWidgetService")
public class ComplianceWidgetServiceImpl extends GenericServiceImpl<ComplianceWidget, Long>
		implements ComplianceWidgetService {

	@Autowired
	ComplianceWidgetDao complianceWidgetDao;

	public ComplianceWidgetServiceImpl(GenericDao<ComplianceWidget, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public List<ComplianceWidgetDto> getAllComplianceWidget() {
		List<ComplianceWidgetDto> complianceWidgetDtoList = new ArrayList<ComplianceWidgetDto>();
		List<ComplianceWidget> ComplianceWidgetList = complianceWidgetDao.findAll();
		for (ComplianceWidget complianceWidget : ComplianceWidgetList) {
			ComplianceWidgetDto complianceWidgetDto = new ComplianceWidgetDto();
			BeanUtils.copyProperties(complianceWidget, complianceWidgetDto);
			complianceWidgetDtoList.add(complianceWidgetDto);
		}
		return complianceWidgetDtoList;
	}

}
