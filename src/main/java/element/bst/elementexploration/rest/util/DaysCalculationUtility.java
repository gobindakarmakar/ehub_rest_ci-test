package element.bst.elementexploration.rest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;
@Component
public class DaysCalculationUtility 
{
	
	public long getDifferenceBetweenDates(String fromDate,String toDate) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date firstDate = sdf.parse(fromDate);
		Date secondDate =sdf.parse(toDate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
	    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		return diff;
	}
	
	
	public Date getPreviousDate(String date,Long days) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date convertDate = sdf.parse(date);
		Integer set = days.intValue();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(convertDate);
	    cal.add(Calendar.DATE, -set);
	    Date dateBefore30Days = cal.getTime();
	   // System.out.println(dateBefore30Days);
	    return dateBefore30Days;
	}

}
