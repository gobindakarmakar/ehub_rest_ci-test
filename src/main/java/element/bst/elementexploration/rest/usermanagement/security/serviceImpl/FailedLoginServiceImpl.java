package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.FailedLoginDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.FailedLogin;
import element.bst.elementexploration.rest.usermanagement.security.service.FailedLoginService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Amit Patel
 *
 */
@Service("failedLoginService")
public class FailedLoginServiceImpl extends GenericServiceImpl<FailedLogin, Long>implements FailedLoginService{

	/**
	 * Allowed Failed Login Attemtpts - Read from configuration file
	 */
	@Value("${allowedFailedLoginAttempts}")
	private Integer ALLOWED_FAILED_LOGIN_ATTEMPTS;
	
	/**
	 * Blocked Failed Login Wait Duration - Read from configuration file
	 */
	@Value("${blockedFailedLoginWaitDuration}")
	private Integer BLOCKED_FAILED_LOGIN_WAIT_DURATION;
	
	@Autowired
	public FailedLoginServiceImpl(GenericDao<FailedLogin, Long> genericDao) {
		super(genericDao);		
	}
	
	@Autowired
	private FailedLoginDao failedLoginDao;
	
	/**
	 * Check if Account is locked
	 * 
	 * @param username
	 * 
	 * @return result true: locked
	 */
	@Override
	@Transactional("transactionManager")
	public boolean isAccountLocked(String username) {		
		boolean result = false;
		FailedLogin failedLogin = failedLoginDao.getFailedLoginByUsername(username);
		
		if (failedLogin != null) {
			long lastFailedLoginTimestamp = failedLogin.getLastFailedLoginTime().getTime();
			long currentTimestamp = (new Date()).getTime();			
			if ( failedLogin.getFailedLoginAttempt() >= this.getAllowedFailedLoginAttempt() &&
					currentTimestamp < (lastFailedLoginTimestamp + getBlockedFailedLoginWaitDurationInMilliseconds())) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * Release Account locked
	 * 
	 * @param username
	 * @param ipAddress
	 * 
	 * @return result true: successful
	 */
	@Override
	@Transactional("transactionManager")
	public boolean releaseAccountLocked(String username, String ipAddress) {
		FailedLogin failedLogin = failedLoginDao.getFailedLoginByUsername(username);
		if (failedLogin != null) {
			failedLogin.setFailedLoginAttempt(0);
			failedLogin.setLastSuccessfulLoginIpAddress(ipAddress);
			failedLogin.setLastSuccessfulLoginTime(new Date());
			failedLoginDao.saveOrUpdate(failedLogin);
		}
		return true;
	}

	/**
	 * Process Failed Authentication
	 * 
	 * @param username
	 * @param screenName
	 * @param ipAddress
	 * 
	 */
	@Override
	@Transactional("transactionManager")
	public void processFailedAuthentication(String username, String screenName, String ipAddress) {
		ElementLogger.log(ElementLoggerLevel.INFO, "Process Failed Authentication. username: " + username + "; ipAddress: " + ipAddress, this.getClass());

		if (this.getCurrentLoginAttempt(username, screenName, ipAddress) >= getAllowedFailedLoginAttempt()) {
			ElementLogger.log(ElementLoggerLevel.WARNING, "Exceeded failed login attempt. username: " + username + "; ipAddress: " + ipAddress, this.getClass());
			//processExceededFailedLoginAttempts(username, password, ipAddress, unknownUser); // code to send an email
		}		
	}

	@Override
	@Transactional("transactionManager")
	public FailedLogin getFailedLoginByUsername(String username) {
		return failedLoginDao.getFailedLoginByUsername(username);
	}
	
	/**
	 * Get Allowed Failed Login Attempts
	 * @return count for allowed Failed Login Attempt
	 */
	private int getAllowedFailedLoginAttempt() {
		int allowedFailedLoginAttempts = 0;
		allowedFailedLoginAttempts = ALLOWED_FAILED_LOGIN_ATTEMPTS;
		return allowedFailedLoginAttempts;
	}
	
	/**
	 * Get Blocked Failed Login Wait Duration
	 * @return wait duration
	 */
	private int getBlockedFailedLoginWaitDuration() {
		int blockedFailedLoginWaitDuration = 0;
		blockedFailedLoginWaitDuration = BLOCKED_FAILED_LOGIN_WAIT_DURATION;
		return blockedFailedLoginWaitDuration;
	}
	
	/**
	 * Get Blocked Failed Login Wait Duration
	 * @return wait duration in milliseconds
	 */
	private long getBlockedFailedLoginWaitDurationInMilliseconds() {
		return getBlockedFailedLoginWaitDuration() * 60 * 60 * (long)1000; //Convert Hours to Milliseconds
	}
	
	/**
	 * Get current login attempt
	 * 
	 * @param username
	 * @return
	 */
	@Transactional("transactionManager")
	@Override
	public int getCurrentLoginAttempt(String username, String screenName, String ipAddress) {
		int currentLoginAttempt = 0;
		FailedLogin failedLogin = failedLoginDao.getFailedLoginByUsername(username);
		if (failedLogin != null) {
			if (failedLogin.getFailedLoginAttempt() != null) {
				currentLoginAttempt = failedLogin.getFailedLoginAttempt().intValue() + 1;				
				failedLogin.setLastFailedLoginIpAddress(ipAddress);
				failedLogin.setLastFailedLoginTime(new Date());
				failedLogin.setFailedLoginAttempt(currentLoginAttempt);
				failedLoginDao.saveOrUpdate(failedLogin);
			}
		}
		else {
			currentLoginAttempt = 1;
			FailedLogin newFailedLogin = new FailedLogin();
			newFailedLogin.setUsername(username);
			newFailedLogin.setScreenName(screenName);
			newFailedLogin.setLastFailedLoginIpAddress(ipAddress);
			newFailedLogin.setLastFailedLoginTime(new Date());
			newFailedLogin.setFailedLoginAttempt(currentLoginAttempt);
			failedLoginDao.saveOrUpdate(newFailedLogin);
		}
		return currentLoginAttempt;
	}

}
