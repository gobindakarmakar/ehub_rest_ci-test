package element.bst.elementexploration.rest.settings.listManagement.daoImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.listManagement.dao.ColorDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.Color;

@Repository("colorDao")
@Transactional("transactionManager")
public class ColorDaoImpl extends GenericDaoImpl<Color, Long> implements 
ColorDao{

	public ColorDaoImpl() {
		super(Color.class);
	}
	
}
