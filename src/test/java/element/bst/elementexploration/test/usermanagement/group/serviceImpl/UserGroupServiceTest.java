package element.bst.elementexploration.test.usermanagement.group.serviceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.usermanagement.group.dao.UserGroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;
import element.bst.elementexploration.rest.usermanagement.group.serviceImpl.UserGroupServiceImpl;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UserGroupServiceTest {
	
	@Mock
	UserGroupDao userGroupDaoMock;
	
	@InjectMocks
	UserGroupServiceImpl userGroupService;
	
	public UserGroup userGroup;
	
	@Before
    public void setupMock() {
		userGroup = new UserGroup(1L,2L);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getUserGroupTest(){
		Mockito.when(userGroupDaoMock.getUserGroup(Mockito.anyLong(), Mockito.anyLong())).thenReturn(userGroup);
		UserGroup existedUserGroup =  userGroupService.getUserGroup(1L, 1L);
		Assert.assertNotNull(existedUserGroup.getGroupId());
	}

}
