package element.bst.elementexploration.rest.extention.textsentiment.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SentimentRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("data")
	private SentimentDto sentimentDto;

	public SentimentDto getSentimentDto() {
		return sentimentDto;
	}

	public void setSentimentDto(SentimentDto sentimentDto) {
		this.sentimentDto = sentimentDto;
	}

}
