package element.bst.elementexploration.rest.extention.significantnews.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantWatchList;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface SignificantWatchListDao extends GenericDao<SignificantWatchList, Long> {
	public SignificantWatchList getSignificantWatchList(String mainEntityId,String identifier,String entityName,String value,String name);

	public List<SignificantWatchList> fetchWtachListByEntityId(String mainEntityId);
	
}
