package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Paul Jalagari
 *
 */
public class MyAlertsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StatusCountDto> statusCountList;

	private Integer totalCount;

	public List<StatusCountDto> getStatusCountList() {
		return statusCountList;
	}

	public void setStatusCountList(List<StatusCountDto> statusCountList) {
		this.statusCountList = statusCountList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
