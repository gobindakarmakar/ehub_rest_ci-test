package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.extention.transactionintelligence.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author suresh
 *
 */
@ApiIgnore
@Api(tags = { "Country API" },description="Manages alerted transactions data of countries")
@RestController
@RequestMapping(value = "/api/country")
@EnableAsync
public class CountryController {

	@Autowired
	CountryService countryService;

	/**
	 * api for country file upload
	 * 
	 * @param multipartFile
	 * @param token
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 *//*
	@RequestMapping(value = "/uploadCountryFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveCountryData(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				countryService.saveCountryData(multipartFile);
				return new ResponseEntity<>(new ResponseMessage("Successfully uploaded file"), HttpStatus.OK);
			} else {
				throw new BadRequestException(ElementConstants.DOCUMENT_FORMAT);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed to read file,Please check the file format"),
					HttpStatus.OK);
		}

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/uploadCountryFileXl", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveCountryDataxl(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("xlsx")) {
				File file = new File(multipartFile.getOriginalFilename());
				FileOutputStream fos = new FileOutputStream(file); 
			    fos.write(multipartFile.getBytes());
			    fos.close(); 
				// here we are not sending csv data
				Boolean value = countryService.saveCountryDataxl(file);
				return new ResponseEntity<>(new ResponseMessage("Successfully uploaded file,started processing"),
						HttpStatus.OK);
			} else {
				throw new BadRequestException(ElementConstants.DOCUMENT_FORMAT);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed to read file,Please check the file format"),
					HttpStatus.OK);
		}

	}*/
	
	@ApiOperation("Saving the countries names,iso codes and indexes into database")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = Boolean.class, message = "Operation successful.")})
	@RequestMapping(value = "/auditCountryData", method = RequestMethod.GET)
	public ResponseEntity<?> auditCountryData(@RequestParam("token") String token) throws Exception {
				return new ResponseEntity<>(countryService.auditCountryData(),HttpStatus.OK);
	}
	
	
}
