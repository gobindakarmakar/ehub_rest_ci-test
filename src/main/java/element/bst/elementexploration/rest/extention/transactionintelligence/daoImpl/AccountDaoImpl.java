package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("accountDao")
public class AccountDaoImpl extends GenericDaoImpl<Account, Long> implements AccountDao {

	Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

	public AccountDaoImpl() {
		super(Account.class);
	}

	@Override
	public boolean checkExistenceOfAccount(List<TxDto> accountIds) {

		boolean flag = true;

		@SuppressWarnings("unused")
		Account account1 = null;

		for (TxDto txDto : accountIds) {
			try {
				String name = txDto.getPartyIdentifier();
				account1 = (Account) this.getCurrentSession()
						.createQuery("from Account where accountNumber = '" + name + "'").getSingleResult();

			} catch (NoResultException ne) {
				flag = false;
				String content1 = txDto.getEventId() + " " + txDto.getPartyIdentifier() + " " + txDto.getPartyRole()
						+ "/n";

				try (FileWriter fw = new FileWriter(
						"/home/ahextech/suresh/transaction_workspace/analytics/src/main/resources/accounts.txt", true);
						BufferedWriter bw = new BufferedWriter(fw);) {
					bw.write(content1);
					bw.flush();
				} catch (IOException e) {
					ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				}
			}

		} // for close
		return flag;

	}// method close

	@Override
	public List<Account> fetchAccounts(String partyIdentfier) {
		@SuppressWarnings("unchecked")
		List<Account> accountslist = this.getCurrentSession()
				.createQuery("from Account where primaryCustomerIdentifier = '" + partyIdentfier + "' ")
				.getResultList();
		return accountslist;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Account> fetchMulAccounts(List<String> customerNumbers) {

		Query query = this.getCurrentSession()
				.createQuery("from Account ac where ac.primaryCustomerIdentifier IN (:customerNumbers)");
		query.setParameter("customerNumbers", customerNumbers);
		List<Account> accountslist = query.getResultList();
		return accountslist;
	}

	@Override
	public Account fetchAccount(String accountNumber) {
		String name = accountNumber;
		Account account = null;
		try {
			 account = (Account) this.getCurrentSession()
					.createQuery("from Account where accountNumber = '" + name + "'").getSingleResult();
			
		} catch (NoResultException ne) {
		}
		return account;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Account> fetchLuxuryAccounts(List<String> luxuryItems) {
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select ac from Account ac,CustomerDetails cd where ac.customerDetails.id=cd.id and cd.industry IN (:luxuryItems)");
		Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
		query.setParameter("luxuryItems", luxuryItems);
		
		List<Account> accountslist = (List<Account>) query.getResultList();
		return accountslist;
	}

	@Override
	public Account fetchLuxuryAccount(String accountNumber) {
		List<String> luxuryItems = new ArrayList<String>();
		luxuryItems.add("Sale of works of art");
		luxuryItems.add("Sales of jewelery");
		luxuryItems.add("Car, boat and plane dealers and manufacturers");		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select ac from Account ac,CustomerDetails cd where ac.customerDetails.id=cd.id and cd.industry IN (:luxuryItems)");
		Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
		query.setParameter("luxuryItems", luxuryItems);
		
		Account luxuryAccount = (Account) query.getSingleResult();
		return luxuryAccount;
	}

}
