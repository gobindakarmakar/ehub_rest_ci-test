package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.List;

public class AssociatedAlertsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StatusCountDto> statusCountList;
	
	private String groupName;
	
	private Integer totalCount;

	public List<StatusCountDto> getStatusCountList() {
		return statusCountList;
	}

	public void setStatusCountList(List<StatusCountDto> statusCountList) {
		this.statusCountList = statusCountList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	
}
