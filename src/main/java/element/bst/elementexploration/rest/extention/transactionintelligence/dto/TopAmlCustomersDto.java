package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Top AML customers")
public class TopAmlCustomersDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Customer name")
	private String customerName;
	@ApiModelProperty(value="Customer number")
	private String customerNumber;
	@ApiModelProperty(value="count")
	private long count;
	
	

	public TopAmlCustomersDto(String customerName, long count) {
		super();
		this.customerName = customerName;
		this.count = count;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	
	

}
