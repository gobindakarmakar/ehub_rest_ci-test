package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.CountrySettingDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;
import element.bst.elementexploration.rest.usermanagement.security.service.CountrySettingService;
/**
 * 
 * @author Amit Patel
 *
 */
@Service("countrySettingService")
public class CountrySettingServiceImpl extends GenericServiceImpl<CountrySetting, Long>implements CountrySettingService {

	@Autowired
	public CountrySettingServiceImpl(GenericDao<CountrySetting, Long> genericDao) {
		super(genericDao);		
	}
	
	@Autowired
	CountrySettingDao countrySettingDao;
	
	@Override
	@Transactional("transactionManager")
	public CountrySetting getCountrySetting(String countryIso) {
		return countrySettingDao.getCountrySetting(countryIso);
	}

}
