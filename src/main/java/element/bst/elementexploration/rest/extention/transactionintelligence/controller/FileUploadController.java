package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.UpdateTransactionsDataDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.UploadFileResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AccountService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerDetailsService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerRelationService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.LuxuryShopsService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.TransactionUtility;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "File upload API" },description="Manages file uploads")
@RestController
@RequestMapping(value = "/api/upload")
@EnableAsync
public class FileUploadController extends BaseController {

	@Autowired
	AlertService alertService;

	@Autowired
	AccountService accountService;

	@Autowired
	CustomerRelationService customerRelationService;

	@Autowired
	CustomerDetailsService customerDetailsService;

	@Autowired
	TxDataService txDataService;

	@Autowired
	TransactionUtility serviceUtility;
	
	@Autowired
	LuxuryShopsService luxuryShopsService;
	
	
	
	
	@SuppressWarnings("unused")
	@ApiOperation("Saves the all transaction intelligence files into database.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UploadFileResponseDto.class,responseContainer = "List", message = "Operation successful.")})
	@RequestMapping(value = "/uploadAllFiles", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadAllFiles(@RequestBody(required=false) MultipartFile multipartFileCustomer,
			@RequestBody(required=false) MultipartFile multipartFileCustomerRelationShipDetailsFile,
			@RequestBody(required=false) MultipartFile multipartFileCustomerAddress,
			@RequestBody(required=false) MultipartFile multipartFileShareholders,
			@RequestBody(required=false) MultipartFile multipartFileAccount,
			@RequestBody(required=false) MultipartFile multipartFileTransaction,
			@RequestBody(required=false) MultipartFile multipartFileAlert,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException 
	{
		//System.out.println("name"+multipartFileCustomer.getOriginalFilename()+" "+multipartFileCustomer.getName());
		List<MultipartFile> multipartFileList = new ArrayList<MultipartFile>();
		if (multipartFileCustomer!=null)
			multipartFileList.add(multipartFileCustomer);
		if (multipartFileCustomerRelationShipDetailsFile!=null)
			multipartFileList.add(multipartFileCustomerRelationShipDetailsFile);
		if (multipartFileCustomerAddress!=null)
			multipartFileList.add(multipartFileCustomerAddress);
		if(multipartFileShareholders!=null)
			multipartFileList.add(multipartFileShareholders);
		if (multipartFileAccount!=null)
			multipartFileList.add(multipartFileAccount);
		if (multipartFileTransaction!=null)
			multipartFileList.add(multipartFileTransaction);
		if (multipartFileAlert!=null)
			multipartFileList.add(multipartFileAlert);
		String fileExtension  = "";
		for (MultipartFile multipartFile : multipartFileList) 
		{
			if(!multipartFile.isEmpty())
			{
			fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
			if(!fileExtension.equals("csv"))
			{
				boolean flag = false;
				/*JSONObject jsonObject = new JSONObject();
				String response = "Failed to read file,Please check the file format";
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);*/
				UploadFileResponseDto uploadFileResponseDto=new UploadFileResponseDto();
				uploadFileResponseDto.setMessage("Failed to read file,Please check the file format");
				uploadFileResponseDto.setFlag(flag);
				return new ResponseEntity<>(uploadFileResponseDto, HttpStatus.OK);
			}
			}
		}
		try 
		{
			Boolean value = txDataService.processAllFiles(multipartFileList,getCurrentUserId());
				boolean flag = true;
				/*JSONObject jsonObject = new JSONObject();
				String response = "File accepted for processing";
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);*/
				UploadFileResponseDto uploadFileResponseDto=new UploadFileResponseDto();
				uploadFileResponseDto.setMessage("File accepted for processing");
				uploadFileResponseDto.setFlag(flag);
				return new ResponseEntity<>(uploadFileResponseDto, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			/*JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);*/
			UploadFileResponseDto uploadFileResponseDto=new UploadFileResponseDto();
			uploadFileResponseDto.setMessage("Failed to read file,Please check the file format");
			uploadFileResponseDto.setFlag(flag);
			return new ResponseEntity<>(uploadFileResponseDto, HttpStatus.OK);
		}

	}
/*
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadAlertFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadFile(@RequestBody MultipartFile multipartFile, @RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				@SuppressWarnings("unused")
				List<AlertResponseSendDto> respDto = alertService.fetchAlerts(multipartFile, getCurrentUserId());
				boolean flag = true;
				JSONObject jsonObject = new JSONObject();
				String response = "succesfuly uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "/uploadAccountFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> fetchFileAccount(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				Boolean value = accountService.fetchAccountDetails(multipartFile);
				boolean flag = true;
				JSONObject jsonObject = new JSONObject();
				String response = "succesfuly uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadCustomerRelationShipDetailsFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> fetchCustomerRelationDetails(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				@SuppressWarnings("unused")
				Boolean value = customerRelationService.fetchCustomerRelationDetails(multipartFile);
				JSONObject jsonObject = new JSONObject();
				String response = "succesfuly uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", true);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadCustomerDetailsFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadCustomerDetailsFile(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				@SuppressWarnings("unused")
				Boolean value = customerDetailsService.uploadCustomerDetailsFile(multipartFile);
				JSONObject jsonObject = new JSONObject();
				String response = "successfully uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", true);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadCustomerAddressFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadCustomerAddressFile(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				@SuppressWarnings("unused")
				Boolean value = customerDetailsService.uploadCustomerAddressFile(multipartFile);
				JSONObject jsonObject = new JSONObject();
				String response = "successfully uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", true);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "/uploadFotFotpData", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadFotFotpData(@RequestBody MultipartFile multipartFile1,
			@RequestBody MultipartFile multipartFile2, @RequestParam("token") String token) {
		String fileExtension1 = FilenameUtils.getExtension(multipartFile1.getOriginalFilename());
		String fileExtension2 = FilenameUtils.getExtension(multipartFile2.getOriginalFilename());
		try {
			if ((fileExtension1.equals("csv")) && (fileExtension2.equals("csv"))) {
				Boolean value = serviceUtility.saveFotFotpData(multipartFile1, multipartFile2);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}

			boolean flag = true;
			JSONObject jsonObject = new JSONObject();
			String response = "File uploaded in process";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadTxData", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadTxData(@RequestBody MultipartFile multipartFile1,
			@RequestParam("token") String token) throws IOException {
		String fileExtension1 = FilenameUtils.getExtension(multipartFile1.getOriginalFilename());
		Long userIdTemp = getCurrentUserId();
		if (!fileExtension1.equals("csv")) {
			throw new BadRequestException(ElementConstants.DOCUMENT_FORMAT);
		}
		// checking the columns format of uploaded csv file
		BufferedReader br1 = new BufferedReader(new InputStreamReader(multipartFile1.getInputStream()));

		String line1 = null;
		String[] data = null;
		int i = 1;

		while ((line1 = br1.readLine()) != null) {
			if (i == 1) {
				if (line1.contains(","))
					data = line1.split(",");
				if (line1.contains(";"))
					data = line1.split(";");
			}
			i++;
		}

		if (data != null) {
			if ((!data[0].equals("ORIGINATOR_ACCOUNT_ID")) || (!data[1].equals("BENEFICIARY_ACCOUNT_ID"))
					|| (!data[2].equals("TRANS_PRODUCT_TYPE")) || (!data[3].equals("TRANSACTION_CHANNEL"))
					|| (!data[4].equals("AMOUNT")) || (!data[5].equals("CURRENCY"))
					|| (!data[6].equals("BUSINESS_DATE")) || (!data[7].equals("CUSTOM_TEXT"))) {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = "document inside columns mismatch";
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		}
		// saving
		try {
			txDataService.saveTxData(multipartFile1, userIdTemp);
			boolean flag = true;
			JSONObject jsonObject = new JSONObject();
			String response = "Successfully uploade file,processing file";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/addCorporateStructureAndIndustry", method = RequestMethod.GET)
	public ResponseEntity<?> addCorporateStructureAndIndustry(@RequestParam("token") String token) {
		try {
			customerDetailsService.addCorporateStructureAndIndustry();
			boolean flag = true;
			JSONObject jsonObject = new JSONObject();
			String responseMessg = "Successfully uploaded file";
			jsonObject.put("message", responseMessg);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/addBankAccount", method = RequestMethod.GET)
	public ResponseEntity<?> addBankAccount(@RequestParam("token") String token) {
		try {
			accountService.addBankAccount();
			boolean flag = true;
			JSONObject jsonObject = new JSONObject();
			String responseMessg = "Successfully uploaded file";
			jsonObject.put("message", responseMessg);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updateCustomerDetails", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateCustomerDetails(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				@SuppressWarnings("unused")
				Boolean updatedStatus = customerDetailsService.updateCustomer(multipartFile);
				JSONObject jsonObject = new JSONObject();
				String response = "successfully uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", true);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}

	}

	@GetMapping(value = "/updateTransactionsCustomer", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateTransactions(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.updateTransactions(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/alterDatabse", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> alterDatabse(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.alterDatabse(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/auditColumns", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> auditColumns(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.auditColumns(), HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/updatePassport", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updatePassport(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.updatePassport(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/updateAverageEstimation", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateAverageEstimation(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(customerDetailsService.updateEstimateAverageAmt(), HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadLuxuryShops", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadLuxuryShopsDetails(@RequestBody MultipartFile multipartFile,
			@RequestParam("token") String token)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {
		String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		try {
			if (fileExtension.equals("csv")) {
				// here we are not sending csv data
				@SuppressWarnings("unused")
				Boolean updatedStatus = luxuryShopsService.uploadLuxuryShopDetails(multipartFile);
				JSONObject jsonObject = new JSONObject();
				String response = "successfully uploaded file,processing file";
				jsonObject.put("message", response);
				jsonObject.put("flag", true);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			} else {
				boolean flag = false;
				JSONObject jsonObject = new JSONObject();
				String response = ElementConstants.DOCUMENT_FORMAT;
				jsonObject.put("message", response);
				jsonObject.put("flag", flag);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			boolean flag = false;
			JSONObject jsonObject = new JSONObject();
			String response = "Failed to read file,Please check the file format";
			jsonObject.put("message", response);
			jsonObject.put("flag", flag);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		}
	}
	@GetMapping(value = "/viewData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> viewData(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException 
	{
		
		return new ResponseEntity<>(txDataService.getMasterDataFromView(),HttpStatus.OK);
	}*/

	
}
