package element.bst.elementexploration.test.extention.sourcecredibility.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceJurisdictionDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceJurisdictionDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl.SourceJurisdictionServiceImpl;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SourceJurisdictionServiceTest {
	
	
	@Mock
	SourceJurisdictionDao sourceJurisdictionDao;
	
	@InjectMocks
	SourceJurisdictionServiceImpl sourceJurisdictionService;
	
	List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
	SourceJurisdiction sourceJurisdictionOne = null;
	SourceJurisdictionDto sourceJurisdictionDto = null;
	
	@Before
    public void setupMock() {
		sourceJurisdictionOne = new SourceJurisdiction("Russia");
		sourceJurisdictionDto = new SourceJurisdictionDto("Germany");
		SourceJurisdiction sourceJurisictionTwo = new SourceJurisdiction("France");
		jurisdictionList.add(sourceJurisdictionOne);
		jurisdictionList.add(sourceJurisictionTwo);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getSourceJurisdiction() {
		Mockito.when(sourceJurisdictionDao.findAll()).thenReturn(jurisdictionList);
		List<SourceJurisdictionDto> domainsCheck =  sourceJurisdictionService.getSourceJurisdiction();
		Assert.assertEquals(2,jurisdictionList.size());
	}
	
	@Test
	public void saveSourceDomain() throws Exception {
		Mockito.when(sourceJurisdictionDao.create(Mockito.anyObject())).thenReturn(sourceJurisdictionOne);
		Mockito.when(sourceJurisdictionDao.findAll()).thenReturn(jurisdictionList);
		List<SourceJurisdictionDto> jurisdictionsCheck =  sourceJurisdictionService.saveSourceJurisdiction(sourceJurisdictionDto);
		Assert.assertEquals(2,jurisdictionList.size());
	}
	

}
