'use strict';
activitiApp.service('HostPathService', hostPathService);

		hostPathService.$inject = [
			'$location',
			'Flash'
		];
		
		function hostPathService(
				$location,
				Flash
				) {
			var hostPath = '',dashboardname = '';
			
			this.setHostPath = function(data) {
				data = data.substring(data.indexOf($location.port()));
				data = data.replace($location.port(), '');
				if (data.indexOf($location.path()) > 0) {
					data = data.replace($location.path(), '');
					data = data.replace("#", '');
					data = data.split("?")[0];
				}
				hostPath = data;
			};
			this.getHostPath = function() {
				return hostPath;
			};
			
			this.setdashboardname = function(data, dashboarDropDownMenuItems){
				if(data.search('#') != -1)
					data =  data.split('#')[1];	
				if(data.search('/') != -1)
					data = data.split('/')[1];
				if(data.search(' ') == -1){
					if(data.match(/[A-Z]/)){
						var subStr = data.match(/[A-Z]/).toString();
						var index = data.indexOf(subStr);
						data = data.slice(0,index) + " " + data.slice(index);
					}
				}
				data = data.charAt(0).toUpperCase() + data.slice(1);
				angular.forEach(dashboarDropDownMenuItems,function(val,key){
					if(data == val.menu){
						dashboardname = val.menu;
						return;
					}
					  angular.forEach(val.content,function(val1,key1){
						  if(data == val1){
							  dashboardname = val.menu;
						  }
					  });
				   });
			};
						
			this.getdashboardname = function(){
				 return dashboardname.charAt(0).toLowerCase() + dashboardname.slice(1);;
			};
			
			/*
		     * @purpose: Flash Success Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function successflashAlert(data){
				var message = '<div class="alert alert-success alert-dismissible ehub-alert-success">'+
							  '<span class="fa fa-info-circle fa-3x"></span>'+
							  '<p>' + data.title +'</p>'+
							  '<strong>' + data.message + '</strong>'+
							  '</div>';
				return message;
			}
			
			/*
		     * @purpose: Flash Error Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function dangerflashAlert(data){
				var message = '<div class="alert alert-danger alert-dismissible ehub-alert-error">'+ 
							  '<span class="fa fa-exclamation-triangle fa-3x"></span>'+
							  '<p>' + data.title +'</p>'+
							  '<strong>' + data.message + '</strong>'+
							  '</div>';
				return message;
			}
			
			/*
		     * @purpose: Flash Success Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			this.FlashSuccessMessage = function(title, message){
				Flash.create('success', successflashAlert({
        			title: title,
        			message: message
        		}), 5000,{class: 'main-flash-wrapper'}, true);
			}
			
			/*
		     * @purpose: Flash Error Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			this.FlashErrorMessage = function(title, message){
				Flash.create('danger', dangerflashAlert({
           			title: title,
        			message: message
        		}),5000,{class: 'main-flash-wrapper'}, true);
			}
			
			this.getParameterByName = function(name, url){
				if (!url) {
    	            url = window.location.href;
    	        }
    	        name = name.replace(/[\[\]]/g, "\\$&");
    	        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    	                results = regex.exec(url);
    	        if (!results)
    	            return null;
    	        if (!results[2])
    	            return '';
    	        return decodeURIComponent(results[2].replace(/\+/g, " "));
			}
			/*
		     * @purpose: calculate random risk values
		     * @created: 10 nov 2017
		     * @params: searchText(string)
		     * @return: no
		     * @author: swathi
		    */
			var  risk = 0, scaledNumArr = [];
			this.setRandomRisk = function(searchText){
				scaledNumArr = [];
				var unscaledIR = (searchText + 'indirect').toUpperCase(),
				unscaledDR = (searchText + 'direct').toUpperCase(),
				unscaledTR = (searchText + 'transaction').toUpperCase();
				
				var searchTextRiskArray = [unscaledIR, unscaledDR, unscaledTR];
				var scale = d3.scaleBand().domain(searchTextRiskArray).range([40,80]);
				
				for(var j=0, length = searchTextRiskArray.length; j<length; j++){
					risk = (scale(searchTextRiskArray[j])) + searchText.length;
					if(risk > 80){
						risk = risk - searchText.length;
					}
					scaledNumArr.push(risk);
				};
			};
			
			this.getRandomRisk = function(){
				return scaledNumArr;
			};
			
			/*
		     * @purpose: setting dashboard Menu Items
		     * @created: 27 mar 2018
		     * @params: items(object)
		     * @return: no
		     * @author: swathi
		    */
			var dropDownMenuItems = [];
			this.setdashboarDropDownMenuItems = function(items){
				dropDownMenuItems = items;
			}
			
			this.getdashboarDropDownMenuItems = function(){
				return dropDownMenuItems;
			}
			
			var dropDownDisableBydomains = [];
			this.setdashboardDisableBydomains = function(items){
				dropDownDisableBydomains = items;
			}
			
			this.getdashboardDisableBydomains = function(){
				return dropDownDisableBydomains;
			}
		};
		