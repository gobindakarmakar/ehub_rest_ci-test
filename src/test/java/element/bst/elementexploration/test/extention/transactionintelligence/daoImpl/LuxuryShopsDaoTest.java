package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.LuxuryShopsDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.LuxuryShops;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class LuxuryShopsDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	LuxuryShopsDaoImpl luxuryShopsDao;

	LuxuryShops luxuryShops;

	@Before
	public void setupMock() {
		luxuryShops = new LuxuryShops(1L, "Jewellery", "http://ww.jewellery.com", "jewellery");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getLuxuryShopTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(luxuryShops);
		LuxuryShops luxuryShopsObj = luxuryShopsDao.getLuxuryShop("jewellery", true);
		assertEquals("Jewellery", luxuryShopsObj.getShopName());
	}

	@Test
	public void getLuxuryShop_whenTypeIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(luxuryShops);
		LuxuryShops luxuryShopsObj = luxuryShopsDao.getLuxuryShop("jewellery", false);
		assertEquals("Jewellery", luxuryShopsObj.getShopName());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getLuxuryShop_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		luxuryShopsDao.getLuxuryShop("jewellery", true);
	}

	@Test
	public void getLuxuryShopNamesTest() {
		List<String> luxuryShopNames = new ArrayList<String>();
		luxuryShopNames.add("Tiffany & Co");
		luxuryShopNames.add("Cartier");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(luxuryShopNames);
		List<String> luxuryShopNameslist = luxuryShopsDao.getLuxuryShopNames();
		assertEquals(2, luxuryShopNameslist.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getLuxuryShopNames_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		luxuryShopsDao.getLuxuryShopNames();
	}

	@Test
	public void getLuxuryShopWebsitesTest() {
		List<String> luxuryShopWebsites = new ArrayList<String>();
		luxuryShopWebsites.add("http://www.bvlgari.com");
		luxuryShopWebsites.add("http://www.cartier.com");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(luxuryShopWebsites);
		List<String> luxuryShopWebsitesList = luxuryShopsDao.getLuxuryShopWebsites();
		assertEquals(2, luxuryShopWebsitesList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getLuxuryShopWebsites_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		luxuryShopsDao.getLuxuryShopWebsites();
	}

}
