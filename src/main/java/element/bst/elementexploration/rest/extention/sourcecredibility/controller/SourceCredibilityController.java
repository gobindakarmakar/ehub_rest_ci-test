package element.bst.elementexploration.rest.extention.sourcecredibility.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceFilterDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCategoryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "Source Management API" })
@RestController
@RequestMapping("/api/sourceCredibility")
public class SourceCredibilityController extends BaseController {

	@Autowired
	SourcesService sourcesService;
	
	@Autowired
	SourceCategoryService sourceCategoryService;

	@ApiOperation("Save General  Source")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveGeneralSource", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveGeneralSource(HttpServletRequest request, @RequestParam String token,
			@RequestBody SourcesDto sourcesDto) throws Exception {
		boolean value = sourcesService.saveSource(sourcesDto, getCurrentUserId());
		if (value)
			return new ResponseEntity<>(
					sourcesService.getSources(null, null, null,
							sourcesDto.getClassifications().get(0).getClassificationId(), null, null, null, null,false),
					HttpStatus.OK);
		return new ResponseEntity<>(new ResponseMessage("Failed to add source"), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation("Gets the sources")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Sources Successfully.") })
	@PostMapping(value = "/getSources", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getSources(@RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Boolean visible,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			@RequestBody(required = false) List<SourceFilterDto> sourceFilterDto,
			@RequestParam(required = false) Long classificationId,
			@RequestParam(required = false) Long subSlassificationId, @RequestParam("token") String token,
			@RequestParam(required = false) Boolean isAllSourcesRequired,
			HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {
		List<SourcesDto> list = new ArrayList<SourcesDto>();
		if(isAllSourcesRequired!=null && isAllSourcesRequired) {
			
			list = sourcesService.getSources(pageNumber, recordsPerPage, visible, classificationId,
					orderBy, orderIn, sourceFilterDto, subSlassificationId,true);
		}else {
			list = sourcesService.getSources(pageNumber, recordsPerPage, visible, classificationId,
					orderBy, orderIn, sourceFilterDto, subSlassificationId,false);
		}
		
		PaginationInformation information = new PaginationInformation();
		
		if(pageNumber != null && recordsPerPage != null ) {

			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = sourcesService.getSourcesCount(null, null, visible, classificationId, orderBy, orderIn,
					sourceFilterDto, subSlassificationId);
			information.setTotalResults(totalResults);
			information.setTitle(request.getRequestURI());
			information.setKind("list");
			information.setCount(list != null ? list.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");
		}
		
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setPaginationInformation(information);
		responseUtil.setResult(list);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);

	}

	@PostMapping(value = "/updateSource", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateTemplateQuestions(@RequestBody SourcesDto sourceDto,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		boolean value = sourcesService.updateSource(sourceDto, getCurrentUserId());
		if (value)
			return new ResponseEntity<>(value, HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("Failed to update source"), HttpStatus.OK);
	}
	
	@ApiOperation("Adds News from common crawler")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getInfoFromDomains", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getInfoFromDomains(@RequestParam("token") String token,@RequestParam String domain,@RequestParam(required = false) Integer count, HttpServletRequest request)
			throws Exception {
		return new ResponseEntity<>(sourcesService.getInfoFromDomains(domain,count), HttpStatus.OK);
	}
	
	@ApiOperation("Gets the source categories")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Sources Successfully.") })
	@GetMapping(value = "/getSourceCategories", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSourceCategories(@RequestParam("token") String token, HttpServletRequest request) {
		return new ResponseEntity<>(sourceCategoryService.getSourceCategories(), HttpStatus.OK);

	}

}
