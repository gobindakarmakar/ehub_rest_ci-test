package element.bst.elementexploration.rest.usermanagement.security.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;

/**
 * @author Paul Jalagari
 *
 */
public interface ElementsSecurityService extends GenericService<UsersToken, Long> {

	String validateEmailPassword(String emailAddress, String password, String ipAddress) throws Exception;

	Users getUserUserFromToken(String token);

	UsersToken getUserToken(Long userId);

	String saveToken(Users user);

	boolean validateTemporaryPassword(Long userId,String type, String password);

	boolean sendStatusChangeEmail(Long userId, String statusFrom, String statusTo, Long newUserId, Long caseId,
			String forward, String reassign, String text);

	boolean sendEmailForPasswordReset(UsersDto user, HttpServletRequest request) throws Exception;

	boolean validateTemporaryPassword(String userIDAndTempPassword,String type, HttpServletRequest request) throws Exception;

	Users getUserUsersFromToken(String token);

	String validateEmailPasswordNew(String emailAddress, String password, String ip, HttpServletRequest request,
			String loginSource, UsersDto usersDto);
	void logUserLogins(UsersDto userDto, HttpServletRequest request, Boolean isLoginSuccess);

	JSONObject getFirebaseAccountInfoByToken(String accesstoken);

	Boolean sendVerificationCodeSendgrid(String verificationCode, String string)throws IOException;

	Boolean sendVerificationCodeSMTP(String verificationCode, String email);
}
