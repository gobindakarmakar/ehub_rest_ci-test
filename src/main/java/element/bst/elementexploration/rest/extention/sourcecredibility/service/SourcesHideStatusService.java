package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesHideStatusDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourcesHideStatusService extends GenericService<SourcesHideStatus, Long> {

	//SourcesHideStatusDto getSourceHideStatus(Long sourceId, Long classificationId);

	List<SourcesHideStatusDto> getSourceHideStatusByClassification(Long classificationId);

}
