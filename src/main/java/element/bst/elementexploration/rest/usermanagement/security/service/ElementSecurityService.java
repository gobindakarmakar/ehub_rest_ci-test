package element.bst.elementexploration.rest.usermanagement.security.service;

import javax.servlet.http.HttpServletRequest;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserToken;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;

/**
 * 
 * @author Amit Patel
 *
 */
public interface ElementSecurityService extends GenericService<UserToken, Long> {

	String validateEmailPassword(String emailAddress, String password, String ipAddress) throws Exception;

	User getUserUserFromToken(String token);

	UserToken getUserToken(Long userId);

	String saveToken(User user);

	boolean validateTemporaryPassword(Long userId, String password);

	boolean sendStatusChangeEmail(Long userId, String statusFrom, String statusTo, Long newUserId, Long caseId,
			String forward, String reassign, String text);
	
	boolean sendEmailForPasswordReset(UserDto user, HttpServletRequest request) throws Exception;

	boolean validateTemporaryPassword(String userIDAndTempPassword, HttpServletRequest request) throws Exception;

	Users getUserUsersFromToken(String token);
}
