package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "PermissionsWithOutObjectsDto")
public class PermissionsWithOutObjectsDto  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Privilege ID")
	private Long permissionId;

	@ApiModelProperty(value = "Privilege Name")
	private String itemName;

	@ApiModelProperty(value = "Status of permission")
	private Boolean active;

	@ApiModelProperty(value = "Parent Permissione")
	private Long parentPermissionId;

	/*@ApiModelProperty(value = "date of creation")
	private Date createdOn;*/

	public PermissionsWithOutObjectsDto() {

	}

	public PermissionsWithOutObjectsDto(String itemName, Boolean active, Long parentPermissionId,/* Date createdOn,*/
			Long permissionId) {
		super();
		this.permissionId = permissionId;
		this.itemName = itemName;
		this.active = active;
		this.parentPermissionId = parentPermissionId;
		//this.createdOn = createdOn;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Boolean getActive() {
		return active;
	}

	/*public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}*/

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public Long getParentPermissionId() {
		return parentPermissionId;
	}

	public void setParentPermissionId(Long parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
