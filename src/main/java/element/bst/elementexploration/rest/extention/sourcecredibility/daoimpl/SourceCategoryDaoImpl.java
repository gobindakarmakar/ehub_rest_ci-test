package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceCategoryDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/*
 * @author Prateek Maurya
 */

@Repository("sourceCategoryDao")
@Transactional("transactionManager")
public class SourceCategoryDaoImpl extends GenericDaoImpl<SourceCategory,Long> 
	implements SourceCategoryDao{

	public SourceCategoryDaoImpl(){
		super(SourceCategory.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkCategoryExists(String dtoCategory) {
		try {
			StringBuilder hql  = new StringBuilder();
			hql.append("from SourceCategory s where s.categoryName =:category");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("category", dtoCategory);
			List<SourceCategory> sources =  (List<SourceCategory>) query.getResultList();
			if(!sources.isEmpty() && dtoCategory.equalsIgnoreCase(sources.get(0).getCategoryName())) {
				return true;
			}
		}catch(Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return false;
	}
	
}
