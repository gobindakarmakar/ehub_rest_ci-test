package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.AssigneeDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek
 *
 */
@ApiModel("Alert Model")
public class AlertsDto  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	
	@ApiModelProperty(value = "Alert Id", required = true)
	@NotEmpty(message = "alert id can not be blank.")
	private Long alertId;
	
	@ApiModelProperty(value = "Customer Id for Alert", required = false)
	private String customerId;
	
	@ApiModelProperty(value = "Watchlist for Alert", required = false)
	private String watchList;
	
	@ApiModelProperty(value = "Entity name for Alert", required = false)
	private String entityName;
	
	@ApiModelProperty(value = "Entity type for Alert", required = false)
	private String entiType;
	
	@ApiModelProperty(value = "Confidence Level for Alert", required = false)
	private Double confidenceLevel;
	
	@ApiModelProperty(value = "created date for Alert", required = false)
	private Date createdDate;
	
	@ApiModelProperty(value = "created date for Alert", required = false)
	private Date updatedDate;
	
	@ApiModelProperty(value = "feed for Alert", required = false)
	private List<FeedManagement> feed;
	
	@ApiModelProperty(value = "group level for Alert", required = false)
	private Integer groupLevel;
	
	@ApiModelProperty(value = "feed groups for Alert", required = false)
	private List<FeedGroups> feedGroups;
	
	@ApiModelProperty(value = "Assignee for Alert", required = false)
	private AssigneeDto asignee;
	
	@ApiModelProperty(value = "Status of Alert", required = false)
	private ListItemDto statuse;
	
	@ApiModelProperty(value = "Risk indicator for Alert", required = false)
	private String riskIndicators;
	
	@ApiModelProperty(value = "Time in Status for Alert", required = false)
	private String timeInStatus;
	
	@ApiModelProperty(value = "comments for Alert", required = false)
	private Integer commentsCount;
	//private List<AuditLog> audits;
	
	@ApiModelProperty(value = "Alert meta data", required = false)
	private String alertMetaData;
	
	@ApiModelProperty(value = "Periodic review for Alert", required = false)
	private Boolean periodicReview;
	
	@ApiModelProperty(value = "Time in status duration", required = false)
	private Integer timeStatusDuration;
	
	@ApiModelProperty(value = "Reviewer")
	private Long reviewer;
	
	@ApiModelProperty(value = "Review Status")
	private ListItemDto reviewStatusId;
	
	@ApiModelProperty(value = "Identified EntityId", required = false)
	private String identifiedEntityId;
	
	@ApiModelProperty(value = "Is entity Identified", required = false)
	private Boolean isEntityIdentified;
	
	@ApiModelProperty(value = "Classification", required = false)
	private List<String> classification;
	
	@ApiModelProperty(value = "Source", required = false)		
	private String source;	
	
	@ApiModelProperty(value = "Jurisdiction", required = false)		
	private ListItemDto jurisdictions;
	
	@ApiModelProperty(value = "userApproved", required = false)		
	private UserApprovedDto userApproved;
	
	
	
	public UserApprovedDto getUserApproved() {
		return userApproved;
	}
	public void setUserApproved(UserApprovedDto userApproved) {
		this.userApproved = userApproved;
	}
	
	public ListItemDto getJurisdictions() {
		return jurisdictions;
	}
	public void setJurisdictions(ListItemDto jurisdictions) {
		this.jurisdictions = jurisdictions;
	}
	
	public Long getAlertId() {
		return alertId;
	}
	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public Double getConfidenceLevel() {
		return confidenceLevel;
	}
	public void setConfidenceLevel(Double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}
	
	public List<FeedManagement> getFeed() {
		return feed;
	}
	public void setFeed(List<FeedManagement> feed) {
		this.feed = feed;
	}
	public AssigneeDto getAsignee() {
		return asignee;
	}
	public void setAsignee(AssigneeDto author) {
		this.asignee = author;
	}
	public List<FeedGroups> getFeedGroups() {
		return feedGroups;
	}
	public void setFeedGroups(List<FeedGroups> list) {
		this.feedGroups = list;
	}
	public String getAlertMetaData() {
		return alertMetaData;
	}
	public void setAlertMetaData(String alertMetaData) {
		this.alertMetaData = alertMetaData;
	}
	public String getWatchList() {
		return watchList;
	}
	public Integer getGroupLevel() {
		return groupLevel;
	}
	public String getRiskIndicators() {
		return riskIndicators;
	}
	public void setWatchList(String watchList) {
		this.watchList = watchList;
	}
	public void setGroupLevel(Integer groupLevel) {
		this.groupLevel = groupLevel;
	}
	public void setRiskIndicators(String riskIndicators) {
		this.riskIndicators = riskIndicators;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCommentsCount() {
		return commentsCount;
	}
	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}
	public ListItemDto getStatuse() {
		return statuse;
	}
	public void setStatuse(ListItemDto statuse) {
		this.statuse = statuse;
	}
	public Boolean getPeriodicReview() {
		return periodicReview;
	}
	public void setPeriodicReview(Boolean periodicReview) {
		this.periodicReview = periodicReview;
	}
	public String getTimeInStatus() {
		return timeInStatus;
	}
	public void setTimeInStatus(String timeInStatus) {
		this.timeInStatus = timeInStatus;
	}
	public Integer getTimeStatusDuration() {
		return timeStatusDuration;
	}
	public void setTimeStatusDuration(Integer timeStatusDuration) {
		this.timeStatusDuration = timeStatusDuration;
	}
	public Long getReviewer() {
		return reviewer;
	}
	public void setReviewer(Long reviewer) {
		this.reviewer = reviewer;
	}
	
	public ListItemDto getReviewStatusId() {
		return reviewStatusId;
	}
	public void setReviewStatusId(ListItemDto reviewStatusId) {
		this.reviewStatusId = reviewStatusId;
	}
	public String getIdentifiedEntityId() {
		return identifiedEntityId;
	}
	public void setIdentifiedEntityId(String identifiedEntityId) {
		this.identifiedEntityId = identifiedEntityId;
	}
	
	public List<String> getClassification() {
		return classification;
	}
	public void setClassification(List<String> classification) {
		this.classification = classification;
	}
	public Boolean getIsEntityIdentified() {
		return isEntityIdentified;
	}
	public void setIsEntityIdentified(Boolean isEntityIdentified) {
		this.isEntityIdentified = isEntityIdentified;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getEntiType() {
		return entiType;
	}
	public void setEntiType(String entiType) {
		this.entiType = entiType;
	}		
}
