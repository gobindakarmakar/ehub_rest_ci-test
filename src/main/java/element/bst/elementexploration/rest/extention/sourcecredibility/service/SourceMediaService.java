package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceMediaDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourceMediaService extends GenericService<SourceMedia, Long> {

	List<SourceMediaDto> saveSourceMedia(SourceMediaDto mediaDto) throws Exception;

	List<SourceMediaDto> getSourceMedia();

	SourceMedia fetchMedia(String string);

}
