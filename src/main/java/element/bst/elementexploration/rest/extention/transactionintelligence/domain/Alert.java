package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AbstractDomainObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;

/**
 * <h1>Alert</h1>
 * <p>
 * This class consists of alert details
 * </p>
 * 
 * @author suresh
 */
@Entity
@Table(name = "ALERT")
public class Alert extends AbstractDomainObject {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String focalNtityDisplayName;
	private String focalNtityDisplayId;
	private String jurisdiction;
	private String scenario;
	private Date alertBusinessDate;
	private String searchName;
	private int age;
	private String alertId;
	private String statusCd;
	private Long userId;
	private String comment;
	private String alertType;
	private AlertStatus alertStatus;
	private String transactionProductType;
	private Long transactionId;
	private String alertDescription;
	private String alertScenarioType;
	private Integer riskScore;
	private Double maximumAmount;
	private EntityType entityType;

	public Alert() {
	}

	public Alert(Long id, String focalNtityDisplayName, String focalNtityDisplayId, String jurisdiction,
			String scenario, Date alertBusinessDate, String searchName, int age, String alertId, String statusCd,
			Long userId, String comment, String alertType, AlertStatus alertStatus, String transactionProductType,
			Long transactionId, String alertDescription, String alertScenarioType, Integer riskScore,
			double maximumAmount) {
		super();
		this.id = id;
		this.focalNtityDisplayName = focalNtityDisplayName;
		this.focalNtityDisplayId = focalNtityDisplayId;
		this.jurisdiction = jurisdiction;
		this.scenario = scenario;
		this.alertBusinessDate = alertBusinessDate;
		this.searchName = searchName;
		this.age = age;
		this.alertId = alertId;
		this.statusCd = statusCd;
		this.userId = userId;
		this.comment = comment;
		this.alertType = alertType;
		this.alertStatus = alertStatus;
		this.transactionProductType = transactionProductType;
		this.transactionId = transactionId;
		this.alertDescription = alertDescription;
		this.alertScenarioType = alertScenarioType;
		this.riskScore = riskScore;
		this.maximumAmount = maximumAmount;
	}

	public Alert(Long id, String focalNtityDisplayName, String focalNtityDisplayId, String jurisdiction,
			String scenario, Date alertBusinessDate, String searchName, int age, String alertId, String statusCd,
			Long userId, String comment, String alertType, AlertStatus alertStatus, String transactionProductType,
			Long transactionId, String alertDescription, String alertScenarioType, Integer riskScore,
			Double maximumAmount, EntityType entityType) {
		super();
		this.id = id;
		this.focalNtityDisplayName = focalNtityDisplayName;
		this.focalNtityDisplayId = focalNtityDisplayId;
		this.jurisdiction = jurisdiction;
		this.scenario = scenario;
		this.alertBusinessDate = alertBusinessDate;
		this.searchName = searchName;
		this.age = age;
		this.alertId = alertId;
		this.statusCd = statusCd;
		this.userId = userId;
		this.comment = comment;
		this.alertType = alertType;
		this.alertStatus = alertStatus;
		this.transactionProductType = transactionProductType;
		this.transactionId = transactionId;
		this.alertDescription = alertDescription;
		this.alertScenarioType = alertScenarioType;
		this.riskScore = riskScore;
		this.maximumAmount = maximumAmount;
		this.entityType = entityType;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "alertId")
	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	@Column(name = "STATUS_CD")
	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	@Column(name = "FOCAL_NTITY_DISPLAY_NAME")
	public String getFocalNtityDisplayName() {
		return focalNtityDisplayName;
	}

	public void setFocalNtityDisplayName(String focalNtityDisplayName) {
		this.focalNtityDisplayName = focalNtityDisplayName;
	}

	@Column(name = "FOCAL_NTITY_DISPLAY_ID")
	public String getFocalNtityDisplayId() {
		return focalNtityDisplayId;
	}

	public void setFocalNtityDisplayId(String focalNtityDisplayId) {
		this.focalNtityDisplayId = focalNtityDisplayId;
	}

	@Column(name = "JURISDICTION")
	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	@Column(name = "SCENARIO")
	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	@Column(name = "ALERT_BUSINESS_DATE")
	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	@Column(name = "AGE")
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Column(name = "SEARCH_NAME")
	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	@Column(name = "USER_ID")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "COMMENT")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Column(name = "ALERTTYPE")
	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ALERT_STATUS")
	public AlertStatus getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(AlertStatus alertStatus) {
		this.alertStatus = alertStatus;
	}

	@Column(name = "TRANSACTION_PRODUCT_TYPE")
	public String getTransactionProductType() {
		return transactionProductType;
	}

	public void setTransactionProductType(String transactionProductType) {
		this.transactionProductType = transactionProductType;
	}

	@Column(name = "TRANSACTION_ID")
	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name = "ALERT_DESCRIPTION", columnDefinition = "TEXT")
	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	@Column(name = "ALERT_SCENARIO_TYPE")
	public String getAlertScenarioType() {
		return alertScenarioType;
	}

	public void setAlertScenarioType(String alertScenarioType) {
		this.alertScenarioType = alertScenarioType;
	}

	@Column(name = "RISK_SCORE")
	public Integer getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}

	@Column(name = "ALERT_MAXIMUM_AMOUNT")
	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ENTITY_TYPE")
	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + age;
		result = prime * result + ((alertBusinessDate == null) ? 0 : alertBusinessDate.hashCode());
		result = prime * result + ((alertDescription == null) ? 0 : alertDescription.hashCode());
		result = prime * result + ((alertId == null) ? 0 : alertId.hashCode());
		result = prime * result + ((alertScenarioType == null) ? 0 : alertScenarioType.hashCode());
		result = prime * result + ((alertStatus == null) ? 0 : alertStatus.hashCode());
		result = prime * result + ((alertType == null) ? 0 : alertType.hashCode());
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		result = prime * result + ((focalNtityDisplayId == null) ? 0 : focalNtityDisplayId.hashCode());
		result = prime * result + ((focalNtityDisplayName == null) ? 0 : focalNtityDisplayName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jurisdiction == null) ? 0 : jurisdiction.hashCode());
		result = prime * result + ((maximumAmount == null) ? 0 : maximumAmount.hashCode());
		result = prime * result + ((riskScore == null) ? 0 : riskScore.hashCode());
		result = prime * result + ((scenario == null) ? 0 : scenario.hashCode());
		result = prime * result + ((searchName == null) ? 0 : searchName.hashCode());
		result = prime * result + ((statusCd == null) ? 0 : statusCd.hashCode());
		result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
		result = prime * result + ((transactionProductType == null) ? 0 : transactionProductType.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alert other = (Alert) obj;
		if (age != other.age)
			return false;
		if (alertBusinessDate == null) {
			if (other.alertBusinessDate != null)
				return false;
		} else if (!alertBusinessDate.equals(other.alertBusinessDate))
			return false;
		if (alertDescription == null) {
			if (other.alertDescription != null)
				return false;
		} else if (!alertDescription.equals(other.alertDescription))
			return false;
		if (alertId == null) {
			if (other.alertId != null)
				return false;
		} else if (!alertId.equals(other.alertId))
			return false;
		if (alertScenarioType == null) {
			if (other.alertScenarioType != null)
				return false;
		} else if (!alertScenarioType.equals(other.alertScenarioType))
			return false;
		if (alertStatus != other.alertStatus)
			return false;
		if (alertType == null) {
			if (other.alertType != null)
				return false;
		} else if (!alertType.equals(other.alertType))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (entityType != other.entityType)
			return false;
		if (focalNtityDisplayId == null) {
			if (other.focalNtityDisplayId != null)
				return false;
		} else if (!focalNtityDisplayId.equals(other.focalNtityDisplayId))
			return false;
		if (focalNtityDisplayName == null) {
			if (other.focalNtityDisplayName != null)
				return false;
		} else if (!focalNtityDisplayName.equals(other.focalNtityDisplayName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jurisdiction == null) {
			if (other.jurisdiction != null)
				return false;
		} else if (!jurisdiction.equals(other.jurisdiction))
			return false;
		if (maximumAmount == null) {
			if (other.maximumAmount != null)
				return false;
		} else if (!maximumAmount.equals(other.maximumAmount))
			return false;
		if (riskScore == null) {
			if (other.riskScore != null)
				return false;
		} else if (!riskScore.equals(other.riskScore))
			return false;
		if (scenario == null) {
			if (other.scenario != null)
				return false;
		} else if (!scenario.equals(other.scenario))
			return false;
		if (searchName == null) {
			if (other.searchName != null)
				return false;
		} else if (!searchName.equals(other.searchName))
			return false;
		if (statusCd == null) {
			if (other.statusCd != null)
				return false;
		} else if (!statusCd.equals(other.statusCd))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		if (transactionProductType == null) {
			if (other.transactionProductType != null)
				return false;
		} else if (!transactionProductType.equals(other.transactionProductType))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Alert [id=" + id + ", focalNtityDisplayName=" + focalNtityDisplayName + ", focalNtityDisplayId="
				+ focalNtityDisplayId + ", jurisdiction=" + jurisdiction + ", scenario=" + scenario
				+ ", alertBusinessDate=" + alertBusinessDate + ", searchName=" + searchName + ", age=" + age
				+ ", alertId=" + alertId + ", statusCd=" + statusCd + ", userId=" + userId + ", comment=" + comment
				+ ", alertType=" + alertType + ", alertStatus=" + alertStatus + ", transactionProductType="
				+ transactionProductType + ", transactionId=" + transactionId + ", alertDescription=" + alertDescription
				+ ", alertScenarioType=" + alertScenarioType + ", riskScore=" + riskScore + ", maximumAmount="
				+ maximumAmount + ", entityType=" + entityType + "]";
	}

}
