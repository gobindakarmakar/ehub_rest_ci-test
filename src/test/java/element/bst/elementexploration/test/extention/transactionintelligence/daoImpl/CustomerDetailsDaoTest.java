package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.CustomerDetailsDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerDetailsDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<CustomerDetails> root;

	@InjectMocks
	CustomerDetailsDaoImpl customerDetailsDao;

	List<CustomerDetails> customerDetailsList = new ArrayList<CustomerDetails>();

	CustomerDetails customerDetails = new CustomerDetails();

	@Before
	public void setupMock() {
		customerDetails.setCustomerNumber("12345");
		customerDetails.setCreatedBy(1L);
		customerDetails.setCreatedOn(new Date());
		customerDetails.setDisplayName("Test");
		customerDetailsList.add(customerDetails);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchCustomerDetailsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(customerDetails);
		CustomerDetails customerDetailsObj = customerDetailsDao.fetchCustomerDetails("12345");
		assertEquals("Test", customerDetailsObj.getDisplayName());
	}

	@Test
	public void fetchAllCustomersTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(customerDetailsList);
		List<CustomerDetails> customerDetailsListNew = customerDetailsDao.fetchAllCustomers();
		assertEquals(1, customerDetailsListNew.size());
	}

	@Test
	public void fetchMulCustomerDetailsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(customerDetailsList);
		List<CustomerDetails> customerDetailsListNew = customerDetailsDao.fetchMulCustomerDetails("12345");
		assertEquals(1, customerDetailsListNew.size());
	}

	@Test
	public void getCustomerDetailsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(customerDetails);
		CustomerDetails customerDetailsObj = customerDetailsDao.getCustomerDetails("12345", "India");
		assertEquals("Test", customerDetailsObj.getDisplayName());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerDetails_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		CustomerDetails customerDetailsObj = customerDetailsDao.getCustomerDetails("12345", "India");
		assertEquals(null, customerDetailsObj);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerDetails_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		CustomerDetails customerDetailsObj = customerDetailsDao.getCustomerDetails("12345", "India");
		assertEquals(null, customerDetailsObj);
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Test public void findByCustomerNumberInListTest(){ List<String>
	 * customerIds = new ArrayList<String>(); customerIds.add("12345");
	 * Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	 * Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
	 * Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(
	 * criteriaQuery);
	 * Mockito.when(criteriaQuery.from(CustomerDetails.class)).thenReturn(root);
	 * Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
	 * Mockito.when(query.getResultList()).thenReturn(customerDetailsList);
	 * List<CustomerDetails> customerDetailsListNew =
	 * customerDetailsDao.findByCustomerNumberInList(customerIds);
	 * assertEquals(1,customerDetailsListNew.size()); }
	 */

	@Test
	public void findByCustomerIdInListTest() {
		Set<Long> customerIds = new HashSet<Long>();
		customerIds.add(12345L);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(customerDetailsList);
		List<CustomerDetails> customerDetailsListNew = customerDetailsDao.findByCustomerIdInList(customerIds);
		assertEquals(1, customerDetailsListNew.size());
	}

	@Test
	public void findByCustomerIdInList_whencustomerIdsSetIsEmptyTest() {
		Set<Long> customerIds = new HashSet<Long>();
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(customerDetailsList);
		List<CustomerDetails> customerDetailsListNew = customerDetailsDao.findByCustomerIdInList(customerIds);
		assertEquals(1, customerDetailsListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void findByCustomerIdInList_whenFailedToExecuteQueryExceptionThrowTest() {
		Set<Long> customerIds = new HashSet<Long>();
		customerIds.add(12345L);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		customerDetailsDao.findByCustomerIdInList(customerIds);
	}

	@Test
	public void findCorporateCustomersTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(customerDetailsList);
		List<CustomerDetails> customerDetailsListNew = customerDetailsDao.findCorporateCustomers();
		assertEquals(1, customerDetailsListNew.size());
	}

	@Test
	public void customerInfoTest() {
		CustomerDto customerDto = new CustomerDto("Test", new Date(), new Date(), "London", "UK", "", CustomerType.IND,
				50000D, "");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(customerDto);
		CustomerDto customerDtoObj = customerDetailsDao.customerInfo(1234L);
		assertEquals("Test", customerDtoObj.getCustomerName());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void customerInfo_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		CustomerDto customerDtoObj = customerDetailsDao.customerInfo(1234L);
		assertEquals("Test", customerDtoObj.getCustomerName());
	}

}
