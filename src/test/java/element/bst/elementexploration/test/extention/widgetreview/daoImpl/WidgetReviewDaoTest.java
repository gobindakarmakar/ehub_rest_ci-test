package element.bst.elementexploration.test.extention.widgetreview.daoImpl;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.widgetreview.daoImpl.WidgetReviewDaoImpl;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class WidgetReviewDaoTest {
	
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@Mock
	ListItem listItem;
	
	@InjectMocks
	WidgetReviewDaoImpl widgetReviewDaoMock;
	
	private Users user = new Users("analyst", "Analyst", "Analyst", new Date(), listItem);
	private List<WidgetReview> widgetReviewList = new ArrayList<WidgetReview>();

	WidgetReview widgetReview;
	ComplianceWidget widget = new ComplianceWidget(1L,"Company Information");

	
	@Before
	public void setupMock() {
		widgetReview = new WidgetReview(false, new Date(), user, "test", widget);
		widgetReview.setId(1L);
		widgetReviewList.add(widgetReview);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getWidgetReviewsByEntityIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(widgetReviewList);
		List<WidgetReview> widgetReviewListNew = widgetReviewDaoMock.getWidgetReviewsByEntityId("test");
		assertEquals(1, widgetReviewListNew.size());
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getWidgetReviewsByEntityId_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<WidgetReview> widgetReviewListNew = widgetReviewDaoMock.getWidgetReviewsByEntityId("test");
		assertEquals(0, widgetReviewListNew.size());
		
	}
	
	@Test
	public void getWidgetWithEntityIdAndWidgetIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(widgetReview);
		WidgetReview widgetReviewNew = widgetReviewDaoMock.getWidgetWithEntityIdAndWidgetId("test",1L);
		assertEquals("test", widgetReviewNew.getEntityId());
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getWidgetWithEntityIdAndWidgetId_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		WidgetReview widgetReviewNew = widgetReviewDaoMock.getWidgetWithEntityIdAndWidgetId("test",1L);
		assertEquals(null, widgetReviewNew);
		
	}
	
	

}
