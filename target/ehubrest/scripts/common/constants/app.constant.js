'use strict';

/*--  server 6060 ---*/
// activitiApp.constant('EHUB_API', 'http://159.65.192.32:6060/ehubrest/api/')
//  	   .constant('ACTIVITI_API', 'http://159.65.192.32:6060/ehubrest')
// 	   .constant('EHUB_FE_API', 'http://159.65.192.32:6060/element/')
// 	   .constant('ACTIVITI_FE_PATH', 'http://159.65.192.32:6060/elementbpm/#/')
// 	   .constant('KYC_QUESTIONNAIRE_PATH', 'http://159.65.192.32/element-questionnaire-builder/index.php/admin')
// 	   .constant('POLICY_ENFORCEMENT_PATH', 'http://159.65.192.32:7070/policyEnforcement')
// 	   .constant('DELEGATE_EXPRESSION_OBJ',{
// 		   MOVE_CASE:'${statusUpdateListener}',
// 		   CASE_ASSIGN:'${caseAssignService}',
// 		   CASE_REASSIGN:'${caseForwadingOrReAssignService}'
// 	   });


/*---  local  ---*/
	   /*activitiApp.constant('EHUB_API', 'http://localhost:8080/ehubrest/api/')
	   .constant('ACTIVITI_API', 'http://localhost:8080/ehubrest')
	   .constant('EHUB_FE_API', 'http://localhost:8080/ehubui/')
	   .constant('ACTIVITI_FE_PATH', 'http://localhost:8080/activiti-app/#/')
	   .constant('KYC_QUESTIONNAIRE_PATH', 'http://188.166.40.105/element-kyc-questioner/index.php/admin')
	   .constant('POLICY_ENFORCEMENT_PATH', 'http://188.166.40.105:7070/policyEnforcement')
	   .constant('DELEGATE_EXPRESSION_OBJ',{
		   MOVE_CASE:'${statusUpdateListener}',
		   CASE_ASSIGN:'${caseAssignService}',
		   CASE_REASSIGN:'${caseForwadingOrReAssignService}'
	   });*/
	   


		activitiApp
		       .constant('EHUB_API', EHUB_API+'/ehubrest/api/')
			   .constant('ACTIVITI_API', EHUB_API+'/ehubrest')
			   .constant('EHUB_FE_API', EHUB_FE_API+'/ehubui/')
			   .constant('ACTIVITI_FE_PATH', ACTIVITI_FE_PATH+'/elementbpm/#/')
			   .constant('KYC_QUESTIONNAIRE_PATH', KYC_QUESTIONNAIRE_PATH+'/element-questionnaire-builder/index.php/admin')
			   .constant('POLICY_ENFORCEMENT_PATH', POLICY_ENFORCEMENT_PATH+'/policyEnforcement')
			   .constant('DELEGATE_EXPRESSION_OBJ',{
				   MOVE_CASE:'${statusUpdateListener}',
				   CASE_ASSIGN:'${caseAssignService}',
				   CASE_REASSIGN:'${caseForwadingOrReAssignService}'
			   });
			  
		

		// activitiApp
	    //    .constant('EHUB_API', '')
		//    .constant('ACTIVITI_API', '')
		//    .constant('EHUB_FE_API', '')
		//    .constant('ACTIVITI_FE_PATH', '')
		//    .constant('KYC_QUESTIONNAIRE_PATH', '')
		//    .constant('POLICY_ENFORCEMENT_PATH', 'http://188.166.40.105:7070/policyEnforcement')
		//    .constant('DELEGATE_EXPRESSION_OBJ',{
		// 	   MOVE_CASE:'${statusUpdateListener}',
		// 	   CASE_ASSIGN:'${caseAssignService}',
		// 	   CASE_REASSIGN:'${caseForwadingOrReAssignService}'
		//    });
		
		
		// activitiApp
	    //    .constant('EHUB_API', 'http://localhost:8080/ehubrest/api/')
		//    .constant('ACTIVITI_API', 'http://localhost:8080/ehubrest')
		//    .constant('EHUB_FE_API', 'http://localhost:8080/ehubui/')
		//    .constant('ACTIVITI_FE_PATH', 'http://localhost:8080/activiti-app/#/')
		//    .constant('KYC_QUESTIONNAIRE_PATH', 'http://188.166.40.105/element-kyc-questioner/index.php/admin')
		//    .constant('POLICY_ENFORCEMENT_PATH', 'http://188.166.40.105:7070/policyEnforcement')
		//    .constant('DELEGATE_EXPRESSION_OBJ',{
		// 	   MOVE_CASE:'${statusUpdateListener}',
		// 	   CASE_ASSIGN:'${caseAssignService}',
		// 	   CASE_REASSIGN:'${caseForwadingOrReAssignService}'
		//    });

	   
	
