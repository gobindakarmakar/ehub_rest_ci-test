package element.bst.elementexploration.rest.extention.workflow.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.workflow.service.WorkflowGraphService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("workflowGraphService")
public class WorkflowGraphServiceImpl implements WorkflowGraphService {

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENTION_API;
	
	@Value("${workflow_extention_url_development}")
	private String WORKFLOW_EXTENSION_DEVELOPMENT_API;

	@Override
	public String listOfOrientDatabases(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/orient/databases/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String listOfVertexAndEdges(String profileId, String database, String classType) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/orient/dbclasses/" + encode(profileId) + "/" + encode(database) + "/"
				+ encode(classType);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteTables() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/contingencyTable";
		return deleteDataInServer(url);	
	}

	@Override
	public String getContingencyTable() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/contingencyTable";
		return getDataFromServer(url);
	}

	@Override
	public String updateContingencyTable(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/contingencyTable";

		String serverResponse[] = ServiceCallHelper.updateDataInServer(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_UPDATE_DATA_ON_SERVER_MSG);
		}
	}

	@Override
	public String deleteContingencyTableById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/contingencyTable/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getContingencyTableById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/contingencyTable/" + encode(workflowId);
		return getDataFromServer(url);
	}

	@Override
	public String insertContingencyTableById(String jsonString, String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/contingencyTable/" + encode(workflowId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String graphdataByCaseId(String caseId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/data/" + encode(caseId);
		return getDataFromServer(url);
	}

	@Override
	public String graphdataByCaseIdAndLimit(String caseId, String limit) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/data/" + encode(caseId) + "/" + limit;
		return getDataFromServer(url);
	}

	@Override
	public String findEntityByName(String name) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/name/" + encode(name);
		return getDataFromServer(url);
	}

	@Override
	public String addEntity(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/unified";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String findEntityByIdentifier(String identifier) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/" + encode(identifier);
		return getDataFromServer(url);
	}

	@Override
	public String fetcherList() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/fetcherlist";
		return getDataFromServer(url);		
	}

	@Override
	public String fetcherListById(String fetcherId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/fetcherlist/" + encode(fetcherId);
		return getDataFromServer(url);	
	}

	@Override
	public String search(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search";
        return postStringDataToServer(url, jsonString);
		
	}

	@Override
	public String listOfOrientDatabases() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search/caseids";
		return getDataFromServer(url);	
	}

	@Override
	public String listOfOrientDatabaseByQuery(String query) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search/caseids/" + encode(query);
		return getDataFromServer(url);	
	}

	@Override
	public String searchGraph(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search/graph";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String searchHints(String searchType, String query) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search/hints/" + encode(searchType) + "/" + encode(query);
		return getDataFromServer(url);	
	}

	@Override
	public String searchResolve(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search/resolve";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String searchGraphSchema(String searchType, String query) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/search/searchGraphSchema/" + encode(searchType) + "/" + encode(query);
		return getDataFromServer(url);	
	}

	private String getDataFromServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	private String postStringDataToServer(String url, String jsonString) throws Exception{
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	private String deleteDataInServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_DELETE_DATA_ON_SERVER_MSG);
		}
	}
	
	private String encode(String value) throws UnsupportedEncodingException{
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	@Override
	public String caseGraph(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/case/graph";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getEntitesForCase(String caseId) throws Exception {
		String url = WORKFLOW_EXTENSION_DEVELOPMENT_API + "/graph/api/case/"+ encode(caseId) +"/entities";
		return getDataFromServer(url);	
	}

	@Override
	public String getRickScoreOfEntity(String entityId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/graph/risk/"+ encode(entityId);
		return getDataFromServer(url);	
	}
}
