package element.bst.elementexploration.rest.casemanagement.enumType;

public enum GranualityEnum {

	HOURLY(1, "hourly"), 
	DAILY(2, "daily"), 
	WEEKLY(3, "weekly"), 
	MONTHLY(4, "monthly"), 
	QUARTERLY(5, "quarterly"), 
	HALF_YEARLY(6, "halfYearly"), 
	YEARLY(7, "yearly"), 
	ALL(8, "all");
	
	private int index = 0;
	
	private String name = null;
	
	
	private GranualityEnum(int index, String name) {
		this.index = index;
		this.name = name;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public String getName() {
		return this.name;
	}
}
