package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Rambabu
 *
 */

@ApiModel("Document answer")
public class DocumentAnswerDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "ID if the answer")
	private Long id;
	@ApiModelProperty(value = "Answer given")
	private String answer;
	@ApiModelProperty(value = "Specified answer for the question")
	private String specifiedAnswer;
	@ApiModelProperty(value = "ID of the question in the document")
	private Long documentQuestionId;
	@ApiModelProperty(value = "Document updated time")
	private Date updatedTime;
	@ApiModelProperty(value = "Document id")
	private Long docId;
	

	public DocumentAnswerDto() {
		super();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getSpecifiedAnswer() {
		return specifiedAnswer;
	}

	public void setSpecifiedAnswer(String specifiedAnswer) {
		this.specifiedAnswer = specifiedAnswer;
	}

	public Long getDocumentQuestionId() {
		return documentQuestionId;
	}

	public void setDocumentQuestionId(Long documentQuestionId) {
		this.documentQuestionId = documentQuestionId;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}


}
