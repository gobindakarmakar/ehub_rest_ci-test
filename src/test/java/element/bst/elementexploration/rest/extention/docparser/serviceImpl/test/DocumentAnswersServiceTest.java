package element.bst.elementexploration.rest.extention.docparser.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentAnswerDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentAnswersService;

public class DocumentAnswersServiceTest {
	
	@Mock
	DocumentAnswersService documentAnswersServiceMock;
	
	public DocumentAnswers documentAnswer;
	
	@Before
    public void setupMock() {
		DocumentQuestions documentQuestions = new DocumentQuestions();
		documentAnswer = new DocumentAnswers(1L, "test", "", documentQuestions,
				new Date(), 123L, 2L, "undefined", "", "abcd",4,new Date(), 1L,"","");
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void saveNewDocumentAnswerTest() throws Exception {		
		Mockito.when(documentAnswersServiceMock.saveNewDocumentAnswer(Mockito.any(DocumentAnswerDto.class), Mockito.anyLong())).thenReturn(documentAnswer);
		DocumentAnswers newDocumentAnswer = documentAnswersServiceMock.saveNewDocumentAnswer(Mockito.any(DocumentAnswerDto.class), Mockito.anyLong());
		Assert.assertNotNull(newDocumentAnswer);		
	}
	@Test
	public void updateEvaluationTest() {		
		Mockito.when(documentAnswersServiceMock.updateEvaluation(Mockito.any(DocumentAnswers.class),Mockito.anyString(),Mockito.anyLong())).thenReturn(201);
		int statusCode = documentAnswersServiceMock.updateEvaluation(Mockito.any(DocumentAnswers.class),Mockito.anyString(),Mockito.anyLong());
		assertEquals(201,statusCode);
	}
	@Test
	public void updateAnswerRankingTest() {		
		Mockito.when(documentAnswersServiceMock.updateAnswerRanking(Mockito.any(DocumentAnswers.class),Mockito.anyInt(),Mockito.anyLong())).thenReturn(201);
		int statusCode = documentAnswersServiceMock.updateAnswerRanking(Mockito.any(DocumentAnswers.class),Mockito.anyInt(),Mockito.anyLong());
		assertEquals(201,statusCode);
	}

}
