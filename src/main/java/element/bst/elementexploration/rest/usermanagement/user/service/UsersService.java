package element.bst.elementexploration.rest.usermanagement.user.service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.dto.UsersListByGroupListDto;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.AssigneeDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.DateCountStatusDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.LogonsDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDtoWithRolesAndGroups;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersRegistrationDto;
import element.bst.elementexploration.rest.util.FilterModelDto;

/**
 * @author Paul Jalagari
 *
 */
public interface UsersService extends GenericService<Users, Long>  {

	void setUserLastLoginDateAndIp(UsersDto userDto, String ip);

	public Properties getProperties();

	void sendEmailVerification(Long userId, String userName, String url, String token, String emailAddress);// may be need to change

	boolean activateUser(Users user);

	boolean deactivateUser(Users user);

	List<UsersDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	List<UsersDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn);

	Long countUserList(Long groupId, String status);

	Long countUserListFullTextSearch(String keyword, String status, Long groupId);
	
	boolean isActiveUser(Long userId);

	List<UsersDto> listUserByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countUserByGroup(Long groupId, Integer userStatus);

	UsersDto getUserByEmailOrUsername(String email);
	
	Users getUserObjectByEmailOrUsername(String email);
	
	UsersDto checkUserNameOrEmailExists(String email, Long id);

	UsersListByGroupListDto getUsersFromGroupsList(List<Long> groupIds, long userId) throws Exception;

	Long activeUsersCount();
	
	boolean sendEmailForVerification(UsersDto userAfterChange, HttpServletRequest request) throws Exception;

	Map<List<AssigneeDto>, Long> getUsers(FilterModelDto filterDto, Boolean isAllRequired,
			Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy);
	boolean updateUser(Users user,UsersRegistrationDto userRegistrationDto,Long currentUserId) throws Exception;

	UsersDtoWithRolesAndGroups getUserProfileById(Long userId, Long currentUserId) throws Exception;

	boolean deactivateUser(Users user, Long currentUserId) throws Exception;

	List<UsersDtoWithRolesAndGroups> quickSearch(Long currentUserId, String searchKey, boolean isScreeNameRequired) throws Exception;

	List<DateCountStatusDto> getCountByStatusWise(Long currentUserId) throws Exception;

	List<Long> getUserIdByRolesList(List<Long> comingIdList);

	List<Long> getUserIdByGroupsList(List<Long> comingIdList);

	Map<Object, Long> getLogonsByHourCount(String period, String from, String to);

	/*Map<List<LogonsDto>, Long> getLogonFailures(FilterModelDto filterDto, Boolean isAllRequired, Integer pageNumber,
			Integer recordsPerPage, Long totalResults, String orderIn, String orderBy);*/

	ByteArrayOutputStream prepareCSV(List<AssigneeDto> csvDto);

	Map<String, Long> getTopTenLogonFailuresUsers(String period, String from, String to);

	Map<Object, Long> getFailedLogonsCount(String period, String from, String to);

	boolean uploadUserImage(Users user, MultipartFile userImage, Long currentUserId) throws Exception;

	boolean deleteUserImage(Users user, Long currentUserId)throws Exception;

	List<Users> getUsersByIds(List<Long> userList);
	Map<List<LogonsDto>, Long> getLogonList(FilterModelDto filterDto, Integer pageNumber,
			Integer recordsPerPage, Long totalResults, String orderIn, String orderBy) throws Exception;

	Users getFirebaseUserBySourceId(String sourceId);

	Users createFirebaseUserBySourceId(JSONObject response);
	public Users updateFirebaseUserBySourceId(JSONObject response);
	
	public Users prepareUserFromFirebase(Users user);
}
