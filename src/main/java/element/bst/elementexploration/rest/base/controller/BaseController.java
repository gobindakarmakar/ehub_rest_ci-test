package element.bst.elementexploration.rest.base.controller;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*import javax.validation.ConstraintViolationException;*/

import org.hibernate.HibernateException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
/*import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;*/
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import element.bst.elementexploration.rest.exception.AuthenticationFailedException;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.DublicateTokenFoundException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.exception.FailedToUploadCaseSeedDocumentException;
import element.bst.elementexploration.rest.exception.FileFormatException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.usermanagement.security.domain.CurrentUser;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.ResponseMessage;

/**
 * 
 * @author Amit Patel
 *
 */

public class BaseController {
	
	public  Long getCurrentUserId(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
		return currentUser.getUserId();
	}
	
	// Custom Exceptions
	@ExceptionHandler(AuthenticationFailedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ResponseEntity<?> handleAuthenticationFailedException(AuthenticationFailedException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(FailedToExecuteQueryException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleFailedToExecuteQueryException(FailedToExecuteQueryException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(InsufficientDataException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleInsufficientDataException(InsufficientDataException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(DublicateTokenFoundException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT)
	@ResponseBody
	public ResponseEntity<?> handleDublicateTokenFoundException(DublicateTokenFoundException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(NoDataFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseEntity<?> handleNoDataFoundException(NoDataFoundException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(BadRequestException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleBadRequestException(BadRequestException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CaseSeedNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseEntity<?> handleCaseSeedNotFoundException(CaseSeedNotFoundException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(DateFormatException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleDateFormatException(DateFormatException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(DocNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseEntity<?> handleDocNotFoundException(DocNotFoundException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(FailedToUploadCaseSeedDocumentException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleFailedToUploadCaseSeedDocumentException(FailedToUploadCaseSeedDocumentException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(FileFormatException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleFileFormatEception(FileFormatException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(PermissionDeniedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ResponseEntity<?> handlePermissionDeniedException(PermissionDeniedException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.UNAUTHORIZED);
	}
	
	//Other System Exceptions	
	@ExceptionHandler(HibernateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleHibernateException(HibernateException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler({MissingServletRequestParameterException.class, UnsatisfiedServletRequestParameterException.class,
	        HttpRequestMethodNotSupportedException.class, ServletRequestBindingException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleRequestException(Exception ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	 
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleValidationException(MethodArgumentNotValidException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ObjectRetrievalFailureException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseEntity<?> handleValidationException(ObjectRetrievalFailureException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
	    return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT)
	@ResponseBody
	public ResponseEntity<?> handleDataIntegrityViolationException(DataIntegrityViolationException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(DataAccessException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleDataAccessException(DataAccessException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ResponseBody
	public ResponseEntity<?> handleUnsupportedMediaTypeException(HttpMediaTypeNotSupportedException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
	    return new ResponseEntity<>(new ResponseMessage("Unsupported Media Type."), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
	}
	
	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleNullPointerException(NullPointerException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleUncaughtException(Exception ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(InvocationTargetException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<?> handleInvocationTargetException(InvocationTargetException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ElementLogger.log(ElementLoggerLevel.ERROR, ex.getMessage(), this.getClass());
		return new ResponseEntity<>(new ResponseMessage("Request body is missing."), HttpStatus.BAD_REQUEST);
	}

}
