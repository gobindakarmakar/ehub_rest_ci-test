package element.bst.elementexploration.rest.extention.entitysearch.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.entitysearch.dto.AdverseNews;
import element.bst.elementexploration.rest.extention.entitysearch.service.EntitySearchService;

/**
 * @author Suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EntitySearchServiceTest{

	@Mock
	EntitySearchService entitySearchService;  
	
	List<AdverseNews> adverseNewsList ;
	
	@Before
	public void setupMock() {
		adverseNewsList = new ArrayList<AdverseNews>();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getBingNewsTest() throws Exception
	{
		Mockito.when(entitySearchService.getBingNews(Mockito.anyString())).thenReturn(adverseNewsList);
		List<AdverseNews> adverseNewsListNew =entitySearchService.getBingNews(Mockito.anyString());
		assertEquals(adverseNewsListNew,adverseNewsList);
	}
	
	
	@Test
	public void checkCognitiveSearch() throws Exception
	{
		Mockito.when(entitySearchService.getCognitiveMicrosoftWithAdvanceSearch(Mockito.anyString(), Mockito.anyString(),Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(adverseNewsList);
		List<AdverseNews> adverseNewsListNew =entitySearchService.getCognitiveMicrosoftWithAdvanceSearch(Mockito.anyString(), Mockito.anyString(),Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		assertEquals(adverseNewsListNew,adverseNewsList);
	}
	
	
}
