package element.bst.elementexploration.rest.graphdata.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.graphdata.service.GraphDataService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Graph data API" },description="Manages graph data")
@RestController
@RequestMapping("/api/graph")
public class GraphDataController extends BaseController{

	@Autowired
	private GraphDataService graphDataService;

	@ApiOperation("Gets all data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllDataFromBigDataServer(@RequestParam("caseId") Long caseId, @RequestParam("limit") Integer limit,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {

		return new ResponseEntity<>(graphDataService.getDataFromBigDataServer(caseId, limit), HttpStatus.OK);
	}

	@ApiOperation("Gets risk data by case ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getRiskDataByCase/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskDataByCaseId(@PathVariable("caseId") Long caseId,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		
		return new ResponseEntity<>(graphDataService.getRiskDataByCaseId(caseId), HttpStatus.OK);
	}

	@ApiOperation("Gets shortest path between two graph vertexes")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/{vertexid1}/shortestpath/{vertexid2}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> shortestPath(@PathVariable String vertexid1, @PathVariable String vertexid2,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		
		return new ResponseEntity<>(graphDataService.shortestPath(vertexid1, vertexid2), HttpStatus.OK);
	}

	@ApiOperation("Gets entity graph by type and name with a limit  ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/{type}/{name}/entitygraph/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> entityGraph(@PathVariable String type, @PathVariable String name,
			@PathVariable String limit, @RequestParam("token") String token, HttpServletRequest request) throws Exception {
		
		return new ResponseEntity<>(graphDataService.entityGraph(type, name, limit), HttpStatus.OK);
	}
}
