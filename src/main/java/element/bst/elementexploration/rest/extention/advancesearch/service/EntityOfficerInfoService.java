package element.bst.elementexploration.rest.extention.advancesearch.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityOfficerInfo;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface EntityOfficerInfoService extends GenericService<EntityOfficerInfo, Long>{

	boolean saveEntityOfficer(List<EntityOfficerInfoDto> entityOfficers);
	
	EntityOfficerInfoDto getOfficerFromLocal(EntityOfficerInfoDto dto);

	List<EntityOfficerInfoDto> getEntityOfficerList(String entityId); 

}