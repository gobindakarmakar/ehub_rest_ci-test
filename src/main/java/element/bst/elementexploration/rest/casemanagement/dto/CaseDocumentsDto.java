package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari PAul
 *
 */
@ApiModel("Case Documents List")
public class CaseDocumentsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="Case document details list")
	private List<CaseDocDetailsDTO> result;

	@ApiModelProperty(value="Page information")
	private PaginationInformation paginationInformation;

	public List<CaseDocDetailsDTO> getResult() {
		return result;
	}

	public void setResult(List<CaseDocDetailsDTO> result) {
		this.result = result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}

}
