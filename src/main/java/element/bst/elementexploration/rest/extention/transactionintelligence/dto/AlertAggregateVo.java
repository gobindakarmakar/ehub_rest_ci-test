package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class AlertAggregateVo {

	private String entityType;
	
	@ApiModelProperty(value = "Granularity", required = true,example="daily,weekly,monthly,yearly")
	@NotEmpty(message = "Granularity can not be blank.")
	private String granularity;
	
	@ApiModelProperty(value = "From date", required = true,example="dd-MM-yyyy")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@NotNull(message = "From date can not be blank.")
	private Date fromDate;

	@ApiModelProperty(value = "To date", required = true,example="dd-MM-yyyy")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@NotNull(message = "To date can not be blank.")
	private Date toDate;

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getGranularity() {
		return granularity;
	}

	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	} 
}
