package element.bst.elementexploration.rest.settings.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author suresh
 *
 */
public class SettingsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long settingId;
	private String name;
	private String section;
	private String type;
	private int maximumLength;
	private String selectedValue;
	private boolean required;
	private String defaultValue;
	private String validation;
	private String systemSettingType;
	private List<SelectedObject> options;
	private String customerLogoImage;
	
	private Integer selectedValueMax;
	private Integer sliderFrom;
	private Integer sliderTo;
	//multipurpose
	private String differentiator;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMaximumLength() {
		return maximumLength;
	}

	public void setMaximumLength(int maximumLength) {
		this.maximumLength = maximumLength;
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

	public List<SelectedObject> getOptions() {
		return options;
	}

	public void setOptions(List<SelectedObject> options) {
		this.options = options;
	}

	public long getSettingId() {
		return settingId;
	}

	public void setSettingId(long settingId) {
		this.settingId = settingId;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

	public String getSystemSettingType() {
		return systemSettingType;
	}

	public void setSystemSettingType(String systemSettingType) {
		this.systemSettingType = systemSettingType;
	}

	public String getCustomerLogoImage() {
		return customerLogoImage;
	}

	public void setCustomerLogoImage(String customerLogoImage) {
		this.customerLogoImage = customerLogoImage;
	}

	public Integer getSelectedValueMax() {
		return selectedValueMax;
	}

	public void setSelectedValueMax(Integer selectedValueMax) {
		this.selectedValueMax = selectedValueMax;
	}

	public Integer getSliderFrom() {
		return sliderFrom;
	}

	public void setSliderFrom(Integer sliderFrom) {
		this.sliderFrom = sliderFrom;
	}

	public Integer getSliderTo() {
		return sliderTo;
	}

	public void setSliderTo(Integer sliderTo) {
		this.sliderTo = sliderTo;
	}

	public String getDifferentiator() {
		return differentiator;
	}

	public void setDifferentiator(String differentiator) {
		this.differentiator = differentiator;
	}

	
}
