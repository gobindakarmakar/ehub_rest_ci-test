package element.bst.elementexploration.test.notification.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.notification.dao.NotificationCheckDao;
import element.bst.elementexploration.rest.notification.dao.NotificationDao;
import element.bst.elementexploration.rest.notification.dao.NotificationMappingDao;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.domain.NotificationMapping;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;
import element.bst.elementexploration.rest.notification.dto.ParticipantDTO;
import element.bst.elementexploration.rest.notification.serviceImpl.NotificationServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

	@Mock
	NotificationDao notificationDaoMock;

	@Mock
	NotificationCheckDao notificationCheckDaoMock;

	@Mock
	NotificationMappingDao notificationMappingDaoMock;

	@Mock
	UsersDao userDaoMock;

	@Mock
	AuditLogDao auditLogDaoMock;

	@InjectMocks
	NotificationServiceImpl notificationServiceImpl;

	public ArrayList<Long> recipients;

	public NotificationAlert notificationMock;

	public List<NotificationAlertDTO> notificationAlertDTOList;

	public NotificationAlertDTO notificationAlertDTO;

	public List<Users> users;

	public List<CaseAnalystMapping> caseSeedAnalystMappingsList;

	public List<EventDTO> eventDtoList;

	public EventDTO eventDto;

	public EventDetailDTO eventDetailDTO;

	public List<ParticipantDTO> participantsDto;

	public NotificationMapping notificationMapping;

	public Users user1;

	public AuditLog auditLogMock;
	
	LocalDateTime localDateTime;
	

	@Before
	public void setupMock() {
		localDateTime= LocalDateTime.now();
		recipients = new ArrayList<>();
		recipients.add(2L);
		recipients.add(3L);
		notificationMock = new NotificationAlert(1L, "test subject", "test body", 2L, recipients);
		notificationAlertDTO = new NotificationAlertDTO(1L, "test subject", "test body", 2, LocalDateTime.now(),
				LocalDateTime.now(), 2, 1L, 22L, 0);
		NotificationAlertDTO notificationAlertDTO2 = new NotificationAlertDTO(2L, "test subject", "test body", 2,
				LocalDateTime.now(), LocalDateTime.now(), 2, 1L, 22L, 0);
		notificationAlertDTOList = new ArrayList<>();
		notificationAlertDTOList.add(notificationAlertDTO);
		notificationAlertDTOList.add(notificationAlertDTO2);
		users = new ArrayList<>();
		List<CaseAnalystMapping> caseSeedAnalystMappingsList = new ArrayList<>();
		user1 = new Users(1L, "test uuid", "test@gmail.com", "test", "test", "test", caseSeedAnalystMappingsList);
		Users user2 = new Users(2L, "test uuid", "test2@gmail.com", "test2", "test2", "test2",
				caseSeedAnalystMappingsList);
		users.add(user1);
		users.add(user2);
		eventDtoList = new ArrayList<>();
		eventDto = new EventDTO(1L, "test subject", "test body", LocalDateTime.now(), LocalDateTime.now(),
				LocalDateTime.now(), 1, 1, 1L, 22L, 1);
		EventDTO eventDto1 = new EventDTO(2L, "test subject", "test body", LocalDateTime.now(), LocalDateTime.now(),
				LocalDateTime.now(), 1, 1, 1L, 22L, 1);
		eventDtoList.add(eventDto);
		eventDtoList.add(eventDto1);
		eventDetailDTO = new EventDetailDTO(1L, "test subject", "test body", LocalDateTime.now(), LocalDateTime.now(),
				LocalDateTime.now(), 1, 1, 1L, 22L, 1);
		notificationMapping = new NotificationMapping(1L, 2L);
		auditLogMock = new AuditLog(new Date(), LogLevels.INFO, "test", 5, "Junit");

		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void createNotificationAlertTest() {
		Mockito.when(notificationDaoMock.create(Mockito.any(NotificationAlert.class))).thenReturn(notificationMock);
		long result = notificationServiceImpl.createNotificationAlert(notificationMock);
		assertEquals(1L, result);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = InsufficientDataException.class)
	public void createNotificationAlert_whenAnyFeildInNotificationalertIsNullTest() {
		notificationMock.setBody(null);
		notificationMock.setSubject("");
		Mockito.when(notificationDaoMock.create(Mockito.any(NotificationAlert.class)))
				.thenThrow(InsufficientDataException.class);
		notificationServiceImpl.createNotificationAlert(notificationMock);
	}

	@Test
	public void checkInNotificationsTest() {
		Mockito.when(notificationCheckDaoMock.checkInNotification(Mockito.anyLong())).thenReturn(LocalDateTime.now());
		String dateTime = notificationServiceImpl.checkInNotifications(1L);
		Assert.assertNotNull(dateTime);
	}

	@SuppressWarnings("static-access")
	@Test
	public void getLastCheckInTest() {
		Mockito.when(notificationCheckDaoMock.getLastCheckIn(Mockito.anyLong())).thenReturn(localDateTime);
		LocalDateTime dateTime = notificationServiceImpl.getLastCheckIn(2L);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String expected = localDateTime.format(formatter);
		//Assert.assertNotNull(dateTime);
        String obtained = dateTime.now().format(formatter);
		assertEquals(expected, obtained);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void retrieveCaseNotificationAlertBasedOnLastCheckInTest() {
		Mockito.when(notificationDaoMock.retrieveCaseNotificationAlertBasedOnLastCheckIn(Mockito.anyLong(),
				Mockito.anyLong(), Mockito.any(LocalDateTime.class), Mockito.anyInt(), Mockito.anyInt(),
				Mockito.any(LocalDateTime.class))).thenThrow(Exception.class);
		notificationServiceImpl.retrieveCaseNotificationAlertBasedOnLastCheckIn(1L, 1L, LocalDateTime.now(), 1, 2,
				LocalDateTime.now());

	}

	@Test
	public void getUserNotificationListTest() {
		Mockito.when(notificationDaoMock.getUserNotificationList(Mockito.anyLong(), Mockito.any(LocalDateTime.class),
				Mockito.any(LocalDateTime.class), Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.any(LocalDateTime.class), Mockito.anyString()))
				.thenReturn(notificationAlertDTOList);
		List<NotificationAlertDTO> returnList = notificationServiceImpl.getUserNotificationList(1L, LocalDateTime.now(),
				LocalDateTime.now(), 2L, 2, 2, 0, "test subject", LocalDateTime.now(), "test body");
		assertEquals(2, returnList.size());
	}

	@Test
	public void retrieveNotificationDetailTest() {
		Mockito.when(notificationDaoMock.retrieveNotificationDetail(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(notificationAlertDTO);
		NotificationAlertDTO result = notificationServiceImpl.retrieveNotificationDetail(1L, 1L);
		assertEquals(notificationAlertDTO, result);
	}

	@Test
	public void countUserNotificationListTest() {
		Mockito.when(notificationDaoMock.countUserNotificationList(Mockito.anyLong(), Mockito.any(LocalDateTime.class),
				Mockito.any(LocalDateTime.class), Mockito.anyLong(), Mockito.anyInt(),
				Mockito.any(LocalDateTime.class))).thenReturn(2L);
		long result = notificationServiceImpl.countUserNotificationList(2L, LocalDateTime.now(), LocalDateTime.now(),
				2L, 10, LocalDateTime.now());
		assertEquals(2L, result);
	}

	@Test
	public void createEventTest() {
		Mockito.when(notificationDaoMock.create(Mockito.any(NotificationAlert.class))).thenReturn(notificationMock);
		Mockito.when(notificationMappingDaoMock.create(Mockito.any(NotificationMapping.class)))
				.thenReturn(notificationMapping);
		long id = notificationServiceImpl.createEvent(notificationMock);
		assertEquals(1L, id);
	}

	@Test
	public void isEventOwnerTest() {
		Mockito.when(notificationDaoMock.isEventOwner(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		boolean result = notificationServiceImpl.isEventOwner(4L, 4L);
		assertEquals(true, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void addEventParticipantsTest() {
		Mockito.when(notificationDaoMock.find(Mockito.anyLong())).thenReturn(notificationMock);
		Mockito.when(userDaoMock.getUsersInList(Mockito.anyListOf(Long.class))).thenReturn(users);
		Mockito.when(notificationMappingDaoMock.addEventParticipants(Mockito.anyLong(), Mockito.anyListOf(Long.class)))
				.thenReturn(recipients);
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user1);
		Mockito.when(auditLogDaoMock.create(Mockito.any(AuditLog.class))).thenThrow(Exception.class);
		notificationServiceImpl.addEventParticipants(2L, recipients, 3L);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = NoDataFoundException.class)
	public void addEventParticipants_whenEventIdIsNullTest() {
		Mockito.when(notificationDaoMock.find(Mockito.anyLong())).thenThrow(NoDataFoundException.class);
		notificationServiceImpl.addEventParticipants(null, recipients, 3L);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = NoDataFoundException.class)
	public void removeEventParticipantsTest() {
		Mockito.when(notificationDaoMock.find(Mockito.anyLong())).thenReturn(notificationMock);
		Mockito.doNothing().when(notificationMappingDaoMock).removeEventParticipants(Mockito.anyLong(),
				Mockito.anyListOf(Long.class));
		Mockito.when(userDaoMock.getUsersInList(Mockito.anyListOf(Long.class))).thenReturn(users);
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user1);
		Mockito.when(auditLogDaoMock.create(Mockito.any(AuditLog.class))).thenThrow(Exception.class);
		notificationServiceImpl.addEventParticipants(2L, recipients, 3L);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = NoDataFoundException.class)
	public void removeEventParticipants_whenEventIdIsNullTest() {
		Mockito.when(notificationDaoMock.find(Mockito.anyLong())).thenThrow(NoDataFoundException.class);
		notificationServiceImpl.addEventParticipants(null, recipients, 3L);
	}

	@Test
	public void updateEventTest() {
		Mockito.doNothing().when(notificationDaoMock).saveOrUpdate(Mockito.any(NotificationAlert.class));
		notificationServiceImpl.updateEvent(notificationMock, notificationMock);
	}

	@Test
	public void getUserEventListTest() {
		Mockito.when(notificationDaoMock.getUserEventList(Mockito.anyLong(), Mockito.any(LocalDateTime.class),
				Mockito.any(LocalDateTime.class), Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(eventDtoList);
		List<EventDTO> result = notificationServiceImpl.getUserEventList(2L, LocalDateTime.now(), LocalDateTime.now(),
				4L, 2, 3, 1, "test", "Mockito");
		assertEquals(eventDtoList.size(), result.size());
	}

	@Test
	public void countUserEventListTest() {
		Mockito.when(notificationDaoMock.countUserEventList(Mockito.anyLong(), Mockito.any(LocalDateTime.class),
				Mockito.any(LocalDateTime.class), Mockito.anyLong(), Mockito.anyInt())).thenReturn(2L);
		long count = notificationServiceImpl.countUserEventList(2L, LocalDateTime.now(), LocalDateTime.now(), 2L, 2);
		assertEquals(2L, count);
	}

	@Test
	public void getEventListByCreatorTest() {
		Mockito.when(notificationDaoMock.getEventListByCreator(Mockito.anyLong(), Mockito.any(LocalDateTime.class),
				Mockito.any(LocalDateTime.class), Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(eventDtoList);
		List<EventDTO> list = notificationServiceImpl.getEventListByCreator(1L, LocalDateTime.now(),
				LocalDateTime.now(), 2L, 1, 2, 3, "test", "Junit");
		assertEquals(eventDtoList.size(), list.size());
	}

	@Test
	public void countEventListByCreatorTest() {
		Mockito.when(notificationDaoMock.countEventListByCreator(Mockito.anyLong(), Mockito.any(LocalDateTime.class),
				Mockito.any(LocalDateTime.class), Mockito.anyLong(), Mockito.anyInt())).thenReturn(2L);
		long count = notificationServiceImpl.countEventListByCreator(2L, LocalDateTime.now(), LocalDateTime.now(), 2L,
				1);
		assertEquals(2L, count);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void retrieveEventDetailTest() {
		Mockito.when(notificationDaoMock.retrieveEventDetail(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(eventDetailDTO);
		Mockito.when(notificationMappingDaoMock.getEventParticipants(Mockito.anyLong())).thenReturn(recipients);
		Mockito.when(userDaoMock.getUsersInList(Mockito.anyListOf(Long.class))).thenThrow(Exception.class);
		notificationServiceImpl.retrieveEventDetail(2L, 1L);
	}

}
