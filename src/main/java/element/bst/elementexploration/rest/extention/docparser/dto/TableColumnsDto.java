package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
/**
 * @author rambabu
 *
 */
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;


@ApiModel("Table columns")
public class TableColumnsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String columnName;
	@JsonProperty("Sub_Column")
	private List<TableSubColumnDto> subColumns;
	@JsonProperty("data")
	private List<DocumentTableRowsDto> columnsData;

	public TableColumnsDto() {
		super();

	}

	public TableColumnsDto(String columnName, List<TableSubColumnDto> subColumns,
			List<DocumentTableRowsDto> columnsData) {
		super();
		this.columnName = columnName;
		this.subColumns = subColumns;
		this.columnsData = columnsData;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public List<TableSubColumnDto> getSubColumns() {
		return subColumns;
	}

	public void setSubColumns(List<TableSubColumnDto> subColumns) {
		this.subColumns = subColumns;
	}

	public List<DocumentTableRowsDto> getColumnsData() {
		return columnsData;
	}

	public void setColumnsData(List<DocumentTableRowsDto> columnsData) {
		this.columnsData = columnsData;
	}

}
