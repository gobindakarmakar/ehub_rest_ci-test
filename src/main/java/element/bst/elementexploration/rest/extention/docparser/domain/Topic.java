package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TOPIC")
public class Topic implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private String isoCode;
	
	public Topic() {
		super();
		
	}

	public Topic(long id, String name, String isoCode) {
		super();
		this.id = id;
		this.name = name;
		this.isoCode = isoCode;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "ISO_CODES")
	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	
	

}
