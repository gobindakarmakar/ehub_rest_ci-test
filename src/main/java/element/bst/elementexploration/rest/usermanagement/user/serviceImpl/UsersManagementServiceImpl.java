package element.bst.elementexploration.rest.usermanagement.user.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersManagementService;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author Prateek Maurya
 *
 */
@Service("usersManagementService")
public class UsersManagementServiceImpl implements UsersManagementService{

	@Autowired
	UsersService usersService;
	
	@Autowired
	GroupsService groupsService;
	
	@Autowired
	RolesService rolesService;
	
	@Autowired
	UserGroupsService userGroupsService;
	
	@Autowired
	UserRolesService userRolesService;
	
	@Override
	@Transactional("transactionManager")
	public Map<String, Long> mainTilesData() {
		Map<String, Long> tilesMap = new HashMap<String, Long>();
		tilesMap.put("Active Users", usersService.activeUsersCount());
		tilesMap.put("Roles", rolesService.getRolesCount());
		tilesMap.put("Groups", groupsService.countUserGroups(null, null));
		tilesMap.put("Policies", 0L);
		return tilesMap;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, Long> groupsWithUserCount() {
		Map<String, Long> finalMap = new HashMap<String, Long>();
		try{
			List<Groups> groupsList = groupsService.findAll();
			Map<Long, Long> groupUserCountMap = userGroupsService.groupsWithUserCount();
			
			if(groupsList != null){
				for(Groups group : groupsList){
					if(groupUserCountMap != null){
						if(groupUserCountMap.containsKey(group.getId())){
							finalMap.put(group.getName(), groupUserCountMap.get(group.getId()));
						}else{
							finalMap.put(group.getName(), 0L);
						}
					}
				}
			}
			return finalMap;
			
		}catch(Exception ex){
			return finalMap;
		}
	}
	
	@Override
	@Transactional("transactionManager")
	public Map<String, Long> rolesWithUserCount() {
		Map<String, Long> finalMap = new HashMap<String, Long>();
		try{
			List<Roles> rolesList = rolesService.findAll();
			Map<Long, Long> roleUserCountMap = userRolesService.rolesWithUserCount();
			if(rolesList != null){
				for(Roles roles : rolesList){
					if(roleUserCountMap != null){
						if(roleUserCountMap.containsKey(roles.getRoleId())){
							finalMap.put(roles.getRoleName(), roleUserCountMap.get(roles.getRoleId()));
						}else{
							finalMap.put(roles.getRoleName(), 0L);
						}
					}
				}
			}
			return finalMap;
			
		}catch(Exception ex){
			return finalMap;
		}
	}

}
