package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @author Paul Jalagari
 *
 */
public class GroupAlertSettingDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Group ID cannot be null")
	private Long groupId;

	private List<Long> listItemId;

	private String type;

	private String geoLocationSetting;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<Long> getListItemId() {
		return listItemId;
	}

	public void setListItemId(List<Long> listItemId) {
		this.listItemId = listItemId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGeoLocationSetting() {
		return geoLocationSetting;
	}

	public void setGeoLocationSetting(String geoLocationSetting) {
		this.geoLocationSetting = geoLocationSetting;
	}

}
