package element.bst.elementexploration.rest.settings.service;

import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.dto.AttributeDto;
import element.bst.elementexploration.rest.settings.dto.SettingsDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsRequestDto;

/**
 * @author suresh
 *
 */
public interface SettingsService extends  GenericService<Settings, Long>
{
	 SystemSettingsDto fetchSystemSettings(String searchKey);
	boolean updateSystemSettings(SystemSettingsRequestDto dto);
	String getDeaultUrl();
	Map<String,String> getMailSettings();
	List<Settings> findAllWithAttributes();
	List<SettingsDto> fetchSystemSettingsByType(String settingType);
	String findSettingToggleValue(String settingType, String settingName);
	boolean addAttributes(AttributeDto attributeDto) throws Exception;
	List<Settings> getBasicSettings();

}
