package element.bst.elementexploration.rest.search.service;

import java.text.ParseException;
import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.search.domain.SearchHistory;

public interface SearchHistoryService extends GenericService<SearchHistory, Long>{

	Long createSearchHistory(Long userId, String search, String searchFlag) throws Exception;
	
	String getSearchData(Long userId, String search, String searchFlag) throws Exception;
	
	boolean setFavouriteSearch(Long userId, Long searchId);
	
	List<SearchHistory> getRecentSearches(Long userId, String dateFrom, String dateTo, Boolean favourite, String flag,
			Integer pageNumber, Integer recordsPerPage) throws ParseException;
	
	long countRecentSearches(Long userId, String dateFrom, String dateTo, Boolean favourite, String flag) throws ParseException;
	
	String getCaseGraphData(String caseId) throws Exception;
}
