package element.bst.elementexploration.rest.util;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * 
 * @author Prateek Maurya
 *
 */
public class RequestFilterDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String filterType;
	
	private String subType;
	
	private String entityColumn;
	
	private String[] keyword;

	private Date dateFrom;
	
	private Date dateTo;
	
	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getEntityColumn() {
		return entityColumn;
	}

	public void setEntityColumn(String entityColumn) {
		this.entityColumn = entityColumn;
	}

	public String[] getKeyword() {
		return keyword;
	}

	public void setKeyword(String[] keyword) {
		this.keyword = keyword;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

}
