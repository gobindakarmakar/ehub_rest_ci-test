package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TxByType 
{
	
	private Long count;
	@JsonProperty("point")
	private String transProductType;
	
	
	public TxByType(Long count, String transProductType) {
		super();
		this.count = count;
		this.transProductType = transProductType;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getTransProductType() {
		return transProductType;
	}
	public void setTransProductType(String transProductType) {
		this.transProductType = transProductType;
	}
	
	
	

}
