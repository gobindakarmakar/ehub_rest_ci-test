package element.bst.elementexploration.test.document.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.document.daoImpl.DocumentCaseMppingDaoImpl;
import element.bst.elementexploration.rest.document.domain.DocumentVaultCaseMapping;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentCaseMppingDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<DocumentVaultCaseMapping> root;

	@InjectMocks
	DocumentCaseMppingDaoImpl documentCaseMppingDao;

	List<CaseDto> caseDtoList;
	DocumentVaultCaseMapping documentVaultCaseMapping = new DocumentVaultCaseMapping();

	@Before
	public void doSetup() {

		CaseDto caseDto = new CaseDto();
		caseDto.setCaseId(1L);
		caseDto.setCreatedOn(new Date());
		caseDtoList = new ArrayList<>();
		caseDtoList.add(caseDto);
		documentVaultCaseMapping.setDocId(1L);
		documentVaultCaseMapping.setCaseId(1L);
		documentVaultCaseMapping.setDocCaseId(1L);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getSeedByDocumentIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListNew = documentCaseMppingDao.getSeedByDocumentId(1L);
		assertEquals(0, caseDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getSeedByDocumentId_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		List<CaseDto> caseDtoListNew = documentCaseMppingDao.getSeedByDocumentId(1L);
		assertEquals(0, caseDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByCaseIdAndDocIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(DocumentVaultCaseMapping.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(documentVaultCaseMapping);
		DocumentVaultCaseMapping documentVaultCaseMappingNew = documentCaseMppingDao.findByCaseIdAndDocId(1L, 1L);
		assertEquals(new Long("1"), documentVaultCaseMappingNew.getDocId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByCaseIdAndDocId_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(DocumentVaultCaseMapping.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		DocumentVaultCaseMapping documentVaultCaseMappingNew = documentCaseMppingDao.findByCaseIdAndDocId(1L, 1L);
		assertEquals(null, documentVaultCaseMappingNew);
	}

}
