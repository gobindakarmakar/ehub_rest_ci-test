package element.bst.elementexploration.rest.extention.significantnews.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantCommentDao;
import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantWatchListDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantWatchList;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantWatchListDto;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantWatchListService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Service("significantWatchListService")
public class SignificantWatchListServiceImpl extends GenericServiceImpl<SignificantWatchList, Long>
		implements SignificantWatchListService {

	@Autowired
	private SignificantWatchListDao significantWatchListDao;
	
	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	@Autowired
	private AuditLogDao auditLogDao;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	SignificantCommentDao significantCommentDao;

	public SignificantWatchListServiceImpl(GenericDao<SignificantWatchList, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public SignificantWatchList getSignificantWatchList(String mainEntityId, String entityId, String entityName,
			String value,String name) {
		SignificantWatchList significantWatchList = significantWatchListDao.getSignificantWatchList(mainEntityId,
				entityId, entityName, value,name);
		return significantWatchList;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteWatchListPepSignificant(Long significantId,String comment, Long userId) {
		boolean status = false;
		SignificantWatchList existingtWatchList = significantWatchListDao.find(significantId);
		if (existingtWatchList != null) {
			AuditLog log = new AuditLog(new Date(), LogLevels.WARNING, "SIGNIFICANT UNMARK", null,
					"Significant watchlist deleted");
			Users author = usersDao.find(userId);
			author = usersService.prepareUserFromFirebase(author);
			log.setUserId(userId);
			log.setName(existingtWatchList.getName());
				existingtWatchList.setPep(false);
				significantWatchListDao.saveOrUpdate(existingtWatchList);
				if (comment != null) {					
					SignificantComment significantComment = new SignificantComment();
					significantComment.setClassification("PEP");
					significantComment.setComment(comment);
					significantComment.setUserId(userId);
					significantComment.setIsSignificant(false);
					significantComment.setSignificantId(existingtWatchList.getId());
					significantCommentDao.create(significantComment);
				}
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " deleted "
						+ existingtWatchList.getValue()+" from significant watchlist");
				status = true;
				auditLogDao.create(log);
		}
		else
			throw new NoDataFoundException("WatchList not found");
		return status;
	}

	@Override
	@Transactional("transactionManager")
	public SignificantWatchListDto saveSignificantWatchList(SignificantWatchListDto significantWatchListDto) {
		SignificantWatchList significantWatchList = new SignificantWatchList();
		SignificantWatchListDto resultSignificantWatchListDto = new SignificantWatchListDto();
		BeanUtils.copyProperties(significantWatchListDto, significantWatchList);
		significantWatchList.setPep(significantWatchListDto.getIsPep());
		significantWatchList.setSanction(significantWatchListDto.getIsSanction());
		SignificantWatchList newSignificantWatchList = null;
		SignificantWatchList existingtWatchList = null;
		if(significantWatchList.getId() != null)
			existingtWatchList = significantWatchListDao.find(significantWatchList.getId());
		else
			existingtWatchList = significantWatchListDao.getSignificantWatchList(
				significantWatchList.getMainEntityId(), significantWatchList.getIdentifier(),
				significantWatchList.getEntityName(), significantWatchList.getValue(), significantWatchList.getName());
		if (existingtWatchList != null) {
			if (existingtWatchList.isPep() != significantWatchList.isPep()) {
				existingtWatchList.setPep(significantWatchList.isPep());
				eventPublisher.publishEvent(new LoggingEvent(existingtWatchList.getId(),
						LoggingEventType.PEP_SIGNIFICANT_MARK, significantWatchList.getUserId(), new Date()));
			}
			if (existingtWatchList.isSanction() != significantWatchList.isSanction()) {
				existingtWatchList.setSanction(significantWatchList.isSanction());
				eventPublisher.publishEvent(new LoggingEvent(existingtWatchList.getId(),
						LoggingEventType.SANCTION_SIGNIFICANT_MARK, significantWatchList.getUserId(), new Date()));
			}
			significantWatchListDao.saveOrUpdate(existingtWatchList);
			newSignificantWatchList = existingtWatchList;
		} else {
			newSignificantWatchList = significantWatchListDao.create(significantWatchList);
			if (newSignificantWatchList.isSanction())
				eventPublisher.publishEvent(new LoggingEvent(newSignificantWatchList.getId(),
						LoggingEventType.SANCTION_SIGNIFICANT_MARK, significantWatchList.getUserId(), new Date()));
			if (newSignificantWatchList.isPep())
				eventPublisher.publishEvent(new LoggingEvent(newSignificantWatchList.getId(),
						LoggingEventType.PEP_SIGNIFICANT_MARK, significantWatchList.getUserId(), new Date()));

		}
		if( newSignificantWatchList.getId() != null){
		BeanUtils.copyProperties(newSignificantWatchList, resultSignificantWatchListDto);
		resultSignificantWatchListDto.setIsPep(newSignificantWatchList.isPep());
		resultSignificantWatchListDto.setIsSanction(newSignificantWatchList.isSanction());
		resultSignificantWatchListDto.setPepComment(significantWatchListDto.getPepComment());
		resultSignificantWatchListDto.setSanctionComment(significantWatchListDto.getSanctionComment());
		}
		return resultSignificantWatchListDto;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteWatchListSanctionSignificant(Long significantId,String comment, Long userIdTemp) {
		boolean status = false;
		SignificantWatchList existingtWatchList = significantWatchListDao.find(significantId);
		if (existingtWatchList != null) {
			AuditLog log = new AuditLog(new Date(), LogLevels.WARNING, "SIGNIFICANT UNMARK", null,
					"Significant watchlist deleted");
			Users author = usersDao.find(userIdTemp);
			author = usersService.prepareUserFromFirebase(author);
			log.setUserId(userIdTemp);
			log.setName(existingtWatchList.getName());
				existingtWatchList.setSanction(false);
				significantWatchListDao.saveOrUpdate(existingtWatchList);				
				if (comment != null) {					
					SignificantComment significantComment = new SignificantComment();
					significantComment.setClassification("SANCTION");
					significantComment.setComment(comment);
					significantComment.setUserId(userIdTemp);
					significantComment.setIsSignificant(false);
					significantComment.setSignificantId(existingtWatchList.getId());
					significantCommentDao.create(significantComment);
				}
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " deleted "
						+ existingtWatchList.getValue()+" from significant watchlist");
				status = true;
				auditLogDao.create(log);
		}
		else
			throw new NoDataFoundException("Watchlist not found");
		return status;
	}

	@Override
	@Transactional("transactionManager")
	public List<SignificantWatchList> fetchWtachListByEntityId(String mainEntityId) {
		return significantWatchListDao.fetchWtachListByEntityId(mainEntityId);
	}

}
