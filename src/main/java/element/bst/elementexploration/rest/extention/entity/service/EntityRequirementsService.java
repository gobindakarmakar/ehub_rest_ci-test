package element.bst.elementexploration.rest.extention.entity.service;

import element.bst.elementexploration.rest.extention.entity.domain.EntityRequirements;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface EntityRequirementsService extends GenericService<EntityRequirements, Long> {

}
