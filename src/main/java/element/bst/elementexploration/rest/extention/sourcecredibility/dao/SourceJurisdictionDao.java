package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourceJurisdictionDao extends GenericDao<SourceJurisdiction, Long> {
	
	SourceJurisdiction fetchJurisdiction(String name);
	List<SourceJurisdiction> fetchJurisdictionExceptAll();
	SourceJurisdiction findByListItemId(Long listItemId);
	
	int updateDBCollation(String schemaName);
	List<String> getNonUTF8Tables(String schemaName);
	int updateTableCollation(String tableName);

}
