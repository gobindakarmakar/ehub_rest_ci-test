package element.bst.elementexploration.rest.extention.advancesearch.dto;

public class ShareHoldersDto {
	
	private String method;
	
	private String url;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
