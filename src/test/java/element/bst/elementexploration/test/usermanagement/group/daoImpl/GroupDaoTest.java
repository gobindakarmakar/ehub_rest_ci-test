package element.bst.elementexploration.test.usermanagement.group.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.usermanagement.group.daoImpl.GroupDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupDto;

@RunWith(MockitoJUnitRunner.class)
public class GroupDaoTest {

	@Mock
	SessionFactory sessionFactory;

	@Mock
	Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	NativeQuery nativeQuery;
	
	@Mock
	CriteriaBuilder builder;
	
	@Mock
	CriteriaQuery<Object> criteriaQuery;
	
	@Mock
	Root<Group> root;

	@InjectMocks
	GroupDaoImpl groupDaoImpl;

	Group group;
	
	UserGroupDto userGroupDto;

	List<Group> listGroups = new ArrayList<Group>();

	@Before
	public void doSetup() {

		group = new Group(1L, new Date(), new Date(), 1, "name", "remarks", 2, 2L, "description");
		userGroupDto = new UserGroupDto(1L, 2L);
		Group group1 = new Group();
		Group group2 = new Group();

		listGroups.add(group1);
		listGroups.add(group2);

		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void getGroupTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(group);
		Group newGroup = groupDaoImpl.getGroup("groupName");
		assertEquals(group.getCreatedBy(), newGroup.getCreatedBy());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getGroup_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Group newGroup = groupDaoImpl.getGroup("groupName");
		assertEquals(null, newGroup);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getGroup_whenHibernateExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		Group newGroup = groupDaoImpl.getGroup("groupName");
		assertEquals(null, newGroup);
	}

	@Test
	public void getUserGroupsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listGroups);
		List<Group> groupList = groupDaoImpl.getUserGroups(1, "keyword", 1, 2, "orderBy", "orderIn");
		assertEquals(2, groupList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserGroups_whenHibernateExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		List<Group> groupList = groupDaoImpl.getUserGroups(1, "keyword", 1, 2, "orderBy", "orderIn");
		assertEquals(0, groupList.size());
	}

	@Test
	public void getGroupsOfUserTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listGroups);
		List<Group> groupList = groupDaoImpl.getGroupsOfUser(1L, 2, 5, "orderBy", "orderIn");
		assertEquals(2, groupList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGroupsOfUser_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		groupDaoImpl.getGroupsOfUser(1L, 2, 5, "orderBy", "orderIn");
	}

	@Test
	public void countUserGroupsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listGroups);
		long result = groupDaoImpl.countUserGroups(1, "keyword");
		assertEquals(2L, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countUserGroups_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		long result = groupDaoImpl.countUserGroups(1, "keyword");
		assertEquals(0L, result);
	}
	
	@Test
	public void countGroupsOfUserTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listGroups);
		long result = groupDaoImpl.countGroupsOfUser(2L);
		assertEquals(2L, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countGroupsOfUser_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		groupDaoImpl.countGroupsOfUser(2L);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void checkGroupExistTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Group.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(group);
		Group newGroup = groupDaoImpl.checkGroupExist("groupName", 2L);
		assertEquals(new Long(2L), newGroup.getCreatedBy());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void checkGroupExist_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Group.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Group newGroup = groupDaoImpl.checkGroupExist("groupName", 2L);
		assertEquals(null, newGroup);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void checkGroupExist_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Group.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		groupDaoImpl.checkGroupExist("groupName", 2L);
	}
	
	@Test
	public void doesUserbelongtoGroupTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(userGroupDto);
		Boolean result = groupDaoImpl.doesUserbelongtoGroup("groupName", 2L);
		assertEquals(true, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void doesUserbelongtoGroup_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Boolean result = groupDaoImpl.doesUserbelongtoGroup("groupName", 2L);
		assertEquals(false, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class )
	public void doesUserbelongtoGroup_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		groupDaoImpl.doesUserbelongtoGroup("groupName", 2L);
	}
}
