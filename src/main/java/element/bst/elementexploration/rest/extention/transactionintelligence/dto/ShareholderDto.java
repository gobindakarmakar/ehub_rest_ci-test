package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

public class ShareholderDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String shareHolderName;

	private Long customerId;

	private Integer NumberOfShares;

	public String getShareHolderName() {
		return shareHolderName;
	}

	public void setShareHolderName(String shareHolderName) {
		this.shareHolderName = shareHolderName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getNumberOfShares() {
		return NumberOfShares;
	}

	public void setNumberOfShares(Integer numberOfShares) {
		NumberOfShares = numberOfShares;
	}
}
