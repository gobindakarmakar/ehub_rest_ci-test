package element.bst.elementexploration.rest.extention.menuitem.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserMenuResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Id of the menu")
	private Long id;

	@ApiModelProperty(value = "Number of Menu Item Clicks")
	private Integer clicksCount;

	@ApiModelProperty(value = "Menu Item Size")
	private String menuItemSize;

	@ApiModelProperty(value = "Id of the user")
	private Long userId;

	@ApiModelProperty(value = "Module Group ID")
	private ModulesGroup moduleGroup;

	@ApiModelProperty(value = "Status of the module")
	private Boolean disabled = false;

	@ApiModelProperty(value = "Module ID")
	private Modules module;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getClicksCount() {
		return clicksCount;
	}

	public void setClicksCount(Integer clicksCount) {
		this.clicksCount = clicksCount;
	}

	public String getMenuItemSize() {
		return menuItemSize;
	}

	public void setMenuItemSize(String menuItemSize) {
		this.menuItemSize = menuItemSize;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ModulesGroup getModuleGroup() {
		return moduleGroup;
	}

	public void setModuleGroup(ModulesGroup moduleGroup) {
		this.moduleGroup = moduleGroup;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Modules getModule() {
		return module;
	}

	public void setModule(Modules module) {
		this.module = module;
	}

}
