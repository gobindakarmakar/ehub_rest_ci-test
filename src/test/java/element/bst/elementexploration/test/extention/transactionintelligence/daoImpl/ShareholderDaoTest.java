package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.ShareholderDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.util.FilterUtility;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ShareholderDaoTest {
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@Mock
	FilterUtility filterUtility;

	@Mock
	CustomerDetailsDao customerDeatilsDao;

	@InjectMocks
	ShareholderDaoImpl shareholderDao;

	List<Object[]> objectList = new ArrayList<Object[]>();

	CorporateStructureDto corporateStructureDto;

	List<CorporateStructureDto> corporateStructureDtoList = new ArrayList<CorporateStructureDto>();

	List<String> scenarios = new ArrayList<String>();

	List<Object[]> objectTestList = new ArrayList<Object[]>();

	List<CorporateStructureDto> corporateStructureDtoTestList = new ArrayList<CorporateStructureDto>();

	CorporateStructureDto corporateStructureDtoTest;

	FilterDto filterDtoTest;

	List<String> scenariosList = new ArrayList<String>();

	@Before
	public void setupMock() {
		corporateStructureDto = new CorporateStructureDto("test", 5000d, 5l);
		Object[] objectArray = new Object[3];
		objectArray[0] = "5000";
		objectArray[2] = "test";
		objectList.add(objectArray);
		Object[] objectArrayNew = new Object[3];
		objectArrayNew[0] = "5000";
		objectArrayNew[2] = "test";
		objectList.add(objectArrayNew);
		scenarios.add("Large reportable ammount wired");
		scenarios.add("Large reportable ammount wired");
		corporateStructureDtoList.add(corporateStructureDto);
		Object[] objectArrayTestNew = new Object[5];
		objectArrayTestNew[0] = "500";
		objectArrayTestNew[1] = "objectArrayNew";
		objectArrayTestNew[2] = "string";
		objectArrayTestNew[3] = "newString";
		objectArrayTestNew[4] = "newArrayString";
		Object[] objectArrayTest = new Object[5];
		objectArrayTest[0] = "500";
		objectArrayTest[1] = "objectArrayNew";
		objectArrayTest[2] = "string";
		objectArrayTest[3] = "newString";
		objectArrayTest[4] = "newArrayString";
		objectTestList.add(objectArrayTestNew);
		objectTestList.add(objectArrayTest);
		filterDtoTest = new FilterDto();
		filterDtoTest.setBankLocations("India");
		filterDtoTest.setBankName("HDFC");
		filterDtoTest.setActivityCategory("activityCategory");
		filterDtoTest.setAlertStatus("alertStatus");
		corporateStructureDtoTest = new CorporateStructureDto(new Date(), 100000.0);
		corporateStructureDtoTestList.add(corporateStructureDtoTest);
		scenariosList.add("large amount of wideTransaction");
		scenariosList.add("large amount of wideTransaction");
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCorporateStructureTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(corporateStructureDto);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		CorporateStructureDto corporateStructureDto = shareholderDao.getCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(new Double("10000"), corporateStructureDto.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCorporateStructure_whenNoResultExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		CorporateStructureDto corporateStructureDto = shareholderDao.getCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(null, corporateStructureDto);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorporateStructure_whenFailedToExecuteQueryExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCorporateStructure(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCorporateStructureAggregatesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructureAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "CorporateStructure");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCorporateStructureAggregates_whenTypeIsShareholderTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructureAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "Shareholder");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCorporateStructureAggregates_whenTypeIsGeographyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructureAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "Geography");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorporateStructureAggregates_whenFailedToExecuteQueryExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCorporateStructureAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "Geography");
	}

	@Test
	public void getCorporateStructuteByTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructuteByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorporateStructuteByType_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCorporateStructuteByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
	}

	@Test
	public void getAlertEntitiesGeographyTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getAlertEntitiesGeography(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertEntitiesGeography_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAlertEntitiesGeography(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder");
	}

	@Test
	public void getAlertEntitiesFilterTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		List<String> countries = new ArrayList<String>();
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getAlertEntitiesFilter(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", countries, "desc");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertEntitiesFilter_whenFailedToExecuteQueryExceptionThrrowTest() {
		List<String> countries = new ArrayList<String>();
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAlertEntitiesFilter(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", countries, "desc");
	}

	@Test
	public void getAlertEntitiesFilterBasesOnScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getAlertEntitiesFilter(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios, "", "germany", "name");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertEntitiesFilterBasesOnScenarios_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAlertEntitiesFilter(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios, "", "germany", "name");
	}

	@Test
	public void getAlertEntitiesFilterBasesOnCustomerIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getAlertEntitiesFilter(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios, "oldfilter", "germany",
				"newfilter", 123l, "name");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertEntitiesFilterBasesOnCustomerId_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAlertEntitiesFilter(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios, "oldfilter", "germany",
				"newfilter", 123l, "name");
	}

	@Test
	public void getCorporateByTypeTest() {
		List<String> typeList = new ArrayList<String>();
		typeList.add("Test");
		typeList.add("test");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(typeList);
		List<String> typeListNew = shareholderDao.getCorporateByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
		assertEquals(2, typeListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorporateByType_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCorporateByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
	}

	@Test
	public void getCorporateStructuteByScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructuteByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "CorporateStructure");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@Test
	public void getCorporateStructuteByShareholderScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructuteByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "Shareholder");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@Test
	public void getCorporateStructuteByGeographyScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCorporateStructuteByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "Geography");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorporateStructuteByScenarios_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCorporateStructuteByScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto(), "");
	}

	@Test
	public void getAlertEntitiesGeographyDtoTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(corporateStructureDtoTest).thenReturn(60000D);
		CorporateStructureDto corporateStructureDtoNew = shareholderDao.getAlertEntitiesGeography(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(new Double("60000"), corporateStructureDtoNew.getAlertAmount());

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertEntitiesGeographyDto_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		CorporateStructureDto corporateStructureDtoNew = shareholderDao.getAlertEntitiesGeography(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(null, corporateStructureDtoNew);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertEntitiesGeographyDto_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAlertEntitiesGeography(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
	}

	@Test
	public void getCountryAggregatesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCountryAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenarios);
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCountryAggregates_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCountryAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenarios);
	}

	@Test
	public void getCountryByTypeTest() {
		List<String> countryList = new ArrayList<String>();
		countryList.add("germany");
		countryList.add("UK");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(countryList);
		List<String> countryListNew = shareholderDao.getCountryByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
		assertEquals(2, countryListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCountryByType_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCountryByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
	}

	@Test
	public void getAlertEntitiesBusinessTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getAlertEntitiesBusinessType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertEntitiesBusinessType_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAlertEntitiesBusinessType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
	}

	@Test
	public void getBusinessAggregatesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getBusinessAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenarios);
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getBusinessAggregates_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getBusinessAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenarios);
	}

	@Test
	public void getBusinessByTypeTest() {
		List<String> businessList = new ArrayList<String>();
		businessList.add("Import&Export");
		businessList.add("Transport");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(businessList);
		List<String> countryListNew = shareholderDao.getBusinessByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
		assertEquals(2, countryListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getBusinessByType_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getBusinessByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholder", scenarios);
	}

	@Test
	public void getCustomerByCountryScenarioTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCustomerByCountryScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Germany", scenarios);
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerByCountryScenario_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCustomerByCountryScenario(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Germany", scenarios);
	}

	@Test
	public void getCustomerByBusinessScenarioTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCustomerByBusinessScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Transport", scenarios);
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerByBusinessScenario_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCustomerByBusinessScenario(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Transport", scenarios);
	}

	@Test
	public void getShareholderCorporateStructureTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getShareholderCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholderCorporateStructure_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholderCorporateStructure(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
	}

	@Test
	public void getShareholderCountryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getShareholderCountry(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholderCountry_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholderCountry(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
	}

	@Test
	public void getShareholderAggregatesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getShareholderAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholderAggregates_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholderAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
	}

	@Test
	public void getCustomerByShareholderScenarioTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> corporateStructureDtoListNew = shareholderDao.getCustomerByShareholderScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Germany", scenarios);
		assertEquals(1, corporateStructureDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerByShareholderScenario_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCustomerByShareholderScenario(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "germany", scenarios);
	}

	@Test
	public void getShareholderByTypeTest() {
		List<String> countryList = new ArrayList<String>();
		countryList.add("germany");
		countryList.add("UK");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(countryList);
		List<String> countryListNew = shareholderDao.getShareholderByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "shareholders", scenarios);
		assertEquals(2, countryListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholderByType_whenFailedToExecuteQueryExceptionThrrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholderByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "shareholders", scenarios);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAllCorporateStructureTest() throws ParseException {
		objectTestList.get(0)[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectTestList.get(0)[2] = "12345";
		objectTestList.get(1)[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectTestList.get(1)[2] = "12345";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAllCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "filter", 1, 10, filterDtoTest);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAllCorporateStructure_whenIsTransactionIsFalseTest1() throws ParseException {
		objectTestList.get(0)[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectTestList.get(0)[2] = "12345";
		objectTestList.get(1)[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectTestList.get(1)[2] = "12345";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAllCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "filter", 1, 10, filterDtoTest);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAllCorporateStructure_whenObjectArrayNotContainsStringIn1Test1() throws ParseException {
		objectTestList.get(0)[1] = "03-01-2019";
		objectTestList.get(0)[2] = "12345";
		objectTestList.get(1)[1] = "03-01-2019";
		objectTestList.get(1)[2] = "12345";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAllCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "filter", 1, 10, filterDtoTest);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAllCorporateStructure_whenObjectArrayNotContainsStringIn1AndTransFalseTest1()
			throws ParseException {
		objectTestList.get(0)[1] = "03-01-2019";
		objectTestList.get(0)[2] = "12345";
		objectTestList.get(1)[1] = "03-01-2019";
		objectTestList.get(1)[2] = "12345";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAllCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "filter", 1, 10, filterDtoTest);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCorporateStructure_whenExceptionThrowTest1() throws ParseException {
		objectTestList.get(0)[1] = "03-01-2019";
		objectTestList.get(0)[2] = "12345";
		objectTestList.get(1)[1] = "03-01-2019";
		objectTestList.get(1)[2] = "12345";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getViewAllCorporateStructure(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "filter", 1, 10, filterDtoTest);
	}

	@Test
	public void getViewAllTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAll(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10, "queryforCondition");
		assertEquals(1, list.size());
	}

	@Test
	public void getViewAll_whenIsTransactionIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAll(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, 1, 10, "queryforCondition");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAll_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getViewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, 1, 10, "queryforCondition");
	}

	@Test
	public void getViewAllCorporateStructureCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		long result = shareholderDao.getViewAllCorporateStructureCount(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true);
		assertEquals(1, result);
	}

	@Test
	public void getViewAllCorporateStructureCount_whenIsTransactionIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		long result = shareholderDao.getViewAllCorporateStructureCount(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false);
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCorporateStructureCount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getViewAllCorporateStructureCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false);
	}

	@Test
	public void getViewAllCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		long result = shareholderDao.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "queryforCondition");
		assertEquals(1, result);
	}

	@Test
	public void getViewAllCount_whenIsTransactionIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		long result = shareholderDao.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "queryforCondition");
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		long result = shareholderDao.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "queryforCondition");
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAssociatedAlertsTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getAssociatedAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(2, list.size());

	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAssociatedAlerts_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAssociatedAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);

	}

	@Test
	public void getViewAllCorporateStructureWithQueryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAllCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "filter", "queryforCondition", 1, 10);
		assertEquals(1, list.size());
	}

	@Test
	public void getViewAllCorporateStructure_whenIsTransIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getViewAllCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "filter", "queryforCondition", 1, 10);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCorporateStructure_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getViewAllCorporateStructure(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "filter", "queryforCondition", 1, 10);
	}

	@Test
	public void getTopFiveAlertsTest() {
		objectTestList.get(0)[1] = "78459";
		objectTestList.get(0)[2] = "54321";
		objectTestList.get(1)[1] = "78459";
		objectTestList.get(1)[2] = "54321";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.setMaxResults(Mockito.anyInt())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getTopFiveAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "CorporateStructure");
		assertEquals(2, list.size());
	}

	@Test
	public void getTopFiveAlerts_whenTypeIsShareholdersTest() {
		objectTestList.get(0)[1] = "78459";
		objectTestList.get(0)[2] = "54321";
		objectTestList.get(1)[1] = "78459";
		objectTestList.get(1)[2] = "54321";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.setMaxResults(Mockito.anyInt())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getTopFiveAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Shareholders");
		assertEquals(2, list.size());
	}

	@Test
	public void getTopFiveAlerts_whenTypeIsGeographyTest() {
		objectTestList.get(0)[1] = "78459";
		objectTestList.get(0)[2] = "54321";
		objectTestList.get(1)[1] = "78459";
		objectTestList.get(1)[2] = "54321";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.setMaxResults(Mockito.anyInt())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getTopFiveAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Geography");
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getTopFiveAlerts_whenExceptionThrowTest() {
		objectTestList.get(0)[1] = "78459";
		objectTestList.get(0)[2] = "54321";
		objectTestList.get(1)[1] = "78459";
		objectTestList.get(1)[2] = "54321";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.setMaxResults(Mockito.anyInt())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getTopFiveAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Geography");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getGeographyAggregatesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		List<CorporateStructureDto> list = shareholderDao.getGeographyAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGeographyAggregates_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGeographyAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
	}

	@Test
	public void getCorporateStructureByTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(scenariosList);
		List<String> list = shareholderDao.getCorporateStructureByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type", scenariosList);
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorporateStructureByType_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getCorporateStructureByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type", scenariosList);
	}

	@Test
	public void getAggregatesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAggregates_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
	}

	@Test
	public void getGeographyByScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getGeographyByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenariosList, "type");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGeographyByScenarios_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGeographyByScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenariosList, "type");
	}

	@Test
	public void getShareholderTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(corporateStructureDto).thenReturn(corporateStructureDto)
				.thenReturn(60000D);
		CorporateStructureDto CorporateStructureDtoNew = shareholderDao.getShareholder(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "shareholders");
		assertEquals(new Double("60000"), CorporateStructureDtoNew.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholder_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholder(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "shareholders");
	}

	@Test
	public void getShareholderAggregateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getShareholderAggregate(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholderAggregate_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholderAggregate(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
	}

	@Test
	public void getGeographyTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(corporateStructureDto).thenReturn(corporateStructureDto)
				.thenReturn(60000D);
		CorporateStructureDto CorporateStructureDtoNew = shareholderDao.getGeography(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "shareholders");
		assertEquals(new Double("60000"), CorporateStructureDtoNew.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGeography_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGeography(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "shareholders");
	}

	@Test
	public void getGeographyAggregateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getGeographyAggregate(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGeographyAggregate_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGeographyAggregate(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
	}

	@Test
	public void getGeographyByTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(scenariosList);
		List<String> list = shareholderDao.getGeographyByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type", scenariosList);
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGeographyByType_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGeographyByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type", scenariosList);
	}

	@Test
	public void getShareholderByScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getShareholderByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenariosList, "type");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareholderByScenarios_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareholderByScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenariosList, "type");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getGroupByScenarioTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoTestList);
		List<CorporateStructureDto> list = shareholderDao.getGroupByScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGroupByScenario_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGroupByScenario(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getShareHolderTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(new CorporateStructureDto());
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		CorporateStructureDto result = shareholderDao.getShareHolder(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(new Double("1000.0"), result.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getShareHolder_whenNoResultExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(new CorporateStructureDto());
		Mockito.when(query.getResultList()).thenThrow(NoResultException.class);
		CorporateStructureDto result = shareholderDao.getShareHolder(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(null, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getShareHolder_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(new CorporateStructureDto());
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getShareHolder(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getGeographyAlertsTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(new CorporateStructureDto());
		Mockito.when(query.getResultList()).thenReturn(objectTestList);
		CorporateStructureDto result = shareholderDao.getGeographyAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(new Double("1000.0"), result.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getGeographyAlerts_whenNoResultExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(new CorporateStructureDto());
		Mockito.when(query.getResultList()).thenThrow(NoResultException.class);
		CorporateStructureDto result = shareholderDao.getGeographyAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
		assertEquals(null, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGeographyAlerts_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(new CorporateStructureDto());
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		shareholderDao.getGeographyAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDtoTest);
	}

}
