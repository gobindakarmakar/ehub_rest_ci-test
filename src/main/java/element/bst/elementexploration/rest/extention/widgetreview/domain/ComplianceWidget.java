package element.bst.elementexploration.rest.extention.widgetreview.domain;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Rambabu
 *
 */
@ApiModel("widget")
@Entity
@Table(name = "compliance_widget")
@Access(AccessType.FIELD)
public class ComplianceWidget implements Serializable {

	private static final long serialVersionUID = 2286808560623928059L;

	@ApiModelProperty(value = "The ID of entity section")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "widget_id")
	private Long id;

	@ApiModelProperty(value = "name of the widget", required = true)
	@Column(name = "widget_name")
	@NotEmpty(message = "Widget name can not be blank.")
	private String widgetName;

	public ComplianceWidget() {
		super();
	}

	public ComplianceWidget(String widgetName) {
		super();
		this.widgetName = widgetName;
	}

	public ComplianceWidget(Long id, String widgetName) {
		super();
		this.id = id;
		this.widgetName = widgetName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}