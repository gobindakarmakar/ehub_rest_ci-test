package element.bst.elementexploration.rest.extention.widgetreview.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.widgetreview.dao.ComplianceWidgetDao;
import element.bst.elementexploration.rest.extention.widgetreview.dao.WidgetReviewDao;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.extention.widgetreview.dto.WidgetReviewDto;
import element.bst.elementexploration.rest.extention.widgetreview.service.WidgetReviewService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author rambabu
 *
 */
@Service("widgetReviewService")
public class WidgetReviewServiceImpl extends GenericServiceImpl<WidgetReview, Long> implements WidgetReviewService {

	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	
	@Autowired
	WidgetReviewDao widgetReviewDao;

	@Autowired
	ComplianceWidgetDao complianceWidgetDao;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	public WidgetReviewServiceImpl(GenericDao<WidgetReview, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public boolean saveWidgetReviews(List<WidgetReviewDto> widdgetReviewDtoList, Long userId) {
		boolean oldStatus;
		Users user = null;
		if (userId != null) {
			user = usersDao.find(userId);
			user = usersService.prepareUserFromFirebase(user);
		}
		if (user != null) {
			for (WidgetReviewDto widgetReviewDto : widdgetReviewDtoList) {
				if (widgetReviewDto.getId() != null) {
					WidgetReview existedWidgetReview = widgetReviewDao.find(widgetReviewDto.getId());
					if (existedWidgetReview != null) {
						if (widgetReviewDto.getIsApproved() != existedWidgetReview.getIsApproved()) {
							oldStatus = existedWidgetReview.getIsApproved();
							existedWidgetReview.setUser(user);
							existedWidgetReview.setApprovedDate(new Date());
							existedWidgetReview.setIsApproved(widgetReviewDto.getIsApproved());
							widgetReviewDao.saveOrUpdate(existedWidgetReview);
							eventPublisher.publishEvent(new LoggingEvent(existedWidgetReview.getId(),
									Boolean.toString(oldStatus), Boolean.toString(widgetReviewDto.getIsApproved()),
									LoggingEventType.UPDATE_COMPLIANCE_WIDGET_REVIEW, userId, new Date()));
						}
					} else
						throw new InsufficientDataException("widget review not found");
				} else {
					WidgetReview checkWidget = widgetReviewDao.getWidgetWithEntityIdAndWidgetId(
							widgetReviewDto.getEntityId(), widgetReviewDto.getWidgetId());
					if (checkWidget != null) {
						if (widgetReviewDto.getIsApproved() != checkWidget.getIsApproved()) {
							oldStatus = checkWidget.getIsApproved();
							checkWidget.setUser(user);
							checkWidget.setApprovedDate(new Date());
							checkWidget.setIsApproved(widgetReviewDto.getIsApproved());
							widgetReviewDao.saveOrUpdate(checkWidget);
							eventPublisher.publishEvent(new LoggingEvent(checkWidget.getId(),
									Boolean.toString(oldStatus), Boolean.toString(widgetReviewDto.getIsApproved()),
									LoggingEventType.UPDATE_COMPLIANCE_WIDGET_REVIEW, userId, new Date()));
						}
					} else {
						WidgetReview newWidgetReview = new WidgetReview();
						newWidgetReview.setEntityId(widgetReviewDto.getEntityId());
						newWidgetReview.setIsApproved(widgetReviewDto.getIsApproved());
						newWidgetReview.setUser(user);
						newWidgetReview.setApprovedDate(new Date());
						ComplianceWidget complianceWidget = complianceWidgetDao.find(widgetReviewDto.getWidgetId());
						if (complianceWidget == null) {
							throw new InsufficientDataException("widget not found");
						} else {
							newWidgetReview.setWidget(complianceWidget);
						}
						WidgetReview widgetReviewNew = widgetReviewDao.create(newWidgetReview);
						eventPublisher.publishEvent(new LoggingEvent(widgetReviewNew.getId(),
								LoggingEventType.CREATE_COMPLIANCE_WIDGET_REVIEW, userId, new Date()));

					}
				}
			}
			return true;
		} else
			throw new InsufficientDataException("User not found");
	}

	@Override
	@Transactional("transactionManager")
	public List<WidgetReviewDto> getWidgetReviewsByEntityId(String entityId) {
		List<WidgetReviewDto> widgetReviewDtoList = new ArrayList<WidgetReviewDto>();
		List<WidgetReview> widgetReviewList = widgetReviewDao.getWidgetReviewsByEntityId(entityId);
		for (WidgetReview widgetReview : widgetReviewList) {
			WidgetReviewDto widgetReviewDto = new WidgetReviewDto();
			BeanUtils.copyProperties(widgetReview, widgetReviewDto);
			widgetReviewDto.setUserName(widgetReview.getUser().getScreenName());
			widgetReviewDto.setWidgetId(widgetReview.getWidget().getId());
			widgetReviewDto.setWidgetName(widgetReview.getWidget().getWidgetName());
			widgetReviewDtoList.add(widgetReviewDto);
		}
		return widgetReviewDtoList;
	}

}
