package element.bst.elementexploration.rest.extention.workflow.service;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;

public interface WorkflowService {

	String getProfile(String profileId) throws Exception;

	String getIndependentStage(String stageId) throws Exception;

	String getStageByWorkflow(String workflowId, String stageId) throws Exception;

	String getWorkflow(String workflowId) throws Exception;

	String deleteWorkflowStates() throws Exception;

	String getWorkflowStates() throws Exception;

	String deleteWorkflowStatesById(String workflowId) throws Exception;

	String getWorkflowStatesById(String workflowId) throws Exception;

	String deleteWorkflowStatesByIdAndNominaltime(String workflowId, String nominalTime) throws Exception;

	String getWorkflowStatesByIdAndNominalTime(String workflowId, String nominalTime) throws Exception;

	String insertWorkflowStates(String workflowId, String nominalTime, String jsonString) throws Exception;

	String deletePublishedModels() throws Exception;

	String getPublishedModels() throws Exception;

	String createPublishedModels(String jsonToBeSent) throws Exception;

	String updatePublishedModels(String jsonToBeSent) throws Exception;

	String deletePublishedModelsInWorkflow(String workflowId) throws Exception;

	String getPublishedModelsById(String workflowId) throws Exception;

	String deletePublishedModelsInWorkflowByModelId(String workflowId, String modelId) throws Exception;

	String getPublishedModelsInWorkflowByModelId(String workflowId, String modelId) throws Exception;

	String deletePublishedModelsInWorkflowByVersion(String workflowId, String modelId, String version) throws Exception;

	String getPublishedModelsInWorkflowByVersion(String workflowId, String modelId, String version) throws Exception;

	String deleteStagingModels() throws Exception;

	String getStagingModels() throws Exception;

	String createStagingModels(String jsonToBeSent) throws Exception;

	String updateStagingModels(String jsonToBeSent) throws Exception;

	String getStagingToPublishModelsByVersion(String workflowId, String modelId, String version) throws Exception;

	String deleteStagingModelsById(String workflowId) throws Exception;

	String getStagingModelsById(String workflowId) throws Exception;

	String deleteStagingModelsByIdAndModelId(String workflowId, String modelId) throws Exception;

	String getStagingModelsByIdAndModelId(String workflowId, String modelId) throws Exception;

	String deleteStagingModelsByVersion(String workflowId, String modelId, String version) throws Exception;

	String getStagingModelsByVersion(String workflowId, String modelId, String version) throws Exception;

	String deleteTags() throws Exception;

	String getTags() throws Exception;

	String createTags(String jsonToBeSent) throws Exception;

	String updateTags(String jsonToBeSent) throws Exception;

	String deleteTagById(String tagId) throws Exception;

	String getTagById(String tagId) throws Exception;

	String getGraphcache() throws Exception;

	String deleteGraphcache() throws Exception;

	String deleteGraphcacheByQuery(String query) throws Exception;

	String getGraphcacheByQuery(String query) throws Exception;

	String createGraphcache(String query, String caseId, String jsonToBeSent) throws Exception;

	String deleteCases() throws Exception;

	String getCases() throws Exception;

	String createCase(String jsonToBeSent) throws Exception;

	String updateCase(String jsonToBeSent) throws Exception;

	String deleteCaseById(String caseId) throws Exception;

	String getCaseById(String caseId) throws Exception;

	String clearDatabase() throws Exception;

	String deleteIndex(String indexName) throws Exception;

	String deleteContingencyTable() throws Exception;

	String getContingencyTable() throws Exception;

	String createContingencyTable(String jsonToBeSent, String contingencytableId) throws Exception;

	String updateContingencyTable(String jsonToBeSent, String contingencytableId) throws Exception;

	String deleteContingencyTableById(String contingencytableId) throws Exception;

	String getContingencyTableById(String contingencytableId) throws Exception;

	String listOfInices(String profileId) throws Exception;

	String createIndex(String jsonToBeSent, String profileId) throws Exception;

	String deleteIndexByIdAndIndexName(String profileId, String indexName) throws Exception;

	String listOfTypes(String profileId, String indexName) throws Exception;

	String deleteAllWorkflowStats() throws Exception;

	String getAllWorkflowStats() throws Exception;

	String deleteWorkflowStatsById(String workflowId) throws Exception;

	String getWorkflowStatsById(String workflowId) throws Exception;

	String deleteWorkflowStatsByIdAndStageId(String workflowId, String stageId) throws Exception;

	String getWorkflowStatsByIdAndStageId(String workflowId, String stageId) throws Exception;

	String deleteWorkflowStatsByIdAndStageIdAndTime(String workflowId, String stageId, String nominalTime)
			throws Exception;

	String getWorkflowStatsByIdAndStageIdAndTime(String workflowId, String stageId, String nominalTime)
			throws Exception;

	String createWorkflowStats(String workflowId, String stageId, String nominalTime, String jsonToBeSent)
			throws Exception;

	String deleteAllStagesInWorkflow(String workflowId) throws Exception;

	String getAllStagesInWorkflow(String workflowId) throws Exception;

	String updateStageInWorkflow(String workflowId, String jsonToBeSent) throws Exception;

	String createStageInWorkflow(String workflowId, String jsonToBeSent) throws Exception;

	String deleteAllStagesInWorkflowByStageId(String workflowId, String stageId) throws Exception;

	String getAllStagesInWorkflowByStageId(String workflowId, String stageId) throws Exception;

	String getPyspark(String type) throws Exception;

	String getAllWorkflows() throws Exception;

	String createWorkflow(String jsonToBeSent) throws Exception;

	String updateWorkflow(String jsonToBeSent) throws Exception;

	String parmeterizedStrtWorkflow(String jsonToBeSent) throws Exception;

	String setWorkflowStatus(String workflowId, String refreshInterval, String status) throws Exception;

	String deleteWorkflow(String workflowId) throws Exception;

	String getworkflowByWorkflowId(String workflowId) throws Exception;

	String startWorkflow(String workflowId) throws Exception;

	String startWorkflowWithResource(String workflowId, String mermory, String cores) throws Exception;

	String getWorkflowStatus(String workflowId) throws Exception;

	String stopWorkflow(String workflowId) throws Exception;

	String deleteAllSamples(String workflowId) throws Exception;

	String getAllSamples(String workflowId) throws Exception;

	String deleteAllSamplesByStageId(String workflowId, String stageId) throws Exception;

	String getAllSamplesByStageId(String workflowId, String stageId) throws Exception;

	String insertSamples(String workflowId, String stageId, String jsonToBeSent) throws Exception;

	String deleteProfile() throws Exception;

	String getProfiles() throws Exception;

	String createProfile(String jsonToBeSent) throws Exception;

	String updateProfile(String jsonToBeSent) throws Exception;

	String deleteProfileById(String profileId) throws Exception;

	String getProfileById(String profileId) throws Exception;

	String getSampleDataOfEs(String workflowId, String stageId) throws Exception;

	String getSchemaDataOfEs(String workflowId, String stageId) throws Exception;

	String getCaseAlerts(String caseId) throws Exception;

	String createCaseAlert(String jsonToBeSent, String caseId) throws Exception;

	String getLatestCaseAlerts(String caseId, String count) throws Exception;

	String getCaseTimeline(String caseId) throws Exception;

	String createCaseTimeline(String caseId, String jsonToBeSent) throws Exception;

	String getCasesSummary() throws Exception;

	String createCasesSummary(String jsonToBeSent) throws Exception;

	String getCasesSummaryWithLimit(String limit) throws Exception;

	String getCasesSummaryWithcaseId(String caseId) throws Exception;

	String createCasesSummaryWithcaseId(String caseId, String jsonToBeSent) throws Exception;

	String getSimilarCases(String caseId, Integer limit) throws Exception;

	String getCaseTimelineWithLimit(String caseId, Integer limit) throws Exception;

	String getCaseTimelineFoundedWithLimit(String caseId, Integer limit) throws Exception;

	String getCasesSumary(Long userId, CaseVo caseVo, Integer limit) throws Exception;

	String getDatabasesByProfileId(String profileId) throws Exception;

	String getDatabaseTablesByProfileId(String profileId, String database) throws Exception;

	String assignRiskScoreToGraph(String jsonString) throws Exception;

	String createCassandraDatabase(String jsonString) throws Exception;

	String createCassandraTable(String jsonString) throws Exception;

	String getCassandraDatabasesList(String profileId) throws Exception;

	String getSampleCassandraData(String workflowId, String profileId) throws Exception;

	String getSampleCassandraSchema(String workflowId, String stageId) throws Exception;

	String getListCassandraDatabaseTables(String database, String profileId) throws Exception;

	String createNotebookForWorkflow(String jsonString) throws Exception;

	String getAllNotebooksByWorkflowId(String workflowId) throws Exception;

	String getNotebookById(String id) throws Exception;

	String deleteNotebook(String workflowId, String notebookName) throws Exception;

	String getNotebookByWorkFlowIdAndNotebookName(String workflowId, String notebookName)throws Exception;

	String runParentStagesForAStage(String workflowId, String stageId)throws Exception;

	String updateNotebook(String workflowId, String notebookName, String jsonToBeSent)throws Exception;

	String deleteFileByPath(String path)throws Exception;

	String getHdfsProfileForFileSource()throws Exception;

	String saveSourceFile(String stageId, String workflowId, MultipartFile uploadFile)throws Exception;

	String ListWorkFlowCasesWithData()throws Exception;

	String deleteWorkflowCaseById(String workflowId)throws Exception;

	String getWorkFlowCaseById(String workflowId)throws Exception;

	String saveWorkflowCase(String workflowId, String jsonString)throws Exception;

	String updateWorkflowCase(String workflowId, String jsonToBeSent)throws Exception;

}
