package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentISOCodeDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentISOCodes;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("documentISOCodeDao")
public class DocumentISOCodeDaoImpl extends GenericDaoImpl<DocumentISOCodes, Long> implements DocumentISOCodeDao {

	public DocumentISOCodeDaoImpl() {
		super(DocumentISOCodes.class);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public DocumentISOCodes fetchDocumnetIsoCode(String question,boolean isQuestion)
	{
		
		DocumentISOCodes documentIsocodes = null;
		try 
		{
			StringBuilder hql = new StringBuilder();
			//hql.append("select cd from DocumentISOCodes cd where cd.question LIKE '%" + question + "%' ");
			hql.append("select cd from DocumentISOCodes cd ");
			if(isQuestion)
				hql.append("where cd.question=:question ");
			else
				hql.append("where cd.normalizedQuestion=:question ");
			Query query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("question", question);
			documentIsocodes = (DocumentISOCodes) query.getSingleResult();
		} 
		catch (NoResultException e)
		{
			return documentIsocodes;
		} 
		catch (Exception e) 
		{
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return documentIsocodes;
		
	}


}
