package element.bst.elementexploration.rest.usermanagement.user.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.hibernate.HibernateException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Amit Patel
 *
 */
@Service("userService")
public class UserServiceImpl extends GenericServiceImpl<User, Long> implements UserService {

	@Autowired
	public UserServiceImpl(GenericDao<User, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private UserDao userDao;

	@Value("${enable}")
	private String USER_REG_ENABLE;

	@Value("${status}")
	private String USER_STATUS;

	@Value("${emailVerification}")
	private String USER_EMAIL_VERIFICATION;

	@Override
	@Transactional("transactionManager")
	public void setUserLastLoginDateAndIp(UserDto userDto, String ip) {
		User user = userDao.find(userDto.getUserId());
		user.setLastLoginDate(new Date());
		user.setLastLoginIP(ip);
		user.setTemporaryPassword(null);
		user.setTemporaryPasswordExpiresOn(null);
		userDao.saveOrUpdate(user);
	}

	public Properties getProperties() {
		Properties properties = new Properties();

		if (USER_REG_ENABLE.isEmpty()) {
			USER_REG_ENABLE = "0";
		}
		if (USER_STATUS.isEmpty()) {
			USER_STATUS = "0";
		}
		if (USER_EMAIL_VERIFICATION.isEmpty()) {
			USER_EMAIL_VERIFICATION = "0";
		}
		properties.setProperty("enable", USER_REG_ENABLE);

		properties.setProperty("status", USER_STATUS);
		properties.setProperty("emailVerification", USER_EMAIL_VERIFICATION);
		return properties;
	}

	@Override
	public void sendEmailVerification(Long userId, String userName, String url, String token, String emailAddress) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpPost post = new HttpPost(url + "/notification/rest/api/notification/send?userId=" + userId);
			StringEntity params;
			params = new StringEntity("{ \"media\":\"EMAIL\", \"metadata\":{ \"fromUser\" : { \"id\" : \"" + userId
					+ "\", " + "\"emailAddress\" : \"test@gmail.com\" }, \"subject\" : \"EHUB User Token\", "
					+ "\"body\" : \"Hello " + userName
					+ ", Thank for registering EHUB. Your account needs to be activated. "
					+ "To activate your account copy the token below and paste it to " + url
					+ "elementexploration/validateToken. " + token + " \", "
					+ "\"priority\" : \"HIGH\", \"level\" : \"WARNING\", \"requestAck\" : true, \"toUserList\" : [ {\"id\" : \""
					+ userId + "\"," + "\"emailAddress\" :\"" + emailAddress
					+ "\"} ] }, \"tags\":\"mytags\", \"token\":\"" + token + "\" }");

			post.addHeader("content-type", "application/json");
			post.setEntity(params);
			httpClient.execute(post);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Unable to send token registration", e, this.getClass());
		}
	}

	@Override
	@Transactional("transactionManager")
	public boolean activateUser(User user) {
		user.setStatus("1");
		try {
			userDao.saveOrUpdate(user);
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Error activating user ", e, this.getClass());
			throw e;
		}
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deactivateUser(User user) {
		user.setStatus("0");
		try {
			userDao.saveOrUpdate(user);
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Error activating user ", e, this.getClass());
			throw e;
		}
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public List<UserDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		return userDao.getUserList(groupId, status,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public List<UserDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn) {
		return userDao.getUserListFullTextSearch(keyword, status, groupId,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public boolean isActiveUser(Long userId) {
		boolean retVal = true;

		User user = userDao.find(userId);

		if (null != user) {
			if (null != user.getStatus()) {
				if (user.getStatus().equalsIgnoreCase("0")) {
					retVal = false;
				}
			} else {
				retVal = false;
			}
		}

		return retVal;
	}

	@Override
	@Transactional("transactionManager")
	public List<UserDto> listUserByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {

		pageNumber = (pageNumber == null) ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber;
		recordsPerPage = (recordsPerPage == null) ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;

		return userDao.listUsersByGroup(groupId, userStatus, pageNumber, recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public UserDto getUserByEmailOrUsername(String email) {
		UserDto userDto = null;
		if (email != null) {
			User user = userDao.getUserByEmailOrUsername(email);
			if (user != null) {
				userDto = new UserDto();
				BeanUtils.copyProperties(user, userDto);
			}
		}
		return userDto;
	}

	@Override
	@Transactional("transactionManager")
	public User getUserObjectByEmailOrUsername(String email) {
		User user = null;
		if (email != null) {
			user = userDao.getUserByEmailOrUsername(email);
		}
		return user;
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserList(Long groupId, String status) {
		return userDao.countUserList(groupId, status);
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserListFullTextSearch(String keyword, String status, Long groupId) {
		return userDao.countUserListFullTextSearch(keyword, status, groupId);
	}

	@Override
	@Transactional("transactionManager")
	public UserDto checkUserNameOrEmailExists(String email, Long id) {
		UserDto userDto = null;
		if (email != null) {
			User user = userDao.checkUserNameOrEmailExists(email, id);
			if (user != null) {
				userDto = new UserDto();
				BeanUtils.copyProperties(user, userDto);
			}
		}
		return userDto;
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserByGroup(Long groupId, Integer userStatus) {
		return userDao.countUserByGroup(groupId, userStatus);
	}

}