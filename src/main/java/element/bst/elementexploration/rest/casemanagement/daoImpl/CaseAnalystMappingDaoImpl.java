package element.bst.elementexploration.rest.casemanagement.daoImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.dao.CaseAnalystMappingDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("caseAnalystMappingDao")
public class CaseAnalystMappingDaoImpl extends GenericDaoImpl<CaseAnalystMapping, Long>
		implements CaseAnalystMappingDao {

	public CaseAnalystMappingDaoImpl() {
		super(CaseAnalystMapping.class);
	}

	@Autowired
	private CaseDao caseSeedDao;

	@Override
	public void createMapping(CaseAnalystMapping analystMapping) {
		try {
			this.getCurrentSession().save(analystMapping);
			Case caseseed = caseSeedDao.find(analystMapping.getCaseSeed().getCaseId());
			caseseed.setCurrentStatus(analystMapping.getCurrentStatus());
			caseSeedDao.update(caseseed);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@Override
	public CaseAnalystMapping getAnalystMappingBasedOnUserId(Long seedId, Long userId, int status) {
		CaseAnalystMapping analystMapping = null;
		try {

			String hql = "FROM CaseAnalystMapping as bcsam WHERE bcsam.caseSeed.caseId = :seedId and bcsam.user.userId = :userId and bcsam.modifiedOn is null and bcsam.currentStatus  = :status";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("seedId", seedId);
			query.setParameter("userId", userId);
			query.setParameter("status", status);
			analystMapping = (CaseAnalystMapping) query.getSingleResult();
			return analystMapping;
		} catch (NoResultException e) {
			return null;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
		}
		return analystMapping;
	}

	@Override
	public CaseAnalystMapping getCaseInStatusList(Long seedId, Long userId, List<Integer> statuses){
		CaseAnalystMapping analystMapping = null;
		try {
			String hql = "FROM CaseAnalystMapping csmap where csmap.caseSeed.caseId = :seedId and csmap.user.userId = :userId and csmap.currentStatus in (:statuses) and csmap.modifiedOn is null)";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("seedId", seedId);
			query.setParameter("userId", userId);
			query.setParameterList("statuses", statuses);
			analystMapping = (CaseAnalystMapping) query.getSingleResult();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
		}

		return analystMapping;
	}

	@Override
	public boolean deleteCaseFromFocusById(Long seedId, Long userId) {
		try {
			String hql = "DELETE FROM CaseAnalystMapping WHERE currentStatus = :status and caseSeed.caseId = :seedId and user.userId = :userId";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("seedId", seedId);
			query.setParameter("userId", userId);
			query.setParameter("status", StatusEnum.FOCUS.getIndex());
			int result = query.executeUpdate();
			if (result > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> getAllCasesInFocusById(Long userId, String orderBy, String orderIn, Integer pageNumber,
			Integer recordsPerPage) {
		List<CaseDto> caseList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(bcs.caseId, bcs.name, ")
					.append("bcs.description, bcs.remarks,bcs.priority, bcs.currentStatus, bcs.health, bcs.directRisk, bcs.indirectRisk, ")
					.append("bcs.transactionalRisk, bcs.createdOn, bcs.createdBy.userId, bcs.modifiedOn, bcs.modifiedBy, bcs.type) ")
					.append("from CaseAnalystMapping csmap, Case bcs ")
					.append(" where csmap.caseSeed.caseId =  bcs.caseId and ")
					.append(" csmap.user.userId = :userId ").append(" and csmap.currentStatus = :status and csmap.modifiedOn is null ");

			if (orderBy != null && !orderBy.isEmpty()) {
				queryBuilder.append(" order by ");
				if (resolveCaseColumnName(orderBy).equalsIgnoreCase("createdOn"))
					queryBuilder.append("csmap.");
				else
					queryBuilder.append("bcs.");
				queryBuilder.append(resolveCaseColumnName(orderBy));
				if (orderIn != null && !orderIn.isEmpty()) {
					if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
						queryBuilder.append(" ");
						queryBuilder.append(orderIn);
					}
				}
			}

			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("userId", userId);
			query.setParameter("status", StatusEnum.FOCUS.getIndex());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseList = (List<CaseDto>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);

		}
		return caseList;
	}

	private static String resolveCaseColumnName(String userColumn) {
		if (userColumn == null)
			return "createdOn";

		switch (userColumn.toLowerCase()) {
		case "name":
			return "name";
		case "priority":
			return "priority";
		case "health":
			return "health";
		case "currentstatus":
			return "currentStatus";
		case "directrisk":
			return "directRisk";
		case "indirectrisk":
			return "indirectRisk";
		case "transactionalrisk":
			return "transactionalRisk";
		default:
			return null;

		}
	}

	@Override
	public long countAllCasesInFocusById(Long userId) {
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select count(*) from CaseAnalystMapping csmap ")
					.append(" where csmap.user.userId = :userId ").append(" and csmap.currentStatus = :status and csmap.modifiedOn is null");
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("userId", userId);
			query.setParameter("status", StatusEnum.FOCUS.getIndex());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> fullTextSearchForCasesInFocus(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, Date creationDate, String orderBy, String orderIn) throws ParseException {
		List<CaseDto> caseSeedList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");

		try {
			StringBuilder builder = new StringBuilder();
			builder.append(
					"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(cs.caseId, cs.name, ")
					.append("cs.description, cs.remarks,cs.priority, cs.currentStatus, cs.health, cs.directRisk, cs.indirectRisk, ")
					.append("cs.transactionalRisk, cs.createdOn, cs.createdBy.userId, cs.modifiedOn, cs.modifiedBy, cs.type) from Case cs, CaseAnalystMapping csmap")
					.append(" where (cs.description LIKE '%").append(caseSearchKeyword)
					.append("%' OR cs.remarks LIKE '%").append(caseSearchKeyword).append("%' OR cs.name LIKE '%")
					.append(caseSearchKeyword).append("%'")
					.append(") and cs.caseId = csmap.caseSeed.caseId and ((csmap.currentStatus= :focus) and (csmap.modifiedOn is null)) and csmap.user.userId= :userId");

			if (creationDate != null) {
				builder.append(" and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");
			} 
			if (orderBy != null && !orderBy.isEmpty()) {
				builder.append(" order by ");
				if (resolveCaseColumnName(orderBy).equalsIgnoreCase("createdOn"))
					builder.append("csmap.");
				else
					builder.append("cs.");
				builder.append(resolveCaseColumnName(orderBy));
				if (orderIn != null && !orderIn.isEmpty()) {
					if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
						builder.append(" ");
						builder.append(orderIn);
					}
				}
			}

			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("userId", userId);
			query.setParameter("focus", StatusEnum.FOCUS.getIndex());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			caseSeedList = (List<CaseDto>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return caseSeedList;
	}

	@Override
	public long countfullTextSearchForCaseInFocus(Long userId, String caseSearchKeyword, Date creationDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("select count(*) from Case cs, CaseAnalystMapping csmap")
					.append(" where (cs.description LIKE '%").append(caseSearchKeyword)
					.append("%' OR cs.remarks LIKE '%").append(caseSearchKeyword).append("%' OR cs.name LIKE '%")
					.append(caseSearchKeyword).append("%'")
					.append(") and cs.caseId = csmap.caseSeed.caseId and ((csmap.currentStatus= :focus) and (csmap.modifiedOn is null)) and csmap.user.userId= :userId");

			if (creationDate != null) {
				builder.append(" and cs.createdOn >='");
				builder.append(sdf.format(creationDate));
				builder.append("'");
			}

			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("userId", userId);
			query.setParameter("focus", StatusEnum.FOCUS.getIndex());
			long count = ((Number)query.getSingleResult()).longValue();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@Override
	public CaseCommentNotificationDto getCaseCommentNotificationById(Long userId, Long caseId) {

		CaseCommentNotificationDto caseCommentNotification = null;
		try {
			String hql = "select new element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto(cm.statusComments,cm.notificationDate) from CaseAnalystMapping cm where cm.caseSeed.caseId = :caseId and cm.user.userId = :userId and cm.currentStatus <> :status and cm.modifiedOn is null";

			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("userId", userId);
			query.setParameter("caseId", caseId);
			query.setParameter("status", StatusEnum.FOCUS.getIndex());

			caseCommentNotification = (CaseCommentNotificationDto) query.getSingleResult();
			return caseCommentNotification;
		}catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseAnalystMapping> getAnalystMetrics(Date dateFrom, Date dateTo, String type, Boolean currentOnly,
			Long analystId) {
		List<CaseAnalystMapping> mappingList = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("select new CaseAnalystMapping(csmap.id, csmap.currentStatus, csmap.createdOn, csmap.modifiedOn, csmap.statusComments, csmap.notificationDate,"
					+ " csmap.caseSeed, csmap.user) from CaseAnalystMapping csmap, Case cs, User bam")
					.append(" where bam.userId = csmap.user.userId").append(" and bam.userId = :userId")
					.append(" and cs.caseId=csmap.caseSeed.caseId");
							
			if (type != null) {
				builder.append(" and cs.type = :type");
			}
			if(currentOnly != null){
				if(currentOnly)
					builder.append(" and csmap.modifiedOn is null");
			}
			if (dateFrom != null && dateTo == null) {
				builder.append(" and csmap.createdOn >='");
				builder.append(sdf.format(dateFrom));
				builder.append("'");
			} else if (dateTo != null && dateFrom == null) {
				builder.append(" and csmap.createdOn <'");
				builder.append(sdf.format(dateTo));
				builder.append("'");
			} else if(dateFrom != null && dateTo != null){
				builder.append(" and csmap.createdOn BETWEEN ");
				builder.append("'" + sdf.format(dateFrom) + "'");
				builder.append(" and '" + sdf.format(dateTo) + "'");
			}
			Query<?> query = getCurrentSession().createQuery(builder.toString());
			query.setParameter("userId", analystId);
			if (type != null)
				query.setParameter("type", type);
			mappingList = (List<CaseAnalystMapping>) query.getResultList();
		} catch (Exception ex) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, ex,
					CaseDaoImpl.class);
		}
		return mappingList;
	}

	@Override
	public CaseAnalystMapping findByCaseId(Long caseId, Long currentUserId) {
		CaseAnalystMapping analystMapping = null;
		try {

			String hql = "FROM CaseAnalystMapping as bcsam WHERE bcsam.caseSeed.caseId = :seedId and bcsam.user.userId = :userId and bcsam.modifiedOn is null and bcsam.currentStatus != :status";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("seedId", caseId);
			query.setParameter("userId", currentUserId);
			query.setParameter("status", StatusEnum.FOCUS.getIndex());
			analystMapping = (CaseAnalystMapping) query.getSingleResult();
			return analystMapping;
		} catch (NoResultException e) {
			return null;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
		}
		return analystMapping;
	}
	
	@Override
	public CaseAnalystMapping findByCaseId(Long caseId) {
		CaseAnalystMapping analystMapping = null;
		try {
			String hql = "FROM CaseAnalystMapping as bcsam WHERE bcsam.caseSeed.caseId = :caseId and bcsam.modifiedOn is null and bcsam.currentStatus  != :status";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("caseId", caseId);
			query.setParameter("status", StatusEnum.FOCUS.getIndex());
			analystMapping = (CaseAnalystMapping) query.getSingleResult();
			return analystMapping;
		} catch (NoResultException e) {
			return null;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
		}
		return analystMapping;
	}

}
