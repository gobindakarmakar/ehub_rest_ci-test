package element.bst.elementexploration.rest.extention.alertmanagement.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.alertmanagement.dao.AlertCommentsDao;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.AlertComments;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author Prateek
 *
 */

@Repository("alertCommentsDao")
public class AlertCommentsDaoImpl extends GenericDaoImpl<AlertComments, Long> implements AlertCommentsDao{

	public AlertCommentsDaoImpl() {
		super(AlertComments.class);
	}
}
