package element.bst.elementexploration.rest.extention.companydata.service;


public interface CompanyDataService {

	String getCompanyData(String jsonString) throws Exception;

}
