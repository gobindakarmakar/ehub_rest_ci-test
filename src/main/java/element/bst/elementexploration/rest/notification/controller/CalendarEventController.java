package element.bst.elementexploration.rest.notification.controller;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.dto.CreateEventResponseDto;
import element.bst.elementexploration.rest.notification.dto.EventAlertDto;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.ParticipantsDTO;
import element.bst.elementexploration.rest.notification.dto.UpdateEventDto;
import element.bst.elementexploration.rest.notification.service.NotificationService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Viswanath
 * 
 */
@Api(tags = { "Calender event API" },description="Manages user's calendar events")
@RestController
@RequestMapping("/api/calendar")
public class CalendarEventController extends BaseController {

	@Autowired
	NotificationService notificationService;
	
	@Autowired
    private ApplicationEventPublisher eventPublisher;


	@ApiOperation("Creates an Event")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CreateEventResponseDto.class, message = "Event Created Successfully."),
			@ApiResponse(code=400 ,response=ResponseMessage.class ,message=ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@PostMapping(value = "/createEvent", produces = { "application/json; charset=UTF-8" }, 
			consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createEvent(@RequestParam("token") String userToken, @Valid @RequestBody EventAlertDto event, 
			BindingResult results, HttpServletRequest request) {
		NotificationAlert notificationAlert=new NotificationAlert();
		BeanUtils.copyProperties(event, notificationAlert);
		if(!results.hasErrors() && event.getScheduledFrom() != null){
			Long userId = getCurrentUserId();
			notificationAlert.setCreatedBy(userId);
			Long id = notificationService.createEvent(notificationAlert);
			/*JSONObject jsonObject = new JSONObject();
			jsonObject.put("eventId", id);*/
			CreateEventResponseDto createEventResponseDto=new CreateEventResponseDto();
			createEventResponseDto.setEventId(id);
			eventPublisher.publishEvent(new LoggingEvent(id, LoggingEventType.CREATE_EVENT, userId, new Date()));
			return new ResponseEntity<>(createEventResponseDto, HttpStatus.OK);
		}else{
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}
	
	@ApiOperation("Adds Event Participants")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.ADD_PARTICIPANTS_SUCESSFUL_MSG),
			@ApiResponse(code=401 ,response=ResponseMessage.class ,message=ElementConstants.USER_NOT_OWNER),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.EVENT_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.NO_PARTICIPANTS_PROVIDED)})
	@PostMapping(value = "/addEventParticipants", produces = { "application/json; charset=UTF-8" }, 
			consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addEventParticipants(@RequestParam("token") String userToken,
			@RequestParam("eventId") Long eventId, @RequestBody ParticipantsDTO participantsDTO,
			HttpServletRequest request) {

		ResponseEntity<?> response = null;
		Long userId = getCurrentUserId();
		if (notificationService.isEventOwner(eventId, userId)) {
			if (participantsDTO.getParticipants() != null && !participantsDTO.getParticipants().isEmpty()) {
				notificationService.addEventParticipants(eventId, participantsDTO.getParticipants(), userId);
				response = new ResponseEntity<>(
						new ResponseMessage(ElementConstants.ADD_PARTICIPANTS_SUCESSFUL_MSG), HttpStatus.OK);
			} else {
				response = new ResponseEntity<>(new ResponseMessage(ElementConstants.NO_PARTICIPANTS_PROVIDED), HttpStatus.BAD_REQUEST);
			}
		} else {
			response = new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_OWNER), HttpStatus.UNAUTHORIZED);
		}
		return response;
	}

	@ApiOperation("Removes Event Participants")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.REMOVE_PARTICIPANTS_SUCESSFUL_MSG),
			@ApiResponse(code=401 ,response=ResponseMessage.class ,message=ElementConstants.USER_NOT_OWNER),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.EVENT_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.NO_PARTICIPANTS_PROVIDED)})
	@PostMapping(value = "/removeEventParticipants", produces = { "application/json; charset=UTF-8" }, 
			consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> removeEventParticipants(@RequestParam("token") String userToken,
			@RequestParam("eventId") Long eventId, @RequestBody ParticipantsDTO participantsDTO,
			HttpServletRequest request) {

		ResponseEntity<?> response = null;
		Long userId = getCurrentUserId();
		if (notificationService.isEventOwner(eventId, userId)) {
			if (participantsDTO.getParticipants() != null && !participantsDTO.getParticipants().isEmpty()) {
				notificationService.removeEventParticipants(eventId, participantsDTO.getParticipants(), userId);
				response = new ResponseEntity<>(
						new ResponseMessage(ElementConstants.REMOVE_PARTICIPANTS_SUCESSFUL_MSG), HttpStatus.OK);
			} else {
				response = new ResponseEntity<>(new ResponseMessage(ElementConstants.NO_PARTICIPANTS_PROVIDED), HttpStatus.BAD_REQUEST);
			}
		} else {
			response = new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_OWNER), HttpStatus.UNAUTHORIZED);
		}		
		return response;
	}

	@ApiOperation("Updates an event by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.CALENDAR_EVENT_UPDATE_MSG),
			@ApiResponse(code=401 ,response=ResponseMessage.class ,message=ElementConstants.USER_NOT_OWNER),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.EVENT_NOT_FOUND),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.NO_PARTICIPANTS_PROVIDED)})
	@PostMapping(value = "/updateEvent", produces = { "application/json; charset=UTF-8" }, 
			consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateEvent(@RequestParam("token") String userToken, @Valid @RequestBody UpdateEventDto updateEventDto,
			BindingResult results, @RequestParam("eventId") Long eventId, HttpServletRequest request) {
		NotificationAlert event=new NotificationAlert();
		BeanUtils.copyProperties(updateEventDto, event);
		ResponseEntity<?> response = null;
		Long userId = this.getCurrentUserId();
		if(!results.hasErrors() && event.getScheduledFrom() != null){
			NotificationAlert oldEvent = notificationService.find(eventId);
			if(oldEvent != null && oldEvent.getScheduledFrom() != null){
				if (notificationService.isEventOwner(eventId, userId)) {
					notificationService.updateEvent(event, oldEvent);
					response = new ResponseEntity<>(new ResponseMessage(ElementConstants.CALENDAR_EVENT_UPDATE_MSG), HttpStatus.OK);
				} else {
					response = new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_OWNER), HttpStatus.UNAUTHORIZED);
				}
			}else{
				throw new NoDataFoundException(ElementConstants.EVENT_NOT_FOUND);
			}
		}else{
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
		return response;
	}
	
	@ApiOperation("Gets owner events list")
	@ApiResponses(value = {
	@ApiResponse(code = 200, response = ResponseUtil.class,responseContainer = "List", message = "Operation successful"),
	@ApiResponse(code = 500, response = ResponseMessage.class, message = "Error in listing events") })	
	@GetMapping(value = "/getEventListByCreator", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOwnerNotificationlist(@RequestParam("token") String token,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTo,
			@RequestParam(required = false) Long caseId, @RequestParam(required = false) Integer type,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		List<EventDTO> eventList = notificationService.getEventListByCreator(userId, dateFrom, dateTo, caseId, type,
				pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = notificationService.countEventListByCreator(userId, dateFrom, dateTo, caseId, type);

		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(eventList != null ? eventList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", eventList);
		jsonObject.put("paginationInformation", information);*/
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setPaginationInformation(information);
		responseUtil.setResult(eventList);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Gets user events list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class,responseContainer = "List", message = "List action user events is successfull"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.EVENT_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Error in listing events") })	
	@GetMapping(value = "/getUserEventList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserEventList(@RequestParam("token") String token,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTo,
			@RequestParam(required = false) Long caseId, @RequestParam(required = false) Integer type,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		List<EventDTO> eventList = notificationService.getUserEventList(userId, dateFrom, dateTo, caseId, type,
				pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = notificationService.countUserEventList(userId, dateFrom, dateTo, caseId, type);

		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(eventList != null ? eventList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", eventList);
		jsonObject.put("paginationInformation", information);*/
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setPaginationInformation(information);
		responseUtil.setResult(eventList);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}
	
	@ApiOperation("Gets event details by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = EventDetailDTO.class, message = "Operation successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, eventId or userId cannot be null"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.EVENT_NOT_FOUND)})	
	@GetMapping(value = "/getEventDetail", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEventDetail(@RequestParam("token") String token, @RequestParam("eventId") Long eventId,
			HttpServletRequest request) {
		ResponseEntity<?> response = null;
		Long userId = getCurrentUserId();
		EventDetailDTO eventDetail = notificationService.retrieveEventDetail(eventId, userId);
		if (null != eventDetail)
			response = new ResponseEntity<>(eventDetail, HttpStatus.OK);
		else
			response = new ResponseEntity<>(new ResponseMessage(ElementConstants.EVENT_NOT_FOUND), HttpStatus.NOT_FOUND);
		return response;
	}
}
