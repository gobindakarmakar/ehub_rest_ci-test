package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.persistence.NoResultException;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionMasterView;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataNotifDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByDate;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByLocationDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByType;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxCountryDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxProductTypeDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.FilterUtility;

/**
 * @author suresh
 *
 */
@Repository("txDao")
public class TxDataDaoImpl extends GenericDaoImpl<TransactionsData, Long> implements TxDataDao {

	public TxDataDaoImpl() {
		super(TransactionsData.class);
	}

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	CustomerDetailsDao customerDetailsDao;
	@Autowired
	AccountDao accountDao;

	@Autowired
	TxDataDao txDataDao;

	@Autowired
	FilterUtility filterUtility;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<TransactionsData> fetchTransaction(String accountNumber) {
		List<TransactionsData> data = this.getCurrentSession()
				.createNativeQuery("select * from TRANSACTIONS t where '" + accountNumber
						+ "' IN(t.ORIGINATOR_ACCOUNT_ID,t.BENEFICIARY_ACCOUNT_ID)", TransactionsData.class)
				.getResultList();
		return data;
	}

	/*
	 * @Override public List<TransactionsData>
	 * fetchTxsForCounterPartiesInputStats(String accountNumber) {
	 * List<TransactionsData> data = this.getCurrentSession()
	 * .createNativeQuery("select * from TRANSACTIONS t where t.ORIGINATOR_ACCOUNT_ID='"
	 * + accountNumber + "'", TransactionsData.class) .getResultList(); return
	 * data; }
	 * 
	 * @Override public List<TransactionsData>
	 * fetchTxsForCounterPartiesOutputStats(String accountNumber) {
	 * List<TransactionsData> data = this.getCurrentSession()
	 * .createNativeQuery(
	 * "select * from TRANSACTIONS t where t.BENEFICIARY_ACCOUNT_ID='" +
	 * accountNumber + "'", TransactionsData.class) .getResultList(); return
	 * data; }
	 */

	@Override
	public List<TxByType> getTxByType(Long entityId) {
		List<Account> accountlist = new ArrayList<Account>();
		List<String> accountNumbersList = new ArrayList<String>();

		Alert alert = this.getCurrentSession().find(Alert.class, entityId);

		StringBuilder hql = new StringBuilder();
		CustomerDetails customerDetails = customerDetailsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId());
		if (customerDetails != null) {
			accountlist = accountDao.fetchAccounts(customerDetails.getCustomerNumber());
			for (Account accountObj : accountlist) {
				accountNumbersList.add(accountObj.getAccountNumber());
				//System.out.println(accountNumbersList);
			}
		}

		hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByType(");
		hql.append("count(t.id),t.transProductType) from TransactionsData t where ");
		hql.append(
				" t.originatorAccountId IN (:accountNumbersList) or t.beneficiaryAccountId IN(:accountNumbersList) GROUP BY t.transProductType");
		//System.out.println(hql.toString());

		@SuppressWarnings("rawtypes")

		Query query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("accountNumbersList", accountNumbersList);
		@SuppressWarnings("unchecked")
		List<TxByType> byTypes = query.getResultList();

		return byTypes;

	}

	@Override
	public List<TxByDate> getTxByDate(Long entityId) {
		List<Account> accountlist = new ArrayList<Account>();
		Alert alert = this.getCurrentSession().find(Alert.class, entityId);
		List<String> accountNumbersList = new ArrayList<String>();

		StringBuilder hql = new StringBuilder();
		CustomerDetails customerDetails = customerDetailsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId());
		if (customerDetails != null) {
			accountlist = accountDao.fetchAccounts(customerDetails.getCustomerNumber());
			for (Account accountObj : accountlist) {
				accountNumbersList.add(accountObj.getAccountNumber());
				//System.out.println(accountNumbersList);
			}
		}

		hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByDate(");
		hql.append("count(t.id),sum(amount),t.businessDate) from TransactionsData t where ");
		hql.append(
				" t.originatorAccountId IN (:accountNumbersList) or t.beneficiaryAccountId IN(:accountNumbersList) GROUP BY t.businessDate");
		//System.out.println(hql.toString());
		@SuppressWarnings("rawtypes")
		Query query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("accountNumbersList", accountNumbersList);
		@SuppressWarnings("unchecked")
		List<TxByDate> byTypes = query.getResultList();

		return byTypes;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TxByLocationDto> getTxByLoc(List<String> partyCustomerIdentifiers)
			throws IllegalAccessException, InvocationTargetException {
		if (!partyCustomerIdentifiers.isEmpty()) {
			StringBuilder hql = new StringBuilder();

			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByLocationDto(");
			hql.append(
					" count(c.id),c.residentCountry) from CustomerDetails c where c.customerNumber IN(:partyCustomerIdentifiers)");
			/*
			 * if (partyCustomerIdentifiers.size() > 0) {
			 * hql.append(partyCustomerIdentifiers.get(0)+"'"); for (int i = 1;
			 * i < partyCustomerIdentifiers.size(); i++) {
			 * hql.append(","+"'"+partyCustomerIdentifiers.get(i)+"'"); } }
			 * hql.append(")");
			 */
			hql.append("GROUP BY c.residentCountry");
			//System.out.println(hql.toString());
			@SuppressWarnings("rawtypes")
			Query query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("partyCustomerIdentifiers", partyCustomerIdentifiers);
			List<TxByLocationDto> byTypes = query.getResultList();

			Country country = null;
			for (TxByLocationDto txByLocationDto : byTypes) {

				try {
					country = (Country) this.getCurrentSession()
							.createQuery("from Country where iso2Code='" + txByLocationDto.getCountryName() + "'")
							.getSingleResult();
				} catch (NoResultException e) {
				}
				TxCountryDto txData = new TxCountryDto();

				BeanUtils.copyProperties(txData, country);

				txByLocationDto.setPoint(txData);

			}

			return byTypes;
		} else {

			return new ArrayList<TxByLocationDto>();
		}

	}

	// Input Stats

	@Override
	public TotalDto getTotalAggregate(List<String> entityId, String minAmount, List<String> locations,
			String transactionType) {
		TotalDto totalDto = new TotalDto();
		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder queryBuilder2 = new StringBuilder();

		// List<String> origentAccounts = getOrigentAccounts(entityId);
		// List<String> benficieryAccounts = getCustomerAccounts(entityId);
		/*
		 * queryBuilder.append(
		 * "select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto( "
		 * ); queryBuilder.append(
		 * "COALESCE(avg(tx.amount),0),COALESCE(count(tx.id),0),COALESCE(min(tx.amount),0),COALESCE(max(tx.amount),0),COALESCE(sum(tx.amount),0) ) "
		 * ); queryBuilder.append(
		 * "from CustomerDetails cd,Account ac,TransactionsData tx,Country c where tx.originatorAccountId IN (:accounts) and tx.beneficiaryAccountId"
		 * + " IN (:benficieryAccounts) "); queryBuilder.append(
		 * "and tx.originatorAccountId=ac.accountNumber and ac.primaryCustomerIdentifier=cd.customerNumber "
		 * );
		 * 
		 * queryBuilder.append(" and c.iso2Code=cd.residentCountry "); if
		 * (transactionType != null)
		 * queryBuilder.append(" and tx.transactionChannel=:transactionType");
		 * if (locations != null)
		 * queryBuilder.append(" and c.country IN (:location) "); if (minAmount
		 * != null) queryBuilder.append(" and tx.amount>=:minAmount");
		 */

		queryBuilder.append(
				"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto( ");
		queryBuilder
				.append("COALESCE(avg(t.amount),0),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0))");
		queryBuilder.append("from CustomerDetails cd,TransactionsData t,Country c where "
				+ "  cd.id=t.originatorCutomerId and c.iso2Code=cd.residentCountry and t.beneficiaryCutomerId in("
				+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) ");
		if (transactionType != null)
			queryBuilder.append(" and t.transactionChannel=:transactionType ");
		if (locations != null)
			  queryBuilder.append(" and c.country IN (:location) ");
		Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
		query.setParameter("entityId", entityId);
		/*
		 * if (transactionType != null) query.setParameter("transactionType",
		 * transactionType); if (minAmount != null)
		 * query.setParameter("minAmount", Double.parseDouble(minAmount)); if
		 * (locations != null) query.setParameter("location", locations);
		 */
		
		if (transactionType != null)
			query.setParameter("transactionType", transactionType);
		if(locations != null) 
			query.setParameter("location", locations);
		totalDto = (TotalDto) query.getSingleResult();
		
		queryBuilder2.append(
				"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto( ");
		queryBuilder2
				.append("COALESCE(avg(t.amount),0),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0))");
		queryBuilder2.append("from TransactionsData t where t.beneficiaryCutomerId in("
				+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) and t.originatorCutomerId is null ");
		if (transactionType != null)
			queryBuilder2.append(" and t.transactionChannel=:transactionType ");
		Query<?> query2 = getCurrentSession().createQuery(queryBuilder2.toString());
		query2.setParameter("entityId", entityId);
		if (transactionType != null)
			query2.setParameter("transactionType", transactionType);
		TotalDto dto2=(TotalDto) query2.getSingleResult();
		totalDto.setCount(totalDto.getCount()+dto2.getCount());
		totalDto.setSum(totalDto.getSum()+dto2.getSum());
		totalDto.setAverage(totalDto.getSum()/totalDto.getCount());
		if(totalDto.getMaximum()<dto2.getMaximum())
			totalDto.setMaximum(dto2.getMaximum());
		if(totalDto.getMinimum()>dto2.getMinimum())
			totalDto.setMinimum(dto2.getMinimum());
		totalDto.setPoint("ALL");
		return totalDto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CounterPartyDto> getCounterPartiesAggregate(List<String> entityId, String counterPartyMin,
			String counterPartyMax, String minTxs, List<String> locations, String transactionType, String minAmount) {
		List<CounterPartyDto> totalDto = new ArrayList<>();

		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder queryBuilder2 = new StringBuilder();

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto( ");
			queryBuilder
					.append("cd.customerNumber,count(t.id),min(t.amount),max(t.amount),sum(t.amount),avg(t.amount) )");
			queryBuilder.append("from CustomerDetails cd,TransactionsData t,Country c where "
					+ "  cd.id=t.originatorCutomerId and c.iso2Code=cd.residentCountry and t.beneficiaryCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) ");
			if (transactionType != null)
				queryBuilder.append(" and t.transactionChannel=:transactionType ");
			if (locations != null)
				  queryBuilder.append(" and c.country IN (:location) ");
			/*
			 * if (transactionType != null)
			 * queryBuilder.append(" and tx.transactionChannel=:transactionType"
			 * ); if (locations != null) queryBuilder.
			 * append(" and c.iso2Code=cd.residentCountry and c.country IN (:location) "
			 * ); if (minAmount != null)
			 * queryBuilder.append(" and tx.amount>=:minAmount");
			 * queryBuilder.append(" group by cd.customerNumber"); if
			 * (counterPartyMin != null)
			 * queryBuilder.append(" HAVING min(tx.amount)>=:counterPartyMin");
			 * if (counterPartyMax != null)
			 * queryBuilder.append(" and max(tx.amount)<=:counterPartyMax"); if
			 * (minTxs != null)
			 * queryBuilder.append(" and count(tx.id) >=:minTransactions");
			 */
			queryBuilder.append(" group by cd.customerNumber");

			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			// query.setParameter("accounts", origentAccounts);
			query.setParameter("entityId", entityId);
			if (transactionType != null)
				query.setParameter("transactionType", transactionType);
			if(locations != null) 
				query.setParameter("location", locations);
			/*
			 * if (counterPartyMin != null)
			 * query.setParameter("counterPartyMin",
			 * Double.parseDouble(counterPartyMin)); if (counterPartyMax !=
			 * null) query.setParameter("counterPartyMax",
			 * Double.parseDouble(counterPartyMax)); if (minTxs != null)
			 * query.setParameter("minTransactions", Long.parseLong(minTxs)); if
			 * (locations != null) query.setParameter("location", locations); if
			 * (transactionType != null) query.setParameter("transactionType",
			 * transactionType); if (minAmount != null)
			 * query.setParameter("minAmount", Double.parseDouble(minAmount));
			 */

			totalDto = (List<CounterPartyDto>) query.getResultList();
			
			queryBuilder2.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto( ");
			queryBuilder2
					.append("COALESCE(t.merchant,'Unknown'),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0),COALESCE(avg(t.amount),0))");
			queryBuilder2.append("from TransactionsData t where t.beneficiaryCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) and t.originatorCutomerId is null ");
			if (transactionType != null)
				queryBuilder2.append(" and t.transactionChannel=:transactionType ");
			queryBuilder2.append(" group by t.merchant ");
			Query<?> query2 = getCurrentSession().createQuery(queryBuilder2.toString());
			query2.setParameter("entityId", entityId);
			if (transactionType != null)
				query2.setParameter("transactionType", transactionType);
			List<CounterPartyDto> list=(List<CounterPartyDto>) query2.getResultList();
			for (CounterPartyDto counterPartyDto : list) {
				counterPartyDto.setCustomerNumer("Unknown");
			}
			totalDto.addAll(list);
		return totalDto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CountryAggDto> getCountryAggregate(List<String> entityId, List<String> locations, String minTxs,
			String transactionType, String minAmount) {
		List<CountryAggDto> totalDto = new ArrayList<>();

		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder queryBuilder2 = new StringBuilder();

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto( ");
			queryBuilder
					.append("cd.residentCountry,count(t.id),min(t.amount),max(t.amount),sum(t.amount),avg(t.amount) )");
			queryBuilder.append("from CustomerDetails cd,TransactionsData t,Country c where "
					+ "  cd.id=t.originatorCutomerId and c.iso2Code=cd.residentCountry and t.beneficiaryCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) ");
			if (transactionType != null)
				queryBuilder.append(" and t.transactionChannel=:transactionType ");
			if (locations != null)
				  queryBuilder.append(" and c.country IN (:location) ");
			/*
			 * queryBuilder.append(
			 * "and tx.originatorCutomerId=cd.id group by cd.id and c.iso2Code=cd.residentCountry "
			 * ); if (transactionType != null)
			 * queryBuilder.append(" and tx.transactionChannel=:transactionType"
			 * ); if (minAmount != null)
			 * queryBuilder.append(" and tx.amount>=:minAmount"); if (locations
			 * != null) queryBuilder.append(" and c.country IN (:location) ");
			 * queryBuilder.append(" group by cd.residentCountry"); if (minTxs
			 * != null)
			 * queryBuilder.append(" HAVING count(tx.id) >=:minTransactions");
			 */
			queryBuilder.append(" group by cd.residentCountry");
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			// query.setParameter("accounts", origentAccounts);
			query.setParameter("entityId", entityId);
			// query.setParameter("minTransactions", Long.parseLong(minTxs));
			if (transactionType != null)
				query.setParameter("transactionType", transactionType);
			if(locations != null) 
				query.setParameter("location", locations);
			/*
			 * if (locations != null) query.setParameter("location", locations);
			 * if (minTxs != null) query.setParameter("minTransactions",
			 * Long.parseLong(minTxs)); if (transactionType != null)
			 * query.setParameter("transactionType", transactionType); if
			 * (minAmount != null) query.setParameter("minAmount",
			 * Double.parseDouble(minAmount));
			 */

			totalDto = (List<CountryAggDto>) query.getResultList();
			queryBuilder2.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto( ");
			queryBuilder2
			.append("COALESCE(t.merchant,'Unknown'),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0),COALESCE(avg(t.amount),0))");
			queryBuilder2.append("from TransactionsData t where t.beneficiaryCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) and t.originatorCutomerId is null ");
			if (transactionType != null)
				queryBuilder2.append(" and t.transactionChannel=:transactionType ");
			queryBuilder2.append(" group by t.merchant ");
			Query<?> query2 = getCurrentSession().createQuery(queryBuilder2.toString());
			query2.setParameter("entityId", entityId);

			if (transactionType != null)
				query2.setParameter("transactionType", transactionType);
			List<CountryAggDto> list=(List<CountryAggDto>) query2.getResultList();
			for (CountryAggDto countryAggDto : list) {
				countryAggDto.setCountryCode("Unknown");
			}
			totalDto.addAll(list);
		return totalDto;
	}

	// FOR INPUT STATS
	@SuppressWarnings("unchecked")
	public List<String> getOrigentAccounts(List<String> entityId) {
		List<String> accounts = new ArrayList<>();

		StringBuilder queryBuilder1 = new StringBuilder();

		queryBuilder1.append("select tx.originatorAccountId ");
		queryBuilder1.append(
				"from CustomerDetails cd,Account ac,TransactionsData tx where cd.customerNumber IN (:entityId) ");
		queryBuilder1.append(
				"and cd.customerNumber=ac.primaryCustomerIdentifier and ac.accountNumber=tx.beneficiaryAccountId ");
		queryBuilder1.append("group by tx.originatorAccountId");
		Query<?> query1 = getCurrentSession().createQuery(queryBuilder1.toString());
		query1.setParameter("entityId", entityId);
		accounts = (List<String>) query1.getResultList();
		return accounts;

	}

	// FOR INPUT STATS AND OUTPUT STATS
	@SuppressWarnings("unchecked")
	public List<String> getCustomerAccounts(List<String> entityId) {
		List<String> accounts = new ArrayList<>();

		StringBuilder queryBuilder1 = new StringBuilder();

		queryBuilder1.append("select ac.accountNumber ");
		queryBuilder1.append("from Account ac where ac.primaryCustomerIdentifier IN (:entityId) ");
		Query<?> query1 = getCurrentSession().createQuery(queryBuilder1.toString());
		query1.setParameter("entityId", entityId);
		accounts = (List<String>) query1.getResultList();
		return accounts;

	}

	// output

	@Override
	public TotalDto getTotalAggregateForOutputStats(List<String> entityId, String minAmount, List<String> locations,
			String transactionType) {
		TotalDto totalDto = new TotalDto();
		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder queryBuilder2 = new StringBuilder();

		queryBuilder.append(
				"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto( ");
		queryBuilder
				.append("COALESCE(avg(t.amount),0),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0))");
		queryBuilder.append("from CustomerDetails cd,TransactionsData t,Country c where "
				+ "  cd.id=t.beneficiaryCutomerId and c.iso2Code=cd.residentCountry and t.originatorCutomerId in("
				+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) "); 
		if (transactionType != null)
			queryBuilder.append(" and t.transactionChannel=:transactionType ");
		if (locations != null)
			  queryBuilder.append(" and c.country IN (:location) ");
		/*
		 * queryBuilder.append(
		 * "a tx.beneficiaryAccountId=ac.accountNumber and ac.primaryCustomerIdentifier=cd.customerNumber "
		 * ); queryBuilder.append(" and c.iso2Code=cd.residentCountry "); if
		 * (transactionType != null)
		 * queryBuilder.append(" and tx.transactionChannel=:transactionType");
		 * if (locations != null)
		 * queryBuilder.append(" and c.country IN (:location) "); if (minAmount
		 * != null) queryBuilder.append(" and tx.amount>=:minAmount");
		 */
		Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
		query.setParameter("entityId", entityId);
		// query.setParameter("benficieryAccounts", beneficiaryAccounts);
		/*
		 * if (transactionType != null) query.setParameter("transactionType",
		 * transactionType); if (minAmount != null)
		 * query.setParameter("minAmount", Double.parseDouble(minAmount)); if
		 * (locations != null) query.setParameter("location", locations);
		 */
		if (transactionType != null)
			query.setParameter("transactionType", transactionType);
		if(locations != null) 
			query.setParameter("location", locations);
		totalDto = (TotalDto) query.getSingleResult();
		queryBuilder2.append(
				"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto( ");
		queryBuilder2
				.append("COALESCE(avg(t.amount),0),COALESCE(count(t.id),0),COALESCE(max(t.amount),0),COALESCE(min(t.amount),0),COALESCE(sum(t.amount),0))");
		queryBuilder2.append("from TransactionsData t where t.originatorCutomerId in("
				+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) and t.beneficiaryCutomerId is null ");
		if (transactionType != null)
			queryBuilder2.append(" and t.transactionChannel=:transactionType ");
		Query<?> query2 = getCurrentSession().createQuery(queryBuilder2.toString());
		query2.setParameter("entityId", entityId);
		if (transactionType != null)
			query2.setParameter("transactionType", transactionType);
		TotalDto dto2=(TotalDto) query2.getSingleResult();
		totalDto.setCount(totalDto.getCount()+dto2.getCount());
		totalDto.setSum(totalDto.getSum()+dto2.getSum());
		totalDto.setAverage(totalDto.getSum()/totalDto.getCount());
		if(totalDto.getMaximum()<dto2.getMaximum())
			totalDto.setMaximum(dto2.getMaximum());
		if(totalDto.getMinimum()==0 || totalDto.getMinimum()>dto2.getMinimum())
			totalDto.setMinimum(dto2.getMinimum());
		totalDto.setPoint("ALL");

		return totalDto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CounterPartyDto> getCounterPartiesAggregateForOutputStats(List<String> entityId, String counterPartyMin,
			String counterPartyMax, String minTxs, List<String> locations, String transactionType, String minAmount) {
		List<CounterPartyDto> totalDto = new ArrayList<>();

			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder queryBuilder2 = new StringBuilder();

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto( ");
			queryBuilder
					.append("cd.customerNumber,count(t.id),min(t.amount),max(t.amount),sum(t.amount),avg(t.amount) )");
			queryBuilder.append("from CustomerDetails cd,TransactionsData t,Country c where "
					+ "  cd.id=t.beneficiaryCutomerId and c.iso2Code=cd.residentCountry and t.originatorCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) "); 
			if (transactionType != null)
				queryBuilder.append(" and t.transactionChannel=:transactionType ");
			if (locations != null)
				  queryBuilder.append(" and c.country IN (:location) ");
			/*
			 * queryBuilder.append(
			 * "and tx.beneficiaryAccountId=ac.accountNumber and ac.primaryCustomerIdentifier=cd.customerNumber "
			 * ); if (transactionType != null)
			 * queryBuilder.append(" and tx.transactionChannel=:transactionType"
			 * ); if (minAmount != null)
			 * queryBuilder.append(" and tx.amount>=:minAmount "); if (locations
			 * != null) queryBuilder.
			 * append(" and c.iso2Code=cd.residentCountry and c.country IN (:location) "
			 * ); queryBuilder.append(" group by cd.customerNumber"); if
			 * (counterPartyMin != null)
			 * queryBuilder.append(" HAVING min(tx.amount)>=:counterPartyMin");
			 * if (counterPartyMax != null)
			 * queryBuilder.append(" and max(tx.amount)<=:counterPartyMax"); if
			 * (minTxs != null)
			 * queryBuilder.append(" and count(tx.id) >=:minTransactions");
			 */
			queryBuilder.append(" group by cd.customerNumber");
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("entityId", entityId);
			if (transactionType != null)
				query.setParameter("transactionType", transactionType);
			if(locations != null) 
				query.setParameter("location", locations);
			// query.setParameter("benficieryAccounts", beneficiaryAccounts);
			/*
			 * if (counterPartyMin != null)
			 * query.setParameter("counterPartyMin",
			 * Double.parseDouble(counterPartyMin)); if (counterPartyMax !=
			 * null) query.setParameter("counterPartyMax",
			 * Double.parseDouble(counterPartyMax)); if (minTxs != null)
			 * query.setParameter("minTransactions", Long.parseLong(minTxs)); if
			 * (locations != null) query.setParameter("location", locations); if
			 * (transactionType != null) query.setParameter("transactionType",
			 * transactionType); if (minAmount != null)
			 * query.setParameter("minAmount", Double.parseDouble(minAmount));
			 */

			totalDto = (List<CounterPartyDto>) query.getResultList();
			queryBuilder2.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto( ");
			queryBuilder2
			.append("COALESCE(t.merchant,'UnKnown'),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0),COALESCE(avg(t.amount),0))");
			queryBuilder2.append("from TransactionsData t where t.originatorCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) and t.beneficiaryCutomerId is null ");
			if (transactionType != null)
				queryBuilder2.append(" and t.transactionChannel=:transactionType ");
			queryBuilder2.append(" group by t.merchant ");
			Query<?> query2 = getCurrentSession().createQuery(queryBuilder2.toString());
			query2.setParameter("entityId", entityId);

			if (transactionType != null)
				query2.setParameter("transactionType", transactionType);
			List<CounterPartyDto> list=(List<CounterPartyDto>) query2.getResultList();
			for (CounterPartyDto counterPartyDto : list) {
				counterPartyDto.setCustomerNumer("UnKnown");
			}
			totalDto.addAll(list);
		return totalDto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CountryAggDto> getCountryAggregateForOutputStats(List<String> entityId, List<String> locations,
			String minTxs, String transactionType, String minAmount) {
		List<CountryAggDto> totalDto = new ArrayList<>();

			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder queryBuilder2 = new StringBuilder();

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto( ");
			queryBuilder
					.append("cd.residentCountry,count(t.id),min(t.amount),max(t.amount),sum(t.amount),avg(t.amount) )");
			queryBuilder.append("from CustomerDetails cd,TransactionsData t,Country c where "
					+ "  cd.id=t.beneficiaryCutomerId and c.iso2Code=cd.residentCountry and t.originatorCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) "); 
			if (transactionType != null)
				queryBuilder.append(" and t.transactionChannel=:transactionType ");
			if (locations != null)
				  queryBuilder.append(" and c.country IN (:location) ");
			/*
			 * queryBuilder.append(
			 * "and tx.beneficiaryAccountId=ac.accountNumber and ac.primaryCustomerIdentifier=cd.customerNumber and c.iso2Code=cd.residentCountry "
			 * ); if (transactionType != null)
			 * queryBuilder.append(" and tx.transactionChannel=:transactionType"
			 * ); if (minAmount != null)
			 * queryBuilder.append(" and tx.amount>=:minAmount "); if (locations
			 * != null) queryBuilder.append(" and c.country IN (:location) ");
			 * queryBuilder.append(" group by cd.residentCountry"); if (minTxs
			 * != null)
			 * queryBuilder.append(" HAVING count(tx.id) >=:minTransactions");
			 */
			queryBuilder.append(" group by cd.residentCountry ");
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("entityId", entityId);
			/*
			 * query.setParameter("benficieryAccounts", beneficiaryAccounts); if
			 * (locations != null) query.setParameter("location", locations); if
			 * (minTxs != null) query.setParameter("minTransactions",
			 * Long.parseLong(minTxs)); if (transactionType != null)
			 * query.setParameter("transactionType", transactionType); if
			 * (minAmount != null) query.setParameter("minAmount",
			 * Double.parseDouble(minAmount));
			 */
			if (transactionType != null)
				query.setParameter("transactionType", transactionType);
			if(locations != null) 
				query.setParameter("location", locations);
			totalDto = (List<CountryAggDto>) query.getResultList();
			queryBuilder2.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto( ");
			queryBuilder2
			.append("COALESCE(t.merchant,'UnKnown'),COALESCE(count(t.id),0),COALESCE(min(t.amount),0),COALESCE(max(t.amount),0),COALESCE(sum(t.amount),0),COALESCE(avg(t.amount),0))");
			queryBuilder2.append("from TransactionsData t where t.originatorCutomerId in("
					+ "select cc.id from CustomerDetails cc where cc.customerNumber in(:entityId)) and t.beneficiaryCutomerId is null ");
			if (transactionType != null)
				queryBuilder2.append(" and t.transactionChannel=:transactionType ");
			queryBuilder2.append(" group by t.merchant ");
			Query<?> query2 = getCurrentSession().createQuery(queryBuilder2.toString());
			query2.setParameter("entityId", entityId);

			if (transactionType != null)
				query2.setParameter("transactionType", transactionType);
			List<CountryAggDto> list=(List<CountryAggDto>) query2.getResultList();
			for (CountryAggDto countryAggDto : list) {
				countryAggDto.setCountryCode("Unknown");
			}
			totalDto.addAll(list);
		return totalDto;
	}

	// FOR OUTPUT STATS
	@SuppressWarnings("unchecked")
	public List<String> getBeneficiariesAccountsOutputStats(List<String> entityId) {
		List<String> accounts = new ArrayList<>();

		StringBuilder queryBuilder1 = new StringBuilder();

		queryBuilder1.append("select tx.beneficiaryAccountId ");
		queryBuilder1.append(
				"from CustomerDetails cd,Account ac,TransactionsData tx where cd.customerNumber IN (:entityId) ");
		queryBuilder1.append(
				"and cd.customerNumber=ac.primaryCustomerIdentifier and ac.accountNumber=tx.originatorAccountId ");
		queryBuilder1.append("group by tx.beneficiaryAccountId");
		Query<?> query1 = getCurrentSession().createQuery(queryBuilder1.toString());
		query1.setParameter("entityId", entityId);
		accounts = (List<String>) query1.getResultList();
		return accounts;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getAllTransactions() {
		List<TransactionsData> transactionList = new ArrayList<TransactionsData>();
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("from TransactionsData");
		Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
		transactionList = (List<TransactionsData>) query.getResultList();
		return transactionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getTransaction(String accountNumber, String transactionType, String minAmount,
			String minTxs) {
		List<TransactionsData> transactionList = new ArrayList<TransactionsData>();
		StringBuilder queryBuilder = new StringBuilder();
		int minimumTrasactions = (minTxs != "" && minTxs != null) ? Integer.parseInt(minTxs) : 0;

		queryBuilder.append("from TransactionsData t where");
		queryBuilder.append(" (t.originatorAccountId=:accountNumber or t.beneficiaryAccountId=:accountNumber)");
		if (transactionType != null)
			queryBuilder.append(" and t.transactionChannel=:transactionType");
		if (minAmount != null)
			queryBuilder.append(" and t.amount>=:minAmount");
		Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
		query.setParameter("accountNumber", accountNumber);
		if (transactionType != null)
			query.setParameter("transactionType", transactionType);
		if (minAmount != null)
			query.setParameter("minAmount", Double.parseDouble(minAmount));
		transactionList = (List<TransactionsData>) query.getResultList();

		if (transactionList.size() >= (minimumTrasactions)) {
			return transactionList;
		} else {
			return new ArrayList<TransactionsData>();

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getTransactionsByAccountIds(List<String> accountIds, Date date,
			String transactionType, String minAmount) {
		List<TransactionsData> list = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"from TransactionsData t where (t.originatorAccountId IN (:accountIds) or t.beneficiaryAccountId IN (:accountIds)) ");
			if (transactionType != null)
				queryBuilder.append(" and t.transactionChannel=:transactionType");
			if (minAmount != null)
				queryBuilder.append(" and t.amount>=:minAmount");
			// queryBuilder.append(" and t.businessDate>:businessDate");

			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("accountIds", accountIds);
			if (transactionType != null)
				query.setParameter("transactionType", transactionType);
			if (minAmount != null)
				query.setParameter("minAmount", Double.parseDouble(minAmount));
			// query.setParameter("businessDate", date);

			list = (List<TransactionsData>) query.getResultList();

			// query.where(builder.and(builder.greaterThan(transaction.get("businessDate"),
			// date)),
			// (builder.or(transaction.get("originatorAccountId").in(accountIds),
			// transaction.get("beneficiaryAccountId").in(accountIds))));
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch accounts. Reason : " + e.getMessage());
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getTransaction(Integer pageNumber, Integer recordsPerPage) {
		List<TransactionsData> transactionList = new ArrayList<TransactionsData>();
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("from TransactionsData t where t.originatorCutomerId is null");
		Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		transactionList = (List<TransactionsData>) query.getResultList();
		return transactionList;
	}

	// fetching list of Txs b/w dates
	@Override
	public List<TransactionsData> fetchTxsBwDates(String fromDate, String toDate) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateFrom = formatter.parse(fromDate);
		Date dateTo = formatter.parse(toDate);

		StringBuilder hql = new StringBuilder();
		hql.append("from TransactionsData t where t.businessDate >=:fromDate and t.businessDate <=:toDate");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("fromDate", dateFrom);
		query.setParameter("toDate", dateTo);
		@SuppressWarnings("unchecked")
		List<TransactionsData> list = (List<TransactionsData>) query.getResultList();
		return list;
	}

	@Override
	public List<TransactionsDataNotifDto> fetchTxsBwDatesCustomized(String fromDate, String toDate,
			Integer recordsPerPage, Integer pageNumber, String productType, boolean input, boolean output,FilterDto filterDto)
			throws ParseException {
		List<TransactionsDataNotifDto> dataNotifDtos = new ArrayList<TransactionsDataNotifDto>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder hql = new StringBuilder();
		//hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataNotifDto(");
		hql.append("select DISTINCT t.id,t.transactionBusinessDate,t.amount ");
		hql.append("from TransactionMasterView t where ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
		
		if (input)
			hql.append(" and t.benificiaryCustomerId is not null ");
		if (output)
			hql.append(" and t.originatorCustomerId is not null ");
		if (productType != null)
			hql.append(" and t.tranactionProductType =:transProductType");
		
		//hql.append(" group  by t.transactionBusinessDate");
		
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		if (productType != null)
			query.setParameter("transProductType", productType);
		
		if(pageNumber!=null && recordsPerPage !=null)
		{
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		}
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		
		@SuppressWarnings("unchecked")
		List<Object[]> objectArray = (List<Object[]>) query.getResultList();
		
			Map<Date,TransactionsDataNotifDto> mapWithoutDuplicatesTransactions = new HashMap<Date,TransactionsDataNotifDto>();
			
			for (Object[] objects : objectArray) 
			{
				
				if(mapWithoutDuplicatesTransactions.containsKey(objects[1]))
				{
					TransactionsDataNotifDto dto = mapWithoutDuplicatesTransactions.get(formatter.parse(objects[1].toString()));
					//dto.setTransactionsCount(dto.getTransactionsCount()+1);
					dto.setTxAmount(dto.getTxAmount()+Double.valueOf(objects[2].toString()));
					dto.setTxDate(dto.getTxDate());
				}
				else 
				{
					TransactionsDataNotifDto dto=
							new TransactionsDataNotifDto(formatter.parse(objects[1].toString()),Double.valueOf(objects[2].toString()));
					mapWithoutDuplicatesTransactions.put(formatter.parse(objects[1].toString()),dto);
				}
			}
			
			for (Entry<Date, TransactionsDataNotifDto> entry : mapWithoutDuplicatesTransactions.entrySet()) 
			{
				dataNotifDtos.add(entry.getValue());
			}
			
			return dataNotifDtos;
	}

	@Override
	public long fetchTxsBwDatesCustomizedCount(String fromDate, String toDate, String productType, boolean input,
			boolean output,FilterDto filterDto) throws ParseException 
	{
		List<TransactionsDataNotifDto> list = fetchTxsBwDatesCustomized(fromDate,toDate,null,null,productType,input,output,filterDto);
		Integer value = list.size();
		return value.longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TxProductTypeDto> fetchSumTxsProductType(String fromDate, String toDate,FilterDto filterDto) throws ParseException 
	{
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<TxProductTypeDto> txProductType = new ArrayList<TxProductTypeDto>();
		StringBuilder hql = new StringBuilder();
		//hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxProductTypeDto(");
		hql.append("select DISTINCT t.id,t.tranactionProductType,t.amount");
		hql.append(" from TransactionMasterView t where ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
		//hql.append(" group by t.tranactionProductType");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		
		List<Object[]> objectArray = (List<Object[]>) query.getResultList();
		
		Map<String,TxProductTypeDto> map = new HashMap<String,TxProductTypeDto>();
		
		for (Object[] object : objectArray) 
		{
			
			if(map.containsKey(object[1].toString()))
			{
				TxProductTypeDto dto = map.get(object[1].toString());
				dto.setTransactionProductType(dto.getTransactionProductType());
				dto.setTransactionProductTypeCount(dto.getTransactionProductTypeCount()+1);
				dto.setAmountTotal(dto.getAmountTotal()+Double.valueOf(object[2].toString()));
			}
			else 
			{
				//Double l = Double.valueOf(df.format(object[2].toString()));
				TxProductTypeDto dto=new TxProductTypeDto(object[1].toString(),Double.valueOf(object[2].toString()),new Long(1));
				map.put(object[1].toString(),dto);
			}
		}
		for (Entry<String, TxProductTypeDto> entry : map.entrySet()) 
		{
			txProductType.add(entry.getValue());
		}
		
		
		return txProductType;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TxProductTypeDto> fetchAlertTransactionProductCount(String fromDate, String toDate,FilterDto filterDto)throws ParseException
	{
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		StringBuilder hql = new StringBuilder();
		hql.append("select distinct t.amount,t.alertTransactionId,t.tranactionProductType from TransactionMasterView t ");
		hql.append(" where t.alertId is not null and t.tranactionProductType is not null and  ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
		//hql.append(" group by t.tranactionProductType");
		
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		
		
		List<Object[]> objArray = (List<Object[]>) query.getResultList();
		
		List<TxProductTypeDto> txProductTypeList = new ArrayList<TxProductTypeDto>();
		
		Map<String,TxProductTypeDto> map = new HashMap<String,TxProductTypeDto>();
		
		for (Object[] object : objArray) 
		{
			
			if(map.containsKey(object[2].toString()))
			{
				TxProductTypeDto dto = map.get(object[2].toString());
				dto.setTransactionProductTypeCount(dto.getTransactionProductTypeCount()+1);
				dto.setAmountTotal(dto.getAmountTotal()+Double.valueOf(object[0].toString()));
			}
			else 
			{
				TxProductTypeDto dto=new TxProductTypeDto(object[2].toString(),Double.valueOf(object[1].toString()),new Long(1));
				map.put(object[2].toString(),dto);
			}
		}
		for (Entry<String, TxProductTypeDto> entry : map.entrySet()) 
		{
			txProductTypeList.add(entry.getValue());
		}
		
		return txProductTypeList;
	}
	

	@Override
	public List<TransactionAmountAndAlertCountDto> getTotalAmountAndAlertCountByProductType(String fromDate,
			String toDate, Boolean groupBy, Integer recordsPerPage, Integer pageNumber,boolean input,boolean output,String type,FilterDto filterDto) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<TransactionAmountAndAlertCountDto> listTransactions = new ArrayList<TransactionAmountAndAlertCountDto>();
		List<TransactionAmountAndAlertCountDto> listAlerts = new ArrayList<TransactionAmountAndAlertCountDto>();
		
		StringBuilder subQueryBuilder = new StringBuilder();
		subQueryBuilder.append(" from TransactionMasterView t where t.id is not null and ");
		subQueryBuilder.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			subQueryBuilder.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		
		if(input)
			subQueryBuilder.append(" and t.benificiaryCustomerId is not null ");
		if(output)
			subQueryBuilder.append(" and t.originatorCustomerId is not null ");
		
		if(filterDto!=null)
			subQueryBuilder.append(filterUtility.filterColumns(filterDto));
		
		
		Query<?> query = this.getCurrentSession().createQuery(subQueryBuilder.toString());
		
		/*if (!groupBy) 
		{
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
		}*/
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		
		/*query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);*/
		
		@SuppressWarnings("unchecked")
		List<TransactionMasterView> listTransactionsHql = (List<TransactionMasterView>) query.getResultList();
		
		
		
		//for transactions amount,count,transaction product type
		Map<Long,TransactionMasterView> mapWithoutDuplicatesTransactions = new HashMap<Long,TransactionMasterView>();
		for (TransactionMasterView transactionMasterView : listTransactionsHql) 
		{
			mapWithoutDuplicatesTransactions.put(transactionMasterView.getId(), transactionMasterView);
		}
		
		List<TransactionMasterView> listwithoutDuplicatesTransactions = new ArrayList<TransactionMasterView>(mapWithoutDuplicatesTransactions.values());
		
		
		Map<String,TransactionAmountAndAlertCountDto> mapForTransactions =new HashMap<String,TransactionAmountAndAlertCountDto>();
		
		for (TransactionMasterView transactionMasterView : listwithoutDuplicatesTransactions) 
		{
			if(transactionMasterView.getTranactionProductType()!=null)
			{
			
			if(mapForTransactions.containsKey(transactionMasterView.getTranactionProductType()))
			{
				TransactionAmountAndAlertCountDto dto = mapForTransactions.get(transactionMasterView.getTranactionProductType());
				
				dto.setTransactionsCount(totalTxCount(fromDate,toDate));
				//dto.setTransactionsCount(dto.getTransactionsCount()+1);
				dto.setTotalTransactionAmount(dto.getTotalTransactionAmount()+transactionMasterView.getAmount());
				dto.setTransactionProductType(transactionMasterView.getTranactionProductType());
			}
			else 
			{
				TransactionAmountAndAlertCountDto dto=
						new TransactionAmountAndAlertCountDto(transactionMasterView.getAmount(),transactionMasterView.getTranactionProductType(),new Long(1));
				mapForTransactions.put(transactionMasterView.getTranactionProductType(),dto);
			}
			}
		}
		
	for (Entry<String, TransactionAmountAndAlertCountDto> entry : mapForTransactions.entrySet()) 
	{
		listTransactions.add(entry.getValue());
	}
	
	//for Alerts
	
	StringBuilder subQueryBuilderAlert = new StringBuilder();
	subQueryBuilderAlert.append(" select DISTINCT t.alertTransactionId,t.amount,t.alertTransactionType from TransactionMasterView t where t.alertTransactionId is not null and ");
	subQueryBuilderAlert.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
	
	/*if(filterDto!=null)
	{
	if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		subQueryBuilder.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
	}*/
	
	if(input)
		subQueryBuilderAlert.append(" and t.benificiaryCustomerId is not null ");
	if(output)
		subQueryBuilderAlert.append(" and t.originatorCustomerId is not null ");
	
	if(filterDto!=null)
		subQueryBuilderAlert.append(filterUtility.filterColumns(filterDto));
	
	
	Query<?> queryAlert = this.getCurrentSession().createQuery(subQueryBuilderAlert.toString());
	
	/*if (!groupBy) 
	{
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
	}*/
	
	/*if(filterDto!=null)
	{
	if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
	{
	query.setParameter("fromDate", formatter.parse(fromDate));
	query.setParameter("toDate", formatter.parse(toDate));
	}
	}*/
	queryAlert.setParameter("fromDate", formatter.parse(fromDate));
	queryAlert.setParameter("toDate", formatter.parse(toDate));
	if(filterDto!=null)
		queryAlert = filterUtility.queryReturn(queryAlert, filterDto, fromDate, toDate);
	
	/*query.setFirstResult((pageNumber - 1) * (recordsPerPage));
	query.setMaxResults(recordsPerPage);*/
	
	@SuppressWarnings("unchecked")
	List<Object[]> objectArrayAlerts = (List<Object[]>) queryAlert.getResultList();
	
	
	//for alertCount and alert transaction product type
	
	/*Map<Long,TransactionMasterView> mapWithoutDuplicatesAlerts = new HashMap<Long,TransactionMasterView>();
	for (TransactionMasterView transactionMasterView : listTransactionsHql) 
	{
		mapWithoutDuplicatesAlerts.put(transactionMasterView.getAlertTransactionId(),transactionMasterView);
	}
	
	List<TransactionMasterView> listwithoutDuplicatesAlerts = new ArrayList<TransactionMasterView>(mapWithoutDuplicatesAlerts.values());*/
	
	
	Map<String,TransactionAmountAndAlertCountDto> mapForAlerts =new HashMap<String,TransactionAmountAndAlertCountDto>();
	
	for (Object[] object : objectArrayAlerts) 
	{
		if(mapForAlerts.containsKey(object[2].toString()))
		{
			TransactionAmountAndAlertCountDto dto = mapForAlerts.get(object[2].toString());
			dto.setAlertCount(dto.getAlertCount()+1);
			dto.setAlertedAmount(dto.getTotalTransactionAmount()+Double.valueOf(object[1].toString()));
			//dto.setTransactionProductType(transactionMasterView.getTranactionProductType());
		}
		else 
		{
			TransactionAmountAndAlertCountDto dto=
					new TransactionAmountAndAlertCountDto(new Long(1),Double.valueOf(object[1].toString()),object[2].toString());
			mapForAlerts.put(object[2].toString(),dto);
		}
	}
	
	for (Entry<String, TransactionAmountAndAlertCountDto> entry : mapForAlerts.entrySet()) 
		{
	listAlerts.add(entry.getValue());
		}
	
	
	
	for (TransactionAmountAndAlertCountDto transactionDto : listTransactions)
	{
		
		for (TransactionAmountAndAlertCountDto alertDto : listAlerts) 
		{
			
			if(transactionDto.getTransactionProductType().equals(alertDto.getTransactionProductType()))
			{
				if(alertDto.getAlertCount()==null)
				{
					alertDto.setAlertCount(new Long(1));
					transactionDto.setAlertCount(new Long(1));
				}
				else
				{
					transactionDto.setAlertCount(alertDto.getAlertCount());
				}
				Double countAlert = (alertDto.getAlertCount()).doubleValue();
				Double countTransaction = (transactionDto.getTransactionsCount()).doubleValue();
				Double ratio = null;
				if(countAlert!=0.0 && countTransaction!=0.0)
				{
				 ratio = ((countAlert/countTransaction)*100.00);
				}
				if(ratio!=null)
				{
					DecimalFormat format = new DecimalFormat(".##");
					transactionDto.setAlertRatio(Double.valueOf(format.format(ratio)));
				}
				
			}
			
		}
		
	}
	
	//for over all Transactions without ant type
	if(groupBy)
	{
		if(type!=null)
			return listTransactions;
	}
	else if (!groupBy)
	{
		if(!groupBy && type!=null)
		{
			Iterator<TransactionAmountAndAlertCountDto> it = listTransactions.iterator();
			while (it.hasNext())
			{
				TransactionAmountAndAlertCountDto objToRemove = it.next();
				
				if(!(type.equals(objToRemove.getTransactionProductType())))
				{
					it.remove();
				}
					
			}
			return listTransactions;
		}
		else
		{
		List<TransactionAmountAndAlertCountDto> listWithoutGroupBy = new ArrayList<TransactionAmountAndAlertCountDto>();
		TransactionAmountAndAlertCountDto alertCountDto = new TransactionAmountAndAlertCountDto();
		alertCountDto.setTransactionsCount(totalTxCount(fromDate, toDate));
		alertCountDto.setAlertCount(new Long(objectArrayAlerts.size()));
		 DoubleSummaryStatistics stats =  listwithoutDuplicatesTransactions.stream().mapToDouble((x) -> x.getAmount()).summaryStatistics(); 
		alertCountDto.setTotalTransactionAmount(stats.getSum());
		Double countAlert = (alertCountDto.getAlertCount()).doubleValue();
		Double countTransaction = (alertCountDto.getTransactionsCount()).doubleValue();
		Double ratio = null;
		if(countAlert!=0.0 && countTransaction!=0.0)
		{
			ratio = ((countAlert/countTransaction)*100.00);
		}
		//alertCountDto.setAlertRatio(((alertCountDto.getAlertCount())/(alertCountDto.getTransactionsCount()))*100.00);
		//alertCountDto.setAlertRatio(Math.floor(ratio));
		//System.out.println(ratio);
		if(ratio!=null)
		{
			DecimalFormat format = new DecimalFormat(".##");
			alertCountDto.setAlertRatio(Double.valueOf(format.format(ratio)));
		}
		listWithoutGroupBy.add(alertCountDto);
		return listWithoutGroupBy;
		}
	}
	return listTransactions;
	/*else if(groupBy)
	{
		TransactionAmountAndAlertCountDto alertCountDto = new TransactionAmountAndAlertCountDto();
		alertCountDto.setTransactionsCount(new Long(listwithoutDuplicatesTransactions.size()));
		alertCountDto.setAlertCount(new Long(listwithoutDuplicatesAlerts.size()));
		 DoubleSummaryStatistics stats =  listwithoutDuplicatesTransactions.stream().mapToDouble((x) -> x.getAmount()).summaryStatistics(); 
		alertCountDto.setTotalTransactionAmount(stats.getSum());
		alertCountDto.setAlertRatio((listwithoutDuplicatesAlerts.size()/listwithoutDuplicatesTransactions.size())*100.00);
		listTransactions.add(alertCountDto);
		return listTransactions;
	}*/
	
	//sending all transaction types 
	/*if(type!=null) 
	{
		return listTransactions;
	}*/
	//only respective type
		
		/*try {
			queryBuilder.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto(");
			if (groupBy || type!=null)
				queryBuilder.append(" t.tranactionProductType,");
			queryBuilder.append(" count(distinct t.alertTransactionId),count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id)) ");
			queryBuilder.append(" from Alert a Right Join TransactionsData t ON a.transactionId=t.id ");
			queryBuilder.append(" where t.id IN (" + subQueryBuilder.toString() + ")");
			
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if (fromDate != null) {
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
			}
			if (!groupBy && type==null) {
				query.setFirstResult((pageNumber - 1) * (recordsPerPage));
				query.setMaxResults(recordsPerPage);
			}
			
				query.setParameter("type", type);
			List<Object[]> objArray = (List<Object[]>) query.getResultList();
			
			Map<String,TransactionAmountAndAlertCountDto>  map = new HashMap<String,TransactionAmountAndAlertCountDto>();
			for (Object[] object : objArray)
			{
				if(map.containsKey(object[2].toString()))
				{
					TransactionAmountAndAlertCountDto dto=map.get(object[0].toString());
					dto.setAlertCount(object[2]);
					dto.setAlertRatio(object[3]);
					dto.setTransactionsCount(object[4]);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[1].toString()));
				}
				else
				{
					TransactionAmountAndAlertCountDto dto=new TransactionAmountAndAlertCountDto(object[2].toString(),Double.valueOf(object[1].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, TransactionAmountAndAlertCountDto> entry : map.entrySet()) 
			{
	            list.add(entry.getValue());
			}
			
			if(type!=null)
				//queryBuilder.append(" and t.transProductType=:type");
			
			if (groupBy || type!=null)
			{
				//queryBuilder.append(" Group By t.transProductType ");
			}
			
			
			return list;
		}*/ 
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertDashBoardRespDto> getAlertsBetweenDates(String fromDate, String toDate, String name,
			Integer pageNumber, Integer recordsPerPage, String orderIn,FilterDto filterDto,boolean isTimeStamp) 
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<AlertDashBoardRespDto> list = new ArrayList<AlertDashBoardRespDto>();
		StringBuilder queryBuilder = new StringBuilder();

		try 
		{
			queryBuilder.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto(");
			queryBuilder.append("t.alertId,t.alertCustomerDisplayName,t.alertBusinessDate,t.amount,t.alertFocalEntityId,t.alertSearchName,t.originatorCustomerType,t.alertScenario,t.alertCreatedDate,alertTransactionId) ");
			queryBuilder.append("from TransactionMasterView t where t.alertTransactionId is not null and ");
			queryBuilder.append(" DATE(t.alertCreatedDate)>=:fromDate and DATE(t.alertCreatedDate)<=:toDate and t.amount > 25000 ");
			//queryBuilder.append(" from Alert a,TransactionsData t,CustomerDetails c where a.transactionId=t.id and ");
			//queryBuilder.append(" c.customerNumber=a.focalNtityDisplayId ");
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
				{
				queryBuilder.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
			}
			}*/
			
			if (name != null) {
				queryBuilder.append(" and t.alertCustomerDisplayName LIKE '%");
				queryBuilder.append(name);
				queryBuilder.append("%' ");
			}
			
			if(filterDto!=null)
				queryBuilder.append(filterUtility.filterColumns(filterDto));
			
			queryBuilder.append(" order by t.alertCreatedDate ");
			if (orderIn == null || orderIn == "" || orderIn.trim().length() == 0 ) {
				queryBuilder.append("DESC");
			} 
			else
			{
				queryBuilder.append(orderIn);
			}
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			{
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			}
			}*/
			if(isTimeStamp)
			{
				Calendar calendar = new GregorianCalendar();
				calendar.setTimeInMillis(Long.valueOf(fromDate));
				formatter.setCalendar(calendar);
				formatter.setTimeZone(TimeZone.getTimeZone("IST"));
				
				Calendar calendarTo = new GregorianCalendar();
				calendarTo.setTimeInMillis(Long.valueOf(toDate));
				formatter.setCalendar(calendarTo);
				formatter.setTimeZone(TimeZone.getTimeZone("IST"));
				
				//System.out.println("Date With calendar time :" +formatter.format(calendar.getTime()));
				
				Timestamp timestampFromDate = new Timestamp((Long.valueOf(fromDate))/1000);
				Timestamp timestampToDate = new Timestamp(Long.valueOf(toDate));
				Date dateFrom = new Date(timestampFromDate.getTime());
				Date dateTo = new Date(timestampToDate.getTime());
				//System.out.println("Dates to be selected:" + dateFrom +"******************"+ dateTo);
				query.setParameter("fromDate", dateFrom);
				query.setParameter("toDate", dateTo);
			}
			else
			{
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
			}
			
			if(filterDto!=null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			
			if(pageNumber!=null || recordsPerPage!=null)
			{
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			}
			list = (List<AlertDashBoardRespDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionAmountAndAlertCountDto> getTransactionAggregates(String fromDate, String toDate,
			String granularity, Boolean isGroupByType, String transType, boolean input, boolean output,FilterDto filterDto)
			throws ParseException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<TransactionAmountAndAlertCountDto> transactionList = new ArrayList<TransactionAmountAndAlertCountDto>();
		
			StringBuilder hql = new StringBuilder();
			String builder = new String();
			//hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto( ");
			
			hql.append("select DISTINCT ");
			if ((granularity).equalsIgnoreCase("daily"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m-%d')";
			if ((granularity).equalsIgnoreCase("weekly"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%U')";
			if ((granularity).equalsIgnoreCase("monthly"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m')";
			if ((granularity).equalsIgnoreCase("yearly"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y')";
			if ((granularity).equalsIgnoreCase("hours"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%H')";
			if ((granularity).equalsIgnoreCase("minute"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%i')";

			hql.append(builder);
			hql.append(",t.id,round(t.amount,2) from TransactionMasterView t where t.id is not null and ");
			hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
			
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
				hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
			}*/
			
			if(input)
				hql.append(" and t.benificiaryCustomerId is not null ");
			if(output)
				hql.append(" and t.originatorCustomerId is not null ");
			
			if(filterDto!=null)
				hql.append(filterUtility.filterColumns(filterDto));
			
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			{
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			}
			}*/
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!=null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			
			
			List<Object[]> objectArray = (List<Object[]>) query.getResultList();
			
			//for transactions amount,count,transaction product type
			
			Map<String,TransactionAmountAndAlertCountDto> mapWithoutDuplicatesTransactions = new HashMap<String,TransactionAmountAndAlertCountDto>();
			
			for (Object[] objects : objectArray) 
			{
				
				if(mapWithoutDuplicatesTransactions.containsKey(objects[0]))
				{
					TransactionAmountAndAlertCountDto dto = mapWithoutDuplicatesTransactions.get(objects[0].toString());
					dto.setTransactionsCount(totalTxCount(fromDate,toDate));
					dto.setTotalTransactionAmount(dto.getTotalTransactionAmount()+Double.valueOf(objects[2].toString()));
					dto.setBusinessDate(dto.getBusinessDate());
				}
				else 
				{
					TransactionAmountAndAlertCountDto dto=
							new TransactionAmountAndAlertCountDto(objects[0].toString(),new Long(1),Double.valueOf(objects[2].toString()));
					mapWithoutDuplicatesTransactions.put(objects[0].toString(),dto);
				}
			}
			
			for (Entry<String, TransactionAmountAndAlertCountDto> entry : mapWithoutDuplicatesTransactions.entrySet()) 
			{
				transactionList.add(entry.getValue());
			}
			
			return transactionList;
			
		/*	hql.append(",t.id,sum(t.amount) ");
			if (isGroupByType)
				hql.append(",t.transProductType");
			hql.append(" )");

			hql.append("from TransactionsData t ");
			if (input || output)
				hql.append(",CustomerDetails c ");

			hql.append(" where t.businessDate>=:fromDate and  t.businessDate<=:toDate ");
			if (input)
				hql.append(" and t.beneficiaryCutomerId = c.id");
			if (output)
				hql.append(" and t.originatorCutomerId = c.id");

			if (transType != null)
				hql.append(" and t.transProductType=:transProductType");

			hql.append(" group by " + builder);

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", sdf.parse(fromDate));
			query.setParameter("toDate", sdf.parse(toDate));
			if (transType != null)
				query.setParameter("transProductType", transType);

			transactionAggregatorList = (List<TransactionAmountAndAlertCountDto>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, TxDataDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}
		return transactionAggregatorList;
*/
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionAmountAndAlertCountDto> getTransactionAggregatesProductType(String fromDate, String toDate,
			String granularity, Boolean isGroupByType, String transType, boolean input, boolean output,FilterDto filterDto)
			throws ParseException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<TransactionAmountAndAlertCountDto> transactionList = new ArrayList<TransactionAmountAndAlertCountDto>();
		
		
			StringBuilder hql = new StringBuilder();
			String builder = new String();
			
			hql.append("select DISTINCT ");
			if ((granularity).equalsIgnoreCase("daily"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m-%d')";
			if ((granularity).equalsIgnoreCase("weekly"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%U')";
			if ((granularity).equalsIgnoreCase("monthly"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y-%m')";
			if ((granularity).equalsIgnoreCase("yearly"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%Y')";
			if ((granularity).equalsIgnoreCase("hours"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%H')";
			if ((granularity).equalsIgnoreCase("minute"))
				builder = "DATE_FORMAT(t.transactionBusinessDate,'%i')";

			hql.append(builder);
			hql.append(",t.id,t.amount,t.tranactionProductType from TransactionMasterView t where t.id is not null and t.tranactionProductType ='"+transType+"'");
			hql.append(" and DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
				hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
			}*/
			
			
			if(input)
				hql.append(" and t.benificiaryCustomerId is not null ");
			if(output)
				hql.append(" and t.originatorCustomerId is not null ");
			
			if(filterDto!=null)
				hql.append(filterUtility.filterColumns(filterDto));
			
			
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			{
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			}
			}*/
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
				
			if(filterDto!=null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			
			
			List<Object[]> objectArray = (List<Object[]>) query.getResultList();
			
			Map<String,TransactionAmountAndAlertCountDto> mapWithoutDuplicatesTransactions = new HashMap<String,TransactionAmountAndAlertCountDto>();
			
			for (Object[] objects : objectArray) 
			{
				
				if(mapWithoutDuplicatesTransactions.containsKey(objects[0]))
				{
					TransactionAmountAndAlertCountDto dto = mapWithoutDuplicatesTransactions.get(objects[0].toString());
					dto.setTransactionsCount(totalTxCount(fromDate, toDate));
					dto.setTotalTransactionAmount(dto.getTotalTransactionAmount()+Double.valueOf(objects[2].toString()));
					//dto.setTransactionProductType(dto.getTransactionProductType());
					dto.setBusinessDate(dto.getBusinessDate());
				}
				else 
				{
					TransactionAmountAndAlertCountDto dto=
							new TransactionAmountAndAlertCountDto(objects[0].toString(),new Long(1),Double.valueOf(objects[2].toString()),objects[3].toString());
					mapWithoutDuplicatesTransactions.put(objects[0].toString(),dto);
				}
			}
			
			for (Entry<String, TransactionAmountAndAlertCountDto> entry : mapWithoutDuplicatesTransactions.entrySet()) 
			{
				transactionList.add(entry.getValue());
			}
			
			return transactionList;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTransProductTypes() {
		List<String> transTypes = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select distinct t.tranactionProductType from TransactionMasterView t where t.id is not null ");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			transTypes = (List<String>) query.getResultList();

		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, TxDataDaoImpl.class);
			throw new FailedToExecuteQueryException();

		}
		return transTypes;

	}

	@Override
	public Long getAlertCountBetweenDates(String fromDate, String toDate, String name,FilterDto filterDto,boolean isTimeStamp)
	{
		try {

			List<AlertDashBoardRespDto> list = getAlertsBetweenDates(fromDate,toDate,name,null,null,null,filterDto,isTimeStamp);
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CorporateStructureDto> fetchCorporateStructure(String fromDate, String toDate,FilterDto filterDto) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder hql = new StringBuilder();
		hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto(");
		hql.append("t.benificiaryCorporateStructure,round(sum(t.amount),2)) ");
		hql.append(" from TransactionMasterView t where  t.benificiaryCustomerId is not null and ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		
		if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
		hql.append(" group by t.benificiaryCorporateStructure");
		
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		
		List<CorporateStructureDto> corporateStructureDtos = (List<CorporateStructureDto>) query.getResultList();
		return corporateStructureDtos;
	}

	/**
	 * Starting Queries For Risk Page
	 */

	@SuppressWarnings("unchecked")
	@Override
	public RiskCountAndRatioDto getCustomerRisk(String fromDate, String toDate,String filterType, FilterDto filterDto)
			throws ParseException {
		RiskCountAndRatioDto responseDto = new RiskCountAndRatioDto();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<Object[]> objArray=new ArrayList<Object[]>();
		List<Object[]> transactionArray=new ArrayList<Object[]>();
		Double alertedAmount=0.0;
		Double transactionAmount=0.0;
		try {
			StringBuilder hql = new StringBuilder();
			StringBuilder hql1 = new StringBuilder();
			StringBuilder hql2 = new StringBuilder();

			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto(");
			hql.append("Round(sum(t.amount),2),count(distinct t.alertTransactionId),(count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id))");
			hql.append(" from TransactionMasterView t where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!= null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			responseDto = (RiskCountAndRatioDto) query.getSingleResult();	
			
			hql1.append(queryForAlertAmount(filterType));
			hql1.append(filterUtility.filterColumns(filterDto));
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!= null)
				filterUtility.queryReturn(query1, filterDto, fromDate, toDate);
			objArray=(List<Object[]>) query1.getResultList();
			for (Object[] objects : objArray) {
				alertedAmount=alertedAmount+Double.valueOf(objects[0].toString());
			}
			
			hql2.append(queryForTransactionAmount());
			hql2.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query2 = this.getCurrentSession().createQuery(hql2.toString());
			query2.setParameter("fromDate", formatter.parse(fromDate));
			query2.setParameter("toDate", formatter.parse(toDate));
			if(filterDto!= null)
				filterUtility.queryReturn(query2, filterDto, fromDate, toDate);
			transactionArray =  (List<Object[]>) query2.getResultList();
			for (Object[] objects : transactionArray) {
				transactionAmount=transactionAmount+Double.valueOf(objects[0].toString());
			}
			responseDto.setAlertAmount(alertedAmount);
			responseDto.setTransactionAmount(transactionAmount);

			return responseDto;
		}catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getCustomerRiskAggregates(String fromDate, String toDate, FilterDto filterDto) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		List<Object[]> objArray=new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			
			Map<String,RiskCountAndRatioDto> map=new HashMap<String,RiskCountAndRatioDto>();
			
			hql.append("select distinct Round(t.amount,2),t.alertTransactionId,t.benificiaryIndustry from TransactionMasterView "
					+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			filterUtility.queryReturn(query, filterDto, fromDate, fromDate);
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] object : objArray) {
				if(map.containsKey(object[2].toString())){
					RiskCountAndRatioDto dto=map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount()+1);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[0].toString()));
				}else {
					RiskCountAndRatioDto dto=new RiskCountAndRatioDto(object[2].toString(),Double.valueOf(object[0].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, RiskCountAndRatioDto> entry : map.entrySet()) {
	            list.add(entry.getValue());
	    }
	        list.sort(Comparator.comparingDouble(RiskCountAndRatioDto :: getAlertAmount).reversed());

			return list;
		}catch (DateFormatException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public RiskCountAndRatioDto getRiskAlerts(String fromDate, String toDate, String queryforCondition,FilterDto filterDto) {
		RiskCountAndRatioDto dto = new RiskCountAndRatioDto();
		List<Object[]> objArray=new ArrayList<>();
		Double alertedAmount=0.0;
		Double transactionAmount=0.0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			StringBuilder hql1 = new StringBuilder();
			StringBuilder hql2 = new StringBuilder();

			hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto(");
			hql.append(" sum(t.amount),count(distinct t.alertTransactionId),(count(distinct t.alertTransactionId)/count(distinct t.id))*100.00,count(distinct t.id)) ");
			hql.append(" from TransactionMasterView t");
			hql.append(" where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate");
			if(queryforCondition != null)
				hql.append(" and "+queryforCondition );
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			dto=(RiskCountAndRatioDto) query.getSingleResult();
			
			hql1.append(queryForAlertAmount(null));
			if(queryforCondition != null)
				hql1.append(" and "+queryforCondition );
			hql1.append(filterUtility.filterColumns(filterDto));
			Query<?> query1 = this.getCurrentSession().createQuery(hql1.toString());
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			if(filterDto != null)
				filterUtility.queryReturn(query1, filterDto, fromDate, toDate);
			objArray= (List<Object[]>) query1.getResultList();
			for (Object[] objects : objArray) {
				alertedAmount=alertedAmount+Double.valueOf(objects[0].toString());
			}
			
			hql2.append(queryForTransactionAmount());
			if(queryforCondition != null)
				hql2.append(" and "+queryforCondition );
			hql2.append(filterUtility.filterColumns(filterDto));
			Query<?> query2 = this.getCurrentSession().createQuery(hql2.toString());
			query2.setParameter("fromDate", formatter.parse(fromDate));
			query2.setParameter("toDate", formatter.parse(toDate));
			if(filterDto != null)
				filterUtility.queryReturn(query2, filterDto, fromDate, toDate);
			objArray= (List<Object[]>) query2.getResultList();
			for (Object[] objects : objArray) {
				transactionAmount=transactionAmount+Double.valueOf(objects[0].toString());
			}
			dto.setAlertAmount(alertedAmount);
			dto.setTransactionAmount(transactionAmount);
			return dto;
		}catch (DateFormatException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Get Risk Aggregates. Reason : " + e.getMessage());
		}
	}

	@Override
	public List<CounterPartyNotiDto> fetchCounterParties(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder hql = new StringBuilder();
		hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto(");
		hql.append("t.benificiaryCustomerName,round(sum(t.amount),2))");
		hql.append(" from TransactionMasterView t where ");
		hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		
		if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
		hql.append(" group by t.benificiaryCustomerId,benificiaryCustomerName");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
			
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		@SuppressWarnings("unchecked")
		List<CounterPartyNotiDto> corporateStructureDtos = (List<CounterPartyNotiDto>) query.getResultList();
		return corporateStructureDtos;
	}

	@Override
	public List<AssociatedEntityRespDto> fetchAssociatedEntity(String fromDate, String toDate,FilterDto filterDto) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<AssociatedEntityRespDto> associatedEntity = new ArrayList<AssociatedEntityRespDto>();
		StringBuilder hql = new StringBuilder();
		//hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto(");
		//hql.append("sum(t.amount),t.originatorCutomerId) from Alert a ,TransactionsData t where t.id=a.transactionId and ");
		
		hql.append("select DISTINCT t.alertTransactionId,t.amount,t.originatorCustomerId  from TransactionMasterView t where t.originatorCustomerId is not null and t.alertTransactionId is not null");
		hql.append(" and DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			hql.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		
		if(filterDto!=null)
			hql.append(filterUtility.filterColumns(filterDto));
		
		//hql.append("group by t.originatorCutomerId");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
		
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		
		
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		@SuppressWarnings("unchecked")
		List<Object[]> objArray = (List<Object[]>) query.getResultList();
		
		Map<String,AssociatedEntityRespDto> map = new HashMap<String,AssociatedEntityRespDto>();
		
		for (Object[] object : objArray) 
		{
			if(map.containsKey(object[2].toString()))
			{
				AssociatedEntityRespDto dto=map.get(object[2].toString());
				dto.setAmount(dto.getAmount()+Double.valueOf(object[1].toString()));
				dto.setCustomerId(Long.valueOf(object[2].toString()));
			}
			else
			{
				AssociatedEntityRespDto dto=new AssociatedEntityRespDto(Double.valueOf(object[1].toString()));
				map.put(object[2].toString(),dto);
			}
		}
		for (Entry<String, AssociatedEntityRespDto> entry : map.entrySet()) {
			associatedEntity.add(entry.getValue());
    }
		
		
		
		return associatedEntity;
	}

	@Override
	public List<CounterPartyNotiDto> fetchTopFiveTransactions(String fromDate, String toDate,FilterDto filterDto) throws ParseException 
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<CounterPartyNotiDto> counterPartyNotiDtos = new ArrayList<CounterPartyNotiDto>();
		
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT t.benificiaryCustomerName as bid,round(sum(t.amount),2) as amt FROM TransactionMasterView t where t.benificiaryCustomerName is not null and ");
		builder.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			builder.append(" and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
		}*/
		
		if(filterDto!=null)
			builder.append(filterUtility.filterColumns(filterDto));
		builder.append(" group by t.benificiaryCustomerId,t.benificiaryCustomerName order by amt DESC");
		
		
		/*String sql = "select bid,amt from (SELECT t.beneficiaryCutomerId as bid,sum(t.amount) as amt FROM TRANSACTIONS t "
				+ "where t.BUSINESS_DATE>='" + fromDate + "' and t.BUSINESS_DATE<='" + toDate
				+ "' group by t.beneficiaryCutomerId) as input order by input.amt desc";*/
		
		Query<?> query = this.getCurrentSession().createQuery(builder.toString());
		
		
		/*if(filterDto!=null)
		{
		if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
		{
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		}
		}*/
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			
		if(filterDto!=null)
			query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
		@SuppressWarnings("unchecked")
		List<Object[]> list = (List<Object[]>) query.setMaxResults(5).getResultList();
		for (Object[] objects : list) 
		{
			CounterPartyNotiDto counterPartyNotiDto = new CounterPartyNotiDto();
			if(objects[0].toString()!=null)
				counterPartyNotiDto.setCustomerName(objects[0].toString());
			counterPartyNotiDto.setAmount(Double.valueOf(objects[1].toString()));
			counterPartyNotiDtos.add(counterPartyNotiDto);
		}
		return counterPartyNotiDtos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertComparisonDto> alertComparisonTransactions(String fromDate, String toDate, Date previousDate,FilterDto filterDto,String originalToDate)
			throws ParseException {
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<AlertComparisonDto> transactionList = new ArrayList<AlertComparisonDto>();
		
		try
		{
			StringBuilder hql = new StringBuilder();
			hql.append("select DISTINCT ");
			hql.append(" t.id,t.tranactionProductType from TransactionMasterView t where t.id is not null and ");
			hql.append(" DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
				hql.append(" and  t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
			}*/
			
			if(filterDto!=null)
				hql.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
				hql.append(" and  t.transactionBusinessDate>=:previousDate and t.transactionBusinessDate<=:toDate ");
			}*/
			
			
			if (toDate == null) 
			{
				query.setParameter("fromDate", previousDate);
				query.setParameter("toDate", formatter.parse(fromDate));
				
			} 
			else 
			{
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
			}
			
			
			if(filterDto!=null)
			{
				if(toDate==null)
				{
				query = filterUtility.queryReturn(query, filterDto, fromDate, originalToDate);
				}
				else
					
				{
					query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
				}
			}
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			{
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			}
			}*/
			
			
			List<Object[]> objectArray = (List<Object[]>) query.getResultList();
			
			Map<String,AlertComparisonDto> map = new HashMap<String,AlertComparisonDto>();
			
			for (Object[] object : objectArray) 
			{
				if(map.containsKey(object[1].toString()))
				{
					AlertComparisonDto dto=map.get(object[1].toString());
					dto.setCount(dto.getCount()+1);
					dto.setProductType(dto.getProductType());
				}
				else 
				{
					AlertComparisonDto dto=new AlertComparisonDto(object[1].toString(),new Long(1));
					map.put(object[1].toString(),dto);
				}
			}
			for (Entry<String, AlertComparisonDto> entry : map.entrySet()) {
				transactionList.add(entry.getValue());
	    }
			
		}
		catch (Exception e) 
		{
			
		}
			return transactionList;
		
		
		/*
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder hql = new StringBuilder();
		hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto(");
		hql.append("t.transProductType,count(t.id))");
		hql.append("from TransactionsData t where t.businessDate >=:fromDate and t.businessDate <=:toDate");
		hql.append(" Group By t.transProductType");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		if (toDate == null) {
			query.setParameter("fromDate", previousDate);
			query.setParameter("toDate", formatter.parse(fromDate));
		} else {
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
		}
		@SuppressWarnings("unchecked")
		List<AlertComparisonDto> alertComparison = (List<AlertComparisonDto>) query.getResultList();
		return alertComparison;*/
	}

	@SuppressWarnings("unused")
	@Override
	public List<RiskCountAndRatioDto> getTopCustomerRisks(String fromDate, String toDate) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();

			/*hql.append("select input.industry, input.amt,input.acount from (SELECT cd.INDUSTRY as "
					+ "industry,sum(t.AMOUNT) as amt,count(a.id) as acount "
					+ "FROM TRANSACTIONS t,ALERT a,CUSTOMER_DETAILS cd where t.id=a.TRANSACTION_ID "
					+ "and cd.id=t.beneficiaryCutomerId and a.ALERT_BUSINESS_DATE>=:fromDate"
					+ "	and a.ALERT_BUSINESS_DATE<=:toDate group by cd.INDUSTRY) as input order by input.amt asc");
			Query<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			List<Object[]> objArray = (List<Object[]>) query.getResultList();

			for (int i = objArray.size() - 1; i >= 0; i--) {

				RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();
				riskCountAndRatioDto.setType((objArray.get(i)[0].toString()));
				riskCountAndRatioDto.setAlertAmount(Double.parseDouble(objArray.get(i)[1].toString()));
				riskCountAndRatioDto.setAlertCount(Long.parseLong(objArray.get(i)[2].toString()));
				list.add(riskCountAndRatioDto);

			}*/
			

			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<RiskCountAndRatioDto> viewAll(String fromDate, String toDate, boolean isTransaction,
			Integer pageNumber, Integer recordsPerPage,FilterDto filterDto,String queryCondition) {
		List<RiskCountAndRatioDto> list=new ArrayList<>();
		List<Object[]> objArray = new ArrayList<Object[]>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();			
			hql.append("select distinct t.id,t.alertTransactionId,t.amount,t.transactionBusinessDate from TransactionMasterView t "
					+ "where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate and t.id is not null");
			if(queryCondition !=null)
				hql.append(" and t.benificiaryCustomerId is not null "+queryCondition);
			if(filterDto != null)
				hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			
			objArray =  (List<Object[]>) query.getResultList();
			for (Object[] object : objArray) {
				RiskCountAndRatioDto dto=new RiskCountAndRatioDto();
				dto.setBusinessDate(formatter.parse(object[3].toString()));
				dto.setTransactionAmount(Double.valueOf(object[2].toString()));
				if(object[1] != null)
					dto.setAlertAmount(Double.valueOf(object[2].toString()));
				list.add(dto);
			}
			return list;
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@Override
	public Long viewAllCount(String fromDate, String toDate, boolean isTransaction,FilterDto filterDto,String queryCondition) {
		Long count=null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select count(distinct t.id) from TransactionMasterView t "
					+ "where DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate and t.id is not null");
			if(queryCondition !=null)
				hql.append(" and t.benificiaryCustomerId is not null "+queryCondition);
			if(filterDto != null)
				hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			count =  (Long) query.getSingleResult();
			return count;
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getViewAll(String fromDate, String toDate, boolean isTransaction,
			Integer pageNumber, Integer recordsPerPage, String queryforCondition,FilterDto filterDto) {

		List<Object[]> objArray = new ArrayList<Object[]>();
		Map<String,RiskCountAndRatioDto> map=new HashMap<String,RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " distinct t.id,t.transactionBusinessDate,round(t.amount)";
				betweenDates = " t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate and t.id is not null "
						+ "and t.benificiaryCustomerId is not null ";

			} else {
				selectColumns = " distinct t.alertTransactionId,t.alertBusinessDate,round(t.amount)";
				betweenDates = " t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate and t.alertTransactionId is not null "
						+ " and t.benificiaryCustomerId is not null ";

			}
			hql.append("select" + selectColumns);
			hql.append(" from TransactionMasterView t where "+ betweenDates + queryforCondition);
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, null, null);
			objArray =  (List<Object[]>) query.getResultList();
			for (Object[] objects : objArray) {
				if(map.containsKey(objects[1].toString())){
					RiskCountAndRatioDto dto=map.get(objects[1].toString());
					if(isTransaction){
						dto.setTransactionAmount(dto.getTransactionAmount()+Double.valueOf(objects[2].toString()));
					}else {
						dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(objects[2].toString()));
					}
					
				}else {
					RiskCountAndRatioDto dto=new RiskCountAndRatioDto();
					if(isTransaction){
						 dto=new RiskCountAndRatioDto(formatter.parse(objects[1].toString()),Double.valueOf(objects[2].toString()));
					}else {
						 dto=new RiskCountAndRatioDto(Double.valueOf(objects[2].toString()),formatter.parse(objects[1].toString()));
					}
					map.put(objects[1].toString(),dto);
				}
			}
			List<RiskCountAndRatioDto> list=new ArrayList<>(map.values());
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getViewAllCount(String fromDate, String toDate, boolean isTransaction, String queryforCondition,
			FilterDto filterDto) {
		List<Object[]> objArray = new ArrayList<Object[]>();
		Map<String, RiskCountAndRatioDto> map = new HashMap<String, RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " distinct t.id,t.transactionBusinessDate,round(t.amount)";
				betweenDates = " t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate and t.id is not null "
						+ "and t.benificiaryCustomerId is not null ";

			} else {
				selectColumns = " distinct t.alertTransactionId,t.alertBusinessDate,round(t.amount)";
				betweenDates = " t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate and t.alertTransactionId is not null "
						+ " and t.benificiaryCustomerId is not null ";

			}
			hql.append("select" + selectColumns);
			hql.append(" from TransactionMasterView t where " + betweenDates + queryforCondition);
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (filterDto != null)
				filterUtility.queryReturn(query, filterDto, null, null);
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] objects : objArray) {
				if (map.containsKey(objects[1].toString())) {
					RiskCountAndRatioDto dto = map.get(objects[1].toString());
					if (isTransaction) {
						dto.setTransactionAmount(dto.getTransactionAmount() + Double.valueOf(objects[2].toString()));
					} else {
						dto.setAlertAmount(dto.getAlertAmount() + Double.valueOf(objects[2].toString()));
					}

				} else {
					RiskCountAndRatioDto dto = new RiskCountAndRatioDto();
					if (isTransaction) {
						dto = new RiskCountAndRatioDto(formatter.parse(objects[1].toString()),
								Double.valueOf(objects[2].toString()));
					} else {
						dto = new RiskCountAndRatioDto(Double.valueOf(objects[2].toString()),
								formatter.parse(objects[1].toString()));
					}
					map.put(objects[1].toString(), dto);
				}
			}
			List<RiskCountAndRatioDto> list = new ArrayList<>(map.values());
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getScenarioAggregates(String fromDate, String toDate, String columnToCondition,
			List<String> scenarios, String type) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(" a.scenario,COALESCE(sum(t.amount),0),count(a.id) ) ");

			hql.append(
					"from Alert a Join TransactionsData t on t.id=a.transactionId Join CustomerDetails cd on t.beneficiaryCutomerId=cd.id ");
			hql.append("join Country c on c.iso2Code=cd.residentCountry");
			hql.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate and ");

			hql.append(columnToCondition + ">0 ");
			if (scenarios != null && !scenarios.isEmpty())
				hql.append(" and a.scenario in(:scenarios)");
			hql.append("group by a.scenario");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (scenarios != null && !scenarios.isEmpty())
				if (!scenarios.isEmpty() && scenarios != null)
					query.setParameter("scenarios", scenarios);

			list = (List<RiskCountAndRatioDto>) query.getResultList();
			return list;
		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<CounterPartyNotiDto> counterPartyLocationsPlot(String fromDate, String toDate, boolean input,
			boolean output, String queryCondition,FilterDto filterDto) throws ParseException {
		List<CounterPartyNotiDto> corporateStructureDtos = new ArrayList<CounterPartyNotiDto>();
		try
		{
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder hql = new StringBuilder();
			//hql.append("from Alert a,CustomerDetails c,Country ct,TransactionsData t where a.transactionId=t.id and ");
			if(output)
			{
				if(filterDto==null)
				{
					filterDto = new FilterDto();
					filterDto.setInputCountry(false);
					filterDto.setOutputCountry(true);
				}
				else
				{
					filterDto.setInputCountry(false);
					filterDto.setOutputCountry(true);
				}
				hql.append("select DISTINCT t.id,t.originatorCountryId,t.amount from TransactionMasterView t where t.originatorCountryId is not null and t.id is not null ");
			}
			if(input)
			{
				if(filterDto==null)
				{
					filterDto = new FilterDto();
					filterDto.setOutputCountry(false);
					filterDto.setInputCountry(true);
				}
				else
				{
					filterDto.setOutputCountry(false);
					filterDto.setInputCountry(true);
				}	
				hql.append("select DISTINCT t.id,t.beneficiaryCountryId,t.amount from TransactionMasterView t where t.beneficiaryCountryId is not null and t.id is not null ");
			}
			hql.append(" and  DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate ");
			if (queryCondition != null)
				hql.append(" and " + queryCondition);
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
				hql.append(" t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");
			}*/

			if(filterDto!=null)
				hql.append(filterUtility.filterColumns(filterDto));

			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			
			/*if(filterDto!=null)
			{
			if((!filterDto.isAlertsByPeriodgreaterthan30Days()) && (!filterDto.isAlertsByPeriod10to20Days()) && (!filterDto.isAlertsByPeriod20to30Days()) && (!filterDto.isAlertByStatusGreaterThan30Days()))
			{
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			}
			}*/
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
				
			if(filterDto!=null)
				query = filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			
			
			
			List<Object[]> objectArray = (List<Object[]>) query.getResultList();
			
			
			Map<Long,CounterPartyNotiDto> map = new HashMap<Long,CounterPartyNotiDto>();
			
			for (Object[] object : objectArray) 
			{
				if(map.containsKey(Long.valueOf(object[1].toString())))
				{
					CounterPartyNotiDto dto = map.get(Long.valueOf(object[1].toString()));
					//dto.setId((dto.getCustomerId()));
					dto.setCount(dto.getCount()+1);
					dto.setAmount(dto.getAmount()+Double.valueOf(object[2].toString()));
				}
				else 
				{
					CounterPartyNotiDto dto=new CounterPartyNotiDto(Double.valueOf(object[2].toString()),Long.valueOf(object[1].toString()),new Long(1));
					map.put(Long.valueOf(object[1].toString()),dto);
				}
			}
			for (Entry<Long, CounterPartyNotiDto> entry : map.entrySet()) 
			{
				corporateStructureDtos.add(entry.getValue());
			}
			
		/*	hql.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto(");
			hql.append("ct.id ,ct.iso2Code,sum(t.amount) as amt,count(ct.id) as va) ");
			if (output)
				hql.append(" c.id=t.originatorCutomerId");
			if (input)
				hql.append(" c.id=t.beneficiaryCutomerId");
			if (fromDate != null)
				hql.append(" and t.businessDate>=:fromDate and t.businessDate<=:toDate ");
			if (queryCondition != null)
				hql.append(" and " + queryCondition);
			hql.append(" and c.residentCountry=ct.iso2Code ");
			hql.append(" group by ct.iso2Code,ct.id");
			hql.append(" order by va DESC");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if (fromDate != null) {
				query.setParameter("fromDate", formatter.parse(fromDate));
				query.setParameter("toDate", formatter.parse(toDate));
			}
			corporateStructureDtos = (List<CounterPartyNotiDto>) query.getResultList();
		} catch (Exception e) {
			
		}
*/		
		}
		catch (Exception e) 
		{
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
		}
		
		
		
			return corporateStructureDtos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertedCounterPartyDto> getAlertedTransaction(String fromDate, String toDate,
			List<String> customerNumbers) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			List<Long> origentAccounts = getTransactionIds(customerNumbers);
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto( ");
			hql.append("t.businessDate,t.transProductType,cd.displayName,c.country,t.amount,ac.bankName,c.country) ");
			hql.append("from TransactionsData t,CustomerDetails cd,Country c,Account ac where ");
			hql.append(" t.originatorAccountId=ac.accountNumber and ac.primaryCustomerIdentifier =cd.customerNumber ");
			hql.append(
					" and cd.residentCountry=c.iso2Code and t.businessDate>=:fromDate and t.businessDate<=:toDate and t.id in (:origentAccounts)");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("origentAccounts", origentAccounts);
			List<AlertedCounterPartyDto> list = (List<AlertedCounterPartyDto>) query.getResultList();
			return list;
		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private List<Long> getTransactionIds(List<String> customerNumbers) {
		List<Long> accounts = new ArrayList<>();

		try {

			StringBuilder queryBuilder = new StringBuilder();

			queryBuilder.append("select distinct tx.id ");
			queryBuilder.append(
					"from CustomerDetails cd,Account ac,TransactionsData tx where cd.customerNumber IN (:customerNumbers) ");
			queryBuilder.append(
					"and cd.customerNumber=ac.primaryCustomerIdentifier and ac.accountNumber=tx.beneficiaryAccountId ");
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("customerNumbers", customerNumbers);
			accounts = (List<Long>) query.getResultList();
			return accounts;

		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getBankAggregates(String fromDate, String toDate, boolean isForCountry,
			boolean isGroupBy, String type) {
		List<RiskCountAndRatioDto> alertedAmount = new ArrayList<>();
		List<RiskCountAndRatioDto> transactionAmount = new ArrayList<>();

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String column = null;

		try {
			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder queryBuilder1 = new StringBuilder();

			if (isForCountry && type == null)
				column = " c.country,c.latitude,c.longitude";
			else if (!isForCountry && type == null)
				column = " t.benficiaryBankName";
			else if (type != null)
				column = " a.scenario";

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			if (isGroupBy) {
				queryBuilder.append(column + ",sum(t.amount),count(a.id)) ");
			} else {
				queryBuilder.append(" sum(t.amount),count(a.id))");
			}
			queryBuilder.append("from Alert a,TransactionsData t,CustomerDetails cd,Country c where ");
			queryBuilder.append("a.transactionId=t.id and ");
			queryBuilder.append("t.beneficiaryCutomerId=cd.id and cd.residentCountry=c.iso2Code");
			queryBuilder.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate");
			if (isGroupBy && type != null && isForCountry)
				queryBuilder.append(" and c.country=:country ");
			if (isGroupBy && type != null && !isForCountry)
				queryBuilder.append(" and t.benficiaryBankName=:bankName ");
			if (isGroupBy)
				queryBuilder.append(" group by" + column);
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (isGroupBy && type != null && !isForCountry)
				query.setParameter("bankName", type);
			if (isGroupBy && type != null && isForCountry)
				query.setParameter("country", type);
			alertedAmount = (List<RiskCountAndRatioDto>) query.getResultList();

			if (!isGroupBy) {

				queryBuilder1.append(
						"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
				queryBuilder1.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id))");
				queryBuilder1.append("from Alert a Right Join TransactionsData t on a.transactionId=t.id join "
						+ " CustomerDetails cd"
						+ " on t.beneficiaryCutomerId=cd.id join Country c on cd.residentCountry=c.iso2Code where ");
				queryBuilder1.append(" t.businessDate>=:fromDate and t.businessDate<=:toDate");
				Query<?> query1 = getCurrentSession().createQuery(queryBuilder1.toString());
				query1.setParameter("fromDate", formatter.parse(fromDate));
				query1.setParameter("toDate", formatter.parse(toDate));
				transactionAmount = (List<RiskCountAndRatioDto>) query1.getResultList();
				alertedAmount.get(0).setTransactionAmount(transactionAmount.get(0).getTransactionAmount());
				alertedAmount.get(0).setAlertRatio(transactionAmount.get(0).getAlertRatio());
				alertedAmount.get(0).setTransactionCount(transactionAmount.get(0).getTransactionCount());

			}

			return alertedAmount;

		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}

	@Override
	public List<TopAmlCustomersDto> amlTopCustomers(String fromDate, String toDate,FilterDto filterDto) throws ParseException 
	{
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder builder = new StringBuilder();
		List<TopAmlCustomersDto> amlCustomersList = new ArrayList<TopAmlCustomersDto>();
		try
		{
		
		builder.append("select DISTINCT t.alertTransactionId,t.alertCustomerNumber,t.amount as amt from TransactionMasterView t "); 
		builder.append(" where t.alertTransactionId is not null and t.alertCustomerNumber is not null "); 
		builder.append(" and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate ");
		if(filterDto!=null)
			builder.append(filterUtility.filterColumns(filterDto));
		
		//builder.append(" order by amt DESC");
		/*String sql = "select cus,value,amt from(select a.FOCAL_NTITY_DISPLAY_ID as cus,count(a.ID) as value,sum(t.AMOUNT) as amt from ALERT a "
				+ "join CUSTOMER_DETAILS c on a.FOCAL_NTITY_DISPLAY_ID=c.CUSTOMER_NUMBER join TRANSACTIONS t on t.ID=a.TRANSACTION_ID "
				+ "and a.ALERT_BUSINESS_DATE>='" + fromDate + "' and a.ALERT_BUSINESS_DATE<='" + toDate + "' "
				+ " group by a.FOCAL_NTITY_DISPLAY_ID)input order by input.value DESC";*/
		
		Query<?> query = this.getCurrentSession().createQuery(builder.toString());
		query.setParameter("fromDate", formatter.parse(fromDate));
		query.setParameter("toDate", formatter.parse(toDate));
		
		if(filterDto!=null)
			builder.append(filterUtility.queryReturn(query, filterDto, fromDate, toDate));
		@SuppressWarnings("unchecked")
		List<Object[]> objectArray = (List<Object[]>) query.getResultList();
		
		Map<String,TopAmlCustomersDto> map = new HashMap<String,TopAmlCustomersDto>();
		
		for (Object[] object : objectArray) 
		{
			
			if(map.containsKey(object[1].toString()))
			{
				TopAmlCustomersDto customersDto = map.get(object[1].toString());
				customersDto.setCount(customersDto.getCount()+1);
				customersDto.setCustomerNumber(customersDto.getCustomerName());
			}
			else
			{
				TopAmlCustomersDto amlCustomersDto = new TopAmlCustomersDto(object[1].toString(),new Long(1));
				map.put(object[1].toString(), amlCustomersDto);
			}
		}
		
		for (Entry<String, TopAmlCustomersDto> entry : map.entrySet()) 
		{
			amlCustomersList.add(entry.getValue());
		}
		}
		catch (Exception e)
		{
			
		}
		
		/*for (Object[] object : objectList) {
			TopAmlCustomersDto amlCustomers = new TopAmlCustomersDto();
			amlCustomers.setCustomerName(object[0].toString());
			amlCustomers.setCount(Long.valueOf(object[1].toString()));
			amlCustomersList.add(amlCustomers);
		}*/
		return amlCustomersList;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public List<RiskCountAndRatioDto> getViewAllForBanks(String fromDate, String toDate, boolean isTransaction,
			Integer pageNumber, Integer recordsPerPage) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			/*if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType + " TransactionsData t on a.transactionId=t.id join "
					+ " Account ac on t.beneficiaryAccountId=ac.accountNumber join CustomerDetails cd"
					+ " on ac.customerDetails.id=cd.id where ac.bankName is not null and");
			hql.append(betweenDates + groupBy);*/
			hql.append("");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<RiskCountAndRatioDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Counter Party. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionAmountAndAlertCountDto> getInputAndOutput(String fromDate, String toDate, boolean isGroupBy,
			String queryCondition) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<TransactionAmountAndAlertCountDto> list = new ArrayList<TransactionAmountAndAlertCountDto>();
		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder subQueryBuilder = new StringBuilder();
		subQueryBuilder.append("select t.id from TransactionsData t ");
		if (fromDate != null)
			subQueryBuilder.append("where t.businessDate>=:fromDate and t.businessDate<=:toDate ");
		try {

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto(");
			if (isGroupBy)
				queryBuilder.append(" t.transProductType,");
			queryBuilder.append(" count(t.id)) ");
			queryBuilder
					.append(" from Alert a Right Join TransactionsData t ON a.transactionId=t.id join CustomerDetails c on "
							+ queryCondition);
			queryBuilder.append(" where t.id IN (" + subQueryBuilder.toString() + ")");
			if (isGroupBy)
				queryBuilder.append(" Group By t.transProductType ");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));

			list = (List<TransactionAmountAndAlertCountDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getViewAllCountBanks(String fromDate, String toDate, boolean isTransaction) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType + " TransactionsData t on a.transactionId=t.id join "
					+ " Account ac on t.beneficiaryAccountId=ac.accountNumber join CustomerDetails cd"
					+ " on ac.customerDetails.id=cd.id join Country c on cd.residentCountry=c.iso2Code where ");
			hql.append(betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<RiskCountAndRatioDto>) query.getResultList();
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getViewAllCountry(String fromDate, String toDate, boolean isTransaction,
			Integer pageNumber, Integer recordsPerPage) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType + " TransactionsData t on a.transactionId=t.id "
					+ " join CustomerDetails cd on cd.id=t.beneficiaryCutomerId "
					+ "  join Country c on cd.residentCountry=c.iso2Code where cd.residentCountry is not null and ");
			hql.append(betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<RiskCountAndRatioDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getViewAllContry(String fromDate, String toDate, boolean isTransaction) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			String joinType = null;
			String groupBy = null;
			String selectColumns = null;
			String betweenDates = null;

			if (isTransaction) {
				selectColumns = " t.businessDate,COALESCE(sum(t.amount),0)";
				joinType = "Right Join";
				groupBy = " group by t.businessDate";
				betweenDates = " t.businessDate>=:fromDate and t.businessDate<=:toDate";

			} else {
				selectColumns = " COALESCE(sum(t.amount),0),a.alertBusinessDate";
				joinType = " Join";
				groupBy = " group by a.alertBusinessDate";
				betweenDates = " a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";

			}
			hql.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			hql.append(selectColumns + ") ");
			hql.append("from Alert a " + joinType + " TransactionsData t on a.transactionId=t.id join "
					+ " join CustomerDetails cd on cd.id=t.beneficiaryCutomerId "
					+ "  join Country c on cd.residentCountry=c.iso2Code where cd.residentCountry is not null and ");
			hql.append(betweenDates + groupBy);
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<RiskCountAndRatioDto>) query.getResultList();
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CounterPartyInfo> counterPartyCountryAggregate(String fromDate, String toDate, Long customerId,
			boolean isCountry, FilterDto filterDto) {
		List<CounterPartyInfo> list = new ArrayList<>();
		List<Object[]> objArray = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String selectColumns = null;
		Map<Long, CounterPartyInfo> map = new HashMap<Long, CounterPartyInfo>();
		try {
			StringBuilder queryBuilder = new StringBuilder();

			if (isCountry) {
				selectColumns = " distinct t.alertTransactionId,t.originatorCountry,t.latitude,t.amount,t.longitude";
			} else {
				selectColumns = " distinct t.alertTransactionId,originatorCustomerId,t.originatorCustomerName,t.amount";
			}
			queryBuilder.append("select " + selectColumns);
			queryBuilder.append(
					"from TransactionMasterView t where t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate ");
			queryBuilder.append(" t.benificiaryCustomerId=:customerId");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("customerId", customerId);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] objects : objArray) {
				if (map.containsKey(Long.valueOf(objects[1].toString()))) {
					CounterPartyInfo dto = map.get(Long.valueOf(objects[1].toString()));
					dto.setAmount(dto.getAmount() + Double.valueOf(objects[3].toString()));
				} else {
					if(isCountry){
						CounterPartyInfo response = new CounterPartyInfo(Long.valueOf(objects[1].toString()),
								objects[2].toString(), Double.valueOf(objects[3].toString()));
						map.put(Long.valueOf(objects[1].toString()), response);
					}
					CounterPartyInfo response = new CounterPartyInfo(Long.valueOf(objects[1].toString()),
							objects[2].toString(), Double.valueOf(objects[3].toString()));
					map.put(Long.valueOf(objects[1].toString()), response);
				}
			}

			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to CounertParty Aggregates. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertedCounterPartyDto> getCustomerTransactions(String fromDate, String toDate, Long customerId,
			Integer pageNumber, Integer recordsPerPage, boolean isDetected) {
		List<AlertedCounterPartyDto> list = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		String joinType = null;
		String betweenDates = null;

		try {

			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder subQuery = new StringBuilder();
			if (isDetected) {
				joinType = "join";
				betweenDates = " and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";
			} else {
				joinType = "Right Join";
				betweenDates = " and t.businessDate>=:fromDate and t.businessDate<=:toDate";
			}

			subQuery.append("select distinct t.id from TransactionsData t where t.beneficiaryCutomerId=:customerId");
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto( ");
			queryBuilder.append("t.businessDate,t.transProductType,cd.displayName,c.country,t.amount) ");
			queryBuilder.append("from Alert a " + joinType
					+ " TransactionsData t on a.transactionId=t.id join CustomerDetails cd on cd.id=t.originatorCutomerId");
			queryBuilder.append(" join Country c on cd.residentCountry=c.iso2Code where  t.id in ("
					+ subQuery.toString() + ") and t.beneficiaryCutomerId=:customerId");
			queryBuilder.append(betweenDates);
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("customerId", customerId);
			// query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			// query.setMaxResults(recordsPerPage);
			list = (List<AlertedCounterPartyDto>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}

		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getCustomerTransactionsCount(String fromDate, String toDate, Long customerId, boolean isDetected) {
		List<AlertedCounterPartyDto> list = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		String joinType = null;
		String betweenDates = null;

		try {

			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder subQuery = new StringBuilder();
			if (isDetected) {
				joinType = "join";
				betweenDates = " and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate";
			} else {
				joinType = "Right Join";
				betweenDates = " and t.businessDate>=:fromDate and t.businessDate<=:toDate";
			}

			subQuery.append("select distinct t.id from TransactionsData t where t.beneficiaryCutomerId=:customerId");
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto( ");
			queryBuilder.append("t.businessDate,t.transProductType,cd.displayName,c.country,t.amount) ");
			queryBuilder.append("from Alert a " + joinType
					+ " TransactionsData t on a.transactionId=t.id join CustomerDetails cd on cd.id=t.originatorCutomerId");
			queryBuilder.append(" join Country c on cd.residentCountry=c.iso2Code where  t.id in ("
					+ subQuery.toString() + ") and t.beneficiaryCutomerId=:customerId");
			queryBuilder.append(betweenDates);
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("customerId", customerId);
			list = (List<AlertedCounterPartyDto>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}

		return (long) list.size();

	}

	@Override
	public RiskCountAndRatioDto getBankByType(String fromDate, String toDate, boolean isForCountry, String type) {
		RiskCountAndRatioDto alertedAmount = new RiskCountAndRatioDto();
		RiskCountAndRatioDto transactionAmount = new RiskCountAndRatioDto();

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder queryBuilder1 = new StringBuilder();

			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			queryBuilder.append("sum(t.amount),count(a.id)) ");

			queryBuilder.append("from Alert a,TransactionsData t,Account ac,CustomerDetails cd,Country c where ");
			queryBuilder.append("a.transactionId=t.id and t.beneficiaryAccountId=ac.accountNumber ");
			queryBuilder.append(" and ac.customerDetails.id=cd.id and cd.residentCountry=c.iso2Code");
			queryBuilder.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate");
			if (isForCountry)
				queryBuilder.append(" and c.country=:country ");
			if (!isForCountry)
				queryBuilder.append(" and ac.bankName=:bankName ");
			Query<?> query = getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if (!isForCountry)
				query.setParameter("bankName", type);
			if (isForCountry)
				query.setParameter("country", type);
			alertedAmount = (RiskCountAndRatioDto) query.getSingleResult();

			queryBuilder1.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto( ");
			queryBuilder1.append(" sum(t.amount),count(a.id),(count(a.id)/count(t.id))*100.00,count(t.id))");
			queryBuilder1.append("from Alert a Right Join TransactionsData t on a.transactionId=t.id join "
					+ " Account ac on t.beneficiaryAccountId=ac.accountNumber join CustomerDetails cd"
					+ " on ac.customerDetails.id=cd.id join Country c on cd.residentCountry=c.iso2Code where ");
			queryBuilder1.append(" t.businessDate>=:fromDate and t.businessDate<=:toDate");
			if (isForCountry)
				queryBuilder1.append(" and c.country=:country ");
			if (!isForCountry)
				queryBuilder1.append(" and ac.bankName=:bankName ");
			Query<?> query1 = getCurrentSession().createQuery(queryBuilder1.toString());
			query1.setParameter("fromDate", formatter.parse(fromDate));
			query1.setParameter("toDate", formatter.parse(toDate));
			if (!isForCountry)
				query1.setParameter("bankName", type);
			if (isForCountry)
				query1.setParameter("country", type);
			transactionAmount = (RiskCountAndRatioDto) query1.getSingleResult();
			transactionAmount.setAlertAmount(alertedAmount.getAlertAmount());
			transactionAmount.setAlertCount(alertedAmount.getAlertCount());
			return transactionAmount;

		} catch (NoResultException e) {
			return null;

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Banks or countries. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CounterPartyInfo> counterPartyCountryAggregateOut(String fromDate, String toDate, Long customerId,
			boolean isCountry) {
		List<CounterPartyInfo> list = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String selectColumns = null;
		String groupByColumns = null;
		try {
			StringBuilder queryBuilder = new StringBuilder();
			StringBuilder subQuery = new StringBuilder();

			if (isCountry) {
				selectColumns = " c.country,c.id,c.latitude,c.longitude";
				groupByColumns = " c.country,c.id,c.latitude,c.longitude";
			} else {
				selectColumns = " cd.id,cd.displayName";
				groupByColumns = " cd.id,cd.displayName";

			}

			subQuery.append("select distinct t.id from TransactionsData t where t.originatorCutomerId=:customerId");
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfo(");
			queryBuilder.append(selectColumns + ",sum(t.amount)) ");
			queryBuilder.append(
					"from Alert a join TransactionsData t on a.transactionId=t.id join CustomerDetails cd on cd.id=t.beneficiaryCutomerId");
			queryBuilder.append(" join Country c on cd.residentCountry=c.iso2Code where  t.id in ("
					+ subQuery.toString() + ") and t.originatorCutomerId=:customerId");
			queryBuilder.append(" and a.alertBusinessDate>=:fromDate and a.alertBusinessDate<=:toDate");
			queryBuilder.append(" group by " + groupByColumns);
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("customerId", customerId);
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			list = (List<CounterPartyInfo>) query.getResultList();

			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to CounertParty Aggregates. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertedCounterPartyDto> getCustomerTransactions(String fromDate, String toDate, Long customerId,
			 boolean isDetected,boolean isOutTransaction,boolean isInTransaction,
			FilterDto filterDto) {
		List<AlertedCounterPartyDto> list = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		String condition = null;
		String selectColumns = null;

		try {

			StringBuilder queryBuilder = new StringBuilder();
			if (isDetected && isOutTransaction) {
				
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,"
						+ "t.benificiaryCountry,t.amount,t.benificiaryCustomerId,t.beneficiaryCountryId,t.latitude,t.longitude";
				
				condition=" where  t.originatorCustomerId=:customerId and"
						+ " t.alertTransactionId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate";
			}else if (isDetected && isInTransaction) {

				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.originatorCustomerName,t.originatorBankName,"
						+ "t.originatorCountry,t.amount,t.originatorCustomerId,t.originatorCountryId,t.originatorCountryLatitude,t.originatorCountryLongitude";

				condition = " where t.benificiaryCustomerId=:customerId and"
						+ " t.alertTransactionId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate";
			}else if(!isDetected && isOutTransaction) {
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,"
						+ "t.benificiaryCountry,t.amount,t.benificiaryCustomerId,t.beneficiaryCountryId,t.latitude,t.longitude";
				
				condition=" where t.originatorCustomerId=:customerId and"
						+ " t.id is not null and DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate";
			}else if (!isDetected && isInTransaction) {
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.originatorCustomerName,t.originatorBankName,"
						+ "t.originatorCountry,t.amount,t.originatorCustomerId,t.originatorCountryId,t.originatorCountryLatitude,t.originatorCountryLongitude";
				condition=" where  t.benificiaryCustomerId=:customerId and"
						+ " t.id is not null and DATE(t.transactionBusinessDate)>=:fromDate and DATE(t.transactionBusinessDate)<=:toDate";
			}
			
			/*if(isOutTransaction){
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,"
						+ "t.benificiaryCountry,t.amount";
			}else{
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.originatorCustomerName,t.originatorBankName,"
						+ "t.originatorCountry,t.amount";
			}*/

			/*subQuery.append("select distinct t.id from TransactionsData t where t.originatorCutomerId=:customerId");
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto( ");
			queryBuilder.append("t.businessDate,t.transProductType,cd.displayName,c.country,t.amount) ");
			queryBuilder.append("from Alert a " + joinType
					+ " TransactionsData t on a.transactionId=t.id join CustomerDetails cd on cd.id=t.beneficiaryCutomerId");
			queryBuilder.append(" join Country c on cd.residentCountry=c.iso2Code where  t.id in ("
					+ subQuery.toString() + ") and t.originatorCutomerId=:customerId");
			queryBuilder.append(betweenDates);*/
			queryBuilder.append("select distinct new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto(");
			//queryBuilder.append("t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,");
			queryBuilder.append(selectColumns +") from TransactionMasterView t ");
			queryBuilder.append(condition);
			if(filterDto != null)
				queryBuilder.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("customerId", customerId);
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			list = (List<AlertedCounterPartyDto>) query.getResultList();

		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}

		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getCustomerTransactionsCount(String fromDate, String toDate, Long customerId, boolean isDetected,
			boolean isOutTransaction,boolean isInTransaction,
			FilterDto filterDto ) {
		List<AlertedCounterPartyDto> list = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		String condition = null;
		String selectColumns = null;

		try {

			StringBuilder queryBuilder = new StringBuilder();
			if (isDetected && isOutTransaction) {
				
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,"
						+ "t.benificiaryCountry,t.amount";
				
				condition=" where t.benificiaryCustomerId is not null and t.originatorCustomerId=:customerId and"
						+ " t.alertTransactionId is not null and t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate";
			}else if (isDetected && isInTransaction) {

				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.originatorCustomerName,t.originatorBankName,"
						+ "t.originatorCountry,t.amount";

				condition = " where t.originatorCustomerId is not null and t.benificiaryCustomerId=:customerId and"
						+ " t.alertTransactionId is not null and t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate";
			}else if(!isDetected && isOutTransaction) {
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,"
						+ "t.benificiaryCountry,t.amount";
				
				condition=" where t.benificiaryCustomerId is not null and t.originatorCustomerId=:customerId and"
						+ " t.id is not null and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate";
			}else if (!isDetected && isInTransaction) {
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.originatorCustomerName,t.originatorBankName,"
						+ "t.originatorCountry,t.amount";
				condition=" where t.originatorCustomerId is not null and t.benificiaryCustomerId=:customerId and"
						+ " t.id is not null and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate";
			}
			
			/*if(isOutTransaction){
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,"
						+ "t.benificiaryCountry,t.amount";
			}else{
				selectColumns="t.id,t.transactionBusinessDate,t.tranactionProductType,t.originatorCustomerName,t.originatorBankName,"
						+ "t.originatorCountry,t.amount";
			}*/

			/*subQuery.append("select distinct t.id from TransactionsData t where t.originatorCutomerId=:customerId");
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto( ");
			queryBuilder.append("t.businessDate,t.transProductType,cd.displayName,c.country,t.amount) ");
			queryBuilder.append("from Alert a " + joinType
					+ " TransactionsData t on a.transactionId=t.id join CustomerDetails cd on cd.id=t.beneficiaryCutomerId");
			queryBuilder.append(" join Country c on cd.residentCountry=c.iso2Code where  t.id in ("
					+ subQuery.toString() + ") and t.originatorCutomerId=:customerId");
			queryBuilder.append(betweenDates);*/
			queryBuilder.append("select distinct new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto(");
			//queryBuilder.append("t.id,t.transactionBusinessDate,t.tranactionProductType,t.benificiaryCustomerName,t.benificiaryBankName,");
			queryBuilder.append(selectColumns +") from TransactionMasterView t ");
			queryBuilder.append(condition);
			if(filterDto != null)
				queryBuilder.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			query.setParameter("customerId", customerId);
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			list = (List<AlertedCounterPartyDto>) query.getResultList();

		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}

		return (long) list.size();

	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public int getCreditCardLuxuryTransactionsCount(String originatorAccountId, String luxuryShopeNames,
			Boolean shopOrWebsite,Date date) throws NoResultException {
		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		StringBuilder queryBuilder = new StringBuilder();
		if(shopOrWebsite){
			queryBuilder.append("from TransactionsData t");
			queryBuilder.append(" where t.originatorAccountId=:originatorAccountId and DATE(t.businessDate) < :date  and t.merchant = (:luxuryShopeNames)");
		}else{
			queryBuilder.append("from TransactionsData t");
			queryBuilder.append(" where t.originatorAccountId=:originatorAccountId and DATE(t.businessDate) < :date and t.merchantWebsite = (:luxuryShopeNames)");
		}
		try {
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("originatorAccountId", originatorAccountId);
			query.setParameter("luxuryShopeNames", luxuryShopeNames);
			query.setParameter("date", date);
			transactionsDataList = (List<TransactionsData>) query.getResultList();
			if(transactionsDataList.size() == 0)
				return 0;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0;
		}
		
		return transactionsDataList.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getCreditCardLuxuryTransactions(String originatorAccountId, String beneficaryAccountId) {
		List<String> creditCardPaymentTypes=new ArrayList<String>();
		creditCardPaymentTypes.add("CREDIT-CARD-P2P-PAYMENT");
		creditCardPaymentTypes.add("CREDIT-CARD-POS-PAYMENT");
		creditCardPaymentTypes.add("CREDIT-CARD-ONLINE-PAYMENT");
		
		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from TransactionsData t");
			queryBuilder.append(" where t.originatorAccountId=:originatorAccountId and t.beneficiaryAccountId=:beneficaryAccountId");
			queryBuilder.append(
					" and t.transProductType IN :creditCardPaymentTypes ");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("originatorAccountId", originatorAccountId);
			query.setParameter("beneficaryAccountId", beneficaryAccountId);
			query.setParameter("creditCardPaymentTypes",creditCardPaymentTypes);
			transactionsDataList = (List<TransactionsData>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
		return transactionsDataList.size();
	}
	@SuppressWarnings("unchecked")
	public List<TransactionMasterView> getMasterDataFromView() {
		List<TransactionMasterView> list = new ArrayList<TransactionMasterView>();
		try {
			list = this.getCurrentSession().createQuery("from TransactionMasterView").getResultList();
			//System.out.println("size" + list.size());
		} catch (Exception e) {
			
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public int getCreditCardNewCountryATMTransaction(String originatorAccountId, String countryName,
			Date transactionDate) throws NoResultException {
		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from TransactionsData t");
			queryBuilder.append(" where t.originatorAccountId=:originatorAccountId and t.countryOfTransaction=:countryName and DATE(t.businessDate)< :transactionDate");
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("originatorAccountId", originatorAccountId);
			query.setParameter("countryName", countryName);
			query.setParameter("transactionDate",transactionDate);
			transactionsDataList = (List<TransactionsData>) query.getResultList();
			if(transactionsDataList.size() == 0)
				return 0;
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0;
		}
		return transactionsDataList.size();
		
	}
	@SuppressWarnings("unchecked")
	public RiskCountAndRatioDto getRiskAlertsAggregates(String fromDate, String toDate, String value,
			FilterDto filterDto) {
		RiskCountAndRatioDto response = new RiskCountAndRatioDto();
		List<Object[]> objArray=new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct t.amount,t.alertTransactionId from TransactionMasterView "
					+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(" and "+value);
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			if(filterDto != null)
				filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			objArray = (List<Object[]>) query.getResultList();
			Double alertAmount=0.0;
			for (Object[] object : objArray) {
				alertAmount=alertAmount+Double.valueOf(object[0].toString());
			}
			response.setAlertCount(new Long(objArray.size()));
			response.setAlertAmount(alertAmount);
			
			return response;
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getCorruptionRiskAggregates(String fromDate, String toDate, String value,
			FilterDto filterDto) {
		List<RiskCountAndRatioDto> response = new ArrayList<>();
		Map<Long,RiskCountAndRatioDto> map = new HashMap<Long,RiskCountAndRatioDto>();

		List<Object[]> objArray=new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select distinct t.amount,t.alertTransactionId,t.benificiaryCountry,t.beneficiaryCountryId,t.latitude"
					+ ",t.longitude from TransactionMasterView "
					+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(" and "+value);
			hql.append(filterUtility.filterColumns(filterDto));
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			filterUtility.queryReturn(query, filterDto, fromDate, fromDate);
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] object : objArray) {
				if(map.containsKey(Long.valueOf(object[3].toString()))){
					RiskCountAndRatioDto dto=map.get(Long.valueOf(object[3].toString()));
					dto.setAlertCount(dto.getAlertCount()+1);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[0].toString()));
				}else {
					RiskCountAndRatioDto dto=new RiskCountAndRatioDto(object[2].toString(),object[4].toString(),object[5].toString()
							,Double.valueOf(object[0].toString()),new Long(1));
					map.put(Long.valueOf(object[3].toString()),dto);
				}
			}
			response=new ArrayList<>(map.values());
			
			return response;
		} catch (ParseException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new DateFormatException("Please ensure date format is yyyy-MM-dd");
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}
	}
	
	@Override
	public String queryForAlertAmount(String filterType){
		StringBuilder hql=new StringBuilder();
		hql.append("select distinct round(t.amount,2),t.alertTransactionId from TransactionMasterView "
				+ "t where t.alertId is not null and t.alertBusinessDate>=:fromDate and t.alertBusinessDate<=:toDate");	
		if ("corporateStructure".equalsIgnoreCase(filterType))
			hql.append(" and t.benificiaryCorporateStructure is not null and t.benificiaryCustomerType='CORP'");
		else
			hql.append(" and t.benificiaryIndustry is not null");
		return hql.toString();
		
	}
	
	@Override
	public String queryForTransactionAmount(){
		StringBuilder hql=new StringBuilder();				
		hql.append(" select distinct t.amount,t.id from TransactionMasterView t where t.id is not null"
				+ " and t.transactionBusinessDate>=:fromDate and t.transactionBusinessDate<=:toDate ");	
		hql.append(" and t.benificiaryIndustry is not null");
		return hql.toString();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getCounterPartyGeoAggregates(String fromDate, String toDate, FilterDto filterDto,boolean isBankCountry) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		List<Object[]> objArray=new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			
			Map<String,RiskCountAndRatioDto> map=new HashMap<String,RiskCountAndRatioDto>();
			
			hql.append("select distinct Round(t.amount,2),t.alertTransactionId,t.benificiaryCountry from TransactionMasterView "
					+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			if(isBankCountry)
				hql.append(" and t.benificiaryBankName is not null ");
			hql.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] object : objArray) {
				if(map.containsKey(object[2].toString())){
					RiskCountAndRatioDto dto=map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount()+1);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[0].toString()));
				}else {
					RiskCountAndRatioDto dto=new RiskCountAndRatioDto(object[2].toString(),Double.valueOf(object[0].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, RiskCountAndRatioDto> entry : map.entrySet()) {
	            list.add(entry.getValue());
	    }
	        list.sort(Comparator.comparingDouble(RiskCountAndRatioDto :: getAlertAmount).reversed());

			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Get Counter Party Aggregates. Reason : " + e.getMessage());
		}

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RiskCountAndRatioDto> getBankAggregates(String fromDate, String toDate, FilterDto filterDto) {
		List<RiskCountAndRatioDto> list = new ArrayList<RiskCountAndRatioDto>();
		List<Object[]> objArray=new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			
			Map<String,RiskCountAndRatioDto> map=new HashMap<String,RiskCountAndRatioDto>();
			
			hql.append("select distinct Round(t.amount,2),t.alertTransactionId,t.benificiaryBankName from TransactionMasterView "
					+ "t where t.alertId is not null and DATE(t.alertBusinessDate)>=:fromDate and DATE(t.alertBusinessDate)<=:toDate");
			hql.append(filterUtility.filterColumns(filterDto));
			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			filterUtility.queryReturn(query, filterDto, fromDate, toDate);
			objArray = (List<Object[]>) query.getResultList();
			for (Object[] object : objArray) {
				if(map.containsKey(object[2].toString())){
					RiskCountAndRatioDto dto=map.get(object[2].toString());
					dto.setAlertCount(dto.getAlertCount()+1);
					dto.setAlertAmount(dto.getAlertAmount()+Double.valueOf(object[0].toString()));
				}else {
					RiskCountAndRatioDto dto=new RiskCountAndRatioDto(object[2].toString(),Double.valueOf(object[0].toString()),new Long(1));
					map.put(object[2].toString(),dto);
				}
			}
			for (Entry<String, RiskCountAndRatioDto> entry : map.entrySet()) {
	            list.add(entry.getValue());
	    }
	        list.sort(Comparator.comparingDouble(RiskCountAndRatioDto :: getAlertAmount).reversed());

			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to Customer Risk Factors. Reason : " + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Double getCreditCardMontlyAverageOfThreeMonthsTransactions(String creditCardAccountNumber, Date date) throws NoResultException {
		Double averageOfTransactions=new Double("0.0");	
		Double sumAmount=new Double("0.0");			
		 Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
	        Date toDate= cal.getTime();
	        
		StringBuilder hql = new StringBuilder();
			try{
				
				hql.append("select DISTINCT ");
					hql.append("DATE_FORMAT(t.businessDate,'%Y-%m')");
				hql.append(",round(sum(t.amount),2) from TransactionsData t where t.originatorAccountId = :creditCardAccountNumber and ");
				hql.append(" DATE(t.businessDate)>= DATE_ADD(:toDate,-90,DAY) and DATE(t.businessDate)< :toDate ");
				hql.append("GROUP BY DATE_FORMAT(t.businessDate,'%Y-%m')");
				
			/*hql.append(
					"select dateround(avg(t.amount),2) from TransactionsData t where t.originatorAccountId = :creditCardAccountNumber and t.businessDate < (:date) and t.businessDate >= DATE_ADD(:date,-365,DAY) ");
			*/Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("creditCardAccountNumber", creditCardAccountNumber);
			query.setParameter("toDate", toDate);
			List<Object[]> objectArray = (List<Object[]>) query.getResultList();
			for (Object[] objects : objectArray) {
				sumAmount=averageOfTransactions+Double.valueOf(objects[1].toString());
				
			}

			if(objectArray.size()==0)
				return 0.0;
			averageOfTransactions = sumAmount/objectArray.size();
			
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0.0;
		}
		
		return averageOfTransactions;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Double getCreditCardMontlyAverageOfTransactions(String creditCardAccountNumber, Date date) throws NoResultException {
		Double averageOfTransactions=new Double("0.0");	
		Double sumAmount=new Double("0.0");			
		 Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
	        Date toDate= cal.getTime();
	        
		StringBuilder hql = new StringBuilder();
			try{
				
				hql.append("select DISTINCT ");
					hql.append("DATE_FORMAT(t.businessDate,'%Y-%m')");
				hql.append(",round(sum(t.amount),2) from TransactionsData t where t.originatorAccountId = :creditCardAccountNumber and ");
				hql.append(" DATE(t.businessDate)>= DATE_ADD(:toDate,-1,MONTH) and DATE(t.businessDate)< :toDate ");
				hql.append("GROUP BY DATE_FORMAT(t.businessDate,'%Y-%m')");
				
			/*hql.append(
					"select dateround(avg(t.amount),2) from TransactionsData t where t.originatorAccountId = :creditCardAccountNumber and t.businessDate < (:date) and t.businessDate >= DATE_ADD(:date,-365,DAY) ");
			*/Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("creditCardAccountNumber", creditCardAccountNumber);
			query.setParameter("toDate", toDate);
			List<Object[]> objectArray = (List<Object[]>) query.getResultList();
			for (Object[] objects : objectArray) {
				sumAmount=averageOfTransactions+Double.valueOf(objects[1].toString());
				
			}

			if(objectArray.size()==0)
				return 0.0;
			averageOfTransactions = sumAmount/objectArray.size();
			
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0.0;
		}
		
		return averageOfTransactions;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<AlertDashBoardRespDto> fetchAlertsByNumber(String customerNumber) {
		List<AlertDashBoardRespDto> list = new ArrayList<AlertDashBoardRespDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try{
			queryBuilder.append(" select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto(");
			queryBuilder.append(" a.id,a.focalNtityDisplayName,a.alertBusinessDate,t.amount,a.scenario,a.transactionId) ");
			queryBuilder.append(" from Alert a Right Join TransactionsData t on t.id=a.transactionId where a.focalNtityDisplayId=:customerNumber and a.entityType='TRANSACTION'");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("customerNumber", customerNumber);
			list = (List<AlertDashBoardRespDto>) query.getResultList();
			return list;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch Alerts by customer numnber. Reason : " + e.getMessage());
		}
		
	}
	
	


	@Override
	public Double getCreditCardATMCashInAverage(String beneficiaryAccountId, Date transactionDate) throws NoResultException {
		Double averageOfTransactions=new Double("0.0");		
		StringBuilder hql = new StringBuilder();
			try{
			hql.append(
					"select round(sum(t.amount),2) from TransactionsData t where t.beneficiaryAccountId = :beneficiaryAccountId and t.transProductType = :transProductType and t.transactionChannel= :transactionChannel and DATE(t.businessDate) <= (:transactionDate) and DATE(t.businessDate) >= DATE_ADD(:transactionDate,-30,DAY) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("beneficiaryAccountId", beneficiaryAccountId);
			query.setParameter("transactionDate", transactionDate);
			query.setParameter("transProductType", "ATM cash in");	
			query.setParameter("transactionChannel", "CREDIT CARD");
			averageOfTransactions = (Double) query.getSingleResult();
			if(averageOfTransactions == 0.0)
				return 0.0;
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0.0;
		}
		
		return averageOfTransactions;
	}
	
	@Override
	public Double getCreditCardATMWithdrawalAverage(String originatorAccountId, Date transactionDate) throws NoResultException {
		Double averageOfTransactions=new Double("0.0");		
		StringBuilder hql = new StringBuilder();
			try{
			hql.append(
					"select round(sum(t.amount),2) from TransactionsData t where t.originatorAccountId = :originatorAccountId and t.transProductType = :transProductType and t.transactionChannel= :transactionChannel and DATE(t.businessDate) <= (:transactionDate) and DATE(t.businessDate) >= DATE_ADD(:transactionDate,-30,DAY) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("originatorAccountId", originatorAccountId);
			query.setParameter("transactionDate", transactionDate);
			query.setParameter("transProductType", "ATM withdrawal");	
			query.setParameter("transactionChannel", "CREDIT CARD");
			averageOfTransactions = (Double) query.getSingleResult();
			if(averageOfTransactions == 0.0)
				return 0.0;
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0.0;
		}
		
		return averageOfTransactions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getCreditCardATMWithdrawlsInShortTime(String originatorAccountId, Date transactionDate,
			String atmAddress) throws NoResultException {
		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from TransactionsData t");
			queryBuilder.append(" where t.originatorAccountId=:originatorAccountId and t.countryOfTransaction=:countryName and t.atmAddress=:atmAddress and DATE(t.businessDate) <= (:transactionDate) and DATE(t.businessDate) >= DATE_ADD(:transactionDate,-3,HOUR) ");
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("originatorAccountId", originatorAccountId);
			query.setParameter("atmAddress", atmAddress);
			query.setParameter("transactionDate",transactionDate);
			transactionsDataList = (List<TransactionsData>) query.getResultList();
			if(transactionsDataList.size() == 0)
				return 0;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0;
		}
		return transactionsDataList.size();
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Double> getCreditCardAtmWithdrawlLastSevenDaysAmount(String creditCardAccountNumber,String transactionCountry, Date transactionDate) {
		List<Double> amountsList=new ArrayList<Double>();		
		StringBuilder hql = new StringBuilder();
			try{
			hql.append(
					"select round(t.amount,2) from TransactionsData t where t.originatorAccountId = :creditCardAccountNumber and t.countryOfTransaction = :transactionCountry and t.transProductType = :transProductType and t.transactionChannel= :transactionChannel and DATE(t.businessDate) <= (:transactionDate) and DATE(t.businessDate) >= DATE_ADD(:transactionDate,-7,DAY) ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("creditCardAccountNumber", creditCardAccountNumber);
			query.setParameter("transactionDate", transactionDate);
			query.setParameter("transactionCountry", transactionCountry);
			query.setParameter("transProductType", "ATM withdrawal");	
			query.setParameter("transactionChannel", "Credit Card");
			amountsList = (List<Double>) query.getResultList();
			
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());			
			return amountsList;
		}
		return amountsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getPreviousTransactions(String originatorAccountNumber,
			String beneficieryAccountNumber) {
		List<TransactionsData> transactionsList=new ArrayList<TransactionsData>();
		StringBuilder hql = new StringBuilder();
			try{
				hql.append("from TransactionsData t");
				hql.append(" where t.originatorAccountId=:originatorAccountNumber and t.beneficiaryAccountId=:beneficieryAccountNumber ");
		
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("originatorAccountNumber", originatorAccountNumber);
			query.setParameter("beneficieryAccountNumber", beneficieryAccountNumber);		
			transactionsList=(List<TransactionsData>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());			
		}
			return transactionsList;
			}

	@Override
	public Double getCreditCardUnrelatedCounterPartyOneMonthAmount(String originatorAccountId,
			String beneficiaryAccountId, Date businessDate) {
		Double amount =0.0;
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" select Round(sum(t.amount),2) from TransactionsData t");
			hql.append(" where t.originatorAccountId=:originatorAccountId and t.beneficiaryAccountId=:beneficiaryAccountId  and DATE(t.businessDate)<=:businessDate");
			hql.append(" and DATE(t.businessDate)>= DATE_ADD(:businessDate,-30,DAY)");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("originatorAccountId", originatorAccountId);
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);	
		query.setParameter("businessDate", businessDate);	
		amount=(Double) query.getSingleResult();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return 0.0;
	}
		return amount;
		}

	@Override
	public Double getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmount(String originatorAccountId,
			String beneficiaryAccountId, Date businessDate) {
		Double amount =0.0;
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" select Round(avg(t.amount),2) from TransactionsData t");
			hql.append(" where t.originatorAccountId=:originatorAccountId and t.beneficiaryAccountId=:beneficiaryAccountId  and DATE(t.businessDate)<=:businessDate");
			hql.append(" and DATE(t.businessDate)>= DATE_ADD(:businessDate,-7,DAY)");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("originatorAccountId", originatorAccountId);
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);	
		query.setParameter("businessDate", businessDate);	
		amount=(Double) query.getSingleResult();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return 0.0;
	}
		return amount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions(
			String beneficiaryAccountId, Date businessDate) {
		List<String> productType=new ArrayList<String>();
		productType.add("Debit card");
		productType.add("Cheque");
		productType.add("Check");
		productType.add("Cash");
		productType.add("Online");
		List<TransactionsData> transactionsList=new ArrayList<TransactionsData>();
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" from TransactionsData t");
			hql.append(" where t.beneficiaryAccountId=:beneficiaryAccountId  and DATE(t.businessDate)<=:businessDate");
			hql.append(" and DATE(t.businessDate)>= DATE_ADD(:businessDate,-7,DAY) and t.transactionChannel IN (:productType)");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("productType", productType);
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);	
		query.setParameter("businessDate", businessDate);	
		transactionsList=(List<TransactionsData>) query.getResultList();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return transactionsList;
	}
		return transactionsList;	
		
		
	}

	@Override
	public Double getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorName(
			String originatorName, String beneficiaryAccountId, Date businessDate) {
		Double amount =0.0;
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" select Round(avg(t.amount),2) from TransactionsData t");
			hql.append(" where t.originatorName=:originatorName and t.beneficiaryAccountId=:beneficiaryAccountId  and DATE(t.businessDate)<=:businessDate");
			hql.append(" and DATE(t.businessDate)>= DATE_ADD(:businessDate,-7,DAY)");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("originatorName", originatorName);
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);	
		query.setParameter("businessDate", businessDate);	
		amount=(Double) query.getSingleResult();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return 0.0;
	}
		return amount;
	}
	

	@Override
	public Double getThreeMonthsTransSumWithMerchant(String beneficiaryAccountId, String merchant, Date businessDate) {
		Double amount =0.0;
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" select Round(sum(t.amount),2) from TransactionsData t");
			hql.append(" where t.merchant=:merchant and t.originatorAccountId=:beneficiaryAccountId  and DATE(t.businessDate)<:businessDate");
			hql.append(" and DATE(t.businessDate)>= DATE_ADD(:businessDate,-90,DAY)");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("merchant", merchant);
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);	
		query.setParameter("businessDate", businessDate);	
		amount=(Double) query.getSingleResult();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return 0.0;
	}
		if(amount==null)
			return 0.0;
		return amount;
	}

	@Override
	public Double getOverAllCreditAmount(String beneficiaryAccountId,String merchant) {
		Double amount =0.0;
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" select Round(sum(t.amount),2) from TransactionsData t");
			hql.append(" where t.beneficiaryAccountId=:beneficiaryAccountId and t.merchant=:merchant");
			//hql.append(" and t.transProductType in('Merchant Refund','Debit card Payment','Cash Payment','P2P payment','Cheque Payment','ATM cash in','Check Payment')");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);
		query.setParameter("merchant", merchant);
		amount=(Double) query.getSingleResult();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return 0.0;
	}
		if(amount==null)
			amount=0.0;
		return amount;
	}

	@Override
	public Double getOverAllDebitAmount(String beneficiaryAccountId,String merchant) {
		Double amount =0.0;
		StringBuilder hql = new StringBuilder();
		try{
			hql.append(" select Round(sum(t.amount),2) from TransactionsData t");
			hql.append(" where t.originatorAccountId=:beneficiaryAccountId and t.merchant=:merchant");
			//hql.append(" and t.transProductType in('POS payment','ATM withdrawal')");
		Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		query.setParameter("beneficiaryAccountId", beneficiaryAccountId);	
		query.setParameter("merchant", merchant);
		amount=(Double) query.getSingleResult();
	} catch (Exception e) {
		ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());	
		return 0.0;
	}
		if(amount==null)
			amount=0.0;
		return amount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getPreviousTransactionsCountWithMerchant(String beneficiaryAccountId, String merchant,
			Date businessDate) {
		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from TransactionsData t");
			queryBuilder.append(" where t.originatorAccountId=:beneficiaryAccountId and t.merchant=:merchant and DATE(t.businessDate) < (:businessDate) ");
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("beneficiaryAccountId", beneficiaryAccountId);
			query.setParameter("merchant", merchant);
			query.setParameter("businessDate",businessDate);
			transactionsDataList =  (List<TransactionsData>) query.getResultList();
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			return 0;
		}
		if(transactionsDataList.size() == 0)
			return 0;
		return transactionsDataList.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertDashBoardRespDto> fetchAlerts() {
		List<AlertDashBoardRespDto> list = new ArrayList<AlertDashBoardRespDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try 
		{
			queryBuilder.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto(");
			queryBuilder.append("t.alertId,t.alertCustomerDisplayName,t.alertBusinessDate,t.amount,t.alertFocalEntityId,t.alertSearchName,t.originatorCustomerType,t.alertScenario,t.alertCreatedDate,t.alertTransactionId,t.alertRiskScore) ");
			queryBuilder.append("from TransactionMasterView t where t.alertTransactionId is not null");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			list = (List<AlertDashBoardRespDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertDashBoardRespDto> getEntityBasedAlerts(String fromDate, String toDate,
			Integer pageNumber, Integer recordsPerPage, String orderIn,String entityType) {
		List<AlertDashBoardRespDto> list = new ArrayList<AlertDashBoardRespDto>();
		StringBuilder queryBuilder = new StringBuilder();
		try 
		{
			queryBuilder.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto(");
			queryBuilder.append("t.alertId,t.alertCustomerDisplayName,t.alertBusinessDate,t.amount,t.alertFocalEntityId,t.alertSearchName,t.originatorCustomerType,t.alertScenario,t.alertCreatedDate,alertTransactionId,t.entityType) ");
			queryBuilder.append("from TransactionMasterView t where t.alertTransactionId is not null");
			if(fromDate!=null && toDate!=null)
				queryBuilder.append(" and DATE(t.alertCreatedDate)>=:fromDate and DATE(t.alertCreatedDate)<=:toDate ");
			if(entityType!=null)
				queryBuilder.append(" and t.entityType =:entityType ");
			queryBuilder.append(" order by t.alertCreatedDate ");
			
			if (orderIn == null || orderIn == "" || orderIn.trim().length() == 0 ) 
				queryBuilder.append("DESC");
			else
				queryBuilder.append(orderIn);
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			
			if(pageNumber!=null || recordsPerPage!=null)
			{
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			}
			if(entityType!=null)
				query.setParameter("entityType", entityType);
			list = (List<AlertDashBoardRespDto>) query.getResultList();
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	
	}

	@Override
	public long getEntityBasedAlertsCount(String fromDate, String toDate,String entityType) {
		try {
			List<AlertDashBoardRespDto> list = getEntityBasedAlerts(fromDate, toDate,  null, null, null,entityType);
			return (long) list.size();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
	}

	   @Override
	  public TransactionMasterView updateFetchAlerts(AlertDashBoardRespDto alertDashBoardRespDto) {
			TransactionMasterView existingTransactionViewData = null;
			try {
			  StringBuilder hql = new StringBuilder();
			  hql.append("select v from TransactionMasterView v where alertId =:alertId");
			  Query<?> query = this.getCurrentSession().createQuery(hql.toString());
		       if(null != alertDashBoardRespDto.getId())
		         {
		         long alertId = alertDashBoardRespDto.getId();
			      query.setParameter("alertId", alertId);
		          }
		          else
		          {
		           throw new RuntimeException("alertId is empty");
		          }
				existingTransactionViewData = (TransactionMasterView) query.getSingleResult();
		
			     } catch (Exception e) {
					ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
					return existingTransactionViewData;
				}
		
				return existingTransactionViewData;
			}
	
	   public Long totalTxCount(String fromDate,
			String toDate) {
		Long count = 0L;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select count(distinct(t.ID)) from TRANSACTIONS t where DATE(t.BUSINESS_DATE)>=:fromDate and DATE(t.BUSINESS_DATE)<=:toDate ");
			Query<?> query = this.getCurrentSession().createNativeQuery(hql.toString());

			query.setParameter("fromDate", formatter.parse(fromDate));
			query.setParameter("toDate", formatter.parse(toDate));
			BigInteger countvalue = (BigInteger) query.getSingleResult();
			count=countvalue.longValue();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return count;
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionsData> getTransactionByAccountId(String originatorAccountId, String beneficiaryAccountId) {
		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from TransactionsData t ");
			queryBuilder.append(" where t.originatorAccountId=:originatorAccountId and t.beneficiaryAccountId=:beneficiaryAccountId and DATE(t.businessDate) <= current_date() ");
			
			Query<TransactionsData> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("originatorAccountId", originatorAccountId);
			query.setParameter("beneficiaryAccountId", beneficiaryAccountId);
			transactionsDataList =  (List<TransactionsData>) query.getResultList();
			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
			return transactionsDataList;
	}
}
