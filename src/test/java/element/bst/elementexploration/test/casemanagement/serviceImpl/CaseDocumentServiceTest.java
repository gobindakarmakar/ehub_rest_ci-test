package element.bst.elementexploration.test.casemanagement.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDocumentDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocDetailsDTO;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.serviceImpl.CaseDocumentServiceImpl;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CaseDocumentServiceTest {

	@Mock
	CaseDocumentDao caseDocumentDaoMock;

	@Mock
	UsersDao userDaoMock;

	@Mock
	AuditLogDao auditLogDaoMock;

	@Mock
	CaseDao caseDaoMock;
	
	@Mock
	ListItem listItem;

	@InjectMocks
	CaseDocumentServiceImpl caseDocumentService;

	public CaseDocDetails caseDocDetails;

	public Users user;

	public CaseDocDetailsDTO caseDocDetailsDTO;

	public List<CaseDocDetails> caseDocDetailsList = new ArrayList<CaseDocDetails>();

	public List<DocAggregatorDto> docAggregatorDtoList = new ArrayList<DocAggregatorDto>();

	@Before
	public void setupMock() {
		caseDocDetails = new CaseDocDetails(1L, "/etc/test/", "Test cases", "junit test", 10D, "pdf", new Date(), 1L,
				new Date(), 1L, "a1sd2f3", new Case(), 2);
		user = new Users("analyst", "Analyst", "Analyst", new Date(), listItem);

		caseDocDetailsDTO = new CaseDocDetailsDTO(123L, "tet/filepath", "testing Case document", " ", 1024.00, "pdf",
				new Date(), 2L, new Date(), 2L, 4);
		caseDocDetailsList.add(caseDocDetails);
		CaseDocDetails newCaseDocDetails = new CaseDocDetails(1L, "/etc/test/", "Test cases", "junit test", 10D, "pdf",
				new Date(), 1L, new Date(), 1L, "a1sd2f3", new Case(), 2);
		caseDocDetailsList.add(newCaseDocDetails);
		DocAggregatorDto docAggregatorDto1 = new DocAggregatorDto(4, 2L);
		DocAggregatorDto docAggregatorDto2 = new DocAggregatorDto(3, 3L);
		docAggregatorDtoList.add(docAggregatorDto1);
		docAggregatorDtoList.add(docAggregatorDto2);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getCaseDocumentDetailsByIdTest() {
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(caseDocDetails);
		CaseDocDetailsDTO caseDocDetailsDTO = caseDocumentService.getCaseDocumentDetailsById(1L);
		Assert.assertNotNull(caseDocDetailsDTO.getFileTitle());
	}

	@Test(expected = DocNotFoundException.class)
	public void getCaseDocumentDetailsById_whenExceptionThrowTest() {
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		caseDocumentService.getCaseDocumentDetailsById(1L);
	}

	@Test(expected = DocNotFoundException.class)
	public void updateCaseDocument_whenCaseDocumentIsNullTest() {
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		caseDocumentService.updateCaseDocument("Change title", 1L, "test junit", 1L);
	}
	
	@Test(expected = Exception.class)
	public void updateCaseDocument_whenExceptionThrowTest() {
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(caseDocDetails);
		Mockito.doThrow(new Exception()).when(caseDocumentDaoMock).saveOrUpdate(Mockito.any(CaseDocDetails.class));
		caseDocumentService.updateCaseDocument("Change title", 1L, "test junit", 1L);
	}

	@Test(expected = DocNotFoundException.class)
	public void deleteCaseDocumentById_whenCaseDocumentIsNullTest() {
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		caseDocumentService.deleteCaseDocumentById(1L, 1L);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DocNotFoundException.class)
	public void deleteCaseDocumentById_whenExceptionThrowTest() {
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(caseDocDetails);
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(auditLogDaoMock.create(Mockito.any(AuditLog.class))).thenThrow(DocNotFoundException.class);
		Mockito.when(caseDocumentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		caseDocumentService.deleteCaseDocumentById(1L, 1L);
	}

	@Test
	public void getAllDocumentsForCaseTest() {
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(new Case());
		Mockito.when(caseDocumentDaoMock.getAllDocumentsForCaseSeed(Mockito.anyLong(), Mockito.anyInt(),
				Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(caseDocDetailsList);
		List<CaseDocDetailsDTO> caseDocDetailsDTOList = caseDocumentService.getAllDocumentsForCase(1L, 2, "file title",
				1, 15);
		assertEquals(2, caseDocDetailsDTOList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = CaseSeedNotFoundException.class)
	public void getAllDocumentsForCase_WHenCaseIsNullTest() {
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenThrow(CaseSeedNotFoundException.class);
		caseDocumentService.getAllDocumentsForCase(1L, 2, "file title", 1, 15);
	}

	@Test
	public void getCaseIdFromDocTest() {
		Mockito.when(caseDocumentDaoMock.getCaseIdFromDoc(Mockito.anyLong())).thenReturn(2L);
		long caseId = caseDocumentService.getCaseIdFromDoc(1L);
		assertEquals(2L, caseId);
	}

	@Test
	public void countAllDocumentsForCaseTest() {
		Mockito.when(caseDocumentDaoMock.countAllDocumentsForCaseSeed(Mockito.anyLong(), Mockito.anyInt()))
				.thenReturn(2L);
		long count = caseDocumentService.countAllDocumentsForCase(1L, 2);
		assertEquals(2L, count);
	}

	@Test
	public void docAggregatorTest() throws ParseException {
		Mockito.when(
				caseDocumentDaoMock.docAggregator(Mockito.anyLong(), Mockito.any(Date.class), Mockito.any(Date.class)))
				.thenReturn(docAggregatorDtoList);
		List<DocAggregatorDto> newDocAggregatorDtoList = caseDocumentService.docAggregator(1L,
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2, newDocAggregatorDtoList.size());
	}

	@Test(expected = DateFormatException.class)
	public void docAggregator_whenFromDateExceptionTest() throws ParseException {
		caseDocumentService.docAggregator(1L, new Date().toString(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
	}

	@Test(expected = DateFormatException.class)
	public void docAggregator_whenToDateExceptionTest() throws ParseException {
		caseDocumentService.docAggregator(1L, new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new Date().toString());
	}
}
