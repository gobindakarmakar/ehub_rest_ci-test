package element.bst.elementexploration.test.recentsearch.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.recentsearch.daoImpl.RecentSearchDaoImpl;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;

@RunWith(MockitoJUnitRunner.class)
public class RecentSearchDaoTest {
	
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	RecentSearchDaoImpl RecentSearchDao;
	
	List<RecentSearchDto>  recentSearchDtoList = new ArrayList<RecentSearchDto>();
	
	RecentSearchDto recentSearchDto;
	
	@Before
	public void setupMock() {
		recentSearchDto = new RecentSearchDto("search", 2L, new Date());
		recentSearchDtoList.add(recentSearchDto);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getRecentSearchsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(recentSearchDtoList);
		List<RecentSearchDto> list = RecentSearchDao.getRecentSearchs(2L);
		assertEquals(1, list.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getRecentSearchs_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		RecentSearchDao.getRecentSearchs(2L);
	}
}
