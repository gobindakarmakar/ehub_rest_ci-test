package element.bst.elementexploration.rest.exception;

/**
 * 
 * @author Amit Patel
 *
 */
public class NoDataFoundException extends RuntimeException{

    /**
     * 
     */
    private static final long serialVersionUID = -5972445803860979255L;
    private  String message;
    
    public static final String CASE_SEED_NOT_FOUND_MSG="No data found";
	
	public NoDataFoundException(String message) {
		super();
		this.message = message;
	}



	public NoDataFoundException() {

		super();
		message = CASE_SEED_NOT_FOUND_MSG;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return message;
	}

}
