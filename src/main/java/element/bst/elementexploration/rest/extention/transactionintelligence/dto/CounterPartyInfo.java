package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Counter party information")
public class CounterPartyInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("Customer id")
	private Long customerId;
	
	@ApiModelProperty("Country id")
	private Long countryId;

	@ApiModelProperty("Country name")
	private String country;
	
	@ApiModelProperty("Transaction amount")
	private Double amount;
	
	@ApiModelProperty("Customer name")
	private String name;
	
	@ApiModelProperty("Country latitude")
	private String latitude;
	
	@ApiModelProperty("Country longitude")
	private String longitude;

	public CounterPartyInfo() {
		super();
	}

	public CounterPartyInfo(Long customerId,String name,Double amount) {
		super();
		this.customerId = customerId;
		this.amount = amount;
		this.name = name;

	}

	public CounterPartyInfo(String country,Long countryId,String latitude,String longitude, Double amount) {
		super();
		this.country = country;
		this.countryId = countryId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.amount = amount;


	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "CounterPartyInfo [customerId=" + customerId + ", countryId=" + countryId + ", country=" + country
				+ ", amount=" + amount + ", name=" + name + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

	

}
