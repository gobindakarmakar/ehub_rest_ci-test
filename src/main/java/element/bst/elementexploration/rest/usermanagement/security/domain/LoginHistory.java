package element.bst.elementexploration.rest.usermanagement.security.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * 
 * @author Prateek Maurya
 *
 */

@Entity
@Table(name = "bst_login_history")
public class LoginHistory implements Serializable{

	private static final long serialVersionUID = 6849884732508858002L;
	
	/**
	 * Primary Key. Login History Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_failed_login_id")
	private Long failedLoginId;
	
	/**
	 * Username or email address or ip address
	 */
	@Column(name = "username")
	private String username;
	
	@Column(name = "screen_name")
	private String screenName;
	
	/**
	 * Failed Login Attempt
	 */
	@Column(name = "failed_login_attempt")
	private Integer failedLoginAttempt;
	
	/**
     * Last Failed Login IP Address
     */
	@Column(name = "last_failed_login_ip_address", length = 255)
	private String lastFailedLoginIpAddress;
	
	/**
     * Last Failed Login Time
     */
	@Column(name = "last_failed_login_time")
	private Date lastFailedLoginTime;
	
	/**
     * Last Successful Login IP Address
     */
	@Column(name = "last_successful_login_ip_address", length = 255)
	private String lastSuccessfulLoginIpAddress;
	
	/**
     * Last Successful Login Time
     */
	@Column(name = "last_successful_login_time")
	private Date lastSuccessfulLoginTime;
	
	//Modification required fields as per new user management requirement
	@Column(name ="login_status", length = 10)
	private String loginStatus;
	
	@Column(name = "login_source", length = 255)
	private String loginSource;
	
	@Column(name = "login_time")
	private Date loginTime;
	
	@Column(name = "login_description", length = 255)
	private String loginDescription;
	
	@Column(name = "login_client_media", length = 255)
	private String loginClientMedia;
	
	@Column(name = "loginIpAddress", length = 255)
	private String loginIpAddress;
	
	@Column(name = "login_client_location", length = 255)
	private String loginClientLocation;
	
	@Column(name = "user_status", length = 255)
	private String userStatus;
	
	@Column(name = "user_id", length = 255)
	private Long userId;
	/**
	 * Constructor
	 */
	public LoginHistory() {
		
	}	

	public LoginHistory(Long failedLoginId, String username, String screenName, Integer failedLoginAttempt,
			String lastFailedLoginIpAddress, Date lastFailedLoginTime, String lastSuccessfulLoginIpAddress,
			Date lastSuccessfulLoginTime) {
		super();
		this.failedLoginId = failedLoginId;
		this.username = username;
		this.screenName = screenName;
		this.failedLoginAttempt = failedLoginAttempt;
		this.lastFailedLoginIpAddress = lastFailedLoginIpAddress;
		this.lastFailedLoginTime = lastFailedLoginTime;
		this.lastSuccessfulLoginIpAddress = lastSuccessfulLoginIpAddress;
		this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
	}

	public LoginHistory(Long failedLoginId, String username, String screenName, Integer failedLoginAttempt,
			String lastFailedLoginIpAddress, Date lastFailedLoginTime, String lastSuccessfulLoginIpAddress,
			Date lastSuccessfulLoginTime, String loginStatus, String loginSource, Date loginTime,
			String loginDescription, String loginClientMedia, String loginIpAddress, String loginClientLocation,
			String userStatus, Long userId) {
		super();
		this.failedLoginId = failedLoginId;
		this.username = username;
		this.screenName = screenName;
		this.failedLoginAttempt = failedLoginAttempt;
		this.lastFailedLoginIpAddress = lastFailedLoginIpAddress;
		this.lastFailedLoginTime = lastFailedLoginTime;
		this.lastSuccessfulLoginIpAddress = lastSuccessfulLoginIpAddress;
		this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
		this.loginStatus = loginStatus;
		this.loginSource = loginSource;
		this.loginTime = loginTime;
		this.loginDescription = loginDescription;
		this.loginClientMedia = loginClientMedia;
		this.loginIpAddress = loginIpAddress;
		this.loginClientLocation = loginClientLocation;
		this.userStatus = userStatus;
		this.userId = userId;
	}

	public Long getFailedLoginId() {
		return failedLoginId;
	}

	public void setFailedLoginId(Long failedLoginId) {
		this.failedLoginId = failedLoginId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Integer getFailedLoginAttempt() {
		return failedLoginAttempt;
	}

	public void setFailedLoginAttempt(Integer failedLoginAttempt) {
		this.failedLoginAttempt = failedLoginAttempt;
	}

	public String getLastFailedLoginIpAddress() {
		return lastFailedLoginIpAddress;
	}

	public void setLastFailedLoginIpAddress(String lastFailedLoginIpAddress) {
		this.lastFailedLoginIpAddress = lastFailedLoginIpAddress;
	}

	public Date getLastFailedLoginTime() {
		return lastFailedLoginTime;
	}

	public void setLastFailedLoginTime(Date lastFailedLoginTime) {
		this.lastFailedLoginTime = lastFailedLoginTime;
	}

	public String getLastSuccessfulLoginIpAddress() {
		return lastSuccessfulLoginIpAddress;
	}

	public void setLastSuccessfulLoginIpAddress(String lastSuccessfulLoginIpAddress) {
		this.lastSuccessfulLoginIpAddress = lastSuccessfulLoginIpAddress;
	}

	public Date getLastSuccessfulLoginTime() {
		return lastSuccessfulLoginTime;
	}

	public void setLastSuccessfulLoginTime(Date lastSuccessfulLoginTime) {
		this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
	}
	
	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(String loginSource) {
		this.loginSource = loginSource;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getLoginDescription() {
		return loginDescription;
	}

	public void setLoginDescription(String loginDescription) {
		this.loginDescription = loginDescription;
	}

	public String getLoginClientMedia() {
		return loginClientMedia;
	}

	public void setLoginClientMedia(String loginClientMedia) {
		this.loginClientMedia = loginClientMedia;
	}

	public String getLoginIpAddress() {
		return loginIpAddress;
	}

	public void setLoginIpAddress(String loginIpAddress) {
		this.loginIpAddress = loginIpAddress;
	}

	public String getLoginClientLocation() {
		return loginClientLocation;
	}

	public void setLoginClientLocation(String loginClientLocation) {
		this.loginClientLocation = loginClientLocation;
	}
	

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
