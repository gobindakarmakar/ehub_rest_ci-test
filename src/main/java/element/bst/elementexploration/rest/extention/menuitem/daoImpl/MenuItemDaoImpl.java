package element.bst.elementexploration.rest.extention.menuitem.daoImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.MenuItemDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.UserMenu;
import element.bst.elementexploration.rest.extention.menuitem.dto.UserMenuFinalDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author Jalagari Paul
 *
 */
@Repository("menuItemDao")
@Transactional("transactionManager")
public class MenuItemDaoImpl extends GenericDaoImpl<UserMenu, Long> implements MenuItemDao {

	public MenuItemDaoImpl() {
		super(UserMenu.class);
	}

	@Override
	public UserMenu getExistingUserMenu(UserMenuFinalDto userMenuFinalDto, Long userId) throws Exception {

		UserMenu userMenu = new UserMenu();
		Long idOfUser = userMenuFinalDto.getUserId();
		Long groupId = userMenuFinalDto.getGroupId();
		Long moduleId = userMenuFinalDto.getModuleId();

		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select um from UserMenu um where um.userId = :idOfUser ");
			if (moduleId != null)
				hql.append("and um.module.moduleId = :moduleId ");
			if (groupId != null)
				hql.append("and um.moduleGroup.moduleGroupId = :groupId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("idOfUser", idOfUser);
			if (moduleId != null)
				query.setParameter("moduleId", moduleId);
			query.setParameter("groupId", groupId);
			userMenu = (UserMenu) query.getSingleResult();
		} catch (Exception e) {
			return userMenu;
		}
		return userMenu;

	}

	@Override
	public int getMaxClickCountOfUserByGroup(Long groupId, Long idOfUser) throws Exception {
		int count = 0;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select max(um.clicksCount) from UserMenu um where um.userId = :idOfUser ");
			if (groupId != null)
				hql.append("and um.moduleGroup.moduleGroupId = :groupId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("idOfUser", idOfUser);
			query.setParameter("groupId", groupId);
			count = (Integer) query.getSingleResult();
		} catch (Exception e) {
			return count;
		}
		return count;
	}

}
