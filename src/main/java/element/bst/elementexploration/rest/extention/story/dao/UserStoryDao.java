package element.bst.elementexploration.rest.extention.story.dao;

import element.bst.elementexploration.rest.extention.story.domain.UserStory;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface UserStoryDao extends GenericDao<UserStory, Long> {

	UserStory getFavouriteStoryByUserId(Long userId);

}
