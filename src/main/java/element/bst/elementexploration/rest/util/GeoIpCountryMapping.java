package element.bst.elementexploration.rest.util;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.stereotype.Component;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.Country;

import element.bst.elementexploration.rest.constants.LoopBackConstants;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserSetting;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Amit Patel	
 *
 */
@Component
public class GeoIpCountryMapping {

	
	
	File database = null;
	
	public GeoIpCountryMapping() {
		ClassLoader classLoader = getClass().getClassLoader();
		database = new File(classLoader.getResource("geodb/Country.mmdb").getFile());
	}

	public UserSetting getCountryMapping(String ip)  {
		
		DatabaseReader reader = null;
		UserSetting userSetting = new UserSetting();
		
		if (LoopBackConstants.LOOP_BACK.contains(ip)) {
			userSetting.setCountry("Localhost");
			userSetting.setIsoCode("Localhost");
			return userSetting;
		}
		
		try {
			reader = new DatabaseReader.Builder(database).build();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		InetAddress ipAddress = null;
		try {
			ipAddress = InetAddress.getByName(ip);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} 
		CountryResponse response = null;
		try {
			if(reader!=null)
				response = reader.country(ipAddress);
		} catch (AddressNotFoundException an) {
			
		} catch (GeoIp2Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		} catch (IOException iox) {
			iox.printStackTrace();
		}

		if (response != null) {
			Country country = response.getCountry();
			userSetting.setCountry(country.getName());
			userSetting.setIsoCode(country.getIsoCode());
		} else {
			userSetting.setCountry("NOTFOUND");
			userSetting.setIsoCode("NOTFOUND");
		}
		
		return userSetting;
	}

}
