package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourceDomainDao extends GenericDao<SourceDomain, Long> {
	
	SourceDomain  fetchDomain(String name);

}
