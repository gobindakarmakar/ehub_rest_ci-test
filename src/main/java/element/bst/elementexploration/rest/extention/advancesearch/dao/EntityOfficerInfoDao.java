package element.bst.elementexploration.rest.extention.advancesearch.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityOfficerInfo;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface EntityOfficerInfoDao extends GenericDao<EntityOfficerInfo, Long>{

	EntityOfficerInfo getOfficer(EntityOfficerInfoDto entityOfficers);

	List<EntityOfficerInfo> getOfficerList(String entityId);

}