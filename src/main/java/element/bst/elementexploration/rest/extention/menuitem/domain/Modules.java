package element.bst.elementexploration.rest.extention.menuitem.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_menu_items")
public class Modules implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menuId")
	private Long moduleId;

	@ApiModelProperty(value = "Menu Item Module")
	@Column(name = "menuName")
	private String moduleName;

	@ApiModelProperty(value = "Menu Item Module")
	@Column(name = "menuIcon", columnDefinition = "LONGTEXT")
	private String moduleIcon;

	@ApiModelProperty(value = "Menu Item Class")
	@Column(name = "menuClass", columnDefinition = "LONGTEXT")
	private String moduleClass;

	@ApiModelProperty(value = "Menu Item Url")
	@Column(name = "menuUrl", columnDefinition = "LONGTEXT")
	private String menuUrl;

	@ApiModelProperty(value = "Menu Item Code(for permissions)")
	@Column(name = "menuCode", columnDefinition = "LONGTEXT")
	private String menuCode;

	@ApiModelProperty(value = "Localization ID")
	@Column(name = "localizationId")
	private Integer localizationId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnore
	@JoinColumn(name = "menuGroupId")
	private ModulesGroup moduleGroup;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "permissionId")
	private Permissions permissionId;
	
	/*@ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name = "bst_menu_domains", joinColumns = { @JoinColumn(name = "menuId") }, inverseJoinColumns = {
			@JoinColumn(name = "domainId") })
	private List<Domains> domains;*/

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleIcon() {
		return moduleIcon;
	}

	public void setModuleIcon(String moduleIcon) {
		this.moduleIcon = moduleIcon;
	}

	public ModulesGroup getModuleGroup() {
		return moduleGroup;
	}

	public void setModuleGroup(ModulesGroup moduleGroup) {
		this.moduleGroup = moduleGroup;
	}

	public Integer getLocalizationId() {
		return localizationId;
	}

	public void setLocalizationId(Integer localizationId) {
		this.localizationId = localizationId;
	}

	public Permissions getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Permissions permissionId) {
		this.permissionId = permissionId;
	}

	public String getModuleClass() {
		return moduleClass;
	}

	public void setModuleClass(String moduleClass) {
		this.moduleClass = moduleClass;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	/*public List<Domains> getDomains() {
		return domains;
	}

	public void setDomains(List<Domains> domains) {
		this.domains = domains;
	}*/

	
}
