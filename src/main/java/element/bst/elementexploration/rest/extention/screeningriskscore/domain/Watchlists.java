package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class Watchlists implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "name")
	private String name;
	@ApiModelProperty(value = "type")
	private String type;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	

}
