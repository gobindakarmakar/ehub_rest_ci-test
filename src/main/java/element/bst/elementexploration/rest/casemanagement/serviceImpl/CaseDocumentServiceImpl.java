package element.bst.elementexploration.rest.casemanagement.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDocumentDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocDetailsDTO;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.service.CaseDocumentService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.FailedToUploadCaseSeedDocumentException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.LoggerUtil;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("caseDocumentService")
public class CaseDocumentServiceImpl extends GenericServiceImpl<CaseDocDetails, Long> implements CaseDocumentService {

	@Autowired
	private CaseDocumentDao caseDocumentDao;

	@Autowired
	private CaseDao caseDao;
	
	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private UsersService usersService;
	
	
	@Autowired
	private AuditLogDao auditLogDao;

	@Value("${bigdata_create_doc_url}")
	private String COMMON_DOC_BIGDATA_URL;
	
	public CaseDocumentServiceImpl(GenericDao<CaseDocDetails, Long> genericDao){
		super(genericDao);
		
	}

	@Override
	@Transactional("transactionManager")
	public Long uploadCaseDocument(MultipartFile uploadDoc, Long seedId, String fileTitle, String remarks,
			long userId,Integer docFlag) throws Exception {
		
		if (uploadDoc == null || uploadDoc.getSize() <= 0) {
			throw new FailedToUploadCaseSeedDocumentException("File cannot be empty");
		}
		Case caseSeed = caseDao.find(seedId);

		if(caseSeed != null){
			String originalFile = uploadDoc.getOriginalFilename();
			String ext = FilenameUtils.getExtension(originalFile);
			String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
			File file = null;
			file = File.createTempFile(fileNameWithoutExtension, ext);
			uploadDoc.transferTo(file);
			String serverResponse[] = ServiceCallHelper.postFileToServer(COMMON_DOC_BIGDATA_URL+"/"+seedId +"/doc", file);
			if (Integer.parseInt(serverResponse[0]) == 200 &&
					!StringUtils.isEmpty(serverResponse[1]) &&  serverResponse[1].trim().length() > 0) {
				//String originalFile = uploadDoc.getOriginalFilename();
				String details[] = originalFile.split("\\.");
				int size = details.length;
				String fileType = details[size - 1];
				CaseDocDetails docDetails = new CaseDocDetails();
				docDetails.setFileTitle(fileTitle);
				docDetails.setFileSize(new Double(uploadDoc.getSize()));
				docDetails.setCaseSeed(caseSeed);
				docDetails.setRemarks(remarks);
				docDetails.setUploadedOn(new Date());
				docDetails.setFileType(fileType);
				docDetails.setFilePath(originalFile);
				docDetails.setDocGuuidFromServer(serverResponse[1]);
				docDetails.setUploadedBy(userId);
				docDetails.setDocFlag(docFlag != null ? docFlag : 0);
				docDetails = caseDocumentDao.create(docDetails);
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, "CASE", null, "Case document uploaded");
				//User author = userDao.find(userId);
				Users author = usersDao.find(userId);
				author = usersService.prepareUserFromFirebase(author);
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " uploaded document " + docDetails.getFileTitle() + " to case " + caseSeed.getName());
				log.setCaseId(caseSeed.getCaseId());
				log.setUserId(userId);
				log.setName(caseSeed.getName());
				log.setDocumentId(docDetails.getDocId());
				auditLogDao.create(log);
				return docDetails.getDocId();
			}else{
				throw new Exception(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
			}
		}else{
			throw new NoDataFoundException(ElementConstants.CASE_SEED_NOT_FOUND);
		}
	}

	@Override
	@Transactional("transactionManager")
	public CaseDocDetailsDTO getCaseDocumentDetailsById(Long docId) {
		CaseDocDetailsDTO bstDocDetailsDTO = new CaseDocDetailsDTO();
		CaseDocDetails bstDocDetails = caseDocumentDao.find(docId);
		if (bstDocDetails != null) {
			BeanUtils.copyProperties(bstDocDetails, bstDocDetailsDTO);
			return bstDocDetailsDTO;
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public void updateCaseDocument(String fileTitle, Long docId, String remarks, long userId)
			throws DocNotFoundException {
		CaseDocDetails docDetails = caseDocumentDao.find(docId);
		if (docDetails != null) {
			docDetails.setFileTitle(fileTitle);
			docDetails.setRemarks(remarks);
			docDetails.setModifiedOn(new Date());
			docDetails.setModifiedBy(userId);
			caseDocumentDao.saveOrUpdate(docDetails);
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public void deleteCaseDocumentById(Long docId, Long userId) {
		CaseDocDetails docDetails = caseDocumentDao.find(docId);
		if (docDetails != null) {
			caseDocumentDao.delete(docDetails);
			AuditLog log = new AuditLog(new Date(), LogLevels.WARNING, "CASE", null, "Case document deleted");
			Users author = usersDao.find(userId);
			author = usersService.prepareUserFromFirebase(author);
			log.setDescription(author.getFirstName() + " " + author.getLastName() + " deleted document " + docDetails.getFileTitle() + " from case " + docDetails.getCaseSeed().getName());
			log.setCaseId(docDetails.getCaseSeed().getCaseId());
			log.setUserId(userId);
			log.setName(docDetails.getCaseSeed().getName());
			log.setDocumentId(docDetails.getDocId());
			auditLogDao.create(log);
		}else{
			throw new DocNotFoundException();
		}
	}

	@Override
	public byte[] downloadCaseDocument(String docUUID) throws Exception {
		String url = COMMON_DOC_BIGDATA_URL + "/doc/" + docUUID;
		byte[] fileData = ServiceCallHelper.downloadFileFromServer(url);
		if(fileData != null){
			return fileData;
		}else{
			throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<CaseDocDetailsDTO> getAllDocumentsForCase(Long caseSeedId, Integer docFlag,String orderBy,Integer pageNumber, Integer recordsPerPage) {
		Case caseSeed = caseDao.find(caseSeedId);
		if (caseSeed != null) {
			List<CaseDocDetailsDTO> caseSeedDocumentsDTO = new ArrayList<CaseDocDetailsDTO>();
			List<CaseDocDetails> caseSeedDocuments = null;
			caseSeedDocuments = caseDocumentDao.getAllDocumentsForCaseSeed(caseSeed.getCaseId(),docFlag, orderBy,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
			if (!caseSeedDocuments.isEmpty()) {
				for (CaseDocDetails DocDetails : caseSeedDocuments) {
					CaseDocDetailsDTO bstDocDetailsDTO = new CaseDocDetailsDTO();
					BeanUtils.copyProperties(DocDetails, bstDocDetailsDTO);
					caseSeedDocumentsDTO.add(bstDocDetailsDTO);
				}
			} 
			return caseSeedDocumentsDTO;
		} else {
			throw new CaseSeedNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public Long getCaseIdFromDoc(long docId) {
		return caseDocumentDao.getCaseIdFromDoc(docId);		
	}

	@Override
	@Transactional("transactionManager")
	public Long countAllDocumentsForCase(Long caseSeedId,Integer docFlag) {
		return caseDocumentDao.countAllDocumentsForCaseSeed(caseSeedId,docFlag);
	}

	@Override
	@Transactional("transactionManager")
	public List<DocAggregatorDto> docAggregator(Long caseId,String fromDate,String toDate) throws ParseException {
		Date fromDateTemp = null;
		Date toDateTemp = null;
		if (fromDate != null && fromDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(fromDate)) {
				fromDateTemp = LoggerUtil.convertToDateddMMyyyy(fromDate);
			} else {
				throw new DateFormatException();
			}
		}
		if (toDate != null && toDate.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(toDate)) {
				toDateTemp = LoggerUtil.convertToDateddMMyyyy(toDate);
			} else {
				throw new DateFormatException();
			}
		}
		return caseDocumentDao.docAggregator(caseId,fromDateTemp,toDateTemp);
	}
	
	@Override
	@Transactional("transactionManager")
	public void updateDocumentContent(MultipartFile uploadDoc, Long userId, Long docId) throws IOException {

		if (uploadDoc == null || uploadDoc.getSize() <= 0) {
			throw new FailedToUploadCaseSeedDocumentException("File cannot be empty");
		}
		CaseDocDetails caseDocDetails = caseDocumentDao.find(docId);
		if (caseDocDetails != null) {
			String originalFile = uploadDoc.getOriginalFilename();
			String ext = FilenameUtils.getExtension(originalFile);
			String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
			File file = null;
			file = File.createTempFile(fileNameWithoutExtension, ext);
			uploadDoc.transferTo(file);
			String serverResponse[] = ServiceCallHelper.postFileToServer(COMMON_DOC_BIGDATA_URL+"/"+caseDocDetails.getCaseSeed().getCaseId() +"/doc", file);
			if (Integer.parseInt(serverResponse[0]) == 200 && serverResponse[1] != null
					&& serverResponse[1].trim().length() > 0) {
				caseDocDetails.setDocGuuidFromServer(serverResponse[1]);
				caseDocDetails.setFileSize(new Double(uploadDoc.getSize()));
				caseDocDetails.setModifiedOn(new Date());
				caseDocDetails.setModifiedBy(userId);
				caseDocDetails.setFileType(ext);
				caseDocDetails.setFilePath(originalFile);
				caseDocumentDao.saveOrUpdate(caseDocDetails);
			} else {
				throw new FailedToUploadCaseSeedDocumentException(
						ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
			}
		} else {
			throw new DocNotFoundException();
		}
	}
	
	
	

	@Override
	@Transactional("transactionManager")
	public byte[] downloadAnnualReturnsDocument(String url) throws Exception {
		byte[] fileData = ServiceCallHelper.downloadFileFromServerWithoutTimeout(url);
		if(fileData != null){
			return fileData;
		}else{
			throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
		}
	}

}
