package element.bst.elementexploration.test.notification.daoImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.notification.daoImpl.NotificationCheckDaoImpl;
import element.bst.elementexploration.rest.notification.domain.NotificationCheck;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NotificationCheckDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<NotificationCheck> root;

	@InjectMocks
	NotificationCheckDaoImpl notificationCheckDao;

	NotificationCheck notificationCheck;

	List<NotificationCheck> notificationChecksList = new ArrayList<>();

	@Before
	public void setupMock() {
		notificationCheck = new NotificationCheck();
		notificationCheck.setTimestamp(LocalDateTime.now());
		notificationChecksList.add(notificationCheck);
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void checkInNotificationTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationCheck.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationChecksList);
		Mockito.doNothing().when(session).saveOrUpdate(Mockito.any(NotificationCheck.class));
		LocalDateTime checkIn = notificationCheckDao.checkInNotification(1L);
		Assert.assertNotNull(checkIn);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void checkInNotification_whenNotificationIsNullTest() {
		notificationChecksList.set(0, null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationCheck.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationChecksList);
		Mockito.doNothing().when(session).saveOrUpdate(Mockito.any(NotificationCheck.class));
		LocalDateTime checkIn = notificationCheckDao.checkInNotification(1L);
		Assert.assertNotNull(checkIn);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void checkInNotification_whenFailedToExecuteQueryExceptionThrowTest() {
		notificationChecksList.set(0, null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationCheck.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		Mockito.doNothing().when(session).saveOrUpdate(Mockito.any(NotificationCheck.class));
		notificationCheckDao.checkInNotification(1L);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getLastCheckInTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationCheck.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationChecksList);
		LocalDateTime checkIn = notificationCheckDao.getLastCheckIn(1L);
		Assert.assertNotNull(checkIn);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getLastCheckIn_whenFailedToExecuteQueryExceptionThrowTest() {
		notificationChecksList.set(0, null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationCheck.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationCheckDao.getLastCheckIn(1L);
	}

}
