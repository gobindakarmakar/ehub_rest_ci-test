package element.bst.elementexploration.rest.extention.sourcecredibility.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_data_attributes")
public class DataAttributes implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long attributeId;

	@Column(name = "sourceAttributeName")
	private String sourceAttributeName;

	@Column(name = "sourceAttributeSchemaGroup")
	private String sourceAttributeSchemaGroup;

	@Column(name = "sourceAttributeSchema")
	private String sourceAttributeSchema;

	@ManyToOne
	@JoinColumn(name = "sub_classifications_id")
	private SubClassifications subClassifications;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "dataAttributes")
	private List<SourceCredibility> sourceCredibilityList;

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public String getSourceAttributeName() {
		return sourceAttributeName;
	}

	public void setSourceAttributeName(String sourceAttributeName) {
		this.sourceAttributeName = sourceAttributeName;
	}

	public String getSourceAttributeSchemaGroup() {
		return sourceAttributeSchemaGroup;
	}

	public void setSourceAttributeSchemaGroup(String sourceAttributeSchemaGroup) {
		this.sourceAttributeSchemaGroup = sourceAttributeSchemaGroup;
	}

	public String getSourceAttributeSchema() {
		return sourceAttributeSchema;
	}

	public void setSourceAttributeSchema(String sourceAttributeSchema) {
		this.sourceAttributeSchema = sourceAttributeSchema;
	}

	public SubClassifications getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(SubClassifications subClassifications) {
		this.subClassifications = subClassifications;
	}

	public List<SourceCredibility> getSourceCredibilityList() {
		return sourceCredibilityList;
	}

	public void setSourceCredibilityList(List<SourceCredibility> sourceCredibilityList) {
		this.sourceCredibilityList = sourceCredibilityList;
	}

}
