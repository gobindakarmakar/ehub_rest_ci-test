package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author rambabu
 *
 */
@ApiModel("Table rows list")
public class DataTableRowsDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Table column data")
	private List<DataTableColumsDto> dataTableColumsDto;

	public DataTableRowsDto() {
		super();

	}

	public DataTableRowsDto(List<DataTableColumsDto> dataTableColumsDto) {
		super();
		this.dataTableColumsDto = dataTableColumsDto;
	}

	public List<DataTableColumsDto> getDataTableColumsDto() {
		return dataTableColumsDto;
	}

	public void setDataTableColumsDto(List<DataTableColumsDto> dataTableColumsDto) {
		this.dataTableColumsDto = dataTableColumsDto;
	}

	@Override
	public String toString() {
		return "DataTableRowsDto [dataTableColumsDto=" + dataTableColumsDto + "]";
	}

}
