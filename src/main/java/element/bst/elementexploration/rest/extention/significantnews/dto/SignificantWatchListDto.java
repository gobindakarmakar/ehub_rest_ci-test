package element.bst.elementexploration.rest.extention.significantnews.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SignificantWatchListDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@JsonProperty(required = true)
	private String mainEntityId;
	private String identifier;
	@JsonProperty(required = true)
	private String entityName;
	@JsonProperty(required = true)
	private String value;
	@JsonProperty(required = true)
	private String name;
	private Long userId;
	@JsonProperty(value="pep",required = false)
	private Boolean isPep;
	@JsonProperty(value="sanction",required = false)
	private Boolean isSanction;
	private String pepComment;
	private String sanctionComment;

	public SignificantWatchListDto() {
		super();
	}

	public SignificantWatchListDto(Long id, String mainEntityId, String identifier, String entityName, String value,
			String name, Long userId, Boolean isPep, Boolean isSanction, String pepComment, String sanctionComment) {
		super();
		this.id = id;
		this.mainEntityId = mainEntityId;
		this.identifier = identifier;
		this.entityName = entityName;
		this.value = value;
		this.name = name;
		this.userId = userId;
		this.isPep = isPep;
		this.isSanction = isSanction;
		this.pepComment = pepComment;
		this.sanctionComment = sanctionComment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMainEntityId() {
		return mainEntityId;
	}

	public void setMainEntityId(String mainEntityId) {
		this.mainEntityId = mainEntityId;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getIsPep() {
		return isPep;
	}

	public void setIsPep(Boolean isPep) {
		this.isPep = isPep;
	}

	public Boolean getIsSanction() {
		return isSanction;
	}

	public void setIsSanction(Boolean isSanction) {
		this.isSanction = isSanction;
	}

	public String getPepComment() {
		return pepComment;
	}

	public void setPepComment(String pepComment) {
		this.pepComment = pepComment;
	}

	public String getSanctionComment() {
		return sanctionComment;
	}

	public void setSanctionComment(String sanctionComment) {
		this.sanctionComment = sanctionComment;
	}

}
