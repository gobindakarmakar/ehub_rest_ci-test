package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class TxEntityRespDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String originatorAccountId;
	private String beneficiaryAccountId;
	@JsonProperty("product-type")
	private String transProductType;
	@JsonProperty("channel")
	private String transactionChannel;

	@JsonProperty("amount-base")
	private double amount;

	@JsonProperty("amount-activity-org")
	private double amountOrig;

	@JsonProperty("amount-activity-bene")
	private double amountBene;

	@JsonProperty("currency")
	private String currency;
	@JsonProperty("currency-org")
	private String currencyOrg;
	@JsonProperty("currency-bene")
	private String currencyBen;

	@JsonProperty("date")
	private Date businessDate;

	@JsonProperty("date-org")
	private Date dateOrg;

	@JsonProperty("date-bene")
	private Date dateBene;

	@JsonProperty("custom-text2")
	private String customText;

	@JsonProperty("country-bene")
	private String countryBene;

	@JsonProperty("country-org")
	private String countryOrg;

	@JsonProperty("name-bene")
	private String beneficiaryName;
	@JsonProperty("name-org")
	private String originatorName;

	@JsonProperty("debit-credit")
	private String debitOrCredit;

	@JsonProperty("party-identifier")
	private String primaryCustomerIdentifier;

	@JsonProperty("id")
	private long id;
	@JsonProperty("tx-id-org")
	private Long orgId;
	@JsonProperty("tx-id-bene")
	private Long beneId;
	
	private boolean isAlertedTransaction;
	private boolean isAlert;
	
	private String merchant;
	private String merchantWebsite;

	public String getOriginatorAccountId() {
		return originatorAccountId;
	}

	public void setOriginatorAccountId(String originatorAccountId) {
		this.originatorAccountId = originatorAccountId;
	}

	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	public String getTransProductType() {
		return transProductType;
	}

	public void setTransProductType(String transProductType) {
		this.transProductType = transProductType;
	}

	public String getTransactionChannel() {
		return transactionChannel;
	}

	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getAmountOrig() {
		return amountOrig;
	}

	public void setAmountOrig(double amountOrig) {
		this.amountOrig = amountOrig;
	}

	public double getAmountBene() {
		return amountBene;
	}

	public void setAmountBene(double amountBene) {
		this.amountBene = amountBene;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrencyOrg() {
		return currencyOrg;
	}

	public void setCurrencyOrg(String currencyOrg) {
		this.currencyOrg = currencyOrg;
	}

	public String getCurrencyBen() {
		return currencyBen;
	}

	public void setCurrencyBen(String currencyBen) {
		this.currencyBen = currencyBen;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public Date getDateOrg() {
		return dateOrg;
	}

	public void setDateOrg(Date dateOrg) {
		this.dateOrg = dateOrg;
	}

	public Date getDateBene() {
		return dateBene;
	}

	public void setDateBene(Date dateBene) {
		this.dateBene = dateBene;
	}

	public String getCustomText() {
		return customText;
	}

	public void setCustomText(String customText) {
		this.customText = customText;
	}

	public String getCountryBene() {
		return countryBene;
	}

	public void setCountryBene(String countryBene) {
		this.countryBene = countryBene;
	}

	public String getCountryOrg() {
		return countryOrg;
	}

	public void setCountryOrg(String countryOrg) {
		this.countryOrg = countryOrg;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getOriginatorName() {
		return originatorName;
	}

	public void setOriginatorName(String originatorName) {
		this.originatorName = originatorName;
	}

	public String getDebitOrCredit() {
		return debitOrCredit;
	}

	public void setDebitOrCredit(String debitOrCredit) {
		this.debitOrCredit = debitOrCredit;
	}

	public String getPrimaryCustomerIdentifier() {
		return primaryCustomerIdentifier;
	}

	@JsonProperty("is-alerted-transaction")
	public boolean isAlertedTransaction() {
		return isAlertedTransaction;
	}

	public void setAlertedTransaction(boolean isAlertedTransaction) {
		this.isAlertedTransaction = isAlertedTransaction;
	}

	public void setPrimaryCustomerIdentifier(String primaryCustomerIdentifier) {
		this.primaryCustomerIdentifier = primaryCustomerIdentifier;
	}

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getBeneId() {
		return beneId;
	}

	public void setBeneId(Long beneId) {
		this.beneId = beneId;
	}

	public boolean isAlert() {
		return isAlert;
	}

	public void setAlert(boolean isAlert) {
		this.isAlert = isAlert;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

	
	

}
