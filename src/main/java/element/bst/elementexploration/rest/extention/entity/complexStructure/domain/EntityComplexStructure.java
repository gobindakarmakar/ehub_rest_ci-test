package element.bst.elementexploration.rest.extention.entity.complexStructure.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author hanuman
 *
 */
@ApiModel("Entity Complex Structure")
@Entity
@Table(name = "entity_complex_structure")
public class EntityComplexStructure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long enity_complex_structure_id;
	
	@ApiModelProperty(value = "ID if the Entity")
	@Column(name = "entity_id")
	private String entityId;
	
	@ApiModelProperty(value = "Name if the Entity")
	@Column(name = "entity_name")
	private String entityName;

	@ApiModelProperty(value = "Source if the Entity")
	@Column(name = "entity_source")
	private String entitySource;

	@ApiModelProperty(value = "Source if the Entity")
	@Column(name = "is_complex_structure")
	private Boolean isComplexStructure;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntitySource() {
		return entitySource;
	}

	public void setEntitySource(String entitySource) {
		this.entitySource = entitySource;
	}

	public Boolean getIsComplexStructure() {
		return isComplexStructure;
	}

	public void setIsComplexStructure(Boolean isComplexStructure) {
		this.isComplexStructure = isComplexStructure;
	}

	public Long getEnity_complex_structure_id() {
		return enity_complex_structure_id;
	}

	public void setEnity_complex_structure_id(Long enity_complex_structure_id) {
		this.enity_complex_structure_id = enity_complex_structure_id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	
	

}
