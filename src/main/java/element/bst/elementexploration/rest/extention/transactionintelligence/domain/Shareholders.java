package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Paul Jalagari
 * 
 */
@Entity
@Table(name = "SHAREHOLDERS")
public class Shareholders {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String shareHolderName;

	private String customerNumber;

	private Integer NumberOfShares;

	@Column(name = "NUMBER_OF_SHARES")
	public Integer getNumberOfShares() {
		return NumberOfShares;
	}

	public void setNumberOfShares(Integer numberOfShares) {
		NumberOfShares = numberOfShares;
	}

	@Column(name = "SHAREHOLDER_NAME")
	public String getShareHolderName() {
		return shareHolderName;
	}

	public void setShareHolderName(String shareHolderName) {
		this.shareHolderName = shareHolderName;
	}

	@Column(name = "CUSTOMER_NUMBER")
	public String getCustomerNumber() {
				return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
			this.customerNumber = customerNumber;
	}

}
