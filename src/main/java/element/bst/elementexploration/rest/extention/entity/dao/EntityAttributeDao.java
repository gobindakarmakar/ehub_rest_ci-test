package element.bst.elementexploration.rest.extention.entity.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface EntityAttributeDao extends GenericDao<EntityAttributes, Long> {

	EntityAttributes get(String entityId, String sourceSchema);

	List<EntityAttributes> getAttributeList(String entityId, String sourceSchema);

}
