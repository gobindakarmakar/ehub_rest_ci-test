package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Matches implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "vcard family-name")
	@JsonProperty("vcard:family-name")
	private List<RiskFieldName> vcardFamilyName;
	
	@ApiModelProperty(value = "vcardgiven-name")
	@JsonProperty("vcard:given-name")
	private List<RiskFieldName> vcardGivenName;
	
	@ApiModelProperty(value = "Place of birth")
	@JsonProperty("POB")
	private List<RiskFieldName> pob;
	
	@ApiModelProperty(value = "Date of birth")
	@JsonProperty("DOB")
	private List<RiskFieldName> dob;
	
	@ApiModelProperty(value = "City")
	@JsonProperty("city")
	private List<RiskFieldName> city;
	
	@ApiModelProperty(value = "Country name")
	@JsonProperty("country-name")
	private List<RiskFieldName> countryName;
	
	@ApiModelProperty(value = "Country iso code")
	@JsonProperty("country-iso-code")
	private List<RiskFieldName> countryIsoCode;
	
	@ApiModelProperty(value = "Organization name")
	@JsonProperty("organization-name")
	private List<RiskFieldName> organizationName;
	
	@ApiModelProperty(value = "whole name")
	@JsonProperty("whole-name")
	private List<RiskFieldName> wholeName;

	public List<RiskFieldName> getVcardFamilyName() {
		return vcardFamilyName;
	}

	public void setVcardFamilyName(List<RiskFieldName> vcardFamilyName) {
		this.vcardFamilyName = vcardFamilyName;
	}

	public List<RiskFieldName> getVcardGivenName() {
		return vcardGivenName;
	}

	public void setVcardGivenName(List<RiskFieldName> vcardGivenName) {
		this.vcardGivenName = vcardGivenName;
	}

	public List<RiskFieldName> getPob() {
		return pob;
	}

	public void setPob(List<RiskFieldName> pob) {
		this.pob = pob;
	}

	public List<RiskFieldName> getDob() {
		return dob;
	}

	public void setDob(List<RiskFieldName> dob) {
		this.dob = dob;
	}

	public List<RiskFieldName> getCity() {
		return city;
	}

	public void setCity(List<RiskFieldName> city) {
		this.city = city;
	}

	public List<RiskFieldName> getCountryName() {
		return countryName;
	}

	public void setCountryName(List<RiskFieldName> countryName) {
		this.countryName = countryName;
	}

	public List<RiskFieldName> getCountryIsoCode() {
		return countryIsoCode;
	}

	public void setCountryIsoCode(List<RiskFieldName> countryIsoCode) {
		this.countryIsoCode = countryIsoCode;
	}

	public List<RiskFieldName> getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(List<RiskFieldName> organizationName) {
		this.organizationName = organizationName;
	}

	public List<RiskFieldName> getWholeName() {
		return wholeName;
	}

	public void setWholeName(List<RiskFieldName> wholeName) {
		this.wholeName = wholeName;
	}

	

	
	
}
