package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ISO_CODES")
public class IsoCodes implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String isoCode;
	private DocumentISOCodes documentISOCodes;
	
	public IsoCodes() {
	}

	public IsoCodes(String isoCode, DocumentISOCodes documentISOCodes) {
		super();
		this.isoCode = isoCode;
		this.documentISOCodes = documentISOCodes;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name="ISO_CODE")
	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	
	@ManyToOne
	@JoinColumn(name="DOCUMENT_ISO_CODE_ID")
	public DocumentISOCodes getDocumentISOCodes() {
		return documentISOCodes;
	}

	public void setDocumentISOCodes(DocumentISOCodes documentISOCodes) {
		this.documentISOCodes = documentISOCodes;
	}

}
