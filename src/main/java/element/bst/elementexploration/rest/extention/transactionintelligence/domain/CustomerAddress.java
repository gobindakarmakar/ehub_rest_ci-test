package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AbstractDomainObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AddressType;

/**
 * <h1>CustomerAddress</h1>
 * <p>
 * This class is used to store the address information of individual customer.
 * </p>
 * 
 * @author suresh
 */
@Entity
@Table(name = "CUSTOMER_ADDRESS")
public class CustomerAddress extends AbstractDomainObject {

	private static final long serialVersionUID = 1L;

	private Long id;
	private AddressType addressType;
	private String addressLine;
	private String street;
	private String country;
	private String city;
	private String state;
	private String postalCode;
	private CustomerDetails customerDetails;

	public CustomerAddress() {

	}

	public CustomerAddress(Long id, AddressType addressType, String addressLine, String street, String country,
			String city, String state, String postalCode) {
		super();
		this.id = id;
		this.addressType = addressType;
		this.addressLine = addressLine;
		this.street = street;
		this.country = country;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ADDRESS_TYPE")
	public AddressType getAddressType() {
		return addressType;
	}

	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}

	@Column(name = "ADDRESS_LINE")
	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	@Column(name = "CITY")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "STATE")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "POSTAL_CODE")
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Column(name = "STREET")
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Column(name = "COUNTRY")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	@ManyToOne
	@JoinColumn(name="CUSTOMER_ID")
	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}



}
