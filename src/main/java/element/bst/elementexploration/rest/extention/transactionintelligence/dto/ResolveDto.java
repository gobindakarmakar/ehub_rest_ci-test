package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class ResolveDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String alertId;
	private String comment;
	public String getAlertId() {
		return alertId;
	}
	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
