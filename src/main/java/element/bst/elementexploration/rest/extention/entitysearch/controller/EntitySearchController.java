package element.bst.elementexploration.rest.extention.entitysearch.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.entitysearch.dto.AritcleNewsInfoResponseDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.ArticleDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.PeerResult;
import element.bst.elementexploration.rest.extention.entitysearch.service.EntitySearchService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Entity search API" })
@RestController
@RequestMapping("/api/entity")
public class EntitySearchController extends BaseController {

	@Autowired
	private EntitySearchService entitySearchService;

	@ApiOperation("Searches for generic organization entity profiles")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/org/search", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> searchOrg(@RequestBody String jsonString, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.searchOrg(jsonString), HttpStatus.OK);
	}

	/*
	 * @GetMapping(value = "/person/search", produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * searchPerson(@RequestParam(required = false) String
	 * query, @RequestParam(required = false) Long size,
	 * 
	 * @RequestParam(required = false) Long from) throws Exception { return new
	 * ResponseEntity<>(entitySearchService.searchPerson(query, size, from),
	 * HttpStatus.OK); }
	 */
	@ApiOperation("Searches for generic person entity profiles")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/person/search", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> searchPerson(@RequestParam String token, @RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(entitySearchService.searchPerson(jsonString), HttpStatus.OK);
	}

	/*
	 * @ApiOperation("Lists entities of a type by ID")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, response = String.class,
	 * message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	 * 
	 * @GetMapping(value = "/lists/{entity-type}/{entityId}", produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * entityLists(@PathVariable("entity-type") String entityType,
	 * 
	 * @PathVariable("entityId") String entityId, @RequestParam String token) throws
	 * Exception { return new
	 * ResponseEntity<>(entitySearchService.entityLists(entityType, entityId),
	 * HttpStatus.OK); }
	 */

	@ApiOperation("Given a organisation description returns its ID in the repository> Either 'url' or 'name' property must be present. Stores organisation description if there was no such organisation")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/org/get-or-create", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrganizationOrCreateOrganizations(@RequestParam String token,
			@RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(entitySearchService.getOrganizationOrCreateOrganizations(jsonString),
				HttpStatus.OK);
	}

	@ApiOperation("Given a person description returns its ID in the repository> Either 'url' or 'name' property must be present. Stores person description if there was no such person")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/person/get-or-create", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getPersonOrCreatePerson(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(entitySearchService.getPersonOrCreatePerson(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Tries to suggest organisation names for a given query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/org/suggest-name", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrganizationBySuggestedName(@RequestParam(required = false) String query,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getOrganizationBySuggestedName(query), HttpStatus.OK);
	}

	@ApiOperation("Tries to suggest person names for a given query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/person/suggest-name", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPersonBySuggestedName(@RequestParam(required = false) String query,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getPersonBySuggestedName(query), HttpStatus.OK);
	}

	@ApiOperation("Tries to suggest organisation names for a given query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/person/suggest-name-loc", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrgNamesSuggestions(@RequestParam String token,
			@RequestParam(required = false) String query, @RequestParam(required = false) String locationCode)
			throws Exception {
		return new ResponseEntity<>(entitySearchService.getOrgNamesSuggestions(query, locationCode), HttpStatus.OK);
	}

	@ApiOperation("Stores person attributes as in the entity repository. If there is no such person identifier then returns 500")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/person/{identifier}/attr", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> storePersonAttributeInEntity(@PathVariable("identifier") String identifier,
			@RequestBody String jsonString, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.storePersonAttributeInEntity(identifier, jsonString),
				HttpStatus.OK);
	}

	@ApiOperation("Retrieves all the attributes of the organization with provided identifier. If there is no such org identifier then it returns 500")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/org/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrganizationByIdentifier(@PathVariable("identifier") String identifier,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getOrganizationByIdentifier(identifier), HttpStatus.OK);
	}

	@ApiOperation("Retrieves all the attributes of the person with provided identifier. If there is no such person identifier then it returns 500")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/person/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPersonByIdentifier(@PathVariable("identifier") String identifier,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getPersonByIdentifier(identifier), HttpStatus.OK);
	}

	@ApiOperation("Stores organisation attributes as in the entity repository. If there is no such organisation identifier then returns 500")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/org/{identifier}/attr", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> storeOrganizationAttributeInEntity(@PathVariable("identifier") String identifier,
			@RequestBody String jsonString, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.storeOrganizationAttributeInEntity(identifier, jsonString),
				HttpStatus.OK);
	}

	@ApiOperation("Gets cognitive microsoft data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getCognitiveMicrosoft", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCognitiveMicrosoft(@RequestParam(required = false) String q,
			@RequestParam(required = false) String count, @RequestParam(required = false) String offset,
			@RequestParam(required = false) String mkt, @RequestParam(required = false) String safeSearch,
			HttpServletRequest request, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getCognitiveMicrosoft(q, count, offset, mkt, safeSearch),
				HttpStatus.OK);
	}

	//////// mock api for report
	@ApiOperation("Generates Report(mock api for report use and under implementation)")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/generateReport", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> generateReport(@RequestBody String jsonString, @RequestParam("token") String token)
			throws Exception {
		return new ResponseEntity<>(entitySearchService.generateReport(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Gets annual returns of company")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAnnualReturns", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAnnualReturns(@RequestParam(required = true) String url, HttpServletRequest request,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getAnnualReports(url,null,null,getCurrentUserId()), HttpStatus.OK);
	}

	@ApiOperation("Returns an ISIC industry mapping")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/industry/{standard}/{code}/isic", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getIndustryStandardCodeIsic(@PathVariable String standard, @PathVariable String code,
			HttpServletRequest request, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getIndustryStandardCodeIsic(standard, code), HttpStatus.OK);
	}

	// our api
	@ApiOperation("Similar companies based on peer group analysis")
	@ApiResponses(value = { @ApiResponse(code = 200, response = PeerResult.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getPeerGroupData", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getPeerGroupData(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		PeerResult response = entitySearchService.getPeerGroupData(jsonString, getCurrentUserId());
		if (response != null)
			return new ResponseEntity<>(response, HttpStatus.OK);
		return new ResponseEntity<>(new ResponseMessage("Failed to get peer group data"),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation("Gets cognitive microsoft data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getCognitiveMicrosoftWithAdvanceSearch", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCognitiveMicrosoftWithAdvanceSearch(@RequestParam(required = false) String q,
			@RequestParam(required = false) String count, @RequestParam(required = false) String offset,
			@RequestParam(required = false) String mkt, @RequestParam(required = false) String safeSearch,
			HttpServletRequest request, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(
				entitySearchService.getCognitiveMicrosoftWithAdvanceSearch(q, count, offset, mkt, safeSearch),
				HttpStatus.OK);
	}

	@ApiOperation("Bing News")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getBingNews", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getBingNews(@RequestParam(required = true) String entityName, HttpServletRequest request,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(entitySearchService.getBingNews(entityName), HttpStatus.OK);
	}

	@ApiOperation("Searches person with matchers")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/person/searchWithMatchers", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> searchPersonWithMatchers(@RequestBody String jsonString, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(entitySearchService.searchPersonWithMatchers(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Organisation search with matcher")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/org/search-with-matchers", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> orgSearchWithMatchers(@RequestBody String jsonString, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(entitySearchService.orgSearchWithMatchers(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Gets Article News Information")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AritcleNewsInfoResponseDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getArticle", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getArticle(@RequestBody(required = true) ArticleDto articleDto, @RequestParam String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(entitySearchService.getArticleNewsInfo(articleDto), HttpStatus.OK);
	}

	@ApiOperation("Gets Article News Information")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AritcleNewsInfoResponseDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getArticles", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getArticles(@RequestBody(required = true) ArticleDto articleDto,
			@RequestParam String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(entitySearchService.getArticleNewsList(articleDto), HttpStatus.OK);
	}

}
