package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SubClassificationsService extends GenericService<SubClassifications, Long> {
	
	public SubClassifications fetchSubClassification(String subClasssifcationName);

}
