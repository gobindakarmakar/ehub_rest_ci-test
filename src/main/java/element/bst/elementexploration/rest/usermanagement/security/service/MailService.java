package element.bst.elementexploration.rest.usermanagement.security.service;

import element.bst.elementexploration.rest.util.Mail;

public interface MailService {

	public void sendEmail(Mail mail);

	public void sendEmail(Mail mail, String type);

	void sendCaseEmail(Mail mail);

}
