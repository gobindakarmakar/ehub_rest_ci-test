package element.bst.elementexploration.rest.util;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class PaginationInformation implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -7631804446071048619L;
    
	@ApiModelProperty(value = "Title of the results")
    private String title;
	@ApiModelProperty(value = "Kind of the results")
    private String kind;
	@ApiModelProperty(value = "Total results")
    private Long totalResults;
	@ApiModelProperty(value = "Count of the results")
    private int count;
	@ApiModelProperty(value = "Index of the page")
    private int index;
	@ApiModelProperty(value = "Title of the page")
    private int startIndex;
	@ApiModelProperty(value = "Title of the page")
    private String inputEncoding;
	@ApiModelProperty(value = "Output encoding of the page")
    private String outputEncoding;
    
    public PaginationInformation() {
	// TODO Auto-generated constructor stub
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Long totalResults) {
        this.totalResults = totalResults;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public String getInputEncoding() {
        return inputEncoding;
    }

    public void setInputEncoding(String inputEncoding) {
        this.inputEncoding = inputEncoding;
    }

    public String getOutputEncoding() {
        return outputEncoding;
    }

    public void setOutputEncoding(String outputEncoding) {
        this.outputEncoding = outputEncoding;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("PaginationInformation [title=");
	builder.append(title);
	builder.append(", kind=");
	builder.append(kind);
	builder.append(", totalResults=");
	builder.append(totalResults);
	builder.append(", count=");
	builder.append(count);
	builder.append(", index=");
	builder.append(index);
	builder.append(", startIndex=");
	builder.append(startIndex);
	builder.append(", inputEncoding=");
	builder.append(inputEncoding);
	builder.append(", outputEncoding=");
	builder.append(outputEncoding);
	builder.append("]");
	return builder.toString();
    }
}
