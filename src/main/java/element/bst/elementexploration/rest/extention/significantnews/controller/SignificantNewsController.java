package element.bst.elementexploration.rest.extention.significantnews.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantWatchListDto;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantnewsDto;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantCommentService;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantWatchListService;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantnewsService;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author rambabu
 *
 */
@Api(tags = { "Significant news API" })
@RestController
@RequestMapping("/api/significantNews")
public class SignificantNewsController extends BaseController {

	@Autowired
	private SignificantnewsService significantnewsService;
	
	@Autowired
	private SignificantWatchListService significantWatchListService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	SignificantCommentService significantCommentService;

	@ApiOperation("Save significant news")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Significant news saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveSignificantNews", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSignificantNews(@Valid @RequestBody SignificantnewsDto significantnewsDto,
			@RequestParam String token) throws Exception {
		Long userIdTemp = getCurrentUserId();
		significantnewsDto.setUserId(userIdTemp);
		SignificantnewsDto newSignificantnewsDto = significantnewsService.saveSignificantNews(significantnewsDto);
		if(newSignificantnewsDto != null)
			return new ResponseEntity<>(newSignificantnewsDto, HttpStatus.OK);
		else
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);	

	}

	@ApiOperation("Deletes the significant news")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Significant news deleted successfully.") })
	@DeleteMapping(value = "/deleteSignificantNews", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteSignificantNews(@Valid @RequestBody SignificantnewsDto significantnewsDto,
			@RequestParam("token") String token, HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		significantnewsDto.setUserId(userIdTemp);
		boolean result = significantnewsService.deleteSignificantNews(significantnewsDto, userIdTemp);
		if (result) {
			return new ResponseEntity<>(new ResponseMessage("Significant news deleted successfully."), HttpStatus.OK);
		} else {
			throw new NoDataFoundException();
		}
	}

	@ApiOperation("Save sentiment news")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Sentiment news saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveNewsSentiment", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSentimentNews(@Valid @RequestBody SignificantNews significantNews,
			@RequestParam String token) throws Exception {
		Long userIdTemp = getCurrentUserId();
		String status = null;
		significantNews.setUserId(userIdTemp);
		SignificantNews existedSignificantNews = significantnewsService.getSignificantNews(
				significantNews.getEntityId(), significantNews.getEntityName(), significantNews.getPublishedDate(),
				significantNews.getTitle(), significantNews.getUrl(), significantNews.getNewsClass());
		if (existedSignificantNews != null) {
			String oldSentiment = null;
			if (significantNews.getSentiment() != null && significantNews.getSentiment().trim().length() != 0) {
				if (existedSignificantNews.getSentiment() == null)
					status = "new";
				if (!significantNews.getSentiment().equalsIgnoreCase(existedSignificantNews.getSentiment())) {
					status = "update";
					oldSentiment = existedSignificantNews.getSentiment();
				}
				existedSignificantNews.setSentiment(significantNews.getSentiment());
				significantnewsService.saveOrUpdate(existedSignificantNews);
				if (status!= null && status.equals("new"))
					eventPublisher.publishEvent(new LoggingEvent(existedSignificantNews.getId(),
							LoggingEventType.SENTIMENT_NEWS, userIdTemp, new Date()));
				if (status.equals("update"))
					eventPublisher.publishEvent(
							new LoggingEvent(existedSignificantNews.getId(), LoggingEventType.UPDATE_SENTIMENT_NEWS,
									userIdTemp, new Date(), oldSentiment, existedSignificantNews.getSentiment()));
				return new ResponseEntity<>(existedSignificantNews, HttpStatus.OK);
			} else
				throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		} else {
			if (significantNews.getSentiment() != null && significantNews.getSentiment().trim().length() != 0) {
				SignificantNews newSignificantNews = significantnewsService.save(significantNews);
				eventPublisher.publishEvent(new LoggingEvent(newSignificantNews.getId(),
						LoggingEventType.SENTIMENT_NEWS, userIdTemp, new Date()));
				return new ResponseEntity<>(newSignificantNews, HttpStatus.OK);
			}

			else
				throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}

	}
	
	@ApiOperation("Save significant watchlist")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Significant watchlist saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveSignificantwatchList", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSignificantWatchList(@Valid @RequestBody SignificantWatchListDto significantWatchListDto,
			@RequestParam String token) throws Exception {
		Long userIdTemp = getCurrentUserId();
		significantWatchListDto.setUserId(userIdTemp);
		SignificantWatchListDto newSignificantWatchListDto = significantWatchListService.saveSignificantWatchList(significantWatchListDto);
		if(newSignificantWatchListDto != null)
			return new ResponseEntity<>(newSignificantWatchListDto, HttpStatus.OK);
		else
			return new  ResponseEntity<>(new ResponseMessage("Failed to save significant watchlist."), HttpStatus.INTERNAL_SERVER_ERROR); 
	}
	
	@ApiOperation("Deletes significant watchlist for pep")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Significant WatchList deleted successfully.") })
	@DeleteMapping(value = "/deleteWatchList/pep", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWatchListPepSignificant(@RequestParam(required = true) Long watchListId,@RequestParam(required = false) String comment,
			@RequestParam("token") String token, HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		boolean result = significantWatchListService.deleteWatchListPepSignificant(watchListId,comment,userIdTemp);
		if (result) {
			return new ResponseEntity<>(new ResponseMessage("Significant watchList deleted successfully."), HttpStatus.OK);
		} else {
			return new  ResponseEntity<>(new ResponseMessage("Significant watchList not deleted."), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
	
	@ApiOperation("Deletessignificant watchlist for sanction")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Significant WatchList deleted successfully.") })
	@DeleteMapping(value = "/deleteWatchList/sanction", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWatchListSanctionSignificant(@RequestParam(required = true) Long watchListId,@RequestParam(required = false) String comment,
			@RequestParam("token") String token, HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		boolean result = significantWatchListService.deleteWatchListSanctionSignificant(watchListId,comment,userIdTemp);
		if (result) {
			return new ResponseEntity<>(new ResponseMessage("Significant watchList deleted successfully."), HttpStatus.OK);
		} else {
			return new  ResponseEntity<>(new ResponseMessage("Significant watchList not deleted."), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
	
	@ApiOperation("get ignificant comments")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Significant news.") })
	@GetMapping(value = "/getSignificantComments", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSignificantComments(@RequestParam(required = true) Long significantId,@RequestParam(required = false) String classification,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {		
			return new ResponseEntity<>(significantCommentService.getSignificantComments(classification, significantId), HttpStatus.OK);
		}


}
