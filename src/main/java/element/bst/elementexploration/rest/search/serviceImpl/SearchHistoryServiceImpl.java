package element.bst.elementexploration.rest.search.serviceImpl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.search.dao.SearchHistoryDao;
import element.bst.elementexploration.rest.search.domain.SearchHistory;
import element.bst.elementexploration.rest.search.service.SearchHistoryService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.LoggerUtil;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("searchHistoryService")
public class SearchHistoryServiceImpl extends GenericServiceImpl<SearchHistory, Long> implements SearchHistoryService {

	@Autowired
	public SearchHistoryServiceImpl(GenericDao<SearchHistory, Long> genericDao) {
		super(genericDao);
	}

	@Value("${bigdata_search_url}")
	private String SEARCH_BASE_URL;

	@Value("${bigdata_search_case_graph_data_url}")
	private String GET_CASE_GRAPH_URL;

	@Autowired
	private SearchHistoryDao searchHistoryDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	
	@Autowired
	AuditLogDao auditLogDao;

	@Override
	@Transactional("transactionManager")
	public Long createSearchHistory(Long userId, String search, String searchFlag) throws Exception {
		SearchHistory history = new SearchHistory();
		history.setUserId(userId);
		history.setCreatedOn(new Date());
		history.setSearchFlag(searchFlag);
		history.setSearchData(search);
		SearchHistory searchHistory = searchHistoryDao.create(history);
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(search);
		 String keyword = jsonObject.get("keyword").toString();
		String searchType = jsonObject.get("searchType").toString();
		Users author = usersDao.find(userId);
		author = usersService.prepareUserFromFirebase(author);
		String authorName =null;
		if(author!=null)
		 authorName = author.getFirstName() + " " + author.getLastName();
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, "ENTITY", null, "Searched for an entity",
				authorName + " searched for " + searchType + " " + keyword, userId, null, null, null, keyword);
		auditLogDao.create(log);
		return searchHistory.getSearchId();
	}

	@Override
	@Transactional("transactionManager")
	public boolean setFavouriteSearch(Long userId, Long searchId) {
		if (searchId != null) {
			SearchHistory search = searchHistoryDao.find(searchId);
			if (search != null && search.getUserId().equals(userId)) {
				search.setFavourite(true);
				search.setUpdatedOn(new Date());
				searchHistoryDao.saveOrUpdate(search);
				return true;
			}
		} else {
			SearchHistory search = searchHistoryDao.findMostRecentSearch(userId);
			if (search != null) {
				search.setFavourite(true);
				search.setUpdatedOn(new Date());
				searchHistoryDao.saveOrUpdate(search);
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public List<SearchHistory> getRecentSearches(Long userId, String dateFrom, String dateTo, Boolean favourite,
			String flag, Integer pageNumber, Integer recordsPerPage) throws ParseException {
		Date FromDate = null;
		Date toDate = null;
		if (dateFrom != null && dateFrom.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(dateFrom)) {
				FromDate = LoggerUtil.convertToDateddMMyyyy(dateFrom);
			} else {
				throw new DateFormatException();
			}
		}
		if (dateTo != null && dateTo.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(dateTo)) {
				toDate = LoggerUtil.convertToDateddMMyyyy(dateTo);
			} else {
				throw new DateFormatException();
			}
		}
		List<SearchHistory> searchList = searchHistoryDao.getRecentSearches(userId, FromDate, toDate, favourite, flag,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
		return searchList;
	}

	@Override
	@Transactional("transactionManager")
	public long countRecentSearches(Long userId, String dateFrom, String dateTo, Boolean favourite, String flag)
			throws ParseException {
		Date FromDate = null;
		Date toDate = null;
		if (dateFrom != null && dateFrom.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(dateFrom)) {
				FromDate = LoggerUtil.convertToDateddMMyyyy(dateFrom);
			} else {
				throw new DateFormatException();
			}
		}
		if (dateTo != null && dateTo.trim().length() != 0) {
			if (LoggerUtil.isValidDateFormate(dateTo)) {
				toDate = LoggerUtil.convertToDateddMMyyyy(dateTo);
			} else {
				throw new DateFormatException();
			}
		}
		return searchHistoryDao.countRecentSearches(userId, FromDate, toDate, favourite, flag);
	}

	@Override
	public String getCaseGraphData(String caseId) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(GET_CASE_GRAPH_URL + caseId);
		if (Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getSearchData(Long userId, String search, String searchFlag) throws Exception {
		String response = null;
		if (searchFlag.equalsIgnoreCase("search") || searchFlag.equalsIgnoreCase("resolve")
				|| searchFlag.equalsIgnoreCase("graph")) {
			String url = null;
			if (searchFlag.equalsIgnoreCase("search"))
				url = SEARCH_BASE_URL;
			else
				url = SEARCH_BASE_URL + "/" + searchFlag;
			String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, search);
			if (Integer.parseInt(serverResponse[0]) == 200) {
				response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
					return response;
				} else {
					throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
				}
			} else {
				throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			}
		} else {
			throw new Exception(ElementConstants.INVALID_SEARCH_FLAG);
		}
	}
}
