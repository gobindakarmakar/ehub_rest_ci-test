package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;

/**
 * 
 * @author Viswanath
 *
 */
public interface GroupDao extends GenericDao<Group, Long>{

	
	public Group getGroup(String groupName);

	public List<Group> getUserGroups(Integer status, String keyword, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	public Long countUserGroups(Integer status, String keyword);

	public List<Group> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	Long countGroupsOfUser(Long userId);
	
	Group checkGroupExist(String name, Long groupId);
	
	public Boolean doesUserbelongtoGroup(String groupName, Long userId);
}
