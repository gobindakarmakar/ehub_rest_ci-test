package element.bst.elementexploration.rest.extention.docparser.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface DocumentQuestionsService extends GenericService<DocumentQuestions, Long>{
	
	public int findQuestions(long docId);
	
	public List<DocumentQuestions> getListOfControlQuestion();

	public boolean deleteQuestion(Long questionId,Long userId);

	
	
}
