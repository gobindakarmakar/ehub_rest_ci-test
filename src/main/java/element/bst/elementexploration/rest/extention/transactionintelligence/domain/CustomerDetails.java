package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AbstractDomainObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerTitle;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.Gender;

/**
 * <h1>CustomerDetails</h1>
 * <p>
 * This class is used to store the Customer details.
 * </p>
 * 
 * @author suresh
 */
@Entity
@Table(name = "CUSTOMER_DETAILS")
public class CustomerDetails extends AbstractDomainObject {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String customerNumber;
	private CustomerType customerType;
	private CustomerTitle customerTitle;
	private Gender gender;
	private Date dateOfBirth;
	private String residentCountry;
	private String taxReportingCountry;
	private String taxIdentificationNumber;
	private Date customerCreationDate;
	private String industry;
	private String occupation;
	private String primaryCitizenShipCountry;
	private String firstName;
	private String middleName;
	private String institutionName;
	private String displayName;
	private String employerName;
	private String domiciledOrganisation;
	private String jurisdiction;
	private String countryOfTaxation;
	private String jobTitle;
	private Date dateOfIncorporation;

	private List<CustomerAddress> customerAddress;

	private List<Account> customerAccount;

	private Double estimatedAverageTurnoverAmount;
	private Double estimatedTurnoverAmount;
	private String amlFraud;
	private String snit;
	private String watchList;
	private String sar;
	private String businessesActivityCountry;
	private String businesses;
	private String productType;
	private Double amlRisk;
	private String corporateStructure;

	public CustomerDetails() {
	}

	public CustomerDetails(Long id, String customerNumber, CustomerType customerType, CustomerTitle customerTitle,
			Gender gender, Date dateOfBirth, String residentCountry, String taxReportingCountry,
			String taxIdentificationNumber, Date customerCreationDate, String industry, String occupation,
			String primaryCitizenShipCountry, String firstName, String middleName, String institutionName,
			String displayName, String employerName, String domiciledOrganisation, String jurisdiction,
			String countryOfTaxation, String jobTitle, Date dateOfIncorporation, List<CustomerAddress> customerAddress,
			List<Account> customerAccount, Double estimatedAverageTurnoverAmount, Double estimatedTurnoverAmount,
			String amlFraud, String snit, String watchList, String sar, String businessesActivityCountry,
			String businesses, String productType, Double amlRisk,String corporateStructure) {
		super();
		this.id = id;
		this.customerNumber = customerNumber;
		this.customerType = customerType;
		this.customerTitle = customerTitle;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.residentCountry = residentCountry;
		this.taxReportingCountry = taxReportingCountry;
		this.taxIdentificationNumber = taxIdentificationNumber;
		this.customerCreationDate = customerCreationDate;
		this.industry = industry;
		this.occupation = occupation;
		this.primaryCitizenShipCountry = primaryCitizenShipCountry;
		this.firstName = firstName;
		this.middleName = middleName;
		this.institutionName = institutionName;
		this.displayName = displayName;
		this.employerName = employerName;
		this.domiciledOrganisation = domiciledOrganisation;
		this.jurisdiction = jurisdiction;
		this.countryOfTaxation = countryOfTaxation;
		this.jobTitle = jobTitle;
		this.dateOfIncorporation = dateOfIncorporation;
		this.customerAddress = customerAddress;
		this.customerAccount = customerAccount;
		this.estimatedAverageTurnoverAmount = estimatedAverageTurnoverAmount;
		this.estimatedTurnoverAmount = estimatedTurnoverAmount;
		this.amlFraud = amlFraud;
		this.snit = snit;
		this.watchList = watchList;
		this.sar = sar;
		this.businessesActivityCountry = businessesActivityCountry;
		this.businesses = businesses;
		this.productType = productType;
		this.amlRisk = amlRisk;
		this.corporateStructure=corporateStructure;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "CUSTOMER_NUMBER",unique = true)
	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "CUSTOMER_TYPE")
	public CustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "CUSTOMER_TITLE")
	public CustomerTitle getCustomerTitle() {
		return customerTitle;
	}

	public void setCustomerTitle(CustomerTitle customerTitle) {
		this.customerTitle = customerTitle;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "GENDER")
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Column(name = "DATE_OF_BIRTH")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Column(name = "RESIDENT_COUNTRY")
	public String getResidentCountry() {
		return residentCountry;
	}

	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}

	@Column(name = "TAX_REPORTING_COUNTRY")
	public String getTaxReportingCountry() {
		return taxReportingCountry;
	}

	public void setTaxReportingCountry(String taxReportingCountry) {
		this.taxReportingCountry = taxReportingCountry;
	}

	@Column(name = "TAX_IDENTIFICATION_NUMBER")
	public String getTaxIdentificationNumber() {
		return taxIdentificationNumber;
	}

	public void setTaxIdentificationNumber(String taxIdentificationNumber) {
		this.taxIdentificationNumber = taxIdentificationNumber;
	}

	@CreationTimestamp
	@Column(name = "CUSTOMER_CREATION_DATE")
	public Date getCustomerCreationDate() {
		return customerCreationDate;
	}

	public void setCustomerCreationDate(Date customerCreationDate) {
		this.customerCreationDate = customerCreationDate;
	}

	@Column(name = "INDUSTRY")
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@Column(name = "OCCUPATION")
	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	@Column(name = "PRIMARY_CITIZENSHIP_COUNTRY")
	public String getPrimaryCitizenShipCountry() {
		return primaryCitizenShipCountry;
	}

	public void setPrimaryCitizenShipCountry(String primaryCitizenShipCountry) {
		this.primaryCitizenShipCountry = primaryCitizenShipCountry;
	}

	@Column(name = "FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "MIDDLE_NAME")
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Column(name = "INSTITUTION_NAME")
	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	@Column(name = "DISPLAY_NAME")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Column(name = "EMPLOYER_NAME")
	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	@Column(name = "DOMICILED_ORGANISATION")
	public String getDomiciledOrganisation() {
		return domiciledOrganisation;
	}

	public void setDomiciledOrganisation(String domiciledOrganisation) {
		this.domiciledOrganisation = domiciledOrganisation;
	}

	@Column(name = "JURISDICTION")
	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	@Column(name = "COUNTRY_OF_TAXATION")
	public String getCountryOfTaxation() {
		return countryOfTaxation;
	}

	public void setCountryOfTaxation(String countryOfTaxation) {
		this.countryOfTaxation = countryOfTaxation;
	}

	@Column(name = "JOB_TITLE")
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Column(name = "DATE_OF_INCORPORATION")
	public Date getDateOfIncorporation() {
		return dateOfIncorporation;
	}

	public void setDateOfIncorporation(Date dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "customerDetails")
	public List<CustomerAddress> getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(List<CustomerAddress> customerAddress) {
		this.customerAddress = customerAddress;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "customerDetails")
	public List<Account> getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(List<Account> customerAccount) {
		this.customerAccount = customerAccount;
	}

	@Column(name = "ESTIMATED_AVERAGE_TURNOVER_AMOUNT")
	public Double getEstimatedAverageTurnoverAmount() {
		return estimatedAverageTurnoverAmount;
	}

	public void setEstimatedAverageTurnoverAmount(Double estimatedAverageTurnoverAmount) {
		this.estimatedAverageTurnoverAmount = estimatedAverageTurnoverAmount;
	}

	@Column(name = "ESTIMATED_TURNOVER_AMOUNT")
	public Double getEstimatedTurnoverAmount() {
		return estimatedTurnoverAmount;
	}

	public void setEstimatedTurnoverAmount(Double estimatedTurnoverAmount) {
		this.estimatedTurnoverAmount = estimatedTurnoverAmount;
	}

	@Column(name = "AML_FAURD")
	public String getAmlFraud() {
		return amlFraud;
	}

	public void setAmlFraud(String amlFraud) {
		this.amlFraud = amlFraud;
	}

	@Column(name = "SNIT")
	public String getSnit() {
		return snit;
	}

	public void setSnit(String snit) {
		this.snit = snit;
	}

	@Column(name = "WATCH_LIST")
	public String getWatchList() {
		return watchList;
	}

	public void setWatchList(String watchList) {
		this.watchList = watchList;
	}

	@Column(name = "SAR")
	public String getSar() {
		return sar;
	}

	public void setSar(String sar) {
		this.sar = sar;
	}

	@Column(name = "BUSINESS_ACTIVITY_COUNTRY")
	public String getBusinessesActivityCountry() {
		return businessesActivityCountry;
	}

	public void setBusinessesActivityCountry(String businessesActivityCountry) {
		this.businessesActivityCountry = businessesActivityCountry;
	}

	@Column(name = "BUSINESS")
	public String getBusinesses() {
		return businesses;
	}

	public void setBusinesses(String businesses) {
		this.businesses = businesses;
	}

	@Column(name = "PRODUCT_TYPE")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "AML_RISK")
	public Double getAmlRisk() {
		return amlRisk;
	}

	public void setAmlRisk(Double amlRisk) {
		this.amlRisk = amlRisk;
	}

	@Column(name = "CORPORATE_STRUCTURE")
	public String getCorporateStructure() {
		return corporateStructure;
	}

	public void setCorporateStructure(String corporateStructure) {
		this.corporateStructure = corporateStructure;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amlFraud == null) ? 0 : amlFraud.hashCode());
		result = prime * result + ((amlRisk == null) ? 0 : amlRisk.hashCode());
		result = prime * result + ((businesses == null) ? 0 : businesses.hashCode());
		result = prime * result + ((businessesActivityCountry == null) ? 0 : businessesActivityCountry.hashCode());
		result = prime * result + ((corporateStructure == null) ? 0 : corporateStructure.hashCode());
		result = prime * result + ((countryOfTaxation == null) ? 0 : countryOfTaxation.hashCode());
		result = prime * result + ((customerAccount == null) ? 0 : customerAccount.hashCode());
		result = prime * result + ((customerAddress == null) ? 0 : customerAddress.hashCode());
		result = prime * result + ((customerCreationDate == null) ? 0 : customerCreationDate.hashCode());
		result = prime * result + ((customerNumber == null) ? 0 : customerNumber.hashCode());
		result = prime * result + ((customerTitle == null) ? 0 : customerTitle.hashCode());
		result = prime * result + ((customerType == null) ? 0 : customerType.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((dateOfIncorporation == null) ? 0 : dateOfIncorporation.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((domiciledOrganisation == null) ? 0 : domiciledOrganisation.hashCode());
		result = prime * result + ((employerName == null) ? 0 : employerName.hashCode());
		result = prime * result
				+ ((estimatedAverageTurnoverAmount == null) ? 0 : estimatedAverageTurnoverAmount.hashCode());
		result = prime * result + ((estimatedTurnoverAmount == null) ? 0 : estimatedTurnoverAmount.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((industry == null) ? 0 : industry.hashCode());
		result = prime * result + ((institutionName == null) ? 0 : institutionName.hashCode());
		result = prime * result + ((jobTitle == null) ? 0 : jobTitle.hashCode());
		result = prime * result + ((jurisdiction == null) ? 0 : jurisdiction.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((occupation == null) ? 0 : occupation.hashCode());
		result = prime * result + ((primaryCitizenShipCountry == null) ? 0 : primaryCitizenShipCountry.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		result = prime * result + ((residentCountry == null) ? 0 : residentCountry.hashCode());
		result = prime * result + ((sar == null) ? 0 : sar.hashCode());
		result = prime * result + ((snit == null) ? 0 : snit.hashCode());
		result = prime * result + ((taxIdentificationNumber == null) ? 0 : taxIdentificationNumber.hashCode());
		result = prime * result + ((taxReportingCountry == null) ? 0 : taxReportingCountry.hashCode());
		result = prime * result + ((watchList == null) ? 0 : watchList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerDetails other = (CustomerDetails) obj;
		if (amlFraud == null) {
			if (other.amlFraud != null)
				return false;
		} else if (!amlFraud.equals(other.amlFraud))
			return false;
		if (amlRisk == null) {
			if (other.amlRisk != null)
				return false;
		} else if (!amlRisk.equals(other.amlRisk))
			return false;
		if (businesses == null) {
			if (other.businesses != null)
				return false;
		} else if (!businesses.equals(other.businesses))
			return false;
		if (businessesActivityCountry == null) {
			if (other.businessesActivityCountry != null)
				return false;
		} else if (!businessesActivityCountry.equals(other.businessesActivityCountry))
			return false;
		if (corporateStructure == null) {
			if (other.corporateStructure != null)
				return false;
		} else if (!corporateStructure.equals(other.corporateStructure))
			return false;
		if (countryOfTaxation == null) {
			if (other.countryOfTaxation != null)
				return false;
		} else if (!countryOfTaxation.equals(other.countryOfTaxation))
			return false;
		if (customerAccount == null) {
			if (other.customerAccount != null)
				return false;
		} else if (!customerAccount.equals(other.customerAccount))
			return false;
		if (customerAddress == null) {
			if (other.customerAddress != null)
				return false;
		} else if (!customerAddress.equals(other.customerAddress))
			return false;
		if (customerCreationDate == null) {
			if (other.customerCreationDate != null)
				return false;
		} else if (!customerCreationDate.equals(other.customerCreationDate))
			return false;
		if (customerNumber == null) {
			if (other.customerNumber != null)
				return false;
		} else if (!customerNumber.equals(other.customerNumber))
			return false;
		if (customerTitle != other.customerTitle)
			return false;
		if (customerType != other.customerType)
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (dateOfIncorporation == null) {
			if (other.dateOfIncorporation != null)
				return false;
		} else if (!dateOfIncorporation.equals(other.dateOfIncorporation))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (domiciledOrganisation == null) {
			if (other.domiciledOrganisation != null)
				return false;
		} else if (!domiciledOrganisation.equals(other.domiciledOrganisation))
			return false;
		if (employerName == null) {
			if (other.employerName != null)
				return false;
		} else if (!employerName.equals(other.employerName))
			return false;
		if (estimatedAverageTurnoverAmount == null) {
			if (other.estimatedAverageTurnoverAmount != null)
				return false;
		} else if (!estimatedAverageTurnoverAmount.equals(other.estimatedAverageTurnoverAmount))
			return false;
		if (estimatedTurnoverAmount == null) {
			if (other.estimatedTurnoverAmount != null)
				return false;
		} else if (!estimatedTurnoverAmount.equals(other.estimatedTurnoverAmount))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender != other.gender)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (industry == null) {
			if (other.industry != null)
				return false;
		} else if (!industry.equals(other.industry))
			return false;
		if (institutionName == null) {
			if (other.institutionName != null)
				return false;
		} else if (!institutionName.equals(other.institutionName))
			return false;
		if (jobTitle == null) {
			if (other.jobTitle != null)
				return false;
		} else if (!jobTitle.equals(other.jobTitle))
			return false;
		if (jurisdiction == null) {
			if (other.jurisdiction != null)
				return false;
		} else if (!jurisdiction.equals(other.jurisdiction))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (occupation == null) {
			if (other.occupation != null)
				return false;
		} else if (!occupation.equals(other.occupation))
			return false;
		if (primaryCitizenShipCountry == null) {
			if (other.primaryCitizenShipCountry != null)
				return false;
		} else if (!primaryCitizenShipCountry.equals(other.primaryCitizenShipCountry))
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		if (residentCountry == null) {
			if (other.residentCountry != null)
				return false;
		} else if (!residentCountry.equals(other.residentCountry))
			return false;
		if (sar == null) {
			if (other.sar != null)
				return false;
		} else if (!sar.equals(other.sar))
			return false;
		if (snit == null) {
			if (other.snit != null)
				return false;
		} else if (!snit.equals(other.snit))
			return false;
		if (taxIdentificationNumber == null) {
			if (other.taxIdentificationNumber != null)
				return false;
		} else if (!taxIdentificationNumber.equals(other.taxIdentificationNumber))
			return false;
		if (taxReportingCountry == null) {
			if (other.taxReportingCountry != null)
				return false;
		} else if (!taxReportingCountry.equals(other.taxReportingCountry))
			return false;
		if (watchList == null) {
			if (other.watchList != null)
				return false;
		} else if (!watchList.equals(other.watchList))
			return false;
		return true;
	}

	
}
