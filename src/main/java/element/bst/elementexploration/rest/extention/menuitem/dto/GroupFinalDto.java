package element.bst.elementexploration.rest.extention.menuitem.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupFinalDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID of the Group")
	private Long moduleGroupId;

	@ApiModelProperty(value = "Menu Group Name")
	private String moduleGroupName;

	@ApiModelProperty(value = "Menu Group Name")
	private String moduleGroupIcon;

	@ApiModelProperty(value = "Menu Group Color")
	private String moduleGroupColor;

	private List<ModuleFinalDto> modules;

	public Long getModuleGroupId() {
		return moduleGroupId;
	}

	public void setModuleGroupId(Long moduleGroupId) {
		this.moduleGroupId = moduleGroupId;
	}

	public String getModuleGroupName() {
		return moduleGroupName;
	}

	public void setModuleGroupName(String moduleGroupName) {
		this.moduleGroupName = moduleGroupName;
	}

	public String getModuleGroupIcon() {
		return moduleGroupIcon;
	}

	public void setModuleGroupIcon(String moduleGroupIcon) {
		this.moduleGroupIcon = moduleGroupIcon;
	}

	public String getModuleGroupColor() {
		return moduleGroupColor;
	}

	public void setModuleGroupColor(String moduleGroupColor) {
		this.moduleGroupColor = moduleGroupColor;
	}

	public List<ModuleFinalDto> getModules() {
		return modules;
	}

	public void setModules(List<ModuleFinalDto> modules) {
		this.modules = modules;
	}

}
