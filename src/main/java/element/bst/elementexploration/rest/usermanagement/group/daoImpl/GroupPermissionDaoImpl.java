package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupPermissionDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionDtoWithParentId;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionsDtoForPermissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupsDtoForPermissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsDtoForPermissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsDtoWithParent;
import element.bst.elementexploration.rest.usermanagement.group.dto.RolesDtoForPermissions;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("groupPermissionDao")
public class GroupPermissionDaoImpl extends GenericDaoImpl<GroupPermission, Long> implements GroupPermissionDao{
	
	@Autowired
	PrivilegesService privilegesService;
	
	@Autowired
	RolesService rolesService;
	
	@Autowired
	GroupsService groupsService;
	
	@Value("${permissions_query}")
	String permissionsQuery;
	
	@Value("${group_permissions_query}")
	String groupPermissionsQuery;

	public GroupPermissionDaoImpl() {
		super(GroupPermission.class);
	}

	@Override

	public GroupPermission getGroupPermission(GroupPermission groupPermission) throws Exception {

		StringBuilder hql = new StringBuilder();

		if (groupPermission.getGroupPrivilegeId() != null)
			hql.append("select gp from GroupPermission gp where gp.groupPrivilegeId=:groupPrivilegeId");
		

		Query<?> query = this.getCurrentSession().createQuery(hql.toString());

		query.setParameter("groupPrivilegeId", groupPermission.getGroupPrivilegeId());
		
		groupPermission = (GroupPermission) query.getSingleResult();

		return groupPermission;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupPermission> getGroupPermissionByRoleId(Long roleId) throws Exception {
		List<GroupPermission> groupPermissions= new ArrayList<GroupPermission>();
		List<Object[]> objects = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select rp.rolePrivilegeId, rp.permissionId, rp.permissionLevel, rp.roleId from bst_um_role_permissions rp where rp.roleId =:roleId");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("roleId", roleId);
			objects = (List<Object[]>) query.getResultList();
			if (objects != null && objects.size() > 0) {
				for (Object[] result : objects) {
					GroupPermission newPermission= new GroupPermission();
					if (result[0].toString() != null)
						newPermission.setGroupPrivilegeId(Long.valueOf(result[0].toString()));
					if (result[1].toString() != null) {
						Permissions permission=privilegesService.find(Long.valueOf(result[1].toString()));
						//Long parentPermissionId=privilegesService.getParentPermissionId(result[1].toString());
						if(permission!=null) {
							newPermission.setPermissionId(permission);
						}
					}
					if (result[2].toString() != null)
						newPermission.setPermissionLevel(Integer.valueOf(result[2].toString()));
					if (result[3].toString() != null) {
						Roles roles=rolesService.find(Long.valueOf(result[3].toString()));
						if(roles!=null)
							newPermission.setRoleId(roles);
					}
					/*if (result[4].toString() != null) {
						Groups groups=groupsService.find(Long.valueOf(result[4].toString()));
						if(groups!=null)
							newPermission.setGroupId(groups);
					}	*/
					groupPermissions.add(newPermission);
					
				}
			}
			return groupPermissions;
		}catch(Exception e) {
			return groupPermissions;
		}
	}

	@Override
	public List<GroupPermission> getGroupPermissionByRole(Long roleId, String roleName, Long currentUserId)
			throws Exception {
		List<GroupPermission> groupPermissions = new ArrayList<GroupPermission>();
		Long finalRoleId = null;
		if (roleName != null && roleId == null) {
			Roles roleByName = rolesService.findRoleByName(roleName);
			if (roleByName != null)
				finalRoleId = roleByName.getRoleId();
		} else {
			finalRoleId = roleId;
		}
		if (finalRoleId != null) {
			groupPermissions = getGroupPermissionByRoleId(finalRoleId);
		}
		return groupPermissions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupPermissionDtoWithParentId> getGroupPermissionByRoleIdNew(Long finalRoleId) throws Exception {
		List<GroupPermissionDtoWithParentId> groupPermissions= new ArrayList<GroupPermissionDtoWithParentId>();
		List<Object[]> objects = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select rp.rolePrivilegeId, rp.permissionId, rp.permissionLevel, rp.roleId from bst_um_role_permissions rp where rp.roleId =:roleId");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("roleId", finalRoleId);
			objects = (List<Object[]>) query.getResultList();
			if (objects != null && objects.size() > 0) {
				for (Object[] result : objects) {
					GroupPermissionDtoWithParentId newPermission= new GroupPermissionDtoWithParentId();
					if (result[0].toString() != null)
						newPermission.setGroupPrivilegeId(Long.valueOf(result[0].toString()));
					if (result[1].toString() != null) {
						PermissionsDtoWithParent permissionsDto= new PermissionsDtoWithParent();
						Permissions permission=privilegesService.find(Long.valueOf(result[1].toString()));
						if(permission!=null) {
							BeanUtils.copyProperties(permission, permissionsDto);
							//PermissionsWithIdDto parentPermissionIdDto=privilegesService.findPermissionById(Long.valueOf(result[1].toString()));
							//permissionsDto.setParentPermissionId(parentPermissionIdDto.getParentPermissionId());
							//permissionsDto.setCreatedOn(parentPermissionIdDto.getCreatedOn());
							newPermission.setPermissionId(permissionsDto);
						}
					}
					if (result[2].toString() != null)
						newPermission.setPermissionLevel(Integer.valueOf(result[2].toString()));
					if (result[3].toString() != null) {
						Roles roles=rolesService.find(Long.valueOf(result[3].toString()));
						if(roles!=null)
							newPermission.setRoleId(roles);
						    newPermission.setIsModifiable(roles.getIsModifiable());
					}
					/*if (result[4].toString() != null) {
						Groups groups=groupsService.find(Long.valueOf(result[4].toString()));
						if(groups!=null)
							newPermission.setGroupId(groups);
					}	*/
					groupPermissions.add(newPermission);
					
				}
			}
			return groupPermissions;
		}catch(Exception e) {
			return groupPermissions;
		}
	}

	@Override
	public GroupPermission findGroupPermissionByRoleIdAndPermissionId(Long roleId, Long permissionId) throws Exception {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupPermissionsDtoForPermissions> getPermissionsByRolesList(List<Long> roleIds) throws Exception {
		List<Object[]> objects = null;
		List<GroupPermissionsDtoForPermissions> object = new ArrayList<>();
		if (permissionsQuery != null) {
			try {
				StringBuilder queryBuilder = new StringBuilder();
				// System.out.println(MessageFormat.format(permissionsQuery, "25782"));
				//queryBuilder.append(MessageFormat.format(permissionsQuery, roleIds));
				queryBuilder.append(permissionsQuery);
				Query<?> query = this.getCurrentSession().createNativeQuery(queryBuilder.toString());
				query.setParameter("list", roleIds);
				objects = (List<Object[]>) query.getResultList();
				// jsonData=new JSONArray(objects);
				if (objects != null && objects.size() > 0) {
					for (Object[] each : objects) {
						GroupPermissionsDtoForPermissions internalObject = new GroupPermissionsDtoForPermissions();
						PermissionsDtoForPermissions permissionIinternal = new PermissionsDtoForPermissions();
						RolesDtoForPermissions rolesInternal = new RolesDtoForPermissions();
						GroupsDtoForPermissions groupsInternal = new GroupsDtoForPermissions();
						if (each[0] != null && each[0].toString() != null)
							internalObject.setGroupPrivilegeId(Long.valueOf(each[0].toString()));
						if (each[1] != null && each[1].toString() != null)
							internalObject.setPermissionLevel(Integer.valueOf(each[1].toString()));
						// role
						if (each[2] != null && each[2].toString() != null)
							rolesInternal.setRoleId(Long.valueOf(each[2].toString()));
						if (each[3] != null && each[3].toString() != null)
							rolesInternal.setRoleName(each[3].toString());
						if (each[4] != null && each[4].toString() != null)
							rolesInternal.setColor(each[4].toString());
						if (each[5] != null)
							rolesInternal.setCreatedOn((Date) each[5]);
						if (each[6] != null && each[6].toString() != null)
							rolesInternal.setDescription(each[6].toString());
						if (each[7] != null && each[7].toString() != null)
							rolesInternal.setIcon(each[7].toString());
						if (each[8] != null)
							rolesInternal.setModifiedOn((Date) (each[8]));
						if (each[9] != null && each[9].toString() != null)
							rolesInternal.setNotes(each[9].toString());
						if (each[10] != null && each[10].toString() != null)
							rolesInternal.setSource(each[10].toString());
						// Group
						/*if (each[11] != null && each[11].toString() != null)
							groupsInternal.setRemarks(each[11].toString());
						if (each[12] != null && each[12].toString() != null)
							groupsInternal.setParentGroupId(Integer.valueOf(each[12].toString()));
						if (each[13] != null && each[13].toString() != null)
							groupsInternal.setId(Long.valueOf(each[13].toString()));
						if (each[14] != null && each[14].toString() != null)
							groupsInternal.setName(each[14].toString());
						if (each[15] != null)
							groupsInternal.setModifiedDate((Date) each[15]);
						if (each[16] != null && each[16].toString() != null)
							groupsInternal.setIcon(each[16].toString());
						if (each[17] != null && each[17].toString() != null)
							groupsInternal.setDescription((each[17].toString()));
						if (each[18] != null)
							groupsInternal.setCreatedDate((Date) each[18]);
						if (each[19] != null && each[19].toString() != null)
							groupsInternal.setCreatedBy(Long.valueOf(each[19].toString()));
						if (each[20] != null && each[20].toString() != null)
							groupsInternal.setColor(each[20].toString());
						if (each[21] != null)
							groupsInternal.setActive((Boolean) each[21]);*/
						// Permissions
						if (each[11] != null)
							permissionIinternal.setActive((Boolean) each[11]);
						if (each[12] != null)
							permissionIinternal.setCreatedOn((Date) each[12]);
						if (each[13] != null && each[13].toString() != null)
							permissionIinternal.setPermissionId(Long.valueOf(each[13].toString()));
						if (each[14] != null && each[14].toString() != null)
							permissionIinternal.setItemName(each[14].toString());
						if (each[15] != null && each[15].toString() != null)
							permissionIinternal.setParentPermissionId(Long.valueOf(each[15].toString()));

						internalObject.setGroupId(groupsInternal);
						internalObject.setPermissionId(permissionIinternal);
						internalObject.setRoleId(rolesInternal);
						object.add(internalObject);
					}
				}
				return object;
			} catch (NoResultException e) {
				return null;
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
						this.getClass());
			}
		}
		return object;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupPermissionsDtoForPermissions> getRolesByGroup(List<Long> groupIds) throws Exception {
		List<Object[]> objects = null;
		List<GroupPermissionsDtoForPermissions> object = new ArrayList<>();
		if (groupPermissionsQuery != null) {
			try {
				StringBuilder queryBuilder = new StringBuilder();
				queryBuilder.append(groupPermissionsQuery);
				Query<?> query = this.getCurrentSession().createNativeQuery(queryBuilder.toString());
				query.setParameter("list", groupIds);
				objects = (List<Object[]>) query.getResultList();
				if (objects != null && objects.size() > 0) {
					for (Object[] each : objects) {
						GroupPermissionsDtoForPermissions internalObject = new GroupPermissionsDtoForPermissions();
						RolesDtoForPermissions rolesInternal = new RolesDtoForPermissions();
						GroupsDtoForPermissions groupsInternal = new GroupsDtoForPermissions();
						if (each[0] != null && each[0].toString() != null)
							internalObject.setGroupPrivilegeId(Long.valueOf(each[0].toString()));
						if (each[1] != null && each[1].toString() != null)
							internalObject.setPermissionLevel(Integer.valueOf(each[1].toString()));
						// role
						if (each[2] != null && each[2].toString() != null)
							rolesInternal.setRoleId(Long.valueOf(each[2].toString()));
						if (each[3] != null && each[3].toString() != null)
							rolesInternal.setRoleName(each[3].toString());
						if (each[4] != null && each[4].toString() != null)
							rolesInternal.setColor(each[4].toString());
						if (each[5] != null)
							rolesInternal.setCreatedOn((Date) each[5]);
						if (each[6] != null && each[6].toString() != null)
							rolesInternal.setDescription(each[6].toString());
						if (each[7] != null && each[7].toString() != null)
							rolesInternal.setIcon(each[7].toString());
						if (each[8] != null)
							rolesInternal.setModifiedOn((Date) (each[8]));
						if (each[9] != null && each[9].toString() != null)
							rolesInternal.setNotes(each[9].toString());
						if (each[10] != null && each[10].toString() != null)
							rolesInternal.setSource(each[10].toString());
						// Group
						/*if (each[11] != null && each[11].toString() != null)
							groupsInternal.setRemarks(each[11].toString());
						if (each[12] != null && each[12].toString() != null)
							groupsInternal.setParentGroupId(Integer.valueOf(each[12].toString()));
						if (each[13] != null && each[13].toString() != null)
							groupsInternal.setId(Long.valueOf(each[13].toString()));
						if (each[14] != null && each[14].toString() != null)
							groupsInternal.setName(each[14].toString());
						if (each[15] != null)
							groupsInternal.setModifiedDate((Date) each[15]);
						if (each[16] != null && each[16].toString() != null)
							groupsInternal.setIcon(each[16].toString());
						if (each[17] != null && each[17].toString() != null)
							groupsInternal.setDescription((each[17].toString()));
						if (each[18] != null)
							groupsInternal.setCreatedDate((Date) each[18]);
						if (each[19] != null && each[19].toString() != null)
							groupsInternal.setCreatedBy(Long.valueOf(each[19].toString()));
						if (each[20] != null && each[20].toString() != null)
							groupsInternal.setColor(each[20].toString());
						if (each[21] != null)
							groupsInternal.setActive((Boolean) each[21]);*/
						internalObject.setRoleId(rolesInternal);
						internalObject.setGroupId(groupsInternal);
						object.add(internalObject);
					}
				}
				return object;
			} catch (NoResultException e) {
				return null;
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
						this.getClass());
			}
		}
		return object;
	}

	@Override
	public void deleteGroupPermissionById(Long groupPrivilegeId) throws Exception {
		
		try {
				StringBuilder hql = new StringBuilder();
				hql.append("delete from bst_um_role_permissions where rolePrivilegeId =:rolePrivilegeId");
				NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
				query.setParameter("rolePrivilegeId", groupPrivilegeId);
				query.executeUpdate();
			
		    }catch (Exception e) {
			 ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					this.getClass());
		}	
		
	}

	@Override
	public void deleteGroupPermissionByRoleId(Long roleId) {
		
		try {
				StringBuilder hql = new StringBuilder();
				hql.append("delete from bst_um_role_permissions where roleId =:roleId");
				NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
				query.setParameter("roleId", roleId);
				query.executeUpdate();
		    }catch (Exception e) {
			 ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					this.getClass());
		}	
		
	}
}
