package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Paul Jalagari
 *
 */
public class DateCountStatusDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date date;

	private String status;

	private int count;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
