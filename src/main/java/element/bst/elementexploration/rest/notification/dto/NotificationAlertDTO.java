package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Viswanath
 *
 */
@ApiModel("Notification alert")
public class NotificationAlertDTO implements Serializable {

	
	private static final long serialVersionUID = 9185078822072651451L;

	@ApiModelProperty("Id of notification")
	private Long notificationId;
	@ApiModelProperty("Subject of notification")
	private String subject;
	@ApiModelProperty("Body of notification")
	private String body;
	@ApiModelProperty("Created date time of notification")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createdOn;
	@ApiModelProperty("Notification delivred date")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime deliverOn;
	@ApiModelProperty("Type of notification")
	private Integer type;
	@ApiModelProperty("Level of notification")
	private Integer level;
	@ApiModelProperty("Created user id of notification")
	private Long createdBy;
	@ApiModelProperty("Case id of notification")
	private Long caseId;
	@ApiModelProperty("Flag of notification")
	private Integer flag;

	/**
	 * 
	 */
	public NotificationAlertDTO() {
		super();
	}

	public NotificationAlertDTO(Long notificationId, String subject, String body, Integer type, LocalDateTime createdOn,
			LocalDateTime deliverOn, Integer level, Long createdBy, Long caseId, Integer flag) {
		super();
		this.notificationId = notificationId;
		this.subject = subject;
		this.body = body;
		this.type = type;
		this.createdOn = createdOn;
		this.deliverOn = deliverOn;
		this.level = level;
		this.createdBy = createdBy;
		this.caseId = caseId;
		this.flag = flag;
	}

	/**
	 * @return the notificationId
	 */
	public Long getNotificationId() {
		return notificationId;
	}

	/**
	 * @param notificationId
	 *            the notificationId to set
	 */
	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getDeliverOn() {
		return deliverOn;
	}

	public void setDeliverOn(LocalDateTime deliverOn) {
		this.deliverOn = deliverOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
