package element.bst.elementexploration.rest.util;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

public class JWTDecoder {

	public static JSONObject getDecodedJWTBody(String jwtToken){
		String[] split_string = jwtToken.split("\\.");
		String base64EncodedBody = split_string[1];
		Base64 base64Url = new Base64(true);
		String body = new String(base64Url.decode(base64EncodedBody));
		JSONObject result = new JSONObject(body);
		
		return result;
		}
	
	public static JSONObject getDecodedJWTHeader(String jwtToken) {
		String[] split_string = jwtToken.split("\\.");
		String base64EncodedHeader = split_string[0];

		Base64 base64Url = new Base64(true);
		String header = new String(base64Url.decode(base64EncodedHeader));

		JSONObject result = new JSONObject(header);

		return result;
	}
	
}
