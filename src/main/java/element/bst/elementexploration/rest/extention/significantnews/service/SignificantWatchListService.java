package element.bst.elementexploration.rest.extention.significantnews.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantWatchList;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantWatchListDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface SignificantWatchListService extends GenericService<SignificantWatchList, Long>{
	
	public SignificantWatchListDto saveSignificantWatchList(SignificantWatchListDto significantWatchListDto);
	
	public SignificantWatchList getSignificantWatchList(String mainEntityId,String entityId,String entityName,String value,String name);

	public boolean deleteWatchListPepSignificant(Long significantId,String comment,Long userId);

	public boolean deleteWatchListSanctionSignificant(Long significantId,String comment,Long userIdTemp);

	public List<SignificantWatchList> fetchWtachListByEntityId(String mainEntityId);

}
