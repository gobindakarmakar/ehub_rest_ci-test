package element.bst.elementexploration.rest.extention.sourcemonitoring.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.json.JSONObject;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ResultsValidityDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String valuesChanged;
	private String matchingStatus;

	public String getValuesChanged() {
		return valuesChanged;
	}

	public void setValuesChanged(String valuesChanged) {
		this.valuesChanged = valuesChanged;
	}

	public String getMatchingStatus() {
		return matchingStatus;
	}

	public void setMatchingStatus(String matchingStatus) {
		this.matchingStatus = matchingStatus;
	}
}
