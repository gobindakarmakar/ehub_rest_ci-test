package element.bst.elementexploration.rest.settings.listManagement.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.listManagement.domain.Color;
import element.bst.elementexploration.rest.settings.listManagement.dto.ColorDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ColorService;
import element.bst.elementexploration.rest.util.EntityConverter;

@Service("colorService")
@Transactional("transactionManager")
public class ColorServiceImpl  extends GenericServiceImpl<Color, Long> implements ColorService {

	@Autowired
	ColorService colorService;
	
	@Autowired
	public ColorServiceImpl(GenericDao<Color, Long> genericDao) {
		super(genericDao);
	}

	public ColorServiceImpl() {

	}
	
	@Override
	public List<ColorDto> getAllColors() {
		List<Color> colorList = colorService.findAll();
		List<ColorDto> colorDtosList = new ArrayList<ColorDto>();
		if (colorList != null) {
			colorDtosList = colorList.stream().map(
					(entityComplexStructureDto) -> new EntityConverter<Color, ColorDto>(Color.class, ColorDto.class)
					.toT2(entityComplexStructureDto, new ColorDto()))
					.collect(Collectors.toList());
		}
		return colorDtosList;
	}
}
