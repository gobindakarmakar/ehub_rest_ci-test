package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

public class SubsidiarieScreening extends Thread {

	private String identifier;

	private String SCREENING_URL;

	private List<List<JSONObject>> list;

	private Integer level;

	private Boolean isShareholder;

	private AdvanceSearchService advanceSearchService;

	private String cORPORATE_STRUCTURE_LOCATION;

	private org.json.simple.JSONArray personTypes;

	private org.json.simple.JSONObject pepTypes;
	
	private String startDate;

	private String endDate;
	
	private String apiKey;

	public SubsidiarieScreening(String identifier, String sCREENING_URL, List<List<JSONObject>> list, Integer level,
			Boolean isShareholder, AdvanceSearchService advanceSearchService, String cORPORATE_STRUCTURE_LOCATION,
			org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,String startDate,String endDate, String apiKey) {
		super();
		this.identifier = identifier;
		SCREENING_URL = sCREENING_URL;
		this.list = list;
		this.level = level;
		this.isShareholder = isShareholder;
		this.advanceSearchService = advanceSearchService;
		this.cORPORATE_STRUCTURE_LOCATION = cORPORATE_STRUCTURE_LOCATION;
		this.personTypes = personTypes;
		this.pepTypes = pepTypes;
		this.startDate=startDate;
		this.endDate=endDate;
		this.apiKey=apiKey;
	}

	@Override
	public void run() {
		try {
			if(level!=0){
				getData();
			}else{
				List<JSONObject> levelwiseList = list.get(level);
				for (JSONObject jsonObject : levelwiseList) {
					jsonObject.put("screeningFlag", true);
					jsonObject.put("news", new JSONArray());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getData() throws Exception {
		List<JSONObject> levelwiseList = list.get(level);
		JSONArray classifications = new JSONArray();
		String requestId = UUID.randomUUID().toString();
		JSONObject finalJsonTosent = new JSONObject();
		JSONArray personEntities = new JSONArray();
		JSONArray orgEntities = new JSONArray();
		JSONObject orgJson = new JSONObject();
		JSONObject personJson = new JSONObject();

		int count = 0;
		for (JSONObject jsonObject : levelwiseList) {
			if(!jsonObject.has("screeningFlag") || !jsonObject.getBoolean("screeningFlag")){
			JSONObject jsonToSent = new JSONObject();
			String refId = UUID.randomUUID().toString();
			jsonObject.put("screeningUrl", SCREENING_URL + "/"
					+ URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20") + "/" + requestId);
			if(jsonObject.has("identifier")){
				jsonObject.put("entity_ref_id", jsonObject.getString("identifier"));
				jsonToSent.put("entity_id", jsonObject.getString("identifier"));
			}else{
				jsonObject.put("entity_ref_id", refId);
				jsonToSent.put("entity_id",refId );
			}
			jsonToSent.put("jurisdiction", jsonObject.getString("jurisdiction"));
			jsonToSent.put("names", new JSONArray().put(jsonObject.getString("name")));
			if(startDate!=null)
				jsonToSent.put("start_date", startDate);
			if(endDate!=null)
				jsonToSent.put("end_date", endDate);
			String entityType = null;
			if (jsonObject.has("entity_type")) {
				entityType = jsonObject.getString("entity_type");
			} else {
				entityType = "organization";
			}
			if ("organization".equalsIgnoreCase(entityType))
				orgEntities.put(jsonToSent);
			else
				personEntities.put(jsonToSent);
			count = count + 1;
			}
		}
		if (personEntities.length() > 0)
			personJson.put("entities", personEntities);
		if (orgEntities.length() > 0)
			orgJson.put("entities", orgEntities);
		finalJsonTosent.put("max_responses", 10);
		// finalJsonTosent.put("min_confidence", 0.2);
		if (personJson.has("entities"))
			finalJsonTosent.put("person", personJson);
		if (orgJson.has("entities"))
			finalJsonTosent.put("organization", orgJson);
		/*File file1 = new File("/opt/corporatestructurepath/requestjsons/" + "sub"+UUID.randomUUID().toString() + ".json");
		file1.createNewFile();
		FileWriter fileWriter = new FileWriter(file1);
		fileWriter.write(finalJsonTosent.toString());
		fileWriter.close();*/
		String putResponse = sendData(identifier, requestId, finalJsonTosent.toString());
		String screeningResponse = getData(identifier, requestId);
		JSONObject newsJson = new JSONObject();
		if (screeningResponse != null) {
			JSONObject screeningJson = new JSONObject(screeningResponse);
			if (screeningJson.has("news")) {
				newsJson = screeningJson.getJSONObject("news");
				if (newsJson.has("status")) {
					int counter = 1;
					while ((newsJson.has("status") && "In Progress".equalsIgnoreCase(newsJson.getString("status")))
							|| (!newsJson.has("status"))) {
						screeningResponse = getData(identifier, requestId);
						if (screeningResponse != null) {
							try {
								screeningJson = new JSONObject(screeningResponse);
							} catch (Exception e) {
							}
						}
						if (screeningJson != null && screeningJson.has("news")) {
							newsJson = screeningJson.getJSONObject("news");
						} else {
							break;
						}
						counter = counter + 1;
						if (counter == 10)
							break;
						Thread.sleep(10000);
					}
				}
			}
			if(!screeningJson.has("watchlists")){
				int temp=0;
				while(screeningJson.has("watchlists")){
					screeningResponse = getData(identifier, requestId);
					if(screeningResponse!=null){
						screeningJson = new JSONObject(screeningResponse);
					}
					temp = temp + 1;
					if (temp == 6)
						break;
					Thread.sleep(5000);
				}
			}
			JSONObject watchlistPepJson = new JSONObject();
			JSONObject watchlistSanctionJson = new JSONObject();
				if (screeningJson.has("watchlists")) {
					JSONObject watchlists = screeningJson.getJSONObject("watchlists");
					if (watchlists.has("status")) {
						int chunksCounter = 1;
						while (!"done".equalsIgnoreCase(watchlists.getString("status"))) {
							screeningResponse = getData(identifier, requestId);
							if (screeningResponse != null) {
								try {
									screeningJson = new JSONObject(screeningResponse);
								} catch (Exception e) {
								}
								if (screeningJson.has("watchlists")) {
									watchlists = screeningJson.getJSONObject("watchlists");
								}
							}
							chunksCounter = chunksCounter + 1;
							if (chunksCounter == 10)
								break;
							Thread.sleep(10000);
						}
					}
					if (watchlists.has("chunks")) {
						JSONArray chunks = watchlists.getJSONArray("chunks");
						if (chunks != null) {
							for (int i = 0; i < chunks.length(); i++) {
								JSONObject chunk = null;
								if (chunks.get(i) instanceof JSONObject)
									chunk = chunks.getJSONObject(i);
								if (chunk != null && chunk.has("_links")) {
									JSONObject links = chunk.getJSONObject("_links");
									String link = null;
									if (links.has("response")) {
										link = links.getString("response");
									}
									if (link != null) {
										HierarchyDto hierarchyDto = new HierarchyDto();
										hierarchyDto.setUrl(link);
										String response = advanceSearchService.getHierarchyData(hierarchyDto, null,null);
										JSONObject json = null;
										if (response != null)
											json = new JSONObject(response);
										if (json != null && json.has("response")) {
											/*JSONArray responseJson = json.getJSONArray("response");
											if (responseJson != null && responseJson.length() > 0) {
												for (int j = 0; j < responseJson.length(); j++) {
													JSONObject watchlist = responseJson.getJSONObject(j);
													watchlistJson.put(watchlist.getString("entity_id"), watchlist);
												}
											}*/
											
											JSONObject responseJson = json.getJSONObject("response");
											if(responseJson!=null && responseJson.has("pep")){
												JSONArray array=responseJson.getJSONArray("pep");
												if(array!=null){
													for (int j = 0; j < array.length(); j++) {
														watchlistPepJson.put(array.getJSONObject(j).getString("entity_id"),
																array.getJSONObject(j));
													}
												}
											}
											if(responseJson!=null && responseJson.has("sanction")){
												JSONArray sanctionArray=responseJson.getJSONArray("sanction");
												if(sanctionArray!=null){
													for (int j = 0; j < sanctionArray.length(); j++) {
														watchlistSanctionJson.put(sanctionArray.getJSONObject(j).getString("entity_id"),
																sanctionArray.getJSONObject(j));
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				for (JSONObject jsonObject : levelwiseList) {
					if(!jsonObject.has("screeningFlag") || !jsonObject.getBoolean("screeningFlag")){
					JSONArray peps = new JSONArray();
					JSONArray sanctions = new JSONArray();
					String entityRefId = jsonObject.getString("entity_ref_id");
					JSONObject entityNews = null;
					if (newsJson.has(entityRefId))
						entityNews = newsJson.getJSONObject(entityRefId);
					if (entityNews != null && entityNews.has("classifications"))
						classifications = entityNews.getJSONArray("classifications");
					else
						classifications = new JSONArray();
					preparePepAndSanction(watchlistPepJson, jsonObject, entityRefId,true,identifier);
					preparePepAndSanction(watchlistSanctionJson, jsonObject, entityRefId,false,identifier);
					/*if (watchlistJson.has(entityRefId)) {
						JSONObject object = watchlistJson.getJSONObject(entityRefId);
						if (object.has("hits")) {
							JSONArray hits = object.getJSONArray("hits");
							if (hits != null) {
								for (int i = 0; i < hits.length(); i++) {
									JSONArray pepsEntries = new JSONArray();
									JSONArray sanctionEntries = new JSONArray();
									JSONObject pepJson = new JSONObject();
									JSONObject sanctionJson = new JSONObject();
									JSONObject hit = hits.getJSONObject(i);
									if (hit.has("entries")) {
										JSONArray entries = hit.getJSONArray("entries");
										if (entries != null) {
											for (int j = 0; j < entries.length(); j++) {
												JSONObject entry = entries.getJSONObject(j);
												if (entry.has("watchlist_id")) {
													String watchlistId = entry.getString("watchlist_id");
													if (pepTypes.containsKey(watchlistId)) {
														String watchlistType = pepTypes.get(watchlistId).toString();
														if ("PEP".equalsIgnoreCase(watchlistType)) {
															pepsEntries.put(entry);
														} else {
															sanctionEntries.put(entry);
														}
													}
												}
											}
										}
										if (pepsEntries.length() > 0) {
											if (hit.has("value"))
												pepJson.put("value", hit.getString("value"));
											if (hit.has("confidence"))
												pepJson.put("confidence", hit.getDouble("confidence"));
											pepJson.put("entries", pepsEntries);
											peps.put(pepJson);
										}
										if (sanctionEntries.length() > 0) {
											if (hit.has("value"))
												sanctionJson.put("value", hit.getString("value"));
											if (hit.has("confidence"))
												sanctionJson.put("confidence", hit.getDouble("confidence"));
											sanctionJson.put("entries", sanctionEntries);
											sanctions.put(sanctionJson);
										}
									}
									// peps.put(pepJson);
									// sanctions.put(sanctionJson);
								}
							}
						}
						JSONArray mergedPeps=new JSONArray();
						JSONArray mergedSanctions=new JSONArray();
						advanceSearchService.getMergedResults(mergedPeps, peps);
						advanceSearchService.getMergedResults(mergedSanctions, sanctions);
						jsonObject.put("pep", mergedPeps);
						jsonObject.put("sanctions", mergedSanctions);

					}*/
					jsonObject.put("news", classifications);
					jsonObject.put("screeningFlag", true);
				}
				}
			/*} else {
				for (JSONObject jsonObject : levelwiseList) {
					jsonObject.put("screeningFlag", true);
				}
			}*/
		} else {
			for (JSONObject jsonObject : levelwiseList) {
				jsonObject.put("screeningFlag", true);
			}
		}
	}

	public String sendData(String identifier, String requestId, String jsonToBeSent) throws Exception {
		String url = SCREENING_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (requestId != null)
			url = url + "/" + requestId;
		String serverResponse[] = ServiceCallHelper.updateDataInServerWithApiKey(url, jsonToBeSent, apiKey);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}

	public String getData(String identifier, String requestId) throws Exception {
		String url = SCREENING_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (requestId != null)
			url = url + "/" + requestId;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}
	
	private void preparePepAndSanction(JSONObject watchlistJson, JSONObject jsonObject, String entityRefId,
			boolean isPep,String identifier) {
		JSONArray watchlistEntityIds = watchlistJson.names();
		if (watchlistEntityIds != null) {
			if (watchlistJson.has(entityRefId)) {
				JSONObject object = watchlistJson.getJSONObject(entityRefId);
				JSONArray mergedResult = new JSONArray();
				if(object.has("hits"))
					advanceSearchService.getMergedResults(mergedResult, object.getJSONArray("hits"),identifier,isPep);
				if (jsonObject.has(entityRefId)) {
					if (isPep)
						jsonObject.getJSONObject(entityRefId).put("pep", mergedResult);
					else
						jsonObject.getJSONObject(entityRefId).put("sanctions", mergedResult);
					jsonObject.getJSONObject(entityRefId).put("pepScreening", true);
				} else {
					if (isPep)
						jsonObject.put(entityRefId, new JSONObject().put("pep", mergedResult));
					else
						jsonObject.put(entityRefId, new JSONObject().put("sanctions", mergedResult));
					jsonObject.getJSONObject(entityRefId).put("pepScreening", true);

				}

			}
		}
	}

}
