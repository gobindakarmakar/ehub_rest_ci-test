package element.bst.elementexploration.rest.usermanagement.admin.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserRegistrationDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author viswanath
 *
 */
@Api(tags = { "Administration API" },description= "Manages admin privileges")
@RestController
@RequestMapping("/api/admin")
public class AdminstrationController extends BaseController {

	@Autowired
	private UserService userService;
	
	@Autowired
	GroupService groupService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
    private ApplicationEventPublisher eventPublisher;

	@ApiOperation("Register a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Username/email already exists or "+ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/register", produces = { "application/json; charset=UTF-8" },
			consumes = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleRegistration(HttpServletRequest request, @RequestParam("token") String adminToken,
			@Valid @RequestBody UserRegistrationDto userRegistrationDto, BindingResult results){
		User user= new User();
		BeanUtils.copyProperties(userRegistrationDto, user);
		Long adminUserId = getCurrentUserId();
		if (!results.hasErrors()) {
			UserDto checkUserByUserName = userService.getUserByEmailOrUsername(user.getScreenName());
			if (checkUserByUserName == null) {
				UserDto checkUserByEmailAddress = userService.getUserByEmailOrUsername(user.getEmailAddress());
				if (checkUserByEmailAddress == null) {
					// Check first if user is an Administrator
					if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminUserId)) {
						user.setPassword(passwordEncoder.encode(user.getPassword()));
						user.setCreatedDate(new Date());
						user.setModifiedDate(null);
						user.setStatus(String.valueOf(ElementConstants.ACTIVATED_USER_CODE));
						user.setCreatedBy(adminUserId);
						user = userService.save(user);
	
						UserDto userAfterChange = userService.getUserByEmailOrUsername(user.getEmailAddress());	
						String url = new String(
								request.getScheme() + "://" + request.getLocalName() + ":" + request.getLocalPort() + "/");
						userService.sendEmailVerification(userAfterChange.getUserId(), userAfterChange.getScreenName(), url,
								"generatedToken", userAfterChange.getEmailAddress());
						eventPublisher.publishEvent(new LoggingEvent(user, LoggingEventType.USER_REGISTRATION_BY_ADMIN, adminUserId, new Date()));
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_CREATION_SUCCESSFUL), HttpStatus.OK);
					} else {
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN),
								HttpStatus.UNAUTHORIZED);
					}
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.EMAILADDRESS_ALREADY_EXISTS + user.getEmailAddress()),
							HttpStatus.BAD_REQUEST);
				}
	
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + user.getScreenName()),
						HttpStatus.BAD_REQUEST);
			}
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Activate a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_ACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/activateUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> activateUser(@RequestParam("token") String adminUserToken,
			@RequestParam("userId") Long userId, HttpServletRequest request) {
		Long adminUserId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminUserId)) {
			User userBeforeChange = userService.find(userId);
			if(userBeforeChange != null){
				userService.activateUser(userBeforeChange);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_ACTIVATED_SUCCESSFULLY_MSG), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Deactivate a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_DEACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/deactivateUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deactivateUser(@RequestParam("token") String adminUserToken,
			@RequestParam("userId") Long userId, HttpServletRequest request) {
		Long adminUserId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminUserId)) {
			User userBeforeChange = userService.find(userId);
			if(userBeforeChange != null){
				userService.deactivateUser(userBeforeChange);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_DEACTIVATED_SUCCESSFULLY_MSG), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}
	@ApiOperation("Get a user details by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserDto.class, message = "Operation Succesfull"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserDetails", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserDetails(@RequestParam("token") String adminToken,
			@RequestParam("userId") Long userId, HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {

		UserDto userDto = new UserDto();

		Long adminId = getCurrentUserId();

		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			User user = userService.find(userId);
			if(user != null){
				BeanUtils.copyProperties(user, userDto);
				return new ResponseEntity<>(userDto, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.NOT_FOUND);
			}
		} else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Update user details by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserDto.class, message = ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.EMAILADDRESS_ALREADY_EXISTS+" or "+ElementConstants.USERNAME_ALREADY_EXISTS),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateUser", produces = { "application/json; charset=UTF-8" },
			consumes = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleUpdateUserByAdmin(@RequestParam("userId") Long userId, 
			@RequestParam("token") String adminToken, @Valid @RequestBody UserDto updatedUser, 
			BindingResult results, HttpServletRequest request) throws Exception {

		Long adminId = getCurrentUserId();

		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {

			User user = userService.find(userId);
			if (user == null) {
				throw new NoDataFoundException(ElementConstants.USER_NOT_FOUND);
			}

			UserDto checkUserByUserName = userService.checkUserNameOrEmailExists(updatedUser.getScreenName(), updatedUser.getUserId());
			if (checkUserByUserName == null) {
				UserDto checkUserByEmailAddress = userService.checkUserNameOrEmailExists(updatedUser.getEmailAddress(), updatedUser.getUserId());
				if (checkUserByEmailAddress == null) {

					// Set user updated record
					user.setEmailAddress(updatedUser.getEmailAddress());
					user.setFirstName(updatedUser.getFirstName());
					user.setLastName(updatedUser.getLastName());
					user.setMiddleName(updatedUser.getMiddleName());
					user.setScreenName(updatedUser.getScreenName());
					user.setDob(updatedUser.getDob());
					user.setCountry(updatedUser.getCountry());	
					user.setModifiedDate(new Date());
					userService.saveOrUpdate(user);

					return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG), HttpStatus.OK);
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.EMAILADDRESS_ALREADY_EXISTS + updatedUser.getEmailAddress()),
							HttpStatus.CONFLICT);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + updatedUser.getScreenName()),
						HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
		
	}
}
