package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.ISOCodeDao;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCodes;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("isoCodeDao")
public class ISOCodeDaoImpl extends GenericDaoImpl<IsoCodes, Long> implements ISOCodeDao {

	public ISOCodeDaoImpl() {
		super(IsoCodes.class);
	}

}
