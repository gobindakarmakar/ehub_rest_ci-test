package element.bst.elementexploration.rest.extention.sourcemonitoring.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.ErrorBubbleChartDto;
import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.ResultsValidityDto;
import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.SourceMonitoringFinalDto;
import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.SourceMonitoringMetadataDto;
import element.bst.elementexploration.rest.extention.sourcemonitoring.service.SourceMonitoringService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("sourceMonitoringService")
public class SourceMonitoringServiceImpl implements SourceMonitoringService {

	@Value("${be_source_monitoring_url}")
	private String BIGDATA_SOURCE_MONITORING_URL;

	@Autowired
	SourcesService sourcesService;

	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	@Override
	@Transactional("transactionManager")
	public SourceMonitoringFinalDto getMetaData(String dateLimit, String source_id) throws Exception {
		//dateLimit = "30";
		//https://myq8n2w3d9.execute-api.eu-west-1.amazonaws.com/Prod/v1/
		//source-monitoring/metadata-async?request_id=some_uniqueid&dateLimit=30
		String requestId = UUID.randomUUID().toString();
		String url = BIGDATA_SOURCE_MONITORING_URL + "/source-monitoring/metadata-async?request_id="+requestId;

		/*if (dateLimit != null && source_id == null)
			url = url + "&dateLimit" + dateLimit;
		if (source_id != null && dateLimit == null)
			url = url + "&source_id" + source_id;
		if (dateLimit != null && source_id != null)
			url = url + "&" + "source_id=" + source_id + "&" + "dateLimit=" + dateLimit;*/
		String response = getDataFromServer(url);
		JSONObject monitoringJson = null;
		if (response != null) {
			monitoringJson = new JSONObject(response);
		}
		if(monitoringJson !=null){
			while(!monitoringJson.has("results")){
				response = getDataFromServer(url);
				if (response != null) {
					monitoringJson = new JSONObject(response);
				}
				Thread.sleep(1500);
			}
		}
		
		List<SourceMonitoringMetadataDto> sourceMonitoringMetadataDtos = new ArrayList<SourceMonitoringMetadataDto>();

		if (response != null) {
			JSONObject json = new JSONObject(response);
			if (json != null && json.has("results")) {
				JSONArray resultsArray = json.getJSONArray("results");
				if (resultsArray != null && resultsArray.length() > 0) {
					for (int i = 0; i < resultsArray.length(); i++) {
						SourceMonitoringMetadataDto sourceMonitoringMetadataDto1 = new SourceMonitoringMetadataDto();
						JSONObject results = resultsArray.getJSONObject(i);
						if (results.has("execution_duration")) {
							sourceMonitoringMetadataDto1.setExecutionDuration(results.getString("execution_duration"));
						}
						if (results.has("result_code")) {
							sourceMonitoringMetadataDto1.setResultCode(results.getLong("result_code"));
						}
						if (results.has("result_status")) {
							sourceMonitoringMetadataDto1.setResultStatus(results.getString("result_status"));
						}
						if (results.has("source_project")) {
							sourceMonitoringMetadataDto1.setSourceProject(results.getString("source_project"));
						}
						if (results.has("cycle_id")) {
							sourceMonitoringMetadataDto1.setCycleId(results.getString("cycle_id"));
						}
						if (results.has("source_id")) {
							sourceMonitoringMetadataDto1.setSourceId(results.getString("source_id"));
						}
						if (results.has("section")) {
							sourceMonitoringMetadataDto1.setSection(results.getString("section"));
						}
						if (results.has("execution_datetime")) {
							sourceMonitoringMetadataDto1.setExecutionDatetime(results.getString("execution_datetime"));
						}
						if (results.has("id")) {
							sourceMonitoringMetadataDto1.setId(results.getString("id"));
						}
						if (results.has("response_datetime")) {
							sourceMonitoringMetadataDto1.setResponseDatetime(results.getString("response_datetime"));
						}
						if (results.has("execution_url")) {
							sourceMonitoringMetadataDto1.setExecutionUrl(results.getString("execution_url"));
						}

						sourceMonitoringMetadataDtos.add(sourceMonitoringMetadataDto1);
					}
				}
			}
		}

		Map<String, Map<String, List<SourceMonitoringMetadataDto>>> result = sourceMonitoringMetadataDtos.stream()
				.collect(Collectors.groupingBy(SourceMonitoringMetadataDto::getSourceId, Collectors.groupingBy(
						SourceMonitoringMetadataDto::getSection, Collectors.mapping(
								o -> new SourceMonitoringMetadataDto(o.getExecutionDuration(), o.getResultCode(),
										o.getResultStatus(), o.getSourceProject(), o.getCycleId(), o.getSourceId(),
										o.getSection(), o.getExecutionDatetime(), o.getId(), o.getResponseDatetime(),
										o.getExecutionUrl(), o.getResultStacktrace(), o.getResultMessage(),
										o.getResultsValidityDto(), false),
								Collectors.toList()))));

		result.entrySet().stream().forEach(o -> o.getValue().entrySet().stream().forEach(j -> {
			/*
			 * List<SourceMonitoringMetadataDto>
			 * filtered=j.getValue().stream().filter(m ->
			 * m.getResultCode()!=200).collect(Collectors.toList());
			 * if(filtered==null || filtered.size()==0) {
			 */
			List<SourceMonitoringMetadataDto> listOfDtos = j.getValue().stream().collect(Collectors.toList());
			if (listOfDtos != null && listOfDtos.size() > 0) {
				Instant maxVal = null;
				int position = 0;
				for (int i = 0; i < listOfDtos.size(); i++) {
					if (i == 0) {
						maxVal = Instant.parse(listOfDtos.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
						position = i;
					} else {
						int val = maxVal.compareTo(
								Instant.parse(listOfDtos.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z"));
						if (val < 0) {
							maxVal = Instant
									.parse(listOfDtos.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
							position = i;
						}
					}
				}
				listOfDtos.get(position).setToBeAdded(true);
			}
			// }
			/*
			 * else { List<SourceMonitoringMetadataDto>
			 * failedDtos=j.getValue().stream().filter(m ->
			 * m.getResultCode()!=200).collect(Collectors.toList());
			 * if(failedDtos!=null && failedDtos.size()>0) { Instant
			 * maxVal=null;int position=0; for(int i=0;i<failedDtos.size();i++)
			 * { if(i==0) {
			 * maxVal=Instant.parse(failedDtos.get(i).getExecutionDatetime().
			 * trim().replace(' ','T' )+"Z"); position=i; } else { int
			 * val=maxVal.compareTo(Instant.parse(failedDtos.get(i).
			 * getExecutionDatetime().trim().replace(' ','T' )+"Z")); if(val>0)
			 * { maxVal=Instant.parse(failedDtos.get(i).getExecutionDatetime().
			 * trim().replace(' ','T' )+"Z"); position=i; } } }
			 * failedDtos.get(position).setToBeAdded(true); } }
			 */
		}));

		result.forEach((p, o) -> o.forEach((i, j) -> j.removeIf(m -> (!m.isToBeAdded()))));
		int failedCount = 0;
		int successCount = 0;
		Map<String, SourceMonitoringMetadataDto> finalResult = new HashedMap<String, SourceMonitoringMetadataDto>();
		for (Map.Entry<String, Map<String, List<SourceMonitoringMetadataDto>>> entry : result.entrySet()) {
			List<SourceMonitoringMetadataDto> tempList = new ArrayList<SourceMonitoringMetadataDto>();
			List<SourceMonitoringMetadataDto> failedStatusList = new ArrayList<SourceMonitoringMetadataDto>();
			for (Map.Entry<String, List<SourceMonitoringMetadataDto>> entry1 : entry.getValue().entrySet()) {
				for (SourceMonitoringMetadataDto dto : entry1.getValue()) {
					tempList.add(dto);
				}
			}
			for (SourceMonitoringMetadataDto tempDto : tempList) {
				if (tempDto.getResultCode() != 200) {
					failedStatusList.add(tempDto);
				}
			}
			if (!failedStatusList.isEmpty() || failedStatusList.size() > 0) {
				Instant maxVal = null;
				int position = 0;
				for (int i = 0; i < failedStatusList.size(); i++) {
					if (i == 0) {
						maxVal = Instant
								.parse(failedStatusList.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
						position = i;
					} else {
						int val = maxVal.compareTo(Instant
								.parse(failedStatusList.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z"));
						if (val < 0) {
							maxVal = Instant.parse(
									failedStatusList.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
							position = i;
						}
					}
				}
				finalResult.put(entry.getKey(), failedStatusList.get(position));
				failedCount++;
			} else if (tempList != null && tempList.size() > 0) {
				Instant maxVal = null;
				int position = 0;
				for (int i = 0; i < tempList.size(); i++) {
					if (i == 0) {
						maxVal = Instant.parse(tempList.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
						position = i;
					} else {
						int val = maxVal.compareTo(
								Instant.parse(tempList.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z"));
						if (val < 0) {
							maxVal = Instant
									.parse(tempList.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
							position = i;
						}
					}
				}
				finalResult.put(entry.getKey(), tempList.get(position));
				successCount++;
			}
		}
		List<Sources> sourceList = sourcesService.findAll();

		for (Map.Entry<String, SourceMonitoringMetadataDto> entry : finalResult.entrySet()) {
			List<Sources> tempSourceList = sourceList;
			tempSourceList = tempSourceList.stream().filter(s -> s.getSourceName().equalsIgnoreCase(entry.getKey()))
					.collect(Collectors.toList());
			if (tempSourceList != null && tempSourceList.size() > 0) {
				Sources source = tempSourceList.get(0);
				entry.getValue().setSourceName(source.getSourceDisplayName());
				entry.getValue().setCategory(source.getCategory());
				Hibernate.initialize(source.getSourceJurisdiction());
				entry.getValue().setJurisdictions(source.getSourceJurisdiction());
				Hibernate.initialize(source.getSourceUrl());
				entry.getValue().setUrl(source.getSourceUrl());
				Hibernate.initialize(source.getSourceDomain());
				entry.getValue().setDomain(source.getSourceDomain());
				Hibernate.initialize(source.getSourceIndustry());
				entry.getValue().setIndustry(source.getSourceIndustry());
			}

		}

		SourceMonitoringFinalDto monitoringDto = new SourceMonitoringFinalDto();
		monitoringDto.setSourceMetadata(finalResult);
		monitoringDto.setFailedCount(failedCount);
		monitoringDto.setSuccessCount(successCount);

		return monitoringDto;

	}

	@Override
	//public List<SourceMonitoringMetadataDto> getDataById(String dateLimit, String source_id) throws Exception {
	public List<SourceMonitoringMetadataDto> getSparklineDataById(String dateLimit, String source_id) throws Exception {
		// String url = BIGDATA_SOURCE_MONITORING_URL + "/source-monitoring?id="
		// + encode(resultId);
		// String response = getDataFromServer(url);

		/*String url = BIGDATA_SOURCE_MONITORING_URL + "/source-monitoring/metadata";

		if (dateLimit != null && source_id == null)
			url = url + "?dateLimit=" + dateLimit;
		if (source_id != null && dateLimit == null){
			dateLimit = "7";
			url = url + "?source_id=" + source_id;
		}
		if (dateLimit != null && source_id != null)
			url = url + "?" + "source_id=" + source_id + "&" + "dateLimit=" + dateLimit;
		String response = getDataFromServer(url);*/
		
		String requestId = UUID.randomUUID().toString();
		String url = BIGDATA_SOURCE_MONITORING_URL + "/source-monitoring/metadata-async?request_id="+requestId;

		if (dateLimit != null && source_id == null)
			url = url + "&dateLimit=" + dateLimit;
		if (source_id != null && dateLimit == null){
			dateLimit = "7";
			url = url + "&source_id=" + source_id;
		}
		if (dateLimit != null && source_id != null)
			url = url + "&" + "source_id=" + source_id + "&" + "dateLimit=" + dateLimit;
		
		String response = getDataFromServer(url);
		JSONObject monitoringJson = null;
		if (response != null) {
			monitoringJson = new JSONObject(response);
		}
		if(monitoringJson !=null){
			while(!monitoringJson.has("results")){
				response = getDataFromServer(url);
				if (response != null) {
					monitoringJson = new JSONObject(response);
				}
				Thread.sleep(1500);
			}
		}

		List<SourceMonitoringMetadataDto> sourceMonitoringMetadataDto = new ArrayList<SourceMonitoringMetadataDto>();
		try {
			if (response != null) {
				JSONObject json = new JSONObject(response);
				if (json != null && json.has("results")) {
					JSONArray resultsArray = json.getJSONArray("results");
					if (resultsArray != null && resultsArray.length() > 0) {
						for (int i = 0; i < resultsArray.length(); i++) {
							SourceMonitoringMetadataDto sourceMonitoringMetadataDto1 = new SourceMonitoringMetadataDto();
							ResultsValidityDto resultsValidityDto = new ResultsValidityDto();

							JSONObject results = resultsArray.getJSONObject(i);
							if (results.has("result_stacktrace")) {
								sourceMonitoringMetadataDto1
										.setResultStacktrace(results.getString("result_stacktrace"));
							}
							if (results.has("execution_duration")) {
								sourceMonitoringMetadataDto1
										.setExecutionDuration(results.getString("execution_duration"));
							}
							if (results.has("result_code")) {
								sourceMonitoringMetadataDto1.setResultCode(results.getLong("result_code"));
							}
							if (results.has("result_status")) {
								sourceMonitoringMetadataDto1.setResultStatus(results.getString("result_status"));
							}
							if (results.has("source_project")) {
								sourceMonitoringMetadataDto1.setSourceProject(results.getString("source_project"));
							}
							if (results.has("cycle_id")) {
								sourceMonitoringMetadataDto1.setCycleId(results.getString("cycle_id"));
							}
							if (results.has("source_id")) {
								sourceMonitoringMetadataDto1.setSourceId(results.getString("source_id"));
							}
							if (results.has("section")) {
								sourceMonitoringMetadataDto1.setSection(results.getString("section"));
							}

							if (results.has("result_message")) {
								sourceMonitoringMetadataDto1.setResultMessage(results.getString("result_message"));
							}

							if (results.has("execution_datetime")) {
								sourceMonitoringMetadataDto1
										.setExecutionDatetime(results.getString("execution_datetime"));
							}
							if (results.has("id")) {
								sourceMonitoringMetadataDto1.setId(results.getString("id"));
							}
							if (results.has("response_datetime")) {
								sourceMonitoringMetadataDto1
										.setResponseDatetime(results.getString("response_datetime"));
							}
							if (results.has("execution_url")) {
								sourceMonitoringMetadataDto1.setExecutionUrl(results.getString("execution_url"));
							}
							if (results.has("results_validity")) {
								JSONObject resultsValidity = results.getJSONObject("results_validity");
								if (resultsValidity.has("values_changed")) {
									resultsValidityDto.setValuesChanged(
											resultsValidity.getJSONObject("values_changed").toString());
								}
								if (resultsValidity.has("matching_status")) {
									resultsValidityDto.setMatchingStatus(resultsValidity.getString("matching_status"));
								}
								sourceMonitoringMetadataDto1.setResultsValidityDto(resultsValidityDto);
							}

							sourceMonitoringMetadataDto.add(sourceMonitoringMetadataDto1);
						}
					}
				}
			}

			return sourceMonitoringMetadataDto;
		} catch (Exception e) {
			e.printStackTrace();
			return sourceMonitoringMetadataDto;
		}
	}

	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<ErrorBubbleChartDto> getErrorData(String dateLimit) throws Exception {

		//String url = BIGDATA_SOURCE_MONITORING_URL + "/source-monitoring/metadata";
		//url = url + "?dateLimit=" + dateLimit;
		
		String requestId = UUID.randomUUID().toString();
		String url = BIGDATA_SOURCE_MONITORING_URL + "/source-monitoring/metadata-async?request_id="+requestId;
		url = url + "&dateLimit=" + dateLimit;
		
		String response = getDataFromServer(url);
		JSONObject monitoringJson = null;
		if (response != null) {
			monitoringJson = new JSONObject(response);
		}
		if(monitoringJson !=null){
			while(!monitoringJson.has("results")){
				response = getDataFromServer(url);
				if (response != null) {
					monitoringJson = new JSONObject(response);
				}
				Thread.sleep(1500);
			}
		}

		List<SourceMonitoringMetadataDto> sourceMonitoringMetadataDtos = new ArrayList<SourceMonitoringMetadataDto>();

		if (response != null) {
			JSONObject json = new JSONObject(response);
			if (json != null && json.has("results")) {
				JSONArray resultsArray = json.getJSONArray("results");
				if (resultsArray != null && resultsArray.length() > 0) {
					for (int i = 0; i < resultsArray.length(); i++) {
						SourceMonitoringMetadataDto sourceMonitoringMetadataDto1 = new SourceMonitoringMetadataDto();
						JSONObject results = resultsArray.getJSONObject(i);
						if (results.has("execution_duration")) {
							sourceMonitoringMetadataDto1.setExecutionDuration(results.getString("execution_duration"));
						}
						if (results.has("result_code")) {
							sourceMonitoringMetadataDto1.setResultCode(results.getLong("result_code"));
						}
						if (results.has("result_status")) {
							sourceMonitoringMetadataDto1.setResultStatus(results.getString("result_status"));
						}
						if (results.has("source_project")) {
							sourceMonitoringMetadataDto1.setSourceProject(results.getString("source_project"));
						}
						if (results.has("cycle_id")) {
							sourceMonitoringMetadataDto1.setCycleId(results.getString("cycle_id"));
						}
						if (results.has("source_id")) {
							sourceMonitoringMetadataDto1.setSourceId(results.getString("source_id"));
						}
						if (results.has("section")) {
							sourceMonitoringMetadataDto1.setSection(results.getString("section"));
						}
						if (results.has("execution_datetime")) {
							Date exec_date = new SimpleDateFormat("yyyy-MM-dd")
									.parse(results.getString("execution_datetime"));
							String []dateTime = results.getString("execution_datetime").trim().split(" ");
							String date = null;
							if(dateTime != null && dateTime.length>0)
								date = dateTime[0];
							sourceMonitoringMetadataDto1.setTempDate(date);
							sourceMonitoringMetadataDto1.setExecutionDatetime(results.getString("execution_datetime"));
						}
						if (results.has("id")) {
							sourceMonitoringMetadataDto1.setId(results.getString("id"));
						}
						if (results.has("response_datetime")) {
							sourceMonitoringMetadataDto1.setResponseDatetime(results.getString("response_datetime"));
						}
						if (results.has("execution_url")) {
							sourceMonitoringMetadataDto1.setExecutionUrl(results.getString("execution_url"));
						}

						sourceMonitoringMetadataDtos.add(sourceMonitoringMetadataDto1);
					}
				}
			}
		}

		Map<String, List<SourceMonitoringMetadataDto>> result = sourceMonitoringMetadataDtos.stream()
				.collect(Collectors.groupingBy(SourceMonitoringMetadataDto::getTempDate,
						Collectors.mapping(o -> new SourceMonitoringMetadataDto(o.getExecutionDuration(),
								o.getResultCode(), o.getResultStatus(), o.getSourceProject(), o.getCycleId(),
								o.getSourceId(), o.getSection(), o.getExecutionDatetime(), o.getId(),
								o.getResponseDatetime(), o.getExecutionUrl(), o.getResultStacktrace(),
								o.getResultMessage(), o.getResultsValidityDto(), false), Collectors.toList())));
		
		
		List<ErrorBubbleChartDto> bubbleChartDtoList = new ArrayList<ErrorBubbleChartDto>();
		result.entrySet().stream().forEach(r-> {
			int errorCount = 0;
			ErrorBubbleChartDto bubbleChartDto = new ErrorBubbleChartDto();
			List<SourceMonitoringMetadataDto> listOfDtos = r.getValue().stream().collect(Collectors.toList());
			for(SourceMonitoringMetadataDto dto : listOfDtos){
				if(dto.getResultCode() != 200){
					errorCount++;
				}
			}
			bubbleChartDto.setErrorCount(errorCount);
			bubbleChartDto.setDate(r.getKey());
			bubbleChartDtoList.add(bubbleChartDto);
		});
		
		/*result.entrySet().stream().forEach(j -> {

			List<SourceMonitoringMetadataDto> listOfDtos = j.getValue().stream().collect(Collectors.toList());
			if (listOfDtos != null && listOfDtos.size() > 0) {
				Instant maxVal = null;
				int position = 0;
				for (int i = 0; i < listOfDtos.size(); i++) {
					if (i == 0) {
						maxVal = Instant.parse(listOfDtos.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
						position = i;
					} else {
						int val = maxVal.compareTo(
								Instant.parse(listOfDtos.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z"));
						if (val < 0) {
							maxVal = Instant
									.parse(listOfDtos.get(i).getExecutionDatetime().trim().replace(' ', 'T') + "Z");
							position = i;
						}
					}
				}
				listOfDtos.get(position).setToBeAdded(true);
			}
		});*/
		//result.forEach((i, j) -> j.removeIf(m -> (!m.isToBeAdded())));

		/*List<Sources> sourceList = sourcesService.findAll();

		for (Map.Entry<String, List<SourceMonitoringMetadataDto>> entry : result.entrySet()) {
			List<Sources> tempSourceList = sourceList;
			tempSourceList = tempSourceList.stream().filter(s -> s.getSourceName().equalsIgnoreCase(source_id))
					.collect(Collectors.toList());
			if (tempSourceList != null && tempSourceList.size() > 0) {
				Sources source = tempSourceList.get(0);
				
				entry.getValue().stream().forEach(s -> {
					s.setSourceName(source.getSourceDisplayName());
					s.setCategory(source.getCategory());
					Hibernate.initialize(source.getSourceJurisdiction());
					s.setJurisdictions(source.getSourceJurisdiction());
					s.setUrl(source.getSourceUrl());
					Hibernate.initialize(source.getSourceDomain());
					s.setDomain(source.getSourceDomain());
					Hibernate.initialize(source.getSourceIndustry());
					s.setIndustry(source.getSourceIndustry());
				});
			}

		}*/

		return bubbleChartDtoList;

	}

}
