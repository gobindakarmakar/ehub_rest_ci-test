package element.bst.elementexploration.rest.document.dao;

import java.util.List;

import element.bst.elementexploration.rest.document.domain.DocumentVaultUserMapping;
import element.bst.elementexploration.rest.document.dto.DocumentUserMappingDTO;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * 
 * @author Viswanath
 *
 */
public interface DocumentMappingDao extends GenericDao<DocumentVaultUserMapping, Long>{

	Boolean isPermissionWrite(Long userId, Long docId);

	Boolean isReadOrWritePermission(Long userId, Long docId);

	DocumentVaultUserMapping findByUserIdAndDocId(long userId, long userId2);

	List<DocumentUserMappingDTO> getUserList(Long docId);
}
