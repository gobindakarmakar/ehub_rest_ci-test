package element.bst.elementexploration.rest.settings.listManagement.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.listManagement.domain.Role;
import element.bst.elementexploration.rest.settings.listManagement.dto.RoleDto;
import element.bst.elementexploration.rest.settings.listManagement.service.RoleService;
import element.bst.elementexploration.rest.util.EntityConverter;

@Service("roleService")
@Transactional("transactionManager")
public class RoleServiceImpl extends GenericServiceImpl<Role, Long> implements RoleService {

	@Autowired
	RoleService roleService;
	
	
	@Autowired
	public RoleServiceImpl(GenericDao<Role, Long> genericDao) {
		super(genericDao);
	}

	public RoleServiceImpl() {

	}
	
	@Override
	public List<RoleDto> getRoles() {
		List<Role> iconList = roleService.findAll();
		List<RoleDto> iconDtosList = new ArrayList<RoleDto>();
		if (iconList != null) {
			iconDtosList = iconList.stream().map(
					(entityComplexStructureDto) -> new EntityConverter<Role, RoleDto>(Role.class, RoleDto.class)
					.toT2(entityComplexStructureDto, new RoleDto()))
					.collect(Collectors.toList());
		}
		return iconDtosList;
	}

}
