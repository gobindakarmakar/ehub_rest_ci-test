package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.advancesearch.dao.AdvanceNewsFavouriteDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;
import element.bst.elementexploration.rest.extention.advancesearch.dto.CustomOfficer;
import element.bst.elementexploration.rest.extention.advancesearch.dto.CustomShareHolder;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityListDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityReponseDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityRequestDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityScreeningDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.IdentifiersRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.OfficerShipRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ProfileDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ProfileResponseDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ShareHolderRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.advancesearch.service.EntityOfficerInfoService;
import element.bst.elementexploration.rest.extention.adversenews.service.AdverseNewsService;
import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.extention.entity.service.EntityAttributeService;
import element.bst.elementexploration.rest.extention.entitysearch.dto.AnnualReturnResponseDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.ShareholdingDto;
import element.bst.elementexploration.rest.extention.entitysearch.service.EntitySearchService;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantWatchList;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantCommentService;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantWatchListService;
import element.bst.elementexploration.rest.extention.source.addToPage.dto.SourceAddToPageDto;
import element.bst.elementexploration.rest.extention.source.addToPage.service.SourceAddToPageService;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.DataAttributesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesHideStatusDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SubClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.ClassificationsService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesHideStatusService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.extention.tuna.service.TunaService;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * 
 * @author Viswanath Reddy G
 *
 */
@Service("advanceSearchService")
@Transactional("transactionManager")
public class AdvanceSearchServiceImpl implements AdvanceSearchService {

	@Value("${workflow_extention_url_development}")
	private String WORKFLOW_EXTENSION_URL_DEVELOPMENT;

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENSION_URL;

	@Value("${imageAllocationArea}")
	private String TESSRACT_IMAGE_ALLOCATION;

	@Value("${corporate_structure_location}")
	private String CORPORATE_STRUCTURE_LOCATION;
	
	@Value("${bigdata_s3_doc_url}")
	private String BIG_DATA_S3_DOC_URL;

	@Value("${bigdata_multisource_url}")
	private String BIGDATA_MULTISOURCE_URL;

	@Value("${be_entity_screenshot_url}")
	private String SCREEN_SHOT_URL;

	@Value("${bigdata_org_url}")
	private String BIG_DATA__ORG_URL;

	@Value("${screening_url}")
	private String SCREENING_URL;
	
	@Value("${client_id}")
	private String clientId;

	@Lazy
	@Autowired
	private EntitySearchService entitySearchService;

	@Autowired
	AdverseNewsService adverseNewsService;

	@Autowired
	TunaService tunaService;

	@Autowired
	private AdvanceNewsFavouriteDao advanceNewsFavouriteDao;

	@Autowired
	private SourcesService sourcesService;
	
	@Autowired
	private SourcesHideStatusService sourcesHideStatusService;

	@Autowired
	private ClassificationsService classificationsService;

	@Autowired
	private EntityAttributeService entityAttributeService;
	
	@Autowired
	private SourceAddToPageService sourceAddToPageService;

	@Autowired
	private SignificantWatchListService significantWatchListService;
	
	@Autowired
	private AuditLogService auditLogService;

	@Value("${client_id}")
	private String CLIENT_ID;

	@Autowired
	private UsersService usersService;

	@Autowired
	private ThreadPoolExecutor executor;

	@Value("${be_entity_api_key}")
	private String BE_ENTITY_API_KEY;
	
	@Autowired
	SignificantCommentService significantCommentService;
	
	@Autowired
	private EntityOfficerInfoService entityOfficerInfoService;

	@Override
	public String getNewSearchResults(String newsSearchRequest) throws Exception {
		String url = WORKFLOW_EXTENSION_URL + "/graph/api/search/entity";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, newsSearchRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getEntityByName(String name) throws Exception {
		String url = WORKFLOW_EXTENSION_URL_DEVELOPMENT + "/graph/api/graph/entity/"
				+ URLEncoder.encode(name, "UTF-8").replaceAll("\\+", "%20");
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}

	}

	@Override
	public String getHierarchyData(HierarchyDto hierarchyDto, Long userId,String apiKey) throws Exception {
		String response = null;
		if (hierarchyDto != null) {
			String url = hierarchyDto.getUrl();
			String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					if (userId == null) {
						return response;
					}
				} else {
					return response;
				}
			} else {
				return response;
			}
		} else {
			return response;
		}
		return response;
	}

	public String mergeShareholderData(String url) throws Exception {
		JSONArray finalShareholders = new JSONArray();
		/*
		 * String serverResponse[] = null; serverResponse =
		 * ServiceCallHelper.getDataFromServerWithoutTimeout(url); if
		 * (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) ==
		 * 200) { String response = serverResponse[1]; JSONObject json = new
		 * JSONObject(); json = new JSONObject(response); if
		 * (json.has("is-completed")) { int counter = 0; while
		 * (json.has("is-completed") && !json.getBoolean("is-completed")) {
		 * serverResponse =
		 * ServiceCallHelper.getDataFromServerWithoutTimeout(url); if
		 * (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) ==
		 * 200) { response = serverResponse[1]; if
		 * (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
		 * json = new JSONObject(response); } } counter++; if (counter == 6)
		 * break; Thread.sleep(5000); } }
		 */
		// if (json.has("data")) {
		// JSONObject data = json.getJSONObject("data");
		JSONObject json = new JSONObject();
		String response = getDataInAsynMode(url);
		if (response != null) {
			json = new JSONObject(response);
		}
		if (json != null && json.has("results")) {
			JSONArray results = json.getJSONArray("results");
			if (results.length() > 0) {
				JSONObject resultsJson = results.getJSONObject(0);
				if (resultsJson!=null && resultsJson.has("shareholders")) {
					JSONObject shareholders = resultsJson.getJSONObject("shareholders");
					// if (result.has("data")) {
					JSONArray bstShareholders = new JSONArray();
					// JSONObject dataJson = result.getJSONObject("data");
					JSONArray sources = shareholders.names();
					if (sources != null && sources.length() > 0) {
						for (int i = 0; i < sources.length(); i++) {
							String source = sources.getString(i);
							if (source.contains("BST")) {
								bstShareholders = formatBstShareholderJson(
										shareholders.getJSONArray(sources.getString(i)));
							}
							if (source.contains("yahoo")) {
								// JSONArray sourceArray =
								// shareholders.getJSONArray(sources.getString(i));
								// JSONObject sourceJson =
								// sourceArray.getJSONObject(0);
								finalShareholders = shareholders.getJSONArray(sources.getString(i));
							}

						}

						for (int i = 0; i < sources.length(); i++) {
							String source = sources.getString(i);
							// JSONArray sourceArray =
							// shareholders.getJSONArray(sources.getString(i));
							// JSONObject sourceJson =
							// sourceArray.getJSONObject(0);
							if (shareholders != null) {
								if (shareholders.get(sources.getString(i)) instanceof JSONArray) {
									if (shareholders.getJSONArray(sources.getString(i)).length() > 0
											|| (source != null && source.equalsIgnoreCase("BST"))) {
										JSONArray shareHoldersArray = new JSONArray();
										if (source.equalsIgnoreCase("BST")) {
											shareHoldersArray = bstShareholders;
										} else {
											shareHoldersArray = shareholders.getJSONArray(sources.getString(i));
										}
										if (finalShareholders.length() == 0) {
											finalShareholders = shareHoldersArray;

										} else if (!source.contains("yahoo")) {
											for (int j = 0; j < shareHoldersArray.length(); j++) {
												JSONObject shareHolderData = shareHoldersArray.getJSONObject(j);
												finalShareholders.put(shareHolderData);
											}
										}
									}
								}
							}

						}
					}

					// }
				}
			}
		}
		// }
		// }
		return finalShareholders.toString();

	}

	private JSONArray formatBstShareholderJson(JSONArray jsonArray) {
		JSONArray bstShareholders = new JSONArray();

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject bstShareholder = new JSONObject();
			JSONObject shareholder = jsonArray.getJSONObject(i);
			if (shareholder.has("basic")) {
				JSONObject basic = shareholder.getJSONObject("basic");
				if (basic.has("vcard:organization-name")) {
					String organizationName = basic.getString("vcard:organization-name");
					bstShareholder.put("holder_name", organizationName);
				}
			}
			bstShareholder.put("holder_type", "company");
			bstShareholder.put("equities", "");
			bstShareholder.put("percentage", "");
			bstShareholder.put("total_value", "");
			bstShareholders.put(bstShareholder);

		}
		return bstShareholders;
	}

	/*
	 * @Override public String getOrgParentData(EntityRequestDto entityRequest)
	 * throws Exception { List<EntityRequestDataDto> entityRequestList = new
	 * ArrayList<EntityRequestDataDto>(); EntityParentDataDto
	 * entityParentDataDto = new EntityParentDataDto(); EntityOfficershipListDto
	 * officership = new EntityOfficershipListDto(); OwnedByOwnershipListDto
	 * ownedBy = new OwnedByOwnershipListDto(); OwnedByOwnershipListDto
	 * ownership = new OwnedByOwnershipListDto();
	 * EntityShareholdersSubsidiarisListDto shareholders = new
	 * EntityShareholdersSubsidiarisListDto();
	 * EntityShareholdersSubsidiarisListDto subsidiaries = new
	 * EntityShareholdersSubsidiarisListDto();
	 * 
	 * if (entityRequest != null) entityRequestList =
	 * entityRequest.getEntityRequest(); for (int i = 0; i <
	 * entityRequestList.size(); i++) { String entityData =
	 * getNewSearchResults(entityRequestList.get(i).toString()); if (entityData
	 * != null) { JSONObject json = new JSONObject(entityData); if
	 * (json.has("data")) { JSONObject data = json.getJSONObject("data"); if
	 * (data != null && data.has("results")) { JSONArray resultsArray =
	 * data.getJSONArray("results"); if (resultsArray != null &&
	 * resultsArray.length() > 0) { JSONObject finalResultObject =
	 * resultsArray.getJSONObject(0); if (finalResultObject != null &&
	 * finalResultObject.has("merged-result")) { JSONObject mergedResultObject =
	 * finalResultObject.getJSONObject("merged-result"); // for officership if
	 * (mergedResultObject != null && mergedResultObject.has("officership")) {
	 * if (mergedResultObject.get("officership") instanceof JSONArray) {
	 * JSONArray officershipArrayObject = mergedResultObject
	 * .getJSONArray("officership"); if (officershipArrayObject != null &&
	 * officershipArrayObject.length() > 0) { List<EntityofficershipDto>
	 * entityofficershipDtosList = new ArrayList<EntityofficershipDto>(); for
	 * (int off = 0; off < officershipArrayObject.length(); off++) {
	 * EntityofficershipDto entityofficershipDto = new EntityofficershipDto();
	 * // for source if
	 * ((officershipArrayObject.getJSONObject(off).has("source")) &&
	 * (!officershipArrayObject.getJSONObject(off).get("source") .equals(null))
	 * && (officershipArrayObject.getJSONObject(off) .get("source") instanceof
	 * String)) { String source = officershipArrayObject.getJSONObject(off)
	 * .getString("source"); entityofficershipDto.setSource(source); }
	 * 
	 * // for value if ((officershipArrayObject.getJSONObject(off).has("value"))
	 * && (!officershipArrayObject.getJSONObject(off).get("value")
	 * .equals(null)) && (officershipArrayObject.getJSONObject(off)
	 * .get("value") instanceof JSONArray)) { List<EntityOfficershipValueDto>
	 * valueList = new ArrayList<EntityOfficershipValueDto>(); JSONArray
	 * officershipValueArray = officershipArrayObject
	 * .getJSONObject(off).getJSONArray("value"); if ((officershipValueArray !=
	 * null) && (officershipValueArray.length() > 0)) { for (int offVal = 0;
	 * offVal < officershipValueArray .length(); offVal++) {
	 * EntityOfficershipValueDto value = new EntityOfficershipValueDto(); // for
	 * address if ((officershipValueArray.getJSONObject(offVal) .has("address"))
	 * && (!officershipValueArray.getJSONObject(offVal)
	 * .get("address").equals(null))) { EntityOfficershipValueAddressDto address
	 * = new EntityOfficershipValueAddressDto(); JSONObject addressObject =
	 * officershipValueArray .getJSONObject(offVal).getJSONObject("address"); //
	 * for country if ((addressObject.has("country")) &&
	 * (!addressObject.get("country").equals(null)) && (addressObject
	 * .get("country") instanceof String)) { String country =
	 * addressObject.getString("country"); address.setCountry(country); } // for
	 * // postal_code if ((addressObject.has("postal_code")) &&
	 * (!addressObject.get("postal_code") .equals(null)) && (addressObject.get(
	 * "postal_code") instanceof String)) { String postal_code = addressObject
	 * .getString("postal_code"); address.setPostal_code(postal_code); } // for
	 * // address_line_1 if ((addressObject.has("address_line_1")) &&
	 * (!addressObject.get("address_line_1") .equals(null)) &&
	 * (addressObject.get( "address_line_1") instanceof String)) { String
	 * address_line_1 = addressObject .getString("address_line_1");
	 * address.setAddress_line_1(address_line_1); } value.setAddress(address); }
	 * 
	 * // for name if ((officershipValueArray.getJSONObject(offVal)
	 * .has("name")) && (!officershipValueArray.getJSONObject(offVal)
	 * .get("name").equals(null)) &&
	 * (officershipValueArray.getJSONObject(offVal) .get("name") instanceof
	 * String)) { String name = officershipValueArray
	 * .getJSONObject(offVal).getString("name"); value.setName(name); }
	 * 
	 * // for status if ((officershipValueArray.getJSONObject(offVal)
	 * .has("status")) && (!officershipValueArray.getJSONObject(offVal)
	 * .get("status").equals(null)) &&
	 * (officershipValueArray.getJSONObject(offVal) .get("status") instanceof
	 * String)) { String status = officershipValueArray
	 * .getJSONObject(offVal).getString("status"); value.setStatus(status); }
	 * 
	 * // for officer_role if ((officershipValueArray.getJSONObject(offVal)
	 * .has("officer_role")) && (!officershipValueArray.getJSONObject(offVal)
	 * .get("officer_role").equals(null)) &&
	 * (officershipValueArray.getJSONObject(offVal) .get("officer_role")
	 * instanceof String)) { String officer_role = officershipValueArray
	 * .getJSONObject(offVal) .getString("officer_role");
	 * value.setOfficer_role(officer_role); }
	 * 
	 * // for appointed_on if ((officershipValueArray.getJSONObject(offVal)
	 * .has("appointed_on")) && (!officershipValueArray.getJSONObject(offVal)
	 * .get("appointed_on").equals(null)) &&
	 * (officershipValueArray.getJSONObject(offVal) .get("appointed_on")
	 * instanceof String)) { String appointed_on = officershipValueArray
	 * .getJSONObject(offVal) .getString("appointed_on");
	 * value.setAppointed_on(appointed_on); }
	 * 
	 * valueList.add(value); } }
	 * 
	 * entityofficershipDto.setValue(valueList);
	 * 
	 * } entityofficershipDtosList.add(entityofficershipDto); }
	 * 
	 * officership.setOfficership(entityofficershipDtosList); } } } }
	 * 
	 * }
	 * 
	 * } } } }
	 * 
	 * return null; }
	 */

	@Override
	public EntityListDataDto getOrgDataOfEntity(EntityRequestDto entityRequestDto) throws Exception {
		EntityListDataDto entityListDataDto = new EntityListDataDto();
		List<EntityReponseDataDto> entityReponseDataDtoList = new ArrayList<EntityReponseDataDto>();
		List<EntityRequestDataDto> entityRequestList = new ArrayList<EntityRequestDataDto>();
		if (entityRequestDto != null)
			entityRequestList = entityRequestDto.getEntityRequest();
		for (int i = 0; i < entityRequestList.size(); i++) {
			EntityRequestDataDto entityRequestDataDto = entityRequestList.get(i);
			JSONObject request = new JSONObject();
			if (entityRequestDataDto.getIdentifier() != null)
				request.put("identifier", entityRequestDataDto.getIdentifier());
			if (entityRequestDataDto.getJurisdiction() != null)
				request.put("jurisdiction", entityRequestDataDto.getJurisdiction());
			if (entityRequestDataDto.getKeyword() != null)
				request.put("keyword", entityRequestDataDto.getKeyword());
			if (entityRequestDataDto.getLight_weight() != null)
				request.put("light-weight", entityRequestDataDto.getLight_weight());
			if (entityRequestDataDto.getSearchType() != null)
				request.put("searchType", entityRequestDataDto.getSearchType());
			String entityData = getNewSearchResults(request.toString());
			if (entityData != null) {
				JSONObject json = new JSONObject(entityData);
				if (json.has("data")) {
					JSONObject data = json.getJSONObject("data");
					if (data != null && data.has("results")) {
						JSONArray resultsArray = data.getJSONArray("results");
						if (resultsArray != null && resultsArray.length() > 0) {
							JSONObject finalResultObject = resultsArray.getJSONObject(0);
							if (finalResultObject != null && finalResultObject.has("merged-result")) {
								JSONObject mergedResultObject = finalResultObject.getJSONObject("merged-result");
								EntityReponseDataDto entityReponseDataDto = new EntityReponseDataDto();
								// for officership
								if (mergedResultObject != null && mergedResultObject.has("officership")) {
									entityReponseDataDto
									.setOfficership(mergedResultObject.getJSONArray("officership").toString());

								}
								// for owned-by
								if (mergedResultObject != null && mergedResultObject.has("owned-by")) {
									entityReponseDataDto
									.setOwnedBy(mergedResultObject.getJSONArray("owned-by").toString());

								}
								// for ownership
								if (mergedResultObject != null && mergedResultObject.has("ownership")) {
									entityReponseDataDto
									.setOwnership(mergedResultObject.getJSONArray("ownership").toString());

								}
								// for shareholders
								if (mergedResultObject != null && mergedResultObject.has("shareholders")) {
									entityReponseDataDto.setShareholders(
											mergedResultObject.getJSONArray("shareholders").toString());

								}
								// for subsidiaries
								if (mergedResultObject != null && mergedResultObject.has("subsidiaries")) {
									entityReponseDataDto.setSubsidiaries(
											mergedResultObject.getJSONArray("subsidiaries").toString());

								}
								entityReponseDataDto.setIdentifier(entityRequestDataDto.getIdentifier());
								entityReponseDataDto.setKeyword(entityRequestDataDto.getKeyword());

								entityReponseDataDtoList.add(entityReponseDataDto);

							}

						}

					}

				}

			}
			entityListDataDto.setEntityReponseDataDtoList(entityReponseDataDtoList);

		}

		return entityListDataDto;
	}

	@SuppressWarnings("unused")
	@Override
	public List<ProfileDataDto> getProfileData(EntityRequestDto entityRequestDto) throws Exception {

		List<ProfileDataDto> result = new ArrayList<>();
		if (entityRequestDto != null) {
			if (entityRequestDto.getEntityRequest() != null && entityRequestDto.getEntityRequest().size() > 0) {
				for (EntityRequestDataDto entityRequestDataDto : entityRequestDto.getEntityRequest()) {
					JSONObject request = new JSONObject();
					if (entityRequestDataDto.getIdentifier() != null)
						request.put("identifier", entityRequestDataDto.getIdentifier());
					if (entityRequestDataDto.getJurisdiction() != null)
						request.put("jurisdiction", entityRequestDataDto.getJurisdiction());
					if (entityRequestDataDto.getKeyword() != null)
						request.put("keyword", entityRequestDataDto.getKeyword());
					if (entityRequestDataDto.getLight_weight() != null)
						request.put("light-weight", entityRequestDataDto.getLight_weight());
					if (entityRequestDataDto.getSearchType() != null)
						request.put("searchType", entityRequestDataDto.getSearchType());
					String entityData = getNewSearchResults(request.toString());

					if (entityData != null) {
						JSONObject json = new JSONObject(entityData);
						if (json.has("data")) {
							JSONObject data = json.getJSONObject("data");
							if (data != null && data.has("results")) {
								JSONArray resultsArray = data.getJSONArray("results");
								if (resultsArray != null && resultsArray.length() > 0) {
									JSONObject finalResultObject = resultsArray.getJSONObject(0);
									if (finalResultObject != null && finalResultObject.has("merged-result")) {
										JSONObject mergedResultObject = finalResultObject
												.getJSONObject("merged-result");
										// for officership
										if (mergedResultObject != null && mergedResultObject.has("officership")) {
											JSONArray officershipSources = mergedResultObject
													.getJSONArray("officership");
											if (officershipSources != null && officershipSources.length() > 0) {
												for (int i = 0; i <= officershipSources.length() - 1; i++) {
													JSONObject officershipSource = officershipSources.getJSONObject(i);
													if (officershipSource!=null && officershipSource.has("value")) {
														JSONArray officershipArray = officershipSource
																.getJSONArray("value");
														if (officershipArray != null && officershipArray.length() > 0) {
															for (int j = 0; j <= officershipArray.length() - 1; j++) {
																JSONObject requestForIdentifier = new JSONObject();
																JSONObject officership = officershipArray
																		.getJSONObject(j);
																requestForIdentifier.put("size", 2);
																if (officership.has("name")
																		&& officership.getString("name") != null)
																	requestForIdentifier.put("vcard:hasName",
																			officership.getString("name"));
																if (officership.has("address")
																		&& officership.get("address") != null
																		&& (officership.get(
																				"address") instanceof JSONObject)) {
																	if (officership.has("address")
																			&& officership
																					.getJSONObject("address") != null
																			&& officership.getJSONObject("address")
																					.has("country")) {
																		requestForIdentifier.put("vcard:hasAddress",
																				new JSONObject().put("country",
																						officership
																								.getJSONObject(
																										"address")
																								.getString("country")));
																	}
																} else if (mergedResultObject.has("basic")) {
																	JSONArray basicArray = mergedResultObject
																			.getJSONArray("basic");
																	if (basicArray != null && basicArray.length() > 0) {
																		JSONObject basicObj = basicArray
																				.getJSONObject(0);
																		if (basicObj!=null && basicObj.has("value")) {
																			JSONObject valueObj = basicObj
																					.getJSONObject("value");
																			if (valueObj
																					.has("mdaas:RegisteredAddress")) {
																				JSONObject addressObj = valueObj
																						.getJSONObject(
																								"mdaas:RegisteredAddress");
																				if (addressObj!=null && addressObj.has("country")) {
																					String finalCountry = addressObj
																							.getString("country");
																					if (finalCountry != null) {
																						requestForIdentifier.put(
																								"vcard:hasAddress",
																								new JSONObject().put(
																										"country",
																										finalCountry));
																					} else {
																						requestForIdentifier.put(
																								"vcard:hasAddress",
																								new JSONObject().put(
																										"country", ""));
																					}

																				}
																			}
																		}
																	}

																}
																String identifierString = entitySearchService
																		.searchPerson(requestForIdentifier.toString());
																if (identifierString != null) {
																	JSONObject identifierJson = new JSONObject(
																			identifierString);
																	if (identifierJson.has("hits")) {
																		JSONArray identifierArray = identifierJson
																				.getJSONArray("hits");
																		if (identifierArray != null
																				&& identifierArray.length() - 1 > 0) {
																			List<ProfileResponseDto> list = new ArrayList<>();
																			for (int k = 0; k <= identifierArray
																					.length() - 1; k++) {
																				JSONObject identifierObject = identifierArray
																						.getJSONObject(k);
																				if (identifierObject != null
																						&& identifierObject
																								.has("@identifier")) {
																					String identifier = identifierObject
																							.getString("@identifier");
																					if (identifier != null) {
																						String response = tunaService
																								.getInfoOps(identifier,
																										"person", null);
																						JSONObject responseJson = new JSONObject(
																								response);

																						ProfileResponseDto profileResponseDto = new ProfileResponseDto();
																						if (responseJson!=null && responseJson
																								.has("watchlist")) {
																							profileResponseDto
																									.setWatchlist(
																											responseJson
																													.getJSONObject(
																															"watchlist")
																													.toString());

																						}
																						if (responseJson!=null && responseJson.has("news")) {
																							profileResponseDto.setNews(
																									responseJson
																											.getJSONArray(
																													"news")
																											.toString());
																						}
																						if (responseJson!=null && responseJson.has("basic")) {
																							profileResponseDto.setBasic(
																									responseJson
																											.getJSONObject(
																													"basic")
																											.toString());
																						}
																						list.add(profileResponseDto);
																					}
																				}
																			}
																			ProfileDataDto profileDataDto = new ProfileDataDto();
																			profileDataDto.setProfileResponseDto(list);
																			profileDataDto.setKeyword(
																					entityRequestDataDto.getKeyword());
																			result.add(profileDataDto);

																		}

																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (entityRequestDto.getOfficersRequest() != null && entityRequestDto.getOfficersRequest().size() > 0) {
			// for (OfficerShipRequestDto officerShipRequestDto :
			// entityRequestDto.getOfficersRequest()) {
			List<OfficerShipRequestDto> inputList = entityRequestDto.getOfficersRequest();
			boolean isCountry = true;
			int count = 0;
			if (inputList != null && inputList.size() > 0) {
				for (int i = 0; i <= inputList.size() - 1; i++) {

					// }
					// getData(inputList.get(i),result);

					JSONObject requestForIdentifier = new JSONObject();
					if (inputList.get(i).getVcardhasName() != null)
						requestForIdentifier.put("vcard:hasName", inputList.get(i).getVcardhasName());
					// if(isCountry){
					requestForIdentifier.put("size", 1);
					if (inputList.get(i).getVcardhasAddress() != null) {
						requestForIdentifier.put("vcard:hasAddress",
								new JSONObject().put("country", inputList.get(i).getVcardhasAddress()));
					}
					// }else{
					// requestForIdentifier.put("size", 2);

					// }
					String identifierString = entitySearchService.searchPerson(requestForIdentifier.toString());
					if (identifierString != null) {
						JSONObject identifierJson = new JSONObject(identifierString);
						if (identifierJson.has("hits")) {
							JSONArray identifierArray = identifierJson.getJSONArray("hits");
							if (identifierArray != null && identifierArray.length() > 0) {
								List<ProfileResponseDto> list = new ArrayList<>();
								String identifier = null;
								for (int k = 0; k <= identifierArray.length() - 1; k++) {
									ProfileResponseDto profileResponseDto = new ProfileResponseDto();
									JSONObject identifierObject = identifierArray.getJSONObject(k);
									if (identifierObject != null && identifierObject.has("@identifier")) {
										identifier = identifierObject.getString("@identifier");
										if (identifier != null) {
											String response = tunaService.getInfoOps(identifier, "person", null);
											JSONObject responseJson = new JSONObject(response);

											// JSONObject jsonResult = new
											// JSONObject();
											// JSONObject jsonResult = new
											// JSONObject();

											if (responseJson!=null && responseJson.has("watchlist")) {
												profileResponseDto.setWatchlist(
														responseJson.getJSONObject("watchlist").toString());

											}
											if (responseJson!=null && responseJson.has("news")) {
												profileResponseDto
														.setNews(responseJson.getJSONArray("news").toString());
											}
											if (responseJson!=null && responseJson.has("basic")) {
												profileResponseDto
														.setBasic(responseJson.getJSONObject("basic").toString());
											}
											list.add(profileResponseDto);
										}
									}

									/*
									 * ProfileDataDto profileDataDto=new ProfileDataDto();
									 * profileDataDto.setProfileResponseDto(list); //if(i!=-1)
									 * profileDataDto.setKeyword(inputList.get(i). getKeyword());
									 * result.add(profileDataDto); ProfileDataDto profileDataDto=new
									 * ProfileDataDto(); profileDataDto.setProfileResponseDto(list); //if(i!=-1)
									 * profileDataDto.setKeyword(inputList.get(i). getKeyword());
									 * result.add(profileDataDto);
									 */

									// isCountry=true;
									/*
									 * if(count==0 && (new JSONArray(list.get(0).getNews())==null || !(new
									 * JSONArray(list.get(0).getNews()).length()>0)) ){ isCountry=false; count++;
									 * i--; break LOOP; if(count==0 && (new JSONArray(list.get(0).getNews())==null
									 * || !(new JSONArray(list.get(0).getNews()).length()>0)) ){ isCountry=false;
									 * count++; i--; break LOOP;
									 * 
									 * }else{ count=0; isCountry=true; }
									 */

								}

								if ((new JSONArray(list.get(0).getNews()).length() == 0)) {
									getData(inputList.get(i), result, list, identifier);
								} else {
									ProfileDataDto profileDataDto = new ProfileDataDto();
									profileDataDto.setProfileResponseDto(list);
									profileDataDto.setKeyword(inputList.get(i).getKeyword());
									profileDataDto.setRole(inputList.get(i).getRole());
									profileDataDto.setDateOfBirth(inputList.get(i).getDateOfBirth());
									result.add(profileDataDto);
								}

								/*
								 * JSONArray ee=new JSONArray(list.get(0).getNews()); if ((list.size()==1) &&
								 * (ee.length()==0)) { isCountry=false; i--; }else{ ProfileDataDto
								 * profileDataDto=new ProfileDataDto();
								 * profileDataDto.setProfileResponseDto(list); if(i!=-1)
								 * profileDataDto.setKeyword(inputList.get(i). getKeyword());
								 * result.add(profileDataDto); isCountry=true; JSONArray ee=new
								 * JSONArray(list.get(0).getNews()); if ((list.size()==1) && (ee.length()==0)) {
								 * isCountry=false; i--; }else{ ProfileDataDto profileDataDto=new
								 * ProfileDataDto(); profileDataDto.setProfileResponseDto(list); if(i!=-1)
								 * profileDataDto.setKeyword(inputList.get(i). getKeyword());
								 * result.add(profileDataDto); isCountry=true;
								 * 
								 * }
								 */

								// result.put(officerShipRequestDto.getVcardhasName(),
								// reponseArray);
								// result.put(officerShipRequestDto.getVcardhasName(),
								// reponseArray);

							}

						}
					}

				}
			}
		}

		return result;
	}

	@Override
	public List<ShareHolderRequestDto> getShareHoldersData(List<ShareHolderRequestDto> jsonString) throws Exception {
		for (ShareHolderRequestDto shareHolderRequestDto : jsonString) {
			String url = shareHolderRequestDto.get_links().getShareHolders().getUrl();
			if (url != null) {
				HierarchyDto hierarchyDto = new HierarchyDto();
				hierarchyDto.setUrl(url);
				// String response = getHierarchyData(hierarchyDto);
				String response = getHierarchyData(hierarchyDto, null,null);
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("results")) {
					JSONArray resultsArray = responseJson.getJSONArray("results");
					if (resultsArray != null && resultsArray.length() > 0) {
						JSONObject resultObj = resultsArray.getJSONObject(0);
						if (resultObj.has("data")) {
							JSONArray dataArray = resultObj.getJSONArray("data");
							if (dataArray != null && dataArray.length() > 0) {
								JSONObject dataObj = dataArray.getJSONObject(0);
								if (dataObj.has("shareholders")) {
									JSONArray shareholdersArray = dataObj.getJSONArray("shareholders");

									shareHolderRequestDto.setShareHolders(shareholdersArray.toString());
									/*
									 * if(shareholdersArray!=null &&
									 * shareholdersArray.length()>10){
									 * HashMap<String, JSONObject> map=new
									 * HashMap<>(); for (int i = 0; i <
									 * if(shareholdersArray!=null &&
									 * shareholdersArray.length()>10){
									 * HashMap<String, JSONObject> map=new
									 * HashMap<>(); for (int i = 0; i <
									 * shareholdersArray.length()-1; i++) {
									 * 
									 * }
									 * 
									 * }
									 */

								}

							}
						}
					}
				}
			}

		}
		return jsonString;
	}

	public void getData(OfficerShipRequestDto officerRequestDto, List<ProfileDataDto> result,
			List<ProfileResponseDto> list, String preIdentifier) throws Exception {

		JSONObject requestForIdentifier = new JSONObject();
		if (officerRequestDto.getVcardhasName() != null)
			requestForIdentifier.put("vcard:hasName", officerRequestDto.getVcardhasName());
		requestForIdentifier.put("size", 2);

		String identifierString = entitySearchService.searchPerson(requestForIdentifier.toString());
		if (identifierString != null) {
			JSONObject identifierJson = new JSONObject(identifierString);
			if (identifierJson.has("hits")) {
				JSONArray identifierArray = identifierJson.getJSONArray("hits");
				if (identifierArray != null && identifierArray.length() > 0) {
					// List<ProfileResponseDto> list = new ArrayList<>();
					for (int k = 0; k <= identifierArray.length() - 1; k++) {
						ProfileResponseDto profileResponseDto = new ProfileResponseDto();
						JSONObject identifierObject = identifierArray.getJSONObject(k);
						if (identifierObject != null && identifierObject.has("@identifier")) {
							String identifier = identifierObject.getString("@identifier");
							if (identifier != null && !identifier.equals(preIdentifier) && list.size() < 2) {
								String response = tunaService.getInfoOps(identifier, "person", null);
								JSONObject responseJson = new JSONObject(response);

								// JSONObject jsonResult = new JSONObject();

								if (responseJson.has("watchlist")) {
									profileResponseDto.setWatchlist(responseJson.getJSONObject("watchlist").toString());

								}
								if (responseJson.has("news")) {
									profileResponseDto.setNews(responseJson.getJSONArray("news").toString());
								}
								if (responseJson.has("basic")) {
									profileResponseDto.setBasic(responseJson.getJSONObject("basic").toString());
								}
								list.add(profileResponseDto);
							}
						}

					}

					ProfileDataDto profileDataDto = new ProfileDataDto();
					profileDataDto.setProfileResponseDto(list);
					profileDataDto.setKeyword(officerRequestDto.getKeyword());
					profileDataDto.setRole(officerRequestDto.getRole());
					profileDataDto.setDateOfBirth(officerRequestDto.getDateOfBirth());
					result.add(profileDataDto);

				}

			}

		}
	}

	@Override
	public List<IdentifiersRequestDto> getOrgData(CopyOnWriteArrayList<IdentifiersRequestDto> identifiers) {
		for (IdentifiersRequestDto identifiersRequestDto : identifiers) {
			try {
				String response = tunaService.getInfoOps(identifiersRequestDto.getIdentifier(), "org", null);
				if (response != null)
					identifiersRequestDto.setResponse(response);
			} catch (Exception e) {
				identifiers.remove(identifiersRequestDto);
			}

		}
		return identifiers;
	}

	@Override
	public String getShareHoloders(String organizationId, Integer level, String fields) throws Exception {
		String url = WORKFLOW_EXTENSION_URL + "/be-entity/v1/organizations/" + organizationId + "/graph?fields="
				+ fields;
		String finalResponse = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("_links")) {
					JSONObject linksJson = responseJson.getJSONObject("_links");
					if (linksJson!=null && linksJson.has("next")) {
						Map<String, String> query_pairs = new LinkedHashMap<String, String>();
						String nextLink=null;
						String[] pairs =null;
						if (linksJson.has("next"))
							nextLink = linksJson.getString("next");
						if(nextLink!=null)
							pairs = nextLink.split("&");
						if (pairs != null && pairs.length > 0) {
							for (String pair : pairs) {
								int idx = pair.indexOf("=");
								query_pairs.put((pair.substring(0, idx)), (pair.substring(idx + 1)));
							}
						}
						String levelVal = query_pairs.get("level");
						if (Integer.parseInt(levelVal) > level && linksJson.has("shareholders")) {
							JSONArray shareholdersArray = linksJson.getJSONArray("shareholders");
							// for (int i = 0; i < shareholdersArray.length();
							// i++) {
							if (shareholdersArray != null && shareholdersArray.length() > 0) {
								JSONObject shareholderObj = shareholdersArray.getJSONObject(level);
								if (shareholderObj!=null && shareholderObj.has("level")) {
									if (shareholderObj.getString("level").equals(level.toString())) {
										if (shareholderObj.has("href")) {
											byte[] jsonByteArray = ServiceCallHelper
													.downloadFileFromServerWithoutTimeout(
															shareholderObj.getString("href"));
											if (jsonByteArray.length > 0)
												finalResponse = new String(jsonByteArray, Charset.forName("UTF16"));
											return finalResponse;
										}

									}

								}
							}
							// }
						} else {
							return finalResponse;
						}
					} else {
						if (linksJson.has("shareholders")) {

							JSONArray shareholdersArray = linksJson.getJSONArray("shareholders");
							if (shareholdersArray.length() > level) {
								// for (int i = 0; i <
								// shareholdersArray.length(); i++) {
								JSONObject shareholderObj = shareholdersArray.getJSONObject(level);
								if (shareholderObj.has("level")) {
									if (shareholderObj.getString("level").equals(level.toString())) {
										if (shareholderObj.has("href")) {
											byte[] jsonByteArray = ServiceCallHelper
													.downloadFileFromServerWithoutTimeout(
															shareholderObj.getString("href"));
											if (jsonByteArray.length > 0)
												finalResponse = new String(jsonByteArray, Charset.forName("UTF16"));
											return finalResponse;
										}

									}

								}
								// }
							} else {
								throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);

							}

						} else {
							throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);

						}

					}
				}
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 202) {
			return finalResponse;
		}
		return finalResponse;
	}

	@Override
	@Async("taskExecutor")
	public Boolean ownershipStructure(String inputIdentifier, HierarchyDto jsonString, Integer maxSubsidiarielevel,
			Integer lowRange, Integer highRange, String path, Integer noOfSubsidiaries, Integer maxShareholderLevel,
			String organisationName, String juridiction,Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate, boolean isRollBack,List<SourcesDto> sourcesDtos, String source,String sourceType)
					throws JSONException, Exception {
		boolean isScreeningRequired=false;
		boolean isBstSource=false;
		String documentUrl=null;
		boolean isOtherSource=false;
		String annualReportLink=null;
		JSONObject sourceInfo=new JSONObject();
		JSONArray sources=new JSONArray();
		String sourceValue=null;
		/*if(sourceInfo!=null && sourceInfo.names()!=null){
		for (int i = 0; i < sourceInfo.names().length(); i++) {
			if(sourceInfo.names().getString(i).equalsIgnoreCase(source))
				sources.put(0, sourceInfo.names().getString(i));
		}
		}*/
		try{
			sourceValue=getSource(jsonString, inputIdentifier, sourcesDtos, sourceInfo,sourceType);
			//sources=sourceInfo.names();
			if (sourceValue == null) {
				sourceValue = CLIENT_ID;
			}

			if (sourceInfo.names() != null) {
				sources = sourceInfo.names();
			} else {
				sources.put(CLIENT_ID);
			}
			if ((source != null && "companyhouse.co.uk".equalsIgnoreCase(source))) {
				annualReportLink = buildMultiSourceUrl(inputIdentifier, "documents", null, null, null, CLIENT_ID,sourceType);
				documentUrl = getAnnualReturnsLink(annualReportLink);
			}else{
				isBstSource=true;
			}
			if (!isRollBack) {
				if (source == null) {
					source = sourceValue;//getSource(jsonString, inputIdentifier, sourcesDtos, sourceInfo);
					sources=sourceInfo.names();
					if (source != null && "BST".equalsIgnoreCase(source)
							&& (jsonString.getUrl() != "" && jsonString.getUrl() != null)) {
						isBstSource = true;
					}
					if ((source != null && "companyhouse.co.uk".equalsIgnoreCase(source)) || !isBstSource) {
						annualReportLink = buildMultiSourceUrl(inputIdentifier, "documents", null, null, null, CLIENT_ID,sourceType);
						documentUrl = getAnnualReturnsLink(annualReportLink);
						if (documentUrl != null) {
							isBstSource = false;
						} else if (jsonString.getUrl() != "" && jsonString.getUrl() != null) {
							isBstSource = true;
							JSONArray names = sourceInfo.names();
							if (names != null && names.length() > 0) {
								int credibility = 0;
								for (int j = 0; j < names.length(); j++) {
									if (j == 0 && !"companyhouse.co.uk".equalsIgnoreCase(names.getString(j))) {
										credibility = sourceInfo.getInt(names.getString(j));
										source = names.getString(j);
									} else if (credibility < sourceInfo.getInt(names.getString(j))
											&& !"companyhouse.co.uk".equalsIgnoreCase(names.getString(j))) {
										credibility = sourceInfo.getInt(names.getString(j));
										source = names.getString(j);
									}
								}
							}
						}
					}
				}
			}

			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			file.createNewFile();
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File jsonfile = new File(classLoader.getResource("entityType.json").getFile());
			Object jsonfiledata = parser.parse(new FileReader(jsonfile));
			org.json.simple.JSONArray personTypes = (org.json.simple.JSONArray) jsonfiledata;
			File pepKeyFile = new File(classLoader.getResource("pepKey.json").getFile());
			Object pepKeyJson = parser.parse(new FileReader(pepKeyFile));
			org.json.simple.JSONObject pepTypes = (org.json.simple.JSONObject) pepKeyJson;

			File subsourcesFile = new File(classLoader.getResource("subsources.json").getFile());
			Object subSourcesData = parser.parse(new FileReader(subsourcesFile));
			org.json.simple.JSONObject subSources = (org.json.simple.JSONObject) subSourcesData;
			JSONArray array = new JSONArray();
			JSONObject isStatusErrorTrue = new JSONObject();
			int tempCount = 0;
			JSONObject links = null;
			List<List<JSONObject>> list = new ArrayList<>();
			List<List<JSONObject>> allShareholders = new ArrayList<>();
			JSONObject persons = new JSONObject();
			JSONObject holders = new JSONObject();
			 String clientIdentifier=new String();
			JSONObject map = new JSONObject();
			//JSONObject allShareHoldersInfo = new JSONObject();
			List<JSONObject> holdingPreviousLeveltData=new ArrayList<JSONObject>();
			if ((jsonString.getUrl() != "" && jsonString.getUrl() != null) && isBstSource
					&& (source != null && !source.equalsIgnoreCase("companyhouse.co.uk"))) {
				boolean flag = true;
				String url = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, null,CLIENT_ID,sourceType);
				while (flag) {
					String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
					if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
						String response = serverResponse[1];
						if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
							JSONObject jsonResponse = new JSONObject(response);
							if (jsonResponse.has("_links")) {
								links = jsonResponse.getJSONObject("_links");
								if (links.has("graph:shareholders")) {
									flag = false;
									JSONArray graphShareholders = links.getJSONArray("graph:shareholders");
									if (graphShareholders!=null && graphShareholders.length() >= maxShareholderLevel) {
										try {
											// get data
											for (int i = 0; i < maxShareholderLevel; i++) {
												list = getLevelwiseData(list, i, url, array,
														maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
														inputIdentifier, maxSubsidiarielevel, array.length(),
														organisationName, juridiction, annualReportLink, personTypes,
														pepTypes, executor, persons, allShareholders, map, userId,
														isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier,sourceType);
												
												
												/*if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}*/
												
												// check if the source status has error true
												if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
													break;
												}else if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}

											}
										} catch (Exception e) {
											e.printStackTrace();
											if (array.length() > 0) {
												array.getJSONObject(array.length()-1).put("status", "completed");
											}
										}

									} else if (links.has("next")) {
										try {
											int counter = 0;
											for (int i = 0; i < graphShareholders.length(); i++) {
												counter = i;
												// get data
												list = getLevelwiseData(list, counter, url, array,
														maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
														inputIdentifier, maxSubsidiarielevel, array.length(),
														organisationName, juridiction, annualReportLink, personTypes,
														pepTypes, executor, persons, allShareholders, map, userId,
														isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier,sourceType);
												/*if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}*/
												
												if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
													break;
												}else if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}
											}
											while (counter < maxShareholderLevel) {
												boolean nextLevel = getNextLevel(jsonString.getUrl(), counter);
												if (nextLevel) {
													// get data
													list = getLevelwiseData(list, counter, url, array,
															maxShareholderLevel, lowRange, highRange, noOfSubsidiaries,
															file, inputIdentifier, maxSubsidiarielevel, array.length(),
															organisationName, juridiction, annualReportLink, personTypes,
															pepTypes, executor, persons, allShareholders, map, userId,
															isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier,sourceType);
													/*if (array.length() > 0 && "completed".equalsIgnoreCase(
															array.getJSONObject(array.length() - 1).getString("status"))) {
														break;
													}*/
													
													if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
														break;
													}else if (array.length() > 0 && "completed".equalsIgnoreCase(
															array.getJSONObject(array.length() - 1).getString("status"))) {
														break;
													}
													
													counter++;
												} else {
													counter++;
													break;
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
											if (array.length() > 0) {
												array.getJSONObject(array.length()-1).put("status", "completed");
											}
										}
									} else {
										try {
											for (int i = 0; i < graphShareholders.length(); i++) {
												// get data
												list = getLevelwiseData(list, i, url, array,
														maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
														inputIdentifier, maxSubsidiarielevel, array.length(),
														organisationName, juridiction, annualReportLink, personTypes,
														pepTypes, executor, persons, allShareholders, map, userId,
														isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier,sourceType);
												/*if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}*/
												
												if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
													break;
												}else if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}
												
												if (i == list.size())
													break;
											}
										} catch (Exception e) {
											e.printStackTrace();
											if (array.length() > 0) {
												array.getJSONObject(array.length()-1).put("status", "completed");
											}
										}
									}
								}
							}
						}
					}
					Thread.sleep(10000);
				}
			} else {
				try {
					if(source.equalsIgnoreCase("companyhouse.co.uk")) {
						String url = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, null,CLIENT_ID,sourceType);
						isStatusErrorTrue.put("source", "true");
						for (int i = 0; i < maxShareholderLevel; i++) {
							list = getLevelwiseData(list, i, url, array,
									maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
									inputIdentifier, maxSubsidiarielevel, array.length(),
									organisationName, juridiction, annualReportLink, personTypes,
									pepTypes, executor, persons, allShareholders, map, userId,
									isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier,sourceType);
							
							
							/*if (array.length() > 0 && "completed".equalsIgnoreCase(
									array.getJSONObject(array.length() - 1).getString("status"))) {
								break;
							}*/
							
							// check if the source status has error true
							if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
								break;
							}else if (array.length() > 0 && "completed".equalsIgnoreCase(
									array.getJSONObject(array.length() - 1).getString("status"))) {
								break;
							}

						}
						
					}else {
					getLevelwiseData(list, null, jsonString.getUrl(), array, maxShareholderLevel, lowRange, highRange, noOfSubsidiaries,
							file, inputIdentifier, maxSubsidiarielevel, tempCount, organisationName, juridiction,
							annualReportLink, personTypes, pepTypes, executor, persons, allShareholders, map, userId,
							isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier,sourceType);
					}
				
				} catch (Exception e) {
					e.printStackTrace();
					if (array.length() > 0) {
						array.getJSONObject(array.length()-1).put("status", "completed");
					}
				}
			}
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	private String getSource(HierarchyDto jsonString,String inputIdentifier,List<SourcesDto> sourcesDtos, JSONObject sourceInfo,String sourceType) throws UnsupportedEncodingException, InterruptedException {
		//JSONObject shareHolderLinks =new JSONObject();
		//JSONObject object = new JSONObject();
		int count=0;
		JSONArray sources=new JSONArray();
		if ((jsonString.getUrl() != "" && jsonString.getUrl() != null)) {
			boolean flag = true;
			String url = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, null,CLIENT_ID,sourceType);
			while (flag) {
				String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
				if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
					String response = serverResponse[1];
					if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
						JSONObject jsonResponse = new JSONObject(response);
						if (jsonResponse != null && jsonResponse.has("statuses")) {
							flag = false;
							JSONObject statuses = jsonResponse.getJSONObject("statuses");
							if (statuses != null) {
								sources = statuses.names();
							}
						} else {
							if (count == 5) {
								flag = false;
								sources.put(CLIENT_ID);
							}
						}
						/*if (jsonResponse.has("_links")) {
							shareHolderLinks = jsonResponse.getJSONObject("_links");
							if (shareHolderLinks.has("graph:shareholders")) {
								flag = false;
								JSONArray graphShareholders = shareHolderLinks.getJSONArray("graph:shareholders");
								String href = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, 0,CLIENT_ID);
								byte[] bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,BE_ENTITY_API_KEY);
								String json = null;
								if (bytes != null)
									json = new String(bytes);
								if (json != null && json.startsWith("{"))
									object = new JSONObject(json);
							}
						}*/
						count++;
					}
				}
			}
			Thread.sleep(5000);
		}
		if(sources!=null && sources.length()==1 && sources.get(0).toString().equalsIgnoreCase(CLIENT_ID)) {
			return sources.getString(0);
		}
		//JSONArray sources=object.names();
		if (sources != null) {
			sources.put("companyhouse.co.uk");
			if (sources != null && sources.length() > 0) {
				JSONObject otherSourceInfo = new JSONObject();
				for (int i = 0; i < sources.length(); i++) {
					for (SourcesDto sourcesDto : sourcesDtos) {
						if (sources.getString(i).equalsIgnoreCase(sourcesDto.getSourceName())) {
							List<ClassificationsDto> classifications = sourcesDto.getClassifications();
							for (ClassificationsDto classificationsDto : classifications) {
								List<SubClassificationsDto> subClassifications = classificationsDto
										.getSubClassifications();
								for (SubClassificationsDto subClassificationsDto : subClassifications) {
									if (subClassificationsDto.getSubClassifcationName()
											.equalsIgnoreCase("Corporate Structure")) {
										sourceInfo.put(sources.getString(i),
												subClassificationsDto.getSubClassificationCredibility().ordinal());
										if (!sourcesDto.getSourceDisplayName().equalsIgnoreCase("bst") && !sourcesDto
												.getSourceDisplayName().equalsIgnoreCase("company house")) {
											otherSourceInfo.put(sources.getString(i),
													subClassificationsDto.getSubClassificationCredibility().ordinal());
										}
									}
								}
							}
						}
					}
				}
			}
		}

		String source=null;
		int credibility=0;
		JSONArray names= new JSONArray();
		if (sourceInfo.names() != null) {
			names = sourceInfo.names();
		} else {
			names.put(CLIENT_ID);
		}
		if(names!=null && names.length()>0){
			for (int j = 0; j < names.length(); j++) {
				if (j == 0) {
					if (!names.getString(j).equals(CLIENT_ID)) {
						credibility = sourceInfo.getInt(names.getString(j));
						source = names.getString(j);
					}
				} else if (credibility < sourceInfo.getInt(names.getString(j))) {
					if (!names.getString(j).equals(CLIENT_ID)) {
						credibility = sourceInfo.getInt(names.getString(j));
						source = names.getString(j);
					}
				}
			}
		}
		return source;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String readCorporateStructure(String path) throws JSONException, Exception {
		JSONObject response = new JSONObject();
		try {
			JSONParser parser = new JSONParser();
			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			if (file.exists() && file.length() > 0) {
				FileReader reader = new FileReader(CORPORATE_STRUCTURE_LOCATION + path + ".json");
				Object obj = parser.parse(reader);
				org.json.simple.JSONArray mainResponse = (org.json.simple.JSONArray) obj;
				if(!mainResponse.isEmpty()){
					org.json.simple.JSONObject firstJson = (org.json.simple.JSONObject) mainResponse.get(0);
					//org.json.JSONObject firstJson = (org.json.JSONObject) mainResponse.get(0);
					//if(firstJson.containsKey("status") && "completed".equalsIgnoreCase(firstJson.get("status").toString())){
						if(firstJson.containsKey("status") && "completed".equalsIgnoreCase(firstJson.get("status").toString())){
							// Prateek
//						Set<String> keys = firstJson.keySet();
//						if(keys.size() == 2){
//							response.put("status", "completed");
//							response.put("data", mainResponse);
//							return response.toString();
//							
//						}
							/// new
							if(firstJson.containsKey("error")) {
								response.put("status", "completed");
								response.put("data", mainResponse);
								return response.toString();
							}
							///
						
					}
					//return response.toString();
				}
				org.json.simple.JSONObject mainEntity = (org.json.simple.JSONObject) mainResponse.get(0);
				String mainEntityId = null;
				String mainEntityName = null;
				if (mainEntity.containsKey("identifier") && mainEntity.get("identifier") != null) {
					mainEntityId = (String) mainEntity.get("identifier").toString();
				}
				if (mainEntity.containsKey("name") && mainEntity.get("name") != null) {
					mainEntityName = (String) mainEntity.get("name").toString();
				}

				// fetching list of watch lists
				List<SignificantWatchList> significantWatchLists = significantWatchListService
						.fetchWtachListByEntityId(mainEntityId);
				for (int i = 0; i < mainResponse.size(); i++) {
					org.json.simple.JSONObject json = (org.json.simple.JSONObject) mainResponse.get(i);
					String identifier = null;
					String name = null;
					if (json.containsKey("identifier")) {
						if (json.get("identifier") != null)
							identifier = json.get("identifier").toString();
					}
					if (json.containsKey("name")) {
						if (json.get("name") != null)
							name = json.get("name").toString();
					}
					getSignificantPep(json, mainEntityId, mainEntityName, identifier, name, false,
							significantWatchLists);
					if (i == 0) {
						if (json.containsKey("officership")) {
							org.json.simple.JSONArray officership = (org.json.simple.JSONArray) json.get("officership");
							if (officership != null) {
								for (int j = 0; j < officership.size(); j++) {
									org.json.simple.JSONObject officer = (org.json.simple.JSONObject) officership
											.get(j);
									String officerName = null;
									if (officer.containsKey("name")) {
										if (officer.get("name") != null)
											officerName = officer.get("name").toString();
									}
									getSignificantPep(officer, mainEntityId, mainEntityName, officerName, officerName,
											true, significantWatchLists);
								}
							}
						}
					}
					if (json.containsKey("subsidiaries")) {
						org.json.simple.JSONArray subsidiaries = (org.json.simple.JSONArray) json.get("subsidiaries");
						if (subsidiaries != null) {
							for (int j = 0; j < subsidiaries.size(); j++) {
								org.json.simple.JSONObject subsidiarie = (org.json.simple.JSONObject) subsidiaries
										.get(j);
								String subsidiarieId = null;
								String subsidiarieName = null;
								if (subsidiarie.containsKey("identifier")) {
									if (subsidiarie.get("identifier") != null)
										subsidiarieId = subsidiarie.get("identifier").toString();
								}
								if (subsidiarie.containsKey("name")) {
									if (subsidiarie.get("name") != null)
										subsidiarieName = subsidiarie.get("name").toString();
								}
								getSignificantPep(subsidiarie, mainEntityId, mainEntityName, subsidiarieId,
										subsidiarieName, false, significantWatchLists);
							}
						}
					}
				}
				if (mainResponse.size() > 0) {
					org.json.simple.JSONObject lastObject = (org.json.simple.JSONObject) mainResponse
							.get(mainResponse.size() - 1);
					String status = (String) lastObject.get("status");
					if (status == null) {
						response.put("status", "inprogress");
					} else {
						response.put("status", status);
					}
					response.put("data", mainResponse);
					return response.toString();
				}
			} else {
				response.put("status", "inprogress");
				response.put("data", new JSONArray());
				return response.toString();

			}
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "inprogress");
			response.put("data", new JSONArray());
			return response.toString();

		}
		return response.toString();

	}

	public boolean getNextLevel(String url, int currentLevel) throws InterruptedException {
		boolean isReady = false;
		boolean flag = true;
		String levelVal1 = null;
		JSONObject links = null;
		while (flag) {
			String serverResponse1[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
			if (serverResponse1[0] != null && Integer.parseInt(serverResponse1[0]) == 200) {
				String response1 = serverResponse1[1];
				if (!StringUtils.isEmpty(response1) && serverResponse1[1].trim().length() > 0) {
					JSONObject jsonResponse1 = new JSONObject(response1);
					if(jsonResponse1.has("_links"))
						links = jsonResponse1.getJSONObject("_links");
					if (links.has("next")) {
						Map<String, String> query_pairs = new LinkedHashMap<String, String>();
						String nextLink = links.getString("next");
						String[] pairs = nextLink.split("&");
						for (String pair : pairs) {
							int idx = pair.indexOf("=");
							query_pairs.put((pair.substring(0, idx)), (pair.substring(idx + 1)));
						}
						levelVal1 = query_pairs.get("level");
						if (Integer.parseInt(levelVal1) > (currentLevel + 1)) {
							isReady = true;
							flag = false;
						}
					} else {
						if(links.has("graph:shareholders")){
							JSONArray shareholders = links.getJSONArray("graph:shareholders");
							if (shareholders.length() < (currentLevel + 1)) {
								isReady = false;
								flag = false;
							}
						}
					}
					Thread.sleep(10000);
				}
			}
		}
		return isReady;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<List<JSONObject>> getLevelwiseData(List<List<JSONObject>> list, Integer level, String url,
			JSONArray array, Integer maxShareholderLevel, Integer lowRange, Integer highRange, Integer noOfSubsidiaries,
			File file, String identifier, Integer maxSubsidiarielevel, Integer tempCount, String requestOrganisation,
			String juridiction, String annualReportLink, org.json.simple.JSONArray personTypes,
			org.json.simple.JSONObject pepTypes, ThreadPoolExecutor executor, JSONObject persons,
			List<List<JSONObject>> allShareholders, JSONObject map, Long userId, Boolean isSubsidiariesRequired,
			String startDate, String endDate, String documentUrl, boolean isOtherSource, JSONObject sourceInfo,
			String path, boolean isScreeningRequired, JSONObject holders, String source, JSONArray sources, org.json.simple.JSONObject subSources,  List<JSONObject> holdingPreviousLeveltData, JSONObject isStatusErrorTrue,String clientIdentifier,String sourceType) throws Exception {
		boolean isBstDataThere = false;
		String errorMsg=null;
		JSONObject holdingDataForNextLevel = new JSONObject();
		if (level != null) {
			JSONObject links = new JSONObject();
			JSONArray shareholders = new JSONArray();
			String serverResponse1[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url,
					BE_ENTITY_API_KEY);
			if (serverResponse1[0] != null && Integer.parseInt(serverResponse1[0]) == 200) {
				String response1 = serverResponse1[1];
				if (!StringUtils.isEmpty(response1) && serverResponse1[1].trim().length() > 0) {
					JSONObject jsonResponse1 = new JSONObject(response1);
					if (jsonResponse1.has("_links"))
						links = jsonResponse1.getJSONObject("_links");
					if (links.has("graph:shareholders"))
						shareholders = links.getJSONArray("graph:shareholders");
				}
			}
			List<JSONObject> refListWithInRange = new ArrayList<>();
			List<JSONObject> refListWithOutRange = new ArrayList<>();
			String href = buildMultiSourceUrl(identifier, "graph:shareholders", "graph", null, level, CLIENT_ID,sourceType);
			byte[] bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, BE_ENTITY_API_KEY);
			String json = null;
			/*if (bytes != null)
				json = new String(bytes);
			if (json == null || !json.startsWith("{")) {
				int coutRepeat = 0;
				while (json == null || !json.startsWith("{")) {
					bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, BE_ENTITY_API_KEY);
					if (bytes != null)
						json = new String(bytes);
					coutRepeat = coutRepeat + 1;
					if (coutRepeat == 5)
						break;
					Thread.sleep(5000);
				}
			}*/
			
			//old code with the whole status check
			if (bytes != null) {
				json = new String(bytes);
				JSONObject data = new JSONObject(json);
				int check =0;
				while(data!=null && data.has("is-completed") && ! data.getBoolean("is-completed")) {
						bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,
								BE_ENTITY_API_KEY);
						if (bytes != null) {
							json = new String(bytes);
							data = new JSONObject(new String(bytes));
							check++;
						}
						Thread.sleep(3000);
						if(check==10)
							break;
					}
					
			}
			 
			// new code with the individual source status check
			if (bytes != null) {
				json = new String(bytes);
				JSONObject data = new JSONObject(json);
				// while(data!=null && data.has("is-completed") && !
				// data.getBoolean("is-completed")) {

				if (data.has("statuses")) {
					JSONObject statuses = data.getJSONObject("statuses");
					if (statuses != null) {
						sources = statuses.names();
						sources.put("companyhouse.co.uk");
					}
					String customKey=identifier+"_"+CLIENT_ID;
					//String clientIdentifier=null;
					if(data.has(CLIENT_ID)) {
						JSONObject clientID= data.getJSONObject(CLIENT_ID);
						JSONArray idsClient = clientID.names();
						//customkey
					
						if (level == 0 && !isStatusErrorTrue.has("clientIdentifier")) {
							for (int i = 0; i < idsClient.length(); i++) {
								if(idsClient.getString(i).equals(customKey))
									isStatusErrorTrue.put("clientIdentifier", customKey);
									//clientIdentifier=customKey;
							}
						}
					}
					for (int i = 0; i < sources.length(); i++) {
						if (statuses.has(source) && source.equalsIgnoreCase(sources.get(i).toString())) {
							JSONObject statucSource = statuses.getJSONObject(sources.get(i).toString());
							if (statucSource.has("level") && statucSource.has("graph")) {
								JSONObject levelObj = statucSource.getJSONObject("level");
								JSONObject graphObj = statucSource.getJSONObject("graph");
								if (levelObj.has("is-completed") && graphObj.has("is-completed")) {
									
									if (levelObj.getBoolean("is-error") || graphObj.getBoolean("is-error")) {
										if(isStatusErrorTrue.has("clientIdentifier") &&isStatusErrorTrue.getString("clientIdentifier").equals(customKey))
											isStatusErrorTrue.put("flag", "false");
										else
											isStatusErrorTrue.put("flag", "true");
										
										errorMsg=ElementConstants.OWNERSHIP_ERROR;
//										array.put(new JSONObject().put("country", "Germany"));
//										//array.getJSONObject(0).put("country","Germany");
//										array.getJSONObject(0).put("entity_id","orgChartmainEntity");
//										array.getJSONObject(0).put("entity_type","organization");
//										array.getJSONObject(0).put("id","p00");
//										array.getJSONObject(0).put("identifier",identifier);
//										array.getJSONObject(0).put("indirectPercentage","100");
//										array.getJSONObject(0).put("jurisdiction",juridiction);
//										array.getJSONObject(0).put("parents",new JSONArray());
//										array.getJSONObject(0).put("source_evidence",source);
//										array.getJSONObject(0).put("name",requestOrganisation);
//										array.getJSONObject(0).put("title",requestOrganisation);
//										array.getJSONObject(0).put("totalPercentage",100);
//										array.getJSONObject(0).put("screeningFlag",true);
//										if(graphObj.getString("error-msg").length()>0)
//											array.getJSONObject(0).put("error",graphObj.getString("error-msg"));
//										else
//											array.getJSONObject(0).put("error","Source not present");
//										array.getJSONObject(0).put("sources", sources);
//
//										
//										array.getJSONObject(array.length() - 1).put("status", "completed");
//
//									
//										
//										writeFile(file,array);
										//return list;
									}else{
										while (levelObj.getBoolean("is-completed") != true
												&& graphObj.getBoolean("is-completed") != true) {


											bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,
													BE_ENTITY_API_KEY);
											if (bytes != null) {
												json = new String(bytes);
												data = new JSONObject(new String(bytes));
											}
											Thread.sleep(5000);
										}
									}
								}

							}
						}
					
					}
				}

				/*
				 * bytes = ServiceCallHelper.
				 * downloadFileFromServerWithoutTimeoutWithApiKey(href,
				 * BE_ENTITY_API_KEY); if (bytes != null) { json = new
				 * String(bytes); data = new JSONObject(new String(bytes)); }
				 * Thread.sleep(5000);
				 */
				// }
			}
			
			JSONObject object = new JSONObject();
			if (json != null && json.startsWith("{"))
				object = new JSONObject(json);
			if (list.size() > 0) {
				for (JSONObject prevJson : list.get(list.size() - 1)) {
					if (prevJson.has("isCustom")) {
						if (!prevJson.getBoolean("isCustom")) {
							isBstDataThere = true;
							break;
						}
					}
				}
			}
			JSONObject bstData = new JSONObject();
			JSONObject clientData = new JSONObject();
			/*if (source != null && !"BST".equalsIgnoreCase(source)) {
				int count=0;
				while (!object.has(source)) {
					bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, BE_ENTITY_API_KEY);
					if (bytes != null)
						json = new String(bytes);
					if (json == null || !json.startsWith("{")) {
						int coutRepeat = 0;
						while (json == null || !json.startsWith("{")) {
							bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,
									BE_ENTITY_API_KEY);
							if (bytes != null)
								json = new String(bytes);
							coutRepeat = coutRepeat + 1;
							if (coutRepeat == 5)
								break;
							Thread.sleep(5000);
						}
					}
					if (json != null && json.startsWith("{"))
						object = new JSONObject(json);
					count++;
					if (count == 5)
						break;
					Thread.sleep(2000);
				}
			}*/
			boolean isDataFromBvd = false;
			if (object.has(source) && (isBstDataThere || level == 0)) {
				bstData = object.getJSONObject(source);
			}/*else if(level==0){
				while(!object.has(source)){
					switchToOtherSource(sourceInfo,source);
				}
				bstData = object.getJSONObject(source);
			}*/
			if (object.has(CLIENT_ID)) {
				clientData = object.getJSONObject(CLIENT_ID);
			}
			JSONArray ids = new JSONArray();
			JSONArray idsFromClient = clientData.names();
			ids = bstData.names();
			/*if (idsFromClient != null) {
				for (int i = 0; i < idsFromClient.length(); i++) {
					if (ids == null) {
						ids = new JSONArray();
					}
					//
					if (level==0 && ids != null && ids.length() > 0) {
							if (!ids.getString(0).equals(idsFromClient.getString(i)))
								ids.put(ids.getString(0));
							else
								ids.put(idsFromClient.getString(i));
					}
					else {
						ids.put(idsFromClient.getString(i));
					}
					//
				}
			}*/
			String customKey=identifier+"_"+CLIENT_ID;
			if (level == 0) {
				if (idsFromClient != null) {
					for (int i = 0; i < idsFromClient.length(); i++) {
						if (ids != null && ids.length() > 0) {
							if (!ids.getString(0).equals(idsFromClient.getString(i)))
								ids.put(ids.getString(0));
						}
					}
				}
			} else {
				if (idsFromClient != null) {
					for (int i = 0; i < idsFromClient.length(); i++) {
						if (ids == null)
							ids = new JSONArray();
						ids.put(idsFromClient.getString(i));
					}
				}
			}
			if (level == 0 && ids == null) {
				ids = new JSONArray();
				if (idsFromClient != null && idsFromClient.length() > 0) {
					for (int i = 0; i < idsFromClient.length(); i++) {
						if (idsFromClient.getString(i).equals(customKey))
							ids.put(customKey);
					}
				}
			}
			JSONObject prevoiuseShareHolders=new JSONObject();
			if(holdingPreviousLeveltData!=null && holdingPreviousLeveltData.size()>0)
				prevoiuseShareHolders=holdingPreviousLeveltData.get(holdingPreviousLeveltData.size()-1);
			if (ids == null)
				ids = new JSONArray();
			org.json.simple.JSONArray presentLevelArray = new org.json.simple.JSONArray();
			if ((ids != null && ids.length() > 0) || level != 0) {
				List<JSONObject> previousLists = new ArrayList<>();
				if (allShareholders.size() > 0 && (list.size() == level))
					previousLists = allShareholders.get(allShareholders.size() - 1);
				else if (list.size() == level) {
					for (int i = 0; i < ids.length(); i++) {
						if(!presentLevelArray.contains(ids.get(i).toString()) && !"status".equalsIgnoreCase(ids.get(i).toString()))
							presentLevelArray.add(ids.get(i).toString());
					}
				}
				JSONArray presentIds = new JSONArray();
				for (JSONObject jsonObject : previousLists) {
					if(jsonObject.has("parentIds"))
						presentIds = jsonObject.getJSONArray("parentIds");
					for (int i = 0; i < presentIds.length(); i++) {
						String parentId = presentIds.getJSONObject(i).getString("parentId");
						for (int j = 0; j < ids.length(); j++) {
							if (parentId.equals(ids.getString(j)) && !presentLevelArray.contains(ids.getString(j)) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								presentLevelArray.add(ids.getString(j));
								break;
							} else if (persons.has(parentId) && !presentLevelArray.contains(parentId) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								presentLevelArray.add(parentId);
								break;
							} else if (presentIds.getJSONObject(i).has("basic")
									&& !presentLevelArray.contains(parentId) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								presentLevelArray.add(parentId);
								break;
							}else if (isBstDataThere && prevoiuseShareHolders.has(parentId)
									&& !presentLevelArray.contains(parentId) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								if (jsonObject.has("indirectPercentage")) {
									if ((jsonObject.getDouble("indirectPercentage") >= lowRange
											&& jsonObject.getDouble("indirectPercentage") <= highRange)) {
										presentLevelArray.add(parentId);
										break;
									}
								}
							}
						}
					}
				}
				if(presentLevelArray!=null && presentLevelArray.size()>0){
					for (int j = 0; j < presentLevelArray.size(); j++) {
						JSONObject mapJson = new JSONObject();
						JSONObject tempJson = new JSONObject();
						JSONObject presentJson = new JSONObject();
						JSONObject clientJson = new JSONObject();
						JSONObject basicJson = new JSONObject();
						JSONObject person = new JSONObject();
						String user = null;
						boolean isCustom = false;
						String organisationName = null;
						String hasURL = null;
						String shareHolderJuridiction = null;
						String country = null;
						String entityType = null;
						double indirectPercentage = 0;
						double totalPercentage = 0;
						String innerSource=null;
						String from=null;
						boolean needParents=true;
						String dateOfBirth=null;
						String role=null;
						String classification=null;
						if (holders.has(presentLevelArray.get(j).toString())) {
							tempJson = holders.getJSONObject(presentLevelArray.get(j).toString());
						}
						for (JSONObject jsonObject : previousLists) {
							if (jsonObject.has("parentIds") && jsonObject.get("parentIds") instanceof JSONArray)
								presentIds = jsonObject.getJSONArray("parentIds");
							if (presentIds != null && presentIds.length() > 0) {
								for (int i = 0; i < presentIds.length(); i++) {
									String parentId = presentIds.getJSONObject(i).getString("parentId");
									if (presentLevelArray.get(j).toString().equalsIgnoreCase(parentId)) {
										String subSource = null;
										if (presentIds.getJSONObject(i).has("basic")) {
											basicJson = presentIds.getJSONObject(i).getJSONObject("basic");
										}
										if (presentIds.getJSONObject(i).has("user")) {
											user = presentIds.getJSONObject(i).getString("user");
										}
										if (presentIds.getJSONObject(i).has("isCustom")) {
											isCustom = presentIds.getJSONObject(i).getBoolean("isCustom");
										}
										///
										if (presentIds.getJSONObject(i).has("dateOfBirth")) {
											dateOfBirth = presentIds.getJSONObject(i).getString("dateOfBirth");
										}
										if (presentIds.getJSONObject(i).has("role")) {
											role = presentIds.getJSONObject(i).getString("role");
										}

										if (presentIds.getJSONObject(i).has("classification")) {
											classification = presentIds.getJSONObject(i).getString("classification");
										}
										///
										if (presentIds.getJSONObject(i).has("source")) {
											subSource = presentIds.getJSONObject(i).getString("source");
											if (subSource != null) {
												if (subSources.get(subSource) != null) {
													innerSource = subSources.get(subSource).toString();
												}
											}

										}
										if (presentIds.getJSONObject(i).has("from")) {
											from = presentIds.getJSONObject(i).getString("from");
										}
										if (presentIds.getJSONObject(i).has("source")) {
											subSource = presentIds.getJSONObject(i).getString("source");
											if (subSource != null) {
												if (subSources.get(subSource) != null) {
													innerSource = subSources.get(subSource).toString();
												}
											}

										}
										if (presentIds.getJSONObject(i).has("from")) {
											from = presentIds.getJSONObject(i).getString("from");
										}
									}
								}
							}
						}
						if (clientData.has(presentLevelArray.get(j).toString())) {
							clientJson = clientData.getJSONObject(presentLevelArray.get(j).toString());
						}
						if (basicJson != null && !basicJson.has("vcard:organization-name")
								&& bstData.has(presentLevelArray.get(j).toString())) {
							presentJson = bstData.getJSONObject(presentLevelArray.get(j).toString());
							
							if(presentJson.has("basic"))
								basicJson = presentJson.getJSONObject("basic");
						} else if (!(bstData.has(presentLevelArray.get(j).toString()))
								&& (clientData.has(presentLevelArray.get(j).toString()))) {
							presentJson = clientData.getJSONObject(presentLevelArray.get(j).toString());
							basicJson = presentJson.getJSONObject("basic");
						} else if (persons.has(presentLevelArray.get(j).toString())) {
							mapJson = persons.getJSONObject(presentLevelArray.get(j).toString());
						}else if (prevoiuseShareHolders.has(presentLevelArray.get(j).toString())) {
							presentJson = prevoiuseShareHolders.getJSONObject(presentLevelArray.get(j).toString());
							if (presentJson.has("basic"))
								basicJson = presentJson.getJSONObject("basic");
							needParents = false;
						}
						if(presentJson!=null && presentJson.has("basic")){
							holdingDataForNextLevel.put(presentLevelArray.get(j).toString(),presentJson);
						}
						if (!mapJson.has("id")) {
							
							if (presentJson.has("entity_type")) {
								if ((presentJson.get("entity_type") instanceof String)) {
									entityType = presentJson.getString("entity_type");
								} else {
									entityType = "C";
								}
								if (entityType != null) {
									if ("S".equalsIgnoreCase(entityType)) {
										mapJson.put("news", new JSONArray());
										mapJson.put("screeningFlag", true);
									}
									if (personTypes.contains(entityType))
										entityType = "person";
									else
										entityType = "organization";
								}
							} else if (basicJson.has("entity_type")) {
								entityType = basicJson.getString("entity_type");
								if (personTypes.contains(entityType)) {
									entityType = "person";
								} else {
									entityType = "organization";
								}
								
							} else {
								entityType = "organization";
							}
							////
							if (presentJson.has("dateOfBirth")) {
								dateOfBirth = presentJson.getString("dateOfBirth");
								mapJson.put("dateOfBirth", presentJson.getString("dateOfBirth"));
							}
							
							if (presentJson.has("role")) {
								role = presentJson.getString("role");
								mapJson.put("role", presentJson.getString("role"));
							}
							
							if (presentJson.has("classification")) {
								classification = presentJson.getString("classification");
								mapJson.put("classification", presentJson.getString("classification"));
							}
							
							///
							if (basicJson.has("vcard:organization-name")) {
								organisationName = basicJson.getString("vcard:organization-name");
							} else if (basicJson.has("vcard:hasName")) {
								organisationName = basicJson.getString("vcard:hasName");
							}
							if (basicJson.has("isDomiciledIn"))
								shareHolderJuridiction = basicJson.getString("isDomiciledIn");
							if (basicJson.has("hasURL"))
								hasURL = basicJson.getString("hasURL");
							if (basicJson.has("mdaas:RegisteredAddress")) {
								JSONObject registeredAddress = basicJson.getJSONObject("mdaas:RegisteredAddress");
								if (registeredAddress.has("country")) {
									country = registeredAddress.getString("country");
								}
							}
							if (country != null)
								mapJson.put("country", country);
							else
								mapJson.put("country", "");
							if (level!=null && level == 0) {
								if (requestOrganisation.equalsIgnoreCase("undefined"))
									mapJson.put("title", organisationName);
								else
									mapJson.put("title", requestOrganisation);
								mapJson.put("jurisdiction", juridiction);
								
								mapJson.put("identifier", identifier);
								if(errorMsg!=null || isStatusErrorTrue.has("source")) {
									mapJson.put("is-error", true);
									mapJson.put("error", errorMsg);
								}else
									mapJson.put("is-error", false);
									
								mapJson.put("entity_id", "orgChartmainEntity");
								JSONArray sourceToShow=new JSONArray();
								if (sources != null && sources.length()>0) {
									for (int i = 0; i < sources.length(); i++) {
										if (!"companyhouse.co.uk".equalsIgnoreCase(sources.getString(i))) {
											sourceToShow.put(sources.getString(i));
										}
									}
								}
								if(juridiction!=null && "GB".equalsIgnoreCase(juridiction))
									mapJson.put("sources", sources);
								else
									mapJson.put("sources", sourceToShow);

								indirectPercentage = 100;
								totalPercentage = 100;
							} else {
								mapJson.put("title", organisationName);
								if (shareHolderJuridiction != null)
									mapJson.put("jurisdiction", shareHolderJuridiction);
								else
									mapJson.put("jurisdiction", juridiction);
								
								
								mapJson.put("identifier", presentLevelArray.get(j).toString());
								
								mapJson.put("entity_id", "orgChartParentEntity");
								mapJson.put("classification", classification);
								mapJson.put("role", role);
								mapJson.put("dateOfBirth", dateOfBirth);
							}
							mapJson.put("bvdId", presentLevelArray.get(j).toString());
							if (user == null)
								mapJson.put("source_evidence", source);
							else
								mapJson.put("source_evidence", user);
							if (isCustom)
								mapJson.put("isCustom", isCustom);
							else
								mapJson.put("isCustom", false);
							mapJson.put("entity_type", entityType);
							mapJson.put("hasURL", hasURL);
							mapJson.put("basic", basicJson);
							mapJson.put("level", level);
							if(innerSource!=null)
								mapJson.put("innerSource", innerSource);
							else if(!isCustom)
								mapJson.put("innerSource", "BST");
							mapJson.put("from", from);
							if (level != 0) {
								mapJson.put("childLevel", level - 1);
							}
							if(basicJson.has("sourceUrl"))
								mapJson.put("sourceUrl", basicJson.getString("sourceUrl"));
							if(basicJson.has("dateOfBirth"))
								mapJson.put("dateOfBirth", basicJson.getString("dateOfBirth"));
							if (level!=0) {
								mapJson.put("childLevel", level-1);
							}
							mapJson.put("parents", new JSONArray());
							mapJson.put("name", mapJson.getString("title"));
							mapJson.put("isSubsidiarie", false);
						}
						if (!"person".equalsIgnoreCase(mapJson.getString("entity_type"))) {
							if (tempJson.has("id")) {
								mapJson.put("id", tempJson.getString("id"));
							} else {
								mapJson.put("id", "p" + level + j);
							}

						}
						double prevIndirectPercentage=0.0;
						double prevTotalPercentage=0.0;
						double presentTotalPercentage=0.0;
						JSONArray parentIds =null;
						for (JSONObject prevJsonObject : previousLists) {
							if (prevJsonObject.has("parentIds") && prevJsonObject.get("parentIds") instanceof JSONArray)
								parentIds = prevJsonObject.getJSONArray("parentIds");
							if (parentIds != null && parentIds.length() > 0) {
								for (int k = 0; k < parentIds.length(); k++) {
									String parentId = parentIds.getJSONObject(k).getString("parentId");
									if (parentId.equals(presentLevelArray.get(j).toString())) {
										if (prevJsonObject.has("indirectPercentage") && prevJsonObject.get("indirectPercentage") instanceof Double)
											prevIndirectPercentage = prevJsonObject.getDouble("indirectPercentage");
										if (prevJsonObject.has("indirectPercentage") && prevJsonObject.get("indirectPercentage") instanceof String)
											prevIndirectPercentage = Double.parseDouble(prevJsonObject.getString("indirectPercentage"));
										if (prevJsonObject.has("indirectPercentage") && prevJsonObject.get("indirectPercentage") instanceof Integer)
											prevIndirectPercentage = prevJsonObject.getInt("indirectPercentage");
										if (prevJsonObject.has("totalPercentage"))
											//////////////////////////////////////////////////////////
											prevTotalPercentage = prevJsonObject.getDouble("totalPercentage");
										if (parentIds.getJSONObject(k).has("totalPercentage"))
											totalPercentage = parentIds.getJSONObject(k).getDouble("totalPercentage");
										indirectPercentage = (totalPercentage * prevIndirectPercentage) / 100;
										double presentIndirectPercentage = (totalPercentage * prevIndirectPercentage)
												/ 100;
										if (parentIds.getJSONObject(k).has("totalPercentage"))
											presentTotalPercentage = parentIds.getJSONObject(k)
													.getDouble("totalPercentage");
										if (tempJson.has("indirectPercentage") && tempJson.has("isCustom")) {
											if (prevIndirectPercentage >= lowRange
													&& prevIndirectPercentage <= highRange) {
												totalPercentage = tempJson.getDouble("totalPercentage")
														+ totalPercentage;
												indirectPercentage = tempJson.getDouble("indirectPercentage")
														+ indirectPercentage;
												tempJson.put("indirectPercentage", indirectPercentage);
												tempJson.put("totalPercentage", totalPercentage);
											}
											mapJson.put("totalPercentage",
													tempJson.getDouble("totalPercentage") + totalPercentage);
											mapJson.put("indirectPercentage",
													tempJson.getDouble("indirectPercentage") + indirectPercentage);
										} else if (mapJson.has("indirectPercentage")
												&& "organization".equalsIgnoreCase(mapJson.getString("entity_type"))) {
											if (prevIndirectPercentage >= lowRange
													&& prevIndirectPercentage <= highRange) {
												indirectPercentage = mapJson.getDouble("indirectPercentage")
														+ indirectPercentage;
												totalPercentage = mapJson.getDouble("totalPercentage")
														+ totalPercentage;
											}
											mapJson.put("indirectPercentage", indirectPercentage);
											mapJson.put("totalPercentage", totalPercentage);
										} else {
											if (prevIndirectPercentage >= lowRange
													&& prevIndirectPercentage <= highRange) {
												mapJson.put("indirectPercentage", indirectPercentage);
												mapJson.put("totalPercentage", totalPercentage);
											}
										}
										if (mapJson.has("entity_type")) {
											String tempEntityType = mapJson.getString("entity_type");
											if ("person".equalsIgnoreCase(tempEntityType)
													&& !persons.has(presentLevelArray.get(j).toString())) {
												mapJson.put("id", "p" + level + j);
												mapJson.put("indirectPercentage", indirectPercentage);
												mapJson.put("totalPercentage", totalPercentage);
												mapJson.put("indirectChilds", new JSONArray());
												JSONArray parentArray = new JSONArray();
												getParents(presentJson, clientJson, indirectPercentage, parentArray);
												mapJson.put("parentIds", parentArray);
												checkEntityType(mapJson, organisationName, entityType);
												JSONObject indirectObject = new JSONObject();
												if (map.has(prevJsonObject.getString("id"))) {
													JSONArray indirectChilds = new JSONArray();
													indirectObject.put("id", prevJsonObject.getString("id"));
													indirectObject.put("totalPercentage", totalPercentage);
													indirectObject.put("indirectPercentage", indirectPercentage);
													indirectObject.put("level", prevJsonObject.getInt("level"));
													indirectChilds.put(indirectObject);
													mapJson.put("indirectChilds", indirectChilds);
												} else {
													JSONArray indirectChilds = new JSONArray();
													String childJson = prevJsonObject.getString("child");
													while (!map.has(childJson)) {
														for (List<JSONObject> listJsonObject : allShareholders) {
															for (JSONObject jsonObject : listJsonObject) {
																if (childJson.equals(jsonObject.getString("id"))) {
																	childJson = jsonObject.getString("child");
																	indirectObject.put("level",
																			jsonObject.getInt("level"));
																}
															}
														}
													}
													indirectObject.put("id", childJson);
													indirectObject.put("totalPercentage", totalPercentage);
													indirectObject.put("indirectPercentage", indirectPercentage);
													indirectChilds.put(indirectObject);
													mapJson.put("indirectChilds", indirectChilds);
												}
												persons.put(presentLevelArray.get(j).toString(), mapJson);
											} else if ("person".equalsIgnoreCase(mapJson.getString("entity_type"))
													&& persons.has(presentLevelArray.get(j).toString())) {
												JSONObject prevPerson = persons
														.getJSONObject(presentLevelArray.get(j).toString());
												double presentIndirect = indirectPercentage;
												mapJson.put("totalPercentage", totalPercentage);
												mapJson.put("id", prevPerson.getString("id"));
												indirectPercentage = indirectPercentage
														+ prevPerson.getDouble("indirectPercentage");
												prevPerson.put("indirectPercentage", indirectPercentage);
												JSONArray indirectChilds = prevPerson.getJSONArray("indirectChilds");
												JSONObject indirectObject = new JSONObject();
												if (map.has(prevJsonObject.getString("id"))) {
													boolean isAlreadChild = false;
													for (int i = 0; i < indirectChilds.length(); i++) {
														if (prevJsonObject.getString("id").equalsIgnoreCase(
																indirectChilds.getJSONObject(i).getString("id"))) {
															indirectChilds.getJSONObject(i)
																	.put("indirectPercentage",
																			indirectChilds.getJSONObject(i)
																					.getDouble("indirectPercentage")
																					+ presentIndirect);
															isAlreadChild = true;
														}
													}
													if (!isAlreadChild) {

														indirectObject.put("id", prevJsonObject.getString("id"));
														indirectObject.put("totalPercentage", totalPercentage);
														if (!isAlreadChild)
															indirectObject.put("indirectPercentage", presentIndirect);
														indirectObject.put("level", prevJsonObject.getInt("level"));
														indirectChilds.put(indirectObject);
													}
													prevPerson.put("indirectChilds", indirectChilds);
													mapJson.put("indirectChilds", indirectChilds);
												} else {
													String childJson = prevJsonObject.getString("child");
													while (!map.has(childJson)) {
														for (List<JSONObject> listJsonObject : allShareholders) {
															for (JSONObject jsonObject : listJsonObject) {
																if (childJson.equals(jsonObject.getString("id"))) {
																	childJson = jsonObject.getString("child");
																}
															}
														}
													}
													boolean isAlreadChild = false;
													for (int i = 0; i < indirectChilds.length(); i++) {
														if (childJson.equalsIgnoreCase(
																indirectChilds.getJSONObject(i).getString("id"))) {
															if (indirectChilds.getJSONObject(i)
																	.has("indirectPercentage")) {
																indirectChilds.getJSONObject(i).put(
																		"indirectPercentage",
																		indirectChilds.getJSONObject(i)
																				.getDouble("indirectPercentage")
																				+ presentIndirect);
															}
															isAlreadChild = true;
														}
													}
													if (!isAlreadChild) {
														indirectObject.put("level",
																map.getJSONObject(childJson).getInt("level"));
														indirectObject.put("id",
																map.getJSONObject(childJson).getString("id"));
														indirectObject.put("totalPercentage", totalPercentage);
														if (isAlreadChild)
															indirectObject.put("indirectPercentage", presentIndirect);
														indirectChilds.put(indirectObject);
													}
													prevPerson.put("indirectChilds", indirectChilds);
													mapJson.put("indirectChilds", indirectChilds);
												}
											}
										}
										if ((indirectPercentage >= lowRange && indirectPercentage <= highRange)
												|| (tempJson.has("indirectPercentage")
														&& tempJson.getDouble("indirectPercentage") >= lowRange
														&& tempJson.getDouble("indirectPercentage") <= highRange)) {
											checkParent(map, prevJsonObject, mapJson, level, j, allShareholders,
													persons, tempJson, holders, presentTotalPercentage,
													presentIndirectPercentage);
										} else if ("organization".equalsIgnoreCase(mapJson.getString("entity_type"))) {
											checkChild(map, prevJsonObject, mapJson, level, j, allShareholders);
										}
									}
								}
							}
						}
						if ("organization".equalsIgnoreCase(entityType)) {
							JSONArray parentArray = new JSONArray();
							if(needParents)
								getParents(presentJson, clientJson, indirectPercentage, parentArray);
							mapJson.put("parentIds", parentArray);
							mapJson.put("totalPercentage", totalPercentage);
							mapJson.put("indirectPercentage", indirectPercentage);
							// checking and adding entity type
							checkEntityType(mapJson, organisationName, entityType);
						}
						refListWithOutRange.add(mapJson);
						if ((indirectPercentage >= lowRange && indirectPercentage <= highRange) || 
								(tempJson.has("indirectPercentage") && tempJson.getDouble("indirectPercentage")>= lowRange
								&& tempJson.getDouble("indirectPercentage")<= highRange)) {
							if (!map.has(mapJson.getString("id"))) {
								map.put(mapJson.getString("id"), mapJson);
								holders.put(mapJson.getString("identifier"), mapJson);
								refListWithInRange.add(mapJson);
								array.put(mapJson);
								//finalJson.add(mapJson);
							} else if (map.has(mapJson.getString("id")) && "organization".equalsIgnoreCase(entityType)) {
								refListWithInRange.add(mapJson);
							}
						}
					}
				}else if(level==0){
					JSONObject mapJson = new JSONObject();
					mapJson.put("id", "p00");
					mapJson.put("identifier", identifier);
					mapJson.put("name", requestOrganisation);
					mapJson.put("title", requestOrganisation);
					mapJson.put("jurisdiction", juridiction);
					mapJson.put("level", 0);
					mapJson.put("parentIds", new JSONArray());
					mapJson.put("parents", new JSONArray());
					mapJson.put("entity_type", "organization");
					mapJson.put("entity_id", "orgChartmainEntity");
					mapJson.put("isSubsidiarie", false);
					mapJson.put("source_evidence", source);
					mapJson.put("screeningFlag", true);
					mapJson.put("subsidiaries", new JSONArray());
					mapJson.put("officership", new JSONArray());
					if (ids != null && ids.length() > 0 && "status".equalsIgnoreCase(ids.getString(0)))
						mapJson.put("isDataNotFound", true);
					refListWithInRange.add(mapJson);
					array.put(mapJson);
				}
				allShareholders.add(refListWithOutRange);
				holdingPreviousLeveltData.add(holdingDataForNextLevel);
				writeFile(file, array);
				if (refListWithInRange.size() > 0) {
					double conflictPercentage=0;
					list.add(refListWithInRange);
					if (isScreeningRequired) {
						executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, list, level, true,
								new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes,
								startDate, endDate, BE_ENTITY_API_KEY));
					} else {
						for (JSONObject jsonObject : list.get(level)) {
							jsonObject.put("screeningFlag", true);
						}
					}
					array.getJSONObject(array.length() - 1).put("status", "inprogress");
					while (array.length() > tempCount) {
						String companyName = array.getJSONObject(tempCount).getString("title");
						String shareHolderJuridiction = array.getJSONObject(tempCount).getString("jurisdiction");
						String country = array.getJSONObject(tempCount).getString("country");
						String website = null;
						if (array.getJSONObject(tempCount).has("hasURL"))
							website = array.getJSONObject(tempCount).getString("hasURL");
						if (isSubsidiariesRequired) {
							executor.submit(new MultiSourceSubsidiaries(identifier, array, noOfSubsidiaries,
									maxSubsidiarielevel, tempCount, companyName, shareHolderJuridiction, website,
									new AdvanceSearchServiceImpl(), BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor,
									personTypes, pepTypes, startDate, endDate, CLIENT_ID, BE_ENTITY_API_KEY));
						} else {
							array.getJSONObject(tempCount).put("subsidiaries", new JSONArray());
						}
						/*if (tempCount == 0) {
							String highCredibilitySource=getKeyManagementSource(keys);
							List<EntityOfficerInfoDto> listOfOfficer= entityOfficerInfoService.getEntityOfficerList(identifier);
							 
							executor.submit(new OfficershipScreening(array, identifier, juridiction, SCREENING_URL,
									BIGDATA_MULTISOURCE_URL, new AdvanceSearchServiceImpl(), country, file, personTypes,
									pepTypes, startDate, endDate, isScreeningRequired, BE_ENTITY_API_KEY, CLIENT_ID,highCredibilitySource,listOfOfficer));
						}*/
						tempCount = tempCount + 1;
					}
					try {
						if (((maxShareholderLevel - 1) == level)
								|| ((shareholders.length() - 1) == level && !(links.has("next")))
								|| (shareholders.length() == 0)) {
							for (int i = 0; i < array.length(); i++) {
								while ((!array.getJSONObject(i).has("subsidiaries"))
										|| (array.getJSONObject(i).has("subsidiaries")
												&& array.getJSONObject(i).getJSONArray("subsidiaries").length() > 0
												&& array.getJSONObject(i).getJSONArray("subsidiaries")
												.getJSONObject(array.getJSONObject(i)
														.getJSONArray("subsidiaries").length() - 1)
												.getString("status").equals("pending"))
										/*|| (!array.getJSONObject(0).has("officership"))*/
										|| (array.getJSONObject(0).has("officership")
												&& array.getJSONObject(0).getJSONArray("officership").length() > 0
												&& array.getJSONObject(0).getJSONArray("officership").getJSONObject(
														array.getJSONObject(0).getJSONArray("officership").length() - 1)
												.getString("status").equals("pending"))
										|| (!array.getJSONObject(i).has("screeningFlag"))) {
									array.getJSONObject(array.length() - 1).put("status", "inprogress");
									Thread.sleep(10000);
									writeFile(file, array);
								}
							}
							//array.getJSONObject(array.length() - 1).put("status", "completed");
							
							if(array != null && array.length() > 0){
								array.getJSONObject(array.length() - 1).put("status", "completed");
							}
							if(array.length() <= 0){
								JSONObject jsonObject = new JSONObject();
								array.put(jsonObject.put("status", "completed"));
							}
							
							getCustomShareholdersFromLatLevel(list, array,lowRange,highRange,holders,map);
							getFactualControl(array, persons);
							getConflicts(array,map,lowRange,highRange);
						} else {
							array.getJSONObject(array.length() - 1).put("status", "inprogress");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					writeFile(file, array);
				} else {
					try {
						for (int i = 0; i < array.length(); i++) {
							while ((!array.getJSONObject(i).has("subsidiaries"))
									|| (array.getJSONObject(i).has("subsidiaries")
											&& array.getJSONObject(i).getJSONArray("subsidiaries").length() > 0
											&& array.getJSONObject(i).getJSONArray("subsidiaries").getJSONObject(
													array.getJSONObject(i).getJSONArray("subsidiaries").length() - 1)
											.getString("status").equals("pending"))
									/*|| (!array.getJSONObject(0).has("officership"))*/
									|| (array.getJSONObject(0).has("officership")
											&& array.getJSONObject(0).getJSONArray("officership").length() > 0
											&& array.getJSONObject(0).getJSONArray("officership").getJSONObject(
													array.getJSONObject(0).getJSONArray("officership").length() - 1)
											.getString("status").equals("pending"))
									|| (!array.getJSONObject(i).has("screeningFlag"))) {
								array.getJSONObject(array.length() - 1).put("status", "inprogress");
								Thread.sleep(10000);
								writeFile(file, array);
							}
						}
						
						if(array != null && array.length() > 0){
							array.getJSONObject(array.length() - 1).put("status", "completed");
						}
						if(array.length() <= 0){
							JSONObject jsonObject = new JSONObject();
							array.put(jsonObject.put("status", "completed"));
						}
						//array.getJSONObject(array.length() - 1).put("status", "completed");
						getCustomShareholdersFromLatLevel(list, array,lowRange,highRange,holders,map);
						getFactualControl(array, persons);
						getConflicts(array,map,lowRange,highRange);
						writeFile(file, array);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				whenShareholderDataEmpty(identifier, requestOrganisation, juridiction, lowRange, highRange,
						noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, executor,
						userId, isSubsidiariesRequired, startDate, endDate, sourceInfo, url, path, isScreeningRequired,true,source,array,sources,errorMsg,sourceType);
			}
		} else if(documentUrl!=null){
			whenShareholderDataEmpty(identifier, requestOrganisation, juridiction, lowRange, highRange,
					noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, executor,
					userId, isSubsidiariesRequired, startDate, endDate, sourceInfo, url, path, isScreeningRequired,true,source,array,sources,errorMsg,sourceType);
		}else {
			whenSourcesNotFound(identifier, requestOrganisation, juridiction, lowRange, highRange,
					noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, 
					userId, isSubsidiariesRequired, startDate, endDate, path, isScreeningRequired,array,sourceType);
		} 
		return list;
	}

	private void whenSourcesNotFound(String identifier, String requestOrganisation, String juridiction, Integer lowRange,
			Integer highRange, Integer noOfSubsidiaries, Integer maxSubsidiarielevel, File file,
			String annualReportLink, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,
			 Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate,
			String path, boolean isScreeningRequired, JSONArray array,String sourceType)
			throws FileNotFoundException, IOException, ParseException, InterruptedException {
		List<List<JSONObject>> refList = new ArrayList<List<JSONObject>>();
		JSONObject finalJson = new JSONObject();
		JSONObject sourceData = null;
		String overviewData = null;
		JSONObject result = null;
		JSONObject overviewDataJson = new JSONObject();
		finalJson.put("name", requestOrganisation);
		finalJson.put("title", requestOrganisation);
		try {
			String url = buildMultiSourceUrl(identifier, "overview", null, null, null, CLIENT_ID,sourceType);
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(url);
			overviewData = getHierarchyData(hierarchyDto, null, BE_ENTITY_API_KEY);
			if (overviewData != null)
				overviewDataJson = new JSONObject(overviewData);
			if (overviewDataJson.has("is-completed")) {
				int counter = 0;
				while (overviewDataJson.has("is-completed") && !overviewDataJson.has("is-completed")) {
					overviewData = getHierarchyData(hierarchyDto, null, BE_ENTITY_API_KEY);
					if (overviewData != null)
						overviewDataJson = new JSONObject(overviewData);
					counter = counter + 1;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
		}
		String country = null;
		if (!overviewDataJson.has("results") || overviewDataJson.getJSONArray("results").length() == 0) {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
			Object fileJsonData = parser.parse(new FileReader(fileJSon));
			org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
			for (int k = 0; k < countryJson.size(); k++) {
				org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
				if (countryObj.get("countryCode").toString().equalsIgnoreCase(juridiction)) {
					country = countryObj.get("country").toString();
				}
			}
			finalJson.put("country", country);
			finalJson.put("name", requestOrganisation);
			finalJson.put("title", requestOrganisation);
		}
		if (overviewDataJson.has("results")) {
			JSONArray results = overviewDataJson.getJSONArray("results");
			if (results.length() > 0) {
				result = results.getJSONObject(0);
				if (result.has("overview")) {
					JSONArray addressArray = null;
					JSONObject address = null;
					JSONObject overview = result.getJSONObject("overview");
					JSONArray sources = overview.names();
					if (sources != null && sources.length() > 0) {
						sourceData = overview.getJSONObject(sources.getString(0));
						if (sourceData.has("vcard:organization-name")) {
							finalJson.put("name", sourceData.getString("vcard:organization-name"));
							finalJson.put("title", sourceData.getString("vcard:organization-name"));
						}
						if (sourceData.has("mdaas:RegisteredAddress")) {
							if (sourceData.get("mdaas:RegisteredAddress") instanceof JSONObject)
								address = sourceData.getJSONObject("mdaas:RegisteredAddress");
							else
								addressArray = sourceData.getJSONArray("mdaas:RegisteredAddress");
							if (addressArray != null && addressArray.length() > 0) {
								address = addressArray.getJSONObject(0);
							}
							if (address != null && address.has("country")) {
								country = address.getString("country");
							}
							finalJson.put("country", country);
						}
					}
				}
			}
		}
		finalJson.put("id", "p00");
		finalJson.put("totalPercentage", 100);
		finalJson.put("indirectPercentage", 100);
		finalJson.put("identifier", identifier);
		finalJson.put("sources", new JSONArray());
		finalJson.put("entity_id", "orgChartmainEntity");
		finalJson.put("entity_type", "organization");
		if (sourceData != null)
			finalJson.put("basic", sourceData);
		finalJson.put("jurisdiction", juridiction);
		finalJson.put("parents", new JSONArray());
		finalJson.put("status", "inprogress");
		array.put(finalJson);
		writeFile(file, array);
		List<JSONObject> lewiseData = new ArrayList<>();
		lewiseData.add(finalJson);
		refList.add(lewiseData);
		if (isScreeningRequired) {
			executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 0, true,
					new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes, startDate,
					endDate, BE_ENTITY_API_KEY));
		} else {
			array.getJSONObject(0).put("screeningFlag", true);
		}
		if (isSubsidiariesRequired) {
			executor.submit(new MultiSourceSubsidiaries(identifier, array, noOfSubsidiaries, maxSubsidiarielevel, 0,
					finalJson.getString("name"), juridiction, null, new AdvanceSearchServiceImpl(),
					BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes, startDate, endDate,
					CLIENT_ID, BE_ENTITY_API_KEY));
		} else {
			array.getJSONObject(0).put("subsidiaries", new JSONArray());
		}
		/*executor.submit(new OfficershipScreening(array, identifier, juridiction, SCREENING_URL, BIGDATA_MULTISOURCE_URL,
				new AdvanceSearchServiceImpl(), country, file, personTypes, pepTypes, startDate, endDate,
				isScreeningRequired, BE_ENTITY_API_KEY, CLIENT_ID));*/
		if (array != null) {
			for (int j = 0; j < array.length(); j++) {
				while ((!array.getJSONObject(j).has("subsidiaries"))
						|| (array.getJSONObject(j).has("subsidiaries")
								&& array.getJSONObject(j).getJSONArray("subsidiaries").length() > 0
								&& array.getJSONObject(j).getJSONArray("subsidiaries")
										.getJSONObject(array.getJSONObject(j).getJSONArray("subsidiaries").length() - 1)
										.getString("status").equals("pending"))
						/*|| (!array.getJSONObject(0).has("officership"))*/
						|| (array.getJSONObject(0).has("officership")
								&& array.getJSONObject(0).getJSONArray("officership").length() > 0
								&& array.getJSONObject(0).getJSONArray("officership")
										.getJSONObject(array.getJSONObject(0).getJSONArray("officership").length() - 1)
										.getString("status").equals("pending"))
						|| (!array.getJSONObject(j).has("screeningFlag"))) {
					array.getJSONObject(array.length() - 1).put("status", "inprogress");
					Thread.sleep(10000);
					writeFile(file, array);
				}
			}
		}
		array.getJSONObject(array.length() - 1).put("status", "completed");
		writeFile(file, array);
	}

	@SuppressWarnings("unused")
	private void getConflicts(JSONArray array) {
		Map<Integer,Double> map=new HashMap<Integer,Double>();
		for (int i = 0; i < array.length(); i++) {
			if(map.containsKey(array.getJSONObject(i).getInt("level"))){
				double value=0;
				value=map.get(array.getJSONObject(i).getInt("level"))+array.getJSONObject(i).getDouble("totalPercentage");
				map.put(array.getJSONObject(i).getInt("level"), value);
			}else{
				map.put(array.getJSONObject(i).getInt("level"), array.getJSONObject(i).getDouble("totalPercentage"));
			}
		}
		for (int i = 0; i < array.length(); i++) {
			double percentage=map.get(array.getJSONObject(i).getInt("level"));
			if(percentage>100)
				array.getJSONObject(i).put("isConflict",true);
			else
				array.getJSONObject(i).put("isConflict",false);
		}
	}

	@SuppressWarnings("unused")
	private void getDataFromOtherSources(String identifier, String requestOrganisation, String juridiction,
			Integer lowRange, Integer highRange, Integer noOfSubsidiaries, Integer maxSubsidiarielevel, File file,
			String annualReportLink, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,
			ThreadPoolExecutor executor, Long userId, Boolean isSubsidiariesRequired, String startDate,
			String endDate, String otherSource,boolean isScreeningRequired,String sourceType) throws Exception {
		JSONArray finalArray = new JSONArray();
		List<List<JSONObject>> refList = new ArrayList<List<JSONObject>>();
		JSONObject finalJson = new JSONObject();
		JSONObject sourceData = null;
		String overviewData = null;
		JSONObject result = null;
		JSONObject overviewDataJson = new JSONObject();
		try {
			String url = buildMultiSourceUrl(identifier, "overview", null, null, null,null,sourceType);
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(url);
			overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
			if (overviewData != null)
				overviewDataJson = new JSONObject(overviewData);
			if (overviewDataJson.has("is-completed")) {
				int counter = 0;
				while (overviewDataJson.has("is-completed") && !overviewDataJson.has("is-completed")) {
					overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
					if (overviewData != null)
						overviewDataJson = new JSONObject(overviewData);
					counter = counter + 1;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
		}
		String country = null;
		if (!overviewDataJson.has("results") || overviewDataJson.getJSONArray("results").length() == 0) {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
			Object fileJsonData = parser.parse(new FileReader(fileJSon));
			org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
			for (int k = 0; k < countryJson.size(); k++) {
				org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
				if (countryObj.get("countryCode").toString().equalsIgnoreCase(juridiction)) {
					country = countryObj.get("country").toString();
				}
			}
			finalJson.put("country", country);
			finalJson.put("name", requestOrganisation);
			finalJson.put("title", requestOrganisation);

		}
		if (overviewDataJson.has("results")) {
			JSONArray results = overviewDataJson.getJSONArray("results");
			if (results.length() > 0) {
				result = results.getJSONObject(0);
				if (result.has("overview")) {
					JSONArray addressArray = null;
					JSONObject address = null;
					JSONObject overview = result.getJSONObject("overview");
					JSONArray sources = overview.names();
					if (sources != null && sources.length() > 0) {
						sourceData = overview.getJSONObject(sources.getString(0));
						if (sourceData.has("vcard:organization-name")) {
							finalJson.put("name", sourceData.getString("vcard:organization-name"));
							finalJson.put("title", sourceData.getString("vcard:organization-name"));
						}
						if (sourceData.has("mdaas:RegisteredAddress")) {
							if (sourceData.get("mdaas:RegisteredAddress") instanceof JSONObject)
								address = sourceData.getJSONObject("mdaas:RegisteredAddress");
							else
								addressArray = sourceData.getJSONArray("mdaas:RegisteredAddress");
							if (addressArray != null && addressArray.length() > 0) {
								address = addressArray.getJSONObject(0);
							}
							if (address != null && address.has("country")) {
								country = address.getString("country");
							}
							finalJson.put("country", country);
						}
					}
				}
			}
		}
		finalJson.put("id", "p00");
		finalJson.put("totalPercentage", 100);
		finalJson.put("indirectPercentage", 100);
		finalJson.put("identifier", identifier);
		finalJson.put("entity_id", "orgChartmainEntity");
		//finalJson.put("source_evidence", "Annual Report");
		finalJson.put("entity_type", "organization");
		if (sourceData != null)
			finalJson.put("basic", sourceData);
		finalJson.put("jurisdiction", juridiction);
		finalJson.put("parents", new JSONArray());
		finalJson.put("status", "inprogress");
		finalArray.put(finalJson);
		writeFile(file, finalArray);
		new ArrayList<>().add(finalJson);
		List<JSONObject> lewiseData = new ArrayList<>();
		lewiseData.add(finalJson);
		refList.add(lewiseData);
		if(isScreeningRequired){
			executor.submit(
					new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 0, true, new AdvanceSearchServiceImpl(),
							CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes, startDate, endDate,BE_ENTITY_API_KEY));
		}else{
			finalArray.getJSONObject(0).put("screeningFlag", true);
		}
		if (isSubsidiariesRequired) {
			executor.submit(new MultiSourceSubsidiaries(identifier, finalArray, noOfSubsidiaries, maxSubsidiarielevel,
					0, finalJson.getString("name"), juridiction, null, new AdvanceSearchServiceImpl(),
					BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes, startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
		} else {
			finalArray.getJSONObject(0).put("subsidiaries", new JSONArray());
		}
		executor.submit(
				new OfficershipScreening(finalArray, identifier, juridiction, SCREENING_URL, BIGDATA_MULTISOURCE_URL,
						new AdvanceSearchServiceImpl(), country, file, personTypes, pepTypes, startDate, endDate,isScreeningRequired,BE_ENTITY_API_KEY,CLIENT_ID));

		String url=buildMultiSourceUrl(identifier, "shareholders", null, userId, null,null,sourceType);
		String response=getDataInAsynMode(url);
		if (response != null) {
			List<JSONObject> levelData = new ArrayList<>();
			JSONObject json = new JSONObject(response);
			if (json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				if (results != null && results.length() > 0) {
					JSONObject shareHolderResult = results.getJSONObject(0);
					if (shareHolderResult.has("shareholders")) {
						JSONObject shareholders = shareHolderResult.getJSONObject("shareholders");
						JSONArray sources = shareholders.names();
						if (sources != null && sources.length() > 0) {
							JSONArray shareholderArray = new JSONArray();
							if (otherSource == null)
								shareholderArray = shareholders.getJSONArray(sources.getString(0));
							else
								shareholderArray = shareholders.getJSONArray(otherSource);
							if (shareholderArray.length() == 0)
								shareholderArray = shareholders.getJSONArray(sources.getString(0));
							int count = 0;
							for (int i = 0; i < shareholderArray.length(); i++) {
								String entity_type = null;
								String entityType = null;
								String organizationName = null;
								String shareHolderJurisdiction = null;
								JSONObject finalShareHolder = new JSONObject();
								JSONObject shareHolder = shareholderArray.getJSONObject(i);
								JSONObject basicJson = new JSONObject();
								if (shareHolder != null && shareHolder.has("basic")) {
									basicJson = shareHolder.getJSONObject("basic");
									if (basicJson.has("vcard:organization-name")) {
										organizationName = basicJson.getString("vcard:organization-name");
										finalShareHolder.put("name", organizationName);
										finalShareHolder.put("title", organizationName);
									}
									if (basicJson.has("isDomiciledIn")) {
										shareHolderJurisdiction = basicJson.getString("isDomiciledIn");
										finalShareHolder.put("jurisdiction", shareHolderJurisdiction);
									} else {
										finalShareHolder.put("jurisdiction", juridiction);
									}
									finalShareHolder.put("basic", basicJson);
								}
								Double totalPercentage = 0.0;
								Double indirectPercentage = 0.0;
								JSONObject relation = new JSONObject();
								if (shareHolder != null && shareHolder.has("relation")) {
									relation = shareHolder.getJSONObject("relation");
								} else if (basicJson != null && basicJson.has("relation")) {
									relation = basicJson.getJSONObject("relation");
								}
								if (relation.has("totalPercentage")) {
									String percentageInString = null;
									if (relation.get("totalPercentage") instanceof Double) {
										totalPercentage = relation.getDouble("totalPercentage");
									} else {
										percentageInString = relation.getString("totalPercentage");
										if (percentageInString != null) {
											String[] percentageArray = percentageInString.split(",");
											if (percentageArray != null && percentageArray.length > 0) {
												String percentage = percentageArray[0].trim();
												if (percentage.matches("[0-9]+")) {
													totalPercentage = Double.valueOf(percentage);
												}

											}
										}
									}
									indirectPercentage = totalPercentage;
								}
								// }
								if (indirectPercentage > lowRange && indirectPercentage <= highRange) {
									count = count + 1;

									if (shareHolder != null && shareHolder.has("entity_type")) {
										entity_type = shareHolder.getString("entity_type");
										if (entity_type != null && personTypes.contains(entity_type))
											entityType = "person";
										else
											entityType = "organization";
									}
									finalShareHolder.put("entity_type", entityType);
									String shareHolderIdentifier = null;
									if (shareHolder.has("@sourceReferenceID")) {
										shareHolderIdentifier = shareHolder.getString("@sourceReferenceID");
									} else {
										shareHolderIdentifier = "";
									}
									finalShareHolder.put("identifier", shareHolderIdentifier);
									finalShareHolder.put("totalPercentage", totalPercentage);
									finalShareHolder.put("indirectPercentage", totalPercentage);
									finalShareHolder.put("source_evidence_url", url);
									finalShareHolder.put("source_evidence", sources.getString(0));
									finalShareHolder.put("id", "p1" + count);
									finalShareHolder.put("level", 1);
									finalShareHolder.put("entity_id", "orgChartParentEntity");
									finalShareHolder.put("parents", new JSONArray());
									finalShareHolder.put("entity_id", "orgChartParentEntity");
									finalArray.put(finalShareHolder);
									levelData.add(finalShareHolder);
									finalArray.getJSONObject(0).getJSONArray("parents").put("p1" + count);
									if (isSubsidiariesRequired) {
										executor.submit(new MultiSourceSubsidiaries(identifier, finalArray,
												noOfSubsidiaries, maxSubsidiarielevel, count, organizationName,
												shareHolderJurisdiction, null, new AdvanceSearchServiceImpl(),
												BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes,
												startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
									} else {
										finalArray.getJSONObject(count).put("subsidiaries", new JSONArray());
									}
								}
							}
							refList.add(levelData);
							if (levelData.size() > 0 && refList.size() == 2 && isScreeningRequired) {
								executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 1, true,
										new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes,
										pepTypes, startDate, endDate,BE_ENTITY_API_KEY));
							}else{
								if (refList.size() == 2) {
									for (JSONObject jsonObject : refList.get(1)) {
										jsonObject.put("screeningFlag", true);
									}
								}
							}
						}
					}
				}
			}
		}
		for (int j = 0; j < finalArray.length(); j++) {
			while ((!finalArray.getJSONObject(j).has("subsidiaries"))
					|| (finalArray.getJSONObject(j).has("subsidiaries")
							&& finalArray.getJSONObject(j).getJSONArray("subsidiaries").length() > 0
							&& finalArray.getJSONObject(j).getJSONArray("subsidiaries")
							.getJSONObject(
									finalArray.getJSONObject(j).getJSONArray("subsidiaries").length() - 1)
							.getString("status").equals("pending"))
					|| (!finalArray.getJSONObject(0).has("officership"))
					|| (finalArray.getJSONObject(0).has("officership")
							&& finalArray.getJSONObject(0).getJSONArray("officership").length() > 0
							&& finalArray.getJSONObject(0).getJSONArray("officership")
							.getJSONObject(
									finalArray.getJSONObject(0).getJSONArray("officership").length() - 1)
							.getString("status").equals("pending"))
					|| (!finalArray.getJSONObject(j).has("screeningFlag"))) {
				finalArray.getJSONObject(finalArray.length() - 1).put("status", "inprogress");
				Thread.sleep(10000);
				writeFile(file, finalArray);
			}
		}
		finalArray.getJSONObject(finalArray.length() - 1).put("status", "completed");
		writeFile(file, finalArray);
	}

	private void checkChild(JSONObject map, JSONObject prevJsonObject, JSONObject mapJson, Integer level, int j,
			List<List<JSONObject>> allShareholders) {
		if (map.has(prevJsonObject.getString("id"))) {
			mapJson.put("child", prevJsonObject.getString("id"));
		} else {
			String child = prevJsonObject.getString("child");
			for (List<JSONObject> list : allShareholders) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject json = list.get(i);
					if (json.getString("id").equalsIgnoreCase(child))
						checkChild(map, json, mapJson, level - 1, j, allShareholders);
				}
			}
		}

	}

	private void getFactualControl(JSONArray array, JSONObject persons) {
		for (int i = 0; i < array.length(); i++) {
			if (persons.has(array.getJSONObject(i).getString("identifier"))) {
				boolean isUbo = false;
				double indirectPercentage = array.getJSONObject(i).getDouble("indirectPercentage");
				if (indirectPercentage < 25 && array.getJSONObject(i).getDouble("totalPercentage") > 50) {
					String child = array.getJSONObject(i).getString("child");
					for (int j = 0; j < array.length(); j++) {
						if (array.getJSONObject(j).getString("id").equalsIgnoreCase(child)) {
							int presentLevel = array.getJSONObject(j).getInt("level");
							if (presentLevel != 0) {
								double totalPercentage = array.getJSONObject(j).getDouble("totalPercentage");
								child = array.getJSONObject(j).getString("child");
								while (totalPercentage >= 50 && presentLevel > 0) {
									for (int k = 0; k < array.length(); k++) {
										if (array.getJSONObject(k).getString("id").equalsIgnoreCase(child)) {
											totalPercentage = array.getJSONObject(k).getDouble("totalPercentage");
											child = array.getJSONObject(k).getString("child");
											presentLevel = array.getJSONObject(k).getInt("level");
											if (presentLevel == 0)
												isUbo = true;
										}
									}
								}
							} else {
								isUbo = true;
							}
						}
					}
				} else if (indirectPercentage >= 25) {
					isUbo = true;
				}
				array.getJSONObject(i).put("isUbo", isUbo);
			}
		}

	}

	@Override
	public boolean deleteFiles(String path) throws IOException {
		boolean flag = true;
		if (path == null) {
			FileUtils.cleanDirectory(new File(CORPORATE_STRUCTURE_LOCATION));
		} else {
			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			flag = file.delete();
		}
		return flag;
	}

	
	@Override
	public String getScreeningStatus() throws Exception {
		JSONObject json=new JSONObject();
		String url = BIG_DATA_S3_DOC_URL;
		try{
			url = url + "/sources?";
			url=url+"&client_id="+CLIENT_ID;

			String serverResponse[]=ServiceCallHelper.getDataFromServer(url);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				String response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					JSONObject jsonResponse = new JSONObject(response);
					if (jsonResponse != null && (((jsonResponse.has("screening") &&
							!jsonResponse.getString("screening").equalsIgnoreCase("none"))
							||jsonResponse.has("message")))) {
						json.put("isScreeningEnabled", true);//If Sources configurations not found or screening is not none then enabled
					}else {
						json.put("isScreeningEnabled", false);
					}
				}else {
					json.put("isScreeningEnabled", false);
				}
			}else {
				json.put("isScreeningEnabled", false);
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	
	@Override
	public String getAnnualReturnsLink(String documentsLink) throws Exception {
		String finalUrl = null;
		List<JSONObject> mergedData = new ArrayList<JSONObject>();
		if (documentsLink != null) {
			String documentsString = getDataInAsynMode(documentsLink);
			if (documentsString != null) {
				JSONObject documentsJson = new JSONObject(documentsString);
				if (documentsJson.has("results")) {
					JSONArray results = documentsJson.getJSONArray("results");
					if (results.length() > 0) {
						JSONObject resultsJson = results.getJSONObject(0);
						if (resultsJson.has("documents") && null !=resultsJson.get("documents") && resultsJson.get("documents") instanceof JSONObject ) {
							JSONObject documents = resultsJson.getJSONObject("documents");
							JSONArray sources = documents.names();
							if (sources != null && sources.length() > 0) {
								for (int i = 0; i < sources.length(); i++) {
									JSONArray documentsArray = new JSONArray();
									if (documents.get(sources.getString(i)) instanceof JSONArray)
										documentsArray = documents.getJSONArray(sources.getString(i));
									for (int j = 0; j < documentsArray.length(); j++) {
										JSONObject document =new JSONObject();
										if(documentsArray.get(j) instanceof JSONObject)
											document = documentsArray.getJSONObject(j);
										if (document.has("description")) {
											String description = document.getString("description");
											if (description.startsWith("Annual return"))
												mergedData.add(documentsArray.getJSONObject(j));
										}
									}
								}
							}
							Collections.sort(mergedData, new DateComparator());
						}
					}
				}
			}
		}
		JSONObject finalJson = null;
		if (mergedData.size() > 0)
			finalJson = mergedData.get(0);
		if (finalJson != null) {
			if (finalJson.has("url"))
				finalUrl = finalJson.getString("url");
			if (finalJson.has("Primary_source_url"))
				finalUrl = finalJson.getString("Primary_source_url");
		}
		return finalUrl;
	}

	public void whenShareholderDataEmpty(String identifier, String requestOrganisation, String juridiction,
			Integer lowRange, Integer highRange, Integer noOfSubsidiaries, Integer maxSubsidiarielevel, File file,
			String annualReportLink, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,
			ThreadPoolExecutor executor, Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate, JSONObject sourceInfo, String bstUrl, String path, boolean isScreeningRequired, boolean isNoData, String source, JSONArray array, JSONArray sources2, String errorMsg,String sourceType)
					throws Exception {
		boolean isRollback=false;
		//JSONArray finalArray = new JSONArray();
		List<List<JSONObject>> refList = new ArrayList<List<JSONObject>>();
		JSONObject finalJson = new JSONObject();
		JSONObject sourceData = null;
		String overviewData = null;
		JSONObject result = null;
		JSONObject overviewDataJson = new JSONObject();
		try {
			String url = buildMultiSourceUrl(identifier, "overview", null, null, null,null,sourceType);
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(url);
			overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
			if (overviewData != null)
				overviewDataJson = new JSONObject(overviewData);
			if (overviewDataJson.has("is-completed")) {
				int counter = 0;
				while (overviewDataJson.has("is-completed") && !overviewDataJson.has("is-completed")) {
					overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
					if (overviewData != null)
						overviewDataJson = new JSONObject(overviewData);
					counter = counter + 1;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
		}
		String country = null;
		if (!overviewDataJson.has("results") || overviewDataJson.getJSONArray("results").length() == 0) {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
			Object fileJsonData = parser.parse(new FileReader(fileJSon));
			org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
			for (int k = 0; k < countryJson.size(); k++) {
				org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
				if (countryObj.get("countryCode").toString().equalsIgnoreCase(juridiction)) {
					country = countryObj.get("country").toString();
				}
			}
			finalJson.put("country", country);
			finalJson.put("name", requestOrganisation);
			finalJson.put("title", requestOrganisation);

		}
		if (overviewDataJson.has("results")) {
			JSONArray results = overviewDataJson.getJSONArray("results");
			if (results.length() > 0) {
				result = results.getJSONObject(0);
				if (result.has("overview")) {
					JSONArray addressArray = null;
					JSONObject address = null;
					JSONObject overview = result.getJSONObject("overview");
					JSONArray sources = overview.names();
					if (sources != null && sources.length() > 0) {
						sourceData = overview.getJSONObject(sources.getString(0));
						if (sourceData.has("vcard:organization-name")) {
							finalJson.put("name", sourceData.getString("vcard:organization-name"));
							finalJson.put("title", sourceData.getString("vcard:organization-name"));
						}
						if (sourceData.has("mdaas:RegisteredAddress")) {
							if (sourceData.get("mdaas:RegisteredAddress") instanceof JSONObject)
								address = sourceData.getJSONObject("mdaas:RegisteredAddress");
							else
								addressArray = sourceData.getJSONArray("mdaas:RegisteredAddress");
							if (addressArray != null && addressArray.length() > 0) {
								address = addressArray.getJSONObject(0);
							}
							if (address != null && address.has("country")) {
								country = address.getString("country");
							}
							finalJson.put("country", country);
						}
					}
				}
			}
		}
		finalJson.put("id", "p00");
		finalJson.put("totalPercentage", 100);
		finalJson.put("indirectPercentage", 100);
		finalJson.put("identifier", identifier);
		finalJson.put("sources", sources2);
		finalJson.put("entity_id", "orgChartmainEntity");
		if(errorMsg!=null) {
			finalJson.put("is-error", true);
			finalJson.put("error", errorMsg);
		}
		else {
			finalJson.put("is-error", false);
		}
		if(source!=null && source.equalsIgnoreCase("companyhouse.co.uk")) {
			finalJson.put("source_evidence", "Annual Report");
			//sources2.put("companyhouse.co.uk");
			//finalJson.put("source_evidence", "companyhouse.co.uk");
		}
		else if(source!=null )
			finalJson.put("source_evidence", source);
		finalJson.put("entity_type", "organization");
		if (sourceData != null)
			finalJson.put("basic", sourceData);
		finalJson.put("jurisdiction", juridiction);
		finalJson.put("parents", new JSONArray());
		finalJson.put("status", "inprogress");
		array.put(finalJson);
		writeFile(file, array);
		new ArrayList<>().add(finalJson);
		List<JSONObject> lewiseData = new ArrayList<>();
		lewiseData.add(finalJson);
		refList.add(lewiseData);
		if(isScreeningRequired){
			executor.submit(
					new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 0, true, new AdvanceSearchServiceImpl(),
							CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes, startDate, endDate,BE_ENTITY_API_KEY));
		}else{
			array.getJSONObject(0).put("screeningFlag", true);
		}
		if (isSubsidiariesRequired) {
			executor.submit(new MultiSourceSubsidiaries(identifier, array, noOfSubsidiaries, maxSubsidiarielevel,
					0, finalJson.getString("name"), juridiction, null, new AdvanceSearchServiceImpl(),
					BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes, startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
		} else {
			array.getJSONObject(0).put("subsidiaries", new JSONArray());
		}
		/*String 
String highCredibilitySource=getKeyManagementSource(keys);
		List<EntityOfficerInfoDto> listOfOfficer= entityOfficerInfoService.getEntityOfficerList(identifier);
		executor.submit(
				new OfficershipScreening(array, identifier, juridiction, SCREENING_URL, BIGDATA_MULTISOURCE_URL,
						new AdvanceSearchServiceImpl(), country, file, personTypes, pepTypes, startDate, endDate,isScreeningRequired,BE_ENTITY_API_KEY,CLIENT_ID,highCredibilitySource,listOfOfficer));
		*/int tempCount = 1;
		boolean flag = true;
		while ((flag && array.length() > 0) || (refList.get(refList.size() - 1).size() > 0 && !isRollback)) {
			List<JSONObject> levelwiseData = new ArrayList<>();
			List<JSONObject> previousShareholders = refList.get(refList.size() - 1);
			int count = 0;
			for (int j = 0; j < previousShareholders.size(); j++) {
				JSONObject previousShareholder = previousShareholders.get(j);
				String previousShareHolderName =null;
				if(previousShareholder.has("title"))
				 previousShareHolderName = previousShareholder.getString("title");
				String tempMultiSourceData = null;
				String presentDocumentLink = null;
				String selectEntity = null;
				if (array.length() == 1) {
					presentDocumentLink = annualReportLink;
				} else {
					try {
						if(previousShareHolderName!=null)
						tempMultiSourceData = getMultisourceData(previousShareHolderName, null, null, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (tempMultiSourceData != null) {
						JSONObject tempMultiSourceDataJson = new JSONObject(tempMultiSourceData);
						if (tempMultiSourceDataJson.has("results")) {
							JSONArray results = tempMultiSourceDataJson.getJSONArray("results");
							if (results.length() > 0) {
								JSONObject resultsJson = results.getJSONObject(0);
								if (resultsJson.has("links")) {
									JSONObject multiSourceLinks = resultsJson.getJSONObject("links");
									if (multiSourceLinks.has("documents")) {
										presentDocumentLink = multiSourceLinks.getString("documents");
									} else if (multiSourceLinks.has("select-entity")) {
										selectEntity = multiSourceLinks.getString("select-entity");
										String selectEntityResponse = getDataInAsynMode(selectEntity);
										if (selectEntityResponse != null) {
											JSONObject selectEntityJson = new JSONObject(selectEntityResponse);
											if (selectEntityJson.has("results")) {
												JSONArray entityArray = selectEntityJson.getJSONArray("results");
												if (entityArray.length() > 0) {
													JSONObject entityJson = entityArray.getJSONObject(0);
													if (entityJson.has("links")) {
														JSONObject entityLinks = entityJson.getJSONObject("links");
														if (entityLinks.has("documents")) {
															presentDocumentLink = entityLinks.getString("documents");
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if (presentDocumentLink != null) {
					String annualreturnsLink = getAnnualReturnsLink(presentDocumentLink);
					if (annualreturnsLink != null) {
						AnnualReturnResponseDto annualReturnResponseDto = entitySearchService
								.getAnnualReports(annualreturnsLink, identifier, requestOrganisation, userId);
						previousShareholder.put("source_evidence_url", annualreturnsLink);
						if (array.length() > 0 && array.length() == 1)
							array.getJSONObject(0).put("numberOfShares",
									annualReturnResponseDto.getTotalNumberOfShares());
						List<ShareholdingDto> shareholdingDto = annualReturnResponseDto.getShareholdingDto();
						if (shareholdingDto != null && shareholdingDto.size()>0) {
							JSONArray parents = new JSONArray();
							for (ShareholdingDto tempShareholdingDto : shareholdingDto) {
								JSONObject tempJson = new JSONObject();
								tempJson.put("id", "p" + refList.size() + count);
								tempJson.put("level", refList.size());
								tempJson.put("identifier", tempShareholdingDto.getIdentifier());
								tempJson.put("name", tempShareholdingDto.getShareHolderName());
								tempJson.put("title", tempShareholdingDto.getShareHolderName());
								tempJson.put("entity_id", "orgChartParentEntity");
								tempJson.put("totalPercentage", tempShareholdingDto.getTotalShareValuePer());
								tempJson.put("parents", new JSONArray());
								tempJson.put("jurisdiction", juridiction);
								double indirectPercentage = (previousShareholder.getDouble("indirectPercentage")
										* tempShareholdingDto.getTotalShareValuePer() / 100);
								if (indirectPercentage > lowRange && indirectPercentage <= highRange) {
									parents.put(tempJson.getString("id"));
									tempJson.put("source_evidence", "Annual Report");
									tempJson.put("entity_type", "organization");
									tempJson.put("numberOfShares", tempShareholdingDto.getNoOfShares());
									tempJson.put("indirectPercentage", indirectPercentage);
									tempJson.put("status", "inprogress");
									levelwiseData.add(tempJson);
									array.put(tempJson);
									previousShareholder.put("parents", parents);
									array.getJSONObject(array.length() - 1).put("status", "inprogress");
									writeFile(file, array);
									if (isSubsidiariesRequired) {
										executor.submit(new MultiSourceSubsidiaries(tempShareholdingDto.getIdentifier(),
												array, noOfSubsidiaries, maxSubsidiarielevel, tempCount,
												tempJson.getString("name"), juridiction, null,
												new AdvanceSearchServiceImpl(), BIGDATA_MULTISOURCE_URL, SCREENING_URL,
												executor, personTypes, pepTypes, startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
									} else {
										array.getJSONObject(tempCount).put("subsidiaries", new JSONArray());
									}
									tempCount = tempCount + 1;
									count = count + 1;
								}
							}
						} 
					}
				}
			}
			flag = false;
			refList.add(levelwiseData);
			if (levelwiseData.size() > 0 && isScreeningRequired) {
				executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, refList.size() - 1,
						true, new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes,
						startDate, endDate,BE_ENTITY_API_KEY));
			}else{
				for (JSONObject jsonObject : refList.get(refList.size()-1)) {
					jsonObject.put("screeningFlag", true);
				}
			}
		}
		if(array.length()==1 && !isNoData){
			int bstCredibility = 0;
			boolean isOtherSource = false;
			int otherSourceCredibility = 0;
			String otherSource = null;
			if (sourceInfo.has("BST") || sourceInfo.has("bst")) {
				bstCredibility = sourceInfo.getInt("BST");
				JSONArray sourcesArray = sourceInfo.names();
				if (sourcesArray != null) {
					for (int i = 0; i < sourcesArray.length(); i++) {
						if (!"BST".equalsIgnoreCase(sourcesArray.getString(i)) &&
								!"companyhouse.co.uk".equalsIgnoreCase(sourcesArray.getString(i))) {
							if (sourceInfo.getInt(sourcesArray.getString(i)) >= bstCredibility) {
								isOtherSource = true;
								otherSourceCredibility = sourceInfo.getInt(sourcesArray.getString(i));
								otherSource = sourcesArray.getString(i);
								break;
							}
						}
					}
				}
			}

			if ((bstUrl != null && !"".equalsIgnoreCase(bstUrl)) && !isOtherSource) {
				isRollback=true;
				HierarchyDto jsonString=new HierarchyDto();
				jsonString.setUrl(bstUrl);
				ownershipStructure(identifier, jsonString, maxSubsidiarielevel, lowRange, highRange, path, noOfSubsidiaries, maxSubsidiarielevel, requestOrganisation, juridiction, userId, isSubsidiariesRequired, startDate, endDate,true,null,null,sourceType);
			}/*else{
					getDataFromOtherSources(identifier, requestOrganisation, juridiction, lowRange, highRange, noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, executor, userId, isSubsidiariesRequired, startDate, endDate,otherSource,isScreeningRequired);
					isRollback=true;
				}*/
		}
		if(!isRollback){
			if (refList.get(refList.size() - 1).size() == 0) {
				for (int j = 0; j < array.length(); j++) {
					while ((!array.getJSONObject(j).has("subsidiaries"))
							|| (array.getJSONObject(j).has("subsidiaries")
									&& array.getJSONObject(j).getJSONArray("subsidiaries").length() > 0
									&& array.getJSONObject(j).getJSONArray("subsidiaries")
									.getJSONObject(
											array.getJSONObject(j).getJSONArray("subsidiaries").length() - 1)
									.getString("status").equals("pending"))
							/*|| (!array.getJSONObject(0).has("officership"))*/
							|| (array.getJSONObject(0).has("officership")
									&& array.getJSONObject(0).getJSONArray("officership").length() > 0
									&& array.getJSONObject(0).getJSONArray("officership")
									.getJSONObject(
											array.getJSONObject(0).getJSONArray("officership").length() - 1)
									.getString("status").equals("pending"))
							|| (!array.getJSONObject(j).has("screeningFlag"))) {
						array.getJSONObject(array.length() - 1).put("status", "inprogress");
						Thread.sleep(10000);
						writeFile(file, array);
					}
				}
				array.getJSONObject(array.length() - 1).put("status", "completed");

			} else {
				array.getJSONObject(array.length() - 1).put("status", "inprogress");
			}
			writeFile(file, array);
		}
	}

	@Override
	public String getMultisourceData(String query, String jurisdiction, String website, Long userId) throws Exception {
		String url = BIGDATA_MULTISOURCE_URL + "?name=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		if (jurisdiction != null)
			url = url + "&jurisdiction=" + URLEncoder.encode(jurisdiction, "UTF-8").replaceAll("\\+", "%20");
		if (website != null)
			url = url + "&website=" + URLEncoder.encode(website, "UTF-8").replaceAll("\\+", "%20");
		url=url+"&client_id="+CLIENT_ID;
		JSONObject json = new JSONObject();
		String response = getDataInAsynMode(url);
		if (response != null) {
			json = new JSONObject(response);
		}
		if (userId == null) {
			return response;
		} else {
			if (json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				for (int i = 0; i < results.length(); i++) {
					JSONObject result = results.getJSONObject(i);
					String identifier = null;
					if (result.has("identifier")) {
						identifier = result.getString("identifier");
					}
					if (result.has("overview")) {
						JSONObject overview = result.getJSONObject("overview");
						JSONArray sources = overview.names();
						for (int j = 0; j < sources.length(); j++) {
							JSONObject overviewJson = overview.getJSONObject(sources.getString(j));
							if (overviewJson.has("vcard:organization-name")) {
								String organizationName = overviewJson.getString("vcard:organization-name");
								AdvanceNewsFavourite advanceNewsFavourite = advanceNewsFavouriteDao
										.fetchadvanceNewsFavourite(identifier, organizationName, userId);
								if (advanceNewsFavourite != null) {
									overviewJson.accumulate("entitySiginificance", true);
								}
							}
							try{
								String category = sourcesService.getSourceCategoryBySourceName(sources.getString(j));
								if(category!=null){
									overviewJson.put("sourceCategory", category);
								}
							}catch (Exception e) {
								//e.printStackTrace();
							}
							
						}
					}
				}

			}
			return json.toString();
		}
	}

	@Override

	public String fetchLinkData(String identifier, String jurisdiction, String fields, String graph, Long userId,String sourceType) throws Exception {
		String response=null;
		String url = BIGDATA_MULTISOURCE_URL+"/";
		String finalFields = null;
		try{
			if ("finance_info".equals(fields)) {
				finalFields = "overview";
			} else {
				finalFields = fields;
			}
			if (identifier != null)
				url = url + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
			
			if (graph != null)
				url = url + "/" + graph;
			if (finalFields != null && sourceType!=null)
				url = url + "?fields=" + finalFields+"&"+sourceType+"="+URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
			url=url+"&client_id="+CLIENT_ID;

			response=getDataFromServer(identifier,jurisdiction, url, userId, fields);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	JSONArray keys=new JSONArray();

	public String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level)
			throws UnsupportedEncodingException {

		String url = BIGDATA_MULTISOURCE_URL;
		String finalFields = null;
		if ("finance_info".equals(fields)) {
			finalFields = "overview";
		} else {
			finalFields = fields;
		}
		if (identifier != null)
			url = url + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		;
		if (graph != null)
			url = url + "/" + graph;
		if (level != null)
			url = url + "/" + level;
		if (finalFields != null)
			url = url + "?fields=" + finalFields;
		if (clientId != null)
			url = url + "&client_id=" + clientId;
		return url;

	}
	

	private String getDataFromServer(String identifier, String jurisdiction, String url, Long userId, String fields) throws Exception {
		JSONObject json = new JSONObject();
		String response = getDataInAsynMode(url);
		if (response != null) {
			json = new JSONObject(response);
		}
		Long classificationId = null;
		List<ClassificationsDto> classificationsDtos = classificationsService.getClassifications();
		for (ClassificationsDto classificationsDto : classificationsDtos) {
			if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
				classificationId = classificationsDto.getClassificationId();
			}
		}
		List<SourcesDto> list = sourcesService.getSources(0, 0, null, classificationId, null, null, null, null,false);
		
		List<SourcesHideStatusDto> listHideStatusdto = sourcesHideStatusService.getSourceHideStatusByClassification(classificationId);
		Map<Long,SourcesHideStatusDto> hideStatusMap = new HashMap<Long,SourcesHideStatusDto>();
		for(SourcesHideStatusDto dto : listHideStatusdto){
			hideStatusMap.put(dto.getSourceId(), dto);
		}
		Map<String, SourcesDto> map = new HashMap<String, SourcesDto>();
		for (SourcesDto sourcesDto : list) {
			Long sourceId = sourcesDto.getSourceId();
			/*SourcesHideStatusDto sourcesHideStatusDto = sourcesHideStatusService.getSourceHideStatus(sourceId, classificationId);
			if(sourcesHideStatusDto != null &&sourcesHideStatusDto.getVisible()){
					map.put(sourcesDto.getSourceName(), sourcesDto);
			}*/
			if(hideStatusMap.containsKey(sourceId) && hideStatusMap.get(sourceId).getVisible()){
				map.put(sourcesDto.getSourceName(), sourcesDto);
			}
				//map.put(sourcesDto.getSourceName(), sourcesDto);
		}
		//
		String country = "";
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File fileJSon = new File(classLoader.getResource("CountryList.json").getFile());
		Object fileJsonData = parser.parse(new FileReader(fileJSon));
		org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;

		//Map<String, String> map = new HashMap<String, String>();
		for (int k = 0; k < countryJson.size(); k++) {
			org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
			if(countryObj.get("Code").toString().equalsIgnoreCase(jurisdiction)){
				country = countryObj.get("Name").toString();
			}
			//map.put(countryObj.get("Code").toString(), countryObj.get("Name").toString());
		}

		
		if(url.contains("fields=officership")){
			
			JSONArray responseArray = new JSONArray();
			JSONObject officership = new JSONObject();
			JSONParser parser1 = new JSONParser();
			ClassLoader classLoader1 = getClass().getClassLoader();
			File fileJSon1 = new File(classLoader1.getResource("BVD_countries.json").getFile());
			Object fileJsonData1 = parser1.parse(new FileReader(fileJSon1));
			org.json.simple.JSONArray countryJson1 = (org.json.simple.JSONArray) fileJsonData1;
			
			List<EntityOfficerInfoDto> listOfOfficer= entityOfficerInfoService.getEntityOfficerList(identifier);
			
			if (json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				if(results != null && results.length() > 0){
					JSONObject result = results.getJSONObject(0);
					if(result.has("officership") && result.get("officership")!=null && result.get("officership") instanceof JSONObject ){
						officership = result.getJSONObject("officership");
						JSONArray keys = officership.names();
						
						List<String> originalIds=new ArrayList<String>();
						JSONObject elementRef=new JSONObject();
						JSONArray elementObjects= new JSONArray();
						for (int i = 0; i < keys.length(); i++) {
							/// element source
							if (keys.getString(i).equalsIgnoreCase(clientId)) {
								JSONArray element = officership.getJSONArray(clientId);
								if (element != null && element.length() > 0) {
									for (int j = 0; j < element.length(); j++) {
										JSONObject elementobj = element.getJSONObject(j);
										if (elementobj.has("identifier")) {
											String element_Identifier = elementobj.getString("identifier");
											String str[] = element_Identifier.split("_");
											originalIds.add(str[0]);
											elementRef.put(str[0], elementobj);
											elementObjects.put(elementobj);
										}
									}
								}
							}
						}
						
						///
						// maintain a map of officer name and array of sources in which he is available
						Map<String, List<String>> officerSourceMap = new HashMap<String, List<String>>();
						// officer ship array for building response
						//JSONArray officersArray = new JSONArray();
						
						//Sources Loop
						int count = 0;
						for(int i = 0; i< keys.length(); i++){
							JSONArray officersFromSource = new JSONArray();
							if(officership.get(keys.getString(i)) instanceof JSONArray){
								officersFromSource = officership.getJSONArray(keys.getString(i));
							}else if(officership.get(keys.getString(i)) instanceof JSONObject) {
								officersFromSource.put(officership.get(keys.getString(i)));
							}
							if(officersFromSource != null){
								//officer Loop
								for(int j = 0; j < officersFromSource.length(); j++){
									JSONObject officerJson = officersFromSource.getJSONObject(j);
									
									List<String> sourceRef = new ArrayList<String>();
									
									if (officerJson.has("name")) {
										String name = officerJson.getString("name");
										Pattern p = Pattern.compile(
												"(Mr.|MR.|Dr.|mr.|DR.|dr.|ms.|Ms.|MS.|Miss.|Mrs.|mrs.|miss.|MR|mr|Mr|Dr|DR|dr|ms|Ms|MS|miss|Miss|Mrs|mrs)"
														+ "\\s|\\b");
										Matcher m = p.matcher(name);
										String nameWithoutSalutation = m.replaceAll("");
										officerJson.put("name", nameWithoutSalutation);
									}
									officerJson.put("entity_id", identifier + "off" + count);
									//officerJson.put("identifier", identifier + "off" + count);
									count++;									
									//
									sourceRef.add(keys.getString(i));
										
									if(officerJson.has("name")){

									//  check if map contains officer
										if(officerSourceMap.containsKey(officerJson.getString("name"))){
											List<String> sourceArray = officerSourceMap.get(officerJson.getString("name"));
											// check if current source is already in the source array in the map
											if(!sourceArray.contains(keys.getString(i))){
												sourceArray.add(keys.getString(i));
												officerSourceMap.put(officerJson.getString("name"), sourceArray);
											}
										}
										// add the new officer in map
										else{
											officerSourceMap.put(officerJson.getString("name"), sourceRef);
										}
									}
								}
							}
						}
						//check if officer exist in database
						if(listOfOfficer!=null && !listOfOfficer.isEmpty()) {
							for(EntityOfficerInfoDto officerDto: listOfOfficer) {
								if(officerSourceMap.containsKey(officerDto.getOfficerName())){
									if(officership.has(officerDto.getSource())){
									if(officership.get(officerDto.getSource()) instanceof JSONArray && officership.has(officerDto.getSource())){
										JSONArray officerArray = officership.getJSONArray(officerDto.getSource());
										
										for(int i = 0; i < officerArray.length(); i++){
											// get officer object and build the json object to send in response
											JSONObject officerObject = officerArray.getJSONObject(i);
											////
											// build officer objects if the status is active  
											if ((officerObject.has("status")
													&& officerObject.getString("status").equalsIgnoreCase("active")) || !officerObject.has("status")) {
												if(officerObject.has("name") 
														&& officerObject.getString("name").equalsIgnoreCase(officerDto.getOfficerName())){
														
													/*if (officerObject.has("name")) {
														String name = officerObject.getString("name");
														Pattern p = Pattern.compile(
																"(Mr.|MR.|Dr.|mr.|DR.|dr.|ms.|Ms.|MS.|Miss.|Mrs.|mrs.|miss.|MR|mr|Mr|Dr|DR|dr|ms|Ms|MS|miss|Miss|Mrs|mrs)"
																		+ "\\b");
														Matcher m = p.matcher(name);
														String nameWithoutSalutation = m.replaceAll("");
														officerObject.put("name", nameWithoutSalutation);
													}*/
													officerObject.put("name", officerDto.getOfficerName());
													
													officerObject.put("highCredibilitySource", officerDto.getSource());
													///
													if(officerObject.has("identifier")){
														officerObject.put("officerIdentifier", officerObject.getString("identifier"));
													}
													officerObject.put("information_provider", officerDto.getSource());
													officerObject.put("source", officerDto.getSource());
													
													officerObject.put("officerSources", officerSourceMap.get(officerDto.getOfficerName()));
													
													///set country and jurisdiction
													if (officerObject.has("address")) {
														String officerCountry = null;
														JSONObject address = null;
														if (officerObject.has("address")
																&& (officerObject.get("address") instanceof JSONObject))
															address = officerObject.getJSONObject("address");
														if (address != null && address.has("country")
																&& !"".equalsIgnoreCase(address.getString("country"))) {
															officerCountry = address.getString("country");
															for (int k = 0; k < countryJson1.size(); k++) {
																org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson1
																		.get(k);
																if (countryObj.get("country").toString()
																		.equalsIgnoreCase(officerCountry)) {
																	officerObject.put("jurisdiction",
																			countryObj.get("countryCode").toString());
																	officerObject.put("country", officerCountry);
																}
															}
														} else {
															officerObject.put("jurisdiction", jurisdiction);
															officerObject.put("country", country);

														}
													} else {
														officerObject.put("jurisdiction", jurisdiction);
														officerObject.put("country", country);
													}
													
													//officerObject.put("status", "pending");
														
													//officerObject.put("entity_id", identifier + "off" + count);
													//officerObject.put("identifier", identifier + "off" + count);
													
													////
													if (!officerDto.getSource().equals(clientId)) {
														if (originalIds != null && originalIds.size() > 0) {
															String str = officerObject.getString("officerIdentifier").toString().split("_")[0];
																if(originalIds.contains(str)) {
																	
																	JSONObject h=elementRef.getJSONObject(str);
																	String removingIdentifier=h.getString("identifier").toString().split("_")[0];
																	if(h.has("name")) {
																		officerObject.put("name", h.getString("name"));
																	}
																	if(h.has("date_of_birth")) {
																		officerObject.put("date_of_birth", h.getString("date_of_birth"));
																		
																	}
																	if(h.has("country_of_residence")) {
																		officerObject.put("country_of_residence", h.getString("country_of_residence"));
																	}
																	if(h.has("country")) {
																		officerObject.put("country", h.getString("country"));
																	}
																	if(h.has("jurisdiction")) {
																		officerObject.put("jurisdiction", h.getString("jurisdiction"));
																	}
																	if(h.has("officer_role")) {
																		officerObject.put("officer_role", h.getString("officer_role"));
																		
																	}
																	if(h.has("id")) {
																		officerObject.put("id", h.getString("id"));
																	}
																	if(h.has("classification")&& h.get("classification") instanceof String) {
																		officerObject.put("classification", h.getString("classification"));
																	}
																	if(h.has("classification")&& h.get("classification") instanceof JSONArray) {
																		officerObject.put("classification", h.getJSONArray("classification").toString());
																	}
																	if(h.has("sourceUrl")) {
																		officerObject.put("sourceUrl", h.getString("sourceUrl"));
																	}
																	if (h.has("customSource")) {
																		officerObject.put("source", h.getString("customSource"));
																	}
																if(h.has("from")) {
																	officerObject.put("from", h.getString("from"));
																	}
																	elementRef.remove(removingIdentifier);
																	originalIds.remove(removingIdentifier);
																}
																
														}
													}
													responseArray.put(officerObject);	
													officerSourceMap.remove(officerDto.getOfficerName());
												}
											}
											/*responseArray.put(officerObject);	
											officerSourceMap.remove(officerDto.getOfficerName());*/
										}
									}
									}else{
										JSONArray officerArray = officership.getJSONArray(clientId);
										
										for(int i = 0; i < officerArray.length(); i++){
											// get officer object and build the json object to send in response
											JSONObject officerObject = officerArray.getJSONObject(i);
											////
											// build officer objects if the status is active  
											if ((officerObject.has("status")
													&& officerObject.getString("status").equalsIgnoreCase("active")) || !officerObject.has("status")) {
												if(officerObject.has("name") 
														&& officerObject.getString("name").equalsIgnoreCase(officerDto.getOfficerName())){
													/*if (officerObject.has("name")) {
														String name = officerObject.getString("name");
														Pattern p = Pattern.compile(
																"(Mr.|MR.|Dr.|mr.|DR.|dr.|ms.|Ms.|MS.|Miss.|Mrs.|mrs.|miss.|MR|mr|Mr|Dr|DR|dr|ms|Ms|MS|miss|Miss|Mrs|mrs)"
																		+ "\\b");
														Matcher m = p.matcher(name);
														String nameWithoutSalutation = m.replaceAll("");
														officerObject.put("name", nameWithoutSalutation);
													}*/
													officerObject.put("name", officerDto.getOfficerName());
													
													officerObject.put("highCredibilitySource", officerDto.getSource());
													///
													if(officerObject.has("identifier")){
														officerObject.put("officerIdentifier", officerObject.getString("identifier"));
													}
													officerObject.put("information_provider", officerDto.getSource());
													officerObject.put("source", officerDto.getSource());
													
													officerObject.put("officerSources", officerSourceMap.get(officerDto.getOfficerName()));
													
													///set country and jurisdiction
													if (officerObject.has("address")) {
														String officerCountry = null;
														JSONObject address = null;
														if (officerObject.has("address")
																&& (officerObject.get("address") instanceof JSONObject))
															address = officerObject.getJSONObject("address");
														if (address != null && address.has("country")
																&& !"".equalsIgnoreCase(address.getString("country"))) {
															officerCountry = address.getString("country");
															for (int k = 0; k < countryJson1.size(); k++) {
																org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson1
																		.get(k);
																if (countryObj.get("country").toString()
																		.equalsIgnoreCase(officerCountry)) {
																	officerObject.put("jurisdiction",
																			countryObj.get("countryCode").toString());
																	officerObject.put("country", officerCountry);
																}
															}
														} else {
															officerObject.put("jurisdiction", jurisdiction);
															officerObject.put("country", country);

														}
													} else {
														officerObject.put("jurisdiction", jurisdiction);
														officerObject.put("country", country);
													}
													
													//officerObject.put("status", "pending");
														
													//officerObject.put("entity_id", identifier + "off" + count);
													//officerObject.put("identifier", identifier + "off" + count);
													
													////
													if (!officerDto.getSource().equals(clientId)) {
														if (originalIds != null && originalIds.size() > 0) {
															String str = officerObject.getString("officerIdentifier").toString().split("_")[0];
																if(originalIds.contains(str)) {
																	
																	JSONObject h=elementRef.getJSONObject(str);
																	String removingIdentifier=h.getString("identifier").toString().split("_")[0];
																	if(h.has("name")) {
																		officerObject.put("name", h.getString("name"));
																	}
																	if(h.has("date_of_birth")) {
																		officerObject.put("date_of_birth", h.getString("date_of_birth"));
																		
																	}
																	if(h.has("country_of_residence")) {
																		officerObject.put("country_of_residence", h.getString("country_of_residence"));
																	}
																	if(h.has("country")) {
																		officerObject.put("country", h.getString("country"));
																	}
																	if(h.has("jurisdiction")) {
																		officerObject.put("jurisdiction", h.getString("jurisdiction"));
																	}
																	if(h.has("officer_role")) {
																		officerObject.put("officer_role", h.getString("officer_role"));
																		
																	}
																	if(h.has("id")) {
																		officerObject.put("id", h.getString("id"));
																	}
																	if(h.has("classification")&& h.get("classification") instanceof String) {
																		officerObject.put("classification", h.getString("classification"));
																	}
																	if(h.has("classification")&& h.get("classification") instanceof JSONArray) {
																		officerObject.put("classification", h.getJSONArray("classification").toString());
																	}
																	if(h.has("sourceUrl")) {
																		officerObject.put("sourceUrl", h.getString("sourceUrl"));
																	}
																	if (h.has("customSource")) {
																		officerObject.put("source", h.getString("customSource"));
																	}
																if(h.has("from")) {
																	officerObject.put("from", h.getString("from"));
																	}
																	elementRef.remove(removingIdentifier);
																	originalIds.remove(removingIdentifier);
																
																	responseArray.put(officerObject);	
																	officerSourceMap.remove(officerDto.getOfficerName());	
																}
																
														}
													}
													/*responseArray.put(officerObject);	
													officerSourceMap.remove(officerDto.getOfficerName());*/
												}
											}
											/*responseArray.put(officerObject);	
											officerSourceMap.remove(officerDto.getOfficerName());*/
										}
									}
									
								}
							}
						}
						
						JSONObject sourceCredibilities = getKeyManagementSource(keys);
						
						for(Entry<String, List<String>> officerFromMap : officerSourceMap.entrySet()){
							String officerName = officerFromMap.getKey();
							String highCredibilitySource = null;
							
							List<String> officerSourceArray = officerFromMap.getValue();
							if(officerSourceArray.contains(clientId)){
								officerSourceArray.remove(clientId);
							}
							if(officerSourceArray.size() == 0){
								 highCredibilitySource = clientId;
							}else{
								 highCredibilitySource = getHighCredSource(sourceCredibilities, officerSourceArray);
							}
							JSONArray officerArray= new JSONArray();
							if (officership.get(highCredibilitySource) instanceof JSONArray) {
								 officerArray = officership.getJSONArray(highCredibilitySource);
							} else if(officership.get(highCredibilitySource) instanceof JSONObject) {
								officerArray.put(officership.getJSONObject(highCredibilitySource));
							}
							for(int i = 0; i < officerArray.length(); i++){
								// get officer object and build the json object to send in response
								JSONObject officerObject = officerArray.getJSONObject(i);
								////
								// build officer objects if the status is active  
								if ((officerObject.has("status")
										&& officerObject.getString("status").equalsIgnoreCase("active")) ||  !officerObject.has("status")) {
									if(officerObject.has("name") 
											&& officerObject.getString("name").equalsIgnoreCase(officerName)){
										
										if (officerObject.has("name")) {
											String name = officerObject.getString("name");
											Pattern p = Pattern.compile(
													"(Mr.|MR.|Dr.|mr.|DR.|dr.|ms.|Ms.|MS.|Miss.|Mrs.|mrs.|miss.|MR|mr|Mr|Dr|DR|dr|ms|Ms|MS|miss|Miss|Mrs|mrs)"
															+ "\\b");
											Matcher m = p.matcher(name);
											String nameWithoutSalutation = m.replaceAll("");
											officerObject.put("name", nameWithoutSalutation);
										}
										
										officerObject.put("highCredibilitySource", highCredibilitySource);
										///
										if(officerObject.has("identifier")){
											officerObject.put("officerIdentifier", officerObject.getString("identifier"));
										}
										officerObject.put("information_provider", highCredibilitySource);
										officerObject.put("source", highCredibilitySource);
										
										officerObject.put("officerSources", officerSourceMap.get(officerName));
										
										///set country and jurisdiction
										if (officerObject.has("address")) {
											String officerCountry = null;
											JSONObject address = null;
											if (officerObject.has("address")
													&& (officerObject.get("address") instanceof JSONObject))
												address = officerObject.getJSONObject("address");
											if (address != null && address.has("country")
													&& !"".equalsIgnoreCase(address.getString("country"))) {
												officerCountry = address.getString("country");
												for (int k = 0; k < countryJson1.size(); k++) {
													org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson1
															.get(k);
													if (countryObj.get("country").toString()
															.equalsIgnoreCase(officerCountry)) {
														officerObject.put("jurisdiction",
																countryObj.get("countryCode").toString());
														officerObject.put("country", officerCountry);
													}
												}
											} else {
												officerObject.put("jurisdiction", jurisdiction);
												officerObject.put("country", country);

											}
										} else {
											officerObject.put("jurisdiction", jurisdiction);
											officerObject.put("country", country);
										}
										
										//officerObject.put("status", "pending");
											
										//officerObject.put("entity_id", identifier + "off" + count);
										//officerObject.put("identifier", identifier + "off" + count);
										
										////
										if (!highCredibilitySource.equals(clientId)) {
											if (originalIds != null && originalIds.size() > 0) {
												String str = officerObject.getString("officerIdentifier").toString().split("_")[0];
													if(originalIds.contains(str)) {
														
														JSONObject h=elementRef.getJSONObject(str);
														String removingIdentifier=h.getString("identifier").toString().split("_")[0];
														if(h.has("name")) {
															officerObject.put("name", h.getString("name"));
														}
														if(h.has("date_of_birth")) {
															officerObject.put("date_of_birth", h.getString("date_of_birth"));
															
														}
														if(h.has("country_of_residence")) {
															officerObject.put("country_of_residence", h.getString("country_of_residence"));
														}
														if(h.has("country")) {
															officerObject.put("country", h.getString("country"));
														}
														if(h.has("jurisdiction")) {
															officerObject.put("jurisdiction", h.getString("jurisdiction"));
														}
														if(h.has("officer_role")) {
															officerObject.put("officer_role", h.getString("officer_role"));
															
														}
														if(h.has("id")) {
															officerObject.put("id", h.getString("id"));
														}
														if(h.has("classification")&& h.get("classification") instanceof String) {
															officerObject.put("classification", h.getString("classification"));
														}
														if(h.has("classification")&& h.get("classification") instanceof JSONArray) {
															officerObject.put("classification", h.getJSONArray("classification").toString());
														}
														if(h.has("sourceUrl")) {
															officerObject.put("sourceUrl", h.getString("sourceUrl"));
														}
														if (h.has("customSource")) {
															officerObject.put("source", h.getString("customSource"));
														}
													if(h.has("from")) {
														officerObject.put("from", h.getString("from"));
														}
														elementRef.remove(removingIdentifier);
														originalIds.remove(removingIdentifier);
													}
													
											}
										}
										responseArray.put(officerObject);
									}
								}
							}
						}
						if (elementRef != null) {
							JSONArray names = elementRef.names();
							if (names != null && names.length() > 0) {
								for (int l = 0; l < names.length(); l++) {
									JSONObject officerJson = elementRef.getJSONObject(names.getString(l));
									if (officerJson.has("address")) {
										String officerCountry = null;
										JSONObject address = null;
										if (officerJson.has("address")
												&& (officerJson.get("address") instanceof JSONObject))
											address = officerJson.getJSONObject("address");
										if (address != null && address.has("country")
												&& !"".equalsIgnoreCase(address.getString("country"))) {
											officerCountry = address.getString("country");
											for (int k = 0; k < countryJson1.size(); k++) {
												org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson1
														.get(k);
												if (countryObj.get("country").toString()
														.equalsIgnoreCase(officerCountry)) {
													officerJson.put("jurisdiction",
															countryObj.get("countryCode").toString());
													officerJson.put("country", officerCountry);
												}
											}
										} else {
											officerJson.put("jurisdiction", jurisdiction);
											officerJson.put("country", country);

										}
									} else {
										officerJson.put("jurisdiction", jurisdiction);
										officerJson.put("country", country);
									}
									responseArray.put(officerJson);
									/*JSONObject jsonToSent = new JSONObject();
									jsonToSent.put("entity_id", identifier + "off" + count);
									if (officerJson.has("jurisdiction"))
										jsonToSent.put("jurisdiction", officerJson.getString("jurisdiction"));
									if (officerJson.has("name"))
										jsonToSent.put("names", new JSONArray().put(officerJson.getString("name")));
									if (startDate != null)
										jsonToSent.put("start_date", startDate);
									if (endDate != null)
										jsonToSent.put("end_date", endDate);
									if (officerJson.has("type")
											&& ("person".equalsIgnoreCase(officerJson.getString("type"))
													|| "individual".equalsIgnoreCase(officerJson.getString("type"))))
										personEntities.put(jsonToSent);
									else if (!officerJson.has("type"))
										personEntities.put(jsonToSent);
									else
										orgEntities.put(jsonToSent);*/
								}
							}
						}
						
					}
				}
				
			}
			
			return officership.put("officershipInfo", responseArray).toString();
		}

		// overview Api call
		if (url.contains("fields=overview")) {
			if (json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				if (results != null && results.length() > 0) {
					JSONObject result = results.getJSONObject(0);
					String id = null;
					if (result.has("identifier")) {
						id = result.getString("identifier");
					}
					if ("finance_info".equalsIgnoreCase(fields)) {
						if (result.has("links")) {
							JSONObject links = result.getJSONObject("links");
							JSONObject finalFinanceInfo = new JSONObject();
							if (links.has("finance_info")) {
								String financeLink = links.getString("finance_info");
								String financeInfo = mergeFinancialInformation(financeLink, map);
								finalFinanceInfo.put("Finanace_Information", new JSONObject(financeInfo));
							} else {
								result.put("finance_info", new JSONArray());
							}
							if (links.has("shareholders")) {
								String shareholderLinks = links.getString("shareholders");
								try {
									String shareholders = mergeShareholderData(shareholderLinks);
									finalFinanceInfo.put("shareholders", new JSONArray(shareholders));
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.put("shareholders", new JSONArray());
							}
							result.put("finance_info", finalFinanceInfo);
						}
					}
					if ("overview".equalsIgnoreCase(fields)) {
						JSONObject overview = result.getJSONObject("overview");
						JSONArray sources = overview.names();
						JSONObject credibilityJson=new JSONObject();
						if(sources!=null){
							for (int i = 0; i < sources.length(); i++) {
								JSONObject jsonObjectOverView = overview.getJSONObject(sources.getString(i));
								if (jsonObjectOverView.has("bst:registryURI")
										&& !"".equalsIgnoreCase(jsonObjectOverView.getString("bst:registryURI"))){
									credibilityJson.put(sources.getString(i), jsonObjectOverView.getString("bst:registryURI"));
								}else{
									credibilityJson.put(sources.getString(i), sources.getString(i));
								}
							}
						}
						JSONObject jsonObject = new JSONObject();
						List<EntityAttributes> entityAttributes = entityAttributeService.getAttributeList(identifier,
								null);
						for (EntityAttributes entityAttribute : entityAttributes) {
							JSONObject jsonObjectOverView = new JSONObject();
							jsonObjectOverView.put(entityAttribute.getSourceSchema(), entityAttribute.getValue());
							jsonObjectOverView.put("source", entityAttribute.getSource());
							jsonObjectOverView.put("user",
									entityAttribute.getUser().getFirstName() + entityAttribute.getUser().getLastName());
							if (entityAttribute.getModifiedDate() != null)
								jsonObjectOverView.put("modifiedOn", entityAttribute.getModifiedDate().toString());
							else
								jsonObjectOverView.put("modifiedOn", entityAttribute.getCreatedDate().toString());
							if (entityAttribute.getPreValue() != null)
								jsonObjectOverView.put("preValue", entityAttribute.getPreValue());
							if (entityAttribute.getUserValue() != null)
								jsonObjectOverView.put("userValue", entityAttribute.getUserValue());
							if (entityAttribute.getSourceUrl() != null)
								jsonObjectOverView.put("sourceUrl", entityAttribute.getSourceUrl());
							if (entityAttribute.getIsUserData()!=null && entityAttribute.getIsUserData().booleanValue())
								jsonObjectOverView.put("isUserValue", true);
							else if (entityAttribute.getIsUserData()!=null && !entityAttribute.getIsUserData().booleanValue())
								jsonObjectOverView.put("isUserValue", false);
							else
								jsonObjectOverView.put("isUserValue", false);
							if (entityAttribute.getPublishedDate() != null)
								jsonObjectOverView.put("publishedDate", entityAttribute.getPublishedDate().toString());
							if (entityAttribute.getUserSource() != null)
								jsonObjectOverView.put("userSource", entityAttribute.getUserSource());
							if (entityAttribute.getUserPublishedDate() != null)
								jsonObjectOverView.put("userPublishedDate", entityAttribute.getUserPublishedDate().toString());
							if (entityAttribute.getUserSourceUrl() != null)
								jsonObjectOverView.put("userSourceUrl", entityAttribute.getUserSourceUrl());
							if (entityAttribute.getSourceDisplayName() != null)
								jsonObjectOverView.put("sourceDisplayName", entityAttribute.getSourceDisplayName());
							if(entityAttribute.getSourceType()!=null)
								jsonObjectOverView.put("sourceType", entityAttribute.getSourceType());
							if(entityAttribute.getDocId()!=null)
								jsonObjectOverView.put("docId", entityAttribute.getDocId());
							
							getSources(null, jsonObjectOverView, jsonObject, entityAttribute.getSourceUrl(),
									entityAttribute.getSourceSchema(), true,entityAttribute.getSource(),entityAttribute.getSourceDisplayName(),map,credibilityJson);
						}
						if(sources!=null){
							for (int i = 0; i < sources.length(); i++) {
								JSONObject jsonObjectOverView = overview.getJSONObject(sources.getString(i));
								jsonObjectOverView.accumulate("date", new Date());
								String sourceUrl = null;
								if (jsonObjectOverView.has("bst:registryURI")
										&& !"".equalsIgnoreCase(jsonObjectOverView.getString("bst:registryURI")))
									sourceUrl = jsonObjectOverView.getString("bst:registryURI");
								else
									sourceUrl = sources.getString(i);
								jsonObject = getSourceInfo(jsonObjectOverView, jsonObject, map, sources.getString(i),
										sourceUrl);
								if (jsonObjectOverView.has("vcard:organization-name")) {
									String organizationName = jsonObjectOverView.getString("vcard:organization-name");
									AdvanceNewsFavourite advanceNewsFavourite = advanceNewsFavouriteDao
											.fetchadvanceNewsFavourite(identifier, organizationName, userId);
									if (advanceNewsFavourite != null) {
										jsonObjectOverView.accumulate("entitySiginificance", true);
									}
									
									//For now disabled Url Check,Due to duplicate Sources coming 
									/*if (sources.getString(i).equals("BST") && jsonObjectOverView.has("hasURL")) {
										boolean sourcesExist = sourcesService.checkSourceExist(identifier,
												jsonObjectOverView.getString("hasURL"));
										if (sourcesExist) {
											sourcesService.saveCompanyWebsiteSource(identifier,
													jsonObjectOverView.getString("hasURL"), organizationName,
													"Company Website", userId);
										}
									}*/
								}
							}
						}
						String countriesOfOperation=getCountriesOfOperation(identifier, jsonObject, jurisdiction, country, map);
						
						/*if(countriesOfOperation != null ||new JSONArray(countriesOfOperation).length()<= 0) {
							countriesOfOperation=getDefaultCountriesOfOperation(jsonObject, jurisdiction, country);
							jsonObject.put("CountryOperations", new JSONArray(countriesOfOperation));
						}else 
							jsonObject.put("CountryOperations", new JSONArray(countriesOfOperation));
						*/
						jsonObject.put("CountryOperations", new JSONArray(countriesOfOperation));
						overview.put("comapnyInfo", jsonObject);
						
												///  Auditing Changes FOr Overview API
						if (json.has("is-completed") && json.getBoolean("is-completed")) {

							if(overview.has("comapnyInfo") && overview.getJSONObject("comapnyInfo").has("vcard:organization-name") ) {
								if(jsonObject.has("vcard:organization-name")) {
									auditLogService.auditLogForOverviewAPI(identifier,overview.getJSONObject("comapnyInfo").getJSONObject("vcard:organization-name").getString("value"), userId);
								}

							}
						}
												///
						
						//Following parameters related to Source Add to Page  
						if(sources!=null){
							for (int i = 0; i < sources.length(); i++) {
								JSONObject jsonObjectOverView = overview.getJSONObject(sources.getString(i));
								if(jsonObjectOverView!=null) {
									SourceAddToPageDto addToPageDto=sourceAddToPageService.getSourceAddToPage(identifier, sources.getString(i));
									if(addToPageDto!=null) {
									jsonObjectOverView.put("source_file_url", addToPageDto.getScreenShotUrl());
									jsonObjectOverView.put("isAddToPage", addToPageDto.getIsAddToPage());
									jsonObjectOverView.put("source_file_name", addToPageDto.getFileName());
									jsonObjectOverView.put("date", addToPageDto.getUpdatedTime());
									jsonObjectOverView.put("docId", addToPageDto.getDocId());
									}

								}
							}
						}
					}
				}
			}
			return json.toString();
		} else {
			return response;
		}
	}

	@Override
	public String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level,String clientId,String sourceType)
			throws UnsupportedEncodingException {

		String url = BIGDATA_MULTISOURCE_URL+"/";
		String finalFields = null;
		if ("finance_info".equals(fields)) {
			finalFields = "overview";
		} else {
			finalFields = fields;
		}
	
		if (identifier != null)
			url = url + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (graph != null)
			url = url + "/" + graph;
		if (level != null)
			url = url + "/" + level;
		if (finalFields != null)
			url = url + "?fields=" + finalFields+"&"+sourceType+"="+URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (clientId != null)
			url = url + "&client_id=" + clientId;
		return url;

	}

	public JSONArray getFinanceJsonArray(JSONObject financeData) {
		JSONArray financialInfo = new JSONArray();
		if (financeData.has("finance_info")) {
			if (financeData.get("finance_info") instanceof JSONArray) {
				JSONArray financeInfoArray = financeData.getJSONArray("finance_info");
				financialInfo = financeInfoArray;
			} else {
				JSONObject financeInfoJson = financeData.getJSONObject("finance_info");
				if (financeInfoJson.has("Financial_Information")) {
					JSONArray financialInformation = financeInfoJson.getJSONArray("Financial_Information");
					financialInfo = financialInformation;
				}
			}
		} else {
			JSONArray financialInformation = financeData.getJSONArray("Financial_Information");
			financialInfo = financialInformation;
		}
		return financialInfo;
	}

	public String mergeFinancialInformation(String financeLink, Map<String, SourcesDto> map) throws Exception {
		JSONObject finalFinanceInfo = new JSONObject();
		JSONArray finalBalanceSheets = new JSONArray();
		JSONArray finalCashFlowStatements = new JSONArray();
		JSONArray finalIncomeStatements = new JSONArray();
		JSONArray finalHistoricalPrices = new JSONArray();
		JSONObject finalStockinfo = new JSONObject();
		JSONObject finalCurrentJson = new JSONObject();
		JSONObject finalSummaryJson = new JSONObject();
		/*
		 * String serverResponse[] = null; serverResponse =
		 * ServiceCallHelper.getDataFromServerWithoutTimeout(financeLink); //if
		 * (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) ==
		 * 200) { String response = serverResponse[1]; JSONObject json = new
		 * JSONObject(); json = new JSONObject(response); if
		 * (json.has("is-completed")) { int counter = 0; while
		 * (json.has("is-completed") && !json.getBoolean("is-completed")) {
		 * serverResponse =
		 * ServiceCallHelper.getDataFromServerWithoutTimeout(financeLink); if
		 * (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) ==
		 * 200) { response = serverResponse[1]; if
		 * (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
		 * json = new JSONObject(response); } } counter++; if (counter == 6)
		 * break; Thread.sleep(5000); } }
		 */
		// if (json.has("data")) {
		// JSONObject dataJson = json.getJSONObject("data");
		JSONObject json = new JSONObject();
		String response = getDataInAsynMode(financeLink);
		if (response != null) {
			json = new JSONObject(response);
		}
		if (json.has("results")) {
			JSONArray resultsArray = json.getJSONArray("results");
			if (resultsArray.length() > 0) {
				JSONObject firstResult = resultsArray.getJSONObject(0);
				// if (firstResult.has("result")) {
				// JSONObject result = firstResult.getJSONObject("result");
				if (firstResult.has("finance_info")) {
					CredibilityEnums previousStockCredibility = CredibilityEnums.LOW;
					boolean isStockCredibilityHigh = false;
					CredibilityEnums previousSummaryCredit = CredibilityEnums.LOW;
					boolean isSummaryCreditHigh = false;
					JSONObject data = firstResult.getJSONObject("finance_info");
					JSONArray sources = data.names();
					Map<String, CredibilityEnums> dataAttributesMap = new HashMap<String, CredibilityEnums>();
					if(sources!=null){
					for (int i = 0; i < sources.length(); i++) {
						if (map.containsKey(sources.getString(i))) {
							SourcesDto sourcesDto = map.get(sources.getString(i));
							List<ClassificationsDto> classifications = sourcesDto.getClassifications();
							for (ClassificationsDto classificationsDto : classifications) {
								List<SubClassificationsDto> subClassifications = classificationsDto
										.getSubClassifications();
								for (SubClassificationsDto subClassificationsDto : subClassifications) {
									if (subClassificationsDto.getSubClassifcationName().contains("Financial")) {

										List<DataAttributesDto> dataAttributes = subClassificationsDto
												.getDataAttributes();
										dataAttributesMap = dataAttributes.stream()
												.collect(Collectors.toMap(DataAttributesDto::getSourceAttributeSchema,
														DataAttributesDto::getCredibilityValue));
									}
								}
							}
						}
						JSONObject financeInfo = new JSONObject();
						if (data.get(sources.getString(i)) instanceof JSONArray) {
							JSONArray dataFromSource = data.getJSONArray(sources.getString(i));
							if (dataFromSource.length() > 0) {
								financeInfo = dataFromSource.getJSONObject(0);

							}
						} else {
							JSONObject dataFromSource = data.getJSONObject(sources.getString(i));
							if (dataFromSource.has("Financial_Information")) {
								JSONArray financialInformation =new JSONArray();
								if(dataFromSource.get("Financial_Information") instanceof JSONArray)
									financialInformation=dataFromSource.getJSONArray("Financial_Information");
								if (financialInformation.length() > 0) {
									financeInfo = financialInformation.getJSONObject(0);
								}

							}
						}
						// JSONArray dataFromSource =
						// data.getJSONArray(sources.getString(i));
						// if (dataFromSource.length() > 0) {
						// JSONObject firstJson =
						// dataFromSource.getJSONObject(0);
						// JSONArray financeInfoArray = new JSONArray();
						/*
						 * if (firstJson.has("finance_info")) { if
						 * (firstJson.get("finance_info") instanceof JSONArray)
						 * { financeInfoArray =
						 * firstJson.getJSONArray("finance_info"); } else {
						 * JSONObject financeInfoObject =
						 * firstJson.getJSONObject("finance_info"); if
						 * (financeInfoObject.has("Financial_Information"))
						 * financeInfoArray = financeInfoObject
						 * .getJSONArray("Financial_Information"); }
						 * 
						 * } else { financeInfoArray =
						 * firstJson.getJSONArray("Financial_Information"); }
						 */
						if (financeInfo.has("financial_statements")) {
							JSONObject financialStatements = financeInfo.getJSONObject("financial_statements");
							if (financialStatements.has("balance_sheet")) {
								CredibilityEnums priority = null;
								if (dataAttributesMap.containsKey("balance_sheet")) {
									priority = dataAttributesMap.get("balance_sheet");
								} else {
									priority = CredibilityEnums.LOW;
								}
								JSONArray balanceSheetArray = financialStatements.getJSONArray("balance_sheet");
								for (int j = 0; j < balanceSheetArray.length(); j++) {
									JSONObject otherBalanceSheet = balanceSheetArray.getJSONObject(j);
									otherBalanceSheet.put("priority", priority.toString());
									boolean flag = true;
									IN_LOOP: for (int k = 0; k < finalBalanceSheets.length(); k++) {
										JSONObject finalBalanceSheet = finalBalanceSheets.getJSONObject(k);
										if (otherBalanceSheet.getString("calendar_year")
												.equals(finalBalanceSheet.getString("calendar_year"))
												&& otherBalanceSheet.getString("section")
												.equals(finalBalanceSheet.getString("section"))) {
											flag = false;
											if ((finalBalanceSheet.getString("priority").equals("LOW")
													|| finalBalanceSheet.getString("priority").equals("MEDIUM"))
													&& otherBalanceSheet.getString("priority").equals("HIGH"))
												finalBalanceSheet = otherBalanceSheet;
											if (finalBalanceSheet.getString("priority").equals("LOW")
													&& otherBalanceSheet.getString("priority").equals("MEDIUM"))
												finalBalanceSheet = otherBalanceSheet;
											break IN_LOOP;
										}
									}
									if (flag)
										finalBalanceSheets.put(otherBalanceSheet);
								}
							}
							// cash flow elements
							if (financialStatements.has("cash_flow_statement")) {
								CredibilityEnums priority = null;
								if (dataAttributesMap.containsKey("cash_flow_statement")) {
									priority = dataAttributesMap.get("cash_flow_statement");
								} else {
									priority = CredibilityEnums.LOW;
								}
								JSONArray cashflowstatements = financialStatements.getJSONArray("cash_flow_statement");
								for (int j = 0; j < cashflowstatements.length(); j++) {
									JSONObject otherCashFlowElement = cashflowstatements.getJSONObject(j);
									otherCashFlowElement.put("priority", priority.toString());
									boolean flag = true;
									IN_LOOP: for (int k = 0; k < finalCashFlowStatements.length(); k++) {
										JSONObject finalCashFlowStatement = finalCashFlowStatements.getJSONObject(k);
										if (otherCashFlowElement.getString("calendar_year")
												.equals(finalCashFlowStatement.getString("calendar_year"))
												&& otherCashFlowElement.getString("section")
												.equals(finalCashFlowStatement.getString("section"))) {
											flag = false;
											if ((finalCashFlowStatement.getString("priority").equals("LOW")
													|| finalCashFlowStatement.getString("priority").equals("MEDIUM"))
													&& otherCashFlowElement.getString("priority").equals("HIGH"))
												finalCashFlowStatement = otherCashFlowElement;
											if (finalCashFlowStatement.getString("priority").equals("LOW")
													&& otherCashFlowElement.getString("priority").equals("MEDIUM"))
												finalCashFlowStatement = otherCashFlowElement;
											break IN_LOOP;
										}
									}
									if (flag)
										finalCashFlowStatements.put(otherCashFlowElement);
								}
							}
							// Income Statements
							if (financialStatements.has("income_statement")) {
								JSONArray incomeStatements = financialStatements.getJSONArray("income_statement");
								CredibilityEnums priority = null;
								if (dataAttributesMap.containsKey("income_statement")) {
									priority = dataAttributesMap.get("income_statement");
								} else {
									priority = CredibilityEnums.LOW;
								}
								for (int j = 0; j < incomeStatements.length(); j++) {
									JSONObject otherIncomeStatement = incomeStatements.getJSONObject(j);
									otherIncomeStatement.put("priority", priority.toString());
									boolean flag = true;
									IN_LOOP: for (int k = 0; k < finalIncomeStatements.length(); k++) {
										JSONObject finalIncomeStatement = finalIncomeStatements.getJSONObject(k);
										if (otherIncomeStatement.getString("year")
												.equals(finalIncomeStatement.getString("year"))
												&& otherIncomeStatement.getString("section")
												.equals(finalIncomeStatement.getString("section"))) {
											flag = false;
											if ((finalIncomeStatement.getString("priority").equals("LOW")
													|| finalIncomeStatement.getString("priority").equals("MEDIUM"))
													&& otherIncomeStatement.getString("priority").equals("HIGH"))
												finalIncomeStatement = otherIncomeStatement;
											if (finalIncomeStatement.getString("priority").equals("LOW")
													&& otherIncomeStatement.getString("priority").equals("MEDIUM"))
												finalIncomeStatement = otherIncomeStatement;
											break IN_LOOP;
										}
									}
									if (flag)
										finalIncomeStatements.put(otherIncomeStatement);
								}
							}
						}
						// stock information
						if (financeInfo.has("stocks_information")) {
							CredibilityEnums priority = null;
							if (dataAttributesMap.containsKey("stocks_information")) {
								priority = dataAttributesMap.get("stocks_information");
							} else {
								priority = CredibilityEnums.LOW;
							}
							JSONArray stocksInformationArray = financeInfo.getJSONArray("stocks_information");
							JSONObject stocksInformationJson = stocksInformationArray.getJSONObject(0);
							// historical prices
							if (stocksInformationJson.has("historical_prices")) {
								JSONArray otherHistoricalPrices = stocksInformationJson
										.getJSONArray("historical_prices");
								for (int j = 0; j < otherHistoricalPrices.length(); j++) {
									boolean flag = true;
									JSONObject otherHistoricalPrice = otherHistoricalPrices.getJSONObject(j);
									otherHistoricalPrice.put("priority", priority.toString());
									for (int k = 0; k < finalHistoricalPrices.length(); k++) {
										JSONObject finalHistoricalPrice = finalHistoricalPrices.getJSONObject(k);
										if (otherHistoricalPrice.getString("data_date")
												.equals(finalHistoricalPrice.getString("data_date"))) {
											flag = false;
											if ((finalHistoricalPrice.getString("priority").equals("LOW")
													|| finalHistoricalPrice.getString("priority").equals("MEDIUM"))
													&& otherHistoricalPrice.getString("priority").equals("HIGH"))
												finalHistoricalPrice = otherHistoricalPrice;
											if (finalHistoricalPrice.getString("priority").equals("LOW")
													&& otherHistoricalPrice.getString("priority").equals("MEDIUM"))
												finalHistoricalPrice = otherHistoricalPrice;
											break;
										}
									}
									if (flag)
										finalHistoricalPrices.put(otherHistoricalPrice);
								}
							}
							// stock name
							if (stocksInformationJson.has("stock_name")) {
								getCredibilityValues("stock_name", stocksInformationJson, finalStockinfo,
										previousStockCredibility, isStockCredibilityHigh, priority);
							}
							if (stocksInformationJson.has("stock_id")) {
								getCredibilityValues("stock_id", stocksInformationJson, finalStockinfo,
										previousStockCredibility, isStockCredibilityHigh, priority);
							}
							if (stocksInformationJson.has("current")) {
								JSONObject currentJson = stocksInformationJson.getJSONObject("current");
								JSONArray keys = currentJson.names();
								for (int j = 0; j < keys.length(); j++) {
									getCredibilityValues(keys.getString(j), currentJson, finalCurrentJson,
											previousStockCredibility, isStockCredibilityHigh, priority);
								}

							}
						}
						// summary
						if (financeInfo.has("summary")) {
							CredibilityEnums priority = null;
							if (dataAttributesMap.containsKey("summary")) {
								priority = dataAttributesMap.get("summary");
							} else {
								priority = CredibilityEnums.LOW;
							}
							JSONObject summaryJson = financeInfo.getJSONObject("summary");
							JSONArray keys = summaryJson.names();
							for (int j = 0; j < keys.length(); j++) {
								getCredibilityValues(keys.getString(j), summaryJson, finalSummaryJson,
										previousSummaryCredit, isSummaryCreditHigh, priority);
							}
						}
						// }
					}
				}
				}
				// }
			}
		}
		// }
		// }
		JSONObject finalFinancialstatement = new JSONObject();
		finalFinancialstatement.put("balance_sheet", finalBalanceSheets);
		finalFinancialstatement.put("cash_flow_statement", finalCashFlowStatements);
		finalFinancialstatement.put("income_statement", finalIncomeStatements);
		finalStockinfo.put("historical_prices", finalHistoricalPrices);
		finalStockinfo.put("current", finalCurrentJson);
		finalFinanceInfo.put("financial_statements", finalFinancialstatement);
		finalFinanceInfo.put("stocks_information", finalStockinfo);
		finalFinanceInfo.put("summary", finalSummaryJson);
		return finalFinanceInfo.toString();
	}

	public void getCredibilityValues(String field, JSONObject currentJson, JSONObject finalCurrentJson,
			CredibilityEnums previousStockCredibility, boolean isStockCredibilityHigh, CredibilityEnums priority) {
		if (currentJson.has(field)) {
			String stockName = currentJson.get(field).toString();
			if (finalCurrentJson.has(field)) {
				String finalStockName = finalCurrentJson.get(field).toString();
				if ((!isStockCredibilityHigh && previousStockCredibility.equals(CredibilityEnums.LOW)
						&& priority.equals(CredibilityEnums.MEDIUM)) || priority.equals(CredibilityEnums.HIGH)) {
					finalCurrentJson.put(field, finalStockName);
					previousStockCredibility = priority;
					if (priority.equals(CredibilityEnums.HIGH))
						isStockCredibilityHigh = true;
				}
			} else {
				finalCurrentJson.put(field, stockName);
				if (priority.equals(CredibilityEnums.HIGH))
					isStockCredibilityHigh = true;
				previousStockCredibility = priority;
			}
		}
	}

	public JSONObject getSourceInfo(JSONObject jsonObjectOverView, JSONObject jsonObject, Map<String, SourcesDto> map,
			String source, String sourceUrl) throws MalformedURLException {
		if (source != null) {
			if (map.containsKey(source)) {
				SourcesDto sourcesDto = map.get(source);
				List<ClassificationsDto> classifications = sourcesDto.getClassifications();
				for (ClassificationsDto classificationsDto : classifications) {
					if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
						List<SubClassificationsDto> subClassifications = classificationsDto.getSubClassifications();
						for (SubClassificationsDto subClassificationsDto : subClassifications) {
							if (subClassificationsDto.getSubClassifcationName().contains("Company")) {
								List<DataAttributesDto> dataAttributes = subClassificationsDto.getDataAttributes();
								Map<String, CredibilityEnums> dataAttributesMap = dataAttributes.stream().filter(d->d.getCredibilityValue()!=null)
										.collect(Collectors.toMap(DataAttributesDto::getSourceAttributeSchema,
												DataAttributesDto::getCredibilityValue));
								JSONArray keys = jsonObjectOverView.names();
								if (keys != null) {
									for (int i = 0; i < keys.length(); i++) {
										String dataAttribute = keys.getString(i);
										if (dataAttribute.equalsIgnoreCase("identifiers")) {
											JSONObject identifierJson = jsonObjectOverView.getJSONObject("identifiers");
											JSONArray identifiers = identifierJson.names();
											for (int j = 0; j < identifiers.length(); j++) {
												dataAttribute = identifiers.getString(j);
												if(!StringUtils.isEmpty(identifierJson.get(dataAttribute).toString())){
												getSources(dataAttributesMap, identifierJson, jsonObject, sourceUrl,
														dataAttribute, false,source,sourcesDto.getSourceDisplayName(),null,null);
												}
											}
										} else if(dataAttribute.equalsIgnoreCase("bst:stock_info")){
											JSONObject stockJson= new JSONObject();
											if (jsonObjectOverView.has("bst:stock_info"))
												stockJson = jsonObjectOverView.getJSONObject("bst:stock_info");
											JSONArray stocks = new JSONArray();
											if(stockJson!=null)
												stocks = stockJson.names();
											if (stocks != null && stocks.length() > 0) {
												for (int j = 0; j < stocks.length(); j++) {
													dataAttribute = stocks.getString(j);
													if (!StringUtils.isEmpty(stockJson.get(dataAttribute).toString())) {
														getSources(dataAttributesMap, stockJson, jsonObject, sourceUrl,
																dataAttribute, false, source,
																sourcesDto.getSourceDisplayName(), null, null);
													}
												}
											}
										} else {
											if(!StringUtils.isEmpty(jsonObjectOverView.get(dataAttribute).toString())){
												getSources(dataAttributesMap, jsonObjectOverView, jsonObject, sourceUrl,
														dataAttribute, false,source,sourcesDto.getSourceDisplayName(),null,null);
											}
											/*getSources(dataAttributesMap, jsonObjectOverView, jsonObject, sourceUrl,
													dataAttribute, false,source,sourcesDto.getSourceDisplayName(),null,null);*/
										}
									} 
								}
							}
						}
					}
				}
			}
		} else {
			JSONArray keys = jsonObjectOverView.names();
			for (int i = 0; i < keys.length(); i++) {
				boolean isUserData = true;
				String dataAttribute = keys.getString(i);
				getSources(null, jsonObjectOverView, jsonObject, sourceUrl, dataAttribute, isUserData,source,source,null,null);
				//getSources(null, jsonObjectOverView, jsonObject, sourceUrl, dataAttribute, isUserData,null,null);
			}

		}
		return jsonObject;
	}

	private void getSources(Map<String, CredibilityEnums> dataAttributesMap, JSONObject jsonObjectOverView,
			JSONObject jsonObject, String sourceUrl, String dataAttribute, boolean isUserData, String source, String  sourceDisplayName,Map<String, SourcesDto> map, JSONObject credibilityJson) {
		if (!isUserData) {
			boolean isPrimary = false;
			int credibility = 0;
			if (!isUserData && dataAttributesMap.containsKey(dataAttribute)
					&& dataAttributesMap.get(dataAttribute).equals(CredibilityEnums.HIGH)) {
				isPrimary = true;
				credibility = dataAttributesMap.get(dataAttribute).ordinal();
			} else if (!isUserData && dataAttributesMap.containsKey(dataAttribute)
					&& (dataAttributesMap.get(dataAttribute).equals(CredibilityEnums.MEDIUM)
							|| dataAttributesMap.get(dataAttribute).equals(CredibilityEnums.LOW))) {
				credibility = dataAttributesMap.get(dataAttribute).ordinal();
			} else if (!isUserData) {
				credibility = dataAttributesMap.containsKey(dataAttribute)
						? dataAttributesMap.get(dataAttribute).ordinal() : 0;
			}
			String sourceType = null;
			String otherSourceType = null;
			if (isPrimary) {
				sourceType = "primarySource";
				otherSourceType = "secondarySource";
			} else {
				otherSourceType = "primarySource";
				sourceType = "secondarySource";
			}
			/*if (dataAttribute.equalsIgnoreCase("CountryOperations")) {
				if (jsonObjectOverView.has("CountryOperations")) {
					if (jsonObject.has("countryOperations")) {
						JSONObject attributJson = jsonObject.getJSONObject("countryOperations");
						attributJson.getJSONArray(sourceType).put(sourceUrl);
						if (attributJson.has(sourceType)) {
							if (!attributJson.getBoolean("isOverriddenData") && !attributJson.getBoolean("isUserData")
									&& (isPrimary || attributJson.getInt("credibility") < credibility)) {
								attributJson.put("value", jsonObjectOverView.get("CountryOperations").toString());
								attributJson.put("credibility", credibility);
							}
						}
					} else {
						jsonObject.put("countryOperations", new JSONObject());
						JSONObject attributJson = jsonObject.getJSONObject("countryOperations");
						attributJson.put("value", jsonObjectOverView.get("CountryOperations").toString());
						attributJson.put("source", sourceUrl);
						attributJson.put("sourceRef", sourceUrl);
						attributJson.put("conflicts", new JSONArray());
						attributJson.put(otherSourceType, new JSONArray());
						attributJson.put(sourceType, new JSONArray().put(sourceUrl));
						attributJson.put("isUserData", isUserData);
						attributJson.put("credibility", credibility);
						attributJson.put("isOverriddenData", false);
					}
				}
			}*/
			if (dataAttribute.equalsIgnoreCase("mdaas:RegisteredAddress")) {
				JSONObject registerAddress = new JSONObject();
				if (jsonObjectOverView.get("mdaas:RegisteredAddress") instanceof JSONObject) {
					registerAddress = jsonObjectOverView.getJSONObject(dataAttribute);
				} else {
					JSONArray arrayAddress = jsonObjectOverView.getJSONArray(dataAttribute);
					registerAddress = arrayAddress.getJSONObject(0);
				}
				JSONArray names = registerAddress.names();
				if (names != null && names.length() > 0) {
					for (int j = 0; j < names.length(); j++) {
						if (jsonObject.has(names.getString(j))
								&& !StringUtils.isEmpty(jsonObject.get(names.getString(j)).toString())) {
							JSONObject attributJson = jsonObject.getJSONObject(names.getString(j));
							if (attributJson.has(sourceType)) {
								String valueRef = attributJson.getString("valueRef");
								String sourceRef = attributJson.getString("sourceRef");
								if (!attributJson.getBoolean("isOverriddenData")
										&& !attributJson.getBoolean("isUserData")
										&& (attributJson.getString("value").equals("") || ((isPrimary
												|| attributJson.getInt("credibility") < credibility)
												&& !registerAddress.getString(names.getString(j)).equals("")))) {
									attributJson.put("value", registerAddress.getString(names.getString(j)));
									attributJson.put("sourceDisplayName", sourceDisplayName);
									attributJson.put("sourceUrl", sourceUrl);
									attributJson.put("source", source);
									attributJson.put("credibility", credibility);

								}
								if (!sourceRef.equalsIgnoreCase(sourceUrl)) {
									JSONObject sourceObj = new JSONObject();
									sourceObj.put("source", source);
									sourceObj.put("sourceUrl", sourceUrl);
									sourceObj.put("sourceDisplayName", sourceDisplayName);
									sourceObj.put("value", registerAddress.getString(names.getString(j)));
									attributJson.getJSONArray(sourceType).put(sourceObj);
									// attributJson.getJSONArray(sourceType).put(sourceUrl);
								}
								if (!"".equalsIgnoreCase(sourceRef) && attributJson.has("conflicts")) {
									// attributJson.getJSONArray(sourceType).put(sourceUrl);
									if (!sourceRef.equalsIgnoreCase(sourceUrl) && !registerAddress
											.getString(names.getString(j)).equalsIgnoreCase(valueRef)) {
										JSONArray conflicts = attributJson.getJSONArray("conflicts");
										JSONObject conflict = new JSONObject();
										conflict.put("source1", attributJson.getString("sourceRef"));
										conflict.put("value1", valueRef);
										conflict.put("source2", sourceUrl);
										conflict.put("value2", registerAddress.getString(names.getString(j)));
										conflicts.put(conflict);
									}
								} else {
									if ("".equalsIgnoreCase(sourceRef)) {
										attributJson.put("sourceRef", sourceUrl);
										attributJson.put("valueRef", registerAddress.getString(names.getString(j)));
									}
								}
							}
						} else {
							jsonObject.put(names.getString(j), new JSONObject());
							JSONObject attributJson = jsonObject.getJSONObject(names.getString(j));
							attributJson.put("value", registerAddress.getString(names.getString(j)));
							attributJson.put("valueRef", registerAddress.getString(names.getString(j)));
							attributJson.put("source", source);
							attributJson.put("sourceRef", sourceUrl);
							attributJson.put("sourceUrl", sourceUrl);
							attributJson.put("sourceDisplayName", sourceDisplayName);
							attributJson.put("conflicts", new JSONArray());
							JSONObject sourceObj = new JSONObject();
							sourceObj.put("source", source);
							sourceObj.put("sourceUrl", sourceUrl);
							sourceObj.put("value", registerAddress.getString(names.getString(j)));
							attributJson.put(sourceType, new JSONArray().put(sourceObj));
							// attributJson.put(sourceType, new JSONArray().put(sourceUrl));
							attributJson.put(otherSourceType, new JSONArray());
							attributJson.put("credibility", credibility);
							attributJson.put("isUserData", isUserData);
							attributJson.put("isOverriddenData", false);
						}
					}
				}
			}
			if (dataAttribute.equalsIgnoreCase("bst:stock_info")) {
				JSONObject stockInfo = new JSONObject();
				if (jsonObjectOverView.get("bst:stock_info") instanceof JSONObject) {
					stockInfo = jsonObjectOverView.getJSONObject(dataAttribute);
				}
				JSONArray names = stockInfo.names();
				if (names!=null && names.length() > 0) {
					for (int j = 0; j < names.length(); j++) {
						if (jsonObject.has(names.getString(j)) && !StringUtils.isEmpty(stockInfo.get(names.getString(j)).toString())) {
							JSONObject attributJson = jsonObject.getJSONObject(names.getString(j));
							if (attributJson.has(sourceType)) {
								String valueRef = attributJson.getString("valueRef");
								String sourceRef = attributJson.getString("sourceRef");
								if (!attributJson.getBoolean("isOverriddenData")
										&& !attributJson.getBoolean("isUserData")
										&& (attributJson.getString("value").equals("")
												|| ((isPrimary || attributJson.getInt("credibility") < credibility)
														&& !stockInfo.getString(names.getString(j)).equals("")))) {
									attributJson.put("value", stockInfo.getString(names.getString(j)));
									attributJson.put("source", source);
									attributJson.put("sourceDisplayName", sourceDisplayName);
									attributJson.put("credibility", credibility);
									attributJson.put("sourceUrl", sourceUrl);
								}
								if (!sourceRef.equalsIgnoreCase(sourceUrl)){
									JSONObject sourceObj=new JSONObject();
									sourceObj.put("source", source);
									sourceObj.put("sourceUrl", sourceUrl);
									sourceObj.put("sourceDisplayName", sourceDisplayName);
									sourceObj.put("value", stockInfo.getString(names.getString(j)));
									attributJson.getJSONArray(sourceType).put(sourceObj);
									//attributJson.getJSONArray(sourceType).put(sourceUrl);
								}
								if (!"".equalsIgnoreCase(sourceRef) && attributJson.has("conflicts")) {
									// attributJson.getJSONArray(sourceType).put(sourceUrl);
									if (!sourceRef.equalsIgnoreCase(sourceUrl)
											&& !stockInfo.getString(names.getString(j)).equalsIgnoreCase(valueRef)) {
										JSONArray conflicts = attributJson.getJSONArray("conflicts");
										JSONObject conflict = new JSONObject();
										conflict.put("source1", attributJson.getString("sourceRef"));
										conflict.put("value1", valueRef);
										conflict.put("source2", sourceUrl);
										conflict.put("value2", stockInfo.getString(names.getString(j)));
										conflicts.put(conflict);
									}
								} else {
									if ("".equalsIgnoreCase(sourceRef)) {
										attributJson.put("sourceRef", sourceUrl);
										attributJson.put("valueRef", stockInfo.getString(names.getString(j)));
									}
								}
							}
						} else {
							if(!"".equalsIgnoreCase(stockInfo.getString(names.getString(j)))){
							jsonObject.put(names.getString(j), new JSONObject());
							JSONObject attributJson = jsonObject.getJSONObject(names.getString(j));
							attributJson.put("value", stockInfo.getString(names.getString(j)));
							attributJson.put("valueRef", stockInfo.getString(names.getString(j)));
							attributJson.put("source", source);
							attributJson.put("sourceRef", sourceUrl);
							attributJson.put("sourceUrl", sourceUrl);
							attributJson.put("sourceDisplayName", sourceDisplayName);
							attributJson.put("conflicts", new JSONArray());
							JSONObject sourceObj=new JSONObject();
							sourceObj.put("source", source);
							sourceObj.put("sourceUrl", sourceUrl);
							sourceObj.put("value", stockInfo.getString(names.getString(j)));
							attributJson.put(sourceType,new JSONArray().put(sourceObj));
							//attributJson.put(sourceType, new JSONArray().put(sourceUrl));
							attributJson.put(otherSourceType, new JSONArray());
							attributJson.put("credibility", credibility);
							attributJson.put("isUserData", isUserData);
							attributJson.put("isOverriddenData", false);
						}
						}
						
					}
				}
			}
			
			if(!jsonObject.has("lei:legalForm")) {
				if (dataAttribute.equalsIgnoreCase("lei:legalForm")) {
					if (jsonObject.has("lei:legalForm")) {
						JSONObject attributJson = jsonObject.getJSONObject("lei:legalForm");
						if (attributJson.has(sourceType)) {
							String valueRef = attributJson.getString("valueRef");
							String sourceRef = attributJson.getString("sourceRef");
							if (!attributJson.getBoolean("isOverriddenData")
									&& !attributJson.getBoolean("isUserData")
									&& (attributJson.getString("value").equals("")
											|| ((isPrimary || attributJson.getInt("credibility") < credibility)
													&&jsonObjectOverView.getJSONObject(dataAttribute).has("label") &&
													!jsonObjectOverView.getJSONObject(dataAttribute).getString("label").equalsIgnoreCase("")))) {
								attributJson.put("value", jsonObjectOverView.get(dataAttribute).toString());
								attributJson.put("source", source);
								attributJson.put("sourceDisplayName", sourceDisplayName);
								attributJson.put("credibility", credibility);
								attributJson.put("sourceUrl", sourceUrl);
							}
							if (!sourceRef.equalsIgnoreCase(sourceUrl)){
								JSONObject sourceObj=new JSONObject();
								sourceObj.put("source", source);
								sourceObj.put("sourceUrl", sourceUrl);
								sourceObj.put("sourceDisplayName", sourceDisplayName);
								sourceObj.put("value", jsonObjectOverView.get(dataAttribute).toString());
								attributJson.getJSONArray(sourceType).put(sourceObj);
								//attributJson.getJSONArray(sourceType).put(sourceUrl);
							}
							if (!"".equalsIgnoreCase(sourceRef) && attributJson.has("conflicts")) {
								// attributJson.getJSONArray(sourceType).put(sourceUrl);
								if (!sourceRef.equalsIgnoreCase(sourceUrl)
										&& !jsonObjectOverView.get(dataAttribute).toString().equalsIgnoreCase(valueRef)) {
									JSONArray conflicts = attributJson.getJSONArray("conflicts");
									JSONObject conflict = new JSONObject();
									conflict.put("source1", attributJson.getString("sourceRef"));
									conflict.put("value1", valueRef);
									conflict.put("source2", sourceUrl);
									conflict.put("value2", jsonObjectOverView.get(dataAttribute).toString());
									conflicts.put(conflict);
								}
							} else {
								if ("".equalsIgnoreCase(sourceRef)) {
									attributJson.put("sourceRef", sourceUrl);
									attributJson.put("valueRef", jsonObjectOverView.get(dataAttribute).toString());
								}
							}
						}
					} else {
						jsonObject.put(dataAttribute, new JSONObject());
						JSONObject attributJson = jsonObject.getJSONObject(dataAttribute);
						if(jsonObjectOverView.getJSONObject(dataAttribute).has("label") &&
								!jsonObjectOverView.getJSONObject(dataAttribute).getString("label").equalsIgnoreCase("")) {
							attributJson.put("value", jsonObjectOverView.get(dataAttribute).toString());
							attributJson.put("valueRef", jsonObjectOverView.get(dataAttribute).toString());
							attributJson.put("source", source);
							attributJson.put("sourceRef", sourceUrl);
							attributJson.put("sourceUrl", sourceUrl);
							attributJson.put("sourceDisplayName", sourceDisplayName);
							attributJson.put("conflicts", new JSONArray());
							JSONObject sourceObj=new JSONObject();
							sourceObj.put("source", source);
							sourceObj.put("sourceUrl", sourceUrl);
							sourceObj.put("value", jsonObjectOverView.get(dataAttribute).toString());
							attributJson.put(sourceType,new JSONArray().put(sourceObj));
							//attributJson.put(sourceType, new JSONArray().put(sourceUrl));
							attributJson.put(otherSourceType, new JSONArray());
							attributJson.put("credibility", credibility);
							attributJson.put("isUserData", isUserData);
							attributJson.put("isOverriddenData", false);
						}
						

					}
				}
			}
			
			if (jsonObject.has(dataAttribute)) {
				JSONObject attributJson = jsonObject.getJSONObject(dataAttribute);
				if (attributJson.has(sourceType)) {
					String valueRef = attributJson.getString("valueRef");
					String sourceRef = attributJson.getString("sourceRef");
					if (!attributJson.getBoolean("isOverriddenData") && !attributJson.getBoolean("isUserData")
							&& (attributJson.getString("value").equals("")
									|| ((isPrimary || attributJson.getInt("credibility") < credibility)
											&& !jsonObjectOverView.get(dataAttribute).toString().equals("")))) {
						attributJson.put("value", jsonObjectOverView.get(dataAttribute).toString());
						attributJson.put("source", source);
						attributJson.put("sourceDisplayName", sourceDisplayName);
						attributJson.put("credibility", credibility);
						attributJson.put("sourceUrl", sourceUrl);
					}
					// this loop fixes EL-5570 Fix the issue with "value"  of overriden source
					if (!attributJson.getBoolean("isUserData")
							&& attributJson.has("source") && attributJson.getString("source").equals(source)) {
						attributJson.put("value", jsonObjectOverView.get(dataAttribute).toString());
						attributJson.put("valueRef", jsonObjectOverView.get(dataAttribute).toString());
						if(attributJson.has("primarySource")) {
							JSONArray primaryArray=attributJson.getJSONArray("primarySource");
							if(primaryArray!=null && primaryArray.length()>0) {
								for(int d=0;d<primaryArray.length();d++) {
									JSONObject primaryObject= primaryArray.getJSONObject(d);
									if(primaryObject.has("source")) {
										String primarySource=primaryObject.getString("source");
										if(primarySource.equalsIgnoreCase(attributJson.getString("source"))) {
											primaryObject.put("value", jsonObjectOverView.get(dataAttribute).toString());
										}
									}
								}
							}
						}
						if(attributJson.has("secondarySource")) {
							JSONArray secondaryArray=attributJson.getJSONArray("secondarySource");
							if(secondaryArray!=null && secondaryArray.length()>0) {
								for(int d=0;d<secondaryArray.length();d++) {
									JSONObject secondaryObject= secondaryArray.getJSONObject(d);
									if(secondaryObject.has("source")) {
										String secondarySource=secondaryObject.getString("source");
										if(secondarySource.equalsIgnoreCase(attributJson.getString("source"))) {
											secondaryObject.put("value", jsonObjectOverView.get(dataAttribute).toString());
										}
									}
								}
							}
						}
					}
					//
					if (!sourceRef.equalsIgnoreCase(sourceUrl)){
						JSONObject sourceObj=new JSONObject();
						sourceObj.put("source", source);
						sourceObj.put("sourceDisplayName", sourceDisplayName);
						sourceObj.put("sourceUrl", sourceUrl);
						sourceObj.put("value", jsonObjectOverView.get(dataAttribute).toString());
						attributJson.getJSONArray(sourceType).put(sourceObj);
					}
					if (!"".equalsIgnoreCase(sourceRef) && attributJson.has("conflicts")) {
						// attributJson.getJSONArray(sourceType).put(sourceUrl);
						if (!sourceRef.equalsIgnoreCase(sourceUrl)
								&& !jsonObjectOverView.get(dataAttribute).toString().equalsIgnoreCase(valueRef)) {
							JSONArray conflicts = attributJson.getJSONArray("conflicts");
							JSONObject conflict = new JSONObject();
							conflict.put("source1", attributJson.getString("sourceRef"));
							conflict.put("value1", valueRef);
							conflict.put("source2", sourceUrl);
							conflict.put("value2", jsonObjectOverView.get(dataAttribute).toString());
							conflicts.put(conflict);
						}
					} else {
						if ("".equalsIgnoreCase(sourceRef)) {
							attributJson.put("sourceRef", sourceUrl);
							attributJson.put("valueRef", jsonObjectOverView.get(dataAttribute).toString());
						}
					}
				}
				
			} else if (!dataAttribute.equalsIgnoreCase("mdaas:RegisteredAddress")
					&& !dataAttribute.equalsIgnoreCase("@is-manual")
					&& !dataAttribute.equalsIgnoreCase("CountryOperations")
					&& !dataAttribute.equalsIgnoreCase("lei:legalForm")
					&& !dataAttribute.equalsIgnoreCase("bst:stock_info")) {
				//if(!"".equalsIgnoreCase(jsonObjectOverView.get(dataAttribute).toString())){
				if(!StringUtils.isEmpty(jsonObjectOverView.get(dataAttribute).toString())){
				jsonObject.put(dataAttribute, new JSONObject());
				JSONObject attributJson = jsonObject.getJSONObject(dataAttribute);
				attributJson.put(otherSourceType, new JSONArray());
				attributJson.put("source", source);
				attributJson.put("sourceRef", sourceUrl);
				attributJson.put("sourceUrl", sourceUrl);
				attributJson.put("sourceDisplayName", sourceDisplayName);
				attributJson.put("value", jsonObjectOverView.get(dataAttribute).toString());
				attributJson.put("valueRef", jsonObjectOverView.get(dataAttribute).toString());
				attributJson.put("conflicts", new JSONArray());
				JSONObject sourceObj=new JSONObject();
				sourceObj.put("source", source);
				sourceObj.put("sourceUrl", sourceUrl);
				sourceObj.put("value", jsonObjectOverView.get(dataAttribute).toString());
				attributJson.put(sourceType, new JSONArray().put(sourceObj));
				attributJson.put("credibility", credibility);
				attributJson.put("isUserData", isUserData);
				attributJson.put("isOverriddenData", false);
			}
			}
		} else {
			if(jsonObjectOverView !=null && jsonObjectOverView.has(dataAttribute) 
					&& !StringUtils.isEmpty(jsonObjectOverView.get(dataAttribute).toString())){
				String key =null;
				Map<String, CredibilityEnums> dataAttributesMapNew =null;
				if (credibilityJson != null) {
					JSONArray array = credibilityJson.names();
					if (array != null) {
						for (int i = 0; i < array.length(); i++) {
							if (array.getString(i).equalsIgnoreCase(source)
									|| (credibilityJson.has(array.getString(i)) && source
											.equalsIgnoreCase(credibilityJson.getString(array.getString(i))))) {
								key = array.getString(i);
							}
						}
					}
				}
				if (map != null && map.containsKey(key)) {
					SourcesDto dto = map.get(key);
					if (dto != null) {
						List<ClassificationsDto> classifications = dto.getClassifications();
						for (ClassificationsDto classificationsDto : classifications) {
							if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
								List<SubClassificationsDto> subClassifications = classificationsDto
										.getSubClassifications();
								for (SubClassificationsDto subClassificationsDto : subClassifications) {
									if (subClassificationsDto.getSubClassifcationName().contains("Company")) {
										List<DataAttributesDto> dataAttributes = subClassificationsDto
												.getDataAttributes();
										dataAttributesMapNew = dataAttributes.stream()
												.filter(d -> d.getCredibilityValue() != null)
												.collect(Collectors.toMap(DataAttributesDto::getSourceAttributeSchema,
														DataAttributesDto::getCredibilityValue));
									}
								}
							}
						}
					}
				}
				jsonObject.put(dataAttribute, new JSONObject());
				int credibility=0;
				JSONObject attributJson = jsonObject.getJSONObject(dataAttribute);
				attributJson.put("primarySource", new JSONArray());
				//old code
				/*if(sourceUrl==null){
					attributJson.put("source", "");
					attributJson.put("sourceRef", "");
				}
				else{
					attributJson.put("source", source);
					attributJson.put("sourceRef", sourceUrl);
				}*/
				
				//new code
				if (source == null) {
					attributJson.put("source", "");
				} else {
					attributJson.put("source", source);
				}
				if (sourceUrl == null) {
					attributJson.put("sourceRef", "");
				} else {
					attributJson.put("sourceRef", sourceUrl);
				}
				
				if(sourceDisplayName!=null){
					attributJson.put("sourceDisplayName", sourceDisplayName);
				}else{
					attributJson.put("sourceDisplayName", source);
				}
				if(dataAttributesMapNew!=null && dataAttributesMapNew.containsKey(dataAttribute)){
					credibility=dataAttributesMapNew.get(dataAttribute).ordinal();
				}
				/*if(credibility==3){
				attributJson.put("primarySource", new JSONArray().put(sourceUrl));
				attributJson.put("secondarySource", new JSONArray());
			}else{
				attributJson.put("primarySource", new JSONArray());
				if (!"".equalsIgnoreCase(sourceUrl))
					attributJson.put("secondarySource", new JSONArray().put(sourceUrl));
				else
					attributJson.put("secondarySource", new JSONArray());
			}*/

				//attributJson.put("source", sourceUrl);
				//attributJson.put("sourceRef", sourceUrl);
				attributJson.put("value", jsonObjectOverView.get(dataAttribute).toString());
				attributJson.put("valueRef", jsonObjectOverView.get(dataAttribute).toString());
				attributJson.put("conflicts", new JSONArray());
				if(jsonObjectOverView.has("publishedDate") && jsonObjectOverView.getString("publishedDate")!=null)
					attributJson.put("publishedDate", jsonObjectOverView.getString("publishedDate"));
				if(jsonObjectOverView.has("sourceUrl") && jsonObjectOverView.getString("sourceUrl")!=null)
					attributJson.put("sourceUrl", jsonObjectOverView.getString("sourceUrl"));
		     	attributJson.put("primarySource", new JSONArray());
				attributJson.put("secondarySource", new JSONArray());
				if (!"".equalsIgnoreCase(sourceUrl) && jsonObjectOverView.has("isUserValue")
						&& !jsonObjectOverView.getBoolean("isUserValue")) {
					JSONObject sourceObj = new JSONObject();
					sourceObj.put("source", source);
					sourceObj.put("sourceDisplayName", sourceDisplayName);
					sourceObj.put("sourceUrl", sourceUrl);
					sourceObj.put("value", jsonObjectOverView.get(dataAttribute).toString());
					if (credibility == 3)
						attributJson.put("primarySource", new JSONArray().put(sourceObj));
					else
						attributJson.put("secondarySource", new JSONArray().put(sourceObj));
				}
				attributJson.put("credibility", 3);
				attributJson.put("modifiedBy", jsonObjectOverView.getString("user"));
				attributJson.put("modifiedOn", jsonObjectOverView.getString("modifiedOn"));
				if (jsonObjectOverView.has("preValue") && jsonObjectOverView.getString("preValue") != null)
					attributJson.put("preValue", jsonObjectOverView.getString("preValue"));
				//if ("".equalsIgnoreCase(sourceUrl)) {
				if (jsonObjectOverView.has("isUserValue") && jsonObjectOverView.getBoolean("isUserValue")) {
					attributJson.put("isUserData", isUserData);
					attributJson.put("isOverriddenData", true);
				} else {
					attributJson.put("isUserData", false);
					attributJson.put("isOverriddenData", true);
				}
				if (jsonObjectOverView.has("userValue") && jsonObjectOverView.getString("userValue") != null)
					attributJson.put("userValue", jsonObjectOverView.getString("userValue"));
				if(jsonObjectOverView.has("userPublishedDate") && jsonObjectOverView.getString("userPublishedDate")!=null)
					attributJson.put("userPublishedDate", jsonObjectOverView.getString("userPublishedDate"));
				if(jsonObjectOverView.has("userSourceUrl") && jsonObjectOverView.getString("userSourceUrl")!=null)
					attributJson.put("userSourceUrl", jsonObjectOverView.getString("userSourceUrl"));
				if(jsonObjectOverView.has("userSource") && jsonObjectOverView.getString("userSource")!=null)
					attributJson.put("userSource", jsonObjectOverView.getString("userSource"));
				if(jsonObjectOverView.has("sourceType") && jsonObjectOverView.getString("sourceType")!=null)
					attributJson.put("sourceType", jsonObjectOverView.getString("sourceType"));
				
				if(jsonObjectOverView.has("docId") && jsonObjectOverView.get("docId")!=null)
					attributJson.put("docId", jsonObjectOverView.get("docId"));
			}

		}
	}
	
	//Followig method is not in Use...

	@Deprecated
	@Override
	public String getEntityScreening(String organizationName, String jurisdiction, String entityType, String website,
			String dob, String inputIdentifier, String inputRequestId) throws Exception {
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File pepKeyFile = new File(classLoader.getResource("pepKey.json").getFile());
		Object pepKeyJson = parser.parse(new FileReader(pepKeyFile));
		org.json.simple.JSONObject pepTypes = (org.json.simple.JSONObject) pepKeyJson;
		JSONObject result = new JSONObject();
		// String country = null;
		result.put("country", jurisdiction);
		result.put("name", organizationName);
		result.put("entity_type", entityType);
		if (website != null)
			result.put("website", website);
		if (dob != null)
			result.put("dob", dob);
		try {
			/*
			 * File fileJSon = new
			 * File(classLoader.getResource("BVD_countries.json").getFile());
			 * Object fileJsonData = parser.parse(new FileReader(fileJSon));
			 * org.json.simple.JSONArray countryJson =
			 * (org.json.simple.JSONArray) fileJsonData;
			 * 
			 * for (int k = 0; k < countryJson.size(); k++) {
			 * org.json.simple.JSONObject countryObj =
			 * (org.json.simple.JSONObject) countryJson.get(k); if
			 * (countryObj.get("countryCode").toString().equalsIgnoreCase(
			 * jurisdiction)) { country = countryObj.get("country").toString();
			 * } } result.put("country", country);
			 */
			JSONArray classifications = new JSONArray();
			String identifier = null;
			String requestId = null;
			if (inputIdentifier != null && !"".equalsIgnoreCase(inputIdentifier)) {
				identifier = inputIdentifier;
			} else {
				identifier = UUID.randomUUID().toString();
			}
			if (inputRequestId != null && !"".equalsIgnoreCase(inputRequestId)) {
				requestId = inputRequestId;
			} else {
				requestId = UUID.randomUUID().toString();
			}
			result.put("requestId", requestId);
			result.put("identifier", identifier);
			JSONObject finalJsonTosent = new JSONObject();
			JSONArray personEntities = new JSONArray();
			JSONArray orgEntities = new JSONArray();
			JSONObject orgJson = new JSONObject();
			JSONObject personJson = new JSONObject();
			result.put("entity_ref_id", identifier);
			result.put("screeningUrl", SCREENING_URL + "/"
					+ URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20") + "/" + requestId);
			JSONObject jsonToSent = new JSONObject();
			jsonToSent.put("entity_id", identifier);
			jsonToSent.put("jurisdiction", jurisdiction);
			jsonToSent.put("names", new JSONArray().put(organizationName));
			if ("organization".equalsIgnoreCase(entityType))
				orgEntities.put(jsonToSent);
			else
				personEntities.put(jsonToSent);
			if (personEntities.length() > 0)
				personJson.put("entities", personEntities);
			if (orgEntities.length() > 0)
				orgJson.put("entities", orgEntities);
			finalJsonTosent.put("max_responses", 10);
			// finalJsonTosent.put("min_confidence", 0.2);
			if (personJson.has("entities"))
				finalJsonTosent.put("person", personJson);
			if (orgJson.has("entities"))
				finalJsonTosent.put("organization", orgJson);
			if (inputIdentifier == null || inputRequestId == null) {
				sendData(identifier, requestId, finalJsonTosent.toString());
			}
			String screeningResponse = getData(identifier, requestId);
			JSONObject watchlists = new JSONObject();
			JSONObject newsJson = new JSONObject();
			if (screeningResponse != null) {
				JSONObject screeningJson = new JSONObject(screeningResponse);
				if (screeningJson.has("news")) {
					newsJson = screeningJson.getJSONObject("news");
					if (newsJson.has("status")) {
						int counter = 1;
						while ("In Progress".equalsIgnoreCase(newsJson.getString("status"))) {
							screeningResponse = getData(identifier, requestId);
							if (screeningResponse != null) {
								try {
									screeningJson = new JSONObject(screeningResponse);
								} catch (Exception e) {
								}
							}
							if (screeningJson != null && screeningJson.has("news")) {
								newsJson = screeningJson.getJSONObject("news");
							} else {
								break;
							}
							counter = counter + 1;
							if (counter == 6)
								break;
							// Thread.sleep(5000);
						}
					}
				}

				JSONObject watchlistJson = new JSONObject();
				if (screeningJson.has("watchlists")) {
					watchlists = screeningJson.getJSONObject("watchlists");
					if (watchlists.has("status")) {
						String status = watchlists.getString("status");
						int chunksCounter = 1;
						while (!"done".equalsIgnoreCase(status)) {
							screeningResponse = getData(identifier, requestId);
							if (screeningResponse != null) {
								try {
									screeningJson = new JSONObject(screeningResponse);
								} catch (Exception e) {
								}
								if (screeningJson.has("watchlists")) {
									watchlists = screeningJson.getJSONObject("watchlists");

								}
							}
							chunksCounter = chunksCounter + 1;
							if (chunksCounter == 6)
								break;
							// Thread.sleep(5000);

						}
					}
					if (watchlists.has("chunks")) {
						JSONArray chunks = watchlists.getJSONArray("chunks");
						if (chunks != null) {
							for (int i = 0; i < chunks.length(); i++) {
								JSONObject chunk = null;
								if (chunks.get(i) instanceof JSONObject)
									chunk = chunks.getJSONObject(i);
								if (chunk != null && chunk.has("_links")) {
									JSONObject links = chunk.getJSONObject("_links");
									String link = null;
									if (links.has("response")) {
										link = links.getString("response");
									}
									if (link != null) {
										HierarchyDto hierarchyDto = new HierarchyDto();
										hierarchyDto.setUrl(link);
										String response = getHierarchyData(hierarchyDto, null,null);
										JSONObject json = null;
										if (response != null)
											json = new JSONObject(response);
										if (json != null && json.has("response")) {
											JSONArray responseJson = json.getJSONArray("response");
											if (responseJson != null && responseJson.length() > 0) {
												for (int j = 0; j < responseJson.length(); j++) {
													JSONObject watchlist = responseJson.getJSONObject(j);
													watchlistJson.put(watchlist.getString("entity_id"), watchlist);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				// for (JSONObject jsonObject : levelwiseList) {
				JSONArray peps = new JSONArray();
				JSONArray sanctions = new JSONArray();
				String entityRefId = result.getString("entity_ref_id");
				JSONObject entityNews = null;
				if (newsJson.has(entityRefId))
					entityNews = newsJson.getJSONObject(entityRefId);
				if (entityNews != null && entityNews.has("classifications"))
					classifications = entityNews.getJSONArray("classifications");
				else
					classifications = new JSONArray();
				if (watchlistJson.has(entityRefId)) {
					JSONObject object = watchlistJson.getJSONObject(entityRefId);
					if (object.has("hits")) {
						JSONArray hits = object.getJSONArray("hits");
						for (int i = 0; i < hits.length(); i++) {
							JSONArray pepsEntries = new JSONArray();
							JSONArray sanctionEntries = new JSONArray();
							JSONObject pepJson = new JSONObject();
							JSONObject sanctionJson = new JSONObject();
							JSONObject hit = hits.getJSONObject(i);
							if (hit.has("entries")) {
								JSONArray entries = hit.getJSONArray("entries");

								for (int j = 0; j < entries.length(); j++) {
									JSONObject entry = entries.getJSONObject(j);
									if (entry.has("watchlist_id")) {
										String watchlistId = entry.getString("watchlist_id");
										if (pepTypes.containsKey(watchlistId)) {
											String watchlistType = pepTypes.get(watchlistId).toString();
											if ("PEP".equalsIgnoreCase(watchlistType)) {
												pepsEntries.put(entry);
											} else {
												sanctionEntries.put(entry);
											}
										}
									}
								}
								if (pepsEntries.length() > 0) {
									if (hit.has("value"))
										pepJson.put("value", hit.getString("value"));
									if (hit.has("confidence"))
										pepJson.put("confidence", hit.getDouble("confidence"));
									pepJson.put("entries", pepsEntries);
									peps.put(pepJson);
								}
								if (sanctionEntries.length() > 0) {
									if (hit.has("value"))
										sanctionJson.put("value", hit.getString("value"));
									if (hit.has("confidence"))
										sanctionJson.put("confidence", hit.getDouble("confidence"));
									sanctionJson.put("entries", sanctionEntries);
									sanctions.put(sanctionJson);
								}
							}
							// peps.put(pepJson);
							// sanctions.put(sanctionJson);
						}
					}
					result.put("pep", peps);
					result.put("sanctions", sanctions);
				}
				if ((newsJson.has("status") && watchlists.has("status")) || !screeningJson.has("news")) {
					if (((newsJson.has("status") && "done".equalsIgnoreCase(newsJson.getString("status")))
							&& "done".equalsIgnoreCase(watchlists.getString("status")))
							|| (!screeningJson.has("news")
									&& "done".equalsIgnoreCase(watchlists.getString("status")))) {
						result.put("status", "done");
					} else {
						result.put("status", "pending");
					}
					result.put("news", classifications);
					result.put("screeningFlag", true);
					// }
				}
			}
		} catch (Exception e) {

		}
		return result.toString();
	}

	public static synchronized void writeFile(File file, JSONArray array) throws IOException {
		FileWriter fileWriter = new FileWriter(file);
		fileWriter.write(array.toString());
		fileWriter.close();
	}

	public void checkParent(JSONObject map, JSONObject prevJsonObject, JSONObject mapJson, Integer level, int j,
			List<List<JSONObject>> allShareholders, JSONObject persons, JSONObject tempJson, JSONObject holders, double totalPercentage, double indirectPercentage) {
		if (persons.has(mapJson.getString("identifier"))) {
			JSONObject person = persons.getJSONObject(mapJson.getString("identifier"));
			JSONArray indirectChilds = person.getJSONArray("indirectChilds");
			JSONArray parents = new JSONArray();
			if (map.has(mapJson.getString("id"))) {
				JSONObject prevPerson = map.getJSONObject(mapJson.getString("id"));
				JSONObject childObj = map.getJSONObject(prevPerson.getString("child"));
				JSONArray childParents = childObj.getJSONArray("parents");
				for (int i = 0; i < childParents.length(); i++) {
					if (!mapJson.getString("id").equalsIgnoreCase(childParents.getString(i))) {
						parents.put(childParents.getString(i));
					}
				}
				map.getJSONObject(prevPerson.getString("child")).put("parents", parents);
				/*
				 * boolean isIndirect = false; for (int i = 0; i <
				 * indirectChilds.length(); i++) {
				 * if(indirectChilds.getJSONObject(i).getString("id").
				 * equalsIgnoreCase(childObj.getString("id"))){ isIndirect =
				 * true; break; } if
				 * (childObj.getString("id").equalsIgnoreCase(indirectChilds.
				 * getString(i))) { isIndirect = true; break; } }
				 */
				/*
				 * if (!isIndirect){ JSONObject indirectObject=new JSONObject();
				 * indirectObject.put("id", childObj.getString("id"));
				 * indirectObject.put("totalPercentage",
				 * mapJson.getDouble("previousTotalPercentage"));
				 * indirectObject.put("indirectPercentage",
				 * mapJson.getDouble("previousIndirectPercentage"));
				 * indirectObject.put("level", childObj.getInt("level"));
				 * indirectChilds.put(indirectObject);
				 * //indirectChilds.put(childObj.getString("id")); }
				 */
			}
			// person.put("indirectChilds", indirectChilds);
			// mapJson.put("indirectChilds", indirectChilds);
		}
		if (map.has(prevJsonObject.getString("id"))) {
			boolean isParent = false;
			if (persons.has(mapJson.getString("identifier"))) {
				JSONObject person = persons.getJSONObject(mapJson.getString("identifier"));
				JSONArray indirectChilds = person.getJSONArray("indirectChilds");
				// JSONArray newIndirectChilds=new JSONArray();
				for (int i = 0; i < indirectChilds.length(); i++) {
					if (map.has(indirectChilds.getJSONObject(i).getString("id"))) {
						if (prevJsonObject.getInt("level") < (indirectChilds.getJSONObject(i).getInt("level"))) {
							map.getJSONObject(indirectChilds.getJSONObject(i).getString("id")).getJSONArray("parents")
							.put(mapJson.getString("id"));
							mapJson.put("child", indirectChilds.getJSONObject(i).getString("id"));
							isParent = true;
							for (int k = 0; k < indirectChilds.length(); k++) {
								if (indirectChilds.getJSONObject(k).getString("id")
										.equalsIgnoreCase(indirectChilds.getJSONObject(i).getString("id")))
									indirectChilds.getJSONObject(k).put("isDirect", true);
								else
									indirectChilds.getJSONObject(k).put("isDirect", false);
								/// newIndirectChilds.put(indirectChilds.getJSONObject(k));
							}
							// mapJson.put("indirectChilds", newIndirectChilds);
							break;
						}
					}
				}
				if (!isParent) {
					if(holders!=null){
						if (holders.has(prevJsonObject.getString("identifier"))) {
							holders.getJSONObject(prevJsonObject.getString("identifier")).getJSONArray("parents")
							.put(mapJson.getString("id"));
						}
					}else{
						prevJsonObject.getJSONArray("parents").put(mapJson.getString("id"));
					}
					if(map.has(mapJson.getString("id"))){
						map.getJSONObject(mapJson.getString("id")).put("level", prevJsonObject.getInt("level")+1);
						map.getJSONObject(mapJson.getString("id")).put("child",prevJsonObject.getString("id"));
						map.getJSONObject(mapJson.getString("id")).put("childIdentifier",prevJsonObject.getString("bvdId"));
						map.getJSONObject(mapJson.getString("id")).put("childLevel",prevJsonObject.getInt("level"));

					}
					mapJson.put("child", prevJsonObject.getString("id"));
					mapJson.put("childIdentifier", prevJsonObject.getString("bvdId"));
					for (int k = 0; k < indirectChilds.length(); k++) {
						if (indirectChilds.getJSONObject(k).getString("id")
								.equalsIgnoreCase(prevJsonObject.getString("id")))
							indirectChilds.getJSONObject(k).put("isDirect", true);
						else
							indirectChilds.getJSONObject(k).put("isDirect", false);
						/// newIndirectChilds.put(indirectChilds.getJSONObject(k));
					}
				}
			} else {
				JSONObject indirectChild=new JSONObject();
				indirectChild.put("id", prevJsonObject.getString("id"));
				//indirectChild.put("totalPercentage", mapJson.getDouble("totalPercentage"));
				//indirectChild.put("indirectPercentage", mapJson.getDouble("indirectPercentage"));
				indirectChild.put("totalPercentage",totalPercentage);
				indirectChild.put("indirectPercentage",indirectPercentage);
				if(tempJson.has("indirectChilds")){
					tempJson.getJSONArray("indirectChilds").put(indirectChild);
					tempJson.put("child", prevJsonObject.getString("id"));
					tempJson.put("childIdentifier", prevJsonObject.getString("bvdId"));
					tempJson.put("childLevel", prevJsonObject.getInt("level"));
					tempJson.put("level", prevJsonObject.getInt("level")+1);
				}else if(mapJson.has("indirectChilds")){
					mapJson.getJSONArray("indirectChilds").put(indirectChild);
				}else{
					mapJson.put("indirectChilds",new JSONArray().put(indirectChild));
				}
				if(holders!=null){
					if (holders.has(prevJsonObject.getString("identifier"))) {
						holders.getJSONObject(prevJsonObject.getString("identifier")).getJSONArray("parents")
						.put(mapJson.getString("id"));
					}
				}else{
					prevJsonObject.getJSONArray("parents").put(mapJson.getString("id"));
				}
				//prevJsonObject.getJSONArray("parents").put(mapJson.getString("id"));
				mapJson.put("child", prevJsonObject.getString("id"));
				mapJson.put("childIdentifier", prevJsonObject.getString("bvdId"));
			}
		} else {
			String child = prevJsonObject.getString("child");
			for (List<JSONObject> list : allShareholders) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject json = list.get(i);
					if (json.getString("id").equalsIgnoreCase(child))
						checkParent(map, json, mapJson, level - 1, j, allShareholders, persons,tempJson,holders,json.getDouble("totalPercentage"),json.getDouble("indirectPercentage"));
				}
			}
		}
	}

	public String sendData(String identifier, String requestId, String jsonToBeSent) throws Exception {
		String url = SCREENING_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (requestId != null)
			url = url + "/" + requestId;
		if(CLIENT_ID!=null)
			url=url+"?client_id="+CLIENT_ID;
	//	String serverResponse[] = ServiceCallHelper.updateDataInServerWithApiKey(url, jsonToBeSent, BE_ENTITY_API_KEY);
		String serverResponse[] = ServiceCallHelper.postStringDataToServer(url, jsonToBeSent);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}

	public String getData(String identifier, String requestId) throws Exception {
		String url = SCREENING_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (requestId != null)
			url = url + "/" + requestId;
		if(CLIENT_ID!=null)
			url=url+"?client_id="+CLIENT_ID;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}

	@SuppressWarnings("unchecked")
	public void getSignificantPep(org.json.simple.JSONObject json, String mainEntityId, String mainEntityName,
			String identifier, String name, boolean isOfficer, List<SignificantWatchList> significantWatchLists) {
		if (json.containsKey("pep")) {
			org.json.simple.JSONArray peps = (org.json.simple.JSONArray) json.get("pep");
			if (peps != null) {
				for (int j = 0; j < peps.size(); j++) {
					org.json.simple.JSONObject pep = (org.json.simple.JSONObject) peps.get(j);
					if (pep.containsKey("value")) {
						String value = pep.get("value").toString();
						Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
								.filter(watch -> watch.getMainEntityId().equalsIgnoreCase(mainEntityId)
										&& watch.getEntityName().equalsIgnoreCase(mainEntityName)
										&& watch.getIdentifier().equalsIgnoreCase(identifier)
										&& watch.getName().equalsIgnoreCase(name)
										&& watch.getValue().equalsIgnoreCase(value))
								.findFirst();
						SignificantWatchList significantWatchList = matchingObject.orElse(null);
						/*
						 * SignificantWatchList significantWatchList =
						 * significantWatchListService
						 * .getSignificantWatchList(mainEntityId, identifier,
						 * mainEntityName, value, name);
						 */
						if (significantWatchList != null) {
							pep.put("pep", significantWatchList.isPep());
							pep.put("significantId", significantWatchList.getId());
						}
					}

				}
			}
		}
		if (json.containsKey("sanctions")) {
			org.json.simple.JSONArray sanctions = (org.json.simple.JSONArray) json.get("sanctions");
			if (sanctions != null) {
				for (int j = 0; j < sanctions.size(); j++) {
					org.json.simple.JSONObject sanction = (org.json.simple.JSONObject) sanctions.get(j);
					String value = null;
					if (sanction.containsKey("value")) {
						if (sanction.get("value") != null)
							value = sanction.get("value").toString();
						SignificantWatchList significantWatchList = significantWatchListService
								.getSignificantWatchList(mainEntityId, identifier, mainEntityName, value, name);
						if (significantWatchList != null) {
							sanction.put("sanction", significantWatchList.isSanction());
							sanction.put("significantId", significantWatchList.getId());
						}
					}

				}
			}
		}
	}

	public String getDataInAsynMode(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("is-completed")) {
					int counter = 0;
					while (json.has("is-completed") && !json.getBoolean("is-completed")) {
						serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
						if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
							response = serverResponse[1];
							if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
								json = new JSONObject(response);
							}
						}
						counter++;
						if (counter == 6)
							break;
						Thread.sleep(5000);
					}
				}
			} else {
				return null;
			}
		} else {
			return response;
		}
		return response;
	}

	@Override
	public String selectEntity( String identifier, String requestId,String sourceType) throws Exception {
		String url = BIGDATA_MULTISOURCE_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20")+"/"+requestId+"/select?";
				
		if(CLIENT_ID!=null && sourceType!=null) {
			url=url+sourceType+"="+URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20")+"&client_id="+CLIENT_ID;
		}
		return getDataInAsynMode(url);
	}
	@Override
	public String selectEntityUrl(String selectEntityUrl) throws Exception {
		if(selectEntityUrl!=null)
			return getDataInAsynMode(selectEntityUrl);
		else
			return null;
	}
	

	@Override
	public void getMergedResults(JSONArray mergedPeps, JSONArray peps,String mainIdentifier,boolean isPep) {
		List<SignificantWatchList> significantWatchLists = significantWatchListService
				.fetchWtachListByEntityId(mainIdentifier);
		
		for (int i = 0; i < peps.length(); i++) {
			if (mergedPeps.length() == 0) {
				JSONObject pep = peps.getJSONObject(i);
				Double confidence=0.0;
				if(pep.has("confidence")){
					confidence=pep.getDouble("confidence");
				}
				if (pep.has("entries")) {
					JSONArray entries = pep.getJSONArray("entries");
					JSONObject entry = entries.getJSONObject(0);
					JSONArray attribute_name = new JSONArray();
					JSONObject attributeJson = new JSONObject();
					if (entry.has("attribute_name") && (entry.get("attribute_name") instanceof String)
							&& !"".equalsIgnoreCase(entry.getString("attribute_name"))) {
						attributeJson.put("attribute_name", entry.getString("attribute_name"));
						attributeJson.put("confidence", confidence);
						attribute_name.put(attributeJson);
						pep.getJSONArray("entries").getJSONObject(0).put("attribute_name", attribute_name);
						mergedPeps.put(pep);
					}
				}
				if (isPep) {
					Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
							.filter(watch -> watch.getValue().equalsIgnoreCase(pep.getString("value")))
							//.filter(watch -> watch.isPep())
							.findFirst();
					SignificantWatchList significantWatchList = matchingObject.orElse(null);
					if (significantWatchList != null) {
						if(significantWatchList.isPep())
							pep.put("pep", true);
						pep.put("significantId", significantWatchList.getId());
						pep.put("comments",significantCommentService.getSignificantComments(null, significantWatchList.getId()));
					}
				}
				else {
					Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
							.filter(watch -> watch.getValue().equalsIgnoreCase(pep.getString("value")))
							//.filter(watch -> watch.isSanction())
							.findFirst();
					SignificantWatchList significantWatchList = matchingObject.orElse(null);
					if (significantWatchList != null) {
						if(significantWatchList.isSanction())
							pep.put("sanctions", true);
						pep.put("significantId", significantWatchList.getId());
						pep.put("comments",significantCommentService.getSignificantComments(null, significantWatchList.getId()));
					}
				}
			} else {
				JSONObject pep = peps.getJSONObject(i);
				if (pep.has("entries")) {
					JSONArray entries = pep.getJSONArray("entries");
					JSONObject entry = entries.getJSONObject(0);
					boolean isMerged = false;
					for (int j = 0; j < mergedPeps.length(); j++) {
						JSONObject mergedPep = mergedPeps.getJSONObject(j);
						if (mergedPep.has("entries")) {
							JSONArray mergedentrire = mergedPep.getJSONArray("entries");
							JSONObject mergedentry = mergedentrire.getJSONObject(0);
							if ((mergedentry.has("entry_id") && entry.has("entry_id"))
									&& (mergedentry.getString("entry_id").equalsIgnoreCase(entry.getString("entry_id")))
									&& (mergedentry.has("watchlist_id") && entry.has("watchlist_id"))
									&& (mergedentry.getString("watchlist_id")
											.equalsIgnoreCase(entry.getString("watchlist_id")))) {
								isMerged = true;
								if (entry.has("attribute_name")
										&& !"".equalsIgnoreCase(entry.getString("attribute_name"))) {
									boolean isAlreadyInArray = false;
									for (int k = 0; k < mergedentry.getJSONArray("attribute_name").length(); k++) {
										if (mergedentry.getJSONArray("attribute_name").getJSONObject(k).getString("attribute_name")
												.equalsIgnoreCase(entry.getString("attribute_name"))) {
											isAlreadyInArray = true;
										}
									}
									if (!isAlreadyInArray){
										JSONObject attributeJson=new JSONObject();
										attributeJson.put("attribute_name", entry.getString("attribute_name"));
										if( pep.has("confidence"))
											attributeJson.put("confidence", pep.getDouble("confidence"));
										else
											attributeJson.put("confidence", 0.0);
										mergedentry.getJSONArray("attribute_name")
										.put(attributeJson);
										if (mergedPep.has("confidence") && pep.has("confidence")) {
											mergedPep.put("confidence", (mergedPep.getDouble("confidence")+pep.getDouble("confidence"))/2);
										}

									}
								}
								/*if (mergedPep.has("confidence") && pep.has("confidence")) {

									//if (mergedPep.getDouble("confidence") < pep.getDouble("confidence")) {
										mergedPep.put("confidence", (mergedPep.getDouble("confidence")+pep.getDouble("confidence"))/2);
									//}

								}*/
							}
						}
						if (isPep) {
							Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
									.filter(watch -> watch.getValue().equalsIgnoreCase(mergedPep.getString("value")))
									//.filter(watch -> watch.isPep())
									.findFirst();
							SignificantWatchList significantWatchList = matchingObject.orElse(null);
							if (significantWatchList != null) {
								if(significantWatchList.isPep())
									mergedPep.put("pep", true);
								mergedPep.put("significantId", significantWatchList.getId());
								mergedPep.put("comments",significantCommentService.getSignificantComments(null, significantWatchList.getId()));
							}
						}
						else {
							Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
									.filter(watch -> watch.getValue().equalsIgnoreCase(mergedPep.getString("value")))
									//.filter(watch -> watch.isSanction())
									.findFirst();
							SignificantWatchList significantWatchList = matchingObject.orElse(null);
							if (significantWatchList != null) {
								if(significantWatchList.isSanction())
									mergedPep.put("sanctions", true);
								mergedPep.put("significantId", significantWatchList.getId());
								mergedPep.put("comments",significantCommentService.getSignificantComments(null, significantWatchList.getId()));
							}
						}
					}
					if (!isMerged) {
						JSONArray attribute_name = new JSONArray();
						JSONObject attributeJson = new JSONObject();
						if (entry.has("attribute_name") && (entry.get("attribute_name") instanceof String)
								&& !"".equalsIgnoreCase(entry.getString("attribute_name"))) {
							attributeJson.put("attribute_name", entry.getString("attribute_name"));
							attributeJson.put("confidence", pep.getDouble("confidence"));
							attribute_name.put(attributeJson);
							pep.getJSONArray("entries").getJSONObject(0).put("attribute_name", attribute_name);
							mergedPeps.put(pep);
						}
						if (isPep) {
							Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
									.filter(watch -> watch.getValue().equalsIgnoreCase(pep.getString("value")))
									//.filter(watch -> watch.isPep())
									.findFirst();
							SignificantWatchList significantWatchList = matchingObject.orElse(null);
							if (significantWatchList != null) {
								if(significantWatchList.isPep())
									pep.put("pep", true);
								pep.put("significantId", significantWatchList.getId());
								pep.put("comments",significantCommentService.getSignificantComments(null, significantWatchList.getId()));
							}
						}
						else {
							Optional<SignificantWatchList> matchingObject = significantWatchLists.stream()
									.filter(watch -> watch.getValue().equalsIgnoreCase(pep.getString("value")))
									//.filter(watch -> watch.isSanction())
									.findFirst();
							SignificantWatchList significantWatchList = matchingObject.orElse(null);
							if (significantWatchList != null) {
								if(significantWatchList.isSanction())
									pep.put("sanctions", true);
								pep.put("significantId", significantWatchList.getId());
								pep.put("comments",significantCommentService.getSignificantComments(null, significantWatchList.getId()));
							}
						}
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService#getCountriesOfOperation(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService#getCountriesOfOperation(java.lang.String)
	 */
	@SuppressWarnings("unused")
	public String getCountriesOfOperation(String identifier, JSONObject mainJsonObject, String mainJurisdiction, String mainCountry, Map<String, SourcesDto> map) throws Exception {

		JSONArray resultantCountries = new JSONArray();
		String url = BIGDATA_MULTISOURCE_URL + identifier + "?fields=branches";
		String branchUrlResponse = getDataInAsynMode(url);
		if (branchUrlResponse != null) {
			JSONObject branchResponseJson = new JSONObject(branchUrlResponse);
			if ((branchResponseJson.has("results")) && (branchResponseJson.get("results") instanceof JSONArray)) {
				JSONArray resultArray = branchResponseJson.getJSONArray("results");
				JSONObject resultObject = new JSONObject();
				if(resultArray!=null && resultArray.length()>0)
					resultObject = resultArray.getJSONObject(0);
				if (resultObject!=null && resultObject.has("branches")) {
					JSONObject branchesObject = resultObject.getJSONObject("branches");
					String[] sources = JSONObject.getNames(branchesObject);
					if(sources!=null && sources.length>0){
						for (String source : sources) {
							JSONArray sourceObjectArray = branchesObject.getJSONArray(source);
							if(sourceObjectArray!=null){
								for (int i = 0; i < sourceObjectArray.length(); i++) {
									String country = "";
									String jurisdiction = "";
									List<String> newSourceArray = new ArrayList<String>();
									JSONObject newCountryObject = new JSONObject();
									JSONObject sourceObject = sourceObjectArray.getJSONObject(i);
							
									//Jurisdiction and Country value from "basic" Json Object
									if(sourceObject!=null && sourceObject.has("basic")){
										JSONObject basicObject =new JSONObject();
										basicObject = sourceObject.getJSONObject("basic");

										// jurisdiction
										if (basicObject!=null && basicObject.has("isDomiciledIn")
												&& basicObject.get("isDomiciledIn") instanceof String) {
											jurisdiction = basicObject.getString("isDomiciledIn");
										}
										// country
										JSONObject addressObject = new JSONObject();
										if (basicObject!=null && basicObject.has("mdaas:RegisteredAddress")) {
											addressObject = basicObject.getJSONObject("mdaas:RegisteredAddress");
											if (addressObject.has("country") && addressObject.get("country") instanceof String) {
												country = addressObject.getString("country");
											}
										}
									}else{
										// jurisdiction from sourceObject
										if (sourceObject!=null && sourceObject.has("isDomiciledIn")
												&& sourceObject.get("isDomiciledIn") instanceof String) {
											jurisdiction = sourceObject.getString("isDomiciledIn");
										}

										// country from sourceObject
										JSONObject addressObject = new JSONObject();
										if (sourceObject!=null && sourceObject.has("mdaas:RegisteredAddress")) {
											addressObject = sourceObject.getJSONObject("mdaas:RegisteredAddress");
											if (addressObject.has("country") && addressObject.get("country") instanceof String) {
												country = addressObject.getString("country");
											}
										}
									}
									
									
									if (resultantCountries != null && resultantCountries.length() > 0) {
										boolean found = false;
										for (int j = 0; j < resultantCountries.length(); j++) {
											JSONObject resultantcountryObj = resultantCountries.getJSONObject(j);
											if (resultantcountryObj.has("country") && resultantcountryObj.has("jurisdiction")
													&& resultantcountryObj.has("sources")) {
												JSONArray sourcesArrayObj = resultantcountryObj.getJSONArray("sources");
												List<String> sourcesData = new ArrayList<String>();
												for (int k = 0; k < sourcesArrayObj.length(); k++) {
													sourcesData.add(sourcesArrayObj.getString(k));
												}
												if (resultantcountryObj.has("country") && country.equals(resultantcountryObj.getString("country")) &&
														resultantcountryObj.has("jurisdiction") && jurisdiction.equals(resultantcountryObj.getString("jurisdiction"))) {
													found = true;
													if (!sourcesData.contains(source)) {
														resultantcountryObj.getJSONArray("sources").put(source);
													}
													break;
												}

											}
										}
										if (!found && !StringUtils.isEmpty(country) && !StringUtils.isEmpty(jurisdiction)) {
											newCountryObject.put("jurisdiction", jurisdiction);
											newCountryObject.put("country", country);
											newSourceArray.add(source);
											newCountryObject.put("sources", newSourceArray);
											resultantCountries.put(newCountryObject);
										}

									} else {
										List<String> firstsourceArray = new ArrayList<String>();
										JSONObject firstcountryObject = new JSONObject();
										firstcountryObject.put("jurisdiction", jurisdiction);
										firstcountryObject.put("country", country);
										firstsourceArray.add(source);
										firstcountryObject.put("sources", firstsourceArray);
										resultantCountries.put(firstcountryObject);
									}
								}
							}
						}
					}
				}
			}
		}
		boolean isMainCountryFound = false;
		for (int i = 0; i < resultantCountries.length(); i++) {
			JSONObject jsonObject = resultantCountries.getJSONObject(i);
			if (jsonObject.has("sources")) {
				JSONArray jsonArray = jsonObject.getJSONArray("sources");
				List<String> primarySource = new ArrayList<String>();
				// JSONObject primarySourceObject = new JSONObject();
				List<String> secondarySource = new ArrayList<String>();
				// JSONObject secondarySourceObject = new JSONObject();
				boolean isPrimary = false;
				for (int j = 0; j < jsonArray.length(); j++) {
					if (map.containsKey(jsonArray.get(j))) {
						SourcesDto sourcesDto = map.get(jsonArray.get(j));
						List<ClassificationsDto> classifications = sourcesDto.getClassifications();
						for (ClassificationsDto classificationsDto : classifications) {
							if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
								List<SubClassificationsDto> subClassifications = classificationsDto
										.getSubClassifications();
								for (SubClassificationsDto subClassificationsDto : subClassifications) {
									if (subClassificationsDto.getSubClassifcationName().contains("Company")) {
										if (subClassificationsDto.getSubClassificationCredibility()
												.equals(CredibilityEnums.HIGH)) {
											isPrimary = true;
										} else {
											isPrimary = false;
										}
									}
								}
							}
						}
					}
					if (isPrimary) {
						primarySource.add(jsonArray.get(j).toString());
					} else {
						secondarySource.add(jsonArray.get(j).toString());
					}
				}
				jsonObject.put("primarySource", primarySource);
				jsonObject.put("secondarySource", secondarySource);
			}
			// check if the main entity's country exist in branches
			// if not add the main entity's country and jurisdiction the final response
			if(jsonObject.getString("jurisdiction").equalsIgnoreCase(mainJurisdiction) 
					&& jsonObject.getString("country").equalsIgnoreCase(mainCountry)){
				isMainCountryFound = true;
			}
		}
		
		if(resultantCountries.length()<= 0) {
			resultantCountries = getDefaultCountriesOfOperation(resultantCountries, mainJsonObject, mainJurisdiction, mainCountry);
			isMainCountryFound = true;
		}
		// if not add the main entity's country and jurisdiction the final response
		if(isMainCountryFound==false){
			resultantCountries = getDefaultCountriesOfOperation(resultantCountries, mainJsonObject, mainJurisdiction, mainCountry);
		}
		
		return resultantCountries.toString();

	}
	
	public JSONArray getDefaultCountriesOfOperation(JSONArray resultantCountries,JSONObject jsonObject, String jurisdiction, String country) throws Exception {
		//JSONArray resultantCountries = new JSONArray();
		//String country="";
		//String jurisdiction="";
		String source="";
		JSONObject newCountryObject = new JSONObject();
		List<String> newSourceArray = new ArrayList<String>();

		if(jsonObject.has("isDomiciledIn")) {
			JSONObject countryJsonObject = new JSONObject();
			countryJsonObject=(JSONObject) jsonObject.get("isDomiciledIn");
			//jurisdiction=countryJsonObject.getString("value");
			source= countryJsonObject.getString("sourceRef");
		}
		/*if(jsonObject.has("country")) {
			JSONObject countryJsonObject = new JSONObject();
			countryJsonObject=(JSONObject) jsonObject.get("country");
			country=countryJsonObject.getString("value");
		}*/

		if (!StringUtils.isEmpty(country) && !StringUtils.isEmpty(jurisdiction)) {
			newCountryObject.put("jurisdiction", jurisdiction);
			newCountryObject.put("country", country);
			newSourceArray.add(source);
			newCountryObject.put("sources", newSourceArray);
			newCountryObject.put("primarySource", newSourceArray);
			newCountryObject.put("secondarySource",new JSONArray());
			resultantCountries.put(newCountryObject);
		}

		return resultantCountries;
	}
	
	
	

	public void getParents(JSONObject presentJson, JSONObject clientJson, double indirectPercentage, JSONArray parentArray) {
		if (presentJson.has("shareholders")) {
			JSONObject shareholderobj = presentJson.getJSONObject("shareholders");
			JSONArray parentIds = shareholderobj.names();
			if(parentIds!=null && parentIds.length()>0) {
			for (int k = 0; k < parentIds.length(); k++) {
				JSONObject parentObj = new JSONObject();
				JSONObject parentJson = shareholderobj.getJSONObject(parentIds.getString(k));
				parentObj.put("parentId", parentIds.getString(k));
					if (parentJson.has("totalPercentage")) {
						if (parentJson.get("totalPercentage") instanceof Double)
							parentObj.put("totalPercentage", parentJson.getDouble("totalPercentage"));
						if (parentJson.get("totalPercentage") instanceof String
								&& !StringUtils.isEmpty(parentJson.getString(("totalPercentage"))))
							parentObj.put("totalPercentage",
									Double.parseDouble(parentJson.getString("totalPercentage")));
						if (parentJson.get("totalPercentage") instanceof Integer)
							parentObj.put("totalPercentage", parentJson.getInt("totalPercentage"));
					} else
						parentObj.put("totalPercentage", 0.0);
				////
				if(parentJson.has("classification"))
					parentObj.put("classification", parentJson.getString("classification"));
				if (parentJson.has("dateOfBirth"))
					parentObj.put("dateOfBirth", parentJson.getString("dateOfBirth"));
				if (parentJson.has("role"))
					parentObj.put("role", parentJson.getString("role"));
				////
				if (parentJson.has("from") && parentJson.get("from") instanceof String)
					parentObj.put("from", parentJson.getString("from"));
				if (parentJson.has("source") && parentJson.get("source") instanceof String)
					parentObj.put("source", parentJson.getString("source"));
				double nextIndirectPercentage=0.0;
					if (parentObj.has("totalPercentage")) {
						nextIndirectPercentage = (parentObj.getDouble("totalPercentage") * indirectPercentage) / 100;
					}
				parentObj.put("indircetPercentage", nextIndirectPercentage);
				//if (nextIndirectPercentage >= 10 && nextIndirectPercentage <= 100)
				parentArray.put(parentObj);
			}
		}
		}
		if (clientJson.has("shareholders")) {
			JSONObject shareholderobj = clientJson.getJSONObject("shareholders");
			JSONArray parentIds = shareholderobj.names();
			for (int k = 0; k < parentIds.length(); k++) {
				for (int i = 0; i < parentArray.length(); i++) {
					String id=parentArray.getJSONObject(i).getString("parentId");
					if(parentIds.getString(k).equalsIgnoreCase(id+"_"+CLIENT_ID) || parentIds.getString(k).equalsIgnoreCase(id)){
						parentArray.remove(i);
					}
				}
				JSONObject parentObj = new JSONObject();
				JSONObject parentJson = shareholderobj.getJSONObject(parentIds.getString(k));
				String parentId=null;
				if(parentIds.getString(k).contains(CLIENT_ID)){
					String [] str=parentIds.getString(k).split("_");
					parentId=str[0];
				}else{
					parentId=parentIds.getString(k);
				}
				parentObj.put("parentId", parentId);
				if(parentJson.has("basic")){
					parentObj.put("basic", parentJson.getJSONObject("basic"));
				}
				if (parentJson.has("totalPercentage"))
					parentObj.put("totalPercentage", parentJson.getDouble("totalPercentage"));
				else
					parentObj.put("totalPercentage", 0.0);
				if(parentJson.has("user")){
					parentObj.put("user", parentJson.getString("user"));
				}
				if (parentJson.has("from") && parentJson.get("from") instanceof String)
					parentObj.put("from", parentJson.getString("from"));
				if (parentJson.has("classification") && parentJson.get("classification") instanceof String)
					parentObj.put("classification", parentJson.getString("classification"));
				if (parentJson.has("role") && parentJson.get("role") instanceof String)
					parentObj.put("role", parentJson.getString("role"));
				if (parentJson.has("dateOfBirth") && parentJson.get("dateOfBirth") instanceof String)
					parentObj.put("dateOfBirth", parentJson.getString("dateOfBirth"));
				if (parentJson.has("source") && parentJson.get("source") instanceof String)
					parentObj.put("source", parentJson.getString("source"));
				parentObj.put("isCustom", true);
				double nextIndirectPercentage = (parentObj.getDouble("totalPercentage") * indirectPercentage) / 100;
				parentObj.put("indircetPercentage", nextIndirectPercentage);
				parentArray.put(parentObj);
			}
		}
	}

	public void checkEntityType(JSONObject mapJson, String organisationName, String entityType) {
		if ("GB".equalsIgnoreCase(mapJson.getString("jurisdiction")) && "organization".equalsIgnoreCase(entityType)) {
			if (organisationName.toLowerCase().contains("trust"))
				mapJson.put("recognizedEntityType", "Trust");
			else if (organisationName.toLowerCase().contains("foundation"))
				mapJson.put("recognizedEntityType", "Foundation");
			else if (organisationName.toLowerCase().contains("nominee"))
				mapJson.put("recognizedEntityType", "Nominee");
			else if (organisationName.toLowerCase().contains("stichting"))
				mapJson.put("recognizedEntityType", "STAK");
			else if (organisationName.toLowerCase().contains("lp") || organisationName.toLowerCase().contains("llp")) {
				String[] arraySplit = organisationName.split(" ");
				if (arraySplit[arraySplit.length - 1].toLowerCase().contains("lp"))
					mapJson.put("recognizedEntityType", "LP");
				else if (arraySplit[arraySplit.length - 1].toLowerCase().contains("llp"))
					mapJson.put("recognizedEntityType", "LLP");
			} else
				mapJson.put("recognizedEntityType", "Standard");
		}
	}

	@Override
	public String getEntitisScreening(String mainEntityId, String requestId)
			throws Exception {
		JSONObject jsonObject = new JSONObject();
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File pepKeyFile = new File(classLoader.getResource("pepKey.json").getFile());
		Object pepKeyJson = parser.parse(new FileReader(pepKeyFile));
		org.json.simple.JSONObject pepTypes = (org.json.simple.JSONObject) pepKeyJson;
		JSONArray classifications = new JSONArray();
		String screeningResponse = getData(mainEntityId, requestId);
		if (screeningResponse != null) {
			JSONObject checkJson = new JSONObject(screeningResponse);
			if (checkJson.has("status") && checkJson.getBoolean("status")==false) {
				jsonObject = new JSONObject();
				jsonObject.put("message", "Screening Service is not allowed");
				jsonObject.put("isError", true);
				jsonObject.put("errorCode", 403);
				return jsonObject.toString();
			}
		}
		JSONObject newsJson = new JSONObject();
		try {
			if (screeningResponse != null) {
				JSONObject screeningJson = new JSONObject(screeningResponse);
				if (screeningJson.has("news")) {
					newsJson = screeningJson.getJSONObject("news");
					if (newsJson.has("status")) {
						int counter = 1;
						while ((newsJson.has("status") && "In Progress".equalsIgnoreCase(newsJson.getString("status")))
								|| (!newsJson.has("status"))) {
							screeningResponse = getData(mainEntityId, requestId);
							if (screeningResponse != null) {
								try {
									screeningJson = new JSONObject(screeningResponse);
								} catch (Exception e) {
								}
							}
							if (screeningJson != null && screeningJson.has("news")) {
								newsJson = screeningJson.getJSONObject("news");
							} else {
								break;
							}
							counter = counter + 1;
							if (counter == 5)
								break;
							// Thread.sleep(10000);
						}
					}
				}
				
				/**
				 * Commented due to Old Screening-API Response
				 * 
				 */
				
				/*if (!screeningJson.has("watchlists")) {
					int temp = 0;
					while (screeningJson.has("watchlists")) {
						screeningResponse = getData(mainEntityId, requestId);
						if (screeningResponse != null) {
							screeningJson = new JSONObject(screeningResponse);
						}
						temp = temp + 1;
						if (temp == 6)
							break;
						// Thread.sleep(5000);
					}
				}*/
				
				JSONObject watchlistPepJson = new JSONObject();
				JSONObject watchlistSanctionJson = new JSONObject();

				if (screeningJson.has("watchlists")) {
					JSONObject watchlists = screeningJson.getJSONObject("watchlists");
					/*if (watchlists.has("status")) {
						int chunksCounter = 1;
						while (!"done".equalsIgnoreCase(watchlists.getString("status"))) {
							screeningResponse = getData(mainEntityId, requestId);
							if (screeningResponse != null) {
								try {
									screeningJson = new JSONObject(screeningResponse);
								} catch (Exception e) {
								}
								if (screeningJson.has("watchlists")) {
									watchlists = screeningJson.getJSONObject("watchlists");
								}
							}
							chunksCounter = chunksCounter + 1;
							if (chunksCounter == 5)
								break;
							// Thread.sleep(10000);
						}
					}*/
					/*if (watchlists.has("chunks")) {
						JSONArray chunks = watchlists.getJSONArray("chunks");
						if (chunks != null) {
							for (int i = 0; i < chunks.length(); i++) {
								JSONObject chunk = null;
								if (chunks.get(i) instanceof JSONObject)
									chunk = chunks.getJSONObject(i);
								if (chunk != null && chunk.has("_links")) {
									JSONObject links = chunk.getJSONObject("_links");
									String link = null;
									if (links.has("response")) {
										link = links.getString("response");
									}
									if (link != null) {
										HierarchyDto hierarchyDto = new HierarchyDto();
										hierarchyDto.setUrl(link);
										String response = getHierarchyData(hierarchyDto, null,null);
										JSONObject json = null;
										if (response != null)
											json = new JSONObject(response);
										if (json != null && json.has("response")) {
											JSONObject responseJson = json.getJSONObject("response");
											if(responseJson!=null && responseJson.has("pep")){
												JSONArray array=responseJson.getJSONArray("pep");
												if(array!=null){
													for (int j = 0; j < array.length(); j++) {
														watchlistPepJson.put(array.getJSONObject(j).getString("entity_id"),
																array.getJSONObject(j));
													}
												}
											}
											if(responseJson!=null && responseJson.has("sanction")){
												JSONArray sanctionArray=responseJson.getJSONArray("sanction");
												if(sanctionArray!=null){
													for (int j = 0; j < sanctionArray.length(); j++) {
														watchlistSanctionJson.put(sanctionArray.getJSONObject(j).getString("entity_id"),
																sanctionArray.getJSONObject(j));
													}
												}
											}
											//JSONArray responseJson = json.getJSONArray("response");
											if (responseJson != null && responseJson.length() > 0) {
												for (int j = 0; j < responseJson.length(); j++) {
													JSONObject watchlist = responseJson.getJSONObject(j);
													watchlistJson.put(watchlist.getString("entity_id"), watchlist);
												}
											}
										}
									}
								}
							}
						}
					}*/
					if (watchlists.has("pep")) {
						JSONObject pepObject= watchlists.getJSONObject("pep");
						if(pepObject!=null) {
							JSONArray pepSources=pepObject.names();
							if(pepSources!=null && pepSources.length()>0) {
								for(int a=0;a<pepSources.length();a++) {
									JSONObject pepSourceObject=pepObject.getJSONObject(pepSources.getString(a));
									if(pepSourceObject!=null&&pepSourceObject.has("results") && pepSourceObject.get("results") instanceof JSONArray) 
									{
										JSONArray resultsArray=pepSourceObject.getJSONArray("results");
										if(resultsArray!=null && resultsArray.length()>0) {
											for(int b=0;b<resultsArray.length();b++) {
												JSONObject responseJson = resultsArray.getJSONObject(b);
												if(responseJson!=null){
													watchlistPepJson.put(responseJson.getString("entity_id"),
															responseJson);

												}


											}
										}

									}

									/**
									 * Commented due to Old Screening-API Response
									 * 
									 */

									//if(pepSourceObject.has("status")) {
									//int chunksCounter = 1;
									/*while (!"done".equalsIgnoreCase(pepSourceObject.getString("status"))) {
											screeningResponse = getData(mainEntityId, requestId);
											if (screeningResponse != null) {
												try {
													screeningJson = new JSONObject(screeningResponse);
												} catch (Exception e) {
												}
												if (screeningJson.has("watchlists")) {
													watchlists = screeningJson.getJSONObject("watchlists");
												}
											}
											chunksCounter = chunksCounter + 1;
											if (chunksCounter == 5)
												break;
											// Thread.sleep(10000);
										}*/

									/*if(pepSourceObject.has("chunks")) {

											JSONArray chunks = pepSourceObject.getJSONArray("chunks");
											if (chunks != null) {
												for (int i = 0; i < chunks.length(); i++) {
													JSONObject chunk = null;
													if (chunks.get(i) instanceof JSONObject)
														chunk = chunks.getJSONObject(i);
													if (chunk != null && chunk.has("_links")) {
														JSONObject links = chunk.getJSONObject("_links");
														String link = null;
														if (links.has("response")) {
															link = links.getString("response");
														}
														if (link != null) {
															HierarchyDto hierarchyDto = new HierarchyDto();
															hierarchyDto.setUrl(link);
															String response = getHierarchyData(hierarchyDto, null,null);
															JSONObject json = null;
															if (response != null)
																json = new JSONObject(response);
															if (json != null && json.has("response")) {
																if(json.get("response") instanceof JSONArray) {
																	JSONArray responseArray=json.getJSONArray("response");
																	if(responseArray!=null && responseArray.length()>0) {
																		for(int b=0;b<responseArray.length();b++) {
																JSONObject responseJson = responseArray.getJSONObject(b);
																	if(responseJson!=null){
																			watchlistPepJson.put(responseJson.getString("entity_id"),
																					responseJson);

																	}


																}
																	}
															}
																//
															}
														}
													}
												}
											}
										}*/
									//}
								}
							}

						}
					}
					//sanctions
					if (watchlists.has("sanction")) {
						JSONObject sanctionObject= watchlists.getJSONObject("sanction");
						if(sanctionObject!=null) {
							JSONArray sanctionSources=sanctionObject.names();
							if(sanctionSources!=null && sanctionSources.length()>0) {
								for(int a=0;a<sanctionSources.length();a++) {
									JSONObject sanctionSourceObject=sanctionObject.getJSONObject(sanctionSources.getString(a));


									if(sanctionSourceObject!=null&&sanctionSourceObject.has("results") && sanctionSourceObject.get("results") instanceof JSONArray) 
									{
										JSONArray resultsArray=sanctionSourceObject.getJSONArray("results");
										if(resultsArray!=null && resultsArray.length()>0) {
											for(int b=0;b<resultsArray.length();b++) {
												JSONObject responseJson = resultsArray.getJSONObject(b);
												if(responseJson!=null){
													watchlistPepJson.put(responseJson.getString("entity_id"),
															responseJson);

												}


											}
										}

									}
									/**
									 * Commented due to Old Screening-API Response
									 * 
									 */

									/*if(sanctionSourceObject.has("status")) {
										int chunksCounter = 1;
										while (!"done".equalsIgnoreCase(sanctionSourceObject.getString("status"))) {
											screeningResponse = getData(mainEntityId, requestId);
											if (screeningResponse != null) {
												try {
													screeningJson = new JSONObject(screeningResponse);
												} catch (Exception e) {
												}
												if (screeningJson.has("watchlists")) {
													watchlists = screeningJson.getJSONObject("watchlists");
												}
											}
											chunksCounter = chunksCounter + 1;
											if (chunksCounter == 5)
												break;
											// Thread.sleep(10000);
										}
										if(sanctionSourceObject.has("chunks")) {

											JSONArray chunks = sanctionSourceObject.getJSONArray("chunks");
											if (chunks != null) {
												for (int i = 0; i < chunks.length(); i++) {
													JSONObject chunk = null;
													if (chunks.get(i) instanceof JSONObject)
														chunk = chunks.getJSONObject(i);
													if (chunk != null && chunk.has("_links")) {
														JSONObject links = chunk.getJSONObject("_links");
														String link = null;
														if (links.has("response")) {
															link = links.getString("response");
														}
														if (link != null) {
															HierarchyDto hierarchyDto = new HierarchyDto();
															hierarchyDto.setUrl(link);
															String response = getHierarchyData(hierarchyDto, null,null);
															JSONObject json = null;
															if (response != null)
																json = new JSONObject(response);
															if (json != null && json.has("response")) {
																if(json.get("response") instanceof JSONArray) {
																	JSONArray responseArray=json.getJSONArray("response");
																	if(responseArray!=null && responseArray.length()>0) {
																		for(int b=0;b<responseArray.length();b++) {
																JSONObject responseJson = responseArray.getJSONObject(b);
																	if(responseJson!=null){
																		watchlistSanctionJson.put(responseJson.getString("entity_id"),
																					responseJson);

																	}


																}
																	}
															}
																//
															}
														}
													}
												}
											}
										}
									}*/
								}
							}

						}
					}
				}
				JSONArray entityIds = newsJson.names();
				if(entityIds!=null && entityIds.length()>0){
					for (int m = 0; m < entityIds.length(); m++) {
						if (!"status".equalsIgnoreCase(entityIds.getString(m))) {
							boolean isNewsScreeningDone = false;
							// if (!jsonObject.has("screeningFlag") ||
							// !jsonObject.getBoolean("screeningFlag")) {
							String entityRefId = entityIds.getString(m);
							JSONObject entityNews = null;
							if (newsJson.has(entityRefId))
								entityNews = newsJson.getJSONObject(entityRefId);
							if (entityNews != null && entityNews.has("classifications"))
								classifications = entityNews.getJSONArray("classifications");
							else
								classifications = new JSONArray();
							if (entityNews.has("status") && "done".equalsIgnoreCase(entityNews.getString("status"))) {
								isNewsScreeningDone = true;
							}
							if (jsonObject.has(entityRefId)) {
								jsonObject.getJSONObject(entityRefId).put("news", classifications);
								jsonObject.getJSONObject(entityRefId).put("newsScreening", isNewsScreeningDone);
							} else {
								jsonObject.put(entityRefId, new JSONObject().put("news", classifications));
								jsonObject.getJSONObject(entityRefId).put("newsScreening", isNewsScreeningDone);
							}
						}
					}
				}
				preparePepAndSanction(watchlistPepJson,jsonObject,true,mainEntityId);
				preparePepAndSanction(watchlistSanctionJson,jsonObject,false,mainEntityId);
				/**
				 * Commented due to Old Screening-API Response
				 * 
				 */

				/*if(watchlistEntityIds!=null){
					for (int n = 0; n < watchlistEntityIds.length(); n++) {
						JSONObject pepJsonObject = new JSONObject();
						if (watchlistPepJson.has(watchlistEntityIds.getString(n))) {
							JSONObject object = watchlistPepJson.getJSONObject(watchlistEntityIds.getString(n));
							if (object.has("hits")) {
								JSONArray hits = object.getJSONArray("hits");
								if (hits != null) {
									for (int i = 0; i < hits.length(); i++) {
										JSONArray pepsEntries = new JSONArray();
										JSONArray sanctionEntries = new JSONArray();
										JSONObject pepJson = new JSONObject();
										JSONObject sanctionJson = new JSONObject();
										JSONObject hit = hits.getJSONObject(i);
										if (hit.has("entries")) {
											JSONArray entries = hit.getJSONArray("entries");
											if (entries != null) {
												for (int j = 0; j < entries.length(); j++) {
													JSONObject entry = entries.getJSONObject(j);
													if (entry.has("watchlist_id")) {
														String watchlistId = entry.getString("watchlist_id");
														if (pepTypes.containsKey(watchlistId)) {
															String watchlistType = pepTypes.get(watchlistId).toString();
															if ("PEP".equalsIgnoreCase(watchlistType)) {
																pepsEntries.put(entry);
															} else {
																sanctionEntries.put(entry);
															}
														}
													}
												}
											}
											if (pepsEntries.length() > 0) {
												if (hit.has("value"))
													pepJson.put("value", hit.getString("value"));
												if (hit.has("confidence"))
													pepJson.put("confidence", hit.getDouble("confidence"));
												pepJson.put("entries", pepsEntries);
												peps.put(pepJson);
											}
											if (sanctionEntries.length() > 0) {
												if (hit.has("value"))
													sanctionJson.put("value", hit.getString("value"));
												if (hit.has("confidence"))
													sanctionJson.put("confidence", hit.getDouble("confidence"));
												sanctionJson.put("entries", sanctionEntries);
												sanctions.put(sanctionJson);
											}
										}
									}
								}
							}
							JSONArray mergedPeps = new JSONArray();
							JSONArray mergedSanctions = new JSONArray();
							getMergedResults(mergedPeps, object.getJSONArray("hits"));
							getMergedResults(mergedSanctions, sanctions);
							pepJsonObject.put("pep", mergedPeps);
							pepJsonObject.put("sanctions", mergedSanctions);
							if (jsonObject.has(watchlistEntityIds.getString(n))) {
								jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("pep", mergedPeps);
								jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("sanctions", mergedSanctions);
								jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("pepScreening", true);
							} else {
								jsonObject.put(watchlistEntityIds.getString(n), new JSONObject().put("pep", mergedPeps));
								jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("sanctions", mergedSanctions);
								jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("pepScreening", true);

							}

						}
					}
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean isCompleted = true;
		JSONArray names = jsonObject.names();
		if(names!=null){
			for (int i = 0; i < names.length(); i++) {
				JSONObject returJson = jsonObject.getJSONObject(names.getString(i));
				if ((returJson.has("newsScreening") && !returJson.getBoolean("newsScreening"))
						|| (returJson.has("pepScreening") && !returJson.getBoolean("pepScreening"))) {
					isCompleted = false;
				}
			}
		}else{
			isCompleted = false;
		}
		jsonObject.put("status", isCompleted);
		jsonObject.put("requestId", requestId);
		return jsonObject.toString();

	}

	@Override
	public String getRequestIdOfScreening(List<EntityScreeningDto> entityScreeningDtos, String mainEntityId,String startDate, String endDate) throws Exception{
		JSONObject json=new JSONObject();
		String requestId = UUID.randomUUID().toString();
		json.put("requestId", requestId);
		JSONObject finalJsonTosent = new JSONObject();
		JSONArray personEntities = new JSONArray();
		JSONArray orgEntities = new JSONArray();
		JSONObject orgJson = new JSONObject();
		JSONObject personJson = new JSONObject();
		int count = 0;
		try{
			for (EntityScreeningDto entityScreeningDto : entityScreeningDtos) {
				JSONObject jsonToSent = new JSONObject();
				if (entityScreeningDto.getIdentifier() != null) {
					jsonToSent.put("entity_id", entityScreeningDto.getIdentifier());
				}
				jsonToSent.put("jurisdiction", entityScreeningDto.getJurisdiction());
				jsonToSent.put("names", new JSONArray().put(entityScreeningDto.getEntityName()));
				if (startDate!=null)
					jsonToSent.put("start_date", startDate);
				if (endDate!=null)
					jsonToSent.put("end_date", endDate);
				String entityType = null;
				if (entityScreeningDto.getEntityType() != null) {
					entityType = entityScreeningDto.getEntityType();
				} else {
					entityType = "organization";
				}
				if ("organization".equalsIgnoreCase(entityType))
					orgEntities.put(jsonToSent);
				else
					personEntities.put(jsonToSent);
				count = count + 1;
			}
			if (personEntities.length() > 0)
				personJson.put("entities", personEntities);
			if (orgEntities.length() > 0)
				orgJson.put("entities", orgEntities);
			finalJsonTosent.put("max_responses", 10);
			// finalJsonTosent.put("min_confidence", 0.2);
			if (personJson.has("entities"))
				finalJsonTosent.put("person", personJson);
			if (orgJson.has("entities"))
				finalJsonTosent.put("organization", orgJson);
			System.out.println("Entity Screening Payload Json  :: "+finalJsonTosent.toString());
			String putResponse = sendData(mainEntityId, requestId, finalJsonTosent.toString());
			JSONObject checkJson=new JSONObject(putResponse);
			if(checkJson.has("status") && checkJson.getBoolean("status")==false) {
				json= new JSONObject();
				json.put("message", "Screening Service is not allowed");
				json.put("isError",true);
				json.put("errorCode", 403);
				return json.toString();
			}
			getData(mainEntityId, requestId);//Calling Get Screening once here..	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@Override
	public String addCustomShareHolder(List<CustomShareHolder> customShareHolders, String mainIdentifier,
			Long userId, Boolean isSubsidiarie) throws Exception {
		Users user=usersService.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		JSONObject jsonReturn = new JSONObject();
		JSONArray childIdentifiers = new JSONArray();
		JSONArray childLevels = new JSONArray();
		String newIdentifier = UUID.randomUUID().toString();
		String fields = null;
		for (CustomShareHolder customShareHolder : customShareHolders) {
			JSONObject childJson = new JSONObject();
			JSONObject finalJson = new JSONObject();
			JSONObject shareHoldingJson = new JSONObject();
			JSONObject shareHolders = new JSONObject();
			JSONObject json = new JSONObject();
			JSONObject basicJson = new JSONObject();
			json.put("from", customShareHolder.getFrom());
			json.put("nature-of-control", customShareHolder.getNatureOfControl());
			json.put("source", customShareHolder.getSource());
			json.put("totalPercentage", customShareHolder.getTotalPercentage());
			if(customShareHolder.getClassification()!=null)
				json.put("classification", customShareHolder.getClassification());
			if(customShareHolder.getRole()!=null)
				json.put("role", customShareHolder.getRole());
			if(customShareHolder.getSource()!=null)
				json.put("user", customShareHolder.getSource());
			else
				json.put("user", user.getFirstName()+" "+user.getLastName());
			basicJson.put("vcard-organization-name", customShareHolder.getEntityName());
			basicJson.put("isDomiciledIn", customShareHolder.getJurisdiction());
			if (customShareHolder.getHasURL() != null)
				basicJson.put("hasURL", customShareHolder.getHasURL());
			if (customShareHolder.getStreetAddress() != null)
				basicJson.put("streetAddress", customShareHolder.getStreetAddress());
			if (customShareHolder.getCity() != null)
				basicJson.put("city", customShareHolder.getCity());
			if (customShareHolder.getCountry() != null)
				basicJson.put("country", customShareHolder.getCountry());
			if (customShareHolder.getZip() != null)
				basicJson.put("zip", customShareHolder.getZip());
			if (customShareHolder.getFullAddress() != null)
				basicJson.put("fullAddress", customShareHolder.getFullAddress());
			
			if(customShareHolder.getEntityType()!=null){
				if("organization".equalsIgnoreCase(customShareHolder.getEntityType()))
					basicJson.put("entity_type", "C");
				else
					basicJson.put("entity_type", "I");
			}
			if (customShareHolder.getSourceUrl() != null)
				basicJson.put("sourceUrl", customShareHolder.getSourceUrl());
			if (customShareHolder.getDateOfBirth() != null)
				basicJson.put("dateOfBirth", customShareHolder.getDateOfBirth());
			json.put("basic", basicJson);
			if (customShareHolder.getIdentifier() != null)
				shareHolders.put(customShareHolder.getIdentifier(), json);
			else
				shareHolders.put(newIdentifier, json);
			if (isSubsidiarie) {
				fields = "graph:subsidiaries";
				shareHoldingJson.put("subsidiaries", shareHolders);
			} else {
				fields = "graph:shareholders";
				shareHoldingJson.put("shareholders", shareHolders);
			}
			finalJson.put(customShareHolder.getChildIdentifier(), shareHoldingJson);
			finalJson.put("user_id", userId.toString());
			String url = BIGDATA_MULTISOURCE_URL + "/" + URLEncoder.encode(mainIdentifier, "UTF-8").replaceAll("\\+", "%20")
					+ "/graph/" + customShareHolder.getChildLevel();
			url = url + "?fields=" + fields + "&client_id=" + CLIENT_ID;
			String serverResponse[] = ServiceCallHelper.updateDataInServerWithApiKey(url, finalJson.toString(),BE_ENTITY_API_KEY);
			String response = null;
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
					JSONObject responseJson=new JSONObject(response);
					JSONArray array=responseJson.names();
					String id=null;
					if(array!=null && array.length()>0){
						for (int i = 0; i < array.length(); i++) {
							if(!"user_id".equalsIgnoreCase(array.getString(i))){
								id=array.getString(i);
							}
						}
					}
					JSONObject idJson=new JSONObject();
					if(id!=null){
						idJson=responseJson.getJSONObject(id);
					}
					if(idJson!=null)
						jsonReturn.put("identifier", idJson.names().getString(0));
					childJson.put("identifier", id);
					childJson.put("childLevel", customShareHolder.getChildLevel());

					childIdentifiers.put(childJson);
					childLevels.put(customShareHolder.getChildLevel());
				}

			}

		}		
		jsonReturn.put("childs", childIdentifiers);
		//jsonReturn.put("childLevel", childLevels);
		int presentLevel=0;
		for (int i = 0; i < childLevels.length(); i++) {
			if(presentLevel<=childLevels.getInt(i))
				presentLevel=childLevels.getInt(i);
		}
		jsonReturn.put("level", presentLevel+1);
		return jsonReturn.toString();
		/*String fields = null;
		if (isSubsidiarie) {
			fields = "graph:subsidiaries";
			shareHoldingJson.put("subsidiaries", shareHolders);
		} else {
			fields = "graph:shareholders";
			shareHoldingJson.put("shareholders", shareHolders);
		}
		finalJson.put(parentIdentifier, shareHoldingJson);
		finalJson.put("user_id", userId.toString());*/
		/*String url = BIGDATA_MULTISOURCE_URL + "/" + URLEncoder.encode(mainIdentifier, "UTF-8").replaceAll("\\+", "%20")
				+ "/graph/" + level;
		url = url + "?fields=" + fields + "&client_id=" + CLIENT_ID;
		String serverResponse[] = ServiceCallHelper.updateDataInServer(url, finalJson.toString());
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json=new JSONObject(response);
				JSONArray array=json.names();
				String id=null;
				if(array!=null && array.length()>0){
					for (int i = 0; i < array.length(); i++) {
						if(!"user_id".equalsIgnoreCase(array.getString(i))){
							id=array.getString(i);
						}
					}
				}
				JSONObject idJson=new JSONObject();
				if(id!=null){
					idJson=json.getJSONObject(id);
				}
				if(idJson!=null)
					json.put("identifier", idJson.names().getString(0));
				json.put("childIdentifier", id);
				json.put("childLevel", level);
				json.put("level", level+1);
				return json.toString();
			} else {
				return response;
			}
		} else {
			return response;
		}
		 */
	}

	public void getCustomShareholdersFromLatLevel(List<List<JSONObject>> list,JSONArray array, Integer lowRange, Integer highRange,JSONObject holders, JSONObject map){
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File jsonfile = new File(classLoader.getResource("entityType.json").getFile());
		Object jsonfiledata = new Object();
		try {
			jsonfiledata = parser.parse(new FileReader(jsonfile));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		org.json.simple.JSONArray personTypes = (org.json.simple.JSONArray) jsonfiledata;
		for (JSONObject jsonObject :list.get(list.size()-1)) {
			
			if(jsonObject.has("parentIds")){
				for (int i = 0; i < jsonObject.getJSONArray("parentIds").length(); i++) {
					String entityType=null;
					JSONObject parentJson=jsonObject.getJSONArray("parentIds").getJSONObject(i);
					if(!holders.has(jsonObject.getJSONArray("parentIds")
							.getJSONObject(i).getString("parentId"))){
						if(jsonObject.getJSONArray("parentIds").getJSONObject(i).has("basic")){
							JSONObject jsonlast=new JSONObject();
							JSONObject basic=jsonObject.getJSONArray("parentIds").getJSONObject(i).getJSONObject("basic");
							jsonlast.put("totalPercentage", jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getDouble("totalPercentage"));
							jsonlast.put("indirectPercentage",
									(jsonObject.getDouble("indirectPercentage")
											* jsonlast.getDouble("totalPercentage"))/100);
							if ((jsonlast.getDouble("indirectPercentage") >= lowRange && jsonlast.getDouble("indirectPercentage") <= highRange)) {
								if(basic.has("vcard:organization-name")) {
									String organisationName = basic.getString("vcard:organization-name");
									jsonlast.put("name", organisationName);
									jsonlast.put("title", organisationName);
								}
								if (basic.has("isDomiciledIn")) {
									String jurisdiction = basic.getString("isDomiciledIn");
									jsonlast.put("jurisdiction", jurisdiction);
								}else{
									jsonlast.put("jurisdiction", jsonObject.getString("jurisdiction"));
								}
								jsonlast.put("level", jsonObject.getInt("level") + 1);
								jsonlast.put("entity_id", "orgChartParentEntity");
								
								jsonlast.put("bvdId", (jsonObject.getJSONArray("parentIds").getJSONObject(i)
										.getString("parentId")));
								jsonlast.put("identifier", (jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("parentId")));
								jsonlast.put("screeningFlag", true);
								jsonlast.put("subsidiaries", new JSONArray());
								jsonlast.put("childIdentifier",jsonObject.getString("identifier"));	
								jsonlast.put("childLevel",jsonObject.getInt("level"));
								/////
								 if (basic.has("entity_type")) {
										entityType = basic.getString("entity_type");
										if (personTypes.contains(entityType)) {
											entityType = "person";
										} else {
											entityType = "organization";
										}
										
									}
								 
								
								 /////
								jsonlast.put("entity_type", entityType);
								jsonlast.put("isCustom", true);
								
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("dateOfBirth"))
									jsonlast.put("dateOfBirth", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("dateOfBirth"));
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("role"))
									jsonlast.put("role", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("role"));
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("classification"))
									jsonlast.put("classification", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("classification"));
								
								if(basic.has("sourceUrl"))
									jsonlast.put("sourceUrl", basic.getString("sourceUrl"));
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("source")){
									jsonlast.put("source_evidence", jsonObject.getJSONArray("parentIds")
											.getJSONObject(i).getString("source"));
								}
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("from")){
									jsonlast.put("from", jsonObject.getJSONArray("parentIds")
											.getJSONObject(i).getString("from"));
								}
								if(basic.has("dateOfBirth")){
									jsonlast.put("dateOfBirth", basic.getString("dateOfBirth"));
								}
								jsonlast.put("id", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("parentId"));
								if(jsonObject.has("id") && map.has(jsonObject.getString("id")))
									map.getJSONObject(jsonObject.getString("id")).getJSONArray("parents").put(jsonlast.getString("id"));
								jsonlast.put("status", "completed");
								if(!map.has(jsonlast.getString("identifier")))
									map.put(jsonlast.getString("id"), jsonlast);
								holders.put(jsonlast.getString("identifier"), jsonlast);
								array.put(jsonlast);
							}
						}
					}else{
						if(parentJson.has("indircetPercentage")){
							double indirectPercentageSum=holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).getDouble("indirectPercentage")+parentJson.getDouble("indircetPercentage");
							holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).put("indirectPercentage", indirectPercentageSum);
						}
						if(parentJson.has("totalPercentage")){
							double totalPercentageSum=holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).getDouble("totalPercentage")+parentJson.getDouble("totalPercentage");
							holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).put("totalPercentage", totalPercentageSum);
						}
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("level", jsonObject.getInt("level") + 1);
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("level", jsonObject.getInt("level") + 1);
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("childIdentifier", jsonObject.getString("identifier"));
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("childLevel",jsonObject.getInt("level"));
						jsonObject.getJSONArray("parents").put(holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).getString("id"));

						/*holders.getJSONObject(jsonObject.getJSONArray("parentIds")
							.getJSONObject(i).getString("parentId")).getJSONArray("parents").put(jsonObject.getString("id"));*/
					}
				}
			}
		} 
	}

	public void switchToOtherSource(JSONObject sourceInfo, String source){
		JSONArray sources=sourceInfo.names();
		String previousSource=source;
		int creadibility=0;
		for (int i = 0; i < sources.length(); i++) {
			if((!previousSource.equalsIgnoreCase(sources.getString(i))) && creadibility<=sourceInfo.getInt(sources.getString(i))
					&& (!"companyhouse.co.uk".equalsIgnoreCase(sources.getString(i)))){
				creadibility=sourceInfo.getInt(sources.getString(i));
				source=sources.getString(i);
			}
		}
	}

	@Override
	public String deleteCustomEntity(String identifier, String field) throws Exception {
		String url =BIGDATA_MULTISOURCE_URL;
		if(identifier!=null &field!=null)
			url =url+identifier+"?field="+field+"&client_id="+CLIENT_ID;
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getScreenShotData(String entityId,String sourceName,String sourceUrl) throws Exception {
		String url =SCREEN_SHOT_URL;
		if(sourceUrl != null && entityId != null && sourceName != null)
			url =url+sourceUrl+"&source_name="+sourceName+"&entity_id="+entityId;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private void getConflicts(JSONArray array,JSONObject map, Integer lowRange,Integer highRange) {
		List<String> ids=new ArrayList<String>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject obj = array.getJSONObject(i);
			JSONArray parentsIds = new JSONArray();
			if(obj.has("parents"))
				parentsIds = obj.getJSONArray("parents");
			double amount = 0;
			if(parentsIds!=null){
				for (int j = 0; j < parentsIds.length(); j++) {
					String parent = parentsIds.getString(j);
					JSONObject jsonObj =new JSONObject();
					if(map.has(parent))
						jsonObj =map.getJSONObject(parent);
					if (jsonObj.has("indirectPercentage") && (jsonObj.getDouble("indirectPercentage") >= lowRange
							&& jsonObj.getDouble("indirectPercentage") <= highRange)) {
						if (jsonObj.has("totalPercentage"))
							amount = amount + jsonObj.getDouble("totalPercentage");
					}
				}
			}
			if(amount>100){
				JSONArray parents=new JSONArray();
				if(obj.has("parents"))
					parents=obj.getJSONArray("parents");
				if(parents!=null){
					for (int j = 0; j < parents.length(); j++) {
						ids.add(parents.getString(j));
					}
				}
			}
		}
		if (ids != null) {
			for (int i = 0; i < array.length(); i++) {
				for (String id : ids) {
					if(id.equalsIgnoreCase(array.getJSONObject(i).getString("id"))){
						array.getJSONObject(i).put("isConflict", true);
					}
				}
			}
		}
		/*JSONArray ids=map.names();
		for (int i = 0; i < ids.length(); i++) {
			map.getJSONObject(ids.getString(i));
		}*/
		/*Map<Integer,Double> map=new HashMap<Integer,Double>();
		for (int i = 0; i < array.length(); i++) {
			if(map.containsKey(array.getJSONObject(i).getInt("level"))){
				double value=0;
				value=map.get(array.getJSONObject(i).getInt("level"))+array.getJSONObject(i).getDouble("totalPercentage");
				map.put(array.getJSONObject(i).getInt("level"), value);
			}else{
				map.put(array.getJSONObject(i).getInt("level"), array.getJSONObject(i).getDouble("totalPercentage"));
			}
		}
		for (int i = 0; i < array.length(); i++) {
			double percentage=map.get(array.getJSONObject(i).getInt("level"));
			if(percentage>100)
				array.getJSONObject(i).put("isConflict",true);
			else
				array.getJSONObject(i).put("isConflict",false);
		}*/
	}
	
	private void preparePepAndSanction(JSONObject watchlistJson, JSONObject jsonObject, boolean isPep,String mainEntityId){
		JSONArray watchlistEntityIds = watchlistJson.names();
		if(watchlistEntityIds!=null){
			for (int n = 0; n < watchlistEntityIds.length(); n++) {
				if (watchlistJson.has(watchlistEntityIds.getString(n))) {
					JSONObject object = watchlistJson.getJSONObject(watchlistEntityIds.getString(n));
					JSONArray mergedResult = new JSONArray();
					if(object.has("hits"))
						getMergedResults(mergedResult, object.getJSONArray("hits"),mainEntityId,isPep);					
					if (jsonObject.has(watchlistEntityIds.getString(n))) {
						if(isPep)
							jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("pep", mergedResult);
						else
							jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("sanctions", mergedResult);
						jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("pepScreening", true);
					} else {
						if(isPep)
							jsonObject.put(watchlistEntityIds.getString(n), new JSONObject().put("pep", mergedResult));
						else
							jsonObject.put(watchlistEntityIds.getString(n), new JSONObject().put("sanctions", mergedResult));
						jsonObject.getJSONObject(watchlistEntityIds.getString(n)).put("pepScreening", true);

					}

				}
			}
		}
	}

	@Override
	public String customKeyManager(CustomOfficer customOfficer, Long currentUserId) throws Exception {
		String officerId= null;
		String url=BIGDATA_MULTISOURCE_URL+customOfficer.getMainIdentifier()+"?client_id="+CLIENT_ID+"&fields=officership";
		JSONObject customerRequestObj= new JSONObject();
		
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
		Object fileJsonData = parser.parse(new FileReader(fileJSon));
		org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
		
		if(customOfficer.getOfficerName()!=null)
		customerRequestObj.put("name", customOfficer.getOfficerName());
		if(customOfficer.getDateOfBirth()!=null)
			customerRequestObj.put("date_of_birth", customOfficer.getDateOfBirth());
		if(customOfficer.getOfficerCountry()!=null)
			customerRequestObj.put("country_of_residence", customOfficer.getOfficerCountry());
		if(customOfficer.getOfficerRole()!=null)
			customerRequestObj.put("officer_role", customOfficer.getOfficerRole());
		if(customOfficer.getSource()!=null)
			customerRequestObj.put("customSource", customOfficer.getSource());
		if (customOfficer.getOfficerId() != null) {
			customerRequestObj.put("id", customOfficer.getOfficerId());
		} else {
			officerId=DigestUtils
				      .md5Hex(customOfficer.getSource() + "-" + customOfficer.getOfficerName() + "-" + CLIENT_ID);
			customerRequestObj.put("id",officerId);
		}
		if(customOfficer.getClassification()!=null)
			customerRequestObj.put("classification", customOfficer.getClassification());
		if(customOfficer.getSourceUrl()!=null) 
			customerRequestObj.put("sourceUrl", customOfficer.getSourceUrl());
		if(customOfficer.getFrom()!=null)
			customerRequestObj.put("from",customOfficer.getFrom());
		for (int k = 0; k < countryJson.size(); k++) {
			org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson
					.get(k);
			if (countryObj.get("country").toString()
					.equalsIgnoreCase(customOfficer.getOfficerCountry())) {
				customerRequestObj.put("jurisdiction", countryObj.get("countryCode").toString());
				customerRequestObj.put("country", customOfficer.getOfficerCountry());
			}
		}
		String serverResponse[] = ServiceCallHelper.putStringDataToServerWithoutTimeout(url, customerRequestObj.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			JSONObject responseJson = new JSONObject(response);
			if (officerId != null)
				responseJson.put("officerIdentifier", officerId);
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return responseJson.toString();
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	@Override
	public JSONObject getKeyManagementSource(JSONArray keys2) {
		
		JSONObject sourceInfo=new JSONObject();
		Long classificationId=null;
		
		List<ClassificationsDto> classificationsDtos = classificationsService.getClassifications();

		for (ClassificationsDto classificationsDto : classificationsDtos) {
			if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
				classificationId = classificationsDto.getClassificationId();
			}
		}

		List<SourcesDto> sourcesDtos = sourcesService.getSources(0, 0, null, classificationId, null, null, null,
				null,false);
		
		for (int i = 0; i < keys2.length(); i++) {
			for (SourcesDto sourcesDto : sourcesDtos) {
				if (sourcesDto.getSourceName().equalsIgnoreCase(keys2.getString((i)))) {
					List<ClassificationsDto> classifications = sourcesDto.getClassifications();
					for (ClassificationsDto classificationsDto : classifications) {
						List<SubClassificationsDto> subClassifications = classificationsDto.getSubClassifications();
						for (SubClassificationsDto subClassificationsDto : subClassifications) {
							if (subClassificationsDto.getSubClassifcationName().equalsIgnoreCase("Key Management")) {
								sourceInfo.put(keys2.getString((i)),
										subClassificationsDto.getSubClassificationCredibility().ordinal());
							}
						}
					}

				}
			}
		}
		for (int i = 0; i < keys2.length(); i++) {
			if(!sourceInfo.has(keys2.getString(i))){
				sourceInfo.put(keys2.getString(i), 0);
			}
		}
		
		/*String source=null;
		int credibility=0;
		JSONArray names=sourceInfo.names();
		if(names!=null && names.length()>0){
			for (int j = 0; j < names.length(); j++) {
				if(j==0){
					credibility=sourceInfo.getInt(names.getString(j));
					source=names.getString(j);
				}else if(credibility<sourceInfo.getInt(names.getString(j))){
					credibility=sourceInfo.getInt(names.getString(j));
					source=names.getString(j);
				}
			}
		}*/
		return sourceInfo;
	}

	public String getHighCredSource(JSONObject sourceCredibilities, List<String> officerSourceArray) {
		String source = null;
		
		JSONObject newObject = new JSONObject();
		JSONArray allNames = sourceCredibilities.names();
		for(int k = 0; k < allNames.length(); k++){
			if(officerSourceArray.contains(allNames.get(k))){
				newObject.put(allNames.getString(k), sourceCredibilities.getInt(allNames.getString((k))));
			}
		}
		int credibility = 0;
		JSONArray names = newObject.names();
		if (names != null && names.length() > 0) {
			for (int j = 0; j < names.length(); j++) {
				if (j == 0) {
					credibility = newObject.getInt(names.getString(j));
					source = names.getString(j);
				} else if (credibility < newObject.getInt(names.getString(j))) {
					credibility = newObject.getInt(names.getString(j));
					source = names.getString(j);
				}
			}
		}
		return source;
	}

	@Override
	public String getCompanyVLAData(String identifier, String requestId) throws Exception {
		String url = BIGDATA_MULTISOURCE_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20")
				+ "/vla";
		if (CLIENT_ID != null)
			url = url + "?client_id=" + CLIENT_ID+"&request_id="+requestId;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}

	}

	
}