package element.bst.elementexploration.rest.logging.dao;

import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.util.LogRequest;

public interface AuditLogDao extends GenericDao<AuditLog, Long>{

	List<AuditLog> getLogs(LogRequest logRequest);
	
	List<AuditLog>getLogsByTypeId(String type,Long typeId);
	
	Long countLogs(LogRequest logRequest);

	List<LogAggregator> logAggregator(LogRequest logRequest);

	List<String> getTypes();

	Date getLatestStatusChangeOfAlert(Long alertId);

	List<AuditLog> getLogsByTypeAndSubtype(String type, String subType);

	List<AuditLog> getLogsByTitleAndTypeId(String name, Long typeId);
}
