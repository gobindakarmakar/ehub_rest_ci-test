package element.bst.elementexploration.rest.extention.isracard.service;

public interface IsraCardService {

	String getAgeDestributionCulster(String clusterId, String criteria) throws Exception;

	String getAsAndBsDistribution(String clusterId, String buckets, String criteria) throws Exception;

	String getAverageExpenditureTrends(String clusterId) throws Exception;

	String getAverageExpenditureForCluster(String clusterId) throws Exception;

	String getAvgExpenditureRangesWithClusterIds(String buckets) throws Exception;

	String bankingCardDistribution(String clusterId, String criteria) throws Exception;

	String getCityDistribution(String clusterId, String criteria) throws Exception;

	String getClusterSize() throws Exception;

	String getClusterFallingOnGivenCriteria(String customerCountStart, String customerCountEnd, String avgExpendiStart,
			String avgExpendiEnd) throws Exception;

	String getClusterRangesWithCustomerCount(String buckets) throws Exception;

	String getCustomerCount(String clusterId) throws Exception;

	String getOverallExpenditureAvg() throws Exception;

	String getSeniorityDistribution(String clusterId, String buckets) throws Exception;

	String issuedByDistribution(String clusterId, String criteria) throws Exception;

	String getOverAllExpenditureAvgAndSum() throws Exception;

	String getDataBasedOnFilters(String filters, String clusterId) throws Exception;

	String getGenderDistributionForCluster(String clusterId, String criteria) throws Exception;

	String getCustomerDetails(String customerId) throws Exception;

	String getCustomerPercentageForCluster(String clusterId) throws Exception;

	String getClusterFallingOnCriteria(String criteria) throws Exception;

	String cardDurationDistribution(String clusterId, String criteria) throws Exception;

	String getClusterSizeById(String clusterId) throws Exception;

	String getAverageExpPerCardForCustomer(String customerId, String cardId) throws Exception;

	String getAverageExpPerMerchantForCustomer(String customerId, String merchantId) throws Exception;

	String getAvgExpPerMerchantTypeForSpecificCustomer(String customerId, String merchantType) throws Exception;

	String getEventAcitivityCodeDistribution(String clusterId) throws Exception;

	String getGroupLevelDistribution(String clusterId) throws Exception;

	String getTrackTwoLevel(String clusterId) throws Exception;

	String getGraphDataByCustomerIdForFirst1000Transactions(String customerId) throws Exception;

	String getGraphDataByCustomerIdForLimit(String customerId, Integer limit) throws Exception;

	String clubStatsWithDistributionForCluster(String clusterId) throws Exception;

	// uc2
	String getClusterSizeForUC2() throws Exception;

	String getClusterFallingOnCriteriaForUC2(String criteria) throws Exception;

	String getClusterSizeByIdForUC2(String clusterId,String criteria) throws Exception;

	String getOverAllExpenditureAvgAndSumForUC2() throws Exception;

	String getAgeDestributionCulsterForUC2(String clusterId, String criteria,String buckets) throws Exception;

	String getAsAndBsDistributionForUC2(String clusterId, String bucketCountAs, String criteria,String bucketCountBs) throws Exception;

	String getAverageExpenditureTrendsForUC2(String clusterId) throws Exception;

	String bankingCardDistributionForUC2(String clusterId, String criteria) throws Exception;

	String cardDurationDistributionForUC2(String clusterId, String criteria,String buckets) throws Exception;

	String getCityDistributionForUC2(String clusterId, String criteria) throws Exception;

	String getGenderDistributionForClusterForUC2(String clusterId, String criteria) throws Exception;

	String issuedByDistributionForUC2(String clusterId, String criteria) throws Exception;

	String getAverageExpPerCardForCustomerForUC2(String customerId, String cardId) throws Exception;

	String getCustomerDetailsForUC2(String customerId) throws Exception;

	String clubStatsWithDistributionForClusterForUC2(String clusterId) throws Exception;

	String getEventAcitivityCodeDistributionForUC2(String clusterId) throws Exception;

	String getTrackTwoLevelForUC2(String clusterId) throws Exception;

	String getGroupLevelDistributionForUC2(String clusterId) throws Exception;

	String getClubConnectionStatsForUC2(String clusterId) throws Exception;

	String getClubConnectionsForUC2(String clusterId, String criteria) throws Exception;

	String getStatsOfClubConnectionForUC2(String clusterId, String firstClubName, String secondClubName,
			String criteria) throws Exception;

	String getGraphDataByCardIdForUC2(String clusterId, String clubName, String graphType) throws Exception;

	// uc3
	String getClusterSizeForUC3() throws Exception;

	String getClusterFallingOnCriteriaForUC3(String criteria) throws Exception;

	String getClusterSizeByIdForUC3(String clusterId) throws Exception;

	String getOverAllExpenditureAvgAndSumForUC3() throws Exception;

	String getAgeDestributionCulsterForUC3(String clusterId, String criteria) throws Exception;

	String getAsAndBsDistributionForUC3(String clusterId, String buckets, String criteria) throws Exception;

	String getAverageExpenditureTrendsForUC3(String clusterId) throws Exception;

	String bankingCardDistributionForUC3(String clusterId, String criteria) throws Exception;

	String cardDurationDistributionForUC3(String clusterId, String criteria) throws Exception;

	String getCityDistributionForUC3(String clusterId, String criteria) throws Exception;

	String clubStatsWithDistributionForClusterForUC3(String clusterId) throws Exception;

	String getGenderDistributionForClusterForUC3(String clusterId, String criteria) throws Exception;

	String issuedByDistributionForUC3(String clusterId, String criteria) throws Exception;

	String getStatsOfClubConnectionForUC3(String clusterId, String firstClubName, String secondClubName,
			String criteria) throws Exception;

	String getClubConnectionsForUC3(String clusterId) throws Exception;

	String getCustomerDetailsForUC3(String customerId) throws Exception;

	String getEventAcitivityCodeDistributionForUC3(String clusterId) throws Exception;

	String getVLAGraphForUC3(String clusterId, String clubName, String graphType) throws Exception;

	String getGroupLevelDistributionForUC3(String clusterId) throws Exception;

	String getTrackThreeLevelForUC2(String clusterId) throws Exception;

	String getStories() throws Exception;

	String getClusterForUc1WithStory(String criteria, String storyId) throws Exception;

	String getDistribution(String jsonToSent, String storyId, String clusterId, String attributeName,
			String attributeType, String distributionCount) throws Exception;

	String getCustomersForUc1WithStory(String criteria, String storyId, String clusterId) throws Exception;

	String getVLAByCustomerId(String customerId) throws Exception;

	String getCustomerDetailsById(String customerId)throws Exception;

	String getClusterByStoryIdAndClusterId(String storyId, String clusterId)throws Exception;

	String getCustomerStoryClustersById(String customerId)throws Exception;

	String getUC3ClustersById(String clusterId)throws Exception;

	String getUC3ClustersByQuery(String criteria)throws Exception;

	String getUC3CustomersByClusterId(String clusterId, String criteria)throws Exception;

	String getUC3DistributionById(String clusterId, String attributeName, String attributeType,
			String distributionCount, String criteria)throws Exception;

	String getCustomerStoryClusterForUC3(String customerId)throws Exception;

	String getVLAGraphByIDForUC3(String customerId)throws Exception;

	String getNamingById(String storyId, String clusterId)throws Exception;

	String getStoryNameById(String storyId)throws Exception;

	String setclusterNamingById(String storyId, String clusterId, String criteria)throws Exception;

	String setStoryNameById(String storyId, String criteria)throws Exception;

	String getAllStories()throws Exception;

	String addANewStory(String criteria,String sotryName,Long userId)throws Exception;

	String deleteAllStories()throws Exception;

	String getStoriesById(String storyId)throws Exception;

	String UpdateStoryById(String storyId, String criteria)throws Exception;

	String deleteStoryById(String storyId,String sotryName,Long userId)throws Exception;

	String getAvaliableAttributesForUC4()throws Exception;

	String getUC4CustomersByFilters(String criteria)throws Exception;

	String getDistributionsForUC4(String attributeName, String attributeType, String distributionCount,
			String criteria)throws Exception;

	String getCustomerCountForUC4(String jsonString)throws Exception;

	String getUC4PopularMerchantTypes(String criteria)throws Exception;

	String getUC4TopEarlyAdaptors(String criteria)throws Exception;

	String getClubConnectionsForUC2ByNameAndId(String clusterId, String criteria, String clubName)throws Exception;

	String searchCustomerByIdAndStoryId(String storyId,String clusterId, String id)throws Exception;

	String getUC3CustomerById(String clusterId,String ownerId)throws Exception;
	
	

}
