package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;

public interface CounterPartyService {

	RiskCountAndRatioDto getActivity(String fromDate, String toDate,FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> getGeoGraphicAggregates(String fromDate, String toDate, FilterDto filterDto,boolean isBankCountry)
			throws ParseException;

	List<RiskCountAndRatioDto> getBankAggregates(String fromDate, String toDate, FilterDto filterDto);

	RiskCountAndRatioDto getGeoGraphic(String fromDate, String toDate, FilterDto filterDto,boolean isBankCountry) throws ParseException;

	RiskCountAndRatioDto getBanks(String fromDate, String toDate, FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> getCouterPartyAggregates(String fromDate, String toDate);

	Map<Integer, List<RiskCountAndRatioDto>> getBankByType(String fromDate, String toDate, boolean falg,String type);

	//Map<Integer, List<RiskCountAndRatioDto>> getCouterPartyByType(String fromDate, String toDate, String type) throws ParseException;

	List<RiskCountAndRatioDto> getViewAll(String fromDate, String toDate,  String type, Integer pageNumber,
			Integer recordsPerPage,FilterDto filterDto);
	
	List<CounterPartyNotiDto> getAlertByScenarios(String fromDate, String toDate,String type,List<String> scenarios,boolean isCountry);

	Map<String, List<RiskCountAndRatioDto>> getTopCounterParties(String fromDate, String toDate);

	Long getViewAllCount(String fromDate, String toDate, String type);

	boolean addBankNames();


}
