package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Associated entity")
public class AssociatedEntityRespDto implements Serializable
{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Customer name")	
	private String customerName;
	@ApiModelProperty(value="Transaction amount")	
	private double amount;
	@ApiModelProperty(value="Customer Id")	
	private long customerId;
	
	
	
	
	
	public AssociatedEntityRespDto(double amount) {
		super();
		this.amount = amount;
	}


	public AssociatedEntityRespDto(double amount, long customerId) {
		super();
		this.amount = amount;
		this.customerId = customerId;
	}
	
	
	public AssociatedEntityRespDto(String customerName, double amount) {
		super();
		this.customerName = customerName;
		this.amount = amount;
	}


	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	
	
	
	
	
	

}
