package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentQuestionsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("documentQuestionsDao")
public class DocumentQuestionsDaoImpl extends GenericDaoImpl<DocumentQuestions, Long> implements DocumentQuestionsDao {

	public DocumentQuestionsDaoImpl() {
		super(DocumentQuestions.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentQuestions> getDocumentQuestionsByTemplateId(Long templateId,boolean isPdf) {
		
		List<DocumentQuestions> documentQuestionsList=new ArrayList<DocumentQuestions>();
		try{
			Query<?> query; 
			
			if(isPdf)	{		
				query = this.getCurrentSession().createQuery("from DocumentQuestions dq where dq.questionStatus = :isStatus and dq.deleteStatus =:isDelete and  dq.templateId= :tempId");
				query.setParameter("isStatus", true);
			}
				else
				query = this.getCurrentSession().createQuery("from DocumentQuestions dq where dq.deleteStatus = :isDelete and dq.templateId= :tempId");			
			query.setParameter("isDelete", false);
			query.setParameter("tempId", templateId);		
			
			documentQuestionsList= (List<DocumentQuestions>) query.getResultList();
			return documentQuestionsList;
		}catch (Exception e) {
			return documentQuestionsList;
		}
		
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentQuestions> getTableQuestionsById(Long tableId) {
		List<DocumentQuestions> documentQuestionsList=new ArrayList<DocumentQuestions>();
		try{
			documentQuestionsList=this.getCurrentSession()
					.createQuery("select dq from DocumentQuestions dq,DocumentContents dc where dq.documentContent.id = dc.id and dc.id=" +tableId).getResultList();
			return documentQuestionsList;
		}catch (Exception e) {
			return documentQuestionsList;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public int findQuestions(long docId) 
	{
		int count = 0;
		try{			
			Query<?> query = this.getCurrentSession().createQuery(" from DocumentQuestions d where d.deleteStatus = 0 and d.templateId ="+docId+"  and d.questionStatus =:question ");
			query.setParameter("question", true);
			List<DocumentQuestions> questions = (List<DocumentQuestions>) query.getResultList();
			count = questions.size();
		}
		catch (Exception e)
		{
			return count;
		}
		return count;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentQuestions> getAllQuestions(long docId, boolean question) {
		List<DocumentQuestions> documentQuestionsList=new ArrayList<DocumentQuestions>();
		try{
			Query<?> query = this.getCurrentSession().createQuery(" select d from DocumentQuestions d join d.documentContent as dc where dc.docId ="+docId+"  and d.questionStatus =:question and d.deleteStatus = 0 ");
			query.setParameter("question", question);			
			documentQuestionsList=(List<DocumentQuestions>) query.getResultList();			
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return documentQuestionsList;
		}
		return documentQuestionsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentQuestions> getDocumentTableQuestionsById(Long tableId) {
		List<DocumentQuestions> documentQuestionsList=this.getCurrentSession()
				.createQuery("from DocumentQuestions dq where dq.deleteStatus = 0 and  dq.documentContent.id = " +tableId ).getResultList(); 
		return documentQuestionsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentQuestions> getOSINTQuestionsByTemplateId(Long templateId) {
		List<DocumentQuestions> documentQuestionsList=new ArrayList<DocumentQuestions>(); 
		try{
			Query<?> query = this.getCurrentSession().createQuery("from DocumentQuestions dq where dq.deleteStatus = 0 and   dq.templateId = :template and dq.OSINT = 1 ");
			query.setParameter("template", templateId);
			//query.setParameter("osint", true);
			documentQuestionsList=(List<DocumentQuestions>) query.getResultList();
			
		}catch (Exception e) {
			return documentQuestionsList;
		}		
		return documentQuestionsList;

	}

	@SuppressWarnings("unchecked")
	@Override
	public int getElements(Long templateId)
	{
		List<DocumentQuestions> documentQuestionsList = new ArrayList<DocumentQuestions>(); 
		try{
			Query<?> query = this.getCurrentSession().createQuery("select dq.contentType from DocumentQuestions dq where dq.deleteStatus = 0 and dq.templateId = :template GROUP BY dq.contentType");
			query.setParameter("template", templateId);
			documentQuestionsList=(List<DocumentQuestions>) query.getResultList();
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return documentQuestionsList.size();
		}		
		return documentQuestionsList.size();
	}
	


}
