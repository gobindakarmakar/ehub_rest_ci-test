package element.bst.elementexploration.rest.extention.gridview.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Jalagari Paul
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GridViewDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID of the grid view")
	private Long gridViewId;

	@ApiModelProperty(value = "Metadata of Grid View")
	private String gridViewMetaData;

	@ApiModelProperty(value = "Table Name of Grid View")
	@JsonProperty(required = true)
	private String gridTableName;

	@ApiModelProperty(value = "View Name of Grid View")
	@NotNull(message = "View name cannot be blank")
	private String gridViewName;

	@ApiModelProperty(value = "is the view default")
	private boolean isDefaultView = false;

	public Long getGridViewId() {
		return gridViewId;
	}

	public void setGridViewId(Long gridViewId) {
		this.gridViewId = gridViewId;
	}

	public String getGridViewMetaData() {
		return gridViewMetaData;
	}

	public void setGridViewMetaData(String gridViewMetaData) {
		this.gridViewMetaData = gridViewMetaData;
	}

	public String getGridTableName() {
		return gridTableName;
	}

	public void setGridTableName(String gridTableName) {
		this.gridTableName = gridTableName;
	}

	public String getGridViewName() {
		return gridViewName;
	}

	public void setGridViewName(String gridViewName) {
		this.gridViewName = gridViewName;
	}

	public boolean isDefaultView() {
		return isDefaultView;
	}

	public void setDefaultView(boolean isDefaultView) {
		this.isDefaultView = isDefaultView;
	}

}
