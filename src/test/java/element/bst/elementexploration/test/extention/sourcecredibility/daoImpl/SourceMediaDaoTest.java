package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourceMediaDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;

/**
 * @author suresh
 *
 */
public class SourceMediaDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourceMediaDaoImpl sourceMediaDao;

	SourceMedia sourceMedia = null;
	List<SourceMedia> mediaList = new ArrayList<SourceMedia>();

	@Before
	public void setupMock() {
		sourceMedia = new SourceMedia();
		sourceMedia.setMediaId(1L);
		sourceMedia.setMediaName("PPT");
		SourceMedia sourceMediaTwo = new SourceMedia();
		sourceMediaTwo.setMediaId(1L);
		sourceMediaTwo.setMediaName("Excel");
		mediaList.add(sourceMedia);
		mediaList.add(sourceMediaTwo);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchMedia() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sourceMedia);
		SourceMedia sourcesMediaSaved = sourceMediaDao.fetchMedia("PPT");
		assertEquals(sourceMedia.getMediaName(), sourcesMediaSaved.getMediaName());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fetchMedia_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		sourceMediaDao.fetchMedia("PPT");
	}

	@Test
	public void fetchMediaListExceptAll() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(mediaList);
		List<SourceMedia> mediaListSaved = sourceMediaDao.fetchMediaListExceptAll();
		assertEquals(mediaListSaved.size(), mediaList.size());
	}

}
