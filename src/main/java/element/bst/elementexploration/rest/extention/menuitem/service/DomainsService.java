package element.bst.elementexploration.rest.extention.menuitem.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Jalagari Paul
 *
 */
public interface DomainsService extends GenericService<Domains, Long> {

	public List<Domains> getDomainsListWithoutItem(String item) throws Exception;
}
