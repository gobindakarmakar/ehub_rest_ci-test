package element.bst.elementexploration.rest.version.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.version.domain.ProductVersion;
import element.bst.elementexploration.rest.version.service.VersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Product version API" })
@RestController
@RequestMapping("/api/productVersions")
public class ProductVersionController extends BaseController {

	@Autowired
	VersionService versionService;

	@ApiOperation("Get all versions")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ProductVersion.class, responseContainer = "List", message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllVersions(HttpServletRequest request) {
		return new ResponseEntity<>(versionService.getAllVersions(), HttpStatus.OK);
	}

	@ApiOperation("Get latest versions")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ProductVersion.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/latest", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getLatestVersion(HttpServletRequest request) {
		return new ResponseEntity<>(versionService.getLatestVersion(), HttpStatus.OK);
	}

}
