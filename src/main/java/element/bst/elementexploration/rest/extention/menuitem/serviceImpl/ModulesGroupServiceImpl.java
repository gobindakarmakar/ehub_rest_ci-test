package element.bst.elementexploration.rest.extention.menuitem.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.extention.menuitem.service.ModulesGroupService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Jalagari Paul
 *
 */
@Service("modulesGroupService")
@Transactional("transactionManager")
public class ModulesGroupServiceImpl extends GenericServiceImpl<ModulesGroup, Long> implements ModulesGroupService {

	public ModulesGroupServiceImpl() {

	}

	@Autowired
	public ModulesGroupServiceImpl(GenericDao<ModulesGroup, Long> genericDao) {
		super(genericDao);
	}
}
