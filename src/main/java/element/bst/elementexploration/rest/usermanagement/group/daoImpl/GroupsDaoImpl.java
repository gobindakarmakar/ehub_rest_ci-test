package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupsDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.dto.UsersGroupDto;
import element.bst.elementexploration.rest.usermanagement.user.daoImpl.UserDaoImpl;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("groupsDao")
public class GroupsDaoImpl extends GenericDaoImpl<Groups, Long> implements GroupsDao {

	public GroupsDaoImpl() {
		super(Groups.class);
	}

	@Override
	public Groups getGroup(String groupName) {
		Groups group = null;
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from Groups g where g.name = :groupName");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("groupName", groupName);
			group = (Groups) query.getSingleResult();
			return group;
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return group;
	}

	/**
	 * Method to resolve Usergroup column names
	 * 
	 * @param groupColumn
	 * @return
	 */
	private String resolveGroupCaseColumnName(String groupColumn) {
		if (groupColumn == null)
			return "createdDate";

		switch (groupColumn.toLowerCase()) {
		case "name":
			return "name";
		default:
			return "createdDate";
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Groups> getUserGroups(Boolean status, String keyword, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		List<Groups> groups = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from Groups g ");
			boolean isOptional = true;
			if (!StringUtils.isEmpty(keyword)) {
				queryBuilder.append(" where g.name like '%" + keyword + "%'");
				queryBuilder.append(" or g.description like '%" + keyword + "%'");
				queryBuilder.append(" or g.remarks like '%" + keyword + "%'");
				isOptional = false;
			}
			if (status != null) {
				if (!isOptional)
					queryBuilder.append(" and g.active =:status");
				else
					queryBuilder.append(" where g.active =:status");
			}
			queryBuilder.append(" order by ").append("g.").append(resolveGroupCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if (status != null)
				query.setParameter("status", status);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			groups = (List<Groups>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return groups;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Groups> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy,
			String orderIn) {
		List<Groups> userGroupList = new ArrayList<>();
		try {

			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Groups(g.id, g.createdDate, "
							+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description) ");
			hql.append("from Groups g ").append("left outer join UserGroups ug on g.id = ug.groupId.id ")
					.append("where ug.userId.userId =:userId and g.active = true");
			// .append("where ug.userId.userId =:userId");

			hql.append(" order by ").append("g.").append(resolveGroupCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					hql.append(" ");
					hql.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			userGroupList = (List<Groups>) query.getResultList();

		} catch (HibernateException e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return userGroupList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserGroups(Boolean status, String keyword) {
		List<Groups> groups = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from Groups g ");
			boolean isOptional = true;
			if (!StringUtils.isEmpty(keyword)) {
				queryBuilder.append(" where g.name like '%" + keyword + "%'");
				queryBuilder.append(" or g.description like '%" + keyword + "%'");
				queryBuilder.append(" or g.remarks like '%" + keyword + "%'");
				isOptional = false;
			}
			if (status != null) {
				if (!isOptional)
					queryBuilder.append(" and g.active =:status");
				else
					queryBuilder.append(" where g.active =:status");
			}

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if (status != null)
				query.setParameter("status", status);
			groups = (List<Groups>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) groups.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countGroupsOfUser(Long userId) {
		List<Groups> userGroupList = new ArrayList<>();
		try {

			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Groups(g.id, g.createdDate, "
							+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description) ");
			hql.append("from Groups g ").append("left outer join UserGroups ug on g.id = ug.groupId ")
					.append("where ug.userId.userId =:userId and g.active = true");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			userGroupList = (List<Groups>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return (long) userGroupList.size();
	}

	@Override
	public Groups checkGroupExist(String name, Long groupId) {
		Groups found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Groups> query = builder.createQuery(Groups.class);
			Root<Groups> group = query.from(Groups.class);
			query.select(group);
			query.where(builder.and(builder.notEqual(group.get("userGroupId"), groupId),
					builder.equal(group.get("name"), name)));
			found = (Groups) this.getCurrentSession().createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

	@Override
	public Boolean doesUserbelongtoGroup(String groupName, Long userId) {
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.dto.UsersGroupDto(ug.userId) ");
			queryBuilder.append("from UserGroups ug left outer join Groups g on g.id = ug.groupId.id ");
			queryBuilder.append("where g.name = '" + groupName + "' AND ug.userId ='" + userId + "'");

			UsersGroupDto userGroup = (UsersGroupDto) this.getCurrentSession().createQuery(queryBuilder.toString())
					.getSingleResult();
			if (null != userGroup) {
				return true;
			}
		} catch (NoResultException e) {
			return false;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "check user from admin group " + userId, e, this.getClass());
			throw new FailedToExecuteQueryException(
					userId + "Could not get user from admin group " + userId + " - " + e.getMessage());
		}
		return false;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public List<Users> getUsersByGroup(Long groupId) {
		List<Groups> userGroupList = new ArrayList<>();
		List<Users> userList = new ArrayList<>();
		try {

			StringBuilder hql = new StringBuilder();
			hql.append("select u from Users u left outer join UserGroups ug on u.id = ug.userId.userId ");
			hql.append("left outer join Groups g on g.id = ug.groupId.id ");
			hql.append(" where ug.groupId.id =:groupId");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("groupId", groupId);
			userList = (List<Users>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return userList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Groups> getGroupsOfUserWithRoles(Long userId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		List<Groups> userGroupList = new ArrayList<>();
		try {

			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Groups(g.id, g.createdDate, "
							+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description, g.groupPermissions) ");
			hql.append("from Groups g ").append(
					"left outer join UserGroups ug on g.id = ug.groupId.id left outer join GroupPermission gp on g.id=gp.groupId ")
					.append("where ug.userId.userId =:userId and g.active = true");
			// .append("where ug.userId.userId =:userId");

			hql.append(" order by ").append("g.").append(resolveGroupCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					hql.append(" ");
					hql.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			userGroupList = (List<Groups>) query.getResultList();

		} catch (HibernateException e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return userGroupList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Groups> getGroupsOfUser(Long userId) {
		List<Groups> userGroupList = new ArrayList<>();
		try {

			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Groups(g.id, g.createdDate, "
							+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description) ");
			hql.append("from Groups g ").append("left outer join UserGroups ug on g.id = ug.groupId.id ")
					.append("where ug.userId.userId =:userId and g.active = true");
			// .append("where ug.userId.userId =:userId");

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			userGroupList = (List<Groups>) query.getResultList();

		} catch (HibernateException e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return userGroupList;
	}

	@Override
	public Object getStatuswiseUsersByGroupId(Long groupId) {
		List<Object[]> queryResult = new ArrayList<Object[]>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = (CriteriaQuery<Object[]>) builder.createQuery(Object[].class);
		Root<UserGroups> result = (Root<UserGroups>) criteriaQuery.from(UserGroups.class);
		//criteriaQuery.multiselect(result.get("roleId"), builder.count(result));

		Join<UserGroups, Users> userJoin = result.join("userId");
		criteriaQuery.multiselect(userJoin.get("statusId").get("displayName"), builder.count(userJoin));
		//criteriaQuery.where(builder.equal(userJoin.get("statusId"), statusId));
		criteriaQuery.where(builder.equal(result.get("groupId"), groupId));
		
		criteriaQuery.groupBy(userJoin.get("statusId"));
		Query<Object[]> query = this.getCurrentSession().createQuery(criteriaQuery);
		
		queryResult = query.getResultList();
		Map<String, Long> resultMap = new HashMap<String, Long>();
		for(Object[] groupCount : queryResult){
			if(groupCount[0] != null &&  groupCount[1] != null){
				resultMap.put(groupCount[0].toString(), Long.valueOf(groupCount[1].toString()));
			}
		}
		return resultMap;
	}
	
	@Override
	public Groups checkIfGroupExist(String name, Long groupId) {
		Groups found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Groups> query = builder.createQuery(Groups.class);
			Root<Groups> group = query.from(Groups.class);
			query.select(group);
			query.where(builder.and(builder.notEqual(group.get("id"), groupId),
					builder.equal(group.get("name"), name)));
			found = (Groups) this.getCurrentSession().createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

}
