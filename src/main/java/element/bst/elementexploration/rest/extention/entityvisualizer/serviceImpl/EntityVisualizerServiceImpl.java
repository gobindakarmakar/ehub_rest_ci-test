package element.bst.elementexploration.rest.extention.entityvisualizer.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.entityvisualizer.service.EntityVisualizerService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("entityVisualizerService")
public class EntityVisualizerServiceImpl implements EntityVisualizerService {

	@Value("${bigdata_ent_url}")
	private String BIG_DATA__ENT_URL;

	@Override
	public String getEntData(String payload) throws Exception {
		String url = BIG_DATA__ENT_URL;
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, payload);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
}
