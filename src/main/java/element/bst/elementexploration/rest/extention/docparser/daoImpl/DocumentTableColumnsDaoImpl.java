package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.List;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTableColumnsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableColumns;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("documentTableColumnsDao")
public class DocumentTableColumnsDaoImpl extends GenericDaoImpl<DocumentTableColumns, Long>
		implements DocumentTableColumnsDao {

	public DocumentTableColumnsDaoImpl() {
		super(DocumentTableColumns.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentTableColumns> getAllColumnsByTableId(Long id) {
		List<DocumentTableColumns> tableColumnsList=this.getCurrentSession()
				.createQuery("from DocumentTableColumns where documentContent.id = " +id).getResultList(); 
		return tableColumnsList;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<DocumentTableColumns> getAllChildColumsByParentId(Long parentId) {		
		List<DocumentTableColumns> childColumnsList=this.getCurrentSession()
				.createQuery("from DocumentTableColumns where parentId = " +parentId).getResultList(); 
		return childColumnsList;
	}

}
