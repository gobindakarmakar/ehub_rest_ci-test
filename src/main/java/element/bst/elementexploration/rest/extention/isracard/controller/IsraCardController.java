package element.bst.elementexploration.rest.extention.isracard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.isracard.service.IsraCardService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Lead Generation API" },description="Manages lead generation data")
@RestController
@RequestMapping("/api/leadGeneration")
public class IsraCardController extends BaseController {

	@Autowired
	private IsraCardService israCardService;

	@ApiOperation("Gets cluster size")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/cluster_stats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterSize(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getClusterSize(), HttpStatus.OK);
	}

	@ApiOperation("given cluster falling on given criteria")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_2/cluster_stats", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterFallingOnCriteria(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterFallingOnCriteria(criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster size")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/cluster_stats/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterSizeById(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterSizeById(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets overall expenditure avg and sum")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/overall_stats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOverAllExpenditureAvgAndSum(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getOverAllExpenditureAvgAndSum(), HttpStatus.OK);
	}

	@ApiOperation("Gets age distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/age_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAgeDestributionCulster(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getAgeDestributionCulster(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets as/bs distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/as_bs_distribution/{clusterId}/{buckets}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAsAndBsDistribution(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId, @PathVariable String buckets) throws Exception {
		return new ResponseEntity<>(israCardService.getAsAndBsDistribution(clusterId, buckets, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure trends")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_3/average_expenditure_trends/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpenditureTrends(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpenditureTrends(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets banking card distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/banking_card_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> bankingCardDistribution(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.bankingCardDistribution(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets card duration distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/card_duration_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> cardDurationDistribution(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.cardDurationDistribution(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets city distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/city_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCityDistribution(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getCityDistribution(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets data on the basis of filters passed")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/data/{clusterId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getDataBasedOnFilters(@RequestParam String token, @RequestBody String filters,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getDataBasedOnFilters(filters, clusterId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets gender distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/gender_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGenderDistributionForCluster(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getGenderDistributionForCluster(clusterId, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets issued by distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1/1_3/issued_by_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> issuedByDistribution(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.issuedByDistribution(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure per card for a customer")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/avg_expenditure_per_card/{customerId}/{cardId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpPerCardForCustomer(@RequestParam String token,
			@PathVariable String customerId, @PathVariable String cardId) throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpPerCardForCustomer(customerId, cardId), HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure per card for a merchant")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/avg_expenditure_per_merchant/{customerId}/{merchantId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpPerMerchantForCustomer(@RequestParam String token,
			@PathVariable String customerId, @PathVariable String merchantId) throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpPerMerchantForCustomer(customerId, merchantId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure per card for a type merchant")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/avg_expenditure_per_merchant_type/{customerId}/{merchantType}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAvgExpPerMerchantTypeForSpecificCustomer(@RequestParam String token,
			@PathVariable String customerId, @PathVariable String merchantType) throws Exception {
		return new ResponseEntity<>(
				israCardService.getAvgExpPerMerchantTypeForSpecificCustomer(customerId, merchantType), HttpStatus.OK);
	}

	@ApiOperation("Gets customer details")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/customer_details/{customerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerDetails(@RequestParam String token, @PathVariable String customerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerDetails(customerId), HttpStatus.OK);
	}

	@ApiOperation("Gets evenet activity code distribution(Installments/Standard)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/event_acitivity_code/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEventAcitivityCodeDistribution(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getEventAcitivityCodeDistribution(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Returns VLA graph data by customer ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/graph/{customerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphDataByCustomerIdForFirst1000Transactions(@RequestParam String token,
			@PathVariable String customerId) throws Exception {
		return new ResponseEntity<>(israCardService.getGraphDataByCustomerIdForFirst1000Transactions(customerId),
				HttpStatus.OK);
	}

	@ApiOperation("Returns VLA graph data by customer ID within the limit")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/graph/{customerId}/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphDataByCustomerIdForLimit(@RequestParam String token,
			@PathVariable String customerId, @PathVariable Integer limit) throws Exception {
		return new ResponseEntity<>(israCardService.getGraphDataByCustomerIdForLimit(customerId, limit), HttpStatus.OK);
	}

	@ApiOperation("Gets group level distribution(local/abroad)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/group_level_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupLevelDistribution(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getGroupLevelDistribution(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets track 2 level(Online/Onsite)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_4/track_2_mark_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTrackTwoLevel(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getTrackTwoLevel(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure for cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/2_3/average_expenditure_for_cluster/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpenditureForCluster(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpenditureForCluster(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets club stats for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/2_3/club_stats/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> clubStatsWithDistributionForCluster(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.clubStatsWithDistributionForCluster(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets customer percentage per cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/2_3/customer_percentage_for_cluster/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerPercentageForCluster(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerPercentageForCluster(clusterId), HttpStatus.OK);
	}
	
	
	@ApiOperation("Search Customer by Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1_with_story/1_4/search_customer/{storyId}/{clusterId}/{id}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> searchCustomerByIdAndStoryId(@RequestParam String token, @PathVariable String storyId,@PathVariable String clusterId,@PathVariable String id)
			throws Exception {
		return new ResponseEntity<>(israCardService.searchCustomerByIdAndStoryId(storyId,clusterId,id), HttpStatus.OK);
	}
	

	// uc2

	@ApiOperation("Gets cluster falling on a given criteria")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_2/cluster_stats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterSizeForUC2(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getClusterSizeForUC2(), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster stats")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_2/cluster_stats", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterFallingOnCriteriaForUC2(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterFallingOnCriteriaForUC2(criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster stats by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_2/cluster_stats/{clusterId}", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterSizeByIdForUC2(@RequestParam String token,@RequestBody String criteria, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterSizeByIdForUC2(clusterId,criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets overall expenditure avg and sum")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_2/overall_stats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOverAllExpenditureAvgAndSumForUC2(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getOverAllExpenditureAvgAndSumForUC2(), HttpStatus.OK);
	}

	@ApiOperation("Gets age distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/age_distribution/{clusterId}/{buckets}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAgeDestributionCulsterForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId,@PathVariable String buckets) throws Exception {
		return new ResponseEntity<>(israCardService.getAgeDestributionCulsterForUC2(clusterId, criteria,buckets),
				HttpStatus.OK);
	}

	@ApiOperation("Gets as/bs distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/as_bs_distribution/{clusterId}/{bucketCountAs}/{bucketCountBs}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAsAndBsDistributionForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId, @PathVariable String bucketCountAs,@PathVariable String bucketCountBs) throws Exception {
		return new ResponseEntity<>(israCardService.getAsAndBsDistributionForUC2(clusterId, bucketCountAs, criteria,bucketCountBs),
				HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure trends")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_3/average_expenditure_trends/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpenditureTrendsForUC2(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpenditureTrendsForUC2(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets banking card distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/banking_card_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> bankingCardDistributionForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.bankingCardDistributionForUC2(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets card duration distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/card_duration_distribution/{clusterId}/{buckets}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> cardDurationDistributionForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId,@PathVariable String buckets) throws Exception {
		return new ResponseEntity<>(israCardService.cardDurationDistributionForUC2(clusterId, criteria,buckets), HttpStatus.OK);
	}

	@ApiOperation("Gets city distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/city_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCityDistributionForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getCityDistributionForUC2(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets gender distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/gender_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGenderDistributionForClusterForUC2(@RequestBody String criteria,
			@RequestParam String token, @PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getGenderDistributionForClusterForUC2(clusterId, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets issued by distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_3/issued_by_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> issuedByDistributionForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.issuedByDistributionForUC2(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets club connection stats for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_4/club_connection_stats/{clusterId}/{firstClubName}/{secondClubName}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStatsOfClubConnectionForUC2(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId, @PathVariable String firstClubName, @PathVariable String secondClubName)
			throws Exception {
		return new ResponseEntity<>(
				israCardService.getStatsOfClubConnectionForUC2(clusterId, firstClubName, secondClubName, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure per card for a customer")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/avg_expenditure_per_card/{customerId}/{cardId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpPerCardForCustomerForUC2(@RequestParam String token,
			@PathVariable String customerId, @PathVariable String cardId) throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpPerCardForCustomerForUC2(customerId, cardId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets customer details")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/customer_details/{customerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerDetailsForUC2(@RequestParam String token, @PathVariable String customerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerDetailsForUC2(customerId), HttpStatus.OK);
	}

	@ApiOperation("Gets club stats for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_3/club_stats/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> clubStatsWithDistributionForClusterForUC2(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.clubStatsWithDistributionForClusterForUC2(clusterId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets event actiivity code distribution(Installments/Standards)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/event_acitivity_code/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEventAcitivityCodeDistributionForUC2(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getEventAcitivityCodeDistributionForUC2(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets track 2 level(Online/Onsite)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/track_2_mark_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTrackTwoLevelForUC2(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getTrackTwoLevelForUC2(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets group level distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/group_level_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupLevelDistributionForUC2(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getGroupLevelDistributionForUC2(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets club connection stats for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/club_connection_stats/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClubConnectionStatsForUC2(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClubConnectionStatsForUC2(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets club connections for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_4/club_connections/{clusterId}", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClubConnectionsForUC2(@RequestParam String token, @PathVariable String clusterId,@RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClubConnectionsForUC2(clusterId,criteria), HttpStatus.OK);
	}

	@ApiOperation("Returns VLA graph dtata by card ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc2/2_4/graph/{clusterId}/{clubName}/{graphType}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphDataByCardIdForUC2(@RequestParam String token, @PathVariable String clusterId,
			@PathVariable String clubName, @PathVariable String graphType) throws Exception {
		return new ResponseEntity<>(israCardService.getGraphDataByCardIdForUC2(clusterId, clubName, graphType),
				HttpStatus.OK);
	}
	
	@ApiOperation("Gets club connections for a cluster by id and name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc2/2_4/club_connections_stats/{clusterId}/{clubName}", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClubConnectionsForUC2ByNameAndId(@RequestParam String token, @PathVariable String clusterId,@PathVariable String clubName,@RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClubConnectionsForUC2ByNameAndId(clusterId,criteria,clubName), HttpStatus.OK);
	}

	// Don't know if they're used

	@ApiOperation("Gets average expenditure ranges with cluster IDs")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/avg_expendi_ranges/{buckets}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAvgExpenditureRangesWithClusterIds(@RequestParam String token,
			@PathVariable String buckets) throws Exception {
		return new ResponseEntity<>(israCardService.getAvgExpenditureRangesWithClusterIds(buckets), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster falling on given criteria with count")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/cluster/{customerCountStart}/{customerCountEnd}/{avgExpendiStart}/{avgExpendiEnd}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterFallingOnGivenCriteria(@RequestParam String token,
			@PathVariable String customerCountStart, @PathVariable String customerCountEnd,
			@PathVariable String avgExpendiStart, @PathVariable String avgExpendiEnd) throws Exception {
		return new ResponseEntity<>(israCardService.getClusterFallingOnGivenCriteria(customerCountStart,
				customerCountEnd, avgExpendiStart, avgExpendiEnd), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster size ranges for given bucket")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/cluster_size_ranges/{buckets}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterRangesWithCustomerCount(@RequestParam String token, @PathVariable String buckets)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterRangesWithCustomerCount(buckets), HttpStatus.OK);
	}

	@ApiOperation("Gets customer count per cluster by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/customers_count/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerCount(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerCount(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets overall average expenditure")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_2/overall_avg_expenditure", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOverallExpenditureAvg(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getOverallExpenditureAvg(), HttpStatus.OK);
	}

	@ApiOperation("Gets seniority distribution for given buckets foe a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1/1_3/seniority_distribution/{clusterId}/{buckets}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getSeniorityDistribution(@RequestParam String token, @PathVariable String clusterId,
			@PathVariable String buckets) throws Exception {
		return new ResponseEntity<>(israCardService.getSeniorityDistribution(clusterId, buckets), HttpStatus.OK);
	}

	// uc3

	@ApiOperation("Gets cluster stats")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_2/cluster_stats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterSizeForUC3(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getClusterSizeForUC3(), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster falling on given criteria")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_2/cluster_stats", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterFallingOnCriteriaForUC3(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterFallingOnCriteriaForUC3(criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets cluster size")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_2/cluster_stats/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterSizeByIdForUC3(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterSizeByIdForUC3(clusterId), HttpStatus.OK);
	}

	

	@ApiOperation("Gets age distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/age_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAgeDestributionCulsterForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getAgeDestributionCulsterForUC3(clusterId, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets as/bs distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/as_bs_distribution/{clusterId}/{buckets}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAsAndBsDistributionForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId, @PathVariable String buckets) throws Exception {
		return new ResponseEntity<>(israCardService.getAsAndBsDistributionForUC3(clusterId, buckets, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets average expenditure trends")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_3/average_expenditure_trends/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAverageExpenditureTrendsForUC3(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getAverageExpenditureTrendsForUC3(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets banking card distribution")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/banking_card_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> bankingCardDistributionForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.bankingCardDistributionForUC3(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets card duration distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/card_duration_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> cardDurationDistributionForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.cardDurationDistributionForUC3(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets city distribution")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/city_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCityDistributionForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getCityDistributionForUC3(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets club stats")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_3/club_stats/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> clubStatsWithDistributionForClusterForUC3(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.clubStatsWithDistributionForClusterForUC3(clusterId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets gender distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/gender_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGenderDistributionForClusterForUC3(@RequestBody String criteria,
			@RequestParam String token, @PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getGenderDistributionForClusterForUC3(clusterId, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets issued by distribution for a cluster")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_3/issued_by_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> issuedByDistributionForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.issuedByDistributionForUC3(clusterId, criteria), HttpStatus.OK);
	}

	@ApiOperation("Gets club connection stats by cluster based on clubs")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_4/club_connection_stats/{clusterId}/{firstClubName}/{secondClubName}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStatsOfClubConnectionForUC3(@RequestBody String criteria, @RequestParam String token,
			@PathVariable String clusterId, @PathVariable String firstClubName, @PathVariable String secondClubName)
			throws Exception {
		return new ResponseEntity<>(
				israCardService.getStatsOfClubConnectionForUC3(clusterId, firstClubName, secondClubName, criteria),
				HttpStatus.OK);
	}

	@ApiOperation("Gets club connections")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/club_connections/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getClubConnectionsForUC3(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClubConnectionsForUC3(clusterId), HttpStatus.OK);
	}

	

	@ApiOperation("Gets event activity code distribution(Installments/Standard)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/event_acitivity_code/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEventAcitivityCodeDistributionForUC3(@RequestParam String token,
			@PathVariable String clusterId) throws Exception {
		return new ResponseEntity<>(israCardService.getEventAcitivityCodeDistributionForUC3(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Returns VLA graph data by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/graph/{clusterId}/{clubName}/{graphType}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getVLAGraphForUC3(@RequestParam String token, @PathVariable String clusterId,
			@PathVariable String clubName, @PathVariable String graphType) throws Exception {
		return new ResponseEntity<>(israCardService.getVLAGraphForUC3(clusterId, clubName, graphType), HttpStatus.OK);
	}

	@ApiOperation("Gets group level distribution(local/abroad)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/group_level_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupLevelDistributionForUC3(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getGroupLevelDistributionForUC3(clusterId), HttpStatus.OK);
	}

	@ApiOperation("Gets track 3 level(Online/Onsite)")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/track_3_mark_distribution/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTrackThreeLevelForUC3(@RequestParam String token, @PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getTrackThreeLevelForUC2(clusterId), HttpStatus.OK);
	}
	
	
	///NEW ISRA CARD UC1_WITH_STORY
	@ApiOperation("Gets stories")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1_with_story/1_2/stories", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getStories(@RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(israCardService.getStories(), HttpStatus.OK);
	}

	@ApiOperation("gets clusters for uc1 with story")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1_with_story/1_2/clusters/{storyId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterForUc1WithStory(@PathVariable String storyId,@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterForUc1WithStory(criteria,storyId), HttpStatus.OK);
	}
	
	@ApiOperation("gets distributions")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1_with_story/1_2/distributions/{storyId}/{clusterId}/{attributeName}/{attributeType}/{distributionCount}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDistribution(@PathVariable String storyId, @PathVariable String clusterId,
			@PathVariable String attributeName, @PathVariable String attributeType,
			@PathVariable String distributionCount, @RequestParam String token, @RequestBody String jsonToSent)
			throws Exception {
		return new ResponseEntity<>(israCardService.getDistribution(jsonToSent, storyId, clusterId, attributeName,
				attributeType, distributionCount), HttpStatus.OK);
	}
	
	@ApiOperation("gets customers for uc1 with story")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc1_with_story/1_2/customers/{storyId}/{clusterId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomersForUc1WithStory(@PathVariable String clusterId,@PathVariable String storyId,@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomersForUc1WithStory(criteria,storyId,clusterId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets VLA graph data by customer Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1_with_story/1_4/graph/{customerId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getVLAByCustomerId(@RequestParam String token,@PathVariable String customerId )
			throws Exception {
		return new ResponseEntity<>(israCardService.getVLAByCustomerId(customerId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets customer details by customer Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1_with_story/1_4/customer_details/{customerId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerDetailsById(@RequestParam String token,@PathVariable String customerId )
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerDetailsById(customerId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets cluster by story Id and cluster Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1_with_story/1_2/cluster/{storyId}/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getClusterByStoryIdAndClusterId(@RequestParam String token,@PathVariable String storyId,@PathVariable String clusterId )
			throws Exception {
		return new ResponseEntity<>(israCardService.getClusterByStoryIdAndClusterId(storyId,clusterId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets customer stories clusters by Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc1_with_story/1_4/customer__stories_clusters/{customerId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerStoryClustersById(@RequestParam String token,@PathVariable String customerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerStoryClustersById(customerId), HttpStatus.OK);
	}
	
	
	
	///////////////////////////////////////////////UC3 NEW APIS /////////////////////////////////////////////////
	
	@ApiOperation("Gets UC3 clusters by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "uc3/3_2/cluster/{clusterId}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC3ClustersById(@RequestParam String token,@PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC3ClustersById(clusterId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC3 clusters by query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_2/clusters", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC3ClustersByQuery(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC3ClustersByQuery(criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC3 customers by cluster ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_2/customers/{clusterId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC3CustomersByClusterId(@PathVariable String clusterId,@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC3CustomersByClusterId(clusterId,criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC3 distribution")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc3/3_2/distributions/{clusterId}/{attributeName}/{attributeType}/{distributionCount}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC3DistributionById(@PathVariable String clusterId,@PathVariable String attributeName,@PathVariable String attributeType,@PathVariable String distributionCount,@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC3DistributionById(clusterId,attributeName,attributeType,distributionCount,criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Gets overall expenditure avg and sum")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_2/overall_stats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOverAllExpenditureAvgAndSumForUC3(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.getOverAllExpenditureAvgAndSumForUC3(), HttpStatus.OK);
	}
	
	@ApiOperation("Gets customer details")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/customer_details/{customerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerDetailsForUC3(@RequestParam String token, @PathVariable String customerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerDetailsForUC3(customerId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets customer story clusters")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/customer_stories_clusters/{customerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerStoryClusterForUC3(@RequestParam String token, @PathVariable String customerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerStoryClusterForUC3(customerId), HttpStatus.OK);
	}
	
	@ApiOperation("Returns VLA graph data by customer ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/graph/{customerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getVLAGraphByIDForUC3(@RequestParam String token, @PathVariable String customerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getVLAGraphByIDForUC3(customerId), HttpStatus.OK);
	}
	
	@ApiOperation("Search UC3 customer by Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc3/3_4/search_customer/{clusterId}/{ownerId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC3CustomerById(@RequestParam String token, @PathVariable String clusterId, @PathVariable String ownerId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC3CustomerById(clusterId,ownerId), HttpStatus.OK);
	}
	
	//naming
	@ApiOperation("Gets cluster name by Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/naming/getClusterName/{storyId}/{clusterId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getclusterNamingById(@RequestParam String token, @PathVariable String storyId,@PathVariable String clusterId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getNamingById(storyId,clusterId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets story name by Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/naming/getStoryName/{storyId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStoryNameById(@RequestParam String token, @PathVariable String storyId)
			throws Exception {
		return new ResponseEntity<>(israCardService.getStoryNameById(storyId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC3 clusters by query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/naming/setClusterName/{storyId}/{clusterId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> setclusterNamingById(@RequestParam String token, @PathVariable String storyId,@PathVariable String clusterId, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.setclusterNamingById(storyId,clusterId,criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC3 clusters by query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/naming/setStoryName/{storyId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> setStoryNameById(@RequestParam String token,@PathVariable String storyId, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.setStoryNameById(storyId,criteria), HttpStatus.OK);
	}
	
	//stories
	@ApiOperation("Gets all stories")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/stories", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllStories(@RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(israCardService.getAllStories(), HttpStatus.OK);
	}
	
	@ApiOperation("Adds a new story")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/stories", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> addANewStory(@RequestParam String token, @RequestBody String criteria,@RequestParam String storyName)
			throws Exception {
		Long userIdTemp = getCurrentUserId();
		return new ResponseEntity<>(israCardService.addANewStory(criteria,storyName,userIdTemp), HttpStatus.OK);
	}
	
	@ApiOperation("Deletes all stories")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/stories", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAllStories(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(israCardService.deleteAllStories(), HttpStatus.OK);
	}
	
	@ApiOperation("Gets story by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/stories/{storyId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStoriesById(@PathVariable String storyId,@RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(israCardService.getStoriesById(storyId), HttpStatus.OK);
	}
	
	@ApiOperation("Updates story by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/stories/{storyId}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> UpdateStoryById(@PathVariable String storyId,@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.UpdateStoryById(storyId,criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Deletes story by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/stories/{storyId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteStoryById(@PathVariable String storyId,@RequestParam String token,@RequestParam String storyName) throws Exception {
		Long userIdTemp = getCurrentUserId();
		return new ResponseEntity<>(israCardService.deleteStoryById(storyId,storyName,userIdTemp), HttpStatus.OK);
	}
	
	/////////////////////////////////////////UC4 APIS///////////////////////////////////////////////////
	
	@ApiOperation("Gets Available UC4 attributes")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/uc4/available_attributes", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAvaliableAttributesForUC4(@RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(israCardService.getAvaliableAttributesForUC4(), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC4 customers by filters")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc4/customers", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC4CustomersByFilters(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC4CustomersByFilters(criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC4 customer count")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc4/customers_count", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerCountForUC4(@RequestParam String token,@RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(israCardService.getCustomerCountForUC4(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC4 distributions")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc4/distributions/{attributeName}/{attributeType}/{distributionCount}", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getDistributionsForUC4(@PathVariable String attributeName,@PathVariable String attributeType,@PathVariable String distributionCount,@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getDistributionsForUC4(attributeName,attributeType,distributionCount,criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Gets UC4 popular merchat types")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc4/popular_merchant_types", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC4PopularMerchantTypes(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC4PopularMerchantTypes(criteria), HttpStatus.OK);
	}
	
	@ApiOperation("Get UC4 top early adaptors")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/uc4/top_early_adapters", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getUC4TopEarlyAdaptors(@RequestParam String token, @RequestBody String criteria)
			throws Exception {
		return new ResponseEntity<>(israCardService.getUC4TopEarlyAdaptors(criteria), HttpStatus.OK);
	}
	
}
