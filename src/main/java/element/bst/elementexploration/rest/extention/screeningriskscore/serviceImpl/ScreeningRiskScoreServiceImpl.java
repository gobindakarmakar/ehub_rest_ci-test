package element.bst.elementexploration.rest.extention.screeningriskscore.serviceImpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.runtime.BatchExecutionCommandImpl;
import org.kie.api.KieServices;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.screeningriskscore.domain.Rules;
import element.bst.elementexploration.rest.extention.screeningriskscore.domain.ScreenigHit;
import element.bst.elementexploration.rest.extention.screeningriskscore.domain.ScreeningDetails;
import element.bst.elementexploration.rest.extention.screeningriskscore.service.ScreeningRiskScoreService;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("screeningRiskScoreService")
public class ScreeningRiskScoreServiceImpl implements ScreeningRiskScoreService{
	
	@Value("${kie_server_url}")
	private String KIE_SERVER_URL;

	@Value("${kie_server_username}")
	private String KIE_SERVER_USERNAME;

	@Value("${kie_server_password}")
	private String KIE_SERVER_PASSWORD;
	
	@Value("${screening_score_ruleset_name}")
	private String SCREENING_SCORE_RULESET_NAME;
	
	@Value("${screening_info_url}")
	private String SCREENING_INFO_URL;
	
	@Autowired
	AlertDao alertDao;

	@Override
	@Transactional("transactionManager")
	public ScreeningDetails screeningRiskSocreCalculation(ScreeningDetails screeningDetails) {
		ScreeningDetails screeingDetailsWithScore = new ScreeningDetails();
		screeingDetailsWithScore.setType(screeningDetails.getType());
		try {
			if (screeningDetails != null) {

				KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(KIE_SERVER_URL,
						KIE_SERVER_USERNAME, KIE_SERVER_PASSWORD);
				Set<Class<?>> allClasses = new HashSet<Class<?>>();
				allClasses.add(ScreenigHit.class);
				config.addExtraClasses(allClasses);
				KieServicesClient client = KieServicesFactory.newKieServicesClient(config);
				RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
				List<ScreenigHit> screenigHits = screeningDetails.getHits();
				
				List<ScreenigHit> screenigHitsWithRule = new ArrayList<ScreenigHit>();
				for (ScreenigHit hit : screenigHits) {
					List<GenericCommand<?>> commands = new ArrayList<GenericCommand<?>>();
					commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newInsert(hit, "Score"));
					commands.add(
							(GenericCommand<?>) KieServices.Factory.get().getCommands().newFireAllRules("FireRules"));
					BatchExecutionCommandImpl batchCommand = new BatchExecutionCommandImpl(commands);
					batchCommand.setLookup("defaultKieSession");
					ServiceResponse<ExecutionResults> responseResult = ruleClient
							.executeCommandsWithResults(SCREENING_SCORE_RULESET_NAME, batchCommand);
					ScreenigHit resultScreenigHits = (ScreenigHit) responseResult.getResult().getValue("Score");
					if (resultScreenigHits != null) {						
						if(resultScreenigHits.getRules().size()!=0){
							Rules maxRiskScore = resultScreenigHits.getRules().stream()
									.max(Comparator.comparingInt(Rules::getMatchScore)).get();							
							hit.getRules().add(maxRiskScore);
							screenigHitsWithRule.add(hit);
							if(maxRiskScore.getMatchScore()>90)
							{
								Alert alert = new Alert();								
								alert.setAlertType("Internal");								
								alert.setCreatedOn(new Date());
								if(EntityType.valueOf(screeningDetails.getType().toUpperCase())!= null)
									alert.setEntityType(EntityType.valueOf(screeningDetails.getType().toUpperCase()));
								alert.setRiskScore(maxRiskScore.getMatchScore());
								alert.setAlertStatus(AlertStatus.OPEN);
								alert.setAlertDescription("rule");
								if(screeningDetails.getType() != null){
									if(screeningDetails.getType().equalsIgnoreCase("person"))
										alert.setEntityType(EntityType.PERSON);
									if(screeningDetails.getType().equalsIgnoreCase("organization"))
										alert.setEntityType(EntityType.ORGANIZATION);
								}
								
								alertDao.create(alert);
							}
							
						}
					}

				}
				screeingDetailsWithScore.setHits(screenigHitsWithRule);
			}
		}catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return screeingDetailsWithScore;
	}

	@Override
	public String getScreeningInfo(String jsonString) throws Exception {
		String url = SCREENING_INFO_URL + "/screenings";
		return postStringDataToServerWithScreeningKey(url, jsonString);
	}
	
	
	private String postStringDataToServerWithScreeningKey(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeoutWithScreeningKey(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

}
