package element.bst.elementexploration.rest.extention.entity.dao;

import element.bst.elementexploration.rest.extention.entity.domain.EntityRequirements;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface EntityRequirementsDao extends GenericDao<EntityRequirements, Long> {

}
