package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTableRowsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableRows;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentTableRowsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TableRowDataDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("documentTableRowsDao")
public class DocumentTableRowsDaoImpl extends GenericDaoImpl<DocumentTableRows, Long> implements DocumentTableRowsDao {

	public DocumentTableRowsDaoImpl() {
		super(DocumentTableRows.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TableRowDataDto> getAllRowsFromTable(Long tableId) {
		 List<TableRowDataDto> tableRowDataDtoList=new ArrayList<TableRowDataDto>();
		 StringBuilder builder = new StringBuilder();
		 try{
			 builder.append("select new element.bst.elementexploration.rest.extention.docparser.dto.TableRowDataDto(dtc.columnName,dtr.rowName)");
			 builder.append(" from  DocumentTableColumns dtc,DocumentTableRows dtr ");
			 builder.append("where dtc.documentContent.id= :tableId ");
			 builder.append(" and dtr.documentTableColumns.id=dtc.id ");
			 
			 
			// builder.append("from  ");
			 Query<?> query = this.getCurrentSession().createQuery(builder.toString());
				query.setParameter("tableId", tableId);
				tableRowDataDtoList=(List<TableRowDataDto>) query.getResultList();	
		 }catch (Exception e) {
			 return tableRowDataDtoList;
		}
		return tableRowDataDtoList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentTableRowsDto> getAllRowsDataByColumnId(Long columnId) {
		List<DocumentTableRowsDto> columnRowDataList=new ArrayList<DocumentTableRowsDto>();
				
		StringBuilder builder = new StringBuilder();
		 try{
			 builder.append("select new element.bst.elementexploration.rest.extention.docparser.dto.DocumentTableRowsDto(dtr.id,dtr.rowName) from DocumentTableRows dtr ");
			 builder.append("where dtr.documentTableColumns.id = :columnId"); 
			 Query<?> query = this.getCurrentSession().createQuery(builder.toString());
				query.setParameter("columnId", columnId);
				columnRowDataList=(List<DocumentTableRowsDto>) query.getResultList();
				return columnRowDataList;
		 }catch (Exception e) {
			 return columnRowDataList;
		}
		
	}
}
