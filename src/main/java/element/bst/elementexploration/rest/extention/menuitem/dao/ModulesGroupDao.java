package element.bst.elementexploration.rest.extention.menuitem.dao;

import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Jalagari Paul
 *
 */
public interface ModulesGroupDao extends GenericDao<ModulesGroup, Long> {

}
