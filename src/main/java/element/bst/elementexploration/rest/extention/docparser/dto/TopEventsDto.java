package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Document content")
public class TopEventsDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Event id")
	private Long eventId;
	@ApiModelProperty(value = "Event location name")
	private String locationName;
	@ApiModelProperty(value = "Event title")
	private String title;
	@ApiModelProperty(value = "Event text")
	private String text;
	@ApiModelProperty(value = "Event url")
	private String url;
	@ApiModelProperty(value = "Event technologies")
	private String technologies;
	@ApiModelProperty(value = "Event published on")
	private Date published;

	public TopEventsDto() {
		super();
	}

	public TopEventsDto(Long eventId, String locationName, String title, String text, String url, String technologies,
			Date published) {
		super();
		this.eventId = eventId;
		this.locationName = locationName;
		this.title = title;
		this.text = text;
		this.url = url;
		this.technologies = technologies;
		this.published = published;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTechnologies() {
		return technologies;
	}

	public void setTechnologies(String technologies) {
		this.technologies = technologies;
	}

	public Date getPublished() {
		return published;
	}

	public void setPublished(Date published) {
		this.published = published;
	}

	

}
