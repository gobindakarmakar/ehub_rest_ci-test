package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Paul Jalagari
 *
 */
public class HitsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ActorDto actor_a;
	private List<String> industry;
	private String time;
	private String snippet;
	private ActorDto actor_b;
	private String source;
	private String title;
	private String class_;
	private String url;
	private LocationDto location;
	private List<String> attack_types;
	private String published;
	private List<TechnologiesDto> technologies;
	private String text;

	public ActorDto getActor_a() {
		return actor_a;
	}

	public void setActor_a(ActorDto actor_a) {
		this.actor_a = actor_a;
	}

	public List<String> getIndustry() {
		return industry;
	}

	public void setIndustry(List<String> industry) {
		this.industry = industry;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public ActorDto getActor_b() {
		return actor_b;
	}

	public void setActor_b(ActorDto actor_b) {
		this.actor_b = actor_b;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getClass_() {
		return class_;
	}

	public void setClass_(String class_) {
		this.class_ = class_;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public List<String> getAttack_types() {
		return attack_types;
	}

	public void setAttack_types(List<String> attack_types) {
		this.attack_types = attack_types;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public List<TechnologiesDto> getTechnologies() {
		return technologies;
	}

	public void setTechnologies(List<TechnologiesDto> technologies) {
		this.technologies = technologies;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
