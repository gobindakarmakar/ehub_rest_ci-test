package element.bst.elementexploration.test.version.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.version.daoImpl.VersionDaoImpl;
import element.bst.elementexploration.rest.version.domain.ProductVersion;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class VersionDaoTest {
	
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@SuppressWarnings("rawtypes")
	@Mock
	NativeQuery nativeQuery;
	
	@Mock
	CriteriaBuilder builder;
	
	@Mock
	CriteriaQuery<Object> criteriaQuery;
	
	@Mock
	Root<ProductVersion> root;

	@InjectMocks
	VersionDaoImpl versionDao;
	
	ProductVersion productVersion;
	
	List<ProductVersion> versionsList = new ArrayList<ProductVersion>(); 
	
	@Before
	public void doSetup() {
		productVersion = new ProductVersion();
		productVersion.setId(1L);
		productVersion.setVersion("1.7");
		versionsList.add(productVersion);
		MockitoAnnotations.initMocks(this);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getLatestVersionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(ProductVersion.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(versionsList);
		ProductVersion releaseProductVersion = versionDao.getLatestVersion();
		assertEquals("1.7", releaseProductVersion.getVersion());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getLatestVersion_whenFailedToExecuteQueryExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(ProductVersion.class)).thenReturn(root);	
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(versionsList);
		versionDao.getLatestVersion();
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllVersionsTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(ProductVersion.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(versionsList);
		List<ProductVersion> versionsListNew = versionDao.getAllVersions();
		assertEquals(1, versionsListNew.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllVersions_whenFailedToExecuteQueryExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(ProductVersion.class)).thenReturn(root);	
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(versionsList);
		versionDao.getLatestVersion();
		
	}
	
	

}
