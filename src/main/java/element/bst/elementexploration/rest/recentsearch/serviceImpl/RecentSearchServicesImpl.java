package element.bst.elementexploration.rest.recentsearch.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.adversenews.service.AdverseNewsService;
import element.bst.elementexploration.rest.extention.entitysearch.service.EntitySearchService;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.recentsearch.dao.RecentSearchDao;
import element.bst.elementexploration.rest.recentsearch.domain.RecentSearchs;
import element.bst.elementexploration.rest.recentsearch.dto.EntityDto;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;
import element.bst.elementexploration.rest.recentsearch.dto.SearchDto;
import element.bst.elementexploration.rest.recentsearch.service.RecentSearchService;
import element.bst.elementexploration.rest.search.service.SearchHistoryService;

@Service("recentService")
public class RecentSearchServicesImpl extends GenericServiceImpl<RecentSearchs, Long> implements RecentSearchService {

	@Autowired
	public RecentSearchServicesImpl(GenericDao<RecentSearchs, Long> genericDao) {
		super(genericDao);
	}
	
	@Autowired
	private RecentSearchDao recentSearchDao;
	
	@Autowired
	private CaseService caseService;
	
	@Autowired
	private AlertService alertService;
	
	@Autowired
	private AdvanceSearchService advanceSearchService;
	
	@Autowired
	private EntitySearchService entitySearchService;
	
	@Autowired
	private AdverseNewsService adverseNewsService;
	
	@Autowired
	private SearchHistoryService searchHistoryService;

	@Override
	@Transactional("transactionManager")
	public SearchDto getSearchs(String searchKeyword) throws Exception {
		List<CaseUnderwritingDto> caseList = new ArrayList<>();
		List<AlertsDto> alertList = new ArrayList<>();
		List<EntityDto> entities = new ArrayList<>();
		SearchDto dto=new SearchDto();

		caseList = caseService.searchCaseList(searchKeyword);
		alertList = alertService.searchAlerts(searchKeyword);
		String response = advanceSearchService.getEntityByName(searchKeyword);
		String response1 = entitySearchService.getOrgNamesSuggestions(searchKeyword,null);
		String response2 =null;
		if(response1!=null){
			JSONObject json=new JSONObject(response1);
			//SONArray array=null;
			if(json.has("hits")){
				JSONArray array=json.getJSONArray("hits");
				if(array.length()>0){
					JSONObject json1=array.getJSONObject(0);
					if(json1.has("@identifier")){
						response2 = adverseNewsService.getAdverseProfileById(json1.getString("@identifier"));

					}

				}
			}
			
		}
		String personResponse=entitySearchService.getPersonBySuggestedName(searchKeyword);
		String personResponse2 =null;
		if(response1!=null){
			JSONObject json=new JSONObject(personResponse);
			//SONArray array=null;
			if(json.has("hits")){
				JSONArray array=json.getJSONArray("hits");
				if(array.length()>0){
					JSONObject json1=array.getJSONObject(0);
					if(json1.has("@identifier")){
						personResponse2 = entitySearchService.getPersonByIdentifier(json1.getString("@identifier"));

					}

				}
			}
			
		}
		JSONObject search = new JSONObject();
        org.json.simple.JSONArray list = new org.json.simple.JSONArray();
        list.add("1016");
        search.put("fetchers", list);
		search.put("keyword", searchKeyword);
		search.put("searchType", "Company");
		search.put("limit", "1");
		String logoResopnse=searchHistoryService.getSearchData(null, search.toString(), "search");
		String logoUrl=null;
		if(logoResopnse != null){
			JSONObject json=new JSONObject(logoResopnse);
			if(json.has("results")){
				JSONObject result=null;
				JSONObject entityObj =null;
				JSONArray resultArray=json.getJSONArray("results");
				if(resultArray.length()>0)	{
					result=(JSONObject) resultArray.get(0);
				}
				if(result.has("entities")){
					JSONArray entityArray=result.getJSONArray("entities");
					if(entityArray.length()>0)	{
						entityObj = (JSONObject) entityArray.get(0);
					}
					if(entityObj.has("properties")){
						JSONObject properties=entityObj.getJSONObject("properties");
						logoUrl=properties.getString("url");
					}
				}

			}

		}
		
		List<Case> cases=caseService.findAll();
		for (Case caseSeed : cases) {
			byte[] fileData = caseSeed.getCaseJsonFile();
			if(fileData!=null) {
			String s=new String(fileData);
			if(Pattern.compile(Pattern.quote(searchKeyword), Pattern.CASE_INSENSITIVE).matcher(s).find()){
				JSONObject json=new JSONObject(s);
				JSONArray personArray=json.getJSONArray("persons");
				JSONArray companiesArray=json.getJSONArray("companies");
				if(personArray.length()>0){
					for (int i = 0; i < personArray.length(); i++) {
						EntityDto entity=new EntityDto();
						JSONObject person=personArray.getJSONObject(i);
						entity.setName(person.getString("name"));
						entity.setType("Individual");
						entity.setCaseId(caseSeed.getCaseId());
						entities.add(entity);

					}
				}
				
				if(companiesArray.length()>0){
					for (int i = 0; i < companiesArray.length(); i++) {
						EntityDto entity=new EntityDto();
						JSONObject company=companiesArray.getJSONObject(i);
						entity.setName(company.getString("name"));
						entity.setType("Corporate");
						entity.setCaseId(caseSeed.getCaseId());
						entity.setLogo(logoUrl);
						entities.add(entity);

					}
				}
					

			}

		}
	}
		
		String details=entitySearchService.getOrganizationBySuggestedName(searchKeyword);
		String id_details=null;
		if(details != null){
			JSONObject json=new JSONObject(details);
			if(json.has("hits")){
				JSONArray array=json.getJSONArray("hits");
				if(array.length()>0){
					JSONObject id_details1=array.getJSONObject(0);
					id_details=id_details1.toString();

				}
			}
		}

			list.remove("1016");
			list.add("1003");
			search.remove("fetchers");
	        search.put("fetchers", list);
			String entityNews=searchHistoryService.getSearchData(null, search.toString(), "search");

		dto.setAlertList(alertList);
		dto.setCaseList(caseList);
		dto.setNetwork(response);
		dto.setOrgNews(response2);
		dto.setPersonNews(personResponse2);
		dto.setEntityNews(entityNews);
		dto.setEntities(entities);
		dto.setId_details(id_details);
		return dto;

	}

	@Override
	@Transactional("transactionManager")
	public RecentSearchs saveSearch(String search,Long userId) {
		RecentSearchs recentSearchs=new RecentSearchs();
		recentSearchs.setCreatedOn(new Date());
		recentSearchs.setSearch(search);
		recentSearchs.setUserId(userId);
		return recentSearchDao.create(recentSearchs);
	}

	@Override
	@Transactional("transactionManager")
	public List<RecentSearchDto> getRecentSearchs(Long userId) {
		return recentSearchDao.getRecentSearchs(userId);
	}
	
}
