package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class DocParserContent implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String contentType;
	private boolean isQuestion;
	private DocParserContentSave contentData;
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public boolean isQuestion() {
		return isQuestion;
	}
	public void setQuestion(boolean isQuestion) {
		this.isQuestion = isQuestion;
	}
	public DocParserContentSave getContentData() {
		return contentData;
	}
	public void setContentData(DocParserContentSave contentData) {
		this.contentData = contentData;
	}
	
	
	
	
	

}
