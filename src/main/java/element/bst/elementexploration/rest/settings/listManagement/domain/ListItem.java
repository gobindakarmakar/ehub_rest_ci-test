package element.bst.elementexploration.rest.settings.listManagement.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("List Management")
@Entity
@Table(name = "LM_LIST_ITEMS")
public class ListItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long listItemId;

	@ApiModelProperty(value = "ID if the Entity")
	@Column(name = "code")
	private String code;

	@ApiModelProperty(value = "Name of the List Item")
	@Column(name = "displayName")
	private String displayName;

	@ApiModelProperty(value = "Icon if the List Type has")
	@Column(name = "icon")
	private String icon;

	@ApiModelProperty(value = "Source if the Entity")
	@Column(name = "allowdelete")
	private Boolean allowDelete;

	@ApiModelProperty(value = "Source if the Entity")
	@Column(name = "list_type")
	private String listType;

	@ApiModelProperty(value = "Color if the ListItem")
	@Column(name = "color_code")
	private String colorCode;

	@ApiModelProperty(value = "Flag if the ListItem")
	@Column(name = "flag_name")
	private String flagName;

	@ManyToOne
	@JoinColumn(name = "ID")
	private Attributes attributes;

	@JsonIgnore
	@OneToMany(mappedBy = "countryId", cascade = CascadeType.ALL, targetEntity = Users.class, fetch = FetchType.LAZY)
	private List<Users> userCountry;

	@JsonIgnore
	@OneToMany(mappedBy = "statusId", cascade = CascadeType.ALL, targetEntity = Users.class, fetch = FetchType.LAZY)
	private List<Users> userStatus;
	
	//@JsonIgnore
	//@OneToMany(mappedBy = "reviewStatusId", cascade = CascadeType.ALL, targetEntity = Alerts.class, fetch = FetchType.LAZY)
	//private List<Alerts> alertReviewStatus;

	public Long getListItemId() {
		return listItemId;
	}

	public void setListItemId(Long listItemId) {
		this.listItemId = listItemId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Boolean getAllowDelete() {
		return allowDelete;
	}

	public void setAllowDelete(Boolean allowDelete) {
		this.allowDelete = allowDelete;
	}

	public String getListType() {
		return listType;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public List<Users> getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(List<Users> userCountry) {
		this.userCountry = userCountry;
	}

	public List<Users> getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(List<Users> userStatus) {
		this.userStatus = userStatus;
	}

}
