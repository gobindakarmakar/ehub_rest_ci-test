package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.List;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.AnswerTopEventsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.AnswerTopEvents;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("answerTopEventsDao")
public class AnswerTopEventsDaoImpl extends GenericDaoImpl<AnswerTopEvents, Long> implements AnswerTopEventsDao {

	public AnswerTopEventsDaoImpl() {
		super(AnswerTopEvents.class);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AnswerTopEvents> getTopEventsByAnswerId(Long answerId) {
		List<AnswerTopEvents> answerTopEvents=this.getCurrentSession()
				.createQuery("from AnswerTopEvents ate where ate.documentAnswer.id = " +answerId ).getResultList(); 
		return answerTopEvents;
	}

	
}
