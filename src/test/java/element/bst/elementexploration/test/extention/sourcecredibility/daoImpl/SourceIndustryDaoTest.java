package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourceIndustryDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;

/**
 * @author suresh
 *
 */
public class SourceIndustryDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourceIndustryDaoImpl sourceIndustryDao;

	SourceIndustry sourceIndustry = null;
	List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();

	@Before
	public void setupMock() {
		sourceIndustry = new SourceIndustry();
		sourceIndustry.setIndustryId(1L);
		sourceIndustry.setIndustryName("Core");
		SourceIndustry sourceIndustryTwo = new SourceIndustry();
		sourceIndustryTwo.setIndustryId(1L);
		sourceIndustryTwo.setIndustryName("Excel");
		industryList.add(sourceIndustry);
		industryList.add(sourceIndustryTwo);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchIndustry() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sourceIndustry);
		SourceIndustry sourcesIndustrySaved = sourceIndustryDao.fetchIndustry("Core");
		assertEquals(sourceIndustry.getIndustryName(), sourcesIndustrySaved.getIndustryName());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fetchIndustry_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		sourceIndustryDao.fetchIndustry("PPT");
	}

	@Test
	public void fetchIndustryListExceptAll() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(industryList);
		List<SourceIndustry> IndustryListSaved = sourceIndustryDao.fetchIndustryListExceptAll();
		assertEquals(IndustryListSaved.size(), industryList.size());
	}

}
