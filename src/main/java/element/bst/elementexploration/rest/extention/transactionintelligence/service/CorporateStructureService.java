package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureTopFiveDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Paul Jalagari
 * 
 */
public interface CorporateStructureService extends GenericService<TransactionsData, Long> {

	/// YES
	public CorporateStructureDto getCorporateStructure(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException;

	/// YES
	public List<CorporateStructureDto> getCorporateStructureAggregates(String fromDate, String toDate,
			FilterDto filterDto);

	public List<CorporateStructureDto> getCorporateStructuteByType(String fromDate, String toDate, String type,
			List<String> scenarios);

	public List<CorporateStructureDto> getAlertEntitiesBusinessType(String fromDate, String toDate);

	public List<CorporateStructureDto> getCustomerByCountryScenario(String fromDate, String toDate, String country,
			List<String> scenarios);

	public List<CorporateStructureDto> getCustomerByBusinessScenario(String fromDate, String toDate, String country,
			List<String> scenarios);

	public List<CorporateStructureDto> getCounterPartyByCountryCustomer(String fromDate, String toDate, String country,
			Long customerId, List<String> scenarios);

	public List<CorporateStructureDto> getCounterPartyByBusinessCustomer(String fromDate, String toDate, String country,
			Long customerId, List<String> scenarios);

	public Boolean insertShareHolders();

	public CorporateStructureDto getCorporateScenariosByType(String fromDate, String toDate, String type);

	public List<CorporateStructureDto> getCorporateStructuteByScenarios(String fromDate, String toDate,
			FilterDto filterDto);

	/// YES
	public CorporateStructureDto getAlertEntitiesGeography(String fromDate, String toDate, FilterDto filterDto);

	public CorporateStructureDto getScenariosByCountry(String fromDate, String toDate, String type);

	public CorporateStructureDto getScenariosByBusinessType(String fromDate, String toDate, String businessType);

	/// YES
	public List<CorporateStructureDto> getShareholderCorporateStructure(String fromDate, String toDate,
			FilterDto filterDto);

	public List<CorporateStructureDto> getShareholderCountry(String fromDate, String toDate);

	/// YES
	public CorporateStructureDto getShareholderAggregates(String fromDate, String toDate, FilterDto filterDto);

	public List<CorporateStructureDto> getCustomerByShareholderScenario(String fromDate, String toDate, String industry,
			List<String> scenarios);

	public CorporateStructureDto getScenariosByShareholder(String fromDate, String toDate, String type);

	public List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate, Integer pageNumber,
			Integer recordsPerPage, String type, FilterDto filterDto);

	public long getViewAllCorporateStructureCount(String fromDate, String toDate, String type);

	public List<CorporateStructureDto> getAssociatedAlerts(String fromDate, String toDate, FilterDto filterDto);

	/// YES
	public CorporateStructureTopFiveDto getTopFiveAlerts(String fromDate, String toDate,
			FilterDto filterDto);

	/// YES
	public List<CorporateStructureDto> getGeographyAggregates(String fromDate, String toDate, FilterDto filterDto);

	public Map<String, List<CorporateStructureDto>> getCorporateStructureByType(String fromDate, String toDate,
			String type, FilterDto filterDto);

	public List<CorporateStructureDto> getGeographyByScenarios(String fromDate, String toDate, FilterDto filterDto);

	public Map<String, List<CorporateStructureDto>> getShareholderByType(String fromDate, String toDate, String type,
			FilterDto filterDto);

	public Map<String, List<CorporateStructureDto>> getGeographyByType(String fromDate, String toDate, String type,
			FilterDto filterDto);

	public List<CorporateStructureDto> getShareholderByScenarios(String fromDate, String toDate, FilterDto filterDto);

}
