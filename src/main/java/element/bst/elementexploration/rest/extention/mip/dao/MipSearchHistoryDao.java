package element.bst.elementexploration.rest.extention.mip.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.mip.domain.MipSearchHistory;
import element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface MipSearchHistoryDao extends GenericDao<MipSearchHistory, Long> {

	List<MipSearchHistoryDTO> getSearchHistoryList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countSearchHistoryList(Long userId);

	boolean isSearchHistoryOwner(Long searchId, Long userId);

}
