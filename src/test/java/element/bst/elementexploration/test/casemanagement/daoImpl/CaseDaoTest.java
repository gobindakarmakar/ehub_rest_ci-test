package element.bst.elementexploration.test.casemanagement.daoImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseDaoImpl;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

@RunWith(MockitoJUnitRunner.class)
public class CaseDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@InjectMocks
	CaseDaoImpl caseDao;

	Users user;

	Case caseSeed;

	List<CaseDto> caseDtoList;

	CaseDto caseDto;

	List<BigInteger> list = new ArrayList<>();

	List<Integer> listInt = new ArrayList<>();

	List<Long> listLong = new ArrayList<>();

	public CaseVo caseVo;
	
	List<CaseAggregator> caseAggregatorList = new ArrayList<CaseAggregator>();

	CaseAggregatorDto caseAggregatorDto = new CaseAggregatorDto();
	 
	List<CaseUnderwritingDto> caseUnderwritingDtoList = new ArrayList<>();

	@Before
	public void setupMock() {
		user = new Users();
		caseSeed = new Case();
		caseSeed.setCreatedBy(user);
		caseSeed.setCaseId(1L);
		list.add(new BigInteger("1"));
		caseDto = new CaseDto();
		caseDto.setCaseId(1L);
		caseDto.setCreatedOn(new Date());
		caseDtoList = new ArrayList<>();
		caseDtoList.add(caseDto);
		listInt.add(1);
		listInt.add(2);
		caseVo = new CaseVo();
		caseVo.setType("type");
		caseVo.setModifiedDate(new Date());
		caseVo.setCreationDate(new Date());
		caseVo.setPriority(PriorityEnums.MEDIUM);
		caseVo.setStatus(listInt);
		caseVo.setKeyword("keyword");
		caseVo.setOrderBy("name");
		caseVo.setOrderIn("desc");
		caseVo.setPageNumber(1);
		caseVo.setRecordsPerPage(15);
		CaseAggregator caseAggregator = new CaseAggregator("period", 1L, 2L, 3L, 4L, 5L, 5L, 1L, 1L);
		caseAggregatorList.add(caseAggregator);
		caseAggregatorDto.setFromDate(new Date());
		caseAggregatorDto.setToDate(new Date());
		caseAggregatorDto.setStatus(listInt);
		caseAggregatorDto.setType("type");
		caseAggregatorDto.setGranularity("granularity");
		listLong.add(1L);
		listLong.add(2L);
		
		CaseUnderwritingDto caseUnderwritingDto = new CaseUnderwritingDto(1L, "name", 2L, 1);
		caseUnderwritingDtoList.add(caseUnderwritingDto);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void isCaseAccessibleOrOwnerTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(list);
		boolean status = caseDao.isCaseAccessibleOrOwner(1L, 1L);
		assertEquals(true, status);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void IsCaseAccessibleOrOwner_whenExceptionThrowTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.isCaseAccessibleOrOwner(1L, null);

	}

	@Test
	public void isAnalystRejectedCaseBeforeTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		boolean status = caseDao.isAnalystRejectedCaseBefore(1L, 1L);
		assertEquals(true, status);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void IsAnalystRejectedCaseBefore_whenExceptionThrowTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.isAnalystRejectedCaseBefore(1L, 1L);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void IsAnalystRejectedCaseBefore_whenNoResultTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		boolean status = caseDao.isAnalystRejectedCaseBefore(1L, 1L);
		assertEquals(false, status);

	}

	@Test
	public void assignCaseToAnalystTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.save(Mockito.any(CaseAnalystMapping.class))).thenReturn(1L);
		long id = caseDao.assignCaseToAnalyst(user, caseSeed);
		assertEquals(1L, id);

	}

	@Test
	public void fullTextSearchForCaseSeedTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> list = caseDao.fullTextSearchForCaseSeed("", 1, 10, new Date(), new Date(), 1, "", 4, 1,
				"createdOn", "asc");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fullTextSearchForCaseSeed_WhenExceptionThrowTest() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.fullTextSearchForCaseSeed("", 1, 10, new Date(), new Date(), 1, "", 4, 1, "createdOn", "asc");
	}

	@Test
	public void countFullTextSearchForCaseSeedTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		long result = caseDao.countFullTextSearchForCaseSeed("", 1L, new Date(), new Date(), "type", 2, 3);
		assertEquals(1L, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countFullTextSearchForCaseSeed_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.countFullTextSearchForCaseSeed("", 1L, new Date(), new Date(), "type", 2, 3);
	}

	@Test
	public void getAllCasesByUserTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.getAllCasesByUser(1L, 2, 5, "type", 2, 3, "name", "desc");
		assertEquals(1L, resultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAllCasesByUser_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		caseDao.getAllCasesByUser(1L, 2, 5, "type", 2, 3, "name", "desc");
	}

	@Test
	public void getAllIncomingtrayBasedOnUserIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.getAllIncomingtrayBasedOnUserId(1L, 2, 5, "name", "desc");
		assertEquals(1L, resultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAllIncomingtrayBasedOnUserId_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getAllIncomingtrayBasedOnUserId(1L, 2, 5, "name", "desc");
	}

	@Test
	public void countgetAllIncomingTrayTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		long result = caseDao.countgetAllIncomingTray(1L);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countgetAllIncomingTray_whenexceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.countgetAllIncomingTray(1L);
	}

	@Test
	public void fullTextSearchForIncomingtrayTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.fullTextSearchForIncomingtray(1L, "caseSearchKeyword", 2, 5, new Date(),
				new Date(), "name", "desc");
		assertEquals(1L, resultList.size());
	}

	@Test
	public void fullTextSearchForIncomingtray_whenCreationDateIsNullTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.fullTextSearchForIncomingtray(1L, "caseSearchKeyword", 2, 5, null,
				new Date(), "name", "desc");
		assertEquals(1L, resultList.size());
	}

	@Test
	public void fullTextSearchForIncomingtray_whenCreationDateAndModifiedDateNullTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.fullTextSearchForIncomingtray(1L, "caseSearchKeyword", 2, 5, null, null,
				"name", "desc");
		assertEquals(1L, resultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fullTextSearchForIncomingtray_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.fullTextSearchForIncomingtray(1L, "caseSearchKeyword", 2, 5, new Date(), new Date(), "name",
				"desc");
	}

	@Test
	public void getAllMyCasesBasedOnUserIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.getAllIncomingtrayBasedOnUserId(1L, 2, 5, "name", "desc");
		assertEquals(1L, resultList.size());
	}

	@Test
	public void getAllMyCasesBasedOnUserId_whennameTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.getAllIncomingtrayBasedOnUserId(1L, 2, 5, "createdOn", "desc");
		assertEquals(1L, resultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAllMyCasesBasedOnUserId_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getAllIncomingtrayBasedOnUserId(1L, 2, 5, "name", "desc");
	}

	@Test
	public void countgetAllMyCasesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		long result = caseDao.countgetAllMyCases(2L);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countgetAllMyCases_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.countgetAllMyCases(2L);
	}

	@Test
	public void fullTextSearchForMyCasesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.fullTextSearchForMyCases(1L, "caseSearchKeyword", 2, 5, new Date(),
				new Date(), "name", "desc");
		assertEquals(1L, resultList.size());
	}

	@Test
	public void fullTextSearchForMyCases_whenCreatedDateNullTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.fullTextSearchForMyCases(1L, "caseSearchKeyword", 2, 5, null, new Date(),
				"name", "desc");
		assertEquals(1L, resultList.size());
	}

	@Test
	public void fullTextSearchForMyCases_whenCreatedDateAndModifedDateNullTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> resultList = caseDao.fullTextSearchForMyCases(1L, "caseSearchKeyword", 2, 5, null, null,
				"name", "desc");
		assertEquals(1L, resultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fullTextSearchForMyCases_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.fullTextSearchForMyCases(1L, "caseSearchKeyword", 2, 5, null, new Date(), "name", "desc");
	}

	@Test
	public void countFullTextSearchForMyCasesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		long result = caseDao.countFullTextSearchForMyCases(1L, "caseSearchKeyword", new Date(), new Date());
		assertEquals(1l, result);
	}

	@Test
	public void countFullTextSearchForMyCases_whenCreationDateNullTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		long result = caseDao.countFullTextSearchForMyCases(1L, "caseSearchKeyword", null, new Date());
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countFullTextSearchForMyCases_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.countFullTextSearchForMyCases(1L, "caseSearchKeyword", null, new Date());
	}

	@Test
	public void countAllCasesByUserTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		long result = caseDao.countAllCasesByUser(1L, "type", 2, 2);
		assertEquals(1L, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countAllCasesByUser_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		caseDao.countAllCasesByUser(1L, "type", 2, 2);
	}

	@Test
	public void getCaseListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> caseSeedList = caseDao.getCaseList(caseVo, 1L);
		assertEquals(1, caseSeedList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCaseList_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getCaseList(caseVo, 1L);
	}

	@Test
	public void getCaseListCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = caseDao.getCaseListCount(caseVo, 1L);
		assertEquals(2L, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCaseListCount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getCaseListCount(caseVo, 1L);
	}

	@Test
	public void caseAggregatorTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseAggregatorList);
		List<CaseAggregator> newResult = caseDao.caseAggregator(1L, caseAggregatorDto);
		assertEquals(1, newResult.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void caseAggregator_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.caseAggregator(1L, caseAggregatorDto);
	}

	@Test
	public void getCasesByUserTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listLong);
		List<Long> result = caseDao.getCasesByUser(2L);
		assertEquals(2, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCasesByUser_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getCasesByUser(2L);
	}

	@Test
	public void getMyCaseIdsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listLong);
		List<Long> result = caseDao.getMyCaseIds(1L, caseVo, 4);
		assertEquals(2, result.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getMyCaseIds_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getMyCaseIds(1L, caseVo, 4);
	}
	
	@Test 
	public void getCaseListForUnderwritingTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseUnderwritingDtoList);
		List<CaseUnderwritingDto> result = caseDao.getCaseListForUnderwriting(15, 1, listLong);
		assertEquals(1, result.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCaseListForUnderwriting_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.getCaseListForUnderwriting(15, 1, listLong);
	}
	
	@Test
	public void caseListCountForUnderwritingTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = caseDao.caseListCountForUnderwriting(listLong);
		assertEquals(2L, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void caseListCountForUnderwriting_whenExceptioThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.caseListCountForUnderwriting(listLong);
	}
	
	@Test
	public void searchCaseListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseUnderwritingDtoList);
		List<CaseUnderwritingDto> resultList = caseDao.searchCaseList("name");
		assertEquals(1, resultList.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void searchCaseList_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.searchCaseList("name");
	}
	
	@Test
	public void searchCaseListCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = caseDao.searchCaseListCount("name");
		assertEquals(2l, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void searchCaseListCount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseDao.searchCaseListCount("name");
	}
	
	@Test
	public void getCaseByDocIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(caseSeed);
		Case newCase = caseDao.getCaseByDocId(2L);
		assertEquals(caseSeed.getCaseId(), newCase.getCaseId());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getCaseByDocId_whenReturnNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		Case newCase = caseDao.getCaseByDocId(2L);
		assertEquals(null, newCase);
	}
}
