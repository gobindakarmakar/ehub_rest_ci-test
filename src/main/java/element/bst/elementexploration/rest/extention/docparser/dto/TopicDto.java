package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class TopicDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "Id")
	private long id;
	@ApiModelProperty(value = "Name of the topic")
	private String name;
	@ApiModelProperty(value = "Topic Iso code")
	private String isoCode;
	public TopicDto() {
		super();
		
	}
	public TopicDto(long id, String name, String isoCode) {
		super();
		this.id = id;
		this.name = name;
		this.isoCode = isoCode;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIsoCode() {
		return isoCode;
	}
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	
	

}
