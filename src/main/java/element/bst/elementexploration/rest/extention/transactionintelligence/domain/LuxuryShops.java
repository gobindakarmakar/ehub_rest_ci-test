package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class consists luxury shop details
 * 
 * @author rambabu
 */
@Entity
@Table(name = "LUXURY_SHOPS")
public class LuxuryShops implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "SHOP_NAME", length = 500)
	private String shopName;
	
	@Column(name = "WEBPAGE", length = 500)
	private String webPage;
	
	@Column(name = "BRAND", length = 500)
	private String brand;

	public LuxuryShops() {
		super();
		
	}

	public LuxuryShops(Long id, String shopName, String webPage, String brand) {
		super();
		this.id = id;
		this.shopName = shopName;
		this.webPage = webPage;
		this.brand = brand;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getWebPage() {
		return webPage;
	}

	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	

}
