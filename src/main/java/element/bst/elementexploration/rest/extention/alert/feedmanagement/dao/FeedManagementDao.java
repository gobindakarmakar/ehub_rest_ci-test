package element.bst.elementexploration.rest.extention.alert.feedmanagement.dao;

import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author hanuman
 *
 */
public interface FeedManagementDao  extends GenericDao<FeedManagement, Long>{

	
	FeedManagement getFeedManagementByFeedName(String feedName);
	
	FeedManagement createNewFeedManagement(FeedManagementDto feedManagmentDto);

	List<FeedGroups> getAllFeedGroups();

	List<FeedGroups> getgetGroupLevelByFeedId(Long feedManagementID);

	List<FeedManagement> getFeedsList(Boolean isAllRequired, Integer pageNumber, Integer recordsPerPage, String orderIn, String orderBy);

	Long getFeedsCount();

	Map<Long, Integer> getCountForFeeds();
	
	FeedManagement getFeedByTypeAndSource(Long typeId,Long sourceId);
	
	List<FeedGroups> getFeedGroupsByGroupId(List<Long> groupIds);

	List<FeedManagement> getFeedsByTypeIds(List<Long> classificationIds);

	List<FeedManagement> getFeedsByFeedIds(List<Long> feedIds);

}
