package element.bst.elementexploration.rest.extention.significantnews.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "bst_significant_article")
public class SignificantArticles implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "is_significant_news")
	private boolean isSignificantNews = false;

	@Column(name = "sentiment")
	private String sentiment;

	@Column(name = "articleUrl")
	private String articleUrl;

	@Column(name = "entityId")
	private String entityId;

	@Column(name = "classification")
	private String classification;
	
	@Transient
	private String comment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isSignificantNews() {
		return isSignificantNews;
	}

	public void setSignificantNews(boolean isSignificantNews) {
		this.isSignificantNews = isSignificantNews;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getArticleUrl() {
		return articleUrl;
	}

	public void setArticleUrl(String articleUrl) {
		this.articleUrl = articleUrl;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "SignificantArticles [id=" + id + ", uuid=" + uuid + ", isSignificantNews=" + isSignificantNews
				+ ", sentiment=" + sentiment + ", articleUrl=" + articleUrl + ", entityId=" + entityId
				+ ", classification=" + classification + ", comment=" + comment + "]";
	}

}
