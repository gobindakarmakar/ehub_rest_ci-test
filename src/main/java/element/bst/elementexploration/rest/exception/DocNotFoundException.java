package element.bst.elementexploration.rest.exception;

public class DocNotFoundException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  String message;
	
	public static final String DOC_NOT_FOUND_MSG="Document not found.";
	
	public DocNotFoundException(String message) {
		super();
		this.message = message;
	}



	public DocNotFoundException() {

		super();
		message = DOC_NOT_FOUND_MSG;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DocNotFoundException [message=" + message + "]";
	}
	
}
