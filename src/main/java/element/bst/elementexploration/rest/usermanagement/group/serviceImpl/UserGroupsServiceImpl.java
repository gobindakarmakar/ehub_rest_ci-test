package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserGroupsDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author Paul Jalagari
 *
 */
@Service("userGroupsService")
public class UserGroupsServiceImpl extends GenericServiceImpl<UserGroups, Long> implements UserGroupsService {

	@Autowired
	private UserGroupsDao userGroupsDao;

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	public UserGroupsServiceImpl(GenericDao<UserGroups, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public UserGroups getUserGroup(Long userId, Long groupId) {
		return userGroupsDao.getUserGroup(userId, groupId);
	}

	@Override
	@Transactional("transactionManager")
	public Map<Long, Long> groupsWithUserCount() {

		return userGroupsDao.groupsWithUserCount();

	}

	@Override
	@Transactional("transactionManager")
	public boolean saveUserGroups(Users user, List<Long> groupIds, Long currentUserId, String action) throws Exception {
		if (user != null) {
			if (groupIds != null && groupIds.size() > 0) {
				for (Long eachGroupId : groupIds) {
					Groups existingGroup = groupsService.find(eachGroupId);
					if (existingGroup != null) {
						if (getUserGroup(user.getUserId(), existingGroup.getId()) == null) {
							UserGroups userGroup = new UserGroups();
							userGroup.setGroupId(existingGroup);
							userGroup.setUserId(user);
							saveOrUpdate(userGroup);
							if (!action.equalsIgnoreCase(ElementConstants.USER_REGISTRATION_TYPE)) {
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
										ElementConstants.STRING_TYPE_USER_GROUP, null,
										ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
								Users admin = null;
								if (currentUserId != null){
									admin = usersService.find(currentUserId);
									admin = usersService.prepareUserFromFirebase(admin);
								}
									
								log.setDescription(admin.getFirstName() + " " + admin.getLastName()
										+ ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
								log.setUserId(currentUserId);
								auditLogService.save(log);
							}
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean removeUserGroups(Users user, List<Long> groupIdsToBeRemoved, Long currentUserId, String action)
			throws Exception {
		if (user != null) {
			if (groupIdsToBeRemoved != null && groupIdsToBeRemoved.size() > 0) {
				for (Long eachGroupId : groupIdsToBeRemoved) {
					Groups existingGroup = groupsService.find(eachGroupId);
					if (existingGroup != null) {
						UserGroups exisitingUserGroup = getUserGroup(user.getUserId(), existingGroup.getId());
						if (exisitingUserGroup != null) {
							delete(exisitingUserGroup);
							if (!action.equalsIgnoreCase(ElementConstants.USER_REGISTRATION_TYPE)) {
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
										ElementConstants.STRING_TYPE_USER_GROUP, null,
										ElementConstants.USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG);
								Users admin = null;
								if (currentUserId != null){
									admin = usersService.find(currentUserId);
									admin = usersService.prepareUserFromFirebase(admin);
								}
									
								log.setDescription(admin.getFirstName() + " " + admin.getLastName()
										+ ElementConstants.USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG);
								log.setUserId(currentUserId);
								auditLogService.save(log);
							}
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean addUserToGroup(Long groupId, Long userId, Long adminId) throws Exception {
		Users user = usersService.find(userId);
		if (user != null) {
			Groups group = groupsService.find(groupId);
			if (group != null) {
				if (getUserGroup(userId, groupId) == null) {
					UserGroups userGroup = new UserGroups();
					userGroup.setGroupId(group);
					userGroup.setUserId(user);
					save(userGroup);
					AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.STRING_TYPE_USER_GROUP,
							null, ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
					Users admin = null;
					admin = usersService.find(adminId);
					log.setDescription(admin.getFirstName() + " " + admin.getLastName()
							+ ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
					log.setUserId(userId);
					auditLogService.save(log);
					return true;
				} else {
					return true;
				}
			}
		}
		return false;
	}

}
