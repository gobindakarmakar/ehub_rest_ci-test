package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Case Vo")
public class CaseVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Type of the case")
	private String type;

	@ApiModelProperty(value = "Priority of the case")
	private PriorityEnums priority;

	@ApiModelProperty(value = "Status list of the case")
	private List<Integer> status;

	@ApiModelProperty(value = "Keyword for search")
	private String keyword;

	@ApiModelProperty(value = "Creation date of case",example="dd-MM-yyyy")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date creationDate;

	@ApiModelProperty(value = "Modified date of case",example="dd-MM-yyyy")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date modifiedDate;

	@ApiModelProperty(value = "Page number for pagination ")
	private Integer pageNumber;

	@ApiModelProperty(value = "Records per page for pagination ")
	private Integer recordsPerPage;

	@ApiModelProperty(value = "Order by")
	private String orderBy;

	@ApiModelProperty(value = "OrderIn")
	private String orderIn;
	
	@ApiModelProperty(value = "Add Risk Scores")
	private Boolean addRiskScore;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public PriorityEnums getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnums priority) {
		this.priority = priority;
	}

	public List<Integer> getStatus() {
		return status;
	}

	public void setStatus(List<Integer> status) {
		this.status = status;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getRecordsPerPage() {
		return recordsPerPage;
	}

	public void setRecordsPerPage(Integer recordsPerPage) {
		this.recordsPerPage = recordsPerPage;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderIn() {
		return orderIn;
	}

	public void setOrderIn(String orderIn) {
		this.orderIn = orderIn;
	}

	public Boolean getAddRiskScore() {
		return addRiskScore;
	}

	public void setAddRiskScore(Boolean addRiskScore) {
		this.addRiskScore = addRiskScore;
	}

	@Override
	public String toString() {
		return "CaseVo [type=" + type + ", priority=" + priority + ", status=" + status + ", keyword=" + keyword
				+ ", creationDate=" + creationDate + ", modifiedDate=" + modifiedDate + ", pageNumber=" + pageNumber
				+ ", recordsPerPage=" + recordsPerPage + ", orderBy=" + orderBy + ", orderIn=" + orderIn
				+ ", addRiskScore=" + addRiskScore + "]";
	}

	

}
