package element.bst.elementexploration.rest.extention.geoencoder.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.util.URIUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.geoencoder.dao.CountryMasterDataDao;
import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;
import element.bst.elementexploration.rest.extention.geoencoder.dto.GoogleResponse;
import element.bst.elementexploration.rest.extention.geoencoder.service.CountryMasterDataService;
import element.bst.elementexploration.rest.extention.geoencoder.service.GeoEncoderService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Service("geoEncoderService")
public class GeoEncoderServiceImpl implements GeoEncoderService {

	@Value("${googleEncoderUrl}")
	private String GOOGLE_ENCODER;

	@Autowired
	CountryMasterDataService countryMasterDataService;

	@Autowired
	CountryMasterDataDao countryMasterDataDao;

	@Override
	@Transactional("transactionManager")
	public List<GoogleResponse> getLatitudeAndLongitude(String address) throws Exception {
		String[] addressNormal = address.split(";");
		List<GoogleResponse> googleResponsesList = new ArrayList<GoogleResponse>();
		// List<GoogleResponse> googleResponsesListLimit = new
		// ArrayList<GoogleResponse>();
		for (int i = 0; i < addressNormal.length; i++) {
			try {
				CountryMasterData countryMasterData = countryMasterDataDao.findCountry(addressNormal[i]);

				if (countryMasterData == null) {

					URL url = new URL(GOOGLE_ENCODER + URIUtil.encodeQuery(addressNormal[i]) + "&sensor=true");
					HttpURLConnection.setFollowRedirects(false);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					// Thread.sleep(10000);
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(10000 * (i + 1));
					conn.setRequestProperty("Accept", "application/json");
					// conn.setConnectTimeout(10000);

					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
					}

					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

					String output = "", full = "";
					while ((output = br.readLine()) != null) {
						// System.out.println(output);
						full += output;
					}

					if (full.contains(
							"You have exceeded your daily request quota for this API. We recommend registering for a key at the Google Developers Console")
							|| full.contains("You have exceeded your rate-limit for this API")) {
						conn.setConnectTimeout(1000 * (i + 1));
						i--;
						/*
						 * GoogleResponse googleResponseOverLimit = new GoogleResponse();
						 * googleResponseOverLimit.setLatitude(ElementConstants.
						 * GET_LATITUDE_LONGITUDE_NOT_FOUND_MSG.toString());
						 * googleResponsesListLimit.add(googleResponseOverLimit); return
						 * googleResponsesListLimit;
						 */
						// throw new Exception(ElementConstants.GET_LATITUDE_LONGITUDE_NOT_FOUND_MSG);
					} else {
						System.out.println("Hit to GOOGLE");
						JSONObject jsonResponseObject = new JSONObject(full);
						JSONArray array = jsonResponseObject.getJSONArray("results");
						JSONObject normalObject = array.getJSONObject(0);
						JSONObject jsonObject1 = normalObject.getJSONObject("geometry");
						JSONObject jsonObject2 = jsonObject1.getJSONObject("location");
						// System.out.println(jsonObject2.get("lat"));
						// System.out.println(jsonObject2.get("lng"));
						GoogleResponse googleResponse = new GoogleResponse();
						googleResponse.setLatitude(jsonObject2.get("lat").toString());
						googleResponse.setLongitude(jsonObject2.get("lat").toString());
						googleResponse.setCountry(addressNormal[i]);
						googleResponsesList.add(googleResponse);
						conn.disconnect();
					}
				} // if close for countrymasterData null
				else {
					System.out.println("Hit to DB");
					GoogleResponse googleResponse = new GoogleResponse();
					googleResponse.setLatitude(countryMasterData.getLatitude());
					googleResponse.setLongitude(countryMasterData.getLongitude());
					googleResponse.setCountry(countryMasterData.getName());
					googleResponsesList.add(googleResponse);
				}
			} // try close
			catch (MalformedURLException e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			} catch (IOException e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				return googleResponsesList;
			}
		} // for close
		return googleResponsesList;
	}

	/*@Override
	@Transactional("transactionManager")
	public boolean saveCountiesMasterData(MultipartFile uploadedFileData) throws IOException, ParseException {
		String originalFile = new String(uploadedFileData.getOriginalFilename());
		String ext = FilenameUtils.getExtension(originalFile);
		String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
		File file = null;
		file = File.createTempFile(fileNameWithoutExtension, ext);
		uploadedFileData.transferTo(file);
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(file));
		JSONArray jsonObject = new JSONArray(object.toString());
		// JSONArray array = jsonObject.get
		for (int i = 1; i < jsonObject.length(); i++) {
			JSONObject normalObject = jsonObject.getJSONObject(i);
			CountryMasterData countryMasterData = new CountryMasterData();
			countryMasterData.setName(normalObject.get("name").toString());
			countryMasterData.setLongitude(normalObject.get("longitude").toString());
			countryMasterData.setLatitude(normalObject.get("latitude").toString());
			countryMasterData.setIsoCode(normalObject.get("iso2Code").toString());
			countryMasterDataService.save(countryMasterData);
		}

		return true;
	}*/
}
