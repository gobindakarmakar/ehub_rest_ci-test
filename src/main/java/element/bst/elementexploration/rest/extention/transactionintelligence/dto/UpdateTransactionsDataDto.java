package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class UpdateTransactionsDataDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	private Long id;
	private String originatorAccountId;
	private String beneficiaryAccountId;
	private String transProductType;
	private String transactionChannel;
	private Double amount;
	private String currency;
	private Date businessDate;
	private String customText;
	private Long originatorCutomerId;
	private Long beneficiaryCutomerId;
	private String beneficiaryBankName;
	private String beneficiaryName;
	private String originatorName;
	private String countryOfBeneficiary;
	private String countryOfTransaction;
	private String atmAddress;
	private String merchant;
	private String merchantWebsite;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginatorAccountId() {
		return originatorAccountId;
	}

	public void setOriginatorAccountId(String originatorAccountId) {
		this.originatorAccountId = originatorAccountId;
	}

	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	public String getTransProductType() {
		return transProductType;
	}

	public void setTransProductType(String transProductType) {

		this.transProductType = transProductType;
	}

	public String getTransactionChannel() {
		return transactionChannel;
	}

	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public String getCustomText() {
		return customText;
	}

	public void setCustomText(String customText) {
		this.customText = customText;
	}

	public Long getOriginatorCutomerId() {
		return originatorCutomerId;
	}

	public void setOriginatorCutomerId(Long originatorCutomerId) {
		this.originatorCutomerId = originatorCutomerId;
	}

	public Long getBeneficiaryCutomerId() {
		return beneficiaryCutomerId;
	}

	public void setBeneficiaryCutomerId(Long beneficiaryCutomerId) {
		this.beneficiaryCutomerId = beneficiaryCutomerId;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getOriginatorName() {
		return originatorName;
	}

	public void setOriginatorName(String originatorName) {
		this.originatorName = originatorName;
	}

	public String getCountryOfBeneficiary() {
		return countryOfBeneficiary;
	}

	public void setCountryOfBeneficiary(String countryOfBeneficiary) {
		this.countryOfBeneficiary = countryOfBeneficiary;
	}

	public String getCountryOfTransaction() {
		return countryOfTransaction;
	}

	public void setCountryOfTransaction(String countryOfTransaction) {
		this.countryOfTransaction = countryOfTransaction;
	}

	public String getAtmAddress() {
		return atmAddress;
	}

	public void setAtmAddress(String atmAddress) {
		this.atmAddress = atmAddress;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

}
