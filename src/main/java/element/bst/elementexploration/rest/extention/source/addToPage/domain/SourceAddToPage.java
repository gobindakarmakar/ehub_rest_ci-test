package element.bst.elementexploration.rest.extention.source.addToPage.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author hanuman
 *
 */
@ApiModel("Source Add To Page")
@Entity
@Table(name = "source_add_to_page")
public class SourceAddToPage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "page_id")
	private Long pageId;

	@ApiModelProperty(value = "ID if the Entity")
	@Column(name = "entity_id")
	private String entityId;

	@ApiModelProperty(value = "ID if the Doc")
	@Column(name = "doc_id")
	private Long docId;

	@ApiModelProperty(value = "name if the Source")
	@Column(name = "source_name")
	private String sourceName;

	@ApiModelProperty(value = "file name if the File has")
	@Column(name = "file_name")
	private String fileName;

	@ApiModelProperty(value = "flag if the source has")
	@Column(name = "is_addToPage")
	private Boolean isAddToPage;

	@UpdateTimestamp
	@Column(name = "updated_time")
	private Date updatedTime;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean getIsAddToPage() {
		return isAddToPage;
	}

	public void setIsAddToPage(Boolean isAddToPage) {
		this.isAddToPage = isAddToPage;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
}