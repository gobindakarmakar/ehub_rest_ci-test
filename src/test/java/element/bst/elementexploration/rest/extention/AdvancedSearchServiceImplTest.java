package element.bst.elementexploration.rest.extention;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.util.ResourceUtils;

public class AdvancedSearchServiceImplTest {

	@Before
    public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void readCorporateStructure() throws IOException, ParseException{
		JSONObject response = new JSONObject();
		JSONParser parser = new JSONParser();
		File file = ResourceUtils.getFile("classpath:corporatejson.json");
		FileReader reader = new FileReader(file);
		Object obj = parser.parse(reader);
		org.json.simple.JSONArray mainResponse = (org.json.simple.JSONArray) obj;
		
		if (mainResponse.size() > 0) {
			org.json.simple.JSONObject lastObject = (org.json.simple.JSONObject) mainResponse
					.get(mainResponse.size() - 1);
			String status = (String) lastObject.get("status");
			if (status == null) {
				response.put("status", "inprogress");
			}
			response.put("status", status);
			response.put("data", mainResponse);
			response.toString();
			Assert.assertNotNull(response);


		}
	}
}
