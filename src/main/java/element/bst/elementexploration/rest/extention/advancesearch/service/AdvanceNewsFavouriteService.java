package element.bst.elementexploration.rest.extention.advancesearch.service;

import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface AdvanceNewsFavouriteService extends GenericService<AdvanceNewsFavourite, Long> {
	
	public AdvanceNewsFavourite saveAdvanceNewsFavourite(AdvanceNewsFavourite advanceNewsFavourite, Long userId);
	
	public int deleteAdvanceNewsFavourite(String entityId);

}
