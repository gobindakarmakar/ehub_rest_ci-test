package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author rambabu
 *
 */
@ApiModel("Document table columns")
public class DataTableColumsDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "Document contents")
	private ContentDataDto contentDataDto;

	public DataTableColumsDto() {
		super();
		
	}

	public ContentDataDto getContentDataDto() {
		return contentDataDto;
	}

	public void setContentDataDto(ContentDataDto contentDataDto) {
		this.contentDataDto = contentDataDto;
	}

	@Override
	public String toString() {
		return "DataTableColumsDto [contentDataDto=" + contentDataDto + "]";
	}

	

}
