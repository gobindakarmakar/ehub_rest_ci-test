package element.bst.elementexploration.test.document.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDocumentDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.document.dao.DocCommentDao;
import element.bst.elementexploration.rest.document.dao.DocumentCaseMppingDao;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.dao.DocumentMappingDao;
import element.bst.elementexploration.rest.document.domain.DocComment;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.domain.DocumentVaultCaseMapping;
import element.bst.elementexploration.rest.document.domain.DocumentVaultUserMapping;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.document.dto.DocCommentVo;
import element.bst.elementexploration.rest.document.dto.DocumentDto;
import element.bst.elementexploration.rest.document.dto.DocumentUserAndCaseDto;
import element.bst.elementexploration.rest.document.dto.DocumentUserMappingDTO;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.document.serviceImpl.DocumentServiceImpl;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentServiceTest {
	
	@Mock
	DocumentDao documentDaoMock;
	
	@Mock
	DocumentMappingDao documentMappingDao;
	
	@Mock
	CaseDao caseDaoMock;
	
	@Mock
	UsersDao usersDaoMock;
	
	@Mock
	ListItem listItem;
	
	@Mock
	DocumentCaseMppingDao documentCaseMppingDaoMock;
	
	@Mock
	DocCommentDao docCommentDaoMock;
	
	@Mock
	CaseDocumentDao caseDocumentDaoMock;
	
	@Mock
	UsersService usersService;
	
	@InjectMocks
	DocumentServiceImpl documentService;
	
	public List<DocumentVaultDTO> documentVaultList;
	
	public DocumentUserAndCaseDto documentUserAndCaseDto;
	
	public byte[] downloadDoc;
	
	public List<DocCommentDTO> documentComments;
	
	public DocumentVaultDTO documentVaultDTO;
	
	public Users user;
	
	public DocumentVault documentVault;
	
	public DocumentDto documentDto;
	
	public DocComment docComment;
	
	public List<CaseDto> caseDtoList = new ArrayList<CaseDto>();
	
	@Before
    public void setupMock() {
		DocumentVaultDTO documentVaultDTO1 = new DocumentVaultDTO(1L, "test", "", 1530L, "pdf", new Date(),
				2L,1L, new Date(), "test document", "analyst");
		DocumentVaultDTO documentVaultDTO2 = new DocumentVaultDTO(2L, "Document test", "", 1530L, "pdf", new Date(),
				2L,1L, new Date(), "test document", "analyst");
		documentVaultList = new ArrayList<DocumentVaultDTO>();
		documentVaultList.add(documentVaultDTO1);
		documentVaultList.add(documentVaultDTO2);
		documentUserAndCaseDto = new DocumentUserAndCaseDto();
		downloadDoc = new byte[10];
		documentComments = new ArrayList<DocCommentDTO>();
		DocCommentDTO docComment1= new DocCommentDTO(1l, "test doc comment", "testing", new Date(), new Date(), "unit testing");
		DocCommentDTO docComment2= new DocCommentDTO(2l, "test doc comment", "testing", new Date(), new Date(), "unit testing");
		documentComments.add(docComment1);
		documentComments.add(docComment2);
		documentVaultDTO = new DocumentVaultDTO();
		user=new Users("analyst","Analyst","Analyst",new Date(),listItem);
		user.setUserId(1L);
		documentVault = new DocumentVault(1L, "Test document", "testing", 15L, "pdf", new Date(),user , "abc123", false, 1L, new Date(),
			"testdoc", 2, "completed", 2);
		documentDto = new DocumentDto();
		documentDto.setDocId(1L);
		documentDto.setTitle("test");
		documentDto.setRemark("test remark");
		docComment = new DocComment();
		docComment.setCommentId(1L);
		
		CaseDto caseDto = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDto);
		CaseDto newCaseDto = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(newCaseDto);
		MockitoAnnotations.initMocks(this);
	}
	
	
	
	@Test(expected = HibernateException.class)
	public void updateDocumentById_whenExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentService.isPermissionWrite(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.doThrow(HibernateException.class).when(documentDaoMock).saveOrUpdate(Mockito.any(DocumentVault.class));
		documentService.updateDocumentById(documentDto, 1L, 1L);
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void updateDocumentById_whenPermissionDeniedExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentService.isPermissionWrite(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
		documentService.updateDocumentById(documentDto, 1L, 2L);
	}
	
	@Test(expected = DocNotFoundException.class)
	public void updateDocumentById_whenDOcNotFoundExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		documentService.updateDocumentById(documentDto, 1L, 1L);
	}
	
	@Test
	public void isPermissionWriteTest(){
		Mockito.when(documentMappingDao.isPermissionWrite(Mockito.anyLong(),  Mockito.anyLong())).thenReturn(true);
		boolean status = documentService.isPermissionWrite(Mockito.anyLong(),  Mockito.anyLong());
		assertEquals(true,status);
	}
	
	@Test
	public void isReadOrWritePermissionTest() {
		Mockito.when(documentMappingDao.isReadOrWritePermission(Mockito.anyLong(),  Mockito.anyLong())).thenReturn(true);
		boolean status = documentService.isReadOrWritePermission(Mockito.anyLong(),  Mockito.anyLong());
		assertEquals(true,status);
	}
	
	@Test(expected = HibernateException.class)
	public void softDeleteDocumentByIdTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.doThrow(HibernateException.class).when(documentDaoMock).saveOrUpdate(Mockito.any(DocumentVault.class));
		documentService.softDeleteDocumentById(1L,1L);
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void softDeleteDocumentById_whenPermissionDeniedExceptionTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);	
		Mockito.when(documentService.isPermissionWrite(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
		documentService.softDeleteDocumentById(1L,2L);
	}
	
	@Test(expected = DocNotFoundException.class)
	public void softDeleteDocumentById_whenDocNotFoundExceptionTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(null);	
		documentService.softDeleteDocumentById(1L,1L);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = HibernateException.class)
	public void shareDocumentWithCaseseed_whenExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(new Case());
		Mockito.when(usersDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(usersService.prepareUserFromFirebase(Mockito.any(Users.class))).thenReturn(user);
		Mockito.when(documentCaseMppingDaoMock.create(Mockito.any(DocumentVaultCaseMapping.class))).thenThrow(HibernateException.class);
		documentService.shareDocumentWithCaseseed(1L, 1L, 1L);
	}
	
	@Test(expected = CaseSeedNotFoundException.class)
	public void shareDocumentWithCaseseed_whenCaseSeedNotFoundExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(null);		
		documentService.shareDocumentWithCaseseed(1L, 1L, 1L);
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void shareDocumentWithCaseseed_whenPermissionDeniedExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
		documentService.shareDocumentWithCaseseed(1L, 1L, 1L);
	}
	
	@Test(expected = Exception.class)
	public void unshareDocumentFromCaseseed_whenExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(new Case());
		Mockito.when(usersDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(documentCaseMppingDaoMock.findByCaseIdAndDocId(Mockito.anyLong(),Mockito.anyLong())).thenReturn(new DocumentVaultCaseMapping());
		Mockito.doThrow(new Exception()).when(documentCaseMppingDaoMock).saveOrUpdate(Mockito.any(DocumentVaultCaseMapping.class));
		documentService.shareDocumentWithCaseseed(1L, 1L, 1L);
	}
	
	@Test(expected = CaseSeedNotFoundException.class)
	public void unshareDocumentFromCaseseed_whenCaseSeedNotFoundExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(null);		
		documentService.shareDocumentWithCaseseed(1L, 1L, 1L);
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void unshareDocumentFromCaseseed_whenPermissionDeniedExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
		documentService.shareDocumentWithCaseseed(1L, 1L, 1L);
	}
	
	@Test(expected = Exception.class)
	public void shareDocWithUser_whenExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);		
		Mockito.doThrow(new Exception()).when(documentCaseMppingDaoMock).saveOrUpdate(Mockito.any(DocumentVaultCaseMapping.class));
		documentService.shareDocWithUser(1L, 1L, 1L,1);
	}
	
	
	@Test(expected = NoDataFoundException.class)
	public void shareDocWithUser_whenNoDataFoundExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		documentService.shareDocWithUser(1L, 1L, 1L,1);
	}
	
	@Test(expected = BadRequestException.class)
	public void shareDocWithUser_whenBadRequestExceptionThrowTest(){		
		documentService.shareDocWithUser(1L, 1L, 1L,3);
	}
	
	@Test(expected = Exception.class)
	public void unShareDocFromUser_whenExceptionThrowTest(){		
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(documentMappingDao.findByUserIdAndDocId(Mockito.anyLong(),Mockito.anyLong())).thenReturn(new DocumentVaultUserMapping());
		Mockito.doThrow(new Exception()).when(documentMappingDao).saveOrUpdate(Mockito.any(DocumentVaultUserMapping.class));
		documentService.unShareDocFromUser(1L, 1L, 1L,"share with user");
	}
	
	
	@Test(expected = PermissionDeniedException.class)
	public void unShareDocFromUser_whenPermissionDeniedExceptionThrowTest(){
		Mockito.when(documentDaoMock.isDocShareable(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
		documentService.unShareDocFromUser(1L, 1L, 1L,"share with user");
	}
	
	
	
	@Test
	public void myDocumentsTest() {
		Mockito.when(documentDaoMock.myDocuments(Mockito.anyLong(),  Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyBoolean())).thenReturn(documentVaultList);
		Mockito.when(usersDaoMock.find(Mockito.anyLong())).thenReturn(user);
		List<DocumentVaultDTO> documenttList = documentService.myDocuments(1L,  1L,2,1,15,"title","desc","","",false);
		assertEquals(2,documenttList.size());
	}
	
	@Test
	public void countMyDocumentsTest() {
		Mockito.when(documentDaoMock.countMyDocuments(Mockito.anyLong(),  Mockito.anyLong(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(3l);
		long count=documentService.countMyDocuments(1L,1L,2,"","");
		assertEquals(3L,count);
	}
	
	
	@Test
	public void fullTextSearchMyDocumentsTest() {
		Mockito.when(documentDaoMock.fullTextSearchMyDocuments(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(documentVaultList);
		 List<DocumentVaultDTO> documenttList = documentService.fullTextSearchMyDocuments(1L,2L,"documenttest",2,"title","desc",1,10,"","");
		assertEquals(2,documenttList.size());
	}
	
	@Test
	public void countFullTextSearchMyDocumentsTest() {
		Mockito.when(documentDaoMock.countFullTextSearchMyDocuments(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(2l);
		 long count = documentService.countFullTextSearchMyDocuments(1L,1L,"documenttest",2,"","");
		assertEquals(2l,count);
	}
	
	@Test
	public void getAllMySharedDocumentTest() {
		Mockito.when(documentDaoMock.getAllMySharedDocument(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString(),Mockito.anyInt())).thenReturn(documentVaultList);
		 List<DocumentVaultDTO> documenttList = documentService.getAllMySharedDocument(1L,2L,1,13,"title","desc",2);
		assertEquals(2,documenttList.size());
	}
	
	@Test
	public void countGetAllMySharedDocumentTest() {
		Mockito.when(documentDaoMock.countGetAllMySharedDocument(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(2l);
		 long count = documentService.countGetAllMySharedDocument(1L,2L,2);
		assertEquals(2l,count);
	}
	
	@Test
	public void fullTextSearchDocumentTest() {
		Mockito.when(documentDaoMock.fullTextSearchDocument(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(documentVaultList);
		 List<DocumentVaultDTO> documenttList = documentService.fullTextSearchDocument(1L,"test",1,11,"title","desc",1L,2);
		assertEquals(2,documenttList.size());
	}
	
	@Test
	public void countFullTextSearchDocumentTest() {
		Mockito.when(documentDaoMock.countFullTextSearchDocument(Mockito.anyLong(),Mockito.anyString(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(2l);
		 long count = documentService.countFullTextSearchDocument(1L,"test",1L,2);
		assertEquals(2l,count);
	}
	
	@Test
	public void getCaseseedFromDocumentTest() throws IllegalAccessException, InvocationTargetException{
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentCaseMppingDaoMock.getSeedByDocumentId(Mockito.anyLong())).thenReturn(caseDtoList);
		Mockito.when(documentMappingDao.getUserList(Mockito.anyLong())).thenReturn(new ArrayList<DocumentUserMappingDTO>());
		DocumentUserAndCaseDto documentUserAndCaseDto = documentService.getCaseseedFromDocument(1L, 2L);
		Assert.assertNotNull(documentUserAndCaseDto.getDocumentVault());
	}
	
	@Test(expected = DocNotFoundException.class)
	public void getCaseseedFromDocument_whenDocNotFoundExceptionTest() throws IllegalAccessException, InvocationTargetException{
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(null);		
		 documentService.getCaseseedFromDocument(1L, 2L);
		
	}
	
	@Test
	public void listDocCommentTest() {
		Mockito.when(documentDaoMock.isDocCommentable(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		Mockito.when(docCommentDaoMock.listDocComment(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString())).thenReturn(documentComments);
		 List<DocCommentDTO> documenttList = documentService.listDocComment(1L,1L,1,11,"desc");
		assertEquals(2,documenttList.size());
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void listDocComment_whenExceptionThrowTest() {
		Mockito.when(documentDaoMock.isDocCommentable(Mockito.anyLong(),Mockito.anyLong())).thenReturn(false);
		documentService.listDocComment(1L,1L,1,11,"desc");
		
	}
	
	@Test
	public void countDocCommentTest() {
		Mockito.when(docCommentDaoMock.countDocComment(Mockito.anyLong(),Mockito.anyLong())).thenReturn(2l);
		 long count = documentService.countDocComment(Mockito.anyLong(),Mockito.anyLong());
		assertEquals(2l,count);
	}
	
	@Test
	public void isDocCommentableTest() {
		Mockito.when(documentDaoMock.isDocCommentable(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		 boolean status = documentService.isDocCommentable(Mockito.anyLong(),Mockito.anyLong());
		assertEquals(true,status);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = HibernateException.class)
	public void documentDissemination_whenExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(new Case());
		Mockito.when(documentService.isPermissionWrite(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDocumentDaoMock.create(Mockito.any(CaseDocDetails.class))).thenThrow(HibernateException.class);
		documentService.documentDissemination(1L,1L,1L);
		
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void documentDissemination_whenPermissionDeniedExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(new Case());
		Mockito.when(documentService.isPermissionWrite(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);		
		documentService.documentDissemination(1L,2L,1L);
	}
	
	@Test(expected = DocNotFoundException.class)
	public void documentDissemination_whenDocNotFoundExceptionThrowTest(){
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(null);
		documentService.documentDissemination(1L,2L,1L);
	}
	
	@Test
	public void getDocumentDetailsTest() throws IllegalAccessException, InvocationTargetException{
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentService.isReadOrWritePermission(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(usersDaoMock.find(Mockito.anyLong())).thenReturn(user);
		DocumentVaultDTO newDocumentVaultDTO = documentService.getDocumentDetails(1L,1L);
		Assert.assertNotNull(newDocumentVaultDTO.getTitle());
	}
	
	@Test
	public void getDocumentDetails_whenModifiedByIsNullTest() throws IllegalAccessException, InvocationTargetException{
		documentVault.setModifiedBy(null);
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentService.isReadOrWritePermission(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
		Mockito.when(usersDaoMock.find(Mockito.anyLong())).thenReturn(user);
		DocumentVaultDTO newDocumentVaultDTO = documentService.getDocumentDetails(1L,1L);
		Assert.assertNotNull(newDocumentVaultDTO.getTitle());
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void getDocumentDetails_whenPermissionDeniedExceptionTest() throws IllegalAccessException, InvocationTargetException{
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(documentService.isReadOrWritePermission(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);		
		documentService.getDocumentDetails(1L,2L);
	}
	
	@Test(expected = DocNotFoundException.class)
	public void getDocumentDetails_whenDocNotFoundExceptionTest() throws IllegalAccessException, InvocationTargetException{
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(null);
		documentService.getDocumentDetails(1L,1L);
	}
	
	@Test
	public void commentDocumentTest(){
		Mockito.when(documentDaoMock.isDocCommentable(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		Mockito.when(usersDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(docCommentDaoMock.create(Mockito.any(DocComment.class))).thenReturn(docComment);
		DocCommentVo docCommentVo = new DocCommentVo();
		docCommentVo.setTitle("test");
		docCommentVo.setDocId(1L);
		docCommentVo.setDescription("description");
		Long commentId = documentService.commentDocument(1L,docCommentVo);
		assertEquals(new Long("1"),commentId);
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void commentDocument_whenPermissionDeniedExceptionTest(){
		Mockito.when(documentDaoMock.isDocCommentable(Mockito.anyLong(),Mockito.anyLong())).thenReturn(false);
		DocCommentVo docCommentVo = new DocCommentVo();
		docCommentVo.setTitle("test");
		docCommentVo.setDocId(1L);
		docCommentVo.setDescription("description");
		documentService.commentDocument(1L,docCommentVo);
	}
	
	
	

}
