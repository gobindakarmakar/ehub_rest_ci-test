package element.bst.elementexploration.rest.usermanagement.security.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.FailedLogin;

/**
 * 
 * @author Amit Patel
 *
 */
public interface FailedLoginDao extends GenericDao<FailedLogin, Long>{

	public FailedLogin getFailedLoginByUsername(String username);
}
