package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Document contents")
public class DocumentContentDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Content type of the document")
	private String contentType;
	
	@ApiModelProperty(value = "Specifies if its a question")
	private boolean isQuestion;
	
	@ApiModelProperty(value = "Document content")
	private ContentDataDto contentDataDto;
	
	@ApiModelProperty(value = "Document tables")
	private ContentDataTableDto contentDataTableDto;
	
	public DocumentContentDto() {
		super();		
	}
	
	
	public DocumentContentDto(String contentType, boolean isQuestion, ContentDataDto contentDataDto,
			ContentDataTableDto contentDataTableDto) {
		super();
		this.contentType = contentType;
		this.isQuestion = isQuestion;
		this.contentDataDto = contentDataDto;
		this.contentDataTableDto = contentDataTableDto;
	}


	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	@JsonProperty("isQuestion")
	public boolean isQuestion() {
		return isQuestion;
	}
	public void setQuestion(boolean isQuestion) {
		this.isQuestion = isQuestion;
	}
	@JsonProperty("contentData")
	public ContentDataDto getContentDataDto() {
		return contentDataDto;
	}
	public void setContentDataDto(ContentDataDto contentDataDto) {
		this.contentDataDto = contentDataDto;
	}

	@JsonProperty("contentDataTable")
	public ContentDataTableDto getContentDataTableDto() {
		return contentDataTableDto;
	}


	public void setContentDataTableDto(ContentDataTableDto contentDataTableDto) {
		this.contentDataTableDto = contentDataTableDto;
	}


	@Override
	public String toString() {
		return "DocumentContentDto [contentType=" + contentType + ", isQuestion=" + isQuestion + ", contentDataDto="
				+ contentDataDto + ", contentDataTableDto=" + contentDataTableDto + "]";
	}
	
	
	
	

}
