package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Customer")
public class CustomerDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("Customer name")
	private String customerName;
	
	@ApiModelProperty("Customer date of birth")
	private Date dateOfBirth;
	
	@ApiModelProperty("Date of incorporation of organigation")
	private Date dateOfIncorporation;
	
	@ApiModelProperty("Customer city")
	private String city;
	
	@ApiModelProperty("Customer country")
	private String country;
	
	@ApiModelProperty("Customer address")
	private String adderssLine;
	
	@ApiModelProperty("Customer type")
	private CustomerType customerType;
	
	@ApiModelProperty("Estimated turnover amount")
	private Double estimatedTurnoverAmount;
	
	@ApiModelProperty("Customer activity type")
	private String activityType;

	

	public CustomerDto() {
		super();
	}

	public CustomerDto(String customerName, Date dateOfBirth, Date dateOfIncorporation, String city, String country,
			String adderssLine, CustomerType customerType,Double estimatedTurnoverAmount,String activityType) {
		super();
		this.customerName = customerName;
		this.dateOfBirth = dateOfBirth;
		this.dateOfIncorporation = dateOfIncorporation;
		this.city = city;
		this.country = country;
		this.adderssLine = adderssLine;
		this.customerType = customerType;
		this.estimatedTurnoverAmount=estimatedTurnoverAmount;
		this.activityType=activityType;

	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfIncorporation() {
		return dateOfIncorporation;
	}

	public void setDateOfIncorporation(Date dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAdderssLine() {
		return adderssLine;
	}

	public void setAdderssLine(String adderssLine) {
		this.adderssLine = adderssLine;
	}

	public CustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	public Double getEstimatedTurnoverAmount() {
		return estimatedTurnoverAmount;
	}

	public void setEstimatedTurnoverAmount(Double estimatedTurnoverAmount) {
		this.estimatedTurnoverAmount = estimatedTurnoverAmount;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	@Override
	public String toString() {
		return "CustomerDto [customerName=" + customerName + ", dateOfBirth=" + dateOfBirth + ", dateOfIncorporation="
				+ dateOfIncorporation + ", city=" + city + ", country=" + country + ", adderssLine=" + adderssLine
				+ ", customerType=" + customerType + ", estimatedTurnoverAmount=" + estimatedTurnoverAmount
				+ ", activityType=" + activityType + "]";
	}

}
