package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupPermissionDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionDtoWithParentId;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionsDtoForPermissions;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupPermissionService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;

/**
 * @author Paul Jalagari
 *
 */
@Service("groupPermissionService")
@Transactional("transactionManager")
public class GroupPermissionServiceImpl extends GenericServiceImpl<GroupPermission, Long>
		implements GroupPermissionService {

	@Autowired
	GroupPermissionDao groupPermissionDao;
	
	@Autowired
	RolesService rolesService;

	@Autowired
	PrivilegesService privilegesService;
	
	@Autowired
	GroupsService groupsService;
	@Autowired
	public GroupPermissionServiceImpl(GenericDao<GroupPermission, Long> genericDao) {

		super(genericDao);
	}

	@Override
	public GroupPermission getGroupPermission(GroupPermission groupPermission) throws Exception {
      
		return groupPermissionDao.getGroupPermission(groupPermission);

	}

	@Override
	public List<GroupPermission> getGroupPermissionByRoleId(Long roleId) throws Exception {
		return groupPermissionDao.getGroupPermissionByRoleId(roleId);
	}

	@Override
	public List<GroupPermission> getGroupPermissionByRole(Long roleId, String roleName, Long currentUserId)
			throws Exception {
		//return groupPermissionDao.getGroupPermissionByRole(roleId,roleName,currentUserId);
		List<GroupPermission> groupPermissions = new ArrayList<GroupPermission>();
		Long finalRoleId = null;
		if (roleName != null && roleId == null) {
			Roles roleByName = rolesService.findRoleByName(roleName);
			if (roleByName != null)
				finalRoleId = roleByName.getRoleId();
		} else {
			finalRoleId = roleId;
		}
		if (finalRoleId != null) {
			groupPermissions = getGroupPermissionByRoleId(finalRoleId);
		}
		return groupPermissions;
	
	}

	@Override
	public List<GroupPermissionDtoWithParentId> getGroupPermissionByRoleIdNew(Long finalRoleId) throws Exception {
		return groupPermissionDao.getGroupPermissionByRoleIdNew(finalRoleId);
	}

	@Override
	public GroupPermission findGroupPermissionByRoleIdAndPermissionId(Long roleId, Long permissionId, Long currentUserId) throws Exception {
		 //groupPermissionDao.findGroupPermissionByRoleIdAndPermissionId(roleId,permissionId);
		GroupPermission finalPermission= new GroupPermission();
		List<GroupPermission> permissionsByRole=getGroupPermissionByRole(roleId,null,currentUserId);
		if(permissionsByRole!=null && permissionsByRole.size()>0) {
			permissionsByRole=permissionsByRole.stream().filter(o -> o.getPermissionId().getPermissionId().equals(permissionId)).collect(Collectors.toList());
			finalPermission=permissionsByRole.get(0);
		}
		 return finalPermission;
	}

	@Override
	public List<GroupPermissionsDtoForPermissions> getPermissionsByRolesList(List<Long> roleIds) throws Exception {
		
		return groupPermissionDao.getPermissionsByRolesList(roleIds);
	}

	@Override
	public List<GroupPermissionsDtoForPermissions> getRolesByGroup(List<Long> groupIds) throws Exception {
		return groupPermissionDao.getRolesByGroup(groupIds);
	}
	@Override
	public void deleteGroupPermissionById(Long groupPrivilegeId)throws Exception
	{
		groupPermissionDao.deleteGroupPermissionById(groupPrivilegeId);
	}
	
	@Override
	public void loadZeroPermissionsForRole(Roles adminRole) throws Exception {
		List<Permissions> allPermissions = privilegesService.findAll();
		//Groups analystGroup = groupsService.getGroup(ElementConstants.ANALYST_GROUP_NAME);
		if (allPermissions != null && allPermissions.size() > 0 && adminRole != null) {
			for (Permissions eachPermission : allPermissions) {
				List<GroupPermission> existingGroupPermissions = groupPermissionDao
						.getGroupPermissionByRoleId(adminRole.getRoleId());
				if (existingGroupPermissions == null || existingGroupPermissions.size() == 0) {
					GroupPermission permission = new GroupPermission();
					// permission.setGroupId(analystGroup);
					permission.setPermissionId(eachPermission);
					permission.setPermissionLevel(2);
					permission.setRoleId(adminRole);
					save(permission);
				}
			}
		}

	}

	@Override
	public void deleteGroupPermissionByRoleId(Long roleId) throws Exception {
		groupPermissionDao.deleteGroupPermissionByRoleId(roleId);
	}
}

