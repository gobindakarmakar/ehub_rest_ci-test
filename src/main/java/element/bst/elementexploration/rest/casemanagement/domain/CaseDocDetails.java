/**
 * 
 */
package element.bst.elementexploration.rest.casemanagement.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * @author Amit Patel
 *
 */

@Entity
@Table(name = "bst_doc_details")
@Access(AccessType.FIELD)
public class CaseDocDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7855301443479015873L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_doc_id")
	private Long docId;
	
	@Column(name = "file_path")
	private String filePath;
	
	@Column(name = "file_title")
	private String fileTitle;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "file_size")
	private Double fileSize;
	
	@Column(name = "file_type")
	private String fileType;
	
	@Column(name = "uploaded_on")
	private Date uploadedOn;
	
	@Column(name = "uploaded_by")
	private Long uploadedBy;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "modified_by")
	private Long modifiedBy;

	@Column(name ="doc_guuid_from_server")
	private String docGuuidFromServer;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_seed_id")
	private Case caseSeed;
	
	private Integer docFlag;

	/**
	 * 
	 */
	public CaseDocDetails() {
		super();
	}

	
	public CaseDocDetails(Long docId, String filePath, String fileTitle, String remarks, Double fileSize,
			String fileType, Date uploadedOn, Long uploadedBy, Date modifiedOn, Long modifiedBy,
			String docGuuidFromServer, Case caseSeed, Integer docFlag) {
		this.docId = docId;
		this.filePath = filePath;
		this.fileTitle = fileTitle;
		this.remarks = remarks;
		this.fileSize = fileSize;
		this.fileType = fileType;
		this.uploadedOn = uploadedOn;
		this.uploadedBy = uploadedBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.docGuuidFromServer = docGuuidFromServer;
		this.caseSeed = caseSeed;
		this.docFlag = docFlag;
	}



	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileTitle() {
		return fileTitle;
	}

	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getFileSize() {
		return fileSize;
	}

	public void setFileSize(Double fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public Long getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(Long uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getDocGuuidFromServer() {
		return docGuuidFromServer;
	}

	public void setDocGuuidFromServer(String docGuuidFromServer) {
		this.docGuuidFromServer = docGuuidFromServer;
	}

	public Case getCaseSeed() {
		return caseSeed;
	}

	public void setCaseSeed(Case caseSeed) {
		this.caseSeed = caseSeed;
	}

	public Integer getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(Integer docFlag) {
		this.docFlag = docFlag;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
