package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionDtoWithParentId;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionsDtoForPermissions;

/**
 * @author Paul Jalagari
 *
 */
public interface GroupPermissionDao extends GenericDao<GroupPermission, Long>{
  
	GroupPermission getGroupPermission(GroupPermission groupPermission )throws Exception;

	List<GroupPermission> getGroupPermissionByRoleId(Long roleId) throws Exception;

	List<GroupPermission> getGroupPermissionByRole(Long roleId, String roleName, Long currentUserId) throws Exception;

	List<GroupPermissionDtoWithParentId> getGroupPermissionByRoleIdNew(Long finalRoleId) throws Exception;

	GroupPermission findGroupPermissionByRoleIdAndPermissionId(Long roleId, Long permissionId) throws Exception;

	List<GroupPermissionsDtoForPermissions> getPermissionsByRolesList(List<Long> roleIds) throws Exception;

	List<GroupPermissionsDtoForPermissions> getRolesByGroup(List<Long> groupIds) throws Exception;
	
	void deleteGroupPermissionById(Long groupPrivilegeId)throws Exception;

	void deleteGroupPermissionByRoleId(Long roleId);
	
	
}
