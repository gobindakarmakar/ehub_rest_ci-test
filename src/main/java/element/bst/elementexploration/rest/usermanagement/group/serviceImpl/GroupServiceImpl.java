package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;

/**
 * 
 * @author Viswanath
 *
 */
@Service("groupService")
public class GroupServiceImpl extends GenericServiceImpl<Group, Long> implements GroupService {

	
	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	public GroupServiceImpl(GenericDao<Group, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public Group getGroup(String name) {
		Group group = groupDao.getGroup(name);
		return group;
	}
		
	@Override
	@Transactional("transactionManager")
	public List<Group> getUserGroups(Integer status, String keyword, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		return groupDao.getUserGroups(status, keyword, 
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, 
    			recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean isActiveGroup(Long groupId) {
		
		Group group = groupDao.find(groupId);
		boolean retVal = false;
		
		if(null != group) {
			if(group.getActive().intValue() == 1) {
				retVal = true;
			}
		}
		
		return retVal;
	}
	
	@Override
	@Transactional("transactionManager")
	public List<Group> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {

		pageNumber = (pageNumber == null) ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber;
		recordsPerPage = (recordsPerPage == null) ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		
		return groupDao.getGroupsOfUser(userId, pageNumber, recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserGroups(Integer status, String keyword) {
		return groupDao.countUserGroups(status, keyword);
	}

	@Override
	@Transactional("transactionManager")
	public Long countGroupsOfUser(Long userId) {
		return groupDao.countGroupsOfUser(userId);
	}

	@Override
	@Transactional("transactionManager")
	public Group checkGroupExist(String name, Long groupId) {
		return groupDao.checkGroupExist(name, groupId);
	}

	@Override
	@Transactional("transactionManager")
	public Boolean doesUserbelongtoGroup(String groupName, Long userId) {
		User bstuser = userDao.find(userId);
		if (bstuser == null)
			throw new NoDataFoundException(ElementConstants.USER_NOT_FOUND_MSG);

		return groupDao.doesUserbelongtoGroup(groupName, userId);
	}
	
}
