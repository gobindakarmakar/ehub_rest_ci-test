package element.bst.elementexploration.rest.extention.entity.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Viswanath Reddy G
 *
 */
@ApiModel("entity_attributes")
@Entity
@Table(name = "entity_attributes")
public class EntityAttributes {

	@ApiModelProperty(value = "The ID of attribute")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ApiModelProperty(value = "source schema")
	@Column(name = "source_schema")
	private String sourceSchema;

	@ApiModelProperty(value = "source")
	@Column(name = "source")
	private String source="";

	@ApiModelProperty(value = "source display name")
	@Column(name = "source_display_name")
	private String sourceDisplayName="";

	@ApiModelProperty(value = "value")
	@Column(name = "value",columnDefinition = "LONGTEXT")
	private String value;

	@ApiModelProperty(value = "entity id")
	@Column(name = "entity_id")
	private String entityId;

	@ApiModelProperty(value = "created by")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private Users user;

	@ApiModelProperty(value = "created on")
	@Column(name = "created_date")
	private Date createdDate;

	@ApiModelProperty(value = "modified on")
	@Column(name = "modified_date")
	private Date modifiedDate;

	@ApiModelProperty(value = "Pre Value")
	@Column(name = "pre_value",columnDefinition = "LONGTEXT")
	private String preValue;

	@ApiModelProperty(value = "User Value")
	@Column(name = "user_value",columnDefinition = "LONGTEXT")
	private String userValue;

	@Column(name = "source_url")
	private String sourceUrl="";

	@Column(name = "isUserData")
	private Boolean isUserData;

	@ApiModelProperty(value = "published on")
	@Column(name = "published_Date")
	private Date publishedDate;

	@Column(name = "user_source_url")
	private String userSourceUrl;

	@ApiModelProperty(value = "user published on")
	@Column(name = "user_published_Date")
	private Date userPublishedDate;

	@ApiModelProperty(value = "user source")
	@Column(name = "user_source")
	private String userSource="";

	@ApiModelProperty(value = "source type")
	@Column(name = "source_type")
	private String sourceType="";

	@ApiModelProperty(value = "Doc Id")
	@Column(name = "doc_id")
	private Long docId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getSourceSchema() {
		return sourceSchema;
	}

	public void setSourceSchema(String sourceSchema) {
		this.sourceSchema = sourceSchema;
	}

	public String getPreValue() {
		return preValue;
	}

	public void setPreValue(String preValue) {
		this.preValue = preValue;
	}

	public String getUserValue() {
		return userValue;
	}

	public void setUserValue(String userValue) {
		this.userValue = userValue;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public Boolean getIsUserData() {
		return isUserData;
	}

	public void setIsUserData(Boolean isUserData) {
		this.isUserData = isUserData;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getUserSourceUrl() {
		return userSourceUrl;
	}

	public void setUserSourceUrl(String userSourceUrl) {
		this.userSourceUrl = userSourceUrl;
	}

	public Date getUserPublishedDate() {
		return userPublishedDate;
	}

	public void setUserPublishedDate(Date userPublishedDate) {
		this.userPublishedDate = userPublishedDate;
	}

	public String getUserSource() {
		return userSource;
	}

	public void setUserSource(String userSource) {
		this.userSource = userSource;
	}

	public String getSourceDisplayName() {
		return sourceDisplayName;
	}

	public void setSourceDisplayName(String sourceDisplayName) {
		this.sourceDisplayName = sourceDisplayName;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

}
