package element.bst.elementexploration.rest.logging.event;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentAnswersService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import element.bst.elementexploration.rest.extention.docparser.service.PossibleAnswersService;
import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantWatchList;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantWatchListService;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantnewsService;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceDomainService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceIndustryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceMediaService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesHideStatusService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.extention.widgetreview.service.WidgetReviewService;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.service.NotificationService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Component("loggingEventListener")
public class LoggingEventListener implements ApplicationListener<LoggingEvent> {

	@Autowired
	private UsersService usersService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private CaseService caseService;

	@Autowired
	private DocumentAnswersService documentAnswersService;

	@Autowired
	private DocumentService documentService;

	@Autowired
	private DocumentTemplatesService documentTemplatesService;

	@Autowired
	private DocumentQuestionsService documentQuestionsService;

	@Autowired
	PossibleAnswersService possibleAnswersService;

	@Autowired
	private SignificantnewsService significantnewsService;

	@Autowired
	private SourceDomainService domainService;

	@Autowired
	private SourceIndustryService industryService;

	@Autowired
	private SourceMediaService mediaService;

	@Autowired
	private SourceJurisdictionService jurisdictionService;

	@Autowired
	private SourcesHideStatusService sourcesHideStatusService;

	@Autowired
	SourcesService sourcesService;
	
	@Autowired
	WidgetReviewService widgetReviewService;
	
	@Autowired
	private SignificantWatchListService significantWatchListService;

	@Override
	public void onApplicationEvent(LoggingEvent loggingEvent) {
		Long authorId = loggingEvent.getAuthorId();
		Users author = null;
		String authorName = null;
		if (authorId != null) {
			author = usersService.find(authorId);
			author = usersService.prepareUserFromFirebase(author);
			authorName = author.getFirstName() + " " + author.getLastName();
		}

		AuditLog log = null;
		if (LoggingEventType.USER_REGISTRATION.equals(loggingEvent.getEventType())) {
			Users newUser = (Users) loggingEvent.getSource();
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, null, null, "Registered to application",
					authorName + " registered to application", newUser.getUserId(), null, null, null, authorName);
		}
		if (LoggingEventType.USER_REGISTRATION_BY_ADMIN.equals(loggingEvent.getEventType())) {
			Users newUser = (Users) loggingEvent.getSource();
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, null, null, "Added a user",
					authorName + " added user " + newUser.getFirstName() + " " + newUser.getLastName(),
					author.getUserId(), null, null, null, newUser.getFirstName() + " " + newUser.getLastName());
		}
		if (LoggingEventType.CREATE_NOTIFICATION.equals(loggingEvent.getEventType())) {
			NotificationAlert notification = notificationService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, null, null, "Created a notification alert",
					authorName + " created notification alert " + notification.getSubject(), author.getUserId(), null,
					null, null, notification.getSubject());
		}
		if (LoggingEventType.CREATE_EVENT.equals(loggingEvent.getEventType())) {
			NotificationAlert event = notificationService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, null, null, "Created an event",
					authorName + " created event " + event.getSubject(), author.getUserId(), null, null, null,
					event.getSubject());
		}
		if (LoggingEventType.CREATE_GROUP.equals(loggingEvent.getEventType())) {
			Groups group = groupsService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, null, null, "Added a group",
					authorName + " added group " + group.getName(), author.getUserId(), null, null, null,
					group.getName());
		}
		if (LoggingEventType.CREATE_CASE.equals(loggingEvent.getEventType())) {
			Case caseSeed = caseService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "CASE", null, "Created a case",
					authorName + " created case " + caseSeed.getName(), author.getUserId(), caseSeed.getCaseId(), null,
					null, caseSeed.getName());
		}
		if (LoggingEventType.SEED_CASE.equals(loggingEvent.getEventType())) {
			Case caseSeed = caseService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "CASE", null, "Seeded a case",
					authorName + " seeded case " + caseSeed.getName(), author.getUserId(), caseSeed.getCaseId(), null,
					null, caseSeed.getName());
		}
		if (LoggingEventType.ASSIGN_CASE.equals(loggingEvent.getEventType())) {
			Case caseSeed = caseService.find((Long) loggingEvent.getSource());
			Users analyst = usersService.find(loggingEvent.getAnalystId());
			analyst = usersService.prepareUserFromFirebase(analyst);
			String description = null;
			if (!author.getUserId().equals(analyst.getUserId()))
				description = authorName + " assigned case " + caseSeed.getName() + " to " + analyst.getFirstName()
						+ " " + analyst.getLastName();
			else
				description = authorName + " self assigned case " + caseSeed.getName();
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "CASE", null, "Case assigned", description,
					author.getUserId(), caseSeed.getCaseId(), null, null, caseSeed.getName());
		}
		if (LoggingEventType.UPDATE_ANSWER.equals(loggingEvent.getEventType())) {
			DocumentAnswers documentAnswer = documentAnswersService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentAnswer.getDocumetnId());
			Case caseSeed = caseService.find(documentVault.getCaseId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "CASE", null, "Updated answer",
					authorName + " updated answer for " + documentAnswer.getDocumentQuestions().getName() + " from "
							+ loggingEvent.getOldValue() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), caseSeed.getCaseId(), documentVault.getDocId(), null, caseSeed.getName(),
					documentAnswer.getId());

		}
		if (LoggingEventType.UPDATE_EVALUATION.equals(loggingEvent.getEventType())) {
			DocumentAnswers documentAnswer = documentAnswersService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentAnswer.getDocumetnId());
			Case caseSeed = caseService.find(documentVault.getCaseId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "CASE", null, "Updated answer evaluation",
					authorName + " updated evaluation for " + documentAnswer.getDocumentQuestions().getName() + " from "
							+ loggingEvent.getOldValue() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), caseSeed.getCaseId(), documentVault.getDocId(), null, caseSeed.getName(),
					documentAnswer.getId());

		}
		if (LoggingEventType.UPDATE_RANKING.equals(loggingEvent.getEventType())) {
			DocumentAnswers documentAnswer = documentAnswersService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentAnswer.getDocumetnId());
			Case caseSeed = caseService.find(documentVault.getCaseId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "CASE", null, "Updated question rank",
					authorName + " updated rank for " + documentAnswer.getDocumentQuestions().getName() + " from "
							+ loggingEvent.getOldValue() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), caseSeed.getCaseId(), documentVault.getDocId(), null, caseSeed.getName(),
					documentAnswer.getId());

		}
		if (LoggingEventType.CREATE_TEMPLATE.equals(loggingEvent.getEventType())) {
			DocumentTemplates documentTemplates = documentTemplatesService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null, "Create a template",
					authorName + " created template " + documentTemplates.getTemplateName(), author.getUserId(), null,
					documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), null, null);
		}
		if (LoggingEventType.DELETE_TEMPLATE.equals(loggingEvent.getEventType())) {
			DocumentTemplates documentTemplates = documentTemplatesService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null, "Delete a template",
					authorName + " deleted template " + documentTemplates.getTemplateName(), author.getUserId(), null,
					documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), null, null);
		}

		if (LoggingEventType.ADD_NEW_QUESTION_TO_TEMPLATE.equals(loggingEvent.getEventType())) {
			DocumentQuestions documentQuestion = documentQuestionsService.find((Long) loggingEvent.getSource());
			DocumentTemplates documentTemplates = documentTemplatesService.find(documentQuestion.getTemplateId());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null,
					"Add new question to a template",
					authorName + " added new question " + documentQuestion.getName() + " to a template "
							+ documentTemplates.getTemplateName(),
					author.getUserId(), null, documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), documentQuestion.getId(), null);
		}

		if (LoggingEventType.ADD_STORY.equals(loggingEvent.getEventType())) {
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "STORY", null, "Create a story",
					authorName + " created story " + loggingEvent.getNewValue(), author.getUserId(), null, null, null,
					loggingEvent.getNewValue(), null, loggingEvent.getNewValue());

		}

		if (LoggingEventType.DELETE_STORY.equals(loggingEvent.getEventType())) {
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "STORY", null, "Delete a story",
					authorName + " deleted story " + loggingEvent.getNewValue(), author.getUserId(), null, null, null,
					loggingEvent.getNewValue(), null, loggingEvent.getNewValue());

		}

		if (LoggingEventType.DELETE_QUESTION_TO_TEMPLATE.equals(loggingEvent.getEventType())) {
			DocumentQuestions documentQuestion = documentQuestionsService.find((Long) loggingEvent.getSource());
			DocumentTemplates documentTemplates = documentTemplatesService.find(documentQuestion.getTemplateId());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null,
					"Delete question in the template",
					authorName + " deleted question " + documentQuestion.getName() + " in the template "
							+ documentTemplates.getTemplateName(),
					author.getUserId(), null, documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), documentQuestion.getId(), null);
		}

		if (LoggingEventType.UPDATE_TEMPLATE.equals(loggingEvent.getEventType())) {
			DocumentTemplates documentTemplates = documentTemplatesService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null, "Update a template",
					authorName + " updated the template " + documentTemplates.getTemplateName(), author.getUserId(),
					null, documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), null, null);
		}

		if (LoggingEventType.ADD_POSSIBLE_ANSWER.equals(loggingEvent.getEventType())) {
			PossibleAnswers possibleAnswers = possibleAnswersService.find((Long) loggingEvent.getSource());
			DocumentTemplates documentTemplates = documentTemplatesService
					.find(possibleAnswers.getDocumentQuestions().getTemplateId());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null, "Create  possible answer",
					authorName + " created possible answer " + possibleAnswers.getPossibleAnswer() + " in the question "
							+ possibleAnswers.getDocumentQuestions().getName(),
					author.getUserId(), null, documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), possibleAnswers.getDocumentQuestions().getId(), possibleAnswers.getId());
		}
		if (LoggingEventType.DELETE_POSSIBLE_ANSWER.equals(loggingEvent.getEventType())) {
			PossibleAnswers possibleAnswers = possibleAnswersService.find((Long) loggingEvent.getSource());
			DocumentTemplates documentTemplates = documentTemplatesService
					.find(possibleAnswers.getDocumentQuestions().getTemplateId());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null,
					"Delete  possible answer in the question",
					authorName + " deleted possible answer " + possibleAnswers.getPossibleAnswer() + " in the question "
							+ possibleAnswers.getDocumentQuestions().getName(),
					author.getUserId(), null, documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), possibleAnswers.getDocumentQuestions().getId(), possibleAnswers.getId());
		}

		if (LoggingEventType.UPDATE_TEMPLATE_NAME.equals(loggingEvent.getEventType())) {
			DocumentTemplates documentTemplates = documentTemplatesService.find((Long) loggingEvent.getSource());
			DocumentVault documentVault = documentService.find(documentTemplates.getInitialDocId());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "TEMPLATE", null, "Update template name",
					authorName + " updated  template name from" + loggingEvent.getOldValue() + " to "
							+ loggingEvent.getNewValue(),
					author.getUserId(), null, documentVault.getDocId(), null, documentTemplates.getTemplateName(), null,
					documentTemplates.getId(), null, null);
		}

		if (LoggingEventType.SIGNIFICANT_NEWS.equals(loggingEvent.getEventType())) {
			SignificantNews significantnews = significantnewsService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "SIGNIFICANT NEWS", null,
					"Marked as significant news",
					authorName + " marked as significant news " + significantnews.getTitle(), author.getUserId(),
					significantnews.getEntityName(), significantnews.getId());

		}

		if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_NAME_CHANGE.equals(loggingEvent.getEventType())) {
			Sources classification = sourcesService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Classification Name Changes",
					authorName + " changed source " + classification.getSourceDisplayName()
							+ " classifcation name from " + classification.getSourceName() + " to "
							+ loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_CLASSIFICATION_DISPLAY_NAME_CHANGE.equals(loggingEvent.getEventType())) {
			Sources classification = sourcesService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY",
					"Source Classification Display Name Changes",
					authorName + " changed source " + classification.getSourceDisplayName()
							+ " classifcation display name from " + classification.getSourceDisplayName() + " to "
							+ loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_CLASSIFICATION_LINK_CHANGE.equals(loggingEvent.getEventType())) {
			Sources classification = sourcesService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Classification Link Changes",
					authorName + " changed source " + classification.getSourceDisplayName()
							+ " classifcation link from " + classification.getSourceUrl() + " to "
							+ loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_DOMAIN_NEW_ASSIGNED.equals(loggingEvent.getEventType())) {
			SourceDomain domain = domainService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Domain Changes", authorName
					+ " added new  domain  for source " + loggingEvent.getOldValue() + " " + domain.getDomainName(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_JURISDICTION_NEW_ASSIGNED.equals(loggingEvent.getEventType())) {
			SourceJurisdiction jurisdiction = jurisdictionService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Jurisdiction Changes",
					authorName + " added new  jurisdiction for source " + loggingEvent.getOldValue() + " "
							+ jurisdiction.getJurisdictionName(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_INDUSTRY_NEW_ASSIGNED.equals(loggingEvent.getEventType())) {
			SourceIndustry industry = industryService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Industry Changes",
					authorName + " added new  industry for source " + loggingEvent.getOldValue() + " "
							+ industry.getIndustryName(),
					author.getUserId(), loggingEvent.getNewValue());
		}
		if (LoggingEventType.SOURCE_MEDIA_NEW_ASSIGNED.equals(loggingEvent.getEventType())) {
			SourceMedia media = mediaService.find((Long) loggingEvent.getSource());
			log = new AuditLog(
					new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Media Changes", authorName
							+ " added new  media for source " + loggingEvent.getOldValue() + " " + media.getMediaName(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_CREATED.equals(loggingEvent.getEventType())) {
			Sources sources = sourcesService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Newly Created",
					authorName + " created new source " + sources.getSourceName(), author.getUserId(),
					loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_DOMAIN_HIDE.equals(loggingEvent.getEventType())) {
			SourcesHideStatus hideStatus = sourcesHideStatusService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Domain Hide Changes",
					authorName + " changed source " + loggingEvent.getOldValue() + " domain hide status  from "
							+ hideStatus.getHideDomain() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}
		if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_INDUSTRY_HIDE.equals(loggingEvent.getEventType())) {
			SourcesHideStatus hideStatus = sourcesHideStatusService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Industry Hide Changes",
					authorName + " changed source " + loggingEvent.getOldValue() + " industry hide status  from "
							+ hideStatus.getHideDomain() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_JURISDICTION_HIDE.equals(loggingEvent.getEventType())) {
			SourcesHideStatus hideStatus = sourcesHideStatusService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Jurisdiction Hide Changes",
					authorName + " changed source " + loggingEvent.getOldValue() + "  jurisdiction hide status  from "
							+ hideStatus.getHideDomain() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_VISIBILITY.equals(loggingEvent.getEventType())) {
			SourcesHideStatus hideStatus = sourcesHideStatusService.find((Long) loggingEvent.getSource());
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "Source Visibility Hide Changes",
					authorName + " changed source " + loggingEvent.getOldValue() + "  visibility hide status  from "
							+ hideStatus.getHideDomain() + " to " + loggingEvent.getNewValue(),
					author.getUserId(), loggingEvent.getNewValue());
		}

		if (LoggingEventType.SENTIMENT_NEWS.equals(loggingEvent.getEventType())) {
			SignificantNews significantnews = significantnewsService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "SENTIMENT NEWS", null,
					"Created sentiment to a news",
					authorName + " created  sentiment to a news " + significantnews.getTitle(), author.getUserId(),
					significantnews.getEntityName(), significantnews.getId());
		}
		if (LoggingEventType.UPDATE_SENTIMENT_NEWS.equals(loggingEvent.getEventType())) {
			SignificantNews significantnews = significantnewsService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "SENTIMENT NEWS", null,
					"Update news sentiment",
					authorName + " updated news sentiment " + significantnews.getTitle() + " from "
							+ loggingEvent.getOldValue() + " to " + significantnews.getSentiment(),
					author.getUserId(), significantnews.getEntityName(), significantnews.getId());
		}
		if (LoggingEventType.UPDATE_SOURCE.equals(loggingEvent.getEventType())) {
			Sources classification = sourcesService.find((Long) loggingEvent.getSource());
			StringBuilder description = new StringBuilder();
			description.append(authorName + " updated source " + classification.getSourceDisplayName() + ",");
			for (LoggingEvent loggingEventObject : loggingEvent.getLoggingEventList()) {
				if (LoggingEventType.SOURCE_CLASSIFICATION_DISPLAY_NAME_CHANGE
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed display name from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_DOMAIN_HIDE
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed domain hide status from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_INDUSTRY_HIDE
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed industry hide status from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_JURISDICTION_HIDE
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed jurisdiction hide status from " + loggingEventObject.getOldValue()
							+ " to " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_LINK_HIDE.equals(loggingEventObject.getEventType())) {
					description.append(" changed link hide status from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_MEDIA_HIDE
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed media hide status from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_VISIBILITY
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed visibility hide status from " + loggingEventObject.getOldValue()
							+ " to " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_NAME_CHANGE
						.equals(loggingEventObject.getEventType())) {
					description.append(" changed name from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_CLASSIFICATION_LINK_CHANGE.equals(loggingEventObject.getEventType())) {
					description.append(" changed link from " + loggingEventObject.getOldValue() + " to "
							+ loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.DELETE_SOURCE_DOMAIN.equals(loggingEventObject.getEventType())) {
					description.append(" removed domain " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_DOMAIN_NEW_ASSIGNED.equals(loggingEventObject.getEventType())) {
					description.append(" added new domain " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.DELETE_SOURCE_JURISDICTION.equals(loggingEventObject.getEventType())) {
					description.append(" removed jurisdiction " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_JURISDICTION_NEW_ASSIGNED.equals(loggingEventObject.getEventType())) {
					description.append(" added new jurisdiction " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.DELETE_SOURCE_INDUSTRY.equals(loggingEventObject.getEventType())) {
					description.append(" removed industry " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_INDUSTRY_NEW_ASSIGNED.equals(loggingEventObject.getEventType())) {
					description.append(" added new  industry " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.DELETE_SOURCE_MEDIA.equals(loggingEventObject.getEventType())) {
					description.append(" removed  media " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SOURCE_MEDIA_NEW_ASSIGNED.equals(loggingEventObject.getEventType())) {
					description.append(" added new media " + loggingEventObject.getNewValue() + ",");
				}
				if (LoggingEventType.SUB_CLASSIFICATION_CREDIBILITY_CHANGE.equals(loggingEventObject.getEventType())) {
					description.append(" changed sub classification credibility from "
							+ loggingEventObject.getOldCredibilityEnums() + " to "
							+ loggingEventObject.getNewCredibilityEnums() + ",");
				}
				if (LoggingEventType.ATTRIBUTE_CREDIBILITY_CHANGE.equals(loggingEventObject.getEventType())) {
					description.append(" changed sub classification attribute credibility from "
							+ loggingEventObject.getOldCredibilityEnums() + " to "
							+ loggingEventObject.getNewCredibilityEnums() + ",");
				}
				if (LoggingEventType.ATTRIBUTE_CREDIBILITY_ADD_NEW.equals(loggingEventObject.getEventType())) {
					description.append(" changed sub classification attribute credibility from "
							+ loggingEventObject.getOldCredibilityEnums() + " to "
							+ loggingEventObject.getNewCredibilityEnums() + ",");
				}
			}
			String descriptionAuditLog = description.substring(0, description.length() - 1);
			log = new AuditLog(new Date(), LogLevels.INFO, "SOURCE CREDIBILITY", "update source credibility",
					descriptionAuditLog, author.getUserId(), classification.getSourceId(),
					classification.getSourceDisplayName());
		}
		if (LoggingEventType.CREATE_COMPLIANCE_WIDGET_REVIEW.equals(loggingEvent.getEventType())) {
			WidgetReview widgetReview = widgetReviewService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "Compliance widget review",
					"Created compliance widget review",
					authorName + " created compliance widget review " + widgetReview.getWidget().getWidgetName() + " " + Boolean.toString(widgetReview.getIsApproved()),
					author.getUserId(),"");
		}
		if (LoggingEventType.UPDATE_COMPLIANCE_WIDGET_REVIEW.equals(loggingEvent.getEventType())) {
			WidgetReview widgetReview = widgetReviewService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "Compliance widget review",
					"Update compliance widget review",
					authorName + " updated compliance widget review " + widgetReview.getWidget().getWidgetName() + " from " +loggingEvent.getOldValue() +" to "+Boolean.toString(widgetReview.getIsApproved()),
					author.getUserId(),"");
		}
		if (LoggingEventType.OVERRIDE_COMPANY_DETAILS.equals(loggingEvent.getEventType())) {
			EntityAttributes entityAttributes = (EntityAttributes) loggingEvent.getSource();
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "Entity", null, "Override company information",
					authorName + " changed " +entityAttributes.getSourceSchema() +" value as " + entityAttributes.getValue() +" in entity information", author.getUserId(), null, null, null, authorName);
		}
		if (LoggingEventType.PEP_SIGNIFICANT_MARK.equals(loggingEvent.getEventType())) {
			SignificantWatchList significantWatchList = significantWatchListService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "PEP SIGNIFICANT MARK", null,
					"Marked watchlist as significant",
					authorName + " marked pep " + significantWatchList.getValue()+" as significant", author.getUserId(),
					significantWatchList.getName(), significantWatchList.getId());

		}
		if (LoggingEventType.SANCTION_SIGNIFICANT_MARK.equals(loggingEvent.getEventType())) {
			SignificantWatchList significantWatchList = significantWatchListService.find((Long) loggingEvent.getSource());
			log = new AuditLog(loggingEvent.getEventTime(), LogLevels.INFO, "SANCTION SIGNIFICANT MARK", null,
					"Marked watchlist as significant",
					authorName + " marked sanction " + significantWatchList.getValue()+" as significant", author.getUserId(),
					significantWatchList.getName(), significantWatchList.getId());

		}
		
		auditLogService.save(log);
	}
}
