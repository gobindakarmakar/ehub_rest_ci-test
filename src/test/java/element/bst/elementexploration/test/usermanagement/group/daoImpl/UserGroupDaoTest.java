package element.bst.elementexploration.test.usermanagement.group.daoImpl;

import static org.junit.Assert.assertEquals;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.usermanagement.group.daoImpl.UserGroupDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;

@RunWith(MockitoJUnitRunner.class)
public class UserGroupDaoTest {
	
	@Mock
	SessionFactory sessionFactory;

	@Mock
	Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	Query query;

	@InjectMocks
	UserGroupDaoImpl userGroupDaoImpl;
	
	UserGroup userGroup;
	
	@Before
	public void doSetup() {
		userGroup = new UserGroup(1L, 2L);
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getUserGroupTest() {
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(userGroup);
		UserGroup result = userGroupDaoImpl.getUserGroup(1L, 2L);
		assertEquals(userGroup.getUserId(), result.getUserId());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserGroup_whenNoResultExceptionThrowTest() {
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		UserGroup result = userGroupDaoImpl.getUserGroup(1L, 2L);
		assertEquals(null, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserGroup_whenHibernateExceptionThrowTest() {
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		UserGroup result = userGroupDaoImpl.getUserGroup(1L, 2L);
		assertEquals(null, result);
	}
}
