package element.bst.elementexploration.rest.notification.serviceImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.notification.dao.NotificationCheckDao;
import element.bst.elementexploration.rest.notification.dao.NotificationDao;
import element.bst.elementexploration.rest.notification.dao.NotificationMappingDao;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.domain.NotificationMapping;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;
import element.bst.elementexploration.rest.notification.dto.ParticipantDTO;
import element.bst.elementexploration.rest.notification.service.NotificationService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Viswanath
 *
 */
@Service("notificationService")
public class NotificationServiceImpl extends GenericServiceImpl<NotificationAlert, Long>
		implements NotificationService {

	@Autowired
	public NotificationServiceImpl(GenericDao<NotificationAlert, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private NotificationDao notificationDao;

	@Autowired
	private NotificationMappingDao notificationMappingDao;

	@Autowired
	private NotificationCheckDao notificationCheckDao;
	
	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	
	@Autowired
	private AuditLogDao auditLogDao;

	@Override
	@Transactional("transactionManager")
	public Long createNotificationAlert(NotificationAlert notification) {
		Long notificationId = null;
		LocalDateTime dateNow = LocalDateTime.now();
		if (!StringUtils.isEmpty(notification.getBody()) && notification.getCreatedBy() != null
				&& !StringUtils.isEmpty(notification.getSubject()) && notification.getRecipients() != null
				&& !notification.getRecipients().isEmpty()) {
			notification.setCreatedOn(dateNow);
			if (notification.getDeliverOn() == null)
				notification.setDeliverOn(dateNow);
			NotificationAlert notificationReturn = notificationDao.create(notification);
			notificationId = notificationReturn.getNotificationId();
			Set<Long> recipients = new HashSet<Long>();
			recipients.addAll(notification.getRecipients());
			for (Long recepientId : recipients) {
				notificationMappingDao.create(new NotificationMapping(notificationId, recepientId));
			}
		} else {
			throw new InsufficientDataException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
		return notificationId;
	}

	@Override
	@Transactional("transactionManager")
	public String checkInNotifications(Long userId) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		String checkInDate = formatter.format(notificationCheckDao.checkInNotification(userId));
		return checkInDate;
	}

	@Override
	@Transactional("transactionManager")
	public LocalDateTime getLastCheckIn(Long userId) {
		return notificationCheckDao.getLastCheckIn(userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<NotificationAlertDTO> retrieveCaseNotificationAlertBasedOnLastCheckIn(Long caseId, Long userId,
			LocalDateTime lastCheckIn, Integer type, Integer level, LocalDateTime dateTo) {
		List<NotificationAlertDTO> notifications = new ArrayList<NotificationAlertDTO>();
		try {
			notifications = notificationDao.retrieveCaseNotificationAlertBasedOnLastCheckIn(caseId, userId, lastCheckIn,
					type, level, dateTo);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw e;
		}
		return notifications;
	}

	@Override
	@Transactional("transactionManager")
	public List<NotificationAlertDTO> getUserNotificationList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo,
			Long caseId, Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy,
			LocalDateTime lastCheckIn, String orderIn) {

		return notificationDao.getUserNotificationList(userId, dateFrom, dateTo, caseId, type,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, lastCheckIn, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public NotificationAlertDTO retrieveNotificationDetail(Long notifId, Long userId) {
		NotificationAlertDTO response = null;
		if (notifId != null) {
			response = notificationDao.retrieveNotificationDetail(notifId, userId);
		}
		return response;
	}
	
	@Override
	@Transactional("transactionManager")
	public Long countUserNotificationList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId, Integer type,
			LocalDateTime lastCheckIn) {
		return notificationDao.countUserNotificationList(userId, dateFrom, dateTo, caseId, type, lastCheckIn);
	}
	
	/**
	 * Create new event
	 */
	@Override
	@Transactional("transactionManager")
	public Long createEvent(NotificationAlert notification){
		Long notificationId = null;
		LocalDateTime dateNow = LocalDateTime.now();
		notification.setCreatedOn(dateNow);
		NotificationAlert notificationReturn = notificationDao.create(notification);
		notificationId = notificationReturn.getNotificationId();
		if (notification.getRecipients() != null && notification.getRecipients().size()!=0) {
			Set<Long> recipients = new HashSet<>();
			recipients.addAll(notification.getRecipients());
			List<Long> finalList = new ArrayList<>();
			finalList.addAll(recipients);
			List<Users> users = usersDao.getUsersInList(finalList);
			for (Users user: users) {
				notificationMappingDao.create(new NotificationMapping(notificationId, user.getUserId()));
			}
		}
		return notificationId;
	}

	@Override
	@Transactional("transactionManager")
	public boolean isEventOwner(Long eventId, Long userId) {
		return notificationDao.isEventOwner(eventId, userId);
	}

	@Override
	@Transactional("transactionManager")
	public void addEventParticipants(Long eventId, List<Long> participants, Long userId) {
		if(notificationDao.find(eventId).getScheduledFrom() != null){
			Set<Long> newParticipants = new HashSet<>();
			newParticipants.addAll(participants);
			List<Long> finalList = new ArrayList<>();
			finalList.addAll(newParticipants);
			List<Users> users = usersDao.getUsersInList(finalList);
			List<Long> finalParticipantList = new ArrayList<>();
			if(!users.isEmpty()){
				finalParticipantList = users.stream()
	                    .map(Users::getUserId).collect(Collectors.toList());
			}
			List<Long> finalUserList = notificationMappingDao.addEventParticipants(eventId, finalParticipantList);
			List<Users> finalUsers = usersDao.getUsersInList(finalUserList);
			if(!finalUsers.isEmpty()){
				String postFix = "added ";
				postFix = postFix + (finalUsers.size() == 1 ? "participant " : "participants ");
				for(Users user : finalUsers){
					postFix = postFix + user.getFirstName() + " " + user.getLastName() + " ";
				}
				postFix = postFix.trim();
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Added participant");
				Users author = usersDao.find(userId);
				author = usersService.prepareUserFromFirebase(author);
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + postFix);
				log.setUserId(userId);
				auditLogDao.create(log);
			}			
		}else
			throw new NoDataFoundException(ElementConstants.EVENT_NOT_FOUND);
	}

	@Override
	@Transactional("transactionManager")
	public void removeEventParticipants(Long eventId, List<Long> participants, Long userId) {
		if(notificationDao.find(eventId).getScheduledFrom() != null){
			notificationMappingDao.removeEventParticipants(eventId, participants);
			List<Users> finalUsers = usersDao.getUsersInList(participants);
			if(!finalUsers.isEmpty()){
				String postFix = "removed ";
				postFix = postFix + (finalUsers.size() == 1 ? "participant " : "participants ");
				for(Users user : finalUsers){
					postFix = postFix + user.getFirstName() + " " + user.getLastName() + " ";
				}
				postFix = postFix.trim();
				AuditLog log = new AuditLog(new Date(), LogLevels.WARNING, null, null, "Removed participant");
				Users author = usersDao.find(userId);
				author = usersService.prepareUserFromFirebase(author);
				log.setDescription(author.getFirstName() + " " + author.getLastName() + " " + postFix);
				log.setUserId(userId);
				auditLogDao.create(log);
			}	
		}else
			throw new NoDataFoundException(ElementConstants.EVENT_NOT_FOUND);
	}

	@Override
	@Transactional("transactionManager")
	public void updateEvent(NotificationAlert event, NotificationAlert oldEvent) {

		event.setCreatedBy(oldEvent.getCreatedBy());
		event.setCreatedOn(oldEvent.getCreatedOn());
		notificationDao.saveOrUpdate(event);
	}

	@Override
	@Transactional("transactionManager")
	public List<EventDTO> getUserEventList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		return notificationDao.getUserEventList(userId, dateFrom, dateTo, caseId, type,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserEventList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId, Integer type) {
		return notificationDao.countUserEventList(userId, dateFrom, dateTo, caseId, type);
	}

	@Override
	@Transactional("transactionManager")
	public List<EventDTO> getEventListByCreator(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		return notificationDao.getEventListByCreator(userId, dateFrom, dateTo, caseId, type,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public Long countEventListByCreator(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId, Integer type) {
		return notificationDao.countEventListByCreator(userId, dateFrom, dateTo, caseId, type);
	}

	@Override
	@Transactional("transactionManager")
	public EventDetailDTO retrieveEventDetail(Long eventId, Long userId) {
		EventDetailDTO response = null;
		if (eventId != null) {
			try {
				response = notificationDao.retrieveEventDetail(eventId, userId);
				if(response != null){
					List<Long> participants = notificationMappingDao.getEventParticipants(eventId);
					if (participants != null && !participants.isEmpty()) {
						List<ParticipantDTO> participantList = new ArrayList<>();
						List<Users> users = usersDao.getUsersInList(participants);
						if (users != null && !users.isEmpty()) {
							for (Users user : users) {
								participantList.add(new ParticipantDTO(user.getFirstName() + " " + user.getLastName(),
										user.getEmailAddress(), user.getUserId()));
							}
							response.setRecipients(participantList);
						}
					}
				}
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				throw e;
			}
		}
		return response;
	}
}
