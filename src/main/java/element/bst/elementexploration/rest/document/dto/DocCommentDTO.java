package element.bst.elementexploration.rest.document.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Document comment")
public class DocCommentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7740114141649381338L;

	@ApiModelProperty("Id of comment")
	private Long commentId;

	@ApiModelProperty("Title of comment")
	private String title;

	@ApiModelProperty("Description of comment")
	private String description;

	@ApiModelProperty("Created date of comment")
	private Date createdOn;

	@ApiModelProperty("Modified date of comment")
	private Date modifiedOn;

	@ApiModelProperty("Modified reason of comment")
	private String modificationReason;


	public DocCommentDTO(Long commentId, String title, String description,
			Date createdOn, Date modifiedOn, String modificationReason) {
		super();
		this.commentId = commentId;
		this.title = title;
		this.description = description;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
		this.modificationReason = modificationReason;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModificationReason() {
		return modificationReason;
	}

	public void setModificationReason(String modificationReason) {
		this.modificationReason = modificationReason;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
