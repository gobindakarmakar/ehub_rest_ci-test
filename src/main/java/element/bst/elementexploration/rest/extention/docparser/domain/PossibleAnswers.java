package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "POSSIBLE_ANSWERS")
public class PossibleAnswers implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String possibleAnswer;
	private String answerPosition;
	private String answerPositionOnThePage;
	private String answerUUID;
	private Boolean deleted = false; 
	private DocumentQuestions documentQuestions;

	public PossibleAnswers() {
	}

	public PossibleAnswers(long id, String possibleAnswer, String answerPosition, String answerPositionOnThePage,
			String answerUUID, DocumentQuestions documentQuestions) {
		super();
		this.id = id;
		this.possibleAnswer = possibleAnswer;
		this.answerPosition = answerPosition;
		this.answerPositionOnThePage = answerPositionOnThePage;
		this.answerUUID = answerUUID;
		this.documentQuestions = documentQuestions;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "POSSIBLE_ANSWER")
	public String getPossibleAnswer() {
		return possibleAnswer;
	}

	public void setPossibleAnswer(String possibleAnswer) {
		this.possibleAnswer = possibleAnswer;
	}

	@Column(name = "ANSWER_POSITION")
	public String getAnswerPosition() {
		return answerPosition;
	}

	public void setAnswerPosition(String answerPosition) {
		this.answerPosition = answerPosition;
	}

	@ManyToOne
	@JoinColumn(name = "DOCUMENT_QUESTION_ID")
	public DocumentQuestions getDocumentQuestions() {
		return documentQuestions;
	}

	public void setDocumentQuestions(DocumentQuestions documentQuestions) {
		this.documentQuestions = documentQuestions;
	}

	@Column(name = "ANSWER_POSITION_ON_THE_PAGE")
	public String getAnswerPositionOnThePage() {
		return answerPositionOnThePage;
	}

	public void setAnswerPositionOnThePage(String answerPositionOnThePage) {
		this.answerPositionOnThePage = answerPositionOnThePage;
	}

	@Column(name = "ANSWER_UUID")
	public String getAnswerUUID() {
		return answerUUID;
	}

	public void setAnswerUUID(String answerUUID) {
		this.answerUUID = answerUUID;
	}
	
	@Column(name = "IS_DELETED")
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
