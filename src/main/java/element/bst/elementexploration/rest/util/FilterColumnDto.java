package element.bst.elementexploration.rest.util;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Prateek Maurya
 *
 */
public class FilterColumnDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private String column;
	private String operator;
	private RequestFilterDto condition1;
	private RequestFilterDto condition2;
	
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public RequestFilterDto getCondition1() {
		return condition1;
	}
	public void setCondition1(RequestFilterDto condition1) {
		this.condition1 = condition1;
	}
	public RequestFilterDto getCondition2() {
		return condition2;
	}
	public void setCondition2(RequestFilterDto condition2) {
		this.condition2 = condition2;
	}
	
}
