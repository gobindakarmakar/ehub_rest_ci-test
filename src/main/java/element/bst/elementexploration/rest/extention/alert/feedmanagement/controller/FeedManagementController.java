package element.bst.elementexploration.rest.extention.alert.feedmanagement.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Hibernate;
import org.json.JSONArray;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.openjson.JSONObject;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedGroupsDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedsListDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedGroupSevice;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedManagementService;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.FeedUsersDto;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.RequestFilterDto;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hanuman
 *
 */

@Api(tags = { "FEED MANAGEMENT API" })
@RestController
@RequestMapping("/api/feedManagement")
public class FeedManagementController extends BaseController {

	@Autowired
	FeedManagementService feedManagementService;

	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	FeedGroupSevice feedGroupsService;
	
	@Autowired
	AlertManagementService alertService;
	
	@ApiOperation("Get All Feed Items")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllFeedItems", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllFeedItems(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) String orderBy,
			@RequestParam(required = true) Boolean isAllRequired, @RequestBody(required = false) RequestFilterDto filterDto) throws Exception {
		
		List<FeedManagementDto> feedDtos = feedManagementService.getAllFeedItems(filterDto, isAllRequired, pageNumber, recordsPerPage, orderIn, orderBy);
		
		PaginationInformation information = new PaginationInformation();
		
		if(pageNumber != null && recordsPerPage != null ) {

			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			Long totalResults = feedManagementService.getFeedsCount();
			information.setTotalResults(totalResults);
			information.setTitle(request.getRequestURI());
			information.setKind("list");
			information.setCount(feedDtos != null ? feedDtos.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");
		}
		FeedsListDto feedListDto = new FeedsListDto();
		feedListDto.setResult(feedDtos);
		feedListDto.setPaginationInformation(information);
		return new ResponseEntity<>(feedListDto,HttpStatus.OK);
	}

	@ApiOperation("Get Feed Item")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getFeedItemById", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFeedItemById(HttpServletRequest request,@RequestParam("token") String token,@RequestParam(required = true) Long feedManagementID) throws Exception {
		FeedManagementDto complexStructureDto=feedManagementService.getFeedItemByID(feedManagementID );
		if(complexStructureDto==null) {
			return new ResponseEntity<>(false, HttpStatus.OK);
		}else {

			return new ResponseEntity<>(complexStructureDto,HttpStatus.OK);
		}
	}


	@ApiOperation("Save Feed Item")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateFeedItem", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateFeedItem(HttpServletRequest request, @RequestParam String token,
			//@RequestParam(required = true) String entityName,@RequestParam(required = true) String entityId,@RequestParam(required = true) String entitySource,@RequestParam(required = true) boolean isComplexStructure) throws Exception {
			@RequestBody FeedManagementDto feedManagementDto) throws Exception {
		long userIdTemp = getCurrentUserId();
		List<FeedGroups> feedGroupsListDTO = feedManagementDto.getGroupLevels();
		FeedManagementDto entityComplexStructureDto = new FeedManagementDto();
		if (null != feedManagementDto.getFeedName()) {
			 entityComplexStructureDto = feedManagementService.saveOrUpdateFeedItem(feedManagementDto,
					userIdTemp);
			 FeedManagement feedManagement = feedManagementService.find(feedManagementDto.getFeed_management_id());
			 if(null != feedGroupsListDTO){
				 for (FeedGroups dto : feedGroupsListDTO) {
						FeedGroups newdto = new FeedGroups();
						newdto.setFeedId(feedManagement);
						Groups newGroup = new Groups();
						Groups existingGroup = groupsService.find(dto.getGroupId().getId());
						if(null !=  existingGroup){
							BeanUtils.copyProperties(existingGroup, newGroup);
							newdto.setGroupId(newGroup);
							newdto.setRank(dto.getRank());
							//dtoGroupsString.append(newGroup.getName()+ ", ");
							feedGroupsService.saveOrUpdate(newdto);
						}
					}
				 alertService.updateGroupLevelOnfeedChangeByRnak(feedManagement.getFeed_management_id(),feedGroupsListDTO);
			 }
			 
			 
			if (entityComplexStructureDto == null) {
				return new ResponseEntity<>(new ResponseMessage("Feed Item Not Updated"), HttpStatus.NOT_FOUND);
			} else {

				return new ResponseEntity<>(entityComplexStructureDto, HttpStatus.OK);
			}
			//return new ResponseEntity<>(entityComplexStructureDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage("Feed Name can not empty"), HttpStatus.OK);
		}
	}


	@ApiOperation("Delete Feed Item")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/deleteFeedItem", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteFeedItem(@RequestParam(required=true) Long feedID, @RequestParam("token") String token,
			HttpServletRequest request) {
		long userIdTemp = getCurrentUserId();
		if(feedID != null){
			boolean isFeedItemDeleted=feedManagementService.deleteFeedItem(feedID, userIdTemp);
			if(isFeedItemDeleted == true){
				return new ResponseEntity<>(isFeedItemDeleted,HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(" can not delete feed associated with Alert/Alerts"), HttpStatus.OK);
			}
		}else{
			return new ResponseEntity<>(new ResponseMessage(" feed id can not be null"), HttpStatus.OK);
		}
	}

	@ApiOperation("Get Group levels by feed ID ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getGroupLevelByFeedId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupLevelByFeedId(HttpServletRequest request,@RequestParam("token") String token,@RequestParam(required = true) Long feedManagementID) throws Exception {
		List<FeedGroupsDto> groupLevels = feedManagementService.getGroupLevelByFeedId(feedManagementID );
		if(groupLevels==null) {
			return new ResponseEntity<>(false, HttpStatus.OK);
		}else {

			return new ResponseEntity<>(groupLevels,HttpStatus.OK);
		}
	}
	
	@ApiOperation("Get Users by feed ID ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUsersByFeed", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUsersByFeed(HttpServletRequest request,@RequestParam("token") String token,@RequestParam(required = false) Long feedManagementID,@RequestParam boolean isAllRequired) throws Exception {
		List<FeedUsersDto> userList = feedManagementService.getUsersByFeed(feedManagementID,isAllRequired );
			return new ResponseEntity<>(userList,HttpStatus.OK);
	}
	
	@ApiOperation("Get Feeds by User ID ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getFeedsByUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFeedsByUser(HttpServletRequest request,@RequestParam("token") String token,@RequestParam(required = true) Long userId) throws Exception {
		
		List<FeedManagementDto> feedDtos = feedManagementService.getFeedsByUserId(userId);
			return new ResponseEntity<>(feedDtos,HttpStatus.OK);
	}
	
	@ApiOperation("Get Feed Groups By Group ID ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getFeedGroupsByGroupId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFeedGroupsByGroupId(HttpServletRequest request,@RequestParam("token") String token,@RequestBody List<Long> groupIds) throws Exception {
		List<FeedGroups> feedGroups = feedManagementService.getFeedGroupsByGroupId(groupIds);
		JSONArray feedGroupArray=null;
		if (feedGroups != null && feedGroups.size()> 0) {
			feedGroupArray = new JSONArray(feedGroups);
			for (int i = 0; i < feedGroupArray.length(); i++) {
					if(feedGroupArray.get(i)!=null && feedGroupArray.get(i) instanceof JSONObject && feedGroupArray.getJSONObject(i).has("feedId") && feedGroupArray.getJSONObject(i).get("feedId") instanceof JSONObject && feedGroupArray.getJSONObject(i).getJSONObject("feedId").has("reviewer"))
						feedGroupArray.getJSONObject(i).getJSONObject("feedId").remove("reviewer");
						
			}
		}
			return new ResponseEntity<>(feedGroupArray.toString(),HttpStatus.OK);
	}
	
}
