package element.bst.elementexploration.rest.extention.alert.feedmanagement.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.dao.FeedGroupsManagementDao;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
@Repository
@Transactional("transactionManager")
public class FeedGroupsManagementDaoImpl extends GenericDaoImpl<FeedGroups, Long> implements 
FeedGroupsManagementDao{

	@Override
	public List<FeedGroups> getFeedGroupsByFeedId(Long feedId) {

		List<FeedGroups> feedManagement = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fg from FeedGroups fg where fg.feedId.feed_management_id = :feedId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("feedId", feedId);
			feedManagement = (List<FeedGroups>) query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return feedManagement;
		}
		return feedManagement;
	}
}
