package element.bst.elementexploration.rest.settings.listManagement.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.settings.listManagement.domain.Icon;
import element.bst.elementexploration.rest.settings.listManagement.dto.IconDto;

public interface IconService extends GenericService<Icon, Long> {

	List<IconDto> getAllIcons();
}
