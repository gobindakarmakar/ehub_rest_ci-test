package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.PrivilegesDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.DefaultPermissionsDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;

/**
 * @author Paul Jalagari
 *
 */
@Repository("privilegesDao")
public class PrivilegesDaoImpl extends GenericDaoImpl<Permissions, Long> implements PrivilegesDao {

	public PrivilegesDaoImpl() {
		super(Permissions.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PermissionsWithIdDto> findAllPermissions() throws Exception {
		List<PermissionsWithIdDto> permissionsDtoList = new ArrayList<>();
		List<Object[]> objects = new ArrayList<>();
		StringBuilder hql = new StringBuilder();
		hql.append("select up.permissionId, up.itemName, up.active, up.parentPermissionId from bst_um_permissions up ");
		NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
		objects = (List<Object[]>) query.getResultList();
		if (objects != null && objects.size() > 0) {
			for (Object[] result : objects) {
				PermissionsWithIdDto permission = new PermissionsWithIdDto();
				if (result[0].toString() != null)
					permission.setPermissionId(Long.valueOf(result[0].toString()));
				if (result[1].toString() != null)
					permission.setItemName(result[1].toString());
				if (result[2]!=null && result[2] instanceof Boolean) {
					//if (Integer.valueOf(result[2].toString()) == 1 && result[2].)
						permission.setActive((Boolean)result[2]);
				}
				if (result[3]!=null&& result[3].toString() != null) {
					//PermissionsWithIdDto parent = findPermissionById(Long.valueOf(result[3].toString()));
					//if (parent != null)
						permission.setParentPermissionId(Long.valueOf(result[3].toString()));
				}
				permissionsDtoList.add(permission);
			}
		}
		return permissionsDtoList;
	}

	@Override
	public PermissionsWithIdDto findPermissionById(Long permissionId) throws Exception {
		PermissionsWithIdDto permissionsWithIdDto = new PermissionsWithIdDto();
		StringBuilder hql = new StringBuilder();
		hql.append("select up.permissionId, up.itemName, up.active, up.parentPermissionId, up.createdOn from bst_um_permissions up ");
		if (permissionId != null)
			hql.append("where up.permissionId =:permissionId ");
		NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
		query.setParameter("permissionId", permissionId);
		Object[] object = (Object[]) query.getSingleResult();
		if (object != null) {
			if(null!=object[0] && object[0].toString()!=null)
			permissionsWithIdDto.setPermissionId(Long.valueOf(object[0].toString()));
			if(null!=object[1] && object[1].toString()!=null)
				permissionsWithIdDto.setItemName(object[1].toString());
			if(null!=object[2])
				permissionsWithIdDto.setActive((Boolean) (object[2]));
			if(null!=object[3] && object[3].toString()!=null)
				permissionsWithIdDto.setParentPermissionId(Long.valueOf(object[3].toString()));
			if(null!=object[4] && object[4].toString()!=null)
				permissionsWithIdDto.setCreatedOn((Date)object[4]);
		}
		return permissionsWithIdDto;
	}

	@Override
	public boolean savePrivilege(DefaultPermissionsDto eachPermission) throws Exception {
		try {
		StringBuilder hql = new StringBuilder();
		//hql.append("INSERT INTO bst_um_permissions(permissionId,itemName,active,parentPermissionId,createdOn,permissionToChild) VALUES(:permissionId,:itemName,:active,:parentPermissionId,:createdOn,:permissionToChild)");
		hql.append("INSERT INTO bst_um_permissions(permissionId,itemName,active,parentPermissionId,createdOn) VALUES(:permissionId,:itemName,:active,:parentPermissionId,:createdOn)");
		NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
		query.setParameter("permissionId", eachPermission.getPermissionId());
		query.setParameter("itemName", eachPermission.getItemName());
		query.setParameter("active", Boolean.TRUE);
		if(eachPermission.getParentPermissionId()!=null)
		query.setParameter("parentPermissionId", eachPermission.getParentPermissionId());
		else
			query.setParameter("parentPermissionId", null);
		query.setParameter("createdOn", new Date());
		/*if(eachPermission.getPermissionToChild()!=null)
		query.setParameter("permissionToChild", eachPermission.getPermissionToChild());
		else
			query.setParameter("permissionToChild", null);*/
		query.executeUpdate();
		return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	
}
