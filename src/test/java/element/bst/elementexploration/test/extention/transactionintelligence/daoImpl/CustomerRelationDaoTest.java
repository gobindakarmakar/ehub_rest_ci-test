package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.CustomerRelationDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerRelationShipDetails;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerRelationDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<CustomerRelationShipDetails> root;

	@InjectMocks
	CustomerRelationDaoImpl customerRelationDao;

	List<CustomerRelationShipDetails> customerRelationShipDetailsList = new ArrayList<CustomerRelationShipDetails>();

	CustomerRelationShipDetails customerRelationShipDetails = new CustomerRelationShipDetails();

	@Before
	public void setupMock() {
		customerRelationShipDetails.setCustomerNumber("1234");
		customerRelationShipDetails.setRelatedCustomerNumber("3456");
		customerRelationShipDetailsList.add(customerRelationShipDetails);
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByCustomerNumberTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(CustomerRelationShipDetails.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(customerRelationShipDetailsList);
		List<CustomerRelationShipDetails> customerRelationShipDetailsListNew = customerRelationDao
				.findByCustomerNumber("1234");
		assertEquals(1, customerRelationShipDetailsListNew.size());

	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void findByCustomerNumber_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(CustomerRelationShipDetails.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		customerRelationDao.findByCustomerNumber("1234");
	}

	@Test
	public void findCustomerRelationTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(customerRelationShipDetails);
		CustomerRelationShipDetails customerRelationShipDetailsObj = customerRelationDao.findCustomerRelation("1234",
				"3456");
		assertEquals("3456", customerRelationShipDetailsObj.getRelatedCustomerNumber());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findCustomerRelation_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		CustomerRelationShipDetails customerRelationShipDetailsObj = customerRelationDao.findCustomerRelation("1234",
				"3456");
		assertEquals(null, customerRelationShipDetailsObj);
	}

	@Test
	public void getCustomerRelationnamesListTest() {
		List<String> names = new ArrayList<String>();
		names.add("Jhon");
		names.add("Michael");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(names);
		List<String> relationNames = customerRelationDao.getCustomerRelationnamesList(1234L);
		assertEquals(2, relationNames.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerRelationnamesList_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<String> relationNames = customerRelationDao.getCustomerRelationnamesList(1234L);
		assertEquals(0, relationNames.size());
	}

}
