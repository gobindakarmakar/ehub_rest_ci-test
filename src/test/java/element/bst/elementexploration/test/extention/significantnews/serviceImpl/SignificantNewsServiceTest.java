package element.bst.elementexploration.test.extention.significantnews.serviceImpl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantNewsDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.serviceImpl.SignificantnewsServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;

@RunWith(MockitoJUnitRunner.class)
public class SignificantNewsServiceTest {

	@Mock
	SignificantNewsDao significantNewsDaoMock;

	@Mock
	UserDao userDaoMock;
	
	@Mock
	AuditLogDao auditLogDaoMock;

	@InjectMocks
	SignificantnewsServiceImpl significantnewsServiceImpl;

	public SignificantNews significantNews;

	@Before
	public void setupMock() {
		significantNews = new SignificantNews();
		significantNews.setEntityId("12345");
		significantNews.setEntityName("entity");
		significantNews.setNewsClass("other");
		significantNews.setPublishedDate("2018-11-16");
		significantNews.setTitle("junit");
		significantNews.setUrl(" ");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getSignificantNewsTest() {
		Mockito.when(significantNewsDaoMock.getSignificantNews(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
				.thenReturn(significantNews);
		SignificantNews newSignificantNews = significantnewsServiceImpl.getSignificantNews("12345", "entity", "other",
				"2018-11-16", " ", "junit");
		assertEquals("12345", newSignificantNews.getEntityId());
	}
	
	/*@Test
	public void deleteSignificantNewsTest() {
		Mockito.when(significantNewsDaoMock.deleteSignificantNews(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(0);
		boolean deleteStatus = significantnewsServiceImpl.deleteSignificantNews(significantNews, 2L);
		assertEquals(false, deleteStatus);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void deleteSignificantNews_whenReturnIntIsZeroTest() {
		Mockito.when(significantNewsDaoMock.deleteSignificantNews(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(10);
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenThrow(NullPointerException.class);
		Mockito.when(auditLogDaoMock.create(Mockito.any(AuditLog.class)));
		boolean deleteStatus = significantnewsServiceImpl.deleteSignificantNews(significantNews, 2L);
		assertEquals(false, deleteStatus);
	}
*/
}
