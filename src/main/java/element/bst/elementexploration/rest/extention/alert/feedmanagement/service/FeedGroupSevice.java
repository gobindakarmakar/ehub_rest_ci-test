package element.bst.elementexploration.rest.extention.alert.feedmanagement.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface FeedGroupSevice extends GenericService<FeedGroups, Long>{

	List<FeedGroups> getFeedGroupsByFeedId(Long feed_management_id);

	StringBuilder deleteFeedgroupList(List<FeedGroups> dbgroups, StringBuilder dbGroupsString); 
	
}
