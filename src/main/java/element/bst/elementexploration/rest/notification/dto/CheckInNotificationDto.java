package element.bst.elementexploration.rest.notification.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Check in notification")
public class CheckInNotificationDto {
	
	@ApiModelProperty("Notification check in time")
	private String checkInTimestamp;

	public CheckInNotificationDto() {
		super();
	}

	public String getCheckInTimestamp() {
		return checkInTimestamp;
	}

	public void setCheckInTimestamp(String checkInTimestamp) {
		this.checkInTimestamp = checkInTimestamp;
	}

}
