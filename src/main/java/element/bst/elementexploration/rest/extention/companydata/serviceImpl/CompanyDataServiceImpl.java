package element.bst.elementexploration.rest.extention.companydata.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.companydata.service.CompanyDataService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("companyDataService")
public class CompanyDataServiceImpl implements CompanyDataService {

	@Value("${site_info_add_keyword}")
	private String COMPANY_DATA_COLLECTOR_URL;
	
	@Override
	public String getCompanyData(String jsonString) throws Exception {
		String url = COMPANY_DATA_COLLECTOR_URL + "/v1/company-data";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
}
