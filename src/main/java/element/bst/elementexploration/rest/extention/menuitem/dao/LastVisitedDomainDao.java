package element.bst.elementexploration.rest.extention.menuitem.dao;

import element.bst.elementexploration.rest.extention.menuitem.domain.LastVisitedDomains;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface LastVisitedDomainDao extends GenericDao<LastVisitedDomains, Long> {

	LastVisitedDomains findLastVisitedDomainByUserId(Long userId);

}
