package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;

public class ActorDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String identifier;
	private String name;
	private String type;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
