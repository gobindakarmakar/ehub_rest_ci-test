package element.bst.elementexploration.rest.extention.widgetreview.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.widgetreview.dao.ComplianceWidgetDao;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author rambabu
 *
 */
@Repository("complianceWidgetDao")
public class ComplianceWidgetDaoImpl extends GenericDaoImpl<ComplianceWidget, Long> implements ComplianceWidgetDao {

	public ComplianceWidgetDaoImpl() {
		super(ComplianceWidget.class);
	}

}
