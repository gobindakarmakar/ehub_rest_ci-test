package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.List;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentContentsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentContents;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("documentContentsDao")
public class DocumentContentsDaoImpl extends GenericDaoImpl<DocumentContents, Long> implements DocumentContentsDao {

	public DocumentContentsDaoImpl() {
		super(DocumentContents.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentContents> getDocumentContentById(Long templateId) {
		List<DocumentContents> documentContentsList=this.getCurrentSession()
				.createQuery("from DocumentContents where templateId = " +templateId).getResultList(); 
		return documentContentsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentContents> getDocumentContentByParentId(Long parentId) {
		List<DocumentContents> documentContentsList=this.getCurrentSession()
				.createQuery("from DocumentContents where parentId = " +parentId).getResultList(); 
		return documentContentsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentContents> getParagraphsAndSubParagraphsByTemplateId(Long templateId) {
		List<DocumentContents> documentContentsList=this.getCurrentSession()
				.createQuery("from DocumentContents where templateId = " +templateId+" and contentType='PARAGRAPH' or contentType='SUB_PARAGRAPH'").getResultList(); 
		return documentContentsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentContents> getTablesByTemplateId(Long templateId) {
		List<DocumentContents> documentContentsList=this.getCurrentSession()
				.createQuery("from DocumentContents where templateId = " +templateId+" and contentType='TABLE'").getResultList(); 
		return documentContentsList;
	}

}
