package element.bst.elementexploration.test.settings.daoimpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.settings.daoimpl.AttributesDaoImpl;
import element.bst.elementexploration.rest.settings.domain.Attributes;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AttributesDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	AttributesDaoImpl attributesDao;

	List<Attributes> attributesList = new ArrayList<Attributes>();

	@Before
	public void doSetup() {
		attributesList.add(new Attributes());
		attributesList.add(new Attributes());
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchSelectedAttributesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(attributesList);
		List<Attributes> attributesListNew = attributesDao.fetchSelectedAttributes(1L);
		assertEquals(2, attributesListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchSelectedAttributes_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<Attributes> attributesListNew = attributesDao.fetchSelectedAttributes(1L);
		assertEquals(null, attributesListNew);
	}

}
