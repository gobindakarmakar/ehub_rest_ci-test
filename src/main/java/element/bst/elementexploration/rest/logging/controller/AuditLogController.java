package element.bst.elementexploration.rest.logging.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.logging.util.LogRequest;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Audit log API" },description="Manages action auditing")
@RestController
@RequestMapping("/api/audit")
public class AuditLogController extends BaseController{

	@Autowired
	GroupsService groupsService;

	@Autowired
	private AuditLogService auditLogService;

	@ApiOperation("List of audit logs")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/listLogs", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listLogs(@RequestParam("token") String token, @RequestBody LogRequest logRequest, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		if (logRequest.getUserId() == null
				&& !groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userId)) {
			logRequest.setUserId(userId);
		} else {
			if (!groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userId)) {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN),
						HttpStatus.UNAUTHORIZED);
			}
		}

		List<AuditLog> logsList = auditLogService.getLogs(logRequest);
		// Pagination
		int pageSize = logRequest.getRecordsPerPage() == null ? ElementConstants.DEFAULT_PAGE_SIZE : logRequest.getRecordsPerPage();
		int index = logRequest.getPageNumber() != null ? logRequest.getPageNumber() : 1;
		long totalResults = auditLogService.countLogs(logRequest);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(logsList != null ? logsList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject jsonObject = new JSONObject();
			jsonObject.put("result", logsList);
			jsonObject.put("paginationInformation", information);*/
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setTypes(auditLogService.getTypes());
		responseUtil.setResult(logsList);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);			
	}

	@ApiOperation("Search audit logs")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/searchLogs", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchLogs(@RequestParam("token") String token, @RequestBody LogRequest logRequest, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		if (logRequest.getUserId() == null
				&& !groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userId)) {
			logRequest.setUserId(userId);
		}else{
			if (!groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userId)) {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
			} 
		}

		if (StringUtils.isEmpty(logRequest.getKeyword()) || logRequest.getKeyword().length() < 3) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG),
					HttpStatus.BAD_REQUEST);
		}
		List<AuditLog> logsList = auditLogService.getLogs(logRequest);
		// Pagination
		int pageSize = logRequest.getRecordsPerPage() == null ? ElementConstants.DEFAULT_PAGE_SIZE : logRequest.getRecordsPerPage();
		int index = logRequest.getPageNumber() != null ? logRequest.getPageNumber() : 1;
		long totalResults = auditLogService.countLogs(logRequest);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(logsList != null ? logsList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setResult(logsList);
		responseUtil.setTypes(auditLogService.getTypes());
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);			
	}

	@ApiOperation("Log Aggregator")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = LogAggregator.class, responseContainer = "List" ,message = "Operation Successful."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/logAggregator", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> logAggregator(@RequestParam("token") String token,
			@RequestBody LogRequest logRequest,HttpServletRequest request) throws ParseException {

		Long userId = getCurrentUserId();
		if (logRequest.getUserId() == null
				&& !groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userId)) {
			logRequest.setUserId(userId);
		} else {
			if (!groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, userId)) {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN),
						HttpStatus.UNAUTHORIZED);
			}
		}
		List<LogAggregator> logAggregatorList = auditLogService.logAggregator(logRequest);
		return new ResponseEntity<>(logAggregatorList, HttpStatus.OK);
	}		

	@ApiOperation("Auditing for Generate Report")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Report Generated successfully") })
	@GetMapping(value = "/auditLogForGeneratedReport", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> auditLogForGeneratedReport( @RequestParam String entityId,@RequestParam String entityName,
			@RequestParam("token") String token, HttpServletRequest request) {
		Long userId = getCurrentUserId();
		AuditLog getLog=auditLogService.auditLogForGeneratedReport(entityId, entityName, userId);
		if(getLog!=null) {
			return new ResponseEntity<>(new ResponseMessage("Generated report audit log created successfully."), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(new ResponseMessage("Generated report audit log not created successfully."), HttpStatus.OK);
		}

	}
	
	
	@ApiOperation("Search audit logs of Feeds by type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getLogsByTypeId", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchLogsForFeeds(@RequestParam("token") String token,
			@RequestParam("type") String type,@RequestParam("typeId") Long typeId,
			HttpServletRequest request) {

		
		List<AuditLog> logsList=auditLogService.getLogsByTypeId(type, typeId);
		
		return new ResponseEntity<>(logsList, HttpStatus.OK);
					
	}
	@ApiOperation("Search audit logs by type and subtype")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getLogsByTypeAndSubtype", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getLogsByTypeAndSubtype(@RequestParam("token") String token,
			@RequestParam("type") String type,@RequestParam("subType") String subType,
			HttpServletRequest request) {

		
		List<AuditLog> logsList=auditLogService.getLogsByTypeAndSubtype(type, subType);
		
		return new ResponseEntity<>(logsList, HttpStatus.OK);
					
	}
	
	@ApiOperation("Search audit logs of Feeds by type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.SEARCH_KEYWORD_VALIDATION_THREE_CHAR_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getLogsByTitleAndTypeId", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getLogsByTitleAndTypeId(@RequestParam("token") String token,
			@RequestParam("title") String title,@RequestParam("typeId") Long typeId,
			HttpServletRequest request) {

		
		List<AuditLog> logsList=auditLogService.getLogsByTitleAndTypeId(title, typeId);
		
		return new ResponseEntity<>(logsList, HttpStatus.OK);
					
	}
}
