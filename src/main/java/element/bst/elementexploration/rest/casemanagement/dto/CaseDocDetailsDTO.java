package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModelProperty;

public class CaseDocDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3837218169317120893L;
	@ApiModelProperty(value="Document ID")
	private Long docId;
	@ApiModelProperty(value="Path of the file")
	private String filePath;
	@ApiModelProperty(value="Title of the file")
	private String fileTitle;
	@ApiModelProperty(value="Remarks on the file")
	private String remarks;
	@ApiModelProperty(value="Size of the file")
	private Double fileSize;
	@ApiModelProperty(value="Type of the file")
	private String fileType;
	@ApiModelProperty(value="File uploaded date")
	private Date uploadedOn;
	@ApiModelProperty(value="File uploaded by")
	private Long uploadedBy;
	@ApiModelProperty(value="File modification date")
	private Date modifiedOn;
	@ApiModelProperty(value="File modified by")
	private Long modifiedBy;
	@ApiModelProperty(value="Flag of the file")
	private Integer docFlag;


	public CaseDocDetailsDTO() {
		super();
	}
	
	public CaseDocDetailsDTO(Long docId, String filePath, String fileTitle, String remarks, Double fileSize,
			String fileType, Date uploadedOn, Long uploadedBy, Date modifiedOn, Long modifiedBy, Integer docFlag) {
		super();
		this.docId = docId;
		this.filePath = filePath;
		this.fileTitle = fileTitle;
		this.remarks = remarks;
		this.fileSize = fileSize;
		this.fileType = fileType;
		this.uploadedOn = uploadedOn;
		this.uploadedBy = uploadedBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.docFlag = docFlag;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileTitle() {
		return fileTitle;
	}

	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getFileSize() {
		return fileSize;
	}

	public void setFileSize(Double fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public Long getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(Long uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(Integer docFlag) {
		this.docFlag = docFlag;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
