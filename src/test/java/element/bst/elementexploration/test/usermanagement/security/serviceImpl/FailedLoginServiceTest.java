package element.bst.elementexploration.test.usermanagement.security.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.annotation.PropertySource;

import element.bst.elementexploration.rest.usermanagement.security.dao.FailedLoginDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.FailedLogin;
import element.bst.elementexploration.rest.usermanagement.security.serviceImpl.FailedLoginServiceImpl;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
@PropertySource("classpath:user.properties")
public class FailedLoginServiceTest {
	
	@Mock
	FailedLoginDao failedLoginDaoMock;
	
	@InjectMocks
	FailedLoginServiceImpl failedLoginService;
	
	FailedLogin failedLogin;
	
	@Before
	public void doSetup() {
		failedLogin = new FailedLogin(1L,"analyst","analyst",2,"10.10.10.10",DateUtils.addDays(new Date(),-2),"10.10.10.100",DateUtils.addDays(new Date(),-1));
		MockitoAnnotations.initMocks(this);
	}
	
	/*@Test
	public void isAccountLockedTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(failedLogin);
		boolean status = failedLoginService.isAccountLocked("analyst");
		assertEquals(true,status);
	}*/
	
	@Test
	public void whenFailedLoginNull_isAccountLockedTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(null);
		boolean status = failedLoginService.isAccountLocked("analyst");
		assertEquals(false,status);
	}
	
	@Test 
	public void releaseAccountLockedTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(failedLogin);
		Mockito.doNothing().when(failedLoginDaoMock).saveOrUpdate(Mockito.any(FailedLogin.class));
		boolean status = failedLoginService.releaseAccountLocked("analyst","10.10.10.10");
		assertEquals(true,status);
	}
	@Test 
	public void whenFailedLoginNull_ReleaseAccountLockedTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(null);		
		boolean status = failedLoginService.releaseAccountLocked("analyst","10.10.10.10");
		assertEquals(true,status);
	}
	
	@Test 
	public void getFailedLoginByUsernameTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(failedLogin);		
		FailedLogin userFailedLogin = failedLoginService.getFailedLoginByUsername("analyst");
		Assert.assertNotNull(userFailedLogin);
	}
	
	@Test 
	public void whenFailedLoginNull_getFailedLoginByUsernameTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(null);		
		FailedLogin userFailedLogin = failedLoginService.getFailedLoginByUsername("analyst");
		assertEquals(null,userFailedLogin);
	}
	
	@Test 
	public void getCurrentLoginAttemptTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(failedLogin);		
		int  loginAttempt = failedLoginService.getCurrentLoginAttempt("analyst","analyst","10.10.10.10");
		assertEquals(3,loginAttempt);
	}
	
	@Test 
	public void whenFailedLoginNull_GetCurrentLoginAttemptTest(){
		Mockito.when(failedLoginDaoMock.getFailedLoginByUsername(Mockito.anyString())).thenReturn(null);		
		int  loginAttempt = failedLoginService.getCurrentLoginAttempt("analyst","analyst","10.10.10");
		assertEquals(1,loginAttempt);
	}
	
	
	
	
	
	

}
