package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplateMappingDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
/**
 * 
 * @author Rambabu
 *
 */
@Repository("documentTemplateMappingDao")
public class DocumentTemplateMappingDaoImpl extends GenericDaoImpl<DocumentTemplateMapping, Long> implements DocumentTemplateMappingDao {

	
	public DocumentTemplateMappingDaoImpl() {
		super(DocumentTemplateMapping.class);
		
	}

	@Override
	public DocumentTemplateMapping getDocumentTemplateMappingByDocId(Long docId) {
		DocumentTemplateMapping documentTemplateMapping=new DocumentTemplateMapping();
		try{			
			Query<?> query = this.getCurrentSession().createQuery(" from DocumentTemplateMapping dtm where dtm.documentId=:docId ");
			query.setParameter("docId", docId);
			documentTemplateMapping = (DocumentTemplateMapping) query.getSingleResult();
			return documentTemplateMapping;
		}
		catch (Exception e)
		{
			return documentTemplateMapping;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentTemplateMapping> getDocumentTemplateMappingByTemplateId(long templateId) {
		List<DocumentTemplateMapping> documentTemplateMappingList=new ArrayList<DocumentTemplateMapping>();
		try{			
			Query<?> query = this.getCurrentSession().createQuery(" from DocumentTemplateMapping dtm where dtm.templateId.id = :templateId ");
			query.setParameter("templateId", templateId);
			documentTemplateMappingList =(List<DocumentTemplateMapping>)query.getResultList();
			return documentTemplateMappingList; 
		}
		catch (Exception e)
		{
			return documentTemplateMappingList;
		}
	}
	

}
