package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CounterPartyAccountNode implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean generated = true;

	private String oid;
	
	private String id;
	
	@JsonProperty("search-name")
	private String searchName;
	
	private String name;
	
	private String location;
	
	private String labelV;
	
	public CounterPartyAccountNode(boolean generated, String oid, String id, String searchName, String name,
			String location, String labelV) {
		super();
		this.generated = generated;
		this.oid = oid;
		this.id = id;
		this.searchName = searchName;
		this.name = name;
		this.location = location;
		this.labelV = labelV;
	}

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLabelV() {
		return labelV;
	}

	public void setLabelV(String labelV) {
		this.labelV = labelV;
	}

}
