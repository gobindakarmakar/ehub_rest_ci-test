package element.bst.elementexploration.rest.extention.screeningriskscore.service;

import element.bst.elementexploration.rest.extention.screeningriskscore.domain.ScreeningDetails;

public interface ScreeningRiskScoreService {
	public ScreeningDetails screeningRiskSocreCalculation(ScreeningDetails screeningDetails);

	public String getScreeningInfo(String jsonString) throws Exception;

}
