package element.bst.elementexploration.test.usermanagement.security.serviceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.usermanagement.security.dao.CountrySettingDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;
import element.bst.elementexploration.rest.usermanagement.security.serviceImpl.CountrySettingServiceImpl;
/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CountrySettingServiceTest {
	
	@Mock
	CountrySettingDao countrySettingDaoMock;
	
	@InjectMocks
	CountrySettingServiceImpl countrySettingService;
	
	public CountrySetting countrySetting;
	
	@Before
	public void doSetup() {
		countrySetting = new CountrySetting();
		countrySetting.setCountryIso("UK");
		countrySetting.setCurrency("pound");
		countrySetting.setCurrencySymbol("£");
		countrySetting.setLanguage("English");
		countrySetting.setId(1L);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test 
	public void getCountrySettingTest(){
		Mockito.when(countrySettingDaoMock.getCountrySetting(Mockito.anyString())).thenReturn(countrySetting);
		CountrySetting resultedCountrySetting = countrySettingService.getCountrySetting("UK");
		Assert.assertNotNull(resultedCountrySetting);
	}
	

}
