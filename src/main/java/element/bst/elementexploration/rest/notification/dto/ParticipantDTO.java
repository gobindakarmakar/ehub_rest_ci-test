package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Participant")
public class ParticipantDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Name of the participant")
	private String name;
	
	@ApiModelProperty(value = "Email ID of the participant")
	private String emailAddress;
	
	@ApiModelProperty(value = "User ID of the participant")
	private Long userId;

	
	public ParticipantDTO() {
		super();
	}

	public ParticipantDTO(String name, String emailAddress, Long userId) {
		super();
		this.name = name;
		this.emailAddress = emailAddress;
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
