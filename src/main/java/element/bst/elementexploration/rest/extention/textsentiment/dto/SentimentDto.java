package element.bst.elementexploration.rest.extention.textsentiment.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class SentimentDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("Sentiment")
	private String sentiment;

	@JsonProperty("SentimentScore")
	private SentimentScore sentimentScore;

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public SentimentScore getSentimentScore() {
		return sentimentScore;
	}

	public void setSentimentScore(SentimentScore sentimentScore) {
		this.sentimentScore = sentimentScore;
	}

}
