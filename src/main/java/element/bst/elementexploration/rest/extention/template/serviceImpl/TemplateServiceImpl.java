package element.bst.elementexploration.rest.extention.template.serviceImpl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToUploadCaseSeedDocumentException;
import element.bst.elementexploration.rest.extention.template.service.TemplateService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("templateService")
public class TemplateServiceImpl implements TemplateService {
	
	@Value("${template_api}")
	private String TEMPLATE_API;

	@Override
	public byte[] getTemplateByName(String requestBody, String templateName) throws Exception {
		String url = TEMPLATE_API +"templates/"+ encode(templateName)+"/generate";
		byte[] fileData = ServiceCallHelper.downloadFileFromServer(url,requestBody);

		return fileData;
	}

	/*private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}*/
	
	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	@Override
	public String [] saveTemplate(MultipartFile uploadFile, String templateName) throws Exception {
		if (uploadFile != null && uploadFile.getSize() > 0) {

		String originalFile = new String(uploadFile.getOriginalFilename());
		String ext = FilenameUtils.getExtension(originalFile);
		String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
		File file = null;
		file = File.createTempFile(fileNameWithoutExtension, ext);
		uploadFile.transferTo(file);
		String url = TEMPLATE_API +"templates/"+ encode(templateName);
		String [] serverResponse=ServiceCallHelper.putFileToServer(url, file);
		return serverResponse;}
		else {
			throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
		}
	}
}
