package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

public class Level1CodeDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String level1Evaluation;

	public String getlevel1Evaluation() {
		return level1Evaluation;
	}

	public void setlevel1Evaluation(String level1Evaluation) {
		this.level1Evaluation = level1Evaluation;
	}
	
	

}
