package element.bst.elementexploration.test.extention.sourcecredibility.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceIndustryDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceIndustryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl.SourceIndustryServiceImpl;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SourceIndustryServiceTest {
	
	
	@Mock
	SourceIndustryDao sourceIndustryDao;
	
	@InjectMocks
	SourceIndustryServiceImpl sourceIndustryService;
	
	List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
	SourceIndustry sourceIndustryOne = null;
	SourceIndustryDto sourceIndustryDto = null;
	
	@Before
    public void setupMock() {
		sourceIndustryOne = new SourceIndustry("Technology");
		sourceIndustryDto = new SourceIndustryDto("Construction");
		SourceIndustry sourceIndustryTwo = new SourceIndustry("Telecommunication");
		industryList.add(sourceIndustryOne);
		industryList.add(sourceIndustryTwo);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getClassifications() {
		Mockito.when(sourceIndustryDao.findAll()).thenReturn(industryList);
		List<SourceIndustryDto> industryCheck =  sourceIndustryService.getSourceIndustry();
		Assert.assertEquals(industryCheck.size(),industryList.size());
	}
	
	@Test
	public void saveSourceDomain() throws Exception {
		Mockito.when(sourceIndustryDao.create(Mockito.anyObject())).thenReturn(sourceIndustryOne);
		Mockito.when(sourceIndustryDao.findAll()).thenReturn(industryList);
		List<SourceIndustryDto> industryCheck =  sourceIndustryService.saveSourceIndustry(sourceIndustryDto);
		Assert.assertEquals(industryCheck.size(),industryList.size());
	}
	

}
