package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModelProperty;

public class UsersGroupDto implements Serializable {

	private static final long serialVersionUID = 957321153960166215L;

	@ApiModelProperty(value = "ID of the group")
	private Groups groupId;

	@ApiModelProperty(value = "ID of the user")
	private Users userId;

	@ApiModelProperty(value = "Group created by")
	private Integer createdBy;

	@ApiModelProperty(value = "Description of the group")
	private String description;

	@ApiModelProperty(value = "Name of the group")
	private String groupName;

	public UsersGroupDto() {
		super();
	}

	public UsersGroupDto(Groups groupId, Users userId) {
		super();
		this.groupId = groupId;
		this.userId = userId;
	}

	public UsersGroupDto(Users userId) {
		super();
		this.userId = userId;
	}

	public UsersGroupDto(Integer createdBy, String description) {
		super();
		this.createdBy = createdBy;
		this.description = description;
	}

	public UsersGroupDto(Groups groupId, String groupName) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
	}

	

	public Groups getGroupId() {
		return groupId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}

	public Users getUserId() {
		return userId;
	}

	public void setUserId(Users userId) {
		this.userId = userId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
