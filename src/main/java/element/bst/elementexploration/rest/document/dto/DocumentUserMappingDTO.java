package element.bst.elementexploration.rest.document.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.document.enumType.ReadWritePermissionEnum;


public class DocumentUserMappingDTO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Long userId;
    private String userName;
    private ReadWritePermissionEnum permissiontType;
    private Date sharedOn;
	
	/**
	 * @param userId
	 * @param userName
	 * @param permissiontType
	 * @param sharedOn
	 */
	public DocumentUserMappingDTO(Long userId, String userName,
		ReadWritePermissionEnum permissiontType, Date sharedOn) {
	    super();
	    this.userId = userId;
	    this.userName = userName;
	    this.permissiontType = permissiontType;
	    this.sharedOn = sharedOn;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
	    return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
	    this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
	    return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
	    this.userName = userName;
	}
	/**
	 * @return the permissiontType
	 */
	public ReadWritePermissionEnum getPermissiontType() {
	    return permissiontType;
	}
	/**
	 * @param permissiontType the permissiontType to set
	 */
	public void setPermissiontType(ReadWritePermissionEnum permissiontType) {
	    this.permissiontType = permissiontType;
	}
	/**
	 * @return the sharedOn
	 */
	public Date getSharedOn() {
	    return sharedOn;
	}
	/**
	 * @param sharedOn the sharedOn to set
	 */
	public void setSharedOn(Date sharedOn) {
	    this.sharedOn = sharedOn;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
