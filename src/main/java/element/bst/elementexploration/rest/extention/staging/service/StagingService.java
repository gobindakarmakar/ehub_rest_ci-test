package element.bst.elementexploration.rest.extention.staging.service;

import element.bst.elementexploration.rest.extention.staging.domain.OrgStaging;

public interface StagingService {

	String getOrgStaging(String status) throws Exception;

	String saveOrgStaging(OrgStaging orgStaging) throws Exception;
}
