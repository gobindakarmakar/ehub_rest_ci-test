package element.bst.elementexploration.test.extention.widgetreview.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.widgetreview.dao.ComplianceWidgetDao;
import element.bst.elementexploration.rest.extention.widgetreview.dao.WidgetReviewDao;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.extention.widgetreview.dto.WidgetReviewDto;
import element.bst.elementexploration.rest.extention.widgetreview.serviceImpl.WidgetReviewServiceImpl;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class WidgetReviewServiceTest {

	@Mock
	UsersDao userDao;
	
	@Mock
	ListItem listItem;

	@Mock
	WidgetReviewDao widgetReviewDao;

	@Mock
	ComplianceWidgetDao complianceWidgetDao;

	@Mock
	private ApplicationEventPublisher eventPublisher;
	
	@Mock
	private UsersService userService;

	@InjectMocks
	WidgetReviewServiceImpl widgetReviewServiceMock;

	private Users user = new Users("analyst", "Analyst", "Analyst", new Date(), listItem);

	List<WidgetReviewDto> widdgetReviewDtoList = new ArrayList<WidgetReviewDto>();

	WidgetReview widgetReview;
	ComplianceWidget widget = new ComplianceWidget("Company Information");

	@Before
	public void setupMock() {

		
		widget.setId(1L);
		widgetReview = new WidgetReview(true, new Date(), user, "test", widget);
		widgetReview.setId(1L);
		widdgetReviewDtoList.add(new WidgetReviewDto(1l, true, new Date(), "analyst", "testing", 123L, "company info"));
		widdgetReviewDtoList.add(new WidgetReviewDto(true, "testing", 124L));
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveWidgetReviewsUpdateTest() {
		Mockito.when(userDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(widgetReviewDao.find(Mockito.anyLong())).thenReturn(widgetReview);
		Mockito.when(widgetReviewDao.getWidgetWithEntityIdAndWidgetId(Mockito.anyString(), Mockito.anyLong()))
				.thenReturn(widgetReview);
		Mockito.when(complianceWidgetDao.find(Mockito.anyLong())).thenReturn(widget);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(widgetReviewDao).saveOrUpdate(Mockito.any(WidgetReview.class));
		Mockito.when(userService.prepareUserFromFirebase(Mockito.any(Users.class))).thenReturn(user);
		boolean status = widgetReviewServiceMock.saveWidgetReviews(widdgetReviewDtoList, 1L);
		assertEquals(true, status);
	}

	@Test
	public void saveWidgetReviewsNewTest() {
		Mockito.when(userDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(widgetReviewDao.find(Mockito.anyLong())).thenReturn(widgetReview);
		Mockito.when(widgetReviewDao.getWidgetWithEntityIdAndWidgetId(Mockito.anyString(), Mockito.anyLong()))
				.thenReturn(null);
		Mockito.when(complianceWidgetDao.find(Mockito.anyLong())).thenReturn(widget);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(widgetReviewDao).saveOrUpdate(Mockito.any(WidgetReview.class));
		Mockito.when(widgetReviewDao.create(Mockito.any(WidgetReview.class))).thenReturn(widgetReview);
		Mockito.when(userService.prepareUserFromFirebase(Mockito.any(Users.class))).thenReturn(user);
		boolean status = widgetReviewServiceMock.saveWidgetReviews(widdgetReviewDtoList, 1L);
		assertEquals(true, status);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = InsufficientDataException.class)
	public void saveWidgetReviews_WhenThrowInsufficientDataException_WidgetreviewNotFoundTest() {
		Mockito.when(userDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(userDao.find(Mockito.anyLong())).thenThrow(InsufficientDataException.class);
		widgetReviewServiceMock.saveWidgetReviews(widdgetReviewDtoList, 1L);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = InsufficientDataException.class)
	public void saveWidgetReviews_WhenThrowInsufficientDataException_WidgetNotFoundTest() {
		Mockito.when(userDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(widgetReviewDao.find(Mockito.anyLong())).thenReturn(widgetReview);
		Mockito.when(widgetReviewDao.getWidgetWithEntityIdAndWidgetId(Mockito.anyString(), Mockito.anyLong()))
				.thenReturn(null);
		Mockito.when(userDao.find(Mockito.anyLong())).thenThrow(InsufficientDataException.class);
		widgetReviewServiceMock.saveWidgetReviews(widdgetReviewDtoList, 1L);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = InsufficientDataException.class)
	public void saveWidgetReviews_WhenThrowInsufficientDataException_UserNotFoundTest() {
		Mockito.when(userDao.find(Mockito.anyLong())).thenThrow(InsufficientDataException.class);
		widgetReviewServiceMock.saveWidgetReviews(widdgetReviewDtoList, 1L);
	}

}
