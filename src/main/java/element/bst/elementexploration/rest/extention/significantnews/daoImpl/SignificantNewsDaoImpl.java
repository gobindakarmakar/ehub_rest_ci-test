package element.bst.elementexploration.rest.extention.significantnews.daoImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantNewsDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("significantNewsDao")
public class SignificantNewsDaoImpl extends GenericDaoImpl<SignificantNews, Long> implements SignificantNewsDao{
	
	public SignificantNewsDaoImpl() {
		super(SignificantNews.class);
	}

	@Override
	public SignificantNews getSignificantNews(String entityId, String entityName, String publishedDate, String title,String url,String newsClass) {
		SignificantNews significantNews = null;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("select sn from SignificantNews sn where sn.entityId = :entityId and sn.entityName = :entityName and sn.publishedDate = :publishedDate and sn.title = :title and sn.url = :url and sn.newsClass = :newsClass");			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("entityName", entityName);
			query.setParameter("publishedDate", publishedDate);
			query.setParameter("title", title);
			query.setParameter("url", url);
			query.setParameter("newsClass", newsClass);			
			significantNews = (SignificantNews) query.getSingleResult();
		}catch (Exception e) {
			return significantNews;
		}
		return significantNews;
	}

	@Override
	public int deleteSignificantNews(String entityId, String entityName, String publishedDate, String title,
			String url, String newsClass) {
		int result=0;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("delete from SignificantNews  where entityId = :entityId and entityName = :entityName and publishedDate = :publishedDate and title = :title and url = :url and newsClass = :newsClass");			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("entityName", entityName);
			query.setParameter("publishedDate", publishedDate);
			query.setParameter("title", title);
			query.setParameter("url", url);
			query.setParameter("newsClass", newsClass);			
			result = query.executeUpdate();			
		}catch (Exception e) {
			return result;
		}
		return result;
	}
	
	

}
