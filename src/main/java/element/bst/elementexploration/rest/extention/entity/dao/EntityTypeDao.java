package element.bst.elementexploration.rest.extention.entity.dao;

import element.bst.elementexploration.rest.extention.entity.domain.EntityType;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface EntityTypeDao extends GenericDao<EntityType, Long> {

}
