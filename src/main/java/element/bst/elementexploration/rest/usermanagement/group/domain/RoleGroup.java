package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@Entity
@Table(name = "bst_um_role_group")
public class RoleGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "roleGroupId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(value = "Role Group ID")
	private Long roleGroupId;

	@ApiModelProperty(value = "Role ID")
	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId")
	private Roles roleId;

	@ApiModelProperty(value = "GroupId")
	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "usergroupId", referencedColumnName = "id")
	private Groups groupId;

	@ApiModelProperty(value = "Created On")
	@Column(name = "createdOn")
	private Date createdOn;

	@ApiModelProperty(value = "Modified On")
	@Column(name = "modifiedOn")
	private Date modifiedOn;

	public Long getRoleGroupId() {
		return roleGroupId;
	}

	public void setRoleGroupId(Long roleGroupId) {
		this.roleGroupId = roleGroupId;
	}

	public Roles getRoleId() {
		return roleId;
	}

	public void setRoleId(Roles roleId) {
		this.roleId = roleId;
	}

	public Groups getGroupId() {
		return groupId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
