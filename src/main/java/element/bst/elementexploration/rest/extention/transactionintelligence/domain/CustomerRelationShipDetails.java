package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "CUSTOMER_RELATIONSHIP_DETAILS")
public class CustomerRelationShipDetails {

	private Long id;
	private String customerNumber;
	private String relatedCustomerNumber;
	private String relationShipDefinition;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "CUSTOMER_NUMBER")
	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	@Column(name = "RELATED_CUSTOMER_NUMBER")
	public String getRelatedCustomerNumber() {
		return relatedCustomerNumber;
	}

	public void setRelatedCustomerNumber(String relatedCustomerNumber) {
		this.relatedCustomerNumber = relatedCustomerNumber;
	}

	@Column(name = "RELATIONSHIP_DEFINITION")
	public String getRelationShipDefinition() {
		return relationShipDefinition;
	}

	public void setRelationShipDefinition(String relationShipDefinition) {
		this.relationShipDefinition = relationShipDefinition;
	}

}
