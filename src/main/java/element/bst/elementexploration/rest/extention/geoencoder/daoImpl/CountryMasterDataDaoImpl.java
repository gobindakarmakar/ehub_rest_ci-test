package element.bst.elementexploration.rest.extention.geoencoder.daoImpl;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.geoencoder.dao.CountryMasterDataDao;
import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("countryMasterDataDao")
public class CountryMasterDataDaoImpl extends GenericDaoImpl<CountryMasterData, Long> implements CountryMasterDataDao {

	public CountryMasterDataDaoImpl() {
		super(CountryMasterData.class);
	}

	@Override
	public CountryMasterData findCountry(String countryName) {
		CountryMasterData countryMasterData = null;
		try {
			countryMasterData = (CountryMasterData) this.getCurrentSession()
					.createQuery("From CountryMasterData c where c.name='" + countryName + "' OR  c.isoCode = '"
							+ countryName + "' ")
					.getSingleResult();
		} catch (NoResultException e) {
			return countryMasterData;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get Country Master Data. Reason : " + e.getMessage());
		}
		return countryMasterData;
	}

}
