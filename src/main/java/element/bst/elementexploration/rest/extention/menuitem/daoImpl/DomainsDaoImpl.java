package element.bst.elementexploration.rest.extention.menuitem.daoImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.DomainsDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("domainsDao")
@Transactional("transactionManager")
public class DomainsDaoImpl extends GenericDaoImpl<Domains, Long> implements DomainsDao {

	public DomainsDaoImpl() {
		super(Domains.class);
	}

}
