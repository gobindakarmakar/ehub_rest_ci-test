package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import org.json.JSONArray;
import org.json.JSONObject;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantnewsService;

public class SignificantSubsidiarieThread extends Thread {
	
	private org.json.simple.JSONObject object;
	
	private SignificantnewsService significantnewsService;

	public SignificantSubsidiarieThread(org.json.simple.JSONObject object,SignificantnewsService significantnewsService) {
		this.object=object;
		this.significantnewsService=significantnewsService;
	}
	
	
	@Override
	public void run() {
		getSignificantSubsidiaries(object,significantnewsService);
	}

	public void getSignificantSubsidiaries(org.json.simple.JSONObject object,SignificantnewsService significantnewsService){

	org.json.simple.JSONArray subsidiaries=(org.json.simple.JSONArray) object.get("subsidiaries");
	if (subsidiaries != null) {
		JSONArray subsidiaries_array=new JSONArray();
		for (int j = 0; j < subsidiaries.size(); j++) {
			JSONObject subsidiarie_json=new JSONObject();
			org.json.simple.JSONObject subsidiaries_temp = (org.json.simple.JSONObject) subsidiaries
					.get(j);
			org.json.simple.JSONArray profileResponseDto = (org.json.simple.JSONArray) subsidiaries_temp
					.get("profileResponseDto");
			String subsidiaries_identifier = (String) subsidiaries_temp.get("identifier");
			String keyword = (String) subsidiaries_temp.get("keyword");
			subsidiarie_json.put("identifier", subsidiaries_identifier);
			subsidiarie_json.put("keyword", keyword);
			JSONArray profileResponseDto_arry=new JSONArray();
			for (int l = 0; l < profileResponseDto.size(); l++) {
				JSONObject profileResponseDto_json_temp=new JSONObject();
				org.json.simple.JSONObject profileResponseDto_json = (org.json.simple.JSONObject) profileResponseDto
						.get(l);
				String basic_string=(String) profileResponseDto_json.get("basic");
				//JSONObject basic=new JSONObject(basic_string);
				profileResponseDto_json_temp.put("basic", basic_string);
				profileResponseDto_json_temp.put("watchlist", profileResponseDto_json.get("watchlist"));
				String news = (String) profileResponseDto_json.get("news");
				JSONArray news_array = new JSONArray(news);
				JSONArray news_array_temp=new JSONArray();
				for (int m = 0; m < news_array.length(); m++) {
					JSONObject temp_news = news_array
							.getJSONObject(m);
					String classType=null;
					String published=null;
					String title=null;
					String url=null;
					if(temp_news.get("class")!=null)
						 classType=temp_news.get("class").toString();
					if(temp_news.get("published")!=null)
						published=temp_news.get("published").toString();
					if(temp_news.get("title")!=null)
						title=temp_news.get("title").toString();
					if(temp_news.get("url")!=null)
						url=temp_news.get("url").toString();
					SignificantNews significantNews = significantnewsService.getSignificantNews(
							subsidiaries_identifier, keyword, published, title, url, classType);
					if (significantNews != null) {
						temp_news.put("isSignificant", true);
					}
					news_array_temp.put(temp_news);
				}
				profileResponseDto_json_temp.put("news", news_array_temp.toString());
				profileResponseDto_arry.put(profileResponseDto_json_temp);

			}
			subsidiarie_json.put("profileResponseDto", profileResponseDto_arry);
			subsidiaries_array.put(subsidiarie_json);

		}
		subsidiaries_array.getJSONObject(subsidiaries_array.length()-1).put("flag", "done");
		object.put("subsidiaries", subsidiaries_array);

	}
	}

}
