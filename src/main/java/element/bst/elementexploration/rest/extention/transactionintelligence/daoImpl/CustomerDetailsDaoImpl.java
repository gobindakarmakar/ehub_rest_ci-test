package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("customerDetailsDao")
public class CustomerDetailsDaoImpl extends GenericDaoImpl<CustomerDetails, Long> implements CustomerDetailsDao {

	public CustomerDetailsDaoImpl() {
		super(CustomerDetails.class);
	}
	
	@Autowired
	CustomerDetailsDao customerDetailsDao;
	
	
	@Override
	public CustomerDetails fetchCustomerDetails(String customerNumber) throws NoResultException {
		String name = customerNumber;
		CustomerDetails customerDetails = (CustomerDetails) this.getCurrentSession()
				.createQuery("from CustomerDetails where customerNumber = '" + name + "'").getSingleResult();
		//CustomerDetails customerDetailsToSend = null;
		/*if (!customerDetails.isEmpty()) {
			customerDetailsToSend = this.getCurrentSession().load(CustomerDetails.class,
					customerDetails.get(0).getId());
		}*/
		return customerDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerDetails> fetchAllCustomers() {
		List<CustomerDetails> customerDetails = this.getCurrentSession()
				.createQuery("from CustomerDetails where rowStatus=0").getResultList();
		return customerDetails;
	}

	@Override
	public List<CustomerDetails> fetchMulCustomerDetails(String entityId) {
		String name = entityId;
		@SuppressWarnings("unchecked")
		List<CustomerDetails> customerDetails = (List<CustomerDetails>) this.getCurrentSession()
				.createQuery("from CustomerDetails where customerNumber = '" + name + "'").getResultList();

		return customerDetails;
	}

	@Override
	public CustomerDetails getCustomerDetails(String customerNumber, String locations) throws NoResultException {
		CustomerDetails customerDetails = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select cd from CustomerDetails cd where cd.customerNumber=:customerNumber and cd.rowStatus=:value");
			Query<?> query = getCurrentSession().createQuery(hql.toString());
			query.setParameter("customerNumber", customerNumber);
			query.setParameter("value", false);
			customerDetails = (CustomerDetails) query.getSingleResult();
		} catch (NoResultException e) {
			return customerDetails;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return customerDetails;
	}

	@Override
	public List<CustomerDetails> findByCustomerNumberInList(List<String> customerIds) {
		List<CustomerDetails> list = new ArrayList<>();
		if (!customerIds.isEmpty() && customerIds != null) {
			try {
				CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CustomerDetails> query = builder.createQuery(CustomerDetails.class);
				Root<CustomerDetails> customer = query.from(CustomerDetails.class);
				query.select(customer);
				query.distinct(true);
				query.where(customer.get("customerNumber").in(customerIds));
				list = (ArrayList<CustomerDetails>) this.getCurrentSession().createQuery(query).getResultList();
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				throw new FailedToExecuteQueryException("Failed to fetch accounts. Reason : " + e.getMessage());
			}
			return list;
		} else {
			return list;

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerDetails> findByCustomerIdInList(Set<Long> customerIds) {
		List<CustomerDetails> list = new ArrayList<>();
		if (customerIds.isEmpty())
			customerIds = null;
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("from CustomerDetails cd where cd.id IN (:customerIds)");
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setParameter("customerIds", customerIds);
			list = (ArrayList<CustomerDetails>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch accounts. Reason : " + e.getMessage());
		}
		return list;
	}
	
	@Override
	public List<CustomerDetails> findCorporateCustomers() {
		@SuppressWarnings("unchecked")
		List<CustomerDetails> customerDetails = (List<CustomerDetails>) this.getCurrentSession().createQuery("from CustomerDetails where customerType ='CORP'").getResultList();
		return customerDetails;
	}

	@Override
	public CustomerDto customerInfo(Long customerId) {
		CustomerDto customerDto=new CustomerDto();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select new element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto(");
			queryBuilder.append("cd.displayName,cd.dateOfBirth,cd.dateOfIncorporation,ca.city,c.country,ca.addressLine,cd.customerType,cd.estimatedTurnoverAmount,cd.industry) ");
			queryBuilder.append("from CustomerDetails cd ,CustomerAddress ca,Country c"
					+ " where cd.id=ca.customerDetails.id and c.iso2Code=cd.residentCountry and cd.id =:customerId ");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("customerId", customerId);
			customerDto = (CustomerDto) query.getSingleResult();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get CustomerDetails. Reason : " + e.getMessage());
		}
		return customerDto;
	}


	
}
