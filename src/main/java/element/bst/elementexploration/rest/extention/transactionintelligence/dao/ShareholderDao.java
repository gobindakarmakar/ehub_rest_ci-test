package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Shareholders;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Paul Jalagari
 * 
 */
public interface ShareholderDao extends GenericDao<Shareholders, Long> {
	/*CorporateStructureDto getCorporateStructure(String fromDate, String toDate);*/
	CorporateStructureDto getCorporateStructure(String fromDate, String toDate,FilterDto filterDto);

	/*List<CorporateStructureDto> getCorporateStructureAggregates(String fromDate, String toDate, List<String> scenarios);*/
	List<CorporateStructureDto> getCorporateStructureAggregates(String fromDate, String toDate, FilterDto filterDto,String type);

	List<CorporateStructureDto> getCorporateStructuteByType(String fromDate, String toDate, String type,
			List<String> scenarios);

	List<CorporateStructureDto> getAlertEntitiesGeography(String fromDate, String toDate, String string);

	List<CorporateStructureDto> getAlertEntitiesFilter(String fromDate, String toDate, String type,
			List<String> countries, String groupBy);

	List<CorporateStructureDto> getAlertEntitiesFilter(String fromDate, String toDate, String string,
			List<String> scenarios, String filter, String country, String groupBy);

	List<CorporateStructureDto> getAlertEntitiesFilter(String fromDate, String toDate, String type,
			List<String> scenarios, String oldfilter, String country, String newFilter, Long customerId,
			String groupBy);

	void insertShareHolders();

	public List<String> getCorporateByType(String fromDate, String toDate, String type, List<String> scenarios);

	/*List<CorporateStructureDto> getCorporateStructuteByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);*/
	List<CorporateStructureDto> getCorporateStructuteByScenarios(String fromDate, String toDate,FilterDto filterDto,String type);

	/*CorporateStructureDto getAlertEntitiesGeography(String fromDate, String toDate);*/
	CorporateStructureDto getAlertEntitiesGeography(String fromDate, String toDate,FilterDto filterDto);

	List<CorporateStructureDto> getCountryAggregates(String fromDate, String toDate, List<String> scenarios);

	List<String> getCountryByType(String fromDate, String toDate, String type, List<String> scenarios);

	List<CorporateStructureDto> getAlertEntitiesBusinessType(String fromDate, String toDate);

	List<CorporateStructureDto> getBusinessAggregates(String fromDate, String toDate, List<String> scenario);

	List<String> getBusinessByType(String fromDate, String toDate, String type, List<String> scenarios);

	List<CorporateStructureDto> getCustomerByCountryScenario(String fromDate, String toDate, String country,
			List<String> scenarios);

	List<CorporateStructureDto> getCustomerByBusinessScenario(String fromDate, String toDate, String industry,
			List<String> scenarios);

	List<CorporateStructureDto> getShareholderCorporateStructure(String fromDate, String toDate, String groupBy);

	List<CorporateStructureDto> getShareholderCountry(String fromDate, String toDate, String groupBy);

	/*List<CorporateStructureDto> getShareholderAggregates(String fromDate, String toDate, List<String> scenarios);*/
	List<CorporateStructureDto> getShareholderAggregates(String fromDate, String toDate, FilterDto filterDto);

	List<CorporateStructureDto> getCustomerByShareholderScenario(String fromDate, String toDate, String industry,
			List<String> scenarios);

	List<String> getShareholderByType(String fromDate, String toDate, String type, List<String> scenarios);

	/*List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate, boolean b, String filter,
			Integer pageNumber, Integer recordsPerPage);*/
	List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate, boolean b, String filter,
			Integer pageNumber, Integer recordsPerPage,FilterDto filterDto);

	List<CorporateStructureDto> getViewAll(String fromDate, String toDate, boolean isTransaction, Integer pageNumber,
			Integer recordsPerPage, String queryforCondition);

	long getViewAllCorporateStructureCount(String fromDate, String toDate, boolean isTransaction);

	long getViewAllCount(String fromDate, String toDate, boolean isTransaction, String queryforCondition);

	/*List<CorporateStructureDto> getAssociatedAlerts(String fromDate, String toDate, String type);*/
	List<CorporateStructureDto> getAssociatedAlerts(String fromDate, String toDate, FilterDto filterDto);

	List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate, boolean b, String filter,
			String queryforCondition, Integer pageNumber, Integer recordsPerPage);

	/*List<CorporateStructureDto> getTopFiveAlerts(String fromDate, String toDate, String type);*/
	List<CorporateStructureDto> getTopFiveAlerts(String fromDate, String toDate, String type);

	/*List<CorporateStructureDto> getGeographyAggregates(String fromDate, String toDate, List<String> scenarios);*/
	List<CorporateStructureDto> getGeographyAggregates(String fromDate, String toDate, FilterDto filterDto);

	/*CorporateStructureDto getCorporateStructure(String fromDate, String toDate, String type);*/
	/*CorporateStructureDto getCorporateStructure(String fromDate, String toDate, FilterDto filterDto);*///used getCorporateStructure

	List<String> getCorporateStructureByType(String fromDate, String toDate, String type, List<String> scenarios);
	public List<CorporateStructureDto> getGroupByScenario(String fromDate, String toDate, FilterDto filterDto);

	List<CorporateStructureDto> getAggregates(String fromDate, String toDate, String type);

	List<CorporateStructureDto> getGeographyByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);

	CorporateStructureDto getShareholder(String fromDate, String toDate, String type);

	List<CorporateStructureDto> getShareholderAggregate(String fromDate, String toDate, String type);

	CorporateStructureDto getGeography(String fromDate, String toDate, String type);

	List<CorporateStructureDto> getGeographyAggregate(String fromDate, String toDate, String type);

	List<String> getGeographyByType(String fromDate, String toDate, String type, List<String> scenarios);

	List<CorporateStructureDto> getShareholderByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);

	CorporateStructureDto getShareHolder(String fromDate, String toDate, FilterDto filterDto);

	CorporateStructureDto getGeographyAlerts(String fromDate, String toDate, FilterDto filterDto);
	
	

}
