package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Case comment")
public class CaseCommentNotificationDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("Comment")
	private String comment;
	
	@ApiModelProperty("Notification date")
	private Date notificationDate;
		
	public CaseCommentNotificationDto() {
		super();		
	}
	
	public CaseCommentNotificationDto(String comment, Date notificationDate) {
		super();
		this.comment = comment;
		this.notificationDate = notificationDate;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public Date getNotificationDate() {
		return notificationDate;
	}
	
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
