'use strict';
activitiApp.controller('DataModalController', dataModalController);

		dataModalController.$inject = [
			'$scope',
			'$rootScope',
			'data',
			'$modalInstance',
			'$timeout'
		];
	    function dataModalController(
	    		$scope,
	    		$rootScope,
	    		data,
	    		$modalInstance,
	    		$timeout){
	    	
	    	/*
	         * @purpose:Set data in modal
	         * @created: 9 Jan 2018
	         * @author: varsha
	        */
	    	$timeout(function(){
	    		  $("#mainData").html(data.txt)	
	    		  console.log(data)
	    	},0)
	    	if(data.name && data.type){
	    		$scope.header=data.name+'('+data.type+')'
	    	}
	    	else{
	    		$scope.header="Additional Information"
	    	}
	    	$scope.closeDataPopUp=function(){
	    		$modalInstance.close();
	    		console.log("closeDataPopUp");
	    	}
          
		
	    }
	    	
	    	