package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebsiteClass implements Serializable {
private static final long serialVersionUID = 1L;
	@JsonProperty("class")
	private String websiteClassName;
	
	@JsonProperty("source_url")
	private List<String> sourceUrls;

	

	public String getWebsiteClassName() {
		return websiteClassName;
	}

	public void setWebsiteClassName(String websiteClassName) {
		this.websiteClassName = websiteClassName;
	}

	public List<String> getSourceUrls() {
		return sourceUrls;
	}

	public void setSourceUrls(List<String> sourceUrls) {
		this.sourceUrls = sourceUrls;
	}
	
	
	
	

}
