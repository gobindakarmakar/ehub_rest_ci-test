package element.bst.elementexploration.rest.usermanagement.security.daoImpl;

import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.ElementSecurityDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserToken;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Amit Patel
 *
 */
@Repository("elementSecurityDao")
public class ElementSecurityDaoImpl extends GenericDaoImpl<UserToken, Long> implements ElementSecurityDao{

	public ElementSecurityDaoImpl(){
        super(UserToken.class);
    }

	@Override
	public boolean isValidToken(String token) {
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Long> query = builder.createQuery(Long.class);
			Root<UserToken> tokenObject = query.from(UserToken.class);
			query.select(builder.count(tokenObject));
			query.where(builder.and(builder.equal(tokenObject.get("token"), token), builder.greaterThan(tokenObject.get("expireOn"), new Date())));
			return this.getCurrentSession().createQuery(query).getSingleResult() > 0;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Token validation", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not validate token - " + e.getMessage());
		}
	}

	@Override
	public UserToken getUserToken(String token) {
		UserToken userToken = null;
		try{
			String hql="from UserToken where token = :token";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("token", token);
			userToken = (UserToken) query.getSingleResult();
			if(userToken!=null)
				Hibernate.initialize(userToken.getUser());
			return userToken;
		} catch (NoResultException e){
			return null;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Fetch token object ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not fetch token object - "+e.getMessage());
		}
	}

	@Override
	public UserToken isTokenExist(long userId) {
		try{
			String hql="from UserToken where user.userId = :userId";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("userId", userId);
			return (UserToken) query.getSingleResult();
		} catch (NoResultException e){
			return null;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Token Exist ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not validate token - "+e.getMessage());
		}
	}

	@Override
	public boolean isTokenExist(String token) {
		try{
			String hql="select count(*) from UserToken where token = :token";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("token", token);
			return (((Number)query.getSingleResult()).longValue()) >0;
		}catch (NoResultException e){
			return false;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Token Exist ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not validate token - "+e.getMessage());
		}
	}

	@Override
	public UsersToken getUsersToken(String token) {
		UsersToken userToken = null;
		try{
			String hql="from UsersToken where token = :token";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("token", token);
			userToken = (UsersToken) query.getSingleResult();
			if(userToken!=null)
				Hibernate.initialize(userToken.getUserId());
			return userToken;
		} catch (NoResultException e){
			return null;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Fetch token object ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not fetch token object - "+e.getMessage());
		}
	}

}
