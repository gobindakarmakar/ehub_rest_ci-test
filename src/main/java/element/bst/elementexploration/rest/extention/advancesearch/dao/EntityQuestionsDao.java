package element.bst.elementexploration.rest.extention.advancesearch.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityQuestions;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author rambabu
 *
 */
public interface EntityQuestionsDao extends GenericDao<EntityQuestions, Long> {

	public List<EntityQuestions> getEntityQuestionByEntityId(String entityId,Long surveyId);

	public EntityQuestions getEntityQuestionByNameAndEntityId(String entityId, String questionDivId,Long userId,Long surveyId);
	
	public List<EntityQuestions> getEntityQuestionByEntityIdAndUserId(String entityId,Long userId,Long surveyId);

	public void deleteExistingEntityAnswersByUser(String entityId, Long surveyId, Long userId) throws Exception;

}
