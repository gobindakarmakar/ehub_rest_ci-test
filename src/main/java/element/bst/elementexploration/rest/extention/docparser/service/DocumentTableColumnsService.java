package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableColumns;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface DocumentTableColumnsService extends GenericService<DocumentTableColumns, Long>{

	
}
