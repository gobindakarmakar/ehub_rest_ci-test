package element.bst.elementexploration.rest.settings.listManagement.daoImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.listManagement.dao.IconDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.Icon;

@Repository("iconDao")
@Transactional("transactionManager")
public class IconDaoImpl extends GenericDaoImpl<Icon, Long> implements 
IconDao {

	public IconDaoImpl() {
		super(Icon.class);
	}
}
