package element.bst.elementexploration.rest.extention.widgetreview.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.widgetreview.domain.WidgetReview;
import element.bst.elementexploration.rest.extention.widgetreview.dto.WidgetReviewDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author rambabu
 *
 */
public interface WidgetReviewService extends GenericService<WidgetReview, Long> {

	boolean saveWidgetReviews(List<WidgetReviewDto> widdgetReviewList, Long userId);

	List<WidgetReviewDto> getWidgetReviewsByEntityId(String entityId);

}
