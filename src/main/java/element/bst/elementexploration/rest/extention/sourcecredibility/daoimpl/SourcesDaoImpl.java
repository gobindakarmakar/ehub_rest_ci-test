package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourcesDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceFilterDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("sourcesDao")
public class SourcesDaoImpl extends GenericDaoImpl<Sources, Long> implements SourcesDao {

	public SourcesDaoImpl() {
		super(Sources.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SourcesDto> getAllSources() {
		List<SourcesDto> sourcesDtoList=null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select new element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto(");
			hql.append(" s.sourceId,s.sourceName,s.sourceUrl, s.category) from Sources s join SourcesHideStatus sh on s.sourceId=sh.sourceId where sh.visible=true ");
			@SuppressWarnings("rawtypes")
			Query query = this.getCurrentSession().createQuery(hql.toString());
			sourcesDtoList = query.getResultList();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return sourcesDtoList;
	}


	@Override
	public List<Sources> getSources(Integer pageNumber, Integer recordsPerPage, Boolean visible, Long classifcationId,
			String orderBy, String orderIn, List<SourceFilterDto> sourceFilterDto, Long subClassifcationId) {
		List<Sources> sources = new ArrayList<Sources>();
		try {

			boolean flagClassiifcation = true;
			boolean flagJurisdiction = true;
			boolean flagIndustry = true;
			boolean flagMedia = true;
			boolean flagDomain = true;
			boolean flagCategory = true;
			boolean flag = true;
			Join<Sources, SourceJurisdiction> sourceJurisdiction = null;
			Join<Sources, SourceMedia> sourceMedia = null;
			Join<Sources, SourceIndustry> sourceIndustry = null;
			Join<Sources, SourceDomain> sourceDomain = null;
			List<Predicate> predicatesFinalList = new ArrayList<Predicate>();
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Sources> query = builder.createQuery(Sources.class);
			Root<Sources> source = query.from(Sources.class);
			Join<Sources, Classifications> classifications = source.join("classifications");
			Join<Sources, SourceCredibility> sourceCredibility = source.join("sourceCredibilityList");
			query.select(source);
			if ((sourceFilterDto == null || sourceFilterDto.isEmpty()) && classifcationId != null)
				query.where(builder.equal(classifications.get("classificationId"), classifcationId));
			List<Predicate> predicates = new ArrayList<Predicate>();
			if (sourceFilterDto != null) {
				if (!sourceFilterDto.isEmpty()) {
					for (SourceFilterDto entry : sourceFilterDto) {
						if (entry.getColumnName().equals("visible") && visible != null && flag) {
							Root<SourcesHideStatus> sourcesHideStatus = query.from(SourcesHideStatus.class);
							Predicate condition = builder.and(
									builder.and(
											builder.equal(source.get("sourceId"), sourcesHideStatus.get("sourceId"))),
									builder.equal(sourcesHideStatus.get("visible"), visible));
							predicates.add(condition);
							flag = false;
							
							if(classifcationId!=null) {

								Predicate condition1 = builder.and(
										builder.and(
												builder.equal(sourcesHideStatus.get("classificationId"), classifcationId)));
								predicates.add(condition1);
							}
						}
						if (flagClassiifcation && classifcationId!=null) {
							predicates.add(builder.equal(classifications.get("classificationId"), classifcationId));
							flagClassiifcation = false;
						}

						if (entry.getColumnName().contains("source")) {
							if(entry.getColumnName().contains("sourcedisplayname") || entry.getColumnName().contains("sourceName")) {
								predicates.add(builder.or(
										builder.like(source.get("sourceDisplayName"), "%" + entry.getFilters().get(0) + "%"),
										builder.like(source.get("sourceName"), "%" + entry.getFilters().get(0) + "%")));
							}else {
								predicates.add(builder.like(source.get(findColumnName(entry.getColumnName())),
										"%" + entry.getFilters().get(0) + "%"));

							}
						}
						if (entry.getColumnName().equals("category")){
							predicates.add(builder.like(source.get(findColumnName(entry.getColumnName())),
									"%" + entry.getFilters().get(0) + "%"));
						}
						if (entry.getColumnName().equals("domain")) {
							sourceDomain = source.join("sourceDomain");
							In<String> inConditions = builder.in(sourceDomain.get("domainName"));
							entry.getFilters().forEach(i -> inConditions.value(i));
							predicates.add(inConditions);
							flagDomain = false;
						}
						if (entry.getColumnName().equals("industry")) {
							sourceIndustry = source.join("sourceIndustry");
							In<String> inConditions = builder.in(sourceIndustry.get("industryName"));
							entry.getFilters().forEach(i -> inConditions.value(i));
							predicates.add(inConditions);
							flagIndustry = false;
						}
						if (entry.getColumnName().equals("jurisdiction")) {
							sourceJurisdiction = source.join("sourceJurisdiction");
							In<String> inConditions = builder.in(sourceJurisdiction.get("jurisdictionName"));
							entry.getFilters().forEach(i -> inConditions.value(i));
							predicates.add(inConditions);
							flagJurisdiction = false;
						}
						if (entry.getColumnName().equals("media")) {
							sourceMedia = source.join("sourceMedia");
							In<String> inConditions = builder.in(sourceMedia.get("mediaName"));
							entry.getFilters().forEach(i -> inConditions.value(i));
							predicates.add(inConditions);
							flagMedia = false;
						}
					}
				}
			}
			// for sub classiifcations
			List<Predicate> predicatesOr = new ArrayList<Predicate>();
			if (sourceFilterDto != null) {
				if (!sourceFilterDto.isEmpty()) {
					for (SourceFilterDto entry : sourceFilterDto) {
						if (entry.getColumnName().contains("subclassification") && entry.getId() != null) {
							In<CredibilityEnums> inConditions = builder.in(sourceCredibility.get("credibility"));
							entry.getFilters().forEach(i -> inConditions.value(CredibilityEnums.valueOf(i)));
							predicatesOr
							.add(builder.and(
									builder.and(builder.equal(sourceCredibility.get("subClassifications")
											.get("subClassificationId"), entry.getId()), inConditions),
									builder.isNull(sourceCredibility.get("dataAttributes"))));
						}
					}
				}
			}
			if (!predicatesOr.isEmpty()) {
				Predicate pFinal = builder.and(builder.and(predicates.toArray(new Predicate[predicates.size()])),
						builder.or(predicatesOr.toArray(new Predicate[predicatesOr.size()])));
				predicatesFinalList.add(pFinal);
			} else {
				if (sourceFilterDto != null) {
					if (!sourceFilterDto.isEmpty()) {
						Predicate pFinal = builder
								.and(builder.and(predicates.toArray(new Predicate[predicates.size()])));
						predicatesFinalList.add(pFinal);
					}
				}
			}
			Predicate predicateSort = null;
			if ((!StringUtils.isEmpty(orderIn)) && (!StringUtils.isEmpty(orderBy))) {
				if (orderBy.equals("credibility") && "asc".equalsIgnoreCase(orderIn) && subClassifcationId != null) {
					predicateSort = builder.equal(
							sourceCredibility.get("subClassifications").get("subClassificationId"), subClassifcationId);
					predicatesFinalList.add(predicateSort);
					query.orderBy(builder.asc(sourceCredibility.get("credibility")));
				}
				if (orderBy.equals("credibility") && "desc".equalsIgnoreCase(orderIn) && subClassifcationId != null) {
					predicateSort = builder.equal(
							sourceCredibility.get("subClassifications").get("subClassificationId"), subClassifcationId);
					query.orderBy(builder.desc(sourceCredibility.get("credibility")));
				}
				if ((orderBy.equals("sourcedisplayname") || orderBy.equals("sourceurl"))
						&& "dsc".equalsIgnoreCase(orderIn))
					query.orderBy(builder.desc(source.get(findColumnName(orderBy))));
				if ((orderBy.equals("sourcedisplayname") || orderBy.equals("sourceurl"))
						&& "asc".equalsIgnoreCase(orderIn))
					query.orderBy(builder.asc(source.get(findColumnName(orderBy))));
				if ((orderBy.equals("category")) && "dsc".equalsIgnoreCase(orderIn)){
					query.orderBy(builder.desc(source.get(findColumnName(orderBy))));
				}
				if ((orderBy.equals("category")) && "asc".equalsIgnoreCase(orderIn)){
					query.orderBy(builder.asc(source.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("domain") && "asc".equalsIgnoreCase(orderIn)) {
					if (flagDomain)
						sourceDomain = source.join("sourceDomain");
					query.orderBy(builder.asc(sourceDomain.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("domain") && "dsc".equalsIgnoreCase(orderIn)) {
					if (flagDomain)
						sourceDomain = source.join("sourceDomain");
					query.orderBy(builder.desc(sourceDomain.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("industry") && "asc".equalsIgnoreCase(orderIn)) {
					if (flagIndustry)
						sourceIndustry = source.join("sourceIndustry");
					query.orderBy(builder.asc(sourceIndustry.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("industry") && "dsc".equalsIgnoreCase(orderIn)) {
					if (flagIndustry)
						sourceIndustry = source.join("sourceIndustry");
					query.orderBy(builder.desc(sourceIndustry.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("jurisdiction") && "asc".equalsIgnoreCase(orderIn)) {
					if (flagJurisdiction)
						sourceJurisdiction = source.join("sourceJurisdiction");
					query.orderBy(builder.asc(sourceJurisdiction.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("jurisdiction") && "dsc".equalsIgnoreCase(orderIn)) {
					if (flagJurisdiction)
						sourceJurisdiction = source.join("sourceJurisdiction");
					query.orderBy(builder.desc(sourceJurisdiction.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("media") && "asc".equalsIgnoreCase(orderIn)) {
					if (flagMedia)
						sourceMedia = source.join("sourceMedia");
					query.orderBy(builder.asc(sourceMedia.get(findColumnName(orderBy))));
				}
				if (orderBy.equals("media") && "dsc".equalsIgnoreCase(orderIn)) {
					if (flagMedia)
						sourceMedia = source.join("sourceMedia");
					query.orderBy(builder.desc(sourceMedia.get(findColumnName(orderBy))));
				}
				
			} else {
				query.orderBy(builder.desc(sourceCredibility.get("credibility")));
			}

			if (!predicatesFinalList.isEmpty())
				query.where(predicatesFinalList.toArray(new Predicate[predicatesFinalList.size()]));

			query.distinct(true);
			TypedQuery<Sources> typedQuery = this.getCurrentSession().createQuery(query);
			if (pageNumber != null && recordsPerPage != null) {
				typedQuery.setFirstResult((pageNumber - 1) * (recordsPerPage));
				typedQuery.setMaxResults(recordsPerPage);
			}
			sources = (ArrayList<Sources>) typedQuery.getResultList();
		} catch (HibernateException he) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "HibernateException", he, SourcesDaoImpl.class);
			throw new FailedToExecuteQueryException();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesDaoImpl.class);
			e.printStackTrace();
			throw new FailedToExecuteQueryException();
		}
		return sources;
	}

	private String findColumnName(String sourceColumn) {
		if (sourceColumn == null)
			return "sourceName";

		switch (sourceColumn.toLowerCase()) {
		case "sourcename":
			return "sourceName";
		case "sourcedisplayname":
			return "sourceDisplayName";
		case "sourceurl":
			return "sourceUrl";
		case "createddate":
			return "sourceCreatedDate";
		case "industry":
			return "industryName";
		case "domain":
			return "domainName";
		case "jurisdiction":
			return "jurisdictionName";
		case "media":
			return "mediaName";
		case "credibility":
			return "credibility";
		case "category":
			return "category";
		default:
			return "sourceName";
		}
	}

	@Deprecated
	@Override
	public boolean checkSourceExist(String identifier, String url) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from Sources s where s.sourceUrl =:link and s.entityId =:entityId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("link", url);
			query.setParameter("entityId", identifier);
			Sources sources = (Sources) query.getSingleResult();
			if (sources != null)
				return false;
		} catch (NoResultException ne) {
			return true;
		} catch (Exception e) {
			return false;
			/*ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesDaoImpl.class);
			throw new FailedToExecuteQueryException();*/
		}
		return false;
	}
	
	@Override
	public boolean checkSourceExistWithSourceNameAndEntityId(String identifier, String sourceName) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from Sources s where s.sourceName =:sourceName and s.entityId =:entityId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("sourceName", sourceName);
			query.setParameter("entityId", identifier);
			Sources sources = (Sources) query.getSingleResult();
			if (sources != null)
				return false;
		} catch (NoResultException ne) {
			return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	@Override
	public boolean checkSourceName(String sourceName, boolean isIndex, Long classificationId) {
		try {
			StringBuilder hql = new StringBuilder();
			if (!StringUtils.isEmpty(sourceName)) {
				if (isIndex)
					hql.append(
							"select s from Sources s join s.classifications c where s.sourceName =:sourceName OR s.sourceDisplayName =:sourceName and c.classificationId =:classificationId");
				else
					hql.append(
							"select s from Sources s join s.classifications c where s.sourceName =:sourceName and c.classificationId =:classificationId");
				Query<?> query = this.getCurrentSession().createQuery(hql.toString());
				query.setParameter("sourceName", sourceName);
				query.setParameter("classificationId", classificationId);
				Sources sources = (Sources) query.getSingleResult();
				if (sources != null)
					return true;
			} else
				throw new Exception(ElementConstants.UPDATE_FAILED);
		} catch (NoResultException ne) {
			return false;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	@Deprecated
	@Override
	public boolean checkSourceLink(String urlFrom, boolean isIndex, Long classificationId) {
		try {
			StringBuilder hql = new StringBuilder();
			String url = findHost(urlFrom);
			if (!StringUtils.isEmpty(url)) {
				if (isIndex)
					hql.append(
							"select s from Sources s join s.classifications c where s.sourceName =:link OR s.sourceDisplayName =:link and c.classificationId =:classificationId");
				else
					hql.append(
							"select s from Sources s join s.classifications c where s.sourceUrl =:link and c.classificationId =:classificationId");
				Query<?> query = this.getCurrentSession().createQuery(hql.toString());
				query.setParameter("link", url);
				query.setParameter("classificationId", classificationId);
				Sources sources = (Sources) query.getSingleResult();
				if (sources != null)
					return true;
			} else
				throw new Exception(ElementConstants.UPDATE_FAILED);
		} catch (NoResultException ne) {
			return false;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	private String findHost(String url) throws MalformedURLException {
		if (url.contains("http://")) {
			URL urlToSent = new URL(url);
			if (urlToSent.getHost().contains("www."))
				return urlToSent.getHost().replace("www.", "");
			else
				return urlToSent.getHost();
		} else if (url.contains("www.")) {
			return url.replace("www.", "");
		} else {
			return url;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sources> fetchClassifctaionSources(String classiifcationName) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select a from Sources a join a.classifications c  where c.classifcationName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", classiifcationName);
			List<Sources> sourcesList = (List<Sources>) query.getResultList();
			if (sourcesList != null)
			{
				sourcesList.get(0).getClassifications().get(0).getClassificationId();
				return sourcesList;
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional("transactionManager")
	public String getSourceCategoryBySourceName(String source) {
		try {
			StringBuilder hql  = new StringBuilder();
			hql.append("from Sources s where s.sourceName =:source");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("source", source);
			List<Sources> sources =  (List<Sources>) query.getResultList();
			if(!sources.isEmpty()) {
				return sources.get(0).getCategory();
			}
		}catch(Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return null;
	}

}
