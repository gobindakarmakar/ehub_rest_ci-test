package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDetailsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface CustomerDetailsService extends GenericService<CustomerDetails, Long> {

	Boolean uploadCustomerDetailsFile(MultipartFile multipartFile) throws IOException, ParseException;

	Boolean uploadCustomerAddressFile(MultipartFile multipartFile) throws IOException,ParseException;
	
	Boolean addCorporateStructureAndIndustry();

	CustomerDto customerInfo(Long customerId);
	
	Boolean updateCustomer(MultipartFile multipartFile) throws IOException,ParseException;

	Boolean updateEstimateAverageAmt();
	
	void updateCustomerDetails(CustomerDetailsDto customerDetailsDto) throws Exception;
	
    CustomerDetails findCustomerById(long customerId);
	
}
