package element.bst.elementexploration.rest.usermanagement.security.domain;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Amit Patel
 *
 */
@ApiModel("user information")
public class UserInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Email ID of the user", required = true)
	private String email;

	@ApiModelProperty(value = "Token generated for the user", required = true)
	private String token;

	@ApiModelProperty(value = "ID of the user", required = true)
	private Long userId;

	@ApiModelProperty(value = "Full name of the user", required = true)
	private String fullName;

	@ApiModelProperty(value = "Last login date of the user", required = true)
	private Date lastLoginDate;

	@ApiModelProperty(value = "Language of the user", required = true)
	private String lang;

	@ApiModelProperty(value = "Specifies if the user is a data entry user(Used for internal purpose)", required = true)
	private boolean dataEntryUser = false;

	@ApiModelProperty(value = "Specifies if the user is an admin", required = true)
	private boolean isAdminUser = false;

	@ApiModelProperty(value = "Specifies default page after login")
	private String defaultUrl;
	
	@ApiModelProperty(value = "Specifies if the user is an analyst", required = true)
	private boolean isAnalystUser = false;
	
	@ApiModelProperty(value = "Image of the user")
	private byte[] userImage;

	@ApiModelProperty(value = "Roles of the user")
	private String userRoles;

	@ApiModelProperty(value = "Groups of the user")
	private String userGroups;
	
	@ApiModelProperty(value = "Permissions of the user")
	private String userPermissions;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isDataEntryUser() {
		return dataEntryUser;
	}

	public void setDataEntryUser(boolean dataEntryUser) {
		this.dataEntryUser = dataEntryUser;
	}

	public boolean isAdminUser() {
		return isAdminUser;
	}

	public void setAdminUser(boolean isAdminUser) {
		this.isAdminUser = isAdminUser;
	}

	public String getDefaultUrl() {
		return defaultUrl;
	}

	public void setDefaultUrl(String defaultUrl) {
		this.defaultUrl = defaultUrl;
	}

	public boolean isAnalystUser() {
		return isAnalystUser;
	}

	public void setAnalystUser(boolean isAnalystUser) {
		this.isAnalystUser = isAnalystUser;
	}

	
	public byte[] getUserImage() {
		return userImage;
	}

	public void setUserImage(byte[] userImage) {
		this.userImage = userImage;
	}

	public String getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String userRoles) {
		this.userRoles = userRoles;
	}

	public String getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(String userGroups) {
		this.userGroups = userGroups;
	}

	
	public String getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(String userPermissions) {
		this.userPermissions = userPermissions;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
