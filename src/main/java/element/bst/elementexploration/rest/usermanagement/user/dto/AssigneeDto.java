package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDtoWithRolesObj;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek Maurya
 *
 */
public class AssigneeDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "The ID of the user requested")
	private Long userId;

	@ApiModelProperty(value = "Email of the user requested", required = true)
	@NotEmpty(message = "Email can not be blank.")
	@Email(message = "Invalid email address.")
	private String emailAddress;

	@ApiModelProperty(value = "First name of the user requested", required = true)
	@NotEmpty(message = "FirstName can not be blank.")
	private String firstName;

	@ApiModelProperty(value = "Last name of the user requested", required = true)
	@NotEmpty(message = "LastName can not be blank.")
	private String lastName;

	@ApiModelProperty(value = "Middle name of the user requested", required = false)
	private String middleName;

	@ApiModelProperty(value = "Screen name of the user requested", required = true)
	@NotEmpty(message = "Screen name can not be blank.")
	private String screenName;

	@ApiModelProperty(value = "Date of birth of the user requested", required = true)
	@NotNull(message = "Date of birth can not be blank.")
	private Date dob;

	@ApiModelProperty(value = "Country of the user requested", required = true)
	@NotEmpty(message = "Country can not be blank.")
	private ListItem countryId;

	@ApiModelProperty(value = "Status of the user requested", required = true)
	@JsonIgnore
	private ListItem statusId;

	@ApiModelProperty(value = "Last login date of the user requested", required = true)
	@JsonIgnore
	private Date lastLoginDate;

	@ApiModelProperty(value = "Image of the user", required = true)
	@JsonIgnore
	private byte[] userImage;
	
	@ApiModelProperty(value = "Job title of the user requested", required = false)
	private String jobTitle;

	@ApiModelProperty(value = "Extension of the user requested", required = false)
	private String extension;

	@ApiModelProperty(value = "Phone number of the user requested", required = false)
	private String phoneNumber;
	
	@ApiModelProperty(value = "department of the user requested", required = false)
	private String department;
	
	@ApiModelProperty(value = "source of the user requested", required = false)
	private String source;
	
	@ApiModelProperty(value = "Roles of the user requested", required = false)
	@JsonIgnore
	private List<UserRolesDtoWithRolesObj> usersRoles;

	@ApiModelProperty(value = "Groups of the user requested", required = false)
	@JsonIgnore
	private List<Groups> usersGroups;

	@ApiModelProperty(value = "isModifiable", required = false)
	private Boolean isModifiable=true;
	
	public AssigneeDto() {
		super();
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public ListItem getCountryId() {
		return countryId;
	}

	public void setCountryId(ListItem countryId) {
		this.countryId = countryId;
	}

	public ListItem getStatusId() {
		return statusId;
	}

	public void setStatusId(ListItem statusId) {
		this.statusId = statusId;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public byte[] getUserImage() {
		return userImage;
	}

	public void setUserImage(byte[] userImage) {
		this.userImage = userImage;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<UserRolesDtoWithRolesObj> getUsersRoles() {
		return usersRoles;
	}

	public void setUsersRoles(List<UserRolesDtoWithRolesObj> usersRoles) {
		this.usersRoles = usersRoles;
	}

	public List<Groups> getUsersGroups() {
		return usersGroups;
	}

	public void setUsersGroups(List<Groups> usersGroups) {
		this.usersGroups = usersGroups;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Boolean getIsModifiable() {
		return isModifiable;
	}

	public void setIsModifiable(Boolean isModifiable) {
		this.isModifiable = isModifiable;
	}
}
