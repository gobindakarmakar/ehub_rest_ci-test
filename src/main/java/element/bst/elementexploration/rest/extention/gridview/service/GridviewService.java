package element.bst.elementexploration.rest.extention.gridview.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.gridview.domain.GridView;
import element.bst.elementexploration.rest.extention.gridview.dto.GridViewDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface GridviewService  extends GenericService<GridView, Long>{

	boolean saveOrUpdateGridView(GridViewDto gridViewDto, Long userId) throws Exception;

	List<GridViewDto> getAllGridViewsByUser(Long userId,String tableName)throws Exception;

	boolean deleteGridView(Long gridViewId,Long userId)throws Exception;

	List<GridView> findGridViewByNameAndUser(String gridViewName, String tableName,Long userId);

	List<GridView> findGridViewById(Long gridViewId);

	public void loadDefaultGridView(String gridViewFileName, Long UserId) throws Exception;



}
