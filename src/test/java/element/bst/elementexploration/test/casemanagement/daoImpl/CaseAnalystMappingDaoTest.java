package element.bst.elementexploration.test.casemanagement.daoImpl;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseAnalystMappingDaoImpl;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CaseAnalystMappingDaoTest {
	
	@Mock
	CaseDao caseSeedDao;
	
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@InjectMocks
	CaseAnalystMappingDaoImpl caseAnalystMappingDao;
	
	public Case caseSeed = new Case(); 
	
	public CaseAnalystMapping caseAnalystMapping = new CaseAnalystMapping();
	
	public List<Integer> statuses = new ArrayList<Integer>();
	
	public List<CaseDto> caseDtoList = new ArrayList<CaseDto>();
	
	public CaseCommentNotificationDto caseCommentNotificationDto = new CaseCommentNotificationDto();
	
	List<CaseAnalystMapping> mappingList  = new ArrayList<CaseAnalystMapping>();
	
	@Before
	public void setupMock() {
		caseSeed.setCaseId(1L);
		caseAnalystMapping.setCurrentStatus(3);
		caseAnalystMapping.setCaseSeed(caseSeed);
		statuses.add(3);
		CaseDto caseDto = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDto);
		CaseDto caseDtoNew = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDtoNew);
		caseCommentNotificationDto.setComment("test");
		mappingList.add(caseAnalystMapping);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected = FailedToExecuteQueryException.class)
	public void createMappingTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.save(Mockito.any(CaseAnalystMapping.class))).thenReturn(caseAnalystMapping);
		Mockito.when(caseSeedDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.doThrow(FailedToExecuteQueryException.class).when(caseSeedDao).update(Mockito.any(Case.class));
		caseAnalystMappingDao.createMapping(caseAnalystMapping);
	}
	
	@Test
	public void getAnalystMappingBasedOnUserIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(caseAnalystMapping);
		CaseAnalystMapping newCaseAnalystMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(1L,1L,3);
		assertEquals(new Integer(3),newCaseAnalystMapping.getCurrentStatus());
	}
	
	@Test
	public void getAnalystMappingBasedOnUserId_whenNoResultExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(NoResultException.class);
		CaseAnalystMapping newCaseAnalystMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(1L,1L,3);
		assertEquals(null,newCaseAnalystMapping);
		
	}
	
	@Test
	public void getAnalystMappingBasedOnUserId_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(Exception.class);
		CaseAnalystMapping newCaseAnalystMapping = caseAnalystMappingDao.getAnalystMappingBasedOnUserId(1L,1L,3);
		assertEquals(null,newCaseAnalystMapping);
		
	}
	
	@Test
	public void getCaseInStatusListTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(caseAnalystMapping);
		CaseAnalystMapping newCaseAnalystMapping = caseAnalystMappingDao.getCaseInStatusList(1L,1L,statuses);
		assertEquals(new Integer(3),newCaseAnalystMapping.getCurrentStatus());
	}
	
	@Test
	public void getCaseInStatusList_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(Exception.class);
		CaseAnalystMapping newCaseAnalystMapping = caseAnalystMappingDao.getCaseInStatusList(1L,1L,statuses);
		assertEquals(null,newCaseAnalystMapping);
	}

	@Test
	public void deleteCaseFromFocusByIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenReturn(1);
		boolean status = caseAnalystMappingDao.deleteCaseFromFocusById(1L,1L);
		assertEquals(true,status);
	}
	
	@Test
	public void deleteCaseFromFocusById_whenFalseTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenReturn(0);
		boolean status = caseAnalystMappingDao.deleteCaseFromFocusById(1L,1L);
		assertEquals(false,status);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void deleteCaseFromFocusById_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenThrow(Exception.class);
		boolean status = caseAnalystMappingDao.deleteCaseFromFocusById(1L,1L);
		assertEquals(false,status);
	}
	

	@Test
	public void getAllCasesInFocusByIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> newCaseDtoList = caseAnalystMappingDao.getAllCasesInFocusById(1L, "name", "desc", 1,11);
		assertEquals(2,newCaseDtoList.size());
	}
	
	@Test
	public void getAllCasesInFocusById_whenOrderByIsNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> newCaseDtoList = caseAnalystMappingDao.getAllCasesInFocusById(1L, null, "desc", 1,11);
		assertEquals(2,newCaseDtoList.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllCasesInFocusById_whenExceptionThrownTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<CaseDto> newCaseDtoList = caseAnalystMappingDao.getAllCasesInFocusById(1L, null, "desc", 1,11);
		assertEquals(0,newCaseDtoList.size());
	}
	
	@Test
	public void countAllCasesInFocusByIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3L);
		long count = caseAnalystMappingDao.countAllCasesInFocusById(1L);
		assertEquals(3l,count);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countAllCasesInFocusById_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseAnalystMappingDao.countAllCasesInFocusById(1L);
	}
	

	@Test
	public void fullTextSearchForCasesInFocusTest() throws ParseException{
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> newCaseDtoList = caseAnalystMappingDao.fullTextSearchForCasesInFocus(1L, "test", 1,10, new Date(), "name", "desc");
		assertEquals(2,newCaseDtoList.size());
	}
	
	@Test
	public void fullTextSearchForCasesInFocus_whenOrderByIsNullTest() throws ParseException{
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDtoList);
		List<CaseDto> newCaseDtoList = caseAnalystMappingDao.fullTextSearchForCasesInFocus(1L, "test", 1,10, new Date(), null, "desc");
		assertEquals(2,newCaseDtoList.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fullTextSearchForCasesInFocus_whenExceptionThrownTest() throws ParseException{
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		 caseAnalystMappingDao.fullTextSearchForCasesInFocus(1L, "test", 1,10, new Date(), null, "desc");
	}
	
	@Test
	public void countfullTextSearchForCaseInFocusTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3L);
		long count = caseAnalystMappingDao.countfullTextSearchForCaseInFocus(1L,"test",new Date());
		assertEquals(3l,count);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countfullTextSearchForCaseInFocus_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseAnalystMappingDao.countfullTextSearchForCaseInFocus(1L,"test",new Date());
	}
	
	@Test
	public void getCaseCommentNotificationByIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(caseCommentNotificationDto);
		CaseCommentNotificationDto caseCommentNotificationDtoNew = caseAnalystMappingDao.getCaseCommentNotificationById(1L,1L);
		assertEquals("test",caseCommentNotificationDtoNew.getComment());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCaseCommentNotificationById_whenExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		caseAnalystMappingDao.getCaseCommentNotificationById(1L,1L);
	}
	
	@Test
	public void getAnalystMetricsTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(mappingList);
		List<CaseAnalystMapping> mappingListNew = caseAnalystMappingDao.getAnalystMetrics(new Date(), new Date(), "type", true,1L);
		assertEquals(1,mappingListNew.size());
	}
	
	@Test
	public void getAnalystMetrics_whenDatefromIsNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(mappingList);
		List<CaseAnalystMapping> mappingListNew = caseAnalystMappingDao.getAnalystMetrics(null, new Date(), "type", true,1L);
		assertEquals(1,mappingListNew.size());
	}
	
	@Test
	public void getAnalystMetrics_whenDateToIsNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(mappingList);
		List<CaseAnalystMapping> mappingListNew = caseAnalystMappingDao.getAnalystMetrics(new Date(),null,"type", true,1L);
		assertEquals(1,mappingListNew.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAnalystMetrics_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<CaseAnalystMapping> mappingListNew = caseAnalystMappingDao.getAnalystMetrics(new Date(),null,"type", true,1L);
		assertEquals(null,mappingListNew);
	}
	
	@Test
	public void findByCaseIdTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(caseAnalystMapping);
		CaseAnalystMapping caseAnalystMappingNew = caseAnalystMappingDao.findByCaseId(1L);
		assertEquals(new Integer(3),caseAnalystMappingNew.getCurrentStatus());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findByCaseId_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		CaseAnalystMapping caseAnalystMappingNew = caseAnalystMappingDao.findByCaseId(1L);
		assertEquals(null,caseAnalystMappingNew);
	}
	
	
	

}
