package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author Prateek Maurya
 */

public class LoginHistoryDto implements Serializable {

	
	private static final long serialVersionUID = 1L;
	

	private Long failedLoginId;
	
	private String username;
	
	private String screenName;
	
	private Integer failedLoginAttempt;
	
	private String loginStatus;
	
	private String loginSource;
	
	private Date loginTime;
	
	private String loginDescription;
	
	private String loginClientMedia;
	
	private String loginIpAddress;
	
	private String loginClientLocation;
	
	private String userStatus;
	
	/**
	 * Constructor
	 */
	public LoginHistoryDto() {
		
	}	

	public Long getFailedLoginId() {
		return failedLoginId;
	}

	public void setFailedLoginId(Long failedLoginId) {
		this.failedLoginId = failedLoginId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Integer getFailedLoginAttempt() {
		return failedLoginAttempt;
	}

	public void setFailedLoginAttempt(Integer failedLoginAttempt) {
		this.failedLoginAttempt = failedLoginAttempt;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(String loginSource) {
		this.loginSource = loginSource;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getLoginDescription() {
		return loginDescription;
	}

	public void setLoginDescription(String loginDescription) {
		this.loginDescription = loginDescription;
	}

	public String getLoginClientMedia() {
		return loginClientMedia;
	}

	public void setLoginClientMedia(String loginClientMedia) {
		this.loginClientMedia = loginClientMedia;
	}

	public String getLoginIpAddress() {
		return loginIpAddress;
	}

	public void setLoginIpAddress(String loginIpAddress) {
		this.loginIpAddress = loginIpAddress;
	}

	public String getLoginClientLocation() {
		return loginClientLocation;
	}

	public void setLoginClientLocation(String loginClientLocation) {
		this.loginClientLocation = loginClientLocation;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	
}
