package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

/**
 * 
 * @author Prateek Maurya
 *
 */
@ApiModel("Login user and login counts")
public class LoginUserCountDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long userId;
	private Long allLoginCount;
	private Long failedLoginCount;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getAllLoginCount() {
		return allLoginCount;
	}
	public void setAllLoginCount(Long allLoginCount) {
		this.allLoginCount = allLoginCount;
	}
	public Long getFailedLoginCount() {
		return failedLoginCount;
	}
	public void setFailedLoginCount(Long failedLoginCount) {
		this.failedLoginCount = failedLoginCount;
	}
	
	
	
}
