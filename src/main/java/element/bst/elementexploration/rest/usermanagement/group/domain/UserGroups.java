package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "bst_um_user_group")
public class UserGroups implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ApiModelProperty(value = "userId")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private Users userId;

	@ApiModelProperty(value = "userGroupId")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userGroupId",referencedColumnName="id")
	private Groups groupId;

	public UserGroups() {
		super();
	}

	public UserGroups(Groups groupId, Users userId) {
		super();
		this.groupId = groupId;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Users getUserId() {
		return userId;
	}

	public void setUserId(Users userId) {
		this.userId = userId;
	}

	public Groups getGroupId() {
		return groupId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}

	

}
