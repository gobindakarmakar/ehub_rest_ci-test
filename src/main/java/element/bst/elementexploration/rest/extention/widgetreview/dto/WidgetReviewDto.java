package element.bst.elementexploration.rest.extention.widgetreview.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rambabu
 *
 */
public class WidgetReviewDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@JsonProperty(value = "isApproved", required = true)
	private Boolean isApproved;

	private Date approvedDate;

	private String userName;
	@JsonProperty(value = "entityId", required = true)
	private String entityId;
	@JsonProperty(value = "widgetId", required = true)
	private Long widgetId;

	private String widgetName;

	public WidgetReviewDto() {
		super();
	}

	public WidgetReviewDto(Long id, Boolean isApproved, Date approvedDate, String userName, String entityId,
			Long widgetId, String widgetName) {
		super();
		this.id = id;
		this.isApproved = isApproved;
		this.approvedDate = approvedDate;
		this.userName = userName;
		this.entityId = entityId;
		this.widgetId = widgetId;
		this.widgetName = widgetName;
	}

	public WidgetReviewDto(Boolean isApproved, String entityId, Long widgetId) {
		super();
		this.isApproved = isApproved;
		this.entityId = entityId;
		this.widgetId = widgetId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Long getWidgetId() {
		return widgetId;
	}

	public void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}

	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

}
