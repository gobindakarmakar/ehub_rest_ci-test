package element.bst.elementexploration.rest.extention.datacuration.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.datacuration.service.DataCurationService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("dataCurationService")
public class DataCurationServiceImpl implements DataCurationService {

	@Value("${bigdata_org_url}")
	private String BIG_DATA__ORG_URL;

	@Override
	public String getNationalities() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/nationalities";
		return getDataFromServer(url);
	}

	@Override
	public String getCountries() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/countries";
		return getDataFromServer(url);
	}

	@Override
	public String getEcnomicSectors() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/economic-sectors";
		return getDataFromServer(url);
	}

	@Override
	public String getBusinessSectors() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/business-sectors";
		return getDataFromServer(url);
	}

	@Override
	public String getIndurstryGroups() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/industry-groups";
		return getDataFromServer(url);
	}

	@Override
	public String getIndustries() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/industries";
		return getDataFromServer(url);
	}

	@Override
	public String getActivities() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/activities";
		return getDataFromServer(url);
	}

	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String createDirectorshipRelation(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "relation/directorship";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String createOfficershipRelation(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "relation/officership";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getOfficershipRelationById(String identifier) throws Exception {
		String url = BIG_DATA__ORG_URL + "relation/officership/" + identifier;
		return getDataFromServer(url);
	}

	@Override
	public String getDirectorshipRelationById(String identifier) throws Exception {
		String url = BIG_DATA__ORG_URL + "relation/directorship/" + identifier;
		return getDataFromServer(url);
	}

	@Override
	public String getDirectorRoles() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/director-roles";
		return getDataFromServer(url);
	}

	@Override
	public String getOfficerRoles() throws Exception {
		String url = BIG_DATA__ORG_URL + "meta/officer-roles";
		return getDataFromServer(url);
	}

	@Override
	public String getDirectorshipRelationsBySearch(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "relation/directorship/search";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getOfficershipRelationsBySearch(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "relation/officership/search";
		return postStringDataToServer(url, jsonString);
	}

}
