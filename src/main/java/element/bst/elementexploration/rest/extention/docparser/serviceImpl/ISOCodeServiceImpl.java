package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.docparser.domain.IsoCodes;
import element.bst.elementexploration.rest.extention.docparser.service.ISOCodeService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("isoCodeService")
public class ISOCodeServiceImpl extends GenericServiceImpl<IsoCodes, Long> implements ISOCodeService {

	@Autowired
	public ISOCodeServiceImpl(GenericDao<IsoCodes, Long> genericDao) {
		super(genericDao);
	}

}
