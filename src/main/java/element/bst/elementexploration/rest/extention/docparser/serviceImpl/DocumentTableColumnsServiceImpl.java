package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableColumns;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTableColumnsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("documentTableColumnsService")
public class DocumentTableColumnsServiceImpl extends GenericServiceImpl<DocumentTableColumns, Long> implements DocumentTableColumnsService {

	@Autowired
	public DocumentTableColumnsServiceImpl(GenericDao<DocumentTableColumns, Long> genericDao) {
		super(genericDao);
	}
	
	

}
