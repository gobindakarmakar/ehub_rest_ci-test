package element.bst.elementexploration.rest.extention.activiti.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.activiti.bpmn.model.FieldExtension;
import org.activiti.bpmn.model.ServiceTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.bpmn.behavior.MailActivityBehavior;
import org.activiti.engine.impl.bpmn.helper.ClassDelegate;
import org.activiti.engine.impl.bpmn.parser.BpmnParse;
import org.activiti.engine.impl.bpmn.parser.FieldDeclaration;
import org.activiti.engine.impl.bpmn.parser.handler.ServiceTaskParseHandler;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.el.FixedValue;
import org.apache.commons.lang3.StringUtils;
/**
 * 
 * @author Viswanath Reddy G
 *
 */
public class CustomBpmnParseHandler extends ServiceTaskParseHandler {

	protected void executeParse(BpmnParse bpmnParse, ServiceTask serviceTask) {

	  
	  super.executeParse(bpmnParse, serviceTask);
	  if (serviceTask.getBehavior() instanceof MailActivityBehavior) {
		  serviceTask.setBehavior(
	     createCustomMailActivityBehavior(serviceTask.getId(), serviceTask.getFieldExtensions()));
	  }
	}

	public CustomMailActivityBehavior createCustomMailActivityBehavior(String taskId, List<FieldExtension> fields) {
	  List<FieldDeclaration> fieldDeclarations = createFieldDeclarations(fields);
	  return (CustomMailActivityBehavior) ClassDelegate.defaultInstantiateDelegate(CustomMailActivityBehavior.class,
	    fieldDeclarations);
	}

	public List<FieldDeclaration> createFieldDeclarations(List<FieldExtension> fieldList) {
	  List<FieldDeclaration> fieldDeclarations = new ArrayList<FieldDeclaration>();

	  for (FieldExtension fieldExtension : fieldList) {
	   FieldDeclaration fieldDeclaration = null;
	   if (StringUtils.isNotEmpty(fieldExtension.getExpression())) {
	    fieldDeclaration = new FieldDeclaration(fieldExtension.getFieldName(), Expression.class.getName(),
	    Context.getProcessEngineConfiguration().getExpressionManager().createExpression(fieldExtension.getExpression()));
	   } else {
	    fieldDeclaration = new FieldDeclaration(fieldExtension.getFieldName(), Expression.class.getName(),
	      new FixedValue(fieldExtension.getStringValue()));
	   }

	   fieldDeclarations.add(fieldDeclaration);
	  }
	  return fieldDeclarations;
	}
}
