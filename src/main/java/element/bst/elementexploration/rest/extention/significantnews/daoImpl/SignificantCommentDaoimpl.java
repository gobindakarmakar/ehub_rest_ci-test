package element.bst.elementexploration.rest.extention.significantnews.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantCommentDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("significantCommentDao")
public class SignificantCommentDaoimpl extends GenericDaoImpl<SignificantComment, Long>
		implements SignificantCommentDao {

	public SignificantCommentDaoimpl() {
		super(SignificantComment.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SignificantComment> getSignificantComments(String classification, Long significantId) {
		List<SignificantComment> significantCommentList = new ArrayList<SignificantComment>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select sc from SignificantComment sc where sc.significantId = :significantId order by sc.createdDate desc");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			//query.setParameter("classification", classification);
			query.setParameter("significantId", significantId);

			significantCommentList = (List<SignificantComment>) query.getResultList();
		} catch (Exception e) {
			return significantCommentList;
		}
		return significantCommentList;
	}

}
