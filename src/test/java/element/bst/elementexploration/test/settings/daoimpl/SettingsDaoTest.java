package element.bst.elementexploration.test.settings.daoimpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.settings.daoimpl.SettingsDaoImpl;
import element.bst.elementexploration.rest.settings.domain.Settings;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SettingsDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SettingsDaoImpl settingsDao;

	Settings settings;

	List<Settings> settingsList = new ArrayList<Settings>();

	@Before
	public void doSetup() {
		settings = new Settings();
		settings.setId(1L);
		settings.setName("mailsettings");
		settingsList.add(settings);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getDeaultUrlTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(settings);
		Settings settingsObj = settingsDao.getDeaultUrl();
		assertEquals("mailsettings", settingsObj.getName());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getDeaultUrl_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Settings settingsObj = settingsDao.getDeaultUrl();
		assertEquals(null, settingsObj);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getDeaultUrl_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		Settings settingsObj = settingsDao.getDeaultUrl();
		assertEquals(null, settingsObj);
	}

	@Test
	public void fetchAllSettingsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(settingsList);
		List<Settings> settingsListNew = settingsDao.fetchAllSettings("mail");
		assertEquals(1, settingsListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAllSettings_whenExceptiopnThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<Settings> settingsListNew = settingsDao.fetchAllSettings("mail");
		assertEquals(null, settingsListNew);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getMailSettings_whenExceptiopnThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyObject())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<Settings> settingsListNew = settingsDao.getMailSettings("mail");
		assertEquals(0, settingsListNew.size());
	}

}
