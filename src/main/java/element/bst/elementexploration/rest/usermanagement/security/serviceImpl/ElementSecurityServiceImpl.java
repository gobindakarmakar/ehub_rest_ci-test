package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriUtils;

import com.google.gson.JsonObject;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.exception.AuthenticationFailedException;
import element.bst.elementexploration.rest.exception.DublicateTokenFoundException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.security.dao.ElementSecurityDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserToken;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementSecurityService;
import element.bst.elementexploration.rest.usermanagement.security.service.FailedLoginService;
import element.bst.elementexploration.rest.usermanagement.security.service.MailService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.Mail;

/**
 * 
 * @author Amit Patel
 *
 */
@Service("elementSecurityService")
public class ElementSecurityServiceImpl extends GenericServiceImpl<UserToken, Long> implements ElementSecurityService {

	@Autowired
	public ElementSecurityServiceImpl(GenericDao<UserToken, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	ElementSecurityDao elementSecurityDao;

	@Autowired
	ElementSecurityService elementSecurityService;

	@Autowired
	private UserService userService;

	@Autowired
	private FailedLoginService failedLoginService;

	/*@Autowired
	private AuthenticationManager authenticationManager;*/

	@Autowired
	public JavaMailSender javaMailSender;

	@Autowired
	Environment env;

	@Autowired
	private MailService mailService;
	
	@Autowired
	private CaseService caseService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Value("${token.validity.days}")
	private int TOKEN_VALIDITY_DAYS;

	@Value("${validation_api}")
	private String VALIDATION_API;

	@Value("${fe_base_url}")
	private String FE_BASE_URL;

	private final int DEFAULT_TOKEN_VALIDITY_DAYS = 60;

	/**
	 * 
	 * @param token
	 * @return
	 */
	@Override
	@Transactional("transactionManager")
	public User getUserUserFromToken(String token) {
		if (token == null)
			return null;
		else if (elementSecurityDao.isValidToken(token)) {
			if (elementSecurityDao.getUserToken(token) != null)
				return elementSecurityDao.getUserToken(token).getUser();
			else
				return null;
		} else
			return null;
	}

	@Override
	@Transactional("transactionManager")
	public UserToken getUserToken(Long userId) {
		return elementSecurityDao.isTokenExist(userId);
	}

	@Override
	public String validateEmailPassword(String emailAddress, String password, String ipAddress) throws Exception {
		String generatedToken = null;
		if (emailAddress == null || password == null || emailAddress.trim().length() == 0
				|| password.trim().length() == 0) {
			throw new InsufficientDataException("Username or Password cannot be empty");
		}

		if (failedLoginService.isAccountLocked(emailAddress)) {
			throw new AuthenticationFailedException("User Account is locked.");
		}

		int validationStatus = 0;
		User user = userService.getUserObjectByEmailOrUsername(emailAddress);
		if (user != null) {
			if (null != user.getStatus()) {
				if (Integer.parseInt(user.getStatus()) != 0) {
					UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
							emailAddress, password);
					try {
					/*	Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
						SecurityContextHolder.getContext().setAuthentication(authentication);*/
						validationStatus = 1;
					} catch (Exception ex) {
						validationStatus = -1;
					}
				} else {
					validationStatus = 2;
				}
			} else {
				validationStatus = 2;
			}
		}

		try {
			if (validateLoginResponse(validationStatus)) {
				if(user!=null)
					generatedToken = elementSecurityService.saveToken(user);
				failedLoginService.releaseAccountLocked(emailAddress, ipAddress);
			}
		} catch (Exception e) {
			failedLoginService.processFailedAuthentication(user.getEmailAddress(), user.getScreenName(), ipAddress);
			throw e;
		}

		return generateResponseMessage(generatedToken);
	}

	/**
	 * Generate proper response message
	 * 
	 * @param validationStatus
	 * @param generatedToken
	 * @return
	 */
	private String generateResponseMessage(String generatedToken) {
		JsonObject object = new JsonObject();
		object.addProperty("token", generatedToken);
		return object.toString();
	}

	/**
	 * 
	 * @param validationStatus
	 * @return true if login is successful
	 */
	private boolean validateLoginResponse(int validationStatus) {
		switch (validationStatus) {
		case 1:
			return true;
		case 0:
			throw new AuthenticationFailedException("User not found.");
		case -1:
			throw new AuthenticationFailedException("Either email or password is incorrect.");
		case 2:
			throw new AuthenticationFailedException("User account deactivated.");
		default:
			throw new AuthenticationFailedException("Could not process request.");
		}
	}

	/**
	 * 
	 * @param user
	 */
	@Override
	@Transactional("transactionManager")
	public String saveToken(User user) {
		String generatedToken = null;
		UserToken userToken = null;
		Long userId = user.getUserId();
		if (null != userId) {
			userToken = elementSecurityDao.isTokenExist(userId);
		}

		if (userToken == null) {
			// generate new token
			generatedToken = genearteToken();
			if (generatedToken == null || elementSecurityDao.isTokenExist(generatedToken)) {
				throw new DublicateTokenFoundException("Could not generate new token, try agian");
			}
			Date currentDate = new Date();
			UserToken userTokenTemp = new UserToken();
			userTokenTemp.setCreatedOn(currentDate);
			userTokenTemp.setExpireOn(calculateTokenExpireDate(currentDate));
			userTokenTemp.setUser(user);
			userTokenTemp.setToken(generatedToken);
			elementSecurityDao.saveOrUpdate(userTokenTemp);
		} else {
			if (validateTokenObject(userToken)) {
				generatedToken = userToken.getToken();
			} else {
				generatedToken = genearteToken();
				if (generatedToken == null || elementSecurityDao.isTokenExist(generatedToken)) {
					throw new DublicateTokenFoundException("Could not generate new token, try agian");
				}
				Date currentDate = new Date();
				userToken.setCreatedOn(currentDate);
				userToken.setExpireOn(calculateTokenExpireDate(currentDate));
				userToken.setToken(generatedToken);
				elementSecurityDao.saveOrUpdate(userToken);
			}
		}
		return generatedToken;
	}

	/**
	 * 
	 * @param userToken
	 * @return return true if no expired token is found, else return false
	 */
	private boolean validateTokenObject(UserToken userToken) {
		// Compare expiry date with current date
		Date expiryDate = userToken.getExpireOn();
		if (expiryDate.compareTo(new Date()) <= 0)
			return false;
		return true;
	}

	/**
	 * Generate unique token
	 * 
	 * @return
	 */
	private String genearteToken() {
		// Creating a random UUID (Universally unique identifier).
		return UUID.randomUUID().toString();
	}

	/**
	 * Add given day to current date
	 * 
	 * @param currentDate
	 * @return
	 */
	private Date calculateTokenExpireDate(Date currentDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.DATE, getTokenExpiryDay()); // minus number would
														// decrement the days
		return cal.getTime();
	}

	/**
	 * Check if expiry days is valid
	 * 
	 * @return
	 */
	private int getTokenExpiryDay() {
		return TOKEN_VALIDITY_DAYS > 0 ? TOKEN_VALIDITY_DAYS : DEFAULT_TOKEN_VALIDITY_DAYS;
	}

	@Override
	@Transactional("transactionManager")
	public boolean validateTemporaryPassword(Long userId, String password) {
		User user = null;
		if (userId != null)
			user = userService.find(userId);
		if (null != user && password != null) {
			if (null != user.getTemporaryPasswordExpiresOn() && user.getTemporaryPassword() != null)
				if (((new Date()).compareTo(user.getTemporaryPasswordExpiresOn()) <= 0)
						&& (passwordEncoder.matches(password, user.getTemporaryPassword())))
					return true;
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean sendEmailForPasswordReset(UserDto user, HttpServletRequest request) throws Exception {
		String remoteAddr;
		if (null != user) {
			User userData = userService.find(user.getUserId());
			String tempPassword = RandomStringUtils.randomAlphanumeric(10);
		///	userData.setTemporaryPassword(passwordEncoder.encode(tempPassword));
			Date currentDate = new Date();
			Calendar calenderDate = Calendar.getInstance();
			calenderDate.setTime(currentDate);
			calenderDate.add(Calendar.DATE, 1);
			userData.setTemporaryPasswordExpiresOn(calenderDate.getTime());
			userService.saveOrUpdate(userData);
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
			String validationUrl = new String(
					request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/");
			Map<String, String> mailSettings = new HashMap<>();
			Mail mail = new Mail();
			// get mail from system settings
			mailSettings = settingsService.getMailSettings();
			mail.setMailFrom(mailSettings.get("From"));
			mail.setMailTo(userData.getEmailAddress());
			mail.setMailSubject("Reset your password");

			String token = userData.getUserId() + "-" + tempPassword;
			String warName = new File(request.getServletContext().getRealPath("/")).getName();
			String link = UriUtils.encodePath(validationUrl + warName + VALIDATION_API + token, "UTF-8");
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("firstName", userData.getFirstName());
			model.put("lastName", userData.getLastName());
			model.put("location", link);
			mail.setModel(model);
			try {
				mailService.sendEmail(mail);
				return true;
			} catch (Exception e) {
				return false;
			}

		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean validateTemporaryPassword(String userIDAndTempPassword, HttpServletRequest request)
			throws Exception {
		Long userId = null;
		String password = null;
		String[] userIdAndPass = userIDAndTempPassword.split("-");
		if (userIdAndPass.length > 0) {
			userId = Long.valueOf(userIdAndPass[0].trim());
			password = userIdAndPass[1];
			if (elementSecurityService.validateTemporaryPassword(userId, password)) {
				return true;
			}
		}

		return false;

	}
	
	@Override
	@Transactional("transactionManager")
	public boolean sendStatusChangeEmail(Long userId, String statusFrom, String statusTo, Long newUserId, Long caseId,
			String forward, String reassign, String text) {
		try {
			Case caseobj = caseService.find(caseId);
			User newUser = null;
			User user = userService.find(userId);
			if (newUserId != null)
				newUser = userService.find(newUserId);

			Mail mail = new Mail();
			mail.setMailFrom(env.getProperty("email.username"));
			mail.setMailTo(user.getEmailAddress());
			mail.setMailSubject(caseobj.getName());
			if (newUserId != null)
				mail.setMailCc(newUser.getEmailAddress());
			if (text == null) {
				if (statusFrom == null && statusTo == null)
					mail.setMailContent("New Case Added to you are case diary " + caseId);

				if (statusTo!=null && statusTo.equalsIgnoreCase("focus")) {
					mail.setMailContent("Case added to focus area");
				} else if (statusTo.equalsIgnoreCase("unfocus")) {
					mail.setMailContent("Case removed from focus area");
				} else if (reassign != null) {
					mail.setMailContent("Case assigned to " + newUser.getFirstName() + " " + newUser.getLastName()
							+ " by " + user.getFirstName());
				} else if (forward != null) {
					mail.setMailContent("Case forward to " + newUser.getFirstName() + " " + newUser.getLastName()
							+ " by " + user.getFirstName());
				} else {
					mail.setMailContent(
							"Status Changed " + statusFrom + " to " + statusTo + " by " + user.getFirstName());
				}
			}else{
				mail.setMailContent(text);
			}

			mailService.sendCaseEmail(mail);
			return true;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return false;
		}
	}

	@Override
	public Users getUserUsersFromToken(String token) {
		if (token == null)
			return null;
		else if (elementSecurityDao.isValidToken(token)) {
			if (elementSecurityDao.getUserToken(token) != null)
				return elementSecurityDao.getUsersToken(token).getUserId();
			else
				return null;
		} else
			return null;
	}

}
