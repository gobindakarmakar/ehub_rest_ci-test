package element.bst.elementexploration.rest.extention.alertmanagement.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import io.swagger.annotations.Api;

/**
 * @author Prateek
 *
 */

@Api(tags = { "Alert Comments API" })
@RestController
@RequestMapping("/api/alertComments")
public class CommentsController extends BaseController{

}
