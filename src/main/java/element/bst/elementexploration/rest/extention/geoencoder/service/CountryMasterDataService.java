package element.bst.elementexploration.rest.extention.geoencoder.service;

import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface CountryMasterDataService extends GenericService<CountryMasterData, Long> {

}
