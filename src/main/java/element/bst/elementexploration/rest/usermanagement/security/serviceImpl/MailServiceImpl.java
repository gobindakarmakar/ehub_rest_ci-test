package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import element.bst.elementexploration.rest.usermanagement.security.service.MailService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.EmailTypes;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.Mail;
import freemarker.template.Configuration;

@Service("mailService")
public class MailServiceImpl implements MailService {
 
    @Autowired
    JavaMailSender javaMailSender;
 
    @Autowired
    Configuration fmConfiguration;
 
    public void sendEmail(Mail mail) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
 
        try {
 
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
 
            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            mail.setMailContent(geContentFromTemplate(mail.getModel()));
            mimeMessageHelper.setText(mail.getMailContent(), true);
            mimeMessageHelper.addInline("logo.png", new ClassPathResource("templates/logo-ele.png"));
            javaMailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
        }
    }
    public void sendEmail(Mail mail,String type) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
 
        try {
 
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
 
            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            mail.setMailContent(geContentFromTemplate(mail.getModel(),type));
            mimeMessageHelper.setText(mail.getMailContent(), true);
            mimeMessageHelper.addInline("logo.png", new ClassPathResource("templates/logo-ele.png"));
            javaMailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
        }
    }
 
    public String geContentFromTemplate(Map < String, Object > model) {
        StringBuffer content = new StringBuffer();
 
        try {
            content.append(FreeMarkerTemplateUtils
                .processTemplateIntoString(fmConfiguration.getTemplate("email-template.html"), model));
        } catch (Exception e) {
            ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
        }
        return content.toString();
    }
    public String geContentFromTemplate(Map < String, Object > model,String type) {
        StringBuffer content = new StringBuffer();
 
        try {
        	if(type!=null && type.equalsIgnoreCase(EmailTypes.REGISTRATION.toString())) {
        		 content.append(FreeMarkerTemplateUtils
        	                .processTemplateIntoString(fmConfiguration.getTemplate("reg-email-template.html"), model));
        	}else {
            content.append(FreeMarkerTemplateUtils
                .processTemplateIntoString(fmConfiguration.getTemplate("email-template.html"), model));
        	}
        } catch (Exception e) {
            ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
        }
        return content.toString();
    }
    
    @Override
    public void sendCaseEmail(Mail mail) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
 
        try {
 
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
 
            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            if(mail.getMailCc()!=null)
            	mimeMessageHelper.setCc(mail.getMailCc());
            mimeMessageHelper.setText(mail.getMailContent(), true);
 
            javaMailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
        }
    }
 
}