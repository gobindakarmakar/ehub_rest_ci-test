package element.bst.elementexploration.rest.util;

public enum EmailTypes {

	REGISTRATION("Registration"),

	RESET("Reset");

	private String type;

	private EmailTypes(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	};

}
