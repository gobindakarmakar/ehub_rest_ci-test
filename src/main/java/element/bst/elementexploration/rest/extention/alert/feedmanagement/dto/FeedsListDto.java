package element.bst.elementexploration.rest.extention.alert.feedmanagement.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModelProperty;

public class FeedsListDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "List of Feeds")
	private List<FeedManagementDto> result;
	
	@ApiModelProperty(value = "Page information")
	private PaginationInformation paginationInformation;

	public List<FeedManagementDto> getResult() {
		return result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setResult(List<FeedManagementDto> result) {
		this.result = result;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}
	
}
