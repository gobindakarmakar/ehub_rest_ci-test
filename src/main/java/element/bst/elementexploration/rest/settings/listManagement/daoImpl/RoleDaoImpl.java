package element.bst.elementexploration.rest.settings.listManagement.daoImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.listManagement.dao.RoleDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.Role;

/**
 * @author hanuman
 *
 */
@Repository("roleDao")
@Transactional("transactionManager")
public class RoleDaoImpl extends GenericDaoImpl<Role, Long> implements 

RoleDao {

	public RoleDaoImpl() {
		super(Role.class);
	}
}
