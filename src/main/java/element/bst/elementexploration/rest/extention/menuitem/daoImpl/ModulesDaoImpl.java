package element.bst.elementexploration.rest.extention.menuitem.daoImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.ModulesDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author Jalagari Paul
 *
 */
@Repository("modulesDao")
@Transactional("transactionManager")
public class ModulesDaoImpl extends GenericDaoImpl<Modules, Long> implements ModulesDao {

	public ModulesDaoImpl() {
		super(Modules.class);
	}
}
