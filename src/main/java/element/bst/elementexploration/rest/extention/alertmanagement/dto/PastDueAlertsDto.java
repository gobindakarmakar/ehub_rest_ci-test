package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.List;

public class PastDueAlertsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PastDueMonthCountDto> monthCountList;

	private Integer latestPastDueCount;
	
	private String percentageThanLastWeek;

	public List<PastDueMonthCountDto> getMonthCountList() {
		return monthCountList;
	}

	public void setMonthCountList(List<PastDueMonthCountDto> monthCountList) {
		this.monthCountList = monthCountList;
	}

	public Integer getLatestPastDueCount() {
		return latestPastDueCount;
	}

	public void setLatestPastDueCount(Integer latestPastDueCount) {
		this.latestPastDueCount = latestPastDueCount;
	}

	public String getPercentageThanLastWeek() {
		return percentageThanLastWeek;
	}

	public void setPercentageThanLastWeek(String percentageThanLastWeek) {
		this.percentageThanLastWeek = percentageThanLastWeek;
	}

	
	
	

}
