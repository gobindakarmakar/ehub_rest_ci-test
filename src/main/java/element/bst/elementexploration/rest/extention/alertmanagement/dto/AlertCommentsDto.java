package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDtoWithRolesAndGroups;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek
 *
 */

public class AlertCommentsDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long commentId;
	
	@ApiModelProperty(value = "Alert Comments", required = true)
	@NotEmpty(message = "comments can not be blank.")
	private String comments;
	
	@ApiModelProperty(value = "Alert Comments", required = true)
	@NotEmpty(message = "alert id can not be blank.")
	private Alerts alertId;
	
	private Date createdDate;

	private Date updatedDate;
	
	private UsersDtoWithRolesAndGroups commentedBy;

	private List<DocumentVaultDTO> attachment;
	
	public Long getCommentId() {
		return commentId;
	}

	public String getComments() {
		return comments;
	}

	public Alerts getAlertId() {
		return alertId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setAlertId(Alerts alertId) {
		this.alertId = alertId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<DocumentVaultDTO> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<DocumentVaultDTO> attachment) {
		this.attachment = attachment;
	}

	public UsersDtoWithRolesAndGroups getCommentedBy() {
		return commentedBy;
	}

	public void setCommentedBy(UsersDtoWithRolesAndGroups commentedBy) {
		this.commentedBy = commentedBy;
	}


	

}
