package element.bst.elementexploration.rest.notification.controller;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.dto.CheckInNotificationDto;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertVo;
import element.bst.elementexploration.rest.notification.dto.NotificationCountDto;
import element.bst.elementexploration.rest.notification.dto.NotificationResponseDto;
import element.bst.elementexploration.rest.notification.service.NotificationService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Viswanath
 *
 */
@Api(tags = { "Notification API" },description="Manages user's notifications")
@RestController
@RequestMapping("/api/notification")
public class NotificationController extends BaseController {

	@Autowired
	private NotificationService notificationService;
	
	@Autowired
    private ApplicationEventPublisher eventPublisher;

	@ApiOperation("Create Notification Alert")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = NotificationResponseDto.class, message = "Notification Alert Created Successfully."),
			@ApiResponse(code=400 ,response=ResponseMessage.class ,message=ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@PostMapping(value = "/createNotificationAlert", produces = { "application/json; charset=UTF-8" }, 
		consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createNotification(@RequestParam("token") String userToken,
			@RequestBody NotificationAlertVo notificationVo, HttpServletRequest request) throws InsufficientDataException {
		Long userId = getCurrentUserId();
		NotificationAlert notification=new NotificationAlert();
		BeanUtils.copyProperties(notificationVo, notification);
		notification.setCreatedBy(userId);
		Long id = notificationService.createNotificationAlert(notification);
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("notificationId", id);*/
		NotificationResponseDto notificationResponseDto=new NotificationResponseDto();
		notificationResponseDto.setNotificationId(id);
		eventPublisher.publishEvent(new LoggingEvent(id, LoggingEventType.CREATE_NOTIFICATION, userId, new Date()));
		return new ResponseEntity<>(notificationResponseDto, HttpStatus.OK);
	}

	@ApiOperation("CheckIn Notifications")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CheckInNotificationDto.class, message = "Notification Alert Created Successfully."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.CHECKIN_FAILED)})
	@PostMapping(value = "/checkInNotifications", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> checkInNotifications(@RequestParam("token") String token, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		String timestampCheckIn = notificationService.checkInNotifications(userId);
		if (timestampCheckIn != null && !timestampCheckIn.equals("")) {
			/*JSONObject jsonObject = new JSONObject();
			jsonObject.put("checkInTimestamp", timestampCheckIn);*/
			CheckInNotificationDto checkInNotificationDto=new CheckInNotificationDto();
			checkInNotificationDto.setCheckInTimestamp(timestampCheckIn);
			return new ResponseEntity<>(checkInNotificationDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.CHECKIN_FAILED), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation("Get notification count")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = NotificationCountDto.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@GetMapping(value = "/getNotificationCount", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getNotifications(@RequestParam("token") String token,
			@RequestParam(required = false) Long caseId,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTo,
			@RequestParam(required = false) Integer type, @RequestParam(required = false) Integer level,
			HttpServletRequest request) {

		Long userId = getCurrentUserId();
		LocalDateTime lastCheckIn = notificationService.getLastCheckIn(userId);
		List<NotificationAlertDTO> notificationList = notificationService
				.retrieveCaseNotificationAlertBasedOnLastCheckIn(caseId, userId, lastCheckIn, type, level, dateTo);
		//JSONObject jsonObject = new JSONObject();
		NotificationCountDto notificationCountDto= new NotificationCountDto();
		if (notificationList != null && notificationList.size() > 0) {			
			//jsonObject.put("notificationCount", notificationList.size());			
			notificationCountDto.setNotificationCount(new Integer(0));
		} else{
			notificationCountDto.setNotificationCount(0);
		}
		notificationCountDto.setLastCheckInTimestamp(lastCheckIn != null ? lastCheckIn.toString() : null);
		//jsonObject.put("lastCheckInTimestamp", lastCheckIn != null ? lastCheckIn.toString() : null);
		return new ResponseEntity<>(notificationCountDto, HttpStatus.OK);
	}

	@ApiOperation("Get user notification list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@GetMapping(value = "/getUserNotificationList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserNotificationlist(@RequestParam("token") String token,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTo,
			@RequestParam(required = false) Long caseId, @RequestParam(required = false) Integer type,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn, HttpServletRequest request) {

			Long userId = getCurrentUserId();
			LocalDateTime lastCheckIn = notificationService.getLastCheckIn(userId);
			List<NotificationAlertDTO> notificationList = notificationService.getUserNotificationList(userId, dateFrom,
					dateTo, caseId, type, pageNumber, recordsPerPage, orderBy, lastCheckIn, orderIn);

			// Pagination
			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = notificationService.countUserNotificationList(userId, dateFrom, dateTo, caseId, type, lastCheckIn);
			PaginationInformation information = new PaginationInformation();
			information.setTotalResults(totalResults);
			information.setTitle(request.getRequestURI());
			information.setKind("list");
			information.setCount(notificationList != null ? notificationList.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");
/*
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("result", notificationList);
			jsonObject.put("paginationInformation", information);*/
			ResponseUtil responseUtil=new ResponseUtil();
			responseUtil.setResult(notificationList);
			responseUtil.setPaginationInformation(information);
			return new ResponseEntity<>(responseUtil, HttpStatus.OK);			
	}

	@ApiOperation("Get notification details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = NotificationAlertDTO.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NOTIFICATION_DETAIL_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@GetMapping(value = "/getNotificationDetail", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getNotificationDetail(@RequestParam("token") String token,
			@RequestParam("notifId") Long notifId, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		NotificationAlertDTO notifDetail = notificationService.retrieveNotificationDetail(notifId, userId);
		if (null != notifDetail)
			return new ResponseEntity<>(notifDetail, HttpStatus.OK);
		else
			throw new NoDataFoundException(ElementConstants.NOTIFICATION_DETAIL_NOT_FOUND);
	}

}
