package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.dto.CreateGroupDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupsDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UpdateGroupDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupStatusDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupsListDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UsersListByGroupListDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserListDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import element.bst.elementexploration.rest.util.UserStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Users group API" }, description = "Manages application's user groups")
@RestController
@RequestMapping("/api/groups")
public class UserGroupsController extends BaseController {

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private UserGroupsService userGroupsService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private SettingsService settingsService;

	@ApiOperation("Creates a user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_CREATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.GROUP_NAME_EXIST),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/create", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createGroup(@RequestParam("token") String token,
			@Valid @RequestBody CreateGroupDto createGroupDto, BindingResult results, HttpServletRequest request) throws Exception {
		Long adminId = getCurrentUserId();
		GenericReturnObject message = new GenericReturnObject();
		UpdateGroupDto group = new UpdateGroupDto();
		BeanUtils.copyProperties(createGroupDto, group);
		if (!results.hasErrors()) {
			Groups groupTemp = groupsService.getGroup(group.getName());
			if (groupTemp == null) {
				if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
						"Allow to create manually-Groups").equalsIgnoreCase("On")) {
					Groups newGroup = new Groups();
					newGroup.setActive(true);
					newGroup.setCreatedBy(adminId);
					newGroup.setCreatedDate(new Date());
					newGroup.setModifiedDate(new Date());
					newGroup.setName(group.getName());
					if (group.getRemarks() != null)
						newGroup.setRemarks(group.getRemarks());
					if (group.getColor() != null)
						newGroup.setColor(group.getColor());
					if (group.getIcon() != null)
						newGroup.setIcon(group.getIcon());
					if (group.getDescription() != null)
						newGroup.setDescription(group.getDescription());
					if(group.getSource()!=null)
						newGroup.setSource(group.getSource());
					newGroup = groupsService.save(newGroup);
					//userGroupsService.addUserToGroup(newGroup.getId(),getCurrentUserId(),adminId);
					message.setData(newGroup);
					message.setResponseMessage(ElementConstants.GROUP_CREATED_SUCCESSFULLY_MSG);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					try {
						eventPublisher.publishEvent(
								new LoggingEvent(newGroup.getId(), LoggingEventType.CREATE_GROUP, adminId, new Date()));
					} catch (Exception e) {
						e.printStackTrace();
						return new ResponseEntity<>(message, HttpStatus.OK);
					}
				} else {
					message.setStatus(ElementConstants.ERROR_STATUS);
					message.setResponseMessage(ElementConstants.GROUP_CREATION_DISABLED);
				}
			} else {
				message.setResponseMessage(ElementConstants.GROUP_NAME_EXIST);
				message.setStatus(ElementConstants.ERROR_STATUS);
			}
		} else {
			message.setResponseMessage(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@ApiOperation("Lists user groups")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserGroupsListDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/groupList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserGroups(@RequestParam("token") String token,
			@RequestParam(required = false) Boolean status, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderIn,
			@RequestParam(required = false) String orderBy, HttpServletRequest request) {

		List<Groups> groupList = groupsService.getUserGroups(status, null, pageNumber, recordsPerPage, orderBy,
				orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = groupsService.countUserGroups(status, null);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(groupList != null ? groupList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		UserGroupsListDto userGroupListDto = new UserGroupsListDto();
		userGroupListDto.setResult(groupList);
		userGroupListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * groupList); jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userGroupListDto, HttpStatus.OK);
	}

	@ApiOperation("Performs full text search operation on user groups")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserGroupsListDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/groupListFullTextSearch", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> userGroupFullTextSearch(@RequestParam("token") String token,
			@RequestParam(required = false) Boolean status, @RequestParam("keyword") String keyword,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		if (StringUtils.isEmpty(keyword) || keyword.length() < 3) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_STRING_LENTH_INVALID),
					HttpStatus.OK);
		}

		List<Groups> groupList = groupsService.getUserGroups(status, keyword, pageNumber, recordsPerPage, orderBy,
				orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = groupsService.countUserGroups(status, keyword);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(groupList != null ? groupList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		UserGroupsListDto userGroupListDto = new UserGroupsListDto();
		userGroupListDto.setResult(groupList);
		userGroupListDto.setPaginationInformation(information);
		;
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * groupList); jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userGroupListDto, HttpStatus.OK);
	}

	@ApiOperation("Gets a user group by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = Groups.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<Groups> getGroup(@RequestParam("groupId") Long groupId, @RequestParam("token") String userId,
			HttpServletRequest request) {

		Groups group = groupsService.find(groupId);
		if (group != null)
			return new ResponseEntity<Groups>(group, HttpStatus.OK);
		else
			throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
	}

	@ApiOperation("Updates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.GROUP_NAME_EXIST),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/update", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateGroup(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String adminToken, @Valid @RequestBody GroupsDto updatedGroup, BindingResult results,
			HttpServletRequest request) {

		Long adminId = getCurrentUserId();
		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Groups existing = groupsService.checkGroupExist(updatedGroup.getName(), groupId);
			if (existing == null) {
				Groups group = groupsService.find(groupId);
				if (group != null) {
					if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
							"Allow update manually-Groups").equalsIgnoreCase("On")) {
						group.setName(updatedGroup.getName());
						group.setDescription(updatedGroup.getDescription());
						group.setRemarks(updatedGroup.getRemarks());
						group.setModifiedDate(new Date());
						groupsService.saveOrUpdate(group);
						return new ResponseEntity<>(
								new ResponseMessage(ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG), HttpStatus.OK);
					} else {
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_UPDATION_DISABLED),
								HttpStatus.CONFLICT);
					}
				} else {
					throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_NAME_EXIST),
						HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}}

	@ApiOperation("Activates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_ACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/activate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> activateGroup(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {
		long adminId = getCurrentUserId();
		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Groups group = groupsService.find(groupId);
			if (group != null) {
				group.setActive(true);
				groupsService.saveOrUpdate(group);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_ACTIVATED_SUCCESSFULLY_MSG),
						HttpStatus.OK);
			} else {
				throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Deactivates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_DEACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/deactivate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deactivateGroup(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {
		long adminId = getCurrentUserId();
		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Groups group = groupsService.find(groupId);
			if (group != null) {
				group.setActive(false);
				groupsService.saveOrUpdate(group);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_DEACTIVATED_SUCCESSFULLY_MSG),
						HttpStatus.OK);
			} else {
				throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@SuppressWarnings("unused")
	@ApiOperation("Adds a user to an user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.USER_OR_GROUP_IS_INACTIVE_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/addUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addUsersGroups(@RequestParam("userId") Long userId, @RequestParam("groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {

		long adminId = getCurrentUserId();
		//if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Users user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			if (user != null) {
				Groups group = groupsService.find(groupId);
				if (group != null) {
					if (userGroupsService.getUserGroup(userId, groupId) == null) {
						if (usersService.isActiveUser(userId) && groupsService.isActiveGroup(groupId)) {
							UserGroups userGroup = new UserGroups();
							userGroup.setGroupId(group);
							userGroup.setUserId(user);
							userGroupsService.save(userGroup);
							AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.STRING_TYPE_USER_GROUP,null, 
									ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
							Users admin = null;
							if(getCurrentUserId() != null){
								admin = usersService.find(getCurrentUserId());
								admin = usersService.prepareUserFromFirebase(admin);
							}
								
							log.setDescription(admin.getFirstName() + " " + admin.getLastName() + ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
							log.setUserId(userId);
							auditLogService.save(log);
							return new ResponseEntity<>(
									new ResponseMessage(ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG),
									HttpStatus.OK);
						} else {
							return new ResponseEntity<>(
									new ResponseMessage(ElementConstants.USER_OR_GROUP_IS_INACTIVE_MSG),
									HttpStatus.BAD_REQUEST);
						}
					} else {
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_ALREADY_IN_GROUP_MSG),
								HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_NOT_FOUND_MSG),
							HttpStatus.NOT_FOUND);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG),
						HttpStatus.NOT_FOUND);
			}
		/*} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}*/
	}

	@ApiOperation("Removes a user from an user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.USER_OR_GROUP_IS_INACTIVE_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/removeUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> removeUsersGroups(@RequestParam("token") String token,
			@RequestParam("groupId") Long groupId, @RequestParam("userId") Long userId, HttpServletRequest request) {
		long adminId = getCurrentUserId();
		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			UserGroups userGroupBeforeChange = userGroupsService.getUserGroup(userId, groupId);
			if (userGroupBeforeChange != null) {
				userGroupsService.delete(userGroupBeforeChange);
			}
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Lists the users of an user group")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserListDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/listUsersByGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUsersFromGroup(@RequestParam("groupId") Long groupId,
			@RequestParam("token") String token, @RequestParam(required = false) Integer userStatus,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		long adminId = getCurrentUserId();
			// Retrieve all users from a particular group.
			List<UsersDto> userList = usersService.listUserByGroup(groupId, userStatus, pageNumber, recordsPerPage,
					orderBy, orderIn);

			// Pagination
			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = usersService.countUserByGroup(groupId, userStatus);
			PaginationInformation information = new PaginationInformation();
			information.setTotalResults(totalResults);
			information.setKind("list");
			information.setTitle(request.getRequestURI());
			information.setCount(userList != null ? userList.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");

			UserListDto usersListDto = new UserListDto();
			usersListDto.setResult(userList);
			usersListDto.setPaginationInformation(information);
			/*
			 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result", userList);
			 * jsonObject.put("paginationInformation", information);
			 */
			return new ResponseEntity<>(usersListDto, HttpStatus.OK);
		
	}

	@ApiOperation("Lists the user groups")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserListDto.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/listUserGroups", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupsFromUser(@RequestParam("token") String token, @RequestParam("userId") Long userId,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		// Retrieve all groups for a particular user.
		List<Groups> groupList = groupsService.getGroupsOfUser(userId, pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = groupsService.countGroupsOfUser(userId);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setKind("list");
		information.setTitle(request.getRequestURI());
		information.setCount(groupList != null ? groupList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		UserGroupsListDto userGroupListDto = new UserGroupsListDto();
		userGroupListDto.setResult(groupList);
		userGroupListDto.setPaginationInformation(information);
		;
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * groupList); jsonObject.put("paginationInformation", information);
		 */

		return new ResponseEntity<>(userGroupListDto, HttpStatus.OK);
	}

	@ApiOperation("Checks if the user group is active")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserGroupStatusDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/checkActiveUserGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> checkActiveUserGroup(@RequestParam("userId") Long userId,
			@RequestParam("token") String token, @RequestParam("groupId") Long groupId, HttpServletRequest request) {

		boolean status = false;
		if (usersService.isActiveUser(userId) && groupsService.isActiveGroup(groupId)) {
			UserGroups userGroup = userGroupsService.getUserGroup(userId, groupId);
			if (userGroup != null)
				status = true;
		}
		UserGroupStatusDto userGroupStatusDto = new UserGroupStatusDto();
		userGroupStatusDto.setStatus(status);
		/*
		 * JsonObject responseObject = new JsonObject();
		 * responseObject.addProperty("status", status);
		 */
		return new ResponseEntity<>(userGroupStatusDto, HttpStatus.OK);
	}

	@ApiOperation("Lists the users of an user group")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserListDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/listUsersByGroupsList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUsersFromGroupsList(@RequestParam("token") String token,HttpServletRequest request,@RequestBody List<Long> groupIds) throws Exception {
		long userId = getCurrentUserId();
		UsersListByGroupListDto resultantList=usersService.getUsersFromGroupsList(groupIds,userId);
		return new ResponseEntity<>(resultantList, HttpStatus.OK);
	}
	
	@ApiOperation("gets user group by name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getGroupByName", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupByName(@RequestParam("token") String token,
			@RequestParam String groupName, HttpServletRequest request)
			throws Exception {
		Groups group = groupsService.getGroup(groupName);
		GenericReturnObject message = new GenericReturnObject();
		if (group != null) {
			message.setData(group);
			message.setResponseMessage(ElementConstants.FETCH_GROUP_NAME_SUCCESSFUL);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
		} else {
			message.setResponseMessage(ElementConstants.FETCH_GROUP_NAME_FAIL);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);

	}
	
	
	@SuppressWarnings("unchecked")
	@ApiOperation("Gets all groups")
	@ApiResponses(value = { @ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllGroups", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllGroups(@RequestParam("token") String token,
			 HttpServletRequest request)
			throws Exception {
		List<Groups> groups = groupsService.findAll();
		JSONArray groupsArray = new JSONArray();
		if (groups != null) {
			for (Groups eachGroup : groups) {
				Map<String, Long> statusObjMap = (Map<String, Long>) groupsService
						.getStatuswiseUsersByGroupId(eachGroup.getId());
				JSONObject statusObj = groupsService.getJsonFromMap(statusObjMap);
				JSONObject groupJson = new JSONObject(eachGroup);
				if (statusObj != null && statusObj.has((UserStatus.Active.toString()))
						&& statusObj.get((UserStatus.Active.toString())) instanceof Long) {
					groupJson.put("activeUsers", statusObj.getLong((UserStatus.Active.toString())));

				} else {
					groupJson.put("activeUsers", 0L);
				}
				groupsArray.put(groupJson);
			}
		}
		GenericReturnObject message = new GenericReturnObject();
		if (groupsArray != null && groupsArray.length()>0) {
			message.setData(groupsArray.toString());
			message.setResponseMessage(ElementConstants.FETCH_GROUP_NAME_SUCCESSFUL);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
		} else {
			message.setResponseMessage(ElementConstants.FETCH_GROUP_NAME_FAIL);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);

	}
	
	@ApiOperation("Adds a user to an user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/addUserList",consumes = { "application/json; charset=UTF-8" },  produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addUsersListGroups(@RequestBody List<Long> userIds, @RequestParam("groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {

		long adminId = getCurrentUserId();
		GenericReturnObject message = new GenericReturnObject();
		Groups group = groupsService.find(groupId);
		if (group != null) {
			boolean status = groupsService.addUsersListToGroup(adminId, userIds, groupId);
			if (status) {
				message.setData(status);
				message.setResponseMessage(ElementConstants.USERS_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
				message.setStatus(ElementConstants.SUCCESS_STATUS);

			} else {
				message.setData(status);
				message.setResponseMessage(ElementConstants.USERS_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
			}
			return new ResponseEntity<>(message, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_NOT_FOUND_MSG),
					HttpStatus.OK);
		}
	}
	@ApiOperation("Gets status wise users by group ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getStatuswiseUsersByGroupId", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getStatuswiseUsersByRoleId(@RequestParam String token,
			@RequestParam Long groupId) throws Exception {
		if(groupId != null ){
			return new ResponseEntity<>(groupsService.getStatuswiseUsersByGroupId(groupId), HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_ID_EMPTY), HttpStatus.OK);
		}
	}
	

	@ApiOperation("Updates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.GROUP_NAME_EXIST),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateGroupById", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateGroupById(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String adminToken, @Valid @RequestBody UpdateGroupDto updatedGroup, BindingResult results,
			HttpServletRequest request) {
		GenericReturnObject message = new GenericReturnObject();
		if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
				"Allow update manually-Groups").equalsIgnoreCase("On")) {
			Long adminId = getCurrentUserId();
			Groups existing = groupsService.checkIfGroupExist(updatedGroup.getName(), groupId);
			if (existing == null) {
				Groups group = groupsService.find(groupId);
				if (group != null) {
					Groups groupAfterUpdate = groupsService.updateGroupById(groupId, updatedGroup, adminId);
					message.setData(groupAfterUpdate);
					message.setResponseMessage(ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
				} else {
					message.setResponseMessage(ElementConstants.GROUP_NOT_FOUND_MSG);
					message.setStatus(ElementConstants.ERROR_STATUS);
				}
			} else {
				message.setResponseMessage(ElementConstants.GROUP_NAME_EXIST);
				message.setStatus(ElementConstants.ERROR_STATUS);
			}
		} else {
			message.setStatus(ElementConstants.ERROR_STATUS);
			message.setResponseMessage(ElementConstants.GROUP_CREATION_DISABLED);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Delete the Group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/deleteGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteRole(@RequestParam(required=true) Long groupId, @RequestParam("token") String token,
			HttpServletRequest request) {
		GenericReturnObject  message= new GenericReturnObject();
		
		try{
			if(groupId != null){
				boolean isRoleDeleted=groupsService.deleteGroup(groupId, getCurrentUserId());
				if(isRoleDeleted){
					message.setResponseMessage(ElementConstants.DELETE_ROLE_SUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}else{
					message.setResponseMessage(ElementConstants.DELETE_ROLE_UNSUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
			}else{
				message.setResponseMessage(ElementConstants.EMPTY_ROLEID);
				message.setData(false);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			message.setResponseMessage(ElementConstants.ROLE_NOT_FOUND);
			message.setData(false);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		
		
		//return new ResponseEntity<>(isRoleDeleted,HttpStatus.OK);
	}
	
	@ApiOperation("Delete the Groups by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/deleteGroups", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteGroups(@RequestParam(required=true) Long groupsId, @RequestParam("token") String token, HttpServletRequest request) {
		
		GenericReturnObject  message= new GenericReturnObject();
		
		try{
			if(groupsId != null){
				boolean isRoleDeleted=groupsService.deleteGroups(groupsId, getCurrentUserId());
				if(isRoleDeleted){
					message.setResponseMessage(ElementConstants.DELETE_GROUPS_SUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}else{
					message.setResponseMessage(ElementConstants.DELETE_GROUPS_UNSUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.ERROR_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
			}else{
				message.setResponseMessage(ElementConstants.EMPTY_GROUPSID);
				message.setData(false);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			message.setResponseMessage(ElementConstants.GROUPS_DELETION_FAILED);
			message.setData(false);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		
		
		//return new ResponseEntity<>(isRoleDeleted,HttpStatus.OK);
	}
	
}
