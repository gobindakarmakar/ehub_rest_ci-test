package element.bst.elementexploration.rest.extention.significantnews.dao;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface SignificantNewsDao extends GenericDao<SignificantNews, Long> {
	
	public SignificantNews getSignificantNews(String entityId,String entityName,String publishedDate,String title,String url,String newsClass);
	
	public int deleteSignificantNews(String entityId,String entityName,String publishedDate,String title,String url,String newsClass);

}
