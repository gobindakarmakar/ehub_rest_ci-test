package element.bst.elementexploration.rest.usermanagement.user.serviceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriUtils;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.listManagement.dao.ListItemDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupUsersListDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDtoWithRolesObj;
import element.bst.elementexploration.rest.usermanagement.group.dto.UsersListByGroupListDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.security.service.LoginHistoryService;
import element.bst.elementexploration.rest.usermanagement.security.service.MailService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.AssigneeDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.DateCountStatusDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.LoginUserCountDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.LogonsDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDtoWithRolesAndGroups;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersRegistrationDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.EmailTypes;
import element.bst.elementexploration.rest.util.FilterColumnDto;
import element.bst.elementexploration.rest.util.FilterModelDto;
import element.bst.elementexploration.rest.util.GenericFilterParser;
import element.bst.elementexploration.rest.util.Mail;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import element.bst.elementexploration.rest.util.UserStatus;


/**
 * @author Paul Jalagari
 *
 */
@Service("usersService")
public class UsersServiceImpl extends GenericServiceImpl<Users, Long> implements UsersService {

	@Autowired
	public UsersServiceImpl(GenericDao<Users, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private ElementsSecurityService elementsSecurityService;
	
	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private LoginHistoryService loginHistoryService;
	
	@Autowired
	private SettingsService settingsService;

	@Value("${enable}")
	private String USER_REG_ENABLE;

	@Value("${status}")
	private String USER_STATUS;

	@Value("${emailVerification}")
	private String USER_EMAIL_VERIFICATION;
	
	@Autowired
	private MailService mailService;
	
	@Value("${validation_api}")
	private String VALIDATION_API;
	
	@Value("${rest_base_url}")
	private String REST_BASE_URL;
	
	@Value("${fe_base_url}")
	private String FE_BASE_URL;
	
	@Value("${temporaryPassword.validity.mins}")
	private String TEMP_PSWD_VAL_MINS;
	
	@Autowired
	@Lazy
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserGroupsService userGroupsService;
	
	@Autowired
	ListItemService listItemService;
	
	@Autowired
	ListItemDao listItemDao;
	
	@Autowired
	AuditLogDao auditLogDao;

	@Autowired
	ServletContext servletContext;
	
	@Autowired
	UsersService usersService;
	
	@Override
	@Transactional("transactionManager")
	public void setUserLastLoginDateAndIp(UsersDto userDto, String ip) {
		Users user = usersDao.find(userDto.getUserId());
		user.setLastLoginDate(new Date());
		user.setLastLoginIP(ip);
		user.setTemporaryPassword(null);
		user.setTemporaryPasswordExpiresOn(null);
		if (settingsService
				.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
						ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
				.equalsIgnoreCase("Off")) {
		usersDao.saveOrUpdate(user);
		}
	}

	public Properties getProperties() {
		Properties properties = new Properties();

		if (USER_REG_ENABLE.isEmpty()) {
			USER_REG_ENABLE = "0";
		}
		if (USER_STATUS.isEmpty()) {
			USER_STATUS = "0";
		}
		if (USER_EMAIL_VERIFICATION.isEmpty()) {
			USER_EMAIL_VERIFICATION = "0";
		}
		properties.setProperty("enable", USER_REG_ENABLE);

		properties.setProperty("status", USER_STATUS);
		properties.setProperty("emailVerification", USER_EMAIL_VERIFICATION);
		return properties;
	}

	@Override
	@Transactional("transactionManager")
    public void sendEmailVerification(Long userId, String userName, String url, String token, String emailAddress) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpPost post = new HttpPost(url + "/notification/rest/api/notification/send?userId=" + userId);
			StringEntity params;
			params = new StringEntity("{ \"media\":\"EMAIL\", \"metadata\":{ \"fromUser\" : { \"id\" : \"" + userId
					+ "\", " + "\"emailAddress\" : \"test@gmail.com\" }, \"subject\" : \"EHUB User Token\", "
					+ "\"body\" : \"Hello " + userName
					+ ", Thank for registering EHUB. Your account needs to be activated. "
					+ "To activate your account copy the token below and paste it to " + url
					+ "elementexploration/validateToken. " + token + " \", "
					+ "\"priority\" : \"HIGH\", \"level\" : \"WARNING\", \"requestAck\" : true, \"toUserList\" : [ {\"id\" : \""
					+ userId + "\"," + "\"emailAddress\" :\"" + emailAddress
					+ "\"} ] }, \"tags\":\"mytags\", \"token\":\"" + token + "\" }");

			post.addHeader("content-type", "application/json");
			post.setEntity(params);
			httpClient.execute(post);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Unable to send token registration", e, this.getClass());
		}
	}

	@Override
	@Transactional("transactionManager")
	public boolean activateUser(Users user) {
		//user.setStatus("1");
		try {
			if (settingsService
					.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
							ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
					.equalsIgnoreCase("Off")) {
			usersDao.saveOrUpdate(user);
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Error activating user ", e, this.getClass());
			throw e;
		}
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deactivateUser(Users user) {
		//user.setStatus("0");
		try {
			if (settingsService
					.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
							ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
					.equalsIgnoreCase("Off")) {
			usersDao.saveOrUpdate(user);
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Error activating user ", e, this.getClass());
			throw e;
		}
		return true;
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean deactivateUser(Users user, Long currentUserId) throws Exception {
		ListItem deactivateStatus = null;
		// ListItemDto deactivateStatusDto = null;
		ListItem statusFrom = null;
		ListItem statusTo = null;
		Users userBeforeChange = user;
		Users adminUser = find(currentUserId);
		ListItem activeItem = null;
		ListItem pendingItem = null;
		List<ListItem> activeStatusesList=listItemService.findAll().stream().filter(o->o.getListType().equalsIgnoreCase(ElementConstants.USER_STATUS_TYPE)).filter(o->o.getDisplayName().equalsIgnoreCase(UserStatus.Active.toString())).collect(Collectors.toList());
		if(activeStatusesList!=null && activeStatusesList.size()>0) {
			activeItem=activeStatusesList.get(0);
		}
		List<ListItem> deactiveStatusesList=listItemService.findAll().stream().filter(o->o.getListType().equalsIgnoreCase(ElementConstants.USER_STATUS_TYPE)).filter(o->o.getDisplayName().equalsIgnoreCase(UserStatus.Deactivated.toString())).collect(Collectors.toList());
		if(deactiveStatusesList!=null && deactiveStatusesList.size()>0) {
			deactivateStatus=deactiveStatusesList.get(0);
		}
		List<ListItem> pendingStatusesList=listItemService.findAll().stream().filter(o->o.getListType().equalsIgnoreCase(ElementConstants.USER_STATUS_TYPE)).filter(o->o.getDisplayName().equalsIgnoreCase(UserStatus.Pending.toString())).collect(Collectors.toList());
		if(pendingStatusesList!=null && pendingStatusesList.size()>0) {
			pendingItem=pendingStatusesList.get(0);
		}
		/* activeItem = listItemService.getListItemByListTypeAndDisplayName("User Status",
				UserStatus.Active.toString());*/

		/*deactivateStatus = listItemService.getListItemByListTypeAndDisplayName("User Status",
				UserStatus.Deactivated.toString());*/
		if ((deactivateStatus!=null)&&(activeItem != null)&&(pendingItem!=null) && (user.getStatusId() != null)
				&& ((user.getStatusId().getListItemId().equals(activeItem.getListItemId()))||(user.getStatusId().getListItemId().equals(pendingItem.getListItemId())))) {
			user.setStatusId(deactivateStatus);
			try {
				if (settingsService
						.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
								ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
						.equalsIgnoreCase("Off")) {
				saveOrUpdate(user);
				}
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.USER_MANAGEMENT_TYPE, null,
						ElementConstants.USER_MANAGEMENT_TYPE);
				if (userBeforeChange.getStatusId() != null) {
					statusFrom = listItemService.find(userBeforeChange.getStatusId().getListItemId());
				}
				if (user.getStatusId() != null) {
					statusTo = listItemService.find(user.getStatusId().getListItemId());
				}
				log.setDescription(
						adminUser.getFirstName() + " " + adminUser.getLastName() + " changed " + user.getFirstName()
								+ " " + user.getLastName() + "'s status from " + statusFrom.getDisplayName() + " to " + statusTo.getDisplayName());
				log.setName(ElementConstants.USER_MANAGEMENT_TYPE);
				log.setUserId(adminUser.getUserId());
				log.setType(ElementConstants.USER_MANAGEMENT_TYPE);
				log.setTypeId(user.getUserId());
				auditLogDao.create(log);
				return true;
			} catch (HibernateException e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, "Error deactivating user ", e, this.getClass());
				throw e;
			}
			// }
		}

		return false;
	}

	@Override
	@Transactional("transactionManager")
	public List<UsersDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		return usersDao.getUserList(groupId, status,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public List<UsersDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn) {
		return usersDao.getUserListFullTextSearch(keyword, status, groupId,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public boolean isActiveUser(Long userId) {
		boolean retVal = true;

		Users user = usersDao.find(userId);
		user = usersService.prepareUserFromFirebase(user);
/*
		if (null != user) {
			if (null != user.getStatus()) {
				if (user.getStatus().equalsIgnoreCase("0")) {
					retVal = false;
				}
			} else {
				retVal = false;
			}
		}*/

		return retVal;
	}

	@Override
	@Transactional("transactionManager")
	public List<UsersDto> listUserByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {

		pageNumber = (pageNumber == null) ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber;
		recordsPerPage = (recordsPerPage == null) ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;

		return usersDao.listUsersByGroup(groupId, userStatus, pageNumber, recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public UsersDto getUserByEmailOrUsername(String email) {
		UsersDto userDto = null;
		if (email != null) {
			Users user = usersDao.getUserByEmailOrUsername(email);
			user = prepareUserFromFirebase(user);
			if (user != null) {
				userDto = new UsersDto();
				BeanUtils.copyProperties(user, userDto);
			}
		}
		return userDto;
	}

	@Override
	@Transactional("transactionManager")
	public Users getUserObjectByEmailOrUsername(String email) {
		Users user = null;
		if (email != null) {
			user = usersDao.getUserByEmailOrUsername(email);
			user = prepareUserFromFirebase(user);
		}
		return user;
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserList(Long groupId, String status) {
		return usersDao.countUserList(groupId, status);
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserListFullTextSearch(String keyword, String status, Long groupId) {
		return usersDao.countUserListFullTextSearch(keyword, status, groupId);
	}

	@Override
	@Transactional("transactionManager")
	public UsersDto checkUserNameOrEmailExists(String email, Long id) {
		UsersDto userDto = null;
		if (email != null) {
			Users user = usersDao.checkUserNameOrEmailExists(email, id);
			user = prepareUserFromFirebase(user);
			if (user != null) {
				userDto = new UsersDto();
				BeanUtils.copyProperties(user, userDto);
			}
		}
		return userDto;
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserByGroup(Long groupId, Integer userStatus) {
		return usersDao.countUserByGroup(groupId, userStatus);
	}

	@Override
	@Transactional("transactionManager")
	public UsersListByGroupListDto getUsersFromGroupsList(List<Long> groupIds, long userId) throws Exception {
		UsersListByGroupListDto resultantList = new UsersListByGroupListDto();
		List<GroupUsersListDto> groupsUsersList = new ArrayList<>();
		if (groupIds != null && groupIds.size() > 0) {
			for (Long eachGroupId : groupIds) {
				List<UsersDto> groupUsers = usersDao.listUsersByGroupWithoutPagination(eachGroupId);
				GroupUsersListDto eachGroupUsers = new GroupUsersListDto();
				eachGroupUsers.setGroupId(eachGroupId);
				eachGroupUsers.setUserList(groupUsers);
				groupsUsersList.add(eachGroupUsers);
			}
		}
		resultantList.setResult(groupsUsersList);
		return resultantList;
	}

	@Override
	@Transactional("transactionManager")
	public Long activeUsersCount() {
		
		return usersDao.activeUsersCount();
		
	}
	@Override
	@Transactional("transactionManager")
	public boolean sendEmailForVerification(UsersDto userAfterChange, HttpServletRequest request) throws Exception {
		String remoteAddr;
		if (null != userAfterChange) {
			Users userData = find(userAfterChange.getUserId());
			String tempPassword = RandomStringUtils.randomAlphanumeric(10);
			userData.setTemporaryPassword(passwordEncoder.encode(tempPassword));
			Date currentDate = new Date();
			Calendar calenderDate = Calendar.getInstance();
			calenderDate.setTime(currentDate);
			calenderDate.add(Calendar.DATE, 1);
			userData.setTemporaryPasswordExpiresOn(calenderDate.getTime());
			saveOrUpdate(userData);
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
			/*String validationUrl = new String(
					request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/");*/
			Map<String, String> mailSettings = new HashMap<>();
			Mail mail = new Mail();
			// get mail from system settings
			mailSettings = settingsService.getMailSettings();
			mail.setMailFrom(mailSettings.get("From"));
			mail.setMailTo(userData.getEmailAddress());
			mail.setMailSubject("Verify Your Account");

			String token = userData.getUserId() + "-" + tempPassword;
			/*final Properties properties = new Properties();
			properties.load(getClass().getClassLoader().getResourceAsStream("ehubrest.properties"));*/
			//String warName = new File(request.getServletContext().getRealPath("/")).getName();
			//String warName = properties.getProperty("artifactId");
			//String link = UriUtils.encodePath(FE_BASE_URL + "/login?" + token+"/"+EmailTypes.REGISTRATION.toString().toLowerCase(), "UTF-8");
			String link = FE_BASE_URL + "/login?" + token+"/"+EmailTypes.REGISTRATION.toString().toLowerCase();
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("firstName", userData.getFirstName());
			model.put("lastName", userData.getLastName());
			model.put("location", link);
			mail.setModel(model);
			try {
				mailService.sendEmail(mail,EmailTypes.REGISTRATION.toString().toLowerCase());
				return true;
			} catch (Exception e) {
				return false;
			}

		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public Map<List<AssigneeDto>, Long> getUsers(FilterModelDto filterDto, Boolean isAllRequired,
			Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy) {
		Map<List<AssigneeDto>, Long> returnResult = new HashMap<>();
		Long totalCount = 0L;
		Map<List<Users>, Long> filteredResult = null;
		List<Users> usersList = null;
		Users userClass = new Users();
		
		if(isAllRequired){
			usersList = usersDao.findAll();
		}else{
			filteredResult = filterData(userClass, filterDto, pageNumber, recordsPerPage, totalResults, orderIn, orderBy);
			for(Entry<List<Users>, Long> entry : filteredResult.entrySet()){
				usersList = entry.getKey();
				totalCount = entry.getValue();
			}
		}
		List<AssigneeDto> userDtosList = new ArrayList<AssigneeDto>();
		if (usersList != null) {
			/*userDtosList = usersList.stream()
					.map((userdto) -> new EntityConverter<Users, UsersDto>(Users.class, UsersDto.class)
							.toT2(userdto, new UsersDto()))
					.collect(Collectors.toList());*/
			
			for(Users user : usersList){
				try {
					AssigneeDto userDto = new AssigneeDto();
					//user = prepareUserFromFirebase(user);									
					 BeanUtils.copyProperties(user, userDto);
					 
					 userDto.setUsersGroups(groupsService.getGroupsOfUser(userDto.getUserId()));
					 List<UserRolesDtoWithRolesObj> userRolesDtoList= new ArrayList<>();
					 userRolesDtoList = userRolesService.getRolesOfUserDtoWithRoleObj(userDto.getUserId(),true);
					 userDto.setUsersRoles(userRolesDtoList);
					 userDto.setIsModifiable(user.getIsModifiable());
					userDtosList.add(userDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		
		}
		returnResult.put(userDtosList, totalCount);
		return returnResult;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public boolean updateUser(Users user, UsersRegistrationDto userRegistrationDto, Long currentUserId)
			throws Exception {
		boolean isCountryChanged = false;
		boolean isDepartmentChanged = false;
		boolean isDOBChanged = false;
		// boolean isEmailAddressChanged = false;
		boolean isExtensionChanged = false;
		boolean isFirstNameChanged = false;
		boolean isJobTitleChanged = false;
		boolean isLastNameChanged = false;
		boolean isMiddleNameChanged = false;
		boolean isPhoneNumberChanged = false;
		boolean isSourceChanged = false;
		boolean isModifiabilityChanged=false;
		boolean isStatusChanged=false;
		boolean isAnyThingChanged=false;
		List<Long> roleIdsToBeAdded = null;
		List<Long> groupIdsToBeAdded = null;
		List<Long> roleIdsToBeRemoved = null;
		List<Long> groupIdsToBeRemoved = null;
		String statusTo=null;
		Users currentUser = find(currentUserId);
		StringBuilder auditString = new StringBuilder();
		if (currentUser != null)
			auditString = auditString
					.append(currentUser.getFirstName() + " " + currentUser.getLastName() + " changed ");
		if (userRegistrationDto != null && user != null) {
			if (userRegistrationDto.getUserId() != null) {
				Users existingUserBeforeChange= new Users();
				Users existingUser = find(userRegistrationDto.getUserId());
				BeanUtils.copyProperties(existingUser, existingUserBeforeChange);
				Users userAfterChange = find(userRegistrationDto.getUserId());
				if (existingUserBeforeChange != null) {
					auditString.append(existingUserBeforeChange.getFirstName() + " "
							+ existingUserBeforeChange.getLastName() + "'s ");
					// country update
					if ((userRegistrationDto.getCountry() == null && existingUserBeforeChange.getCountryId() != null)
							|| (userRegistrationDto.getCountry() != null && ((existingUserBeforeChange
									.getCountryId() == null)
									|| (existingUserBeforeChange.getCountryId() != null
											&& (!userRegistrationDto.getCountry().equals(
													existingUserBeforeChange.getCountryId().getListItemId())))))) {
						if (userRegistrationDto.getCountry() != null) {
							ListItem countryItem = listItemService.find(userRegistrationDto.getCountry());
							if (countryItem != null) {
								userAfterChange.setCountryId(countryItem);
								isCountryChanged = true;
								isAnyThingChanged=true;
							}
						} else {
							userAfterChange.setCountryId(null);
							isCountryChanged = true;
							isAnyThingChanged=true;
						}
					}
					// department update
					if ((userRegistrationDto.getDepartment() == null
							&& existingUserBeforeChange.getDepartment() != null)
							|| (userRegistrationDto.getDepartment() != null
									&& ((existingUserBeforeChange.getDepartment() == null)
											|| (existingUserBeforeChange.getDepartment() != null
													&& (!userRegistrationDto.getDepartment()
															.equals(existingUserBeforeChange.getDepartment())))))) {
						if (userRegistrationDto.getDepartment() != null) {
							userAfterChange.setDepartment(userRegistrationDto.getDepartment());
							isDepartmentChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setDepartment(null);
							isDepartmentChanged = true;
							isAnyThingChanged=true;
						}
					}

					// DOB update
					if ((userRegistrationDto.getDob() == null && existingUserBeforeChange.getDob() != null)
							|| (userRegistrationDto.getDob() != null && ((existingUserBeforeChange.getDob() == null)
									|| (existingUserBeforeChange.getDob() != null && (!userRegistrationDto.getDob()
											.equals(existingUserBeforeChange.getDob())))))) {
						if (userRegistrationDto.getDob() != null) {
							userAfterChange.setDob(userRegistrationDto.getDob());
							isDOBChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setDob(null);
							isDOBChanged = true;
							isAnyThingChanged=true;
						}
					}
					// email update
					/*
					 * if ((userRegistrationDto.getEmailAddress() == null &&
					 * existingUserBeforeChange.getEmailAddress() != null) ||
					 * (userRegistrationDto.getEmailAddress() != null &&
					 * ((existingUserBeforeChange.getEmailAddress() == null) ||
					 * (existingUserBeforeChange.getEmailAddress() != null &&
					 * (!userRegistrationDto.getEmailAddress()
					 * .equals(existingUserBeforeChange.getEmailAddress())))))) { if
					 * (userRegistrationDto.getEmailAddress() != null) { Users userWithMail =
					 * getUserObjectByEmailOrUsername(userRegistrationDto.getEmailAddress()); if
					 * (userWithMail == null) {
					 * userAfterChange.setEmailAddress(userRegistrationDto.getEmailAddress());
					 * isDOBChanged = true; } else { if
					 * (userWithMail.getUserId().equals(existingUserBeforeChange.getUserId())) { //
					 * doNothing } } } else { // userAfterChange.setEmailAddress(null); //
					 * isDOBChanged = true; } }
					 */
					// extension update
					if ((userRegistrationDto.getExtension() == null && existingUserBeforeChange.getExtension() != null)
							|| (userRegistrationDto.getExtension() != null
									&& ((existingUserBeforeChange.getExtension() == null)
											|| (existingUserBeforeChange.getExtension() != null
													&& (!userRegistrationDto.getExtension()
															.equals(existingUserBeforeChange.getExtension())))))) {
						if (userRegistrationDto.getExtension() != null) {
							userAfterChange.setExtension(userRegistrationDto.getExtension());
							isExtensionChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setExtension(null);
							isExtensionChanged = true;
							isAnyThingChanged=true;
						}
					}
					// first name update
					if ((userRegistrationDto.getFirstName() == null && existingUserBeforeChange.getFirstName() != null)
							|| (userRegistrationDto.getFirstName() != null
									&& ((existingUserBeforeChange.getFirstName() == null)
											|| (existingUserBeforeChange.getFirstName() != null
													&& (!userRegistrationDto.getFirstName()
															.equals(existingUserBeforeChange.getFirstName())))))) {
						if (userRegistrationDto.getFirstName() != null) {
							userAfterChange.setFirstName(userRegistrationDto.getFirstName());
							isFirstNameChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setFirstName(null);
							isFirstNameChanged = true;
							isAnyThingChanged=true;
						}
					}
					// Job title update
					if ((userRegistrationDto.getJobTitle() == null && existingUserBeforeChange.getJobTitle() != null)
							|| (userRegistrationDto.getJobTitle() != null
									&& ((existingUserBeforeChange.getJobTitle() == null)
											|| (existingUserBeforeChange.getJobTitle() != null && (!userRegistrationDto
													.getJobTitle().equals(existingUserBeforeChange.getJobTitle())))))) {
						if (userRegistrationDto.getJobTitle() != null) {
							userAfterChange.setJobTitle(userRegistrationDto.getJobTitle());
							isJobTitleChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setJobTitle(null);
							isJobTitleChanged = true;
							isAnyThingChanged=true;
						}
					}
					// last name update
					if ((userRegistrationDto.getLastName() == null && existingUserBeforeChange.getLastName() != null)
							|| (userRegistrationDto.getLastName() != null
									&& ((existingUserBeforeChange.getLastName() == null)
											|| (existingUserBeforeChange.getLastName() != null && (!userRegistrationDto
													.getLastName().equals(existingUserBeforeChange.getLastName())))))) {
						if (userRegistrationDto.getLastName() != null) {
							userAfterChange.setLastName(userRegistrationDto.getLastName());
							isLastNameChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setLastName(null);
							isLastNameChanged = true;
							isAnyThingChanged=true;
						}
					}
					// middle name update
					if ((userRegistrationDto.getMiddleName() == null
							&& existingUserBeforeChange.getMiddleName() != null)
							|| (userRegistrationDto.getMiddleName() != null
									&& ((existingUserBeforeChange.getMiddleName() == null)
											|| (existingUserBeforeChange.getMiddleName() != null
													&& (!userRegistrationDto.getMiddleName()
															.equals(existingUserBeforeChange.getMiddleName())))))) {
						if (userRegistrationDto.getMiddleName() != null) {
							userAfterChange.setMiddleName(userRegistrationDto.getMiddleName());
							isMiddleNameChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setMiddleName(null);
							isMiddleNameChanged = true;
							isAnyThingChanged=true;
						}
					}
					// phone number update
					if ((userRegistrationDto.getPhoneNumber() == null
							&& existingUserBeforeChange.getPhoneNumber() != null)
							|| (userRegistrationDto.getPhoneNumber() != null
									&& ((existingUserBeforeChange.getPhoneNumber() == null)
											|| (existingUserBeforeChange.getPhoneNumber() != null
													&& (!userRegistrationDto.getPhoneNumber()
															.equals(existingUserBeforeChange.getPhoneNumber())))))) {
						if (userRegistrationDto.getPhoneNumber() != null) {
							userAfterChange.setPhoneNumber(userRegistrationDto.getPhoneNumber());
							isPhoneNumberChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setPhoneNumber(null);
							isPhoneNumberChanged = true;
							isAnyThingChanged=true;
						}
					}
					// Source update
					if ((userRegistrationDto.getSource() == null && existingUserBeforeChange.getSource() != null)
							|| (userRegistrationDto.getSource() != null
									&& ((existingUserBeforeChange.getSource() == null)
											|| (existingUserBeforeChange.getSource() != null && (!userRegistrationDto
													.getSource().equals(existingUserBeforeChange.getSource())))))) {
						if (userRegistrationDto.getSource() != null) {
							userAfterChange.setSource(userRegistrationDto.getSource());
							isSourceChanged = true;
							isAnyThingChanged=true;
						} else {
							userAfterChange.setSource(null);
							isSourceChanged = true;
							isAnyThingChanged=true;
						}
					}
					
					//status update
					if ((userRegistrationDto.getUserStatus() == null && existingUserBeforeChange.getStatusId() != null)
							|| (userRegistrationDto.getUserStatus() != null
									&& ((existingUserBeforeChange.getStatusId() == null)
											|| (existingUserBeforeChange.getStatusId() != null
													&& (!userRegistrationDto.getUserStatus()
															.equals(existingUserBeforeChange.getStatusId()
																	.getListItemId())))))) {
						if (userRegistrationDto.getUserStatus() != null) {
							ListItem statusItem = listItemService.find(userRegistrationDto.getUserStatus());
							if (statusItem != null) {
								userAfterChange.setStatusId(statusItem);
								isStatusChanged = true;
								isAnyThingChanged=true;
							}
						} else {
							userAfterChange.setStatusId(null);
							isStatusChanged = true;
							isAnyThingChanged=true;
						}
					}
					
					if(Boolean.compare(userRegistrationDto.getIsModifiable(), existingUserBeforeChange.getIsModifiable())!=0) {
						userAfterChange.setIsModifiable(userRegistrationDto.getIsModifiable());
						isModifiabilityChanged=true;
						isAnyThingChanged=true;
					}

					// roles/groups to be added/removed
					if (null != userRegistrationDto.getRolesToBeAdded()
							&& userRegistrationDto.getRolesToBeAdded().size() > 0) {
						roleIdsToBeAdded = userRegistrationDto.getRolesToBeAdded();
					}
					if (null != userRegistrationDto.getGroupsToBeAdded()
							&& userRegistrationDto.getGroupsToBeAdded().size() > 0) {
						groupIdsToBeAdded = userRegistrationDto.getGroupsToBeAdded();
					}
					if (null != userRegistrationDto.getRolesToBeRemoved()
							&& userRegistrationDto.getRolesToBeRemoved().size() > 0) {
						roleIdsToBeRemoved = userRegistrationDto.getRolesToBeRemoved();
					}
					if (null != userRegistrationDto.getGroupsToBeRemoved()
							&& userRegistrationDto.getGroupsToBeRemoved().size() > 0) {
						groupIdsToBeRemoved = userRegistrationDto.getGroupsToBeRemoved();
					}
					if (null != roleIdsToBeAdded && roleIdsToBeAdded.size() > 0) {
						boolean isRolesSaved = userRolesService.saveUserRoles(userAfterChange, roleIdsToBeAdded,
								currentUserId,ElementConstants.USER_MANAGEMENT_TYPE);
					}
					if (null != groupIdsToBeAdded && groupIdsToBeAdded.size() > 0) {
						boolean isGroupsSaved = userGroupsService.saveUserGroups(userAfterChange, groupIdsToBeAdded,
								currentUserId,ElementConstants.USER_MANAGEMENT_TYPE);
					}
					if (null != roleIdsToBeRemoved && roleIdsToBeRemoved.size() > 0) {
						boolean isRolesRemoved = userRolesService.removeUserRoles(userAfterChange, roleIdsToBeRemoved,
								currentUserId,ElementConstants.USER_MANAGEMENT_TYPE);
					}
					if (null != groupIdsToBeRemoved && groupIdsToBeRemoved.size() > 0) {
						boolean isGroupsRemoved = userGroupsService.removeUserGroups(userAfterChange,
								groupIdsToBeRemoved, currentUserId,ElementConstants.USER_MANAGEMENT_TYPE);
					}
					if (isCountryChanged) {
						String countryFrom = null;
						String countryTo = null;
						ListItem countryItemFrom = null;
						ListItem countryItemTO = null;
						if (null != existingUserBeforeChange.getCountryId())
							countryItemFrom = listItemService
									.find(existingUserBeforeChange.getCountryId().getListItemId());
						if (null != userAfterChange.getCountryId())
							countryItemTO = listItemService.find(userAfterChange.getCountryId().getListItemId());
						if (countryItemFrom != null)
							countryFrom = countryItemFrom.getDisplayName();
						if (countryItemTO != null)
							countryTo = countryItemTO.getDisplayName();
						if (countryFrom == null || StringUtils.isEmpty(countryFrom))
							countryFrom = ElementConstants.EMPTY;
						if (countryTo == null || StringUtils.isEmpty(countryTo))
							countryTo = ElementConstants.EMPTY;
						auditString = auditString.append(" country from " + countryFrom + " to " + countryTo + ",");
					}
					if (isStatusChanged) {
						String From = null;
						String To = null;
						ListItem statusItemFrom = null;
						ListItem statusItemTO = null;
						if (null != existingUserBeforeChange.getStatusId())
							statusItemFrom = listItemService
									.find(existingUserBeforeChange.getStatusId().getListItemId());
						if (null != userAfterChange.getStatusId())
							statusItemTO = listItemService.find(userAfterChange.getStatusId().getListItemId());
						if (statusItemFrom != null)
							From = statusItemFrom.getDisplayName();
						if (statusItemTO != null)
							To = statusItemTO.getDisplayName();
						if (From == null||StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;
						statusTo=To;
						auditString = auditString.append(" status from " + From + " to " + To + ",");
					}
					if (isDepartmentChanged) {
						String departmentFrom = null;
						String departmentTo = null;
						if (null != existingUserBeforeChange.getDepartment())
							departmentFrom = existingUserBeforeChange.getDepartment();
						if (null != userAfterChange.getDepartment())
							departmentTo = userAfterChange.getDepartment();
						if (departmentFrom == null|| StringUtils.isEmpty(departmentFrom))
							departmentFrom = ElementConstants.EMPTY;
						if (departmentTo == null|| StringUtils.isEmpty(departmentTo))
							departmentTo = ElementConstants.EMPTY;
						auditString = auditString
								.append(" department from " + departmentFrom + " to " + departmentTo + ",");
					}
					if (isDOBChanged) {
						Date dobFrom = null;
						Date dobTo = null;
						if (null != existingUserBeforeChange.getDob())
							dobFrom = existingUserBeforeChange.getDob();
						if (null != userAfterChange.getDob())
							dobTo = userAfterChange.getDob();
						if (dobFrom == null)
							dobFrom = new Date();
						if (dobTo == null)
							dobTo = new Date();
						auditString = auditString.append(" date of birth from " + dobFrom + " to " + dobTo + ",");
					}
					if (isExtensionChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getExtension())
							From = existingUserBeforeChange.getExtension();
						if (null != userAfterChange.getExtension())
							To = userAfterChange.getExtension();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null || StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" extension from " + From + " to " + To + ",");
					}
					if (isFirstNameChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getFirstName())
							From = existingUserBeforeChange.getFirstName();
						if (null != userAfterChange.getFirstName())
							To = userAfterChange.getFirstName();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" first name from " + From + " to " + To + ",");
					}
					if (isJobTitleChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getJobTitle())
							From = existingUserBeforeChange.getJobTitle();
						if (null != userAfterChange.getJobTitle())
							To = userAfterChange.getJobTitle();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" job title from " + From + " to " + To + ",");
					}
					if (isLastNameChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getLastName())
							From = existingUserBeforeChange.getLastName();
						if (null != userAfterChange.getLastName())
							To = userAfterChange.getLastName();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" last name from " + From + " to " + To + ",");
					}
					if (isMiddleNameChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getMiddleName())
							From = existingUserBeforeChange.getMiddleName();
						if (null != userAfterChange.getMiddleName())
							To = userAfterChange.getMiddleName();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" middle name from " + From + " to " + To + ",");
					}
					if (isPhoneNumberChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getPhoneNumber())
							From = existingUserBeforeChange.getPhoneNumber();
						if (null != userAfterChange.getPhoneNumber())
							To = userAfterChange.getPhoneNumber();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" phone number from " + From + " to " + To + ",");
					}
					if (isSourceChanged) {
						String From = null;
						String To = null;
						if (null != existingUserBeforeChange.getSource())
							From = existingUserBeforeChange.getSource();
						if (null != userAfterChange.getSource())
							To = userAfterChange.getSource();
						if (From == null|| StringUtils.isEmpty(From))
							From = ElementConstants.EMPTY;;
						if (To == null|| StringUtils.isEmpty(To))
							To = ElementConstants.EMPTY;;
						auditString = auditString.append(" source from " + From + " to " + To + ",");
					}
					if(isModifiabilityChanged) {
						boolean From = false;
						boolean To = false;
							From = existingUserBeforeChange.getIsModifiable();
						if (null != userAfterChange.getIsModifiable())
							To = userAfterChange.getIsModifiable();
						auditString = auditString.append(" modifiability from " + From + " to " + To + ",");
					}
					String finalString = "";
					if (auditString.toString().endsWith(",")) {
						finalString = auditString.toString().substring(0, auditString.lastIndexOf(","));
					}else
						finalString=auditString.toString();
					//saves User
					saveOrUpdate(userAfterChange);
					
					//Audit Log
					if (isAnyThingChanged) {
						AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.USER_MANAGEMENT_TYPE,
								null, ElementConstants.USER_MANAGEMENT_TYPE);
						log.setDescription(finalString + " ");
						log.setName(currentUser.getFirstName() + currentUser.getLastName());
						log.setUserId(currentUser.getUserId());
						log.setTypeId(currentUser.getUserId());
						if (isStatusChanged)
							log.setSubType(statusTo);
						log.setTypeId(existingUserBeforeChange.getUserId());
						auditLogDao.create(log);
					}
					return true;
				} 
			}
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public UsersDtoWithRolesAndGroups getUserProfileById(Long userId, Long currentUserId) throws Exception {
		UsersDtoWithRolesAndGroups usersDtoWithRolesAndGroups = new UsersDtoWithRolesAndGroups();
		Users user = null;
		List<Roles> roleslist = new ArrayList<>();
		List<Groups> groupsList = null;
		if (userId != null) {
			user = find(userId);
			user = prepareUserFromFirebase(user);
			BeanUtils.copyProperties(user, usersDtoWithRolesAndGroups);
			if (user != null && user.getUserRoles() != null) {
				List<UserRolesDtoWithRolesObj> userRolesDtoWithRolesObjList = userRolesService
						.getRolesOfUserDtoWithRoleObj(user.getUserId(),true);
				if (userRolesDtoWithRolesObjList != null && userRolesDtoWithRolesObjList.size() > 0) {
					for (UserRolesDtoWithRolesObj eachObj : userRolesDtoWithRolesObjList) {
						if (eachObj.getRoleId() != null) {
							roleslist.add(eachObj.getRoleId());
						}
					}
				}
			}
			if (user != null && user.getUserRoles() != null) {
				groupsList = groupsService.getGroupsOfUser(user.getUserId());
			}
			if (user != null && null != user.getUserImage())
				usersDtoWithRolesAndGroups.setUserImage(user.getUserImage());
		}
		if (roleslist != null)
			usersDtoWithRolesAndGroups.setRoles(roleslist);
		if (groupsList != null)
			usersDtoWithRolesAndGroups.setGroups(groupsList);
		return usersDtoWithRolesAndGroups;
	}

	@Override
	@Transactional("transactionManager")
	public List<UsersDtoWithRolesAndGroups> quickSearch(Long currentUserId, String searchKey,boolean isScreeNameRequired) throws Exception {
		List<UsersDtoWithRolesAndGroups> usersList=new ArrayList<>();
		List<Long> userIds= usersDao.quickSearch(searchKey,isScreeNameRequired);
		for(Long userId:userIds) {
			UsersDtoWithRolesAndGroups userData=getUserProfileById(userId,currentUserId);
			usersList.add(userData);
		}
		return usersList;
	}

	@Override
	@Transactional("transactionManager")
	public List<Long> getUserIdByRolesList(List<Long> comingIdList) {
		// TODO Auto-generated method stub
		return usersDao.getUserIdByRolesList(comingIdList);
	}

	@Override
	@Transactional("transactionManager")
	public List<Long> getUserIdByGroupsList(List<Long> comingIdList) {
		// TODO Auto-generated method stub
		return usersDao.getUserIdByGroupsList(comingIdList);
	}
	
	@Override
	@Transactional("transactionManager")
	public List<DateCountStatusDto> getCountByStatusWise(Long currentUserId) throws Exception {
		List<DateCountStatusDto> statusWiseList = new ArrayList<>();
		 statusWiseList=usersDao.getCountByStatusWise();
		return statusWiseList;
	}

	@Override
	@Transactional("transactionManager")
	public Map<Object, Long> getLogonsByHourCount(String period, String from, String to) {
		// TODO Auto-generated method stub
		Date fromDate = null;
		Date toDate = null;
		if(from != null && to != null){
			try{
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String date1 = from+" 00:00:00";
				String date2 = to+" 23:59:59";
				
				fromDate=format.parse(date1);
				toDate=format.parse(date2);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return usersDao.getLogonsByHourCount(period, fromDate, toDate);
	}
	
	@Override
	@Transactional("transactionManager")
	public Map<Object, Long> getFailedLogonsCount(String period, String from, String to) {
		// TODO Auto-generated method stub
				Date fromDate = null;
				Date toDate = null;
				if(from != null && to != null){
					try{
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String date1 = from+" 00:00:00";
						String date2 = to+" 23:59:59";
						
						fromDate=format.parse(date1);
						toDate=format.parse(date2);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				return usersDao.getFailedLogonsCount(period, fromDate, toDate);
	}

	/*@Override
	@Transactional("transactionManager")
	public Map<List<LogonsDto>, Long> getLogonFailures(FilterModelDto filterDto, Boolean isAllRequired, Integer pageNumber,
			Integer recordsPerPage, Long totalResults, String orderIn, String orderBy) {
		
		List<FilterColumnDto> columnsList = GenericFilterParser.filterDataParser(filterDto);
		
		//Temporary from and to dates
		String from = "2019-12-20";
		String to = "2019-12-27";
		Date fromDate = null;
		Date toDate = null;
		if(from != null && to != null){
			try{
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String date1 = from+" 00:00:00";
				String date2 = to+" 23:59:59";
				
				fromDate=format.parse(date1);
				toDate=format.parse(date2);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		//List<Users> usersList = usersDao.findAll();
		//List<String> usernamesList = usersList.stream().map(Users::getScreenName).collect(Collectors.toList());
		List<LoginHistory> loginHistoryList = loginHistoryService.getlogonsByUsernames(null, fromDate ,toDate);
		List<String> usernamesList = loginHistoryList.stream().map(LoginHistory::getUsername).collect(Collectors.toList());
		List<Users> usersList = usersDao.getUsersByUsernames(usernamesList);
		
		Map<String, List<LoginHistory>> loginHistoryListMap = loginHistoryList.stream()
				 .collect(Collectors.groupingBy(l -> l.getUsername()));
		
		List<LogonsDto> logonDtoList = new ArrayList<>();
		for(Map.Entry<String, List<LoginHistory>> map : loginHistoryListMap.entrySet()){
			String fullname = null;
			LogonsDto logonDto = new LogonsDto();
			Users user = usersList.stream().filter(u -> u.getScreenName().equals(map.getKey())).findFirst().orElse(null);
			if(user != null){
				if(null != user.getFirstName()){
					if(null != user.getMiddleName() && null != user.getLastName()){
						fullname = user.getFirstName() + " "+ user.getMiddleName()+ " " + user.getLastName();
					}else if(null == user.getMiddleName() && null != user.getLastName()){
						fullname = user.getFirstName()+ " " + user.getLastName();
					}else{
						fullname = user.getFirstName()+ " " + user.getMiddleName();
					}
				}else{
					fullname = map.getKey();
				}
			}else{
				fullname = map.getKey();
			}
			//full name
			logonDto.setFullName(fullname);
			//status
			if(null != user.getStatusId() && null != user.getStatusId().getDisplayName())
				logonDto.setStatus(user.getStatusId().getDisplayName());
			//role
			if(null != user.getUserRoles()){
				List<Roles> roles = user.getUserRoles().stream().map(UserRoles::getRoleId).collect(Collectors.toList());
				logonDto.setRoles(roles);
			}
			//last login
			if(null != user.getLastLoginDate()){
				logonDto.setLoginTime(user.getLastLoginDate());
			}
			
			//no of logons
			logonDto.setNoOfLogons(map.getValue().size());
			
			//failed logins
			List<LoginHistory> failedLogins = map.getValue().stream()
					.filter(p -> p.getLoginStatus().equalsIgnoreCase(ElementConstants.FAILED_STATUS))
					.collect(Collectors.toList());
			logonDto.setFailedList(failedLogins);
			//failed logins count
			logonDto.setLogonFailures(failedLogins.size());
			
			logonDtoList.add(logonDto);
		}
		
	return logonDtoList;
	
	//
	Map<List<LogonsDto>, Long> returnResult = new HashMap<>();
	Long totalCount = 0L;
	Map<List<LoginHistory>, Long> filteredResult = null;
	List<LoginHistory> loginList = null;
	LoginHistory loginHistoryClass = new LoginHistory();

	if (isAllRequired) {
		loginList = loginHistoryService.findAll();
	} else {
		//alertList = alertManagementDao.getAlerts(pageNumber, recordsPerPage, orderIn, orderBy, feedId);
		//filteredResult = loginHistoryService.loginfilterData(loginHistoryClass, filterDto, pageNumber, recordsPerPage, totalResults, orderIn, orderBy);
		for(Entry<List<LoginHistory>, Long> entry : filteredResult.entrySet()){
			loginList = entry.getKey();
			totalCount = entry.getValue();
		}
		
	}
	List<String> usernamesList = loginList.stream().map(LoginHistory::getUsername).collect(Collectors.toList());
	List<Users> usersList = usersDao.getUsersByUsernames(usernamesList);
	List<LogonsDto> logonDtoList = new ArrayList<LogonsDto>();
	if (loginList != null) {
		Map<String, List<LoginHistory>> loginHistoryListMap = loginList.stream()
				 .collect(Collectors.groupingBy(l -> l.getUsername()));
		
		//List<LogonsDto> logonDtoList = new ArrayList<>();
		for(Map.Entry<String, List<LoginHistory>> map : loginHistoryListMap.entrySet()){
			String fullname = null;
			LogonsDto logonDto = new LogonsDto();
			Users user = usersList.stream().filter(u -> u.getScreenName().equals(map.getKey())).findFirst().orElse(null);
			if(user != null){
				if(null != user.getFirstName()){
					if(null != user.getMiddleName() && null != user.getLastName()){
						fullname = user.getFirstName() + " "+ user.getMiddleName()+ " " + user.getLastName();
					}else if(null == user.getMiddleName() && null != user.getLastName()){
						fullname = user.getFirstName()+ " " + user.getLastName();
					}else{
						fullname = user.getFirstName()+ " " + user.getMiddleName();
					}
				}else{
					fullname = map.getKey();
				}
			}else{
				fullname = map.getKey();
			}
			//full name
			logonDto.setFullName(fullname);
			//status
			if(null != user.getStatusId() && null != user.getStatusId().getDisplayName())
				logonDto.setStatus(user.getStatusId().getDisplayName());
			//role
			if(null != user.getUserRoles()){
				List<Roles> roles = user.getUserRoles().stream().map(UserRoles::getRoleId).collect(Collectors.toList());
				logonDto.setRoles(roles);
			}
			//last login
			if(null != user.getLastLoginDate()){
				logonDto.setLoginTime(user.getLastLoginDate());
			}
			
			//no of logons
			logonDto.setNoOfLogons(map.getValue().size());
			
			//failed logins
			List<LoginHistory> failedLogins = map.getValue().stream()
					.filter(p -> p.getLoginStatus().equalsIgnoreCase(ElementConstants.FAILED_STATUS))
					.collect(Collectors.toList());
			logonDto.setFailedList(failedLogins);
			//failed logins count
			//logonDto.setLogonFailures(failedLogins.size());
			
			logonDtoList.add(logonDto);
		}
	}
	
	returnResult.put(logonDtoList, totalCount);
	return returnResult;
	}*/

	@Override
	public ByteArrayOutputStream prepareCSV(List<AssigneeDto> csvDto) {
		StringBuilder sb = null;
		String csvFile = null;
		int count = 1;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			final String FILE_HEADER = "S.No,FULL NAME,USERNAME,DATE OF BIRTH,EMAIL,DEPARTMENT,STATUS,ROLES,GROUPS,LAST LOGIN,JOB TITLE,COUNTRY";
			sb = new StringBuilder();
			sb.append(FILE_HEADER);
			sb.append(System.getProperty("line.separator"));
			for(AssigneeDto dto: csvDto) {
				sb.append(count++);
				sb.append(",");
				if(null != dto.getFirstName()){
					if(null != dto.getMiddleName() && null != dto.getLastName())
						sb.append(dto.getFirstName()+" "+dto.getMiddleName()+" "+dto.getLastName());
					if(null == dto.getMiddleName() && null != dto.getLastName())
						sb.append(dto.getFirstName()+" "+dto.getLastName());
					if(null == dto.getLastName() && null != dto.getMiddleName())
						sb.append(dto.getFirstName()+" "+dto.getMiddleName()+" "+dto.getLastName());
				}else{
					sb.append(" ");
				}
				sb.append(",");
				
				if(null != dto.getScreenName())
					sb.append(dto.getScreenName());
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getDob())
					sb.append(dto.getDob());
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getEmailAddress())
					sb.append(dto.getEmailAddress());
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getDepartment())
					sb.append(dto.getDepartment());
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getStatusId() && null != dto.getStatusId().getDisplayName())
					sb.append(dto.getStatusId().getDisplayName());
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getUsersRoles()){
					List<String> roles = dto.getUsersRoles().stream()
							.map((UserRolesDtoWithRolesObj u) -> u.getRoleId().getRoleName()).collect(Collectors.toList());
					String role = roles.stream().map(String::toUpperCase).collect(Collectors.joining(" "));
					sb.append(role);
				}
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getUsersGroups()){
					List<String> groups = dto.getUsersGroups().stream()
							.map((Groups u) -> u.getName()).collect(Collectors.toList());
					String group = groups.stream().map(String::toUpperCase).collect(Collectors.joining(" "));
					sb.append(group);
				}
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getLastLoginDate())
					sb.append(dto.getLastLoginDate());
				else
					sb.append(" ");
				sb.append(",");
				
				if(null != dto.getJobTitle())
					sb.append(dto.getJobTitle());
				else
					sb.append(" ");
				sb.append(",");

				if(null != dto.getCountryId() && null != dto.getCountryId().getDisplayName())
					sb.append(dto.getCountryId().getDisplayName());
				else
					sb.append(" ");
				
				sb.append(System.getProperty("line.separator"));
			}
			csvFile = sb.toString();
			bos.write(csvFile.getBytes());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return bos;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, Long> getTopTenLogonFailuresUsers(String period, String from, String to) {
		// TODO Auto-generated method stub
		Date fromDate = null;
		Date toDate = null;
		if (from != null && to != null) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String date1 = from + " 00:00:00";
				String date2 = to + " 23:59:59";

				fromDate = format.parse(date1);
				toDate = format.parse(date2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Map<String, Long> result = usersDao.getTopTenLogonFailuresUsers(period, fromDate, toDate);
		Map<String, Long> finalMap = new HashMap<String, Long>();
		for(Map.Entry<String, Long> entry : result.entrySet()){
			Users users = usersDao.getUserByEmailOrUsername(entry.getKey());
			users = prepareUserFromFirebase(users);
			String fullname = null;
			if(users != null){
				if(null != users.getFirstName()){
					if(null != users.getMiddleName() && null != users.getLastName()){
						fullname = users.getFirstName() + " "+ users.getMiddleName()+ " " + users.getLastName();
					}else if(null == users.getMiddleName() && null != users.getLastName()){
						fullname = users.getFirstName()+ " " + users.getLastName();
					}else{
						fullname = users.getFirstName()+ " " + users.getMiddleName();
					}
				}else{
					fullname = entry.getKey();
				}
			}else{
				fullname = entry.getKey();
			}
			finalMap.put(fullname, entry.getValue());
		}
		return finalMap ;
	}

	@Override
	@Transactional("transactionManager")
	public boolean uploadUserImage(Users user, MultipartFile userImage, Long currentUserId) throws Exception {
		Users currentUser = null;
		if (currentUserId != null)
			currentUser = find(currentUserId);
		else
			currentUser = user;
		if (null != user) {
			user.setUserImage(userImage.getBytes());
			usersDao.update(user);
			String fullName = user.getFirstName();
			if (null != user.getLastName())
				fullName = fullName + " " + user.getLastName();
			fullName = fullName + " " + user.getLastName();
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.USER_MANAGEMENT_TYPE, null,
					ElementConstants.USER_MANAGEMENT_TYPE);
			log.setDescription(ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG + fullName + " by "
					+ currentUser.getFirstName() + currentUser.getLastName());
			log.setName(currentUser.getFirstName() + currentUser.getLastName());
			log.setUserId(currentUser.getUserId());
			log.setTypeId(user.getUserId());
			auditLogDao.create(log);
			return true;
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteUserImage(Users user, Long currentUserId) throws Exception {
		Users currentUser = null;
		if (currentUserId != null)
			currentUser = find(currentUserId);
		else
			currentUser = user;
		if (null != user) {
			user.setUserImage(null);
			usersDao.update(user);
			String fullName = user.getFirstName();
			if (null != user.getLastName())
				fullName = fullName + " " + user.getLastName();
			fullName = fullName + " " + user.getLastName();
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.USER_MANAGEMENT_TYPE, null,
					ElementConstants.USER_MANAGEMENT_TYPE);
			log.setDescription(ElementConstants.USER_PROFILE_PICTURE_REMOVED_SUCCESSFULLY_MSG + fullName + " by "
					+ currentUser.getFirstName() + currentUser.getLastName());
			log.setName(currentUser.getFirstName() + currentUser.getLastName());
			log.setUserId(currentUser.getUserId());
			log.setTypeId(user.getUserId());
			auditLogDao.create(log);
			return true;
		}
		return false;
	}
	@Override
	@Transactional("transactionManager")
	public Map<List<LogonsDto>, Long> getLogonList(FilterModelDto filterDto, Integer pageNumber,
			Integer recordsPerPage, Long totalResults, String orderIn, String orderBy) throws Exception {
		Map<List<LogonsDto>, Long> returnResult = new HashMap<>();
		Long totalCount = 0L;
		Map<List<LoginUserCountDto>, Long> filteredResult = null;
		List<LoginUserCountDto> userIdNdCountList = null;
		Users userClass = new Users();

		List<FilterColumnDto> filterColumnDto = GenericFilterParser.filterDataParser(filterDto);
		filteredResult = usersDao.userFilterData(userClass, filterColumnDto, pageNumber, recordsPerPage, totalResults, orderIn,
				orderBy);
		for (Entry<List<LoginUserCountDto>, Long> entry : filteredResult.entrySet()) {
			userIdNdCountList = entry.getKey();
			totalCount = entry.getValue();
		}
		List<Long> userIdList = userIdNdCountList.stream().map(LoginUserCountDto::getUserId).collect(Collectors.toList());
		Map<Long, List<LoginHistory>> failedLoginMap = loginHistoryService.getFailedlogonsByUserIdList(userIdList);
		List<Users> usersList = usersDao.getUsersInList(userIdList);
		List<LogonsDto> logonsDtosList = new ArrayList<LogonsDto>();
		for (LoginUserCountDto dto : userIdNdCountList) {
			for (Users user : usersList) {
				if (user.getUserId().equals(dto.getUserId())) {
					LogonsDto logon = new LogonsDto();
					String fullname = null;
					if (null != user.getFirstName()) {
						if (null != user.getMiddleName() && null != user.getLastName()) {
							fullname = user.getFirstName() + " " + user.getMiddleName() + " " + user.getLastName();
						} else if (null == user.getMiddleName() && null != user.getLastName()) {
							fullname = user.getFirstName() + " " + user.getLastName();
						} else {
							fullname = user.getFirstName() + " " + user.getMiddleName();
						}
					} 
					logon.setUserId(user.getUserId());
					logon.setFullName(fullname);
					logon.setLoginTime(user.getLastLoginDate());
					logon.setRoles(userRolesService.getRolesOfUserDtoWithRoleObj(user.getUserId(),false));
					logon.setStatus(user.getStatusId());
					logon.setLogonsCount(dto.getAllLoginCount());
					logon.setLogonFailuresCount(dto.getFailedLoginCount());
					
					logon.setFailedList(failedLoginMap.get(user.getUserId()));
					
					logonsDtosList.add(logon);
				} 
			}
		}
		
		returnResult.put(logonsDtosList, totalCount);
		return returnResult;
	}

	@Override
	public List<Users> getUsersByIds(List<Long> userList) {
		
		return usersDao.getUsersByIds(userList);
	}

	public void deactivatePendingUsers() throws Exception {
		int pendingDaysOfUser = 0;
		ListItem pendingStatusItem = listItemDao.getListItemByListTypeAndDisplayName(ElementConstants.USER_STATUS_TYPE,
				UserStatus.Pending.getStatus());
		ListItem deactivatedStatusItem = listItemDao.getListItemByListTypeAndDisplayName(
				ElementConstants.USER_STATUS_TYPE, UserStatus.Deactivated.getStatus());
		Optional<Settings> pendingSetting = settingsService.findAll().stream()
				.filter(s -> s.getName().equalsIgnoreCase("Pending users deactivation after")).findFirst();
		Settings pendingSettingObject = null;
		if (pendingSetting.isPresent()) {
			pendingSettingObject = pendingSetting.get();
		}
		if (pendingStatusItem != null && pendingStatusItem.getListItemId() != null && pendingSetting != null) {
			List<Users> pendingUsers = findAll().stream()
					.filter(user -> user.getStatusId().getListItemId().equals(pendingStatusItem.getListItemId()))
					.collect(Collectors.toList());
			if (pendingUsers != null && pendingUsers.size() > 0) {
				for (Users user : pendingUsers) {
					Long days = (user.getCreatedDate().getTime() - new Date().getTime()) / 86400000;
					pendingDaysOfUser = Math.abs(days.intValue());
					if ((pendingDaysOfUser - Integer.valueOf(pendingSettingObject.getDefaultValue())) > 0) {
						user.setStatusId(deactivatedStatusItem);
						saveOrUpdate(user);
					}
				}
			}
		}
	}

	@Override
	@Transactional("transactionManager")
	public Users getFirebaseUserBySourceId(String sourceId) {
		return usersDao.getFirebaseUserBySourceId(sourceId);
		
	}
	@Override
	@Transactional("transactionManager")
	public Users createFirebaseUserBySourceId(JSONObject response) {
		
		if(response != null){
			Users user = new Users();
			
			if(response.has("localId")){
				Users existinguser = getFirebaseUserBySourceId(response.getString("localId"));
				if(existinguser == null ){
					
					user.setSourceId(response.getString("localId"));
					if (response.has("displayName")) {
						String name[] = response.getString("displayName").split(" ");
						user.setFirstName(name[0]);
						user.setLastName(name[1]);
					}else{
						String name[] = response.getString("email").split("@");
						user.setFirstName(name[0]);
						user.setLastName(name[0]);
					}
					//user.setFirstName(response.getString("localId"));
					//user.setLastName(response.getString("localId"));
					if (response.has("email")) {
						user.setEmailAddress(response.getString("email"));
						user.setScreenName(response.getString("email"));
					}
					//user.setScreenName(response.getString("localId"));
					//user.setEmailAddress(response.getString("localId")+"@mail.com");
					user.setScreenName(response.getString("localId"));
					user.setPassword(response.getString("localId"));
					
					Date expiry = DateUtils.addMinutes(new Date(), Integer.valueOf(TEMP_PSWD_VAL_MINS).intValue());
					if(response.has("verificationCode"))
						user.setTemporaryPassword(response.getString("verificationCode"));
					user.setTemporaryPasswordExpiresOn(expiry);
					user.setDob(new Date());
					user.setLastLoginDate(new Date());
					//user.setPassword(UUID.randomUUID().toString());
					user.setCreatedDate(new Date());
					user.setSource("Firebase");//TODo::need to use enum 
					user.setEmailAddressVerified(1);
					try {
						//TODo::need to use enum 
						
						ListItem status = listItemDao.getListItemByListTypeAndDisplayName("User Status","Active");
						//ListItem statusId = null;
						//BeanUtils.copyProperties(status, statusId);
						user.setStatusId(status);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					user = usersDao.create(user);
				}
				return user;
			}
				
			return null;
		}else{
			return null;
		}
		
	}
	@Override
	@Transactional("transactionManager")
	public Users updateFirebaseUserBySourceId(JSONObject response) {
		
		if(response != null){
			Users user = null;
			
			if(response.has("localId")){
				user = getFirebaseUserBySourceId(response.getString("localId"));
				user.setSourceId(response.getString("localId"));
				user.setFirstName(response.getString("localId"));
				user.setLastName(response.getString("localId"));
				user.setScreenName(response.getString("localId"));
				user.setEmailAddress(response.getString("localId")+"@mail.com");
				user.setScreenName(response.getString("localId"));
				user.setPassword(response.getString("localId"));
			}
				
			/*
			if(response.has("displayName")){
				user.setFirstName(response.getString("displayName"));
				user.setLastName(response.getString("displayName"));
			}
			
			if(response.has("email")){
				user.setEmailAddress(response.getString("email"));
				user.setScreenName(response.getString("displayName"));
			}*/
			Date expiry = DateUtils.addMinutes(new Date(), Integer.valueOf(TEMP_PSWD_VAL_MINS).intValue());
			if(response.has("verificationCode"))
				user.setTemporaryPassword(response.getString("verificationCode"));
			user.setTemporaryPasswordExpiresOn(expiry);
			user.setDob(new Date());
			//user.setPassword(UUID.randomUUID().toString());
			user.setCreatedDate(new Date());
			user.setSource("Firebase");//TODo::need to use enum 
			user.setEmailAddressVerified(1);
			try {
				//TODo::need to use enum 
				
				ListItem status = listItemDao.getListItemByListTypeAndDisplayName("User Status","Active");
				//ListItem statusId = null;
				//BeanUtils.copyProperties(status, statusId);
				user.setStatusId(status);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(user != null)
				usersDao.update(user);
			
			return user;
		}else{
			return null;
		}
		
	}
	/*@Override
	public Users prepareUserFromFirebase(Users user) {
		// TODO Auto-generated method stub
		return null;
	}*/
	@Override
	public Users prepareUserFromFirebase(Users user) {

		if (null!=user && null != user.getSource() && user.getSource().equals(ElementConstants.FIREBASE_SOURCE) 
				&& null != servletContext.getAttribute(user.getPassword())) {
			String token = servletContext.getAttribute(user.getPassword()).toString();
			JSONObject response = elementsSecurityService.getFirebaseAccountInfoByToken(token);
			if(response != null && response.has("users")){
				org.json.JSONArray userArray = response.getJSONArray("users");
				if(userArray.length()>0){
					JSONObject userObject = (JSONObject) userArray.get(0);
					//check existing user with localId or create new user with localId
					if(userObject.has("localId")){
						if (userObject.has("email")) {
							user.setEmailAddress(userObject.getString("email"));
							user.setScreenName(userObject.getString("email"));
						}
						if (userObject.has("displayName")) {
							String name[] = userObject.getString("displayName").split(" ");
							user.setFirstName(name[0]);
							user.setLastName(name[1]);
						}else{
							String name[] = userObject.getString("email").split("@");
							user.setFirstName(name[0]);
							user.setLastName(name[0]);
						}
					}
				}
			}
		}
			
		return user;

	}
}
