package element.bst.elementexploration.rest.extention.template.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Paul
 *
 */
public interface TemplateService {

	byte[] getTemplateByName(String requestBody, String templateName) throws Exception;

	String [] saveTemplate(MultipartFile uploadFile, String templateName) throws Exception;

}
