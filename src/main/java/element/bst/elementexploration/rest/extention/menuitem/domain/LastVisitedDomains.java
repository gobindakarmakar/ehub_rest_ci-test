package element.bst.elementexploration.rest.extention.menuitem.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "bst_um_last_visited_domains")
public class LastVisitedDomains implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "lastVisitedDomainId")
	private Long lastVisitedDomainId;

	@ApiModelProperty(value = "userId of the Domain")
	@Column(name = "userId")
	private Long userId;

	@ApiModelProperty(value = "permissionId of the Domain")
	@Column(name = "permissionId")
	private Long permissionId;

	@ApiModelProperty(value = "Name of the Domain")
	@Column(name = "domainName", columnDefinition = "LONGTEXT")
	private String domainName;

	public Long getLastVisitedDomainId() {
		return lastVisitedDomainId;
	}

	public void setLastVisitedDomainId(Long lastVisitedDomainId) {
		this.lastVisitedDomainId = lastVisitedDomainId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
