package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserGroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("userGroupDao")
public class UserGroupDaoImpl extends GenericDaoImpl<UserGroup, Long> implements UserGroupDao{

	
	public UserGroupDaoImpl() {
		super(UserGroup.class);
	}
	
	/**
	 * Get UserGroup
	 * 
	 * @param userId
	 * @param groupId
	 */
	@Override
	public UserGroup getUserGroup(Long userId, Long groupId) {
		UserGroup userGroup = null;

		try {
			String hql = "FROM UserGroup as ug WHERE ug.userId = :userId and ug.groupId = :groupId";
			Query<?> query = this.getCurrentSession().createQuery(hql);
			query.setParameter("userId", userId);
			query.setParameter("groupId", groupId);
			userGroup=(UserGroup) query.getSingleResult();			
			return userGroup;
		} catch (NoResultException e) {
			return userGroup;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Get UserGroup", e, this.getClass());
		} 
		return userGroup;
	}

}
