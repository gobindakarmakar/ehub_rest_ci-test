package element.bst.elementexploration.rest.usermanagement.user.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.document.domain.DocumentVault;

@Entity
@Table(name = "bst_user")
@Access(AccessType.FIELD)
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;

	@Column(name = "emailAddress", length = 75)
	@NotEmpty(message = "Email can not be blank.")
	@Email(message = "Invalid email address.")
	private String emailAddress;

	@Column(name = "firstName", length = 75)
	@NotEmpty(message = "FirstName can not be blank.")
	private String firstName;

	@Column(name = "lastName", length = 75)
	@NotEmpty(message = "LastName can not be blank.")
	private String lastName;

	@Column(name = "middleName", length = 75)
	private String middleName;

	@Column(name = "dateOfBirth")
	@NotNull(message = "Date of birth can not be blank.")
	Date dob;

	@Column(length = 75)
	@NotEmpty(message = "Country can not be blank.")
	private String country;

	@Column(name = "password")
	@NotEmpty(message = "Password can not be blank.")
	private String password;

	@Column(name = "screenName", length = 75)
	@NotEmpty(message = "Screen name can not be blank.")
	private String screenName;

	@Column(name = "status")
	private String status;

	@Column(name = "emailAddressVerified", columnDefinition = "int default 0")
	private int emailAddressVerified;

	@Column
	private Long createdBy;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "modifiedDate")
	private Date modifiedDate;

	@Column(name = "lastLoginDate")
	private Date lastLoginDate;

	@Column(name = "lastLoginIP", length = 20)
	private String lastLoginIP;

	@Column(name = "temporaryPassword")
	private String temporaryPassword;

	@Column(name = "temporaryPassword_expire_on")
	private Date temporaryPasswordExpiresOn;
	
	@OneToMany(mappedBy = "uploadedBy", cascade = CascadeType.ALL)
	private List<DocumentVault> documentVaults;

	/*@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, targetEntity = CaseAnalystMapping.class, fetch = FetchType.LAZY)
	private List<CaseAnalystMapping> caseSeedAnalystMappingsList;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "createdBy", cascade = CascadeType.ALL, targetEntity = Case.class)
	private List<Case> bstCaseSeedsList;*/

	/**
	 * 
	 */
	public User() {
		super();
	}

	/**
	 * @param userId
	 * @param uuid
	 * @param emailAddress
	 * @param firstName
	 * @param lastName
	 * @param middleName
	 * @param contactId
	 * @param caseSeedAnalystMappingsList
	 */
	public User(Long userId, String uuid, String emailAddress, String firstName, String lastName, String middleName,
			List<CaseAnalystMapping> caseSeedAnalystMappingsList) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		//this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
	}

	public User(String screenName, String firstName, String lastName, Date createdDate, String status) {
		super();
		this.screenName = screenName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createdDate = createdDate;
		this.status = status;
	}

	public User(Long userId, String emailAddress, String firstName, String lastName, String middleName, Date dob,
			String country, String password, String screenName, String status, int emailAddressVerified, Long createdBy,
			Date createdDate, Date modifiedDate, Date lastLoginDate, String lastLoginIP, String temporaryPassword,
			Date temporaryPasswordExpiresOn, List<CaseAnalystMapping> caseSeedAnalystMappingsList,
			List<DocumentVault> documentVaults, List<Case> bstCaseSeedsList) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.dob = dob;
		this.country = country;
		this.password = password;
		this.screenName = screenName;
		this.status = status;
		this.emailAddressVerified = emailAddressVerified;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.lastLoginDate = lastLoginDate;
		this.lastLoginIP = lastLoginIP;
		this.temporaryPassword = temporaryPassword;
		this.temporaryPasswordExpiresOn = temporaryPasswordExpiresOn;
		//this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
		//this.documentVaults = documentVaults;
		//this.bstCaseSeedsList = bstCaseSeedsList;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastLoginDate
	 */
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	/**
	 * @param lastLoginDate the lastLoginDate to set
	 */
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	/**
	 * @return the lastLoginIP
	 */
	public String getLastLoginIP() {
		return lastLoginIP;
	}

	/**
	 * @param lastLoginIP the lastLoginIP to set
	 */
	public void setLastLoginIP(String lastLoginIP) {
		this.lastLoginIP = lastLoginIP;
	}

	/**
	 * @return the caseSeedAnalystMappingsList
	 */
	/*public List<CaseAnalystMapping> getCaseSeedAnalystMappingsList() {
		return caseSeedAnalystMappingsList;
	}

	*//**
	 * @param caseSeedAnalystMappingsList the caseSeedAnalystMappingsList to set
	 *//*
	public void setCaseSeedAnalystMappingsList(List<CaseAnalystMapping> caseSeedAnalystMappingsList) {
		this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
	}

	public List<DocumentVault> getDocumentVaults() {
		return documentVaults;
	}

	public void setDocumentVaults(List<DocumentVault> documentVaults) {
		this.documentVaults = documentVaults;
	}

	public List<Case> getBstCaseSeedsList() {
		return bstCaseSeedsList;
	}

	public void setBstCaseSeedsList(List<Case> bstCaseSeedsList) {
		this.bstCaseSeedsList = bstCaseSeedsList;
	}*/

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getEmailAddressVerified() {
		return emailAddressVerified;
	}

	public void setEmailAddressVerified(int emailAddressVerified) {
		this.emailAddressVerified = emailAddressVerified;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTemporaryPassword() {
		return temporaryPassword;
	}

	public void setTemporaryPassword(String temporaryPassword) {
		this.temporaryPassword = temporaryPassword;
	}

	public Date getTemporaryPasswordExpiresOn() {
		return temporaryPasswordExpiresOn;
	}

	public void setTemporaryPasswordExpiresOn(Date temporaryPasswordExpiresOn) {
		this.temporaryPasswordExpiresOn = temporaryPasswordExpiresOn;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}