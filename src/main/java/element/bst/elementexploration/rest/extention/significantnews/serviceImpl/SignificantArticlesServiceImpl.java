package element.bst.elementexploration.rest.extention.significantnews.serviceImpl;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.entitysearch.dto.ArticleDto;
import element.bst.elementexploration.rest.extention.entitysearch.service.EntitySearchService;
import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantArticleDao;
import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantCommentDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantArticles;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantArticlesService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Service("significantArticlesService")
public class SignificantArticlesServiceImpl extends GenericServiceImpl<SignificantArticles, Long>
		implements SignificantArticlesService {

	public SignificantArticlesServiceImpl(GenericDao<SignificantArticles, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private SignificantArticleDao significantArticleDao;

	@Autowired
	private EntitySearchService entitySearchService;
	
	@Autowired
	SignificantCommentDao significantCommentDao;

	@Transactional("transactionManager")
	@Override
	public SignificantArticles saveSignificantArticles(SignificantArticles significantArticles,Long userId) {
		SignificantArticles existsignificantArticles = significantArticleDao
				.getSignificantArticles(significantArticles.getUuid());
		if (existsignificantArticles != null) {
			existsignificantArticles.setSignificantNews(significantArticles.isSignificantNews());
			significantArticleDao.saveOrUpdate(existsignificantArticles);
			if(!significantArticles.isSignificantNews())
				saveComment(significantArticles.getComment(),existsignificantArticles.getId(),userId);
		} else {
			return significantArticleDao.create(significantArticles);
		}
		return existsignificantArticles;
	}

	private void saveComment(String comment, Long id, Long userId) {
		if (comment != null) {
			SignificantComment significantComment = new SignificantComment();
			significantComment.setClassification("NEWS");
			significantComment.setComment(comment);
			significantComment.setUserId(userId);
			significantComment.setIsSignificant(false);
			significantComment.setSignificantId(id);
			significantCommentDao.create(significantComment);
		}
			
	}
	
	@Override
	@Transactional("transactionManager")
	public SignificantArticles getSignificantArticles(String uuid) {
		SignificantArticles significantNews = significantArticleDao.getSignificantArticles(uuid);
		return significantNews;
	}

	@Override
	@Transactional("transactionManager")
	public SignificantArticles saveArticleSentiment(SignificantArticles significantArticles) {
		SignificantArticles existsignificantArticles = significantArticleDao
				.getSignificantArticles(significantArticles.getUuid());
		if (existsignificantArticles != null) {
			existsignificantArticles.setSentiment(significantArticles.getSentiment());
			significantArticleDao.saveOrUpdate(existsignificantArticles);
		} else {
			return significantArticleDao.create(significantArticles);
		}
		return existsignificantArticles;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteSignificantArticles(String uuid,String comment,Long userId) {
		boolean found = false;
		SignificantArticles existsignificantArticles = significantArticleDao.getSignificantArticles(uuid);
		if (existsignificantArticles != null) {
			if (existsignificantArticles.getSentiment() != null) {
				existsignificantArticles.setSignificantNews(false);
				significantArticleDao.saveOrUpdate(existsignificantArticles);
				if (comment != null) {
					SignificantComment significantComment = new SignificantComment();
					significantComment.setClassification("ARTICLES");
					significantComment.setComment(comment);
					significantComment.setUserId(userId);
					significantComment.setIsSignificant(false);
					significantComment.setSignificantId(existsignificantArticles.getId());
					significantCommentDao.create(significantComment);
				}
				found = true;
			} else {
				significantArticleDao.delete(existsignificantArticles);
				found = true;
			}
		}
		return found;
	}

	@Override
	@Transactional("transactionManager")
	public JSONArray getAllSignificantNews(String entityId) throws Exception {
		JSONArray array = new JSONArray();
		try
		{
		List<SignificantArticles> articlesList = significantArticleDao.findArticlesByEntityId(entityId);
		if (articlesList != null) {
			for (SignificantArticles significantArticles : articlesList) {
				if (significantArticles != null) {
					ArticleDto articleDto = new ArticleDto();
					articleDto.setArticlePath(significantArticles.getArticleUrl());
					String response = entitySearchService.getArticleNewsInfo(articleDto);
					JSONObject jsonObject = new JSONObject(response);
					jsonObject.put("classification", significantArticles.getClassification());
					array.put(jsonObject);
				}
			}
		}
		}
		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return array;
	}

}
