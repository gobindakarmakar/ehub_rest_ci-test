package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;

public class CountryAggDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private String countryCode;
	private Country country;
	private long count;
	private double minimum;
	private double maximum;
	private double sum;
	private double average;
	
	
	public CountryAggDto() {
		super();
	}

	public CountryAggDto(String countryCode, Country country, long count, double minimum, double maximum, double sum,
			double average) {
		super();
		this.countryCode = countryCode;
		this.country = country;
		this.count = count;
		this.minimum = minimum;
		this.maximum = maximum;
		this.sum = sum;
		this.average = average;
	}
	
	public CountryAggDto(String countryCode, long count, double minimum, double maximum, double sum, double average) {
		super();
		this.countryCode = countryCode;
		this.count = count;
		this.minimum = minimum;
		this.maximum = maximum;
		this.sum = sum;
		this.average = average;
	}

	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public double getMinimum() {
		return minimum;
	}
	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}
	public double getMaximum() {
		return maximum;
	}
	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	@Override
	public String toString() {
		return "TestDtoCountry [countryCode=" + countryCode + ", country=" + country + ", count=" + count + ", minimum="
				+ minimum + ", maximum=" + maximum + ", sum=" + sum + ", average=" + average + "]";
	}
	
	
	
}
