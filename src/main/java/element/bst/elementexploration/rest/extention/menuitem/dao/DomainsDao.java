package element.bst.elementexploration.rest.extention.menuitem.dao;

import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface DomainsDao extends GenericDao<Domains, Long> {

}
