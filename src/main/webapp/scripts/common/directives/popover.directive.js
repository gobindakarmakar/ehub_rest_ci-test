/*
 * @purpose: for popover in clipboard and sticky notes
 * @created: 20 april 2018
 * @returns: no
 * @author: zameer
 */

activitiApp.directive('customPopover', function() {
	return {
		restrict: 'A',
		link: function(scope, el, attrs) {
			$(el).popover({
				trigger: 'click',
				content: attrs.popoverHtml,
				placement: 'top'
			});
		}
	};
});