package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.LuxuryShopsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.LuxuryShops;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.LuxuryShopsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Service("luxuryShopsServiceService")
@Transactional("transactionManager")
public class LuxuryShopsServiceImpl extends GenericServiceImpl<LuxuryShops, Long> implements LuxuryShopsService {

	@Autowired
	LuxuryShopsDao luxuryShopsDao;

	public LuxuryShopsServiceImpl(GenericDao<LuxuryShops, Long> genericDao) {
		super(genericDao);

	}

	@Override
	public Boolean uploadLuxuryShopDetails(MultipartFile file)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {
		BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
		String line = null;
		int i = 1;

		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if (line.contains(","))
					data = line.split(",");
				if (line.contains(";"))
					data = line.split(";");
				LuxuryShops luxuryShops = new LuxuryShops();
				if (data!=null && data.length == 3) {
					luxuryShops.setShopName(data[0]);
					luxuryShops.setWebPage(data[1]);
					luxuryShops.setBrand(data[2]);
				}
				if (data!=null && data.length == 2) {
					luxuryShops.setShopName(data[0]);
					luxuryShops.setWebPage(data[1]);
				}
				try {
					luxuryShopsDao.create(luxuryShops);
				} catch (Exception e) {
					ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				}
			}
			i++;
		}
		return true;
	}

}
