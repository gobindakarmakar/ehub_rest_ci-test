package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_CONTENTS")
public class DocumentContents implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String contentType;
	private long parentId;
	private long templateId;
	private List<DocumentStatements> documentStatements;
	private List<DocumentTableColumns> documentTableColumns;
	private List<DocumentQuestions> documentQuestions;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "CONTENT_TYPE")
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Column(name = "TEMPLATE_ID")
	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentContent")
	public List<DocumentStatements> getDocumentStatements() {
		return documentStatements;
	}

	public void setDocumentStatements(List<DocumentStatements> documentStatements) {
		this.documentStatements = documentStatements;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentContent")
	public List<DocumentTableColumns> getDocumentTableColumns() {
		return documentTableColumns;
	}

	public void setDocumentTableColumns(List<DocumentTableColumns> documentTableColumns) {
		this.documentTableColumns = documentTableColumns;
	}
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentContent")
	public List<DocumentQuestions> getDocumentQuestions() {
		return documentQuestions;
	}

	public void setDocumentQuestions(List<DocumentQuestions> documentQuestions) {
		this.documentQuestions = documentQuestions;
	}
	@Column(name="PARENT_ID")
	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	
	

}
