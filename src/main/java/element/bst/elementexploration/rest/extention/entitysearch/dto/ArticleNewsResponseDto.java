package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArticleNewsResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("results")
	private List<ArticleNewsDto> results;

	public List<ArticleNewsDto> getResults() {
		return results;
	}

	public void setResults(List<ArticleNewsDto> results) {
		this.results = results;
	}

}
