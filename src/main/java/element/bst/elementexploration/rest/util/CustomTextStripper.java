package element.bst.elementexploration.rest.util;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;


public class CustomTextStripper extends PDFTextStripper {
	public CustomTextStripper(PDDocument document, List<String> wordList) throws IOException {
		super.setSortByPosition(true);
		this.wordList = wordList;
	}

	private StringBuilder tWord = new StringBuilder();
	private List<String> wordList;
	private boolean is1stChar = true;
	private boolean lineMatch;
	private double lastYVal;

	@Override
	protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
		for (TextPosition text : textPositions) {
			String tChar = text.getUnicode();
			String REGEX = "[,.\\[\\](:;!?)/]";
			// String REGEX =(//.*);
			char c = tChar.charAt(0);
			lineMatch = matchCharLine(text);
			if ((!tChar.matches(REGEX)) && (!java.lang.Character.isWhitespace(c))) {
				if ((!is1stChar) && (lineMatch == true)) {
					appendChar(tChar);
				} else if (is1stChar == true) {
					setWordCoord(text, tChar);
				}
			} else {
				endWord();
			}

		}
	}

	protected void appendChar(String tChar) {
		tWord.append(tChar);
		is1stChar = false;
	}

	protected void setWordCoord(TextPosition text, String tChar) {
		tWord.append(text.getY() + " ").append(tChar);
		is1stChar = false;
	}

	protected void endWord() {
		String newWord = tWord.toString().replaceAll("[^\\x00-\\x7F]", "");
		String sWord = newWord.substring(newWord.lastIndexOf(' ') + 1);
		if (!"".equals(sWord)) {
			wordList.add(newWord);
		}
		tWord.delete(0, tWord.length());
		is1stChar = true;
	}

	protected boolean matchCharLine(TextPosition text) {
		Double yVal = roundVal(Float.valueOf(text.getYDirAdj()));
		if (yVal.doubleValue() == lastYVal) {
			return true;
		}
		lastYVal = yVal.doubleValue();
		endWord();
		return false;
	}

	protected Double roundVal(Float yVal) {
		DecimalFormat rounded = new DecimalFormat("0.0'0'");
		Double yValDub = new Double(rounded.format(yVal));
		return yValDub;
	}
}
