package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourcesHideStatusDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("sourcesHideStatusDao")
public class SourcesHideStatusDaoImpl extends GenericDaoImpl<SourcesHideStatus, Long> implements SourcesHideStatusDao {

	public SourcesHideStatusDaoImpl() {
		super(SourcesHideStatus.class);
	}

	/*@Override
	public SourcesHideStatus getSourceHideStatus(Long sourceId, Long classifcationId) {
		SourcesHideStatus sourcesHideStatus = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"from SourcesHideStatus s  where s.sourceId =:sourceId and  s.classificationId =:classifcationId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("sourceId", sourceId);
			query.setParameter("classifcationId", classifcationId);
			sourcesHideStatus = (SourcesHideStatus) query.getSingleResult();
			if (sourcesHideStatus != null)
				return sourcesHideStatus;
		} catch (NoResultException ne) {
			return sourcesHideStatus;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesHideStatusDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return sourcesHideStatus;
	}*/

	@SuppressWarnings("unchecked")
	@Override
	public List<SourcesHideStatus> getSourceHideStatusByClassification(Long classificationId) {
		List<SourcesHideStatus> sourcesHideStatus = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourcesHideStatus s  where  s.classificationId =:classifcationId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("classifcationId", classificationId);
			sourcesHideStatus = (List<SourcesHideStatus>) query.getResultList();
			if (sourcesHideStatus != null)
				return sourcesHideStatus;
		} catch (NoResultException ne) {
			return sourcesHideStatus;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourcesHideStatusDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return sourcesHideStatus;
	}

}
