package element.bst.elementexploration.rest.extention.menuitem.daoImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.ModulesGroupDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author Jalagari Paul
 *
 */
@Repository("modulesGroupDao")
@Transactional("transactionManager")
public class ModulesGroupDaoImpl extends GenericDaoImpl<ModulesGroup, Long> implements ModulesGroupDao {

	public ModulesGroupDaoImpl() {
		super(ModulesGroup.class);
	}
}
