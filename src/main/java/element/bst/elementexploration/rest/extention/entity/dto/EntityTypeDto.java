package element.bst.elementexploration.rest.extention.entity.dto;

import java.io.Serializable;
import java.util.List;

public class EntityTypeDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long type_id;

	private String entityType;

	private List<EntityRequirementsDto> requirementsList;

	public long getType_id() {
		return type_id;
	}

	public void setType_id(long type_id) {
		this.type_id = type_id;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public List<EntityRequirementsDto> getRequirementsList() {
		return requirementsList;
	}

	public void setRequirementsList(List<EntityRequirementsDto> requirementsList) {
		this.requirementsList = requirementsList;
	}

	
}
