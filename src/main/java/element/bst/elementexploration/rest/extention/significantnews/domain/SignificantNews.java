package element.bst.elementexploration.rest.extention.significantnews.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author rambabu
 *
 */
@Entity
@Table(name = "bst_significant_news")
public class SignificantNews implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "entity_id")
	@NotEmpty(message = "entity id can not be blank.")
	private String entityId;

	@Column(name = "entity_name")
	@NotEmpty(message = "entity name can not be blank.")
	private String entityName;

	@Column(name = "published_date")
	@NotEmpty(message = "published date can not be blank.")
	private String publishedDate;

	@Column(name = "title")
	@NotEmpty(message = "title can not be blank.")
	private String title;

	@Column(name = "url", columnDefinition = "LONGTEXT")
	@NotEmpty(message = "url can not be blank.")
	private String url;

	@Column(name = "class")
	@NotEmpty(message = "class can not be blank.")
	private String newsClass;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "is_significant_news")
	private Boolean isSignificantNews;

	@Column(name = "sentiment")
	private String sentiment;

	public SignificantNews() {
		super();

	}

	public SignificantNews(Long id, String entityId, String entityName, String publishedDate, String title, String url,
			String newsClass, Long userId, Boolean isSignificantNews, String sentiment) {
		super();
		this.id = id;
		this.entityId = entityId;
		this.entityName = entityName;
		this.publishedDate = publishedDate;
		this.title = title;
		this.url = url;
		this.newsClass = newsClass;
		this.userId = userId;
		this.isSignificantNews = isSignificantNews;
		this.sentiment = sentiment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNewsClass() {
		return newsClass;
	}

	public void setNewsClass(String newsClass) {
		this.newsClass = newsClass;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean isSignificantNews() {
		return isSignificantNews;
	}

	public void setSignificantNews(Boolean isSignificantNews) {
		this.isSignificantNews = isSignificantNews;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
