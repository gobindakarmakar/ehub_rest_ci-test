package element.bst.elementexploration.rest.extention.alertmanagement.daoImpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.daoImpl.DocumentDaoImpl;
import element.bst.elementexploration.rest.extention.alertmanagement.dao.AlertManagementDao;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.PastDueMonthCountDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.StatusCountDto;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Prateek
 *
 */

@Repository("alertManagementDao")
public class AlertManagementDaoImpl extends GenericDaoImpl<Alerts, Long> implements AlertManagementDao {

	@Autowired
	ListItemService listItemService;

	public AlertManagementDaoImpl() {
		super(Alerts.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alerts> getUserAlertByUserId(Long userId) {
		List<Alerts> alertList = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from Alerts fm where fm.assignee = :assigneeId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("assigneeId", userId);
			alertList = (List<Alerts>) query.getResultList();
		} catch (Exception e) {
			return alertList;
		}
		return alertList;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Alerts> findAlertsByCustomerId(String customeId) {

		List<Alerts> alertList = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from Alerts fm where fm.customerId = :customeId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("customeId", customeId);
			alertList = (List<Alerts>) query.getResultList();
		} catch (Exception e) {
			return alertList;
		}
		return alertList;
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alerts> getAlerts(Integer pageNumber, Integer recordsPerPage, String orderIn, String orderBy,
			Long feedId) {
		List<Alerts> alertList = new ArrayList<Alerts>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from Alerts a");

			if (feedId != null) {
				queryBuilder.append(" where ").append("a.").append("feed.feed_management_id =:feed ");
			}
			queryBuilder.append(" order by ").append("a.").append(resolveAlertColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			if (feedId != null) {
				query.setParameter("feed", feedId);
			}
			alertList = (List<Alerts>) query.getResultList();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return alertList;

	}

	@Override
	public Long getAlertsCount() {
		//List<Alerts> alertList = new ArrayList<Alerts>();
		Long count = 0L;
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select count(distinct fm.alertId) from Alerts fm");

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			count =  (Long) query.getSingleResult();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return count;

	}

	private String resolveAlertColumnName(String groupColumn) {
		if (groupColumn == null)
			return "createdDate";

		switch (groupColumn.toLowerCase()) {
		case "name":
			return "name";
		default:
			return "createdDate";
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusCountDto> getMyAlerts(Long userId) {
		List<StatusCountDto> myAlerts = new ArrayList<>();
		List<Object[]> objects = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select fm.status,count(distinct fm.alertId) from Alerts fm where fm.assignee = :assigneeId group by fm.status");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("assigneeId", userId);
			objects = (List<Object[]>) query.getResultList();
			if (objects.size() > 0) {
				for (Object[] result : objects) {
					if (result[0].toString() != null) {
						Long statusId = Long.valueOf(result[0].toString());
						ListItem item = listItemService.find(statusId);
						if (null != item) {
							StatusCountDto statusCount = new StatusCountDto();
							statusCount.setStatus(item.getDisplayName());
							statusCount.setCount(Integer.valueOf(result[1].toString()));
							myAlerts.add(statusCount);
						}
					}
				}
			}
		} catch (Exception e) {
			return myAlerts;
		}
		return myAlerts;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusCountDto> getAlertsOfGroups(Set<Long> groupIds) {
		List<StatusCountDto> myAlerts = new ArrayList<>();
		List<Object[]> objects = new ArrayList<>();
		List<Integer> intGroupIds= new ArrayList<>();
		intGroupIds=groupIds.stream().map(Long::intValue).collect(Collectors.toList());
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select fm.status,count(distinct fm.alertId),fm.groupLevel from Alerts fm where fm.groupLevel in(:groupIds) group by fm.status,fm.groupLevel");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("groupIds", intGroupIds);
			objects = (List<Object[]>) query.getResultList();
			if (objects.size() > 0) {
				for (Object[] result : objects) {
					if (result[0].toString() != null) {
						Long statusId = Long.valueOf(result[0].toString());
						ListItem item = listItemService.find(statusId);
						if (null != item) {
							StatusCountDto statusCount = new StatusCountDto();
							statusCount.setStatus(item.getDisplayName());
							statusCount.setCount(Integer.valueOf(result[1].toString()));
							statusCount.setGroupId(Long.valueOf(result[2].toString()));
							myAlerts.add(statusCount);
						}
					}
				}
			}
		} catch (Exception e) {
			return myAlerts;
		}
		return myAlerts;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PastDueMonthCountDto> getAlertsGroupByMonth(List<Long> filteredAlerts) {
		List<PastDueMonthCountDto> monthAndCount= new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select YEAR(fm.updatedDate),MONTH(fm.updatedDate),count(distinct fm.alertId) from Alerts fm where fm.alertId in (:filteredAlerts)");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("filteredAlerts", filteredAlerts);
			monthAndCount = (List<PastDueMonthCountDto>) query.getResultList();
		} catch (Exception e) {
			return monthAndCount;
		}
		return monthAndCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alerts> getAlertsByFeed(Long feedID) {
		List<Alerts> alertList = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from Alerts fm where fm.feed.feed_management_id =:feedManagementId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("feedManagementId", feedID);
			alertList = (List<Alerts>) query.getResultList();
		} catch (Exception e) {
			return alertList;
		}
		return alertList;
	}
	
	@Override
	public boolean deleteComment(Long alertId, Long commentId, long userId) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("delete from AlertComments ac where ac.commentId = :commentId");
			if (alertId != null)
				hql.append(" and ac.alertId.alertId=:alertId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("commentId", commentId);
			query.setParameter("alertId", alertId);
			query.executeUpdate();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alerts> findAllWithRisk() {
		List<Alerts> alertList = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from Alerts fm where fm.riskIndicators is not null");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			alertList = (List<Alerts>) query.getResultList();
		} catch (Exception e) {
			return alertList;
		}
		return alertList;
	}

	@Override
	public Alerts findBydetailId(Long detailId) {
		Alerts existingAlert=null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from Alerts fm where fm.detailId = :detailId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("detailId", detailId);
			existingAlert = (Alerts) query.getSingleResult();
		} catch (Exception e) {
			return existingAlert;
		}
		return existingAlert;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getAlertIdsByFeedIds(List<Long> comingIdList) {
		List<Long> list = new ArrayList<>();
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("SELECT af.alertId FROM bst_am_alert_feed as af where af.feed_id in (:feedId) group by af.alertId");
			
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(builder.toString());
			query.setParameter("feedId", comingIdList);
			
			List<BigInteger> resultlist =  (List<BigInteger>) query.getResultList();
			list = resultlist.stream().map(s -> s.longValue()).collect(Collectors.toList());
			
			
			return list;
		} catch (HibernateException e) {
			
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					DocumentDaoImpl.class);
			return list;
		}
		
	}

	@Override
	public List<Alerts> getAlertsByAlertIdList(List<Long> alertIdList) {
		List<Alerts> alertList = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select fm from Alerts fm where fm.alertId in(:alertIds)");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("alertIds", alertIdList);
			alertList = (List<Alerts>) query.getResultList();
		} catch (Exception e) {
			return alertList;
		}
		return alertList;
	}

	
}
