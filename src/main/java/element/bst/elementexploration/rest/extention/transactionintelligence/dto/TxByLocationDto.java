package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

/**
 * @author suresh
 *
 */
public class TxByLocationDto {

	private Long count;
	private String countryName;
	private TxCountryDto point;

	public TxByLocationDto(Long count, String countryName) {
		super();
		this.count = count;
		this.countryName = countryName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public TxByLocationDto(Long count) {
		super();
		this.count = count;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public TxCountryDto getPoint() {
		return point;
	}

	public void setPoint(TxCountryDto point) {
		this.point = point;
	}

	

}
