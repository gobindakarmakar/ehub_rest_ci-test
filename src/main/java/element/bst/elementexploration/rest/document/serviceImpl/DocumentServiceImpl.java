package element.bst.elementexploration.rest.document.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDocumentDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dao.DocCommentDao;
import element.bst.elementexploration.rest.document.dao.DocumentCaseMppingDao;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.dao.DocumentMappingDao;
import element.bst.elementexploration.rest.document.domain.DocComment;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.domain.DocumentVaultCaseMapping;
import element.bst.elementexploration.rest.document.domain.DocumentVaultUserMapping;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.document.dto.DocCommentVo;
import element.bst.elementexploration.rest.document.dto.DocumentDto;
import element.bst.elementexploration.rest.document.dto.DocumentUserAndCaseDto;
import element.bst.elementexploration.rest.document.dto.DocumentUserMappingDTO;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.document.enumType.ReadWritePermissionEnum;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.FailedToUploadCaseSeedDocumentException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.extention.docparser.dto.CheckPdfTypeDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.settings.dto.SelectedObject;
import element.bst.elementexploration.rest.settings.dto.SettingsDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsDto;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import net.sourceforge.tess4j.TesseractException;

/**
 * 
 * @author Viswanath
 *
 */
@Service("documentService")
public class DocumentServiceImpl extends GenericServiceImpl<DocumentVault, Long> implements DocumentService {

	@Value("${bigdata_create_doc_url}")
	private String BIG_DATA_BASE_URL;
	
	
	@Value("${bigdata_s3_doc_url}")
	private String BIG_DATA_S3_DOC_URL;
	
	@Value("${bigdata_case_management_s3_doc_url}")
	private String CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL;
	
	@Value("${corporate_structure_location}")
	private String CORPORATE_STRUCTURE_LOCATION;
	
	@Value("${client_id}")
	private String CLIENT_ID;

	@Autowired
	UsersService usersService;
	
	@Autowired
	private DocumentDao documentDao;

	@Autowired
	private DocumentMappingDao documentMappingDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private CaseDao caseDao;

	@Autowired
	private DocumentCaseMppingDao documentCaseMppingDao;

	@Autowired
	private DocCommentDao docCommentDao;

	@Autowired
	private CaseDocumentDao caseDocumentDao;

	@Autowired
	private AuditLogDao auditLogDao;

	@Autowired
	private DocumentParserService documentParserService;
	
	@Autowired
	SettingsService settingsService;

	@Autowired
	public DocumentServiceImpl(GenericDao<DocumentVault, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public Long uploadDocument(MultipartFile uploadDoc, String fileTitle, String remarks, Long userId, Integer docFlagVal,
			Long templateId, String entityId, String entityName,String entitySource,String docSection, File annualPdf)
					throws IOException, IllegalStateException, InvalidFormatException, TesseractException,Exception {
		System.out.println("Entered into uploadDocument....");

		Integer docFlag = docFlagVal == null ? 0 : docFlagVal;
		if (uploadDoc != null && uploadDoc.getSize() > 0 || annualPdf != null) {
			Users user = usersDao.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			File file = null;
			String ext = "";
			if (uploadDoc != null) {
				String originalFile = new String(uploadDoc.getOriginalFilename());
				ext = FilenameUtils.getExtension(originalFile);
				
				
				if(!isFileTypeValid(ext)) {
					throw new FailedToUploadCaseSeedDocumentException(ElementConstants.INVALID_FILE_TYPE_MSG);
				}
				String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
				file = File.createTempFile(fileNameWithoutExtension, ext);
				uploadDoc.transferTo(file);
				if(!isFileSizeValid(file)) {
					
					throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
				}
				
				
			} else {
				ext = "pdf";
				file = annualPdf;
			}
			DocumentVault documentVault = new DocumentVault();

			/*if(getFileSizeMegaBytes(file)<=ElementConstants.FILE_MAX_SIZE.doubleValue()) {
				String serverResponse[] = ServiceCallHelper.postFileToServer(BIG_DATA_BASE_URL + "/doc", file);
				if (Integer.parseInt(serverResponse[0]) == 200 && serverResponse[1] != null
						&& serverResponse[1].trim().length() > 0) {
					documentVault.setDocGuuidFromServer(serverResponse[1]);

				}
			}else if(entityId==null && getFileSizeMegaBytes(file) >= ElementConstants.FILE_MAX_SIZE.doubleValue()) {
				throw new FailedToUploadCaseSeedDocumentException(ElementConstants.INVALID_FILE_SIZE_MSG.replace("%s", ElementConstants.FILE_MAX_SIZE.toString()+" MB"));
			}*/

			documentVault.setRemark(remarks);
			documentVault.setTitle(fileTitle);
			if(uploadDoc!=null)
				documentVault.setSize(Long.valueOf(uploadDoc.getSize()));
			documentVault.setType(ext);
			documentVault.setUploadedOn(new Date());
			documentVault.setIsDeleted(false);
			documentVault.setUploadedBy(user);
			if(uploadDoc!=null)
				documentVault.setDocName(uploadDoc.getOriginalFilename());
			else
				documentVault.setDocName(file.getName());
			documentVault.setDocFlag(docFlag != null ? docFlag : 0);
			if(uploadDoc!=null)
				documentVault.setDocName(uploadDoc.getOriginalFilename());
			else
				documentVault.setDocName(file.getName());
			if (entityId != null)
				documentVault.setEntityId(entityId);
			if (entityName != null)
				documentVault.setEntityName(entityName);
			if(entitySource!=null)
				documentVault.setEntitySource(entitySource);
			if (docSection != null)
				documentVault.setDocSection(docSection);
			if (docFlag != null && (docFlag == 7 || docFlag == 10 || docFlag == 11)) {
				documentVault.setDocumentParsingStatus("uploaded");
				CheckPdfTypeDto checkPdfType = documentParserService.checkPdfType(uploadDoc.getBytes());
				if (checkPdfType.isFillablePdf() == true)
					documentVault.setDocStructureType(1);// for fillable pdf
				else if (checkPdfType.isNormalPdf() == true)
					documentVault.setDocStructureType(2);// for normal pdf
				else if (checkPdfType.isScannedPdf() == true)
					documentVault.setDocStructureType(3);
				else
					documentVault.setDocStructureType(0);
			} else
				documentVault.setDocumentParsingStatus("regular document");

			//saving annual pdf url from remarks
			if(annualPdf!=null)
				documentVault.setAnnualUrl(remarks);

			//Upload to S3 Bucket,In case of entityId present..

				if (uploadDoc != null) {
					String originalFile = new String(uploadDoc.getOriginalFilename());
					ext = FilenameUtils.getExtension(originalFile);
					String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
					file = File.createTempFile(fileNameWithoutExtension, ext);
					uploadDoc.transferTo(file);
				}
				documentVault.setIsS3Storage(true);//For now we are storing all doc's in S3 bucket.
				documentVault.setDocGuuidFromServer(UUID.randomUUID().toString());
				
				if(entityId!=null && null!=docFlag && docFlag !=17) { 
					String getS3LinkServerResponse[] = ServiceCallHelper.getS3LinkForUploadDoc(BIG_DATA_S3_DOC_URL+"organizations/" + entityId+"?fields=documents&client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());

					if (Integer.parseInt(getS3LinkServerResponse[0]) == 200 && getS3LinkServerResponse[1] != null
							&& getS3LinkServerResponse[1].trim().length() > 0) {
						JSONObject json = new JSONObject();

						json = new JSONObject(getS3LinkServerResponse[1]);
						if(json.has("message") &&json.getString("message").equalsIgnoreCase("success") &&
								json.has("status")&& json.has("url")&&json.getBoolean("status")==true){
							int uploadS3ServerResponse =ServiceCallHelper.putFileToS3Server(json.getString("url"), file);
							if (uploadS3ServerResponse != 200) {
								throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
							}
						}else {
							throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
						}
					}else {
						throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
					}
					
					documentVault = documentDao.create(documentVault);
				}else {

					String getS3LinkServerResponse[] = ServiceCallHelper.getCaseManagementS3LinkForUploadDoc(CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL +"document?client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());

					if (Integer.parseInt(getS3LinkServerResponse[0]) == 200 && getS3LinkServerResponse[1] != null
							&& getS3LinkServerResponse[1].trim().length() > 0) {
						JSONObject json = new JSONObject();

						json = new JSONObject(getS3LinkServerResponse[1]);
						if(json.has("doc_id") &&json.getString("doc_id")!=null && 
								json.has("upload_link") &&json.getString("upload_link")!=null){
							int uploadS3ServerResponse =ServiceCallHelper.putCaseManagmentFileToS3Server(json.getString("upload_link"), file);
							if (uploadS3ServerResponse != 200) {
								throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
							}
						}else {
							throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
						}
					}else {
						throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
					}
					
					documentVault = documentDao.create(documentVault);
				}
				
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Uploaded a document");
			if (entityId != null && docFlag != null && docFlag == 5 && docSection!=null)
				log.setDescription(user.getFirstName() + " " + user.getLastName() + " added note "
						+ documentVault.getTitle() + " to " + "entity " + entityName +docSection +" section");
			else if (entityId != null)
				log.setDescription(user.getFirstName() + " " + user.getLastName() + " added document "
						+ documentVault.getTitle() + " to " + "entity " + entityName);
			else
				log.setDescription(user.getFirstName() + " " + user.getLastName() + " uploaded document "
						+ documentVault.getTitle());
			log.setUserId(userId);
			log.setDocumentId(documentVault.getDocId());
			log.setName(documentVault.getTitle());
			auditLogDao.create(log);

			return documentVault.getDocId();
		} else {
			throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
		}

	}

	@Override
	@Transactional("transactionManager")
	public void updateDocumentById(DocumentDto documentDto, Long docId, Long userId) {
		DocumentVault document = documentDao.find(docId);
		if (document != null && !document.getIsDeleted()) {
			if (document.getUploadedBy().getUserId().compareTo(userId) == 0 || isPermissionWrite(userId, docId)) {
				document.setModifiedBy(userId);
				document.setModifiedOn(new Date());
				document.setRemark(documentDto.getRemark());
				document.setTitle(documentDto.getTitle());
				if (documentDto.getDocumentParsingStatus() != null)
					document.setDocumentParsingStatus(documentDto.getDocumentParsingStatus());
				if (documentDto.getDocStructureType() != null)
					document.setDocStructureType(documentDto.getDocStructureType());
				documentDao.saveOrUpdate(document);
			} else {
				throw new PermissionDeniedException();
			}
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public boolean isPermissionWrite(Long userId, Long docId) {
		return documentMappingDao.isPermissionWrite(userId, docId);
	}
	
	
	@Transactional("transactionManager")
	private boolean isFileTypeValid(String extension) {
		boolean isFileTypeValid=false;
		SystemSettingsDto systemSettingsDto= settingsService.fetchSystemSettings(extension.toLowerCase());
		if(systemSettingsDto!=null && systemSettingsDto.getGeneralSettingsList()!=null) {
			for(SettingsDto settingsDto:systemSettingsDto.getGeneralSettingsList()){
				if(settingsDto.getSelectedValue().equalsIgnoreCase("On")&&settingsDto.getName().equalsIgnoreCase(extension)) {
					isFileTypeValid=true;
				}
			}
		}
		return isFileTypeValid;
	}
	
	@Transactional("transactionManager")
	private boolean isFileSizeValid(File file) throws Exception {
		boolean isFileTypeValid=false;
		SystemSettingsDto systemSettingsDto= settingsService.fetchSystemSettings("Maximum file size allowed");
		if(systemSettingsDto!=null && systemSettingsDto.getGeneralSettingsList()!=null) {
			double fileSizeInMb=getFileSizeMegaBytes(file);
			for(SettingsDto settingsDto:systemSettingsDto.getGeneralSettingsList()){
				for(SelectedObject selectedObject:settingsDto.getOptions()) {
					if(selectedObject.isSelected()) {
						double maxSize=getFileSize(selectedObject.getAttributeValue());
						if(fileSizeInMb <=maxSize) {
							isFileTypeValid=true;
						}else {
							throw new FailedToUploadCaseSeedDocumentException(ElementConstants.INVALID_FILE_SIZE_MSG.replace("%s", selectedObject.getAttributeValue()));	
						}
					}

				}
			}

		}
		return isFileTypeValid;
	}

	private Double getFileSize(String sizeStr) {
        return Double.valueOf(sizeStr.replace("MB", ""));//In MB
    }
	
	/*private Double filesize_in_megaBytes(Long size) {
        return (double) size/(1024*1024);//In MB
    }*/
	@Override
	@Transactional("transactionManager")
	public void updateDocumentContent(MultipartFile docFile, Long userId, Long docId) throws Exception {

		if (docFile != null && docFile.getSize() > 0) {
			DocumentVault documentVault = documentDao.find(docId);
			if (documentVault != null) {
				if (documentVault.getUploadedBy().getUserId().equals(userId)
						|| isReadOrWritePermission(userId, docId)) {
					String originalFile = new String(docFile.getOriginalFilename());
					String ext = FilenameUtils.getExtension(originalFile);
					String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
					File file = null;
					file = File.createTempFile(fileNameWithoutExtension, ext);
					docFile.transferTo(file);

					if(!isFileTypeValid(ext)) {
						throw new Exception(ElementConstants.INVALID_FILE_TYPE_MSG);
					}

					if(!isFileSizeValid(file)) {

						throw new Exception(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
					}

					/*					if(getFileSizeMegaBytes(file)<=ElementConstants.FILE_MAX_SIZE.doubleValue()) {
						String serverResponse[] = ServiceCallHelper.postFileToServer(BIG_DATA_BASE_URL + "/doc", file);
						if (Integer.parseInt(serverResponse[0]) == 200 && serverResponse[1] != null
								&& serverResponse[1].trim().length() > 0) {
							documentVault.setDocGuuidFromServer(serverResponse[1]);
							documentVault.setSize(Long.valueOf(docFile.getSize()));
							documentVault.setModifiedOn(new Date());
							documentVault.setModifiedBy(userId);
							documentVault.setType(ext);
							documentVault.setDocName(docFile.getOriginalFilename());
							documentVault.setIsS3Storage(false);
							documentDao.saveOrUpdate(documentVault);
						} else {
							throw new FailedToUploadCaseSeedDocumentException(
									ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
						}
					}else if(documentVault.getEntityId() == null && getFileSizeMegaBytes(file) >= ElementConstants.FILE_MAX_SIZE.doubleValue()) {
						throw new FailedToUploadCaseSeedDocumentException(ElementConstants.INVALID_FILE_SIZE_MSG.replace("%s", ElementConstants.FILE_MAX_SIZE.toString()+" MB"));
					}*/


					if(documentVault.getEntityId()!=null) {
						String getS3LinkServerResponse[] = ServiceCallHelper.getS3LinkForUploadDoc(BIG_DATA_S3_DOC_URL+"organizations/" + documentVault.getEntityId()+"?fields=documents&client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());
						if (Integer.parseInt(getS3LinkServerResponse[0]) == 200 && getS3LinkServerResponse[1] != null
								&& getS3LinkServerResponse[1].trim().length() > 0) {

							file = File.createTempFile(fileNameWithoutExtension, ext);
							docFile.transferTo(file);



							JSONObject json = new JSONObject();
							json = new JSONObject(getS3LinkServerResponse[1]);
							if(json.has("message") &&json.getString("message").equalsIgnoreCase("success") &&
									json.has("status")&& json.has("url")&&json.getBoolean("status")==true){
								int uploadS3ServerResponse =ServiceCallHelper.putFileToS3Server(json.getString("url"), file);
								if (uploadS3ServerResponse != 200) {
									throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
								}
								documentVault.setSize(Long.valueOf(docFile.getSize()));
								documentVault.setModifiedOn(new Date());
								documentVault.setModifiedBy(userId);
								documentVault.setType(ext);
								documentVault.setDocName(docFile.getOriginalFilename());
								documentVault.setIsS3Storage(true);
								documentDao.saveOrUpdate(documentVault);
							}else {
								throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
							}
						}else {
							throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
						}
					}else {
						documentVault.setDocGuuidFromServer(UUID.randomUUID().toString());						
						String getS3LinkServerResponse[] = ServiceCallHelper.getCaseManagementS3LinkForUploadDoc(CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL +"document?client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());

						if (Integer.parseInt(getS3LinkServerResponse[0]) == 200 && getS3LinkServerResponse[1] != null
								&& getS3LinkServerResponse[1].trim().length() > 0) {
							JSONObject json = new JSONObject();

							json = new JSONObject(getS3LinkServerResponse[1]);
							if(json.has("doc_id") &&json.getString("doc_id")!=null && 
									json.has("upload_link") &&json.getString("upload_link")!=null){
								int uploadS3ServerResponse =ServiceCallHelper.putCaseManagmentFileToS3Server(json.getString("upload_link"), file);
								if (uploadS3ServerResponse != 200) {
									throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
								}
							}else {
								throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
							}
						}else {
							throw new FailedToUploadCaseSeedDocumentException(ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
						}

						//String serverResponse[] = ServiceCallHelper.postFileToServer(BIG_DATA_BASE_URL + "/doc", file);
						/*if (Integer.parseInt(serverResponse[0]) == 200 && serverResponse[1] != null
									&& serverResponse[1].trim().length() > 0) {
								documentVault.setDocGuuidFromServer(serverResponse[1]);*/
						documentVault.setSize(Long.valueOf(docFile.getSize()));
						documentVault.setModifiedOn(new Date());
						documentVault.setModifiedBy(userId);
						documentVault.setType(ext);
						documentVault.setDocName(docFile.getOriginalFilename());
						documentVault.setIsS3Storage(false);
						documentDao.saveOrUpdate(documentVault);
						/*} else {
								throw new FailedToUploadCaseSeedDocumentException(
										ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG);
							}*/
					}

				} else {
					throw new PermissionDeniedException();
				}
			} else {
				throw new DocNotFoundException();
			}
		} else {
			throw new InsufficientDataException("File can not be empty.");
		}
	}

	@Override
	public Boolean isReadOrWritePermission(Long userId, Long docId) {
		return documentMappingDao.isReadOrWritePermission(userId, docId);
	}

	@Override
	@Transactional("transactionManager")
	public void softDeleteDocumentById(Long docId, Long userId) {

		DocumentVault documentVault = documentDao.find(docId);
		if (documentVault != null && !documentVault.getIsDeleted()) {
			if (documentVault.getEntityId() != null ||documentVault.getDocSection()!= null || (documentVault.getUploadedBy().getUserId().compareTo(userId) == 0
					|| isPermissionWrite(userId, docId))) {
				documentVault.setIsDeleted(true);
				documentVault.setModifiedBy(userId);
				documentVault.setModifiedOn(new Date());
				documentDao.saveOrUpdate(documentVault);
			} else {
				throw new PermissionDeniedException();
			}
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public void shareDocumentWithCaseseed(Long userId, Long docId, Long caseseedId) {
		if (documentDao.isDocShareable(userId, docId)) {
			if (caseDao.find(caseseedId) != null) {
				DocumentVaultCaseMapping vaultCaseSeedMapping = new DocumentVaultCaseMapping();
				Users user = usersDao.find(userId);
				user = usersService.prepareUserFromFirebase(user);
				vaultCaseSeedMapping.setAssociatedBy(user);
				vaultCaseSeedMapping.setAssociatedOn(new Date());
				vaultCaseSeedMapping.setIsAssociated(true);
				vaultCaseSeedMapping.setCaseId(caseseedId);
				vaultCaseSeedMapping.setDocId(docId);
				documentCaseMppingDao.create(vaultCaseSeedMapping);
			} else {
				throw new CaseSeedNotFoundException();
			}
		} else {
			throw new PermissionDeniedException("You dont have permission to share this document.");
		}
	}

	@Override
	@Transactional("transactionManager")
	public void unshareDocumentFromCaseseed(Long userId, Long docId, Long caseseedId) {
		if (documentDao.isDocShareable(userId, docId)) {
			if (caseDao.find(caseseedId) != null) {
				DocumentVaultCaseMapping vaultCaseMapping = documentCaseMppingDao.findByCaseIdAndDocId(docId,
						caseseedId);
				vaultCaseMapping.setDissociatedOn(new Date());
				vaultCaseMapping.setIsAssociated(false);
				vaultCaseMapping.setDissociatedBy(userId);
				documentCaseMppingDao.saveOrUpdate(vaultCaseMapping);
			} else {
				throw new CaseSeedNotFoundException();
			}
		} else {
			throw new PermissionDeniedException();
		}

	}

	@Override
	@Transactional("transactionManager")
	public void shareDocWithUser(long userId, long sharedByUserId, long docId, int permission) {

		if (permission >= 0 && permission < 2) {
			DocumentVault documentVault = documentDao.find(docId);
			if ((documentVault != null) && !documentVault.getIsDeleted()
					&& !(documentVault.getUploadedBy().getUserId().compareTo(userId) == 0)) {
				if ((documentDao.isDocShareable(sharedByUserId, docId))) {
					DocumentVaultUserMapping documentVaultUserMapping = new DocumentVaultUserMapping();
					documentVaultUserMapping.setUserId(userId);
					documentVaultUserMapping.setDocId(docId);
					documentVaultUserMapping.setSharedBy(sharedByUserId);
					documentVaultUserMapping.setSharedOn(new Date());
					if (permission == 0)
						documentVaultUserMapping.setPermission(ReadWritePermissionEnum.READ);
					else
						documentVaultUserMapping.setPermission(ReadWritePermissionEnum.WRITE);
					documentVaultUserMapping.setIsShared(true);
					documentMappingDao.saveOrUpdate(documentVaultUserMapping);
				} else {
					throw new PermissionDeniedException("You dont have permission to share this document.");
				}
			} else {
				throw new NoDataFoundException("Invalid document id or document is deleted.");
			}
		} else {
			throw new BadRequestException("Invalid permission value.");
		}
	}

	@Override
	@Transactional("transactionManager")
	public void unShareDocFromUser(long userId, long unsharedByUseId, long docId, String comments) {

		if ((documentDao.isDocShareable(unsharedByUseId, docId))) {
			DocumentVaultUserMapping documentUserMapping = documentMappingDao.findByUserIdAndDocId(userId, docId);
			documentUserMapping.setComment(comments);
			documentUserMapping.setUnsharedOn(new Date());
			documentUserMapping.setIsShared(false);
			documentMappingDao.saveOrUpdate(documentUserMapping);
		} else {
			throw new PermissionDeniedException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<DocumentVaultDTO> myDocuments(Long userId, Long caseseedId, Integer docFlag, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, String entityId,String docSection,Boolean isAllRequired) {

		List<DocumentVaultDTO> vaultDTOsList = new ArrayList<DocumentVaultDTO>();
		
		if(isAllRequired!=null && isAllRequired) {
			vaultDTOsList = documentDao.myDocuments(userId, caseseedId, docFlag,
					null,null, orderBy, orderIn,
					entityId,docSection,true);
		}else {
			vaultDTOsList = documentDao.myDocuments(userId, caseseedId, docFlag,
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn,
					entityId,docSection,false);
		}

		for (DocumentVaultDTO documentVaultDTO : vaultDTOsList) {
			if (documentVaultDTO.getModifiedBy() != null) {
				Users user = usersDao.find(documentVaultDTO.getModifiedBy());
				documentVaultDTO.setModifiedByName(user.getFirstName());
			} else {
				documentVaultDTO.setModifiedByName("");
			}
			documentVaultDTO.setHasConflicts(false);
		}
		return vaultDTOsList;
	}

	@Override
	@Transactional("transactionManager")
	public long countMyDocuments(Long userId, Long caseseedId, Integer docFlag, String entityId,String docSection) {

		return documentDao.countMyDocuments(userId, caseseedId, docFlag, entityId,docSection);
	}

	@Override
	@Transactional("transactionManager")
	public List<DocumentVaultDTO> fullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword,
			Integer docFlag, String orderBy, String orderIn, Integer pageNumber, Integer recordsPerPage,
			String entityId,String docSection) {
		return documentDao.fullTextSearchMyDocuments(userId, caseseedId, keyword, docFlag,
				pageNumber != null ? pageNumber : ElementConstants.DEFAULT_PAGE_NUMBER,
				recordsPerPage != null ? recordsPerPage : ElementConstants.DEFAULT_PAGE_SIZE, orderBy, orderIn,
				entityId,docSection);
	}

	@Override
	@Transactional("transactionManager")
	public long countFullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,
			String entityId,String docSection) {
		return documentDao.countFullTextSearchMyDocuments(userId, caseseedId, keyword, docFlag, entityId,docSection);

	}

	@Override
	@Transactional("transactionManager")
	public List<DocumentVaultDTO> getAllMySharedDocument(Long userId, Long caseseedId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Integer docFlag) {
		Integer flagValue = docFlag == null ? 0 : docFlag;
		return documentDao.getAllMySharedDocument(userId, caseseedId,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn,
				flagValue);
	}

	@Override
	@Transactional("transactionManager")
	public Long countGetAllMySharedDocument(Long userId, Long caseseedId, Integer docFlag) {
		Integer flagValue = docFlag == null ? 0 : docFlag;
		return documentDao.countGetAllMySharedDocument(userId, caseseedId, flagValue);
	}

	@Override
	@Transactional("transactionManager")
	public List<DocumentVaultDTO> fullTextSearchDocument(Long userId, String searchKeyWord, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Long caseId, Integer docFlag) {
		Integer flagValue = docFlag == null ? 0 : docFlag;
		return documentDao.fullTextSearchDocument(userId, searchKeyWord,
				pageNumber != null ? pageNumber : ElementConstants.DEFAULT_PAGE_NUMBER,
				recordsPerPage != null ? recordsPerPage : ElementConstants.DEFAULT_PAGE_SIZE, orderBy, orderIn, caseId,
				flagValue);
	}

	@Override
	@Transactional("transactionManager")
	public Long countFullTextSearchDocument(Long userId, String searchKeyWord, Long caseId, Integer docFlag) {
		Integer flagValue = docFlag == null ? 0 : docFlag;
		return documentDao.countFullTextSearchDocument(userId, searchKeyWord, caseId, flagValue);
	}

	@Override
	@Transactional("transactionManager")
	public DocumentUserAndCaseDto getCaseseedFromDocument(Long userId, Long docId)
			throws IllegalAccessException, InvocationTargetException {

		DocumentUserAndCaseDto documentUserAndCaseDto = new DocumentUserAndCaseDto();
		DocumentVault documentVault = documentDao.find(docId);
		if (documentVault != null && !documentVault.getIsDeleted()) {
			if (documentVault.getEntityId() != null || (documentVault.getUploadedBy().getUserId().compareTo(userId) == 0
					|| isPermissionWrite(userId, docId))) {
				List<CaseDto> caseList = documentCaseMppingDao.getSeedByDocumentId(docId);
				List<DocumentUserMappingDTO> docUserList = new ArrayList<DocumentUserMappingDTO>();
				docUserList = documentMappingDao.getUserList(docId);
				DocumentVaultDTO documentVaultDto = new DocumentVaultDTO();
				ConvertUtils.register(new DateConverter(null), Date.class);
				BeanUtils.copyProperties(documentVault, documentVaultDto);
				documentUserAndCaseDto.setCaseList(caseList);
				documentUserAndCaseDto.setDocumentVault(documentVaultDto);
				documentUserAndCaseDto.setDocUserList(docUserList);
				return documentUserAndCaseDto;
			} else {
				// throw new PermissionDeniedException("Permission is denied or Document not
				// found");
				return documentUserAndCaseDto;

			}
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public byte[] downloadDocument(Long docId, Long userId, HttpServletResponse response) throws Exception {
		DocumentVault documentVault = documentDao.find(docId);
		byte[] fileData =null;
		if (documentVault != null && !documentVault.getIsDeleted()) {
			if(documentVault.getEntityId()!=null && documentVault.getDocFlag() != 17) {
				String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server(BIG_DATA_S3_DOC_URL+"organizations/" + documentVault.getEntityId()+"?fields=documents&client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());
				if (s3DownloadLinkServerResponse[0] != null
						&& s3DownloadLinkServerResponse[0].trim().length() > 0) {
					JSONObject json = new JSONObject();
					json = new JSONObject(s3DownloadLinkServerResponse[0]);
					if(json.has("doc_id") && json.has("url")) {
						fileData = ServiceCallHelper.downloadFileFromS3Server(json.getString("url"));
					}
				}
			}/*else {//Old API Code
				//ToDo::Enable when we need user permission check..
				if (documentVault.getEntityId() != null || (documentVault.getUploadedBy().getUserId().compareTo(userId) == 0
					|| isReadOrWritePermission(userId, docId))) {
				String url = BIG_DATA_BASE_URL + "/doc/" + documentVault.getDocGuuidFromServer();
				fileData = ServiceCallHelper.downloadFileFromServer(url);
				} else {//ToDO::Enable when we need user permission 
				throw new PermissionDeniedException("Permission is denied or Document not found.");
			}
			}*/
			else {
				String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server(CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL+"document?client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());
				if (s3DownloadLinkServerResponse[0] != null
						&& s3DownloadLinkServerResponse[0].trim().length() > 0) {
					JSONObject json = new JSONObject();
					json = new JSONObject(s3DownloadLinkServerResponse[0]);
					if(json.has("download_link") && json.has("doc_id") && json.getString("download_link") != null) {
						fileData = ServiceCallHelper.downloadFileFromS3Server(json.getString("download_link"));
					}
				}
			}
			if (fileData != null) {
				return fileData;
			} else {
				throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
			}
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public byte[] downloadDocumentDocParer(Long docId, Long userId, HttpServletResponse response) throws Exception {
		DocumentVault documentVault = documentDao.find(docId);
		
		/*if (documentVault != null && !documentVault.getIsDeleted()) {
			String url = BIG_DATA_BASE_URL + "/doc/" + documentVault.getDocGuuidFromServer();
			byte[] fileData = ServiceCallHelper.downloadFileFromServer(url);
			if (fileData != null) {
				return fileData;
			} else {
				throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
			}
		} */
		
		byte[] fileData=null;
		if (documentVault != null && !documentVault.getIsDeleted()) {
			String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server(CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL+"document?client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());
			if (s3DownloadLinkServerResponse[0] != null
					&& s3DownloadLinkServerResponse[0].trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(s3DownloadLinkServerResponse[0]);
				if(json.has("download_link") && json.has("doc_id") && json.getString("download_link") != null) {
					fileData = ServiceCallHelper.downloadFileFromS3Server(json.getString("download_link"));
				}
			}

			if (fileData != null) {
				return fileData;
			} else {
				throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
			}
		}
		else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<DocCommentDTO> listDocComment(Long userId, Long docId, Integer pageNumber, Integer recordsPerPage,
			String orderIn) {
		if (!documentDao.isDocCommentable(userId, docId))
			throw new PermissionDeniedException("You do not have permission to view comment on this document");

		return docCommentDao.listDocComment(userId, docId,
				pageNumber != null ? pageNumber : ElementConstants.DEFAULT_PAGE_NUMBER,
				recordsPerPage != null ? recordsPerPage : ElementConstants.DEFAULT_PAGE_SIZE,
				orderIn == null ? "asc" : orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public long countDocComment(Long userId, Long docId) {
		return docCommentDao.countDocComment(userId, docId);
	}

	@Override
	@Transactional("transactionManager")
	public boolean isDocCommentable(long userId, Long docId) {
		return documentDao.isDocCommentable(userId, docId);
	}

	@Override
	@Transactional("transactionManager")
	public void documentDissemination(Long docId, Long userId, Long seedId) {

		DocumentVault documentVault = documentDao.find(docId);
		Case caseseed = caseDao.find(seedId);
		if (caseseed != null && documentVault != null && !documentVault.getIsDeleted()) {
			if (documentVault.getUploadedBy().getUserId().compareTo(userId) == 0 || isPermissionWrite(userId, docId)) {
				CaseDocDetails docDetails = new CaseDocDetails();
				docDetails.setFileTitle(documentVault.getTitle());
				docDetails.setFileSize(new Double(documentVault.getSize()));
				docDetails.setCaseSeed(caseseed);
				docDetails.setRemarks(documentVault.getRemark());
				docDetails.setUploadedOn(new Date());
				docDetails.setFileType(documentVault.getType());
				docDetails.setFilePath(documentVault.getDocName());
				docDetails.setDocGuuidFromServer(documentVault.getDocGuuidFromServer());
				caseDocumentDao.create(docDetails);
			} else {
				throw new PermissionDeniedException("Permission is denied or Document not found");
			}
		} else {
			throw new DocNotFoundException("Either document or case is not found.");
		}
	}

	@Override
	@Transactional("transactionManager")
	public DocumentVaultDTO getDocumentDetails(Long docId, Long userId)
			throws IllegalAccessException, InvocationTargetException {
		DocumentVaultDTO documentDetails = new DocumentVaultDTO();
		DocumentVault document = documentDao.find(docId);
		if (document != null) {
			if (document.getUploadedBy().getUserId().compareTo(userId) == 0 || isReadOrWritePermission(userId, docId)) {
				ConvertUtils.register(new DateConverter(null), Date.class);
				BeanUtils.copyProperties(document, documentDetails);
				if (document.getModifiedBy() != null) {
					Users user = usersDao.find(document.getModifiedBy());
					documentDetails.setModifiedByName(user.getFirstName());
				} else {
					documentDetails.setModifiedByName("");
				}
				documentDetails.setUploadedUserName(
						document.getUploadedBy().getFirstName() + " " + document.getUploadedBy().getLastName());
				return documentDetails;
			} else {
				throw new PermissionDeniedException("Permission is denied or Document not found");
			}
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public Long commentDocument(Long userId, DocCommentVo docCommentDto) {
		if (documentDao.isDocCommentable(userId, docCommentDto.getDocId())) {
			Users Users = usersDao.find(userId);
			Users = usersService.prepareUserFromFirebase(Users);
			DocumentVault documentVault = documentDao.find(docCommentDto.getDocId());
			DocComment docComment = new DocComment();
			docComment.setTitle(docCommentDto.getTitle());
			docComment.setDescription(docCommentDto.getDescription());
			docComment.setCreatedOn(new Date());
			docComment.setDocument(documentVault);
			docComment.setUser(Users);
			DocComment result = docCommentDao.create(docComment);
			return result.getCommentId();
		} else {
			throw new PermissionDeniedException();
		}
	}
	
	@Override
	@Transactional("transactionManager")
	public Long uploadDocumentByScreenShotURL(String fileUrl, String fileTitle, String remarks, Long userIdTemp,
			Integer docFlag, Long templateId, String entityId, String entityName,String entitySource, String docSection, File annualPdf,Long docId)
					throws Exception {

		File fileTmp=null;
		URL url=null;
		File file =null;
		MultipartFile multipartFile =null;
		
		try {

			if(docId != null && fileTitle != null) {
				byte [] fileData=downloadDocument(docId, userIdTemp, null);
				file = new File(CORPORATE_STRUCTURE_LOCATION+fileTitle);
				if(fileData != null) {
					FileUtils.writeByteArrayToFile(file, fileData);
				}else {
					System.out.println("First else executed...");
					throw new FileNotFoundException("Could not find any file");	
				}

			}else if(fileUrl!=null && (fileUrl.startsWith("https://") || fileUrl.startsWith("http://"))) {
				System.out.println("fileUrl is :: "+fileUrl);
				String decodedURL = URLDecoder.decode(fileUrl, "UTF-8");//To avoid Accent symbols Source_TélévisionFse1SATF1_id_20190715_063057.png
				
				URL urlDecode = new URL(decodedURL);
				URI uri = new URI(urlDecode.getProtocol(), urlDecode.getUserInfo(), urlDecode.getHost(), urlDecode.getPort(), urlDecode.getPath(), urlDecode.getQuery(), urlDecode.getRef());
				url = new URL(uri.toASCIIString());
				fileTmp = new File(fileUrl);
				file = new File(CORPORATE_STRUCTURE_LOCATION+fileTmp.getName());
				
				FileUtils.copyURLToFile(url, file);

			}else if(fileUrl!=null){
				file = new File(fileUrl);
			}else {
				System.out.println("Final Else executed...");
				throw new FileNotFoundException("Could not find any file");	
			}
			FileItem fileItem = new DiskFileItem(file.getName(), Files.probeContentType(file.toPath()), false, file.getName(), (int) file.length(), file.getParentFile());
			InputStream input = new FileInputStream(file);
			OutputStream os = fileItem.getOutputStream();
			IOUtils.copy(input, os);
			multipartFile = new CommonsMultipartFile(fileItem);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("Exception occured");
			throw new FileNotFoundException("Could not find any file");
		}
		return uploadDocument(multipartFile, fileTitle, remarks, userIdTemp, docFlag, templateId, entityId, entityName,entitySource, docSection, annualPdf);
	}
	
	private double getFileSizeMegaBytes(File file) {//size returns in mb
		return 	Double.parseDouble(new DecimalFormat("##.##").format((double) file.length() / (1024 * 1024)));
	}
	
	@Override
	@Transactional("transactionManager")
	public String getS3DownloadLinkFromServer(Long docId, String entityId) throws Exception {
		DocumentVault documentVault = documentDao.find(docId);
		String  downloadLink =null;
		if (documentVault != null && !documentVault.getIsDeleted()) {
			if(documentVault.getIsS3Storage()!=null&&documentVault.getIsS3Storage() && documentVault.getEntityId()!=null) {
				String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server(BIG_DATA_S3_DOC_URL+"organizations/" + documentVault.getEntityId()+"?fields=documents&client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());
				if (s3DownloadLinkServerResponse[0] != null
						&& s3DownloadLinkServerResponse[0].trim().length() > 0) {
					JSONObject json = new JSONObject();
					json = new JSONObject(s3DownloadLinkServerResponse[0]);
					if(json.has("doc_id") && json.has("url")) {
						downloadLink=json.getString("url");
					}else {
						downloadLink=null;
					}
				}
			}
		} /*else {
			throw new DocNotFoundException();
		}*/
		return downloadLink;
	}

	@Override
	public List<DocumentVaultDTO> getDocumentVaultByEntityIdAndDocflag(int docFlag, Long entityId) {
		List<DocumentVault> vaultList = documentDao.getDocumentVaultByEntityIdAndDocflag(docFlag, entityId);
		List<DocumentVaultDTO> vaultDtoList = new ArrayList<DocumentVaultDTO>();
		
		/*vaultDtoList = vaultList.stream()
				.map(vaultDto -> new EntityConverter<DocumentVault, DocumentVaultDTO>(
						DocumentVault.class, DocumentVaultDTO.class).toT2(vaultDto, new DocumentVaultDTO()))
						.collect(Collectors.toList());*/
		
		for(DocumentVault vault : vaultList){
			DocumentVaultDTO dto = new DocumentVaultDTO();
			dto.setDocFlag(vault.getDocFlag());
			dto.setDocId(vault.getDocId());
			dto.setDocName(vault.getDocName());
			dto.setDocSection(vault.getDocSection());
			dto.setDocumentParsingStatus(vault.getDocumentParsingStatus());
			dto.setEntityId(vault.getEntityId());
			dto.setEntitySource(vault.getEntitySource());
			dto.setModifiedBy(vault.getModifiedBy());
			dto.setModifiedOn(vault.getModifiedOn());
			dto.setRemark(vault.getRemark());
			dto.setSize(vault.getSize());
			dto.setTitle(vault.getTitle());
			dto.setType(vault.getType());
			dto.setUploadedBy(vault.getUploadedBy().getUserId());
			dto.setUploadedOn(vault.getUploadedOn());
			vaultDtoList.add(dto);
			}
		return vaultDtoList;
	}
}