package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Rambabu
 * 
 */
@ApiModel("Transactions count and ratio")
public class TransactionsCountAndRatioResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Transactions alerts count and ratio")	
	private TxsListAggRespDto transactionsListAlertCountRatioDto;
	@ApiModelProperty(value="pagenation")	
	private PaginationInformation pagination;
	public TransactionsCountAndRatioResponseDto() {
		super();
		
	}
	public TxsListAggRespDto getTransactionsListAlertCountRatioDto() {
		return transactionsListAlertCountRatioDto;
	}
	public void setTransactionsListAlertCountRatioDto(TxsListAggRespDto transactionsListAlertCountRatioDto) {
		this.transactionsListAlertCountRatioDto = transactionsListAlertCountRatioDto;
	}
	public PaginationInformation getPagination() {
		return pagination;
	}
	public void setPagination(PaginationInformation pagination) {
		this.pagination = pagination;
	}
	
	

}
