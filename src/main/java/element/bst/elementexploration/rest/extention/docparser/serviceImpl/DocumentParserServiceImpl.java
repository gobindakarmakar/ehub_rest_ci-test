package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.IBody;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.tika.exception.TikaException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.opencsv.CSVReaderHeaderAware;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.dto.Answer;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.docparser.dao.AnswerTopEventsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentAnswersDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentContentsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentISOCodeDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentQuestionsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentStatementsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTableColumnsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTableRowsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplateMappingDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplatesDao;
import element.bst.elementexploration.rest.extention.docparser.dao.ISOCodeDao;
import element.bst.elementexploration.rest.extention.docparser.dao.IsoCategoryDao;
import element.bst.elementexploration.rest.extention.docparser.domain.AnswerTopEvents;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentContents;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentStatements;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableColumns;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableRows;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.extention.docparser.dto.Categories;
import element.bst.elementexploration.rest.extention.docparser.dto.CheckPdfTypeDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ColumDataDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ColumnsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ContentDataDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ContentTypeTableDto;
import element.bst.elementexploration.rest.extention.docparser.dto.CoordinatesResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DOCResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentContentDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentParagraphDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentTableRowsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ParagraphDto;
import element.bst.elementexploration.rest.extention.docparser.dto.PossibleAnswersDto;
import element.bst.elementexploration.rest.extention.docparser.dto.RowsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TableColumnsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TableDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TableSubColumnDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TopEventsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.ValueDto;
import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentAnswersService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentContentsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentStatementsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTableColumnsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTableRowsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import element.bst.elementexploration.rest.extention.docparser.service.IsoCategoryService;
import element.bst.elementexploration.rest.extention.docparser.service.PossibleAnswersService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.PDFLayoutTextStripper;
import element.bst.elementexploration.rest.util.PdfReaderNewTemplateUtil;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.PdfUtilities;

/**
 * @author suresh
 *
 */
@Service("documentParserService")
public class DocumentParserServiceImpl implements DocumentParserService {

	private Random random = null;

	public DocumentParserServiceImpl() {
		this.random = new Random();
	}

	@Autowired
	DocumentContentsService documentContentsService;

	@Autowired
	DocumentStatementsService documentStatementsService;

	@Autowired
	DocumentTableColumnsService documentTableColumnsService;

	@Autowired
	DocumentTableRowsService documentTableRowsService;

	@Autowired
	DocumentContentsDao documentContentsDao;

	@Autowired
	DocumentStatementsDao documentStatementsDao;

	@Autowired
	DocumentQuestionsService documentsQuestionsService;

	@Autowired
	DocumentTableRowsDao documentTableRowsDao;

	@Autowired
	DocumentTableColumnsDao documentTableColumnsDao;

	@Autowired
	DocumentAnswersService documentAnswersService;

	@Autowired
	DocumentQuestionsDao documentQuestionsDao;

	@Autowired
	DocumentAnswersDao documentAnswersDao;

	@Autowired
	DocumentISOCodeDao documentISOCodeDao;

	@Autowired
	ISOCodeDao isoCodeDao;

	@Autowired
	UsersDao usersDao;

	@Autowired
	DocumentTemplatesDao documentTemplatesDao;

	@Autowired
	DocumentTemplatesService documentTemplatesService;

	@Autowired
	DocumentTemplateMappingDao documentTemplateMappingDao;

	@Autowired
	AnswerTopEventsDao answerTopEventsDao;

	@Autowired
	IsoCategoryDao isoCategoryDao;

	@Autowired
	IsoCategoryService isoCategoryService;

	@Autowired
	CaseDao caseDao;

	@Autowired
	DocumentService documentService;

	@Autowired
	PdfReaderNewTemplateUtil pdfReaderNewTemplateUtil;

	@Autowired
	private DocumentDao documentDao;

	@Autowired
	PossibleAnswersService possibleAnswersService;

	@Value("${isocode_question_url}")
	private String ISOCODE_QUESTION_URL;

	@Value("${tesseract_language}")
	private String TESSRACT_LANGUAGE;

	@Value("${imageAllocationArea}")
	private String TESSRACT_IMAGE_ALLOCATION;
	
	@Value("${bigdata_case_management_s3_doc_url}")
	private String CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL;
	
	@Value("${client_id}")
	private String CLIENT_ID;

	@Value("${iso_category_url}")
	private String ISOCODE_CATEGORY_URL;

	@Value("${bigdata_create_doc_url}")
	private String BIG_DATA_BASE_URL;

	@SuppressWarnings("resource")
	@Override
	@Transactional("transactionManager")
	public List<DOCResponseDto> exploreOptions(byte[] streamFile, Long docId)
			throws IllegalStateException, IOException, InvalidFormatException {

		XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(streamFile));
		List<DOCResponseDto> docResponseDtoList = new ArrayList<DOCResponseDto>();
		try {
			Iterator<IBodyElement> iter = doc.getBodyElementsIterator();
			int i = 0;
			int j = 0;
			while (iter.hasNext()) {
				DOCResponseDto docResponseDto = new DOCResponseDto();
				IBodyElement elem = iter.next();
				if (elem instanceof XWPFParagraph) {
					ParagraphDto paragraphDto = new ParagraphDto();
					List<XWPFParagraph> paragraphs = doc.getParagraphs();

					if ((paragraphs.get(i).getStyle() != null) && (paragraphs.get(i).getNumFmt() != null)) {
						paragraphDto.setStyle(paragraphs.get(i).getStyle());
						paragraphDto.setNumberFont(paragraphs.get(i).getNumFmt());
						List<XWPFRun> run = paragraphs.get(i).getRuns();
						for (XWPFRun xwpfRun : run) {
							paragraphDto.setFontSize(xwpfRun.getFontSize());
							paragraphDto.setColour(xwpfRun.getColor());
							paragraphDto.setIsBold(xwpfRun.isBold());
							System.out.println(xwpfRun.getFontSize());
							System.out.println(xwpfRun.getColor());
							System.out.println(xwpfRun.isBold());
							System.out.println(xwpfRun.getFontFamily());
							// System.out.println(xwpfRun.getCTR());
							System.out.println(xwpfRun.getUnderline());
						}
						System.out.println(paragraphs.get(i).getText() + "++++++++++++++++++++++++++");
						paragraphDto.setContentType("SUB_PARAGRAPH");
						paragraphDto.setContent(paragraphs.get(i).getText());
						docResponseDto.setContentTypeParagraph(paragraphDto);
					} else {
						System.out.println(paragraphs.get(i).getText() + "++++++++++++++++++++++++++");
						paragraphDto.setStyle(paragraphs.get(i).getStyle());
						paragraphDto.setNumberFont(paragraphs.get(i).getNumFmt());
						String content = paragraphs.get(i).getText();
						// System.out.println(content);
						paragraphDto.setContentType("PARAGRAPH");
						paragraphDto.setContent(content.toString());
						List<XWPFRun> run = paragraphs.get(i).getRuns();
						for (XWPFRun xwpfRun : run) {
							paragraphDto.setFontSize(xwpfRun.getFontSize());
							paragraphDto.setColour(xwpfRun.getColor());
							paragraphDto.setIsBold(xwpfRun.isBold());
							System.out.println(xwpfRun.getFontSize());
							System.out.println(xwpfRun.getColor());
							System.out.println(xwpfRun.isBold());
							System.out.println(xwpfRun.getFontFamily());
							// System.out.println(xwpfRun.getCTR());
							System.out.println(xwpfRun.getUnderline());
						}
						docResponseDto.setContentTypeParagraph(paragraphDto);
					}
					docResponseDtoList.add(docResponseDto);
					i++;

				} else if (elem instanceof XWPFTable) {
					List<XWPFTable> xwpfTable = doc.getTables();
					XWPFTable table = xwpfTable.get(j);
					TableDto tableDto = new TableDto();
					List<ValueDto> valueDtosList = new ArrayList<ValueDto>();
					for (XWPFTableRow row : table.getRows()) {
						ValueDto valueDto = new ValueDto();
						List<LinkedHashMap<String, BigInteger>> values = new ArrayList<LinkedHashMap<String, BigInteger>>();
						for (XWPFTableCell cell : row.getTableCells()) {

							// System.out.println(cell.getCTTc().getTcPr().getGridSpan());
							// cell.getCTTc().getTcPr().getGridSpan()
							// System.out.println(cell.getCTTc().getTcPr().getVMerge());
							// System.out.println(cell.getCTTc().getTcPr().getGridSpan().getVal());

							LinkedHashMap<String, BigInteger> map = new LinkedHashMap<String, BigInteger>();
							map.put(cell.getText(), null);
							System.out.println(cell.getBodyElements() + "*********************************");
							List<IBodyElement> bodyElements = cell.getBodyElements();
							for (IBodyElement iBodyElement : bodyElements) {
								IBody body = iBodyElement.getBody();
								List<XWPFParagraph> p = body.getParagraphs();
								for (XWPFParagraph xwpfParagraph : p) {
									System.out.println(xwpfParagraph.getText() + "Inside Tbale value");

								}
							}
							values.add(map);
							// System.out.print(cell.getText());
							// System.out.print("\t");

						}
						// System.out.println("");
						valueDto.setRowWiseAnsweredData(values);
						valueDtosList.add(valueDto);
					}
					// tableDto.setContentTypeTableList(valueDtosList);
					// tableDto.setContentType("TABLE");
					docResponseDto.setContentTypeTable(tableDto);
					docResponseDtoList.add(docResponseDto);
					j++;
				}

			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return docResponseDtoList;
	}

	@Override
	public boolean saveIntoDatabase(byte[] streamFile, Long docId)
			throws IllegalStateException, IOException, InvalidFormatException {

		@SuppressWarnings("resource")
		XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(streamFile));
		Iterator<IBodyElement> iter = doc.getBodyElementsIterator();

		// saving content type into database
		int i = 0;
		int j = 0;
		String[] questionAndAnswers;
		String[] answers;
		try {
			while (iter.hasNext()) {
				IBodyElement elem = iter.next();
				// for paragraph
				if (elem instanceof XWPFParagraph) {
					List<XWPFParagraph> paragraphs = doc.getParagraphs();
					// style found
					if ((paragraphs.get(i).getStyle() != null) && (paragraphs.get(i).getNumFmt() != null)) {
						DocumentContents documentContents = new DocumentContents();
						documentContents.setContentType("SUB_PARAGRAPH");
						// documentContents.setDocId(docId);
						documentContentsService.save(documentContents);

						DocumentStatements documentStatements = new DocumentStatements();
						if (paragraphs.get(i).getText().contains("?")) {
							questionAndAnswers = paragraphs.get(i).getText().split("(?<=\\?/?)");
							if (questionAndAnswers.length > 1) {
								answers = questionAndAnswers[1].split("/");
								if (answers.length > 1) {
									Character yes = new Character(answers[0].trim().charAt(3));
									System.out.println("yes = " + yes.hashCode());
									Character no = new Character(answers[1].trim().charAt(2));
									System.out.println("no = " + no.hashCode());
									System.out.println("question = " + questionAndAnswers[0]);
									documentStatements.setContentQuestion(questionAndAnswers[0]);
									documentStatements.setAnswerType(AnswerType.CHECKBOX);
									documentStatements.setExpectedAnswer("yes,no");
									documentStatements.setQuestion(true);
									if (yes.hashCode() == 9746) {
										System.out.println("answer = yes");
										documentStatements.setContentAnswer("yes");
									} else {
										if (no.hashCode() == 9746) {
											System.out.println("answer = no");
											documentStatements.setContentAnswer("no");
										} else
											documentStatements.setContentAnswer("empty");
									}
								}
							}
						}

						else {
							documentStatements.setContentQuestion(paragraphs.get(i).getText());
							documentStatements.setQuestion(false);
						}
						documentStatements.setDocumentContent(documentContents);
						documentStatementsService.save(documentStatements);
					}
					// not found
					else {
						DocumentContents documentContents = new DocumentContents();
						documentContents.setContentType("PARAGRAPH");
						// documentContents.setDocId(docId);
						documentContentsService.save(documentContents);

						DocumentStatements documentStatements = new DocumentStatements();
						if (paragraphs.get(i).getText().contains("?")) {
							System.out.println("---------------------------------");
							questionAndAnswers = paragraphs.get(i).getText().split("(?<=\\?/?)");
							if (questionAndAnswers.length > 1) {
								answers = questionAndAnswers[1].split("/");
								if (answers.length > 1) {
									Character yes = new Character(answers[0].trim().charAt(3));
									System.out.println("yes = " + yes.hashCode());
									Character no = new Character(answers[1].trim().charAt(2));
									System.out.println("no = " + no.hashCode());
									System.out.println("question = " + questionAndAnswers[0]);
									documentStatements.setContentQuestion(questionAndAnswers[0]);
									documentStatements.setAnswerType(AnswerType.CHECKBOX);
									documentStatements.setExpectedAnswer("yes,no");
									documentStatements.setQuestion(true);
									if (yes.hashCode() == 9746) {
										System.out.println("answer = yes");
										documentStatements.setContentAnswer("yes");
									} else {
										if (no.hashCode() == 9746) {
											System.out.println("answer = no");
											documentStatements.setContentAnswer("no");
										} else
											documentStatements.setContentAnswer("empty");
									}
								}
								System.out.println("---------------------------------");
							}
						} else {
							documentStatements.setContentQuestion(paragraphs.get(i).getText());
							documentStatements.setQuestion(false);
						} //
						documentStatements.setContentQuestion(paragraphs.get(i).getText());
						documentStatements.setDocumentContent(documentContents);
						documentStatementsService.save(documentStatements);
					}
					i++;

				} // if close
					// found table

				else if (elem instanceof XWPFTable) {
					DocumentContents documentContents = new DocumentContents();
					documentContents.setContentType("TABLE");
					// documentContents.setDocId(docId);
					documentContentsService.save(documentContents);

					List<XWPFTable> xwpfTable = doc.getTables();
					XWPFTable table = xwpfTable.get(j);
					long colid1 = 0;
					long colid2 = 0;
					long colid3 = 0;
					long colid4 = 0;
					long colid5 = 0;
					long innerColid1 = 0;
					long innerColid2 = 0;
					long innerColid3 = 0;
					long innerColid4 = 0;
					long innerColid5 = 0;
					long innerColid6 = 6;
					long innerColid7 = 0;
					int z = 1;
					int y = 1;
					int w = 1;
					for (XWPFTableRow row : table.getRows()) {
						if (j == 0 || j == 3) {
							boolean value = true;
							long id = 0;
							for (XWPFTableCell cell : row.getTableCells()) {
								DocumentTableColumns tableColumns = new DocumentTableColumns();
								if (value) {
									System.out.println(cell.getText());
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									id = tableColumnsSaved.getId();
									value = false;
								} else {
									if (cell.getText().equals("Telephone:")) {
										tableColumns.setColumnName(cell.getText());
										tableColumns.setDocumentContent(documentContents);
										DocumentTableColumns tableColumnsSaved = documentTableColumnsService
												.save(tableColumns);
										id = tableColumnsSaved.getId();
										value = false;
									} else {
										DocumentTableRows documentTableRows = new DocumentTableRows();
										documentTableRows.setRowName(cell.getText());
										if (checkYesOrNo(cell.getTextRecursively().trim().hashCode())) {
											documentTableRows.setRowName("yes");
										}
										documentTableRows.setDocumentTableColumns(documentTableColumnsService.find(id));
										documentTableRowsService.save(documentTableRows);
									}
								}
							} // inside for
						} // j if close -------1st table
						if (j == 1) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2)
										colid2 = tableColumnsSaved.getId();
									if (z == 3) {
										colid3 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								} else {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid2));
									if (w == 3) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid3));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}

							}
						} // 2nd table
						if (j == 2) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1 || y == 3) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2)
										colid2 = tableColumnsSaved.getId();
									if (z == 3) {
										colid3 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								}

								if (y == 2 || y == 4) {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									System.out.println(cell.getText());
									System.out.println(cell.hashCode());
									System.out.println(cell.getTextRecursively().trim().hashCode());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2) {
										if ((cell.getTextRecursively().trim().hashCode() == 9746)) {
											documentTableRows.setRowName("yes");
											// yesOrNoFlag=true;
										}
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid2));
									}
									if (w == 3) {
										if ((cell.getTextRecursively().trim().hashCode() == 9746)) {
											documentTableRows.setRowName("no");
											// yesOrNoFlag=true;
										}
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid3));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}
							}
						} // 3rd table

						if (j == 4) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2)
										colid2 = tableColumnsSaved.getId();
									if (z == 3)
										colid3 = tableColumnsSaved.getId();
									if (z == 4) {
										colid4 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								}

								if (y == 2) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									if (z == 1)
										tableColumns.setParentId(colid1);
									if (z == 2 || z == 3)
										tableColumns.setParentId(colid2);
									if (z == 4 || z == 5)
										tableColumns.setParentId(colid3);
									if (z == 6 || z == 7)
										tableColumns.setParentId(colid4);
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										innerColid1 = tableColumnsSaved.getId();
									if (z == 2)
										innerColid2 = tableColumnsSaved.getId();
									if (z == 3)
										innerColid3 = tableColumnsSaved.getId();
									if (z == 4)
										innerColid4 = tableColumnsSaved.getId();
									if (z == 5)
										innerColid5 = tableColumnsSaved.getId();
									if (z == 6)
										innerColid6 = tableColumnsSaved.getId();
									if (z == 7) {
										innerColid7 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								}

								if (y > 2 && y <= 8) {

									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid1));
									if (w == 2)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid2));
									if (w == 3)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid3));
									if (w == 4)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid4));
									if (w == 5)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid5));
									if (w == 6)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid6));
									if (w == 7) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid7));
										w = 0;
									}

									documentTableRowsService.save(documentTableRows);
									w++;
								}

								if (y == 9) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									if (z == 1)
										tableColumns.setParentId(colid1);
									if (z == 2 || z == 3)
										tableColumns.setParentId(colid2);
									if (z == 4 || z == 5)
										tableColumns.setParentId(colid3);
									if (z == 6 || z == 7)
										tableColumns.setParentId(colid4);
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										innerColid1 = tableColumnsSaved.getId();
									if (z == 2)
										innerColid2 = tableColumnsSaved.getId();
									if (z == 3)
										innerColid3 = tableColumnsSaved.getId();
									if (z == 4)
										innerColid4 = tableColumnsSaved.getId();
									if (z == 5)
										innerColid5 = tableColumnsSaved.getId();
									if (z == 6)
										innerColid6 = tableColumnsSaved.getId();
									if (z == 7) {
										innerColid7 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								}

								if (y > 9) {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid1));
									if (w == 2)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid2));
									if (w == 3)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid3));
									if (w == 4)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid4));
									if (w == 5)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid5));
									if (w == 6)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid6));
									if (w == 7) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(innerColid7));
										w = 0;
									}

									documentTableRowsService.save(documentTableRows);
									w++;
								}

							}
						} // 4 th table

						if (j == 5) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2) {
										colid2 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								} else {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid2));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}

							}

						} // 5 th table

						if (j == 6 || j == 7 || j == 9 || j == 12 || j == 13 || j == 14 || j == 15 || j == 16 || j == 17
								|| j == 18 || j == 19 || j == 21 || j == 26 || j == 27 || j == 28 || j == 29 || j == 30
								|| j == 31 || j == 32 || j == 33 || j == 34 || j == 35) {
							for (XWPFTableCell cell : row.getTableCells()) {
								DocumentTableColumns tableColumns = new DocumentTableColumns();
								tableColumns.setDocumentContent(documentContents);
								DocumentTableColumns tableColumnsSaved = documentTableColumnsService.save(tableColumns);
								colid1 = tableColumnsSaved.getId();
								DocumentTableRows documentTableRows = new DocumentTableRows();
								documentTableRows.setRowName(cell.getText());
								documentTableRows.setDocumentTableColumns(documentTableColumnsService.find(colid1));
								documentTableRowsService.save(documentTableRows);
							}

						} // 6th table & 7th table

						if (j == 8 || j == 10 || j == 11) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2) {
										colid2 = tableColumnsSaved.getId();
										z = 0;
									}
									z++;
								}

								if (y > 1) {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid2));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}
							}

						} // 8th table

						if (j == 20) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2)
										colid2 = tableColumnsSaved.getId();
									if (z == 3)
										colid3 = tableColumnsSaved.getId();
									if (z == 4)
										colid4 = tableColumnsSaved.getId();
									if (z == 5) {
										colid5 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								} else {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2)
										if (checkYesOrNo(cell.getTextRecursively().trim().hashCode())) {
											documentTableRows.setRowName("yes");
										}
									documentTableRows.setDocumentTableColumns(documentTableColumnsService.find(colid2));
									if (w == 3)
										if (checkYesOrNo(cell.getTextRecursively().trim().hashCode())) {
											documentTableRows.setRowName("no");
										}
									documentTableRows.setDocumentTableColumns(documentTableColumnsService.find(colid3));
									if (w == 4)
										if (checkYesOrNo(cell.getTextRecursively().trim().hashCode())) {
											documentTableRows.setRowName("not applicable");
										}
									documentTableRows.setDocumentTableColumns(documentTableColumnsService.find(colid4));
									if (w == 5) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid5));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}
							}
						} // 20th table

						if (j == 22) {
							for (XWPFTableCell cell : row.getTableCells()) {
								DocumentTableColumns tableColumns = new DocumentTableColumns();
								tableColumns.setColumnName(cell.getText());
								System.out.println(cell.getTextRecursively());
								String[] checkBoxAnswers = cell.getTextRecursively().split(" ");
								System.out.println(checkBoxAnswers[0]);
								if ((checkBoxAnswers.length > 1) && (checkBoxAnswers[1].hashCode() == 9746)) {
									tableColumns.setColumnName(checkBoxAnswers[0] + " yes");
								}
								tableColumns.setDocumentContent(documentContents);
								documentTableColumnsService.save(tableColumns);
							}
						} // 22nd table

						if (j == 23) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2)
										colid2 = tableColumnsSaved.getId();
									if (z == 3)
										colid3 = tableColumnsSaved.getId();

									if (z == 4) {
										colid4 = tableColumnsSaved.getId();
										z = 0;
									}
									System.out.println(cell.getText());
									z++;
								} else {
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2) {
										if ((cell.getTextRecursively().trim().hashCode() == 9746)) {
											documentTableRows.setRowName("yes");
										}
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid2));
									}
									if (w == 3) {
										if ((cell.getTextRecursively().trim().hashCode() == 9746)) {
											documentTableRows.setRowName("no");
										}
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid3));
									}
									if (w == 4) {
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid4));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}
							}
						} // 23 rd table

						if (j == 36) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentTableColumns tableColumns = new DocumentTableColumns();
									tableColumns.setColumnName(cell.getText());
									tableColumns.setDocumentContent(documentContents);
									DocumentTableColumns tableColumnsSaved = documentTableColumnsService
											.save(tableColumns);
									if (z == 1)
										colid1 = tableColumnsSaved.getId();
									if (z == 2) {
										colid2 = tableColumnsSaved.getId();
										z = 0;
									}
									z++;
								}

								if (y > 1) {
									String[] yesOrNoAnswers;
									DocumentTableRows documentTableRows = new DocumentTableRows();
									documentTableRows.setRowName(cell.getText());
									if (w == 1)
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid1));
									if (w == 2) {
										yesOrNoAnswers = cell.getText().split("/");
										if (yesOrNoAnswers.length > 1) {

											Character yes = new Character(yesOrNoAnswers[0].trim().charAt(3));
											System.out.println("yes = " + yes.hashCode());
											Character no = new Character(yesOrNoAnswers[1].trim().charAt(2));
											System.out.println("no = " + no.hashCode());
											// System.out.println("question = " + questionAndAnswers[0]);
											// documentStatements.setContentQuestion(questionAndAnswers[0]);
											// documentStatements.setQuestion(true);
											if (yes.hashCode() == 9746) {
												// System.out.println("answer = yes");
												documentTableRows.setRowName("yes");
											} else {
												if (no.hashCode() == 9746) {
													// System.out.println("answer = no");
													documentTableRows.setRowName("no");
												} else
													documentTableRows.setRowName("empty");
											}

										}
										documentTableRows
												.setDocumentTableColumns(documentTableColumnsService.find(colid2));
										w = 0;
									}
									documentTableRowsService.save(documentTableRows);
									w++;
								}
							}
						}

					} // outside for
					y++;// for row iteration
					j++;// for table iteration
				} // else if close for table

			} // while close
		} // try
		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public List<DocumentResponseDto> getDocumentContentById(Long docId) {
		List<DocumentResponseDto> documentResponseDtoList = new ArrayList<DocumentResponseDto>();

		List<DocumentContents> documentContentsList = documentContentsDao.getDocumentContentById(docId);

		try {
			for (DocumentContents documentContents : documentContentsList) {
				DocumentResponseDto documentResponseDto = new DocumentResponseDto();
				DocumentParagraphDto documentParagraphDto = new DocumentParagraphDto();
				DocumentParagraphDto documentSubParagraphDto = new DocumentParagraphDto();
				if (documentContents.getContentType().trim().equalsIgnoreCase("PARAGRAPH")) {
					DocumentStatements documentStatements = documentStatementsDao
							.getDocumentStatementById(documentContents.getId());
					if (documentStatements != null) {
						documentParagraphDto.setContentType("PARAGRAPH");
						if (documentStatements.isQuestion()) {
							documentParagraphDto.setQuestion(documentStatements.getContentQuestion());
							documentParagraphDto.setAnswer(documentStatements.getContentAnswer());
							documentParagraphDto.setQuestion(documentStatements.isQuestion());
							documentParagraphDto.setAnswerType(documentStatements.getAnswerType().name());
							documentParagraphDto.setExpectedAnswer(documentStatements.getExpectedAnswer());

						} else {
							documentParagraphDto.setParagraphTitle(documentStatements.getContentQuestion());
						}
						documentResponseDto.setDocumentParagraphDto(documentParagraphDto);

					}
				}
				if (documentContents.getContentType().trim().equalsIgnoreCase("SUB_PARAGRAPH")) {
					DocumentStatements documentSubStatements = documentStatementsDao
							.getDocumentStatementById(documentContents.getId());
					if (documentSubStatements != null) {
						documentSubParagraphDto.setContentType("SUB PARAGRAPH");
						if (documentSubStatements.isQuestion()) {
							documentSubParagraphDto.setQuestion(documentSubStatements.getContentQuestion());
							documentSubParagraphDto.setAnswer(documentSubStatements.getContentAnswer());
							documentSubParagraphDto.setQuestion(documentSubStatements.isQuestion());
							documentSubParagraphDto.setAnswerType(documentSubStatements.getAnswerType().name());
							documentSubParagraphDto.setExpectedAnswer(documentSubStatements.getExpectedAnswer());
						} else {
							documentSubParagraphDto.setParagraphTitle(documentSubStatements.getContentQuestion());
						}
						documentResponseDto.setDocumentParagraphDto(documentSubParagraphDto);

					}
				}
				if (documentContents.getContentType().trim().equalsIgnoreCase("TABLE")) {
					ContentTypeTableDto contentTypeTableDto = new ContentTypeTableDto();
					List<DocumentTableColumns> tableColumnsList = documentTableColumnsDao
							.getAllColumnsByTableId(documentContents.getId());
					List<TableColumnsDto> tableColumnsDtoList = new ArrayList<TableColumnsDto>();
					for (DocumentTableColumns documentTableColumns : tableColumnsList) {
						TableColumnsDto tableColumnsDto = new TableColumnsDto();
						List<DocumentTableColumns> childColumnsList = documentTableColumnsDao
								.getAllChildColumsByParentId(documentTableColumns.getId());
						List<TableSubColumnDto> tableSubColumnDtoList = new ArrayList<TableSubColumnDto>();
						if (childColumnsList.size() != 0) {
							for (DocumentTableColumns documentTableColumns2 : childColumnsList) {
								TableSubColumnDto tableSubColumnDto = new TableSubColumnDto();
								List<DocumentTableRowsDto> childColumnsDataList = documentTableRowsDao
										.getAllRowsDataByColumnId(documentTableColumns2.getId());
								tableSubColumnDto.setSubColumnName(documentTableColumns2.getColumnName());
								tableSubColumnDto.setSubColumnData(childColumnsDataList);
								tableSubColumnDtoList.add(tableSubColumnDto);
							}
							tableColumnsDto.setColumnName(documentTableColumns.getColumnName());
							tableColumnsDto.setSubColumns(tableSubColumnDtoList);

						}

						else {
							List<DocumentTableRowsDto> columnsDataList = documentTableRowsDao
									.getAllRowsDataByColumnId(documentTableColumns.getId());
							tableColumnsDto.setColumnName(documentTableColumns.getColumnName());
							tableColumnsDto.setColumnsData(columnsDataList);
							System.out.println(tableColumnsDto.toString());
						}
						tableColumnsDtoList.add(tableColumnsDto);
					}
					contentTypeTableDto.setContentType("TABLE");
					contentTypeTableDto.setContent(tableColumnsDtoList);
					documentResponseDto.setContentTypeTableDto(contentTypeTableDto);

				}
				documentResponseDtoList.add(documentResponseDto);
			}

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return documentResponseDtoList;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public List<DocumentContentDto> getDocumentParseDataById(Long docId) {
		List<DocumentContentDto> documentContentDtoList = new ArrayList<DocumentContentDto>();
		List<DocumentContents> documentContentsList = documentContentsDao.getDocumentContentById(docId);

		/*
		 * try { int i = 0; for (DocumentContents documentContents :
		 * documentContentsList) { DocumentContentDto documentContentDto = new
		 * DocumentContentDto(); DocumentQuestions documentQuestions =
		 * documentQuestionsDao .getDocumentQuestionsById(documentContents.getId());
		 * DocumentAnswers documentAnswers = new DocumentAnswers(); ContentDataDto
		 * contentDataDto = new ContentDataDto(); i++; if (documentQuestions != null) {
		 * if (documentContents.getContentType().trim().equalsIgnoreCase("PARAGRAPH")) {
		 * 
		 * documentContentDto.setContentType("PARAGRAPH"); if (i == 2)
		 * documentContentDto.setContentType("HEADING"); if
		 * (documentQuestions.isQuestion()) { documentAnswers = documentAnswersDao
		 * .getDcoumentAnswerByQuestionId(documentQuestions.getId());
		 * documentContentDto.setQuestion(documentQuestions.isQuestion());
		 * contentDataDto.setQuestion(documentQuestions.getName());
		 * contentDataDto.setQuestionId(documentQuestions.getId());
		 * contentDataDto.setAnswerType(documentQuestions.getAnswerType());
		 * contentDataDto.setPossibleAnswer(documentQuestions.getPossibleAnswer());
		 * contentDataDto.setRiskScore(documentQuestions.getRiskScore()); if
		 * (documentAnswers != null) {
		 * contentDataDto.setAnswer(documentAnswers.getAnswer());
		 * contentDataDto.setAnswerId(documentAnswers.getId()); }
		 * 
		 * } if (!documentQuestions.isQuestion()) {
		 * documentContentDto.setQuestion(documentQuestions.isQuestion());
		 * contentDataDto.setText(documentQuestions.getName()); }
		 * documentContentDto.setContentDataDto(contentDataDto); } if
		 * (documentContents.getContentType().trim().equalsIgnoreCase("SUB_PARAGRAPH"))
		 * { documentContentDto.setContentType("SUB PARAGRAPH"); if
		 * (documentQuestions.isQuestion()) { documentAnswers = documentAnswersDao
		 * .getDcoumentAnswerByQuestionId(documentQuestions.getId());
		 * documentContentDto.setQuestion(documentQuestions.isQuestion());
		 * contentDataDto.setQuestion(documentQuestions.getName());
		 * contentDataDto.setQuestionId(documentQuestions.getId());
		 * contentDataDto.setAnswerType(documentQuestions.getAnswerType());
		 * contentDataDto.setPossibleAnswer(documentQuestions.getPossibleAnswer());
		 * contentDataDto.setRiskScore(documentQuestions.getRiskScore()); if
		 * (documentAnswers != null) {
		 * contentDataDto.setAnswer(documentAnswers.getAnswer());
		 * contentDataDto.setAnswerId(documentAnswers.getId()); } } if
		 * (!documentQuestions.isQuestion()) {
		 * documentContentDto.setQuestion(documentQuestions.isQuestion());
		 * contentDataDto.setText(documentQuestions.getName()); }
		 * 
		 * documentContentDto.setContentDataDto(contentDataDto); }
		 * 
		 * if (documentContents.getContentType().trim().equalsIgnoreCase("TABLE")) {
		 * 
		 * 
		 * documentContentDto.setContentType("TABLE"); List<DocumentQuestions>
		 * documentTableQuestions =
		 * documentQuestionsDao.getTableQuestionsById(documentContents.getId());
		 * if(documentTableQuestions.size()>0){ HashSet<String> tableRowsCount=new
		 * HashSet<String>(); HashSet<String> tableColumnsCount=new HashSet<String>();
		 * String answerMatrix=null; String questionMatrix=null; for (DocumentQuestions
		 * documentQuestionsTable : documentTableQuestions) {
		 * answerMatrix=documentQuestionsTable.getComponentAnswerMatrix();
		 * if(answerMatrix!=null){ String rowCount[]=answerMatrix.trim().split("\\*");
		 * tableRowsCount.add(rowCount[0]); }
		 * questionMatrix=documentQuestionsTable.getComponentQuestionMatrix();
		 * if(questionMatrix!=null){ if(questionMatrix.contains(",")){ String
		 * colums[]=questionMatrix.split(","); String colum[]=colums[1].split("\\*");
		 * tableColumnsCount.add(colum[1]); }else{ String
		 * colums1[]=questionMatrix.split("\\*"); tableColumnsCount.add(colums1[1]); }
		 * 
		 * }
		 * 
		 * } tableColumnsCount.add("extracolumn"); tableRowsCount.add("extrarow");
		 * System.out.println(tableRowsCount.size());
		 * 
		 * System.out.println(tableColumnsCount.size()); ContentDataTableDto
		 * contentDataTableDto=new ContentDataTableDto(); ContentDataTableDto
		 * contentDataTableDto1=new ContentDataTableDto(); List<DataTableRowsDto>
		 * dataTableRows=new ArrayList<DataTableRowsDto>(); for(int
		 * a=0;a<tableRowsCount.size();a++){ DataTableRowsDto dataTableRowsDto=new
		 * DataTableRowsDto(); List<DataTableColumsDto> dataTableColums=new
		 * ArrayList<DataTableColumsDto>(); for(int b=0;b<tableColumnsCount.size();b++){
		 * DataTableColumsDto dataTableColumsDto=new DataTableColumsDto();
		 * ContentDataDto tableContentDataDto=new ContentDataDto();
		 * dataTableColumsDto.setContentDataDto(tableContentDataDto);
		 * dataTableColums.add(dataTableColumsDto);
		 * 
		 * } dataTableRowsDto.setDataTableColumsDto(dataTableColums);
		 * dataTableRows.add(dataTableRowsDto); }
		 * 
		 * for (DocumentQuestions documentQuestionsTable1 : documentTableQuestions) {
		 * documentAnswers = documentAnswersDao
		 * .getDcoumentAnswerByQuestionId(documentQuestionsTable1.getId());
		 * ContentDataDto contentDataDtoTable1=new ContentDataDto(); ContentDataDto
		 * contentDataDtoTable2=new ContentDataDto(); ContentDataDto
		 * contentDataDtoTableAns=new ContentDataDto(); String
		 * answerMatrix1=documentQuestionsTable1.getComponentAnswerMatrix(); String[]
		 * answerMatrix1Pos=answerMatrix1.split("\\*");
		 * 
		 * contentDataDtoTableAns.setAnswerId(documentAnswers.getId());
		 * contentDataDtoTableAns.setAnswer(documentAnswers.getAnswer()); int
		 * ansRowPosition=Integer.parseInt(answerMatrix1Pos[0])-1; int
		 * ansColumPosition=Integer.parseInt(answerMatrix1Pos[1])-1; String
		 * quesMatrix1=documentQuestionsTable1.getComponentQuestionMatrix();
		 * 
		 * if(quesMatrix1.contains(",")){ String[]
		 * quesMatrix1Split=quesMatrix1.split(","); String[]
		 * firstQues=quesMatrix1Split[0].split("\\*"); String[]
		 * secondQues=quesMatrix1Split[1].split("\\*"); int
		 * firstQuesRow=Integer.parseInt(firstQues[0])-1; int
		 * firstQuesColum=Integer.parseInt(firstQues[1])-1; int
		 * secondQuesRow=Integer.parseInt(secondQues[0])-1; int
		 * secondQuesColum=Integer.parseInt(secondQues[1])-1; String
		 * ques[]=documentQuestionsTable1.getName().split(",");
		 * contentDataDtoTable1.setQuestion(ques[0]);
		 * contentDataDtoTable1.setAnswerType(documentQuestionsTable1.getAnswerType());
		 * 
		 * contentDataDtoTable1.setPossibleAnswer(documentQuestionsTable1.
		 * getPossibleAnswer());
		 * contentDataDtoTable1.setRiskScore(documentQuestionsTable1.getRiskScore());
		 * 
		 * contentDataDtoTable2.setQuestion(ques[1]);
		 * contentDataDtoTable2.setAnswerType(documentQuestionsTable1.getAnswerType());
		 * 
		 * contentDataDtoTable2.setPossibleAnswer(documentQuestionsTable1.
		 * getPossibleAnswer());
		 * contentDataDtoTable2.setRiskScore(documentQuestionsTable1.getRiskScore());
		 * dataTableRows.get(firstQuesRow).getDataTableColumsDto().get(firstQuesColum).
		 * setContentDataDto(contentDataDtoTable1);
		 * dataTableRows.get(secondQuesRow).getDataTableColumsDto().get(secondQuesColum)
		 * .setContentDataDto(contentDataDtoTable2);
		 * 
		 * }else { String[] quesMatrix1Pos=quesMatrix1.split("\\*"); int
		 * quesRowPosition=Integer.parseInt(quesMatrix1Pos[0])-1; int
		 * quesColumPosition=Integer.parseInt(quesMatrix1Pos[1])-1;
		 * contentDataDtoTable1.setQuestion(documentQuestionsTable1.getName());
		 * contentDataDtoTable1.setQuestionId(documentQuestionsTable1.getId());
		 * contentDataDtoTable1.setAnswerType(documentQuestionsTable1.getAnswerType());
		 * 
		 * contentDataDtoTable1.setPossibleAnswer(documentQuestionsTable1.
		 * getPossibleAnswer());
		 * contentDataDtoTable1.setRiskScore(documentQuestionsTable1.getRiskScore());
		 * dataTableRows.get(quesRowPosition).getDataTableColumsDto().get(
		 * quesColumPosition).setContentDataDto(contentDataDtoTable1);
		 * 
		 * }
		 * 
		 * //System.out.println(rowPosition+" ppp"+columPosition);
		 * 
		 * dataTableRows.get(ansRowPosition).getDataTableColumsDto().get(
		 * ansColumPosition).setContentDataDto(contentDataDtoTableAns);
		 * 
		 * 
		 * } contentDataTableDto.setDataTableRowsDto(dataTableRows);
		 * documentContentDto.setContentDataTableDto(contentDataTableDto);
		 * 
		 * 
		 * List<DataTableRowsDto> dataTableRows=new ArrayList<DataTableRowsDto>();
		 * List<DataTableColumsDto> dataTableColums=new ArrayList<DataTableColumsDto>();
		 * DataTableRowsDto dataTableRow=new DataTableRowsDto(); DataTableColumsDto
		 * dataTableColumsDto=new DataTableColumsDto(); DataTableColumsDto
		 * dataTableColumsAnswer=new DataTableColumsDto();
		 * 
		 * for (DocumentQuestions documentQuestionsTable1 : documentTableQuestions) {
		 * 
		 * String questionPosition=documentQuestionsTable1.getComponentQuestionMatrix();
		 * String question[]=questionPosition.split("\\*");
		 * dataTableColumsDto.setId(documentQuestionsTable1.getId());
		 * dataTableColumsDto.setColumData(documentQuestionsTable1.getName());
		 * 
		 * String answerPosition=documentQuestionsTable1.getComponentAnswerMatrix();
		 * String answer[]=answerPosition.split("\\*");
		 * documentAnswers=documentAnswersDao.getDcoumentAnswerByQuestionId(
		 * documentQuestionsTable1.getId());
		 * dataTableColumsAnswer.setId(documentAnswers.getId());
		 * dataTableColumsAnswer.setColumData(documentAnswers.getAnswer());
		 * dataTableColums.add(dataTableColumsDto);
		 * dataTableColums.add(dataTableColumsAnswer); if(Integer.parseInt(answer[1]) !=
		 * dataTableColums.size()){ dataTableColumsDto=new DataTableColumsDto();
		 * dataTableColumsAnswer=new DataTableColumsDto(); }
		 * if(Integer.parseInt(answer[1])==dataTableColums.size()){
		 * dataTableRow.setDataTableColumsDto(dataTableColums);
		 * dataTableRows.add(dataTableRow); dataTableColumsDto=new DataTableColumsDto();
		 * dataTableColumsAnswer=new DataTableColumsDto(); dataTableColums=new
		 * ArrayList<DataTableColumsDto>(); dataTableRow=new DataTableRowsDto(); }
		 * 
		 * 
		 * } contentDataTableDto.setDataTableRowsDto(dataTableRows);
		 * documentContentDto.setContentDataTableDto(contentDataTableDto);
		 * 
		 * }
		 * 
		 * 
		 * 
		 * } }
		 * 
		 * } documentContentDtoList.add(documentContentDto); }
		 * 
		 * } catch (Exception e) { ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass()); }
		 */
		return documentContentDtoList;

	}

	public boolean checkYesOrNo(int hashCode) {
		if (hashCode == 9746)
			return true;
		else
			return false;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public Boolean auditDatabase(byte[] streamFile, Long docId)
			throws IllegalStateException, IOException, InvalidFormatException {

		@SuppressWarnings("resource")
		XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(streamFile));
		Iterator<IBodyElement> iter = doc.getBodyElementsIterator();

		// saving content type into database
		int i = 0;
		int j = 0;
		String[] questionAndAnswers;
		String[] answers;
		long childSubToParagrah = 0;
		int overAll = 0;
		long subParagraphTableId = 0; // 87
		try {
			while (iter.hasNext()) {
				IBodyElement elem = iter.next();
				// for paragraph
				if (elem instanceof XWPFParagraph) {
					List<XWPFParagraph> paragraphs = doc.getParagraphs();
					if (paragraphs.get(i).getText().length() != 0) {
						// style found
						if ((paragraphs.get(i).getStyle() != null) && (paragraphs.get(i).getNumFmt() != null)) {
							DocumentContents documentContents = new DocumentContents();
							documentContents.setContentType("SUB_PARAGRAPH");
							// documentContents.setDocId(docId);
							documentContents.setParentId(calculateOverAll(overAll));
							documentContentsService.save(documentContents);
							System.out.println(
									paragraphs.get(i).getText() + "SUB_PARAGRAPH" + " " + "position :" + overAll);

							DocumentQuestions documentQuestions = new DocumentQuestions();
							DocumentAnswers documentAnswers = new DocumentAnswers();
							// documentQuestions.setName(paragraphs.get(i).getText());
							String[] val = paragraphs.get(i).getText().split(":");
							documentQuestions.setName(val[0]);
							// DocumentISOCodes documentISOCodes =
							// documentISOCodeDao.fetchDocumnetIsoCode(paragraphs.get(i).getText(), true);
							/*
							 * if (documentISOCodes != null)
							 * documentQuestions.setNormalizedQuestion(documentISOCodes.
							 * getNormalizedQuestion());
							 */
							documentQuestions.setQuestionStatus(false);
							documentQuestions.setDocumentContent(documentContents);
							if (overAll == 19) {
								documentQuestions.setAnswerType(AnswerType.CHECKBOX);
								documentQuestions.setQuestionStatus(true);
								String response = getIsoCodesJson(documentQuestions.getName());// primary and secondary
								if (response != null) {
									JSONObject jsonResponseObject = new JSONObject(response);
									JSONArray array = jsonResponseObject.getJSONArray("hits");
									if (array.length() > 0) {
										documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
										documentQuestions
												.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
									} else {
										documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
										documentQuestions
												.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
									}
								}
							}
							DocumentQuestions documentQuestionsSaved = documentsQuestionsService
									.save(documentQuestions);
							if (overAll == 121) {
								subParagraphTableId = documentQuestionsSaved.getId();
								System.out.println(
										overAll + "  " + documentQuestionsSaved.getName() + "***********************");
							} else {
								System.out.println(overAll + "  " + documentQuestionsSaved.getName());
							}

							if (paragraphs.get(i).getText().contains("?")
									|| paragraphs.get(i).getText().contains("YES?/NO?")) {
								questionAndAnswers = paragraphs.get(i).getText().split("(?<=\\?/?)");
								if (questionAndAnswers.length > 1) {
									answers = questionAndAnswers[1].split("/");
									if (answers.length > 1) {
										Character yes = new Character(answers[0].trim().charAt(3));
										Character no = new Character(answers[1].trim().charAt(2));
										documentQuestionsSaved.setName(questionAndAnswers[0]);
										/*
										 * DocumentISOCodes documentISOCodes1 =
										 * documentISOCodeDao.fetchDocumnetIsoCode(questionAndAnswers[0], true); if
										 * (documentISOCodes1 != null) documentQuestionsSaved
										 * .setNormalizedQuestion(documentISOCodes1.getNormalizedQuestion());
										 */

										String response = getIsoCodesJson(questionAndAnswers[0]);// primary and
																									// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}

										documentQuestionsSaved.setAnswerType(AnswerType.CHECKBOX);
										documentQuestionsSaved.setPossibleAnswer("yes,no");
										documentQuestionsSaved.setQuestionStatus(true);
										documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);
										if (answers[1].length() > 4) {
											String specifiedAnswer = answers[1].substring(4, answers[1].length());
											documentAnswers.setSpecifiedAnswer(specifiedAnswer);
										}
										if (yes.hashCode() == 9746) {
											documentAnswers.setAnswer("yes");
											documentAnswers.setDocumentQuestions(
													documentsQuestionsService.find(documentQuestionsSaved.getId()));
										} else {
											if (no.hashCode() == 9746) {
												documentAnswers.setAnswer("no");
												documentAnswers.setDocumentQuestions(
														documentsQuestionsService.find(documentQuestionsSaved.getId()));
											} else
												documentAnswers.setAnswer("empty");
											documentAnswers.setDocumentQuestions(
													documentsQuestionsService.find(documentQuestionsSaved.getId()));
										}

										documentAnswersService.save(documentAnswers);
									} else {
										documentQuestionsSaved.setAnswerType(AnswerType.TEXT_AREA);
										documentQuestionsSaved.setPossibleAnswer("text");
										documentQuestionsSaved.setQuestionStatus(true);
										String response = getIsoCodesJson(documentQuestionsSaved.getName());// primary
																											// and
																											// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}
										documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);
									}

								}
							}
						}
						// not found
						else {
							DocumentContents documentContents = new DocumentContents();
							documentContents.setContentType("PARAGRAPH");
							// documentContents.setDocId(docId);
							documentContents.setParentId(calculateOverAll(overAll));
							System.out
									.println(paragraphs.get(i).getText() + "PARAGRAPH" + " " + "position :" + overAll);
							// documentContentsService.save(documentContents);
							documentContentsService.save(documentContents);

							DocumentQuestions documentQuestions = new DocumentQuestions();
							DocumentAnswers documentAnswers = new DocumentAnswers();
							if (i == 9) {
								String[] val = paragraphs.get(i).getText().split(": -");
								documentQuestions.setName(val[0]);
							} else {
								String[] val = paragraphs.get(i).getText().split(":");
								documentQuestions.setName(val[0]);
							}

							/*
							 * DocumentISOCodes documentISOCodes = documentISOCodeDao
							 * .fetchDocumnetIsoCode(paragraphs.get(i).getText(), true); if
							 * (documentISOCodes != null)
							 * documentQuestions.setNormalizedQuestion(documentISOCodes.
							 * getNormalizedQuestion());
							 */
							documentQuestions.setQuestionStatus(false);
							documentQuestions.setDocumentContent(documentContents);
							DocumentQuestions documentQuestionsSaved = documentsQuestionsService
									.save(documentQuestions);

							if (paragraphs.get(i).getText().contains("?")
									|| paragraphs.get(i).getText().contains("YES?/NO?")) {
								questionAndAnswers = paragraphs.get(i).getText().split("(?<=\\?/?)");
								if (questionAndAnswers.length > 1) {
									answers = questionAndAnswers[1].split("/");
									if (answers.length > 1) {
										Character yes = new Character(answers[0].trim().charAt(3));
										Character no = new Character(answers[1].trim().charAt(2));
										documentQuestionsSaved.setName(questionAndAnswers[0]);
										/*
										 * DocumentISOCodes documentISOCodes1 = documentISOCodeDao
										 * .fetchDocumnetIsoCode(questionAndAnswers[0], true); if (documentISOCodes1 !=
										 * null) documentQuestionsSaved
										 * .setNormalizedQuestion(documentISOCodes1.getNormalizedQuestion());
										 */

										String response = getIsoCodesJson(questionAndAnswers[0]);// primary and
																									// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}

										documentQuestionsSaved.setAnswerType(AnswerType.CHECKBOX);
										documentQuestionsSaved.setPossibleAnswer("yes,no");
										documentQuestionsSaved.setQuestionStatus(true);
										documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);
										if (yes.hashCode() == 9746) {
											documentAnswers.setAnswer("yes");
											documentAnswers.setDocumentQuestions(
													documentsQuestionsService.find(documentQuestionsSaved.getId()));
										} else {
											if (no.hashCode() == 9746) {
												documentAnswers.setAnswer("no");
												documentAnswers.setDocumentQuestions(
														documentsQuestionsService.find(documentQuestionsSaved.getId()));
											} else
												documentAnswers.setAnswer("empty");
											documentAnswers.setDocumentQuestions(
													documentsQuestionsService.find(documentQuestionsSaved.getId()));
										}

										documentAnswersService.save(documentAnswers);
									} else {
										documentQuestionsSaved.setAnswerType(AnswerType.TEXT_AREA);
										documentQuestionsSaved.setPossibleAnswer("text");
										documentQuestionsSaved.setQuestionStatus(true);
										String response = getIsoCodesJson(documentQuestionsSaved.getName());// primary
																											// and
																											// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestionsSaved.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}
										documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);

									}
								}
							}
						}
					} // empty if close

					i++;// for next paragraph iteration

				} // if close for paragraph

				// found table
				else if (elem instanceof XWPFTable) {
					int y = 1;
					DocumentContents documentContents = new DocumentContents();
					documentContents.setContentType("TABLE");
					// documentContents.setDocId(docId);
					documentContents.setParentId(calculateOverAll(overAll));
					DocumentContents documentContentTable = documentContentsService.save(documentContents);
					List<XWPFTable> xwpfTable = doc.getTables();
					XWPFTable table = xwpfTable.get(j);
					int a = 1;
					int b = 1;
					String column1 = null;
					String column2 = null;
					String column3 = null;

					int k = 1;
					long colId1 = 0;
					long colId2 = 0;
					for (XWPFTableRow row : table.getRows()) {
						if (j == 0 || j == 3) {
							boolean value = true;
							long id = 0;

							for (XWPFTableCell cell : row.getTableCells()) {
								if (value) {
									DocumentQuestions documentQuestions = new DocumentQuestions();
									System.out.println(cell.getText());
									if (j != 3) {
										String[] val = cell.getText().split(":");
										documentQuestions.setName(val[0]);
									} else {
										documentQuestions.setName(cell.getText());
									}
									if (j == 3)
										documentQuestions.setAnswerType(AnswerType.CHECKBOX);
									else
										documentQuestions.setAnswerType(AnswerType.TEXT);
									documentQuestions.setDocumentContent(documentContentTable);
									documentQuestions.setQuestionStatus(true);
									String response = getIsoCodesJson(documentQuestions.getName());// primary and
																									// secondary
									if (response != null) {
										JSONObject jsonResponseObject = new JSONObject(response);
										JSONArray array = jsonResponseObject.getJSONArray("hits");
										if (array.length() > 0) {
											documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
											documentQuestions
													.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
										} else {
											documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
											documentQuestions
													.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
										}
									}
									ArrayList<String> arrayListString = new ArrayList<String>();
									arrayListString.add(cell.getText());
									documentQuestions.setComponent(arrayListString.toString());
									documentQuestions.setComponentQuestionMatrix(a + "*" + 1);
									documentQuestions.setComponentAnswerMatrix(b + "*" + 2);
									DocumentQuestions documentQuestionsSaved = documentsQuestionsService
											.save(documentQuestions);
									id = documentQuestionsSaved.getId();
									value = false;
									a++;
									b++;
								} else {
									if (cell.getText().equals("Telephone:")) {
										DocumentQuestions documentQuestions = new DocumentQuestions();
										String[] val = cell.getText().split(":");
										documentQuestions.setName(val[0]);
										documentQuestions.setDocumentContent(documentContentTable);
										ArrayList<String> arrayListString = new ArrayList<String>();
										arrayListString.add(cell.getText());
										documentQuestions.setQuestionStatus(true);
										String response = getIsoCodesJson(documentQuestions.getName());// primary and
																										// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}
										documentQuestions.setAnswerType(AnswerType.TEXT);
										documentQuestions.setComponent(arrayListString.toString());
										documentQuestions.setComponentQuestionMatrix(4 + "*" + 1);
										documentQuestions.setComponentAnswerMatrix(4 + "*" + 2);
										DocumentQuestions documentQuestionsSaved = documentsQuestionsService
												.save(documentQuestions);
										id = documentQuestionsSaved.getId();
										value = false;
										a++;
										b++;
									} else {
										DocumentAnswers documentAnswers = new DocumentAnswers();
										documentAnswers.setAnswer(cell.getText());
										if (checkYesOrNo(cell.getTextRecursively().trim().hashCode())) {
											documentAnswers.setAnswer("yes");
										} else {
											if (j == 3) {
												documentAnswers.setAnswer("empty");
											}
										}
										documentAnswers.setDocumentQuestions(documentsQuestionsService.find(id));
										documentAnswersService.save(documentAnswers);
									}
								}
							}

						} // zeroth table

						if (j == 1) {
							long id = 0;
							String rowValue = "";
							for (XWPFTableCell cell : row.getTableCells()) {

								// retreiving columns
								if (y == 1) {
									if (a == 2)
										column1 = cell.getText();
									if (a == 3)
										column2 = cell.getText();
									a++;
								}
								if (y > 1) {

									if (k == 1) {
										DocumentQuestions documentQuestions = new DocumentQuestions();
										documentQuestions.setQuestionStatus(true);
										documentQuestions.setAnswerType(AnswerType.TEXT);
										documentQuestions.setDocumentContent(documentContentTable);
										StringBuilder builder = new StringBuilder();
										builder.append(cell.getText());
										builder.append(",");
										builder.append(column1);
										documentQuestions.setName(builder.toString());
										String response = getIsoCodesJson(documentQuestions.getName());// primary and
																										// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}
										ArrayList<String> arrayList = new ArrayList<String>();
										arrayList.add(cell.getText());
										arrayList.add(column1);
										rowValue = cell.getText();
										documentQuestions.setComponent(arrayList.toString());
										documentQuestions.setComponentQuestionMatrix(y + "*1" + "," + "1*2");
										documentQuestions.setComponentAnswerMatrix(y + "*2");
										DocumentQuestions documentQuestionsSaved = documentsQuestionsService
												.save(documentQuestions);
										id = documentQuestionsSaved.getId();
									}
									if (k == 2) {
										DocumentAnswers answersSecond = new DocumentAnswers();
										answersSecond.setAnswer(cell.getText());
										answersSecond.setDocumentQuestions(documentsQuestionsService.find(id));
										documentAnswersService.save(answersSecond);
									}
									if (k == 3) {
										DocumentQuestions documentQuestions = new DocumentQuestions();
										documentQuestions.setQuestionStatus(true);
										documentQuestions.setAnswerType(AnswerType.TEXT);
										documentQuestions.setDocumentContent(documentContentTable);
										StringBuilder builder = new StringBuilder();
										builder.append(rowValue);
										builder.append(",");
										builder.append(column2);
										documentQuestions.setName(builder.toString());
										String response = getIsoCodesJson(documentQuestions.getName());// primary and
																										// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}
										ArrayList<String> arrayList = new ArrayList<String>();
										arrayList.add(rowValue);
										arrayList.add(column2);
										documentQuestions.setComponent(arrayList.toString());
										documentQuestions.setComponentQuestionMatrix(y + "*1" + "," + "1*3");
										documentQuestions.setComponentAnswerMatrix(y + "*3");
										DocumentQuestions documentQuestionsSaved = documentsQuestionsService
												.save(documentQuestions);
										DocumentAnswers answersThird = new DocumentAnswers();
										answersThird.setAnswer(cell.getText());
										answersThird.setDocumentQuestions(documentQuestionsSaved);
										documentAnswersService.save(answersThird);
									}
									k++;
								}
							}
						} // first table

						if (j == 2) {
							outer: for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1 || y == 3) {
									DocumentQuestions documentQuestions = new DocumentQuestions();
									documentQuestions.setName(cell.getText());
									documentQuestions.setDocumentContent(documentContentTable);
									documentQuestions.setQuestionStatus(true);
									String response = getIsoCodesJson(documentQuestions.getName());// primary and
																									// secondary
									if (response != null) {
										JSONObject jsonResponseObject = new JSONObject(response);
										JSONArray array = jsonResponseObject.getJSONArray("hits");
										if (array.length() > 0) {
											documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
											documentQuestions
													.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
										} else {
											documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
											documentQuestions
													.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
										}
									}
									documentQuestions.setPossibleAnswer("yes,no");
									documentQuestions.setAnswerType(AnswerType.CHECKBOX);
									DocumentQuestions documentQuestionsSaved = documentsQuestionsService
											.save(documentQuestions);
									if (y == 1)
										colId1 = documentQuestionsSaved.getId();
									if (y == 3)
										colId2 = documentQuestionsSaved.getId();
									break outer;
								}
								if (y == 2 || y == 4) {
									DocumentAnswers documentAnswers = new DocumentAnswers();
									if (k == 1) {
										column1 = cell.getText();
									}
									if (k == 2) {
										if ((cell.getTextRecursively().trim().hashCode() == 9746)) {
											documentAnswers.setAnswer("yes");
											documentAnswers.setSpecifiedAnswer(column1);
											if (y == 2)
												documentAnswers
														.setDocumentQuestions(documentsQuestionsService.find(colId1));
											if (y == 4)
												documentAnswers
														.setDocumentQuestions(documentsQuestionsService.find(colId2));
											documentAnswersService.save(documentAnswers);
										}
									}

									if (k == 3) {
										if ((cell.getTextRecursively().trim().hashCode() == 9746)) {
											documentAnswers.setAnswer("no");
											if (y == 2)
												documentAnswers
														.setDocumentQuestions(documentsQuestionsService.find(colId1));
											if (y == 4)
												documentAnswers
														.setDocumentQuestions(documentsQuestionsService.find(colId2));
											documentAnswersService.save(documentAnswers);
										}
									}
								}
								k++;
							}
						} // Second table

						if (j == 4) {
							String rowHeader = "";
							String rowHeaderRplace = "";
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									if (a == 2)
										column1 = cell.getText();
									if (a == 3)
										column2 = cell.getText();
									if (a == 4)
										column3 = cell.getText();
									a++;
								}
								if ((y > 2 && y <= 8) || (y > 9)) {
									if (k == 1 || k == 4 || k == 6) {
										DocumentQuestions documentQuestions = new DocumentQuestions();
										documentQuestions.setQuestionStatus(true);
										documentQuestions.setAnswerType(AnswerType.TEXT);
										documentQuestions.setDocumentContent(documentContentTable);
										if (k == 1)
											rowHeaderRplace = cell.getText();
										if (rowHeaderRplace.contains(","))
											rowHeader = rowHeaderRplace.replaceAll(",", "&");
										else
											rowHeader = rowHeaderRplace;

										StringBuilder builder = new StringBuilder();
										builder.append(rowHeader + ",");
										if (k == 1)
											builder.append(column1);
										if (k == 4)
											builder.append(column2);
										if (k == 6)
											builder.append(column3);
										builder.append(",");
										builder.append("Processed");
										documentQuestions.setName(builder.toString());
										String response = getIsoCodesJson(documentQuestions.getName());// primary and
																										// secondary
										if (response != null) {
											JSONObject jsonResponseObject = new JSONObject(response);
											JSONArray array = jsonResponseObject.getJSONArray("hits");
											if (array.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(response)).toString());
											}
										}
										ArrayList<String> arrayList = new ArrayList<String>();
										arrayList.add(rowHeader);
										if (k == 1)
											arrayList.add(column1);
										if (k == 4)
											arrayList.add(column2);
										if (k == 6)
											arrayList.add(column3);
										arrayList.add("Processed");
										documentQuestions.setComponent(arrayList.toString());
										// documentQuestions.setComponentQuestionMatrix(1+"*"+k+","+y+"*1");
										if (k == 1) {
											documentQuestions
													.setComponentQuestionMatrix((y - 1) + "*1" + "," + "1*" + (k + 1));
										} else {
											documentQuestions
													.setComponentQuestionMatrix((y - 1) + "*1" + "," + "1*" + (k));
										}
										if (k == 1) {
											documentQuestions.setComponentAnswerMatrix((y - 1) + "*" + (k + 1));
										} else {
											documentQuestions.setComponentAnswerMatrix((y - 1) + "*" + k);
										}
										DocumentQuestions documentQuestionsSavedFirst = documentsQuestionsService
												.save(documentQuestions);
										colId1 = documentQuestionsSavedFirst.getId();

										DocumentQuestions documentQuestionsOuter = new DocumentQuestions();
										documentQuestionsOuter.setQuestionStatus(true);
										documentQuestionsOuter.setAnswerType(AnswerType.TEXT);
										documentQuestionsOuter.setDocumentContent(documentContentTable);
										StringBuilder builder1 = new StringBuilder();
										builder1.append(rowHeader + ",");
										if (k == 1)
											builder1.append(column1);
										if (k == 4)
											builder1.append(column2);
										if (k == 6)
											builder1.append(column3);
										builder1.append(",");
										builder1.append("Stored");
										documentQuestionsOuter.setName(builder1.toString());
										String responseOuter = getIsoCodesJson(documentQuestions.getName());// primary
																											// and
																											// secondary
										if (responseOuter != null) {
											JSONObject jsonResponseObjectOuter = new JSONObject(responseOuter);
											JSONArray arrayOuter = jsonResponseObjectOuter.getJSONArray("hits");
											if (arrayOuter.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(responseOuter));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(responseOuter)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(responseOuter)).toString());
											}
										}
										ArrayList<String> arrayListOuter = new ArrayList<String>();
										arrayListOuter.add(rowHeader);
										if (k == 1)
											arrayListOuter.add(column1);
										if (k == 4)
											arrayListOuter.add(column2);
										if (k == 6)
											arrayListOuter.add(column3);
										arrayListOuter.add("Stored");
										documentQuestionsOuter.setComponent(arrayListOuter.toString());

										// documentQuestions.setComponentQuestionMatrix(1+"*"+(k+1)+","+y+"*1");
										if (k == 1) {
											documentQuestionsOuter
													.setComponentQuestionMatrix((y - 1) + "*1" + "," + "1*" + (k + 2));
										} else {
											documentQuestionsOuter
													.setComponentQuestionMatrix((y - 1) + "*1" + "," + "1*" + (k + 1));
										}
										if (k == 1) {
											documentQuestionsOuter.setComponentAnswerMatrix((y - 1) + "*" + (k + 2));
										} else {
											documentQuestionsOuter.setComponentAnswerMatrix((y - 1) + "*" + (k + 1));
										}
										DocumentQuestions documentQuestionsSavedSecond = documentsQuestionsService
												.save(documentQuestionsOuter);
										colId2 = documentQuestionsSavedSecond.getId();
									}

									if (k == 2 || k == 4 || k == 6) {
										DocumentAnswers answersInner = new DocumentAnswers();
										answersInner.setAnswer(cell.getText());
										answersInner.setDocumentQuestions(documentsQuestionsService.find(colId1));
										documentAnswersService.save(answersInner);
									}
									if (k == 3 || k == 5 || k == 7) {
										DocumentAnswers answersOutside = new DocumentAnswers();
										answersOutside.setAnswer(cell.getText());
										answersOutside.setDocumentQuestions(documentsQuestionsService.find(colId2));
										documentAnswersService.save(answersOutside);
									}
									k++;
								}

							}
						} // 4rd table

						if (j == 5) {
							for (XWPFTableCell cell : row.getTableCells()) {
								if (y == 1) {
									DocumentQuestions documentQuestions = new DocumentQuestions();
									ArrayList<String> arrayList = new ArrayList<String>();
									documentQuestions.setQuestionStatus(true);
									documentQuestions.setAnswerType(AnswerType.TEXT);
									documentQuestions.setDocumentContent(documentContentTable);
									if (a == 1) {
										documentQuestions.setName(cell.getText());
										String responseOuter = getIsoCodesJson(documentQuestions.getName());// primary
																											// and
																											// secondary
										if (responseOuter != null) {
											JSONObject jsonResponseObjectOuter = new JSONObject(responseOuter);
											JSONArray arrayOuter = jsonResponseObjectOuter.getJSONArray("hits");
											if (arrayOuter.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(responseOuter));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(responseOuter)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(responseOuter)).toString());
											}
										}
										documentQuestions.setComponentQuestionMatrix("1*" + a);
										documentQuestions.setComponentAnswerMatrix("2*" + a);
										arrayList.add(cell.getText());
									}
									if (a == 2) {
										documentQuestions.setName(cell.getText());
										String responseOuter = getIsoCodesJson(documentQuestions.getName());// primary
																											// and
																											// secondary
										if (responseOuter != null) {
											documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(responseOuter));
											documentQuestions.setSecondaryIsoCode(
													(getSecondaryIsoCode(responseOuter)).toString());
										} else {
											documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
											documentQuestions.setSecondaryIsoCode(
													(getSecondaryIsoCode(responseOuter)).toString());
										}
										documentQuestions.setComponentQuestionMatrix("1*" + a);
										documentQuestions.setComponentAnswerMatrix("2*" + a);
										arrayList.add(cell.getText());
									}
									documentQuestions.setComponent(arrayList.toString());
									DocumentQuestions documentQuestionsSaved = documentsQuestionsService
											.save(documentQuestions);
									if (a == 1)
										colId1 = documentQuestionsSaved.getId();
									else
										colId2 = documentQuestionsSaved.getId();
									a++;
								}
								if (y > 1) {
									DocumentAnswers answersInner = new DocumentAnswers();
									answersInner.setAnswer(cell.getTextRecursively());
									if (k == 1)
										answersInner.setDocumentQuestions(documentsQuestionsService.find(colId1));
									else
										answersInner.setDocumentQuestions(documentsQuestionsService.find(colId2));
									documentAnswersService.save(answersInner);
									k++;
								}
							}
						} // 5th table

						if (j == 6 || j == 7 || j == 9 || j == 12 || j == 13 || j == 14 || j == 15 || j == 16 || j == 17
								|| j == 18 || j == 19 || j == 21 || j == 24 || j == 25 || j == 26 || j == 27 || j == 28
								|| j == 29 || j == 30 || j == 31 || j == 32 || j == 33 || j == 34 || j == 35) {
							for (XWPFTableCell cell : row.getTableCells()) {
								DocumentQuestions documentQuestions = new DocumentQuestions();
								documentQuestions.setQuestionStatus(false);
								documentQuestions.setAnswerType(AnswerType.TEXT_AREA);
								documentQuestions.setDocumentContent(documentContentTable);
								// documentQuestions.setName(cell.getText());
								DocumentQuestions documentQuestionsSaved = documentsQuestionsService
										.save(documentQuestions);
								DocumentAnswers documentAnswers = new DocumentAnswers();
								documentAnswers.setAnswer(cell.getTextRecursively());
								documentAnswers.setDocumentQuestions(documentQuestionsSaved);
								documentAnswersService.save(documentAnswers);
							}
						} // 6th table & 7th table upto 35

						if (j == 36) {
							long id = 0;
							String rowValue = "";
							for (XWPFTableCell cell : row.getTableCells()) {
								// retreiving columns
								if (y == 1) {
									if (a == 1)
										column1 = cell.getText();
									if (a == 2)
										column2 = cell.getText();
									a++;
								}
								if (y > 1) {
									if (k == 1) {
										DocumentQuestions documentQuestions = new DocumentQuestions();
										documentQuestions.setQuestionStatus(true);
										documentQuestions.setAnswerType(AnswerType.CHECKBOX);
										documentQuestions.setPossibleAnswer("yes,no");
										documentQuestions.setDocumentContent(documentContentTable);
										StringBuilder builder = new StringBuilder();
										builder.append(cell.getText());
										builder.append(",");
										builder.append(column1);
										documentQuestions.setName(builder.toString());
										String responseOuter = getIsoCodesJson(documentQuestions.getName());// primary
																											// and
																											// secondary
										if (responseOuter != null) {
											JSONObject jsonResponseObjectOuter = new JSONObject(responseOuter);
											JSONArray arrayOuter = jsonResponseObjectOuter.getJSONArray("hits");
											if (arrayOuter.length() > 0) {
												documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(responseOuter));
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(responseOuter)).toString());
											} else {
												documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
												documentQuestions.setSecondaryIsoCode(
														(getSecondaryIsoCode(responseOuter)).toString());
											}
										}
										ArrayList<String> arrayList = new ArrayList<String>();
										arrayList.add(cell.getText());
										arrayList.add(column1);
										rowValue = cell.getText();
										documentQuestions.setComponent(arrayList.toString());
										documentQuestions.setComponentQuestionMatrix(y + "*1" + "," + "1*1");
										documentQuestions.setComponentAnswerMatrix(y + "*2" + "," + "1*2");
										DocumentQuestions documentQuestionsSaved = documentsQuestionsService
												.save(documentQuestions);
										id = documentQuestionsSaved.getId();
									}
									if (k == 2) {
										DocumentAnswers answersSecond = new DocumentAnswers();
										StringBuilder builder = new StringBuilder();
										ArrayList<String> arrayList = new ArrayList<String>();
										String[] yesOrNoAnswers;
										yesOrNoAnswers = cell.getText().split("/");
										if (yesOrNoAnswers.length > 1) {
											Character yes = new Character(yesOrNoAnswers[0].trim().charAt(3));
											System.out.println("yes = " + yes.hashCode());
											Character no = new Character(yesOrNoAnswers[1].trim().charAt(2));
											System.out.println("no = " + no.hashCode());
											if (yes.hashCode() == 9746) {
												// System.out.println("answer = yes");
												builder.append("yes");
												builder.append(",");
												builder.append(column2);
												arrayList.add("yes");
												arrayList.add(column2);
												answersSecond.setAnswer("yes");
											} else {
												if (no.hashCode() == 9746) {
													builder.append("no");
													builder.append(",");
													builder.append(column2);
													arrayList.add("no");
													arrayList.add(column2);
													answersSecond.setAnswer("no");
												} else {
													builder.append("empty");
													builder.append(",");
													builder.append(column2);
													arrayList.add("empty");
													arrayList.add(column2);
													answersSecond.setAnswer("empty");
												}
											}

										}
										answersSecond.setAnswer(builder.toString());
										answersSecond.setDocumentQuestions(documentsQuestionsService.find(id));
										documentAnswersService.save(answersSecond);
									}
									k++;
								}
							}
						} // 36 table

						if (j == 22) {
							ArrayList<String> arrayList = new ArrayList<>();
							DocumentQuestions documentQuestions = documentsQuestionsService.find(subParagraphTableId);
							DocumentAnswers documentAnswers = new DocumentAnswers();
							for (XWPFTableCell cell : row.getTableCells()) {

								String[] checkBoxAnswers = cell.getTextRecursively().split(" ");
								System.out.println(checkBoxAnswers[0]);
								if (checkBoxAnswers[1].hashCode() == 9746) {
									arrayList.add(cell.getText());
									documentAnswers.setAnswer(checkBoxAnswers[0]);
								} else if ((checkBoxAnswers[1].equals("Click here to enter text."))) {
									documentAnswers.setAnswer(checkBoxAnswers[0] + "," + checkBoxAnswers[1]);
								} else {
									arrayList.add(cell.getText());
								}
								if (k > 3) {
									// documentQuestions.setOptions(arrayList.toString());
									documentQuestions.setPossibleAnswer(arrayList.toString());
									documentQuestions.setAnswerType(AnswerType.CHECKBOX);
									documentsQuestionsService.saveOrUpdate(documentQuestions);
									documentAnswers
											.setDocumentQuestions(documentsQuestionsService.find(subParagraphTableId));
									documentAnswersService.save(documentAnswers);
								}

								k++;
							}
						} // 22nd table

						y++;
						k = 1;
					}
					j++;// for table iteration
				} // else if close

				overAll++;
			} // while close
		} // try
		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return true;

	}

	@SuppressWarnings("unused")
	@Override
	public boolean auditGroups(byte[] streamFile, Long docId)
			throws IllegalStateException, IOException, InvalidFormatException {
		@SuppressWarnings("resource")
		XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(streamFile));
		Iterator<IBodyElement> iter = doc.getBodyElementsIterator();

		// saving content type into database
		int i = 0;

		int j = 0;
		String[] questionAndAnswers;
		String[] answers;
		long childSubToParagrah = 0;
		int overAll = 0;
		try {
			while (iter.hasNext()) {
				IBodyElement elem = iter.next();
				// for paragraph
				if (elem instanceof XWPFParagraph) {
					List<XWPFParagraph> paragraphs = doc.getParagraphs();
					if (paragraphs.get(i).getText().length() != 0) {
						// style found
						if ((paragraphs.get(i).getStyle() != null) && (paragraphs.get(i).getNumFmt() != null)) {
							DocumentContents documentContents = new DocumentContents();
							documentContents.setContentType("SUB_PARAGRAPH");
							// documentContents.setDocId(docId);
							// documentContentsService.save(documentContents);
							documentContents.setParentId(calculateOverAll(overAll));
							System.out
									.println(paragraphs.get(i).getText() + "PARAGRAPH" + " " + "position :" + overAll);
							documentContentsService.save(documentContents);

						}
						// not found
						else {
							DocumentContents documentContents = new DocumentContents();
							documentContents.setContentType("PARAGRAPH");
							// documentContents.setDocId(docId);
							documentContents.setParentId(calculateOverAll(overAll));
							System.out
									.println(paragraphs.get(i).getText() + "PARAGRAPH" + " " + "position :" + overAll);
							documentContentsService.save(documentContents);
						}
					} // empty if close

					i++;// for next paragraph iteration

				} // if close for paragraph

				// found table
				else if (elem instanceof XWPFTable) {
					int y = 1;
					DocumentContents documentContents = new DocumentContents();
					documentContents.setContentType("TABLE");
					// documentContents.setDocId(docId);
					documentContents.setParentId(calculateOverAll(overAll));
					System.out.println("TABLE" + " " + "position :" + overAll);
					DocumentContents documentContentTable = documentContentsService.save(documentContents);
					List<XWPFTable> xwpfTable = doc.getTables();
					XWPFTable table = xwpfTable.get(j);
					int a = 1;
					int b = 1;
					String column1 = null;
					String column2 = null;
					String column3 = null;
					String column4 = null;

					int k = 1;
					long colId1 = 0;
					long colId2 = 0;
					for (XWPFTableRow row : table.getRows()) {
					}
					j++;// for table iteration
				} // else if close

				overAll++;
			} // while close
		} // try
		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return true;

	}

	/*
	 * @SuppressWarnings("unused")
	 * 
	 * @Override
	 * 
	 * @Transactional("transactionManager") public List<OverAllResponseDto>
	 * getDocumentParseData(Long docId) {
	 * 
	 * List<DocumentContents> documentContentsList =
	 * documentContentsDao.getDocumentContentById(docId);
	 * 
	 * List<OverAllResponseDto> overAllResponseDto = new
	 * ArrayList<OverAllResponseDto>(); try { OverAllResponseDto allResponseDto =
	 * new OverAllResponseDto(); List<DocumentContentDto> documentContentDtoList =
	 * new ArrayList<DocumentContentDto>(); int i = 1; for (DocumentContents
	 * documentContents : documentContentsList) { if (i == 2 || i == 3 || i == 4 ||
	 * i == 5 || i == 6 || i == 7 || i == 8 || i == 9 || i == 10 || i == 11 || (i >=
	 * 12 && i <= 33) || (i >= 34 && i <= 42) || (i >= 43 && i <= 55) || (i >= 56 &&
	 * i <= 71) || (i >= 72 && i <= 84) || (i >= 85 && i <= 96) || (i >= 97 && i <=
	 * 103) || (i >= 104 && i <= 114) || (i >= 115 && i <= 143) || (i >= 143 && i <=
	 * 149) || (i >= 150 && i <= 160)) { if
	 * (documentContents.getContentType().trim().equalsIgnoreCase("PARAGRAPH")) {
	 * DocumentContentDto documentContentDto = new DocumentContentDto();
	 * DocumentQuestions documentQuestions = new DocumentQuestions();
	 * DocumentAnswers documentAnswers = new DocumentAnswers(); ContentDataDto
	 * contentDataDto = new ContentDataDto(); documentQuestions =
	 * documentQuestionsDao.getDocumentQuestionsById(documentContents.getId()); if
	 * (documentQuestions != null) { if (i == 2)
	 * documentContentDto.setContentType("HEADING"); else if (i == 3 || i == 5 || i
	 * == 7 || i == 9 || i == 12 || i == 34 || i == 43 || i == 56 || i == 72 || i ==
	 * 85 || i == 97 || i == 104 || i == 115 || i == 144 || i == 150)
	 * documentContentDto.setContentType("SUB HEADING"); else
	 * documentContentDto.setContentType("PARAGRAPH"); if
	 * (documentQuestions.isQuestionStatus()) { documentAnswers = documentAnswersDao
	 * .getDcoumentAnswerByQuestionId(documentQuestions.getId());
	 * documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
	 * contentDataDto.setQuestion(documentQuestions.getName());
	 * contentDataDto.setQuestionId(documentQuestions.getId());
	 * contentDataDto.setAnswerType(documentQuestions.getAnswerType());
	 * contentDataDto.setPossibleAnswer(documentQuestions.getPossibleAnswer());
	 * contentDataDto.setPrimaryIsoCode(getRandomISOCODE()); // random generating
	 * code List<String> secondaryIsoCodes = new ArrayList<String>();
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * contentDataDto.setSecondaryIsoCodes(secondaryIsoCodes); if
	 * (documentQuestions.getNormalizedQuestion() != null) { DocumentISOCodes codes
	 * = documentISOCodeDao .fetchDocumnetIsoCode(documentQuestions.getName(),
	 * true); if (codes != null) {
	 * contentDataDto.setNormalizedAnswer(codes.getNormalizedQuestion());
	 * contentDataDto.setPrimaryIsoCode(codes.getPrimaryIsoCode()); List<IsoCodes>
	 * isoCodesSecondary = codes.getIsoCodes(); List<String> secondaryKeys = new
	 * ArrayList<String>(); for (IsoCodes isoCodes : isoCodesSecondary) { if
	 * (!(isoCodes.getIsoCode().equals(codes.getPrimaryIsoCode()))) {
	 * secondaryKeys.add(isoCodes.getIsoCode()); } }
	 * contentDataDto.setSecondaryIsoCodes(secondaryKeys);
	 * 
	 * } } contentDataDto.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode());
	 * contentDataDto.setSecondaryIsoCodes(Arrays.asList(documentQuestions.
	 * getSecondaryIsoCode().split(",")));
	 * contentDataDto.setRiskScore(documentQuestions.getRiskScore()); if
	 * (documentAnswers != null) {
	 * contentDataDto.setAnswer(documentAnswers.getAnswer());
	 * contentDataDto.setAnswerId(documentAnswers.getId());
	 * contentDataDto.setSpecifiedAnswer(documentAnswers.getSpecifiedAnswer()); }
	 * 
	 * } if (!documentQuestions.isQuestionStatus()) {
	 * documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
	 * contentDataDto.setText(documentQuestions.getName()); } }
	 * documentContentDto.setContentDataDto(contentDataDto);
	 * documentContentDtoList.add(documentContentDto);
	 * 
	 * } // paragraph close
	 * 
	 * if
	 * (documentContents.getContentType().trim().equalsIgnoreCase("SUB_PARAGRAPH"))
	 * { DocumentContentDto documentContentDto = new DocumentContentDto();
	 * DocumentQuestions documentQuestions = new DocumentQuestions();
	 * DocumentAnswers documentAnswers = new DocumentAnswers(); ContentDataDto
	 * contentDataDto = new ContentDataDto(); documentQuestions =
	 * documentQuestionsDao.getDocumentQuestionsById(documentContents.getId()); if
	 * (documentQuestions != null) {
	 * documentContentDto.setContentType("SUB PARAGRAPH"); if
	 * (documentQuestions.isQuestionStatus()) { documentAnswers = documentAnswersDao
	 * .getDcoumentAnswerByQuestionId(documentQuestions.getId());
	 * documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
	 * contentDataDto.setQuestion(documentQuestions.getName());
	 * contentDataDto.setQuestionId(documentQuestions.getId());
	 * contentDataDto.setAnswerType(documentQuestions.getAnswerType());
	 * contentDataDto.setPrimaryIsoCode(getRandomISOCODE()); // random generating
	 * code List<String> secondaryIsoCodes = new ArrayList<String>();
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * contentDataDto.setSecondaryIsoCodes(secondaryIsoCodes); if
	 * (documentQuestions.getNormalizedQuestion() != null) { DocumentISOCodes codes
	 * = documentISOCodeDao .fetchDocumnetIsoCode(documentQuestions.getName(),
	 * true); if (codes != null) {
	 * contentDataDto.setNormalizedAnswer(codes.getNormalizedQuestion());
	 * contentDataDto.setPrimaryIsoCode(codes.getPrimaryIsoCode()); List<IsoCodes>
	 * isoCodesSecondary = codes.getIsoCodes(); List<String> secondaryKeys = new
	 * ArrayList<String>(); for (IsoCodes isoCodes : isoCodesSecondary) { if
	 * (!(isoCodes.getIsoCode().equals(codes.getPrimaryIsoCode()))) {
	 * secondaryKeys.add(isoCodes.getIsoCode()); } }
	 * contentDataDto.setSecondaryIsoCodes(secondaryKeys);
	 * 
	 * } } contentDataDto.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode());
	 * contentDataDto.setSecondaryIsoCodes(Arrays.asList(documentQuestions.
	 * getSecondaryIsoCode().split(",")));
	 * contentDataDto.setPossibleAnswer(documentQuestions.getPossibleAnswer());
	 * contentDataDto.setRiskScore(documentQuestions.getRiskScore()); if
	 * (documentAnswers != null) {
	 * contentDataDto.setAnswer(documentAnswers.getAnswer());
	 * contentDataDto.setAnswerId(documentAnswers.getId());
	 * contentDataDto.setSpecifiedAnswer(documentAnswers.getSpecifiedAnswer()); } }
	 * if (!documentQuestions.isQuestionStatus()) {
	 * documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
	 * contentDataDto.setText(documentQuestions.getName()); } }
	 * documentContentDto.setContentDataDto(contentDataDto);
	 * documentContentDtoList.add(documentContentDto); } // sub close
	 * 
	 * if (documentContents.getContentType().trim().equalsIgnoreCase("TABLE")) { //
	 * documentContentDtoList.add(documentContentDto);
	 * 
	 * if (i == 4 || i == 8) { List<DocumentQuestions> documentQuestionsList =
	 * documentQuestionsDao .getTableQuestionsById(documentContents.getId());
	 * 
	 * for (DocumentQuestions documentQuestions : documentQuestionsList) {
	 * DocumentContentDto documentContentDto = new DocumentContentDto();
	 * ContentDataDto contentDataDtot = new ContentDataDto();
	 * documentContentDto.setContentType("SUB PARAGRAPH"); if
	 * (documentQuestions.isQuestionStatus()) { DocumentAnswers documentAnswerst =
	 * new DocumentAnswers(); documentAnswerst = documentAnswersDao
	 * .getDcoumentAnswerByQuestionId(documentQuestions.getId());
	 * documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
	 * contentDataDtot.setQuestion(documentQuestions.getName());
	 * contentDataDtot.setQuestionId(documentQuestions.getId());
	 * contentDataDtot.setAnswerType(documentQuestions.getAnswerType());
	 * contentDataDtot.setPossibleAnswer(documentQuestions.getPossibleAnswer());
	 * contentDataDtot.setPrimaryIsoCode(getRandomISOCODE()); // random generating
	 * code List<String> secondaryIsoCodes = new ArrayList<String>();
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * secondaryIsoCodes.add(getRandomISOCODE());
	 * contentDataDtot.setSecondaryIsoCodes(secondaryIsoCodes); if
	 * (documentQuestions.getNormalizedQuestion() != null) { DocumentISOCodes codes
	 * = documentISOCodeDao .fetchDocumnetIsoCode(documentQuestions.getName(),
	 * true); if (codes != null) {
	 * contentDataDtot.setNormalizedAnswer(codes.getNormalizedQuestion());
	 * contentDataDtot.setPrimaryIsoCode(codes.getPrimaryIsoCode()); List<IsoCodes>
	 * isoCodesSecondary = codes.getIsoCodes(); List<String> secondaryKeys = new
	 * ArrayList<String>(); for (IsoCodes isoCodes : isoCodesSecondary) { if
	 * (!(isoCodes.getIsoCode().equals(codes.getPrimaryIsoCode()))) {
	 * secondaryKeys.add(isoCodes.getIsoCode()); } }
	 * contentDataDtot.setSecondaryIsoCodes(secondaryKeys);
	 * 
	 * } } contentDataDtot.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode());
	 * contentDataDtot.setSecondaryIsoCodes(Arrays.asList(documentQuestions.
	 * getSecondaryIsoCode().split(",")));
	 * contentDataDtot.setRiskScore(documentQuestions.getRiskScore()); if
	 * (documentAnswerst != null) {
	 * contentDataDtot.setAnswer(documentAnswerst.getAnswer());
	 * contentDataDtot.setAnswerId(documentAnswerst.getId());
	 * contentDataDtot.setSpecifiedAnswer(documentAnswerst.getSpecifiedAnswer()); }
	 * } if (!documentQuestions.isQuestionStatus()) {
	 * documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
	 * contentDataDtot.setText(documentQuestions.getName()); }
	 * documentContentDto.setContentDataDto(contentDataDtot);
	 * documentContentDtoList.add(documentContentDto); }
	 * 
	 * } else if (i == 19 || i == 22 || i == 33 || i == 42 || i == 52 || i == 58 ||
	 * i == 60 || i == 63 || i == 66 || i == 68 || i == 71 || i == 78 || i == 96 ||
	 * i == 101 || i == 114 || i == 119 || i == 123 || i == 127 || i == 130 || i ==
	 * 133 || i == 137 || i == 141 || i == 148 || i == 152) { DocumentContentDto
	 * documentContentDto1 = new DocumentContentDto(); DocumentQuestions
	 * documentQuestions1 = new DocumentQuestions(); DocumentAnswers
	 * documentAnswers1 = new DocumentAnswers(); ContentDataDto contentDataDto1 =
	 * new ContentDataDto(); documentQuestions1 = documentQuestionsDao
	 * .getDocumentQuestionsById(documentContents.getId()); if (documentQuestions1
	 * != null) { documentContentDto1.setContentType("TEXTAREA"); documentAnswers1 =
	 * documentAnswersDao
	 * .getDcoumentAnswerByQuestionId(documentQuestions1.getId());
	 * documentContentDto1.setQuestion(documentQuestions1.isQuestionStatus());
	 * 
	 * contentDataDto1.setAnswerType(documentQuestions1.getAnswerType());
	 * contentDataDto1.setPossibleAnswer(documentQuestions1.getPossibleAnswer());
	 * contentDataDto1.setRiskScore(documentQuestions1.getRiskScore());
	 * contentDataDto1.setAnswer(documentAnswers1.getAnswer());
	 * contentDataDto1.setAnswerId(documentAnswers1.getId());
	 * contentDataDto1.setSpecifiedAnswer(documentAnswers1.getSpecifiedAnswer());
	 * 
	 * 
	 * } documentContentDto1.setContentDataDto(contentDataDto1);
	 * documentContentDtoList.add(documentContentDto1); }
	 * 
	 * else {
	 * 
	 * if (!((i == 88) || (i == 158))) { DocumentContentDto documentContentDto = new
	 * DocumentContentDto(); documentContentDto.setContentType("TABLE");
	 * List<DocumentQuestions> documentTableQuestions = documentQuestionsDao
	 * .getTableQuestionsById(documentContents.getId()); if
	 * (documentTableQuestions.size() > 0) { HashSet<String> tableRowsCount = new
	 * HashSet<String>(); HashSet<String> tableColumnsCount = new HashSet<String>();
	 * String answerMatrix = null; String questionMatrix = null; for
	 * (DocumentQuestions documentQuestionsTable : documentTableQuestions) {
	 * answerMatrix = documentQuestionsTable.getComponentAnswerMatrix(); if
	 * (answerMatrix != null) { String rowCount[] =
	 * answerMatrix.trim().split("\\*"); tableRowsCount.add(rowCount[0]); }
	 * questionMatrix = documentQuestionsTable.getComponentQuestionMatrix(); if
	 * (questionMatrix != null) { if (questionMatrix.contains(",")) { String
	 * colums[] = questionMatrix.split(","); String colum[] =
	 * colums[1].split("\\*"); tableColumnsCount.add(colum[1]); } else { String
	 * colums1[] = questionMatrix.split("\\*"); tableColumnsCount.add(colums1[1]); }
	 * 
	 * }
	 * 
	 * } if (i != 16) tableColumnsCount.add("extracolumn"); if (i != 11)
	 * tableRowsCount.add("extrarow"); if (i == 14) tableRowsCount.add("extra2row");
	 * ContentDataTableDto contentDataTableDto = new ContentDataTableDto();
	 * ContentDataTableDto contentDataTableDto1 = new ContentDataTableDto();
	 * List<DataTableRowsDto> dataTableRows = new ArrayList<DataTableRowsDto>();
	 * List<Integer> tableHeaderPostion = new ArrayList<Integer>();
	 * 
	 * if (i == 6 || i == 14 || i == 16) { contentDataTableDto.setTableHeader(true);
	 * tableHeaderPostion.add(0);
	 * contentDataTableDto.setTableHeaderPostions(tableHeaderPostion); } else {
	 * contentDataTableDto.setTableHeader(false); }
	 * 
	 * DocumentAnswers documentAnswers = new DocumentAnswers(); for (int a = 0; a <
	 * tableRowsCount.size(); a++) { DataTableRowsDto dataTableRowsDto = new
	 * DataTableRowsDto(); List<DataTableColumsDto> dataTableColums = new
	 * ArrayList<DataTableColumsDto>(); for (int b = 0; b <
	 * tableColumnsCount.size(); b++) { DataTableColumsDto dataTableColumsDto = new
	 * DataTableColumsDto(); ContentDataDto tableContentDataDto = new
	 * ContentDataDto(); dataTableColumsDto.setContentDataDto(tableContentDataDto);
	 * dataTableColums.add(dataTableColumsDto);
	 * 
	 * } dataTableRowsDto.setDataTableColumsDto(dataTableColums);
	 * dataTableRows.add(dataTableRowsDto); }
	 * 
	 * for (DocumentQuestions documentQuestionsTable1 : documentTableQuestions) {
	 * 
	 * documentAnswers = documentAnswersDao
	 * .getDcoumentAnswerByQuestionId(documentQuestionsTable1.getId());
	 * ContentDataDto contentDataDtoTable1 = new ContentDataDto(); ContentDataDto
	 * contentDataDtoTable2 = new ContentDataDto(); ContentDataDto
	 * contentDataDtoTableAns = new ContentDataDto(); String answerMatrix1 =
	 * documentQuestionsTable1.getComponentAnswerMatrix(); String[] answerMatrix1Pos
	 * = answerMatrix1.split("\\*");
	 * 
	 * contentDataDtoTableAns.setAnswerId(documentAnswers.getId());
	 * contentDataDtoTableAns.setAnswer(documentAnswers.getAnswer());
	 * contentDataDtoTableAns.setAnswerType(documentQuestionsTable1.getAnswerType())
	 * ;
	 * contentDataDtoTableAns.setSpecifiedAnswer(documentAnswers.getSpecifiedAnswer(
	 * ));
	 * 
	 * int ansRowPosition = Integer.parseInt(answerMatrix1Pos[0]) - 1; int
	 * ansColumPosition = Integer.parseInt(answerMatrix1Pos[1]) - 1; String
	 * quesMatrix1 = documentQuestionsTable1.getComponentQuestionMatrix();
	 * 
	 * if (quesMatrix1.contains(",")) { String[] quesMatrix1Split =
	 * quesMatrix1.split(","); String[] firstQues =
	 * quesMatrix1Split[0].split("\\*"); String[] secondQues =
	 * quesMatrix1Split[1].split("\\*"); int firstQuesRow =
	 * Integer.parseInt(firstQues[0]) - 1; int firstQuesColum =
	 * Integer.parseInt(firstQues[1]) - 1; int secondQuesRow =
	 * Integer.parseInt(secondQues[0]) - 1; int secondQuesColum =
	 * Integer.parseInt(secondQues[1]) - 1; if (documentQuestionsTable1.getName() !=
	 * null && documentQuestionsTable1.getName().contains(",")) { String ques[] =
	 * documentQuestionsTable1.getName().split(","); if (ques[0].contains("&")) {
	 * String nameReplace1 = ques[0].replaceAll("&", ",");
	 * contentDataDtoTable1.setQuestion(nameReplace1); } else
	 * contentDataDtoTable1.setQuestion(ques[0]); if (ques[1].contains("&")) {
	 * String nameReplace2 = ques[1].replaceAll("&", ",");
	 * contentDataDtoTable2.setQuestion(nameReplace2); } else
	 * contentDataDtoTable2.setQuestion(ques[1]); }
	 * contentDataDtoTable1.setAnswerType(documentQuestionsTable1.getAnswerType());
	 * 
	 * contentDataDtoTable1
	 * .setPossibleAnswer(documentQuestionsTable1.getPossibleAnswer());
	 * contentDataDtoTable1.setRiskScore(documentQuestionsTable1.getRiskScore());
	 * contentDataDtoTable1.setPrimaryIsoCode(getRandomISOCODE());
	 * contentDataDtoTable1.setSecondaryIsoCodes(getRandomIsoCodeList());
	 * contentDataDtoTable2.setAnswerType(documentQuestionsTable1.getAnswerType());
	 * 
	 * contentDataDtoTable2
	 * .setPossibleAnswer(documentQuestionsTable1.getPossibleAnswer());
	 * contentDataDtoTable2.setRiskScore(documentQuestionsTable1.getRiskScore());
	 * contentDataDtoTable2.setPrimaryIsoCode(getRandomISOCODE());
	 * contentDataDtoTable2.setSecondaryIsoCodes(getRandomIsoCodeList());
	 * dataTableRows.get(firstQuesRow).getDataTableColumsDto().get(firstQuesColum)
	 * .setContentDataDto(contentDataDtoTable1);
	 * dataTableRows.get(secondQuesRow).getDataTableColumsDto()
	 * .get(secondQuesColum).setContentDataDto(contentDataDtoTable2);
	 * 
	 * } else {
	 * 
	 * String[] quesMatrix1Pos = quesMatrix1.split("\\*"); int quesRowPosition =
	 * Integer.parseInt(quesMatrix1Pos[0]) - 1; int quesColumPosition =
	 * Integer.parseInt(quesMatrix1Pos[1]) - 1;
	 * contentDataDtoTable1.setQuestion(documentQuestionsTable1.getName());
	 * contentDataDtoTable1.setQuestionId(documentQuestionsTable1.getId());
	 * contentDataDtoTable1.setAnswerType(documentQuestionsTable1.getAnswerType());
	 * contentDataDtoTable1.setPrimaryIsoCode(getRandomISOCODE());
	 * contentDataDtoTable1.setSecondaryIsoCodes(getRandomIsoCodeList());
	 * contentDataDtoTable1
	 * .setPossibleAnswer(documentQuestionsTable1.getPossibleAnswer());
	 * contentDataDtoTable1.setRiskScore(documentQuestionsTable1.getRiskScore());
	 * dataTableRows.get(quesRowPosition).getDataTableColumsDto()
	 * .get(quesColumPosition).setContentDataDto(contentDataDtoTable1);
	 * 
	 * } // System.out.println(rowPosition+" // ppp"+columPosition);
	 * dataTableRows.get(ansRowPosition).getDataTableColumsDto().get(
	 * ansColumPosition) .setContentDataDto(contentDataDtoTableAns);
	 * 
	 * } List<Integer> tableHeaderPositions = new ArrayList<Integer>();
	 * List<DataTableRowsDto> dataTableRowsWithheaders = new
	 * ArrayList<DataTableRowsDto>(); if (i == 14) { DataTableRowsDto firstRow = new
	 * DataTableRowsDto(); List<String> firstHeader = new ArrayList<String>();
	 * firstHeader.add("Data Type"); firstHeader.add("UAE / GCC / ROW");
	 * firstHeader.add("EU"); firstHeader.add("USA / Canada"); DataTableRowsDto
	 * secondRow = new DataTableRowsDto(); List<String> secondHeader = new
	 * ArrayList<String>(); secondHeader.add("External");
	 * secondHeader.add("Processed"); secondHeader.add("Stored");
	 * secondHeader.add("Processed"); secondHeader.add("Stored");
	 * secondHeader.add("Processed"); secondHeader.add("Stored");
	 * List<DataTableColumsDto> dataTableFirstHeaderColums = new
	 * ArrayList<DataTableColumsDto>(); int i1 = 0; for (String colum : firstHeader)
	 * { DataTableColumsDto firstHeaderColum = new DataTableColumsDto();
	 * ContentDataDto contentDataDto = new ContentDataDto();
	 * contentDataDto.setQuestion(colum);
	 * contentDataDto.setAnswerType(AnswerType.TEXT);
	 * contentDataDto.setPrimaryIsoCode(getRandomISOCODE());
	 * contentDataDto.setSecondaryIsoCodes(getRandomIsoCodeList()); if (i1 == 0)
	 * contentDataDto.setColumnSpan(1); else contentDataDto.setColumnSpan(2);
	 * firstHeaderColum.setContentDataDto(contentDataDto);
	 * dataTableFirstHeaderColums.add(firstHeaderColum); i1++;
	 * 
	 * } firstRow.setDataTableColumsDto(dataTableFirstHeaderColums);
	 * List<DataTableColumsDto> dataTableSecondHeaderColums = new
	 * ArrayList<DataTableColumsDto>(); for (String colum2 : secondHeader) {
	 * DataTableColumsDto secondHeaderColum = new DataTableColumsDto();
	 * ContentDataDto contentDataDto = new ContentDataDto();
	 * contentDataDto.setQuestion(colum2);
	 * contentDataDto.setAnswerType(AnswerType.TEXT);
	 * contentDataDto.setPrimaryIsoCode(getRandomISOCODE());
	 * contentDataDto.setSecondaryIsoCodes(getRandomIsoCodeList());
	 * secondHeaderColum.setContentDataDto(contentDataDto);
	 * dataTableSecondHeaderColums.add(secondHeaderColum);
	 * 
	 * } secondRow.setDataTableColumsDto(dataTableSecondHeaderColums);
	 * 
	 * dataTableRowsWithheaders.add(firstRow); tableHeaderPositions.add(0);
	 * dataTableRowsWithheaders.add(secondRow); tableHeaderPositions.add(1); for
	 * (int a = 0; a < dataTableRows.size(); a++) { if (a != 0)
	 * dataTableRowsWithheaders.add(dataTableRows.get(a));
	 * 
	 * }
	 * 
	 * DataTableRowsDto eightRow = new DataTableRowsDto(); List<String> eightHeader
	 * = new ArrayList<String>(); eightHeader.add("Internal");
	 * eightHeader.add("Processed"); eightHeader.add("Stored");
	 * eightHeader.add("Processed"); eightHeader.add("Stored");
	 * eightHeader.add("Processed"); eightHeader.add("Stored");
	 * List<DataTableColumsDto> dataTableEightHeaderColums = new
	 * ArrayList<DataTableColumsDto>(); for (String colum3 : eightHeader) {
	 * DataTableColumsDto eightHeaderColum = new DataTableColumsDto();
	 * ContentDataDto contentDataDto = new ContentDataDto();
	 * contentDataDto.setQuestion(colum3);
	 * contentDataDto.setAnswerType(AnswerType.TEXT);
	 * contentDataDto.setPrimaryIsoCode(getRandomISOCODE());
	 * contentDataDto.setSecondaryIsoCodes(getRandomIsoCodeList());
	 * eightHeaderColum.setContentDataDto(contentDataDto);
	 * dataTableEightHeaderColums.add(eightHeaderColum); }
	 * eightRow.setDataTableColumsDto(dataTableEightHeaderColums);
	 * 
	 * dataTableRowsWithheaders.set(8, eightRow); tableHeaderPositions.add(8);
	 * contentDataTableDto.setTableHeaderPostions(tableHeaderPositions);
	 * contentDataTableDto.setDataTableRowsDto(dataTableRowsWithheaders);
	 * 
	 * } else { contentDataTableDto.setDataTableRowsDto(dataTableRows); }
	 * documentContentDto.setContentDataTableDto(contentDataTableDto);
	 * documentContentDtoList.add(documentContentDto);
	 * 
	 * }
	 * 
	 * } } } // table close
	 * 
	 * if (i == 2 || i == 4 || i == 6 || i == 8 || i == 11 || i == 33 || i == 42 ||
	 * i == 55 || i == 71 || i == 84 || i == 96 || i == 103 || i == 114 || i == 143
	 * || i == 149 || i == 160) {
	 * allResponseDto.setDocumentContentDto(documentContentDtoList);
	 * overAllResponseDto.add(allResponseDto); allResponseDto = new
	 * OverAllResponseDto(); documentContentDtoList = new
	 * ArrayList<DocumentContentDto>(); } }
	 * 
	 * i++; }
	 * 
	 * } catch (Exception e) { ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
	 * ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
	 * }
	 * 
	 * return overAllResponseDto;
	 * 
	 * }
	 */
	public String getRandomISOCODE() {
		// Random random = new Random();
		int index = (int) (((Math.random() * ((40 - 20) + 1)) + 20));

		// int index = random.nextInt(((40 - 20) + 1) + 20);

		int index1 = this.random.nextInt(30);
		int index2 = this.random.nextInt(9);
		StringBuilder builder = new StringBuilder();
		builder.append("A");
		builder.append(index);
		builder.append(".");
		builder.append(index1);
		builder.append(".");
		builder.append(index2);
		return builder.toString();

	}

	public List<String> getRandomIsoCodeList() {
		List<String> randomIsoCode = new ArrayList<String>();
		randomIsoCode.add(getRandomISOCODE());
		randomIsoCode.add(getRandomISOCODE());
		randomIsoCode.add(getRandomISOCODE());
		return randomIsoCode;
	}

	@SuppressWarnings("unused")
	private int getRandomNo() {
		int Low = 1;
		int High = 100;
		int result = this.random.nextInt(High - Low) + Low;
		return (int) (result);
	}

	private int getRandomNumberLess40() {
		int Low = 0;
		int High = 40;
		int result = this.random.nextInt(High - Low) + Low;
		return (int) (result);
	}

	private int getRandomNumberLess70() {
		int Low = 41;
		int High = 70;
		int result = this.random.nextInt(High - Low) + Low;
		return (int) (result);
	}

	private int getRandomNumberLess100() {
		int Low = 71;
		int High = 100;
		int result = this.random.nextInt(High - Low) + Low;
		return (int) (result);
	}

	@Override
	@Transactional("transactionManager")
	public boolean setRiskScoreToQuestions(Long docId) {

		Map<String, Integer> highRiskQuestions = new HashMap<String, Integer>();
		Map<String, Integer> mediumRiskQuestions = new HashMap<String, Integer>();
		Map<String, Integer> lowRiskQuestions = new HashMap<String, Integer>();
		/*
		 * List<String> highRiskQuestions=new ArrayList<String>(); List<String>
		 * mediumRiskQuestions=new ArrayList<String>(); List<String>
		 * lowRiskQuestions=new ArrayList<String>();
		 */

		mediumRiskQuestions.put("Is personal data held in more than one database?", 1);
		mediumRiskQuestions.put(
				"Do you have an incident response plan specifically governing your response to the unauthorised release of personal information?",
				2);
		mediumRiskQuestions.put("Do you ever share personal data with third party organisations?", 3);
		highRiskQuestions.put(
				"Are all third party organisations with which you share personal data, required to indemnify you under contract for their unauthorised disclosure of such personal data?",
				4);
		mediumRiskQuestions.put(
				"Do you verify that outsource providers and organisations with which you share personal information are able to meet a satisfactory standard of security?",
				5);
		mediumRiskQuestions.put("Have these plans been tested in the last 12 months?", 6);
		mediumRiskQuestions.put("Is all critical/sensitive data backed up on a daily basis?", 7);
		mediumRiskQuestions.put(
				"Is all sensitive information stored on back-up tapes/cassettes/disks, etc. encrypted as a standard practice?",
				8);
		mediumRiskQuestions.put(
				"If you maintain your own back-up tapes/cassettes/disks, etc., are these stored in a physically secured location?",
				9);
		mediumRiskQuestions.put(
				"Do you have formalised data destruction procedures in place for data and documents no longer needed by your organisation?",
				10);
		mediumRiskQuestions.put(
				"Are you compliant with applicable statutory or regulatory standards concerning personal data stored or processed by you?",
				11);
		mediumRiskQuestions
				.put("How many credit or debit transactions do you process annually? Click here to enter text.", 12);
		mediumRiskQuestions.put(
				"Do you mask all but the last four digits of a card number when displaying or printing card holder data?",
				13);
		mediumRiskQuestions.put("Do you encrypt all account information on your databases?", 14);
		mediumRiskQuestions.put("Do you solicit/promote your business via unsolicited email blasts?", 15);
		mediumRiskQuestions.put("Do you host any user-generated content or social media networks?", 16);
		mediumRiskQuestions.put(
				"Do you have a formalized take-down procedure for comments or content placed on your social media sites by third parties?",
				17);
		mediumRiskQuestions.put(
				"Unavailability of an IT system or data, that has had a material negative revenue impact or has resulted in increased cost of working?",
				18);
		mediumRiskQuestions.put("Receipt of an extortion demand based on a threat to your IT system or data?", 19);
		mediumRiskQuestions.put(
				"Receipt of a claim for financial compensation arising from the content of your website(s), intranet(s), other web accessible corporate content, or any other electronic communication?",
				20);
		mediumRiskQuestions.put(
				"Been subject to a regulatory proceeding in connection with the breach of any applicable law concerning personal information?",
				21);
		mediumRiskQuestions.put("Have there been any mergers, acquisitions, disposals in the last 12 months?", 22);
		mediumRiskQuestions.put("Are there any mergers, acquisitions, divestitures planned in the next 12 months?", 23);
		highRiskQuestions.put("Is personal or other confidential data held on or accessible by web servers?", 1);
		highRiskQuestions.put("Has the proposer designated a Chief Privacy Officer?", 2);
		highRiskQuestions.put("Do you have a corporate wide privacy policy?", 3);
		highRiskQuestions.put(
				"Do you ensure that card validation codes are not stored in any of your databases, log files or anywhere else within your network?",
				4);
		highRiskQuestions
				.put("Have you implemented an identity theft prevention programme, i.e. FTC “Red Flags” Programme?", 5);
		highRiskQuestions
				.put("Has any third party audit of your privacy practices been undertaken in the last two years?", 6);
		highRiskQuestions.put("Has the proposer designated a Chief Security Officer?", 7);
		highRiskQuestions.put("Do you track and monitor access to sensitive information on your network?", 8);
		highRiskQuestions.put(
				"Do you use anti-virus software that is updated/patched in accordance with the vendor’s recommendations?",
				9);
		highRiskQuestions.put("Have you installed and do you maintain a firewall configuration to protect data?", 10);
		highRiskQuestions.put("Have you installed and do you maintain an intrusion detection system?", 11);
		highRiskQuestions
				.put("Do you have a defined process implemented to regularly patch your systems and applications?", 12);
		highRiskQuestions.put("Does the proposer distribute information security policies to its employees?", 13);
		highRiskQuestions.put(
				"Do you provide awareness training for employees in data privacy ad security including legal liability and social engineering issues?",
				14);
		highRiskQuestions.put("Is write access to USB drives disabled for employees?", 15);
		highRiskQuestions.put(
				"Do you have a procedure for the revocation of user accounts and the recovery of inventoried information assets following an employee’s departure from your organisation?",
				16);
		highRiskQuestions.put(
				"Does your website feature opt in/opt out procedures when collecting individual users’ information?",
				17);
		highRiskQuestions.put("Do you or have you ever used flash cookies on your website to track visitors?", 18);
		highRiskQuestions
				.put("Do you have written guidelines for your use of social media and its use by your employees?", 19);
		highRiskQuestions.put(
				"Has legal counsel verified that your domain names(s) and meta tags do not infringe on any third party’s copyright or trademark?",
				20);
		highRiskQuestions.put("Unauthorised disclosure of non-public personally identifiable information?", 21);
		highRiskQuestions.put("Damage, destruction, corruption deletion or loss of data or computer programs?", 22);
		highRiskQuestions.put(
				"Receipt of a claim for financial compensation from a third party arising from a breach of your network security?",
				23);

		List<DocumentQuestions> documentQuestionsList = documentQuestionsDao.getAllQuestions(docId, true);
		DocumentQuestions documentQuestionsUpdate = new DocumentQuestions();
		try {
			for (DocumentQuestions documentQuestions : documentQuestionsList) {
				DocumentAnswers documentAnswer = documentAnswersDao
						.getDcoumentAnswerByQuestionId(documentQuestions.getId());
				if (documentQuestions.getName().trim().equalsIgnoreCase(
						"Please specify all indemnity limit options for which you would like to receive quotations")) {
					documentQuestionsUpdate = documentQuestions;
				}
				if (documentQuestions.getName().trim().equalsIgnoreCase("USD 5,000,000")
						|| documentQuestions.getName().trim().equalsIgnoreCase("USD 10,000,000")) {
					documentQuestionsUpdate.setRiskType("MEDIUM");
					if (documentAnswer.getAnswer().equalsIgnoreCase("yes"))
						documentQuestionsUpdate.setRiskScore(getRandomNumberLess70());
					if (documentAnswer.getAnswer().equalsIgnoreCase("no"))
						documentQuestionsUpdate.setRiskScore(getRandomNumberLess40());
					documentQuestionsDao.saveOrUpdate(documentQuestionsUpdate);
				}
				if (documentQuestions.getName().trim().equalsIgnoreCase("USD 20,000,000")
						|| documentQuestions.getName().trim().equalsIgnoreCase("USD 50,000,000")) {
					documentQuestionsUpdate.setRiskType("HIGH");
					if (documentAnswer.getAnswer().equalsIgnoreCase("yes"))
						documentQuestionsUpdate.setRiskScore(getRandomNumberLess100());
					if (documentAnswer.getAnswer().equalsIgnoreCase("no"))
						documentQuestionsUpdate.setRiskScore(getRandomNumberLess70());
					documentQuestionsDao.saveOrUpdate(documentQuestionsUpdate);
				}

				if (documentAnswer.getAnswer() != null) {

					if (highRiskQuestions.containsKey(documentQuestions.getName().trim())) {
						documentQuestions.setRiskType("HIGH");
						if (documentAnswer.getAnswer().equalsIgnoreCase("yes")
								|| documentAnswer.getAnswer().contains("yes"))
							documentQuestions.setRiskScore(getRandomNumberLess100());
						if (documentAnswer.getAnswer().equalsIgnoreCase("no"))
							documentQuestions.setRiskScore(getRandomNumberLess70());
						if (documentAnswer.getAnswer().equalsIgnoreCase("empty"))
							documentQuestions.setRiskScore(getRandomNumberLess40());
						documentQuestionsDao.saveOrUpdate(documentQuestions);
					}

					if (mediumRiskQuestions.containsKey(documentQuestions.getName().trim())) {
						documentQuestions.setRiskType("MEDIUM");
						if (documentAnswer.getAnswer().equalsIgnoreCase("yes")
								|| documentAnswer.getAnswer().contains("yes"))
							documentQuestions.setRiskScore(getRandomNumberLess70());
						if (documentAnswer.getAnswer().equalsIgnoreCase("no"))
							documentQuestions.setRiskScore(getRandomNumberLess40());

						documentQuestionsDao.saveOrUpdate(documentQuestions);
					}

					if (lowRiskQuestions.containsKey(documentQuestions.getName().trim())) {
						documentQuestions.setRiskType("LOW");
						if (documentAnswer.getAnswer().equalsIgnoreCase("yes")
								|| documentAnswer.getAnswer().contains("yes"))
							documentQuestions.setRiskScore(getRandomNumberLess40());
						documentQuestionsDao.saveOrUpdate(documentQuestions);
					}

				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return true;

	}

	private int calculateOverAll(int i) {
		int j = 0;
		if (i == 7 || i == 8)
			j = 1;
		else if (i == 10 || i == 11)
			j = 2;
		else if (i == 13 || i == 14)
			j = 3;
		else if (i == 18 || i == 19 || i == 21)
			j = 4;
		else if (i >= 23 && i <= 56)
			j = 5;
		else if (i >= 58 && i <= 66)
			j = 6;
		else if (i >= 68 && i <= 83)
			j = 7;
		else if (i >= 84 && i <= 102)
			j = 8;
		else if (i >= 103 && i <= 117)
			j = 9;
		else if (i >= 118 && i <= 131)
			j = 10;
		else if (i >= 133 && i <= 139)
			j = 11;
		else if (i >= 140 && i <= 150)
			j = 12;
		else if (i >= 152 && i <= 184)
			j = 13;
		else if (i >= 185 && i <= 194)
			j = 14;
		else if (i >= 195 && i <= 201)
			j = 15;
		else if (i >= 203)
			j = 16;
		return j;

	}

	private String getIsoCodesJson(String question) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("text", question);
		jsonObject.put("size", 5);
		String response = null;
		String url = ISOCODE_QUESTION_URL + "iso/map";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonObject.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 404)
			return null;
		else
			return response;

	}

	private String getPrimaryIsoCode(String josnResponse) {
		String primaryIsocodeResponse = "";
		JSONObject jsonResponse = new JSONObject(josnResponse);
		JSONArray array = jsonResponse.getJSONArray("hits");
		if (array.length() > 0) {
			JSONObject normalObject = array.getJSONObject(0);
			JSONObject primaryControlObject = normalObject.getJSONObject("controls").getJSONObject("level3");
			primaryIsocodeResponse = primaryControlObject.getString("code").toString();
		}
		return primaryIsocodeResponse;
	}

	/*
	 * private String getTopic(String josnResponse) { String topic = null;
	 * JSONObject jsonResponse = new JSONObject(josnResponse); JSONArray array =
	 * jsonResponse.getJSONArray("hits"); if (array.length() > 0) { JSONObject
	 * normalObject = array.getJSONObject(0);
	 * if(normalObject.getJSONObject("controls").has("topic")) topic =
	 * normalObject.getJSONObject("controls").getString("topic");
	 * 
	 * } return topic; }
	 */

	private StringBuilder getSecondaryIsoCode(String jsonResponse) {
		ArrayList<String> secondaryIsocodesList = new ArrayList<String>();
		StringBuilder builder = new StringBuilder();
		if (jsonResponse != null) {
			JSONObject jsonResponseObject = new JSONObject(jsonResponse);
			JSONArray array = jsonResponseObject.getJSONArray("hits");
			if (array.length() > 0) {
				for (int i = 1; i < array.length(); i++) {
					JSONObject resultObject = array.getJSONObject(i);
					JSONObject secondaryControlObject = resultObject.getJSONObject("controls").getJSONObject("level3");
					secondaryIsocodesList.add(secondaryControlObject.getString("code").toString());
					builder.append(secondaryControlObject.getString("code").toString());
					if (i != (array.length() - 1))
						builder.append(",");
				}
			}
		}
		return builder;
	}

	@Override
	public boolean compareFileWithJsonTemplate(MultipartFile uploadedFile, String templateName)
			throws IllegalStateException, IOException, InvalidFormatException, URISyntaxException, ParseException {
		boolean compareStatus = false;
		/*
		 * String fileExtension =
		 * FilenameUtils.getExtension(uploadedFile.getOriginalFilename()); if
		 * (uploadedFile != null && templateName != null) { byte[] uploadedFileData =
		 * uploadedFile.getBytes();
		 * 
		 * if (!fileExtension.equals("pdf")) {
		 * 
		 * try {
		 * 
		 * ClassLoader classLoader = Thread.currentThread().getContextClassLoader(); URL
		 * resource = classLoader.getResource("cyberinsuranceform.json"); File file =
		 * new File(resource.toURI()); JSONParser parser = new JSONParser(); Object
		 * object = parser.parse(new FileReader(file)); JSONObject jsonObject = new
		 * JSONObject(object.toString()); JSONArray jsonArray =
		 * jsonObject.getJSONArray("data"); List<Boolean> compareStatusList=new
		 * ArrayList<Boolean>();
		 * 
		 * XWPFDocument doc = new XWPFDocument(new
		 * ByteArrayInputStream(uploadedFileData)); Iterator<IBodyElement> iter =
		 * doc.getBodyElementsIterator(); while(iter.hasNext()){ IBodyElement element =
		 * iter.next(); } List<XWPFParagraph> paragraphs = doc.getParagraphs();
		 * 
		 * List<XWPFTable> xwpfTable = doc.getTables();
		 * System.out.println("No of cells in first row"+xwpfTable.get(35).getRow(0).
		 * getCell(0).getTextRecursively());
		 * System.out.println("No of cells in first row"+xwpfTable.get(0).getRow(0).
		 * getCell(0).getTextRecursively()); int t = 0; try {
		 * 
		 * ClassLoader classLoader = Thread.currentThread().getContextClassLoader(); URL
		 * resource = classLoader.getResource("cyberinsuranceform.json"); File file =
		 * new File(resource.toURI()); JSONParser parser = new JSONParser(); Object
		 * object = parser.parse(new FileReader(file)); JSONObject jsonObject = new
		 * JSONObject(object.toString()); JSONArray jsonArray =
		 * jsonObject.getJSONArray("data"); List<Boolean> compareStatusList = new
		 * ArrayList<Boolean>();
		 * 
		 * XWPFDocument doc = new XWPFDocument(new
		 * ByteArrayInputStream(uploadedFileData)); Iterator<IBodyElement> iter =
		 * doc.getBodyElementsIterator(); while (iter.hasNext()) { IBodyElement element
		 * = iter.next(); } List<XWPFParagraph> paragraphs = doc.getParagraphs();
		 * 
		 * List<XWPFTable> xwpfTable = doc.getTables();
		 * System.out.println("No of cells in first row" +
		 * xwpfTable.get(0).getRow(0).getTableCells().size()); int t = 0; try { for (int
		 * j = 0; j < jsonArray.length(); j++) {
		 * 
		 * JSONObject jsonObject1 = jsonArray.getJSONObject(j);
		 * 
		 * if (jsonObject1.get("contentType").toString().equals("table")) {
		 * 
		 * int columsCount = 0; int columsMatchCount = 0; int tablecount = 0;
		 * 
		 * XWPFTable table = xwpfTable.get(t); JSONObject tabledata =
		 * jsonObject1.getJSONObject("content"); int k = 0; // System.out.println(j+"
		 * table first row cells : // "+table.getRow(0).getCell(0).getText()); if
		 * (table.getRow(0).getTableCells().size() > 1) { for (XWPFTableRow row :
		 * table.getRows()) {
		 * 
		 * JSONArray tableRows = tabledata.getJSONArray("rows"); if (k <
		 * tableRows.length()) { JSONObject tableRow = tableRows.getJSONObject(k);
		 * JSONArray rowColums = tableRow.getJSONArray("colums"); columsCount =
		 * columsCount + rowColums.length(); JSONObject cellObject = new JSONObject();
		 * int m = 0;
		 * 
		 * for (XWPFTableCell cell : row.getTableCells()) { if (m < rowColums.length())
		 * { cellObject = rowColums.getJSONObject(m); System.out.println("In doc -- " +
		 * cell.getText().toString() + cell.getText().toString().length() +
		 * " in json -- " + cellObject.get("columName").toString() +
		 * cellObject.get("columName").toString().length()); if
		 * (cell.getText().toString().trim().equalsIgnoreCase(
		 * cellObject.get("columName").toString().trim()) ||
		 * cell.getText().toString().trim().contains(
		 * cellObject.get("columName").toString().trim())) { columsMatchCount =
		 * columsMatchCount + 1; System.out.println( cellObject.get("columName") + "---"
		 * + cell.getText()); } } m++; } // cell ends
		 * 
		 * } k++;
		 * 
		 * } // rows end }
		 * 
		 * t++; if (columsCount > 1) { if (columsCount == columsMatchCount)
		 * compareStatusList.add(true); else compareStatusList.add(false); } }
		 * 
		 * if (jsonObject1.get("contentType").toString().equals("subparagraph")) {
		 * 
		 * int paragraphCount = 0;
		 * 
		 * for (XWPFParagraph paragraph : paragraphs) {
		 * 
		 * if (paragraph.getText() != null && paragraph.getText().length() != 0) {
		 * 
		 * JSONObject paragraphObject = jsonObject1.getJSONObject("content");
		 * System.out.println("name :--" + paragraphObject.get("name") +
		 * paragraphObject.get("name").toString().length() + "--text in doc" +
		 * paragraph.getText() + "--" + paragraph.getText().length()); if
		 * (paragraph.getText().contains(paragraphObject.get("name").toString())) {
		 * paragraphCount = paragraphCount + 1;
		 * System.out.println(paragraph.getText().trim() + "--" +
		 * paragraphObject.get("name").toString().trim()); } }
		 * 
		 * } if (paragraphCount > 0) compareStatusList.add(true); else
		 * compareStatusList.add(false); }
		 * 
		 * }
		 * 
		 * if (compareStatusList.size() != 0) { int falseCount = 0; for (boolean status
		 * : compareStatusList) { if (!status) falseCount = falseCount + 1; } if
		 * (falseCount == 0) compareStatus = true; } } catch (Exception e) {
		 * ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass()); }
		 * 
		 * } catch (Exception e) {
		 * 
		 * ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass()); } } } else { ClassLoader classLoader =
		 * Thread.currentThread().getContextClassLoader(); URL resource =
		 * classLoader.getResource("questionarie4_pdf.json"); File file = new
		 * File(resource.toURI()); JSONParser parser = new JSONParser(); Object object =
		 * parser.parse(new FileReader(file)); JSONObject jsonObject = new
		 * JSONObject(object.toString()); JSONArray jsonArray =
		 * jsonObject.getJSONArray("data"); PDDocument document =
		 * PDDocument.load(uploadedFile.getInputStream()); PDFTextStripper
		 * pdfTextStripper = new PDFLayoutTextStripper(); String string =
		 * pdfTextStripper.getText(document); String strArray[] = string.split("\n");
		 * 
		 * for (int i = 0; i < jsonArray.length(); i++) { JSONObject jsonObjectOverall =
		 * jsonArray.getJSONObject(i); if
		 * (jsonObjectOverall.get("contentType").equals("header")) { JSONObject
		 * josnObjectInner = jsonObjectOverall.getJSONObject("content"); for (int j = 0;
		 * j < strArray.length; j++) { if
		 * (strArray[j].contains(josnObjectInner.getString("name").toString())) ; {
		 * return compareStatus = true; } }
		 * 
		 * } else if (jsonObjectOverall.get("contentType").equals("subparagraph")) {
		 * JSONObject josnObjectInner = jsonObjectOverall.getJSONObject("content"); for
		 * (int j = 0; j < strArray.length; j++) { if
		 * (strArray[j].contains(josnObjectInner.getString("name").toString())) ; {
		 * return compareStatus = true; } } } else { } }
		 * 
		 * }
		 */

		return compareStatus;
	}

	@Override
	@Transactional("transactionManager")
	public DocumentTemplates saveDocumentTemplates(MultipartFile uploadedFile, String fileExtensionOfTemplate)
			throws IllegalStateException, IOException, InvalidFormatException {
		DocumentTemplates documentTemplates = new DocumentTemplates();
		boolean templateParseStatus = false;
		if (uploadedFile != null && fileExtensionOfTemplate != null) {
			try {
				DocumentTemplates documentTemplateSave = new DocumentTemplates();
				String originalFile = new String(uploadedFile.getOriginalFilename());
				// String fileExtension = FilenameUtils.getExtension(originalFile);
				documentTemplateSave.setTemplateName(originalFile);
				documentTemplateSave.setTemplateFileType(fileExtensionOfTemplate);
				documentTemplates = documentTemplatesService.save(documentTemplateSave);
				if (fileExtensionOfTemplate.equals("docx")) {
					templateParseStatus = saveTemplate(uploadedFile, documentTemplates.getId());
				} else if (fileExtensionOfTemplate.equals("csv")) {
					templateParseStatus = saveCsvTemplate(uploadedFile, documentTemplates.getId());
				} else {
					// templateParseStatus = parseImageUisngOCR(uploadedFile,
					// documentTemplates.getId());
					templateParseStatus = savePdftemplate(uploadedFile, documentTemplates.getId());
				}
				if (templateParseStatus)
					return documentTemplates;
				else {
					throw new IOException("Document template not saved");
				}

			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			}
		}

		else {
			throw new IOException("Document template or templateName not provided");
		}
		return documentTemplates;
	}

	@SuppressWarnings({ "resource", "unused" })
	@Transactional("transactionManager")
	public boolean saveCsvTemplate(MultipartFile uploadedFile, long id) throws IOException, Exception {
		List<Answer> answers = new ArrayList<Answer>();
		String originalFile = new String(uploadedFile.getOriginalFilename());
		String ext = FilenameUtils.getExtension(originalFile);
		String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
		File file = null;
		file = File.createTempFile(fileNameWithoutExtension, ext);
		uploadedFile.transferTo(file);
		try (FileReader reader = new FileReader(file);) {
			Map<String, String> values = new CSVReaderHeaderAware(reader).readMap();
			values.forEach((question, value) -> {
				DocumentQuestions documentQuestions = new DocumentQuestions();
				documentQuestions.setName(question);
				documentQuestions.setQuestionStatus(true);
				documentQuestions.setShowInUI(true);
				documentQuestions.setContentType("SUB_PARAGRAPH");
				documentQuestions.setAnswerType(AnswerType.TEXT);
				documentQuestions.setTemplateId(id);
				if (question.equals("Head Quarter Address")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("address");
				}
				if (question.equals("Website")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("website");
				}
				if (question.equals("Company Name")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("name");
				}
				if (question.equals("Address")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("address");
				}
				if (question.equals("Name Of Person")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("name");
				}
				if (question.equals("Phone Number")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("phoneNumber");
				}
				if (question.equals("Email")) {
					documentQuestions.setRequiredForOSINT(true);
					documentQuestions.setCaseField("email");
				}
				String response = null;
				try {
					response = getIsoCodesJson(question);
				} catch (Exception e) {
					ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				}
				// primary
				// and
				// secondary
				if (response != null) {
					JSONObject jsonResponseObjectNew = new JSONObject(response);
					JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
					if (arrayObjectNew.length() > 0) {

						JSONObject normalObject = arrayObjectNew.getJSONObject(0);
						if (normalObject.has("controls")) {
							if (normalObject.getJSONObject("controls").has("level3")) {
								JSONObject level3Object = normalObject.getJSONObject("controls")
										.getJSONObject("level3");
								if (level3Object.has("code")) {
									documentQuestions.setPrimaryIsoCode(level3Object.getString("code"));
									documentQuestions.setHasIsoCode(true);
								}
								if (level3Object.has("title"))
									documentQuestions.setLevel3Title(level3Object.getString("title"));
								if (level3Object.has("title"))
									documentQuestions.setLevel3Description(level3Object.getString("title"));
							}
							// documentQuestion.setPrimaryIsoCode(getPrimaryIsoCode(response));
							if (normalObject.getJSONObject("controls").has("level1")) {
								JSONObject level1Object = normalObject.getJSONObject("controls")
										.getJSONObject("level1");
								if (level1Object.has("code"))
									documentQuestions.setLevel1Code(level1Object.getString("code"));
								if (level1Object.has("title"))
									documentQuestions.setLevel1Title(level1Object.getString("title"));
							}
							documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
							if (normalObject.getJSONObject("controls").has("topic"))
								documentQuestions.setTopic(normalObject.getJSONObject("controls").getString("topic"));
						}

					}
				}
				if (documentQuestions.getPrimaryIsoCode() == null)
					documentQuestions.setHasIsoCode(false);
				documentsQuestionsService.save(documentQuestions);
			});
			if (values.size() > 0) {
				List<DocumentQuestions> controlQuestion = documentsQuestionsService.getListOfControlQuestion();
				for (DocumentQuestions documentQuestions2 : controlQuestion) {
					documentQuestions2.setTemplateId(id);
					documentsQuestionsService.save(documentQuestions2);
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return true;
	}

	// don't delete this method

	@Transactional("transactionManager")
	public boolean parseImageUisngOCR(MultipartFile uploadedFileData, long id) throws IOException, TesseractException {
		// Tesseract tesseract = Tesseract.getInstance();
		ITesseract tesseract = new Tesseract();
		tesseract.setDatapath("/home/ahextech/Desktop");

		File convFile = new File(uploadedFileData.getOriginalFilename());
		convFile.createNewFile();
		uploadedFileData.transferTo(convFile);

		/*
		 * TesseractOCRConfig config = new TesseractOCRConfig(); config.setTessdataPath(
		 * "/home/ahextech/suresh/ehubWorkspace/BITBUCKETCODE/element-backend/tessdata")
		 * ; config.setLanguage("eng");
		 */
		/*
		 * String originalFile = new String(uploadedFileData.getOriginalFilename());
		 * String fileExtension = FilenameUtils.getExtension(originalFile); String
		 * fileNameWithoutExtension1 = FilenameUtils.getBaseName(originalFile); File
		 * file1 = null; file1 = File.createTempFile(fileNameWithoutExtension1,
		 * fileExtension); uploadedFileData.transferTo(file1);
		 */
		// String text = tesseract.doOCR(new
		// File("/home/ahextech/Downloads/pocpdfconverting/totalpdfs/pdfs/Questionnaire_5.pdf"));
		String text = tesseract.doOCR(convFile);
		System.out.println(text);

		/*
		 * try { String string = null; String originalFile = new
		 * String(uploadedFileData.getOriginalFilename()); String ext =
		 * FilenameUtils.getExtension(originalFile); String fileNameWithoutExtension =
		 * FilenameUtils.getBaseName(originalFile); File file = null; file =
		 * File.createTempFile(fileNameWithoutExtension, ext);
		 * uploadedFileData.transferTo(file); File parent = new
		 * File("/home/ahextech/Downloads/opencvpoc/noiseReduction/");
		 * 
		 * PDDocument document = new PDDocument(); byte[] fileData = null; try {
		 * document = PDDocument.load(file); PDFRenderer pdfRenderer = new
		 * PDFRenderer(document); int pageCounter = 0; for (PDPage page :
		 * document.getPages()) { // note that the page number parameter is zero based
		 * BufferedImage imageProcessed = pdfRenderer.renderImageWithDPI(pageCounter,
		 * 300,org.apache.pdfbox.rendering.ImageType.RGB); String fileName =
		 * fileNameWithoutExtension + "-" + (pageCounter++); if (!parent.exists()) {
		 * parent.mkdir(); } String pathToFile = parent+"/"+fileName+".png";
		 * ImageIOUtil.writeImage(imageProcessed, pathToFile, 300);
		 * //ImageIO.write(image,"JPEG",new File(pathToFile)); try (FileInputStream
		 * inputStream = new FileInputStream(pathToFile)) { fileData =
		 * IOUtils.toByteArray(inputStream); } } } catch (Exception e) {
		 * ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		 * 
		 * } finally { if (file != null) { file.delete(); } if (document != null) { try
		 * { document.close(); } catch (IOException e) { ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass()); } } }
		 */

		/*
		 * // applying opencv on image System.loadLibrary(Core.NATIVE_LIBRARY_NAME); Mat
		 * source = Imgcodecs.imread(uploadedFileData.getOriginalFilename(),
		 * Imgcodecs.CV_LOAD_IMAGE_COLOR); Mat destination = new Mat(source.rows(),
		 * source.cols(), source.type()); destination = source;
		 * Photo.fastNlMeansDenoisingColored(source, destination, 10, 10, 7, 21);
		 * Imgcodecs.imwrite("/home/ahextech/Downloads/opencvpoc/denoise/latest2.png",
		 * destination);
		 * System.out.println("successfully created image with reduced noise");
		 * 
		 * // applying tesseractc Tesseract tesseract = new Tesseract(); String text =
		 * tesseract.doOCR(new
		 * File("/home/ahextech/Downloads/opencvpoc/denoise/latest2.png"));
		 * System.out.println(text);
		 * 
		 * ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		 * InputStream is =
		 * classloader.getResourceAsStream("updated_Questionnaire_2 .pdf");
		 */
		/*
		 * } catch (Exception e) { ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		 * 
		 * }
		 */
		return true;
	}

	@Transactional("transactionManager")
	public boolean savePdftemplate(MultipartFile uploadedFileData, long id)
			throws IOException, SAXException, TikaException, URISyntaxException, ParseException {
		try {
			int isScannedPdf = 0;
			int scannedNumber = 0;
			long parentId = 0;
			String originalFile = new String(uploadedFileData.getOriginalFilename());
			String ext = FilenameUtils.getExtension(originalFile);
			String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
			File file = null;
			file = File.createTempFile(fileNameWithoutExtension, ext);
			uploadedFileData.transferTo(file);
			JSONParser parser = new JSONParser();
			Object object = parser.parse(new FileReader(file));
			JSONObject jsonObject = new JSONObject(object.toString());
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			for (int i = 0; i < jsonArray.length(); i++) {
				System.out.println(i + "inside header");
				JSONObject jsonObjectOverall = jsonArray.getJSONObject(i);
				if (jsonObjectOverall.get("contentType").equals("HEADER")) {
					/*
					 * JSONObject josnObjectInner = jsonObjectOverall.getJSONObject("content");
					 * DocumentQuestions documentQuestions = new DocumentQuestions();
					 * documentQuestions.setName(josnObjectInner.getString("name").toString());
					 * String value = josnObjectInner.getString("question").toString();
					 * documentQuestions.setQuestionStatus(Boolean.parseBoolean(value));
					 * documentQuestions.setContentType("HEADER");
					 * documentQuestions.setTemplateId(id);
					 * documentsQuestionsService.save(documentQuestions);
					 */} else if (jsonObjectOverall.get("contentType").equals("SUB_PARAGRAPH")) {
					System.out.println(i + "inside subpara");

					JSONObject josnObjectInner = jsonObjectOverall.getJSONObject("content");
					DocumentQuestions documentQuestions = new DocumentQuestions();
					documentQuestions.setName(josnObjectInner.getString("name").toString());
					if (josnObjectInner.has("scanned"))
						scannedNumber = josnObjectInner.getInt("scanned");
					documentQuestions.setShowInUI(true);
					if (josnObjectInner.has("pageNumber")) {
						documentQuestions.setScannedPdfPageNumber(josnObjectInner.getInt("pageNumber"));
						if (documentQuestions.getScannedPdfPageNumber() > 0)
							isScannedPdf = documentQuestions.getScannedPdfPageNumber();
					}
					documentQuestions.setTemplateId(id);
					documentQuestions.setContentType("SUB_PARAGRAPH");
					if (parentId != 0) {
						documentQuestions.setHasParentId(parentId);
						parentId = 0;
					}
					String value = josnObjectInner.getString("question").toString();
					documentQuestions.setQuestionStatus(Boolean.parseBoolean(value));
					String response = getIsoCodesJson(josnObjectInner.getString("name").toString());// primary //
																									// secondary
					if (response != null) {
						JSONObject jsonResponseObjectNew = new JSONObject(response);
						JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
						if (arrayObjectNew.length() > 0) {
							JSONObject normalObject = arrayObjectNew.getJSONObject(0);
							if (normalObject.has("controls")) {
								if (normalObject.getJSONObject("controls").has("level3")) {
									JSONObject level3Object = normalObject.getJSONObject("controls")
											.getJSONObject("level3");
									if (level3Object.has("code")) {
										documentQuestions.setPrimaryIsoCode(level3Object.getString("code"));
										documentQuestions.setHasIsoCode(true);
									}
									if (level3Object.has("title"))
										documentQuestions.setLevel3Title(level3Object.getString("title"));
									if (level3Object.has("title"))
										documentQuestions.setLevel3Description(level3Object.getString("title"));
								}
								// documentQuestion.setPrimaryIsoCode(getPrimaryIsoCode(response));
								if (normalObject.getJSONObject("controls").has("level1")) {
									JSONObject level1Object = normalObject.getJSONObject("controls")
											.getJSONObject("level1");
									if (level1Object.has("code"))
										documentQuestions.setLevel1Code(level1Object.getString("code"));
									if (level1Object.has("title"))
										documentQuestions.setLevel1Title(level1Object.getString("title"));
								}
								documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
								if (normalObject.getJSONObject("controls").has("topic"))
									documentQuestions
											.setTopic(normalObject.getJSONObject("controls").getString("topic"));
							}
						} /*
							 * else { documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
							 * documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).
							 * toString()); }
							 */
					}
					if (documentQuestions.getPrimaryIsoCode() == null)
						documentQuestions.setHasIsoCode(false);
					documentQuestions
							.setAnswerType(AnswerType.valueOf(josnObjectInner.getString("answerType").toString()));
					documentQuestions.setPossibleAnswer(josnObjectInner.getString("possibleAnswers").toString());
					documentQuestions.setPdfAnswerCode(josnObjectInner.getString("answerCode").toString());
					if (josnObjectInner.has("requiredForOSINT"))
						documentQuestions.setRequiredForOSINT(josnObjectInner.getBoolean("requiredForOSINT"));
					if (josnObjectInner.has("caseField"))
						documentQuestions.setCaseField(josnObjectInner.getString("caseField"));
					DocumentQuestions savedQuestions = documentsQuestionsService.save(documentQuestions);
					if (josnObjectInner.has("hasChild"))
						parentId = savedQuestions.getId();
				} else {
					/*
					 * OverAllResponseDto overAllResponseDtoHeader = new OverAllResponseDto();
					 * List<DocumentContentDto> contentDtoHeaderList = new
					 * ArrayList<DocumentContentDto>(); DocumentContentDto contentDtoHeader = new
					 * DocumentContentDto();
					 * contentDtoHeader.setContentType(jsonObjectOverall.get("contentType").toString
					 * ()); JSONObject josnObjectInner= jsonObjectOverall.getJSONObject("content");
					 * contentDtoHeader.setQuestion(josnObjectInner.getBoolean("question"));
					 * ContentDataTableDto contentDataTableDto = new ContentDataTableDto();
					 * List<DataTableRowsDto> tableRowsDtoList = new ArrayList<DataTableRowsDto>();
					 */

				}
			}
			if (jsonArray.length() > 0) {
				List<DocumentQuestions> controlQuestion = documentsQuestionsService.getListOfControlQuestion();
				for (DocumentQuestions documentQuestions2 : controlQuestion) {
					documentQuestions2.setTemplateId(id);
					documentsQuestionsService.save(documentQuestions2);
				}
			}

			if (isScannedPdf > 0 || scannedNumber > 0) {
				DocumentTemplates documentTemplates = documentTemplatesService.find(id);
				if (isScannedPdf > 0)
					documentTemplates.setScannedPdf(true);
				if (scannedNumber > 0)
					documentTemplates.setScannedValue(scannedNumber);
				documentTemplatesService.saveOrUpdate(documentTemplates);
			}

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return true;
	}

	/*
	 * @Transactional("transactionManager") =======
	 * 
	 * @Transactional("transactionManager") >>>>>>>
	 * 716bfe66d5cb38ecab27e65a126481cc1990532a private Boolean saveTemplate(byte[]
	 * streamFile, Long templateId) throws IllegalStateException, IOException,
	 * InvalidFormatException {
	 * 
	 * @SuppressWarnings("resource") XWPFDocument doc = new XWPFDocument(new
	 * ByteArrayInputStream(streamFile)); Iterator<IBodyElement> iter =
	 * doc.getBodyElementsIterator();
	 * 
	 * // saving content type into database int i = 0; int j = 0; String[]
	 * questionAndAnswers; String[] answers; int overAll = 0;
	 * 
	 * try { while (iter.hasNext()) { IBodyElement elem = iter.next(); // for
	 * paragraph if (elem instanceof XWPFParagraph) { List<XWPFParagraph> paragraphs
	 * = doc.getParagraphs(); if (paragraphs.get(i).getText().length() != 0) { //
	 * style found if ((paragraphs.get(i).getStyle() != null) &&
	 * (paragraphs.get(i).getNumFmt() != null)) { DocumentContents documentContents
	 * = new DocumentContents(); documentContents.setContentType("SUB_PARAGRAPH");
	 * documentContents.setTemplateId(templateId); //
	 * documentContents.setParentId(calculateOverAll(overAll));
	 * documentContentsService.save(documentContents); System.out.println(
	 * paragraphs.get(i).getText() + "SUB_PARAGRAPH" + " " + "position :" +
	 * overAll);
	 * 
	 * DocumentQuestions documentQuestions = new DocumentQuestions();
	 * 
	 * // documentQuestions.setName(paragraphs.get(i).getText()); String[] val =
	 * paragraphs.get(i).getText().split(":"); documentQuestions.setName(val[0]);
	 * 
	 * documentQuestions.setQuestionStatus(false);
	 * documentQuestions.setDocumentContent(documentContents);
	 * 
	 * DocumentQuestions documentQuestionsSaved = documentsQuestionsService
	 * .save(documentQuestions);
	 * 
	 * if (paragraphs.get(i).getText().contains("?") ||
	 * paragraphs.get(i).getText().contains("YES?/NO?")) { questionAndAnswers =
	 * paragraphs.get(i).getText().split("(?<=\\?/?)"); if
	 * (questionAndAnswers.length > 1) { answers = questionAndAnswers[1].split("/");
	 * if (answers.length > 1) { // Character yes = new
	 * Character(answers[0].trim().charAt(3)); // Character no = new
	 * Character(answers[1].trim().charAt(2));
	 * documentQuestionsSaved.setName(questionAndAnswers[0]);
	 * 
	 * String response = getIsoCodesJson(questionAndAnswers[0]);// primary // and //
	 * secondary if (response != null) { JSONObject jsonResponseObjectNew = new
	 * JSONObject(response); JSONArray arrayObjectNew =
	 * jsonResponseObjectNew.getJSONArray("hits"); if (arrayObjectNew.length() > 0)
	 * { documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } else {
	 * documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } }
	 * 
	 * documentQuestionsSaved.setAnswerType(AnswerType.CHECKBOX);
	 * documentQuestionsSaved.setPossibleAnswer("yes,no");
	 * documentQuestionsSaved.setQuestionStatus(true);
	 * documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);
	 * 
	 * } else { documentQuestionsSaved.setAnswerType(AnswerType.TEXT_AREA);
	 * documentQuestionsSaved.setPossibleAnswer("text");
	 * documentQuestionsSaved.setQuestionStatus(true); String response =
	 * getIsoCodesJson(documentQuestionsSaved.getName());// primary // and //
	 * secondary if (response != null) { JSONObject jsonResponseObjectText = new
	 * JSONObject(response); JSONArray arrayText =
	 * jsonResponseObjectText.getJSONArray("hits"); if (arrayText.length() > 0) {
	 * documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } else {
	 * documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } }
	 * documentsQuestionsService.saveOrUpdate(documentQuestionsSaved); }
	 * 
	 * } } } // not found else { DocumentContents documentContentsParagraph = new
	 * DocumentContents(); documentContentsParagraph.setContentType("PARAGRAPH");
	 * documentContentsParagraph.setTemplateId(templateId);
	 * 
	 * // documentContents.setDocId(docId); //
	 * documentContentsParagraph.setParentId(calculateOverAll(overAll)); System.out
	 * .println(paragraphs.get(i).getText() + "PARAGRAPH" + " " + "position :" +
	 * overAll); // documentContentsService.save(documentContents);
	 * documentContentsService.save(documentContentsParagraph);
	 * 
	 * DocumentQuestions documentQuestionsParagraph = new DocumentQuestions(); //
	 * DocumentAnswers documentAnswers = new DocumentAnswers();
	 * 
	 * String[] valParagraph = paragraphs.get(i).getText().split(":");
	 * documentQuestionsParagraph.setName(valParagraph[0]);
	 * 
	 * documentQuestionsParagraph.setQuestionStatus(false);
	 * documentQuestionsParagraph.setDocumentContent(documentContentsParagraph);
	 * DocumentQuestions documentQuestionsSaved = documentsQuestionsService
	 * .save(documentQuestionsParagraph);
	 * 
	 * if (paragraphs.get(i).getText().contains("?") ||
	 * paragraphs.get(i).getText().contains("YES?/NO?")) { questionAndAnswers =
	 * paragraphs.get(i).getText().split("(?<=\\?/?)"); if
	 * (questionAndAnswers.length > 1) { answers = questionAndAnswers[1].split("/");
	 * if (answers.length > 1) { // Character yes = new
	 * Character(answers[0].trim().charAt(3)); // Character no = new
	 * Character(answers[1].trim().charAt(2));
	 * documentQuestionsSaved.setName(questionAndAnswers[0]);
	 * 
	 * String response = getIsoCodesJson(questionAndAnswers[0]);// primary // and //
	 * secondary if (response != null) { JSONObject jsonResponseObject = new
	 * JSONObject(response); JSONArray array =
	 * jsonResponseObject.getJSONArray("hits"); if (array.length() > 0) {
	 * documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } else {
	 * documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } }
	 * 
	 * documentQuestionsSaved.setAnswerType(AnswerType.CHECKBOX);
	 * documentQuestionsSaved.setPossibleAnswer("yes,no");
	 * documentQuestionsSaved.setQuestionStatus(true);
	 * documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);
	 * 
	 * } else { documentQuestionsSaved.setAnswerType(AnswerType.TEXT_AREA);
	 * documentQuestionsSaved.setPossibleAnswer("text");
	 * documentQuestionsSaved.setQuestionStatus(true); String response =
	 * getIsoCodesJson(documentQuestionsSaved.getName());// primary // and //
	 * secondary if (response != null) { JSONObject jsonResponseObject = new
	 * JSONObject(response); JSONArray array =
	 * jsonResponseObject.getJSONArray("hits"); if (array.length() > 0) {
	 * documentQuestionsSaved.setPrimaryIsoCode(getPrimaryIsoCode(response));
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } else {
	 * documentQuestionsSaved.setPrimaryIsoCode(getRandomISOCODE());
	 * documentQuestionsSaved.setSecondaryIsoCode(
	 * (getSecondaryIsoCode(response)).toString()); } }
	 * documentsQuestionsService.saveOrUpdate(documentQuestionsSaved);
	 * 
	 * } } } } } // empty if close
	 * 
	 * i++;// for next paragraph iteration
	 * 
	 * } // if close for paragraph
	 * 
	 * // found table else if (elem instanceof XWPFTable) {
	 * 
	 * DocumentContents documentContents = new DocumentContents();
	 * documentContents.setContentType("TABLE");
	 * documentContents.setTemplateId(templateId); //
	 * documentContents.setDocId(docId); //
	 * documentContents.setParentId(calculateOverAll(overAll)); DocumentContents
	 * documentContentTable = documentContentsService.save(documentContents);
	 * List<XWPFTable> xwpfTable = doc.getTables(); XWPFTable table =
	 * xwpfTable.get(j); int a = 0; DocumentQuestions documentQuestionsSaved = new
	 * DocumentQuestions(); System.out.println("table row size =" +
	 * table.getRows().size() + "table row =" + a);
	 * 
	 * for (XWPFTableRow row : table.getRows()) { int b = 0; for (XWPFTableCell cell
	 * : row.getTableCells()) { System.out.println("table row size =" +
	 * table.getRows().size() + "table row =" + a + "  cell data=" +
	 * cell.getText()); DocumentQuestions documentQuestions = new
	 * DocumentQuestions(); System.out.println("cell text : " +
	 * cell.getTextRecursively() + " hash code : " +
	 * cell.getTextRecursively().trim().hashCode());
	 * 
	 * documentQuestions.setName(cell.getText());
	 * documentQuestions.setDocumentContent(documentContentTable);
	 * documentQuestions.setQuestionStatus(true); String response =
	 * getIsoCodesJson(documentQuestions.getName()); if (response != null) {
	 * JSONObject jsonResponseObject = new JSONObject(response); JSONArray array =
	 * jsonResponseObject.getJSONArray("hits"); if (array.length() > 0) {
	 * documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
	 * documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).
	 * toString()); } else {
	 * documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
	 * documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).
	 * toString()); } } if (a != 0) { if (b == 0) {
	 * documentQuestions.setName(cell.getText()); } else { XWPFTableRow rowData =
	 * table.getRow(a); XWPFTableCell rowFirstCell = rowData.getCell(0);
	 * XWPFTableRow firstRow = table.getRow(0); if (rowData.getTableCells().size() >
	 * firstRow.getTableCells().size()) { documentQuestions.setName(cell.getText());
	 * } else { XWPFTableCell firstRowColumName = firstRow.getCell(b); String
	 * questionName = rowFirstCell.getText(); if (questionName != null) { if
	 * (firstRowColumName.getText() != null && firstRowColumName.getText().length()
	 * != 0) questionName = questionName + "," + firstRowColumName.getText();
	 * documentQuestions.setName(questionName); }
	 * 
	 * else { if (firstRowColumName.getText() != null &&
	 * firstRowColumName.getText().length() != 0) { questionName =
	 * firstRowColumName.getText(); documentQuestions.setName(questionName); } else
	 * { documentQuestions.setName(""); } }
	 * 
	 * } if (cell.getTextRecursively().trim().hashCode() == 9744)
	 * documentQuestions.setAnswerType(AnswerType.CHECKBOX); else
	 * documentQuestions.setAnswerType(AnswerType.TEXT); } }
	 * documentQuestions.setComponentAnswerMatrix((a + 1) + "*" + (b + 1));
	 * System.out.println("matrix position : " +
	 * documentQuestions.getComponentAnswerMatrix());
	 * 
	 * documentQuestionsSaved = documentsQuestionsService.save(documentQuestions);
	 * b++; }
	 * 
	 * a++; } if (table.getNumberOfRows() == 1) { XWPFTableRow singleRow =
	 * table.getRow(0); if (singleRow.getTableCells().size() == 1) {
	 * documentQuestionsSaved.setAnswerType(AnswerType.TEXT_AREA);
	 * documentsQuestionsService.saveOrUpdate(documentQuestionsSaved); }
	 * 
	 * }
	 * 
	 * j++;// for table iteration } // else if close
	 * 
	 * overAll++;
	 * 
	 * } // while close return true; } // try catch (Exception e) {
	 * ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass()); return false; }
	 * 
	 * }
	 */

	@SuppressWarnings("resource")
	@Override
	@Transactional("transactionManager")
	public CaseMapDocIdDto saveDocumetnAnswers(MultipartFile uploadedFile, Long userId, Long docId, Long templateId)
			throws IllegalStateException, IOException, InvalidFormatException, InsufficientDataException {
		CaseMapDocIdDto caseMapDocIdDto = new CaseMapDocIdDto();
		Map<String, String> caseFields = new HashMap<>();
		List<DocumentAnswers> answersList = new ArrayList<DocumentAnswers>();
		List<DocumentAnswers> documentTableAnswersList = new ArrayList<DocumentAnswers>();
		byte[] streamFile = uploadedFile.getBytes();
		long count = 0;
		XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(streamFile));
		// Iterator<IBodyElement> iter = doc.getBodyElementsIterator();
		try {
			// DocumentTemplates
			// documentTemplates=documentTemplatesService.find(templateId);
			/*
			 * ClassLoader classLoader = Thread.currentThread().getContextClassLoader(); URL
			 * resource = classLoader.getResource(documentTemplates.getTemplateName() +
			 * ".json"); File file = new File(resource.toURI()); JSONParser parser = new
			 * JSONParser(); Object object = parser.parse(new FileReader(file)); JSONObject
			 * jsonObject = new JSONObject(object.toString()); JSONArray jsonArray =
			 * jsonObject.getJSONArray("data");
			 */

			String[] questionAndAnswers = new String[2];
			String[] answers = new String[2];
			// List<DocumentContents>
			// documentContentsList=documentContentsDao.getParagraphsAndSubParagraphsByTemplateId(templateId);
			List<XWPFParagraph> paragraphs = doc.getParagraphs();
			List<XWPFTable> documentTablesList = doc.getTables();
			List<DocumentQuestions> documentQuestionList = documentQuestionsDao
					.getDocumentQuestionsByTemplateId(templateId, false);
			for (DocumentQuestions documentQuestions : documentQuestionList) {
				DocumentAnswers documentAnswers = new DocumentAnswers();
				documentAnswers.setDocumetnId(docId);
				documentAnswers.setDocumentQuestions(documentQuestions);
				// DocumentQuestions documentQuestions= new DocumentQuestions();
				// //documentQuestionsDao.getDocumentQuestionsById(documentContents.getId());
				if (documentQuestions != null && documentQuestions.isQuestionStatus() == true) {
					if (documentQuestions.getComponentAnswerMatrix() == null) {
						for (XWPFParagraph xwpfParagraph : paragraphs) {
							if (xwpfParagraph.getText() != null && xwpfParagraph.getText().length() != 0) {
								// System.out.println(xwpfParagraph.getText());
								if (xwpfParagraph.getText().contains(documentQuestions.getName())) {
									if (documentQuestions.getAnswerType() != null && documentQuestions.getAnswerType()
											.toString().equalsIgnoreCase("CHECKBOX")) {
										// questionAndAnswers =
										// xwpfParagraph.getText().split("(?<=\\?/?)");
										// System.out.println("in doc :- "+xwpfParagraph.getText());
										questionAndAnswers = xwpfParagraph.getText()
												.split(documentQuestions.getSplitWith());

										if (questionAndAnswers.length > 1) {
											answers = documentQuestions.getAnswerAtPosition().split(",");
											if (answers.length > 1) {
												Character yes = new Character(
														questionAndAnswers[1].charAt(Integer.parseInt(answers[0])));
												Character no = new Character(
														questionAndAnswers[1].charAt(Integer.parseInt(answers[1])));
												if (yes.hashCode() == 9746) {
													documentAnswers.setAnswer("YES");
												} else {
													if (no.hashCode() == 9746) {
														documentAnswers.setAnswer("NO");
													}

												}
											}
										}
									}
									if (documentQuestions.getAnswerType() != null
											&& documentQuestions.getAnswerType().toString().equalsIgnoreCase("TEXT")) {
										if (documentQuestions.getSplitWith() != null
												&& documentQuestions.getSplitWith().length() > 0) {

											String[] questingSplit = xwpfParagraph.getText()
													.split(documentQuestions.getSplitWith());
											if (questingSplit[1] != null && questingSplit[1] != "") {
												if (questingSplit[1].trim().startsWith("-"))
													documentAnswers
															.setAnswer(questingSplit[1].trim().replaceAll("_", ""));
											}

										}

									}
									documentAnswers.setModefiedBy(userId);

									if (documentAnswers.getAnswer() != null && documentAnswers.getAnswer() != "") {
										documentAnswers.setActualAnswer(documentAnswers.getAnswer());
										/*
										 * String evaluation = getEvaluation(documentQuestions.getName(),
										 * documentAnswers.getAnswer(), documentQuestions.getPrimaryIsoCode()); if
										 * (evaluation != null) { JSONObject evaluationObject = new
										 * JSONObject(evaluation);
										 * if(!evaluationObject.getString("evaluation").equalsIgnoreCase("error"))
										 * documentAnswers.setEvaluation(evaluationObject.getString("evaluation"));
										 * 
										 * } documentAnswersService.save(documentAnswers);
										 */
										answersList.add(documentAnswers);
									}

								}
							}

						}
					}
					if (documentQuestions.getComponentAnswerMatrix() != null
							&& documentQuestions.getComponentAnswerMatrix().length() != 0) {
						XWPFTable table = null;
						if (documentQuestions.getTableID() != null)
							table = documentTablesList.get(documentQuestions.getTableID() - 1);
						if (documentQuestions.getAnswerTableId() != null)
							table = documentTablesList.get(documentQuestions.getAnswerTableId() - 1);

						if (table != null) {
							if (documentQuestions.getComponentAnswerMatrix().contains("yes")
									&& documentQuestions.getComponentAnswerMatrix().contains(",")) {
								String[] answervalues = documentQuestions.getComponentAnswerMatrix().split(",");
								String[] yesPosition = answervalues[0].split("-");
								String[] noPosition = answervalues[1].split("-");
								String[] yesMatrix = yesPosition[1].split("\\*");
								String[] noMatrix = noPosition[1].split("\\*");
								int yesRowPosition = Integer.parseInt(yesMatrix[0]);
								int yesColumPosition = Integer.parseInt(yesMatrix[1]);
								if ((yesRowPosition - 1) < table.getNumberOfRows() && (yesColumPosition - 1) < table
										.getRow(yesRowPosition - 1).getTableCells().size()) {
									// System.out.println("yes - : "+table.getRow(yesRowPosition -
									// 1).getCell(yesColumPosition - 1)
									// .getTextRecursively().length());
									if (table.getRow(yesRowPosition - 1).getCell(yesColumPosition - 1)
											.getTextRecursively().length() != 0)
										documentAnswers.setAnswer("YES");

								}
								int noRowPosition = Integer.parseInt(noMatrix[0]);
								int noColumPosition = Integer.parseInt(noMatrix[1]);
								if ((noRowPosition - 1) < table.getNumberOfRows() && (noColumPosition - 1) < table
										.getRow(noRowPosition - 1).getTableCells().size()) {
									// System.out.println("no - : "+table.getRow(noRowPosition -
									// 1).getCell(noColumPosition - 1)
									// .getTextRecursively().length());
									if (table.getRow(noRowPosition - 1).getCell(noColumPosition - 1)
											.getTextRecursively().length() != 0)
										documentAnswers.setAnswer("NO");
								}
							} else if (documentQuestions.getComponentAnswerMatrix().contains("&")) {
								String[] answerValues = documentQuestions.getComponentAnswerMatrix().split("&");
								for (String string : answerValues) {
									String[] optinValues = string.split("-");
									String[] matrixPosition = optinValues[1].split("\\*");
									int noRowPosition = Integer.parseInt(matrixPosition[0]);
									int noColumPosition = Integer.parseInt(matrixPosition[1]);
									if ((noRowPosition - 1) < table.getNumberOfRows() && (noColumPosition - 1) < table
											.getRow(noRowPosition - 1).getTableCells().size()) {
										String answer = table.getRow(noRowPosition - 1).getCell(noColumPosition - 1)
												.getTextRecursively();
										if (answer != null) {
											if (answer.contains("Automatic") || answer.contains("Weekly")
													|| answer.contains("Monthly")) {
												String[] anseroption = answer.split(optinValues[0]);
												Character checkBox1 = new Character(anseroption[1].charAt(1));
												if (checkBox1.hashCode() == 9746)
													documentAnswers.setAnswer(optinValues[0]);

											} else {
												Character checkBox = new Character(answer.charAt(0));
												if (checkBox.hashCode() == 9746)
													documentAnswers.setAnswer(optinValues[0]);
												if (optinValues[0].contains("Other please specify")
														|| optinValues[0].equalsIgnoreCase("Other please specify")) {
													String questionMartrix = documentQuestions
															.getComponentQuestionMatrix();
													String[] questionPosition = questionMartrix.split("\\*");
													int quesRowPosition = Integer.parseInt(questionPosition[0]);
													int quesColumPosition = Integer.parseInt(questionPosition[1]);
													String answerText = table.getRow(quesRowPosition - 1)
															.getCell(quesColumPosition - 1).getTextRecursively();
													documentAnswers.setAnswer(answerText);

												}
											}

										}

									}

								}

							}

							else if (documentQuestions.getComponentQuestionMatrix() != null
									&& documentQuestions.getComponentAnswerMatrix() != null
									&& documentQuestions.getComponentQuestionMatrix()
											.equals(documentQuestions.getComponentAnswerMatrix())) {

								String[] matrixPosition = documentQuestions.getComponentAnswerMatrix().split("\\*");
								int noRowPosition = Integer.parseInt(matrixPosition[0]);
								int noColumPosition = Integer.parseInt(matrixPosition[1]);
								String answerText = table.getRow(noRowPosition - 1).getCell(noColumPosition - 1)
										.getTextRecursively();
								if (answerText != null) {
									if (documentQuestions.getSplitWith() != null) {
										if (answerText.contains(documentQuestions.getSplitWith())) {
											String[] textAnswer = answerText.split(documentQuestions.getSplitWith());
											if (!textAnswer[1].contains("Click here to enter text"))
												documentAnswers.setAnswer(textAnswer[1]);

										}

									}
								}

							}

							else if (documentQuestions.getAnswerType().toString().equalsIgnoreCase("CHECKBOX")
									&& documentQuestions.getSplitWith() != null
									&& documentQuestions.getAnswerAtPosition() != null) {
								String[] tableQuestion = documentQuestions.getComponentAnswerMatrix().split("\\*");
								int tableRowPosition = Integer.parseInt(tableQuestion[0]);
								int tableColumnPosition = Integer.parseInt(tableQuestion[1]);
								if ((tableRowPosition - 1) < table.getNumberOfRows()
										&& (tableColumnPosition - 1) < table.getRow(tableRowPosition - 1)
												.getTableCells().size()) {
									if (table.getRow(tableRowPosition - 1).getCell(tableColumnPosition - 1)
											.getTextRecursively() != null) {
										String answertext = table.getRow(tableRowPosition - 1)
												.getCell(tableColumnPosition - 1).getText();
										if (answertext != null) {
											String[] questionAndAnswers1 = answertext
													.split(documentQuestions.getSplitWith());

											if (questionAndAnswers1.length > 1) {
												String[] answers1 = documentQuestions.getAnswerAtPosition().split(",");
												if (answers1.length > 1) {
													Character yes = new Character(questionAndAnswers1[1]
															.charAt(Integer.parseInt(answers1[0])));
													Character no = new Character(questionAndAnswers1[1]
															.charAt(Integer.parseInt(answers1[1])));
													if (yes.hashCode() == 9746) {
														documentAnswers.setAnswer("YES");
													} else {
														if (no.hashCode() == 9746) {
															documentAnswers.setAnswer("NO");
														}

													}
												}
											}
										}
									}
								}
							}

							else {
								String[] tableQuestion = documentQuestions.getComponentAnswerMatrix().split("\\*");
								int tableRowPosition = Integer.parseInt(tableQuestion[0]);
								int tableColumnPosition = Integer.parseInt(tableQuestion[1]);
								if ((tableRowPosition - 1) < table.getNumberOfRows()
										&& (tableColumnPosition - 1) < table.getRow(tableRowPosition - 1)
												.getTableCells().size()) {
									if (table.getRow(tableRowPosition - 1).getCell(tableColumnPosition - 1)
											.getTextRecursively() != null)
										documentAnswers.setAnswer(table.getRow(tableRowPosition - 1)
												.getCell(tableColumnPosition - 1).getText());
								}
							}

						}
					}

					if (documentQuestions.getAnswerType() != null
							&& documentQuestions.getAnswerType().toString().equalsIgnoreCase("TABLE")) {
						XWPFTable table = null;
						if (documentQuestions.getAnswerTableId() != null) {
							table = documentTablesList.get(documentQuestions.getAnswerTableId() - 1);
							if (table != null) {
								for (int i = 1; i <= table.getRows().size(); i++) {
									XWPFTableRow row = table.getRow(i - 1);
									for (int j = 1; j <= row.getTableCells().size(); j++) {
										XWPFTableCell cell = row.getCell(j - 1);
										DocumentAnswers documentTableAnswers = new DocumentAnswers();
										documentTableAnswers.setDocumetnId(docId);
										documentTableAnswers.setDocumentQuestions(documentQuestions);
										String position = i + "*" + j;
										documentTableAnswers.setTableDataPosition(position);
										if (cell.getTextRecursively() != null) {
											documentTableAnswers.setAnswer(cell.getTextRecursively());
											documentTableAnswers.setActualAnswer(documentAnswers.getAnswer());
											documentTableAnswers.setModefiedBy(userId);
											// documentAnswersService.save(documentTableAnswers);
											documentTableAnswersList.add(documentTableAnswers);
										}

									}

								}
							}
						}

					}

					/*
					 * if ((documentAnswers.getAnswer() != null) &&
					 * (!documentAnswers.getAnswer().contains("Click here to enter text"))) {
					 */
					// if (documentAnswers.getAnswer().length() != 0) {
					if ((documentAnswers.getAnswer() != null)) {
						documentAnswers.setActualAnswer(documentAnswers.getAnswer());
					} else {
						documentAnswers.setAnswer("");
						documentAnswers.setActualAnswer("");
					}

					documentAnswers.setModefiedBy(userId);
					/*
					 * String evaluation = getEvaluation(documentQuestions.getName(),
					 * documentAnswers.getAnswer(), documentQuestions.getPrimaryIsoCode()); if
					 * (evaluation != null) { JSONObject evaluationObject = new
					 * JSONObject(evaluation); documentAnswers.setEvaluation(evaluationObject.
					 * getString("evaluation"));
					 * 
					 * } documentAnswersService.save(documentAnswers);
					 */
					answersList.add(documentAnswers);
					if (documentQuestions.isRequiredForOSINT()) {
						if (documentQuestions.getCaseField() != null)
							caseFields.put(documentQuestions.getCaseField(), documentAnswers.getAnswer());
					}
					// }
					// }
				}

			}

			count = answersList.stream().filter(answer -> answer.getAnswer().trim() != "")
					.filter(answer -> answer.getAnswer().trim().length() != 0).count();
			if (count <= 0)
				throw new InsufficientDataException("The document is empty");
			else {
				for (DocumentAnswers documentAnswer1 : documentTableAnswersList) {
					documentAnswersService.save(documentAnswer1);

				}
			}

			caseMapDocIdDto.setCaseFields(caseFields);
			caseMapDocIdDto.setDocumentAnswers(answersList);
			boolean isoCategoryStatus = saveIsoCategory(documentQuestionList, docId);
			if (!isoCategoryStatus)
				throw new InsufficientDataException("ISO categories are not saved");

			/*
			 * List<DocumentContents>
			 * documentContentTablesList=documentContentsDao.getTablesByTemplateId(
			 * templateId); List<XWPFTable> documentTablesList=doc.getTables(); int j=0; for
			 * (DocumentContents documentTable : documentContentTablesList) {
			 * List<DocumentQuestions>
			 * tableQuestions=documentQuestionsDao.getDocumentTableQuestionsById(
			 * documentTable.getId()); XWPFTable table=documentTablesList.get(j);
			 * 
			 * for (DocumentQuestions documentQuestions : tableQuestions) {
			 * //System.out.println("name : "+documentQuestions.getName()+"  length : "
			 * +documentQuestions.getName().length()); String answerMatrix =
			 * documentQuestions.getComponentAnswerMatrix(); if (answerMatrix != null &&
			 * documentQuestions.getName() != null) { String[] matrixPositions =
			 * answerMatrix.split("\\*"); int tableRowPosition =
			 * Integer.parseInt(matrixPositions[0]); int tableColumPosition =
			 * Integer.parseInt(matrixPositions[1]); if ((tableRowPosition - 1) != 0 &&
			 * (tableColumPosition - 1) != 0) {
			 * 
			 * XWPFTableRow tableRowAtPosition = table.getRow(tableRowPosition - 1);
			 * if((tableColumPosition - 1) < tableRowAtPosition.getTableCells().size()){
			 * System.out.println("No of cells in a row : "+tableRowAtPosition.getTableCells
			 * ().size()); DocumentAnswers documentTableAnswers = new DocumentAnswers();
			 * documentTableAnswers.setDocumetnId(docId); documentTableAnswers
			 * .setAnswer(tableRowAtPosition.getCell(tableColumPosition - 1).getText());
			 * documentTableAnswers.setDocumentQuestions(documentQuestions);
			 * documentTableAnswers.setModefiedBy(userId);
			 * System.out.println("ans : "+tableRowAtPosition.getCell(tableColumPosition -
			 * 1).getTextRecursively().trim()+"hash code:-"+tableRowAtPosition.getCell(
			 * tableColumPosition - 1).getTextRecursively().trim().hashCode());
			 * if(tableRowAtPosition.getCell(tableColumPosition -
			 * 1).getTextRecursively().trim().hashCode()==9746){
			 * 
			 * documentTableAnswers .setAnswer(table.getRow(0).getCell(tableColumPosition -
			 * 1).getText());
			 * 
			 * } if (documentTableAnswers.getAnswer() != null)
			 * documentAnswersService.save(documentTableAnswers); } }
			 * 
			 * if((tableColumPosition - 1) == 0 &&
			 * documentQuestions.getName().equalsIgnoreCase("Click here to enter text.")){
			 * DocumentAnswers documentTableAnswers = new DocumentAnswers();
			 * documentTableAnswers.setDocumetnId(docId); documentTableAnswers
			 * .setAnswer(table.getRow(tableRowPosition-1).getCell(tableColumPosition -
			 * 1).getText()); documentTableAnswers.setDocumentQuestions(documentQuestions);
			 * documentTableAnswers.setModefiedBy(userId); if
			 * (documentTableAnswers.getAnswer() != null)
			 * documentAnswersService.save(documentTableAnswers); } if ((tableRowPosition -
			 * 1) == 0 && (documentQuestions.getName() == null ||
			 * documentQuestions.getName().length()==0) && tableQuestions.size() != 1 ) {
			 * DocumentAnswers documentTableAnswers = new DocumentAnswers();
			 * documentTableAnswers.setDocumetnId(docId); documentTableAnswers
			 * .setAnswer(table.getRow(tableRowPosition-1).getCell(tableColumPosition -
			 * 1).getText()); documentTableAnswers.setDocumentQuestions(documentQuestions);
			 * documentTableAnswers.setModefiedBy(userId); if
			 * (documentTableAnswers.getAnswer() != null)
			 * documentAnswersService.save(documentTableAnswers); }
			 * 
			 * 
			 * } if (tableQuestions.size() == 1 &&
			 * documentQuestions.getComponentAnswerMatrix().equals("1*1")) {
			 * 
			 * DocumentAnswers documentTableAnswers = new DocumentAnswers();
			 * documentTableAnswers.setDocumetnId(docId);
			 * documentTableAnswers.setModefiedBy(userId);
			 * documentTableAnswers.setAnswer(table.getRow(0).getCell(0).getText());
			 * documentTableAnswers.setDocumentQuestions(documentQuestions); if
			 * (documentTableAnswers.getAnswer() != null)
			 * documentAnswersService.save(documentTableAnswers); }
			 * 
			 * 
			 * 
			 * 
			 * } j++; }
			 */

		} catch (Exception e) {
			// ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			if (count <= 0)
				throw new InsufficientDataException("The document is empty");
			else
				throw new InsufficientDataException("Document answers not saved into database.");
		}
		return caseMapDocIdDto;

	}

	@SuppressWarnings({ "unused", "resource", "static-access" })
	@Override
	@Transactional("transactionManager")
	public DocumentTemplates compareFileWithDocumentTemplate(MultipartFile uploadedFile, Long docId, Long userId,
			byte[] dataStream) throws IllegalStateException, IOException, InvalidFormatException, TesseractException {
		DocumentTemplates matchedDocumentTemplate = null;
		String originalFile = new String();
		DocumentVault documentValut = new DocumentVault();
		String fileExtension = "";
		if (docId != null) {
			documentValut = documentService.find(docId);
			fileExtension = documentValut.getType();
		} else {
			if (uploadedFile != null)
				originalFile = new String(uploadedFile.getOriginalFilename());
			fileExtension = FilenameUtils.getExtension(originalFile);
		}
		if (uploadedFile != null || dataStream != null) {
			if (fileExtension.equalsIgnoreCase("docx")) {
				String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
				File file = null;
				file = File.createTempFile(fileNameWithoutExtension, fileExtension);
				uploadedFile.transferTo(file);
				byte[] uploadedFileData = uploadedFile.getBytes();

				XWPFDocument doc = new XWPFDocument(new ByteArrayInputStream(uploadedFileData));
				Iterator<IBodyElement> iter = doc.getBodyElementsIterator();
				List<XWPFParagraph> paragraphs = doc.getParagraphs();
				List<XWPFTable> documentTablesList = doc.getTables();
				List<DocumentTemplates> documentTemplatesList = documentTemplatesDao
						.getDocumentTemplatesByFileExtension("docx");
				if (documentTemplatesList.size() != 0) {

					for (DocumentTemplates documentTemplates : documentTemplatesList) {
						if (matchedDocumentTemplate == null) {

							List<Boolean> statusList = new ArrayList<Boolean>();
							List<DocumentQuestions> documentQuestionList = documentQuestionsDao
									.getDocumentQuestionsByTemplateId(documentTemplates.getId(), false);

							for (DocumentQuestions documentQuestion : documentQuestionList) {

								if (documentQuestion.getContentType().equalsIgnoreCase("SUB_PARAGRAPH")
										&& documentQuestion.getTableID() == null) {
									if (documentQuestion.getName().length() != 0 && (!documentQuestion.isOSINT())) {
										int comparevalue = 0;
										for (XWPFParagraph paragraph : paragraphs) {
											if (paragraph.getText().length() != 0) {
												// System.out.println(
												// paragraph.getText() + "----" + documentQuestion.getName());
												if (paragraph.getText().contains(documentQuestion.getName())
														|| paragraph.getText().equals(documentQuestion.getName()))
													comparevalue = comparevalue + 1;
											}

										}
										if (comparevalue > 0)
											statusList.add(true);
										if (comparevalue == 0)
											statusList.add(false);

									}
									if (documentQuestion.isOSINT())
										statusList.add(true);

								}
								if (documentQuestion.getContentType().equalsIgnoreCase("SUB_PARAGRAPH")
										&& documentQuestion.getTableID() != null) {
									if (documentQuestion.getTableID() <= documentTablesList.size()) {
										XWPFTable table = documentTablesList.get(documentQuestion.getTableID() - 1);
										String[] tableQuestion = documentQuestion.getComponentQuestionMatrix()
												.split("\\*");
										int tableRowPosition = Integer.parseInt(tableQuestion[0]);
										int tableColumnPosition = Integer.parseInt(tableQuestion[1]);
										int comparevalue = 0;
										// System.out.println(table.getRow(tableRowPosition
										// - 1)
										// .getCell(tableColumnPosition -
										// 1).getTextRecursively().toString() +
										// "----"
										// + documentQuestion.getName());
										if ((tableRowPosition - 1) < table.getNumberOfRows()
												&& (tableColumnPosition - 1) < table.getRow(tableRowPosition - 1)
														.getTableCells().size()) {
											// System.out.println(table.getRow(tableRowPosition
											// - 1)
											// .getCell(tableColumnPosition -
											// 1).getTextRecursively().toString()
											// + "----"
											// + documentQuestion.getName());
											if (documentQuestion.getName().contains(table.getRow(tableRowPosition - 1)
													.getCell(tableColumnPosition - 1).getTextRecursively().toString())
													|| documentQuestion.getName()
															.equals(table.getRow(tableRowPosition - 1)
																	.getCell(tableColumnPosition - 1)
																	.getTextRecursively().toString())
													|| table.getRow(tableRowPosition - 1)
															.getCell(tableColumnPosition - 1).getTextRecursively()
															.toString().contains(documentQuestion.getName()))
												comparevalue = comparevalue + 1;
										}
										if (comparevalue > 0)
											statusList.add(true);
										if (comparevalue == 0)
											statusList.add(false);
									} else {
										statusList.add(false);
									}

								}

							}
							int falseCount = 0;
							for (Boolean status : statusList) {
								if (!status)
									falseCount = falseCount + 1;
							}
							if (falseCount == 0) {
								matchedDocumentTemplate = documentTemplates;
							}

						}
					}

				}

			} else if (fileExtension.equalsIgnoreCase("csv")) {

				String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
				File file = null;
				file = File.createTempFile(fileNameWithoutExtension, fileExtension);
				uploadedFile.transferTo(file);

				List<DocumentTemplates> documentTemplatesCsvList = documentTemplatesDao
						.getDocumentTemplatesByFileExtension(fileExtension);
				if (documentTemplatesCsvList.size() != 0) {
					out: for (DocumentTemplates documentTemplateCsv : documentTemplatesCsvList) {
						int unMatchCount = 0;
						if (matchedDocumentTemplate == null) {
							List<DocumentQuestions> documentQuestionList = documentQuestionsDao
									.getDocumentQuestionsByTemplateId(documentTemplateCsv.getId(), false);
							try (FileReader reader = new FileReader(file);) {
								Map<String, String> values = new CSVReaderHeaderAware(reader).readMap();
								for (DocumentQuestions documentQuestions : documentQuestionList) {
									if (!documentQuestions.isOSINT()
											&& !values.containsKey(documentQuestions.getName())) {
										unMatchCount = unMatchCount + 1;
									}
								}
							} catch (Exception e) {
								ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
							}
						}
						if (unMatchCount == 0) {
							matchedDocumentTemplate = documentTemplateCsv;
							break out;
						}

					}
				}
			}
			// for pdf
			else {

				PDDocument document = null;
				boolean scannedPdf = false;
				File someFile = null;
				File[] images = null;
				List<DocumentTemplates> documentTemplatesList = documentTemplatesDao
						.getDocumentTemplatesByFileExtension(fileExtension);
				String string = "";
				String stringValue = "";
				if (documentTemplatesList.size() != 0) {
					try {
						if (docId != null && (dataStream != null && dataStream.length > 0)) {
							document = PDDocument.load(dataStream);
						} else {
							document = PDDocument.load(uploadedFile.getInputStream());
						}
						PDFTextStripper pdfTextStripper = new PDFLayoutTextStripper();
						string = pdfTextStripper.getText(document);
					} finally {
						if (document != null)
							document.close();
					}

					if (string == null || string.length() == 0) {
						ITesseract tesseract = new Tesseract();
						tesseract.setDatapath(TESSRACT_LANGUAGE);
						if (uploadedFile != null) {
							File convFile = new File(uploadedFile.getOriginalFilename());
							convFile.createNewFile();
							uploadedFile.transferTo(convFile);
							stringValue = tesseract.doOCR(convFile);// scanned pdf
						} else {
							someFile = new File(TESSRACT_IMAGE_ALLOCATION + "/scanned.pdf");// creating file to pass for
																							// OCR
							FileOutputStream fos = new FileOutputStream(someFile);
							if (dataStream != null)
								fos.write(dataStream);
							fos.flush();
							fos.close();
							stringValue = tesseract.doOCR(someFile);
						}
						string = "";
						string = stringValue;
						// System.out.println(string);
						scannedPdf = true;
					}
					out: for (DocumentTemplates documentTemplatesPdf : documentTemplatesList) {
						double documentQuestionsSize = 0;
						List<Boolean> statusList = new ArrayList<Boolean>();
						if (matchedDocumentTemplate == null) {
							List<DocumentQuestions> documentQuestionList = documentQuestionsDao
									.getDocumentQuestionsByTemplateId(documentTemplatesPdf.getId(), true);
							if (!documentQuestionList.isEmpty())
								documentQuestionsSize = documentQuestionList.size() - 11;// removing extra osint
																							// questions
							String trimValue = string.trim().replaceAll("\\s{2,}", " ");
							// String[] strArray = string.split("\n");
							// Map<String,String> mapDocument = new HashMap<String,String>();
							List<String> myList = new ArrayList<String>();
							try {
								if (scannedPdf) {
									someFile = new File(TESSRACT_IMAGE_ALLOCATION + "/scanned.pdf");
									FileOutputStream fos = new FileOutputStream(someFile);
									if (dataStream != null)
										fos.write(dataStream);
									if (uploadedFile != null)
										fos.write(uploadedFile.getBytes());
									fos.flush();
									fos.close();
									images = PdfUtilities.convertPdf2Png(someFile);
								}
								for (DocumentQuestions documentQuestion : documentQuestionList) {
									int comparevalue = 0;
									// if (documentQuestion.getContentType().equalsIgnoreCase("SUB_PARAGRAPH") ||
									// documentQuestion.getContentType().equalsIgnoreCase("Question")) {
									if (documentQuestion.getName().length() != 0 && (!documentQuestion.isOSINT())) {
										// String question ="(.*)"+documentQuestion.getName()+"(.*)";
										System.out.println(documentQuestion.getName());
										boolean val = false;
										if (scannedPdf) {
											// for new scanned pdf comparison wirth coordinates
											if (documentTemplatesPdf.isDocParserTemplate()
													&& documentTemplatesPdf.getInitialDocId() != null) {
												List<String> items = null;
												if (documentQuestion.getQuestionPosition() != null
														&& !documentQuestion.getQuestionPosition().equals(""))
													items = Arrays.asList(
															documentQuestion.getQuestionPosition().split("\\s*,\\s*"));
												Integer a = (Integer.parseInt(items.get(0)) * 300) / 72;
												Integer b = (Integer.parseInt(items.get(1)) * 300) / 72;
												Integer c = (Integer.parseInt(items.get(2)) * 300) / 72;
												Integer d = (Integer.parseInt(items.get(3)) * 300) / 72;
												Rectangle rectangle = new Rectangle(a, b, c, d); // doc parser coord
												ITesseract tesseract = new Tesseract();
												tesseract.setDatapath(TESSRACT_LANGUAGE);
												String replaceExtra1 = tesseract
														.doOCR(images[documentQuestion.getPageNumber() - 1], rectangle);
												if (replaceExtra1 != null
														&& (replaceExtra1.contains(documentQuestion.getName())
																|| replaceExtra1.equals(documentQuestion.getName()))) {
													val = true;
												}
											}
											// for old scanned pdf comparison
											else {
												val = trimValue.contains(documentQuestion.getName());
											}
										} else {
											if (documentTemplatesPdf.isDocParserTemplate()
													&& documentTemplatesPdf.getInitialDocId() != null) {
												CoordinatesResponseDto coordinatesDto = new CoordinatesResponseDto();
												if (documentQuestion.getQuestionPosition() != null
														&& !documentQuestion.getQuestionPosition().equals("")) {
													coordinatesDto.setQuestionCoordinates(
															documentQuestion.getQuestionPosition());
													coordinatesDto.setPageNumber(documentQuestion.getPageNumber());
													String question = "";
													if (dataStream == null)
														question = pdfReaderNewTemplateUtil.parsePdfFile(
																uploadedFile.getBytes(), coordinatesDto, false);
													else
														question = pdfReaderNewTemplateUtil.parsePdfFile(dataStream,
																coordinatesDto, false);
													String replaceExtra1 = question.replaceAll("_", " ");
													if (replaceExtra1 != null && (replaceExtra1.trim()
															.contains(documentQuestion.getName())
															|| replaceExtra1.trim()
																	.equals(documentQuestion.getName()))) {
														val = true;
													}
												}
											} else
												val = Pattern.compile(Pattern.quote(documentQuestion.getName()),
														Pattern.CASE_INSENSITIVE).matcher(trimValue).find();
										}
										if (val)
											comparevalue = comparevalue + 1;
									}
									// }

									if (comparevalue > 0)
										statusList.add(true);
									if (comparevalue == 0)
										statusList.add(false);

								}
							} // try close
							catch (Exception e) {
								ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
							} finally {
								File old = new File(TESSRACT_IMAGE_ALLOCATION + "/scanned.pdf");
								if (old != null)
									old.delete();
								if (images != null) {
									for (File file : images) {
										file.delete();
									}
								}
							}
						}
						int falseCount = 0;
						int trueCount = 0;
						for (Boolean status : statusList) {
							if (!status)
								falseCount = 0;// has to change
							else
								trueCount = trueCount + 1;
						}

						if (documentQuestionsSize > 0) {
							double compareVal = 0;
							if (scannedPdf) {
								if (documentTemplatesPdf.getInitialDocId() != null) {
									compareVal = documentQuestionsSize;
								} else {
									compareVal = (documentQuestionsSize * 1) / 4;
									compareVal = compareVal - 8;
								}
							} else {
								if (documentTemplatesPdf.getInitialDocId() != null)
									compareVal = documentQuestionsSize;
								else
									compareVal = (documentQuestionsSize * 75) / 100;
							}
							if (Double.valueOf(trueCount) >= compareVal) {
								matchedDocumentTemplate = documentTemplatesPdf;
								break out;
							}
						}

					} // template list close
				} // template size if close

			} // else close
		} // over all if close for empty file
		return matchedDocumentTemplate;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public List<OverAllResponseDto> getDocumentParseData(Long docId) {

		DocumentTemplateMapping documentTemplateMapping = documentTemplateMappingDao
				.getDocumentTemplateMappingByDocId(docId);
		List<DocumentContents> documentContentsList = documentContentsDao
				.getDocumentContentById(documentTemplateMapping.getTemplateId().getId());
		List<DocumentQuestions> documentQuestionList = documentQuestionsDao
				.getDocumentQuestionsByTemplateId(documentTemplateMapping.getTemplateId().getId(), false);
		List<IsoCategory> isoCategoryList = isoCategoryDao.getISOCategoriesByDocId(docId);
		List<OverAllResponseDto> overAllResponseDto = new ArrayList<OverAllResponseDto>();
		boolean isFillablePdf = documentTemplateMapping.getTemplateId().isFillablePdf();
		try {
			OverAllResponseDto allResponseDto = new OverAllResponseDto();
			List<DocumentContentDto> documentContentDtoList = new ArrayList<DocumentContentDto>();
			List<Categories> categoriesList = new ArrayList<Categories>();

			for (DocumentQuestions documentQuestions : documentQuestionList) {
				if (documentQuestions.getContentType().trim().equalsIgnoreCase("PARAGRAPH")
						|| documentQuestions.getContentType().trim().equalsIgnoreCase("SUB_PARAGRAPH")
						|| documentQuestions.getContentType().trim().equalsIgnoreCase("Question")) {
					// for child object
					if (documentQuestions.getHasParentId() != 0) {
						for (DocumentContentDto documentContentDtoInner : documentContentDtoList) {
							ContentDataDto contentDataDtoInner = documentContentDtoInner.getContentDataDto();
							if (contentDataDtoInner != null) {
								if (contentDataDtoInner.getQuestionId() != null) {
									if (contentDataDtoInner.getQuestionId() == documentQuestions.getHasParentId()) {
										DocumentAnswers documentAnswers = new DocumentAnswers();
										documentAnswers = documentAnswersDao
												.getDcoumentAnswerByQuestionId(documentQuestions.getId(), docId);
										contentDataDtoInner.setSpecifiedQuestion(documentQuestions.getName());
										contentDataDtoInner.setSpecifiedQuestionId(documentQuestions.getId());
										if (documentAnswers != null) {
											contentDataDtoInner.setSpecifiedAnswer(documentAnswers.getAnswer());
											contentDataDtoInner.setSpecifiedAnswerId(documentAnswers.getId());
										}
										if (isFillablePdf) {
											List<PossibleAnswers> possibleAnswersList = documentQuestions
													.getPossibleAnswers();
											List<PossibleAnswersDto> possibleAnswersDtoList = new ArrayList<PossibleAnswersDto>();
											if (!possibleAnswersList.isEmpty()) {
												for (PossibleAnswers possibleAnswers : possibleAnswersList) {
													PossibleAnswersDto possibleAnswersDto = new PossibleAnswersDto();
													BeanUtils.copyProperties(possibleAnswers, possibleAnswersDto);
													possibleAnswersDtoList.add(possibleAnswersDto);
												}
												contentDataDtoInner
														.setSpecifiedPossibleAnswersDto(possibleAnswersDtoList);
											}
										}

									}
								}
							}
						}
					}
					// for parent object
					else {
						DocumentContentDto documentContentDto = new DocumentContentDto();
						// DocumentQuestions documentQuestions = new DocumentQuestions();
						DocumentAnswers documentAnswers = new DocumentAnswers();
						ContentDataDto contentDataDto = new ContentDataDto();
						// documentQuestions =
						// documentQuestionsDao.getDocumentQuestionsById(documentContents.getId());

						/*
						 * if (documentQuestions.getContentType().trim().equalsIgnoreCase("PARAGRAPH"))
						 * documentContentDto.setContentType("PARAGRAPH"); if
						 * (documentQuestions.getContentType().trim().equalsIgnoreCase("Question"))
						 * documentContentDto.setContentType("Question");
						 */
						documentContentDto.setContentType(documentQuestions.getContentType());
						if (documentQuestions.isQuestionStatus()) {
							documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
							contentDataDto.setQuestion(documentQuestions.getName());
							contentDataDto.setQuestionId(documentQuestions.getId());
							contentDataDto.setAnswerType(documentQuestions.getAnswerType());
							contentDataDto.setHasIsoCode(documentQuestions.isHasIsoCode());
							if (documentQuestions.getPossibleAnswer() != null
									|| documentQuestions.getPossibleAnswer() != "")
								contentDataDto.setPossibleAnswer(documentQuestions.getPossibleAnswer());
							if (isFillablePdf) {
								List<PossibleAnswers> possibleAnswersList = documentQuestions.getPossibleAnswers();
								List<PossibleAnswersDto> possibleAnswersDtoList = new ArrayList<PossibleAnswersDto>();
								if (!possibleAnswersList.isEmpty()) {
									for (PossibleAnswers possibleAnswers : possibleAnswersList) {
										PossibleAnswersDto possibleAnswersDto = new PossibleAnswersDto();
										BeanUtils.copyProperties(possibleAnswers, possibleAnswersDto);
										possibleAnswersDtoList.add(possibleAnswersDto);
									}
									contentDataDto.setPossibleAnswersDto(possibleAnswersDtoList);
								}
							}
							if (documentQuestions.getPrimaryIsoCode() != null
									|| documentQuestions.getPrimaryIsoCode() != "")
								contentDataDto.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode());
							if (documentQuestions.getSecondaryIsoCode() != null
									|| documentQuestions.getSecondaryIsoCode() != "")
								contentDataDto.setSecondaryIsoCodes(documentQuestions.getSecondaryIsoCode());
							if (documentQuestions.getTopic() != null || documentQuestions.getTopic() != "")
								contentDataDto.setTopic(documentQuestions.getTopic());
							if (documentQuestions.getLevel1Code() != null || documentQuestions.getLevel1Code() != "")
								contentDataDto.setLevel1Code(documentQuestions.getLevel1Code());
							if (documentQuestions.getLevel1Title() != null || documentQuestions.getLevel1Title() != "")
								contentDataDto.setLevel1Title(documentQuestions.getLevel1Title());
							if (documentQuestions.getLevel3Title() != null || documentQuestions.getLevel3Title() != "")
								contentDataDto.setLevel3Title(documentQuestions.getLevel3Title());
							if (documentQuestions.getLevel3Description() != null
									|| documentQuestions.getLevel3Description() != "")
								contentDataDto.setLevel3Description(documentQuestions.getLevel3Description());
							contentDataDto.setIsOSINT(documentQuestions.isOSINT());
							if (documentQuestions.getShowInUI() != null)
								contentDataDto.setShowINUI(documentQuestions.getShowInUI());
							if (documentQuestions.getAnswerType() != null) {
								if (!documentQuestions.getAnswerType().equals(AnswerType.TABLE)) {
									documentAnswers = documentAnswersDao
											.getDcoumentAnswerByQuestionId(documentQuestions.getId(), docId);
									if (documentAnswers != null) {
										contentDataDto.setAnswer(documentAnswers.getAnswer());
										contentDataDto.setAnswerId(documentAnswers.getId());
										contentDataDto.setEvaluation(documentAnswers.getEvaluation());
										contentDataDto.setActualAnswer(documentAnswers.getActualAnswer());
										Users user = usersDao.find(documentAnswers.getModefiedBy());
										contentDataDto.setModefiedBy(user.getScreenName().toString());
										if (documentAnswers.getRankUpdatedBy() != 0) {
											Users rankUpdatedbUser = usersDao.find(documentAnswers.getRankUpdatedBy());
											contentDataDto
													.setRankModefiedBy(rankUpdatedbUser.getScreenName().toString());
										}
										if (documentAnswers.getRankUpdatedTime() != null)
											contentDataDto.setRankUpdatedOn(documentAnswers.getRankUpdatedTime());
										if (documentAnswers.getUpdatedTime() != null)
											contentDataDto.setAnswerUpdatedOn(documentAnswers.getUpdatedTime());
										contentDataDto.setAnswerRanking(documentAnswers.getAnswerRank());
										if (documentAnswers.getSourceUrls() != null)
											contentDataDto.setSourceUrls(documentAnswers.getSourceUrls());

									}
								}
								if (documentQuestions.getAnswerType().equals(AnswerType.TABLE)) {
									TableDto tableDto = new TableDto();
									List<DocumentAnswers> tableAnswersList = documentAnswersDao
											.getListOfTableAnswers(documentQuestions.getId(), docId);
									HashSet<String> columsSet = new HashSet<String>();
									HashSet<String> rowsSet = new HashSet<String>();
									for (DocumentAnswers tableAnswer : tableAnswersList) {
										String tablePosition = tableAnswer.getTableDataPosition();
										if (tablePosition != null && tablePosition.contains("*")) {
											String[] positoins = tablePosition.split("\\*");
											if (positoins.length == 2) {
												rowsSet.add(positoins[0]);
												columsSet.add(positoins[1]);
											}
										}

									}
									List<RowsDto> rowsDto = new ArrayList<RowsDto>();
									for (int i = 0; i < rowsSet.size(); i++) {
										RowsDto row = new RowsDto();
										List<ColumnsDto> ColumnsDto = new ArrayList<ColumnsDto>();
										for (int j = 0; j < columsSet.size(); j++) {
											ColumnsDto colum = new ColumnsDto();
											ColumnsDto.add(colum);
										}
										row.setColumns(ColumnsDto);
										rowsDto.add(row);
									}

									for (DocumentAnswers tableAnswer : tableAnswersList) {
										String tablePosition = tableAnswer.getTableDataPosition();
										if (tablePosition != null && tablePosition.contains("*")) {
											String[] positoins = tablePosition.split("\\*");
											int row = Integer.parseInt(positoins[0]);
											int colum = Integer.parseInt(positoins[1]);
											ColumDataDto columDataDto = new ColumDataDto();
											columDataDto.setId(tableAnswer.getId());
											columDataDto.setName(tableAnswer.getAnswer());
											if (row == 1)
												columDataDto.setHeader(true);
											else
												columDataDto.setHeader(false);
											rowsDto.get(row - 1).getColumns().get(colum - 1)
													.setColumDataDto(columDataDto);
										}

									}
									tableDto.setRows(rowsDto);
									contentDataDto.setTable(tableDto);
								}
							}
							if (documentQuestions.getPrimaryIsoCode() != null
									&& documentQuestions.getPrimaryIsoCode().equalsIgnoreCase("BST.1.3")) {
								if (documentAnswers != null && documentAnswers.getId() != 0) {
									List<AnswerTopEvents> topEvents = answerTopEventsDao
											.getTopEventsByAnswerId(documentAnswers.getId());
									List<TopEventsDto> topEventsList = new ArrayList<TopEventsDto>();
									for (AnswerTopEvents answerTopEvents : topEvents) {
										TopEventsDto topEventsDto = new TopEventsDto();
										topEventsDto.setEventId(answerTopEvents.getId());
										if (answerTopEvents.getLocationName() != null)
											topEventsDto.setLocationName(answerTopEvents.getLocationName());
										if (answerTopEvents.getPublished() != null)
											topEventsDto.setPublished(answerTopEvents.getPublished());
										if (answerTopEvents.getTechnologies() != null)
											topEventsDto.setTechnologies(answerTopEvents.getTechnologies());
										if (answerTopEvents.getText() != null)
											topEventsDto.setText(answerTopEvents.getText());
										if (answerTopEvents.getTitle() != null)
											topEventsDto.setTitle(answerTopEvents.getTitle());
										if (answerTopEvents.getUrl() != null)
											topEventsDto.setUrl(answerTopEvents.getUrl());
										topEventsList.add(topEventsDto);

									}
									if (topEventsList.size() > 0)
										contentDataDto.setTopEvents(topEventsList);
								}
							}
						}
						if (!documentQuestions.isQuestionStatus()) {
							documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
							contentDataDto.setText(documentQuestions.getName());
						}
						documentContentDto.setContentDataDto(contentDataDto);
						documentContentDtoList.add(documentContentDto);
					} // else close
				} // sub paragraph close

				/*
				 * if (documentContents.getContentType().trim().equalsIgnoreCase("TABLE")) {
				 * DocumentContentDto documentContentDto = new DocumentContentDto();
				 * documentContentDto.setContentType("TABLE"); ContentDataDto contentDataDtot =
				 * new ContentDataDto(); List<DocumentQuestions> documentQuestionsList =
				 * documentQuestionsDao .getTableQuestionsById(documentContents.getId());
				 * ContentDataTableDto contentDataTableDto = new ContentDataTableDto(); if
				 * (documentQuestionsList.size() > 0) { HashSet<String> tableRowsCount = new
				 * HashSet<String>(); HashSet<String> tableColumnsCount = new HashSet<String>();
				 * String answerMatrix = null; String questionMatrix = null; for
				 * (DocumentQuestions documentQuestionsTable : documentQuestionsList) {
				 * answerMatrix = documentQuestionsTable.getComponentAnswerMatrix(); if
				 * (answerMatrix != null) { String rowCount[] =
				 * answerMatrix.trim().split("\\*"); tableRowsCount.add(rowCount[0]);
				 * tableColumnsCount.add(rowCount[1]); } questionMatrix =
				 * documentQuestionsTable.getComponentQuestionMatrix();
				 * 
				 * }
				 * 
				 * List<DataTableRowsDto> dataTableRows = new ArrayList<DataTableRowsDto>(); for
				 * (int a = 0; a < tableRowsCount.size(); a++) { DataTableRowsDto
				 * dataTableRowsDto = new DataTableRowsDto(); List<DataTableColumsDto>
				 * dataTableColums = new ArrayList<DataTableColumsDto>(); for (int b = 0; b <
				 * tableColumnsCount.size(); b++) { DataTableColumsDto dataTableColumsDto = new
				 * DataTableColumsDto(); ContentDataDto tableContentDataDto = new
				 * ContentDataDto(); dataTableColumsDto.setContentDataDto(tableContentDataDto);
				 * dataTableColums.add(dataTableColumsDto);
				 * 
				 * } dataTableRowsDto.setDataTableColumsDto(dataTableColums);
				 * dataTableRows.add(dataTableRowsDto); } for (DocumentQuestions
				 * documentQuestions : documentQuestionsList) { DocumentAnswers documentAnswers
				 * = documentAnswersDao
				 * .getDcoumentAnswerByQuestionId(documentQuestions.getId(),docId);
				 * ContentDataDto contentDataDtoTable1 = new ContentDataDto(); ContentDataDto
				 * contentDataDtoTable2 = new ContentDataDto(); ContentDataDto
				 * contentDataDtoTableAns = new ContentDataDto(); String answerMatrix1 =
				 * documentQuestions.getComponentAnswerMatrix(); String[] answerMatrix1Pos =
				 * answerMatrix1.split("\\*");
				 * 
				 * contentDataDtoTableAns.setAnswerId(documentAnswers.getId());
				 * contentDataDtoTableAns.setAnswer(documentAnswers.getAnswer());
				 * contentDataDtoTableAns.setAnswerType(documentQuestions.getAnswerType());
				 * 
				 * 
				 * int ansRowPosition = Integer.parseInt(answerMatrix1Pos[0]) - 1; int
				 * ansColumPosition = Integer.parseInt(answerMatrix1Pos[1]) - 1;
				 * contentDataDtoTableAns.setAnswerType(documentQuestions.getAnswerType());
				 * contentDataDtoTableAns.setQuestion(documentQuestions.getName());
				 * contentDataDtoTableAns.setQuestionId(documentQuestions.getId());
				 * contentDataDtoTableAns
				 * .setPossibleAnswer(documentQuestions.getPossibleAnswer());
				 * contentDataDtoTableAns.setRiskScore(documentQuestions.getRiskScore());
				 * contentDataDtoTableAns.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode(
				 * )); contentDataDtoTableAns.setSecondaryIsoCodes(documentQuestions.
				 * getSecondaryIsoCode());
				 * dataTableRows.get(ansRowPosition).getDataTableColumsDto()
				 * .get(ansColumPosition).setContentDataDto(contentDataDtoTableAns); }
				 * contentDataTableDto.setDataTableRowsDto(dataTableRows); }
				 * documentContentDto.setContentDataTableDto(contentDataTableDto);
				 * documentContentDtoList.add(documentContentDto); }
				 */

			}
			allResponseDto.setDocumentContentDto(documentContentDtoList);

			for (IsoCategory isoCategory : isoCategoryList) {
				Categories category = new Categories();
				category.setCategoryId(isoCategory.getId());
				category.setLevel1Code(isoCategory.getLevel1Code());
				category.setLevelTitle(isoCategory.getLevel1Title());
				category.setEvaluation(isoCategory.getEvaluation());
				categoriesList.add(category);

			}
			allResponseDto.setCategories(categoriesList);
			overAllResponseDto.add(allResponseDto);

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return overAllResponseDto;

	}

	@SuppressWarnings({ "unused", "resource" })
	@Override
	@Transactional("transactionManager")
	public CaseMapDocIdDto saveDocumetnAnswersPdf(MultipartFile uploadFile, Long userId, long docId1,
			DocumentTemplates documentTemplates, byte[] dataStream)
			throws InvalidPasswordException, IOException, TesseractException {
		Map<String, String> map = new HashMap<String, String>();
		Map<String, String> caseFields = new HashMap<>();
		CaseMapDocIdDto caseMapDocIdDto = new CaseMapDocIdDto();
		List<DocumentAnswers> answersList = new ArrayList<DocumentAnswers>();
		long count = 0;
		if (!documentTemplates.isScannedPdf() && !documentTemplates.isNormalPdf()) {
			PDDocument document = null;
			try {
				if (dataStream != null && dataStream.length > 0)
					document = PDDocument.load(dataStream);
				else
					document = PDDocument.load(uploadFile.getInputStream());
				// new implementation

				if (documentTemplates.isDocParserTemplate() == false) {
					PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

					if (acroForm != null) {
						for (PDField form : acroForm.getFieldTree()) {
							map.put(form.getFullyQualifiedName(), form.getValueAsString());
						}
					}
				}
				List<org.json.simple.JSONObject> jsonObjectsList = getAnswerPositions(uploadFile, dataStream);
				List<DocumentQuestions> documentQuestionsList = documentQuestionsDao
						.getDocumentQuestionsByTemplateId(documentTemplates.getId(), true);
				for (DocumentQuestions documentQuestions : documentQuestionsList) {

					if (documentQuestions.isQuestionStatus() == true && !documentQuestions.isOSINT()) {
						DocumentAnswers documentAnswers = new DocumentAnswers();
						documentAnswers.setDocumetnId(docId1);
						if (documentTemplates.isDocParserTemplate() == true) {
							// new doc parser answers reading
							if ((!documentQuestions.getAnswerType().toString().equals("CHECKBOX"))
									&& (!documentQuestions.getAnswerType().toString().equals("YES_NO_CHECKBOX"))) {
								org.json.simple.JSONObject object = getAnswer(jsonObjectsList,
										documentQuestions.getPageNumber(), documentQuestions.getAnswerPosition());
								if (object.get("answer") != null)
									documentAnswers.setAnswer(object.get("answer").toString());
							} else {
								List<PossibleAnswers> possibleAnswers = documentQuestions.getPossibleAnswers();
								if (!possibleAnswers.isEmpty()) {
									if (possibleAnswers.size() == 2)// for yes or no
									{
										// for checkboxes for some files
										if (documentQuestions.getAnswerType().toString().equals("CHECKBOX")) {
											iner: for (PossibleAnswers possible : possibleAnswers) {
												org.json.simple.JSONObject object = getAnswer(jsonObjectsList,
														documentQuestions.getPageNumber(),
														possible.getAnswerPosition());
												if (object.get("answer").toString().equals("Yes")) {
													if (possible.getPossibleAnswer().contains("No"))
														documentAnswers.setAnswer("No");
													else
														documentAnswers.setAnswer("Yes");
													// documentAnswers.setAnswer(possible.getPossibleAnswer());
													break iner;
												}
												/*
												 * if (object.get("answer").toString().equals("Yes")) {
												 * documentAnswers.setAnswer(object.get("answer").toString()); //break
												 * out; }
												 */
											}
										}
										// for radio buttons
										else if (documentQuestions.getAnswerType().toString()
												.equals("YES_NO_CHECKBOX")) {
											for (PossibleAnswers possible : possibleAnswers) {
												org.json.simple.JSONObject object = getAnswer(jsonObjectsList,
														documentQuestions.getPageNumber(),
														possible.getAnswerPosition());
												documentAnswers.setAnswer(possible.getPossibleAnswer());
											}
										}
									} // if close
									else// for multiple checkboxes
									{
										if (possibleAnswers.size() == 1) {
											for (PossibleAnswers possible : possibleAnswers) {
												org.json.simple.JSONObject object = getAnswer(jsonObjectsList,
														documentQuestions.getPageNumber(),
														possible.getAnswerPosition());
												documentAnswers.setAnswer(object.get("answer").toString());
											}
										} // if close
										else {
											out: for (PossibleAnswers possible : possibleAnswers) {
												org.json.simple.JSONObject object = getAnswer(jsonObjectsList,
														documentQuestions.getPageNumber(),
														possible.getAnswerPosition());
												if (object.get("answer").toString().equals("Yes")) {
													documentAnswers.setAnswer(possible.getPossibleAnswer());
													break out;
												}
											}
										} // else close
									} // else close
								} // size if close
							}

						} // new doc parser close if
							// old fillable pdf
						else {
							documentAnswers.setAnswer(map.get(documentQuestions.getPdfAnswerCode()));
						}

						// documentAnswers.setAnswer(map.get(documentQuestions.getPdfAnswerCode()));
						if (documentAnswers.getAnswer() != null) {
							if (documentAnswers.getAnswer().matches("Yes_.*")) {
								documentAnswers.setAnswer("Yes");
							}
							if (documentAnswers.getAnswer().matches("No_.*")
									|| (documentAnswers.getAnswer().equals("Off"))) {
								documentAnswers.setAnswer("No");
							}
						}
						documentAnswers.setModefiedBy(userId);
						documentAnswers.setDocumentQuestions(documentQuestions);

						// if (documentAnswers.getAnswer() != null && documentAnswers.getAnswer() != "")
						// {
						if (documentAnswers.getAnswer() != null && documentAnswers.getAnswer() != "") {
							documentAnswers.setActualAnswer(documentAnswers.getAnswer());
						} else {
							documentAnswers.setAnswer("");
							documentAnswers.setActualAnswer("");
						}
						answersList.add(documentAnswers);
						if (documentQuestions.isRequiredForOSINT()) {
							if (documentQuestions.getCaseField().equals("website")) {
								String REGEX = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
								if (documentAnswers.getActualAnswer().matches(REGEX))
									caseFields.put(documentQuestions.getCaseField(), documentAnswers.getAnswer());
								else
									caseFields.put(documentQuestions.getCaseField(), "");
							} else if (documentQuestions.getCaseField().equals("email")) {
								String REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
								if (documentAnswers.getActualAnswer().matches(REGEX))
									caseFields.put(documentQuestions.getCaseField(), documentAnswers.getAnswer());
								else
									caseFields.put(documentQuestions.getCaseField(), "");
							} else {
								caseFields.put(documentQuestions.getCaseField(), documentAnswers.getAnswer());
							}
						}
					}

				} // questions list for close
				count = answersList.stream().filter(answer -> answer.getAnswer().trim() != "")
						.filter(answer -> answer.getAnswer().trim().length() != 0).count();
				if (count <= 0)
					throw new InsufficientDataException("The document is empty");
				boolean isoCategoryStatus = saveIsoCategory(documentQuestionsList, docId1);
				if (!isoCategoryStatus)
					throw new InsufficientDataException("ISO categories are not saved");
			} finally {
				if (document != null)
					document.close();
			}

		} else// for scanned pdf
		{
			if (documentTemplates.isScannedPdf() || (!documentTemplates.isDocParserTemplate())) {
				PDDocument document = null;
				File[] val = null;
				File file = null;
				String fileNameWithoutExtension = "";
				String fileExtension = "";
				File someFile = null;
				try {
					document = new PDDocument();
					if (uploadFile != null) {
						String originalFile = new String(uploadFile.getOriginalFilename());
						fileExtension = FilenameUtils.getExtension(originalFile);
						fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
						file = File.createTempFile(fileNameWithoutExtension, fileExtension);
						uploadFile.transferTo(file);
						document = PDDocument.load(file);
					}
					if (dataStream != null)
						document = PDDocument.load(dataStream);
					/*
					 * else document = PDDocument.load(uploadFile.getBytes());
					 */
					someFile = new File(TESSRACT_IMAGE_ALLOCATION + "/scanned.pdf");
					FileOutputStream fos = new FileOutputStream(someFile);
					if (dataStream != null)
						fos.write(dataStream);
					else if (uploadFile != null)
						fos.write(uploadFile.getBytes());
					fos.flush();
					fos.close();
					val = PdfUtilities.convertPdf2Png(someFile);
					PDFRenderer pdfRenderer = new PDFRenderer(document);
					int pageCounter = 0;
					int iterator = 1;
					List<DocumentQuestions> documentQuestionsList = documentQuestionsDao
							.getDocumentQuestionsByTemplateId(documentTemplates.getId(), true);
					ITesseract tesseract = new Tesseract();
					tesseract.setDatapath(TESSRACT_LANGUAGE);

					for (PDPage page : document.getPages()) {
						BufferedImage imageProcessed = null;
						if (documentTemplates.isDocParserTemplate() == false)
							imageProcessed = pdfRenderer.renderImageWithDPI(pageCounter, 300,
									org.apache.pdfbox.rendering.ImageType.RGB);
						if (fileNameWithoutExtension.equals(""))
							fileNameWithoutExtension = "sample";
						String fileName = fileNameWithoutExtension + "-" + (pageCounter++);
						String pathToFile = TESSRACT_IMAGE_ALLOCATION + "/" + fileName + ".png";
						if (documentTemplates.isDocParserTemplate() == false)
							ImageIOUtil.writeImage(imageProcessed, pathToFile, 300);

						for (DocumentQuestions documentQuestions : documentQuestionsList) {
							if (documentQuestions.getContentType().equals("SUB_PARAGRAPH")
									|| documentQuestions.getContentType().equals("Question")) {
								int pageNUmber = 0;
								if (documentTemplates.isDocParserTemplate() == false) {
									pageNUmber = documentQuestions.getScannedPdfPageNumber();
								} else {
									pageNUmber = documentQuestions.getPageNumber();
								}
								if (pageNUmber == iterator) {
									DocumentAnswers documentAnswers = new DocumentAnswers();
									documentAnswers.setDocumetnId(docId1);

									if (documentQuestions.getAnswerType().toString().equals("CHECKBOX")) {
										String coordinatesFromJson = documentQuestions.getPdfAnswerCode();
										List<String> result = Arrays.asList(coordinatesFromJson.split("\\s*,\\s*"));
										Rectangle rectangle = null;
										if (documentTemplates.isDocParserTemplate() == false) {
											rectangle = new Rectangle(Integer.parseInt(result.get(0)),
													Integer.parseInt(result.get(1)), Integer.parseInt(result.get(2)),
													Integer.parseInt(result.get(3)));
										} else {
											Integer a = (Integer.parseInt(result.get(0)) * 300) / 72;
											Integer b = (Integer.parseInt(result.get(1)) * 300) / 72;
											Integer c = (Integer.parseInt(result.get(2)) * 300) / 72;
											Integer d = (Integer.parseInt(result.get(3)) * 300) / 72;
											rectangle = new Rectangle(a, b, c, d);
										}
										String text = "";
										if (documentTemplates.isDocParserTemplate() == false)
											text = tesseract.doOCR(imageProcessed, rectangle);
										else
											text = tesseract.doOCR(val[iterator - 1], rectangle);
										boolean variable = false;
										out: if (text != null) {
											if (documentTemplates.getScannedValue() == 1
													|| documentTemplates.isScannedPdf() == true) {
												if (text.contains("lYes") || text.contains("IYes")
														|| text.contains("“Yes") || text.contains("‘Yes")
														|| text.contains("Yes.")) {
													documentAnswers.setAnswer("yes");
													variable = true;
												}
												if (text.contains("Yes")) {
													Pattern p = Pattern.compile("^Yes *.");
													Matcher matcher = p.matcher(text);
													if (matcher.find()) {
														if (variable)
															break out;
														documentAnswers.setAnswer("yes");
													}
												}

												if (text.contains("No") || text.contains("N0")) {
													Pattern p = Pattern.compile(".*Yes No*.");
													Matcher matcher = p.matcher(text);
													if (matcher.find()) {
														documentAnswers.setAnswer("no");
													}
												}
											} // first scanned pdf if close
											else if (documentTemplates.getScannedValue() == 2
													|| documentTemplates.isScannedPdf() == true) {
												text = text.replace("\n", " ").replace("\r", "");
												text = text.trim();
												documentAnswers.setAnswer(getResponseYesOrNo(text));
											} // second scanned pdf if close
											/*
											 * else {
											 * 
											 * }
											 */
										} // text null close
									} // checkbox if close
									else {
										String coordinatesFromJson = "";
										if (documentTemplates.isDocParserTemplate() == true)
											coordinatesFromJson = documentQuestions.getAnswerPosition();
										else
											coordinatesFromJson = documentQuestions.getPdfAnswerCode();
										List<String> result = Arrays.asList(coordinatesFromJson.split("\\s*,\\s*"));
										Rectangle rectangle = null;
										if (documentTemplates.isDocParserTemplate() == false) {
											rectangle = new Rectangle(Integer.parseInt(result.get(0)),
													Integer.parseInt(result.get(1)), Integer.parseInt(result.get(2)),
													Integer.parseInt(result.get(3)));
										} else {
											Integer a = (Integer.parseInt(result.get(0)) * 300) / 72;
											Integer b = (Integer.parseInt(result.get(1)) * 300) / 72;
											Integer c = (Integer.parseInt(result.get(2)) * 300) / 72;
											Integer d = (Integer.parseInt(result.get(3)) * 300) / 72;
											rectangle = new Rectangle(a, b, c, d);
										}
										String text = "";
										if (documentTemplates.isDocParserTemplate() == false)
											text = tesseract.doOCR(imageProcessed, rectangle);
										else
											text = tesseract.doOCR(val[iterator - 1], rectangle);
										documentAnswers.setAnswer(text);
									}
									documentAnswers.setModefiedBy(userId);
									documentAnswers.setDocumentQuestions(documentQuestions);
									/*
									 * if (documentQuestions.isRequiredForOSINT())
									 * caseFields.put(documentQuestions.getCaseField(),
									 * documentAnswers.getAnswer());
									 */
									// if (documentAnswers.getAnswer() != null && documentAnswers.getAnswer() != "")
									// {
									if (documentAnswers.getAnswer() != null && documentAnswers.getAnswer() != "") {
										documentAnswers.setActualAnswer(documentAnswers.getAnswer());
									} else {
										documentAnswers.setAnswer("");
										documentAnswers.setActualAnswer("");
									}
									answersList.add(documentAnswers);
									if (documentQuestions.isRequiredForOSINT()) {
										if (documentQuestions.getCaseField().equals("website")) {
											String REGEX = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
											if (documentAnswers.getActualAnswer().matches(REGEX))
												caseFields.put(documentQuestions.getCaseField(),
														documentAnswers.getAnswer());
											else
												caseFields.put(documentQuestions.getCaseField(), "");
										} else if (documentQuestions.getCaseField().equals("email")) {
											String REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
											if (documentAnswers.getActualAnswer().matches(REGEX))
												caseFields.put(documentQuestions.getCaseField(),
														documentAnswers.getAnswer());
											else
												caseFields.put(documentQuestions.getCaseField(), "");
										} else {
											caseFields.put(documentQuestions.getCaseField(),
													documentAnswers.getAnswer());
										}
									}
									// }

								} // iterator if close
							} // sub paragraph close

						} // for close json array
						File old = new File(pathToFile);
						old.delete();
						iterator++;
					} // for close page
					count = answersList.stream().filter(answer -> answer.getAnswer().trim() != "")
							.filter(answer -> answer.getAnswer().trim().length() != 0).count();
					if (count <= 0)
						throw new InsufficientDataException("The document is empty");
					boolean isoCategoryStatus = saveIsoCategory(documentQuestionsList, docId1);
					if (!isoCategoryStatus)
						throw new InsufficientDataException("ISO categories are not saved");
				} catch (Exception e) {
					// ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
					if (count <= 0)
						throw new InsufficientDataException("The document is empty");
					else
						throw new InsufficientDataException("Document answers not saved into database.");
				} finally {
					if (document != null)
						document.close();
					if (val != null) {
						for (File fileToDelete : val) {
							fileToDelete.delete();
						}
					}
					if (someFile != null)
						someFile.delete();
				}
			}
			// for normal pdf
			else {
				/*
				 * PDDocument document = null; List<DocumentQuestions> documentQuestionsList =
				 * documentQuestionsDao.getDocumentQuestionsByTemplateId(documentTemplates.getId
				 * (), true); if (dataStream != null && dataStream.length > 0) document =
				 * PDDocument.load(dataStream); else document =
				 * PDDocument.load(uploadFile.getInputStream()); for (DocumentQuestions
				 * docQuestion : documentQuestionsList) {
				 * if(docQuestion.getContentType().equals("Question")) { DocumentAnswers
				 * documentAnswers = new DocumentAnswers();
				 * documentAnswers.setDocumetnId(docId1); List<String> items = null;
				 * if(docQuestion.getAnswerPosition()!=null &&
				 * !docQuestion.getAnswerPosition().equals("")) items =
				 * Arrays.asList(docQuestion.getAnswerPosition().split("\\s*,\\s*"));
				 * PDFTextStripperByArea textStripper = new PDFTextStripperByArea(); Rectangle2D
				 * rect = new
				 * java.awt.geom.Rectangle2D.Float(Float.valueOf(items.get(0)),Float.valueOf(
				 * items.get(1)),Float.valueOf(items.get(2)),Float.valueOf(items.get(3)));
				 * textStripper.addRegion("region", rect); PDPage docPage =
				 * document.getPage(docQuestion.getPageNumber());
				 * textStripper.extractRegions(docPage);
				 * 
				 * if(docQuestion.getAnswerType().toString().equals("CHECKBOX")) { String text =
				 * textStripper.getTextForRegion("region"); boolean variable = false; out: if
				 * (text != null) { if (text.contains("lYes") || text.contains("IYes") ||
				 * text.contains("“Yes") || text.contains("‘Yes") || text.contains("Yes.")) {
				 * documentAnswers.setAnswer("yes"); variable = true; } if
				 * (text.contains("Yes")) { Pattern p = Pattern.compile("^Yes *."); Matcher
				 * matcher = p.matcher(text); if (matcher.find()) { if (variable) break out;
				 * documentAnswers.setAnswer("yes"); } }
				 * 
				 * if (text.contains("No") || text.contains("N0")) { Pattern p =
				 * Pattern.compile(".*Yes No*."); Matcher matcher = p.matcher(text); if
				 * (matcher.find()) { documentAnswers.setAnswer("no"); } } } // text null close
				 * } else { documentAnswers.setAnswer(textStripper.getTextForRegion("region"));
				 * } documentAnswers.setModefiedBy(userId);
				 * documentAnswers.setDocumentQuestions(docQuestion); if
				 * (documentAnswers.getAnswer() != null && documentAnswers.getAnswer() != "") {
				 * documentAnswers.setActualAnswer(documentAnswers.getAnswer()); } else {
				 * documentAnswers.setAnswer(""); documentAnswers.setActualAnswer(""); }
				 * answersList.add(documentAnswers); if (docQuestion.isRequiredForOSINT()) { if
				 * (docQuestion.getCaseField().equals("website")) { String REGEX =
				 * "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
				 * if (documentAnswers.getActualAnswer().matches(REGEX))
				 * caseFields.put(docQuestion.getCaseField(), documentAnswers.getAnswer()); else
				 * caseFields.put(docQuestion.getCaseField(), ""); } else if
				 * (docQuestion.getCaseField().equals("email")) { String REGEX =
				 * "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"; if
				 * (documentAnswers.getActualAnswer().matches(REGEX))
				 * caseFields.put(docQuestion.getCaseField(), documentAnswers.getAnswer()); else
				 * caseFields.put(docQuestion.getCaseField(), ""); } else {
				 * caseFields.put(docQuestion.getCaseField(), documentAnswers.getAnswer()); } }
				 * }//question close }
				 * 
				 */}
		} // close else
		caseMapDocIdDto.setCaseFields(caseFields);
		caseMapDocIdDto.setDocumentAnswers(answersList);

		return caseMapDocIdDto;
	}

	@Transactional("transactionManager")
	public Boolean saveTemplate(MultipartFile uploadedFile, Long templateId) throws JSONException, Exception {
		try {
			long parentId = 0;
			String originalFile = new String(uploadedFile.getOriginalFilename());
			String ext = FilenameUtils.getExtension(originalFile);
			String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
			File file = null;

			file = File.createTempFile(fileNameWithoutExtension, ext);
			uploadedFile.transferTo(file);

			JSONParser parser = new JSONParser();
			Object object = parser.parse(new FileReader(file));
			JSONObject jsonObject = new JSONObject(object.toString());
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			for (int j = 0; j < jsonArray.length(); j++) {
				JSONObject jsonObject1 = jsonArray.getJSONObject(j);
				JSONObject content = jsonObject1.getJSONObject("content");
				if (jsonObject1.get("contentType").toString().equalsIgnoreCase("SUB_PARAGRAPH")) {
					DocumentQuestions documentQuestion = new DocumentQuestions();
					documentQuestion.setContentType(jsonObject1.get("contentType").toString());
					documentQuestion.setName(content.get("name").toString());
					documentQuestion.setQuestionStatus(content.getBoolean("question"));
					if (parentId != 0) {
						documentQuestion.setHasParentId(parentId);
						parentId = 0;
					}
					if (content.has("possibleAnswers"))
						documentQuestion.setPossibleAnswer(content.getString("possibleAnswers"));
					// System.out.println("name : "+documentQuestion.getName());
					if (content.get("answerType").toString().equalsIgnoreCase("CHECKBOX")) {
						documentQuestion.setAnswerType(AnswerType.CHECKBOX);
						if (content.has("splitWith"))
							documentQuestion.setSplitWith(content.get("splitWith").toString());
						if (content.has("answerAtPosition"))
							documentQuestion.setAnswerAtPosition(content.get("answerAtPosition").toString());
						if (content.has("answerPosition"))
							documentQuestion.setComponentAnswerMatrix(content.getString("answerPosition"));
						if (content.has("answerTableId"))
							documentQuestion.setAnswerTableId(content.getInt("answerTableId"));
						if (content.has("questionPosition"))
							documentQuestion.setComponentQuestionMatrix(content.getString("questionPosition"));

					}
					if (content.get("answerType").toString().equalsIgnoreCase("TEXT")) {
						documentQuestion.setAnswerType(AnswerType.TEXT);
						if (content.has("answerTableId"))
							documentQuestion.setAnswerTableId(content.getInt("answerTableId"));
						if (content.has("answerPosition"))
							documentQuestion.setComponentAnswerMatrix(content.getString("answerPosition"));
						if (content.has("splitWith"))
							documentQuestion.setSplitWith(content.getString("splitWith"));
					}
					if (content.get("answerType").toString().equalsIgnoreCase("TABLE")) {
						documentQuestion.setAnswerType(AnswerType.TABLE);
						if (content.has("answerTableId"))
							documentQuestion.setAnswerTableId(content.getInt("answerTableId"));
					}
					if (content.has("requiredForOSINT"))
						documentQuestion.setRequiredForOSINT(content.getBoolean("requiredForOSINT"));
					if (content.has("caseField"))
						documentQuestion.setCaseField(content.getString("caseField"));
					String response = getIsoCodesJson(content.get("name").toString());// primary
					// and
					// secondary
					if (response != null) {
						JSONObject jsonResponseObjectNew = new JSONObject(response);
						JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
						if (arrayObjectNew.length() > 0) {
							JSONObject normalObject = arrayObjectNew.getJSONObject(0);
							if (normalObject.has("controls")) {
								if (normalObject.getJSONObject("controls").has("level3")) {
									JSONObject level3Object = normalObject.getJSONObject("controls")
											.getJSONObject("level3");
									if (level3Object.has("code")) {
										documentQuestion.setPrimaryIsoCode(level3Object.getString("code"));
										documentQuestion.setHasIsoCode(true);
									}
									if (level3Object.has("title"))
										documentQuestion.setLevel3Title(level3Object.getString("title"));
									if (level3Object.has("title"))
										documentQuestion.setLevel3Description(level3Object.getString("title"));
								}
								// documentQuestion.setPrimaryIsoCode(getPrimaryIsoCode(response));
								if (normalObject.getJSONObject("controls").has("level1")) {
									JSONObject level1Object = normalObject.getJSONObject("controls")
											.getJSONObject("level1");
									if (level1Object.has("code"))
										documentQuestion.setLevel1Code(level1Object.getString("code"));
									if (level1Object.has("title"))
										documentQuestion.setLevel1Title(level1Object.getString("title"));
								}
								documentQuestion.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
								if (normalObject.getJSONObject("controls").has("topic"))
									documentQuestion
											.setTopic(normalObject.getJSONObject("controls").getString("topic"));
							}
						} /*
							 * else { documentQuestion.setPrimaryIsoCode(getRandomISOCODE());
							 * documentQuestion.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString
							 * ()); }
							 */
					}
					if (documentQuestion.getPrimaryIsoCode() == null)
						documentQuestion.setHasIsoCode(false);
					documentQuestion.setTemplateId(templateId);
					documentQuestion.setShowInUI(true);
					DocumentQuestions savedQuestion = documentsQuestionsService.save(documentQuestion);
					if (content.has("hasChild"))
						parentId = savedQuestion.getId();
				}
				if (jsonObject1.get("contentType").toString().equalsIgnoreCase("TABLE")) {
					// JSONObject tabledata = jsonObject1.getJSONObject("content");
					long parentQuestionId = 0;
					JSONArray tableRows = content.getJSONArray("rows");
					for (int k = 0; k < tableRows.length(); k++) {
						JSONObject tablerow = tableRows.getJSONObject(k);
						JSONArray tableRowColums = tablerow.getJSONArray("colums");
						for (int m = 0; m < tableRowColums.length(); m++) {
							JSONObject tableRowColumn = tableRowColums.getJSONObject(m);
							DocumentQuestions documentQuestions = new DocumentQuestions();
							if (parentQuestionId != 0) {
								documentQuestions.setHasParentId(parentQuestionId);
								parentQuestionId = 0;
							}
							documentQuestions.setTemplateId(templateId);
							documentQuestions.setTableID((int) jsonObject1.get("id"));
							documentQuestions.setShowInUI(true);
							documentQuestions.setName(tableRowColumn.get("columName").toString());
							documentQuestions.setQuestionStatus(tableRowColumn.getBoolean("question"));
							// System.out.println("name : "+documentQuestions.getName());
							documentQuestions.setComponentAnswerMatrix(tableRowColumn.get("answerPosition").toString());
							if (tableRowColumn.has("possibleAnswers"))
								documentQuestions.setPossibleAnswer(tableRowColumn.getString("possibleAnswers"));
							if (tableRowColumn.get("answerType").toString().equalsIgnoreCase("TEXT"))
								documentQuestions.setAnswerType(AnswerType.TEXT);
							if (tableRowColumn.get("answerType").toString().equalsIgnoreCase("CHECKBOX"))
								documentQuestions.setAnswerType(AnswerType.CHECKBOX);
							if (tableRowColumn.has("requiredForOSINT"))
								documentQuestions.setRequiredForOSINT(tableRowColumn.getBoolean("requiredForOSINT"));
							if (tableRowColumn.has("caseField"))
								documentQuestions.setCaseField(tableRowColumn.getString("caseField"));
							if (tableRowColumn.has("splitWith"))
								documentQuestions.setSplitWith(tableRowColumn.getString("splitWith"));
							if (tableRowColumn.has("answerAtPosition"))
								documentQuestions
										.setAnswerAtPosition(tableRowColumn.get("answerAtPosition").toString());

							String response = getIsoCodesJson(documentQuestions.getName());// primary
							// and
							// secondary
							if (response != null) {
								JSONObject jsonResponseObjectNew = new JSONObject(response);
								JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
								if (arrayObjectNew.length() > 0) {
									JSONObject normalObject = arrayObjectNew.getJSONObject(0);
									JSONObject level3Object = normalObject.getJSONObject("controls")
											.getJSONObject("level3");
									documentQuestions.setPrimaryIsoCode(level3Object.getString("code"));
									documentQuestions.setLevel3Title(level3Object.getString("title"));
									documentQuestions.setLevel3Description(level3Object.getString("title"));
									JSONObject level1Object = normalObject.getJSONObject("controls")
											.getJSONObject("level1");
									documentQuestions.setLevel1Code(level1Object.getString("code"));
									documentQuestions.setLevel1Title(level1Object.getString("title"));
									// documentQuestions.setPrimaryIsoCode(getPrimaryIsoCode(response));
									documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
									// documentQuestions.setTopic(getTopic(response));
									documentQuestions
											.setTopic(normalObject.getJSONObject("controls").getString("topic"));
								} /*
									 * else { documentQuestions.setPrimaryIsoCode(getRandomISOCODE());
									 * documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).
									 * toString()); }
									 */
							}
							documentQuestions
									.setComponentQuestionMatrix(tableRowColumn.get("questionPosition").toString());
							documentQuestions.setContentType(tableRowColumn.get("type").toString());
							DocumentQuestions documentTableQuestions = documentsQuestionsService
									.save(documentQuestions);
							if (tableRowColumn.has("hasChild"))
								parentQuestionId = documentTableQuestions.getId();

						}
					}

				}

			}
			// control question
			if (jsonArray.length() > 0) {
				List<DocumentQuestions> controlQuestion = documentsQuestionsService.getListOfControlQuestion();
				for (DocumentQuestions documentQuestions2 : controlQuestion) {
					documentQuestions2.setTemplateId(templateId);
					documentsQuestionsService.save(documentQuestions2);
				}
			}
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	private String getEvaluation(String question, String answer, String isoCode, String orgId, boolean osint)
			throws Exception {
		JSONObject jsonObject = new JSONObject();
		String url = ISOCODE_QUESTION_URL;
		if (osint) {
			jsonObject.put("org_id", orgId);
			jsonObject.put("session_id", UUID.randomUUID());
			jsonObject.put("ISO_code", isoCode);
			url = url + "osint/evaluate";
		} else {
			jsonObject.put("org_id", orgId);
			jsonObject.put("session_id", UUID.randomUUID());
			jsonObject.put("question", question);
			jsonObject.put("answer", answer);
			jsonObject.put("ISO_code", isoCode);
			url = url + "questionnaire/evaluate";
		}

		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonObject.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	@SuppressWarnings("resource")
	@Override
	@Transactional("transactionManager")
	public CaseMapDocIdDto saveDocumetnAnswersCsv(MultipartFile uploadFile, Long userId, long docId, long templateId)
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		Map<String, String> caseFields = new HashMap<>();
		CaseMapDocIdDto caseMapDocIdDto = new CaseMapDocIdDto();
		List<DocumentAnswers> answersList = new ArrayList<DocumentAnswers>();
		// List<Answer> answers=caseService.readDataFromCsvFile(uploadFile);
		String originalFile = new String(uploadFile.getOriginalFilename());
		String ext = FilenameUtils.getExtension(originalFile);
		String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
		File file = null;
		file = File.createTempFile(fileNameWithoutExtension, ext);
		uploadFile.transferTo(file);
		long count = 0;
		try (FileReader reader = new FileReader(file);) {
			Map<String, String> values = new CSVReaderHeaderAware(reader).readMap();
			List<DocumentQuestions> documentQuestionList = documentQuestionsDao
					.getDocumentQuestionsByTemplateId(templateId, false);
			for (DocumentQuestions documentQuestion : documentQuestionList) {
				DocumentAnswers documentAnswer = new DocumentAnswers();
				documentAnswer.setAnswer(values.get(documentQuestion.getName()));
				documentAnswer.setDocumentQuestions(documentQuestion);
				documentAnswer.setDocumetnId(docId);
				documentAnswer.setModefiedBy(userId);
				/*
				 * String evaluation = getEvaluation(documentQuestion.getName(),
				 * documentAnswer.getAnswer(), documentQuestion.getPrimaryIsoCode()); if
				 * (evaluation != null) { JSONObject evaluationObject = new
				 * JSONObject(evaluation);
				 * if(!evaluationObject.getString("evaluation").equalsIgnoreCase("error"))
				 * documentAnswer.setEvaluation(evaluationObject.getString("evaluation"));
				 * 
				 * }
				 */
				// documentAnswersService.save(documentAnswer);
				if (documentAnswer.getAnswer() != null && documentAnswer.getAnswer() != "") {
					documentAnswer.setActualAnswer(documentAnswer.getAnswer());
				} else {
					documentAnswer.setAnswer("");
					documentAnswer.setActualAnswer("");
				}
				answersList.add(documentAnswer);
				if (documentQuestion.isRequiredForOSINT())
					caseFields.put(documentQuestion.getCaseField(), documentAnswer.getAnswer());

			}
			count = answersList.stream().filter(answer -> answer.getAnswer().trim() != "")
					.filter(answer -> answer.getAnswer().trim().length() != 0).count();
			if (count <= 0)
				throw new InsufficientDataException("The document is empty");

			boolean isoCategoryStatus = saveIsoCategory(documentQuestionList, docId);
			if (!isoCategoryStatus)
				throw new InsufficientDataException("ISO categories are not saved");
			caseMapDocIdDto.setCaseFields(caseFields);
			caseMapDocIdDto.setDocumentAnswers(answersList);
		} catch (Exception e) {
			// ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			if (count <= 0)
				throw new InsufficientDataException("The document is empty");
			else
				throw new InsufficientDataException("Document answers not saved into database.");
		}
		return caseMapDocIdDto;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public void setEntityToAnswers(List<DocumentAnswers> answersList, String entityId) throws Exception {
		try {
			long templateId = 0;
			long docId = 0;
			long tempUserId = 0;
			List<DocumentAnswers> savedAnswers = new ArrayList<DocumentAnswers>();
			if (answersList.size() != 0 && entityId != null) {
				for (DocumentAnswers documentAnswer : answersList) {
					if (entityId != null && entityId.length() != 0)
						documentAnswer.setEntityId(entityId);
					DocumentQuestions documentQuestions = documentQuestionsDao
							.find(documentAnswer.getDocumentQuestions().getId());
					templateId = documentQuestions.getTemplateId();
					docId = documentAnswer.getDocumetnId();
					tempUserId = documentAnswer.getModefiedBy();
					if (documentQuestions != null) {
						if (documentAnswer.getAnswer() != "") {
							String evaluation = getEvaluation(documentQuestions.getName(), documentAnswer.getAnswer(),
									documentQuestions.getPrimaryIsoCode(), entityId, false);
							if (evaluation != null) {
								JSONObject evaluationObject = new JSONObject(evaluation);
								if (!evaluationObject.getString("evaluation").equalsIgnoreCase("error"))
									documentAnswer.setEvaluation(evaluationObject.getString("evaluation"));
								if (evaluationObject.has("ranking"))
									documentAnswer.setAnswerRank(evaluationObject.getInt("ranking"));

							}
						} else {
							documentAnswer.setEvaluation("no_info");
							documentAnswer.setAnswerRank(3);

						}
						if (documentAnswer.getEvaluation() == null) {
							documentAnswer.setEvaluation("no_info");
							documentAnswer.setAnswerRank(3);
						}

						DocumentAnswers savedAnswer = documentAnswersService.save(documentAnswer);
						savedAnswers.add(savedAnswer);

					} else {
						throw new InsufficientDataException("Documetn question not found.");
					}

				}
				List<IsoCategory> isoCategoryList = isoCategoryDao.getISOCategoriesByDocId(docId);
				Case caseSeed = caseDao.getCaseByDocId(docId);
				if (caseSeed != null) {
					JSONObject finalObject = new JSONObject();
					JSONObject caseId = new JSONObject();
					JSONArray evaluationArray = new JSONArray();
					JSONArray finalArray = new JSONArray();
					for (DocumentAnswers savedAnswer1 : savedAnswers) {
						JSONObject evaluation = new JSONObject();
						if (savedAnswer1.getDocumentQuestions().getPrimaryIsoCode() != null) {
							evaluation.put("evaluation", savedAnswer1.getEvaluation());
							evaluation.put("ISO_code", savedAnswer1.getDocumentQuestions().getPrimaryIsoCode());
							evaluation.put("ranking", savedAnswer1.getAnswerRank().toString());
							evaluationArray.put(evaluation);

						}

					}
					finalObject.put("case_id", caseSeed.getCaseId().toString());
					finalObject.put("entity_id", caseSeed.getEntityId());
					finalObject.put("evaluations", evaluationArray);
					if (caseSeed.getIndustry() != null)
						finalObject.put("industry", caseSeed.getIndustry());
					else
						finalObject.put("industry", "");
					finalArray.put(finalObject);
					String response = getisoCategory(finalArray.toString());
					if (response != null) {
						JSONObject responseCode = new JSONObject(response);
						if (responseCode.has("data")) {
							String data = responseCode.getString("data");
							// String dataReplace = data.replace("\\","");

							JSONArray jsonArray = new JSONArray(data);
							JSONObject isoCodes = jsonArray.getJSONObject(0);
							if (isoCodes.has("evaluation")) {
								JSONArray isoEvaluationArray = isoCodes.getJSONArray("evaluation");
								for (int j = 0; j < isoEvaluationArray.length(); j++) {
									JSONObject isoEvaluation = isoEvaluationArray.getJSONObject(j);
									out: for (IsoCategory isoCategory : isoCategoryList) {
										if (isoCategory.getLevel1Code()
												.equals(isoEvaluation.getString("L1_ISO_code"))) {
											isoCategory.setEvaluation(isoEvaluation.getString("evaluation"));
											isoCategoryDao.saveOrUpdate(isoCategory);
											break out;
										}
									}
								}
							}
						}
					}

				}
				for (IsoCategory isoCategory : isoCategoryList) {
					String evaluation = getIsoCategoryEvaluation(isoCategory.getLevel1Code(), entityId,
							isoCategory.isOSINT());
					if (evaluation != null) {
						JSONObject evaluationObject = new JSONObject(evaluation);
						if (evaluationObject.has("ranking"))
							isoCategory.setRank(evaluationObject.getInt("ranking"));
					}
					isoCategoryDao.saveOrUpdate(isoCategory);

				}

				isoCategoryService.storeIsoCategoryEvaluation(docId);

				if (templateId != 0) {
					List<DocumentQuestions> documentQuestionsList = documentQuestionsDao
							.getOSINTQuestionsByTemplateId(templateId);
					for (DocumentQuestions documentQuestions2 : documentQuestionsList) {
						DocumentAnswers documentAnswersOsInt = new DocumentAnswers();
						String evaluation = getEvaluation(documentQuestions2.getName(), null,
								documentQuestions2.getPrimaryIsoCode(), entityId, true);
						if (evaluation != null) {
							JSONObject evaluationObject = new JSONObject(evaluation);
							if (!evaluationObject.getString("evaluation").equalsIgnoreCase("error"))
								documentAnswersOsInt.setEvaluation(evaluationObject.getString("evaluation"));
							documentAnswersOsInt.setAnswerRank(evaluationObject.getInt("ranking"));
							if (docId != 0)
								documentAnswersOsInt.setDocumetnId(docId);
							documentAnswersOsInt.setDocumentQuestions(documentQuestions2);
							if (tempUserId != 0)
								documentAnswersOsInt.setModefiedBy(tempUserId);
							documentAnswersOsInt.setEntityId(entityId);
							Object object = evaluationObject.get("answer");
							if (documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.1.3")) {
								if (object instanceof JSONArray) {
									JSONArray osIntAnswer = evaluationObject.getJSONArray("answer");

									for (int i = 0; i < osIntAnswer.length(); i++) {
										JSONObject answer = osIntAnswer.getJSONObject(i);
										if (answer.has("total-count"))
											documentAnswersOsInt.setAnswer(answer.get("total-count").toString());
										DocumentAnswers savedAnswer = new DocumentAnswers();
										if (documentAnswersOsInt.getAnswer() != null) {
											documentAnswersOsInt.setActualAnswer(documentAnswersOsInt.getAnswer());
											savedAnswer = documentAnswersDao.create(documentAnswersOsInt);
										}
										JSONArray topEvents = answer.getJSONArray("top-events");
										for (int j = 0; j < topEvents.length(); j++) {
											AnswerTopEvents answerTopEvents = new AnswerTopEvents();
											if (savedAnswer != null)
												answerTopEvents.setDocumentAnswer(savedAnswer);
											JSONObject topEvent = topEvents.getJSONObject(j);
											if (topEvent.has("title"))
												answerTopEvents.setTitle(topEvent.getString("title"));
											if (topEvent.has("url"))
												answerTopEvents.setUrl(topEvent.getString("url"));
											if (topEvent.has("text"))
												answerTopEvents.setText(topEvent.getString("text"));
											if (topEvent.has("location")) {
												JSONObject location = (JSONObject) topEvent.get("location");
												if (location.has("name")) {
													answerTopEvents.setLocationName(location.getString("name"));
												}
											}
											if (topEvent.has("technologies")) {
												JSONArray technologies = topEvent.getJSONArray("technologies");
												String tech = null;

												for (int k = 0; k < technologies.length(); k++) {
													if (technologies.get(k) instanceof JSONObject) {
														JSONObject technologiesObj = technologies.getJSONObject(k);
														if (technologiesObj.has("type")) {
															if (k == 0)
																tech = tech + technologiesObj.getString("type");
															else
																tech = tech + "," + technologiesObj.getString("type");
														}
													} else {

														if (k == 0)
															tech = tech + technologies.getString(k);
														else
															tech = tech + "," + technologies.getString(k);
													}
												}
												answerTopEvents.setTechnologies(tech);
											}
											if (topEvent.has("published")) {
												DateFormat format = new SimpleDateFormat(
														"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
												/*
												 * DateTimeFormatter formatter = DateTimeFormatter
												 * .ofPattern("yyyy-MM-dd HH:mm:ss"); formatter.format("") String
												 * published = topEvent.getString("published"); String[] dateTime1 =
												 * published.split("T"); String time = dateTime1[0] + " " +
												 * dateTime1[1].substring(0, (dateTime1[1].length() - 5)); LocalDateTime
												 * dateTime = LocalDateTime.parse(time, formatter);
												 */
												Date dateIn = format.parse(topEvent.getString("published"));
												answerTopEvents.setPublished(dateIn);
											}

											answerTopEventsDao.create(answerTopEvents);
										}

									}
								}
							}
							if (documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.2.1")) {

								if (object instanceof JSONArray) {
									JSONArray osIntAnswer = evaluationObject.getJSONArray("answer");
									String answer = null;
									for (int i = 0; i < osIntAnswer.length(); i++) {
										if (i == 0)
											answer = osIntAnswer.getString(i);
										else
											answer = answer + "," + osIntAnswer.getString(i);

									}
									documentAnswersOsInt.setAnswer(answer);
								}
								if (documentAnswersOsInt.getAnswer() != null) {
									documentAnswersOsInt.setActualAnswer(documentAnswersOsInt.getAnswer());
									documentAnswersDao.create(documentAnswersOsInt);
								}
							}
							if (documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.2.2")) {

								if (object instanceof JSONArray) {
									JSONArray osIntAnswer = evaluationObject.getJSONArray("answer");
									String answer = null;
									for (int i = 0; i < osIntAnswer.length(); i++) {
										JSONObject answerdata = osIntAnswer.getJSONObject(i);
										if (answerdata.has("label")) {
											if (i == 0)
												answer = answerdata.getString("label");
											else
												answer = answer + "," + answerdata.getString("label");
										}
									}
									documentAnswersOsInt.setAnswer(answer);
								}
								if (documentAnswersOsInt.getAnswer() != null) {
									documentAnswersOsInt.setActualAnswer(documentAnswersOsInt.getAnswer());
									documentAnswersDao.create(documentAnswersOsInt);
								}
							}
							if (documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.2.3")) {

								if (object instanceof JSONArray) {
									JSONArray osIntAnswer = evaluationObject.getJSONArray("answer");
									String finalAnswer = null;
									for (int i = 0; i < osIntAnswer.length(); i++) {
										JSONObject answerdata = osIntAnswer.getJSONObject(i);
										if (answerdata.has("bst:category")) {
											String answer = null;
											JSONArray category = answerdata.getJSONArray("bst:category");
											for (int j = 0; j < category.length(); j++) {
												if (j == 0)
													answer = category.getString(j);
												else
													answer = answer + "," + category.getString(j);
											}
											if (answer != null) {
												if (i == 0)
													finalAnswer = answer;
												else
													finalAnswer = finalAnswer + "," + answer;
											}
										}
									}
									documentAnswersOsInt.setAnswer(finalAnswer);
								}
								if (documentAnswersOsInt.getAnswer() != null) {
									documentAnswersOsInt.setActualAnswer(documentAnswersOsInt.getAnswer());
									documentAnswersDao.create(documentAnswersOsInt);
								}
							}
							if (documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.3.1")
									|| documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.3.2")
									|| documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.3.3")
									|| documentQuestions2.getPrimaryIsoCode().equalsIgnoreCase("BST.3.4")) {

								if (object instanceof JSONObject) {
									JSONObject answerObject = evaluationObject.getJSONObject("answer");
									if (answerObject != null) {
										if (answerObject.has("exposure-class"))
											documentAnswersOsInt.setAnswer(answerObject.getString("exposure-class"));
										if (answerObject.has("source-urls")) {
											JSONArray sourceurl = answerObject.getJSONArray("source-urls");
											String sourceUrls = null;
											for (int i = 0; i < sourceurl.length(); i++) {
												if (i == 0)
													sourceUrls = sourceurl.getString(i);
												else
													sourceUrls = sourceUrls + "," + sourceurl.getString(i);

											}
											if (sourceUrls != null)
												documentAnswersOsInt.setSourceUrls(sourceUrls);
										}

									}
								}
								if (documentAnswersOsInt.getAnswer() != null) {
									documentAnswersOsInt.setActualAnswer(documentAnswersOsInt.getAnswer());
									documentAnswersDao.create(documentAnswersOsInt);
								}
							}
						}

					}
				}

			} else {
				throw new InsufficientDataException("Documetn answers or entityId is empty.");
			}
		} catch (Exception e) {
			// ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

	}
	// using for BST control questions
	/*
	 * public List<DocumentQuestions> controlQuestion() { List<DocumentQuestions>
	 * documentQuestionsList = new ArrayList<DocumentQuestions>(); for (int i = 0; i
	 * < 4; i++) { DocumentQuestions documentQuestion = new DocumentQuestions();
	 * documentQuestion.setContentType("SUB_PARAGRAPH");
	 * documentQuestion.setLevel1Code("BST.1");
	 * documentQuestion.setLevel3Title("Adverse cyber events");
	 * documentQuestion.setLevel3Description(
	 * "Data from news monitoring collecting cyber events in the domain relevant to company activities:(location,industry,technology)"
	 * ); documentQuestion.setOSINT(true);
	 * documentQuestion.setAnswerType(AnswerType.TEXT);
	 * documentQuestion.setQuestionStatus(true);
	 * documentQuestion.setShowInUI(false);
	 * documentQuestion.setLevel1Title("Adverse cyber events");
	 * 
	 * if (i == 0) { documentQuestion.setPrimaryIsoCode("BST.1.1");
	 * documentQuestion.setName("Was company involved in cyber incidents");
	 * documentQuestionsList.add(documentQuestion); } if (i == 1) {
	 * documentQuestion.setPrimaryIsoCode("BST.1.2"); documentQuestion
	 * .setName("How many attacks happened on the companies in the same industry in past 6 months"
	 * ); documentQuestionsList.add(documentQuestion); } if (i == 2) {
	 * documentQuestion.setPrimaryIsoCode("BST.1.3");
	 * documentQuestion.setShowInUI(true); documentQuestion
	 * .setName("How many attacks happened on the companies in the same location in past 6 months"
	 * ); documentQuestionsList.add(documentQuestion); } if (i == 3) {
	 * documentQuestion.setPrimaryIsoCode("BST.1.4"); documentQuestion.
	 * setName("How many attacks happened on the companies using same tech in past 6 months"
	 * ); documentQuestionsList.add(documentQuestion); }
	 * 
	 * } for (int j = 0; j < 3; j++) { DocumentQuestions documentQuestion = new
	 * DocumentQuestions(); documentQuestion.setContentType("SUB_PARAGRAPH");
	 * documentQuestion.setLevel1Code("BST.2");
	 * documentQuestion.setLevel3Title("Company profile");
	 * documentQuestion.setLevel3Description(
	 * "Provide information about elements in company profile which expose it to cyber threats(technology,industry,location)"
	 * ); documentQuestion.setOSINT(true);
	 * documentQuestion.setAnswerType(AnswerType.TEXT);
	 * documentQuestion.setQuestionStatus(true); documentQuestion.setShowInUI(true);
	 * documentQuestion.setLevel1Title("Company profile"); if (j == 0) {
	 * documentQuestion.setPrimaryIsoCode("BST.2.1"); documentQuestion.
	 * setName("Is company operating in location with high cyber risk");
	 * documentQuestionsList.add(documentQuestion); } if (j == 1) {
	 * documentQuestion.setPrimaryIsoCode("BST.2.2"); documentQuestion.
	 * setName("Is company operating in industry with high cyber risk");
	 * documentQuestionsList.add(documentQuestion); } if (j == 2) {
	 * documentQuestion.setPrimaryIsoCode("BST.2.3");
	 * documentQuestion.setName("Is company using technology with high cyber risk");
	 * documentQuestionsList.add(documentQuestion); }
	 * 
	 * } for (int k = 0; k < 4; k++) { DocumentQuestions documentQuestion = new
	 * DocumentQuestions(); documentQuestion.setContentType("SUB_PARAGRAPH");
	 * documentQuestion.setLevel1Code("BST.3");
	 * documentQuestion.setLevel3Title("Company BI exposures");
	 * documentQuestion.setLevel3Description(
	 * "Given company's online footprint,what are the potential points of business interruption"
	 * ); documentQuestion.setOSINT(true);
	 * documentQuestion.setAnswerType(AnswerType.TEXT);
	 * documentQuestion.setQuestionStatus(true); documentQuestion.setShowInUI(true);
	 * documentQuestion.setLevel1Title("Company BI exposures"); if (k == 0) {
	 * documentQuestion.setPrimaryIsoCode("BST.3.1");
	 * documentQuestion.setName("Is company collecting PII");
	 * documentQuestionsList.add(documentQuestion); } if (k == 1) {
	 * documentQuestion.setPrimaryIsoCode("BST.3.2");
	 * documentQuestion.setName("Is company running e-shop");
	 * documentQuestionsList.add(documentQuestion); } if (k == 2) {
	 * documentQuestion.setPrimaryIsoCode("BST.3.3");
	 * documentQuestion.setName("Is company publishing content");
	 * documentQuestionsList.add(documentQuestion); } if (k == 3) {
	 * documentQuestion.setPrimaryIsoCode("BST.3.4");
	 * documentQuestion.setName("Is company allowing third party content(comments)"
	 * ); documentQuestionsList.add(documentQuestion); }
	 * 
	 * } return documentQuestionsList;
	 * 
	 * }
	 */

	public String getResponseYesOrNo(String response) {
		List<String> yesResponseList = new ArrayList<String>();
		yesResponseList.add("Yes RH No 5:1");
		yesResponseList.add("Yes 52 N0 {Ti");
		yesResponseList.add("Yes E7? km; N0 D");
		yesResponseList.add("Yes 521 NOD");
		yesResponseList.add("Year“ m NOD");
		yesResponseList.add("Yes r” xii N0 ﬂ");
		yesResponseList.add("Yes if} NOE!");
		yesResponseList.add("Yes “E No[");
		yesResponseList.add("Yes [njj No [3");
		yesResponseList.add("Yes {2} No Lt]");
		yesResponseList.add("Yes @ NM;1");
		yesResponseList.add("Yes @ NOE}");
		yesResponseList.add("Yes M No {3");
		yesResponseList.add("Yes [vﬂ NOD");
		yesResponseList.add("Yes ﬁ git No D");
		yesResponseList.add("Yes 52} No D");
		yesResponseList.add("Yes f\"" + " K! N0 5]");
		yesResponseList.add("Yes [4] No [I");
		yesResponseList.add("Yes [a No D");
		yesResponseList.add("Yes [IE N0 {1}");
		yesResponseList.add("Yes r“ v1: Na K]");
		yesResponseList.add("Yes 515 N0 L]");
		yesResponseList.add("Yes {21 No [3");
		yesResponseList.add("Yes 12; NO [If");

		List<String> noResponseList = new ArrayList<String>();
		noResponseList.add("Yes {3 No M");
		noResponseList.add("Yes D No [a");
		noResponseList.add("Yes if] N0 [if/J");
		noResponseList.add("Yes [I] No M");
		noResponseList.add("Yes {fl No Vi");
		noResponseList.add("Yes [I] No 3/1");
		noResponseList.add("Yes [3 No [2");
		noResponseList.add("Yes 53 No 521");
		noResponseList.add("Yes [j N0 V!");
		noResponseList.add("Yes LIE N0 VJ");
		noResponseList.add("Yes If} N0 VT!");
		noResponseList.add("Yes H No ‘yj");
		noResponseList.add("Yes [3 No [2a");
		noResponseList.add("Yes E} No [E");
		noResponseList.add("Yes {1} No B");
		noResponseList.add("Yes i] No [vii");
		noResponseList.add("Yes [3 No If;");

		if (yesResponseList.contains(response))
			return "yes";
		if (noResponseList.contains(response))
			return "no";
		return "";
	}

	@Override
	@Transactional("transactionManager")
	public boolean getUpdatedISODocumentParseData(Long docId) {
		DocumentTemplateMapping documentTemplateMapping = documentTemplateMappingDao
				.getDocumentTemplateMappingByDocId(docId);
		List<DocumentQuestions> documentQuestionList = documentQuestionsDao
				.getDocumentQuestionsByTemplateId(documentTemplateMapping.getTemplateId().getId(), false);
		try {

			for (DocumentQuestions documentQuestions : documentQuestionList) {

				if (documentQuestions.isQuestionStatus()) {
					if (!documentQuestions.isOSINT()) {
						String response = getIsoCodesJson(documentQuestions.getName());
						if (response != null) {
							JSONObject jsonResponseObjectNew = new JSONObject(response);
							JSONArray arrayObjectNew = jsonResponseObjectNew.getJSONArray("hits");
							if (arrayObjectNew.length() > 0) {
								JSONObject normalObject = arrayObjectNew.getJSONObject(0);
								JSONObject level3Object = normalObject.getJSONObject("controls")
										.getJSONObject("level3");
								JSONObject level1Object = normalObject.getJSONObject("controls")
										.getJSONObject("level1");
								if (!level1Object.getString("code")
										.equalsIgnoreCase(documentQuestions.getLevel1Code())) {
									List<DocumentTemplateMapping> documentTemplateMappingList = documentTemplateMappingDao
											.getDocumentTemplateMappingByTemplateId(documentTemplateMapping.getId());
									for (DocumentTemplateMapping documentTemplateMapping2 : documentTemplateMappingList) {
										IsoCategory oldIsoCategory = isoCategoryDao.getISOCategoriesByCodeAndDocId(
												documentQuestions.getLevel1Code(),
												documentTemplateMapping2.getDocumentId());
										if (oldIsoCategory != null) {
											oldIsoCategory.setEvaluation("undefined");
											isoCategoryDao.saveOrUpdate(oldIsoCategory);
											IsoCategory newIsoCategoryExist = isoCategoryDao
													.getISOCategoriesByCodeAndDocId(level1Object.getString("code"),
															documentTemplateMapping2.getDocumentId());
											if (newIsoCategoryExist != null) {
												newIsoCategoryExist.setEvaluation("undefined");
												isoCategoryDao.saveOrUpdate(newIsoCategoryExist);
											} else {
												IsoCategory newIsoCategory = new IsoCategory();
												newIsoCategory.setDocId(documentTemplateMapping2.getDocumentId());
												newIsoCategory.setLevel1Code(level1Object.getString("code"));
												newIsoCategory.setLevel1Title(level1Object.getString("title"));
												Case caseSeed = caseDao
														.getCaseByDocId(documentTemplateMapping2.getDocumentId());
												if (caseSeed != null) {
													String evaluation = getIsoCategoryEvaluation(
															level1Object.getString("code"), caseSeed.getEntityId(),
															documentQuestions.isOSINT());
													if (evaluation != null) {
														JSONObject evaluationObject = new JSONObject(evaluation);
														if (!evaluationObject.getString("evaluation")
																.equalsIgnoreCase("error"))
															newIsoCategory.setEvaluation(
																	evaluationObject.getString("evaluation"));
														if (evaluationObject.has("ranking"))
															newIsoCategory.setRank(evaluationObject.getInt("ranking"));
													}
												}
												isoCategoryDao.create(newIsoCategory);

											}

										}
									}

								}
								if ((!level3Object.getString("code")
										.equalsIgnoreCase(documentQuestions.getPrimaryIsoCode()))
										|| (!level1Object.getString("code")
												.equalsIgnoreCase(documentQuestions.getLevel1Code()))) {
									documentQuestions.setPrimaryIsoCode(level3Object.getString("code"));
									documentQuestions.setLevel3Title(level3Object.getString("title"));
									documentQuestions.setLevel3Description(level3Object.getString("title"));
									documentQuestions.setSecondaryIsoCode((getSecondaryIsoCode(response)).toString());
									documentQuestions
											.setTopic(normalObject.getJSONObject("controls").getString("topic"));
									documentQuestions.setLevel1Code(level1Object.getString("code"));
									documentQuestions.setLevel1Title(level1Object.getString("title"));
									documentQuestionsDao.saveOrUpdate(documentQuestions);
									documentQuestions = documentQuestionsDao.find(documentQuestions.getId());
								}
							}
						}
					}

				}

			}
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean saveIsoCategory(List<DocumentQuestions> documentQuestionsList, long docId) {
		try {
			Map<String, String> isoCategoryMap = new HashMap<String, String>();
			Map<String, String> bstIsoCategoryMap = new HashMap<String, String>();
			for (DocumentQuestions documentQuestions : documentQuestionsList) {
				if (documentQuestions.getLevel1Code() != null)
					if (documentQuestions.isOSINT())
						bstIsoCategoryMap.put(documentQuestions.getLevel1Code(), documentQuestions.getLevel1Title());
					else
						isoCategoryMap.put(documentQuestions.getLevel1Code(), documentQuestions.getLevel1Title());
			}
			if (isoCategoryMap.size() > 0)
				isoCategoryMap.put("", "Un mapped");

			for (Map.Entry<String, String> entry : isoCategoryMap.entrySet()) {

				IsoCategory isoCategory = new IsoCategory();
				isoCategory.setLevel1Code(entry.getKey());
				isoCategory.setLevel1Title(entry.getValue());
				isoCategory.setDocId(docId);
				isoCategory.setOSINT(false);
				isoCategoryDao.create(isoCategory);
			}
			for (Map.Entry<String, String> entry : bstIsoCategoryMap.entrySet()) {

				IsoCategory isoCategory = new IsoCategory();
				isoCategory.setLevel1Code(entry.getKey());
				isoCategory.setLevel1Title(entry.getValue());
				isoCategory.setDocId(docId);
				isoCategory.setOSINT(true);
				isoCategoryDao.create(isoCategory);
			}

			return true;
		} catch (Exception e) {
			return false;
		}

	}

	private String getIsoCategoryEvaluation(String isoCode, String orgId, boolean osint) throws Exception {
		JSONObject jsonObject = new JSONObject();
		String url = ISOCODE_QUESTION_URL;

		jsonObject.put("org_id", orgId);
		jsonObject.put("session_id", UUID.randomUUID());
		jsonObject.put("ISO_code", isoCode);
		if (osint)
			url = url + "osint/evaluate/sum";
		else
			url = url + "questionnaire/evaluate/sum";

		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonObject.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	private String getisoCategory(String jsonData) {
		String url = ISOCODE_CATEGORY_URL;
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonData);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "unused", "resource" })
	public List<org.json.simple.JSONObject> getAnswerPositions(MultipartFile uploadedFile, byte[] dataStream)
			throws InvalidPasswordException, IOException {
		List<org.json.simple.JSONObject> jsonObjectsList = new ArrayList<org.json.simple.JSONObject>();
		PDDocument document = null;
		// gettinng answers for respective page.
		try {
			document = new PDDocument();
			if (uploadedFile != null) {
				String originalFile = new String(uploadedFile.getOriginalFilename());
				String ext = FilenameUtils.getExtension(originalFile);
				String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
				File file = null;
				file = File.createTempFile(fileNameWithoutExtension, ext);
				uploadedFile.transferTo(file);
				document = PDDocument.load(file);
			}
			if (dataStream != null)
				document = PDDocument.load(dataStream);
			PDPage page = document.getPage(0);
			PDRectangle pdRectangle = page.getCropBox();
			// System.out.println(pdRectangle.getLowerLeftX()+"
			// "+pdRectangle.getUpperRightY()+"&&&&&&&&&&&&");
			PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
			if (acroForm != null) {
				for (PDField form : acroForm.getFieldTree()) {
					for (PDAnnotationWidget widget : form.getWidgets()) {
						COSDictionary widgetObject = widget.getCOSObject();
						PDPageTree pages = document.getPages();
						for (int i = 0; i < pages.getCount(); i++) {
							for (PDAnnotation annotation : pages.get(i).getAnnotations()) {
								COSDictionary annotationObject = annotation.getCOSObject();
								if (annotationObject.equals(widgetObject)) {
									COSDictionary field = (COSDictionary) form.getCOSObject();
									COSArray rectArray = (COSArray) field.getDictionaryObject("Rect");
									if (rectArray != null) {
										COSBase base = rectArray.get(0);
										PDRectangle mediaBox = new PDRectangle(rectArray);
										// System.out.println(mediaBox.getHeight() +" "+ mediaBox.getWidth());
										double upperRightY = pdRectangle.getHeight() - (mediaBox.getUpperRightY());
										org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
										jsonObject.put("answer", form.getValueAsString());
										jsonObject.put("answerCode", form.getFullyQualifiedName());
										jsonObject.put("lowerleftX", mediaBox.getLowerLeftX());
										jsonObject.put("upperRightY", upperRightY);
										jsonObject.put("pageNumber", i + 1);
										String type = null;
										if (form instanceof PDCheckBox) {
											jsonObject.put("type", "CHECKBOX");
										} else if (form instanceof PDTextField) {
											jsonObject.put("type", "TEXT");
										} else if (form instanceof PDRadioButton) {
											jsonObject.put("type", "YES_NO_CHECKBOX");
										}
										// answersmap.put(val1, form.getFullyQualifiedName()+" "+form.getValueAsString()
										// + " "+i+1);
										jsonObjectsList.add(jsonObject);
										// System.out.println("----------------------------");
									} // if close
									else {
										org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
										double upperRightY = pdRectangle.getHeight()
												- (widget.getRectangle().getUpperRightY());
										jsonObject.put("answer", form.getValueAsString());
										jsonObject.put("lowerleftX", widget.getRectangle().getLowerLeftX());
										jsonObject.put("upperRightY", upperRightY);
										jsonObject.put("pageNumber", i + 1);
										jsonObject.put("type", "YES_NO_CHECKBOX");
										jsonObjectsList.add(jsonObject);
										// System.out.println(form.getValueAsString());
										// System.out.println(jsonObject.get("answer") + " " +jsonObject.get("type") + "
										// "+jsonObject.get("pageNumber") +" "+jsonObject.get("lowerleftX") +"
										// "+jsonObject.get("upperRightY")+ " "+widget.getRectangle());

									}
								}
							}
						} // page close
					}
				} // pdfield close for

			} // acroform if close
				// getAnswerPositions1(i);
		} finally {
			document.close();
		}
		return jsonObjectsList;
	}

	@SuppressWarnings("unchecked")
	public org.json.simple.JSONObject getAnswer(List<org.json.simple.JSONObject> jsonObjectsList, int pageNumberAnswer,
			String coordinates) {
		org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
		out: for (org.json.simple.JSONObject entry : jsonObjectsList) {
			int pageNumber = (int) entry.get("pageNumber");
			if (pageNumber == pageNumberAnswer) {
				double x = Double.parseDouble(entry.get("lowerleftX").toString());
				double y = (double) entry.get("upperRightY");
				List<String> items = Arrays.asList(coordinates.split("\\s*,\\s*"));
				if (Double.valueOf(items.get(0)) <= x && Double.valueOf(items.get(1)) <= y)// change to x and y values
				{
					jsonObject.put("answer", entry.get("answer"));
					jsonObject.put("type", entry.get("type"));
					// System.out.println(entry.get("answer") + " " + entry.get("lowerleftX") + " "+
					// entry.get("upperRightY") + " " + entry.get("type") + " " + pageNumber);
					break out;
				}
			}
		}
		return jsonObject;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public List<OverAllResponseDto> getTemplateQandA(Long tempId) {
		boolean isFillablePdf = true;
		List<DocumentQuestions> documentQuestionList = documentQuestionsDao.getDocumentQuestionsByTemplateId(tempId,
				false);
		List<OverAllResponseDto> overAllResponseDto = new ArrayList<OverAllResponseDto>();
		try {
			OverAllResponseDto allResponseDto = new OverAllResponseDto();
			List<DocumentContentDto> documentContentDtoList = new ArrayList<DocumentContentDto>();
			List<Categories> categoriesList = new ArrayList<Categories>();

			for (DocumentQuestions documentQuestions : documentQuestionList) {
				/*
				 * if (documentQuestions.getContentType().trim().equalsIgnoreCase("PARAGRAPH")
				 * ||
				 * documentQuestions.getContentType().trim().equalsIgnoreCase("SUB_PARAGRAPH")
				 * || documentQuestions.getContentType().trim().equalsIgnoreCase("Question")) {
				 */ // for child object
				if (documentQuestions.getHasParentId() != 0) {
					for (DocumentContentDto documentContentDtoInner : documentContentDtoList) {
						ContentDataDto contentDataDtoInner = documentContentDtoInner.getContentDataDto();
						if (contentDataDtoInner != null) {
							if (contentDataDtoInner.getQuestionId() != null) {
								if (contentDataDtoInner.getQuestionId() == documentQuestions.getHasParentId()) {
									DocumentAnswers documentAnswers = new DocumentAnswers();
									// documentAnswers =
									// documentAnswersDao.getDcoumentAnswerByQuestionId(documentQuestions.getId(),
									// docId);
									contentDataDtoInner.setSpecifiedQuestion(documentQuestions.getName());
									contentDataDtoInner.setSpecifiedQuestionId(documentQuestions.getId());
									contentDataDtoInner.setPageNumber(documentQuestions.getPageNumber());
									if (documentQuestions.getElementName() != null)
										contentDataDtoInner.setElementName(documentQuestions.getElementName());
									if (documentQuestions.getFormField() != null)
										contentDataDtoInner.setFormField(documentQuestions.getFormField());
									if (documentQuestions.getSubType() != null)
										contentDataDtoInner.setSubType(documentQuestions.getSubType());
									contentDataDtoInner
											.setSpecifiedQuestionPosition(documentQuestions.getQuestionPosition());
									contentDataDtoInner.setSpecifiedQuestionPositionOnThePage(
											documentQuestions.getQuestionPositionOnThePage());
									contentDataDtoInner.setSpecifiedQuestionUUID(documentQuestions.getQuestionUUID());
									if (documentQuestions.getAnswerPosition() != null) {
										contentDataDtoInner
												.setSpecifiedAnswerPosition(documentQuestions.getAnswerPosition());
										contentDataDtoInner.setSpecifiedAnswerPositionOnThePage(
												documentQuestions.getAnswerPositionOnThePage());
									}
									if (documentQuestions.getAnswerUUID() != null)
										contentDataDtoInner.setSpecifiedAnswerUUID(documentQuestions.getAnswerUUID());

									if (documentAnswers != null) {
										contentDataDtoInner.setSpecifiedAnswer(documentAnswers.getAnswer());
										contentDataDtoInner.setSpecifiedAnswerId(documentAnswers.getId());
									}
									if (isFillablePdf) {
										List<PossibleAnswers> possibleAnswersList = possibleAnswersService
												.fetchPossibleAnswers(documentQuestions.getId());
										List<PossibleAnswersDto> possibleAnswersDtoList = new ArrayList<PossibleAnswersDto>();
										if (!possibleAnswersList.isEmpty()) {
											for (PossibleAnswers possibleAnswers : possibleAnswersList) {
												PossibleAnswersDto possibleAnswersDto = new PossibleAnswersDto();
												BeanUtils.copyProperties(possibleAnswers, possibleAnswersDto);
												possibleAnswersDtoList.add(possibleAnswersDto);
											}
											contentDataDtoInner.setSpecifiedPossibleAnswersDto(possibleAnswersDtoList);
										}
									}

								}
							}
						}
					}
				}
				// for parent object
				else {
					DocumentContentDto documentContentDto = new DocumentContentDto();
					// DocumentQuestions documentQuestions = new DocumentQuestions();
					DocumentAnswers documentAnswers = new DocumentAnswers();
					ContentDataDto contentDataDto = new ContentDataDto();
					// documentQuestions =
					// documentQuestionsDao.getDocumentQuestionsById(documentContents.getId());

					/*
					 * if (documentQuestions.getContentType().trim().equalsIgnoreCase("PARAGRAPH"))
					 * documentContentDto.setContentType("PARAGRAPH"); if
					 * (documentQuestions.getContentType().trim().equalsIgnoreCase("Question"))
					 * documentContentDto.setContentType("Question");
					 */
					documentContentDto.setContentType(documentQuestions.getContentType());
					if (documentQuestions.isQuestionStatus()) {
						documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
						contentDataDto.setQuestion(documentQuestions.getName());
						if (documentQuestions.getFormField() != null)
							contentDataDto.setFormField(documentQuestions.getFormField());
						if (documentQuestions.getSubType() != null)
							contentDataDto.setSubType(documentQuestions.getSubType());
						if (documentQuestions.getElementName() != null)
							contentDataDto.setElementName(documentQuestions.getElementName());
						contentDataDto.setQuestionId(documentQuestions.getId());
						contentDataDto.setPageNumber(documentQuestions.getPageNumber());
						contentDataDto.setAnswerType(documentQuestions.getAnswerType());
						contentDataDto.setQuestionPosition(documentQuestions.getQuestionPosition());
						contentDataDto.setQuestionPositionOnThePage(documentQuestions.getQuestionPositionOnThePage());
						contentDataDto.setQuestionUUID(documentQuestions.getQuestionUUID());
						if (documentQuestions.getAnswerPosition() != null) {
							contentDataDto.setAnswerPosition(documentQuestions.getAnswerPosition());
						}
						contentDataDto.setAnswerPositionOnThePage(documentQuestions.getAnswerPositionOnThePage());
						if (documentQuestions.getAnswerUUID() != null)
							contentDataDto.setAnswerUUID(documentQuestions.getAnswerUUID());
						contentDataDto.setHasIsoCode(documentQuestions.isHasIsoCode());
						if (documentQuestions.getPossibleAnswer() != null
								|| documentQuestions.getPossibleAnswer() != "")
							contentDataDto.setPossibleAnswer(documentQuestions.getPossibleAnswer());
						if (isFillablePdf) {
							List<PossibleAnswers> possibleAnswersList = possibleAnswersService
									.fetchPossibleAnswers(documentQuestions.getId());
							List<PossibleAnswersDto> possibleAnswersDtoList = new ArrayList<PossibleAnswersDto>();
							if (!possibleAnswersList.isEmpty()) {
								for (PossibleAnswers possibleAnswers : possibleAnswersList) {
									PossibleAnswersDto possibleAnswersDto = new PossibleAnswersDto();
									BeanUtils.copyProperties(possibleAnswers, possibleAnswersDto);
									possibleAnswersDtoList.add(possibleAnswersDto);
								}
								contentDataDto.setPossibleAnswersDto(possibleAnswersDtoList);
							}
						}
						if (documentQuestions.getPrimaryIsoCode() != null
								|| documentQuestions.getPrimaryIsoCode() != "")
							contentDataDto.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode());
						if (documentQuestions.getSecondaryIsoCode() != null
								|| documentQuestions.getSecondaryIsoCode() != "")
							contentDataDto.setSecondaryIsoCodes(documentQuestions.getSecondaryIsoCode());
						if (documentQuestions.getTopic() != null || documentQuestions.getTopic() != "")
							contentDataDto.setTopic(documentQuestions.getTopic());
						if (documentQuestions.getLevel1Code() != null || documentQuestions.getLevel1Code() != "")
							contentDataDto.setLevel1Code(documentQuestions.getLevel1Code());
						if (documentQuestions.getLevel1Title() != null || documentQuestions.getLevel1Title() != "")
							contentDataDto.setLevel1Title(documentQuestions.getLevel1Title());
						if (documentQuestions.getLevel3Title() != null || documentQuestions.getLevel3Title() != "")
							contentDataDto.setLevel3Title(documentQuestions.getLevel3Title());
						if (documentQuestions.getLevel3Description() != null
								|| documentQuestions.getLevel3Description() != "")
							contentDataDto.setLevel3Description(documentQuestions.getLevel3Description());
						contentDataDto.setIsOSINT(documentQuestions.isOSINT());
						if (documentQuestions.getShowInUI() != null)
							contentDataDto.setShowINUI(documentQuestions.getShowInUI());
						if (documentQuestions.getAnswerType() != null) {
							if (!documentQuestions.getAnswerType().equals(AnswerType.TABLE)) {
								// documentAnswers =
								// documentAnswersDao.getDcoumentAnswerByQuestionId(documentQuestions.getId(),
								// );
								if (documentAnswers != null) {
									contentDataDto.setAnswer(documentAnswers.getAnswer());
									contentDataDto.setAnswerId(documentAnswers.getId());
									contentDataDto.setEvaluation(documentAnswers.getEvaluation());
									contentDataDto.setActualAnswer(documentAnswers.getActualAnswer());
									Users user = usersDao.find(documentAnswers.getModefiedBy());
									// contentDataDto.setModefiedBy(user.getScreenName().toString());
									if (documentAnswers.getRankUpdatedBy() != 0) {
										Users rankUpdatedbUser = usersDao.find(documentAnswers.getRankUpdatedBy());
										contentDataDto.setRankModefiedBy(rankUpdatedbUser.getScreenName().toString());
									}
									if (documentAnswers.getRankUpdatedTime() != null)
										contentDataDto.setRankUpdatedOn(documentAnswers.getRankUpdatedTime());
									if (documentAnswers.getUpdatedTime() != null)
										contentDataDto.setAnswerUpdatedOn(documentAnswers.getUpdatedTime());
									contentDataDto.setAnswerRanking(documentAnswers.getAnswerRank());
									if (documentAnswers.getSourceUrls() != null)
										contentDataDto.setSourceUrls(documentAnswers.getSourceUrls());

								}
							}
							if (documentQuestions.getAnswerType().equals(AnswerType.TABLE)) {
								TableDto tableDto = new TableDto();
								List<DocumentAnswers> tableAnswersList = new ArrayList<DocumentAnswers>();
								// List<DocumentAnswers> tableAnswersList = documentAnswersDao
								// .getListOfTableAnswers(documentQuestions.getId(), docId);
								HashSet<String> columsSet = new HashSet<String>();
								HashSet<String> rowsSet = new HashSet<String>();
								for (DocumentAnswers tableAnswer : tableAnswersList) {
									String tablePosition = tableAnswer.getTableDataPosition();
									if (tablePosition != null && tablePosition.contains("*")) {
										String[] positoins = tablePosition.split("\\*");
										if (positoins.length == 2) {
											rowsSet.add(positoins[0]);
											columsSet.add(positoins[1]);
										}
									}

								}
								List<RowsDto> rowsDto = new ArrayList<RowsDto>();
								for (int i = 0; i < rowsSet.size(); i++) {
									RowsDto row = new RowsDto();
									List<ColumnsDto> ColumnsDto = new ArrayList<ColumnsDto>();
									for (int j = 0; j < columsSet.size(); j++) {
										ColumnsDto colum = new ColumnsDto();
										ColumnsDto.add(colum);
									}
									row.setColumns(ColumnsDto);
									rowsDto.add(row);
								}

								for (DocumentAnswers tableAnswer : tableAnswersList) {
									String tablePosition = tableAnswer.getTableDataPosition();
									if (tablePosition != null && tablePosition.contains("*")) {
										String[] positoins = tablePosition.split("\\*");
										int row = Integer.parseInt(positoins[0]);
										int colum = Integer.parseInt(positoins[1]);
										ColumDataDto columDataDto = new ColumDataDto();
										columDataDto.setId(tableAnswer.getId());
										columDataDto.setName(tableAnswer.getAnswer());
										if (row == 1)
											columDataDto.setHeader(true);
										else
											columDataDto.setHeader(false);
										rowsDto.get(row - 1).getColumns().get(colum - 1).setColumDataDto(columDataDto);
									}

								}
								tableDto.setRows(rowsDto);
								contentDataDto.setTable(tableDto);
							}
						}
						if (documentQuestions.getPrimaryIsoCode() != null
								&& documentQuestions.getPrimaryIsoCode().equalsIgnoreCase("BST.1.3")) {
							if (documentAnswers != null && documentAnswers.getId() != 0) {
								List<AnswerTopEvents> topEvents = answerTopEventsDao
										.getTopEventsByAnswerId(documentAnswers.getId());
								List<TopEventsDto> topEventsList = new ArrayList<TopEventsDto>();
								for (AnswerTopEvents answerTopEvents : topEvents) {
									TopEventsDto topEventsDto = new TopEventsDto();
									topEventsDto.setEventId(answerTopEvents.getId());
									if (answerTopEvents.getLocationName() != null)
										topEventsDto.setLocationName(answerTopEvents.getLocationName());
									if (answerTopEvents.getPublished() != null)
										topEventsDto.setPublished(answerTopEvents.getPublished());
									if (answerTopEvents.getTechnologies() != null)
										topEventsDto.setTechnologies(answerTopEvents.getTechnologies());
									if (answerTopEvents.getText() != null)
										topEventsDto.setText(answerTopEvents.getText());
									if (answerTopEvents.getTitle() != null)
										topEventsDto.setTitle(answerTopEvents.getTitle());
									if (answerTopEvents.getUrl() != null)
										topEventsDto.setUrl(answerTopEvents.getUrl());
									topEventsList.add(topEventsDto);

								}
								if (topEventsList.size() > 0)
									contentDataDto.setTopEvents(topEventsList);
							}
						}
					}
					if (!documentQuestions.isQuestionStatus()) {
						documentContentDto.setQuestion(documentQuestions.isQuestionStatus());
						contentDataDto.setText(documentQuestions.getName());
						contentDataDto.setQuestionPosition(documentQuestions.getQuestionPosition());
						contentDataDto.setQuestionPositionOnThePage(documentQuestions.getQuestionPositionOnThePage());
						contentDataDto.setQuestionUUID(documentQuestions.getQuestionUUID());
						contentDataDto.setPageNumber(documentQuestions.getPageNumber());
						if (documentQuestions.getElementName() != null)
							contentDataDto.setElementName(documentQuestions.getElementName());
						contentDataDto.setQuestionId(documentQuestions.getId());
					}
					documentContentDto.setContentDataDto(contentDataDto);
					documentContentDtoList.add(documentContentDto);
				} // else close
					// } // sub paragraph close

				/*
				 * if (documentContents.getContentType().trim().equalsIgnoreCase("TABLE")) {
				 * DocumentContentDto documentContentDto = new DocumentContentDto();
				 * documentContentDto.setContentType("TABLE"); ContentDataDto contentDataDtot =
				 * new ContentDataDto(); List<DocumentQuestions> documentQuestionsList =
				 * documentQuestionsDao .getTableQuestionsById(documentContents.getId());
				 * ContentDataTableDto contentDataTableDto = new ContentDataTableDto(); if
				 * (documentQuestionsList.size() > 0) { HashSet<String> tableRowsCount = new
				 * HashSet<String>(); HashSet<String> tableColumnsCount = new HashSet<String>();
				 * String answerMatrix = null; String questionMatrix = null; for
				 * (DocumentQuestions documentQuestionsTable : documentQuestionsList) {
				 * answerMatrix = documentQuestionsTable.getComponentAnswerMatrix(); if
				 * (answerMatrix != null) { String rowCount[] =
				 * answerMatrix.trim().split("\\*"); tableRowsCount.add(rowCount[0]);
				 * tableColumnsCount.add(rowCount[1]); } questionMatrix =
				 * documentQuestionsTable.getComponentQuestionMatrix();
				 * 
				 * }
				 * 
				 * List<DataTableRowsDto> dataTableRows = new ArrayList<DataTableRowsDto>(); for
				 * (int a = 0; a < tableRowsCount.size(); a++) { DataTableRowsDto
				 * dataTableRowsDto = new DataTableRowsDto(); List<DataTableColumsDto>
				 * dataTableColums = new ArrayList<DataTableColumsDto>(); for (int b = 0; b <
				 * tableColumnsCount.size(); b++) { DataTableColumsDto dataTableColumsDto = new
				 * DataTableColumsDto(); ContentDataDto tableContentDataDto = new
				 * ContentDataDto(); dataTableColumsDto.setContentDataDto(tableContentDataDto);
				 * dataTableColums.add(dataTableColumsDto);
				 * 
				 * } dataTableRowsDto.setDataTableColumsDto(dataTableColums);
				 * dataTableRows.add(dataTableRowsDto); } for (DocumentQuestions
				 * documentQuestions : documentQuestionsList) { DocumentAnswers documentAnswers
				 * = documentAnswersDao
				 * .getDcoumentAnswerByQuestionId(documentQuestions.getId(),docId);
				 * ContentDataDto contentDataDtoTable1 = new ContentDataDto(); ContentDataDto
				 * contentDataDtoTable2 = new ContentDataDto(); ContentDataDto
				 * contentDataDtoTableAns = new ContentDataDto(); String answerMatrix1 =
				 * documentQuestions.getComponentAnswerMatrix(); String[] answerMatrix1Pos =
				 * answerMatrix1.split("\\*");
				 * 
				 * contentDataDtoTableAns.setAnswerId(documentAnswers.getId());
				 * contentDataDtoTableAns.setAnswer(documentAnswers.getAnswer());
				 * contentDataDtoTableAns.setAnswerType(documentQuestions.getAnswerType());
				 * 
				 * 
				 * int ansRowPosition = Integer.parseInt(answerMatrix1Pos[0]) - 1; int
				 * ansColumPosition = Integer.parseInt(answerMatrix1Pos[1]) - 1;
				 * contentDataDtoTableAns.setAnswerType(documentQuestions.getAnswerType());
				 * contentDataDtoTableAns.setQuestion(documentQuestions.getName());
				 * contentDataDtoTableAns.setQuestionId(documentQuestions.getId());
				 * contentDataDtoTableAns
				 * .setPossibleAnswer(documentQuestions.getPossibleAnswer());
				 * contentDataDtoTableAns.setRiskScore(documentQuestions.getRiskScore());
				 * contentDataDtoTableAns.setPrimaryIsoCode(documentQuestions.getPrimaryIsoCode(
				 * )); contentDataDtoTableAns.setSecondaryIsoCodes(documentQuestions.
				 * getSecondaryIsoCode());
				 * dataTableRows.get(ansRowPosition).getDataTableColumsDto()
				 * .get(ansColumPosition).setContentDataDto(contentDataDtoTableAns); }
				 * contentDataTableDto.setDataTableRowsDto(dataTableRows); }
				 * documentContentDto.setContentDataTableDto(contentDataTableDto);
				 * documentContentDtoList.add(documentContentDto); }
				 */

			}
			allResponseDto.setDocumentContentDto(documentContentDtoList);

			/*
			 * for (IsoCategory isoCategory : isoCategoryList) { Categories category = new
			 * Categories(); category.setCategoryId(isoCategory.getId());
			 * category.setLevel1Code(isoCategory.getLevel1Code());
			 * category.setLevelTitle(isoCategory.getLevel1Title());
			 * category.setEvaluation(isoCategory.getEvaluation());
			 * categoriesList.add(category);
			 * 
			 * }
			 */ allResponseDto.setCategories(categoriesList);
			allResponseDto.setTemplateId(tempId);
			overAllResponseDto.add(allResponseDto);

		} catch (Exception e) {
			throw new NoDataFoundException("Failed Due to Internal Server Error");
		}
		return overAllResponseDto;

	}

	@SuppressWarnings({ "unused" })
	@Override
	public CheckPdfTypeDto checkPdfType(byte[] byteArray)
			throws IllegalStateException, IOException, TesseractException {
		String pdfText = null;
		String pdfScanned = null;
		List<String> listAcroForm = new ArrayList<String>();
		CheckPdfTypeDto checkPdfTypeDto = new CheckPdfTypeDto();
		PDDocument document = null;

		try {
			document = PDDocument.load(byteArray);
			PDFTextStripper pdfTextStripper = new PDFLayoutTextStripper();
			pdfText = pdfTextStripper.getText(document);

			PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
			if (acroForm != null) {
				for (PDField form : acroForm.getFieldTree()) {
					listAcroForm.add(form.getValueAsString());
				}
			}
			if ((pdfText != null && pdfText.length() > 0)) {
				if (!listAcroForm.isEmpty()) {
					checkPdfTypeDto.setFillablePdf(true);
					return checkPdfTypeDto;
				} else {
					checkPdfTypeDto.setNormalPdf(true);
					return checkPdfTypeDto;
				}
			}

			if (pdfText == null
					|| pdfText.length() == 0) {/*
												 * ITesseract tesseract = new Tesseract();
												 * tesseract.setDatapath(TESSRACT_LANGUAGE); File convFile = new
												 * File(uploadFile.getOriginalFilename()); convFile.createNewFile();
												 * uploadFile.transferTo(convFile); pdfScanned =
												 * tesseract.doOCR(convFile);// scanned pdf pdfText = ""; pdfText =
												 * pdfScanned; } String trimValue = pdfText.trim().replaceAll("\\s{2,}",
												 * " "); if ((trimValue != null || trimValue.length() > 0)) {
												 * checkPdfTypeDto.setScannedPdf(true); return checkPdfTypeDto; }
												 */
				checkPdfTypeDto.setScannedPdf(true);
				return checkPdfTypeDto;
			}

		} // try close
		catch (Exception e) {
			return checkPdfTypeDto;
		} finally {
			if (document != null)
				document.close();
		} // finally close

		/*
		 * if(checkPdfTypeDto.isFillablePdf()==false &&
		 * checkPdfTypeDto.isNormalPdf()==false &&
		 * checkPdfTypeDto.isScannedPdf()==false) throw new
		 * NoDataFoundException("not able to recognise pdf");
		 */
		return checkPdfTypeDto;
	}

	@Override
	@Transactional("transactionManager")
	public DocumentVaultDTO getDocumentDetailsDocParser(Long docId, Long userId)
			throws IllegalAccessException, InvocationTargetException {
		DocumentVaultDTO documentDetails = new DocumentVaultDTO();
		DocumentVault document = documentDao.find(docId);
		if (document != null) {
			// if (document.getUploadedBy().getUserId().compareTo(userId) == 0 ||
			// isReadOrWritePermission(userId, docId)) {
			ConvertUtils.register(new DateConverter(null), Date.class);
			BeanUtils.copyProperties(document, documentDetails);
			if (document.getModifiedBy() != null) {
				Users user = usersDao.find(document.getModifiedBy());
				documentDetails.setModifiedByName(user.getFirstName());
			} else {
				documentDetails.setModifiedByName("");
			}
			documentDetails.setUploadedUserName(
					document.getUploadedBy().getFirstName() + " " + document.getUploadedBy().getLastName());
			return documentDetails;
			/*
			 * }else{ throw new
			 * PermissionDeniedException("Permission is denied or Document not found"); }
			 */
		} else {
			throw new DocNotFoundException();
		}
	}

	@Override
	@Transactional("transactionManager")
	public byte[] downloadDocumentDocParser(Long docId, Long userId, HttpServletResponse response) throws Exception {

		DocumentVault documentVault = documentDao.find(docId);
		
		byte[] fileData=null;
		if (documentVault != null && !documentVault.getIsDeleted()) {
			String s3DownloadLinkServerResponse[]= ServiceCallHelper.getdownloadLinkFromS3Server(CASE_MANAGEMENT_BIG_DATA_S3_DOC_URL+"document?client_id="+CLIENT_ID+"&doc_id="+documentVault.getDocGuuidFromServer());
			if (s3DownloadLinkServerResponse[0] != null
					&& s3DownloadLinkServerResponse[0].trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(s3DownloadLinkServerResponse[0]);
				if(json.has("download_link") && json.has("doc_id") && json.getString("download_link") != null) {
					fileData = ServiceCallHelper.downloadFileFromS3Server(json.getString("download_link"));
				}
			}

			if (fileData != null) {
				return fileData;
			} else {
				throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
			}
		
		/*if (documentVault != null && !documentVault.getIsDeleted()) {
			// if (documentVault.getUploadedBy().getUserId().compareTo(userId) == 0 ||
			// isReadOrWritePermission(userId, docId)) {
			String url = BIG_DATA_BASE_URL + "/doc/" + documentVault.getDocGuuidFromServer();
			byte[] fileData = ServiceCallHelper.downloadFileFromServer(url);
			if (fileData != null) {
				return fileData;
			} else {
				throw new Exception(ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG);
			}*/
			/*
			 * } else { throw new
			 * PermissionDeniedException("Permission is denied or Document not found."); }
			 */
		} else {
			throw new DocNotFoundException();
		}
	}

}