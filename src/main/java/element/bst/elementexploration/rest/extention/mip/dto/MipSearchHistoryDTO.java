package element.bst.elementexploration.rest.extention.mip.dto;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("MIP search history")
public class MipSearchHistoryDTO {
	@ApiModelProperty(value = "Search ID")
	private Long searchId;
	@ApiModelProperty(value = "Name of the entity")
	private String name;
	@ApiModelProperty(value = "Data of the entity")
	private String data;
	@ApiModelProperty(value = "Source page of the entity")
	private String sourcePage;
	@ApiModelProperty(value = "Date of creation")
	private Date createdOn;
	@ApiModelProperty(value = "Date of updation")
	private Date updatedOn;

	public MipSearchHistoryDTO() {
		super();
	}

	public MipSearchHistoryDTO(Long searchId, String name, String data, String sourcePage, Date createdOn,
			Date updatedOn) {
		super();
		this.searchId = searchId;
		this.name = name;
		this.data = data;
		this.sourcePage = sourcePage;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
	}

	public Long getSearchId() {
		return searchId;
	}

	public void setSearchId(Long searchId) {
		this.searchId = searchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSourcePage() {
		return sourcePage;
	}

	public void setSourcePage(String sourcePage) {
		this.sourcePage = sourcePage;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
