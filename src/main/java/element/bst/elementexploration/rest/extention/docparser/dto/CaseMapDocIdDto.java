package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;

public class CaseMapDocIdDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Map<String, String> caseFields;	
	private long docId;		
	private List<DocumentAnswers> documentAnswers;
	
	public CaseMapDocIdDto() {
		super();
		
	}
	public Map<String, String> getCaseFields() {
		return caseFields;
	}
	public void setCaseFields(Map<String, String> caseFields) {
		this.caseFields = caseFields;
	}
	public long getDocId() {
		return docId;
	}
	public void setDocId(long docId) {
		this.docId = docId;
	}
	public List<DocumentAnswers> getDocumentAnswers() {
		return documentAnswers;
	}
	public void setDocumentAnswers(List<DocumentAnswers> documentAnswers) {
		this.documentAnswers = documentAnswers;
	}
	
	
	
	
}
