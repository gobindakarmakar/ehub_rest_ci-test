package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceDomainDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourceDomainService extends GenericService<SourceDomain, Long> {

	List<SourceDomainDto> saveSourceDomain(SourceDomainDto domainDto) throws Exception;

	List<SourceDomainDto> getSourceDomain();

	SourceDomain fetchDomain(String string);

}
