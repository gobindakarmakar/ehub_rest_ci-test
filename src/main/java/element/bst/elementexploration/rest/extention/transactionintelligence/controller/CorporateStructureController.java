package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureTopFiveDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CorporateStructureService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 * 
 */
@Api(tags = { "Corporate structure API" },description="Manages alerted transactions data of corporate structures")
@RestController
@RequestMapping(value = "/api/corporateStructure")
public class CorporateStructureController extends BaseController {

	@Autowired
	CorporateStructureService corporateStructureService;

	///YES
	@ApiOperation("Gets corporate structure data")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCorporateStructure/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorporateStructure(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {

		/*return new ResponseEntity<>(corporateStructureService.getCorporateStructure(fromDate, toDate), HttpStatus.OK);*/
		return new ResponseEntity<>(corporateStructureService.getCorporateStructure(fromDate, toDate,filterDto), HttpStatus.OK);

	}

	///YES
	@ApiOperation("Gets corporate structure data aggregates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureDto.class, responseContainer="List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getCorporateStructureAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorporateStructureAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getCorporateStructureAggregates(fromDate, toDate,filterDto),
				HttpStatus.OK);

	}

	/*//redundant
	@GetMapping(value = "/getCorporateScenariosByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorporateScenariosByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getCorporateScenariosByType(fromDate, toDate, type),
				HttpStatus.OK);

	}*/

	//////////// unknown
	/*
	 * @PostMapping(value =
	 * "/getCorporateStructuteByType/{date-from}/{date-to}", produces = {
	 * "application/json; charset=UTF-8" }, consumes = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * getCustomerRiskByType(@PathVariable("date-from") String fromDate,
	 * 
	 * @PathVariable("date-to") String toDate, @RequestParam("token")
	 * List<String> token,
	 * 
	 * @RequestParam(required = false) String type, @RequestBody List<String>
	 * scenarios) throws ParseException { return new ResponseEntity<>(
	 * corporateStructureService.getCorporateStructuteByType(fromDate, toDate,
	 * type, scenarios), HttpStatus.OK);
	 * 
	 * }
	 */

	/*@PostMapping(value = "/getCorporateStructureByScenarios/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorporateStructuteByScenarios(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(
				corporateStructureService.getCorporateStructuteByScenarios(fromDate, toDate,filterDto),
				HttpStatus.OK);

	}*/

	///YES
	@ApiOperation("Gets alerted corporate structures data")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getAlertEntitiesGeography/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertEntitiesGeography(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {

		/*return new ResponseEntity<>(corporateStructureService.getAlertEntitiesGeography(fromDate, toDate),
				HttpStatus.OK);*/
		return new ResponseEntity<>(corporateStructureService.getAlertEntitiesGeography(fromDate, toDate,filterDto),
				HttpStatus.OK);

	}

	//redundant
	/*@GetMapping(value = "/getScenariosByCountry/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getScenariosByCountry(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") List<String> token,

			@RequestParam(required = false) String type) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getScenariosByCountry(fromDate, toDate, type),
				HttpStatus.OK);
	}

	//redundant
	@GetMapping(value = "/getAlertEntitiesBusinessType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertEntitiesBusinessType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getAlertEntitiesBusinessType(fromDate, toDate),
				HttpStatus.OK);

	}

	//redundant
	@GetMapping(value = "/getScenariosByBusinessType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getScenariosByBusinessType(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") List<String> token,

			@RequestParam String type) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getScenariosByBusinessType(fromDate, toDate, type),
				HttpStatus.OK);
	}

	//redundant
	@PostMapping(value = "/getCustomerByCountryScenario/{date-from}/{date-to}/{country}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerByCountryScenario(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") List<String> token,
			@PathVariable("country") String country,

			@RequestBody List<String> scenarios) throws ParseException {
		return new ResponseEntity<>(
				corporateStructureService.getCustomerByCountryScenario(fromDate, toDate, country, scenarios),
				HttpStatus.OK);
	}

	//redundant
	@PostMapping(value = "/getCustomerByBusinessScenario/{date-from}/{date-to}/{industry}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerByBusinessScenario(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") List<String> token,
			@PathVariable("industry") String industry,

			@RequestBody List<String> scenarios) throws ParseException {
		return new ResponseEntity<>(
				corporateStructureService.getCustomerByBusinessScenario(fromDate, toDate, industry, scenarios),
				HttpStatus.OK);
	}

	//redundant
	@PostMapping(value = "/getCounterPartyByCountryCustomer/{date-from}/{date-to}/{country}/{customerId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCounterPartyByCountryCustomer(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") List<String> token,
			@PathVariable("country") String country, @PathVariable("customerId") Long customerId,

			@RequestBody List<String> scenarios) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getCounterPartyByCountryCustomer(fromDate, toDate,
				country, customerId, scenarios), HttpStatus.OK);
	}

	//redundant
	@PostMapping(value = "/getCounterPartyByBusinessCustomer/{date-from}/{date-to}/{industry}/{customerId}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCounterPartyByBusinessCustomer(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") List<String> token,
			@PathVariable("industry") String industry, @PathVariable("customerId") Long customerId,

			@RequestBody List<String> scenarios) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getCounterPartyByBusinessCustomer(fromDate, toDate,
				industry, customerId, scenarios), HttpStatus.OK);
	}

	//////////// ShareHolders/////////////

	@GetMapping(value = "/insertShareHolders", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> insertShareHolders(@RequestParam("token") String token) {
		return new ResponseEntity<>(corporateStructureService.insertShareHolders(), HttpStatus.OK);
	}*/

	///YES
	@ApiOperation("Gets share holders of corporate structure")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureDto.class, responseContainer = "List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getShareholderCorporateStructure/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareholderCorporateStructure(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		/*return new ResponseEntity<>(corporateStructureService.getShareholderCorporateStructure(fromDate, toDate),
				HttpStatus.OK);*/
		return new ResponseEntity<>(corporateStructureService.getShareholderCorporateStructure(fromDate, toDate,filterDto),
				HttpStatus.OK);
	}

	/*//redundant
	@GetMapping(value = "/getShareholderCountry/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareholderCountry(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getShareholderCountry(fromDate, toDate), HttpStatus.OK);
	}*/

	///YES
	@ApiOperation("Gets shareholders corporate structure data aggregates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getShareholderAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareholderAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		/*return new ResponseEntity<>(corporateStructureService.getShareholderAggregates(fromDate, toDate),
				HttpStatus.OK);
*/
		return new ResponseEntity<>(corporateStructureService.getShareholderAggregates(fromDate, toDate,filterDto),
				HttpStatus.OK);

	}

	/*//redundant
	@GetMapping(value = "/getScenariosByShareholder/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getScenariosByShareholder(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") String token,

			@RequestParam(required = false) String type) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getScenariosByShareholder(fromDate, toDate, type),
				HttpStatus.OK);
	}

	//redundant
	@PostMapping(value = "/getCustomerByShareholderScenario/{date-from}/{date-to}/{country}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCustomerByShareholderScenario(@PathVariable("date-from") String fromDate,

			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@PathVariable("country") String country,

			@RequestBody List<String> scenarios) throws ParseException {
		return new ResponseEntity<>(
				corporateStructureService.getCustomerByShareholderScenario(fromDate, toDate, country, scenarios),
				HttpStatus.OK);
	}*/

	// viewAll
	/*@SuppressWarnings("unchecked")
	@GetMapping(value = "/getViewAllCorporateStructure/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getViewAllCorporateStructure(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "recordsPerPage", required = false) Integer recordsPerPage,
			@RequestParam("type") String type, HttpServletRequest request) throws ParseException {

		List<CorporateStructureDto> list = corporateStructureService.getViewAllCorporateStructure(fromDate, toDate,
				pageNumber, recordsPerPage, type);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = corporateStructureService.getViewAllCorporateStructureCount(fromDate, toDate, type);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", list);
		jsonObject.put("paginationInformation", information);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);

	}*/
	
/*	@SuppressWarnings("unchecked")
	@PostMapping(value = "/getViewAllCorporateStructure/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getViewAllCorporateStructure(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "recordsPerPage", required = false) Integer recordsPerPage,
			@RequestParam("type") String type, HttpServletRequest request,@RequestBody FilterDto filterDto) throws ParseException {

		List<CorporateStructureDto> list = corporateStructureService.getViewAllCorporateStructure(fromDate, toDate,
				pageNumber, recordsPerPage, type,filterDto);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = corporateStructureService.getViewAllCorporateStructureCount(fromDate, toDate, type);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", list);
		jsonObject.put("paginationInformation", information);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);

	}

	@PostMapping(value = "/getAssociatedAlerts/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAssociatedAlerts(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate,@RequestParam("token") String token,@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getAssociatedAlerts(fromDate, toDate, type),
				HttpStatus.OK);
		return new ResponseEntity<>(corporateStructureService.getAssociatedAlerts(fromDate, toDate, filterDto),
				HttpStatus.OK);
	}*/

	/////YES
	@ApiOperation("Gets top five alerted corporate structures data")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureTopFiveDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getTopFiveAlerts/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTopFiveAlerts(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		/*return new ResponseEntity<>(corporateStructureService.getTopFiveAlerts(fromDate, toDate), HttpStatus.OK);*/
		return new ResponseEntity<>(corporateStructureService.getTopFiveAlerts(fromDate, toDate,filterDto), HttpStatus.OK);
	}

	///YES
	@ApiOperation("Gets alerted geography data aggregate")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CorporateStructureDto.class,responseContainer="List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getGeographyAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeographyAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		/*return new ResponseEntity<>(corporateStructureService.getGeographyAggregates(fromDate, toDate), HttpStatus.OK);*/
		return new ResponseEntity<>(corporateStructureService.getGeographyAggregates(fromDate, toDate,filterDto), HttpStatus.OK);

	}

	/*@GetMapping(value = "/getCorporateStructureByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCorporateStructureByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getCorporateStructureByType(fromDate, toDate, type),
				HttpStatus.OK);

	}*/

	/*@PostMapping(value = "/getCorporateStructureByType/{date-from}/{date-to}", produces = {
	"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
		public ResponseEntity<?> getCorporateStructureByType(@PathVariable("date-from") String fromDate,
				@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestParam(required = false) String type,
				@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getCorporateStructureByType(fromDate, toDate,type, filterDto),
		HttpStatus.OK);

}
	@PostMapping(value = "/getGeographyByScenarios/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeographyByScenarios(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(
				corporateStructureService.getGeographyByScenarios(fromDate, toDate, scenarios, type), HttpStatus.OK);
		return new ResponseEntity<>(
				corporateStructureService.getGeographyByScenarios(fromDate, toDate, filterDto), HttpStatus.OK);

	}
	
	@PostMapping(value = "/getShareholderByScenarios/{date-from}/{date-to}", produces = {
	"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareholderByScenarios(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(
				corporateStructureService.getShareholderByScenarios(fromDate, toDate, scenarios, type), HttpStatus.OK);
		return new ResponseEntity<>(
				corporateStructureService.getShareholderByScenarios(fromDate, toDate,filterDto), HttpStatus.OK);

}

	@PostMapping(value = "/getShareholderByType/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareholderByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestParam(required=false) String type,
			@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getShareholderByType(fromDate, toDate, type),
				HttpStatus.OK);
		return new ResponseEntity<>(corporateStructureService.getShareholderByType(fromDate, toDate,type, filterDto),
				HttpStatus.OK);

	}

	@PostMapping(value = "/getGeographyByType/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeographyByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestParam(required=false) String type,
			@RequestBody FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(corporateStructureService.getGeographyByType(fromDate, toDate, type),
				HttpStatus.OK);
		return new ResponseEntity<>(corporateStructureService.getGeographyByType(fromDate, toDate,type, filterDto),
				HttpStatus.OK);

	}*/

}
