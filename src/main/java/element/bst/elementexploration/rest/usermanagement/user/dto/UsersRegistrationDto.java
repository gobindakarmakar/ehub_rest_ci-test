package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@ApiModel("Users registration")
public class UsersRegistrationDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID of the user", required = false)
	private Long userId;

	@ApiModelProperty(value = "Screen name of the user", required = true)
	private String screenName;

	@ApiModelProperty(value = "First name of the user", required = true)
	private String firstName;

	@ApiModelProperty(value = "Last name of the user", required = true)
	private String lastName;

	@ApiModelProperty(value = "Middle name of the user", required = false)
	private String middleName;

	@ApiModelProperty(value = "Date of birth of the user", required = false)
	private Date dob;

	@ApiModelProperty(value = "Country of the user", required = true)
	private Long country;

	@ApiModelProperty(value = "Email of the user", required = true)
	private String emailAddress;

	@ApiModelProperty(value = "password of the user", required = false)
	private String password;

	@ApiModelProperty(value = "Job title of the user", required = false)
	private String jobTitle;

	@ApiModelProperty(value = "extension of the user", required = false)
	private String extension;

	@ApiModelProperty(value = "phone number of the user", required = false)
	private String phoneNumber;

	@ApiModelProperty(value = "department of the user", required = false)
	private String department;

	@ApiModelProperty(value = "source of the user", required = false)
	private String source;
	
	@ApiModelProperty(value = "status of the user", required = false)
	private Long userStatus;
	
	@ApiModelProperty(value = "modifiability of the user", required = false)
	private Boolean isModifiable=true;

	@ApiModelProperty(value = "roles to be added to the user", required = false)
	private List<Long> rolesToBeAdded;

	@ApiModelProperty(value = "groups to be added to the user", required = false)
	private List<Long> groupsToBeAdded;

	@ApiModelProperty(value = "roles of the user to be removed", required = false)
	private List<Long> rolesToBeRemoved;

	@ApiModelProperty(value = "groups of the user to be removed", required = false)
	private List<Long> groupsToBeRemoved;

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getCountry() {
		return country;
	}

	public void setCountry(Long country) {
		this.country = country;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<Long> getRolesToBeAdded() {
		return rolesToBeAdded;
	}

	public void setRolesToBeAdded(List<Long> rolesToBeAdded) {
		this.rolesToBeAdded = rolesToBeAdded;
	}

	public List<Long> getGroupsToBeAdded() {
		return groupsToBeAdded;
	}

	public void setGroupsToBeAdded(List<Long> groupsToBeAdded) {
		this.groupsToBeAdded = groupsToBeAdded;
	}

	public List<Long> getRolesToBeRemoved() {
		return rolesToBeRemoved;
	}

	public void setRolesToBeRemoved(List<Long> rolesToBeRemoved) {
		this.rolesToBeRemoved = rolesToBeRemoved;
	}

	public List<Long> getGroupsToBeRemoved() {
		return groupsToBeRemoved;
	}

	public void setGroupsToBeRemoved(List<Long> groupsToBeRemoved) {
		this.groupsToBeRemoved = groupsToBeRemoved;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Long userStatus) {
		this.userStatus = userStatus;
	}

	public Boolean getIsModifiable() {
		return isModifiable;
	}

	public void setIsModifiable(Boolean isModifiable) {
		this.isModifiable = isModifiable;
	}


	
}
