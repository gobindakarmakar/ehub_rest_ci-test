package element.bst.elementexploration.rest.usermanagement.user.service;

import java.util.List;
import java.util.Properties;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;

/**
 * 
 * @author Amit Patel
 *
 */
public interface UserService extends GenericService<User, Long> {

	void setUserLastLoginDateAndIp(UserDto userDto, String ip);

	public Properties getProperties();

	void sendEmailVerification(Long userId, String userName, String url, String token, String emailAddress);// may be need to change

	boolean activateUser(User user);

	boolean deactivateUser(User user);

	List<UserDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	List<UserDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn);

	Long countUserList(Long groupId, String status);

	Long countUserListFullTextSearch(String keyword, String status, Long groupId);
	
	boolean isActiveUser(Long userId);

	List<UserDto> listUserByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countUserByGroup(Long groupId, Integer userStatus);

	UserDto getUserByEmailOrUsername(String email);
	
	User getUserObjectByEmailOrUsername(String email);
	
	UserDto checkUserNameOrEmailExists(String email, Long id);
	

}
