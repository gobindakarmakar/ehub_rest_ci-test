package element.bst.elementexploration.rest.document.daoImpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseDaoImpl;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Viswanath
 *
 */
@Repository("documentDao")
public class DocumentDaoImpl extends GenericDaoImpl<DocumentVault, Long> implements DocumentDao {

	public DocumentDaoImpl() {
		super(DocumentVault.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isDocShareable(long userId, long docId) {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("select count(*) from bst_doc_vault as dv where dv.pk_doc_id= :docId and dv.is_deleted= :isDeleted and dv.uploaded_by= :userId  ");
			builder.append("union ");
			builder.append(
					"select count(*) from bst_doc_user_mapping as dvm inner join bst_doc_vault as dv on dvm.fk_doc_id=dv.pk_doc_id and dv.is_deleted= :isDeleted and dvm.fk_doc_id= :docId and dvm.fk_user_id= :userId and dvm.is_shared= :isShared and dvm.permission = :permission ");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(builder.toString());
			query.setParameter("docId", docId);
			query.setParameter("userId", userId);
			query.setParameter("isShared", true);
			query.setParameter("isDeleted", false);
			query.setParameter("permission", "1");
			List<BigInteger> list = (List<BigInteger>) query.getResultList();
			BigInteger bigInteger = new BigInteger("0");
			// Checking if data found in Owner table or shared mapping table.
			for (BigInteger l : list) {
				if (l.compareTo(bigInteger) != 0)
					return true;
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					DocumentDaoImpl.class);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentVaultDTO> myDocuments(Long userId, Long caseseedId, Integer docFlag, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn,String entityId,String docSection,Boolean isAllRequired) {
		List<DocumentVaultDTO> list = new ArrayList<DocumentVaultDTO>();
		try {
			Integer flagValue = docFlag == null ? 0 : docFlag;
			StringBuilder hqlQuery =null;
			if(isAllRequired) {
				hqlQuery = new StringBuilder(createHqlQueryForAllMyDocuments(caseseedId,entityId,docSection));
			}else {
				hqlQuery = new StringBuilder(createHqlQueryForMyDocuments(caseseedId,entityId,docSection));
			}
			if (orderBy == null && orderIn == null) {
				hqlQuery.append(" order by dv.uploadedOn asc");
			} else if (orderBy != null && orderIn != null && orderBy.trim().length() > 0
					&& orderIn.trim().length() > 0) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hqlQuery.append(" order by dv.").append(orderBy.trim()).append(" ").append(orderIn.toLowerCase());
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			if(entityId==null)
				query.setParameter("userId", userId);
			if(entityId!=null)
				query.setParameter("entityId", entityId);
			if(docSection!=null)
				query.setParameter("docSection", docSection);
			query.setParameter("flagValue", flagValue);
			if (caseseedId != null)
				query.setParameter("caseseedId", caseseedId);
			if(pageNumber!=null && recordsPerPage!=null) {
				query.setFirstResult((pageNumber - 1) * (recordsPerPage));
				query.setMaxResults(recordsPerPage);
			}
			list = (List<DocumentVaultDTO>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return list;
	}

	@Override
	public long countMyDocuments(Long userId, Long caseseedId, Integer docFlag,String entityId,String docSection) {

		Long count = 0L;
		try {
			StringBuilder hqlQuery = new StringBuilder(createHqlQueryForMyDocumentCount(caseseedId,entityId,docSection));
			Integer flagValue = docFlag == null ? 0 : docFlag;
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			if(entityId==null)
				query.setParameter("userId", userId);
			if(entityId!=null)
				query.setParameter("entityId", entityId);
			if(docSection!=null)
				query.setParameter("docSection", docSection);
			query.setParameter("flagValue", flagValue);
			if (caseseedId != null)
				query.setParameter("caseseedId", caseseedId);
			count = (Long) query.getSingleResult();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentVaultDTO> fullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,
			Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn,String entityId,String docSection) {
		List<DocumentVaultDTO> list = new ArrayList<DocumentVaultDTO>();
		try {
			Integer flagValue = docFlag == null ? 0 : docFlag;
			StringBuilder hqlQuery = new StringBuilder(createHqlQueryForMyDocuments(caseseedId,entityId,docSection));
			if (!StringUtils.isEmpty(keyword)) {
				hqlQuery.append(" and (dv.docName like '%" + keyword.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.remark like '%" + keyword.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.title like '%" + keyword.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.type like '%" + keyword.replace("\"", "") + "%')");
			}
			if (orderBy == null && orderIn == null) {
				hqlQuery.append(" order by dv.uploadedOn asc");

			} else if (orderBy != null && orderIn != null && orderBy.trim().length() > 0
					&& orderIn.trim().length() > 0) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hqlQuery.append(" order by dv.").append(orderBy.trim()).append(" ").append(orderIn.toLowerCase());
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			if(entityId==null)
				query.setParameter("userId", userId);
			if(entityId!=null)
				query.setParameter("entityId", entityId);
			if(docSection!=null)
				query.setParameter("docSection", docSection);
			query.setParameter("flagValue", flagValue);
			if (caseseedId != null)
				query.setParameter("caseseedId", caseseedId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<DocumentVaultDTO>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return list;
	}

	@Override
	public long countFullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,String entityId,String docSection) {
		Long count = 0L;
		try {
			Integer flagValue = docFlag == null ? 0 : docFlag;
			StringBuilder hqlQuery = new StringBuilder(createHqlQueryForMyDocumentCount(caseseedId,entityId,docSection));
			if (!StringUtils.isEmpty(keyword)) {
				hqlQuery.append(" and (dv.docName like '%" + keyword.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.remark like '%" + keyword.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.title like '%" + keyword.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.type like '%" + keyword.replace("\"", "") + "%')");
			}
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			if(entityId==null)
				query.setParameter("userId", userId);
			if(entityId!=null)
				query.setParameter("entityId", entityId);
			if(docSection!=null)
				query.setParameter("docSection", docSection);
			query.setParameter("flagValue", flagValue);
			if (caseseedId != null)
				query.setParameter("caseseedId", caseseedId);
			count = (Long) query.getSingleResult();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return count;
	}

	public String createHqlQueryForMyDocumentCount(Long caseseedId,String entityId,String docSection) {
		StringBuilder hqlQuery = new StringBuilder();
		if (caseseedId == null) {
			hqlQuery.append("select count(*) from DocumentVault dv where ")
					.append(" dv.isDeleted = 0 and dv.docFlag =:flagValue ");
			if(entityId==null)
				hqlQuery.append(" and dv.uploadedBy.userId =:userId and dv.entityId is null and dv.docSection is null");
			if(entityId!=null)
				hqlQuery.append(" and dv.entityId=:entityId ");
			if(docSection!=null)
				hqlQuery.append(" and dv.docSection=:docSection ");
		} else {
			hqlQuery.append(
					"select count(*) from DocumentVault dv, DocumentVaultCaseMapping dvm where dv.docId = dvm.docId ");
			hqlQuery.append("and dvm.caseId =:caseseedId ")
					//.append(" and dv.uploadedBy.userId =:userId ")
					.append(" and dv.isDeleted = 0 and dvm.isAssociated = 1 and dv.docFlag =:flagValue ");
			if(entityId==null)
				hqlQuery.append(" and dv.uploadedBy.userId =:userId and dv.entityId is null and dv.docSection is null ");
			if(entityId!=null)
				hqlQuery.append(" and dv.entityId=:entityId ");
			if(docSection!=null)
				hqlQuery.append(" and dv.docSection=:docSection ");
		}
		return hqlQuery.toString();

	}
	
	public String createHqlQueryForAllMyDocuments(Long caseseedId,String entityId,String docSection) {
		StringBuilder hqlQuery = new StringBuilder();
		if (caseseedId == null) {
			hqlQuery.append(
					"select new element.bst.elementexploration.rest.document.dto.DocumentVaultDTO(dv.docId, dv.title, ")
					.append("dv.remark, dv.size, dv.type, dv.uploadedOn, dv.uploadedBy.userId, dv.modifiedBy, dv.modifiedOn, dv.docName,concat(dv.uploadedBy.firstName, ' ', dv.uploadedBy.lastName), dv.documentParsingStatus,dv.docSection,dv.entityId,dv.docFlag,dv.entitySource) ")
					.append(" from DocumentVault dv where ")
					.append(" dv.isDeleted = 0 and dv.docFlag = :flagValue ");
			if(entityId==null)
				hqlQuery.append(" and dv.uploadedBy.userId =:userId ");
			if(entityId!=null)
				hqlQuery.append(" and dv.entityId=:entityId ");
			if(docSection!=null)
				hqlQuery.append(" and dv.docSection=:docSection ");

		} else {
			hqlQuery.append(
					"select new element.bst.elementexploration.rest.document.dto.DocumentVaultDTO(dv.docId, dv.title, ")
					.append(" dv.remark, dv.size, dv.type, dv.uploadedOn, dv.uploadedBy.userId, dv.modifiedBy, dv.modifiedOn, dv.docName,concat(dv.uploadedBy.firstName, ' ', dv.uploadedBy.lastName), dv.documentParsingStatus,dv.docSection,dv.entityId,dv.docFlag,dv.entitySource) ")
					.append(" from DocumentVault dv, DocumentVaultCaseMapping dvm where dv.docId = dvm.docId ")
					.append("and dvm.caseId = :caseseedId ")
					//.append(" and dv.uploadedBy.userId = :userId ")
					.append(" and dv.isDeleted = 0 and dvm.isAssociated = 1 and dv.docFlag =:flagValue ");
			if(entityId!=null)
				hqlQuery.append(" and dv.entityId=:entityId ");
			if(docSection!=null)
				hqlQuery.append(" and dv.docSection=:docSection ");
		} 
		return hqlQuery.toString();
	}

	public String createHqlQueryForMyDocuments(Long caseseedId,String entityId,String docSection) {
		StringBuilder hqlQuery = new StringBuilder();
		if (caseseedId == null) {
			hqlQuery.append(
					"select new element.bst.elementexploration.rest.document.dto.DocumentVaultDTO(dv.docId, dv.title, ")
					.append("dv.remark, dv.size, dv.type, dv.uploadedOn, dv.uploadedBy.userId, dv.modifiedBy, dv.modifiedOn, dv.docName,concat(dv.uploadedBy.firstName, ' ', dv.uploadedBy.lastName), dv.documentParsingStatus,dv.docSection,dv.entityId,dv.docFlag,dv.entitySource) ")
					.append(" from DocumentVault dv where ")
					.append(" dv.isDeleted = 0 and dv.docFlag = :flagValue ");
			if(entityId==null)
				hqlQuery.append(" and dv.uploadedBy.userId =:userId and dv.docSection is null ");
			if(entityId!=null)
				hqlQuery.append(" and dv.entityId=:entityId ");
			if(docSection!=null)
				hqlQuery.append(" and dv.docSection=:docSection ");


		} else {
			hqlQuery.append(
					"select new element.bst.elementexploration.rest.document.dto.DocumentVaultDTO(dv.docId, dv.title, ")
					.append(" dv.remark, dv.size, dv.type, dv.uploadedOn, dv.uploadedBy.userId, dv.modifiedBy, dv.modifiedOn, dv.docName,concat(dv.uploadedBy.firstName, ' ', dv.uploadedBy.lastName), dv.documentParsingStatus,dv.docSection,dv.entityId,dv.docFlag,dv.entitySource) ")
					.append(" from DocumentVault dv, DocumentVaultCaseMapping dvm where dv.docId = dvm.docId ")
					.append("and dvm.caseId = :caseseedId ")
					//.append(" and dv.uploadedBy.userId = :userId ")
					.append(" and dv.isDeleted = 0 and dvm.isAssociated = 1 and dv.docFlag =:flagValue ");
			if(entityId==null)
				hqlQuery.append(" and dv.uploadedBy.userId =:userId and dv.docSection is null ");
			if(entityId!=null)
				hqlQuery.append(" and dv.entityId=:entityId ");
			if(docSection!=null)
				hqlQuery.append(" and dv.docSection=:docSection ");
		} 
		return hqlQuery.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentVaultDTO> getAllMySharedDocument(Long userId, Long caseseedId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Integer flagValue) {
		List<DocumentVaultDTO> vaultUserMappingDtos = new ArrayList<>();
		try {
			StringBuilder hqlQuery = new StringBuilder(getAllMySharedDocument(caseseedId));
			if (orderBy != null && orderIn != null && orderBy.trim().length() > 0 && orderIn.trim().length() > 0) {
				hqlQuery.append(" order by dv.").append(orderBy.trim()).append(" ").append(orderIn.toLowerCase());
			}
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("isShared", true);
			query.setParameter("userId", userId);
			query.setParameter("flagValue", flagValue);
			if (caseseedId != null) {
				query.setParameter("seedId", caseseedId);
			}
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			vaultUserMappingDtos = (List<DocumentVaultDTO>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return vaultUserMappingDtos;
	}

	@Override
	public Long countGetAllMySharedDocument(Long userId, Long caseseedId, Integer flagValue) {
		try {
			StringBuilder hqlQuery = new StringBuilder(getAllMySharedDocumentCount(caseseedId));
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("userId", userId);
			query.setParameter("flagValue", flagValue);
			query.setParameter("isShared", true);
			if (caseseedId != null) {
				query.setParameter("seedId", caseseedId);
			}
			Long count = (Long) query.getSingleResult();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, this.getClass());
		}
		return 0L;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentVaultDTO> fullTextSearchDocument(Long userId, String searchKeyWord, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Long caseId, Integer flagValue) {
		List<DocumentVaultDTO> vaultUserMappingDtos = new ArrayList<>();
		try {
			StringBuilder hqlQuery = new StringBuilder(getAllMySharedDocument(caseId));
			if (!StringUtils.isEmpty(searchKeyWord)) {
				hqlQuery.append(" and (dv.docName like '%" + searchKeyWord.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.remark like '%" + searchKeyWord.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.title like '%" + searchKeyWord.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.type like '%" + searchKeyWord.replace("\"", "") + "%')");
			}
			if (orderBy == null && orderIn == null) {
				hqlQuery.append(" order by dv.uploadedOn asc");

			} else if (orderBy != null && orderIn != null && orderBy.trim().length() > 0
					&& orderIn.trim().length() > 0) {
				hqlQuery.append(" order by dv.").append(orderBy.trim()).append(" ").append(orderIn.toLowerCase());
			}
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("isShared", true);
			query.setParameter("userId", userId);
			query.setParameter("flagValue", flagValue);
			if (caseId != null) {
				query.setParameter("seedId", caseId);
			}
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			vaultUserMappingDtos = (List<DocumentVaultDTO>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return vaultUserMappingDtos;
	}

	@Override
	public Long countFullTextSearchDocument(Long userId, String searchKeyWord, Long caseId, Integer flagValue) {
		try {
			StringBuilder hqlQuery = new StringBuilder(getAllMySharedDocumentCount(caseId));
			if (!StringUtils.isEmpty(searchKeyWord)) {
				hqlQuery.append(" and (dv.docName like '%" + searchKeyWord.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.remark like '%" + searchKeyWord.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.title like '%" + searchKeyWord.replace("\"", "") + "%'");
				hqlQuery.append(" or dv.type like '%" + searchKeyWord.replace("\"", "") + "%')");
			}
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("userId", userId);
			query.setParameter("flagValue", flagValue);
			query.setParameter("isShared", true);
			if (caseId != null) {
				query.setParameter("seedId", caseId);
			}
			Long count = (Long) query.getSingleResult();
			return count;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, this.getClass());
		}
		return 0L;
	}

	public String getAllMySharedDocument(Long caseId) {
		StringBuilder hqlQuery = new StringBuilder();
		if (caseId == null) {
			hqlQuery.append(
					"select new element.bst.elementexploration.rest.document.dto.DocumentVaultDTO(dv.docId,dv.title,dv.remark, ")
					.append(" dv.size,dv.type,dv.uploadedOn,dv.uploadedBy.userId,dv.modifiedBy,dv.modifiedOn,dv.docName,concat(dv.uploadedBy.firstName, ' ', dv.uploadedBy.lastName)) ")
					.append("from DocumentVaultUserMapping dumap, DocumentVault dv ,User u where ")
					.append(" (dumap.isShared =:isShared and dumap.userId = :userId) ")
					.append(" and u.userId=dumap.sharedBy ")
					.append(" and dumap.docId = dv.docId and dv.isDeleted = 0 and dv.docFlag =:flagValue ");
		} else {
			hqlQuery.append(
					"select new element.bst.elementexploration.rest.document.dto.DocumentVaultDTO(dv.docId,dv.title,dv.remark,dv.size, ")
					.append(" dv.type,dv.uploadedOn,dv.uploadedBy.userId,dv.modifiedBy,dv.modifiedOn,dv.docName,concat(dv.uploadedBy.firstName, ' ', dv.uploadedBy.lastName)) ")
					.append("from DocumentVaultUserMapping dumap, ")
					.append("DocumentVault dv, DocumentVaultCaseMapping dvm,User u where")
					.append(" dv.docId = dvm.docId and dvm.docId = dumap.docId ")
					.append(" and dvm.caseId = :seedId and u.userId=dumap.sharedBy ")
					.append(" and dumap.isShared =:isShared and dumap.userId = :userId ")
					.append(" and dvm.isAssociated = 1 and dv.isDeleted = 0 and dv.docFlag =:flagValue");
		} 
		return hqlQuery.toString();
	}

	public String getAllMySharedDocumentCount(Long caseId) {
		StringBuilder hqlQuery = new StringBuilder();
		if (caseId == null) {
			hqlQuery.append("select count(*) ").append("from DocumentVaultUserMapping dumap, ")
					.append("DocumentVault dv,User u where")
					.append(" dumap.isShared =:isShared and dumap.userId = :userId ")
					.append(" and u.userId=dumap.sharedBy and dumap.docId = dv.docId and dv.isDeleted = 0 and dv.docFlag =:flagValue ");
		} else {
			hqlQuery.append("select count(*) ").append("from DocumentVaultUserMapping dumap, ")
					.append("DocumentVault dv, DocumentVaultCaseMapping dvm,User u where")
					.append(" dv.docId = dvm.docId and dvm.docId = dumap.docId   and dvm.caseId = :seedId ")
					.append(" and dumap.isShared =:isShared and dumap.userId = :userId and u.userId=dumap.sharedBy and dvm.isAssociated = 1 and dv.isDeleted = 0 and dv.docFlag =:flagValue ");
		}
		return hqlQuery.toString();

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isDocCommentable(long userId, long docId) {
		try {
			StringBuilder builder = new StringBuilder();			
			builder.append(
					"select count(*) from bst_doc_vault as dv where dv.pk_doc_id= :docId and dv.is_deleted= :isDeleted and dv.uploaded_by= :userId  ");
			builder.append("union ");
			builder.append(
					"select count(*) from bst_doc_user_mapping as dvm inner join bst_doc_vault as dv on dvm.fk_doc_id=dv.pk_doc_id and dv.is_deleted= :isDeleted and dvm.fk_doc_id= :docId and dvm.fk_user_id= :userId and dvm.is_shared= :isShared  ");

			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(builder.toString());
			query.setParameter("docId", docId);
			query.setParameter("userId", userId);
			query.setParameter("isShared", true);
			query.setParameter("isDeleted", false);

			List<BigInteger> list = (List<BigInteger>) query.getResultList();
			BigInteger bigInteger = new BigInteger("0");
			// Checking if data found in Owner table or shared mapping table.
			for (BigInteger l : list) {
				if (l.compareTo(bigInteger) != 0)
					return true;
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					this.getClass());
		} 
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> searchUnderwritingDocumentsIdsByName(String keyword) {
		List<Long> docIds = new ArrayList<>();
		try {
			StringBuilder builder = new StringBuilder();			
			builder.append("from DocumentVault docVault where ");
			builder.append("docVault.docFlag = 7 and docVault.docName like '%" + keyword + "%'");
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			List<DocumentVault> documents = (List<DocumentVault>) query.getResultList();
			if(!documents.isEmpty()){
				docIds = documents.stream()
			              .map(DocumentVault::getDocId)
			              .collect(Collectors.toList());
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return docIds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentVault> getDocumentVaultByEntityIdAndDocflag(int docFlag, Long entityId) {
		
		List<DocumentVault> documentsVaultList = new ArrayList<DocumentVault>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select dc from DocumentVault dc where dc.docFlag =:docFlag and dc.entityId =:entityId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("docFlag", docFlag);
			query.setParameter("entityId", entityId.toString());
			documentsVaultList = (List<DocumentVault>) query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return documentsVaultList;
		}
		
		return documentsVaultList;
	}
}
