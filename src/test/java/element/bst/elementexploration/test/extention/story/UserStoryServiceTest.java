package element.bst.elementexploration.test.extention.story;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.story.dao.UserStoryDao;
import element.bst.elementexploration.rest.extention.story.domain.UserStory;
import element.bst.elementexploration.rest.extention.story.dto.StoryDto;
import element.bst.elementexploration.rest.extention.story.serviceImpl.UserStoryServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserStoryServiceTest {
	
	@Mock
	UserStoryDao storyDaoMock;
	
	@InjectMocks
	UserStoryServiceImpl userStoryServiceImpl;
	
	public UserStory userStory;
	
	@Before
	public void setupMock() {
		userStory = new UserStory(1L, 2L, "userId", new Date(), new Date());
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getFavouriteStoryTest(){
		Mockito.when(storyDaoMock.getFavouriteStoryByUserId(Mockito.anyLong())).thenReturn(userStory);
		StoryDto result = userStoryServiceImpl.getFavouriteStory(2L);
		assertEquals("userId", result.getStoryId());
	}
	
	@Test
	public void markAsFavouriteTest() {
		Mockito.when(storyDaoMock.getFavouriteStoryByUserId(Mockito.anyLong())).thenReturn(userStory);
		Mockito.doNothing().when(storyDaoMock).saveOrUpdate(userStory);
		userStoryServiceImpl.markAsFavourite("test", 2L);
		
	}
	
	@Test(expected=Exception.class)
	public void markAsFavourite_whenUserStoryIsNull() {
		Mockito.when(storyDaoMock.getFavouriteStoryByUserId(Mockito.anyLong())).thenReturn(null);
		Mockito.doNothing().when(storyDaoMock).create(Mockito.any(UserStory.class));
		userStoryServiceImpl.markAsFavourite("test", null);
	}
	
}
