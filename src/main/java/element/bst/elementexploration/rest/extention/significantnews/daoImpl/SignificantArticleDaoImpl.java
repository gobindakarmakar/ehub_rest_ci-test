package element.bst.elementexploration.rest.extention.significantnews.daoImpl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantArticleDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantArticles;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Service("significantArticleDao")
public class SignificantArticleDaoImpl extends GenericDaoImpl<SignificantArticles, Long>
		implements SignificantArticleDao {

	public SignificantArticleDaoImpl() {
		super(SignificantArticles.class);
	}

	@Override
	public SignificantArticles getSignificantArticles(String uuid) {
		SignificantArticles significantArticles = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select sa from SignificantArticles sa where sa.uuid = :uuid");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("uuid", uuid);
			significantArticles = (SignificantArticles) query.getSingleResult();
		} catch (Exception e) {
			return significantArticles;
		}
		return significantArticles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SignificantArticles> findArticlesByEntityId(String entityId) {
		List<SignificantArticles> significantArticles = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select sa from SignificantArticles sa where sa.entityId = :entityId and sa.isSignificantNews= 1");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			significantArticles = (List<SignificantArticles>) query.getResultList();
		} catch (Exception e) {
			return significantArticles;
		}
		return significantArticles;
	}
}
