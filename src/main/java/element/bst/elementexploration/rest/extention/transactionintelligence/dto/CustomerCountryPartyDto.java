package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;

public class CustomerCountryPartyDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private InputStatsResponseDto inputStatsResponseDto;
	
	private List<AlertedCounterPartyDto> alertedCounterPartyDto;
	
	private CustomerDetails customerDetails;

	public CustomerCountryPartyDto() {
		super();
	}

	public CustomerCountryPartyDto(InputStatsResponseDto inputStatsResponseDto,
			List<AlertedCounterPartyDto> alertedCounterPartyDto, CustomerDetails customerDetails) {
		super();
		this.inputStatsResponseDto = inputStatsResponseDto;
		this.alertedCounterPartyDto = alertedCounterPartyDto;
		this.customerDetails = customerDetails;
	}

	public InputStatsResponseDto getInputStatsResponseDto() {
		return inputStatsResponseDto;
	}

	public void setInputStatsResponseDto(InputStatsResponseDto inputStatsResponseDto) {
		this.inputStatsResponseDto = inputStatsResponseDto;
	}

	public List<AlertedCounterPartyDto> getAlertedCounterPartyDto() {
		return alertedCounterPartyDto;
	}

	public void setAlertedCounterPartyDto(List<AlertedCounterPartyDto> alertedCounterPartyDto) {
		this.alertedCounterPartyDto = alertedCounterPartyDto;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

}
