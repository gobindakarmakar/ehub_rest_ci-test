package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;
import element.bst.elementexploration.rest.usermanagement.group.dto.CreateGroupDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupListDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupStatusDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersListDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "User group API" }, description = "Manages application's user groups")
@RestController
@RequestMapping("/api/group")
public class UserGroupController extends BaseController {

	@Autowired
	private GroupService groupService;

	@Autowired
	private UserGroupService userGroupService;

	@Autowired
	private UserService userService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@ApiOperation("Creates a user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_CREATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.GROUP_NAME_EXIST),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/create", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createGroup(@RequestParam("token") String token,
			@Valid @RequestBody CreateGroupDto createGroupDto, BindingResult results, HttpServletRequest request) {
		Long adminId = getCurrentUserId();
		GroupDto group = new GroupDto();
		BeanUtils.copyProperties(createGroupDto, group);
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			if (!results.hasErrors()) {
				Group groupTemp = groupService.getGroup(group.getName());
				if (groupTemp == null) {
					Group newGroup = new Group();
					newGroup.setActive(1);
					newGroup.setCreatedBy(adminId);
					newGroup.setCreatedDate(new Date());
					newGroup.setName(group.getName());
					newGroup.setRemarks(group.getRemarks());
					newGroup.setDescription(group.getDescription());
					newGroup = groupService.save(newGroup);
					eventPublisher.publishEvent(new LoggingEvent(newGroup.getUserGroupId(),
							LoggingEventType.CREATE_GROUP, adminId, new Date()));
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_CREATED_SUCCESSFULLY_MSG),
							HttpStatus.OK);
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_NAME_EXIST),
							HttpStatus.CONFLICT);
				}
			} else {
				throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Lists user groups")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserGroupListDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/groupList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserGroups(@RequestParam("token") String token,
			@RequestParam(required = false) Integer status, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderIn,
			@RequestParam(required = false) String orderBy, HttpServletRequest request) {

		List<Group> groupList = groupService.getUserGroups(status, null, pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = groupService.countUserGroups(status, null);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(groupList != null ? groupList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		UserGroupListDto userGroupListDto = new UserGroupListDto();
		userGroupListDto.setResult(groupList);
		userGroupListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * groupList); jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userGroupListDto, HttpStatus.OK);
	}

	@ApiOperation("Performs full text search operation on user groups")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserGroupListDto.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/groupListFullTextSearch", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> userGroupFullTextSearch(@RequestParam("token") String token,
			@RequestParam(required = false) Integer status, @RequestParam("keyword") String keyword,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		if (StringUtils.isEmpty(keyword) || keyword.length() < 3) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_STRING_LENTH_INVALID),
					HttpStatus.OK);
		}

		List<Group> groupList = groupService.getUserGroups(status, keyword, pageNumber, recordsPerPage, orderBy,
				orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = groupService.countUserGroups(status, keyword);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(groupList != null ? groupList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		UserGroupListDto userGroupListDto = new UserGroupListDto();
		userGroupListDto.setResult(groupList);
		userGroupListDto.setPaginationInformation(information);
		;
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * groupList); jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userGroupListDto, HttpStatus.OK);
	}

	@ApiOperation("Gets a user group by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = Group.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<Group> getGroup(@RequestParam("groupId") Long groupId, @RequestParam("token") String userId,
			HttpServletRequest request) {

		Group group = groupService.find(groupId);
		if (group != null)
			return new ResponseEntity<Group>(group, HttpStatus.OK);
		else
			throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
	}

	@ApiOperation("Updates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.GROUP_NAME_EXIST),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/update", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateGroup(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String adminToken, @Valid @RequestBody GroupDto updatedGroup, BindingResult results,
			HttpServletRequest request) {

		Long adminId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Group existing = groupService.checkGroupExist(updatedGroup.getName(), groupId);
			if (existing == null) {
				Group group = groupService.find(groupId);
				if (group != null) {
					group.setName(updatedGroup.getName());
					group.setDescription(updatedGroup.getDescription());
					group.setRemarks(updatedGroup.getRemarks());
					group.setModifiedDate(new Date());
					groupService.saveOrUpdate(group);

					return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG),
							HttpStatus.OK);
				} else {
					throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_NAME_EXIST),
						HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Activates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_ACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/activate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> activateGroup(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {
		long adminId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Group group = groupService.find(groupId);
			if (group != null) {
				group.setActive(1);
				groupService.saveOrUpdate(group);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_ACTIVATED_SUCCESSFULLY_MSG),
						HttpStatus.OK);
			} else {
				throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Deactivates a user group by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_DEACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/deactivate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deactivateGroup(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {
		long adminId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Group group = groupService.find(groupId);
			if (group != null) {
				group.setActive(0);
				groupService.saveOrUpdate(group);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_DEACTIVATED_SUCCESSFULLY_MSG),
						HttpStatus.OK);
			} else {
				throw new NoDataFoundException(ElementConstants.GROUP_NOT_FOUND_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Adds a user to an user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.USER_OR_GROUP_IS_INACTIVE_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/addUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addUsersGroups(@RequestParam("userId") Long userId, @RequestParam("groupId") Long groupId,
			@RequestParam("token") String token, HttpServletRequest request) {

		long adminId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			User user = userService.find(userId);
			if (user != null) {
				Group group = groupService.find(groupId);
				if (group != null) {
					if (userGroupService.getUserGroup(userId, groupId) == null) {
						if (userService.isActiveUser(userId) && groupService.isActiveGroup(groupId)) {
							UserGroup userGroup = new UserGroup();
							userGroup.setGroupId(groupId);
							userGroup.setUserId(userId);
							userGroupService.save(userGroup);
							return new ResponseEntity<>(
									new ResponseMessage(ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG),
									HttpStatus.OK);
						} else {
							return new ResponseEntity<>(
									new ResponseMessage(ElementConstants.USER_OR_GROUP_IS_INACTIVE_MSG),
									HttpStatus.BAD_REQUEST);
						}
					} else {
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_ALREADY_IN_GROUP_MSG),
								HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.GROUP_NOT_FOUND_MSG),
							HttpStatus.NOT_FOUND);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG),
						HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Removes a user from an user group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.USER_OR_GROUP_IS_INACTIVE_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/removeUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> removeUsersGroups(@RequestParam("token") String token,
			@RequestParam("groupId") Long groupId, @RequestParam("userId") Long userId, HttpServletRequest request) {
		long adminId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			UserGroup userGroupBeforeChange = userGroupService.getUserGroup(userId, groupId);
			if (userGroupBeforeChange != null) {
				userGroupService.delete(userGroupBeforeChange);
			}
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_REMOVED_FROM_GROUP_SUCCESSFULLY_MSG),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Lists the users of an user group")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersListDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/listUsersByGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUsersFromGroup(@RequestParam("groupId") Long groupId,
			@RequestParam("token") String token, @RequestParam(required = false) Integer userStatus,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		long adminId = getCurrentUserId();
		if (groupService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			// Retrieve all users from a particular group.
			List<UserDto> userList = userService.listUserByGroup(groupId, userStatus, pageNumber, recordsPerPage,
					orderBy, orderIn);

			// Pagination
			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = userService.countUserByGroup(groupId, userStatus);
			PaginationInformation information = new PaginationInformation();
			information.setTotalResults(totalResults);
			information.setKind("list");
			information.setTitle(request.getRequestURI());
			information.setCount(userList != null ? userList.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");

			UsersListDto usersListDto = new UsersListDto();
			usersListDto.setResult(userList);
			usersListDto.setPaginationInformation(information);
			/*
			 * JSONObject jsonObject = new JSONObject();
			 * jsonObject.put("result", userList);
			 * jsonObject.put("paginationInformation", information);
			 */
			return new ResponseEntity<>(usersListDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Lists the user groups")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UsersListDto.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/listUserGroups", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupsFromUser(@RequestParam("token") String token, @RequestParam("userId") Long userId,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		// Retrieve all groups for a particular user.
		List<Group> groupList = groupService.getGroupsOfUser(userId, pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = groupService.countGroupsOfUser(userId);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setKind("list");
		information.setTitle(request.getRequestURI());
		information.setCount(groupList != null ? groupList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		UserGroupListDto userGroupListDto = new UserGroupListDto();
		userGroupListDto.setResult(groupList);
		userGroupListDto.setPaginationInformation(information);
		;
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * groupList); jsonObject.put("paginationInformation", information);
		 */

		return new ResponseEntity<>(userGroupListDto, HttpStatus.OK);
	}

	@ApiOperation("Checks if the user group is active")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UserGroupStatusDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG
					+ " or " + ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/checkActiveUserGroup", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> checkActiveUserGroup(@RequestParam("userId") Long userId,
			@RequestParam("token") String token, @RequestParam("groupId") Long groupId, HttpServletRequest request) {

		boolean status = false;
		if (userService.isActiveUser(userId) && groupService.isActiveGroup(groupId)) {
			UserGroup userGroup = userGroupService.getUserGroup(userId, groupId);
			if (userGroup != null)
				status = true;
		}
		UserGroupStatusDto userGroupStatusDto = new UserGroupStatusDto();
		userGroupStatusDto.setStatus(status);
		/*
		 * JsonObject responseObject = new JsonObject();
		 * responseObject.addProperty("status", status);
		 */
		return new ResponseEntity<>(userGroupStatusDto, HttpStatus.OK);
	}

}
