package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceDomainDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("sourceDomainDao")
public class SourceDomainDaoImpl extends GenericDaoImpl<SourceDomain, Long> implements SourceDomainDao {

	public SourceDomainDaoImpl() {
		super(SourceDomain.class);
	}

	@Override
	public SourceDomain fetchDomain(String name) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SourceDomain s where s.domainName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", name);
			SourceDomain sourcesDomain = (SourceDomain) query.getSingleResult();
			if (sourcesDomain != null)
				return sourcesDomain;
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourceDomainDaoImpl.class);
			e.printStackTrace();
			throw new FailedToExecuteQueryException();
		}
		return null;
	}

}
