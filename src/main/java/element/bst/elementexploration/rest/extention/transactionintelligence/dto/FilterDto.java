package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Filters")
public class FilterDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Transaction type for filter")
	private String transactionType;
	
	@ApiModelProperty(value = "Is CorporateStructure for filter",example="true/false")
	@JsonProperty
	private boolean isCorporateStructure;
	
	@ApiModelProperty(value = "Is shareholder for filter",example="true/false")
	@JsonProperty
	private boolean isShareholder;

	public boolean isShareholder() {
		return isShareholder;
	}

	public void setShareholder(boolean isShareholder) {
		this.isShareholder = isShareholder;
	}

	@ApiModelProperty(value = "Corporate structure type for filter")
	private String corporateStructureType;
	
	@ApiModelProperty(value = "Customer name for filter")
	private String customerName;
	
	@ApiModelProperty(value = "Scenario for filter")
	private String scenario;
	
	@ApiModelProperty(value = "Country for filter")
	private String country;

	@ApiModelProperty(value = "Shareholder type for filter")
	private String shareholderType;
	
	@ApiModelProperty(value = "Transaction type for filter")
	@JsonProperty
	private boolean isAlertByStatusGreaterThan30Days;
	
	@ApiModelProperty(value = "Is alerts by period greater than 30 days for filter",example="true/false")
	@JsonProperty
	private boolean isAlertsByPeriodgreaterthan30Days;
	
	@ApiModelProperty(value = "Is alerts by period 10 to 20 days for filter",example="true/false")
	@JsonProperty
	private boolean isAlertsByPeriod10to20Days;
	
	@ApiModelProperty(value = "Is alerts by period 20 to 300 days for filter",example="true/false")
	@JsonProperty
	private boolean isAlertsByPeriod20to30Days;
	
	@ApiModelProperty(value = "Is input transaction for filter",example="true/false")
	@JsonProperty
	private boolean isInput;
	
	@ApiModelProperty(value = "Is output transaction for filter",example="true/false")
	@JsonProperty
	private boolean isOutput;
	
	@ApiModelProperty(value = "Is corporate country transaction for filter",example="true/false")
	@JsonProperty
	private boolean isCorporateGeography;
	private String corporateGeographyType;
	
	@ApiModelProperty(value = "Bank name for filter")
	private String bankName;
	
	@ApiModelProperty(value = "Bank location for filter")
	private String bankLocations;
	
	@ApiModelProperty(value = "Is product risk tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isProductRisk;
	
	@ApiModelProperty(value = "Is customer risk tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isCustomerRiskType;
	
	@ApiModelProperty(value = "Is geographic risk tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isGeographyRiskType;
	
	@ApiModelProperty(value = "Product risk type for filter")
	private String productRiskType;

	@ApiModelProperty(value = "Customer risk type for filter")
	private String customerRisk;
	
	@ApiModelProperty(value = "Geo graphic risk type for filter")
	private String geoRiskType;
	
	@ApiModelProperty(value = "Corruption risk type for filter")
	private String corruptionRiskType;
	
	@ApiModelProperty(value = "Political risk type for filter")
	private String politicalRiskType;
	
	@ApiModelProperty(value = "Is activity type customer tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isActivityType;
	
	@ApiModelProperty(value = "Activitiy type for filter")
	private String activityCategory;

	@ApiModelProperty(value = "Is counterparty country tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isCounterPartyCountry;
	
	@ApiModelProperty(value = "Country party country for filter")
	private String counterpartyCountry;

	@ApiModelProperty(value = "Is corrupution risk country tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isCorruptionRisk;

	@ApiModelProperty(value = "Is political risk country tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isPoliticalRisk;

	@ApiModelProperty(value = "Is counter party bank tarnsaction for filter",example="true/false")
	@JsonProperty
	private boolean isBank;
	
	@ApiModelProperty(value = "Is counter party bank location for filter",example="true/false")
	@JsonProperty
	private boolean isBankLocation;

	@ApiModelProperty(value = "Country party name for filter")
	private String counterPartyNames;
	
	@ApiModelProperty(value = "Is input country transactions for filter",example="true/false")
	private boolean isInputCountry;
	
	@ApiModelProperty(value = "Is output country transactions for filter",example="true/false")
	private boolean isOutputCountry;
	
	@ApiModelProperty(value = "Alert status for filter")
	private String alertStatus;
	
	@ApiModelProperty(value = "Is transactions done between one to five million usd for filter",example="true/false")
	@JsonProperty
	private boolean isBetweenOneToFiveMillionUSD;
	
	@ApiModelProperty(value = "Is transactions done between five to ten million usd for filter",example="true/false")
	@JsonProperty
	private boolean isBetweenFiveToTenMillionUSD;
	
	@ApiModelProperty(value = "Is transactions done above ten million usd for filter",example="true/false")
	@JsonProperty
	private boolean isAboveTenMillionUSD;
	
	@ApiModelProperty(value = "Is transactions done below one million usd for filter",example="true/false")
	@JsonProperty
	private boolean isBelowOneMillionUSD;

	public String getTransactionType() {
		return transactionType;
	}

	public boolean isBetweenOneToFiveMillionUSD() {
		return isBetweenOneToFiveMillionUSD;
	}

	public void setBetweenOneToFiveMillionUSD(boolean isBetweenOneToFiveMillionUSD) {
		this.isBetweenOneToFiveMillionUSD = isBetweenOneToFiveMillionUSD;
	}

	public boolean isBetweenFiveToTenMillionUSD() {
		return isBetweenFiveToTenMillionUSD;
	}

	public void setBetweenFiveToTenMillionUSD(boolean isBetweenFiveToTenMillionUSD) {
		this.isBetweenFiveToTenMillionUSD = isBetweenFiveToTenMillionUSD;
	}

	public boolean isAboveTenMillionUSD() {
		return isAboveTenMillionUSD;
	}

	public void setAboveTenMillionUSD(boolean isAboveTenMillionUSD) {
		this.isAboveTenMillionUSD = isAboveTenMillionUSD;
	}

	public boolean isBelowOneMillionUSD() {
		return isBelowOneMillionUSD;
	}

	public void setBelowOneMillionUSD(boolean isBelowOneMillionUSD) {
		this.isBelowOneMillionUSD = isBelowOneMillionUSD;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public boolean isCorporateStructure() {
		return isCorporateStructure;
	}

	public void setCorporateStructure(boolean isCorporateStructure) {
		this.isCorporateStructure = isCorporateStructure;
	}

	public String getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(String alertStatus) {
		this.alertStatus = alertStatus;
	}

	public String getCorporateStructureType() {
		return corporateStructureType;
	}

	public void setCorporateStructureType(String corporateStructureType) {
		this.corporateStructureType = corporateStructureType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getShareholderType() {
		return shareholderType;
	}

	public void setShareholderType(String shareholderType) {
		this.shareholderType = shareholderType;
	}

	public boolean isAlertByStatusGreaterThan30Days() {
		return isAlertByStatusGreaterThan30Days;
	}

	public void setAlertByStatusGreaterThan30Days(boolean isAlertByStatusGreaterThan30Days) {
		this.isAlertByStatusGreaterThan30Days = isAlertByStatusGreaterThan30Days;
	}

	public boolean isAlertsByPeriodgreaterthan30Days() {
		return isAlertsByPeriodgreaterthan30Days;
	}

	public void setAlertsByPeriodgreaterthan30Days(boolean isAlertsByPeriodgreaterthan30Days) {
		this.isAlertsByPeriodgreaterthan30Days = isAlertsByPeriodgreaterthan30Days;
	}

	public boolean isAlertsByPeriod10to20Days() {
		return isAlertsByPeriod10to20Days;
	}

	public void setAlertsByPeriod10to20Days(boolean isAlertsByPeriod10to20Days) {
		this.isAlertsByPeriod10to20Days = isAlertsByPeriod10to20Days;
	}

	public boolean isAlertsByPeriod20to30Days() {
		return isAlertsByPeriod20to30Days;
	}

	public void setAlertsByPeriod20to30Days(boolean isAlertsByPeriod20to30Days) {
		this.isAlertsByPeriod20to30Days = isAlertsByPeriod20to30Days;
	}

	public boolean isInput() {
		return isInput;
	}

	public void setInput(boolean isInput) {
		this.isInput = isInput;
	}

	public boolean isOutput() {
		return isOutput;
	}

	public void setOutput(boolean isOutput) {
		this.isOutput = isOutput;
	}

	public String getProductRiskType() {
		return productRiskType;
	}

	public void setProductRiskType(String productRiskType) {
		this.productRiskType = productRiskType;
	}

	public String getCustomerRisk() {
		return customerRisk;
	}

	public void setCustomerRisk(String customerRisk) {
		this.customerRisk = customerRisk;
	}

	public String getGeoRiskType() {
		return geoRiskType;
	}

	public void setGeoRiskType(String geoRiskType) {
		this.geoRiskType = geoRiskType;
	}

	public String getCorruptionRiskType() {
		return corruptionRiskType;
	}

	public void setCorruptionRiskType(String corruptionRiskType) {
		this.corruptionRiskType = corruptionRiskType;
	}

	public String getPoliticalRiskType() {
		return politicalRiskType;
	}

	public void setPoliticalRiskType(String politicalRiskType) {
		this.politicalRiskType = politicalRiskType;
	}

	public boolean isCorporateGeography() {
		return isCorporateGeography;
	}

	public void setCorporateGeography(boolean isCorporateGeography) {
		this.isCorporateGeography = isCorporateGeography;
	}

	public String getCorporateGeographyType() {
		return corporateGeographyType;
	}

	public void setCorporateGeographyType(String corporateGeographyType) {
		this.corporateGeographyType = corporateGeographyType;
	}

	public boolean isActivityType() {
		return isActivityType;
	}

	public void setActivityType(boolean isActivityType) {
		this.isActivityType = isActivityType;
	}

	public String getActivityCategory() {
		return activityCategory;
	}

	public void setActivityCategory(String activityCategory) {
		this.activityCategory = activityCategory;
	}

	public boolean isCounterPartyCountry() {
		return isCounterPartyCountry;
	}

	public void setCounterPartyCountry(boolean isCounterPartyCountry) {
		this.isCounterPartyCountry = isCounterPartyCountry;
	}

	public String getCounterpartyCountry() {
		return counterpartyCountry;
	}

	public void setCounterpartyCountry(String counterpartyCountry) {
		this.counterpartyCountry = counterpartyCountry;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankLocations() {
		return bankLocations;
	}

	public void setBankLocations(String bankLocations) {
		this.bankLocations = bankLocations;
	}

	public boolean isProductRisk() {
		return isProductRisk;
	}

	public void setProductRisk(boolean isProductRisk) {
		this.isProductRisk = isProductRisk;
	}

	public boolean isCustomerRiskType() {
		return isCustomerRiskType;
	}

	public void setCustomerRiskType(boolean isCustomerRiskType) {
		this.isCustomerRiskType = isCustomerRiskType;
	}

	public boolean isGeographyRiskType() {
		return isGeographyRiskType;
	}

	public void setGeographyRiskType(boolean isGeographyRiskType) {
		this.isGeographyRiskType = isGeographyRiskType;
	}

	public boolean isBank() {
		return isBank;
	}

	public void setBank(boolean isBank) {
		this.isBank = isBank;
	}

	public boolean isCorruptionRisk() {
		return isCorruptionRisk;
	}

	public void setCorruptionRisk(boolean isCorruptionRisk) {
		this.isCorruptionRisk = isCorruptionRisk;
	}

	public boolean isPoliticalRisk() {
		return isPoliticalRisk;
	}

	public void setPoliticalRisk(boolean isPoliticalRisk) {
		this.isPoliticalRisk = isPoliticalRisk;
	}

	public boolean isBankLocation() {
		return isBankLocation;
	}
	
	
	
	

	public boolean isInputCountry() {
		return isInputCountry;
	}

	public void setInputCountry(boolean isInputCountry) {
		this.isInputCountry = isInputCountry;
	}

	public boolean isOutputCountry() {
		return isOutputCountry;
	}

	public void setOutputCountry(boolean isOutputCountry) {
		this.isOutputCountry = isOutputCountry;
	}

	public void setBankLocation(boolean isBankLocation) {
		this.isBankLocation = isBankLocation;
	}

	public String getCounterPartyNames() {
		return counterPartyNames;
	}

	public void setCounterPartyNames(String counterPartyNames) {
		this.counterPartyNames = counterPartyNames;
	}

}
