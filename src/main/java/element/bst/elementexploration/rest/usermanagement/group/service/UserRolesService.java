package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDtoWithRolesObj;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author Paul Jalagari
 *
 */
public interface UserRolesService  extends GenericService<UserRoles, Long> {

	List<UserRoles> getRolesOfUser(Long userId) throws Exception;

	List<UserRolesDto> getRolesOfUserDto(Long userId) throws Exception;

	boolean saveUserRoles(Users user, List<Long> roleIds, Long currentUserId,String action) throws Exception;
	
	Map<Long, Long> rolesWithUserCount();
	
	boolean removeUserRoles(Users user, List<Long> roleIdsToBeRemoved, Long currentUserId,String action) throws Exception;

	List<UserRolesDtoWithRolesObj> getRolesOfUserDtoWithRoleObj(Long userId, Boolean isGroupRolesRequired)  throws Exception;

	Map<Long, Long> getActiveUserNRoleCount() throws Exception ;

	Object getStatuswiseUsersByRoleId(Long roleId);

	boolean unassignUserFromRole(Long roleId, Long userId, Long currentUserId);

	Map<Long, Long> getUsersCountByRole(Long roleId);
	
	boolean isUserRolesExist(Long userId, Long roleId);


}
