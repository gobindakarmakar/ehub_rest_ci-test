package element.bst.elementexploration.rest.document.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * 
 * @author Paul Jalagari
 *
 */
@Entity
@Table(name = "bst_document_vault")
@Access(AccessType.FIELD)
public class DocumentsVault implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3865886880458366812L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_doc_id")
	private Long docId;

	@Column(name = "title")
	private String title;

	@Column(name = "remark")
	private String remark;

	@Column(name = "size")
	private Long size;

	@Column(name = "type")
	private String type;

	@Column(name = "isS3Storage")
	private Boolean isS3Storage;

	@Column(name = "uploaded_on")
	private Date uploadedOn;

	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uploaded_by")
	private Users uploadedBy;*/

	@Column(name = "doc_guuid_from_server")
	private String docGuuidFromServer;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@Column(name = "modified_by")
	private Long modifiedBy;

	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "docName")
	private String docName;

	// store value between 1-10
	@Column(name = "doc_flag")
	private Integer docFlag;

	@Column(name = "doc_parsing_status")
	private String documentParsingStatus;

	@Column(name = "case_id")
	private Long caseId;

	@Column(name = "doc_structure_type")
	private Integer docStructureType;

	@Column(name = "entity_id")
	private String entityId;

	@Column(name = "entity_name")
	private String entityName;

	@Column(name = "entity_source")
	private String entitySource;

	@Column(name = "doc_section")
	private String docSection;

	@Column(name = "annual_url")
	private String annualUrl;

	// Don't change this constructor.
	public DocumentsVault(Long docId, String docName, String title, String remark, String type, Long size,
			Date uploadedOn) {
		this.docId = docId;
		this.docName = docName;
		this.title = title;
		this.remark = remark;
		this.type = type;
		this.size = size;
		this.uploadedOn = uploadedOn;
	}

	public DocumentsVault(Long docId, String title, String remark, Long size, String type, Date uploadedOn,
			Users uploadedBy, String docGuuidFromServer, Boolean isDeleted, Long modifiedBy, Date modifiedOn,
			String docName, Integer docFlag, String documentParsingStatus, Integer docStructureType) {
		super();
		this.docId = docId;
		this.title = title;
		this.remark = remark;
		this.size = size;
		this.type = type;
		this.uploadedOn = uploadedOn;
		//this.uploadedBy = uploadedBy;
		this.docGuuidFromServer = docGuuidFromServer;
		this.isDeleted = isDeleted;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.docName = docName;
		this.docFlag = docFlag;
		this.documentParsingStatus = documentParsingStatus;
		this.docStructureType = docStructureType;
	}

	/**
	 * @return the docName
	 */
	public String getDocName() {
		return docName;
	}

	/**
	 * @param docName
	 *            the docName to set
	 */
	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getEntitySource() {
		return entitySource;
	}

	public void setEntitySource(String entitySource) {
		this.entitySource = entitySource;
	}

	public DocumentsVault() {
		// TODO Auto-generated constructor stub
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	/*public Users getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(Users uploadedBy) {
		this.uploadedBy = uploadedBy;
	}*/

	public String getDocGuuidFromServer() {
		return docGuuidFromServer;
	}

	public void setDocGuuidFromServer(String docGuuidFromServer) {
		this.docGuuidFromServer = docGuuidFromServer;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Integer getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(Integer docFlag) {
		this.docFlag = docFlag;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDocumentParsingStatus() {
		return documentParsingStatus;
	}

	public void setDocumentParsingStatus(String documentParsingStatus) {
		this.documentParsingStatus = documentParsingStatus;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Integer getDocStructureType() {
		return docStructureType;
	}

	public void setDocStructureType(Integer docStructureType) {
		this.docStructureType = docStructureType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getDocSection() {
		return docSection;
	}

	public void setDocSection(String docSection) {
		this.docSection = docSection;
	}

	public String getAnnualUrl() {
		return annualUrl;
	}

	public void setAnnualUrl(String annualUrl) {
		this.annualUrl = annualUrl;
	}

	public Boolean getIsS3Storage() {
		return isS3Storage;
	}

	public void setIsS3Storage(Boolean isS3Storage) {
		this.isS3Storage = isS3Storage;
	}
}
