package element.bst.elementexploration.rest.extention.entity.complexStructure.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.entity.complexStructure.dao.EntityComplexStructureDao;
import element.bst.elementexploration.rest.extention.entity.complexStructure.domain.EntityComplexStructure;
import element.bst.elementexploration.rest.extention.entity.complexStructure.dto.EntityComplexStructureDto;
import element.bst.elementexploration.rest.extention.entity.complexStructure.service.EntityComplexStructureService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author hanuman
 *
 */
@Service("entityComplexStructureService")
@Transactional("transactionManager")
public class EntityComplexStructureServiceImpl extends GenericServiceImpl<EntityComplexStructure, Long> implements 
EntityComplexStructureService {

	public EntityComplexStructureServiceImpl() {

	}

	@Autowired
	EntityComplexStructureDao entityComplexStructureDao;

	@Autowired
	private AuditLogDao auditLogDao;

	@Autowired
	public EntityComplexStructureServiceImpl(GenericDao<EntityComplexStructure, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	EntityComplexStructureService entityComplexStructureService;

	@Override
	public List<EntityComplexStructureDto> getAllEntityComplexStructure() {
		List<EntityComplexStructure> entityComplexStructureList = entityComplexStructureService.findAll();
		List<EntityComplexStructureDto> entityComplexStructureDtosList = new ArrayList<EntityComplexStructureDto>();
		if (entityComplexStructureList != null) {
			entityComplexStructureDtosList = entityComplexStructureList.stream().map(
					(entityComplexStructureDto) -> new EntityConverter<EntityComplexStructure, EntityComplexStructureDto>(EntityComplexStructure.class, EntityComplexStructureDto.class)
					.toT2(entityComplexStructureDto, new EntityComplexStructureDto()))
					.collect(Collectors.toList());
		}
		return entityComplexStructureDtosList;
	}

	@Override
	public EntityComplexStructureDto getEntityComplexStructure(String entityId,String entitySource,String entityName) {
		EntityComplexStructure entityComplexStructure = entityComplexStructureDao.getEntityComplexStructure(entityId, entitySource,entityName);
		EntityComplexStructureDto entityComplexStructureDto=null;
		if (entityComplexStructure != null) {
			entityComplexStructureDto = new EntityComplexStructureDto();
			BeanUtils.copyProperties(entityComplexStructure, entityComplexStructureDto);
		} 
		return entityComplexStructureDto;
	}

	@Override
	@Transactional("transactionManager")
	public EntityComplexStructureDto saveEntityComplexStructure(EntityComplexStructureDto entityComplexStructureDto) throws Exception {
		EntityComplexStructure complexStructure =null;
		EntityComplexStructureDto complexStructureDto=null;
		if (entityComplexStructureDto != null) {
			complexStructure =	entityComplexStructureDao.getEntityComplexStructure(entityComplexStructureDto.getEntityId(), entityComplexStructureDto.getEntitySource(),entityComplexStructureDto.getEntityName());
			if(complexStructure ==null) {

				complexStructure = new EntityComplexStructure();
				BeanUtils.copyProperties(entityComplexStructureDto, complexStructure);
				complexStructure=entityComplexStructureDao.create(complexStructure);
				return getEntityComplexStructure(complexStructure.getEntityId(),complexStructure.getEntitySource(),complexStructure.getEntityName());
			}
		}
		return complexStructureDto;
	}

	@Override
	public EntityComplexStructureDto saveOrUpdateEntityComplexStructure(String entityId, String entityName,String entitySource,boolean isComplexStructure,Long userId)
			throws Exception {

		EntityComplexStructureDto entityComplexStructureDto = new EntityComplexStructureDto();
		EntityComplexStructure complexStructure=entityComplexStructureDao.getEntityComplexStructure(entityId, entitySource,entityName);
		if(complexStructure!=null) {
			complexStructure.setIsComplexStructure(isComplexStructure);
		}else {
			complexStructure= new EntityComplexStructure();
			complexStructure.setEntityId(entityId);
			complexStructure.setEntitySource(entitySource);
			complexStructure.setIsComplexStructure(isComplexStructure);
			complexStructure.setEntityName(entityName);
		}
		entityComplexStructureDao.saveOrUpdate(complexStructure);
		BeanUtils.copyProperties(complexStructure, entityComplexStructureDto);
		
		String complexStructureBtn="OFF";

		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Modify Entity Complex Structure");
		if (entityComplexStructureDto.getEntityName() != null)
			if(entityComplexStructureDto.getIsComplexStructure()) {
				complexStructureBtn="ON";
			}
		log.setDescription("Entity "+entityComplexStructureDto.getEntityName() +" Complex Structure is "+ complexStructureBtn);
		log.setUserId(userId);
		//log.setEntityId(entityComplexStructureDto.getEntityId().toString());
		log.setName(entityComplexStructureDto.getEntityName());
		auditLogDao.create(log);

		return entityComplexStructureDto;
	}

}
