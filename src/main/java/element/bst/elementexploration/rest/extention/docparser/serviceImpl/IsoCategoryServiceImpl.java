package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentAnswersDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplateMappingDao;
import element.bst.elementexploration.rest.extention.docparser.dao.IsoCategoryDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.extention.docparser.dto.Level1CodeDto;
import element.bst.elementexploration.rest.extention.docparser.service.IsoCategoryService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
/**
 * @author rambabu
 *
 */
@Service("isoCategoryService")
public class IsoCategoryServiceImpl extends GenericServiceImpl<IsoCategory, Long> implements IsoCategoryService {
	
	@Autowired
	IsoCategoryDao isoCategoryDao;
	
	@Autowired
	CaseDao caseDao;
	
	@Autowired
	DocumentAnswersDao documentAnswersDao;
	
	@Autowired
	DocumentTemplateMappingDao documentTemplateMappingDao;
	
	@Value("${iso_category_evaluation_url}")
	private String ISO_CATEGOEY_URL;
	
	@Value("${iso_category_url}")
	private String ISOCODE_CATEGORY_URL;

	public IsoCategoryServiceImpl(GenericDao<IsoCategory, Long> genericDao) {
		super(genericDao);		
	}

	@Override
	@Transactional("transactionManager")
	public int updateIsoCategoryEvaluation(IsoCategory isoCategory, String evaluation, long userIdTemp) {
		int status=0;
		if(evaluation != null || evaluation != ""){
			isoCategory.setEvaluation(evaluation);
			isoCategory.setEvaluationUpdatedTime(new Date());
			isoCategory.setEvaluationUpdatedBy(userIdTemp);
			isoCategoryDao.saveOrUpdate(isoCategory);
			
			status=201;
		}
		return status;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public void storeIsoCategoryEvaluation(Long docId) {
		try{
		List<IsoCategory> isoCategoryList=isoCategoryDao.getISOCategoriesByDocId(docId);
		Case caseSeed=caseDao.getCaseByDocId(docId);
		if(caseSeed!=null){
			JSONObject finalObject=new JSONObject();
			JSONObject entities=new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray entitiesArray = new JSONArray();			
			JSONObject evaluation=new JSONObject();
		for (IsoCategory isoCategory : isoCategoryList) {
				String code=isoCategory.getLevel1Code();
				String newCode=code.replace(".", "");					
				String title=isoCategory.getLevel1Title().replace(" ", "_");
				String codeTitle=newCode+"_"+title;
				if(isoCategory.getEvaluation() != null){
				String evaluationValue = isoCategory.getEvaluation().contains("_")?isoCategory.getEvaluation().replaceAll("_"," "):isoCategory.getEvaluation();
				evaluation.put(codeTitle, evaluationValue);
				}
		}
		jsonArray.put(evaluation);
		
		entities.put("caseID", caseSeed.getCaseId());		
		
		entities.put("entityID", caseSeed.getDecisionScoringEntityId());
		if(caseSeed.getType().equalsIgnoreCase("Corporate-KYC"))
			entities.put("entityType", "Company");	
		else
			entities.put("entityType", "Person");			
			
		entities.put("attributes", jsonArray);
		entitiesArray.put(entities);
		finalObject.put("entities", entitiesArray);
		
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(ISO_CATEGOEY_URL, finalObject.toString());
			
		
		}
		
		}catch (Exception e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
	}

	@Override
	@Transactional("transactionManager")
	public Level1CodeDto updateIsoCategoryEvaluationByAnswer(DocumentAnswers documentAnswer,long userIdTemp) {
		//DocumentTemplateMapping documentTemplateMapping = documentTemplateMappingDao.getDocumentTemplateMappingByDocId(documentAnswer.getDocumetnId());
		Level1CodeDto level1CodeDto=new Level1CodeDto();
		IsoCategory isoCategory = isoCategoryDao.getISOCategoriesByCodeAndDocId(documentAnswer.getDocumentQuestions().getLevel1Code(), documentAnswer.getDocumetnId());
		List<DocumentAnswers> documentAnswers = documentAnswersDao.allDocumentAnswersbyLevel1Code(documentAnswer);
		Case caseSeed=caseDao.getCaseByDocId(documentAnswer.getDocumetnId());
		if(caseSeed != null){
			JSONObject finalObject=new JSONObject();
			//JSONObject caseId=new JSONObject();
			JSONArray evaluationArray = new JSONArray();
			JSONArray finalArray = new JSONArray();	
			for (DocumentAnswers savedAnswer1 : documentAnswers) {
				JSONObject evaluation=new JSONObject();
				if(savedAnswer1.getDocumentQuestions().getPrimaryIsoCode() != null){
					if (savedAnswer1.getEvaluation() != null && savedAnswer1.getAnswerRank() != null && savedAnswer1.getDocumentQuestions().getHasParentId() ==0) {
						evaluation.put("evaluation", savedAnswer1.getEvaluation());
						evaluation.put("ISO_code", savedAnswer1.getDocumentQuestions().getPrimaryIsoCode());
						evaluation.put("ranking", savedAnswer1.getAnswerRank().toString());
						evaluationArray.put(evaluation);
					}
				
				}
				
			}
			finalObject.put("case_id", caseSeed.getCaseId().toString());
			finalObject.put("entity_id", caseSeed.getEntityId());
			finalObject.put("evaluations", evaluationArray);
			if(caseSeed.getIndustry() != null)
				finalObject.put("industry", caseSeed.getIndustry());
			else
				finalObject.put("industry", "");
			finalArray.put(finalObject);
			String response=getisoCategory(finalArray.toString());
			if(response != null){
				JSONObject responseCode=new JSONObject(response);
				if (responseCode.has("data")) {
					String data = responseCode.getString("data");
					// String dataReplace = data.replace("\\","");
					JSONArray jsonArray = new JSONArray(data);
					JSONObject isoCodes = jsonArray.getJSONObject(0);
					if (isoCodes.has("evaluation")) {
						JSONArray isoEvaluationArray = isoCodes.getJSONArray("evaluation");
						for (int j = 0; j < isoEvaluationArray.length(); j++) {
							JSONObject isoEvaluation = isoEvaluationArray.getJSONObject(j);
							if (isoEvaluation.has("L1_ISO_code")) {
								if (documentAnswer.getDocumentQuestions().getLevel1Code()
										.equals(isoEvaluation.getString("L1_ISO_code"))) {
									if (isoEvaluation.has("evaluation")) {
										if (!isoEvaluation.getString("evaluation")
												.equals(isoCategory.getEvaluation())) {
											isoCategory.setEvaluation(isoEvaluation.getString("evaluation"));
											isoCategory.setEvaluationUpdatedBy(userIdTemp);
											isoCategory.setEvaluationUpdatedTime(new Date());
											isoCategoryDao.saveOrUpdate(isoCategory);

											storeIsoCategoryEvaluation(documentAnswer.getDocumetnId());
										}
									}
								}
							}
							/*
							 * for (IsoCategory isoCategory : isoCategoryList) {
							 * if(isoCategory.getLevel1Code().equals(
							 * isoEvaluation.getString("L1_ISO_code"))){
							 * isoCategory.setEvaluation(isoEvaluation.getString
							 * ("evaluation"));
							 * isoCategoryDao.saveOrUpdate(isoCategory); } }
							 */
						}
					}
				}
			}
			
			
		}
		
		level1CodeDto.setlevel1Evaluation(isoCategory.getEvaluation());
		return level1CodeDto;
	}
	
	private String getisoCategory(String jsonData) {
		String url = ISOCODE_CATEGORY_URL;
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonData);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	

}
