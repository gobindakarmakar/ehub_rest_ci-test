package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Groups Update Dto")
public class UpdateGroupDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 957321153960166215L;

	@ApiModelProperty(value = "ID of the user group")
	private Long userGroupId;

	@ApiModelProperty(value = "Remarks on the user group")
	private String remarks;

	@ApiModelProperty(value = "Description of the user group")
	private String description;

	@ApiModelProperty(value = "Name of the user group",required=true)
	@NotEmpty(message = "Group name can not be blank.")
	private String name;

	@ApiModelProperty(value = "Source of group")
	private String source;

	@ApiModelProperty(value = "Icon if the List Type has")
	private String icon;

	@ApiModelProperty(value = "Color of group")
	private String color;

	@ApiModelProperty(value = "Group status")
	private Boolean active;

	public Long getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
