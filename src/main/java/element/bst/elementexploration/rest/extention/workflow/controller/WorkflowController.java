package element.bst.elementexploration.rest.extention.workflow.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.dto.SimilarCaseDto;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.workflow.service.WorkflowService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "Wrokflow API" }, description = "Manages workflow data")
@RestController
@RequestMapping("/api/workflow/es")
public class WorkflowController extends BaseController {

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private CaseService caseService;

	@ApiOperation("Gets profile")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/wrapper/profile/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getProfile(@RequestParam String token, @PathVariable String profileId) throws Exception {
		return new ResponseEntity<>(workflowService.getProfile(profileId), HttpStatus.OK);
	}

	@ApiOperation("Gets stage by workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/wrapper/stage/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getIndependentStage(@RequestParam String token, @PathVariable String stageId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getIndependentStage(stageId), HttpStatus.OK);
	}

	@ApiOperation("Gets stage by workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/wrapper/stage/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStageByWorkflow(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.getStageByWorkflow(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Gets workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/wrapper/workflow/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflow(@RequestParam String token, @PathVariable String workflowId) throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes workflow states")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstates", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowStates(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflowStates(), HttpStatus.OK);
	}

	@ApiOperation("Gets workflow states")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstates", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStates(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflowStates(), HttpStatus.OK);
	}

	@ApiOperation("Deletes workflow states by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstates/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowStatesById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflowStatesById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets workflow states by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstates/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStatesById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflowStatesById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes workflow states by ID and nominal time")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstates/{workflowId}/{nominalTime}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowStatesByIdAndNominalTime(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String nominalTime) throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflowStatesByIdAndNominaltime(workflowId, nominalTime),
				HttpStatus.OK);
	}

	@ApiOperation("Gets workflow states by ID and nominal time")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstates/{workflowId}/{nominalTime}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStatesByIdAndNominalTime(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String nominalTime) throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflowStatesByIdAndNominalTime(workflowId, nominalTime),
				HttpStatus.OK);
	}

	@ApiOperation("Inserts workflow states")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/workflowstates/{workflowId}/{nominalTime}", consumes = {
			"application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> insertWorkflowStates(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String nominalTime, @RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(workflowService.insertWorkflowStates(workflowId, nominalTime, jsonString),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes published models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/published", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deletePublishedModels(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deletePublishedModels(), HttpStatus.OK);
	}

	@ApiOperation("Gets published models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/published", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPublishedModels(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getPublishedModels(), HttpStatus.OK);
	}

	@ApiOperation("Creates published models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/model/published", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createPublishedModels(@RequestParam String token, @RequestBody String JsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.createPublishedModels(JsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates published models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/model/published", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updatePublishedModels(@RequestParam String token, @RequestBody String JsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.updatePublishedModels(JsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes published models in workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/published/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deletePublishedModelsInWorkflow(@RequestParam String token,
			@PathVariable String workflowId) throws Exception {
		return new ResponseEntity<>(workflowService.deletePublishedModelsInWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets published models by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/published/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPublishedModelsById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getPublishedModelsById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes published models by ID in workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = String.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/published/{workflowId}/{modelId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deletePublishedModelsInWorkflowByModelId(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String modelId) throws Exception {
		return new ResponseEntity<>(workflowService.deletePublishedModelsInWorkflowByModelId(workflowId, modelId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets published models in workflow by model ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/published/{workflowId}/{modelId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPublishedModelsInWorkflowByModelId(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String modelId) throws Exception {
		return new ResponseEntity<>(workflowService.getPublishedModelsInWorkflowByModelId(workflowId, modelId),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes published models in workflow by version")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/published/{workflowId}/{modelId}/{version}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> deletePublishedModelsInWorkflowByVersion(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String modelId, @PathVariable String version)
			throws Exception {
		return new ResponseEntity<>(
				workflowService.deletePublishedModelsInWorkflowByVersion(workflowId, modelId, version), HttpStatus.OK);
	}

	@ApiOperation("Gets published models in workflow by version")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/published/{workflowId}/{modelId}/{version}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getPublishedModelsInWorkflowByVersion(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String modelId, @PathVariable String version)
			throws Exception {
		return new ResponseEntity<>(workflowService.getPublishedModelsInWorkflowByVersion(workflowId, modelId, version),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes satging models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/staging", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteStagingModels(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteStagingModels(), HttpStatus.OK);
	}

	@ApiOperation("Gets staging models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/staging", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStagingModels(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getStagingModels(), HttpStatus.OK);
	}

	@ApiOperation("Creates staging models ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/model/staging", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createStagingModels(@RequestParam String token, @RequestBody String JsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.createStagingModels(JsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates staging models")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/model/staging", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateStagingModels(@RequestParam String token, @RequestBody String JsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.updateStagingModels(JsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Gets staging to publish models by version")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/staging/publish/{workflowId}/{modelId}/{version}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getStagingToPublishModelsByVersion(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String modelId, @PathVariable String version)
			throws Exception {
		return new ResponseEntity<>(workflowService.getStagingToPublishModelsByVersion(workflowId, modelId, version),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes staging models by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/staging/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteStagingModelsById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteStagingModelsById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets Staging models by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/staging/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStagingModelsById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getStagingModelsById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes staging models by ID and model ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/staging/{workflowId}/{modelId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteStagingModelsByIdAndModelId(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String modelId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteStagingModelsByIdAndModelId(workflowId, modelId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets staging models by ID and model ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/staging/{workflowId}/{modelId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getStagingModelsByIdAndModelId(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String modelId) throws Exception {
		return new ResponseEntity<>(workflowService.getStagingModelsByIdAndModelId(workflowId, modelId), HttpStatus.OK);
	}

	@ApiOperation("Deletes staging models by version")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/model/staging/{workflowId}/{modelId}/{version}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteStagingModelsByVersion(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String modelId, @PathVariable String version) throws Exception {
		return new ResponseEntity<>(workflowService.deleteStagingModelsByVersion(workflowId, modelId, version),
				HttpStatus.OK);
	}

	@ApiOperation("Gets staging models by version")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/model/staging/{workflowId}/{modelId}/{version}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getStagingModelsByVersion(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String modelId, @PathVariable String version) throws Exception {
		return new ResponseEntity<>(workflowService.getStagingModelsByVersion(workflowId, modelId, version),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes tags")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/tag", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteTags(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteTags(), HttpStatus.OK);
	}

	@ApiOperation("Gets tags")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/tag", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTags(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getTags(), HttpStatus.OK);
	}

	@ApiOperation("Creates tags")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/tag", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createTags(@RequestParam String token, @RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createTags(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates tags")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/tag", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateTags(@RequestParam String token, @RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.updateTags(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes tag by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/tag/{tagId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteTagById(@RequestParam String token, @PathVariable String tagId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteTagById(tagId), HttpStatus.OK);
	}

	@ApiOperation("Gets tag by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/tag/{tagId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTagById(@RequestParam String token, @PathVariable String tagId) throws Exception {
		return new ResponseEntity<>(workflowService.getTagById(tagId), HttpStatus.OK);
	}

	@ApiOperation("Deletes graph cache")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/graphcache", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteGraphcache(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteGraphcache(), HttpStatus.OK);
	}

	@ApiOperation("Gets graph cache")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graphcache", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphcache(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getGraphcache(), HttpStatus.OK);
	}

	@ApiOperation("Deletes graph cache by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/graphcache/{query}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteGraphcacheByQuery(@RequestParam String token, @PathVariable String query)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteGraphcacheByQuery(query), HttpStatus.OK);
	}

	@ApiOperation("Gets graph cache by query")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graphcache/{query}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphcacheByQuery(@RequestParam String token, @PathVariable String query)
			throws Exception {
		return new ResponseEntity<>(workflowService.getGraphcacheByQuery(query), HttpStatus.OK);
	}

	@ApiOperation("Creates graph cache")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/graphcache/{query}/{caseId}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createGraphcache(@RequestParam String token, @PathVariable String query,
			@PathVariable String caseId, @RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createGraphcache(query, caseId, jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/cases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteCases(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteCases(), HttpStatus.OK);
	}

	@ApiOperation("Gets cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCases(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getCases(), HttpStatus.OK);
	}

	@ApiOperation("Creates case")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cases", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCase(@RequestParam String token, @RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createCase(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates case")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/cases", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateCase(@RequestParam String token, @RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.updateCase(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes case by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/cases/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteCaseById(@RequestParam String token, @PathVariable String caseId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteCaseById(caseId), HttpStatus.OK);
	}

	@ApiOperation("Gets case by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseById(@RequestParam String token, @PathVariable String caseId) throws Exception {
		return new ResponseEntity<>(workflowService.getCaseById(caseId), HttpStatus.OK);
	}

	@ApiOperation("Clears database")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/esdatabase/clear", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> clearDatabase(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.clearDatabase(), HttpStatus.OK);
	}

	@ApiOperation("Deletes index")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/esdatabase/clear/{indexName}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteIndex(@RequestParam String token, @PathVariable String indexName) throws Exception {
		return new ResponseEntity<>(workflowService.deleteIndex(indexName), HttpStatus.OK);
	}

	@ApiOperation("Deletes contingency table")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/contingencytable", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteContingencyTable(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteContingencyTable(), HttpStatus.OK);
	}

	@ApiOperation("Gets contingency table")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/contingencytable", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getContingencyTable(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getContingencyTable(), HttpStatus.OK);
	}

	@ApiOperation("Creates contingency table")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/contingencytable/{contingencytableId}", consumes = {
			"application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createContingencyTable(@RequestParam String token, @PathVariable String contingencytableId,
			@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createContingencyTable(jsonToBeSent, contingencytableId),
				HttpStatus.OK);
	}

	@ApiOperation("Updates contingency table")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/contingencytable/{contingencytableId}", consumes = {
			"application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateContingencyTable(@RequestParam String token, @PathVariable String contingencytableId,
			@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.updateContingencyTable(jsonToBeSent, contingencytableId),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes contingency table by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/contingencytable/{contingencytableId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteContingencyTableById(@RequestParam String token,
			@PathVariable String contingencytableId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteContingencyTableById(contingencytableId), HttpStatus.OK);
	}

	@ApiOperation("Gets contingency table by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/contingencytable/{contingencytableId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getContingencyTableById(@RequestParam String token,
			@PathVariable String contingencytableId) throws Exception {
		return new ResponseEntity<>(workflowService.getContingencyTableById(contingencytableId), HttpStatus.OK);
	}

	@ApiOperation("Gets list of indices")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/index/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfInices(@RequestParam String token, @PathVariable String profileId) throws Exception {
		return new ResponseEntity<>(workflowService.listOfInices(profileId), HttpStatus.OK);
	}

	@ApiOperation("Creates index")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/index/{profileId}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createIndex(@RequestParam String token, @PathVariable String profileId,
			@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createIndex(jsonToBeSent, profileId), HttpStatus.OK);
	}

	@ApiOperation("Deletes index by ID and index name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/index/{profileId}/{indexName}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteIndexByIdAndIndexName(@RequestParam String token, @PathVariable String profileId,
			@PathVariable String indexName) throws Exception {
		return new ResponseEntity<>(workflowService.deleteIndexByIdAndIndexName(profileId, indexName), HttpStatus.OK);
	}

	@ApiOperation("Gets list of types")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/index/{profileId}/{indexName}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfTypes(@RequestParam String token, @PathVariable String profileId,
			@PathVariable String indexName) throws Exception {
		return new ResponseEntity<>(workflowService.listOfTypes(profileId, indexName), HttpStatus.OK);
	}

	@ApiOperation("Deletes all workflow stats")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAllWorkflowStats(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteAllWorkflowStats(), HttpStatus.OK);
	}

	@ApiOperation("Gets all workflow stats")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstats", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllWorkflowStats(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getAllWorkflowStats(), HttpStatus.OK);
	}

	@ApiOperation("Deletes all workflow stats by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstats/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowStatsById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflowStatsById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets workflow stats by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstats/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStatsById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflowStatsById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes workflow stats by ID and stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstats/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowStatsByIdAndStageId(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflowStatsByIdAndStageId(workflowId, stageId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets workflow stats by ID and stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstats/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStatsByIdAndStageId(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflowStatsByIdAndStageId(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Deletes  workflow stats by ID, stage ID and time")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflowstats/{workflowId}/{stageId}/{nominalTime}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowStatsByIdAndStageIdAndTime(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String stageId, @PathVariable String nominalTime)
			throws Exception {
		return new ResponseEntity<>(
				workflowService.deleteWorkflowStatsByIdAndStageIdAndTime(workflowId, stageId, nominalTime),
				HttpStatus.OK);
	}

	@ApiOperation("Gets  workflow stats by ID, stage ID and time")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflowstats/{workflowId}/{stageId}/{nominalTime}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStatsByIdAndStageIdAndTime(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String stageId, @PathVariable String nominalTime)
			throws Exception {
		return new ResponseEntity<>(
				workflowService.getWorkflowStatsByIdAndStageIdAndTime(workflowId, stageId, nominalTime), HttpStatus.OK);
	}

	@ApiOperation("Creates workflow stats")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/workflowstats/{workflowId}/{stageId}/{nominalTime}", consumes = {
			"application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createWorkflowStats(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId, @PathVariable String nominalTime, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.createWorkflowStats(workflowId, stageId, nominalTime, jsonToBeSent),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes all stages in workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/stage/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAllStagesInWorkflow(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteAllStagesInWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets all stages in workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/stage/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllStagesInWorkflow(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getAllStagesInWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Creates stage in workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/stage/{workflowId}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createStageInWorkflow(@RequestParam String token, @PathVariable String workflowId,
			@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createStageInWorkflow(workflowId, jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates stage in workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/stage/{workflowId}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateStageInWorkflow(@RequestParam String token, @PathVariable String workflowId,
			@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.updateStageInWorkflow(workflowId, jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes all stages in workflow by stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/stage/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAllStagesInWorkflowByStageId(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteAllStagesInWorkflowByStageId(workflowId, stageId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets all stages in workflow by stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/stage/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllStagesInWorkflowByStageId(@RequestParam String token,
			@PathVariable String workflowId, @PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.getAllStagesInWorkflowByStageId(workflowId, stageId),
				HttpStatus.OK);
	}

	@ApiOperation("Gets Pyspark")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/pyspark/{type}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getPyspark(@RequestParam String token, @PathVariable String type) throws Exception {
		return new ResponseEntity<>(workflowService.getPyspark(type), HttpStatus.OK);
	}

	@ApiOperation("Gets all workflows")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllWorkflows(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getAllWorkflows(), HttpStatus.OK);
	}

	@ApiOperation("Creates workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/workflow", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createWorkflow(@RequestParam String token, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.createWorkflow(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/workflow", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateWorkflow(@RequestParam String token, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.updateWorkflow(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Inserts a parameterized start workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/workflow/parameterized/start", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> parmeterizedStrtWorkflow(@RequestParam String token, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.parmeterizedStrtWorkflow(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Sets workflow status")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/setexecutionstatus/{workflowId}/{refreshInterval}/{status}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> setWorkflowStatus(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String refreshInterval, @PathVariable String status) throws Exception {
		return new ResponseEntity<>(workflowService.setWorkflowStatus(workflowId, refreshInterval, status),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflow/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflow(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets workflow by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getworkflowByWorkflowId(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getworkflowByWorkflowId(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Starts the workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/{workflowId}/start", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> startWorkflow(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.startWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Starts the workflow with resource")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/{workflowId}/start/{mermory}/{cores}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> startWorkflowWithResource(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String mermory, @PathVariable String cores) throws Exception {
		return new ResponseEntity<>(workflowService.startWorkflowWithResource(workflowId, mermory, cores),
				HttpStatus.OK);
	}

	@ApiOperation("Stops the workflow")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/{workflowId}/status", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> stopWorkflow(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getWorkflowStatus(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets workflow status")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/{workflowId}/stop", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkflowStatus(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.stopWorkflow(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes all samples")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/sample/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAllSamples(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteAllSamples(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Gets all samples")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/sample/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllSamples(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getAllSamples(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Deletes all samples by stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/sample/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAllSamplesByStageId(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteAllSamplesByStageId(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Gets all samples by stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/sample/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllSamplesByStageId(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.getAllSamplesByStageId(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Inserts samples")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/sample/{workflowId}/{stageId}", consumes = {
			"application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> insertSamples(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId, @RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.insertSamples(workflowId, stageId, jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes profiles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/profile", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteProfile(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteProfile(), HttpStatus.OK);
	}

	@ApiOperation("Gets profiles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/profile", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getProfiles(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getProfiles(), HttpStatus.OK);
	}

	@ApiOperation("Creates profile")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/profile", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createProfile(@RequestParam String token, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.createProfile(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Updates profile")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/profile", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateProfile(@RequestParam String token, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.updateProfile(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Deletes profile by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/profile/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteProfileById(@RequestParam String token, @PathVariable String profileId)
			throws Exception {
		return new ResponseEntity<>(workflowService.deleteProfileById(profileId), HttpStatus.OK);
	}

	@ApiOperation("Gets profile by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/profile/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getProfileById(@RequestParam String token, @PathVariable String profileId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getProfileById(profileId), HttpStatus.OK);
	}

	@ApiOperation("Gets sample data of Es")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/sampledata/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSampleDataOfEs(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.getSampleDataOfEs(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Gets schema of Es")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/schema/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSchemaDataOfEs(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(workflowService.getSchemaDataOfEs(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Gets case alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/alerts", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseAlerts(@RequestParam String token, @PathVariable String caseId) throws Exception {
		return new ResponseEntity<>(workflowService.getCaseAlerts(caseId), HttpStatus.OK);
	}

	@ApiOperation("creates case alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cases/{caseId}/alerts", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCaseAlert(@RequestParam String token, @RequestBody String jsonToBeSent,
			@PathVariable String caseId) throws Exception {
		return new ResponseEntity<>(workflowService.createCaseAlert(jsonToBeSent, caseId), HttpStatus.OK);
	}

	@ApiOperation("Gets latest case alerts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/alerts/{count}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getLatestCaseAlerts(@RequestParam String token, @PathVariable String caseId,
			@PathVariable String count) throws Exception {
		return new ResponseEntity<>(workflowService.getLatestCaseAlerts(caseId, count), HttpStatus.OK);
	}

	@ApiOperation("Gets case timeline")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/timeline", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseTimeline(@RequestParam String token, @PathVariable String caseId) throws Exception {
		return new ResponseEntity<>(workflowService.getCaseTimeline(caseId), HttpStatus.OK);
	}

	@ApiOperation("Creates case timeline")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cases/{caseId}/timeline", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> createCaseTimeline(@RequestBody String jsonToBeSent, @RequestParam String token,
			@PathVariable String caseId) throws Exception {
		return new ResponseEntity<>(workflowService.createCaseTimeline(caseId, jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Gets case summary")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/summary", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCasesSummary(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getCasesSummary(), HttpStatus.OK);
	}

	@ApiOperation("creates case summary")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 404, response = String.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cases/summary", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCasesSummary(@RequestParam String token, @RequestBody String jsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.createCasesSummary(jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Gets case summary with limit")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/summary/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCasesSummaryWithLimit(@RequestParam String token, @PathVariable String limit)
			throws Exception {
		return new ResponseEntity<>(workflowService.getCasesSummaryWithLimit(limit), HttpStatus.OK);
	}

	@ApiOperation("Gets case summary with case ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/summary", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCasesSummaryWithcaseId(@RequestParam String token, @PathVariable String caseId)
			throws Exception {
		return new ResponseEntity<>(workflowService.getCasesSummaryWithcaseId(caseId), HttpStatus.OK);
	}

	@ApiOperation("Creates case sunmary with case ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cases/{caseId}/summary", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCasesSummaryWithcaseId(@RequestParam String token, @PathVariable String caseId,
			@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.createCasesSummaryWithcaseId(caseId, jsonToBeSent), HttpStatus.OK);
	}

	@ApiOperation("Gets similar cases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/similar/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSimilarCases(@RequestParam String token, @PathVariable String caseId,
			@PathVariable Integer limit) throws Exception {
		List<SimilarCaseDto> similarCases = new ArrayList<>();
		List<SimilarCaseDto> list = new ArrayList<>();

		String response = workflowService.getSimilarCases(caseId, limit);
		ObjectMapper objectMapper = new ObjectMapper();
		TypeFactory typeFactory = objectMapper.getTypeFactory();
		similarCases = objectMapper.readValue(response,
				typeFactory.constructCollectionType(List.class, SimilarCaseDto.class));
		Map<String, SimilarCaseDto> map = new HashMap<String, SimilarCaseDto>();
		for (SimilarCaseDto similarCase : similarCases) {
			map.put(similarCase.getId(), similarCase);
		}
		List<Long> caseIds = caseService.getCasesByUser(getCurrentUserId());
		for (Long value : caseIds) {
			if (map.containsKey(String.valueOf(value))) {
				list.add(map.get(String.valueOf(value)));
			}
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@ApiOperation("Gets case timeline with limit")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/timeline/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseTimelineWithLimit(@RequestParam String token, @PathVariable String caseId,
			@PathVariable Integer limit) throws Exception {
		return new ResponseEntity<>(workflowService.getCaseTimelineWithLimit(caseId, limit), HttpStatus.OK);
	}

	@ApiOperation("Gets case timeline founded with limit")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cases/{caseId}/timeline/founded/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseTimelineFoundedWithLimit(@RequestParam String token, @PathVariable String caseId,
			@PathVariable Integer limit) throws Exception {
		return new ResponseEntity<>(workflowService.getCaseTimelineFoundedWithLimit(caseId, limit), HttpStatus.OK);
	}

	@ApiOperation("Gets case summary with limit")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cases/casesSummary/{limit}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCasesSumary(@RequestParam String token, @RequestBody CaseVo caseVo,
			@PathVariable Integer limit) throws Exception {
		return new ResponseEntity<>(workflowService.getCasesSumary(this.getCurrentUserId(), caseVo, limit),
				HttpStatus.OK);
	}

	@ApiOperation("Gets databases by profile ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/mysql/databases/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDatabasesByProfileId(@PathVariable String profileId, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(workflowService.getDatabasesByProfileId(profileId), HttpStatus.OK);
	}

	@ApiOperation("Gets database tables by profile ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/mysql/tables/{database}/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDatabaseTablesByProfileId(@PathVariable String profileId, @RequestParam String token,
			@PathVariable String database) throws Exception {
		return new ResponseEntity<>(workflowService.getDatabaseTablesByProfileId(profileId, database), HttpStatus.OK);
	}

	// Risk Score
	@ApiOperation("Assigns risk score to graph")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/graph/risk/score", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> assignRiskScoreToGraph(@RequestBody String jsonString, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(workflowService.assignRiskScoreToGraph(jsonString), HttpStatus.OK);
	}

	// cassandra apis
	@ApiOperation("Creates cassandra database")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cassandra/create_keyspace", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCassandraDatabase(@RequestBody String jsonString, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(workflowService.createCassandraDatabase(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Creates cassandra table")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/cassandra/create_family", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCassandraTable(@RequestBody String jsonString, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(workflowService.createCassandraTable(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets a list of cassandra databases")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cassandra/keyspaces/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCassandraDatabasesList(@PathVariable String profileId, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getCassandraDatabasesList(profileId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets sample data of cassandra")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cassandra/sampledata/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSampleCassandraData(@PathVariable String stageId,@PathVariable String workflowId, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getSampleCassandraData(workflowId,stageId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets sample schema of cassandra")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cassandra/schema/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSampleCassandraSchema(@PathVariable String stageId,@PathVariable String workflowId, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getSampleCassandraSchema(workflowId,stageId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets a list of cassandra database tables")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/cassandra/families/{database}/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getListCassandraDatabaseTables(@PathVariable String database,@PathVariable String profileId, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getListCassandraDatabaseTables(database,profileId), HttpStatus.OK);
	}
	
	//Notebooks apis
	@ApiOperation("Creates a new notebook for workflow stage")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/notebook", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createNotebookForWorkflow(@RequestBody String jsonString, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(workflowService.createNotebookForWorkflow(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Gets all notebooks of a workflow Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/notebook/list/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllNotebooksByWorkflowId(@PathVariable String workflowId, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getAllNotebooksByWorkflowId(workflowId), HttpStatus.OK);
	}
	

	@ApiOperation("Gets notebook by Id")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/notebook/{id}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getNotebookById(@PathVariable String id, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getNotebookById(id), HttpStatus.OK);
	}
	
	@ApiOperation("Deletes notebook")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/notebook/{workflowId}/{notebookName}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteNotebook(@PathVariable String workflowId, @RequestParam String token,@PathVariable String notebookName) throws Exception {
		return new ResponseEntity<>(workflowService.deleteNotebook(workflowId,notebookName), HttpStatus.OK);
	}
	
	@ApiOperation("Gets notebook by worflow Id and notebook name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/notebook/{workflowId}/{notebookName}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getNotebookByWorkFlowIdAndNotebookName(@PathVariable String workflowId, @RequestParam String token,@PathVariable String notebookName) throws Exception {
		return new ResponseEntity<>(workflowService.getNotebookByWorkFlowIdAndNotebookName(workflowId,notebookName), HttpStatus.OK);
	}
	
	@ApiOperation("Updates notebook")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/notebook/{workflowId}/{notebookName}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8"  })
	public ResponseEntity<?> updateNotebook(@PathVariable String workflowId, @RequestParam String token,@PathVariable String notebookName,@RequestBody String jsonToBeSent) throws Exception {
		return new ResponseEntity<>(workflowService.updateNotebook(workflowId,notebookName,jsonToBeSent), HttpStatus.OK);
	}
	
	@ApiOperation("Runs parent stages of a given stage")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow/{workflowId}/{stageId}/start", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> runParentStagesForAStage(@PathVariable String stageId,@PathVariable String workflowId, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.runParentStagesForAStage(workflowId,stageId), HttpStatus.OK);
	}
	
	//////////////////////////Source Apis //////////////
	@ApiOperation("Deletes the file specified in the path")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/local/source/delete/{path}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteFileByPath(@PathVariable String path, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.deleteFileByPath(path), HttpStatus.OK);
	}
	
	@ApiOperation("Gets the HDFS profile used for file source")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/local/source/profile", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getHdfsProfileForFileSource( @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.getHdfsProfileForFileSource(), HttpStatus.OK);
	}
	
	@ApiOperation("Saves the source file")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/local/source/save_file/{workflowId}/{stageId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSourceFile(@PathVariable String stageId,@PathVariable String workflowId,@RequestParam MultipartFile uploadFile, @RequestParam String token)
			throws Exception {
		return new ResponseEntity<>(workflowService.saveSourceFile(stageId,workflowId,uploadFile), HttpStatus.OK);
	}
	
	/////////////////////////////////workflow cases///////////////////////////////////
	
	@ApiOperation("Lists workflow cases where data is available")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow-cases/list", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> ListWorkFlowCasesWithData( @RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowService.ListWorkFlowCasesWithData(), HttpStatus.OK);
	}
	
	@ApiOperation("Deletes workflow case")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/workflow-cases/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteWorkflowCaseById(@RequestParam String token,@PathVariable String workflowId) throws Exception {
		return new ResponseEntity<>(workflowService.deleteWorkflowCaseById(workflowId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets workflow case")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/workflow-cases/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWorkFlowCaseById( @RequestParam String token,@PathVariable String workflowId) throws Exception {
		return new ResponseEntity<>(workflowService.getWorkFlowCaseById(workflowId), HttpStatus.OK);
	}
	
	@ApiOperation("Saves workflow case")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/workflow-cases/{workflowId}", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveWorkflowCase(@PathVariable String workflowId, @RequestParam String token,@RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(workflowService.saveWorkflowCase(workflowId,jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Updates workflow case")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/workflow-cases/{workflowId}", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateWorkflowCase(@PathVariable String workflowId,@RequestParam String token, @RequestBody String JsonToBeSent)
			throws Exception {
		return new ResponseEntity<>(workflowService.updateWorkflowCase(workflowId,JsonToBeSent), HttpStatus.OK);
	}
}
