package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerRelationShipDetails;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface CustomerRelationDao extends GenericDao<CustomerRelationShipDetails,Long>
{
	List<CustomerRelationShipDetails> findByCustomerNumber(String customerNumber);
	
	public CustomerRelationShipDetails findCustomerRelation(String customerNumber,String customerRelationNumber);

	List<String> getCustomerRelationnamesList(Long beneficiaryCustomerId);

}
