package element.bst.elementexploration.rest.extention.textsentiment.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class TextSentiment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "Language of the text sentiment", required = true)
	@NotEmpty(message = "Language can not be blank.")
	private String language;

	@ApiModelProperty(value = "ID of the text sentiment", required = true)
	//@NotEmpty(message = "Id can not be blank.")
	private String id;
	
	@ApiModelProperty(value = "Text of the sentiment", required = true)
	@NotEmpty(message = "Text can not be blank.")
	private String text;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
