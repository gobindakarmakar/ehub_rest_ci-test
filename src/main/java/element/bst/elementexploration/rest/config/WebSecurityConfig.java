package element.bst.elementexploration.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import element.bst.elementexploration.rest.usermanagement.security.AuthenticationTokenProcessingFilter;
import element.bst.elementexploration.rest.usermanagement.security.UnauthorizedEntryPoint;
import element.bst.elementexploration.rest.usermanagement.security.serviceImpl.UserDetailServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailServiceImpl userDetailService;
	
	@Autowired
	AuthenticationTokenProcessingFilter authenticationTokenProcessingFilter;
	
	@Autowired
	UnauthorizedEntryPoint unauthorizedEntryPoint;
	
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth)
      throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
     
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
          = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }

    @Override
    @Order(1)
    protected void configure(HttpSecurity http) throws Exception {

    	http.
	        csrf().disable().
	        sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
	        and().
	        authorizeRequests().
	        antMatchers("/api/securityNew/validateEmailPassword","/api/usersNew/registerByUsers", "/api/usersNew/registerByUser", "/api/productVersions/**", "/api/dashboard/fetchAlerts", "/api/securityNew/forgotPassword", "/api/securityNew/validateTemporaryPassword/**", "/api/securityNew/resetPassword/**","/api/screeningRiskScore/getRiskScore","/api/userNew/uploadUserImage","/api/securityNew/generateTwoFAVerificationCode","/api/securityNew/verifyTwoFACode","/api/securityNew/validateEmailPassword1","/api/systemSettings/getBasicSettings","/api/systemSettings/downloadFileJsonFromJson").permitAll().
	        antMatchers("/api/**").authenticated().
	        and().
	        exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint);

    	http
    		.addFilterBefore(authenticationTokenProcessingFilter, UsernamePasswordAuthenticationFilter.class);
    }
    
    /**
     * Added for suppress the security to evidence document download.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/api/documentStorage/downloadDocument/**");
    }

	

    

/*
	@Autowired
	UserDetailServiceImpl userDetailService;
	
	@Autowired
	AuthenticationTokenProcessingFilter authenticationTokenProcessingFilter;
	
	@Autowired
	UnauthorizedEntryPoint unauthorizedEntryPoint;
	
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth)
      throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
     
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
          = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }

    @Override
    @Order(1)
    protected void configure(HttpSecurity http) throws Exception {

    	http.
	        csrf().disable().
	        sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
	        and().
	        authorizeRequests().
	        antMatchers("/api/security/validateEmailPassword","/api/user/registerByUsers", "/api/user/registerByUser", "/api/productVersions/**", "/api/dashboard/fetchAlerts", "/api/security/forgotPassword", "/api/security/validateTemporaryPassword/**", "/api/security/resetPassword/**","/api/screeningRiskScore/getRiskScore").permitAll().
	        antMatchers("/api/**").authenticated().
	        and().
	        exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint);

    	http
    		.addFilterBefore(authenticationTokenProcessingFilter, UsernamePasswordAuthenticationFilter.class);
    }
    
    *//**
     * Added for suppress the security to evidence document download.
     *//*
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/api/documentStorage/downloadDocument/**");
    }*/
}
