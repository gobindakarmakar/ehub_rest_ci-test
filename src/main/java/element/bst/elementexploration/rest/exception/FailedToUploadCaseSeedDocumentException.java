package element.bst.elementexploration.rest.exception;

public class FailedToUploadCaseSeedDocumentException extends RuntimeException{

    
    /**
     * 
     */
    private static final long serialVersionUID = 9102258785076123488L;

    private  String message;;
    
    public static final String FAILED_TO_UPLOAD_CASESEED_MSG="Failed to upload the case seed document";
	
	public FailedToUploadCaseSeedDocumentException(String message) {
		super();
		this.message = message;
	}



	public FailedToUploadCaseSeedDocumentException() {

		super();
		message = FAILED_TO_UPLOAD_CASESEED_MSG;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return message;
	}

}