package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
/**
 * @author rambabu
 *
 */
@ApiModel("Document table rows")
public class DocumentTableRowsDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String rowName;
	
	public DocumentTableRowsDto() {
		super();
		
	}
	public DocumentTableRowsDto(long id, String rowName) {
		super();
		this.id = id;
		this.rowName = rowName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRowName() {
		return rowName;
	}
	public void setRowName(String rowName) {
		this.rowName = rowName;
	}
	
	
	

}
