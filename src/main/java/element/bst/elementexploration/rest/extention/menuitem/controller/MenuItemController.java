package element.bst.elementexploration.rest.extention.menuitem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.menuitem.domain.LastVisitedDomains;
import element.bst.elementexploration.rest.extention.menuitem.dto.LastVisitedDomainDto;
import element.bst.elementexploration.rest.extention.menuitem.dto.UserMenuFinalDto;
import element.bst.elementexploration.rest.extention.menuitem.service.LastVisitedDomainService;
import element.bst.elementexploration.rest.extention.menuitem.service.MenuItemService;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Menu Item API" }, description = "Provides Menu Item Calculation based on usage")
@RestController
@RequestMapping("/api/menuItem")
public class MenuItemController extends BaseController {

	@Autowired
	MenuItemService menuItemService;
	
	@Autowired
	LastVisitedDomainService lastVisitedDomainService;

	@ApiOperation("Updates User Menu Data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateUserMenu", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateUserMenu(HttpServletRequest request, @RequestParam String token,
			@Valid @RequestBody UserMenuFinalDto userMenuFinalDto) throws Exception {
		Long userId = getCurrentUserId();
		return new ResponseEntity<>(menuItemService.updateUserMenu(userMenuFinalDto, userId), HttpStatus.OK);

	}

	@ApiOperation("Gets All User Menu")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllUserMenus", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllUserMenu(HttpServletRequest request, @RequestParam String token) throws Exception {
		Long userId = getCurrentUserId();
		return new ResponseEntity<>(menuItemService.getAllUserMenu(userId), HttpStatus.OK);
	}
	
	@ApiOperation("Gets All User Menu")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllUserMenu", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllUserMenus(HttpServletRequest request, @RequestParam String token) throws Exception {
		
		return new ResponseEntity<>(menuItemService.getAllUserMenus(), HttpStatus.OK);
	}
	
	@ApiOperation("Save/Updates Last Visited Domains")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PutMapping(value = "/saveOrUpdateLastVisitedDomains", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateLastVisitedDomains(HttpServletRequest request, @RequestParam String token,
			@Valid @RequestBody LastVisitedDomainDto lastVisitedDomainDto, BindingResult results) throws Exception {
		Long userId = getCurrentUserId();
		GenericReturnObject message = new GenericReturnObject();
		if (!results.hasErrors()) {
			LastVisitedDomains lastVisited = lastVisitedDomainService
					.saveOrUpdateLastVisitedDomain(lastVisitedDomainDto, userId);
			if (lastVisited != null) {
				message.setData(lastVisited);
				message.setResponseMessage(ElementConstants.FETCH_LAST_VISITED_DOMAINS_SUCCESSFUL);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
			} else {
				message.setResponseMessage(ElementConstants.LAST_VISITED_DOMAINS_FAILED);
				message.setStatus(ElementConstants.ERROR_STATUS);
			}

		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@ApiOperation("Gets All Last Visited Domains By UserId ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllLastVisitedDomainByUserId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllLastVisitedDomainByUserId(HttpServletRequest request, @RequestParam Long userId,
			@RequestParam String token) throws Exception {
		GenericReturnObject message = new GenericReturnObject();
			LastVisitedDomains lastVisited = lastVisitedDomainService.findLastVisitedDomainByUserId(userId);
			if (lastVisited != null) {
				message.setData(lastVisited);
				message.setResponseMessage(ElementConstants.FETCH_LAST_VISITED_DOMAINS_SUCCESSFUL);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
			} else {
				message.setResponseMessage(ElementConstants.LAST_VISITED_DOMAINS_FAILED);
				message.setStatus(ElementConstants.ERROR_STATUS);
			}


		return new ResponseEntity<>(message, HttpStatus.OK);
	}
}
