package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.AccountDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.CustomerDetailsDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.TxDataDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionMasterView;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataNotifDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByDate;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByLocationDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByType;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxProductTypeDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountOwnershipType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.util.FilterUtility;

@RunWith(MockitoJUnitRunner.class)
public class TxDataDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@Mock
	CustomerDetailsDaoImpl customerDetailsDao;

	@Mock
	AccountDaoImpl accountDao;

	@Mock
	FilterUtility filterUtility;

	@InjectMocks
	public TxDataDaoImpl txDataDaoImpl;

	List<TransactionsData> listData = new ArrayList<TransactionsData>();

	public TransactionsData transactionsData;

	public CustomerDetails customerDetails;

	Country country;

	List<Account> accountlist = new ArrayList<Account>();

	Account account;

	List<TxByDate> txByDateList = new ArrayList<TxByDate>();

	List<TxByType> txByTypeList = new ArrayList<TxByType>();

	TxByDate txByDate;

	TxByType txByType;

	Alert alert;

	List<TxByLocationDto> txByLocationDtolist = new ArrayList<TxByLocationDto>();

	TxByLocationDto txByLocationDto;

	List<String> stringList = new ArrayList<>();

	List<String> locations = new ArrayList<>();

	List<CounterPartyDto> counterPartyDtoList = new ArrayList<>();

	TotalDto totalDto;

	CounterPartyDto counterPartyDto;

	List<CountryAggDto> countryAggDtoList = new ArrayList<>();

	CountryAggDto countryAggDto;

	List<String> accountList = new ArrayList<>();

	TransactionsData transactionsData1;

	FilterDto filter;

	List<Object[]> objectList = new ArrayList<Object[]>();

	List<TransactionMasterView> transactionMasterViewList = new ArrayList<TransactionMasterView>();

	TransactionMasterView transactionMasterView = new TransactionMasterView();

	List<AlertDashBoardRespDto> alertDashBoardRespDtoList = new ArrayList<AlertDashBoardRespDto>();

	AlertDashBoardRespDto alertDashBoardRespDto;

	List<CorporateStructureDto> corporateStructureDtoList = new ArrayList<CorporateStructureDto>();

	CorporateStructureDto corporateStructureDto;

	RiskCountAndRatioDto riskCountAndRatioDto;

	List<CounterPartyNotiDto> counterPartyNotiDtoList = new ArrayList<CounterPartyNotiDto>();

	CounterPartyNotiDto counterPartyNotiDto;

	List<RiskCountAndRatioDto> riskCountAndRatioDtoList = new ArrayList<RiskCountAndRatioDto>();

	List<AlertedCounterPartyDto> alertedCounterPartyDtoList = new ArrayList<AlertedCounterPartyDto>();

	AlertedCounterPartyDto alertedCounterPartyDto;

	List<TransactionAmountAndAlertCountDto> transactionAmountAndAlertCountDtoList = new ArrayList<TransactionAmountAndAlertCountDto>();

	TransactionAmountAndAlertCountDto transactionAmountAndAlertCountDto;

	List<CounterPartyInfo> counterPartyInfoList = new ArrayList<CounterPartyInfo>();

	CounterPartyInfo counterPartyInfo;

	List<Double> listDouble = new ArrayList<Double>();

	@Before
	public void setupMock() {
		country = new Country();
		country.setCountry("India");
		country.setIso2Code("IN");
		country.setIs03Code("IND");
		country.setLatitude("0 0 0 N");
		country.setLongitude("0 0 0 N");
		transactionsData1 = new TransactionsData();
		transactionsData1.setAtmAddress("Hyderabad");
		transactionsData1.setBeneficiaryAccountId("123456");
		transactionsData1.setOriginatorAccountId("654321");
		listData.add(transactionsData1);
		customerDetails = new CustomerDetails();
		customerDetails.setCustomerNumber("12345");
		account = new Account(1L, "12345", AccountOwnershipType.IND, "primaryCustomerIdentifier", new Date(),
				"jurisdiction", "sourceSystem", AccountType.SAV, "personal", "45678", "rupee", 100000.0,
				customerDetails, "bankName");
		accountlist.add(account);
		txByDate = new TxByDate(2L, 15000.0, new Date());
		txByDateList.add(txByDate);
		txByType = new TxByType(2L, "transProductType");
		alert = new Alert();
		alert.setFocalNtityDisplayId("12345678");
		txByLocationDto = new TxByLocationDto(1L, "India");
		txByLocationDtolist.add(txByLocationDto);
		stringList.add("1234567");
		stringList.add("123456");
		totalDto = new TotalDto(100000, 2L, 10000000, 10000, 110000);
		locations.add("india");
		locations.add("usa");
		counterPartyDto = new CounterPartyDto("123456", 2L, 10000, 1000000, 1100000, 200000);
		counterPartyDtoList.add(counterPartyDto);
		countryAggDto = new CountryAggDto("IND", 2L, 10000, 1000000, 500000, 500000);
		countryAggDtoList.add(countryAggDto);
		accountList.add("businessAccount");
		accountList.add("normal");
		filter = new FilterDto();
		filter.setBankLocation(true);
		filter.setGeoRiskType("FATF High Risk Countries");
		Object[] objectArrayNew = new Object[6];
		objectArrayNew[0] = AlertStatus.OPEN;
		objectArrayNew[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectArrayNew[2] = "60000";
		objectArrayNew[3] = "50000";
		Object[] objectArray = new Object[6];
		objectArray[0] = AlertStatus.OPEN;
		objectArray[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectArray[2] = "60000";
		objectArray[3] = "5000";
		objectList.add(objectArrayNew);
		objectList.add(objectArray);
		transactionMasterView.setId(2L);
		transactionMasterView.setTranactionProductType("tranactionProductType");
		transactionMasterView.setAmount(10000.0);
		TransactionMasterView transactionMasterView1 = new TransactionMasterView();
		transactionMasterView1.setId(2L);
		transactionMasterView1.setTranactionProductType("tranactionProductType");
		transactionMasterView1.setAmount(10000.0);
		transactionMasterViewList.add(transactionMasterView);
		alertDashBoardRespDto = new AlertDashBoardRespDto("analyat", new Date(), 10000.0);
		alertDashBoardRespDtoList.add(alertDashBoardRespDto);
		corporateStructureDto = new CorporateStructureDto(2L, "analyst", new Date(), 100000.0);
		corporateStructureDtoList.add(corporateStructureDto);
		riskCountAndRatioDto = new RiskCountAndRatioDto(new Date(), 10000.0);
		riskCountAndRatioDtoList.add(riskCountAndRatioDto);
		counterPartyNotiDto = new CounterPartyNotiDto(2L, 10000.0);
		counterPartyNotiDtoList.add(counterPartyNotiDto);
		alertedCounterPartyDto = new AlertedCounterPartyDto(1L, new Date(), "transactionType", "counterParty", "india",
				10000.0, "counterPartyBank", "Hyderabad");
		alertedCounterPartyDtoList.add(alertedCounterPartyDto);
		transactionAmountAndAlertCountDto = new TransactionAmountAndAlertCountDto(new Date(), 2L, 100000.0);
		transactionAmountAndAlertCountDtoList.add(transactionAmountAndAlertCountDto);
		counterPartyInfo = new CounterPartyInfo(1L, "analyat", 10000.0);
		counterPartyInfoList.add(counterPartyInfo);
		listDouble.add(2.0);
		listDouble.add(3.0);
		MockitoAnnotations.initMocks(this);
	}

	/*
	 * @Test public void fetchTransactionTest() {
	 * Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	 * Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(
	 * nativeQuery);
	 * Mockito.when(nativeQuery.getResultList()).thenReturn(listData);
	 * List<TransactionsData> result = txDataDaoImpl.fetchTransaction("123456");
	 * assertEquals(1, result.size()); }
	 */

	@Test
	public void getTxByTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.find(Mockito.anyObject(), Mockito.anyObject())).thenReturn(alert);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(customerDetailsDao.fetchCustomerDetails(Mockito.anyString())).thenReturn(customerDetails);
		Mockito.when(accountDao.fetchAccounts(Mockito.anyString())).thenReturn(accountlist);
		Mockito.when(query.getResultList()).thenReturn(txByDateList);
		List<TxByType> result = txDataDaoImpl.getTxByType(1L);
		assertEquals(1, result.size());

	}

	@Test
	public void getTxByDateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.find(Mockito.anyObject(), Mockito.anyObject())).thenReturn(alert);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(customerDetailsDao.fetchCustomerDetails(Mockito.anyString())).thenReturn(customerDetails);
		Mockito.when(accountDao.fetchAccounts(Mockito.anyString())).thenReturn(accountlist);
		Mockito.when(query.getResultList()).thenReturn(txByDateList);
		List<TxByDate> result = txDataDaoImpl.getTxByDate(1L);
		assertEquals(1, result.size());

	}

	@Test
	public void getTxByLocTest() throws IllegalAccessException, InvocationTargetException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(txByLocationDtolist);
		Mockito.when(query.getSingleResult()).thenReturn(country);
		List<TxByLocationDto> result = txDataDaoImpl.getTxByLoc(stringList);
		assertEquals(1, result.size());
	}

	@Test
	public void getTotalAggregateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(totalDto);
		TotalDto result = txDataDaoImpl.getTotalAggregate(stringList, "10000", locations, "business");
		assertEquals(4l, result.getCount());
	}

	@Test
	public void getCounterPartiesAggregateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(counterPartyDtoList);
		Mockito.when(query.getResultList()).thenReturn(counterPartyDtoList);
		List<CounterPartyDto> result = txDataDaoImpl.getCounterPartiesAggregate(stringList, "10000", "1000000", "5",
				locations, "business", "5000");
		assertEquals(2, result.size());
	}

	@Test
	public void getCountryAggregateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(countryAggDtoList);
		Mockito.when(query.getResultList()).thenReturn(countryAggDtoList);
		List<CountryAggDto> newList = txDataDaoImpl.getCountryAggregate(stringList, locations, "5", "business",
				"10000");
		assertEquals(2, newList.size());
	}

	@Test
	public void getOrigentAccountsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(accountList);
		List<String> accounts = txDataDaoImpl.getOrigentAccounts(stringList);
		assertEquals(2, accounts.size());
	}

	@Test
	public void getCustomerAccountsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(accountList);
		List<String> accounts = txDataDaoImpl.getCustomerAccounts(stringList);
		assertEquals(2, accounts.size());
	}

	@Test
	public void getTotalAggregateForOutputStatsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(totalDto);
		TotalDto result = txDataDaoImpl.getTotalAggregateForOutputStats(stringList, "10000", locations, "business");
		assertEquals(4l, result.getCount());
	}

	@Test
	public void getCounterPartiesAggregateForOutputStatsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(counterPartyDtoList);
		Mockito.when(query.getResultList()).thenReturn(counterPartyDtoList);
		List<CounterPartyDto> result = txDataDaoImpl.getCounterPartiesAggregateForOutputStats(stringList, "1", "10",
				"5", locations, "business", "10000");
		assertEquals(2, result.size());
	}

	@Test
	public void getCountryAggregateForOutputStatsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(countryAggDtoList);
		Mockito.when(query.getResultList()).thenReturn(countryAggDtoList);
		List<CountryAggDto> newList = txDataDaoImpl.getCountryAggregateForOutputStats(stringList, locations, "5",
				"business", "10000");
		assertEquals(2, newList.size());
	}

	@Test
	public void getBeneficiariesAccountsOutputStatsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(accountList);
		List<String> accounts = txDataDaoImpl.getBeneficiariesAccountsOutputStats(stringList);
		assertEquals("normal", accounts.get(1));
	}

	@Test
	public void getAllTransactionsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.getAllTransactions();
		assertEquals(1, result.size());
	}

	@Test
	public void getTransactionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.getTransaction("12345", "business", "10000", "");
		assertEquals(1, result.size());
	}

	@Test
	public void getTransaction_whenMintxsIsNotNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.getTransaction("12345", "business", "10000", "5");
		Assert.assertNotNull(result);
	}

	@Test
	public void getTransactionsByAccountIdsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.getTransactionsByAccountIds(stringList, new Date(), "business",
				"10000");
		assertEquals(1, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getTransactionsByAccountIds_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getTransactionsByAccountIds(stringList, new Date(), "business", "10000");
	}

	@Test
	public void getTransactionTest1() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.getTransaction(1, 10);
		assertEquals(1, result.size());
	}

	@Test
	public void fetchTxsBwDatesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.fetchTxsBwDates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(1, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchTxsBwDatesCustomizedTest() throws ParseException {
		objectList.get(0)[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionsDataNotifDto> newList = txDataDaoImpl.fetchTxsBwDatesCustomized(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 10, 1, "productType", true, true, filter);
		assertEquals(1, newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchSumTxsProductTypeTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TxProductTypeDto> newList = txDataDaoImpl.fetchSumTxsProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAlertTransactionProductCountTest() throws ParseException {
		objectList.get(0)[0] = "12345";
		objectList.get(0)[1] = "5345";
		objectList.get(0)[2] = "analyst";
		objectList.get(1)[0] = "12345";
		objectList.get(1)[1] = "5345";
		objectList.get(1)[2] = "analyst";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TxProductTypeDto> newList = txDataDaoImpl.fetchAlertTransactionProductCount(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, newList.size());
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Test public void getTotalAmountAndAlertCountByProductTypeTest() throws
	 * ParseException {
	 * Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	 * Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
	 * Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).
	 * thenReturn(new StringBuilder());
	 * Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class),Mockito.
	 * any(
	 * FilterDto.class),Mockito.anyString(),Mockito.anyString())).thenReturn(
	 * query);
	 * Mockito.when(query.getResultList()).thenReturn(transactionMasterViewList)
	 * ; List<TransactionAmountAndAlertCountDto> newList =
	 * txDataDaoImpl.getTotalAmountAndAlertCountByProductType(new
	 * SimpleDateFormat("dd-MM-yyyy").format(new Date()), new
	 * SimpleDateFormat("dd-MM-yyyy").format(new Date()), null, 10, 1, true,
	 * true, "type", filter);
	 * Mockito.when(session.createQuery(Mockito.anyString()));
	 * Mockito.when(query.getResultList()).thenReturn(objectList1);
	 * assertEquals(1, newList.size()); }
	 */

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertsBetweenDatesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> newList = txDataDaoImpl.getAlertsBetweenDates("16548", "214554", "name", 1, 10,
				"desc", filter, true);
		assertEquals(1, newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertsBetweenDates_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		List<AlertDashBoardRespDto> newList = txDataDaoImpl.getAlertsBetweenDates("16548", "214554", "name", 1, 10,
				"desc", filter, true);
		assertEquals(1, newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionAggregatesTest() throws ParseException {
		objectList.get(0)[0] = "string";
		objectList.get(1)[0] = "string";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> result = txDataDaoImpl.getTransactionAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "granularity", null, "transType", true, true,
				filter);
		assertEquals(1, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionAggregatesProductTypeTest() throws ParseException {
		objectList.get(0)[0] = "string";
		objectList.get(1)[0] = "string";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> result = txDataDaoImpl.getTransactionAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "granularity", null, "transType", true, true,
				filter);
		assertEquals(1, result.size());
	}

	@Test
	public void getTransProductTypesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(stringList);
		List<String> result = txDataDaoImpl.getTransProductTypes();
		assertEquals(2, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchCorporateStructureTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(corporateStructureDtoList);
		List<CorporateStructureDto> list = txDataDaoImpl.fetchCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerRiskTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(riskCountAndRatioDto);
		RiskCountAndRatioDto result = txDataDaoImpl.getCustomerRisk(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "",filter);
		assertEquals(riskCountAndRatioDto.getAlertCount(), result.getAlertCount());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getCustomerRisk_whenDateFormatExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(DateFormatException.class);
		txDataDaoImpl.getCustomerRisk(new Date().toString(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				"",filter);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerRisk_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCustomerRisk(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "",null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerRiskAggregatesTest() throws ParseException {
		objectList.get(0)[0] = "8745";
		objectList.get(0)[1] = "89778";
		objectList.get(1)[0] = "8745";
		objectList.get(1)[1] = "89778";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getCustomerRiskAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getCustomerRiskAggregates_whenDateFormatExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		txDataDaoImpl.getCustomerRiskAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerRiskAggregates_whenExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCustomerRiskAggregates("String", " ", null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getRiskAlertsTest() throws ParseException {
		objectList.get(0)[0] = "60000";
		objectList.get(1)[0] = "60000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(riskCountAndRatioDto);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		RiskCountAndRatioDto result = txDataDaoImpl.getRiskAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "queryforCondition", filter);
		assertEquals(new Double("120000"), result.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getRiskAlerts_whenDateFormatExceptionThrowTest() throws ParseException {
		objectList.get(0)[0] = "60000";
		objectList.get(1)[0] = "60000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(DateFormatException.class);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		txDataDaoImpl.getRiskAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "queryforCondition", filter);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getRiskAlerts_whenFailedToExecuteQueryExceptionThrowTest() throws ParseException {
		objectList.get(0)[0] = "60000";
		objectList.get(1)[0] = "60000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		txDataDaoImpl.getRiskAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "queryforCondition", filter);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchCounterPartiesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(counterPartyNotiDtoList);
		List<CounterPartyNotiDto> list = txDataDaoImpl.fetchCounterParties(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAssociatedEntityTest() throws ParseException {
		objectList.get(0)[0] = "8745";
		objectList.get(0)[1] = "89778";
		objectList.get(1)[0] = "8745";
		objectList.get(1)[1] = "89778";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AssociatedEntityRespDto> list = txDataDaoImpl.fetchAssociatedEntity(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchTopFiveTransactionsTest() throws ParseException {
		objectList.get(0)[0] = "analyst";
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		objectList.get(1)[0] = "analyst";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setMaxResults(Mockito.anyInt())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CounterPartyNotiDto> list = txDataDaoImpl.fetchTopFiveTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchTopFiveTransactions_WhenObjectsListIsEmptyTest() throws ParseException {
		List<Object[]> objectEmptyList = new ArrayList<Object[]>();
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setMaxResults(Mockito.anyInt())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectEmptyList);
		List<CounterPartyNotiDto> list = txDataDaoImpl.fetchTopFiveTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(0, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void alertComparisonTransactionsTest() throws ParseException {
		objectList.get(0)[0] = "analyst";
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		objectList.get(1)[0] = "analyst";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AlertComparisonDto> list = txDataDaoImpl.alertComparisonTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new Date(), filter,
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void viewAllTest() throws ParseException {
		objectList.get(0)[3] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectList.get(1)[3] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.viewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10, filter, "queryCondition");
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void viewAll_whenDateFormatExceptionTest() throws ParseException {
		objectList.get(0)[3] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectList.get(1)[3] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		txDataDaoImpl.viewAll(new Date().toString(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10,
				filter, "queryCondition");
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void viewAll_whenExceptionTest() throws ParseException {
		objectList.get(0)[3] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectList.get(1)[3] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.viewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10, null, "queryCondition");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void viewAllCountTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = txDataDaoImpl.viewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, filter, "queryCondition");
		assertEquals(2L, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void viewAllCount_whenDateFormatExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(DateFormatException.class);
		txDataDaoImpl.viewAllCount(new Date().toString(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true,
				filter, "queryCondition");
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void viewAllCount_whenExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.viewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, null, "queryCondition");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAllTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getViewAll(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10, "queryCondition", filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAll_whenObjectArrayNotContainsStringTest() throws ParseException {
		objectList.get(0)[0] = 1245;
		objectList.get(1)[0] = 3214;
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getViewAll(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10, "queryCondition", filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAll_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[0] = 1245;
		objectList.get(1)[0] = 3214;
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getViewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10, "queryCondition", null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getViewAllCountTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		long result = txDataDaoImpl.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "queryCondition", filter);
		assertEquals(1L, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCount_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "queryCondition", filter);
	}

	@Test
	public void getScenarioAggregatesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getScenarioAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "columnToCondition", stringList, "type");
		assertEquals(1, list.size());
	}

	@Test
	public void getScenarioAggregates_whenReturnNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(null);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getScenarioAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "columnToCondition", stringList, "type");
		assertEquals(null, list);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getScenarioAggregates_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getScenarioAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "columnToCondition", stringList, "type");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void counterPartyLocationsPlotTest() throws ParseException {
		objectList.get(0)[1] = "1245";
		objectList.get(1)[1] = "3214";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CounterPartyNotiDto> list = txDataDaoImpl.counterPartyLocationsPlot(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, true, "queryCondition", filter);
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void counterPartyLocationsPlot_whenFilterDtoIsNullTest() throws ParseException {
		objectList.get(0)[1] = "1245";
		objectList.get(1)[1] = "3214";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CounterPartyNotiDto> list = txDataDaoImpl.counterPartyLocationsPlot(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, true, "queryCondition", null);
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void counterPartyLocationsPlot_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[1] = "1245";
		objectList.get(1)[1] = "3214";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.counterPartyLocationsPlot(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, true, "queryCondition", null);
	}

	@Test
	public void getAlertedTransactionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getAlertedTransaction(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), stringList);
		assertEquals(1, list.size());
	}

	@Test
	public void getAlertedTransaction_whenReturnNullTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(null);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getAlertedTransaction(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), stringList);
		assertEquals(null, list);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertedTransaction_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getAlertedTransaction(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), stringList);
	}

	@Test
	public void getBankAggregatesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getBankAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, true, "type");
		assertEquals(1, list.size());
	}

	@Test
	public void getBankAggregates_whenReturnNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(null);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getBankAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, true, "type");
		assertEquals(null, list);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getBankAggregates_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getBankAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, true, "type");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void amlTopCustomersTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TopAmlCustomersDto> list = txDataDaoImpl.amlTopCustomers(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void amlTopCustomers_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.amlTopCustomers(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
	}

	@Test
	public void getViewAllForBanksTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getViewAllForBanks(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllForBanks_ExceptionthrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getViewAllForBanks(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10);
	}

	@Test
	public void getInputAndOutputTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(transactionAmountAndAlertCountDtoList);
		List<TransactionAmountAndAlertCountDto> list = txDataDaoImpl.getInputAndOutput(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "queryCondition");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getInputAndOutput_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getInputAndOutput(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "queryCondition");
	}

	@Test
	public void getViewAllCountBanksTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		long result = txDataDaoImpl.getViewAllCountBanks(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true);
		assertEquals(1L, result);
	}

	@Test
	public void getViewAllCountBanks_whenIsTransIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		long result = txDataDaoImpl.getViewAllCountBanks(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false);
		assertEquals(1L, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCountBanks_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getViewAllCountBanks(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true);
	}

	@Test
	public void getViewAllCountryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getViewAllCountry(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, 1, 10);
		assertEquals(1, list.size());
	}

	@Test
	public void getViewAllCountry_whenIsTransIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getViewAllCountry(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, 1, 10);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllCountry_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getViewAllCountry(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, 1, 10);
	}

	@Test
	public void getViewAllContryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		long result = txDataDaoImpl.getViewAllContry(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true);
		assertEquals(1L, result);
	}

	@Test
	public void getViewAllContry_whenIsTransIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		long result = txDataDaoImpl.getViewAllContry(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false);
		assertEquals(1L, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getViewAllContry_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getViewAllContry(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void counterPartyCountryAggregateTest() throws ParseException {
		objectList.get(0)[1] = "9745";
		objectList.get(0)[2] = "new SimpleDateFormat(\"dd-MM-yyyy\").format(new Date())";
		objectList.get(1)[1] = "9754";
		objectList.get(1)[2] = "new SimpleDateFormat(\"dd-MM-yyyy\").format(new Date())";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<CounterPartyInfo> list = txDataDaoImpl.counterPartyCountryAggregate(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, filter);
		assertEquals(0, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void counterPartyCountryAggregate_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[1] = "9745";
		objectList.get(0)[2] = "new SimpleDateFormat(\"dd-MM-yyyy\").format(new Date())";
		objectList.get(1)[1] = "9754";
		objectList.get(1)[2] = "new SimpleDateFormat(\"dd-MM-yyyy\").format(new Date())";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.counterPartyCountryAggregate(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, filter);
	}

	@Test
	public void getCustomerTransactionsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getCustomerTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, 1, 10, true);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerTransactions_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCustomerTransactions(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, 1, 10, true);
	}

	@Test
	public void getCustomerTransactionsCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		long result = txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerTransactionsCount_whenExcecptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true);
	}

	@Test
	public void getBankByTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(riskCountAndRatioDto);
		RiskCountAndRatioDto result = txDataDaoImpl.getBankByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "type");
		assertEquals(riskCountAndRatioDto.getTransactionAmount(), result.getTransactionAmount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getBankByType_whenReturnNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		RiskCountAndRatioDto result = txDataDaoImpl.getBankByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, "type1");
		assertEquals(null, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getBankByType_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getBankByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "type");
	}

	@Test
	public void counterPartyCountryAggregateOutTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(counterPartyInfoList);
		List<CounterPartyInfo> list = txDataDaoImpl.counterPartyCountryAggregateOut(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void counterPartyCountryAggregateOut_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.counterPartyCountryAggregateOut(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), null, true);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactionsTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getCustomerTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, true, true, filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactions_whenisOutTransactionIsFalseTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getCustomerTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, false, true, filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactions_whenisDetectedFalseTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getCustomerTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, false, true, true, filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactions_whenisDetectedAndOutTransFalseTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		List<AlertedCounterPartyDto> list = txDataDaoImpl.getCustomerTransactions(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, false, false, true, filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getCustomerTransactions_whenDateFormatExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		txDataDaoImpl.getCustomerTransactions(new Date().toString(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, true, true, filter);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerTransactions_whenExceptionTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCustomerTransactions(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, true, true, filter);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactionsCountTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		long result = txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, true, true, filter);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactionsCount_whenIsOutTransFalseTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		long result = txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, true, false, true, filter);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactionsCount_whenIsDetectIsFalseTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		long result = txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, false, true, true, filter);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCustomerTransactionsCount_whenIsDetectAndIsOutTransFalseTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertedCounterPartyDtoList);
		long result = txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, false, false, true, filter);
		assertEquals(1l, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getCustomerTransactionsCount_whenDateFormatExceptioTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		txDataDaoImpl.getCustomerTransactionsCount(new Date().toString(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, false, true, true, filter);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerTransactionsCount_whenExceptioTest1() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCustomerTransactionsCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1L, false, true, true, filter);
	}

	@Test
	public void getCreditCardLuxuryTransactionsCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		int result = txDataDaoImpl.getCreditCardLuxuryTransactionsCount("originatorAccountId", "luxuryShopeNames", true,
				new Date());
		assertEquals(1, result);
	}

	@Test
	public void getCreditCardLuxuryTransactionsCount_whenshopOrWebsiteIsFalseTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		int result = txDataDaoImpl.getCreditCardLuxuryTransactionsCount("originatorAccountId", "luxuryShopeNames",
				false, new Date());
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardLuxuryTransactionsCount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		int result = txDataDaoImpl.getCreditCardLuxuryTransactionsCount("originatorAccountId", "luxuryShopeNames",
				false, new Date());
		assertEquals(0, result);
	}

	@Test
	public void getCreditCardLuxuryTransactionsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		int result = txDataDaoImpl.getCreditCardLuxuryTransactions("originatorAccountId", "beneficaryAccountId");
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCreditCardLuxuryTransactions_whenExceptionThrow() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCreditCardLuxuryTransactions("originatorAccountId", "beneficaryAccountId");
	}

	@Test
	public void getMasterDataFromViewTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(transactionMasterViewList);
		List<TransactionMasterView> list = txDataDaoImpl.getMasterDataFromView();
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getMasterDataFromView_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getMasterDataFromView();
	}

	@Test
	public void getCreditCardNewCountryATMTransactionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		int result = txDataDaoImpl.getCreditCardNewCountryATMTransaction("originatorAccountId", "india", new Date());
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardNewCountryATMTransaction_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardNewCountryATMTransaction("originatorAccountId", "india", new Date());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getRiskAlertsAggregatesTest() throws ParseException {
		objectList.get(0)[0] = "456223";
		objectList.get(1)[0] = "77852";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		RiskCountAndRatioDto result = txDataDaoImpl.getRiskAlertsAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "value", filter);
		Assert.assertNotNull(result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getRiskAlertsAggregates_whenDateFormatExceptionTest() throws ParseException {
		objectList.get(0)[0] = "456223";
		objectList.get(1)[0] = "77852";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		txDataDaoImpl.getRiskAlertsAggregates(new Date().toString(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "value", filter);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getRiskAlertsAggregates_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[0] = "456223";
		objectList.get(1)[0] = "77852";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getRiskAlertsAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "value", filter);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCorruptionRiskAggregatesTest() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[4] = "String";
		objectList.get(0)[5] = "newString";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[4] = "String";
		objectList.get(1)[5] = "newString";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getCorruptionRiskAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "value", filter);
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getCorruptionRiskAggregates_whenDateFormatExceptionTest() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[4] = "String";
		objectList.get(0)[5] = "newString";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[4] = "String";
		objectList.get(1)[5] = "newString";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		txDataDaoImpl.getCorruptionRiskAggregates(new Date().toString(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "value", filter);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCorruptionRiskAggregates_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[4] = "String";
		objectList.get(0)[5] = "newString";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[4] = "String";
		objectList.get(1)[5] = "newString";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCorruptionRiskAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "value", filter);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCounterPartyGeoAggregatesTest() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[2] = "String";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[2] = "String";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getCounterPartyGeoAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter, true);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCounterPartyGeoAggregates_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[2] = "String";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[2] = "String";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getCounterPartyGeoAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter, true);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getBankAggregatesTest1() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[2] = "String";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[2] = "String";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> list = txDataDaoImpl.getBankAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getBankAggregates_whenExceptionThrowTest1() throws ParseException {
		objectList.get(0)[0] = "9985";
		objectList.get(0)[2] = "String";
		objectList.get(1)[0] = "9985";
		objectList.get(1)[2] = "String";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getBankAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filter);
	}

	@Test
	public void getCreditCardMontlyAverageOfThreeMonthsTransactionsTest() {
		objectList.get(0)[1] = "6";
		objectList.get(1)[0] = null;
		objectList.get(1)[1] = "6";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		Double result = txDataDaoImpl.getCreditCardMontlyAverageOfThreeMonthsTransactions("123466789", new Date());
		assertEquals(new Double(3.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardMontlyAverageOfThreeMonthsTransactions_WhenExceptionThrowTest() {
		objectList.get(0)[1] = "6";
		objectList.get(1)[0] = null;
		objectList.get(1)[1] = "6";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardMontlyAverageOfThreeMonthsTransactions("123466789", new Date());
	}

	@Test
	public void getCreditCardMontlyAverageOfTransactionsTest() {
		objectList.get(0)[1] = "6";
		objectList.get(1)[0] = null;
		objectList.get(1)[1] = "6";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		Double result = txDataDaoImpl.getCreditCardMontlyAverageOfTransactions("1234566899", new Date());
		assertEquals(new Double(3.0), result);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardMontlyAverageOfTransactions_whenExceptionThrowTest() {
		objectList.get(0)[1] = "6";
		objectList.get(1)[0] = null;
		objectList.get(1)[1] = "6";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardMontlyAverageOfTransactions("1234566899", new Date());
	}

	@Test
	public void fetchAlertsByNumberTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> list = txDataDaoImpl.fetchAlertsByNumber("123456789");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fetchAlertsByNumber_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.fetchAlertsByNumber("123456789");
	}

	@Test
	public void getCreditCardATMCashInAverageTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2.0);
		Double result = txDataDaoImpl.getCreditCardATMCashInAverage("124563", new Date());
		assertEquals(new Double(2.0), result);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardATMCashInAverage_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardATMCashInAverage("124563", new Date());
	}

	@Test
	public void getCreditCardATMWithdrawalAverageTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2.0);
		Double result = txDataDaoImpl.getCreditCardATMWithdrawalAverage("123456789", new Date());
		assertEquals(new Double(2.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardATMWithdrawalAverage_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardATMWithdrawalAverage("123456789", new Date());
	}

	@Test
	public void getCreditCardATMWithdrawlsInShortTimeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		int result = txDataDaoImpl.getCreditCardATMWithdrawlsInShortTime("123456789", new Date(), "hyderabad");
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardATMWithdrawlsInShortTime_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardATMWithdrawlsInShortTime("123456789", new Date(), "hyderabad");
	}

	@Test
	public void getCreditCardAtmWithdrawlLastSevenDaysAmountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listDouble);
		List<Double> result = txDataDaoImpl.getCreditCardAtmWithdrawlLastSevenDaysAmount("123456789", "India",
				new Date());
		assertEquals(2, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardAtmWithdrawlLastSevenDaysAmount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardAtmWithdrawlLastSevenDaysAmount("123456789", "India", new Date());
	}

	@Test
	public void getPreviousTransactionsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> result = txDataDaoImpl.getPreviousTransactions("123456789", "125478963");
		assertEquals(1, result.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getPreviousTransactions_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getPreviousTransactions("123456789", "125478963");
	}

	@Test
	public void getCreditCardUnrelatedCounterPartyOneMonthAmountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(200000.0);
		Double result = txDataDaoImpl.getCreditCardUnrelatedCounterPartyOneMonthAmount("1234566", "123456789",
				new Date());
		assertEquals(new Double(200000.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardUnrelatedCounterPartyOneMonthAmount_whenExceptionthrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardUnrelatedCounterPartyOneMonthAmount("1234566", "123456789", new Date());
	}

	@Test
	public void getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(200000.0);
		Double result = txDataDaoImpl.getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmount("123456789",
				"124785963", new Date());
		assertEquals(new Double(200000.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmount("123456789", "124785963",
				new Date());
	}

	@Test
	public void getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactionsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		List<TransactionsData> list = txDataDaoImpl
				.getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions("123456789", new Date());
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		txDataDaoImpl.getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions("123456789", new Date());
	}

	@Test
	public void getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorNameTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(10000.0);
		Double result = txDataDaoImpl.getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorName(
				"programmer", "123456789", new Date());
		assertEquals(new Double(10000.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorName_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		Double result = txDataDaoImpl.getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorName(
				"programmer", "123456789", new Date());
		assertEquals(new Double(0.0), result);
	}

	@Test
	public void getThreeMonthsTransSumWithMerchantTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(10000.0);
		Double result = txDataDaoImpl.getThreeMonthsTransSumWithMerchant("123456789", "merchant", new Date());
		assertEquals(new Double(10000.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getThreeMonthsTransSumWithMerchant_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		Double result = txDataDaoImpl.getThreeMonthsTransSumWithMerchant("123456789", "merchant", new Date());
		assertEquals(new Double(0.0), result);
	}

	@Test
	public void getOverAllCreditAmountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(10000.0);
		Double result = txDataDaoImpl.getOverAllCreditAmount("123456789", "merchant");
		assertEquals(new Double(10000.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getOverAllCreditAmount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		Double result = txDataDaoImpl.getOverAllCreditAmount("123456789", "merchant");
		assertEquals(new Double(0.0), result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getOverAllDebitAmountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		Double result = txDataDaoImpl.getOverAllDebitAmount("123456789", "merchant");
		assertEquals(new Double(0.0), result);
	}

	@Test
	public void getPreviousTransactionsCountWithMerchantTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(listData);
		int result = txDataDaoImpl.getPreviousTransactionsCountWithMerchant("123456789", "merchant", new Date());
		assertEquals(1, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getPreviousTransactionsCountWithMerchant_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		int result = txDataDaoImpl.getPreviousTransactionsCountWithMerchant("123456789", "merchant", new Date());
		assertEquals(0, result);
	}

	@Test
	public void fetchAlertsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> list = txDataDaoImpl.fetchAlerts();
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fetchAlerts_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.fetchAlerts();
	}

	@Test
	public void getEntityBasedAlertsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> list = txDataDaoImpl.getEntityBasedAlerts(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 10, "name", "entityType");
		assertEquals(1, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getEntityBasedAlerts_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		txDataDaoImpl.getEntityBasedAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 10, "name", "entityType");
	}

}
