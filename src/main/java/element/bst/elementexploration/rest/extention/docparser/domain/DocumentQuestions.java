package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_QUESTIONS")
public class DocumentQuestions implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String name;// question
	private boolean questionStatus = false;
	private AnswerType answerType;
	private DocumentContents documentContent;

	private String component;// only for tables
	private String componentQuestionMatrix;
	private String componentAnswerMatrix;// only for tables

	private String possibleAnswer;
	private int riskScore;
	private String riskType;
	private String primaryIsoCode;
	private String secondaryIsoCode;
	private Integer evaluation;
	private String pdfAnswerCode;
	private List<DocumentAnswers> documentAnswers;
	private String contentType;
	private Integer tableID;
	private long templateId;
	private String splitWith;
	private String answerAtPosition;
	private Integer answerTableId;
	private String topic;
	private boolean requiredForOSINT;
	private String level1Code;
	private String level1Title;
	private String level3Title;
	private String level3Description;
	private String caseField;
	private int scannedPdfPageNumber;
	private boolean OSINT;
	private Boolean showInUI;
	private long hasParentId;
	private Boolean hasIsoCode;
	private String questionPosition;
	private String answerPosition;
	private List<PossibleAnswers> possibleAnswers;
	private int pageNumber;
	private String formField;
	private String subType;
	private boolean deleteStatus = false;
	private String questionPositionOnThePage;
	private String answerPositionOnThePage;
	private String questionUUID;
	private String answerUUID;
	private String elementName;

	public DocumentQuestions() {
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "NAME", columnDefinition = "LONGTEXT")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne
	@JoinColumn(name = "DOCUMENT_CONTENT_ID")
	public DocumentContents getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(DocumentContents documentContent) {
		this.documentContent = documentContent;
	}

	@Column(name = "IS_QUESTION")
	public boolean isQuestionStatus() {
		return questionStatus;
	}

	public void setQuestionStatus(boolean questionStatus) {
		this.questionStatus = questionStatus;
	}

	@Column(name = "COMPONENT")
	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	@Column(name = "POSSIBLE_ANSWER")
	public String getPossibleAnswer() {
		return possibleAnswer;
	}

	public void setPossibleAnswer(String possibleAnswer) {
		this.possibleAnswer = possibleAnswer;
	}

	@Column(name = "RISK_SCORE")
	public int getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(int riskScore) {
		this.riskScore = riskScore;
	}

	@Column(name = "ANSWER_TYPE")
	@Enumerated(EnumType.STRING)
	public AnswerType getAnswerType() {
		return answerType;
	}

	public void setAnswerType(AnswerType answerType) {
		this.answerType = answerType;
	}

	@Column(name = "COMPONENT_QUESTION_MATRIX")
	public String getComponentQuestionMatrix() {
		return componentQuestionMatrix;
	}

	public void setComponentQuestionMatrix(String componentQuestionMatrix) {
		this.componentQuestionMatrix = componentQuestionMatrix;
	}

	@Column(name = "COMPONENT_ANSWER_MATRIX")
	public String getComponentAnswerMatrix() {
		return componentAnswerMatrix;
	}

	public void setComponentAnswerMatrix(String componentAnswerMatrix) {
		this.componentAnswerMatrix = componentAnswerMatrix;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentQuestions")
	public List<DocumentAnswers> getDocumentAnswers() {
		return documentAnswers;
	}

	public void setDocumentAnswers(List<DocumentAnswers> documentAnswers) {
		this.documentAnswers = documentAnswers;
	}

	@Column(name = "RISK_TYPE")
	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	@Column(name = "PRIMARY_ISOCODE")
	public String getPrimaryIsoCode() {
		return primaryIsoCode;
	}

	public void setPrimaryIsoCode(String primaryIsoCode) {
		this.primaryIsoCode = primaryIsoCode;
	}

	@Column(name = "SECONDARY_ISOCODE")
	public String getSecondaryIsoCode() {
		return secondaryIsoCode;
	}

	public void setSecondaryIsoCode(String secondaryIsoCode) {
		this.secondaryIsoCode = secondaryIsoCode;
	}

	@Column(name = "EVALUATION")
	public Integer getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Integer evalution) {
		this.evaluation = evalution;
	}

	@Column(name = "PDF_ANSWER_CODE")
	public String getPdfAnswerCode() {
		return pdfAnswerCode;
	}

	public void setPdfAnswerCode(String pdfAnswerCode) {
		this.pdfAnswerCode = pdfAnswerCode;
	}

	@Column(name = "CONTENT_TYPE")
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Column(name = "TABLE_ID")
	public Integer getTableID() {
		return tableID;
	}

	public void setTableID(Integer tableID) {
		this.tableID = tableID;
	}

	@Column(name = "TEMPLATE_ID")
	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	@Column(name = "SPLIT_WITH")
	public String getSplitWith() {
		return splitWith;
	}

	public void setSplitWith(String splitWith) {
		this.splitWith = splitWith;
	}

	@Column(name = "ANSWER_POSITION")
	public String getAnswerAtPosition() {
		return answerAtPosition;
	}

	public void setAnswerAtPosition(String answerAtPosition) {
		this.answerAtPosition = answerAtPosition;
	}

	@Column(name = "ANSWER_TABLE_ID")
	public Integer getAnswerTableId() {
		return answerTableId;
	}

	public void setAnswerTableId(Integer answerTableId) {
		this.answerTableId = answerTableId;
	}

	@Column(name = "TOPIC")
	public String getTopic() {
		return topic;
	}

	@Column(name = "TOPIC")
	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Column(name = "REQUIRED_FOR_OSINT")
	public boolean isRequiredForOSINT() {
		return requiredForOSINT;
	}

	public void setRequiredForOSINT(boolean requiredForOSINT) {
		this.requiredForOSINT = requiredForOSINT;
	}

	@Column(name = "LEVEL1_ISO_CODE")
	public String getLevel1Code() {
		return level1Code;
	}

	public void setLevel1Code(String level1Code) {
		this.level1Code = level1Code;
	}

	@Column(name = "LEVEL1_TITLE")
	public String getLevel1Title() {
		return level1Title;
	}

	public void setLevel1Title(String level1Title) {
		this.level1Title = level1Title;
	}

	@Column(name = "LEVEL3_TITLE")
	public String getLevel3Title() {
		return level3Title;
	}

	public void setLevel3Title(String level3Title) {
		this.level3Title = level3Title;
	}

	@Column(name = "LEVEL3_DESCRIPTION")
	public String getLevel3Description() {
		return level3Description;
	}

	public void setLevel3Description(String level3Description) {
		this.level3Description = level3Description;
	}

	@Column(name = "CASE_FIELD")
	public String getCaseField() {
		return caseField;
	}

	public void setCaseField(String caseField) {
		this.caseField = caseField;
	}

	@Column(name = "SCANNED_PDF_PAGE_NUMBER")
	public int getScannedPdfPageNumber() {
		return scannedPdfPageNumber;
	}

	public void setScannedPdfPageNumber(int scannedPdfPageNumber) {
		this.scannedPdfPageNumber = scannedPdfPageNumber;
	}

	@Column(name = "IS_OSINT")
	public boolean isOSINT() {
		return OSINT;
	}

	public void setOSINT(boolean oSINT) {
		OSINT = oSINT;
	}

	@Column(name = "SHOW_IN_UI")
	public Boolean getShowInUI() {
		return showInUI;
	}

	public void setShowInUI(Boolean showInUI) {
		this.showInUI = showInUI;
	}

	@Column(name = "HAS_PARENT_ID")
	public long getHasParentId() {
		return hasParentId;
	}

	public void setHasParentId(long hasParentId) {
		this.hasParentId = hasParentId;
	}

	@Column(name = "HAS_ISO_CODE")
	public Boolean isHasIsoCode() {
		return hasIsoCode;
	}

	public void setHasIsoCode(Boolean hasIsoCode) {
		this.hasIsoCode = hasIsoCode;
	}

	@Column(name = "QUESTION_POSITION_COORD")
	public String getQuestionPosition() {
		return questionPosition;
	}

	public void setQuestionPosition(String questionPosition) {
		this.questionPosition = questionPosition;
	}

	@Column(name = "ANSWER_POSITION_COORD")
	public String getAnswerPosition() {
		return answerPosition;
	}

	public void setAnswerPosition(String answerPosition) {
		this.answerPosition = answerPosition;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentQuestions")
	public List<PossibleAnswers> getPossibleAnswers() {
		return possibleAnswers;
	}

	public void setPossibleAnswers(List<PossibleAnswers> possibleAnswers) {
		this.possibleAnswers = possibleAnswers;
	}

	@Column(name = "PAGE_NUMBER")
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	@Column(name = "FORM_FIELD")
	public String getFormField() {
		return formField;
	}

	public void setFormField(String formField) {
		this.formField = formField;
	}

	@Column(name = "SUB_TYPE")
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	@Column(name = "DELETE_STATUS")
	public boolean isDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(boolean deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	@Column(name = "QUESTION_POSITION_ON_THE_PAGE")
	public String getQuestionPositionOnThePage() {
		return questionPositionOnThePage;
	}

	public void setQuestionPositionOnThePage(String questionPositionOnThePage) {
		this.questionPositionOnThePage = questionPositionOnThePage;
	}

	@Column(name = "ANSWER_POSITION_ON_THE_PAGE")
	public String getAnswerPositionOnThePage() {
		return answerPositionOnThePage;
	}

	public void setAnswerPositionOnThePage(String answerPositionOnThePage) {
		this.answerPositionOnThePage = answerPositionOnThePage;
	}
	
	@Column(name = "QUESTION_UUID")
	public String getQuestionUUID() {
		return questionUUID;
	}

	public void setQuestionUUID(String questionUUID) {
		this.questionUUID = questionUUID;
	}
	
	@Column(name = "ANSWER_UUID")
	public String getAnswerUUID() {
		return answerUUID;
	}

	public void setAnswerUUID(String answerUUID) {
		this.answerUUID = answerUUID;
	}
	
	@Column(name = "ELEMENT_NAME")
	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	
	

}
