package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author rambabu
 *
 */
@ApiModel("Transactions between dates")
public class TransactionsBetweenDatesResponseDto implements Serializable {

		private static final long serialVersionUID = 1L;
		@ApiModelProperty(value="Transactions aggregation response")	
		private NotificationDto txsAggRespDto;

		
		public TransactionsBetweenDatesResponseDto() {
			super();			
		}
		
		public NotificationDto getTxsAggRespDto() {
			return txsAggRespDto;
		}

		public void setTxsAggRespDto(NotificationDto txsAggRespDto) {
			this.txsAggRespDto = txsAggRespDto;
		}
		
		
		
		

}
