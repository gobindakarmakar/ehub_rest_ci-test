package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;

/**
 * @author suresh
 *
 */
public class DocParserContentSave implements Serializable {

	private static final long serialVersionUID = 1L;

	private String question;
	private String answer;
	private Long questionId;
	private Long answerId;
	private String text;
	private AnswerType answerType;
	private String possibleAnswer;
	private String actualAnswer;

	private String questionPosition;
	private String answerPosition;
	private List<PossibleAnswersDto> possibleAnswersDto;

	private List<PossibleAnswersDto> specifiedPossibleAnswersDto;

	private int pageNumber;
	private String formField;
	private String subType;
	private String specifiedQuestionPosition;
	private String specifiedAnswerPosition;
	private String specifiedQuestion;
	private String specifiedAnswer;
	private AnswerType specifiedAnswerType;
	private long specifiedQuestionId;
	private long specifiedAnswerId;
	private String questionPositionOnThePage;
	private String answerPositionOnThePage;
	private String specifiedQuestionPositionOnThePage;
	private String specifiedAnswerPositionOnThePage;
	private String questionUUID;
	private String answerUUID;
	private String elementName;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public AnswerType getAnswerType() {
		return answerType;
	}

	public void setAnswerType(AnswerType answerType) {
		this.answerType = answerType;
	}

	public String getPossibleAnswer() {
		return possibleAnswer;
	}

	public void setPossibleAnswer(String possibleAnswer) {
		this.possibleAnswer = possibleAnswer;
	}

	public String getActualAnswer() {
		return actualAnswer;
	}

	public void setActualAnswer(String actualAnswer) {
		this.actualAnswer = actualAnswer;
	}

	public String getQuestionPosition() {
		return questionPosition;
	}

	public void setQuestionPosition(String questionPosition) {
		this.questionPosition = questionPosition;
	}

	public String getAnswerPosition() {
		return answerPosition;
	}

	public void setAnswerPosition(String answerPosition) {
		this.answerPosition = answerPosition;
	}

	public List<PossibleAnswersDto> getPossibleAnswersDto() {
		return possibleAnswersDto;
	}

	public void setPossibleAnswersDto(List<PossibleAnswersDto> possibleAnswersDto) {
		this.possibleAnswersDto = possibleAnswersDto;
	}

	public List<PossibleAnswersDto> getSpecifiedPossibleAnswersDto() {
		return specifiedPossibleAnswersDto;
	}

	public void setSpecifiedPossibleAnswersDto(List<PossibleAnswersDto> specifiedPossibleAnswersDto) {
		this.specifiedPossibleAnswersDto = specifiedPossibleAnswersDto;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getFormField() {
		return formField;
	}

	public void setFormField(String formField) {
		this.formField = formField;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getSpecifiedQuestionPosition() {
		return specifiedQuestionPosition;
	}

	public void setSpecifiedQuestionPosition(String specifiedQuestionPosition) {
		this.specifiedQuestionPosition = specifiedQuestionPosition;
	}

	public String getSpecifiedAnswerPosition() {
		return specifiedAnswerPosition;
	}

	public void setSpecifiedAnswerPosition(String specifiedAnswerPosition) {
		this.specifiedAnswerPosition = specifiedAnswerPosition;
	}

	public String getSpecifiedQuestion() {
		return specifiedQuestion;
	}

	public void setSpecifiedQuestion(String specifiedQuestion) {
		this.specifiedQuestion = specifiedQuestion;
	}

	public String getSpecifiedAnswer() {
		return specifiedAnswer;
	}

	public void setSpecifiedAnswer(String specifiedAnswer) {
		this.specifiedAnswer = specifiedAnswer;
	}

	public AnswerType getSpecifiedAnswerType() {
		return specifiedAnswerType;
	}

	public void setSpecifiedAnswerType(AnswerType specifiedAnswerType) {
		this.specifiedAnswerType = specifiedAnswerType;
	}

	public long getSpecifiedQuestionId() {
		return specifiedQuestionId;
	}

	public void setSpecifiedQuestionId(long specifiedQuestionId) {
		this.specifiedQuestionId = specifiedQuestionId;
	}

	public long getSpecifiedAnswerId() {
		return specifiedAnswerId;
	}

	public void setSpecifiedAnswerId(long specifiedAnswerId) {
		this.specifiedAnswerId = specifiedAnswerId;
	}

	public String getQuestionPositionOnThePage() {
		return questionPositionOnThePage;
	}

	public void setQuestionPositionOnThePage(String questionPositionOnThePage) {
		this.questionPositionOnThePage = questionPositionOnThePage;
	}

	public String getAnswerPositionOnThePage() {
		return answerPositionOnThePage;
	}

	public void setAnswerPositionOnThePage(String answerPositionOnThePage) {
		this.answerPositionOnThePage = answerPositionOnThePage;
	}

	public String getSpecifiedQuestionPositionOnThePage() {
		return specifiedQuestionPositionOnThePage;
	}

	public void setSpecifiedQuestionPositionOnThePage(String specifiedQuestionPositionOnThePage) {
		this.specifiedQuestionPositionOnThePage = specifiedQuestionPositionOnThePage;
	}

	public String getSpecifiedAnswerPositionOnThePage() {
		return specifiedAnswerPositionOnThePage;
	}

	public void setSpecifiedAnswerPositionOnThePage(String specifiedAnswerPositionOnThePage) {
		this.specifiedAnswerPositionOnThePage = specifiedAnswerPositionOnThePage;
	}

	public String getQuestionUUID() {
		return questionUUID;
	}

	public void setQuestionUUID(String questionUUID) {
		this.questionUUID = questionUUID;
	}

	public String getAnswerUUID() {
		return answerUUID;
	}

	public void setAnswerUUID(String answerUUID) {
		this.answerUUID = answerUUID;
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

}
