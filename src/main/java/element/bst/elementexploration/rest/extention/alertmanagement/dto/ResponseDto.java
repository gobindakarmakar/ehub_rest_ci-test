package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.util.List;

public class ResponseDto {
	
	String customerId;
	String entityName;
	String entityType;
	String feed_classification;
	String feed_source;
	List<AlertMetaDataDto> alertMetaData;
	
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public String getFeed_classification() {
		return feed_classification;
	}
	public void setFeed_classification(String feed_classification) {
		this.feed_classification = feed_classification;
	}
	public String getFeed_source() {
		return feed_source;
	}
	public void setFeed_source(String feed_source) {
		this.feed_source = feed_source;
	}
	public List<AlertMetaDataDto> getAlertMetaData() {
		return alertMetaData;
	}
	public void setAlertMetaData(List<AlertMetaDataDto> alertMetaData) {
		this.alertMetaData = alertMetaData;
	}
	
	
	

}
