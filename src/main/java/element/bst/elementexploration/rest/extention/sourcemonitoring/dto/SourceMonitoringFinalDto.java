package element.bst.elementexploration.rest.extention.sourcemonitoring.dto;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * @author Prateek Maurya
 */

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class SourceMonitoringFinalDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer failedCount;
	private Integer successCount;
	private Map<String, SourceMonitoringMetadataDto> sourceMetadata;
	public Integer getFailedCount() {
		return failedCount;
	}
	public void setFailedCount(Integer failedCount) {
		this.failedCount = failedCount;
	}
	public Integer getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}
	public Map<String, SourceMonitoringMetadataDto> getSourceMetadata() {
		return sourceMetadata;
	}
	public void setSourceMetadata(Map<String, SourceMonitoringMetadataDto> sourceMetadata) {
		this.sourceMetadata = sourceMetadata;
	}
	
	
	
}
