package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;

@Entity
@Table(name = "transaction_master_view")
public class TransactionMasterView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Double amount;

	private String benificiaryAccountId;

	private String originatorAccountId;

	private Date transactionBusinessDate;

	private String tranactionProductType;

	private String transactionChannel;

	private Long alertId;

	private Long alertTransactionId;

	private Date alertBusinessDate;

	private String alertDescription;

	private String alertScenarioType;

	private String alertStatus;

	private String alertType;

	private String alertJurisdiction;

	private String alertComment;

	private String alertFocalEntityId;

	private String alertFocalEntityName;

	private Double alertMaxAmount;

	private String alertTransactionType;

	private String alertScenario;

	private String alertSearchName;

	private String alertCustomerNumber;

	private String alertCustomerDisplayName;

	private Date alertCreatedDate;

	private String alertCustomerCountry;

	private Long originatorCustomerId;

	private String originatorCustomerNumber;

	private String originatorCustomerName;

	private CustomerType originatorCustomerType;

	private String originatorIndustry;

	private String originatorCorporateStructure;

	private String originatorEstimatedAmount;

	private String originatorBusiness;

	private String originatorCountry;

	private String originatorBankName;

	private Long benificiaryCustomerId;

	private String benificiaryCustomerNumber;

	private String benificiaryCustomerName;

	private CustomerType benificiaryCustomerType;

	private String benificiaryIndustry;

	private String benificiaryCorporateStructure;

	private String benificiaryEstimatedAmount;

	private String benificiaryBusiness;

	private String benificiaryCountry;

	private String benificiaryBankName;

	private String shareHolderName;

	private Long numberOfShares;

	private Float fatfRisk;

	private Float freedomHouserisk;

	private Float internationalNarcoticsRisk;

	private Float politicalRisk;

	private Float taskJusticeRisk;

	private Float transparencyRisk;

	private Float wefRisk;

	private Float worldBankRisk;

	private Float worldJusticeRisk;

	private String latitude;

	private String longitude;

	private String beneficiaryIso2Code;

	private String originatorIso2Code;

	private Long originatorCountryId;

	private Long beneficiaryCountryId;

	private String originatorCountryLatitude;

	private String originatorCountryLongitude;

	private Integer alertRiskScore;
	
	private String entityType;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "transaction_amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "beneficiary_account")
	public String getBenificiaryAccountId() {
		return benificiaryAccountId;
	}

	public void setBenificiaryAccountId(String benificiaryAccountId) {
		this.benificiaryAccountId = benificiaryAccountId;
	}

	@Column(name = "originator_account")
	public String getOriginatorAccountId() {
		return originatorAccountId;
	}

	public void setOriginatorAccountId(String originatorAccountId) {
		this.originatorAccountId = originatorAccountId;
	}

	@Column(name = "transaction_businessdate")
	public Date getTransactionBusinessDate() {
		return transactionBusinessDate;
	}

	public void setTransactionBusinessDate(Date transactionBusinessDate) {
		this.transactionBusinessDate = transactionBusinessDate;
	}

	@Column(name = "transaction_type")
	public String getTranactionProductType() {
		return tranactionProductType;
	}

	public void setTranactionProductType(String tranactionProductType) {
		this.tranactionProductType = tranactionProductType;
	}

	@Column(name = "transaction_channel")
	public String getTransactionChannel() {
		return transactionChannel;
	}

	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

	@Column(name = "alert_id")
	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	@Column(name = "alert_businessdate")
	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	@Column(name = "alert_description")
	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	@Column(name = "alert_scenario_type")
	public String getAlertScenarioType() {
		return alertScenarioType;
	}

	public void setAlertScenarioType(String alertScenarioType) {
		this.alertScenarioType = alertScenarioType;
	}

	@Column(name = "alert_status")
	public String getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(String alertStatus) {
		this.alertStatus = alertStatus;
	}

	@Column(name = "alert_type")
	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	@Column(name = "alert_jurisdiction")
	public String getAlertJurisdiction() {
		return alertJurisdiction;
	}

	public void setAlertJurisdiction(String alertJurisdiction) {
		this.alertJurisdiction = alertJurisdiction;
	}

	@Column(name = "alert_comment")
	public String getAlertComment() {
		return alertComment;
	}

	public void setAlertComment(String alertComment) {
		this.alertComment = alertComment;
	}

	@Column(name = "alert_focalEntity_id")
	public String getAlertFocalEntityId() {
		return alertFocalEntityId;
	}

	public void setAlertFocalEntityId(String alertFocalEntityId) {
		this.alertFocalEntityId = alertFocalEntityId;
	}

	@Column(name = "alert_focal_entityname")
	public String getAlertFocalEntityName() {
		return alertFocalEntityName;
	}

	public void setAlertFocalEntityName(String alertFocalEntityName) {
		this.alertFocalEntityName = alertFocalEntityName;
	}

	@Column(name = "alert_max_amount")
	public Double getAlertMaxAmount() {
		return alertMaxAmount;
	}

	public void setAlertMaxAmount(Double alertMaxAmount) {
		this.alertMaxAmount = alertMaxAmount;
	}

	@Column(name = "alert_transaction_type")
	public String getAlertTransactionType() {
		return alertTransactionType;
	}

	public void setAlertTransactionType(String alertTransactionType) {
		this.alertTransactionType = alertTransactionType;
	}

	@Column(name = "alert_scenario")
	public String getAlertScenario() {
		return alertScenario;
	}

	public void setAlertScenario(String alertScenario) {
		this.alertScenario = alertScenario;
	}

	@Column(name = "alert_search_name")
	public String getAlertSearchName() {
		return alertSearchName;
	}

	public void setAlertSearchName(String alertSearchName) {
		this.alertSearchName = alertSearchName;
	}

	@Column(name = "alert_customer_number")
	public String getAlertCustomerNumber() {
		return alertCustomerNumber;
	}

	public void setAlertCustomerNumber(String alertCustomerNumber) {
		this.alertCustomerNumber = alertCustomerNumber;
	}

	@Column(name = "alert_customer_country")
	public String getAlertCustomerCountry() {
		return alertCustomerCountry;
	}

	public void setAlertCustomerCountry(String alertCustomerCountry) {
		this.alertCustomerCountry = alertCustomerCountry;
	}

	@Column(name = "originator_id")
	public Long getOriginatorCustomerId() {
		return originatorCustomerId;
	}

	public void setOriginatorCustomerId(Long originatorCustomerId) {
		this.originatorCustomerId = originatorCustomerId;
	}

	@Column(name = "originator_customer_number")
	public String getOriginatorCustomerNumber() {
		return originatorCustomerNumber;
	}

	public void setOriginatorCustomerNumber(String originatorCustomerNumber) {
		this.originatorCustomerNumber = originatorCustomerNumber;
	}

	@Column(name = "originator_customer_displayname")
	public String getOriginatorCustomerName() {
		return originatorCustomerName;
	}

	public void setOriginatorCustomerName(String originatorCustomerName) {
		this.originatorCustomerName = originatorCustomerName;
	}

	@Column(name = "originator_customer_type")
	@Enumerated(EnumType.STRING)
	public CustomerType getOriginatorCustomerType() {
		return originatorCustomerType;
	}

	public void setOriginatorCustomerType(CustomerType originatorCustomerType) {
		this.originatorCustomerType = originatorCustomerType;
	}

	@Column(name = "originator_industry")
	public String getOriginatorIndustry() {
		return originatorIndustry;
	}

	public void setOriginatorIndustry(String originatorIndustry) {
		this.originatorIndustry = originatorIndustry;
	}

	@Column(name = "originator_corporate_structure")
	public String getOriginatorCorporateStructure() {
		return originatorCorporateStructure;
	}

	public void setOriginatorCorporateStructure(String originatorCorporateStructure) {
		this.originatorCorporateStructure = originatorCorporateStructure;
	}

	@Column(name = "originator_estimated_amount")
	public String getOriginatorEstimatedAmount() {
		return originatorEstimatedAmount;
	}

	public void setOriginatorEstimatedAmount(String originatorEstimatedAmount) {
		this.originatorEstimatedAmount = originatorEstimatedAmount;
	}

	@Column(name = "originator_business")
	public String getOriginatorBusiness() {
		return originatorBusiness;
	}

	public void setOriginatorBusiness(String originatorBusiness) {
		this.originatorBusiness = originatorBusiness;
	}

	@Column(name = "originator_country")
	public String getOriginatorCountry() {
		return originatorCountry;
	}

	public void setOriginatorCountry(String originatorCountry) {
		this.originatorCountry = originatorCountry;
	}

	@Column(name = "originator_bank_name")
	public String getOriginatorBankName() {
		return originatorBankName;
	}

	public void setOriginatorBankName(String originatorBankName) {
		this.originatorBankName = originatorBankName;
	}

	@Column(name = "beneficiary_id")
	public Long getBenificiaryCustomerId() {
		return benificiaryCustomerId;
	}

	public void setBenificiaryCustomerId(Long benificiaryCustomerId) {
		this.benificiaryCustomerId = benificiaryCustomerId;
	}

	@Column(name = "beneficiary_customer_number")
	public String getBenificiaryCustomerNumber() {
		return benificiaryCustomerNumber;
	}

	public void setBenificiaryCustomerNumber(String benificiaryCustomerNumber) {
		this.benificiaryCustomerNumber = benificiaryCustomerNumber;
	}

	@Column(name = "beneficiary_customer_displayname")
	public String getBenificiaryCustomerName() {
		return benificiaryCustomerName;
	}

	public void setBenificiaryCustomerName(String benificiaryCustomerName) {
		this.benificiaryCustomerName = benificiaryCustomerName;
	}

	@Column(name = "beneficiary_customer_type")
	@Enumerated(EnumType.STRING)
	public CustomerType getBenificiaryCustomerType() {
		return benificiaryCustomerType;
	}

	public void setBenificiaryCustomerType(CustomerType benificiaryCustomerType) {
		this.benificiaryCustomerType = benificiaryCustomerType;
	}

	@Column(name = "beneficiary_industry")
	public String getBenificiaryIndustry() {
		return benificiaryIndustry;
	}

	public void setBenificiaryIndustry(String benificiaryIndustry) {
		this.benificiaryIndustry = benificiaryIndustry;
	}

	@Column(name = "beneficiary_corporate_structure")
	public String getBenificiaryCorporateStructure() {
		return benificiaryCorporateStructure;
	}

	public void setBenificiaryCorporateStructure(String benificiaryCorporateStructure) {
		this.benificiaryCorporateStructure = benificiaryCorporateStructure;
	}

	@Column(name = "beneficiary_estimated_amount")
	public String getBenificiaryEstimatedAmount() {
		return benificiaryEstimatedAmount;
	}

	public void setBenificiaryEstimatedAmount(String benificiaryEstimatedAmount) {
		this.benificiaryEstimatedAmount = benificiaryEstimatedAmount;
	}

	@Column(name = "beneficiary_business")
	public String getBenificiaryBusiness() {
		return benificiaryBusiness;
	}

	public void setBenificiaryBusiness(String benificiaryBusiness) {
		this.benificiaryBusiness = benificiaryBusiness;
	}

	@Column(name = "beneficiary_country")
	public String getBenificiaryCountry() {
		return benificiaryCountry;
	}

	public void setBenificiaryCountry(String benificiaryCountry) {
		this.benificiaryCountry = benificiaryCountry;
	}

	@Column(name = "beneficiary_bank_name")
	public String getBenificiaryBankName() {
		return benificiaryBankName;
	}

	public void setBenificiaryBankName(String benificiaryBankName) {
		this.benificiaryBankName = benificiaryBankName;
	}

	@Column(name = "customer_shareholder")
	public String getShareHolderName() {
		return shareHolderName;
	}

	public void setShareHolderName(String shareHolderName) {
		this.shareHolderName = shareHolderName;
	}

	@Column(name = "customer_share_value")
	public Long getNumberOfShares() {
		return numberOfShares;
	}

	public void setNumberOfShares(Long numberOfShares) {
		this.numberOfShares = numberOfShares;
	}

	@Column(name = "fatf_risk")
	public Float getFatfRisk() {
		return fatfRisk;
	}

	public void setFatfRisk(Float fatfRisk) {
		this.fatfRisk = fatfRisk;
	}

	@Column(name = "freedom_house_risk")
	public Float getFreedomHouserisk() {
		return freedomHouserisk;
	}

	public void setFreedomHouserisk(Float freedomHouserisk) {
		this.freedomHouserisk = freedomHouserisk;
	}

	@Column(name = "international_narcotics_risk")
	public Float getInternationalNarcoticsRisk() {
		return internationalNarcoticsRisk;
	}

	public void setInternationalNarcoticsRisk(Float internationalNarcoticsRisk) {
		this.internationalNarcoticsRisk = internationalNarcoticsRisk;
	}

	@Column(name = "political_risk")
	public Float getPoliticalRisk() {
		return politicalRisk;
	}

	public void setPoliticalRisk(Float politicalRisk) {
		this.politicalRisk = politicalRisk;
	}

	@Column(name = "task_justice_risk")
	public Float getTaskJusticeRisk() {
		return taskJusticeRisk;
	}

	public void setTaskJusticeRisk(Float taskJusticeRisk) {
		this.taskJusticeRisk = taskJusticeRisk;
	}

	@Column(name = "transparency_risk")
	public Float getTransparencyRisk() {
		return transparencyRisk;
	}

	public void setTransparencyRisk(Float transparencyRisk) {
		this.transparencyRisk = transparencyRisk;
	}

	@Column(name = "wef_risk")
	public Float getWefRisk() {
		return wefRisk;
	}

	public void setWefRisk(Float wefRisk) {
		this.wefRisk = wefRisk;
	}

	@Column(name = "world_bank_risk")
	public Float getWorldBankRisk() {
		return worldBankRisk;
	}

	public void setWorldBankRisk(Float worldBankRisk) {
		this.worldBankRisk = worldBankRisk;
	}

	@Column(name = "world_justice_risk")
	public Float getWorldJusticeRisk() {
		return worldJusticeRisk;
	}

	public void setWorldJusticeRisk(Float worldJusticeRisk) {
		this.worldJusticeRisk = worldJusticeRisk;
	}

	@Column(name = "beneficiary_latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "beneficiary_longitutde")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "alert_transaction_id")
	public Long getAlertTransactionId() {
		return alertTransactionId;
	}

	public void setAlertTransactionId(Long alertTransactionId) {
		this.alertTransactionId = alertTransactionId;
	}

	@Column(name = "alert_customer_displayname")
	public String getAlertCustomerDisplayName() {
		return alertCustomerDisplayName;
	}

	public void setAlertCustomerDisplayName(String alertCustomerDisplayName) {
		this.alertCustomerDisplayName = alertCustomerDisplayName;
	}

	@Column(name = "alert_created_date")
	public Date getAlertCreatedDate() {
		return alertCreatedDate;
	}

	public void setAlertCreatedDate(Date alertCreatedDate) {
		this.alertCreatedDate = alertCreatedDate;
	}

	@Column(name = "beneficiary_iso2_code")
	public String getBeneficiaryIso2Code() {
		return beneficiaryIso2Code;
	}

	public void setBeneficiaryIso2Code(String beneficiaryIso2Code) {
		this.beneficiaryIso2Code = beneficiaryIso2Code;
	}

	@Column(name = "originator_iso2_code")
	public String getOriginatorIso2Code() {
		return originatorIso2Code;
	}

	public void setOriginatorIso2Code(String originatorIso2Code) {
		this.originatorIso2Code = originatorIso2Code;
	}

	@Column(name = "originator_country_id")
	public Long getOriginatorCountryId() {
		return originatorCountryId;
	}

	public void setOriginatorCountryId(Long originatorCountryId) {
		this.originatorCountryId = originatorCountryId;
	}

	@Column(name = "beneficiary_country_id")
	public Long getBeneficiaryCountryId() {
		return beneficiaryCountryId;
	}

	public void setBeneficiaryCountryId(Long beneficiaryCountryId) {
		this.beneficiaryCountryId = beneficiaryCountryId;
	}

	@Column(name = "originator_latitude")
	public String getOriginatorCountryLatitude() {
		return originatorCountryLatitude;
	}

	public void setOriginatorCountryLatitude(String originatorCountryLatitude) {
		this.originatorCountryLatitude = originatorCountryLatitude;
	}

	@Column(name = "originator_longitutde")
	public String getOriginatorCountryLongitude() {
		return originatorCountryLongitude;
	}

	public void setOriginatorCountryLongitude(String originatorCountryLongitude) {
		this.originatorCountryLongitude = originatorCountryLongitude;
	}
	
	@Column(name="alert_risk_score")
	public Integer getAlertRiskScore() {
		return alertRiskScore;
	}

	public void setAlertRiskScore(Integer alertRiskScore) {
		this.alertRiskScore = alertRiskScore;
	}
	
	@Column(name="entity_type")
	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	
	

	
}
