package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupAlertDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupAlertSettingDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupAlertService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;

/**
 * @author Paul Jalagari
 *
 */
@Service("groupAlertService")
@Transactional("transactionManager")
public class GroupAlertServiceImpl extends GenericServiceImpl<GroupAlert, Long> implements GroupAlertService {

	@Autowired
	private GroupAlertDao groupAlertDao;

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private ListItemService listItemService;

	@Autowired
	public GroupAlertServiceImpl(GenericDao<GroupAlert, Long> genericDao) {

		super(genericDao);
	}

	@Override
	public List<GroupAlert> saveGroupAlertSetting(GroupAlertSettingDto groupAlertSettingDto, Long groupId,
			Long currentUserId) {
		List<GroupAlert> savedGroupAlertSettings = new ArrayList<GroupAlert>();
		if (groupAlertSettingDto != null) {
			removeExistingGroupAlertSettings(groupId,groupAlertSettingDto.getType());
			Groups group = null;
			String geoLocationSetting = null;
			String settingType = null;
			if (null != groupAlertSettingDto.getGroupId())
				group = groupsService.find(groupAlertSettingDto.getGroupId());
			if (null != groupAlertSettingDto.getGeoLocationSetting())
				geoLocationSetting = groupAlertSettingDto.getGeoLocationSetting();
			if (null != groupAlertSettingDto.getType()
					&& (groupAlertSettingDto.getType().equals(ElementConstants.FEED_CLASSIFICATION_TYPE)
							|| groupAlertSettingDto.getType().equals(ElementConstants.JURISDICTION_TYPE_STRING)))
				settingType = groupAlertSettingDto.getType().toUpperCase();
			if (null != groupAlertSettingDto.getListItemId() && groupAlertSettingDto.getListItemId().size() > 0) {

				for (Long eachListItemId : groupAlertSettingDto.getListItemId()) {
					GroupAlert newSetting = new GroupAlert();
					if (geoLocationSetting != null)
						newSetting.setGeoLocationSetting(groupAlertSettingDto.getGeoLocationSetting());
					if (group != null)
						newSetting.setGroupId(group);
					if (null != eachListItemId) {
						ListItem item = listItemService.find(eachListItemId);
						if (item != null)
							newSetting.setListTypeId(item);
					}
					if (settingType != null)
						newSetting.setType(settingType);
					savedGroupAlertSettings.add(save(newSetting));

				}
			} else {
				GroupAlert setting = new GroupAlert();
				if (geoLocationSetting != null)
					setting.setGeoLocationSetting(geoLocationSetting);
				if (group != null)
					setting.setGroupId(group);
				if (settingType != null)
					setting.setType(settingType);
				savedGroupAlertSettings.add(save(setting));
			}
		}
		return savedGroupAlertSettings;}

	@Override
	public void removeExistingGroupAlertSettings(Long groupId, String type) {
		groupAlertDao.removeExistingGroupAlertSettings(groupId,type);
	}

	@Override
	public List<GroupAlert> findGroupAlertsByGroup(Long groupId) {
		return groupAlertDao.findGroupAlertsByGroup(groupId);
	}

	@Override
	public List<GroupAlert> getGroupAlertsByGroupIdList(List<Long> groupIds) {
		
		return groupAlertDao.getGroupAlertsByGroupIdList(groupIds);
	}
	@Override
	public void loadZeroGroupAlertSettings(Long groupId) {
		if (groupId != null) {
			GroupAlertSettingDto groupAlertSettingDtoJurisdiction = new GroupAlertSettingDto();
			groupAlertSettingDtoJurisdiction.setGeoLocationSetting("Off");
			groupAlertSettingDtoJurisdiction.setGroupId(groupId);
			groupAlertSettingDtoJurisdiction.setType("JURISDICTION_TYPE");
			saveGroupAlertSetting(groupAlertSettingDtoJurisdiction, groupId, null);
			GroupAlertSettingDto groupAlertSettingDtoFeed = new GroupAlertSettingDto();
			groupAlertSettingDtoFeed.setGeoLocationSetting("Off");
			groupAlertSettingDtoFeed.setGroupId(groupId);
			groupAlertSettingDtoFeed.setType("FEED_CLASSIFICATION_TYPE");
			saveGroupAlertSetting(groupAlertSettingDtoFeed, groupId, null);
		}
	}
}
