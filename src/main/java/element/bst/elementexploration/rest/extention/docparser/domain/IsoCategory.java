package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author rambabu
 *
 */
@Entity
@Table(name = "iso_category")
public class IsoCategory implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private long id;
	private String level1Code;
	private String level1Title;
	private String evaluation;	
	private Integer rank;
	private long docId;
	private boolean OSINT;
	private Date evaluationUpdatedTime;
	private Long evaluationUpdatedBy;
	
	
	public IsoCategory() {
		super();		
	}	
	
	public IsoCategory(long id, String level1Code, String level1Title, String evaluation, Integer rank, long docId,
			boolean oSINT, Date evaluationUpdatedTime, Long evaluationUpdatedBy) {
		super();
		this.id = id;
		this.level1Code = level1Code;
		this.level1Title = level1Title;
		this.evaluation = evaluation;
		this.rank = rank;
		this.docId = docId;
		OSINT = oSINT;
		this.evaluationUpdatedTime = evaluationUpdatedTime;
		this.evaluationUpdatedBy = evaluationUpdatedBy;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "code")
	public String getLevel1Code() {
		return level1Code;
	}

	public void setLevel1Code(String level1Code) {
		this.level1Code = level1Code;
	}
	@Column(name = "title",columnDefinition = "LONGTEXT")
	public String getLevel1Title() {
		return level1Title;
	}

	public void setLevel1Title(String level1Title) {
		this.level1Title = level1Title;
	}
	@Column(name = "evaluation")
	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	@Column(name = "rank")
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}
	
	@Column(name = "document_id")
	public long getDocId() {
		return docId;
	}

	public void setDocId(long docId) {
		this.docId = docId;
	}
	@Column(name = "is_osint")
	public boolean isOSINT() {
		return OSINT;
	}
	public void setOSINT(boolean oSINT) {
		OSINT = oSINT;
	}
	@Column(name = "evaluation_updated_time")
	public Date getEvaluationUpdatedTime() {
		return evaluationUpdatedTime;
	}

	public void setEvaluationUpdatedTime(Date evaluationUpdatedTime) {
		this.evaluationUpdatedTime = evaluationUpdatedTime;
	}
	@Column(name = "evaluation_updated_by")
	public Long getEvaluationUpdatedBy() {
		return evaluationUpdatedBy;
	}

	public void setEvaluationUpdatedBy(Long evaluationUpdatedBy) {
		this.evaluationUpdatedBy = evaluationUpdatedBy;
	}
	
	

	

}
