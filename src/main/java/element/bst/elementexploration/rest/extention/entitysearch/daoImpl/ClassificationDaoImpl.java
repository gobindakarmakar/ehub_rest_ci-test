package element.bst.elementexploration.rest.extention.entitysearch.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.entitysearch.dao.ClassificationDao;
import element.bst.elementexploration.rest.extention.entitysearch.domain.Classification;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("classificationDao")
public class ClassificationDaoImpl extends GenericDaoImpl<Classification, Long> implements ClassificationDao {

	public ClassificationDaoImpl() {
		super(Classification.class);
	}

}
