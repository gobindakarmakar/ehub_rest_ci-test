package element.bst.elementexploration.rest.usermanagement.user.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;

/**
 * 
 * @author Amit Patel
 *
 */
public interface UserDao extends GenericDao<User, Long>{

	List<UserDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	List<UserDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countUserList(Long groupId, String status);

	Long countUserListFullTextSearch(String keyword, String status, Long groupId);
	
	User getUserByEmailOrUsername(String email);

	List<UserDto> listUsersByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countUserByGroup(Long groupId, Integer userStatus);
	
	User checkUserNameOrEmailExists(String email, Long id);

	List<User> getUsersInList(List<Long> participants);
	
	public User getAnalystByUserId(long analustId);

	public List<User> getAllAnalystWhoDidNotRejectedCaseBefore(Long caseId);
} 
