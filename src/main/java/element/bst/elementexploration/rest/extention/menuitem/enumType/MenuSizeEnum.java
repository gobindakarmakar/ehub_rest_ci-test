package element.bst.elementexploration.rest.extention.menuitem.enumType;

/**
 * @author Jalagari Paul
 *
 */
public enum MenuSizeEnum {

	SMALL, MEDIUM, LARGE;
}
