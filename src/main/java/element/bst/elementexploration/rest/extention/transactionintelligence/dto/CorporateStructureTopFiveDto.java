package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@ApiModel("Top Corporate Structures")
public class CorporateStructureTopFiveDto {

	@ApiModelProperty(value="List of top corporate structures")
	private List<CorporateStructureDto> topCorporateStructures;

	@ApiModelProperty(value="List of top share holders")
	private List<CorporateStructureDto> topShareholders;

	@ApiModelProperty(value="List of top alerted geographies")
	private List<CorporateStructureDto> topgeography;

	public List<CorporateStructureDto> getTopCorporateStructures() {
		return topCorporateStructures;
	}

	public void setTopCorporateStructures(List<CorporateStructureDto> topCorporateStructures) {
		this.topCorporateStructures = topCorporateStructures;
	}

	public List<CorporateStructureDto> getTopShareholders() {
		return topShareholders;
	}

	public void setTopShareholders(List<CorporateStructureDto> topShareholders) {
		this.topShareholders = topShareholders;
	}

	public List<CorporateStructureDto> getTopgeography() {
		return topgeography;
	}

	public void setTopgeography(List<CorporateStructureDto> topgeography) {
		this.topgeography = topgeography;
	}

}
