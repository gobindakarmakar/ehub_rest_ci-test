package element.bst.elementexploration.rest.extention.textsentiment.service;

import element.bst.elementexploration.rest.extention.textsentiment.domain.TextSentiment;

public interface TextSentimentService {

	String getTextSentiments(TextSentiment textSentiment) throws Exception;

}
