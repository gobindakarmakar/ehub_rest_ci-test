package element.bst.elementexploration.rest.base.controller;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

import org.apache.http.impl.entity.StrictContentLengthStrategy;

public class Test {

    // Complete the alternatingCharacters function below.
    static int alternatingCharacters(String s) {
      char[] ch = s.toCharArray();
      int count = 0;
	  for(int i=0 ; i<ch.length-1 ; i++)
	  {
		  if(ch[i]==ch[i+1])
		  {
			  count= count+1;
		  }
	  }
      
      return count; 
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String s = scanner.nextLine();

            int result = alternatingCharacters(s);

            System.out.println("Result : "+result);
        }

        scanner.close();
    }
}
