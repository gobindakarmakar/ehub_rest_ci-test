package element.bst.elementexploration.rest.casemanagement.service;

import java.text.ParseException;
import java.util.List;

import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface CaseAnalystMappingService extends GenericService<CaseAnalystMapping, Long> {
	
	void updateCaseStatus(Long caseId, Long userId, String statusComment, String notificationDate, int statusFrom, int statusTo) throws ParseException;

	CaseAnalystMapping caseReassignment(Long caseId, Long currentUserId, Long NewUserId, String statusComment);

	CaseAnalystMapping caseForwarding(Long caseId, Long currentUserId, Long NewUserId, String statusComment);

	boolean addStatusFocus(Long seedId, Long userId);

	boolean deleteCaseFromFocusById(Long seedId, Long userId);

	List<CaseDto> getAllCasesInFocusById(Long userId, String orderBy, String orderIn, Integer pageNumber,
			Integer recordsPerPage);

	long countAllCasesInFocusById(Long userId);

	List<CaseDto> fullTextSearchForCasesInFocus(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, String creationDate, String orderBy, String orderIn) throws ParseException;

	long countfullTextSearchForCaseInFocus(Long userId, String caseSearchKeyword, String creationDate)
			throws ParseException;
	
	CaseCommentNotificationDto getCaseCommentNotificationById(Long userId,Long caseId);
	
	CaseAnalystMapping findByCaseId(Long caseId);

	CaseAnalystMapping updateCaseStatusBpm(Long caseId, Long userId, String statusComment, String notificationDate, int statusFrom, int statusTo) throws ParseException;

	void createMapping(CaseAnalystMapping analystMapping);

	CaseAnalystMapping getAnalystMappingBasedOnUserId(Long seedId, Long userId, int index);

	CaseAnalystMapping caseForwardingBpm(Long caseId, Long currentUserId, Long NewUserId, String statusComment)
			throws InsufficientDataException;

	CaseAnalystMapping caseReassignmentBpm(Long caseId, Long currentUserId, Long NewUserId, String statusComment)
			throws InsufficientDataException;

	boolean addStatusFocusBpm(Long seedId, Long userId);

	void startTask(Long caseId, Long userIdTemp, int statusFrom, int statusTo, String statusComment,
			String notificationDate, Long newUserId, String reassign, String forward);
}
