package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountOwnershipType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountType;

public class AccountDto  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotNull
	private Long id;
	private String accountNumber;
	private AccountOwnershipType accountOwnershipType;
	private String primaryCustomerIdentifier;
	private Date accountOpenDate;
	private String jurisdiction;
	private String sourceSystem;
	private AccountType accountType;
	private String accountName;
	private String segmentId;
	private String accountCurrency;
	private Double maximumAmount;
	private long customerId;
	private String bankName;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public AccountOwnershipType getAccountOwnershipType() {
		return accountOwnershipType;
	}
	public void setAccountOwnershipType(AccountOwnershipType accountOwnershipType) {
		this.accountOwnershipType = accountOwnershipType;
	}
	public String getPrimaryCustomerIdentifier() {
		return primaryCustomerIdentifier;
	}
	public void setPrimaryCustomerIdentifier(String primaryCustomerIdentifier) {
		this.primaryCustomerIdentifier = primaryCustomerIdentifier;
	}
	public Date getAccountOpenDate() {
		return accountOpenDate;
	}
	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}
	public String getJurisdiction() {	
		return jurisdiction;
	}
   public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
   public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public AccountType getAccountType() {
		return accountType;
	}
	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getSegmentId() {
		return segmentId;
	}
	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	public Double getMaximumAmount() {
		return maximumAmount;
	}
	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	
}