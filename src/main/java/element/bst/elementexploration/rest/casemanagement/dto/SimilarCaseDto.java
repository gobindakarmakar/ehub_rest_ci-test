package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Similar case")
public class SimilarCaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="Proximity of the case")
	private String proximity;
	
	@ApiModelProperty(value="ID of the case")
	private String id;

	public SimilarCaseDto() {
		super();
	}

	public SimilarCaseDto(String proximity, String id) {
		super();
		this.proximity = proximity;
		this.id = id;
	}

	public String getProximity() {
		return proximity;
	}

	public void setProximity(String proximity) {
		this.proximity = proximity;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "SimilarCase [proximity=" + proximity + ", id=" + id + "]";
	}
	
	

}
