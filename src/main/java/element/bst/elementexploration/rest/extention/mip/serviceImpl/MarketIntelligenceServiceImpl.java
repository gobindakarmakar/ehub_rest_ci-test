package element.bst.elementexploration.rest.extention.mip.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.mip.domain.OrgWithFacts;
import element.bst.elementexploration.rest.extention.mip.service.MarketIntelligenceService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("marketIntelligenceService")
public class MarketIntelligenceServiceImpl implements MarketIntelligenceService {

	@Value("${bigdata_org_url}")
	private String BIG_DATA__ORG_URL;

	/*@Value("${bigdata_entity_search_url}")
	private String BIG_DATA__ENTITY_SEARCH_URL;*/

	@Override
	public String getOrgWithFacts(OrgWithFacts orgWithFacts) throws Exception {
		String url = BIG_DATA__ORG_URL + "search/org-with-facets";
		ObjectMapper mapper = new ObjectMapper();
		String data = mapper.writeValueAsString(orgWithFacts);
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, data);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	/*@Override
	public String searchEntity(String entity) throws Exception {
		String entityEncode = java.net.URLEncoder.encode(entity, "UTF-8").replaceAll("\\+", "%20");
		String url = BIG_DATA__ENTITY_SEARCH_URL + "search?entity=" + entityEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}*/
}
