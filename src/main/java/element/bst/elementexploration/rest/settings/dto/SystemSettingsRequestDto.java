package element.bst.elementexploration.rest.settings.dto;

import java.io.Serializable;

/**
 * @author ahextech
 *
 */
public class SystemSettingsRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long settingId;
	private String selectedValue;
	private Long attributeId;
	private boolean selected;
	private String systemSettingsType;
	private String customerLogoImage;
	private Integer selectedValueMax;
	private Integer sliderFrom;
	private Integer sliderTo;
	
	public Long getSettingId() {
		return settingId;
	}

	public void setSettingId(Long settingId) {
		this.settingId = settingId;
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getSystemSettingsType() {
		return systemSettingsType;
	}

	public void setSystemSettingsType(String systemSettingsType) {
		this.systemSettingsType = systemSettingsType;
	}

	public String getCustomerLogoImage() {
		return customerLogoImage;
	}

	public void setCustomerLogoImage(String customerLogoImage) {
		this.customerLogoImage = customerLogoImage;
	}

	public Integer getSelectedValueMax() {
		return selectedValueMax;
	}

	public void setSelectedValueMax(Integer selectedValueMax) {
		this.selectedValueMax = selectedValueMax;
	}

	public Integer getSliderFrom() {
		return sliderFrom;
	}

	public void setSliderFrom(Integer sliderFrom) {
		this.sliderFrom = sliderFrom;
	}

	public Integer getSliderTo() {
		return sliderTo;
	}

	public void setSliderTo(Integer sliderTo) {
		this.sliderTo = sliderTo;
	}

}
