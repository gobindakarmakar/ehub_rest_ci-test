'use strict';
angular.module('ehubApp')
.controller('AddDataCurationController', addStagingModalController);

addStagingModalController.$inject = [
	'$scope',
	'$rootScope',
	'$timeout',
	'DataCurationApiService',
	'$filter',
	'$window',
	'$cookies',
	'$stateParams',
	'EHUB_API',
	'$uibModal',
	'$state',
	'HostPathService',
	'DTOptionsBuilder',
	'DTColumnDefBuilder'
];

function addStagingModalController(
		$scope,
		$rootScope,
		$timeout,
		DataCurationApiService,
		$filter,
		$window,
		$cookies,
		$stateParams,
		EHUB_API,
		$uibModal,
		$state,
		HostPathService,
		DTOptionsBuilder,
		DTColumnDefBuilder
		){

	// Initalizing local variables
	var allRelationsData = {
			 directorRoles : [],
			 officerRoles : []
	};
	$scope.editTab = true;
	$scope.isOrganisationPerson = false;   // to determine the entered personal
											// information is individual or for
											// current organization.
	// Initializing scope variables
	$scope.dataCuration = {
		tabActive : 0,
		locationsModel: [],
		countriesData : [],
		nationalityData: [],
		popupfoundedDate: {
			opened: false
		},
		popupbirthDate: {
			opened: false
		},
		popupdeathDate: {
			opened: false
		},
		dateOptionsto: {
			formatYear: 'yyyy',
		    maxDate: new Date(2020, 5, 22),
		    minDate: new Date(),
		    startingDay: 1
		},
	    openfoundedDate: openfoundedDate,
	    popupInactiveDate: {
			opened: false
		},
		inactiveDateOptions: {
			formatYear: 'yyyy',
		    maxDate: new Date(2020, 5, 22),
		    minDate: new Date(),
		    startingDay: 1
		},
		openInactiveDate: openInactiveDate,
		popupIPODate: {
			opened: false
		},
		dateOptionsfounded :{
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1850, 1, 1),
		    startingDay: 1
		},
		IPODateOptions: {
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1950, 1, 1),
		    startingDay: 1
		},
		dateOptionsbirth: {
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1935, 1, 1),
		    startingDay: 1
		},
		dateOptionsdeath: {
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1850, 1, 1),
		    startingDay: 1
		},
		openIPODate: openIPODate,
	    saveCurationDetails: saveCurationDetails,
	    birthDateModal: '',
		popupfromDate: {
			opened: false
		},
		deathDateModal: '',
		 openbirthDate: openbirthDate,
		 opendeathDate: opendeathDate,
		 CurrentDay : "",
		 genderval : "male",
		 toggleSwitch: toggleSwitch,
		 changeGender: changeGender,
		 changePrefix: changePrefix,
		 resetFields :resetFields,
		 getCompanySuggestions: getCompanySuggestions,
		 addNewPerson:addNewPerson,
		 getPersonSuggestions: getPersonSuggestions,
		 directorLength : 0,
  	 	 officerLength : 0,
	   	 directorsRetireve : false,
	  	 officersRetrieve : false,
	  	 getPersonSearchResult : getPersonSearchResult
	};

	$scope.allFormsModal = {
			organizationModal: {
//				"description": "",
	  			"bst:facebook": null,
	  			"mdaas:HeadquartersAddress": null,
	  			"isDomiciledIn": null,
	  			"bst:youtube": null,
	  			"hasLatestOrganizationFoundedDate": null,
	  			"tr-org:hasRegisteredPhoneNumber": null,
	  			"@is-manual": "true",
	  			"bst:linkedin": null,
	  			"mdaas:RegisteredAddress": null,
	  			"hasPrimaryEconomicSector": null,
//	  			"disambiguatingDescription": "",
				"hasHoldingClassification": null,
	  			"vcard:organization-name":null,
	  			"bst:email": null,
	  			"hasPrimaryBusinessSector": null,
	  			"hasRegisteredFaxNumber":null,
	  			"hasURL": null,
//	  			"hasIPODate": "",
	  			"@source-id": "data-curation-ui",
	  			"isIncorporatedIn": null,
//	  			"tr-org:hasInactiveDate": "",
//	  			"hasPrimaryInstrument": "",
	  			"tr-org:hasHeadquartersFaxNumber":null,
				"hasPrimaryIndustryGroup": null,
//	  			"hasActivityStatus": "false",
	  			"bst:VAT": null,
//	  			"tr-org:hasLEI": "",
	  			"bst:instagram": null,
	  			"bst:gplus": null,
	  			"tr-org:hasHeadquartersPhoneNumber": null,
	  			"bst:twitter": null,
//				"hasOrganizationPrimaryQuote": "",
				"bst:registrationId":null,
				"personalDetails" : [
			        {
			        	'type' : 'Registered address',
			            'streetAddress':'',
			            'city':'',
			            'zip':'',
			            'country':'',
			            'selected' : true
			        },
			        {
			        	'type' : 'HQ address',
			        	'streetAddress':'',
			            'city':'',
			            'zip':'',
			            'country':'',
			            'selected' : true

			        },
			        {
			        	'type' : 'Registered address',
			        	'streetAddress':'',
			            'city':'',
			            'zip':'',
			            'country':'',
			            'selected' : true

			        }]
			},
			personModal: {
				  "bst:facebook": null,
				  "vcard:hasAddress": null,
				  "vcard:hasGender": "male",
				  "vcard:hasName": null,
				  "bst:deathDate": null,
				  "bst:youtube": null,
				  "vcard:honorific-prefix": "Mr.",
				  "@is-manual": "true",
				  "bst:linkedin": null,
				  "vcard:hasPhoto": null,
				  "vcard:bday": null,
				  "vcard:hasTelephone": null,
				  "vcard:family-name": null,
				  "vcard:additional-name": null,
				  "bst:email": null,
				  "@source-id": "data-curation-ui",
				  "vcard:hasURL": null,
				  "vcard:honorific-suffix": null,
				  "tr-vcard:preferred-name": null,
				  "bst:aka": null,
				  "bst:description": null,
				  "vcard:given-name": null,
				  "bst:nationality": null,
				  "bst:instagram": null,
				  "bst:gplus": null,
				  "bst:twitter": null
				},
				relatedEntities:{

				}
	}
	$scope.ShowClicked = false;
	$scope.clickedPerson = {
			"personName" : "",
			"relation":"",
			"title": "Director",
			"from":$filter('date')(new Date(), 'yyyy-MM-dd'),
			"to":$filter('date')(new Date(), 'yyyy-MM-dd')
			}
//	var count = 0;
	$scope.persons = {
        	list: [],
        	list1 : [],
        	retrieve : true,
        	dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10)
    		        .withOption('bFilter', false)
    		        .withOption('lengthChange', false)
    		        .withDOM('lrtip'),
            dtColumnDefs: [
    		    DTColumnDefBuilder.newColumnDef(5).notSortable()
    		],
        	onClickEditStaging: onClickEditStaging
        };

	
	/*For display list of contents in Address table*/
    
    $scope.ContentAdd = function(personalDetail){
        $scope.allFormsModal.organizationModal.personalDetails.push({ 
        	'type' : 'HQ address',
            'street': "", 
            'building': "",
            'city': "",
            'zip':'',
            'country':'',
            'selected' : true
        });
    };

    $scope.ContentRemove = function(){
        var newDataList=[];
        $scope.selectedAll = false;
        angular.forEach($scope.allFormsModal.organizationModal.personalDetails, function(selected){
            if(!selected.selected){
                newDataList.push(selected);
            }
        }); 
        $scope.allFormsModal.organizationModal.personalDetails = newDataList;
    };

$scope.checkAll = function () {
    if (!$scope.selectedAll) {
        $scope.selectedAll = true;
    } else {
        $scope.selectedAll = false;
    }
    console.log($scope.allFormsModal.organizationModal.personalDetails)
    angular.forEach($scope.allFormsModal.organizationModal.personalDetails, function(personalDetail) {
        personalDetail.selected = $scope.selectedAll;
    });
};    

$scope.checkUncheckAll = function(){
	$scope.selectedAll = true;
	console.log($scope.allFormsModal.organizationModal.personalDetails.length,"vdoisjv",$scope.allFormsModal.organizationModal.personalDetails)
	$("#CheckAllId").prop("checked",true);
    for (var i = 0; i < $scope.allFormsModal.organizationModal.personalDetails.length; i++) {
        if (!$scope.allFormsModal.organizationModal.personalDetails[i].selected) {
            $scope.selectedAll = false;
            $("#CheckAllId").prop("checked",false);
            break;
        }
    };
//    console.log($scope.selectedAll)
}

$scope.checkUncheckAll();
	
	
	
	/*
	 * @purpose: toggle switch @created: 8 feb 2018 @params: null @return: no
	 * @author: Ankit
	 */
	function toggleSwitch(hasStatus){
		$scope.allFormsModal.organizationModal['hasActivityStatus'] = !hasStatus;
	}

	if($stateParams.identifier !== 'new'){
		$scope.editTab = false;
		$scope.isLoading = true;
		var type =  $stateParams.identifier.split("#")[1];
		var id = $stateParams.identifier.split("#")[0];
		if(type == "02"){
			getdirectorOfficerData();
			$scope.dataCuration.tabActive = 0;
			var url = EHUB_API + "entity/org/"+ id;
			getData(url,type);
		}else if(type == "01"){
			var url = EHUB_API + "entity/person/"+ id;
			getData(url,type);
			$scope.dataCuration.tabActive = 1;

		}
	}
	/*
	 * @purpose: change gender @created: 8 feb 2018 @params: null @return: no
	 * @author: Ankit
	 */
	 function changeGender(prefix){
		if(prefix == "Mrs." ||  prefix == "Miss"){
			$scope.allFormsModal.personModal['vcard:hasGender'] = "female";
		}else{
			$scope.allFormsModal.personModal['vcard:hasGender'] = "male";
		}

	 }
	 /*
		 * @purpose: get company suggestions @created: 8 feb 2018 @params: null
		 * @return: no @author: Ankit
		 */
	 function getCompanySuggestions(val){
		 return DataCurationApiService.getOrganationNamesSuggestions(val,10).then(function(orgResponse){
			 console.log(orgResponse,'orgResponseorgResponse');
			 return orgResponse.data.hits;
		 },function(orgError){

		 });
	 }

	 /*
	     * @purpose: get person suggestions
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
	*/
	 function getPersonSuggestions(val){
		 return DataCurationApiService.getPersonNamesSuggestions(val).then(function(personResponse){
			 console.log(personResponse,'personResponsepersonResponse');
			 return personResponse.data.hits;
		 },function(personError){

		 });
	 }
	 
	 
	 /*
	     * @purpose: get person suggestions
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
	*/
	 function getPersonSearchResult(val){
		 console.log("whats up???");
		 return DataCurationApiService.getPersonNamesSuggestions(val).then(function(personResponse){
			 console.log(personResponse,'personResponsepersonResponse');
			 return personResponse.data.hits;
		 },function(personError){

		 });
	 }

	 /*
	     * @purpose: get company details
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
		*/

	 $scope.$watch("allFormsModal.organizationModal['vcard:organization-name']", function(newVal){
		 if(typeof newVal === "object" && newVal){
			 console.log(newVal)
			 $state.go("addDataCuration",{'identifier': newVal['@identifier']+"#02"}, {notify:true});
			 $stateParams.identifier = newVal['@identifier'];
			 getdirectorOfficerData();
			 getData(EHUB_API + "entity/org/"+ newVal['@identifier'],"02");
		 }

	 });

	 /*
	     * @purpose: get person details
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
		*/

	 $scope.$watch("allFormsModal['relatedEntities'].entitySearch", function(newVal){
		 if(typeof newVal === "object" && newVal){
			 console.log(newVal);
			 $scope.clickedPerson.identifier = newVal["@identifier"];
			 allRelationsData.personIdentifier = newVal["@identifier"];
			 $scope.clickedPerson.personName = newVal["vcard:hasName"]
//			 $scope.isOrganisationPerson = true;
//				openRelationModal();
			 
			 getlistforRelationship();
			 $scope.ShowClicked = true;
		 }

	 });
	 
	 
	 /*
	     * @purpose: get person details
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Virendra
		*/

	 $scope.$watch("allFormsModal.personModal['vcard:hasName']", function(newVal){
		 console.log(newVal,"selct");
		 if(typeof newVal === "object" && newVal){
			 console.log("inisde");
			 $scope.isLoading = true;
			 var url = EHUB_API + "entity/person/"+ newVal["@identifier"];
				getData(url,"01");
			 }

	 });
	 

	 
	 $scope.$watch("allFormsModal.personModal['vcard:hasName']", function(newVal){
		 console.log(newVal,"selct");
		 if(typeof newVal === "object" && newVal){
			 console.log("inisde");
			 $scope.isLoading = true;
			 var url = EHUB_API + "entity/person/"+ newVal["@identifier"];
				getData(url,"01");
		 }

	 });

	 
	 function getlistforRelationship(){
		 $scope.allrelationsList = [];
		 angular.extend($scope.allrelationsList, allRelationsData.officerRoles);
		 angular.extend($scope.allrelationsList, allRelationsData.directorRoles);
		 $scope.allrelationsList = _.uniq($scope.allrelationsList)
	 }
	 
	 
	 /*
	     * @purpose: update chosen details
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
		*/
	 function update_all_chosen(){
		 setTimeout(function(){
			 $("#domiciledData").trigger("chosen:updated");

			 $('#countriesData').trigger("chosen:updated")

			 $('#industriesData').trigger("chosen:updated")

			 $('#EconomicSectorData').trigger("chosen:updated")

			 $('#businessSectorDatas').trigger("chosen:updated")

			 $("#nationalityDatas").trigger("chosen:updated");
			 
//			 $("#")
		 }, 100);
	 }

	 
	 $scope.functionnn = function(){
		 if(allRelationsData.officerRoles.indexOf($scope.clickedPerson.relation) > -1){
			 $scope.clickedPerson.title = "officer";
		 }else{
			 $scope.clickedPerson.title = "director";
		 }
	 }
	 
	 $scope.addNewRelation = function(){
		 console.log($scope.clickedPerson);
		 var role;
		 if($stateParams.identifier != "new"){
			 if($scope.clickedPerson.title == 'director')
					role = 'directorship';
				else if($scope.clickedPerson.title == 'officer')
					role = 'officership';
				var rolesData = {};
				rolesData['@source-id'] = 'data-curation-ui';
				rolesData['to'] = $scope.clickedPerson['to'] || null;
				rolesData['from'] = $scope.clickedPerson['from'];
//				rolesData['hasReportedTitle'] = relationVal['hasReportedTitle'];
				rolesData['hasPositionType'] = $scope.clickedPerson['relation'];
				rolesData['hasHolder'] = $scope.clickedPerson.identifier;
				rolesData['isPositionIn'] = $stateParams.identifier.split("#")[0];;
				rolesData['@is-manual'] = "true";

				DataCurationApiService.saveRelationdata(role, rolesData).then(function(relationResponse){
					console.log(relationResponse);
					var response = angular.extend(relationResponse,{"name":$scope.clickedPerson.personName});
					$rootScope.$broadcast('eventFired', {
		                data: response
		            });
					HostPathService.FlashSuccessMessage('Added Role Successfully','');
					$scope.ShowClicked = false;
				}, function(relationError){
					console.log(relationError);
					HostPathService.FlashErrorMessage('Sorry, Failed To Add SourceId '+rolesData['@source-id'],'');
					console.log(rolesData['@source-id']);
				}); 
		 }else{
			 HostPathService.FlashErrorMessage('Sorry, Organisation Id is not available','');
		 }
	 }
	 
	 function changePrefix(gender){
		 if(gender == "female")
			 $scope.allFormsModal.personModal['vcard:honorific-prefix'] = "Miss";
		 else
			 $scope.allFormsModal.personModal['vcard:honorific-prefix'] = "Mr.";
	 }

	function openfoundedDate(){
		$scope.dataCuration.popupfoundedDate.opened = true;
	}

	function openbirthDate(){
		$scope.dataCuration.dateOptionsbirth.maxDate = $scope.allFormsModal.personModal['bst:deathDate'];
		$scope.dataCuration.popupbirthDate.opened = true;
	}

	function opendeathDate(){
		$scope.dataCuration.dateOptionsdeath.minDate = $scope.allFormsModal.personModal['vcard:bday'];
		$scope.dataCuration.popupdeathDate.opened = true;
	}

	function openInactiveDate(){
		$scope.dataCuration.popupInactiveDate.opened = true;
	}

	function openIPODate(){
		$scope.dataCuration.popupIPODate.opened = true;
	}

	/*
	 * @purpose: get countries/locations names @created: 2 feb 2018 @params:
	 * null @return: no @author: Ankit
	 */
	DataCurationApiService.getAllCountriesNames().then(function(countriesResponse){
		$scope.dataCuration.countriesData = countriesResponse.data;
	},function(countriesError){
		console.log(countriesError)
	});
	/*
	 * @purpose: get Industries names @created: 3 feb 2018 @params: null
	 * @return: no @author: Virendra
	 */
	DataCurationApiService.getAllIndustriesNames().then(function(IndustriesResponse){
		$scope.dataCuration.industriesData = IndustriesResponse.data;
	},function(industriesError){
		console.log(industriesError)
	});

	/*
	 * @purpose: get business Sector Data @created: 3 feb 2018 @params: null
	 * @return: no @author: Virendra
	 */

	DataCurationApiService.getbusinessSectorsData().then(function(IndustriesResponse){
		$scope.dataCuration.businessSectorData = IndustriesResponse.data;
	},function(industriesError){
		console.log(industriesError)
	});

	/*
	 * @purpose: get Economic Sector names @created: 3 feb 2018 @params: null
	 * @return: no @author: Virendra
	 */
	DataCurationApiService.getAllEconomicSectorsData().then(function(IndustriesResponse){
		$scope.dataCuration.economicSectorData = IndustriesResponse.data;
	},function(industriesError){
		console.log(industriesError)
	});

	/*
	 * @purpose: get nationality names @created: 2 feb 2018 @params: null
	 * @return: no @author: Ankit
	 */
	DataCurationApiService.getAllNationalityData().then(function(nationalityResponse){
		$scope.dataCuration.nationalityData = nationalityResponse.data;
	},function(nationalityError){
		console.log(nationalityError)
	});

	/*
	 * @purpose: get director roles @created: 8 feb 2018 @params: null @return:
	 * no @author: Ankit
	 */
	DataCurationApiService.getAllDirectorRoles().then(function(directorResponse){
		allRelationsData.directorRoles = directorResponse.data;
	},function(directorError){
		console.log(directorError)
	});

	/*
	 * @purpose: get officer roles @created: 8 feb 2018 @params: null @return:
	 * no @author: Ankit
	 */
	DataCurationApiService.getAllOfficerRoles().then(function(officerResponse){
		allRelationsData.officerRoles = officerResponse.data;
	},function(officerError){
		console.log(officerError)
	});

	/*
	 * @purpose: get nationality names @created: 2 feb 2018 @params: null
	 * @return: no @author: Ankit
	 */
	function getData(data,type){
		DataCurationApiService.getIdentifierInfo(data).then(function(resp){
			if(type == "02"){
				debugger;
				var addressData = []; 
				angular.extend($scope.allFormsModal.organizationModal, resp.data);
				$scope.allFormsModal.organizationModal.hasLatestOrganizationFoundedDate = new Date(resp.data.hasLatestOrganizationFoundedDate);
				$scope.allFormsModal.organizationModal['hasIPODate'] = new Date(resp.data.hasIPODate);
				$scope.allFormsModal.organizationModal['tr-org:hasInactiveDate'] = new Date(resp.data["tr-org:hasInactiveDate"]);
				$scope.allFormsModal.organizationModal['hasActivityStatus'] = resp.data.hasActivityStatus ==='Inactive' ? false :true;
				if(resp.data["mdaas:HeadquartersAddress"].length > 0){
					angular.forEach(resp.data["mdaas:HeadquartersAddress"],function(d,i){
						var updateType = $.extend({},d,{"type":"HQ address",'selected' : true});
						addressData.push(updateType);
					});
				}
				if(resp.data["mdaas:RegisteredAddress"].length > 0){
					angular.forEach(resp.data["mdaas:RegisteredAddress"],function(d,i){
						var updateType = $.extend({},d,{"type":"Registered address",'selected' : true});
						addressData.push(updateType);	
					});
				}
				$scope.allFormsModal.organizationModal.personalDetails= addressData;
				console.log($scope.allFormsModal.organizationModal.personalDetails,"here")
				update_all_chosen();
			}else if(type == "01"){
				console.log(resp.data,'resp.data')
				angular.extend($scope.allFormsModal.personModal, resp.data);
				$scope.allFormsModal.personModal['bst:deathDate'] = resp.data["bst:deathDate"] ? new Date(resp.data["bst:deathDate"]) : "";
				$scope.allFormsModal.personModal['vcard:bday'] = resp.data["vcard:bday"] ? new Date(resp.data["vcard:bday"]) : "";
				update_all_chosen();
			}
			$scope.isLoading = false;
		},function(nationalityError){
			$scope.isLoading = false;
			console.log(nationalityError)
		});
	}

	/*
	 * @purpose: save DataCuration Details @created: 3 feb 2018 @params: null
	 * @return: no @author: Ankit
	 */
	function saveCurationDetails(curationData,createNew){
		console.log(curationData,createNew,"wowow")
		if($scope.dataCuration.tabActive == 1){
				if(curationData.personModal.hasOwnProperty("@status") || curationData.personModal.hasOwnProperty("@identifier")  || curationData.personModal.hasOwnProperty("@created")){
					var identifier = curationData.personModal["@identifier"];
// var identifier = $stateParams.identifier.split("#")[0];
					curationData.personModal["@status"] ? delete curationData.personModal["@status"] : "";
					curationData.personModal["@identifier"] ? delete curationData.personModal["@identifier"] : " ";
					curationData.personModal["@created"] ?  delete curationData.personModal["@created"] : "";
					curationData.personModal["@updated"] ?  delete curationData.personModal["@updated"] : "";
					curationData.personModal["@is-manual"] =  "true";
					DataCurationApiService.updatePersonDataCurationDetails(curationData.personModal,identifier).then(function(personResponse){
						HostPathService.FlashSuccessMessage('Updated Suceesfully','');
						$scope.allFormsModal.personModal["@identifier"] = identifier;
						$state.go("addDataCuration",{'identifier': $stateParams.identifier});
						console.log(personResponse,$scope.allFormsModal.personModal)
					},function(personError){
						$scope.allFormsModal.personModal["@identifier"] = identifier;
						console.log(personError,'personError')
					});
				}else{
					DataCurationApiService.savePersonDataCurationDetails(curationData.personModal).then(function(personResponse){
						if(personResponse.data.identifier && $scope.isOrganisationPerson){
							allRelationsData.personIdentifier = personResponse.data.identifier;
							allRelationsData.personName = curationData.personModal["vcard:hasName"];
							openRelationModal();

						}
						},function(personError){
							console.log(personError,'personError')
						});
				}
			}else{
			curationData.organizationModal['hasActivityStatus'] = curationData.organizationModal['hasActivityStatus'].toString();
			//delete curationData.organizationModal["personalDetails"];
			curationData.organizationModal["mdaas:RegisteredAddress"] = [];
			curationData.organizationModal["mdaas:HeadquartersAddress"] = [];
			angular.forEach(curationData.organizationModal["personalDetails"],function(d,i){
				if(d.type == "HQ address"){
					var key = {
							   "streetAddress": d.building + " " + d.street,  
						       "city": d.city,
						       "zip": d.zip,  
						       "country": d.country
					};
					curationData.organizationModal["mdaas:HeadquartersAddress"].push(key);

				}else if(d.type =="Registered address"){
					var key = {
							   "streetAddress": d.building + " " + d.street,  
						       "city": d.city,
						       "zip": d.zip,  
						       "country": d.country
					};
					curationData.organizationModal["mdaas:RegisteredAddress"].push(key);
				}
			});
			delete curationData.organizationModal["personalDetails"];
			if($stateParams.identifier == "new"){
				if(curationData.organizationModal['vcard:organization-name'] && curationData.organizationModal['bst:registrationId'] && curationData.organizationModal['hasURL'])
				DataCurationApiService.saveOrgDataCurationDetails(curationData.organizationModal).then(function(organizationResponse){
					allRelationsData.orgIdentifier = organizationResponse.data.identifier;
					$stateParams.identifier = organizationResponse.data.identifier;
// $scope.allFormsModal.organizationModal.identifier =
// organizationResponse.data.identifier;
				$stateParams.identifier = organizationResponse.data.identifier;
				HostPathService.FlashSuccessMessage('Added Suceesfully','');
					if(createNew){
						$state.go("addDataCuration",{'identifier': "new"}, {notify:true})
					}else{
						$state.go("addDataCuration",{'identifier': organizationResponse.data.identifier+"#02"}, {notify:false})
					}
				},function(organizatioError){
					console.log(organizatioError,'organizatioError')
				});
				else{
					HostPathService.FlashErrorMessage('All * marked are mandetory','');
				}
			}else{
				if(curationData.organizationModal['vcard:organization-name'] && curationData.organizationModal['bst:registrationId'] && curationData.organizationModal['hasURL']){
					$scope.samplevar = curationData.organizationModal["@identifier"];
					delete curationData.organizationModal["@created"];
					delete curationData.organizationModal["@identifier"];
					delete curationData.organizationModal["@status"];
					delete curationData.organizationModal["@updated"];
					DataCurationApiService.updateOrgDataCurationDetails(curationData.organizationModal).then(function(organizationResponse){
					$state.go("addDataCuration", {notify:true})
					HostPathService.FlashSuccessMessage('Updated','');
					},function(organizatioError){
						console.log(organizatioError,'organizatioError')
					});
			}else{
					HostPathService.FlashErrorMessage('All * marked are mandetory','');
				}
			}
		}

	}

	/*
	 * @purpose: open Relation modal @created: 8 feb 2018 @params: null @return:
	 * no @author: Ankit
	 */
	function openRelationModal(){
		var relationtModalInstance = $uibModal.open({
            templateUrl: 'scripts/manage/modal/views/relation.modal.html',
            controller: 'RelationModalController',
            windowClass: 'custom-modal related-person-modal relation-curation-modal',
            resolve: {
            	AllRelationsData: function(){
            		return allRelationsData;
            	}
            }

		});

		relationtModalInstance.result.then(function (response) {
        }, function (reject) {
        });
	};
	/*
	 * @purpose: append same value @created: 3 feb 2018 @params: null @return:
	 * no @author: Virendra
	 */
	$scope.appendSameValue = function(status){
		if(status){
			$scope.allFormsModal.organizationModal['mdaas:HeadquartersAddress'] = $scope.allFormsModal.organizationModal['mdaas:RegisteredAddress'];
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersPhoneNumber'] = $scope.allFormsModal.organizationModal['tr-org:hasRegisteredPhoneNumber'];
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersFaxNumber'] = $scope.allFormsModal.organizationModal['hasRegisteredFaxNumber'];
		}else{
			$scope.allFormsModal.organizationModal['mdaas:HeadquartersAddress'] = "";
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersPhoneNumber'] = "";
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersFaxNumber'] = "";
		}

	}

	document.addEventListener("keydown", function(e) {
		  if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
		    e.preventDefault();
		    saveCurationDetails($scope.allFormsModal);
		  }
		}, false);


	$rootScope.ChangeForm =function(val){
		$cookies.put("selection", val);
		if(val == "simple")
			$rootScope.CheckDisable = false;
		else
			$rootScope.CheckDisable= true;
	}

	setTimeout(function(){
		if(!$cookies.get('selection')){
			$rootScope.CheckDisable = false;
			$("#simpleForm").prop("checked",true);
			$cookies.put("selection", 'simple');
		}else{

				var val = $cookies.get('selection');
				if(val == "simple"){
					$rootScope.CheckDisable = false;
					$("#simpleForm").prop("checked",true);
				}else{
					$rootScope.CheckDisable= true;
					$("#fullForm").prop("checked",true);
				}
		}
	},10);
//	$scope.dataCuration.locationEvents = {
//
//			onInitDone() { // This event is not firing on selection of max
//							// limit
//				$(".OtherLocationsScroll").find("ul").mCustomScrollbar({
//					axis : "y",
//					theme : "minimal"
//				});
//// $scope.staging['general_information']['other_locations'] = "";
//// if($scope.dataCuration.locationsModel.length > 0)
//// {
////
//// angular.forEach($scope.dataCuration.locationsModel,function(d,i){
//// if(i != 0)
//// $scope.staging['general_information']['other_locations'] += " , ";
////
//// $scope.staging['general_information']['other_locations'] += d.label;
//// });
//// }
//			},
//	};

	/*-----functions----*/
	  $scope.onSelect = function (item) {
		var aa = $scope.names.indexOf(item);
		$scope.job_title= $scope.names[aa].title;
		$scope.job_category = $scope.names[aa].catagory;
		$scope.addNewDetails.hide();
	  };

	  $scope.getStates = function(current) {
		    var statesCopy = $scope.groups.slice(0);
		    if (current) {
		      statesCopy.unshift(current);
		    }
		    return statesCopy;
		  };

	  $scope.onSet=function(item){
		  if($scope.groups.indexOf(item) < 0){
			  $scope.groups.push(item);
		  }
	  }
	  setTimeout(function(){
		    $('#domiciledData').chosen({allow_single_deselect: true});

		    $('#countriesData').chosen({allow_single_deselect: true});

		    $('#industriesData').chosen({allow_single_deselect: true});

		    $('#EconomicSectorData').chosen({allow_single_deselect: true});

		    $('#businessSectorDatas').chosen({allow_single_deselect: true});

		    $("#nationalityDatas").chosen({allow_single_deselect: true});
		    
		    $("#relatedPersonId").chosen({allow_single_deselect: true});

// $("#nationalityDatas_chosen").width("289px");
	  },100)

	  $scope.$on('eventFired', function(event, data) {
//        $scope.someFunction();
		  console.log("we are here", event,data)
		  var key = {
				"name" : data.data.name,
				"position":data.data.config.data.hasPositionType, //d["hasPositionType"],
				"title": "person",
				"from": data.data.config.data.from, //d["from"],
				"to": data.data.config.data.to, //["to"],
				"orgId":data.data.config.data.isPositionIn, //["isPositionIn"],
				"personId":data.data.config.data.hasHolder   ///["hasHolder"]
		  }
			$scope.persons.list.push(key);
	  })

	    /*
		 * @purpose: Getting person details @created: 6th Feb 2018 @params:
		 * params @return: no @author: Zameer
		 */
	  	var officersTotal,DirectorTotal;
	    function getdirectorOfficerData(){
		var dirApi = EHUB_API + 'datacuration/relation/directorship/search';
		var officerAPI =  EHUB_API + 'datacuration/relation/officership/search';
		var id = $stateParams.identifier.split("#")[0];
		var data = {  "isPositionIn": id };
		
		/*--- for list of Directorship ----*/
		  DataCurationApiService.getDirectorOfficeriInfodetails(dirApi, data).then(function(response){
				if(response.data.hits.length > 0){
					DirectorTotal = response.data.hits.length;
					$scope.dataCuration.directorLength = 0;
					angular.forEach(response.data.hits,function(d,i){
						  var url = EHUB_API + 'entity/person/'+ d["hasHolder"];
						  DataCurationApiService.getPersonInDetails(d,url).then(function(res){
//							  console.log(res,'Hey its her')
								$scope.dataCuration.directorLength++;
							  var key = {
								  		"name" : res[0].data["vcard:hasName"],
										"position":res[1]["hasPositionType"],
										"title": "person",
										"from":res[1]["from"],
										"to":res[1]["to"],
										"orgId":res[1]["isPositionIn"],
										"personId":res[1]["hasHolder"]
								}
								$scope.persons.list1.push(key);
							  	if($scope.dataCuration.directorLength == DirectorTotal){
//									console.log("oooookkkkk")
									$scope.dataCuration.directorsRetireve = true;
									checkContent();
								}
						  },function(error){
							  HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
						  });
						})
				}else{
					$scope.dataCuration.directorsRetireve = true;
					checkContent();
				}
				$scope.isLoading = false;
			}, function(error){
				$scope.isLoading = false;
				HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
			});

		  /*----  for list of officership  -----*/
		  DataCurationApiService.getDirectorOfficeriInfodetails(officerAPI, data).then(function(response){
//			  console.log(response,response.data.hits.length);
			  if(response.data.hits.length > 0){
				  officersTotal = response.data.hits.length;
				  $scope.dataCuration.officerLength = 0;
				  angular.forEach(response.data.hits,function(d,i){
				  var url = EHUB_API + 'entity/person/'+ d["hasHolder"];
				  DataCurationApiService.getPersonInDetails(d,url).then(function(res){
//					  console.log(res,'Hey its here another')
					  $scope.dataCuration.officerLength++;
					  var key = {
					  		"name" : res[0].data["vcard:hasName"],
							"position":res[1]["hasPositionType"],
							"title": "person",
							"from":res[1]["from"],
							"to":res[1]["to"],
							"orgId":res[1]["isPositionIn"],
							"personId":res[1]["hasHolder"]

					}
					$scope.persons.list1.push(key);
					  if( $scope.dataCuration.officerLength == officersTotal){
//						  console.log("yup its oookkkk");
						  $scope.dataCuration.officersRetrieve = true;
						  checkContent();
					  }
				  },function(error){
					  HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
				  });
				});
			  }else{
				  $scope.dataCuration.officersRetrieve = true;
				  checkContent();
			  }
			}, function(error){
				HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
			});

		  console.log($scope.persons.list,'$scope.persons.list')
		}

		function onClickEditStaging(stage){
			console.log(stage);
			$scope.dataCuration.tabActive = 1;
			var url = EHUB_API + "entity/person/"+ stage.personId;
			var type = "01"
			$scope.isLoading = true;
			getData(url,type);
		}

		function resetFields(){
			$scope.allFormsModal.personModal = {
					  "bst:facebook": "",
					  "vcard:hasAddress": "",
					  "vcard:hasGender": "male",
					  "vcard:hasName": "",
					  "bst:deathDate": "",
					  "bst:youtube": "",
					  "vcard:honorific-prefix": "Mr.",
					  "@is-manual": "true",
					  "bst:linkedin": "",
					  "vcard:hasPhoto": "",
					  "vcard:bday": "",
					  "vcard:hasTelephone": "",
					  "vcard:family-name": "",
					  "vcard:additional-name": "",
					  "bst:email": "",
					  "@source-id": "data-curation-ui",
					  "vcard:hasURL": "",
					  "vcard:honorific-suffix": "",
					  "tr-vcard:preferred-name": "",
					  "bst:aka": "",
					  "bst:description": "",
					  "vcard:given-name": "",
					  "bst:nationality": "",
					  "bst:instagram": "",
					  "bst:gplus": "",
					  "bst:twitter": ""
					};
		}

		function addNewPerson(){
			$scope.dataCuration.tabActive = 1;
			$scope.isOrganisationPerson = true;
		}
		
		function checkContent(){
			if($scope.dataCuration.directorsRetireve && $scope.dataCuration.officersRetrieve){
//				console.log("oooookkkkk   done donaana done!!!!",$scope.persons.list1);
				$scope.persons.list = $scope.persons.list1;
			}
		}


// $scope.klocations = function(current) {
// // console.log($scope.workLocations)
// var locCopy = $scope.workLocations.slice(0);
// if (current) {
// var ss = current + ' -add new'
// locCopy.unshift(ss);
// }
// console.log($scope.workLocations,locCopy)
// return locCopy;
// };

// $scope.onSetlocations=function(item){
// console.log(item)
// if(item.indexOf(' -add new') < 0){
//
// }
// if($scope.workLocations.indexOf(item) < 0){
// $scope.workLocations.push(item);
// }
// }

	$scope.workLocations =['Delhi','Mumbai','Chennai','Hyderabad','Pune','Banglore'];
	$scope.industry = ['Agriculture, forestry and fishing','Mining and quarrying','Manufacturing','Electricity, gas, steam and air conditioning supply','Water supply; sewerage, waste management and remediation activities','Construction','Wholesale and retail trade; repair of motor vehicles and motorcycles','Transportation and storage','Accommodation and food service activities','Information and communication','Financial and insurance activities','Real estate activities','Professional, scientific and technical activities','Administrative and support service activities','Public administration and defence; compulsory social security','Education','Human health and social work activities','Arts, entertainment and recreation','Other service activities','Activities of households as employers; undifferentiated goods- and services-producing activities of households for own use','Activities of extraterritorial organizations and bodies']
	$scope.groups = ['insurance','marketing','healthcare','IT','App Development'];
	$scope.positions = ['CEO','CFO','CIO','COO','CMO'];
	$scope.names =[{name:'ajay',title:'IT',location:'delhi',catagory:'junior'},{name:'manoj',title:'teacher',location:'chennai',catagory:'Professor'},{name:'shankar',title:'MBA',location:'mumbai',catagory:'marketing'},{name:'neha',title:'dance',location:'kolkata',catagory:'teacher'},{name:'mohan',title:'singer',location:'delhi',catagory:'junior'}]

	// initialzing staging variables
	$scope.staging = {
		'general_information': {
			'description': '',
			'company_status': '',
			'website': '',
			'additional_operating_verticals': '',
			'name': '',
			'employees': '',
			'year_founded': '',
			'ownership_status': '',
			'investment_status': '',
			'total_investments': '',
			'product_status': '',
			'business_status': '',
			'business_model': {
				'b2c': false,
				'b2b': false,
				'ip_licensing': false,
				'xaas': false,
				'other': false
			},
			'technology_classifier': '',
			'main_operating_verticals': '',
			'ID':'',
			'VAT':'',
			'industry' : ''
		},
		'leadership': [{
			'name': '',
			'title': '',
			'board_seats': '',
			'office': '',
			'email': '',
			'phone': ''
		}],
		'investment': [{
			'name':'',
			'job_title':'',
			'linkedin_url':'',
			'work_location':'',
			'subsidary_name':'',
			'job_category':''
		}],
		'job_openings': [{
			'title_five': '',
			'company_subsidary': '',
			'location_five': '',
			'description_five': ''
		}],
		'Documents_eleven': [{
			'title_eleven': '',
			'url_eleven': ''
		}],
		'companyLocations':[{
			'locations_one':'',
			'continent_one':''
		}],
		'personals':[{
			'contact_person_one':'',
			'contact_details_one':''
		}],
		'subsidiaries':[{
			'status':'',
			'company_name':'',
			'ceo':'',
			'revenue':'',
			'company_size':'',
			'main_business':'',
			'hq_location':'',
			'linkedin_profile':'',
			'facebook_profile':'',
			'twitter_url':''
		}],
		'BoardMgmt_BM':[{
			   'name':'',
			   'title':'',
			   'area_acountability':'',
			   'apponitment':'',
			   'salary':'',
			   'country':'',
			   'phone_work':'',
			   'work_fax':'',
			   'birth_year':''
			  }],
			  'BoardMgmt_AC':[{
			   'name':'',
			   'title':''
			  }],
			  'BoardMgmt_KE':[{
			   'name':'',
			   'title':''
			  }],
			  'BoardMgmt_OBM':[{
			   'name':'',
			   'primary_company':'',
			   'type_board_members':''
			  }],
		'Competitors':[{
			'source_eleven':'',
			'size_eleven':'',
			'revenue_eleven':'',
			'ceo_eleven':'',
			'linkedin_eleven':''
		}],
		'stocksGrpah':[{
			'holder':'',
			'shares':'',
			'data_reported':'',
			'percent_out':'',
			'total_value':''
		}],
		'news':[{
			'fax':'',
			'contact':'',
			'country':'',
			'compensation':'',
			'birth_year':'',
			'name':''
		}],
		"mgmt_key_exec":[{
			'name':'',
			'links':''
		}],
		"mgmt_profile":[{
			'name':'',
			'birth_year':'',
			'compensation':'',
			'country':'',
			'contact':'',
			'fax':''
		}],
		"source_links":[{
			'from':'',
			'dump_uRL':'',
			'information':''
		}],
		"source_gossips":[{
			'link':'',
			'title':''
		}],
		"aquisition":[{
			'entity':'',
			'date':'',
			'company':'',
			'shares_of':'',
			'links':''
		}],
		"technology":[{
			'source':'',
			'url':'',
			'about':''
		}],
		onClickAddInvestment: onClickAddInvestment,
		onClickRemoveInvestment: onClickRemoveInvestment,
		jobOpenings: jobOpenings,
		onClickRemoveJobOpenings: onClickRemoveJobOpenings,
		munchiReDocuments: munchiReDocuments,
		onClickMunchiReDocuments: onClickMunchiReDocuments,
		onClickCompetitors: onClickCompetitors,
		onClickRemoveCompetitors: onClickRemoveCompetitors,
		closeModal: closeModal,
		onClickSave: onClickSave,
		addNewPersonDetail: addNewPersonDetail,
		newLocation:newLocation,
		onClickRemoveLocation:onClickRemoveLocation,
		newContact:newContact,
		onClickRemoveContact:onClickRemoveContact,
		addnewGroup:addnewGroup,
		removeCurrentGroup:removeCurrentGroup,
		onClickRemoveboardMembersBM:onClickRemoveboardMembersBM,
		boardMembersBM:boardMembersBM,
		onClickRemoveboardMembersKE:onClickRemoveboardMembersKE,
		boardMembersKE:boardMembersKE,
		onClickRemoveboardMembersAC:onClickRemoveboardMembersAC,
		boardMembersAC:boardMembersAC,
		onClickRemoveboardMembersOBM:onClickRemoveboardMembersOBM,
		boardMembersOBM:boardMembersOBM,
		removeStocksGroup:removeStocksGroup,
		stocksGroup:stocksGroup,
		removemanagementProfile:removemanagementProfile,
		addManagementProfile:addManagementProfile,
		addexecutiveProfile:addexecutiveProfile,
		removeexecutiveProfile:removeexecutiveProfile,
		addNewSource:addNewSource,
		removeCurrentSource:removeCurrentSource,
		addNewgossipTopic:addNewgossipTopic,
		removeNewgossipTopic:removeNewgossipTopic,
		addnewAquisition:addnewAquisition,
		removenewAquisition:removenewAquisition,
		addNewInvestmentDetail:addNewInvestmentDetail,
		removeInvestmentDetail:removeInvestmentDetail,
		addnewtech:addnewtech,
		removecurrenttech:removecurrenttech
	};

	// pop-up modal to receive person details
	function addNewPersonDetail(){

// alert("inside addNewPersonDetail() ");
		var newPersonModalInstance = $uibModal.open({
			templateUrl: 'static/js/modal/views/enter-person-details.html',
			controller: 'enterPersonDetailsModalController',
			backdrop: 'true',
			windowClass: 'custom-modal request-candidate-modal'
		});

		newPersonModalInstance.result.then(function(response){
		});
	}

	// me for current job openings...............................
	function jobOpenings(){
		console.log("hi.........", $scope.staging);
// alert("inside jobOpenings() ")
 		$scope.staging['job_openings'].push({
 			title_five: '',
			company_subsidary: '',
			location_five: '',
			description_five: ''
 		});
	}

	function onClickRemoveJobOpenings(index){
// alert(' inside job opening remove ')
 		$scope.staging.job_openings.splice(index, 1);
 	}

	// me for MunchiRe documents..................................
	function munchiReDocuments(){
// alert("inside Documents_eleven() ")
 		$scope.staging['Documents_eleven'].push({
 			title_eleven: '',
			url_eleven: ''
 		});
	}

	function onClickMunchiReDocuments(index){
// alert(' inside Documents_eleven() remove ');
 		$scope.staging.Documents_eleven.splice(index, 1);
 	}

	// for Locations
	function newLocation(){
 		$scope.staging['companyLocations'].push({
 			locations_one:'',
			continent_one:''
 		});
	}

	function onClickRemoveLocation(index){
 		$scope.staging.companyLocations.splice(index, 1);
 	}

	// For Contacts persons Details
	function newContact(){
 		$scope.staging['personals'].push({
 			contact_person_one:'',
			contact_details_one:''
 		});
	}

	function onClickRemoveContact(index){
// alert("inside Location Remove function ")
 		$scope.staging.personals.splice(index, 1);
 	}

	// for Subsidiaries
	function addnewGroup(){
 		$scope.staging['subsidiaries'].push({
 			status:'',
			company_name:'',
			ceo:'',
			revenue:'',
			company_size:'',
			main_business:'',
			hq_location:'',
			linkedin_profile:'',
			facebook_profile:'',
			twitter_url:''
 		});
	}

	function removeCurrentGroup(index){
 		$scope.staging.subsidiaries.splice(index, 1);
 	}

	/*--------Board Management--------*/
	 // for Board Members BM
	 function boardMembersBM(){
// alert("added");
	   $scope.staging['BoardMgmt_BM'].push({
	    status:'',
	   name:'',
	   title:'',
	   area_acountability:'',
	   apponitment:'',
	   salary:'',
	   country:'',
	   phone_work:'',
	   work_fax:'',
	   birth_year:''
	   });
	 }

	 function onClickRemoveboardMembersBM(index){
	   $scope.staging.BoardMgmt_BM.splice(index, 1);
	  }

	 // for Board Members AC
	 function boardMembersAC(){
	   $scope.staging['BoardMgmt_AC'].push({
	   name:'',
	   title:''
	   });
	 }

	 function onClickRemoveboardMembersAC(index){
	   $scope.staging.BoardMgmt_AC.splice(index, 1);
	  }

	 // for Board Members KE
	 function boardMembersKE(){
	   $scope.staging['BoardMgmt_KE'].push({
	   name:'',
	   title:''
	   });
	 }

	 function onClickRemoveboardMembersKE(index){
	   $scope.staging.BoardMgmt_KE.splice(index, 1);
	  }

	 // for Board Members OBM
	 function boardMembersOBM(){
	   $scope.staging['BoardMgmt_OBM'].push({
	   name:'',
	   primary_company:'',
	   type_board_members:''
	   });
	 }

	 function onClickRemoveboardMembersOBM(index){
	   $scope.staging.BoardMgmt_OBM.splice(index, 1);
	  }

	 // for Board Stocks Graph
	 function stocksGroup(){
	   $scope.staging['stocksGrpah'].push({
		holder:'',
		shares:'',
		data_reported:'',
		percent_out:'',
		total_value:''
	   });
	 }

	 function removeStocksGroup(index){
	   $scope.staging.stocksGrpah.splice(index, 1);
	  }

	 // for Board Management Profile
	 function addManagementProfile(){
	   $scope.staging['mgmt_profile'].push({
		    name:'',
			birth_year:'',
			compensation:'',
			country:'',
			contact:'',
			fax:''
	   });
	 }

	 function removemanagementProfile(index){
	   $scope.staging.mgmt_profile.splice(index, 1);
	  }

	 // for Board Key Executive
	 function addexecutiveProfile(){
	   $scope.staging['mgmt_key_exec'].push({
		name:'',
		links:''
	   });
	 }

	 function removeexecutiveProfile(index){
	   $scope.staging.mgmt_key_exec.splice(index, 1);
	  }

		 // for Article Source
		 function addNewSource(){
		   $scope.staging['source_links'].push({
			   from:'',
				dump_uRL:'',
				information:''
		   });
		 }

		 function removeCurrentSource(index){
		   $scope.staging.source_links.splice(index, 1);
		  }

		 // for new Gossip Topic
		 function addNewgossipTopic(){
		   $scope.staging['source_gossips'].push({
			title:'',
			link:''
		   });
		 }

		 function removeNewgossipTopic(index){
		   $scope.staging.source_gossips.splice(index, 1);
		  }

		 // for new Gossip Topic
		 function addNewInvestmentDetail(){
		   $scope.staging['invest_details'].push({
			compnay_name:'',
			amount:'',
			fund_date:'',
			article:''
		   });
		 }

		 function removeInvestmentDetail(index){
		   $scope.staging.invest_details.splice(index, 1);
		  }

		 // for new Aquisition
		 function addnewAquisition(){
			   $scope.staging['aquisition'].push({
						entity:'',
						date:'',
						company:'',
						shares_of:'',
						links:''
			   });
			 }

			 function removenewAquisition(index){
			   $scope.staging.aquisition.splice(index, 1);
			  }

				 // for new technology
				 function addnewtech(){
					   $scope.staging['technology'].push({
								source:'',
								url:'',
								about:''
					   });
					 }

					 function removecurrenttech(index){
					   $scope.staging.technology.splice(index, 1);
					  }

	 // me for competetors .......................................
	function onClickCompetitors(){
// alert("inside onClickcompetitors() ")
 		$scope.staging['Competitors'].push({
 			source_eleven:'',
			size_eleven:'',
			revenue_eleven:'',
			ceo_eleven:'',
			linkedin_eleven:''
 		});
	}

	function onClickRemoveCompetitors(index){
// alert(' inside onClickcompetitors() remove ');
 		$scope.staging.Competitors.splice(index, 1);
 	}

	/*
	 * @purpose: close modal function @created: 06 oct 2017 @params: none
	 * @returns: none @author: sandeep
	 */
 	function closeModal(){
 		$uibModalInstance.dismiss('close');
// $scope.staging['investment'].push({
// name:'',
// job_title:'',
// linkedin_url:'',
// work_location:'',
// subsidary_name:'',
// job_category:''
// })
 	};

	/*
	 * @purpose: onClickSave function @created: 09 oct 2017 @params: none
	 * @returns: none @author: sandeep
	 */
 	function onClickSave(){
 		var objectData = {
 			general_information: $scope.staging['general_information'],
 			contact_information: $scope.staging['contact_information'],
 			job_openings: $scope.staging['job_openings'],
 			Documents_eleven: $scope.staging['Documents_eleven'],
 			leadership: $scope.staging['leadership'],
 			investments: $scope.staging['investment'],
 			investors: $scope.staging['investors'],
 			lead_partners_on_deals: $scope.staging['lead_partners_on_deals'],
 			news: $scope.staging['news']
 		};
 		console.log('Saving Data: ', objectData);
 	};

	/*
	 * @purpose: onClickAddInvestment function @created: 09 oct 2017 @params:
	 * none @returns: none @author: sandeep
	 */
 	function onClickAddInvestment(){
// alert("inside add...!!!");
 		$scope.staging['investment'].push({
 			name:'',
			job_title:'',
			linkedin_url:'',
			work_location:'',
			subsidary_name:'',
			job_category:''
 		});
 	};

	/*
	 * @purpose: onClickRemoveInvestment function @created: 09 oct 2017 @params:
	 * index(number) @returns: none @author: sandeep
	 */
 	function onClickRemoveInvestment(index){
 		$scope.staging.investment.splice(index, 1);
 	}

	/*
	 * @purpose: openDatepickerModal function @created: 09 oct 2017 @params:
	 * modelType(string), fieldType(string), index(nnumber) @returns: none
	 * @author: sandeep
	 */
 	function openDatepickerModal($event, dateObject){
 	    $event.preventDefault();
 	    $event.stopPropagation();
 	    dateObject.isModalOpened = true;
 	}

	$scope.disableSave = function(){
		$scope.dsbl = true;
	}
//	$scope.dsbl = true;
	$scope.enableSave = function(){
		$scope.dsbl = false;
	}

// to get person data

	/*
	 * function getPersonData(){
	 * DataCurationApiService.getPersonDetails().then(function(response){
	 * console.log(response, 'personResponse'); }, function(error){
	 * HostPathService.FlashErrorMessage('ERROR',error.responseMessage); }) }
	 *
	 * getPersonData();
	 */

	// get person data end
}