package element.bst.elementexploration.test.generic.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GenericServiceTest {

	@SuppressWarnings("rawtypes")
	@Mock
	protected GenericDao genericDao;

	@SuppressWarnings("rawtypes")
	@InjectMocks
	GenericServiceImpl genericService;

	List<Object> objectsList = new ArrayList<Object>();

	Object object = new Object();

	@Before
	public void setupMock() {
		objectsList.add(object);
		objectsList.add(new Object());
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findTest() {
		Mockito.when(genericDao.find(Mockito.anyObject())).thenReturn(object);
		Object objectNew = genericService.find(1L);
		Assert.assertNotNull(objectNew);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void saveTest() {
		Mockito.when(genericDao.create(Mockito.anyObject())).thenReturn(object);
		Object objectNew = genericService.save(object);
		Assert.assertNotNull(objectNew);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void saveOrUpdateTest() {
		Mockito.doThrow(Exception.class).when(genericDao).saveOrUpdate(Mockito.any(Object.class));
		genericService.saveOrUpdate(object);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void deleteTest() {
		Mockito.doThrow(Exception.class).when(genericDao).delete(Mockito.any(Object.class));
		genericService.delete(object);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findAllTest() {
		Mockito.when(genericDao.findAll()).thenReturn(objectsList);
		List<Object> objectListNew = genericService.findAll();
		assertEquals(2, objectListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void updateTest() {
		Mockito.doThrow(Exception.class).when(genericDao).update(Mockito.any(Object.class));
		genericService.update(object);
	}

	@Test(expected = Exception.class)
	public void flushTest() {
		Mockito.doThrow(Exception.class).when(genericDao).flush();
		genericService.flush();
	}

}
