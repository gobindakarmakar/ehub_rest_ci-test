package element.bst.elementexploration.rest.extention.widgetreview.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.widgetreview.dto.ComplianceWidgetDto;
import element.bst.elementexploration.rest.extention.widgetreview.dto.WidgetReviewDto;
import element.bst.elementexploration.rest.extention.widgetreview.service.ComplianceWidgetService;
import element.bst.elementexploration.rest.extention.widgetreview.service.WidgetReviewService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author rambabu
 *
 */
@Api(tags = { "Widget Review API" }, description = "Manages widget reviews")
@RestController
@RequestMapping(value = "/api/widget")
public class WidgetReviewController extends BaseController {

	@Autowired
	ComplianceWidgetService complianceWidgetService;

	@Autowired
	WidgetReviewService widgetReviewService;

	@ApiOperation("Gets compliance widgets")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllComplianceWidgets", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllComplianceWidget(@RequestParam("token") String token, HttpServletRequest request)
			throws Exception {
		List<ComplianceWidgetDto> complianceWidgetDtoList = complianceWidgetService.getAllComplianceWidget();
		return new ResponseEntity<>(complianceWidgetDtoList, HttpStatus.OK);
	}

	@ApiOperation("Save widget reviews")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveWidgetReviews", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveWidgetReviews(@Valid @RequestBody List<WidgetReviewDto> WidgetReviewDtoList,
			BindingResult results, @RequestParam String token, HttpServletRequest request) throws Exception {

		Long userId = getCurrentUserId();
		boolean status = widgetReviewService.saveWidgetReviews(WidgetReviewDtoList, userId);
		if (status)
			return new ResponseEntity<>(status, HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("Failed to add widget review"),
					HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ApiOperation("Gets widgets reviews")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getWidgetReviewsByEntityId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getWidgetReviewsByEntityId(@RequestParam(required = true) String entityId,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		List<WidgetReviewDto> widgetReviewDtoList = widgetReviewService.getWidgetReviewsByEntityId(entityId);
		return new ResponseEntity<>(widgetReviewDtoList, HttpStatus.OK);
	}

}
