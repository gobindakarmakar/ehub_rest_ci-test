package element.bst.elementexploration.rest.extention.menuitem.service;

import element.bst.elementexploration.rest.extention.menuitem.domain.LastVisitedDomains;
import element.bst.elementexploration.rest.extention.menuitem.dto.LastVisitedDomainDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface LastVisitedDomainService extends GenericService<LastVisitedDomains, Long> {

	LastVisitedDomains saveOrUpdateLastVisitedDomain(LastVisitedDomainDto lastVisitedDomainDto, Long userId)
			throws Exception;
	
	LastVisitedDomains findLastVisitedDomainByUserId(Long userId);

}
