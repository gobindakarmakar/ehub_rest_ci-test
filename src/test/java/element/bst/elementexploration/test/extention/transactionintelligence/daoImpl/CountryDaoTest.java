package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.CountryDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CountryDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	CountryDaoImpl countryDao;

	Country country = new Country();

	@Before
	public void setupMock() {
		country.setCountry("India");
		country.setIso2Code("IN");
		country.setIs03Code("IND");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findByNameTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(country);
		Country countryObject = countryDao.findByName("IN");
		assertEquals("IND", countryObject.getIs03Code());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByName_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Country countryObject = countryDao.findByName("IN");
		assertEquals(null, countryObject);
	}

	@Test
	public void getCountryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(country);
		Country countryObject = countryDao.getCountry("IN");
		assertEquals("IND", countryObject.getIs03Code());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCountry_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Country countryObject = countryDao.getCountry("IN");
		assertEquals(null, countryObject);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getCountry_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		Country countryObject = countryDao.getCountry("IN");
		assertEquals(null, countryObject);
	}
}
