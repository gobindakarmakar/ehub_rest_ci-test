package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class BatchScreeningResultDto {
	
	@JsonProperty("STAGE")
	private String stage;
	@JsonProperty("STATUS")
	private String status;
	@JsonProperty("TOTAL_ENTITIES")
	private int totalEntities;
	@JsonProperty("BAD_RECORDS")
	private int badRecords;
	@JsonProperty("VALID_ENTITIES")
	private int validEntity;
	@JsonProperty("WARNINGS")
	private int warnings;
	@JsonProperty("WORKFLOW_STATUS")
	private String workflowStatus;
	@JsonProperty("MESSAGE")
	private String message;
	
	
	
	public int getTotalEntities() {
		return totalEntities;
	}
	public void setTotalEntities(int totalEntities) {
		this.totalEntities = totalEntities;
	}
	public int getBadRecords() {
		return badRecords;
	}
	public void setBadRecords(int badRecords) {
		this.badRecords = badRecords;
	}
	public int getValidEntity() {
		return validEntity;
	}
	public void setValidEntity(int validEntity) {
		this.validEntity = validEntity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public int getWarnings() {
		return warnings;
	}
	public void setWarnings(int warnings) {
		this.warnings = warnings;
	}
	public String getWorkflowStatus() {
		return workflowStatus;
	}
	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
