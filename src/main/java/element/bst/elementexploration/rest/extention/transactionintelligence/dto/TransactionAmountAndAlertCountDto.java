package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Transaction amount and count")
public class TransactionAmountAndAlertCountDto implements Serializable,Comparable<TransactionAmountAndAlertCountDto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Transaction product type")	
	private String transactionProductType;
	@ApiModelProperty(value="Total transactions amount")	
	private Double totalTransactionAmount;
	@ApiModelProperty(value="Total alerts count")	
	private Long alertCount;
	@ApiModelProperty(value="Alerts ratio")
	private double alertRatio;
	@ApiModelProperty(value="Total transactions count")
	private Long transactionsCount;
	@ApiModelProperty(value="Total alerted transactions count")
	private Double alertedAmount;
	@ApiModelProperty(value="Business date")
	private String businessDate;
	@ApiModelProperty(value="Input count")
	private Long inputCount;
	@ApiModelProperty(value="Output count")
	private Long outputCount;
	@ApiModelProperty(value="Transaction business date")
	private Date transactionBusinessDate;

	public TransactionAmountAndAlertCountDto() {
		super();
	}
	
	
	

	public TransactionAmountAndAlertCountDto(Date transactionBusinessDate,Long transactionsCount,Double totalTransactionAmount,String transactionProductType) 
	{
		super();
		this.transactionProductType = transactionProductType;
		this.totalTransactionAmount = totalTransactionAmount;
		this.transactionsCount = transactionsCount;
		this.transactionBusinessDate = transactionBusinessDate;
	}
	
	public TransactionAmountAndAlertCountDto(Date transactionBusinessDate,Long transactionsCount,Double totalTransactionAmount) 
	{
		super();
		this.totalTransactionAmount = totalTransactionAmount;
		this.transactionsCount = transactionsCount;
		this.transactionBusinessDate = transactionBusinessDate;
	}

	//getTotalAmountAndAlertCountByProductType-Alerts
	public TransactionAmountAndAlertCountDto(
			Long alertCount,Double totalTransactionAmount, String transactionProductType) {
		super();
		this.transactionProductType = transactionProductType;
		this.totalTransactionAmount = totalTransactionAmount;
		this.alertCount = alertCount;
	}


	//getTotalAmountAndAlertCountByProductType-Transactions
	public TransactionAmountAndAlertCountDto(Double totalTransactionAmount, String transactionProductType,
			Long transactionsCount) {
		super();
		this.transactionProductType = transactionProductType;
		this.totalTransactionAmount = totalTransactionAmount;
		this.transactionsCount = transactionsCount;
	}

	public TransactionAmountAndAlertCountDto(Double totalTransactionAmount, Date transactionBusinessDate,
			Long transactionsCount) {
		super();
		this.transactionBusinessDate = transactionBusinessDate;
		this.totalTransactionAmount = totalTransactionAmount;
		this.transactionsCount = transactionsCount;
	}

	public TransactionAmountAndAlertCountDto(Double totalTransactionAmount, Long alertCount, Double alertRatio,
			Long transactionsCount) {
		super();
		this.totalTransactionAmount = totalTransactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionsCount = transactionsCount;
	}

	public TransactionAmountAndAlertCountDto(String transactionProductType, Double totalTransactionAmount,
			Long alertCount, Double alertRatio, Long transactionsCount) {
		super();
		this.transactionProductType = transactionProductType;
		this.totalTransactionAmount = totalTransactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionsCount = transactionsCount;
	}

	public TransactionAmountAndAlertCountDto(String transactionProductType, Double totalTransactionAmount,
			Long alertCount, Double alertRatio, Long transactionsCount, Double alertedAmount, String businessDate) {
		super();
		this.transactionProductType = transactionProductType;
		this.totalTransactionAmount = totalTransactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionsCount = transactionsCount;
		this.alertedAmount = alertedAmount;
		this.businessDate = businessDate;
	}

	//getTransactionAndAlertAggregates --Transactions
	public TransactionAmountAndAlertCountDto(String businessDate, Long transactionsCount,
			Double totalTransactionAmount) {
		super();
		this.businessDate = businessDate;
		this.transactionsCount = transactionsCount;
		this.totalTransactionAmount = totalTransactionAmount;
	}
	
	//getTransactionAndAlertAggregates --alerts
	public TransactionAmountAndAlertCountDto(Long alertCount,String businessDate,Double alertedAmount) {
		super();
		this.businessDate = businessDate;
		this.alertedAmount = alertedAmount;
		this.alertCount = alertCount;
	}
	
	

	public TransactionAmountAndAlertCountDto(String businessDate, Double alertedAmount, Long alertCount) {
		super();
		this.alertCount = alertCount;
		this.alertedAmount = alertedAmount;
		this.businessDate = businessDate;
	}

	//for getTransactionAndAlertAggregatesByProductType -transactions
	public TransactionAmountAndAlertCountDto(String businessDate, Long transactionsCount, Double totalTransactionAmount,
			String transactionProductType) {
		super();
		this.businessDate = businessDate;
		this.transactionsCount = transactionsCount;
		this.totalTransactionAmount = totalTransactionAmount;
		this.transactionProductType = transactionProductType;

	}
	
	//for getTransactionAndAlertAggregatesByProductType -alerts
	public TransactionAmountAndAlertCountDto( Long alertCount,String businessDate, Double totalTransactionAmount,
			String transactionProductType) {
		super();
		this.businessDate = businessDate;
		this.alertCount = alertCount;
		this.totalTransactionAmount = totalTransactionAmount;
		this.transactionProductType = transactionProductType;

	}

	public TransactionAmountAndAlertCountDto(String businessDate, Double alertedAmount, Long alertCount,
			String transactionProductType) {
		super();
		this.alertCount = alertCount;
		this.alertedAmount = alertedAmount;
		this.businessDate = businessDate;
		this.transactionProductType = transactionProductType;

	}

	public TransactionAmountAndAlertCountDto(String transactionProductType, Long inputCount) {
		super();
		this.transactionProductType = transactionProductType;
		this.inputCount = inputCount;
	}

	public String getTransactionProductType() {
		return transactionProductType;
	}

	public void setTransactionProductType(String transactionProductType) {
		this.transactionProductType = transactionProductType;
	}

	public Double getTotalTransactionAmount() {
		return totalTransactionAmount;
	}

	public void setTotalTransactionAmount(Double totalTransactionAmount) {
		this.totalTransactionAmount = totalTransactionAmount;
	}

	public Long getAlertCount() {
		return alertCount;
	}

	public void setAlertCount(Long alertCount) {
		this.alertCount = alertCount;
	}

	public Double getAlertRatio() {
		return alertRatio;
	}

	public void setAlertRatio(Double alertRatio) {
		this.alertRatio = alertRatio;
	}

	public Long getTransactionsCount() {
		return transactionsCount;
	}

	public void setTransactionsCount(Long transactionsCount) {
		this.transactionsCount = transactionsCount;
	}

	public Double getAlertedAmount() {
		return alertedAmount;
	}

	public void setAlertedAmount(Double alertedAmount) {
		this.alertedAmount = alertedAmount;
	}

	public String getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(String businessDate) {
		this.businessDate = businessDate;
	}

	public Long getInputCount() {
		return inputCount;
	}

	public void setInputCount(Long inputCount) {
		this.inputCount = inputCount;
	}

	public Long getOutputCount() {
		return outputCount;
	}

	public void setOutputCount(Long outputCount) {
		this.outputCount = outputCount;
	}

	public Date getTransactionBusinessDate() {
		return transactionBusinessDate;
	}

	public void setTransactionBusinessDate(Date transactionBusinessDate) {
		this.transactionBusinessDate = transactionBusinessDate;
	}

	@Override
	public String toString() {
		return "TransactionAmountAndAlertCountDto [transactionProductType=" + transactionProductType
				+ ", totalTransactionAmount=" + totalTransactionAmount + ", alertCount=" + alertCount + ", alertRatio="
				+ alertRatio + ", transactionsCount=" + transactionsCount + ", alertedAmount=" + alertedAmount
				+ ", businessDate=" + businessDate + "]";
	}
	
	@Override
	public int compareTo(TransactionAmountAndAlertCountDto o) {
		return this.getBusinessDate().compareTo(o.getBusinessDate());
	}

}
