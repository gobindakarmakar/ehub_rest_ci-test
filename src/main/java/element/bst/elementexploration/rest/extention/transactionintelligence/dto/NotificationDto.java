package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author suresh
 *
 */
@ApiModel("Notification")
public class NotificationDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="List of transactions by different product types")	
	private List<TxProductTypeDto> transactionsProductTypeGroupByAndCountList;
	@ApiModelProperty(value="List of alerts by different product types")	
	private List<TxProductTypeDto> alertProductTypeGroupByAndCountList;
	@ApiModelProperty(value="List of alert statuses")	
	private List<AlertStatusDto> alertStatusDto;
	@ApiModelProperty(value="List of corporate structures")	
	private List<CorporateStructureDto> corporateStructureDto;
	@ApiModelProperty(value="List of top counter parties")	
	private List<CounterPartyNotiDto> counterpartiesNotifDtos;
	@ApiModelProperty(value="List of top five transactions")	
	private List<CounterPartyNotiDto> topFiveTransactions;

	public List<CounterPartyNotiDto> getTopFiveTransactions() {
		return topFiveTransactions;
	}

	public void setTopFiveTransactions(List<CounterPartyNotiDto> topFiveTransactions) {
		this.topFiveTransactions = topFiveTransactions;
	}

	public List<TxProductTypeDto> getAlertProductTypeGroupByAndCountList() {
		return alertProductTypeGroupByAndCountList;
	}

	public void setAlertProductTypeGroupByAndCountList(List<TxProductTypeDto> alertProductTypeGroupByAndCountList) {
		this.alertProductTypeGroupByAndCountList = alertProductTypeGroupByAndCountList;
	}

	public List<CorporateStructureDto> getCorporateStructureDto() {
		return corporateStructureDto;
	}

	public void setCorporateStructureDto(List<CorporateStructureDto> corporateStructureDto) {
		this.corporateStructureDto = corporateStructureDto;
	}

	public List<TxProductTypeDto> getTransactionsProductTypeGroupByAndCountList() {
		return transactionsProductTypeGroupByAndCountList;
	}

	public void setTransactionsProductTypeGroupByAndCountList(
			List<TxProductTypeDto> transactionsProductTypeGroupByAndCountList) {
		this.transactionsProductTypeGroupByAndCountList = transactionsProductTypeGroupByAndCountList;
	}

	public List<AlertStatusDto> getAlertStatusDto() {
		return alertStatusDto;
	}

	public void setAlertStatusDto(List<AlertStatusDto> alertStatusDto) {
		this.alertStatusDto = alertStatusDto;
	}

	public List<CounterPartyNotiDto> getCounterpartiesNotifDtos() {
		return counterpartiesNotifDtos;
	}

	public void setCounterpartiesNotifDtos(List<CounterPartyNotiDto> counterpartiesNotifDtos) {
		this.counterpartiesNotifDtos = counterpartiesNotifDtos;
	}

}