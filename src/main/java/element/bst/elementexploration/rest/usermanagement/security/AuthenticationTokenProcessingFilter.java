package element.bst.elementexploration.rest.usermanagement.security;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.security.domain.CurrentUser;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.SystemSettingTypes;

/**
 * 
 * @author Amit Patel
 *
 */
@Component("authenticationTokenProcessingFilter")
public class AuthenticationTokenProcessingFilter extends GenericFilterBean
{
	@Autowired
	private ElementsSecurityService elementsSecurityService;

	@Autowired
	private UsersService usersService;
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	SettingsService settingsService;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException
	{
		HttpServletRequest httpRequest = this.getAsHttpRequest(request);

		String token = this.extractAuthTokenFromRequest(httpRequest);
		
		//validate token from httpSession firebase object instead of DB
		Users user = elementsSecurityService.getUserUserFromToken(token);	
		if (null==user) {
				String email = "";
			String userId = "";
			if(null != token && null != servletContext.getAttribute(token)){
				JSONObject firebaseObject = (JSONObject) servletContext.getAttribute(token);
				Long authTime = firebaseObject.getLong("authTime");
				Long expirytime = firebaseObject.getLong("expireTime");
				Long currentTime = new Date().getTime()/1000;
				org.json.JSONArray userArray = firebaseObject.getJSONArray("users");
				if(userArray.length()>0){
					JSONObject userObject = (JSONObject) userArray.get(0);
					if(userObject.has("email")){
						email = userObject.get("email").toString();
					}
					if(userObject.has("localId"))
						userId = userObject.getString("localId");
				}
				 user = usersService.getFirebaseUserBySourceId(userId);
				if(expirytime>currentTime && authTime<expirytime){
					UserDetails userDetails = new CurrentUser(user.getEmailAddress(), user.getEmailAddress(), user.getUserId());
					
					Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
					authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
					/*UserDetails userDetails = new org.springframework.security.core.userdetails.User(email, userId, 
			                 true, true, true, true, authorities);*/
					UsernamePasswordAuthenticationToken authentication =
							new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}else{
					UserDetails userDetails = new CurrentUser(user.getEmailAddress(), user.getEmailAddress(), user.getUserId());
					
					Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
					authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
					/*UserDetails userDetails = new org.springframework.security.core.userdetails.User(email, userId, 
			                 false, false, false, false, authorities);*/
					UsernamePasswordAuthenticationToken authentication =
							new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
				
			}
		}else{
		//	Users user = elementsSecurityService.getUserUserFromToken(token);	
			if (user != null){
				boolean isActive = false;
			//	if(null != user.getStatus()){
					//if (Integer.parseInt(user.getStatus()) != 0)
						isActive = true;
				//}
				if(isActive){
					UserDetails userDetails = new CurrentUser(user.getEmailAddress(), user.getPassword(), user.getUserId());
					UsernamePasswordAuthenticationToken authentication =
							new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}
		}
	
		
		chain.doFilter(request, response);
	}


	private HttpServletRequest getAsHttpRequest(ServletRequest request)
	{
		if (!(request instanceof HttpServletRequest)) {
			throw new RuntimeException("Expecting an HTTP request");
		}

		return (HttpServletRequest) request;
	}


	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest)
	{
		String authToken = httpRequest.getParameter("token");
		return authToken;
	}


	

/*
	@Autowired
	private ElementSecurityService elementSecurityService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException
	{
		HttpServletRequest httpRequest = this.getAsHttpRequest(request);

		String token = this.extractAuthTokenFromRequest(httpRequest);
		User user = elementSecurityService.getUserUserFromToken(token);		
		if (user != null){
			boolean isActive = false;
			if(null != user.getStatus()){
				if (Integer.parseInt(user.getStatus()) != 0)
					isActive = true;
			}
			if(isActive){
				UserDetails userDetails = new CurrentUser(user.getEmailAddress(), user.getPassword(), user.getUserId());
				UsernamePasswordAuthenticationToken authentication =
						new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}
		chain.doFilter(request, response);
	}


	private HttpServletRequest getAsHttpRequest(ServletRequest request)
	{
		if (!(request instanceof HttpServletRequest)) {
			throw new RuntimeException("Expecting an HTTP request");
		}

		return (HttpServletRequest) request;
	}


	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest)
	{
		String authToken = httpRequest.getParameter("token");
		return authToken;
	}
*/

}