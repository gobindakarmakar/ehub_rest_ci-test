package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import java.io.File;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.advancesearch.service.EntityOfficerInfoService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
/**
 * 
 * @author Viswanath Reddy G
 *
 */
public class OfficershipScreening extends Thread {

	private JSONArray referenceArray;

	private String identifier;

	private String jurisdiction;

	private String SCREENING_URL;

	private String BIGDATA_MULTISOURCE_URL;

	private AdvanceSearchService advanceSearchService;

	private String country;

	private File file;

	private org.json.simple.JSONArray personTypes;

	private org.json.simple.JSONObject pepTypes;
	
	private String startDate;

	private String endDate;
	
	private boolean isScreeningRequired;
	
	private String apiKey;
	
	private String highCredibilitySource;
	
	private List<EntityOfficerInfoDto> listOfOfficer;
	
	@Autowired
	private EntityOfficerInfoService entityOfficerInfoService;
	
	@Value("${client_id}")
	private String clientId;

	public OfficershipScreening(JSONArray referenceArray, String identifier, String jurisdiction, String sCREENING_URL,
			String bIGDATA_MULTISOURCE_URL, AdvanceSearchService advanceSearchService, String country, File file,
			org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,String startDate,String endDate, boolean isScreeningRequired, String apiKey,String clientId) {
		super();
		this.referenceArray = referenceArray;
		this.identifier = identifier;
		this.jurisdiction = jurisdiction;
		SCREENING_URL = sCREENING_URL;
		BIGDATA_MULTISOURCE_URL = bIGDATA_MULTISOURCE_URL;
		this.advanceSearchService = advanceSearchService;
		this.country = country;
		this.file = file;
		this.personTypes = personTypes;
		this.pepTypes = pepTypes;
		this.startDate=startDate;
		this.endDate=endDate;
		this.isScreeningRequired=isScreeningRequired;
		this.apiKey=apiKey;
		this.clientId=clientId;
	}
	
	public OfficershipScreening(JSONArray referenceArray, String identifier, String jurisdiction, String sCREENING_URL,
			String bIGDATA_MULTISOURCE_URL, AdvanceSearchService advanceSearchService, String country, File file,
			org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,String startDate,String endDate,
			boolean isScreeningRequired, String apiKey,String clientId,String highCredibilitySource,List<EntityOfficerInfoDto> listOfOfficer) {
		super();
		this.referenceArray = referenceArray;
		this.identifier = identifier;
		this.jurisdiction = jurisdiction;
		SCREENING_URL = sCREENING_URL;
		BIGDATA_MULTISOURCE_URL = bIGDATA_MULTISOURCE_URL;
		this.advanceSearchService = advanceSearchService;
		this.country = country;
		this.file = file;
		this.personTypes = personTypes;
		this.pepTypes = pepTypes;
		this.startDate=startDate;
		this.endDate=endDate;
		this.isScreeningRequired=isScreeningRequired;
		this.apiKey=apiKey;
		this.highCredibilitySource=highCredibilitySource;
		this.listOfOfficer=listOfOfficer;
		this.clientId=clientId;
	}


	@Override
	public void run() {
		try {
			getOfficers();
		} catch (Exception e) {
			if (referenceArray.getJSONObject(0).has("officership")) {
				if (referenceArray.getJSONObject(0).getJSONArray("officership") != null
						&& referenceArray.getJSONObject(0).getJSONArray("officership").length() > 0) {
					referenceArray.getJSONObject(0).getJSONArray("officership").getJSONObject(0).put("status", "done");
				}
			}
			e.printStackTrace();
		}
	}


	private void getOfficers() throws Exception {
		String requestId = UUID.randomUUID().toString();
		JSONObject map = new JSONObject();
		JSONObject finalJsonTosent = new JSONObject();
		JSONArray personEntities = new JSONArray();
		JSONArray orgEntities = new JSONArray();
		JSONObject orgJson = new JSONObject();
		JSONObject personJson = new JSONObject();
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
		Object fileJsonData = parser.parse(new FileReader(fileJSon));
		org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
		String officerlink = buildMultiSourceUrl(identifier, "officership", null, null, null);
		List<JSONObject> officersreturn = new ArrayList<JSONObject>();
		Map<String, JSONObject> refMap = new HashedMap<String, JSONObject>();
		if (officerlink != null) {
			JSONObject officersJson = new JSONObject();
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(officerlink);
			String response = advanceSearchService.getHierarchyData(hierarchyDto, null,apiKey);
			if(response!=null)
				officersJson = new JSONObject(response);
			if (officersJson.has("is-completed")) {
				int counter = 0;
				while (officersJson.has("is-completed") && !officersJson.getBoolean("is-completed")) {
					response = advanceSearchService.getHierarchyData(hierarchyDto, null,apiKey);
					if (response != null) {
						if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
							officersJson = new JSONObject(response);
						}
					}
					counter++;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
			if (officersJson.has("results")) {
				JSONArray results = officersJson.getJSONArray("results");
				if (results.length() > 0) {
					JSONObject result = results.getJSONObject(0);
					if (result.has("officership")) {
						JSONObject officership = result.getJSONObject("officership");
						JSONArray keys = officership.names();
						List<String> originalIds=new ArrayList<String>();
						JSONObject elementRef=new JSONObject();
						JSONArray elementObjects= new JSONArray();
						for (int i = 0; i < keys.length(); i++) {
						///element source
						if(keys.getString(i).equalsIgnoreCase(clientId)) {
							JSONArray element=officership.getJSONArray(clientId);
								if (element != null && element.length() > 0) {
									for (int j = 0; j < element.length(); j++) {
										JSONObject elementobj = element.getJSONObject(j);
										if (elementobj.has("identifier")) {
											String element_Identifier = elementobj.getString("identifier");
											String str[] = element_Identifier.split("_");
											originalIds.add(str[0]);
											elementRef.put(str[0], elementobj);
											elementObjects.put(elementobj);
										}
									}
								}
						}
						
					}
				
						////

						int count = 0;
						for (int i = 0; i < keys.length(); i++) {
							JSONArray officersFromSource = new JSONArray();
							if (officership.get(keys.getString(i)) instanceof JSONArray)
								officersFromSource = officership.getJSONArray(keys.getString(i));
							if (officersFromSource != null) {
								for (int j = 0; j < officersFromSource.length(); j++) {
									JSONObject officerJson = officersFromSource.getJSONObject(j);
									///
									
									////
									if (officerJson.has("status")
											&& officerJson.getString("status").equalsIgnoreCase("active")) {
										if (officerJson.has("name")) {
											String name = officerJson.getString("name");
											Pattern p = Pattern.compile(
													"(Mr.|MR.|Dr.|mr.|DR.|dr.|ms.|Ms.|MS.|Miss.|Mrs.|mrs.|miss.|MR|mr|Mr|Dr|DR|dr|ms|Ms|MS|miss|Miss|Mrs|mrs)"
															+ "\\b");
											Matcher m = p.matcher(name);
											String nameWithoutSalutation = m.replaceAll("");
											officerJson.put("name", nameWithoutSalutation);
										}
										///
										EntityOfficerInfoDto entityOfficerInfoDto= new EntityOfficerInfoDto();
										entityOfficerInfoDto.setOfficerName(officerJson.getString("name"));
										entityOfficerInfoDto.setSource(keys.getString(i));
										entityOfficerInfoDto.setEntityId(identifier);
										if(listOfOfficer!=null && !listOfOfficer.isEmpty()) {
										for(EntityOfficerInfoDto dto: listOfOfficer) {
											if(officerJson.getString("name").equalsIgnoreCase(dto.getOfficerName())) {
												officerJson.put("highCredibilitySource",dto.getSource());
											}
											else {
												officerJson.put("highCredibilitySource", highCredibilitySource);
												}
										}
										}
										else {
										officerJson.put("highCredibilitySource", highCredibilitySource);
										}
										///}
										if(officerJson.has("identifier")) {
											officerJson.put("officerIdentifier", officerJson.getString("identifier"));
											}
										///
										
											officerJson.put("information_provider", keys.getString(i));
										
										
											officerJson.put("source", keys.getString(i));
										
										///
										if (officerJson.has("address")) {
											String officerCountry = null;
											JSONObject address = null;
											if (officerJson.has("address")
													&& (officerJson.get("address") instanceof JSONObject))
												address = officerJson.getJSONObject("address");
											if (address != null && address.has("country")
													&& !"".equalsIgnoreCase(address.getString("country"))) {
												officerCountry = address.getString("country");
												for (int k = 0; k < countryJson.size(); k++) {
													org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson
															.get(k);
													if (countryObj.get("country").toString()
															.equalsIgnoreCase(officerCountry)) {
														officerJson.put("jurisdiction",
																countryObj.get("countryCode").toString());
														officerJson.put("country", officerCountry);
													}
												}
											} else {
												officerJson.put("jurisdiction", jurisdiction);
												officerJson.put("country", country);

											}
										} else {
											officerJson.put("jurisdiction", jurisdiction);
											officerJson.put("country", country);
										}
										officerJson.put("status", "pending");
										officerJson.put("screeningUrl", SCREENING_URL + "/"
												+ URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20") + "/"
												+ requestId);
										officerJson.put("entity_id", identifier + "off" + count);
										officerJson.put("identifier", identifier + "off" + count);
										///
										if (!keys.getString(i).equals(clientId)) {
											if (originalIds != null && originalIds.size() > 0) {
													if(originalIds.contains(officerJson.getString("officerIdentifier"))) {
														
														JSONObject h=elementRef.getJSONObject(officerJson.getString("officerIdentifier"));
														String removingIdentifier=h.getString("identifier");
														if(h.has("name")) {
																officerJson.put("name", h.getString("name"));
														}
														if(h.has("date_of_birth")) {
																officerJson.put("date_of_birth", h.getString("date_of_birth"));
															
														}
														if(h.has("country_of_residence")) {
																officerJson.put("country_of_residence", h.getString("country_of_residence"));
														}
														if(h.has("country")) {
															officerJson.put("country", h.getString("country"));
														}
														if(h.has("jurisdiction")) {
															officerJson.put("jurisdiction", h.getString("jurisdiction"));
														}
														if(h.has("officer_role")) {
																officerJson.put("officer_role", h.getString("officer_role"));
															
														}
														if(h.has("id")) {
															officerJson.put("id", h.getString("id"));
														}
														if(h.has("classification")&& h.get("classification") instanceof String) {
															officerJson.put("classification", h.getString("classification"));
														}
														if(h.has("classification")&& h.get("classification") instanceof JSONArray) {
															officerJson.put("classification", h.getJSONArray("classification").toString());
														}
														if(h.has("sourceUrl")) {
															officerJson.put("sourceUrl", h.getString("sourceUrl"));
														}
														if (h.has("customSource")) {
															officerJson.put("source", h.getString("customSource"));
														}
													if(h.has("from")) {
															officerJson.put("from", h.getString("from"));
														}
														elementRef.remove(removingIdentifier);
														originalIds.remove(removingIdentifier);
													}
													
											}
										}
										///
										if (refMap.containsKey(officerJson.getString("name"))) {
											JSONObject existJson = refMap.get(officerJson.getString("name"));
											if (existJson.has("officer_roles") && officerJson.has("officer_role")) {
												JSONArray existingRoles = existJson.getJSONArray("officer_roles");
												existingRoles.put(officerJson.getString("officer_role"));
											}
										} else {
											JSONArray roles = new JSONArray();
											if (officerJson.has("officer_role")) {
												roles.put(officerJson.getString("officer_role"));
												officerJson.put("officer_roles", roles);
											} else {
												officerJson.put("officer_roles", roles);
											}
											refMap.put(officerJson.getString("name"), officerJson);
											JSONObject jsonToSent = new JSONObject();
											jsonToSent.put("entity_id", identifier + "off" + count);
											jsonToSent.put("jurisdiction", officerJson.getString("jurisdiction"));
											jsonToSent.put("names", new JSONArray().put(officerJson.getString("name")));
											if(startDate!=null)
												jsonToSent.put("start_date", startDate);
											if(endDate!=null)
												jsonToSent.put("end_date", endDate);
											if (officerJson.has("type") && ("person"
													.equalsIgnoreCase(officerJson.getString("type"))
													|| "individual".equalsIgnoreCase(officerJson.getString("type"))))
												personEntities.put(jsonToSent);
											else if (!officerJson.has("type"))
												personEntities.put(jsonToSent);
											else
												orgEntities.put(jsonToSent);
										}
										referenceArray.getJSONObject(0).put("officership", refMap.values());
										map.put(identifier + "off" + count, officerJson);
										count = count + 1;
									}
								}
							}
						}
						officersreturn = new ArrayList<JSONObject>(refMap.values());
						if (elementRef != null) {
							JSONArray names = elementRef.names();
							if (names != null && names.length() > 0) {
								for (int l = 0; l < names.length(); l++) {
									JSONObject officerJson = elementRef.getJSONObject(names.getString(l));
									if (officerJson.has("address")) {
										String officerCountry = null;
										JSONObject address = null;
										if (officerJson.has("address")
												&& (officerJson.get("address") instanceof JSONObject))
											address = officerJson.getJSONObject("address");
										if (address != null && address.has("country")
												&& !"".equalsIgnoreCase(address.getString("country"))) {
											officerCountry = address.getString("country");
											for (int k = 0; k < countryJson.size(); k++) {
												org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson
														.get(k);
												if (countryObj.get("country").toString()
														.equalsIgnoreCase(officerCountry)) {
													officerJson.put("jurisdiction",
															countryObj.get("countryCode").toString());
													officerJson.put("country", officerCountry);
												}
											}
										} else {
											officerJson.put("jurisdiction", jurisdiction);
											officerJson.put("country", country);

										}
									} else {
										officerJson.put("jurisdiction", jurisdiction);
										officerJson.put("country", country);
									}
									officersreturn.add(officerJson);
									JSONObject jsonToSent = new JSONObject();
									jsonToSent.put("entity_id", identifier + "off" + count);
									if (officerJson.has("jurisdiction"))
										jsonToSent.put("jurisdiction", officerJson.getString("jurisdiction"));
									if (officerJson.has("name"))
										jsonToSent.put("names", new JSONArray().put(officerJson.getString("name")));
									if (startDate != null)
										jsonToSent.put("start_date", startDate);
									if (endDate != null)
										jsonToSent.put("end_date", endDate);
									if (officerJson.has("type")
											&& ("person".equalsIgnoreCase(officerJson.getString("type"))
													|| "individual".equalsIgnoreCase(officerJson.getString("type"))))
										personEntities.put(jsonToSent);
									else if (!officerJson.has("type"))
										personEntities.put(jsonToSent);
									else
										orgEntities.put(jsonToSent);
								}
							}
						}
						referenceArray.getJSONObject(0).put("officership", officersreturn);
						AdvanceSearchServiceImpl.writeFile(file, referenceArray);
						if(isScreeningRequired){
						if (personEntities.length() > 0)
							personJson.put("entities", personEntities);
						if (orgEntities.length() > 0)
							orgJson.put("entities", orgEntities);
						finalJsonTosent.put("max_responses", 10);
						// finalJsonTosent.put("min_confidence", 0.2);
						if (personJson.has("entities"))
							finalJsonTosent.put("person", personJson);
						if (orgJson.has("entities"))
							finalJsonTosent.put("organization", orgJson);
						/*File file1 = new File("/opt/corporatestructurepath/requestjsons/" + "off"+UUID.randomUUID().toString() + ".json");
						file1.createNewFile();
						FileWriter fileWriter = new FileWriter(file1);
						fileWriter.write(finalJsonTosent.toString());
						fileWriter.close();*/
						sendData(identifier, requestId, finalJsonTosent.toString());
						String screeningResponse = getData(identifier, requestId);
						if (screeningResponse != null) {
							JSONObject screeningJson = new JSONObject(screeningResponse);
							if (screeningJson.has("news")) {
								JSONObject newsJson = screeningJson.getJSONObject("news");
								if (newsJson.has("status")) {
									int counter = 1;
									while ("In Progress".equalsIgnoreCase(newsJson.getString("status"))) {
										screeningResponse = getData(identifier, requestId);
										if (screeningResponse != null) {
											try {
												screeningJson = new JSONObject(screeningResponse);
											} catch (Exception e) {
											}
										}
										boolean isNewsThere = false;
										if (screeningJson != null && screeningJson.has("news")) {
											newsJson = screeningJson.getJSONObject("news");
											isNewsThere = true;
										}
										if (isNewsThere) {
											for (int i = 0; i < officersreturn.size(); i++) {
												JSONObject officerJson = officersreturn.get(i);
												if (!officerJson.has("flag")) {
													if (newsJson.has(officerJson.getString("entity_id"))) {
														JSONObject entity = newsJson
																.getJSONObject(officerJson.getString("entity_id"));
														if (entity.has("classifications")) {
															officerJson.put("news",
																	entity.getJSONArray("classifications"));
															if (entity.has("status") && "done"
																	.equalsIgnoreCase(entity.getString("status")))
																officerJson.put("flag", true);
														}
													}
												}
											}
										} else {
											break;
										}
										referenceArray.getJSONObject(0).put("officership", officersreturn);
										counter++;
										if (counter == 6)
											break;
										Thread.sleep(50000);
									}
								}
							}
							if(!screeningJson.has("watchlists")){
								int temp=0;
								while(screeningJson.has("watchlists")){
									screeningResponse = getData(identifier, requestId);
									if(screeningResponse!=null){
										screeningJson = new JSONObject(screeningResponse);
									}
									temp = temp + 1;
									if (temp == 6)
										break;
									Thread.sleep(5000);
								}
							}
							JSONObject watchlistJson = new JSONObject();
							JSONObject watchlistPepJson = new JSONObject();
							JSONObject watchlistSanctionJson = new JSONObject();
							if (screeningJson.has("watchlists")) {
								JSONObject watchlists = screeningJson.getJSONObject("watchlists");
								if (watchlists.has("status")) {
									int chunksCounter = 1;
									while ((watchlists.has("status") && !"done".equalsIgnoreCase(watchlists.getString("status"))) 
											||(!watchlists.has("status")) ) {
										screeningResponse = getData(identifier, requestId);
										if (screeningResponse != null) {
											try {
												screeningJson = new JSONObject(screeningResponse);
											} catch (Exception e) {
											}
											if (screeningJson.has("watchlists")) {
												watchlists = screeningJson.getJSONObject("watchlists");

											}
										}
										chunksCounter = chunksCounter + 1;
										if (chunksCounter == 10)
											break;
										Thread.sleep(50000);

									}
								}
/*								if (watchlists.has("chunks")) {
									JSONArray chunks = watchlists.getJSONArray("chunks");
									if (chunks != null) {
										for (int i = 0; i < chunks.length(); i++) {
											JSONObject chunk = null;
											if (chunks.get(i) instanceof JSONObject)
												chunk = chunks.getJSONObject(i);
											if (chunk != null && chunk.has("_links")) {
												JSONObject links = chunk.getJSONObject("_links");
												String link = null;
												if (links.has("response")) {
													link = links.getString("response");
												}
												if (link != null) {
													HierarchyDto dto = new HierarchyDto();
													dto.setUrl(link);
													String responseString = advanceSearchService.getHierarchyData(dto,
															null,null);
													if (responseString != null) {
														JSONObject json = new JSONObject(responseString);
														if (json.has("response")) {
															JSONObject responseJson = json.getJSONObject("response");
															if(responseJson!=null && responseJson.has("pep")){
																JSONArray array=responseJson.getJSONArray("pep");
																if(array!=null){
																	for (int j = 0; j < array.length(); j++) {
																		watchlistPepJson.put(array.getJSONObject(j).getString("entity_id"),
																				array.getJSONObject(j));
																	}
																}
															}
															if(responseJson!=null && responseJson.has("sanction")){
																JSONArray sanctionArray=responseJson.getJSONArray("sanction");
																if(sanctionArray!=null){
																	for (int j = 0; j < sanctionArray.length(); j++) {
																		watchlistSanctionJson.put(sanctionArray.getJSONObject(j).getString("entity_id"),
																				sanctionArray.getJSONObject(j));
																	}
																}
															}
															JSONArray responseJson = json.getJSONArray("response");
															if (responseJson != null) {
																for (int j = 0; j < responseJson.length(); j++) {
																	JSONObject watchlist = responseJson
																			.getJSONObject(j);
																	watchlistJson.put(watchlist.getString("entity_id"),
																			watchlist);
																}
															}
														}
													}
												}
											}
										}
									}
								}*/
								if (watchlists.has("pep")) {
									JSONObject pepObject= watchlists.getJSONObject("pep");
									if(pepObject!=null) {
										JSONArray pepSources=pepObject.names();
										if(pepSources!=null && pepSources.length()>0) {
											for(int a=0;a<pepSources.length();a++) {
												JSONObject pepSourceObject=pepObject.getJSONObject(pepSources.getString(a));
												if(pepSourceObject.has("status")) {
													int chunksCounter = 1;
													while (!"done".equalsIgnoreCase(pepSourceObject.getString("status"))) {
														screeningResponse = getData(identifier, requestId);
														if (screeningResponse != null) {
															try {
																screeningJson = new JSONObject(screeningResponse);
															} catch (Exception e) {
															}
															if (screeningJson.has("watchlists")) {
																watchlists = screeningJson.getJSONObject("watchlists");
															}
														}
														chunksCounter = chunksCounter + 1;
														if (chunksCounter == 5)
															break;
														// Thread.sleep(10000);
													}
													if(pepSourceObject.has("chunks")) {

														JSONArray chunks = pepSourceObject.getJSONArray("chunks");
														if (chunks != null) {
															for (int i = 0; i < chunks.length(); i++) {
																JSONObject chunk = null;
																if (chunks.get(i) instanceof JSONObject)
																	chunk = chunks.getJSONObject(i);
																if (chunk != null && chunk.has("_links")) {
																	JSONObject links = chunk.getJSONObject("_links");
																	String link = null;
																	if (links.has("response")) {
																		link = links.getString("response");
																	}
																	if (link != null) {
																		HierarchyDto requestDto = new HierarchyDto();
																		requestDto.setUrl(link);
																		String responseNew = getHierarchyData(requestDto, null,null);
																		JSONObject json = null;
																		if (responseNew != null)
																			json = new JSONObject(responseNew);
																		if (json != null && json.has("response")) {
																			if(json.get("response") instanceof JSONArray) {
																				JSONArray responseArray=json.getJSONArray("response");
																				if(responseArray!=null && responseArray.length()>0) {
																					for(int b=0;b<responseArray.length();b++) {
																			JSONObject responseJson = responseArray.getJSONObject(b);
																				if(responseJson!=null){
																						watchlistPepJson.put(responseJson.getString("entity_id"),
																								responseJson);
																					
																				}
																			
																		
																			}
																				}
																		}
																			//
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
										
									}
								}
								//sanctions
								if (watchlists.has("sanction")) {
									JSONObject sanctionObject= watchlists.getJSONObject("sanction");
									if(sanctionObject!=null) {
										JSONArray sanctionSources=sanctionObject.names();
										if(sanctionSources!=null && sanctionSources.length()>0) {
											for(int a=0;a<sanctionSources.length();a++) {
												JSONObject sanctionSourceObject=sanctionObject.getJSONObject(sanctionSources.getString(a));
												if(sanctionSourceObject.has("status")) {
													int chunksCounter = 1;
													while (!"done".equalsIgnoreCase(sanctionSourceObject.getString("status"))) {
														screeningResponse = getData(identifier, requestId);
														if (screeningResponse != null) {
															try {
																screeningJson = new JSONObject(screeningResponse);
															} catch (Exception e) {
															}
															if (screeningJson.has("watchlists")) {
																watchlists = screeningJson.getJSONObject("watchlists");
															}
														}
														chunksCounter = chunksCounter + 1;
														if (chunksCounter == 5)
															break;
														// Thread.sleep(10000);
													}
													if(sanctionSourceObject.has("chunks")) {

														JSONArray chunks = sanctionSourceObject.getJSONArray("chunks");
														if (chunks != null) {
															for (int i = 0; i < chunks.length(); i++) {
																JSONObject chunk = null;
																if (chunks.get(i) instanceof JSONObject)
																	chunk = chunks.getJSONObject(i);
																if (chunk != null && chunk.has("_links")) {
																	JSONObject links = chunk.getJSONObject("_links");
																	String link = null;
																	if (links.has("response")) {
																		link = links.getString("response");
																	}
																	if (link != null) {
																		HierarchyDto hierarchyDtoNew = new HierarchyDto();
																		hierarchyDtoNew.setUrl(link);
																		String responseSanction = getHierarchyData(hierarchyDtoNew, null,null);
																		JSONObject json = null;
																		if (responseSanction != null)
																			json = new JSONObject(responseSanction);
																		if (json != null && json.has("response")) {
																			if(json.get("response") instanceof JSONArray) {
																				JSONArray responseArray=json.getJSONArray("response");
																				if(responseArray!=null && responseArray.length()>0) {
																					for(int b=0;b<responseArray.length();b++) {
																			JSONObject responseJson = responseArray.getJSONObject(b);
																				if(responseJson!=null){
																					watchlistSanctionJson.put(responseJson.getString("entity_id"),
																								responseJson);
																					
																				}
																			
																		
																			}
																				}
																		}
																			//
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
										
									}
								}
							}
							for (int i = 0; i < officersreturn.size(); i++) {
								JSONArray peps = new JSONArray();
								JSONArray sanctions = new JSONArray();
								JSONObject officerJson = officersreturn.get(i);
								String entityRefId = officerJson.getString("entity_id");
								preparePepAndSanction(watchlistPepJson, officerJson, entityRefId,true,identifier);
								preparePepAndSanction(watchlistSanctionJson, officerJson, entityRefId,false,identifier);
								/*if (watchlistJson.has(entityRefId)) {

									JSONObject object = watchlistJson.getJSONObject(entityRefId);
									if (object.has("hits")) {
										JSONArray hits = object.getJSONArray("hits");
										for (int j = 0; j < hits.length(); j++) {
											JSONArray pepsEntries = new JSONArray();
											JSONArray sanctionEntries = new JSONArray();
											JSONObject pepJson = new JSONObject();
											JSONObject sanctionJson = new JSONObject();
											JSONObject hit = hits.getJSONObject(j);
											if (hit.has("entries")) {
												JSONArray entries = hit.getJSONArray("entries");
												if (entries != null) {
													for (int k = 0; k < entries.length(); k++) {
														JSONObject entry = entries.getJSONObject(k);
														if (entry.has("watchlist_id")) {
															String watchlistId = entry.getString("watchlist_id");
															if (pepTypes.containsKey(watchlistId)) {
																String watchlistType = pepTypes.get(watchlistId)
																		.toString();
																if ("PEP".equalsIgnoreCase(watchlistType)) {
																	pepsEntries.put(entry);
																} else {
																	sanctionEntries.put(entry);
																}
															}
														}
													}
												}
												if (pepsEntries.length() > 0) {
													if (hit.has("value"))
														pepJson.put("value", hit.getString("value"));
													if (hit.has("confidence"))
														pepJson.put("confidence", hit.getDouble("confidence"));
													pepJson.put("entries", pepsEntries);
													peps.put(pepJson);
												}
												if (sanctionEntries.length() > 0) {
													if (hit.has("value"))
														sanctionJson.put("value", hit.getString("value"));
													if (hit.has("confidence"))
														sanctionJson.put("confidence", hit.getDouble("confidence"));
													sanctionJson.put("entries", sanctionEntries);
													sanctions.put(sanctionJson);
												}
											}
											// peps.put(pepJson);
											// sanctions.put(sanctionJson);
										}
									}
								}*/
								/*JSONArray mergedPeps=new JSONArray();
								JSONArray mergedSanctions=new JSONArray();
								advanceSearchService.getMergedResults(mergedPeps, peps);
								advanceSearchService.getMergedResults(mergedSanctions, sanctions);
								officerJson.put("pep", mergedPeps);
								officerJson.put("sanctions", mergedSanctions);*/
							}
						}
					}
					}
				}
			}
		}
		if (officersreturn.size() > 0)
			officersreturn.get(officersreturn.size() - 1).put("status", "done");
		referenceArray.getJSONObject(0).put("officership", officersreturn);
		AdvanceSearchServiceImpl.writeFile(file, referenceArray);

	}

	public String getHierarchyData(HierarchyDto hierarchyDto, Long userId,String apiKey) throws Exception {
		String response = null;
		if (hierarchyDto != null) {
			String url = hierarchyDto.getUrl();
			String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					if (userId == null) {
						return response;
					}
				} else {
					return response;
				}
			} else {
				return response;
			}
		} else {
			return response;
		}
		return response;
	}
	
	public String getMultisourceData(String query, String jurisdiction, String requestUrl) throws Exception {
		String url = requestUrl + "?query=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		if (jurisdiction != null)
			url = url + "&jurisdiction=" + URLEncoder.encode(jurisdiction, "UTF-8").replaceAll("\\+", "%20");
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}

	public String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level)
			throws UnsupportedEncodingException {

		String url = BIGDATA_MULTISOURCE_URL;
		String finalFields = null;
		if ("finance_info".equals(fields)) {
			finalFields = "overview";
		} else {
			finalFields = fields;
		}
		if (identifier != null)
			url = url + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		;
		if (graph != null)
			url = url + "/" + graph;
		if (level != null)
			url = url + "/" + level;
		if (finalFields != null)
			url = url + "?fields=" + finalFields;
		if (clientId != null)
			url = url + "&client_id=" + clientId;
		return url;

	}

	public String sendData(String identifier, String requestId, String jsonToBeSent) throws Exception {
		String url = SCREENING_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (requestId != null)
			url = url + "/" + requestId;
		String serverResponse[] = ServiceCallHelper.updateDataInServerWithApiKey(url, jsonToBeSent, apiKey);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}

	public String getData(String identifier, String requestId) throws Exception {
		String url = SCREENING_URL + "/" + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (requestId != null)
			url = url + "/" + requestId;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}
	
	private void preparePepAndSanction(JSONObject watchlistJson, JSONObject jsonObject, String entityRefId,
			boolean isPep,String identifier) {
		JSONArray watchlistEntityIds = watchlistJson.names();
		if (watchlistEntityIds != null) {
			if (watchlistJson.has(entityRefId)) {
				JSONObject object = watchlistJson.getJSONObject(entityRefId);
				JSONArray mergedResult = new JSONArray();
				if(object.has("hits"))
					advanceSearchService.getMergedResults(mergedResult, object.getJSONArray("hits"),identifier,isPep);
				if (jsonObject.has(entityRefId)) {
					if (isPep)
						jsonObject.getJSONObject(entityRefId).put("pep", mergedResult);
					else
						jsonObject.getJSONObject(entityRefId).put("sanctions", mergedResult);
					jsonObject.getJSONObject(entityRefId).put("pepScreening", true);
				} else {
					if (isPep)
						jsonObject.put(entityRefId, new JSONObject().put("pep", mergedResult));
					else
						jsonObject.put(entityRefId, new JSONObject().put("sanctions", mergedResult));
					jsonObject.getJSONObject(entityRefId).put("pepScreening", true);

				}

			}
		}
	}

}
