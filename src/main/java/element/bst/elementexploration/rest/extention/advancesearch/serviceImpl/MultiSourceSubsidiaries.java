package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
/**
 * 
 * @author Viswanath Reddy G
 *
 */
public class MultiSourceSubsidiaries extends Thread {

	private JSONArray refArray;

	private Integer maxlevel;

	private Integer topsubsidiaries;

	private Integer tempCount;

	private String companyName;

	private String rootJurisdiction;

	private String website;

	AdvanceSearchService advanceSearchService;

	private String BIGDATA_MULTISOURCE_URL;

	String SCREENING_URL;

	JSONArray subsidiarieArray = new JSONArray();

	ObjectMapper mapper = new ObjectMapper();

	Map<String, Integer> subsidiarieNames = new HashMap<String, Integer>();

	private String identifier;

	private ThreadPoolExecutor executor;

	private org.json.simple.JSONArray personTypes;

	private org.json.simple.JSONObject pepTypes;
	
	private String startDate;

	private String endDate;
	
	private String clientId;
	
	private String apiKey;

	public MultiSourceSubsidiaries(String identifier, JSONArray refArray, Integer topsubsidiaries, Integer maxlevel,
			Integer tempCount, String companyName, String rootJurisdiction, String website,
			AdvanceSearchService advanceSearchService, String BIGDATA_MULTISOURCE_URL, String SCREENING_URL,
			ThreadPoolExecutor executor, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,String startDate,String endDate,String clientId, String apiKey) {
		super();
		this.identifier = identifier;
		this.refArray = refArray;
		this.maxlevel = maxlevel;
		this.topsubsidiaries = topsubsidiaries;
		this.tempCount = tempCount;
		this.companyName = companyName;
		this.rootJurisdiction = rootJurisdiction;
		this.website = website;
		this.advanceSearchService = advanceSearchService;
		this.BIGDATA_MULTISOURCE_URL = BIGDATA_MULTISOURCE_URL;
		this.SCREENING_URL = SCREENING_URL;
		this.executor = executor;
		this.personTypes=personTypes;
		this.pepTypes=pepTypes;
		this.startDate=startDate;
		this.endDate=endDate;
		this.clientId=clientId;
		this.apiKey=apiKey;

	}

	@Override
	public void run() {
		try {
			getSubsidiaries();
		} catch (Exception e) {
			e.printStackTrace();
			if (subsidiarieArray.length() > 0)
				subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
			refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
		}
	}

	public void getSubsidiaries() throws Exception {
		JSONArray resultArray = new JSONArray();
		String mainEntity = null;
		try {
			if ("".equals(rootJurisdiction)) {
				rootJurisdiction = null;
			}
			int hits = 0;
			int searchCount = 0;
			if (tempCount != 0) {
				while (tempCount != 0 && hits <= 5 && resultArray.length() == 0) {
					mainEntity = getMultisourceData(companyName, rootJurisdiction, website, BIGDATA_MULTISOURCE_URL);
					if (mainEntity != null && tempCount != 0) {
						JSONObject mainJson = new JSONObject(mainEntity);
						if (mainJson.has("is-completed")) {
							while (mainJson.has("is-completed") && !mainJson.getBoolean("is-completed")) {
								mainEntity = getMultisourceData(companyName, rootJurisdiction, website,
										BIGDATA_MULTISOURCE_URL);
								if (mainEntity != null && tempCount != 0) {
									mainJson = new JSONObject(mainEntity);
								}
								searchCount = searchCount + 1;
								if (searchCount == 5)
									break;
								Thread.sleep(5000);
							}
						}
						if (mainJson.has("results")) {
							resultArray = mainJson.getJSONArray("results");
						}
					}
					hits = hits + 1;
				}
			}
		} catch (Exception e) {
			mainEntity = null;
		}
		if (tempCount == 0) {
			String encodeIdentifier = null;
			if (identifier != null) {
				encodeIdentifier = URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
			}
			String subsidiareLink = fetchLinkData(encodeIdentifier, "graph:subsidiaries", "graph", null,clientId);
			getLevelwiseSubsidiaries(subsidiareLink, refArray, encodeIdentifier);
		}
		if (mainEntity != null && tempCount != 0) {
			if (resultArray != null && resultArray.length() > 0) {
				JSONObject resultJson = resultArray.getJSONObject(0);
				String identifier = null;
				if (resultJson.has("identifier")) {
					identifier = resultJson.getString("identifier");
				}
				boolean isSubsidiariesThere=false;
				if (resultJson.has("links")) {
					JSONObject linkJson = resultJson.getJSONObject("links");
					if (linkJson.has("select-entity")) {
						String selectLink = linkJson.getString("select-entity");
						String selectResponse = getDataInAsynMode(selectLink);
						if (selectResponse != null) {
							JSONObject selectJson = new JSONObject(selectResponse);
							if (selectJson.has("results")) {
								JSONArray results = selectJson.getJSONArray("results");
								if (results != null && results.length() > 0) {
									JSONObject result = results.getJSONObject(0);
									if (result.has("links")) {
										JSONObject links = result.getJSONObject("links");
										if (links.has("graph:subsidiaries")) {
											isSubsidiariesThere = true;
										}
									}
								}
							}
						}
					} else {
						if (linkJson.has("graph:subsidiaries")) {
							isSubsidiariesThere = true;
						}
					}
				}
				
				String encodedId = null;
				if (identifier != null) {
					encodedId = URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
				}
				if(isSubsidiariesThere){
				String subsidiareLink = fetchLinkData(encodedId, "graph:subsidiaries", "graph", null,clientId);
				refArray.getJSONObject(tempCount).put("subsidiarieLink", subsidiareLink);
				getLevelwiseSubsidiaries(subsidiareLink, refArray, encodedId);
				}
			}

		} else {
			if (subsidiarieArray.length() > 0)
				subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
			refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
		}
		refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
	}

	public void getLevelwiseSubsidiaries(String url, JSONArray refArray, String encodedId) throws Exception {
		List<List<JSONObject>> eachLevelDataList = new ArrayList<>();
		JSONObject links = null;
		boolean flag = true;
		int count = 0;
		while (flag) {
			String serverResponse1[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
			if (serverResponse1[0] != null && Integer.parseInt(serverResponse1[0]) == 200) {
				String response1 = serverResponse1[1];
				if (!StringUtils.isEmpty(response1) && serverResponse1[1].trim().length() > 0) {
					JSONObject jsonResponse1 = new JSONObject(response1);
					if (jsonResponse1.has("message")) {
						String message = jsonResponse1.getString("message");
						if (message.contains("Field 'graph:subsidiaries' not found")
								|| message.contains("Data for field 'subsidiaries' is not ready")) {
							count = count + 1;
						}
						if (count == 6) {
							flag = false;
							refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
						}
					}
					if (jsonResponse1.has("_links")) {
						links = jsonResponse1.getJSONObject("_links");
						if (links.has("graph:subsidiaries")) {
							flag = false;
							JSONArray subsidiaries = links.getJSONArray("graph:subsidiaries");
							if (subsidiaries.length() >= maxlevel) {
								for (int i = 0; i < maxlevel; i++) {
									getLevelwiseData(subsidiarieArray, i, url, refArray, eachLevelDataList,
											subsidiaries, encodedId, links);
								}

							} else if (links.has("next")) {
								int counter = 0;
								for (int i = 0; i < subsidiaries.length(); i++) {
									counter = i;
									getLevelwiseData(subsidiarieArray, counter, url, refArray, eachLevelDataList,
											subsidiaries, encodedId, links);
								}
								while (counter < maxlevel) {
									boolean nextLevel = getNextLevel(url, counter, subsidiaries, links);
									if (nextLevel) {
										getLevelwiseData(subsidiarieArray, counter, url, refArray, eachLevelDataList,
												subsidiaries, encodedId, links);
										counter++;
									} else {
										if (subsidiarieArray.length() > 0)
											subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status",
													"done");
										refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
										counter++;
										break;
									}
								}
							} else {
								for (int i = 0; i < subsidiaries.length(); i++) {
									getLevelwiseData(subsidiarieArray, i, url, refArray, eachLevelDataList,
											subsidiaries, encodedId, links);
									// if(i==subsidiarieArray.length())
									// break;
								}
							}
						}
					}
				}

			}
			count = count + 1;
			if (count == 6) {
				flag = false;
				refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
			}
			Thread.sleep(10000);

		}
		if (subsidiarieArray.length() > 0)
			subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
		refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
	}

	private void getLevelwiseData(JSONArray subsidiarieArray, int level, String subsidiarieUrl, JSONArray refArray,
			List<List<JSONObject>> eachLevelDataList, JSONArray subsidiaries, String encodedId, JSONObject links)
			throws Exception {
		List<JSONObject> listForRef = new ArrayList<>();
		String href = fetchLinkData(encodedId, "graph:subsidiaries", "graph", level,clientId);
		//HierarchyDto hierarchyDto = new HierarchyDto();
		//hierarchyDto.setUrl(href);
		byte[] bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,apiKey);
		String json = null;
		if(bytes!=null)
			json = new String(bytes);
		//String json = advanceSearchService.getHierarchyData(hierarchyDto, null);
		JSONObject data = new JSONObject(json);
		while(data!=null && data.has("is-completed") && ! data.getBoolean("is-completed")) {
			bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, apiKey);
			if (bytes != null) {
				json = new String(bytes);
				data = new JSONObject(new String(bytes));
			}
			Thread.sleep(5000);
		}
		if (json != null) {
			JSONObject subsidiarieJson = new JSONObject(json);
			JSONObject bstData=new JSONObject();
			JSONObject clientData=new JSONObject();
			if(subsidiarieJson.has("BST")){
				bstData=subsidiarieJson.getJSONObject("BST");
			}
			if(subsidiarieJson.has(clientId)){
				clientData=subsidiarieJson.getJSONObject(clientId);
			}
			JSONArray subsidiarieIds = new JSONArray();
			JSONArray idsFromClient =clientData.names();
			subsidiarieIds = bstData.names();
			if (idsFromClient != null) {
			for (int i = 0; i < idsFromClient.length(); i++) {
				subsidiarieIds.put(idsFromClient.getString(i));
			}
			}
			if (subsidiarieIds != null) {
				for (int j = 0; j < subsidiarieIds.length(); j++) {
					String subsidiarieId = null;
					List<JSONObject> previousList = new ArrayList<>();
					if (eachLevelDataList.size() > 0) {
						previousList = eachLevelDataList.get(eachLevelDataList.size() - 1);
					} else {
						subsidiarieId = subsidiarieIds.getString(0);
					}
					String organisationName = null;
					JSONObject presentJson = new JSONObject();
					JSONObject clientJson = new JSONObject();
					JSONObject basicJson = new JSONObject();
					JSONObject subsidiaresJson = new JSONObject();
					for (JSONObject jsonObject : previousList) {
						JSONArray parentSubsidireis = jsonObject.getJSONArray("parentIds");
						for (int i = 0; i < parentSubsidireis.length(); i++) {
							if (parentSubsidireis.getString(i).equals(subsidiarieIds.getString(j))) {
								presentJson = bstData.getJSONObject(subsidiarieIds.getString(j));
								basicJson = presentJson.getJSONObject("basic");
								if (basicJson.has("vcard:organization-name")) {
									organisationName = basicJson.getString("vcard:organization-name");
								}
								if (organisationName != null
										&& !subsidiarieNames.containsKey(organisationName.toLowerCase())) {
									subsidiarieId = subsidiarieIds.getString(j);
									if (level == 0)
										subsidiaresJson.put("id", refArray.getJSONObject(tempCount).getString("id"));
									else
										subsidiaresJson.put("id", "c" + tempCount +level + j);
									subsidiaresJson.put("parents", new JSONArray().put(jsonObject.getString("id")));
									jsonObject.getJSONArray("childs").put(subsidiaresJson.getString("id"));
								}
								break;
							}
						}
					}
					if (subsidiarieId != null) {
						presentJson = bstData.getJSONObject(subsidiarieIds.getString(j));
						String entityType = null;
						String jurisdiction = null;
						/*if (presentJson.has("entity_type"))
							entityType = presentJson.getString("entity_type");*/
						if (presentJson.has("entity_type")) {
							if((presentJson.get("entity_type") instanceof String) ){
								entityType = presentJson.getString("entity_type");
							}else{
								entityType = "C";
							}
							if (entityType != null) {
								if("S".equalsIgnoreCase(entityType)){
									subsidiaresJson.put("news", new JSONArray());
									subsidiaresJson.put("screeningFlag", true);
								}
								if (personTypes.contains(entityType))
									entityType = "person";
								else
									entityType = "organization";
							}
						} else {
							entityType = "organization";
						}
						basicJson = presentJson.getJSONObject("basic");
						subsidiaresJson.put("basic",basicJson);
						String hasURL = null;
						String country = null;
						if (basicJson.has("vcard:organization-name")) {
							organisationName = basicJson.getString("vcard:organization-name");
						} else if (basicJson.has("vcard:hasName")) {
							organisationName = basicJson.getString("vcard:hasName");

						}
						if (basicJson.has("hasURL"))
							hasURL = basicJson.getString("hasURL");

						if (basicJson.has("mdaas:RegisteredAddress")) {
							JSONObject registeredAddress = basicJson.getJSONObject("mdaas:RegisteredAddress");
							if (registeredAddress.has("country")) {
								country = registeredAddress.getString("country");
							}
						}
						if (basicJson.has("isDomiciledIn"))
							jurisdiction = basicJson.getString("isDomiciledIn");
						subsidiaresJson.put("identifier", subsidiarieIds.getString(j));
						subsidiaresJson.put("name", organisationName);
						subsidiaresJson.put("entity_type", entityType);
						subsidiaresJson.put("hasURL", hasURL);
						if (jurisdiction != null && !"".equalsIgnoreCase(jurisdiction))
							subsidiaresJson.put("jurisdiction", jurisdiction);
						else
							subsidiaresJson.put("jurisdiction", rootJurisdiction);
						JSONArray parents = new JSONArray();
						if (presentJson.has("subsidiaries")) {
							JSONObject nextLevelSubsidiaries = presentJson.getJSONObject("subsidiaries");
							JSONArray ids = nextLevelSubsidiaries.names();
							int noOfSubsidiaries = 0;
							if (ids.length() < topsubsidiaries)
								noOfSubsidiaries = ids.length();
							else
								noOfSubsidiaries = topsubsidiaries;
							for (int i = 0; i < noOfSubsidiaries; i++) {
								parents.put(ids.getString(i));
							}
						}
						if (clientJson.has("subsidiaries")) {
							JSONObject nextLevelSubsidiaries = clientJson.getJSONObject("subsidiaries");
							JSONArray clientIds = nextLevelSubsidiaries.names();
							if(clientIds!=null && clientIds.length()>0){
							for (int i = 0; i < clientIds.length(); i++) {
								parents.put(clientIds.getString(i));
							}
							}
						}
						subsidiaresJson.put("parentIds", parents);
						subsidiaresJson.put("level", level);
						if (level == 0) {
							subsidiaresJson.put("entity_id", "orgChartmainEntity");
							subsidiaresJson.put("id", refArray.getJSONObject(tempCount).getString("id"));
						} else {
							subsidiaresJson.put("entity_id", "orgChartsubEntity");
						}
						subsidiaresJson.put("childs", new JSONArray());
						subsidiarieNames.put(organisationName.toLowerCase(), 1);
						listForRef.add(subsidiaresJson);
						subsidiaresJson.put("status", "pending");
						subsidiaresJson.put("isSubsidiarie", true);
						subsidiaresJson.put("bvdId", subsidiarieIds.getString(j));
						subsidiarieArray.put(subsidiaresJson);
					}
				}
				refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
				if (listForRef.size() > 0) {
					eachLevelDataList.add(listForRef);
					executor.submit(new SubsidiarieScreening(identifier, SCREENING_URL, eachLevelDataList, level,
							false, new AdvanceSearchServiceImpl(), null, personTypes, pepTypes,startDate,endDate,apiKey));
					subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "pending");
					try {
						if (((maxlevel - 1) == level)
								|| ((subsidiaries.length() - 1) == level && !(links.has("next")))) {
							for (int i = 0; i < subsidiarieArray.length(); i++) {
								while (!subsidiarieArray.getJSONObject(i).has("screeningFlag")) {
									subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status",
											"pending");
									refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
								}
							}
							subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
							refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
						} else {
							subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "pending");
							refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
						}
					} catch (Exception e) {
						e.printStackTrace();
						if (subsidiarieArray.length() > 0)
							subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
						refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
					}
				} else {
					for (int i = 0; i < subsidiarieArray.length(); i++) {
						while (!subsidiarieArray.getJSONObject(i).has("screeningFlag")) {
							subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "pending");
							refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
						}
					}
					subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
					refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
				}
				// }
				// }
			}
		}
		if (listForRef.isEmpty()) {
			if (subsidiarieArray.length() > 0) {
				for (int i = 0; i < subsidiarieArray.length(); i++) {
					while (!subsidiarieArray.getJSONObject(i).has("screeningFlag")) {
						subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "pending");
						refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
					}
				}
				subsidiarieArray.getJSONObject(subsidiarieArray.length() - 1).put("status", "done");
			}
			refArray.getJSONObject(tempCount).put("subsidiaries", subsidiarieArray);
		}
	}

	public boolean getNextLevel(String url, int currentLevel, JSONArray subsidiaries, JSONObject links)
			throws InterruptedException {
		boolean isReady = false;
		boolean flag = true;
		String levelVal = null;
		// JSONObject links = null;
		int count = 0;
		while (flag) {
			String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				String response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					JSONObject jsonResponse = new JSONObject(response);
					if (jsonResponse.has("message")) {
						String message = jsonResponse.getString("message");
						if (message.contains("Field 'graph:subsidiaries' not found")) {
							count = count + 1;
							if (count == 10) {
								isReady = false;
								flag = false;
							}
						}
					}
					if (jsonResponse.has("_links")) {
						links = jsonResponse.getJSONObject("_links");
						if (links.has("next")) {
							Map<String, String> query_pairs = new LinkedHashMap<String, String>();
							String nextLink = links.getString("next");
							String[] pairs = nextLink.split("&");
							for (String pair : pairs) {
								int idx = pair.indexOf("=");
								query_pairs.put((pair.substring(0, idx)), (pair.substring(idx + 1)));
							}
							levelVal = query_pairs.get("level");
							if (Integer.parseInt(levelVal) > (currentLevel + 1)) {
								subsidiaries = links.getJSONArray("graph:subsidiaries");
								isReady = true;
								flag = false;
							}
							count = count + 1;
							if (count == 10) {
								isReady = false;
								flag = false;
							}
						} else {
							subsidiaries = links.getJSONArray("graph:subsidiaries");
							if (subsidiaries.length() < (currentLevel + 1)) {
								isReady = false;
								flag = false;
							}
						}
						Thread.sleep(10000);
					}
					Thread.sleep(10000);

				}
			}
		}
		return isReady;
	}

	public String getMultisourceData(String query, String jurisdiction, String website, String requestUrl)
			throws Exception {
		String url = requestUrl + "?name=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		if (jurisdiction != null)
			url = url + "&jurisdiction=" + URLEncoder.encode(jurisdiction, "UTF-8").replaceAll("\\+", "%20");
		if (website != null)
			url = url + "&website=" + URLEncoder.encode(website, "UTF-8").replaceAll("\\+", "%20");
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url,apiKey);
		String response = null;
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return response;
			}
		} else {
			return response;
		}

	}

	public String fetchLinkData(String identifier, String fields, String graph, Integer level,String clientId) throws Exception {
		String url = BIGDATA_MULTISOURCE_URL;
		if (identifier != null)
			url = url + identifier;
		if (graph != null)
			url = url + "/" + graph;
		if (level != null)
			url = url + "/" + level;
		if (fields != null)
			url = url + "?fields=" + fields;
		if (clientId != null)
			url = url + "&client_id=" + clientId;
		return url;
	}
	
	public String getDataInAsynMode(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("is-completed")) {
					int counter = 0;
					while (json.has("is-completed") && !json.getBoolean("is-completed")) {
						serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
						if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
							response = serverResponse[1];
							if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
								json = new JSONObject(response);
							}
						}
						counter++;
						if (counter == 6)
							break;
						Thread.sleep(5000);
					}
				}
			} else {
				return response;
			}
		} else {
			return response;
		}
		return response;
	}
}
