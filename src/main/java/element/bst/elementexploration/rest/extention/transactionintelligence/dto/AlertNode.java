package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlertNode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date date;
	
	@JsonProperty("scenario_code")
	private String scenario;

	private String name;
	
	@JsonProperty("scenario_lookback")
	private Integer scenarioLookBack;

	private String oid;
	
	private String id;
	
	@JsonProperty("search-name")
	private String searchName;
	
	private String location;
	
	private String labelV;
	
	private Integer riskScore;
	
	private String merchant;
	private String merchantWebsite;
	private String beneficiaryName;	
	private String originatorName;	
	private Double amount;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getScenarioLookBack() {
		return scenarioLookBack;
	}

	public void setScenarioLookBack(Integer scenarioLookBack) {
		this.scenarioLookBack = scenarioLookBack;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLabelV() {
		return labelV;
	}

	public void setLabelV(String labelV) {
		this.labelV = labelV;
	}

	public Integer getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getOriginatorName() {
		return originatorName;
	}

	public void setOriginatorName(String originatorName) {
		this.originatorName = originatorName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}	
	
	
	
	

}
