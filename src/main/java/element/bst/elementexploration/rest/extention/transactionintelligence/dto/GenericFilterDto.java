package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

public class GenericFilterDto {
	

	private RequestFilterDto dto;
	
	private String entity;
	


	public RequestFilterDto getDto() {
		return dto;
	}

	public void setDto(RequestFilterDto dto) {
		this.dto = dto;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	


	
	

}
