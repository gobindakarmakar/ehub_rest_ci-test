package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;

/**
 * 
 * @author Viswanath
 *
 */
public interface GroupService extends GenericService<Group, Long> {

	Group getGroup(String name);

	List<Group> getUserGroups(Integer status, String keyword, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	public Long countUserGroups(Integer status, String keyword);

	boolean isActiveGroup(Long groupId);

	List<Group> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countGroupsOfUser(Long userId);
	
	Group checkGroupExist(String name, Long groupId);
	
	public Boolean doesUserbelongtoGroup(String groupName, Long userId);
}
