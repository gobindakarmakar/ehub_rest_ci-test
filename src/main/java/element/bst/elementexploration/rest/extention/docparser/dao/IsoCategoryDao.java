package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface IsoCategoryDao extends GenericDao<IsoCategory, Long> {
	
	public List<IsoCategory> getISOCategoriesByDocId(Long docId);

	public IsoCategory getISOCategoriesByCodeAndDocId(String level1Code, Long docId);
	
	
}
