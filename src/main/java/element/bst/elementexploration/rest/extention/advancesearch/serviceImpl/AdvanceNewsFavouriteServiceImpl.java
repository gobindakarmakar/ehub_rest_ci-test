package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.advancesearch.dao.AdvanceNewsFavouriteDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceNewsFavouriteService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("advanceNewsFavouriteService")
public class AdvanceNewsFavouriteServiceImpl extends GenericServiceImpl<AdvanceNewsFavourite, Long> implements AdvanceNewsFavouriteService {
	
	@Autowired
	private AdvanceNewsFavouriteDao advanceNewsFavouriteDao;
	

	@Autowired
	public AdvanceNewsFavouriteServiceImpl(GenericDao<AdvanceNewsFavourite, Long> genericDao) {
		super(genericDao);
		
	}

	@Override
	@Transactional("transactionManager")
	public AdvanceNewsFavourite saveAdvanceNewsFavourite(AdvanceNewsFavourite advanceNewsFavourite,Long userId) {
		//advanceNewsFavourite.setEntityName(advanceNewsFavourite.getEntityName().toLowerCase());
		advanceNewsFavourite.setUserId(userId);
		AdvanceNewsFavourite advanceNewsFavouriteReturn = advanceNewsFavouriteDao.create(advanceNewsFavourite);
		return advanceNewsFavouriteReturn;
	}

	@Override
	@Transactional("transactionManager")
	public int deleteAdvanceNewsFavourite(String entityId) {		
		int result = advanceNewsFavouriteDao.deleteAdvanceNewsFavourite(entityId);
		return result;		
	}

	

}
