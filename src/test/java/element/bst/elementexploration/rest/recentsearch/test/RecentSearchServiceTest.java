package element.bst.elementexploration.rest.recentsearch.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.recentsearch.domain.RecentSearchs;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;
import element.bst.elementexploration.rest.recentsearch.service.RecentSearchService;

public class RecentSearchServiceTest {

	@Mock
	private RecentSearchService recentSearchService;
	
	private RecentSearchs recentSearchs; 
	
	private List<RecentSearchDto> list;
	
	@Before
	public void setupMock() {
		recentSearchs=new RecentSearchs(1L, "microsoft", 1L, new Date());
		list=new ArrayList<>();
		RecentSearchDto recentSearchDto=new RecentSearchDto("search test", 1L, new Date());
		list.add(recentSearchDto);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void saveSearch(){
		Mockito.when(recentSearchService.saveSearch(Mockito.anyString(),Mockito.anyLong())).thenReturn(recentSearchs);
		RecentSearchs result=recentSearchService.saveSearch(Mockito.anyString(),Mockito.anyLong());
		assertEquals(recentSearchs.getId(), result.getId());
	}
	
	@Test
	public void getRecentSearch(){
		Mockito.when(recentSearchService.getRecentSearchs(Mockito.anyLong())).thenReturn(list);
		List<RecentSearchDto> result=recentSearchService.getRecentSearchs(Mockito.anyLong());
		assertEquals(list.size(), result.size());
	}
}
