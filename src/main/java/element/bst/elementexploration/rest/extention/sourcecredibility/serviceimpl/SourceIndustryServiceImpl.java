package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceIndustryDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceIndustryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceIndustryService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author suresh
 *
 */
@Service("sourceIndustryService")
public class SourceIndustryServiceImpl extends GenericServiceImpl<SourceIndustry, Long>
		implements SourceIndustryService {

	@Autowired
	public SourceIndustryServiceImpl(GenericDao<SourceIndustry, Long> genericDao) {
		super(genericDao);
	}

	public SourceIndustryServiceImpl() {
	}

	@Autowired
	SourceIndustryDao sourceIndustryDao;

	@Override
	@Transactional("transactionManager")
	public List<SourceIndustryDto> saveSourceIndustry(SourceIndustryDto industryDto) throws Exception {
		SourceIndustry industryFromDB = sourceIndustryDao.fetchIndustry(industryDto.getIndustryName());
		if (industryFromDB == null) {
			SourceIndustry industry = new SourceIndustry();
			industry.setIndustryName(industryDto.getIndustryName());
			sourceIndustryDao.create(industry);
			return getSourceIndustry();
		} else
			throw new Exception(ElementConstants.INDUSTRY_ADD_FAILED);
	}

	@Override
	@Transactional("transactionManager")
	public List<SourceIndustryDto> getSourceIndustry() {
		List<SourceIndustry> industryList = sourceIndustryDao.findAll();
		List<SourceIndustryDto> sourcesDtoList = new ArrayList<SourceIndustryDto>();
		if (industryList != null) {
			sourcesDtoList = industryList.stream()
					.map((sourcesDto) -> new EntityConverter<SourceIndustry, SourceIndustryDto>(SourceIndustry.class,
							SourceIndustryDto.class).toT2(sourcesDto, new SourceIndustryDto()))
					.collect(Collectors.toList());
		}
		return sourcesDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public SourceIndustry fetchIndustry(String industry) {
		return sourceIndustryDao.fetchIndustry(industry);
	}

}
