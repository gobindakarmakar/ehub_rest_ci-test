package element.bst.elementexploration.rest.extention.entity.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.entity.domain.EntityType;
import element.bst.elementexploration.rest.extention.entity.dto.EntityTypeDto;
import element.bst.elementexploration.rest.extention.entity.service.EntityTypeService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

@Service("entityTypeService")
@Transactional("transactionManager")
public class EntityTypeServiceImpl extends GenericServiceImpl<EntityType, Long> implements EntityTypeService {
	
	@Autowired
	EntityTypeService entityTypeService;

	@Autowired
	public EntityTypeServiceImpl(GenericDao<EntityType, Long> genericDao) {
		super(genericDao);
	}

	public EntityTypeServiceImpl() {

	}

	@Override
	public List<EntityTypeDto> getEntityRequirements() {
		List<EntityType> entityTypesList = entityTypeService.findAll();
		List<EntityTypeDto> entityDtosList = new ArrayList<EntityTypeDto>();
		if (entityTypesList != null) {
			entityDtosList = entityTypesList.stream().map(
					(entityDto) -> new EntityConverter<EntityType, EntityTypeDto>(EntityType.class, EntityTypeDto.class)
							.toT2(entityDto, new EntityTypeDto()))
					.collect(Collectors.toList());
		}
		return entityDtosList;
	}

}
