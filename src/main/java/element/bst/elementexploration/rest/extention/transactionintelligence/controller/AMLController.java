package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AmlAlertByPeriodResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AmlAlertByStatusResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AmlAlertsAggregationResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyGraphDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "AML API" },description="Manages AML transactions data")
@RestController
@RequestMapping(value = "/api/aml")
public class AMLController extends BaseController {
	@Autowired
	TxDataService txDataService;
	@Autowired
	AlertService alertService;

	/*@SuppressWarnings("unchecked")
	@GetMapping(value = "/amlAlertNotifications/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> amlAlertNotifications(@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String name, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) throws ParseException {
		List<AlertDashBoardRespDto> list = txDataService.getAlertsBetweenDates(null, null, name, pageNumber,
				recordsPerPage, orderIn);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = txDataService.getAlertCountBetweenDates(null, null, name);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("amlAlerts", list);
		jsonObject.put("paginationInformation", information);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}*/

	/*@SuppressWarnings("unchecked")
	@GetMapping(value = "/amlAlertAggregation/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> amlAlertAggregation(@RequestParam("token") String token) throws ParseException {
		List<TransactionAmountAndAlertCountDto> list = txDataService.amlAlertAggregation(null, null, null, null, null,
				false, false);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("amlAlertsAggregation", list);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}*/

	@ApiOperation("Gets list of aml top scenarios between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AmlAlertsAggregationResponseDto.class, message = "Operation successful.")})
	@PostMapping(value = "/amlTopScenarios/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> amlTopScenarios(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate,@RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		List<AlertScenarioDto> list = alertService.amlTopScenarios(fromDate,toDate,filterDto);
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("amlAlertsAggregation", list);*/
		AmlAlertsAggregationResponseDto amlAlertsAggregationResponseDto=new AmlAlertsAggregationResponseDto();
		amlAlertsAggregationResponseDto.setAmlAlertsAggregation(list);
		return new ResponseEntity<>(amlAlertsAggregationResponseDto, HttpStatus.OK);
	}

	@ApiOperation("Gets list of aml alerts by status between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AmlAlertByStatusResponseDto.class, message = "Operation successful.")})	
	@PostMapping(value = "/amlAlertByStatus/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> amlAlertByStatus(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate,
			@RequestParam("token") String token,
			@RequestParam(required = false) boolean periodBy,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		List<AlertStatusDto> list = alertService.fetchGroupByAlertStatus(fromDate, toDate, periodBy,filterDto);
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("amlAlertByStatus", list);*/
		AmlAlertByStatusResponseDto amlAlertByStatusResponseDto=new AmlAlertByStatusResponseDto();
		amlAlertByStatusResponseDto.setAmlAlertByStatus(list);
		return new ResponseEntity<>(amlAlertByStatusResponseDto, HttpStatus.OK);
	}

	@ApiOperation("Gets aml alerts by period")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AmlAlertByPeriodResponseDto.class, message = "Operation successful.")})
	@PostMapping(value = "/amlAlertByPeriod/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> amlAlertByPeriod(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate,@RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		AlertByPeriodDto list = alertService.amlAlertByPeriod(fromDate,toDate,filterDto);
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("amlAlertByStatus", list);*/
		AmlAlertByPeriodResponseDto amlAlertByPeriodResponseDto=new AmlAlertByPeriodResponseDto();
		amlAlertByPeriodResponseDto.setAmlAlertByStatus(list);
		return new ResponseEntity<>(amlAlertByPeriodResponseDto, HttpStatus.OK);
	}

	@ApiOperation("Gets aml top geography and transfers of transaction between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CounterPartyGraphDto.class, message = "Operation successful.")})
	@PostMapping(value = "/amlTopGeographyAndTransfers/{date-from}/{date-to}", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> counterPartyLocationsPlot(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate,@RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.counterPartyLocationsPlot(fromDate,toDate,null,filterDto), HttpStatus.OK);
	}
	@ApiOperation("Gets aml top customers between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = TopAmlCustomersDto.class,responseContainer = "List", message = "Operation successful.")})
	@PostMapping(value = "/amlTopCustomers/{date-from}/{date-to}", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> amlTopCustomers(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate,@RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.amlTopCustomers(fromDate,toDate,filterDto), HttpStatus.OK);
	}
	
	/*@GetMapping(value = "/amlTransferByMonth", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> amlTransferByMonth(@RequestParam("token") String token)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.amlTransferByMonth(), HttpStatus.OK);
	}*/
}
