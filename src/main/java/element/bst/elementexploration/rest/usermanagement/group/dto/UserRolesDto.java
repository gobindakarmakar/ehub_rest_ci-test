package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@ApiModel(value = "User Roles Dto")
public class UserRolesDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "User Role ID")
	private Long userRoleId;

	@ApiModelProperty(value = "Role ID",required=true)
	private Long roleId;

	@ApiModelProperty(value = "User Role Description")
	private String description;

	@ApiModelProperty(value = "Role ID",required=true)
	private Long userId;

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
