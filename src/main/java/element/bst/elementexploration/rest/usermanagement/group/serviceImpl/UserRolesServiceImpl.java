package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.app.service.exception.NotFoundException;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.activiti.app.conf.Bootstrapper;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserRolesDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDtoWithRolesObj;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.RoleGroupService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.UserStatus;

/**
 * @author Paul Jalagari
 *
 */
@Service("userRolesService")
@Transactional("transactionManager")
public class UserRolesServiceImpl extends GenericServiceImpl<UserRoles, Long> implements UserRolesService {

	@Autowired
	UserRolesDao userRolesDao;
	
	@Autowired
	RolesService rolesService;
	
	@Autowired
	UsersService usersService;
	
	@Autowired
	private AuditLogDao auditLogDao;
	
	@Autowired
	AuditLogService auditLogService;
	
	@Autowired
	ListItemService listItemService;
	
	@Autowired
	GroupsService groupsService;
	
	@Autowired
	RoleGroupService roleGroupService;
	
	@Autowired
	Bootstrapper bootstrapper;

	@Autowired
	public UserRolesServiceImpl(GenericDao<UserRoles, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public List<UserRoles> getRolesOfUser(Long userId) throws Exception {
		List<UserRoles> userRoles = userRolesDao.findAll();
				userRoles=userRoles.stream()
				.filter(role -> role.getUserId().getUserId().equals(userId)).collect(Collectors.toList());
		return userRoles;
	}
	
	@Override
	public List<UserRolesDto> getRolesOfUserDto(Long userId) throws Exception {
		List<UserRolesDto> userRolesDtoList= new ArrayList<>();
		List<UserRoles> userRoles = userRolesDao.findAll();
				userRoles=userRoles.stream()
				.filter(role -> role.getUserId().getUserId().equals(userId)).collect(Collectors.toList());
				userRoles.stream().forEach(ur -> Hibernate.initialize(ur.getRoleId()));
				if(userRoles!=null && userRoles.size()>0) {
					for(UserRoles userRole:userRoles) {
						UserRolesDto dto = new UserRolesDto();
						dto.setDescription(userRole.getDescription());
						dto.setRoleId(userRole.getRoleId().getRoleId());
						dto.setUserId(userRole.getUserId().getUserId());
						dto.setUserRoleId(userRole.getUserRoleId());
						userRolesDtoList.add(dto);
					}
				}
		return userRolesDtoList;
	}

	@Override
	public Map<Long, Long> rolesWithUserCount() {
		
		return userRolesDao.rolesWithUserCount();
	}
	
	@Override
	public boolean saveUserRoles(Users user, List<Long> roleIds,Long currentUserId,String action) throws Exception {
		if (user != null) {
			if (roleIds != null && roleIds.size() > 0) {
				for (Long eachRoleId : roleIds) {
					List<UserRoles> existingUserRoles = null;
					Roles existingRole = rolesService.find(eachRoleId);
					// adds role to new user
					if (existingRole != null) {
						List<UserRoles> allExistingUserRoles = getRolesOfUser(user.getUserId());
						if (allExistingUserRoles == null || allExistingUserRoles.size() == 0)
							existingUserRoles = allExistingUserRoles.stream()
									.filter(o -> o.getRoleId().getRoleId().equals(existingRole.getRoleId()))
									.collect(Collectors.toList());
						if (existingUserRoles == null || existingUserRoles.size() == 0) {
							UserRoles newUserRole = new UserRoles();
							if (existingRole.getDescription() != null)
								newUserRole.setDescription(existingRole.getDescription());
							if (existingRole.getRoleId() != null)
								newUserRole.setRoleId(existingRole);
							if (user.getUserId() != null)
								newUserRole.setUserId(user);
							saveOrUpdate(newUserRole);
							if (!action.equalsIgnoreCase(ElementConstants.USER_REGISTRATION_TYPE)) {
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
										ElementConstants.STRING_TYPE_USER_ROLE, null,
										ElementConstants.USER_MANAGEMENT_TYPE);
								Users admin = null;
								if (currentUserId != null){
									admin = usersService.find(currentUserId);
									admin = usersService.prepareUserFromFirebase(admin);
								}
								log.setDescription(admin.getFirstName() + " " + admin.getLastName() + " "
										+ ElementConstants.ROLE_ASSIGN_ACTION + " " + existingRole.getRoleName() + " "
										+ ElementConstants.TO_THE_USER + " " + user.getFirstName() + " "
										+ user.getLastName());
								log.setTypeId(user.getUserId());
								log.setUserId(admin.getUserId());
								log.setName(ElementConstants.ROLE_ASSIGN_ACTION);
								auditLogService.save(log);
							}
						}

						// adds role to existing user(role creator)
						/*List<UserRoles> existingUserRolesOfCreator = null;
						List<UserRoles> allExistingUserRolesOfCreator = getRolesOfUser(currentUserId);
						if (allExistingUserRolesOfCreator != null && allExistingUserRolesOfCreator.size() > 0)
							existingUserRolesOfCreator = allExistingUserRolesOfCreator.stream()
									.filter(o -> o.getRoleId().getRoleId().equals(existingRole.getRoleId()))
									.collect(Collectors.toList());
						Users admin = null;
						if (currentUserId != null)
							admin = usersService.find(currentUserId);
						if ((existingUserRolesOfCreator == null || existingUserRolesOfCreator.size() == 0) && admin != null) {

							UserRoles newUserRole = new UserRoles();
							if (existingRole.getDescription() != null)
								newUserRole.setDescription(existingRole.getDescription());
							if (existingRole.getRoleId() != null)
								newUserRole.setRoleId(existingRole);
							if (user.getUserId() != null)
								newUserRole.setUserId(admin);
							saveOrUpdate(newUserRole);
							if (!action.equalsIgnoreCase(ElementConstants.USER_REGISTRATION_TYPE)) {
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
										ElementConstants.STRING_TYPE_USER_ROLE, null,
										ElementConstants.USER_MANAGEMENT_TYPE);

								log.setDescription(admin.getFirstName() + " " + admin.getLastName() + " "
										+ ElementConstants.ROLE_ASSIGN_ACTION + " " + existingRole.getRoleName() + " "
										+ ElementConstants.TO_THE_USER + " " + admin.getFirstName() + " "
										+ admin.getLastName());
								log.setUserId(admin.getUserId());
								log.setTypeId(admin.getUserId());
								log.setName(ElementConstants.ROLE_ASSIGN_ACTION);
								auditLogService.save(log);
							}

						}*/
					}
				}
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean removeUserRoles(Users user, List<Long> roleIdsToBeRemoved, Long currentUserId,String action) throws Exception {
		boolean removeStatus = false;
		if (user != null) {
			if (roleIdsToBeRemoved != null && roleIdsToBeRemoved.size() > 0) {
				for (Long eachRoleId : roleIdsToBeRemoved) {
					
					Roles existingRole = rolesService.find(eachRoleId);
					if (existingRole != null) {
						List<UserRoles> allExistingUserRoles = getRolesOfUser(user.getUserId());
						if (allExistingUserRoles != null && allExistingUserRoles.size() > 0) {
							List<UserRoles> existingUserRoles = allExistingUserRoles.stream()
									.filter(o -> o.getRoleId().getRoleId().equals(existingRole.getRoleId()))
									.collect(Collectors.toList());
							if (existingUserRoles != null && existingUserRoles.size() != 0) {
								UserRoles roleToBeDeleted=existingUserRoles.get(0);
								if(null != existingUserRoles.get(0)){
									delete(existingUserRoles.get(0));
									removeStatus = true;
								}
								
								if (!action.equalsIgnoreCase(ElementConstants.USER_REGISTRATION_TYPE)) {
									AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
											ElementConstants.STRING_TYPE_USER_ROLE, null,
											ElementConstants.USER_MANAGEMENT_TYPE);
									Users admin = null;
									if (currentUserId != null){
										admin = usersService.find(currentUserId);
										admin = usersService.prepareUserFromFirebase(admin);
									}
									log.setDescription(admin.getFirstName() + " " + admin.getLastName()+" "
											+ ElementConstants.ROLE_UNASSIGN_ACTION + " " + existingRole.getRoleName()
											+ " " + ElementConstants.TO_THE_USER + " " + user.getFirstName() + " "
											+ user.getLastName());
									log.setTypeId(user.getUserId());
									log.setUserId(admin.getUserId());
									log.setName(ElementConstants.ROLE_UNASSIGN_ACTION);
									auditLogService.save(log);
								}
							}
						}
					}
				}
				return removeStatus;
			}
		}
		return removeStatus;
	}

	@Override
	public List<UserRolesDtoWithRolesObj> getRolesOfUserDtoWithRoleObj(Long userId,Boolean isGroupRolesRequired) throws Exception {
		List<UserRolesDtoWithRolesObj> userRolesDtoList= new ArrayList<>();
		
		if (isGroupRolesRequired) {
			List<Groups> groupsList = groupsService.getGroupsOfUser(userId);
			List<Long> groupIds = new ArrayList<>();
			if (groupsList != null && groupsList.size() > 0) {
				for (Groups eachGroup : groupsList) {
					groupIds.add(eachGroup.getId());
				}
			}
			
			if (groupIds != null && groupIds.size() > 0) {
				List<RoleGroup> roleGroupsList = roleGroupService.getRolesByGroup(groupIds);
				if (roleGroupsList != null && roleGroupsList.size() > 0) {
					for (RoleGroup eachRoleGroup : roleGroupsList) {
						UserRolesDtoWithRolesObj rolesObj = new UserRolesDtoWithRolesObj();
						RoleGroup eachRoleGroupData = new RoleGroup();
						BeanUtils.copyProperties(eachRoleGroup, eachRoleGroupData);
						rolesObj.setUserId(userId);																	
						rolesObj.setRoleId(eachRoleGroupData.getRoleId());												
						if(!isRoleAlreadyAdded(userRolesDtoList, rolesObj.getRoleId().getRoleId())){
							//setting Roles from RoleGroup not Removable
							rolesObj.getRoleId().setIsRemovable(false);
						    userRolesDtoList.add(rolesObj);							
						}
						
					}
				}
			}
		}
		
		List<UserRoles> userRoles = userRolesDao.findAll();
				userRoles=userRoles.stream()
				.filter(role -> role.getUserId().getUserId().equals(userId)).collect(Collectors.toList());
				userRoles.stream().forEach(ur -> Hibernate.initialize(ur.getRoleId()));
				if(userRoles!=null && userRoles.size()>0) {
					for(UserRoles userRole:userRoles) {
						UserRolesDtoWithRolesObj dto = new UserRolesDtoWithRolesObj();
						UserRoles userRolesData = new UserRoles();
						BeanUtils.copyProperties(userRole, userRolesData);						
						dto.setDescription(userRolesData.getDescription());																	
						dto.setRoleId(userRolesData.getRoleId());
						dto.setUserId(userRolesData.getUserId().getUserId());
						dto.setUserRoleId(userRolesData.getUserRoleId());
						
						if(!isRoleAlreadyAdded(userRolesDtoList, dto.getRoleId().getRoleId())){
							//setting Roles from UserRoles Removable	
							dto.getRoleId().setIsRemovable(true);
							userRolesDtoList.add(dto);							
					    }
						
					}
				}
		
		return userRolesDtoList;
	}

	@Override
	public Map<Long, Long> getActiveUserNRoleCount() throws Exception {
		ListItemDto status = listItemService.getListItemByListTypeAndDisplayName(ElementConstants.USER_STATUS_TYPE, UserStatus.Active.getStatus());
		Map<Long, Long> map = userRolesDao.getActiveUserNRoleCount(status.getListItemId());
		return map;
	}

	@Override
	@Transactional("transactionManager")
	public Object getStatuswiseUsersByRoleId(Long roleId) {
		
		return userRolesDao.getStatuswiseUsersByRoleId(roleId);
	}

	@Override
	@Transactional("transactionManager")
	public boolean unassignUserFromRole(Long roleId, Long userId, Long currentUserId) {
		boolean isDeleted = false;
		if(roleId != null && userId != null){
			UserRoles userRoles = userRolesDao.getUserRoleByRoleAndUserId(roleId,userId);
			if(userRoles != null){
				Roles roles = rolesService.find(roleId);
				userRolesDao.delete(userRoles);
				
				//Delete from Activity User table if removed from Admin Role
				if(roles.getRoleName().equals(ElementConstants.ROLE_NAME_ADMIN))
				{
					bootstrapper.deleteActivityUser(userRoles.getUserId().getScreenName());
				}
				
				isDeleted = true;
				
				Users deletedUser = usersService.find(userId);
				deletedUser = usersService.prepareUserFromFirebase(deletedUser);
				Users user = usersService.find(currentUserId);
				user = usersService.prepareUserFromFirebase(user);
				
				//AlertComments comment=alertCommentsService.find(commentId);
				AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ROLE_MANAGEMENT_TYPE, null, ElementConstants.ROLE_MANAGEMENT_TYPE);
				log.setDescription(user.getFirstName() + " " + user.getLastName() + " removed user - "+
						deletedUser.getFirstName() + " " + deletedUser.getLastName()+" from the Role ");
				log.setName(ElementConstants.ROLE_MANAGEMENT_TYPE);
				log.setUserId(currentUserId);
				log.setType(ElementConstants.ROLE_MANAGEMENT_TYPE);
				log.setTypeId(roleId);
				auditLogDao.create(log);
			}else{
				throw new NotFoundException("Role User Association not found!");
			}
		}else{
			isDeleted = false;
		}
		return isDeleted;
	}

	@Override
	@Transactional("transactionManager")
	public Map<Long, Long> getUsersCountByRole(Long roleId) {
		
		return userRolesDao.getUsersCountByRole(roleId);
	}

	@Override
	@Transactional("transactionManager")
	public boolean isUserRolesExist(Long userId, Long roleId) {
		
		return userRolesDao.isUserRolesExist(userId, roleId);
	}
	
	public boolean isRoleAlreadyAdded(List<UserRolesDtoWithRolesObj> userRolesDtoList, Long roleId)
	{
		// UserRolesDtoWithRolesObj
		boolean roleExist = false;
		for(UserRolesDtoWithRolesObj userRolesDto: userRolesDtoList){
			if(null != userRolesDto){
				if(null != userRolesDto.getRoleId() && null != userRolesDto.getRoleId().getRoleId()){
					if(userRolesDto.getRoleId().getRoleId().equals(roleId)){
						roleExist = true;
					}
				}
				
			}							
		}
		return roleExist;		
	}

}
