package element.bst.elementexploration.rest.extention.geoencoder.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;
import element.bst.elementexploration.rest.extention.geoencoder.service.CountryMasterDataService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("countryMasterDataService")
public class CountryMasterDataServiceImpl extends GenericServiceImpl<CountryMasterData, Long> implements CountryMasterDataService {

	@Autowired
	public CountryMasterDataServiceImpl(GenericDao<CountryMasterData, Long> genericDao) {
		super(genericDao);
	}

}
