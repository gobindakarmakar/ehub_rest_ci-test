package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.LuxuryShops;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface LuxuryShopsDao extends GenericDao<LuxuryShops, Long> {
	
	public void getAllLuxuryShops();
	
	public LuxuryShops getLuxuryShop(String shopName,Boolean shopType);
	
	public List<String> getLuxuryShopNames();
	
	public List<String> getLuxuryShopWebsites();
	

}
