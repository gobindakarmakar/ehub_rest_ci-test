package element.bst.elementexploration.rest.extention.datacuration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.datacuration.service.DataCurationService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Datacuration API" })
@Controller
@RequestMapping("/api/datacuration")
public class DataCurationController extends BaseController {

	@Autowired
	private DataCurationService dataCurationService;

	@ApiOperation("Gets nationalities data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/nationalities", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getNationalities(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getNationalities(), HttpStatus.OK);
	}

	@ApiOperation("Gets countries list taken from http://www.geonames.org/")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/countries", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCountries(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getCountries(), HttpStatus.OK);
	}

	@ApiOperation("Gets economic-sectors taken from https://en.wikipedia.org/wiki/Thomson_Reuters_Business_Classification")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/economic-sectors", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEcnomicSectors(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getEcnomicSectors(), HttpStatus.OK);
	}

	@ApiOperation("Gets business-sectors taken from https://en.wikipedia.org/wiki/Thomson_Reuters_Business_Classification")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/business-sectors", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getBusinessSectors(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getBusinessSectors(), HttpStatus.OK);
	}

	@ApiOperation("Gets industry-groups taken from https://en.wikipedia.org/wiki/Thomson_Reuters_Business_Classification")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/industry-groups", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getIndurstryGroups(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getIndurstryGroups(), HttpStatus.OK);
	}

	@ApiOperation("Gets industries taken from https://en.wikipedia.org/wiki/Thomson_Reuters_Business_Classification")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/industries", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getIndustries(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getIndustries(), HttpStatus.OK);
	}

	@ApiOperation("Gets activities taken from https://en.wikipedia.org/wiki/Thomson_Reuters_Business_Classification")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/activities", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getActivities(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getActivities(), HttpStatus.OK);
	}

	@ApiOperation("Gets directorship-roles taken from https://permid.org/")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/relation/directorship", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createDirectorshipRelation(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(dataCurationService.createDirectorshipRelation(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Gets officer-roles taken from https://permid.org/")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation successful"),

			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/relation/officership", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createOfficershipRelation(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(dataCurationService.createOfficershipRelation(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Gets officer-roles by ID taken from https://permid.org/")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/relation/officership/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOfficershipRelationById(@RequestParam String token,
			@PathVariable("identifier") String identifier) throws Exception {
		return new ResponseEntity<>(dataCurationService.getOfficershipRelationById(identifier), HttpStatus.OK);
	}

	@ApiOperation("Gets director-roles by ID taken from https://permid.org/")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/relation/directorship/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDirectorshipRelationById(@RequestParam String token,
			@PathVariable("identifier") String identifier) throws Exception {
		return new ResponseEntity<>(dataCurationService.getDirectorshipRelationById(identifier), HttpStatus.OK);
	}

	@ApiOperation("Gets director-roles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/director-roles", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDirectorRoles(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getDirectorRoles(), HttpStatus.OK);
	}

	@ApiOperation("Gets officer-roles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/meta/officer-roles", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOfficerRoles(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(dataCurationService.getOfficerRoles(), HttpStatus.OK);
	}

	@ApiOperation("searches for directorship relations by given filters")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/relation/directorship/search", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getDirectorshipRelationsBySearch(@RequestParam String token,
			@RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(dataCurationService.getDirectorshipRelationsBySearch(jsonString), HttpStatus.OK);
	}

	@ApiOperation("searches for officership relations by given filters")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/relation/officership/search", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getOfficershipRelationsBySearch(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(dataCurationService.getOfficershipRelationsBySearch(jsonString), HttpStatus.OK);
	}
}
