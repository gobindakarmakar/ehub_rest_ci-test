package element.bst.elementexploration.rest.extention.datacuration.service;

public interface DataCurationService {

	String getNationalities() throws Exception;

	String getCountries() throws Exception;

	String getEcnomicSectors() throws Exception;

	String getBusinessSectors() throws Exception;

	String getIndurstryGroups() throws Exception;

	String getIndustries() throws Exception;

	String getActivities() throws Exception;

	String createDirectorshipRelation(String jsonString) throws Exception;

	String createOfficershipRelation(String jsonString) throws Exception;

	String getOfficershipRelationById(String identifier) throws Exception;

	String getDirectorshipRelationById(String identifier) throws Exception;

	String getDirectorRoles() throws Exception;

	String getOfficerRoles() throws Exception;

	String getDirectorshipRelationsBySearch(String jsonString) throws Exception;

	String getOfficershipRelationsBySearch(String jsonString) throws Exception;

}
