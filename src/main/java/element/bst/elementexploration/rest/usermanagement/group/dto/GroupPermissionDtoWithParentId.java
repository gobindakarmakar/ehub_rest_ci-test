package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "GroupPermissionDtoWithParentId")
public class GroupPermissionDtoWithParentId implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Role Privilege ID")
	private Long groupPrivilegeId;

	@ApiModelProperty(value = "Privilege ID")
	private PermissionsDtoWithParent permissionId;

	@ApiModelProperty(value = "Privilege Level")
	private Integer permissionLevel;

	@ApiModelProperty(value = "Role ID")
	private Roles roleId;

	@JsonIgnore
	@ApiModelProperty(value = "GroupId")
	private Groups groupId;
	
	@ApiModelProperty(name = "isModifiable",required = true)
	private Boolean isModifiable=true;

	public PermissionsDtoWithParent getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(PermissionsDtoWithParent permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(Integer permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public Roles getRoleId() {
		return roleId;
	}

	public void setRoleId(Roles roleId) {
		this.roleId = roleId;
	}

	public Groups getGroupId() {
		return groupId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}

	public Long getGroupPrivilegeId() {
		return groupPrivilegeId;
	}

	public void setGroupPrivilegeId(Long groupPrivilegeId) {
		this.groupPrivilegeId = groupPrivilegeId;
	}

	public Boolean getIsModifiable() {
		return isModifiable;
	}

	public void setIsModifiable(Boolean isModifiable) {
		this.isModifiable = isModifiable;
	}

	
	
	
}
