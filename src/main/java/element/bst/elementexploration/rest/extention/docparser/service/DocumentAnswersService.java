package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentAnswerDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface DocumentAnswersService extends GenericService<DocumentAnswers, Long>{

	public DocumentAnswers saveNewDocumentAnswer(DocumentAnswerDto documentAnswers,Long userId) throws Exception;
	
	public int updateEvaluation(DocumentAnswers documentAnswers,String evaluation,Long userId);
	
	public int updateAnswerRanking(DocumentAnswers documentAnswers,Integer rank,Long userId);
	

	
}
