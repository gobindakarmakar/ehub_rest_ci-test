package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentStatementsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentStatements;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("documentStatementsDao")
public class DocumentStatementsDaoImpl extends GenericDaoImpl<DocumentStatements, Long> implements DocumentStatementsDao {
	
	public DocumentStatementsDaoImpl() {
		super(DocumentStatements.class);
	}

	public DocumentStatements getDocumentStatementById(Long id){
		DocumentStatements documentStatements=null;
		try{
		 documentStatements=(DocumentStatements) this.getCurrentSession()
				.createQuery("select ds from DocumentStatements ds,DocumentContents dc where ds.documentContent.id = dc.id and dc.id=" +id+" and ds.contentQuestion is not null").getSingleResult(); 
		 return documentStatements;
		}catch (Exception e) {
			return documentStatements;
		}
		
		
	}

}
