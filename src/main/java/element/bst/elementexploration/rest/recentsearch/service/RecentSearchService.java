package element.bst.elementexploration.rest.recentsearch.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.recentsearch.domain.RecentSearchs;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;
import element.bst.elementexploration.rest.recentsearch.dto.SearchDto;

public interface RecentSearchService extends GenericService<RecentSearchs, Long>{


	SearchDto getSearchs(String searchKeyword) throws Exception;

	RecentSearchs saveSearch(String search,Long userId);

	List<RecentSearchDto> getRecentSearchs( Long userId);

}
