package element.bst.elementexploration.rest.extention.questionnairebuilder.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.questionnairebuilder.dto.QuestionnaireDto;

/**
 * @author rambabu
 *
 */
public interface QuestionnaireBuilderService {
	
	public String getGroupsBySurveyId(String surveyId);
	
	public String getQuestionsBySurveyId(String surveyId);
	
	public String getQuestionPropertiesById(String questionId);

	public List<QuestionnaireDto> getActiveQuestionnaire();

}