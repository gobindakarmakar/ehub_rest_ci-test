package element.bst.elementexploration.rest.extention.docparser.service;

import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.dto.CoordinatesResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocParserSaveDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TemplatesRepsonseDto;
import element.bst.elementexploration.rest.generic.service.GenericService;
import net.sourceforge.tess4j.TesseractException;
/**
 * @author Rambabu
 *
 */
public interface DocumentTemplatesService extends GenericService<DocumentTemplates,Long>{
	
	public long saveNewPdfFillableTemplate(DocParserSaveDto responseDto, Long docIdUploaded, Long userId,Integer docFlag, byte[] byteArray) throws IllegalStateException, IOException, TesseractException, Exception ;
	public CoordinatesResponseDto getQuestionAndAnswerNewTemplate(CoordinatesResponseDto coordinatesDto, byte[] byteArrayFile) throws InvalidPasswordException, IOException, TesseractException;
	public List<TemplatesRepsonseDto> getTemplatesPagination(Integer recordsPerPage, Integer pageNumber,boolean isPagination, String searchKey);
	public boolean updateTemplateName(TemplatesRepsonseDto repsonseDto,Long userId);
	/*public String saveTemplateFile(DocumentVault documentVault,Long userIdTemp, HttpServletResponse response) throws DocNotFoundException, Exception;
	public boolean deleteTemplateFile(DocumentVault documentVault) throws DocNotFoundException, Exception;*/
	public boolean updateTemplateQuestions(OverAllResponseDto repsonseDto, Long tempId, Long currentUserId)  throws Exception ;
	boolean deleteTemplate(Long templateId,Long userId);
	public long getTemplates(boolean isPagination, String searchKey);	
	
}
