package element.bst.elementexploration.rest.util;

public enum UserStatus {
	Pending("Pending", "clock-o", ""), 
	Active("Active", "check-circle", "#3eb6ff"),
	Blocked("Blocked", "ban","#ef5350"),
	Suspended("Suspended", "pause-circle","#e6ae20"),
	Deactivated("Deactivated", "times-circle", "rgba(255, 255, 255, 0.54)");

	String status;

	String icon;

	String color;

	private UserStatus(String status, String icon, String color) {
		this.status = status;
		this.icon = icon;
		this.color = color;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
