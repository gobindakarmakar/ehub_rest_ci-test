package element.bst.elementexploration.rest.extention.transactionintelligence.enums;

/**
 * @author suresh
 *
 */
public enum AccountType {

	TDP, CCD, LNS, CUR ,GTD ,SAV, OTH
} 
