package element.bst.elementexploration.rest.recentsearch.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.recentsearch.domain.RecentSearchs;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;

public interface RecentSearchDao extends GenericDao<RecentSearchs, Long> {

	List<RecentSearchDto> getRecentSearchs(Long userId);

}
