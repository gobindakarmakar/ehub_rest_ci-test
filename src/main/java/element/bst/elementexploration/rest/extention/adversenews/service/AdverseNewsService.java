package element.bst.elementexploration.rest.extention.adversenews.service;

import java.io.File;

public interface AdverseNewsService {

	String getEntities() throws Exception;

	String postFiles(File[] files) throws Exception;

	String getSummary(String type, String name) throws Exception;

	String getGraphById(String type, String name) throws Exception;

	/*String getAdversenNewsById(String type, String id, Double offset, Double limit)
			throws UnsupportedEncodingException, Exception;*/

	//String getSearchResultList(String newsSearchRequest) throws Exception;

	String getAdverseProfileById(String identifier) throws Exception;

	String getSearchMultiResourcesGraph(String newsSearchRequest) throws Exception;

	String getSearchEventResultList(String newsSearchRequest)throws Exception;

	String getSearchEventAggregate(String newsSearchRequest)throws Exception;

}
