package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String country;
	
	@JsonProperty("iso2_code")
	private String iso2_code;
	
	@JsonProperty("iso3Code")
	private String iso3Code;
	
    private Double latitude;
    
    private Double longitude;
    
    private LocationRisk risk;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIso2_code() {
		return iso2_code;
	}

	public void setIso2_code(String iso2_code) {
		this.iso2_code = iso2_code;
	}

	public String getIso3Code() {
		return iso3Code;
	}

	public void setIso3Code(String iso3Code) {
		this.iso3Code = iso3Code;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public LocationRisk getRisk() {
		return risk;
	}

	public void setRisk(LocationRisk risk) {
		this.risk = risk;
	}
 
}
