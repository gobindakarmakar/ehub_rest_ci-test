package element.bst.elementexploration.rest.extention.alertmanagement.dao;

import element.bst.elementexploration.rest.extention.alertmanagement.domain.AlertComments;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Prateek
 *
 */

public interface AlertCommentsDao extends GenericDao<AlertComments, Long>{

}
