package element.bst.elementexploration.rest.extention.tuna.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.tuna.service.TunaService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * @author Paul Jalagari
 *
 */
@Service("tunaService")
public class TunaServiceImpl implements TunaService {

	@Value("${company_house_url}")
	private String COMPANY_HOUSE_URL;
	
	@Value("${bigdata_profile_url}")
	private String BIGDATA_PROFILE_URL;
	
	@Value("${bigdata_finance_url}")
	private String BIGDATA_FINANCE_URL;
	
	
	@Override
	public String getCompaniesHouse(String id) throws Exception {
		String idEncode = java.net.URLEncoder.encode(id, "UTF-8").replaceAll("\\+", "%20");
		String url = COMPANY_HOUSE_URL+"company/" +idEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithBasicAuth(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getFilingHistory(String company_number)
			 throws Exception {
		String numberEncode = java.net.URLEncoder.encode(company_number, "UTF-8").replaceAll("\\+", "%20");
		String url = COMPANY_HOUSE_URL+"company/" +numberEncode +"/filing-history";
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithBasicAuth(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getCompanyOfficers(String company_number) throws Exception {
		String numberEncode = java.net.URLEncoder.encode(company_number, "UTF-8").replaceAll("\\+", "%20");
		String url = COMPANY_HOUSE_URL+"company/" +numberEncode +"/officers";
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithBasicAuth(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String searchCompanies(String q) throws Exception {
		String numberEncode = java.net.URLEncoder.encode(q, "UTF-8").replaceAll("\\+", "%20");
		String url = COMPANY_HOUSE_URL +"search/companies?q="+numberEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithBasicAuth(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	/*@Override
	public String getOrgSanctions(String identifier) throws Exception {
		String identifierEncode = java.net.URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		String url = BIGDATA_PROFILE_URL + "profile/org/" + identifierEncode+"/watchlist";
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}*/

	@Override
	public String getInfoOps(String identifier,String orgOrPerson, String filter) throws Exception {
		/*String identifierEncode = java.net.URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		String orgOrPersonEncode = java.net.URLEncoder.encode(orgOrPerson, "UTF-8").replaceAll("\\+", "%20");*/
		String urlToSend = BIGDATA_PROFILE_URL + "profile/"+java.net.URLEncoder.encode(orgOrPerson, "UTF-8").replaceAll("\\+", "%20")+"/"+java.net.URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if(filter!=null){
		 urlToSend = urlToSend +"/"+java.net.URLEncoder.encode(filter, "UTF-8").replaceAll("\\+", "%20");}
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(urlToSend);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getFinanceStatement(String source, String query,String exchange, String apiKey) throws Exception {
		String sourceEncode = java.net.URLEncoder.encode(source, "UTF-8").replaceAll("\\+", "%20");
		String queryEncode = java.net.URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		String apiKeyEncode = java.net.URLEncoder.encode(apiKey, "UTF-8").replaceAll("\\+", "%20");
		String url = BIGDATA_FINANCE_URL + "api?source=" + sourceEncode+"&query="+queryEncode;
		if(exchange!=null)
			url=url+"&exchange="+java.net.URLEncoder.encode(exchange, "UTF-8").replaceAll("\\+", "%20");
		url=url+"&api_key="+apiKeyEncode;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getSignificantPersonsWithControl(String company_number) throws Exception {
		String comapanyNumberEncode = java.net.URLEncoder.encode(company_number, "UTF-8").replaceAll("\\+", "%20");
		String url = COMPANY_HOUSE_URL+"company/"+comapanyNumberEncode+"/persons-with-significant-control" ;
		String serverResponse[] =  ServiceCallHelper.getDataFromServerWithBasicAuth(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	

}
