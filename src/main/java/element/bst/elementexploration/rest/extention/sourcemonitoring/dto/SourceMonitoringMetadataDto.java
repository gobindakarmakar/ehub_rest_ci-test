package element.bst.elementexploration.rest.extention.sourcemonitoring.dto;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class SourceMonitoringMetadataDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String executionDuration;
	private Long resultCode;
	private String resultStatus;
	private String sourceProject;
	private String cycleId;
	private String sourceId;
	private String section;
	private String executionDatetime;
	private String id;
	private String responseDatetime;
	private String executionUrl;
	private String resultStacktrace;
	private String resultMessage;
	private boolean toBeAdded;
	
	private String tempDate;
	//source management keys
	private String sourceName;
	private String Category;
	private List<SourceJurisdiction> jurisdictions;
	private String url;
	private List<SourceDomain> domain;
	private List<SourceIndustry> industry;

	
	private ResultsValidityDto resultsValidityDto;

	public String getExecutionDuration() {
		return executionDuration;
	}

	public void setExecutionDuration(String executionDuration) {
		this.executionDuration = executionDuration;
	}

	public Long getResultCode() {
		return resultCode;
	}

	public void setResultCode(Long resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	public String getSourceProject() {
		return sourceProject;
	}

	public void setSourceProject(String sourceProject) {
		this.sourceProject = sourceProject;
	}

	public String getCycleId() {
		return cycleId;
	}

	public void setCycleId(String cycleId) {
		this.cycleId = cycleId;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getExecutionDatetime() {
		return executionDatetime;
	}

	public void setExecutionDatetime(String executionDatetime) {
		this.executionDatetime = executionDatetime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResponseDatetime() {
		return responseDatetime;
	}

	public void setResponseDatetime(String responseDatetime) {
		this.responseDatetime = responseDatetime;
	}

	public String getExecutionUrl() {
		return executionUrl;
	}

	public void setExecutionUrl(String executionUrl) {
		this.executionUrl = executionUrl;
	}

	public String getResultStacktrace() {
		return resultStacktrace;
	}

	public void setResultStacktrace(String resultStacktrace) {
		this.resultStacktrace = resultStacktrace;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public ResultsValidityDto getResultsValidityDto() {
		return resultsValidityDto;
	}

	public void setResultsValidityDto(ResultsValidityDto resultsValidityDto) {
		this.resultsValidityDto = resultsValidityDto;
	}

	
	public boolean isToBeAdded() {
		return toBeAdded;
	}

	public void setToBeAdded(boolean toBeAdded) {
		this.toBeAdded = toBeAdded;
	}

	
	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public List<SourceJurisdiction> getJurisdictions() {
		return jurisdictions;
	}

	public void setJurisdictions(List<SourceJurisdiction> jurisdictions) {
		this.jurisdictions = jurisdictions;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<SourceDomain> getDomain() {
		return domain;
	}

	public void setDomain(List<SourceDomain> domain) {
		this.domain = domain;
	}

	public List<SourceIndustry> getIndustry() {
		return industry;
	}

	public void setIndustry(List<SourceIndustry> industry) {
		this.industry = industry;
	}

	public String getTempDate() {
		return tempDate;
	}

	public void setTempDate(String tempDate) {
		this.tempDate = tempDate;
	}

	public SourceMonitoringMetadataDto(String executionDuration, Long resultCode, String resultStatus,
			String sourceProject, String cycleId, String sourceId, String section, String executionDatetime, String id,
			String responseDatetime, String executionUrl, String resultStacktrace, String resultMessage,
			ResultsValidityDto resultsValidityDto,boolean tobeAdded) {
		super();
		this.executionDuration = executionDuration;
		this.resultCode = resultCode;
		this.resultStatus = resultStatus;
		this.sourceProject = sourceProject;
		this.cycleId = cycleId;
		this.sourceId = sourceId;
		this.section = section;
		this.executionDatetime = executionDatetime;
		this.id = id;
		this.responseDatetime = responseDatetime;
		this.executionUrl = executionUrl;
		this.resultStacktrace = resultStacktrace;
		this.resultMessage = resultMessage;
		this.resultsValidityDto = resultsValidityDto;
		this.toBeAdded=tobeAdded;
	}

	public SourceMonitoringMetadataDto() {
		
	}
}
