package element.bst.elementexploration.rest.extention.menuitem.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ModuleFinalDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "ID of the Module")
	private Long moduleId;

	@ApiModelProperty(value = "Menu Item Module")
	private String moduleName;

	@ApiModelProperty(value = "Menu Item Module")
	private String moduleIcon;

	@ApiModelProperty(value = "Number of Menu Item Clicks")
	private Integer clicksCount;

	@ApiModelProperty(value = "Menu Item Size")
	private String menuItemSize;

	@ApiModelProperty(value = "Id of the user")
	private Long userId;

	@ApiModelProperty(value = "Status of the module")
	private Boolean disabled = false;
	
	@ApiModelProperty(value = "ID of module group")
	private Long moduleGroupId;

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleIcon() {
		return moduleIcon;
	}

	public void setModuleIcon(String moduleIcon) {
		this.moduleIcon = moduleIcon;
	}

	public Integer getClicksCount() {
		return clicksCount;
	}

	public void setClicksCount(Integer clicksCount) {
		this.clicksCount = clicksCount;
	}

	public String getMenuItemSize() {
		return menuItemSize;
	}

	public void setMenuItemSize(String menuItemSize) {
		this.menuItemSize = menuItemSize;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Long getModuleGroupId() {
		return moduleGroupId;
	}

	public void setModuleGroupId(Long moduleGroupId) {
		this.moduleGroupId = moduleGroupId;
	}

	
}
