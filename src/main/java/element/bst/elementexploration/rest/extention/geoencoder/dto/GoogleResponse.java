package element.bst.elementexploration.rest.extention.geoencoder.dto;

import java.io.Serializable;

public class GoogleResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String country;
	private String latitude;
	private String longitude;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
