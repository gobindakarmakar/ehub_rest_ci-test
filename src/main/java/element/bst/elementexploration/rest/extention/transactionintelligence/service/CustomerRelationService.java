package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerRelationShipDetails;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface CustomerRelationService  extends GenericService<CustomerRelationShipDetails,Long>{
	
	boolean fetchCustomerRelationDetails(MultipartFile multipartFile) throws IOException;

}
