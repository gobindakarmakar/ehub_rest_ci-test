package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;

/**
 * @author Paul Jalagari
 *
 */
public class GroupUsersListDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long groupId;

	private List<UsersDto> userList;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<UsersDto> getUserList() {
		return userList;
	}

	public void setUserList(List<UsersDto> userList) {
		this.userList = userList;
	}

}
