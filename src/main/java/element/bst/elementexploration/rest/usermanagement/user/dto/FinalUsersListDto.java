package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek Maurya
 */

public class FinalUsersListDto implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="Users list")
	private Object result;

	@ApiModelProperty("Page information")
	private PaginationInformation paginationInformation;

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}

	
}
