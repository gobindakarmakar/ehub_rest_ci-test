package element.bst.elementexploration.rest.extention.questionnairebuilder.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.questionnairebuilder.dto.QuestionnaireDto;
import element.bst.elementexploration.rest.extention.questionnairebuilder.service.QuestionnaireBuilderService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
/**
 * @author rambabu
 *
 */
@Service("questionnaireBuilderService")
public class QuestionnaireBuilderServiceImpl implements QuestionnaireBuilderService {
	
	@Value("${questionnaire_api_url}")
	private String questionnaireApiUrl;
	
	@Value("${questionnaire_user_name}")
	private String questionnaireUserName;
	
	@Value("${questionnaire_user_password}")
	private String questionnaireUserPassword;

	@Override
	public String getGroupsBySurveyId(String surveyId) {
		String response = null;
		if (surveyId != null) {
			String sessionKey = getSessionKey();
			if (sessionKey != null) {
				String groupsData = "{\"method\": \"list_groups\", \"params\": [ \"" + sessionKey + "\", \"" + surveyId
						+ "\" ], \"id\": 1}";
				String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(questionnaireApiUrl,
						groupsData);
				if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
					response = serverResponse[1];
					if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
						return response;
					}
					return response;
				}
				return response;
			} else {
				throw new InsufficientDataException("UserName or Password not valid for questionnaire builder");
			}

		} else {
			throw new InsufficientDataException("surveyId can not be empty");
		}
	}

	@Override
	public String getQuestionsBySurveyId(String surveyId) {
		String response = null;
		if (surveyId != null) {
			String sessionKey = getSessionKey();
			if (sessionKey != null) {
				String questionsData = "{\"method\": \"list_questions\", \"params\": [ \"" + sessionKey + "\", \"" + surveyId
						+ "\" ], \"id\": 1}";
				String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(questionnaireApiUrl,
						questionsData);
				if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
					response = serverResponse[1];
					if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
						return response;
					}
					return response;
				}
				return response;
			} else {
				throw new InsufficientDataException("UserName or Password not valid for questionnaire builder");
			}

		} else {
			throw new InsufficientDataException("surveyId can not be empty");
		}
	}
	
	@Override
	public String getQuestionPropertiesById(String questionId) {
		String response = null;
		if (questionId != null) {
			String sessionKey = getSessionKey();
			if (sessionKey != null) {
				String questionsData = "{\"method\": \"get_question_properties\", \"params\": [ \"" + sessionKey + "\", \"" + questionId
						+ "\" ], \"id\": 1}";
				String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(questionnaireApiUrl,
						questionsData);
				if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
					response = serverResponse[1];
					if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
						return response;
					}
					return response;
				}
				return response;
			} else {
				throw new InsufficientDataException("UserName or Password not valid for questionnaire builder");
			}

		} else {
			throw new InsufficientDataException("questionId can not be empty");
		}
	}
	
	public String getSessionKey(){
		String sessionKey = null;	
		String jsonToSent = "{\"method\": \"get_session_key\", \"params\": [\""+questionnaireUserName+"\", \""+questionnaireUserPassword+"\" ], \"id\": 1}";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(questionnaireApiUrl, jsonToSent);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				JSONObject json = new JSONObject(response);
				if(json.has("result")){
					sessionKey = json.getString("result");
			}
		}
	}
		return sessionKey;
	}

	@Override
	public List<QuestionnaireDto> getActiveQuestionnaire() {
		List<QuestionnaireDto> questionnaireDtoList = new ArrayList<QuestionnaireDto>();
		String response = getQuestionnaire();
		if (response != null) {
			JSONObject data = new JSONObject(response);
			if (data.has("result")) {
				JSONArray results = data.getJSONArray("result");
				for (int i = 0; i < results.length(); i++) {
					JSONObject questionnaire = results.getJSONObject(i);
					if (questionnaire.has("active")) {
						if (questionnaire.getString("active").equalsIgnoreCase("Y")) {
							QuestionnaireDto questionnaireDto = new QuestionnaireDto();
							questionnaireDto.setActive(questionnaire.getString("active"));
							if (questionnaire.has("sid"))
								questionnaireDto.setQuestionnaireId(questionnaire.getString("sid"));
							if (questionnaire.has("surveyls_title"))
								questionnaireDto.setQuestionnaireTitle(questionnaire.getString("surveyls_title"));
							questionnaireDtoList.add(questionnaireDto);
						}
					}
				}
			}

		}

		return questionnaireDtoList;
	}
	
	public String getQuestionnaire() {
		String response = null;
		
			String sessionKey = getSessionKey();
			if (sessionKey != null) {
				String questionsData = "{\"method\": \"list_surveys\", \"params\": [ \"" + sessionKey + "\" ], \"id\": 1}";
				String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(questionnaireApiUrl,
						questionsData);
				if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
					response = serverResponse[1];
					if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
						return response;
					}
					return response;
				}
				return response;
			} else {
				throw new InsufficientDataException("UserName or Password not valid for questionnaire builder");
			}
		
	}

}
