package element.bst.elementexploration.rest.graphdata.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.graphdata.service.GraphDataService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("graphDataService")
public class GraphDataServiceImpl implements GraphDataService{

	@Value("${bigdata_graph_data_url}")
	private String BIG_DATA_BASE_URL;
	
	@Value("${bigdata_entity_url}")
	private String BIG_DATA_ENTITY_URL;
	
	@Autowired
	private CaseDao caseDao;
	
	@Override
	@Transactional("transactionManager")
	public String getDataFromBigDataServer(Long caseId, Integer limit) throws Exception {
		Case caseSeed = caseDao.find(caseId);
		if (caseSeed != null && caseSeed.getCaseIdFromServer() != null) {
			String url = BIG_DATA_BASE_URL + "/" + caseSeed.getCaseIdFromServer() + "/graph/" + limit;
			String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
			if(serverResponse[0] != null &&  Integer.parseInt(serverResponse[0]) == 200){
				String response = serverResponse[1];
				if (!StringUtils.isEmpty(response) &&  serverResponse[1].trim().length() > 0) {
					return response;
				} else {
					throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
				}			
			}else{
				throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			}
		} else {
			throw new CaseSeedNotFoundException();
		}		
	}

	@Override
	@Transactional("transactionManager")
	public String getRiskDataByCaseId(Long caseId) throws Exception {
		Case caseSeed = caseDao.find(caseId);
		if (caseSeed != null && caseSeed.getCaseIdFromServer() != null) {
			String url = BIG_DATA_BASE_URL + "/" + caseSeed.getCaseIdFromServer() + "/risk";
			String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
			if(serverResponse[0] != null &&  Integer.parseInt(serverResponse[0]) == 200){
				String response = serverResponse[1];
				if (!StringUtils.isEmpty(response) &&  serverResponse[1].trim().length() > 0) {
					return response;
				} else {
					throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
				}			
			}else{
				throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			}
		} else {
			throw new CaseSeedNotFoundException();
		}		
	}

	@Override
	public String shortestPath(String vertexid1, String vertexid2) throws Exception {
		String url = BIG_DATA_BASE_URL + "/" + encodeURL(vertexid1) + "/" + "pathto" + "/" + encodeURL(vertexid2);
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if(serverResponse[0] != null &&  Integer.parseInt(serverResponse[0]) == 200){
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) &&  serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}			
		}else{
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String entityGraph(String type, String name, String limit) throws Exception {
		String url = BIG_DATA_ENTITY_URL + "/" + encodeURL(type) + "/" + encodeURL(name) + "/graph/" + limit;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if(serverResponse[0] != null &&  Integer.parseInt(serverResponse[0]) == 200){
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) &&  serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}			
		}else{
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String encodeURL(String url) throws UnsupportedEncodingException {
		return URLEncoder.encode(url, "UTF-8");
	}
}
