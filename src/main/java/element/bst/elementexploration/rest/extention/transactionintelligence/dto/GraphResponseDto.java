package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GraphResponseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Object> vertices = new ArrayList<>();
	
	private List<GraphEdge> edges = new ArrayList<>();

	public List<Object> getVertices() {
		return vertices;
	}

	public void setVertices(List<Object> vertices) {
		this.vertices = vertices;
	}

	public List<GraphEdge> getEdges() {
		return edges;
	}

	public void setEdges(List<GraphEdge> edges) {
		this.edges = edges;
	}
	
	
}
