package element.bst.elementexploration.rest.notification.daoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.notification.dao.NotificationMappingDao;
import element.bst.elementexploration.rest.notification.domain.NotificationMapping;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Viswanath
 *
 */
@Repository("notificationMappingDao")
public class NotificationMappingDaoImpl extends GenericDaoImpl<NotificationMapping, Long> implements NotificationMappingDao {

	public NotificationMappingDaoImpl() {
		super(NotificationMapping.class);
	}
	
	@Override
	public List<Long> addEventParticipants(Long eventId, List<Long> participants) {

		List<NotificationMapping> list = new ArrayList<>();
		List<Long> oldParticipants = new ArrayList<>();
		List<Long> finalList = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<NotificationMapping> query = builder.createQuery(NotificationMapping.class);
			Root<NotificationMapping> mapping = query.from(NotificationMapping.class);
			query.select(mapping);
			query.where(builder.equal(mapping.get("notifId"), eventId));
			list = (ArrayList<NotificationMapping>) this.getCurrentSession().createQuery(query).getResultList();
			oldParticipants = list.stream()
                    .map(NotificationMapping::getUserId).collect(Collectors.toList());
			for(Long id : participants){
				if (!oldParticipants.contains(id)){
					this.create(new NotificationMapping(eventId, id));
					finalList.add(id);
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to add participants. Reason : " + e.getMessage());
		}	
		return finalList;
	}
	
	@Override
	public void removeEventParticipants(Long eventId, List<Long> participants) {
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaDelete<NotificationMapping> query = builder.createCriteriaDelete(NotificationMapping.class);
			Root<NotificationMapping> mapping = query.from(NotificationMapping.class);
			query.where(builder.and(builder.equal(mapping.get("notifId"), eventId),
					mapping.get("userId").in(participants)));
			this.getCurrentSession().createQuery(query).executeUpdate();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to remove participants. Reason : " + e.getMessage());
		}
	}
	
	@Override
	public List<Long> getEventParticipants(Long eventId) {
		List<NotificationMapping> list = new ArrayList<>();
		List<Long> participants = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<NotificationMapping> query = builder.createQuery(NotificationMapping.class);
			Root<NotificationMapping> mapping = query.from(NotificationMapping.class);
			query.select(mapping);
			query.where(builder.equal(mapping.get("notifId"), eventId));
			list = (ArrayList<NotificationMapping>) this.getCurrentSession().createQuery(query).getResultList();
			participants = list.stream()
                    .map(NotificationMapping::getUserId).collect(Collectors.toList());
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to add participants. Reason : " + e.getMessage());
		} 	
		return participants;
	}

}
