package element.bst.elementexploration.rest.document.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Document")
public class DocumentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "DocId of the Document requested")
	private Long docId;

	@ApiModelProperty(value = "Remark of the notification requested", required = true)
	@NotEmpty(message = "Remark can not be empty")
	private String remark;

	@ApiModelProperty(value = "Title of the notification requested", required = true)
	@NotEmpty(message = "Title can not be empty")
	private String title;

	@ApiModelProperty(value = "Document Parsing Status of the Document requested")
	private String documentParsingStatus;

	@ApiModelProperty(value = "Document Structure Type of the Document requested")
	private Integer docStructureType;

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDocumentParsingStatus() {
		return documentParsingStatus;
	}

	public void setDocumentParsingStatus(String documentParsingStatus) {
		this.documentParsingStatus = documentParsingStatus;
	}

	public Integer getDocStructureType() {
		return docStructureType;
	}

	public void setDocStructureType(Integer docStructureType) {
		this.docStructureType = docStructureType;
	}

}
