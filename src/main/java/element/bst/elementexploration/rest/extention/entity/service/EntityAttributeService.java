package element.bst.elementexploration.rest.extention.entity.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.extention.entity.dto.EntityAttributeDto;
import element.bst.elementexploration.rest.generic.service.GenericService;
/**
 * 
 * @author Viswanath Reddy G
 *
 */
public interface EntityAttributeService extends GenericService<EntityAttributes, Long> {

	EntityAttributes saveEntityAttributes(EntityAttributeDto entityAttributes, Long userId);

	EntityAttributes getEntityAttributes(String entityId, String sourceSchema);

	List<EntityAttributes> getAttributeList(String entityId, String sourceSchema);

}
