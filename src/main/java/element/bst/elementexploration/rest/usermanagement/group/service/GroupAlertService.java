package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupAlertSettingDto;

/**
 * @author Paul Jalagari
 *
 */
public interface GroupAlertService extends GenericService<GroupAlert, Long> {

	List<GroupAlert> saveGroupAlertSetting(GroupAlertSettingDto groupAlertSettingDto, Long groupId, Long currentUserId);

	void removeExistingGroupAlertSettings(Long groupId,String type);

	List<GroupAlert> findGroupAlertsByGroup(Long groupId);
	
	void loadZeroGroupAlertSettings(Long groupId);

	List<GroupAlert> getGroupAlertsByGroupIdList(List<Long> groupIds);

}
