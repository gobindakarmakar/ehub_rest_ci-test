package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SubClassificationsDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SubClassificationsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("subClassificationsService")
public class SubClassificationsServiceImpl extends GenericServiceImpl<SubClassifications, Long>
		implements SubClassificationsService {

	@Autowired
	public SubClassificationsServiceImpl(GenericDao<SubClassifications, Long> genericDao) {
		super(genericDao);
	}

	public SubClassificationsServiceImpl() {
	}
	
	@Autowired
	SubClassificationsDao subClassifications;
	

	@Override
	@Transactional("transactionManager")
	public SubClassifications fetchSubClassification(String subClasssifcationName) {
		return subClassifications.fetchSubClassification(subClasssifcationName);
	}

}
