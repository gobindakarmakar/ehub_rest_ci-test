package element.bst.elementexploration.test.document.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.document.daoImpl.DocumentMappingDaoImpl;
import element.bst.elementexploration.rest.document.domain.DocumentVaultUserMapping;
import element.bst.elementexploration.rest.document.dto.DocumentUserMappingDTO;
import element.bst.elementexploration.rest.document.enumType.ReadWritePermissionEnum;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentMappingDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@InjectMocks
	DocumentMappingDaoImpl documentMappingDao;

	DocumentVaultUserMapping documentVaultUserMapping = new DocumentVaultUserMapping();

	List<DocumentUserMappingDTO> documentUserMappingDtoList = new ArrayList<DocumentUserMappingDTO>();

	@Before
	public void doSetup() {
		documentUserMappingDtoList
				.add(new DocumentUserMappingDTO(1L, "analyst", ReadWritePermissionEnum.WRITE, new Date()));
		documentUserMappingDtoList
				.add(new DocumentUserMappingDTO(1L, "analyst", ReadWritePermissionEnum.READ, new Date()));
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void isPermissionWriteTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(documentVaultUserMapping);
		boolean status = documentMappingDao.isPermissionWrite(1L, 1L);
		assertEquals(true, status);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void isPermissionWrite_whenNoResultExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		boolean status = documentMappingDao.isPermissionWrite(1L, 1L);
		assertEquals(false, status);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void isPermissionWrite_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		boolean status = documentMappingDao.isPermissionWrite(1L, 1L);
		assertEquals(false, status);

	}

	@Test
	public void isReadOrWritePermissionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(documentVaultUserMapping);
		boolean status = documentMappingDao.isReadOrWritePermission(1L, 1L);
		assertEquals(true, status);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void isReadOrWritePermission_whenNoResultExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		boolean status = documentMappingDao.isReadOrWritePermission(1L, 1L);
		assertEquals(false, status);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void isReadOrWritePermission_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		boolean status = documentMappingDao.isReadOrWritePermission(1L, 1L);
		assertEquals(false, status);

	}

	@Test
	public void getUserListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(documentUserMappingDtoList);
		List<DocumentUserMappingDTO> documentUserMappingDtoListNew = documentMappingDao.getUserList(1L);
		assertEquals(2, documentUserMappingDtoListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserList_WhenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		List<DocumentUserMappingDTO> documentUserMappingDtoListNew = documentMappingDao.getUserList(1L);
		assertEquals(0, documentUserMappingDtoListNew.size());
	}

}
