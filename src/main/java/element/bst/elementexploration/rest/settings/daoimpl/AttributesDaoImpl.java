package element.bst.elementexploration.rest.settings.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.dao.AttributesDao;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.settings.dto.AttributeDto;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("attributesDao")
public class AttributesDaoImpl extends GenericDaoImpl<Attributes, Long> implements AttributesDao {

	public AttributesDaoImpl() {
		super(Attributes.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Attributes> fetchSelectedAttributes(long id) {

		List<Attributes> attributes = null;
		try {
			attributes = this.getCurrentSession().createQuery("select a from Attributes a join a.settings s  where s.id ="+id+"").getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return attributes;
	}

	@Override
	public boolean addAttributes(AttributeDto attributeDto) {
		
		return false;
	}

}
