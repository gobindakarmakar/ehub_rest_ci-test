package element.bst.elementexploration.rest.contacts.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.contacts.service.ContactsService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Amit Patel
 * 
 */
@Api(tags = { "Contacts API" })
@RestController
@RequestMapping("/api/contacts")
public class ContactsController extends BaseController{

	@Autowired
	private ContactsService contactsService;

	@ApiOperation("Gets a users contacts list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserContactList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getContactsList(@RequestParam("token") String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {

		Long userId = getCurrentUserId();
		List<ContactDTO> contacts = contactsService.getUserContactList(userId, pageNumber, recordsPerPage, orderBy, orderIn, null);		
			
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = contactsService.countUserContactList(userId, null);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(contacts != null ? contacts.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		ResponseUtil resultantObject= new ResponseUtil();
		resultantObject.setResult(contacts);
		resultantObject.setPaginationInformation(information);
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", contacts);
		jsonObject.put("paginationInformation", information);*/
		return new ResponseEntity<>(resultantObject, HttpStatus.OK);				
	}

	@ApiOperation("Performs full text search on user's contacts list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/fullTextSearchUserContactList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fullTextSearchForContactList(@RequestParam("token") String token,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			@RequestParam String keyword, HttpServletRequest request) {
		
		if (keyword != null && keyword.length() < 3) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_STRING_LENTH_INVALID), HttpStatus.OK);
		}
		
		Long userId = getCurrentUserId();
		List<ContactDTO> contacts = contactsService.getUserContactList(userId, pageNumber, recordsPerPage, orderBy, orderIn, keyword);		

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = contactsService.countUserContactList(userId, keyword);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(contacts != null ? contacts.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		
		ResponseUtil resultantObject= new ResponseUtil();
		resultantObject.setResult(contacts);
		resultantObject.setPaginationInformation(information);
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", contacts);
		jsonObject.put("paginationInformation", information);*/
		return new ResponseEntity<>(resultantObject, HttpStatus.OK);				
	}

	@ApiOperation("Deletes a contact")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.REMOVE_CONTACT_SUCESSFUL_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.CONTACT_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@DeleteMapping(value = "/removeContact", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> removeContact(@RequestParam("token") String userToken,
			@RequestParam("contactId") Long contactId, HttpServletRequest request) {
		Long userId = getCurrentUserId();
		contactsService.removeContact(contactId, userId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.REMOVE_CONTACT_SUCESSFUL_MSG), HttpStatus.OK);			
	}

	@ApiOperation("Adds a contact")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.ADD_CONTACT_SUCESSFUL_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.CONTACT_ALREADY_EXISTS_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.CONTACT_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/addContact", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addContact(@RequestParam("token") String userToken,
			@RequestParam("contactId") Long contactId, HttpServletRequest request) {
		ResponseEntity<?> response = null;
		Long userId = getCurrentUserId();
		if (contactsService.addContact(contactId, userId)){
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.ADD_CONTACT_SUCESSFUL_MSG), HttpStatus.OK);	
		}else{
			response = new ResponseEntity<>(new ResponseMessage(ElementConstants.CONTACT_ALREADY_EXISTS_MSG), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
}
