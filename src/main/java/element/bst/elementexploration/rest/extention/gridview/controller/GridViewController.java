package element.bst.elementexploration.rest.extention.gridview.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.gridview.domain.GridView;
import element.bst.elementexploration.rest.extention.gridview.dto.GridViewDto;
import element.bst.elementexploration.rest.extention.gridview.service.GridviewService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Grid View API" }, description = "Provides Custom view creation for grids")
@RestController
@RequestMapping("/api/gridView")
public class GridViewController extends BaseController {

	@Autowired
	GridviewService gridviewService;

	@ApiOperation("Saves Grid View")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateGridView", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateGridView(HttpServletRequest request, @RequestParam String token,
			@Valid @RequestBody GridViewDto gridViewDto) throws Exception {
		Long userId = getCurrentUserId();
		if (gridViewDto != null) {
			if (!StringUtils.isBlank(gridViewDto.getGridTableName())) {
				if (gridViewDto.getGridViewId() == null) {
					if (StringUtils.isBlank(gridViewDto.getGridViewName()))
						return new ResponseEntity<>(new ResponseMessage("View name cannot be empty"), HttpStatus.OK);
					List<GridView> existingGridViews = gridviewService
							.findGridViewByNameAndUser(gridViewDto.getGridViewName(), gridViewDto.getGridTableName(),userId);
					if (existingGridViews != null && existingGridViews.size() > 0) {
						return new ResponseEntity<>(new ResponseMessage("View with this name already exists"),
								HttpStatus.OK);
					}
				}
				boolean status = gridviewService.saveOrUpdateGridView(gridViewDto, userId);
				if (status)
					return new ResponseEntity<>(status, HttpStatus.OK);
				else
					return new ResponseEntity<>(new ResponseMessage("Failed to save/update Grid View"), HttpStatus.OK);
			}
			return new ResponseEntity<>(new ResponseMessage("Table Name cannot be null"), HttpStatus.OK);
		}
		return new ResponseEntity<>(new ResponseMessage("View Object cannot be null"), HttpStatus.OK);
	}

	@ApiOperation("Gets All Grid Views")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllGridViewsByUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllGridViewsByUser(HttpServletRequest request, @RequestParam String token,
			@RequestParam String tableName) throws Exception {
		Long userId = getCurrentUserId();
		return new ResponseEntity<>(gridviewService.getAllGridViewsByUser(userId, tableName), HttpStatus.OK);
	}

	@ApiOperation("Deletes the grid view")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@DeleteMapping(value = "/deleteGridView", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteGridView(HttpServletRequest request, @RequestParam String token,
			@RequestParam Long gridViewId) throws Exception {
		Long userId = getCurrentUserId();
		List<GridView> gridViews = new ArrayList<>();
		if (gridViewId != null) {
			gridViews = gridviewService.findGridViewById(gridViewId);
			if (gridViews == null || gridViews.size() == 0)
				return new ResponseEntity<>(new ResponseMessage("View with thid id doesn't exist"), HttpStatus.OK);
			if (gridViews != null && gridViews.size() > 0) {
				boolean status = gridviewService.deleteGridView(gridViewId, userId);
				if (status)
					return new ResponseEntity<>(status, HttpStatus.OK);
				else
					return new ResponseEntity<>(new ResponseMessage("Failed to delete Grid View"), HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new ResponseMessage("View ID cannot be null"), HttpStatus.OK);
	}

}
