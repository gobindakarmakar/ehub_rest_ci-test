package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TransactionsDataDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String originatorAccountId;
	private String beneficiaryAccountId;
	private String transProductType;
	private String transactionChannel;
	private Double amount;
	private String currency;
	private Date businessDate;
	private String customText;
	private Long originatorCutomerId;
	private Long beneficiaryCutomerId;
	private String benficiaryBankName;
	private String beneficiaryName;
	private String originatorName;
	private String countryOfBeneficiary;
	private String countryOfTransaction;
	private String atmAddress;
	private String merchant;
	private String merchantWebsite;
	private String originatorCountry;
	private String beneficiaryCountry;
	private String originatorAccountCurrency;
	private String beneficiaryAccountCurrency;
	private String highalert;
	private String scenario;
	private String searchName;
	private String originatorAccountBankCountry;
	private String beneficiaryAccountBankCountry;
	private Double counterpartyTurnoverIncrement;
	private String transactionCountry;
	private String structureCounterpartyCustomer;
	private Double exportImportPercentage;
	private String transactionPurpose;
	private Double maximumAmount;
	private Double estimatedAverageTurnoverAmount;
	private Double estimatedTurnoverAmount;
	private String originatorAmlFraud;
	private String beneficiaryAmlFraud;
	private String originatorBusinesses;
	private String beneficiaryBusinesses;
	private String originatorSnit;
	private String beneficiarySnit;
	private String originatorWatchList;
	private String beneficiaryWatchList;
	private String originatorBusinessesActivityCountry;
	private String beneficiaryBusinessesActivityCountry;
	private String originatorSar;
	private String beneficiarySar;
	private String originatorProductType;
	private String beneficiaryProductType;
	private Double originatorAmlRisk;
	private Double beneficiaryAmlRisk;
	private String alertDescription;
	private String alertScenarioType;
	private Float originatorFatfRisk;
	private Float originatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex;
	private Float originatorInternationalNarcoticsControlRisk;
	private Float originatorTransparencyInternationalRisk;
	private Float originatorWorldBankRisk;
	private Float originatorFreedomHouseRisk;
	private Float originatorWorldJusticeRiskWorld_CriminalJusticeRiskRating;
	private Float originatorWefRisk;
	private Float originatorPoliticalRiskServicesInternationalCountryRisk_PRS;
	private Double originatorBaselAMLIndex2017;
	private Float beneficiaryFatfRisk;
	private Float beneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex;
	private Float beneficiaryInternationalNarcoticsControlRisk;
	private Float beneficiaryTransparencyInternationalRisk;
	private Float beneficiaryWorldBankRisk;
	private Float beneficiaryFreedomHouseRisk;
	private Float beneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating;
	private Float beneficiaryWefRisk;
	private Float beneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS;
	private Double beneficiaryBaselAMLIndex2017;

	private String transactionProductTypeAmountScenario;
	private String transactionProductTypeAmountAlertDescription;

	private Double CreditcardTransAverageIncrement;
	private String CreditcardTransAverageIncrementDescription;
	private String CreditcardTransAverageIncrementScenario;
	private String CreditcardTransAverageIncrementScenarioType;
	private int CreditcardTransAverageIncrementRiskScore;
	private String creditCardLuxuryTransaction;
	private String creditCardLuxuryTransactionDescription;
	private String creditCardLuxuryTransactionScenario;
	private String creditCardLuxuryTransactionScenarioType;
	private int creditCardLuxuryTransactionRiskScore;
	private String newCreditcardLuxuryCounterpartyTransaction;
	private String newCreditcardLuxuryCounterpartyTransactionDescription;
	private String newCreditcardLuxuryCounterpartyTransactionScenario;
	private String newCreditcardLuxuryCounterpartyTransactionScenarioType;
	private int newCreditcardLuxuryCounterpartyTransactionRiskScore;

	private String creditCardAtmWithdrawlsInHighRiskCountry;
	private String creditCardAtmWithdrawlsInHighRiskCountryScenario;
	private String creditCardAtmWithdrawlsInHighRiskCountryDescription;
	private String creditCardAtmWithdrawlsInHighRiskCountryScenarioType;
	private int creditCardAtmWithdrawlsInHighRiskCountryRiskScore;

	private String creditCardAtmTransactionRelationCountries;
	private String creditCardAtmTransactionInNewCountries;
	private String creditCardAtmWithdrwalsInShortTime;
	private Double creditCardAtmHighVolumeWithdrwal;
	private String creditCardAtmWithdrwalTransactionMoreThanOne;
	private String creditCardAtmWithdrwalCountryName;

	private String creditCardUnrelatedCounterPartiesTransactions;
	private Double creditCardUnrelatedCounterPartyPercentage;
	private String creditCardUnrelatedCounterPartiesScenario;
	private String creditCardUnrelatedCounterPartiesDescription;
	private String creditCardUnrelatedCounterPartiesScenarioType;
	private int creditCardUnrelatedCounterPartiesRiskScore;

	// Refund/overPayment
	private String creditCardMerchantRefund;
	private String creditCardNewMerchantRefund;
	private String creditCardMerchantRefundScenario;
	private String creditCardMerchantRefundDescription;
	private String creditCardMerchantRefundScenarioType;
	private Integer creditCardMerchantRefundRiskScore;
	private Double creditCardOverPaymentPercentage;

	/// old data
	private List<String> scenarioOfAlert;
	private List<String> descriptionOfAlert;
	private List<String> scenarioTypeOfAlert;
	private List<Integer> riskScoreOfAlert;
	private String addingList;
	

	public TransactionsDataDto() {
		super();

	}

	public TransactionsDataDto(Long id, String originatorAccountId, String beneficiaryAccountId,
			String transProductType, String transactionChannel, Double amount, String currency, Date businessDate,
			String customText, Long originatorCutomerId, Long beneficiaryCutomerId, String benficiaryBankName,
			String beneficiaryName, String originatorName, String countryOfBeneficiary, String countryOfTransaction,
			String atmAddress, String merchant, String merchantWebsite) {
		super();
		this.id = id;
		this.originatorAccountId = originatorAccountId;
		this.beneficiaryAccountId = beneficiaryAccountId;
		this.transProductType = transProductType;
		this.transactionChannel = transactionChannel;
		this.amount = amount;
		this.currency = currency;
		this.businessDate = businessDate;
		this.customText = customText;
		this.originatorCutomerId = originatorCutomerId;
		this.beneficiaryCutomerId = beneficiaryCutomerId;
		this.benficiaryBankName = benficiaryBankName;
		this.beneficiaryName = beneficiaryName;
		this.originatorName = originatorName;
		this.countryOfBeneficiary = countryOfBeneficiary;
		this.countryOfTransaction = countryOfTransaction;
		this.atmAddress = atmAddress;
		this.merchant = merchant;
		this.merchantWebsite = merchantWebsite;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginatorAccountId() {
		return originatorAccountId;
	}

	public void setOriginatorAccountId(String originatorAccountId) {
		this.originatorAccountId = originatorAccountId;
	}

	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	public String getTransProductType() {
		return transProductType;
	}

	public void setTransProductType(String transProductType) {
		this.transProductType = transProductType;
	}

	public String getTransactionChannel() {
		return transactionChannel;
	}

	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public String getCustomText() {
		return customText;
	}

	public void setCustomText(String customText) {
		this.customText = customText;
	}

	public Long getOriginatorCutomerId() {
		return originatorCutomerId;
	}

	public void setOriginatorCutomerId(Long originatorCutomerId) {
		this.originatorCutomerId = originatorCutomerId;
	}

	public Long getBeneficiaryCutomerId() {
		return beneficiaryCutomerId;
	}

	public void setBeneficiaryCutomerId(Long beneficiaryCutomerId) {
		this.beneficiaryCutomerId = beneficiaryCutomerId;
	}

	public String getBenficiaryBankName() {
		return benficiaryBankName;
	}

	public void setBenficiaryBankName(String benficiaryBankName) {
		this.benficiaryBankName = benficiaryBankName;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getOriginatorName() {
		return originatorName;
	}

	public void setOriginatorName(String originatorName) {
		this.originatorName = originatorName;
	}

	public String getCountryOfBeneficiary() {
		return countryOfBeneficiary;
	}

	public void setCountryOfBeneficiary(String countryOfBeneficiary) {
		this.countryOfBeneficiary = countryOfBeneficiary;
	}

	public String getCountryOfTransaction() {
		return countryOfTransaction;
	}

	public void setCountryOfTransaction(String countryOfTransaction) {
		this.countryOfTransaction = countryOfTransaction;
	}

	public String getAtmAddress() {
		return atmAddress;
	}

	public void setAtmAddress(String atmAddress) {
		this.atmAddress = atmAddress;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

	public String getOriginatorCountry() {
		return originatorCountry;
	}

	public void setOriginatorCountry(String originatorCountry) {
		this.originatorCountry = originatorCountry;
	}

	public String getBeneficiaryCountry() {
		return beneficiaryCountry;
	}

	public void setBeneficiaryCountry(String beneficiaryCountry) {
		this.beneficiaryCountry = beneficiaryCountry;
	}

	public String getOriginatorAccountCurrency() {
		return originatorAccountCurrency;
	}

	public void setOriginatorAccountCurrency(String originatorAccountCurrency) {
		this.originatorAccountCurrency = originatorAccountCurrency;
	}

	public String getBeneficiaryAccountCurrency() {
		return beneficiaryAccountCurrency;
	}

	public void setBeneficiaryAccountCurrency(String beneficiaryAccountCurrency) {
		this.beneficiaryAccountCurrency = beneficiaryAccountCurrency;
	}

	public String getHighalert() {
		return highalert;
	}

	public void setHighalert(String highalert) {
		this.highalert = highalert;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getOriginatorAccountBankCountry() {
		return originatorAccountBankCountry;
	}

	public void setOriginatorAccountBankCountry(String originatorAccountBankCountry) {
		this.originatorAccountBankCountry = originatorAccountBankCountry;
	}

	public String getBeneficiaryAccountBankCountry() {
		return beneficiaryAccountBankCountry;
	}

	public void setBeneficiaryAccountBankCountry(String beneficiaryAccountBankCountry) {
		this.beneficiaryAccountBankCountry = beneficiaryAccountBankCountry;
	}

	public Double getCounterpartyTurnoverIncrement() {
		return counterpartyTurnoverIncrement;
	}

	public void setCounterpartyTurnoverIncrement(Double counterpartyTurnoverIncrement) {
		this.counterpartyTurnoverIncrement = counterpartyTurnoverIncrement;
	}

	public String getTransactionCountry() {
		return transactionCountry;
	}

	public void setTransactionCountry(String transactionCountry) {
		this.transactionCountry = transactionCountry;
	}

	public String getStructureCounterpartyCustomer() {
		return structureCounterpartyCustomer;
	}

	public void setStructureCounterpartyCustomer(String structureCounterpartyCustomer) {
		this.structureCounterpartyCustomer = structureCounterpartyCustomer;
	}

	public Double getExportImportPercentage() {
		return exportImportPercentage;
	}

	public void setExportImportPercentage(Double exportImportPercentage) {
		this.exportImportPercentage = exportImportPercentage;
	}

	public String getTransactionPurpose() {
		return transactionPurpose;
	}

	public void setTransactionPurpose(String transactionPurpose) {
		this.transactionPurpose = transactionPurpose;
	}

	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

	public Double getEstimatedAverageTurnoverAmount() {
		return estimatedAverageTurnoverAmount;
	}

	public void setEstimatedAverageTurnoverAmount(Double estimatedAverageTurnoverAmount) {
		this.estimatedAverageTurnoverAmount = estimatedAverageTurnoverAmount;
	}

	public Double getEstimatedTurnoverAmount() {
		return estimatedTurnoverAmount;
	}

	public void setEstimatedTurnoverAmount(Double estimatedTurnoverAmount) {
		this.estimatedTurnoverAmount = estimatedTurnoverAmount;
	}

	public String getOriginatorAmlFraud() {
		return originatorAmlFraud;
	}

	public void setOriginatorAmlFraud(String originatorAmlFraud) {
		this.originatorAmlFraud = originatorAmlFraud;
	}

	public String getBeneficiaryAmlFraud() {
		return beneficiaryAmlFraud;
	}

	public void setBeneficiaryAmlFraud(String beneficiaryAmlFraud) {
		this.beneficiaryAmlFraud = beneficiaryAmlFraud;
	}

	public String getOriginatorBusinesses() {
		return originatorBusinesses;
	}

	public void setOriginatorBusinesses(String originatorBusinesses) {
		this.originatorBusinesses = originatorBusinesses;
	}

	public String getBeneficiaryBusinesses() {
		return beneficiaryBusinesses;
	}

	public void setBeneficiaryBusinesses(String beneficiaryBusinesses) {
		this.beneficiaryBusinesses = beneficiaryBusinesses;
	}

	public String getOriginatorSnit() {
		return originatorSnit;
	}

	public void setOriginatorSnit(String originatorSnit) {
		this.originatorSnit = originatorSnit;
	}

	public String getBeneficiarySnit() {
		return beneficiarySnit;
	}

	public void setBeneficiarySnit(String beneficiarySnit) {
		this.beneficiarySnit = beneficiarySnit;
	}

	public String getOriginatorWatchList() {
		return originatorWatchList;
	}

	public void setOriginatorWatchList(String originatorWatchList) {
		this.originatorWatchList = originatorWatchList;
	}

	public String getBeneficiaryWatchList() {
		return beneficiaryWatchList;
	}

	public void setBeneficiaryWatchList(String beneficiaryWatchList) {
		this.beneficiaryWatchList = beneficiaryWatchList;
	}

	public String getOriginatorBusinessesActivityCountry() {
		return originatorBusinessesActivityCountry;
	}

	public void setOriginatorBusinessesActivityCountry(String originatorBusinessesActivityCountry) {
		this.originatorBusinessesActivityCountry = originatorBusinessesActivityCountry;
	}

	public String getBeneficiaryBusinessesActivityCountry() {
		return beneficiaryBusinessesActivityCountry;
	}

	public void setBeneficiaryBusinessesActivityCountry(String beneficiaryBusinessesActivityCountry) {
		this.beneficiaryBusinessesActivityCountry = beneficiaryBusinessesActivityCountry;
	}

	public String getOriginatorSar() {
		return originatorSar;
	}

	public void setOriginatorSar(String originatorSar) {
		this.originatorSar = originatorSar;
	}

	public String getBeneficiarySar() {
		return beneficiarySar;
	}

	public void setBeneficiarySar(String beneficiarySar) {
		this.beneficiarySar = beneficiarySar;
	}

	public String getOriginatorProductType() {
		return originatorProductType;
	}

	public void setOriginatorProductType(String originatorProductType) {
		this.originatorProductType = originatorProductType;
	}

	public String getBeneficiaryProductType() {
		return beneficiaryProductType;
	}

	public void setBeneficiaryProductType(String beneficiaryProductType) {
		this.beneficiaryProductType = beneficiaryProductType;
	}

	public Double getOriginatorAmlRisk() {
		return originatorAmlRisk;
	}

	public void setOriginatorAmlRisk(Double originatorAmlRisk) {
		this.originatorAmlRisk = originatorAmlRisk;
	}

	public Double getBeneficiaryAmlRisk() {
		return beneficiaryAmlRisk;
	}

	public void setBeneficiaryAmlRisk(Double beneficiaryAmlRisk) {
		this.beneficiaryAmlRisk = beneficiaryAmlRisk;
	}

	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public String getAlertScenarioType() {
		return alertScenarioType;
	}

	public void setAlertScenarioType(String alertScenarioType) {
		this.alertScenarioType = alertScenarioType;
	}

	public Float getOriginatorFatfRisk() {
		return originatorFatfRisk;
	}

	public void setOriginatorFatfRisk(Float originatorFatfRisk) {
		this.originatorFatfRisk = originatorFatfRisk;
	}

	public Float getOriginatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex() {
		return originatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex;
	}

	public void setOriginatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex(
			Float originatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex) {
		this.originatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex = originatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex;
	}

	public Float getOriginatorInternationalNarcoticsControlRisk() {
		return originatorInternationalNarcoticsControlRisk;
	}

	public void setOriginatorInternationalNarcoticsControlRisk(Float originatorInternationalNarcoticsControlRisk) {
		this.originatorInternationalNarcoticsControlRisk = originatorInternationalNarcoticsControlRisk;
	}

	public Float getOriginatorTransparencyInternationalRisk() {
		return originatorTransparencyInternationalRisk;
	}

	public void setOriginatorTransparencyInternationalRisk(Float originatorTransparencyInternationalRisk) {
		this.originatorTransparencyInternationalRisk = originatorTransparencyInternationalRisk;
	}

	public Float getOriginatorWorldBankRisk() {
		return originatorWorldBankRisk;
	}

	public void setOriginatorWorldBankRisk(Float originatorWorldBankRisk) {
		this.originatorWorldBankRisk = originatorWorldBankRisk;
	}

	public Float getOriginatorFreedomHouseRisk() {
		return originatorFreedomHouseRisk;
	}

	public void setOriginatorFreedomHouseRisk(Float originatorFreedomHouseRisk) {
		this.originatorFreedomHouseRisk = originatorFreedomHouseRisk;
	}

	public Float getOriginatorWorldJusticeRiskWorld_CriminalJusticeRiskRating() {
		return originatorWorldJusticeRiskWorld_CriminalJusticeRiskRating;
	}

	public void setOriginatorWorldJusticeRiskWorld_CriminalJusticeRiskRating(
			Float originatorWorldJusticeRiskWorld_CriminalJusticeRiskRating) {
		this.originatorWorldJusticeRiskWorld_CriminalJusticeRiskRating = originatorWorldJusticeRiskWorld_CriminalJusticeRiskRating;
	}

	public Float getOriginatorWefRisk() {
		return originatorWefRisk;
	}

	public void setOriginatorWefRisk(Float originatorWefRisk) {
		this.originatorWefRisk = originatorWefRisk;
	}

	public Float getOriginatorPoliticalRiskServicesInternationalCountryRisk_PRS() {
		return originatorPoliticalRiskServicesInternationalCountryRisk_PRS;
	}

	public void setOriginatorPoliticalRiskServicesInternationalCountryRisk_PRS(
			Float originatorPoliticalRiskServicesInternationalCountryRisk_PRS) {
		this.originatorPoliticalRiskServicesInternationalCountryRisk_PRS = originatorPoliticalRiskServicesInternationalCountryRisk_PRS;
	}

	public Double getOriginatorBaselAMLIndex2017() {
		return originatorBaselAMLIndex2017;
	}

	public void setOriginatorBaselAMLIndex2017(Double originatorBaselAMLIndex2017) {
		this.originatorBaselAMLIndex2017 = originatorBaselAMLIndex2017;
	}

	public Float getBeneficiaryFatfRisk() {
		return beneficiaryFatfRisk;
	}

	public void setBeneficiaryFatfRisk(Float beneficiaryFatfRisk) {
		this.beneficiaryFatfRisk = beneficiaryFatfRisk;
	}

	public Float getBeneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex() {
		return beneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex;
	}

	public void setBeneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex(
			Float beneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex) {
		this.beneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex = beneficiaryTaskJusticeNetworkHighRisk_FinancialSecracyIndex;
	}

	public Float getBeneficiaryInternationalNarcoticsControlRisk() {
		return beneficiaryInternationalNarcoticsControlRisk;
	}

	public void setBeneficiaryInternationalNarcoticsControlRisk(Float beneficiaryInternationalNarcoticsControlRisk) {
		this.beneficiaryInternationalNarcoticsControlRisk = beneficiaryInternationalNarcoticsControlRisk;
	}

	public Float getBeneficiaryTransparencyInternationalRisk() {
		return beneficiaryTransparencyInternationalRisk;
	}

	public void setBeneficiaryTransparencyInternationalRisk(Float beneficiaryTransparencyInternationalRisk) {
		this.beneficiaryTransparencyInternationalRisk = beneficiaryTransparencyInternationalRisk;
	}

	public Float getBeneficiaryWorldBankRisk() {
		return beneficiaryWorldBankRisk;
	}

	public void setBeneficiaryWorldBankRisk(Float beneficiaryWorldBankRisk) {
		this.beneficiaryWorldBankRisk = beneficiaryWorldBankRisk;
	}

	public Float getBeneficiaryFreedomHouseRisk() {
		return beneficiaryFreedomHouseRisk;
	}

	public void setBeneficiaryFreedomHouseRisk(Float beneficiaryFreedomHouseRisk) {
		this.beneficiaryFreedomHouseRisk = beneficiaryFreedomHouseRisk;
	}

	public Float getBeneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating() {
		return beneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating;
	}

	public void setBeneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating(
			Float beneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating) {
		this.beneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating = beneficiaryWorldJusticeRiskWorld_CriminalJusticeRiskRating;
	}

	public Float getBeneficiaryWefRisk() {
		return beneficiaryWefRisk;
	}

	public void setBeneficiaryWefRisk(Float beneficiaryWefRisk) {
		this.beneficiaryWefRisk = beneficiaryWefRisk;
	}

	public Float getBeneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS() {
		return beneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS;
	}

	public void setBeneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS(
			Float beneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS) {
		this.beneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS = beneficiaryPoliticalRiskServicesInternationalCountryRisk_PRS;
	}

	public Double getBeneficiaryBaselAMLIndex2017() {
		return beneficiaryBaselAMLIndex2017;
	}

	public void setBeneficiaryBaselAMLIndex2017(Double beneficiaryBaselAMLIndex2017) {
		this.beneficiaryBaselAMLIndex2017 = beneficiaryBaselAMLIndex2017;
	}

	public Double getCreditcardTransAverageIncrement() {
		return CreditcardTransAverageIncrement;
	}

	public void setCreditcardTransAverageIncrement(Double creditcardTransAverageIncrement) {
		CreditcardTransAverageIncrement = creditcardTransAverageIncrement;
	}

	public String getCreditcardTransAverageIncrementDescription() {
		return CreditcardTransAverageIncrementDescription;
	}

	public void setCreditcardTransAverageIncrementDescription(String creditcardTransAverageIncrementDescription) {
		CreditcardTransAverageIncrementDescription = creditcardTransAverageIncrementDescription;
	}

	public String getCreditcardTransAverageIncrementScenario() {
		return CreditcardTransAverageIncrementScenario;
	}

	public void setCreditcardTransAverageIncrementScenario(String creditcardTransAverageIncrementScenario) {
		CreditcardTransAverageIncrementScenario = creditcardTransAverageIncrementScenario;
	}

	public String getCreditcardTransAverageIncrementScenarioType() {
		return CreditcardTransAverageIncrementScenarioType;
	}

	public void setCreditcardTransAverageIncrementScenarioType(String creditcardTransAverageIncrementScenarioType) {
		CreditcardTransAverageIncrementScenarioType = creditcardTransAverageIncrementScenarioType;
	}

	public int getCreditcardTransAverageIncrementRiskScore() {
		return CreditcardTransAverageIncrementRiskScore;
	}

	public void setCreditcardTransAverageIncrementRiskScore(int creditcardTransAverageIncrementRiskScore) {
		CreditcardTransAverageIncrementRiskScore = creditcardTransAverageIncrementRiskScore;
	}

	public String getCreditCardLuxuryTransaction() {
		return creditCardLuxuryTransaction;
	}

	public void setCreditCardLuxuryTransaction(String creditCardLuxuryTransaction) {
		this.creditCardLuxuryTransaction = creditCardLuxuryTransaction;
	}

	public String getCreditCardLuxuryTransactionDescription() {
		return creditCardLuxuryTransactionDescription;
	}

	public void setCreditCardLuxuryTransactionDescription(String creditCardLuxuryTransactionDescription) {
		this.creditCardLuxuryTransactionDescription = creditCardLuxuryTransactionDescription;
	}

	public String getCreditCardLuxuryTransactionScenario() {
		return creditCardLuxuryTransactionScenario;
	}

	public void setCreditCardLuxuryTransactionScenario(String creditCardLuxuryTransactionScenario) {
		this.creditCardLuxuryTransactionScenario = creditCardLuxuryTransactionScenario;
	}

	public String getCreditCardLuxuryTransactionScenarioType() {
		return creditCardLuxuryTransactionScenarioType;
	}

	public void setCreditCardLuxuryTransactionScenarioType(String creditCardLuxuryTransactionScenarioType) {
		this.creditCardLuxuryTransactionScenarioType = creditCardLuxuryTransactionScenarioType;
	}

	public int getCreditCardLuxuryTransactionRiskScore() {
		return creditCardLuxuryTransactionRiskScore;
	}

	public void setCreditCardLuxuryTransactionRiskScore(int creditCardLuxuryTransactionRiskScore) {
		this.creditCardLuxuryTransactionRiskScore = creditCardLuxuryTransactionRiskScore;
	}

	public String getNewCreditcardLuxuryCounterpartyTransaction() {
		return newCreditcardLuxuryCounterpartyTransaction;
	}

	public void setNewCreditcardLuxuryCounterpartyTransaction(String newCreditcardLuxuryCounterpartyTransaction) {
		this.newCreditcardLuxuryCounterpartyTransaction = newCreditcardLuxuryCounterpartyTransaction;
	}

	public String getNewCreditcardLuxuryCounterpartyTransactionDescription() {
		return newCreditcardLuxuryCounterpartyTransactionDescription;
	}

	public void setNewCreditcardLuxuryCounterpartyTransactionDescription(
			String newCreditcardLuxuryCounterpartyTransactionDescription) {
		this.newCreditcardLuxuryCounterpartyTransactionDescription = newCreditcardLuxuryCounterpartyTransactionDescription;
	}

	public String getNewCreditcardLuxuryCounterpartyTransactionScenario() {
		return newCreditcardLuxuryCounterpartyTransactionScenario;
	}

	public void setNewCreditcardLuxuryCounterpartyTransactionScenario(
			String newCreditcardLuxuryCounterpartyTransactionScenario) {
		this.newCreditcardLuxuryCounterpartyTransactionScenario = newCreditcardLuxuryCounterpartyTransactionScenario;
	}

	public String getNewCreditcardLuxuryCounterpartyTransactionScenarioType() {
		return newCreditcardLuxuryCounterpartyTransactionScenarioType;
	}

	public void setNewCreditcardLuxuryCounterpartyTransactionScenarioType(
			String newCreditcardLuxuryCounterpartyTransactionScenarioType) {
		this.newCreditcardLuxuryCounterpartyTransactionScenarioType = newCreditcardLuxuryCounterpartyTransactionScenarioType;
	}

	public int getNewCreditcardLuxuryCounterpartyTransactionRiskScore() {
		return newCreditcardLuxuryCounterpartyTransactionRiskScore;
	}

	public void setNewCreditcardLuxuryCounterpartyTransactionRiskScore(
			int newCreditcardLuxuryCounterpartyTransactionRiskScore) {
		this.newCreditcardLuxuryCounterpartyTransactionRiskScore = newCreditcardLuxuryCounterpartyTransactionRiskScore;
	}

	public String getCreditCardAtmWithdrawlsInHighRiskCountry() {
		return creditCardAtmWithdrawlsInHighRiskCountry;
	}

	public void setCreditCardAtmWithdrawlsInHighRiskCountry(String creditCardAtmWithdrawlsInHighRiskCountry) {
		this.creditCardAtmWithdrawlsInHighRiskCountry = creditCardAtmWithdrawlsInHighRiskCountry;
	}

	public String getCreditCardAtmWithdrawlsInHighRiskCountryScenario() {
		return creditCardAtmWithdrawlsInHighRiskCountryScenario;
	}

	public void setCreditCardAtmWithdrawlsInHighRiskCountryScenario(
			String creditCardAtmWithdrawlsInHighRiskCountryScenario) {
		this.creditCardAtmWithdrawlsInHighRiskCountryScenario = creditCardAtmWithdrawlsInHighRiskCountryScenario;
	}

	public String getCreditCardAtmWithdrawlsInHighRiskCountryDescription() {
		return creditCardAtmWithdrawlsInHighRiskCountryDescription;
	}

	public void setCreditCardAtmWithdrawlsInHighRiskCountryDescription(
			String creditCardAtmWithdrawlsInHighRiskCountryDescription) {
		this.creditCardAtmWithdrawlsInHighRiskCountryDescription = creditCardAtmWithdrawlsInHighRiskCountryDescription;
	}

	public String getCreditCardAtmWithdrawlsInHighRiskCountryScenarioType() {
		return creditCardAtmWithdrawlsInHighRiskCountryScenarioType;
	}

	public void setCreditCardAtmWithdrawlsInHighRiskCountryScenarioType(
			String creditCardAtmWithdrawlsInHighRiskCountryScenarioType) {
		this.creditCardAtmWithdrawlsInHighRiskCountryScenarioType = creditCardAtmWithdrawlsInHighRiskCountryScenarioType;
	}

	public int getCreditCardAtmWithdrawlsInHighRiskCountryRiskScore() {
		return creditCardAtmWithdrawlsInHighRiskCountryRiskScore;
	}

	public void setCreditCardAtmWithdrawlsInHighRiskCountryRiskScore(
			int creditCardAtmWithdrawlsInHighRiskCountryRiskScore) {
		this.creditCardAtmWithdrawlsInHighRiskCountryRiskScore = creditCardAtmWithdrawlsInHighRiskCountryRiskScore;
	}

	public String getCreditCardAtmTransactionRelationCountries() {
		return creditCardAtmTransactionRelationCountries;
	}

	public void setCreditCardAtmTransactionRelationCountries(String creditCardAtmTransactionRelationCountries) {
		this.creditCardAtmTransactionRelationCountries = creditCardAtmTransactionRelationCountries;
	}

	public String getCreditCardAtmTransactionInNewCountries() {
		return creditCardAtmTransactionInNewCountries;
	}

	public void setCreditCardAtmTransactionInNewCountries(String creditCardAtmTransactionInNewCountries) {
		this.creditCardAtmTransactionInNewCountries = creditCardAtmTransactionInNewCountries;
	}

	public String getCreditCardAtmWithdrwalsInShortTime() {
		return creditCardAtmWithdrwalsInShortTime;
	}

	public void setCreditCardAtmWithdrwalsInShortTime(String creditCardAtmWithdrwalsInShortTime) {
		this.creditCardAtmWithdrwalsInShortTime = creditCardAtmWithdrwalsInShortTime;
	}

	public Double getCreditCardAtmHighVolumeWithdrwal() {
		return creditCardAtmHighVolumeWithdrwal;
	}

	public void setCreditCardAtmHighVolumeWithdrwal(Double creditCardAtmHighVolumeWithdrwal) {
		this.creditCardAtmHighVolumeWithdrwal = creditCardAtmHighVolumeWithdrwal;
	}

	public String getCreditCardAtmWithdrwalTransactionMoreThanOne() {
		return creditCardAtmWithdrwalTransactionMoreThanOne;
	}

	public void setCreditCardAtmWithdrwalTransactionMoreThanOne(String creditCardAtmWithdrwalTransactionMoreThanOne) {
		this.creditCardAtmWithdrwalTransactionMoreThanOne = creditCardAtmWithdrwalTransactionMoreThanOne;
	}

	public String getCreditCardAtmWithdrwalCountryName() {
		return creditCardAtmWithdrwalCountryName;
	}

	public void setCreditCardAtmWithdrwalCountryName(String creditCardAtmWithdrwalCountryName) {
		this.creditCardAtmWithdrwalCountryName = creditCardAtmWithdrwalCountryName;
	}

	public String getCreditCardUnrelatedCounterPartiesTransactions() {
		return creditCardUnrelatedCounterPartiesTransactions;
	}

	public void setCreditCardUnrelatedCounterPartiesTransactions(String creditCardUnrelatedCounterPartiesTransactions) {
		this.creditCardUnrelatedCounterPartiesTransactions = creditCardUnrelatedCounterPartiesTransactions;
	}

	public Double getCreditCardUnrelatedCounterPartyPercentage() {
		return creditCardUnrelatedCounterPartyPercentage;
	}

	public void setCreditCardUnrelatedCounterPartyPercentage(Double creditCardUnrelatedCounterPartyPercentage) {
		this.creditCardUnrelatedCounterPartyPercentage = creditCardUnrelatedCounterPartyPercentage;
	}

	public String getCreditCardUnrelatedCounterPartiesScenario() {
		return creditCardUnrelatedCounterPartiesScenario;
	}

	public void setCreditCardUnrelatedCounterPartiesScenario(String creditCardUnrelatedCounterPartiesScenario) {
		this.creditCardUnrelatedCounterPartiesScenario = creditCardUnrelatedCounterPartiesScenario;
	}

	public String getCreditCardUnrelatedCounterPartiesDescription() {
		return creditCardUnrelatedCounterPartiesDescription;
	}

	public void setCreditCardUnrelatedCounterPartiesDescription(String creditCardUnrelatedCounterPartiesDescription) {
		this.creditCardUnrelatedCounterPartiesDescription = creditCardUnrelatedCounterPartiesDescription;
	}

	public String getCreditCardUnrelatedCounterPartiesScenarioType() {
		return creditCardUnrelatedCounterPartiesScenarioType;
	}

	public void setCreditCardUnrelatedCounterPartiesScenarioType(String creditCardUnrelatedCounterPartiesScenarioType) {
		this.creditCardUnrelatedCounterPartiesScenarioType = creditCardUnrelatedCounterPartiesScenarioType;
	}

	public int getCreditCardUnrelatedCounterPartiesRiskScore() {
		return creditCardUnrelatedCounterPartiesRiskScore;
	}

	public void setCreditCardUnrelatedCounterPartiesRiskScore(int creditCardUnrelatedCounterPartiesRiskScore) {
		this.creditCardUnrelatedCounterPartiesRiskScore = creditCardUnrelatedCounterPartiesRiskScore;
	}

	public String getCreditCardMerchantRefund() {
		return creditCardMerchantRefund;
	}

	public void setCreditCardMerchantRefund(String creditCardMerchantRefund) {
		this.creditCardMerchantRefund = creditCardMerchantRefund;
	}

	public String getCreditCardNewMerchantRefund() {
		return creditCardNewMerchantRefund;
	}

	public void setCreditCardNewMerchantRefund(String creditCardNewMerchantRefund) {
		this.creditCardNewMerchantRefund = creditCardNewMerchantRefund;
	}

	public String getCreditCardMerchantRefundScenario() {
		return creditCardMerchantRefundScenario;
	}

	public void setCreditCardMerchantRefundScenario(String creditCardMerchantRefundScenario) {
		this.creditCardMerchantRefundScenario = creditCardMerchantRefundScenario;
	}

	public String getCreditCardMerchantRefundDescription() {
		return creditCardMerchantRefundDescription;
	}

	public void setCreditCardMerchantRefundDescription(String creditCardMerchantRefundDescription) {
		this.creditCardMerchantRefundDescription = creditCardMerchantRefundDescription;
	}

	public String getCreditCardMerchantRefundScenarioType() {
		return creditCardMerchantRefundScenarioType;
	}

	public void setCreditCardMerchantRefundScenarioType(String creditCardMerchantRefundScenarioType) {
		this.creditCardMerchantRefundScenarioType = creditCardMerchantRefundScenarioType;
	}

	public Integer getCreditCardMerchantRefundRiskScore() {
		return creditCardMerchantRefundRiskScore;
	}

	public void setCreditCardMerchantRefundRiskScore(Integer creditCardMerchantRefundRiskScore) {
		this.creditCardMerchantRefundRiskScore = creditCardMerchantRefundRiskScore;
	}

	public Double getCreditCardOverPaymentPercentage() {
		return creditCardOverPaymentPercentage;
	}

	public void setCreditCardOverPaymentPercentage(Double creditCardOverPaymentPercentage) {
		this.creditCardOverPaymentPercentage = creditCardOverPaymentPercentage;
	}

	public List<String> getScenarioOfAlert() {
		return scenarioOfAlert;
	}

	public void setScenarioOfAlert(List<String> scenarioOfAlert) {
		this.scenarioOfAlert = scenarioOfAlert;
	}

	public List<String> getDescriptionOfAlert() {
		return descriptionOfAlert;
	}

	public void setDescriptionOfAlert(List<String> descriptionOfAlert) {
		this.descriptionOfAlert = descriptionOfAlert;
	}

	public List<String> getScenarioTypeOfAlert() {
		return scenarioTypeOfAlert;
	}

	public void setScenarioTypeOfAlert(List<String> scenarioTypeOfAlert) {
		this.scenarioTypeOfAlert = scenarioTypeOfAlert;
	}

	public List<Integer> getRiskScoreOfAlert() {
		return riskScoreOfAlert;
	}

	public void setRiskScoreOfAlert(List<Integer> riskScoreOfAlert) {
		this.riskScoreOfAlert = riskScoreOfAlert;
	}

	public String getAddingList() {
		return addingList;
	}

	public void setAddingList(String addingList) {
		this.addingList = addingList;
	}

	public String getTransactionProductTypeAmountScenario() {
		return transactionProductTypeAmountScenario;
	}

	public void setTransactionProductTypeAmountScenario(String transactionProductTypeAmountScenario) {
		this.transactionProductTypeAmountScenario = transactionProductTypeAmountScenario;
	}

	public String getTransactionProductTypeAmountAlertDescription() {
		return transactionProductTypeAmountAlertDescription;
	}

	public void setTransactionProductTypeAmountAlertDescription(String transactionProductTypeAmountAlertDescription) {
		this.transactionProductTypeAmountAlertDescription = transactionProductTypeAmountAlertDescription;
	}
	

}
