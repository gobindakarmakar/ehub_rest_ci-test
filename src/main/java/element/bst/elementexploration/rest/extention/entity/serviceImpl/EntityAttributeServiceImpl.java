package element.bst.elementexploration.rest.extention.entity.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.openjson.JSONObject;

import element.bst.elementexploration.rest.extention.entity.dao.EntityAttributeDao;
import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.extention.entity.dto.EntityAttributeDto;
import element.bst.elementexploration.rest.extention.entity.service.EntityAttributeService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * 
 * @author Viswanath Reddy G
 *
 */
@Service("entityAttributeService")
public class EntityAttributeServiceImpl extends GenericServiceImpl<EntityAttributes, Long>
		implements EntityAttributeService {

	@Autowired
	public EntityAttributeServiceImpl(GenericDao<EntityAttributes, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private EntityAttributeDao entityAttributeDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	 
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Value("${bigdata_override_analyst_info_url}")
	private String BIGDATA_OVERRIDE_ANALYST_INFO_URL;
	
	@Value("${client_id}")
	private String CLIENT_ID;
	
	@Value("${be_entity_api_key}")
	private String BE_ENTITY_API_KEY;

	@Override
	@Transactional("transactionManager")
	public EntityAttributes saveEntityAttributes(EntityAttributeDto entityAttributes, Long userId) {
		EntityAttributes oldEntityAttributes = entityAttributeDao.get(entityAttributes.getEntityId(),
				entityAttributes.getSourceSchema());
		if (oldEntityAttributes != null) {
			Users user = usersDao.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			oldEntityAttributes.setUser(user);
			if(entityAttributes.getPublishedDate()!=null)
				oldEntityAttributes.setPublishedDate(entityAttributes.getPublishedDate());
			oldEntityAttributes.setModifiedDate(new Date());
			//if(entityAttributes.getSource()!=null && "".equalsIgnoreCase(entityAttributes.getSource()))
			if(entityAttributes.getIsUserData()!=null && entityAttributes.getIsUserData().booleanValue()){
				oldEntityAttributes.setIsUserData(entityAttributes.getIsUserData().booleanValue());
				oldEntityAttributes.setUserValue(entityAttributes.getValue());
				oldEntityAttributes.setUserPublishedDate(entityAttributes.getPublishedDate());
				oldEntityAttributes.setUserSource(entityAttributes.getSource());
				oldEntityAttributes.setUserSourceUrl(entityAttributes.getSourceUrl());
				oldEntityAttributes.setDocId(entityAttributes.getDocId());

			}else{
				oldEntityAttributes.setIsUserData(false);
			}
			oldEntityAttributes.setValue(entityAttributes.getValue());
			oldEntityAttributes.setSource(entityAttributes.getSource());
			if(entityAttributes.getSourceUrl()!=null)
				oldEntityAttributes.setSourceUrl(entityAttributes.getSourceUrl());
			if(entityAttributes.getSourceDisplayName() != null)
				oldEntityAttributes.setSourceDisplayName(entityAttributes.getSourceDisplayName());
			if(entityAttributes.getSourceType()!=null)
				oldEntityAttributes.setSourceType(entityAttributes.getSourceType());
			if(oldEntityAttributes.getPreValue()==null)
				oldEntityAttributes.setPreValue(entityAttributes.getPreValue());
			oldEntityAttributes.setDocId(entityAttributes.getDocId());
			entityAttributeDao.saveOrUpdate(oldEntityAttributes);
			overrideInformationFromAnalyst(entityAttributes);
			eventPublisher.publishEvent(new LoggingEvent(oldEntityAttributes, LoggingEventType.OVERRIDE_COMPANY_DETAILS,
					userId, new Date()));
			return oldEntityAttributes;
		} else {
			EntityAttributes entityAttributesNew = new EntityAttributes();
			BeanUtils.copyProperties(entityAttributes, entityAttributesNew);
			
			Users user = usersDao.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			entityAttributesNew.setUser(user);
			if(entityAttributes.getPublishedDate()!=null)
				entityAttributesNew.setPublishedDate(entityAttributes.getPublishedDate());
			entityAttributesNew.setCreatedDate(new Date());
			if(entityAttributes.getSourceType()!=null)
				entityAttributesNew.setSourceType(entityAttributes.getSourceType());
			//if("".equalsIgnoreCase(entityAttributes.getSource())){
			if(entityAttributes.getIsUserData()!=null && entityAttributes.getIsUserData().booleanValue()){
				entityAttributesNew.setUserValue(entityAttributes.getValue());
				entityAttributesNew.setIsUserData(entityAttributes.getIsUserData());
				entityAttributesNew.setUserPublishedDate(entityAttributes.getPublishedDate());
				entityAttributesNew.setUserSource(entityAttributes.getSource());
				entityAttributesNew.setUserSourceUrl(entityAttributes.getSourceUrl());
			}else{
				entityAttributesNew.setIsUserData(false);
			}
			EntityAttributes returnEntityAttributes = entityAttributeDao.create(entityAttributesNew);
			overrideInformationFromAnalyst(entityAttributes);
			eventPublisher.publishEvent(new LoggingEvent(returnEntityAttributes, LoggingEventType.OVERRIDE_COMPANY_DETAILS,
					userId, new Date()));
			return returnEntityAttributes;
		}

	}

	@Override
	@Transactional("transactionManager")
	public EntityAttributes getEntityAttributes(String entityId, String sourceSchema) {
		return entityAttributeDao.get(entityId, sourceSchema);
	}

	@Override
	@Transactional("transactionManager")
	public List<EntityAttributes> getAttributeList(String entityId, String sourceSchema) {
		List<EntityAttributes> list=entityAttributeDao.getAttributeList(entityId, sourceSchema);
		for (EntityAttributes entityAttributes : list) {
			
		}
		return entityAttributeDao.getAttributeList(entityId, sourceSchema);
	}
	
	@SuppressWarnings("unused")
	public void overrideInformationFromAnalyst(EntityAttributeDto entityAttributes){
		if(entityAttributes.getSource()!=null && "".equalsIgnoreCase(entityAttributes.getSource())&& entityAttributes.getValue()!=null){
			JSONObject entityInfo= new JSONObject();
			entityInfo.put(entityAttributes.getSource(), entityAttributes.getValue());
			String url=BIGDATA_OVERRIDE_ANALYST_INFO_URL+"/"+entityAttributes.getEntityId()+"?fields=overview&client_id="+CLIENT_ID;
			String serverResponse[]=ServiceCallHelper.updateDataInServerWithApiKey(url, entityInfo.toString(),BE_ENTITY_API_KEY);
		}
	}

}
