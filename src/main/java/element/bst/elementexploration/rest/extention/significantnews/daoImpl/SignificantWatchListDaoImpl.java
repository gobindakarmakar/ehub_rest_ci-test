package element.bst.elementexploration.rest.extention.significantnews.daoImpl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantWatchListDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantWatchList;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("significantWatchListDao")
public class SignificantWatchListDaoImpl extends GenericDaoImpl<SignificantWatchList, Long> implements SignificantWatchListDao{

	public SignificantWatchListDaoImpl() {
		super(SignificantWatchList.class);
	}

	@Override
	public SignificantWatchList getSignificantWatchList(String mainEntityId,String identifier, String entityName, String value,String name) {
		SignificantWatchList significantWatchList = null;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("select sw from SignificantWatchList sw where sw.mainEntityId = :mainEntityId and sw.identifier = :identifier and sw.entityName = :entityName and sw.name = :name and sw.value = :value");			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("mainEntityId", mainEntityId);
			query.setParameter("identifier", identifier);
			query.setParameter("entityName", entityName);
			query.setParameter("name", name);
			query.setParameter("value", value);
			significantWatchList = (SignificantWatchList) query.getSingleResult();
		}catch (Exception e) {
			return significantWatchList;
		}
		return significantWatchList;
	}

	@Override
	public List<SignificantWatchList> fetchWtachListByEntityId(String mainEntityId) {
		List<SignificantWatchList> significantWatchList = null;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("select sw from SignificantWatchList sw where sw.mainEntityId = :mainEntityId");			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("mainEntityId", mainEntityId);
			significantWatchList = (List<SignificantWatchList>) query.getResultList();
		}catch (Exception e) {
			return significantWatchList;
		}
		return significantWatchList;
	}
	
	
	

	
	
	

}
