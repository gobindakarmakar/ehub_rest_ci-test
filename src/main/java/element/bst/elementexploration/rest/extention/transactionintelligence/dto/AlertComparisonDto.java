package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Alert comaparison")
public class AlertComparisonDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Transaction product type")	
	private String productType;
	@ApiModelProperty(value="Total count of alerts")	
	private long count;
	
	

	public AlertComparisonDto(String productType, long count) {
		super();
		this.productType = productType;
		this.count = count;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
