package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Shareholders;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface ShareholderService  extends GenericService<Shareholders, Long>
{
	Boolean uoploadShareholders(MultipartFile multipartFile) throws IOException ;
}
