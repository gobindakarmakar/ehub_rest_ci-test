package element.bst.elementexploration.rest.extention.docparser.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.docparser.dto.CoordinatesResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TemplatesRepsonseDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import net.sourceforge.tess4j.TesseractException;

public class DocumentTemplatesServiceTest {
	
	@Mock
	DocumentTemplatesService documentTemplatesServiceMock;
	
	public CoordinatesResponseDto coordinatesResponseDto;
	
	public List<TemplatesRepsonseDto> templatesRepsonseDtoList;
	
	@Before
    public void setupMock() {
		TemplatesRepsonseDto templatesRepsonseDto1 = new TemplatesRepsonseDto();
		TemplatesRepsonseDto templatesRepsonseDto2 = new TemplatesRepsonseDto();
		templatesRepsonseDtoList = new ArrayList<TemplatesRepsonseDto>();
		templatesRepsonseDtoList.add(templatesRepsonseDto1);
		templatesRepsonseDtoList.add(templatesRepsonseDto2);
		coordinatesResponseDto = new CoordinatesResponseDto();		
		MockitoAnnotations.initMocks(this);
	}
	
	/*@Test
	public void saveNewPdfFillableTemplateTest() {	
		Mockito.when(documentTemplatesServiceMock.saveNewPdfFillableTemplate(Mockito.any(DocParserSaveDto.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(123L);
		Long templateId = documentTemplatesServiceMock.saveNewPdfFillableTemplate(Mockito.any(DocParserSaveDto.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt());
		Assert.assertNotNull(templateId);	
	}*/
	
	@Test
	public void getQuestionAndAnswerNewTemplateTest() throws InvalidPasswordException, IOException, TesseractException {	
		Mockito.when(documentTemplatesServiceMock.getQuestionAndAnswerNewTemplate(Mockito.any(CoordinatesResponseDto.class),Mockito.any(byte[].class))).thenReturn(coordinatesResponseDto);
		CoordinatesResponseDto newcoordinatesResponseDto = documentTemplatesServiceMock.getQuestionAndAnswerNewTemplate(Mockito.any(CoordinatesResponseDto.class),Mockito.any(byte[].class));
		Assert.assertNotNull(newcoordinatesResponseDto);
	}
	
	@Test
	public void getTemplatesPaginationTest() {	
		Mockito.when(documentTemplatesServiceMock.getTemplatesPagination(Mockito.anyInt(),Mockito.anyInt(),Mockito.anyBoolean(),Mockito.anyString())).thenReturn(templatesRepsonseDtoList);
		List<TemplatesRepsonseDto> newTemplatesRepsonseDtoList = documentTemplatesServiceMock.getTemplatesPagination(Mockito.anyInt(),Mockito.anyInt(),Mockito.anyBoolean(),Mockito.anyString());
		assertEquals(2,newTemplatesRepsonseDtoList.size());	
	}
	
	@Test
	public void updateTemplateNameTest() {	
		Mockito.when(documentTemplatesServiceMock.updateTemplateName(Mockito.any(TemplatesRepsonseDto.class),Mockito.anyLong())).thenReturn(true);
		boolean status = documentTemplatesServiceMock.updateTemplateName(Mockito.any(TemplatesRepsonseDto.class),Mockito.anyLong());
		assertEquals(true,status);	
	}
	
	@Test
	public void updateTemplateQuestionsTest() throws Exception {	
		Mockito.when(documentTemplatesServiceMock.updateTemplateQuestions(Mockito.any(OverAllResponseDto.class),Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		boolean status = documentTemplatesServiceMock.updateTemplateQuestions(Mockito.any(OverAllResponseDto.class),Mockito.anyLong(),Mockito.anyLong());
		assertEquals(true,status);	
	}
	
	@Test
	public void deleteTemplateTest() throws Exception {	
		Mockito.when(documentTemplatesServiceMock.deleteTemplate(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		boolean status = documentTemplatesServiceMock.deleteTemplate(Mockito.anyLong(),Mockito.anyLong());
		assertEquals(true,status);	
	}
	
	@Test
	public void getTemplatesTest() {	
		Mockito.when(documentTemplatesServiceMock.getTemplates(Mockito.anyBoolean(),Mockito.anyString())).thenReturn(123L);
		Long status = documentTemplatesServiceMock.getTemplates(Mockito.anyBoolean(),Mockito.anyString());
		Assert.assertNotNull(status);	
	}
	
}
