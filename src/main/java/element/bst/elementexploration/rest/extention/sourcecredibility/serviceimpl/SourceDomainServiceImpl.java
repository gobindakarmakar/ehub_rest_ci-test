package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceDomainDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceDomainDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceDomainService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author suresh
 *
 */
@Service("sourceDomainService")
public class SourceDomainServiceImpl extends GenericServiceImpl<SourceDomain, Long> implements SourceDomainService {

	@Autowired
	public SourceDomainServiceImpl(GenericDao<SourceDomain, Long> genericDao) {
		super(genericDao);
	}

	public SourceDomainServiceImpl() {
	}

	@Autowired
	SourceDomainDao sourceDomainDao;

	@Override
	@Transactional("transactionManager")
	public List<SourceDomainDto> saveSourceDomain(SourceDomainDto domainDto) throws Exception {
		SourceDomain domainFromDB = sourceDomainDao.fetchDomain(domainDto.getDomainName());
		if (domainFromDB == null) {
			SourceDomain domain = new SourceDomain();
			domain.setDomainName(domainDto.getDomainName());
			sourceDomainDao.create(domain);
			return getSourceDomain();
		} else
			throw new Exception(ElementConstants.DOMAIN_ADD_FAILED);
	}

	@Override
	@Transactional("transactionManager")
	public List<SourceDomainDto> getSourceDomain() {
		List<SourceDomain> domainList = sourceDomainDao.findAll();
		List<SourceDomainDto> sourcesDtoList = new ArrayList<SourceDomainDto>();
		if (domainList != null) {
			sourcesDtoList = domainList.stream()
					.map((sourcesDto) -> new EntityConverter<SourceDomain, SourceDomainDto>(SourceDomain.class,
							SourceDomainDto.class).toT2(sourcesDto, new SourceDomainDto()))
					.collect(Collectors.toList());
		}
		return sourcesDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public SourceDomain fetchDomain(String domain) {
		return sourceDomainDao.fetchDomain(domain);
	}

}
