package element.bst.elementexploration.rest.recentsearch.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.recentsearch.dao.RecentSearchDao;
import element.bst.elementexploration.rest.recentsearch.domain.RecentSearchs;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("recentSearchDao")
public class RecentSearchDaoImpl extends GenericDaoImpl<RecentSearchs, Long> implements RecentSearchDao {

	public RecentSearchDaoImpl() {
		super(RecentSearchs.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RecentSearchDto> getRecentSearchs(Long userId) {

		List<RecentSearchDto> list = new ArrayList<RecentSearchDto>();
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("select new element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto( ");
			builder.append("rs.search,rs.userId,rs.createdOn ) from RecentSearchs rs where rs.userId=:userId");
			builder.append(" order by rs.createdOn desc ");
			Query<?> query = this.getCurrentSession().createQuery(builder.toString());
			query.setFirstResult(0);
			query.setMaxResults(7);
			query.setParameter("userId", userId);
			list = (List<RecentSearchDto>) query.getResultList();
			
			return list;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return list;
	}

}
