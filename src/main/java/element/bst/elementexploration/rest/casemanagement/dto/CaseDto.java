package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Case")
public class CaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "The ID of the case")
	private Long caseId;
	@ApiModelProperty(value = "The name of the case")
	private String name;
	@ApiModelProperty(value = "The description of the case")
	private String description;
	@ApiModelProperty(value = "The ID of the case")
	private String remarks;
	@ApiModelProperty(value = "The remarks of the case")
	private PriorityEnums priority;
	@ApiModelProperty(value = "The current status of the case")
	private Integer currentStatus;
	@ApiModelProperty(value = "The health of the case")
	private Double health;
	@ApiModelProperty(value = "The direct risk of the case")
	private Double directRisk;
	@ApiModelProperty(value = "The indirect risk of the case")
	private Double indirectRisk;
	@ApiModelProperty(value = "The transactional risk of the case")
	private Double transactionalRisk;
	@ApiModelProperty(value = "Date of creation of case")
	private Date createdOn;
	@ApiModelProperty(value = "Creator of the case")
	private Long createdBy;
	@ApiModelProperty(value = "Modified date of the case")
	private Date modifiedOn;
	@ApiModelProperty(value = "Modifier of the case")
	private Long modifiedBy;
	@ApiModelProperty(value = "Type of the case")
	private String type;
	@ApiModelProperty(value = "Questionnaire Document ID")
	private Long questionnaireDocId;
	private Double riskScore;
	@ApiModelProperty(value = "Decision Scoring EntityId")
	private String decisionScoringEntityId;
	@ApiModelProperty(value = "Work flow name")
	private String workflowName;
	@ApiModelProperty(value = "Work flow id")
	private String workflowId;
	@ApiModelProperty(value = "lead generation")
	private Boolean isLeadGeneration;
	@ApiModelProperty(value = "sub type of the case")	
	private String subType;
	@ApiModelProperty(value = "assigned by user")	
	private String assignedBy;
	@ApiModelProperty(value = "")
	private String caseData;
	@ApiModelProperty(value = "multisource id")	
	private String multiSourceId;


	public CaseDto() {
		super();
	}

	public CaseDto(Long caseId, String name, String description, String remarks, PriorityEnums priority,
			Integer currentStatus, Double health, Double directRisk, Double indirectRisk, Double transactionalRisk,
			Date createdOn, Long createdBy, Date modifiedOn, Long modifiedBy, String type) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.description = description;
		this.remarks = remarks;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.health = health;
		this.directRisk = directRisk;
		this.indirectRisk = indirectRisk;
		this.transactionalRisk = transactionalRisk;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.type = type;
	}
	
	public CaseDto(Long caseId, String name, String description, String remarks, PriorityEnums priority,
			Integer currentStatus, Double health, Double directRisk, Double indirectRisk, Double transactionalRisk,
			Date createdOn, Long createdBy, Date modifiedOn, Long modifiedBy, String type,String decisionScoringEntityId) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.description = description;
		this.remarks = remarks;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.health = health;
		this.directRisk = directRisk;
		this.indirectRisk = indirectRisk;
		this.transactionalRisk = transactionalRisk;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.type = type;
		this.decisionScoringEntityId=decisionScoringEntityId;
	}
	
	public CaseDto(Long caseId, String name, String description, String remarks, PriorityEnums priority,
			Integer currentStatus, Double health, Double directRisk, Double indirectRisk, Double transactionalRisk,
			Date createdOn, Long createdBy, Date modifiedOn, Long modifiedBy, String type,String decisionScoringEntityId,String workflowName,String workflowId,Boolean isLeadGeneration,String subType) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.description = description;
		this.remarks = remarks;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.health = health;
		this.directRisk = directRisk;
		this.indirectRisk = indirectRisk;
		this.transactionalRisk = transactionalRisk;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.type = type;
		this.decisionScoringEntityId=decisionScoringEntityId;
		this.workflowId = workflowId;
		this.workflowName = workflowName;
		this.isLeadGeneration = isLeadGeneration;
		this.subType = subType;
				
	}

	public CaseDto(Long caseId, String name, String description, String remarks, PriorityEnums priority,
			Integer currentStatus, Double health, Double directRisk, Double indirectRisk, Double transactionalRisk,
			Date createdOn, Long createdBy, Date modifiedOn, Long modifiedBy, String type,String decisionScoringEntityId,String workflowName,String workflowId,Boolean isLeadGeneration,String subType,String assignedBy,String multiSourceId) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.description = description;
		this.remarks = remarks;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.health = health;
		this.directRisk = directRisk;
		this.indirectRisk = indirectRisk;
		this.transactionalRisk = transactionalRisk;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.type = type;
		this.decisionScoringEntityId=decisionScoringEntityId;
		this.workflowId = workflowId;
		this.workflowName = workflowName;
		this.isLeadGeneration = isLeadGeneration;
		this.subType = subType;
		this.assignedBy=assignedBy;
		this.multiSourceId = multiSourceId;
	}
	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PriorityEnums getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnums priority) {
		this.priority = priority;
	}

	public Integer getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Integer currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Double getHealth() {
		return health;
	}

	public void setHealth(Double health) {
		this.health = health;
	}

	public Double getDirectRisk() {
		return directRisk;
	}

	public void setDirectRisk(Double directRisk) {
		this.directRisk = directRisk;
	}

	public Double getIndirectRisk() {
		return indirectRisk;
	}

	public void setIndirectRisk(Double indirectRisk) {
		this.indirectRisk = indirectRisk;
	}

	public Double getTransactionalRisk() {
		return transactionalRisk;
	}

	public void setTransactionalRisk(Double transactionalRisk) {
		this.transactionalRisk = transactionalRisk;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getQuestionnaireDocId() {
		return questionnaireDocId;
	}

	public void setQuestionnaireDocId(Long questionnaireDocId) {
		this.questionnaireDocId = questionnaireDocId;
	}

	public Double getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(Double riskScore) {
		this.riskScore = riskScore;
	}

	public String getDecisionScoringEntityId() {
		return decisionScoringEntityId;
	}

	public void setDecisionScoringEntityId(String decisionScoringEntityId) {
		this.decisionScoringEntityId = decisionScoringEntityId;
	}	

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public Boolean getIsLeadGeneration() {
		return isLeadGeneration;
	}

	public void setIsLeadGeneration(Boolean isLeadGeneration) {
		this.isLeadGeneration = isLeadGeneration;
	}	

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}

	public String getCaseData() {
		return caseData;
	}

	public void setCaseData(String caseData) {
		this.caseData = caseData;
	}
	public String getMultiSourceId() {
		return multiSourceId;
	}

	public void setMultiSourceId(String multiSourceId) {
		this.multiSourceId = multiSourceId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
