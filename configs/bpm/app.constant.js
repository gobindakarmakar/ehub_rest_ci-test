
'use strict';

/*--  For ING ---*/
/*-- activitiApp.constant('EHUB_API', 'http://159.65.192.32:6060/ehubrest/api/')
 	   .constant('ACTIVITI_API', 'http://159.65.192.32:6060/ehubrest')
	   .constant('EHUB_FE_API', 'http://159.65.192.32:6060/element/')
	   .constant('ACTIVITI_FE_PATH', 'http://159.65.192.32:6060/elementbpm/#/')
	   .constant('KYC_QUESTIONNAIRE_PATH', 'http://159.65.192.32/element-questionnaire-builder/index.php/admin')
	   .constant('POLICY_ENFORCEMENT_PATH', 'http://159.65.192.32:7070/policyEnforcement')
	   .constant('DELEGATE_EXPRESSION_OBJ',{
		   MOVE_CASE:'${statusUpdateListener}',
		   CASE_ASSIGN:'${caseAssignService}',
		   CASE_REASSIGN:'${caseForwadingOrReAssignService}'
	   });---*/

/**
activitiApp.constant('EHUB_API', EHUB_API+'/ehubrest/api/')
        .constant('ACTIVITI_API',EHUB_API+'/ehubrest')
        .constant('EHUB_FE_API', ACTIVITI_FE_PATH+'/element/')
        .constant('ACTIVITI_FE_PATH', EHUB_API+'/elementbpm/#/')
        .constant('KYC_QUESTIONNAIRE_PATH', KYC_QUESTIONNAIRE_PATH+'/element-questionnaire-builder/index.php/admin')
        .constant('POLICY_ENFORCEMENT_PATH', POLICY_ENFORCEMENT_PATH+'/policyEnforcement')
        .constant('DELEGATE_EXPRESSION_OBJ',{
         MOVE_CASE:'${statusUpdateListener}',
        CASE_ASSIGN:'${caseAssignService}',
        CASE_REASSIGN:'${caseForwadingOrReAssignService}'
        });

***/

activitiApp.constant('EHUB_API', EHUB_API+'/ehubrest/api/')
        .constant('ACTIVITI_API',EHUB_API+'/ehubrest')
        .constant('EHUB_FE_API', EHUB_FE_API+'/element/')
        .constant('ACTIVITI_FE_PATH', EHUB_API+'/elementbpm/#/')
        .constant('KYC_QUESTIONNAIRE_PATH', KYC_QUESTIONNAIRE_PATH+'/element-questionnaire-builder/index.php/admin')
        .constant('POLICY_ENFORCEMENT_PATH', POLICY_ENFORCEMENT_PATH+'/policyEnforcement/')
        .constant('DELEGATE_EXPRESSION_OBJ',{
         MOVE_CASE:'${statusUpdateListener}',
        CASE_ASSIGN:'${caseAssignService}',
        CASE_REASSIGN:'${caseForwadingOrReAssignService}'
        });

