package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Counter party list")
public class CounterPartyInfoDto {
	
	@ApiModelProperty("List of counter party customer transations")
	@JsonProperty("Customer")
	private List<CounterPartyInfo> customer;
	
	@ApiModelProperty("List of counter party customer country transations")
	@JsonProperty("Country")
	private List<CounterPartyInfo> country;

	public List<CounterPartyInfo> getCustomer() {
		return customer;
	}

	public void setCustomer(List<CounterPartyInfo> customer) {
		this.customer = customer;
	}

	public List<CounterPartyInfo> getCountry() {
		return country;
	}

	public void setCountry(List<CounterPartyInfo> country) {
		this.country = country;
	}

}
