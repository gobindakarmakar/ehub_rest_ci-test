package element.bst.elementexploration.rest.extention.docparser.dao;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentISOCodes;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentISOCodeDao extends GenericDao<DocumentISOCodes, Long>{
	
	public DocumentISOCodes fetchDocumnetIsoCode(String question,boolean isQuestion);

	
}
