package element.bst.elementexploration.rest.extention.alert.feedmanagement.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import io.swagger.annotations.ApiModel;

/**
 * @author Prateek
 *
 */

@ApiModel("Alert Management Feed and Groups")
@Entity
@Table(name = "bst_am_feed_groups")
public class FeedGroups implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long feedGroupsId;
	
	@Column(name = "rank")
	private Integer rank;
	
	@ManyToOne
	@JoinColumn(name = "feed_id")
	private FeedManagement feedId;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "group_id")
	private Groups groupId;

	public Long getFeedGroupsId() {
		return feedGroupsId;
	}

	public void setFeedGroupsId(Long feedGroupsId) {
		this.feedGroupsId = feedGroupsId;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public FeedManagement getFeedId() {
		return feedId;
	}

	public Groups getGroupId() {
		return groupId;
	}

	public void setFeedId(FeedManagement feedId) {
		this.feedId = feedId;
	}

	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}

	
	
	
}
