package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.RolesGroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("rolesGroupDao")
public class RolesGroupDaoImpl extends GenericDaoImpl<RoleGroup, Long> implements RolesGroupDao {

	public RolesGroupDaoImpl() {
		super(RoleGroup.class);
	}

	@Override
	public List<RoleGroup> getRolesByGroup(List<Long> groupIds) {
		List<RoleGroup> roleGroupList = new ArrayList<RoleGroup>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<RoleGroup> query = builder.createQuery(RoleGroup.class);
			Root<RoleGroup> roleGroupRoot = query.from(RoleGroup.class);
			query.select(roleGroupRoot);
			query.where(roleGroupRoot.get("groupId").in(groupIds));
			roleGroupList = this.getCurrentSession().createQuery(query).getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, RolesGroupDaoImpl.class);
			return roleGroupList;
		}
		return roleGroupList;
	}

	@Override
	public void deleteExistingRolesForGroup(Long groupId) {
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("delete from RoleGroup rg where rg.groupId.id = :groupId");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("groupId", groupId);
			query.executeUpdate();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
	}

}
