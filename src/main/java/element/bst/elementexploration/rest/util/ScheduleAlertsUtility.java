package element.bst.elementexploration.rest.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.settings.dto.SettingsDto;
import element.bst.elementexploration.rest.settings.service.SettingsService;

@Component
public class ScheduleAlertsUtility {

	@Autowired
	AlertManagementService alertManagementService;

	@Autowired
	SettingsService settingsService;

	//@Scheduled(cron = "0 0 23 * * ?")
	public void riskFactorCron() throws Exception {
		String alertType = "PEP";
		List<Alerts> alertList = alertManagementService.findAllWithRisk();
		List<SettingsDto> settings = settingsService.fetchSystemSettingsByType("ALERT_MANAGEMENT");
		List<SettingsDto> settingsSlider = settings.stream()
				.filter(setting -> setting.getType().equalsIgnoreCase("Slider")).collect(Collectors.toList());
		// Map<String,Integer>
		// nameSliderToMap=settingsSlider.stream().collect(Collectors.toMap(SettingsDto::getName,
		// SettingsDto::getSliderTo));
		Map<String, Integer> nameSliderToMap = new HashMap<String, Integer>();
		settingsSlider.stream().forEach(setting -> {
			if (setting.getDefaultValue().equalsIgnoreCase("Days")) {
				nameSliderToMap.put(setting.getName(), setting.getSliderTo());
				//System.out.println("Days");
			}
			if (setting.getDefaultValue().equalsIgnoreCase("Months")) {
				int periodValue = setting.getSliderTo();
				periodValue = Math.round(periodValue * 30.417f);
				nameSliderToMap.put(setting.getName(), periodValue);
				//System.out.println("month");

			}
			if (setting.getDefaultValue().equalsIgnoreCase("Years")) {
				int periodValue = setting.getSliderTo();
				periodValue = periodValue * 365;
				nameSliderToMap.put(setting.getName(), periodValue);
				//System.out.println("Years");
			}
			if (setting.getDefaultValue().equalsIgnoreCase("Weeks")) {
				int periodValue = setting.getSliderTo();
				periodValue = periodValue * 7;
				nameSliderToMap.put(setting.getName(), periodValue);
				//System.out.println("Weeks");
			}
		});
		for (Map.Entry<String, Integer> entry : nameSliderToMap.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}

		List<Alerts> alertsWithPeriodicTrue = new ArrayList<Alerts>();
		for (Alerts alert : alertList) {
			if (!alert.getPeriodicReview()) {
				Date alertCreatedDate = alert.getCreatedDate();
				// Date currentDate= new Date();
				Integer finalDaysDifference = 0;
				if (alertCreatedDate != null) {
					Long days = (alertCreatedDate.getTime() - new Date().getTime()) / 86400000;
					finalDaysDifference = Math.abs(days.intValue());
				}

				if (alertType.equalsIgnoreCase("PEP")) {
					Integer periodDbHigh = nameSliderToMap.get("PEP High Slider");
					Integer periodDbMedium = nameSliderToMap.get("PEP Medium Slider");
					Integer periodDbLow = nameSliderToMap.get("PEP Low Slider");
					
					if (alert.getRiskIndicators().equalsIgnoreCase("High")) {
						//Integer periodDb = nameSliderToMap.get("PEP High Slider");
						if (periodDbHigh != null && finalDaysDifference > periodDbHigh) {
							alert.setPeriodicReview(true);
							alertManagementService.saveOrUpdate(alert);
							alertsWithPeriodicTrue.add(alert);
						}

					} else if (alert.getRiskIndicators().equalsIgnoreCase("Medium")) {

						//Integer periodDb = nameSliderToMap.get("PEP Medium Slider");
						if (periodDbMedium != null && finalDaysDifference > periodDbHigh && finalDaysDifference < periodDbMedium ) {
							alert.setPeriodicReview(true);
							alertManagementService.saveOrUpdate(alert);
							alertsWithPeriodicTrue.add(alert);
						}
					} else {
						//Integer periodDb = nameSliderToMap.get("PEP Low Slider");
						if (periodDbLow != null && finalDaysDifference > periodDbMedium && finalDaysDifference < periodDbLow) {
							alert.setPeriodicReview(true);
							alertManagementService.saveOrUpdate(alert);
							alertsWithPeriodicTrue.add(alert);
						}
					}
				} else if (alertType.equalsIgnoreCase("SANCTION")) {
					Integer periodDbHigh = nameSliderToMap.get("Sanction High Slider");
					Integer periodDbMedium = nameSliderToMap.get("Sanction Medium Slider");
					Integer periodDbLow = nameSliderToMap.get("Sanction Low Slider");
					
					if (alert.getRiskIndicators().equalsIgnoreCase("High")) {
						//Integer periodDb = nameSliderToMap.get("Sanction High Slider");
						if (periodDbHigh != null && finalDaysDifference > periodDbHigh) {
							alert.setPeriodicReview(true);
							alertManagementService.saveOrUpdate(alert);
							alertsWithPeriodicTrue.add(alert);
						}

					} else if (alert.getRiskIndicators().equalsIgnoreCase("Medium")) {
						//Integer periodDb = nameSliderToMap.get("Sanction Medium Slider");
						if (periodDbMedium != null && finalDaysDifference > periodDbHigh && finalDaysDifference < periodDbMedium ) {
							alert.setPeriodicReview(true);
							alertManagementService.saveOrUpdate(alert);
							alertsWithPeriodicTrue.add(alert);
						}

					} else {
						//Integer periodDb = nameSliderToMap.get("Sanction Low Slider");
						if (periodDbLow != null && finalDaysDifference > periodDbMedium && finalDaysDifference < periodDbLow) {
							alert.setPeriodicReview(true);
							alertManagementService.saveOrUpdate(alert);
							alertsWithPeriodicTrue.add(alert);
						}

					}

				}
			}
			else{
				alertsWithPeriodicTrue.add(alert);
			}
		}

	}
	
	//@Scheduled(cron = "0 0 */2 ? * *")
	public void scheduledCreateNewAlerts() throws Exception {
				  
		   String requestId = null;		   
		   alertManagementService.createNewAlerts(requestId);
		
	}  
	
}
