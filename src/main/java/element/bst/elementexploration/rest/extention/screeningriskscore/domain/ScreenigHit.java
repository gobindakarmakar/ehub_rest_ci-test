package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreenigHit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<FieldDetails> fields= new ArrayList<FieldDetails>();
	
    private List<Rules> rules = new ArrayList<Rules>();
    @JsonProperty("source-id")
    private String sourceId;    
    private String id;
    @JsonProperty("watchlists")
    private String watchLists ;

	public List<FieldDetails> getFields() {
		return fields;
	}

	public void setFields(List<FieldDetails> fields) {
		this.fields = fields;
	}

	public List<Rules> getRules() {
		return rules;
	}

	public void setRules(List<Rules> rules) {
		this.rules = rules;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWatchLists() {
		return watchLists;
	}

	public void setWatchLists(String watchLists) {
		this.watchLists = watchLists;
	}

	
	
}
