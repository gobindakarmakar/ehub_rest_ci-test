package element.bst.elementexploration.rest.extention.entity.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.entity.domain.EntityType;
import element.bst.elementexploration.rest.extention.entity.dto.EntityTypeDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface EntityTypeService extends GenericService<EntityType, Long> {

	List<EntityTypeDto> getEntityRequirements();

}
