'use strict';
activitiApp.controller('AdvancedSearchController', advancedSearchController);

		advancedSearchController.$inject = [
			'$scope',
			'$rootScope',
			'$window',
			'TopPanelApiService'
		];
	    function advancedSearchController(
	    		$scope, 
	    		$rootScope,
	    		$window,
	    		TopPanelApiService){
//    	$scope.token = $rootScope.ehubObject.token;
    	var params = {
	    	token: $scope.token		    	
	    };
    	/*
		 * @purpose: on search change
		 * @created: 12 sep 2017
		 * @params: value(string)
		 * @returns: no
		 * @author: swathi
		 */
		$scope.onSearchChange = function(event, value){
			$scope.suggest = {
				"text": value,
				"completion":{
					"field":"suggest_entity"
				}
			};
			/*$scope.name = [];*/
			$scope.names = [];
			
			if(value !== ""){
				$("ul.typeahead").show();
				TopPanelApiService.getElasticSearchSuggest(params, $scope.suggest).then(function(response){
					$scope.items = response.data;
			        angular.forEach($scope.items.payload, function(item){
			        	$scope.arr = item.options;
			        	angular.forEach($scope.arr,function(t){	  
			        		$scope.names.push(t.text);
			        	});
			        });
			        /*$scope.names = $scope.name;*/
				}, function(error) {
			    });
			} else {
	            $("ul.typeahead").hide();
				$scope.names = [];
			}	
	};
	/*
	 * @purpose: search
	 * @created: 13 sep 2017
	 * @params: searchItem(string)
	 * @returns: no
	 * @author: swathi
	 */
	$scope.doSearch = function(searchItem) {
//		$state.go('enrichSearchItem',{'searchItem': searchItem});
		$("ul.typeahead").hide();
		$scope.searchText = '';
		var data =  {
    		"query": {
			"bool": {
				"must": [{
        			"prefix": {
        				"ner_location": "",
        				"ner_person": searchItem,
        				"ner_organization": ""
        			}
    			}],
    			"must_not": [],
    			"should": []
				}
			},
			"from": 0,
			"size": 10,
			"sort": [],
			"aggs": {}
		};
		TopPanelApiService.getElasticSearchData(params, data).then(function(response){
			
		}, function(error) {
	    });
	}
};
