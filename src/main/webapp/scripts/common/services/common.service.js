'use strict';
activitiModule.factory('CommonService',function(
		$http,
		EHUB_API,
		$rootScope,
		$routeParams,
		HostPathService,
		EHUB_FE_API,
		KYC_QUESTIONNAIRE_PATH,
		POLICY_ENFORCEMENT_PATH,
		ACTIVITI_FE_PATH){
	
	return {
		dashboarDropDownMenuItems: dashboarDropDownMenuItems,
		getSubMenuData :getSubMenuData,
		checkSubmenuDisable: checkSubmenuDisable,
		checkMainmenuDisable: checkMainmenuDisable
	}
	
	function dashboarDropDownMenuItems(){
		return [
			{ menu: 'Discover', content: ['Dashboard', 'Workspace', 'Investigation Console','Transaction Monitoring',
				 'Market Intelligence','Adverse Transactions', 'Transaction Intelligence'] 
			},
			{ menu: 'Enrich', content: ['Advanced Search', 'On Boarding'] 
		    },
		    { menu: 'Act', content: ['Entity', 'Cases', 'Link Analysis'] 
		    },
		    { menu: 'Predict', content: ['Personalization'] 
		    },
		    { menu: 'Manage', content: ['User Management', 'Big Data Workflow', 'App Manager','Data Management','System Monitoring', 'General Settings', 'Data Curation'] 
		    }
		]
	};
	/*
     * @purpose: disable submenus
     * @created: 24 oct 2017
     * @params: null
     * @return: no
     * @author: ankit
    */  
	function checkSubmenuDisable(value){
		if(value == "yes")
			return true;
		if(value == "no")
			return false;
		if(value == "maybe" && $routeParams.caseId)
			return false;
		else 
			return true;
	}
	/*
     * @purpose: disable main menu
     * @created: 21 feb 2018
     * @params: value
     * @return: boolean value
     * @author: swathi
    */  
	function checkMainmenuDisable(value){
		if(value == "yes")
			return true;
		if(value == "no")
			return false;
		if(value == "maybe" && $routeParams.caseId)
			return false;
		else 
			return true;
	}
	/*
     * @purpose: setting dashboardItems
     * @created: 18 jan 2018
     * @params: data(object)
     * @return: no
     * @author: Ankit
    */
	
	function getSubMenuData(submenuname,  content, dashboardItems, currentState, caseSeedDetails){
		var disable = checkSubmenuDisable(content);
		   if(disable)
			   return false;
		   HostPathService.setdashboardname(submenuname.name, dashboardItems);	
		   var submenuname = submenuname.name.replace(/ /g,'');
		   submenuname = submenuname.charAt(0).toLowerCase()+submenuname.slice(1);
		   console.log(submenuname)
		   if(submenuname == 'bigDataSciencePlatform'){
			   submenuname = 'workflow';
			   window.location.href= EHUB_FE_API + submenuname + '/'; 
		   }
		  if(submenuname == 'questionnaireBuilder'){
			   var url = KYC_QUESTIONNAIRE_PATH;
		   	   window.open(url, '_blank');
		   }
		   if(submenuname == 'policyEnforcement'){
			   var url = POLICY_ENFORCEMENT_PATH;
		   	   window.open(url, '_blank');
		   }
		   if(submenuname == 'dashboard'){
			   submenuname = 'discover';
			   window.location.href= EHUB_FE_API +'#/'+ submenuname; 
		   }
		   if(submenuname == 'investigationConsole'){
			   if($routeParams.caseId != undefined){
				   window.location.href= EHUB_FE_API +'#/'+ submenuname + '/' + $routeParams.caseId;
			   }			    
			   else{
				   window.location.href= EHUB_FE_API +'#/'+ submenuname + '/';
			   }
		   }
		   if(submenuname == 'advancedSearch'){
			   submenuname = 'enrich';
			   window.location.href= EHUB_FE_API +'#/'+ submenuname; 
		   }
		   if(submenuname == 'onboarding'){
			   submenuname = 'enrich';
			   window.location.href= EHUB_FE_API +'#/'+ submenuname + '?onBoard=true'; 
		   }
		   if(submenuname == 'underwriting'){
			   submenuname = 'underwriting';
			   window.location.href= EHUB_FE_API +'uploadDocuments/#!/'+ "landing";
		   }
		   if(submenuname == 'marketIntelligence'){
			   submenuname = 'mip';
			   window.location.href= EHUB_FE_API + submenuname + '/';  
		   }
		   if(submenuname == 'cases'){
			   submenuname ='casePageLanding';
			   window.location.href= EHUB_FE_API +'#/'+ submenuname; 
		   }
		   if(submenuname == 'auditTrail'){
			   submenuname ='auditTrail';
			   window.location.href= EHUB_FE_API +'#/'+ submenuname;
		   }
		   if(submenuname == 'entity'){
			   submenuname = 'entity';
			   var url;
			   if(currentState == 'actCase'){
				   url = EHUB_FE_API + submenuname + '/#!/company/' + caseSeedDetails.name;
			   }else{
				   url = EHUB_FE_API + submenuname + '/#!/company/oracle';
				}
			   	window.open(url, '_blank');	
		   }
		   if(submenuname == 'linkAnalysis'){
			   var url;
			   if(currentState == 'actCase'){
					url = EHUB_FE_API +'#/linkAnalysis?caseId=' + $routeParams.caseId;
				}else if($routeParams.query){
					url = EHUB_FE_API +'#/linkAnalysis?q=' + $routeParams.query;
				}else{
					url = EHUB_FE_API +'#/linkAnalysis';
				}
				window.open(url, '_blank');
		   }
		   if(submenuname == 'fraud'){
			   window.location.href= EHUB_FE_API + 'transactionIntelligence'; 
		   }
		   if(submenuname == 'leadGeneration'){
			   window.location.href= EHUB_FE_API + 'leadGeneration'; 
		   }
		   if(submenuname == 'transactionIntelligence'){
			   window.location.href= EHUB_FE_API + 'transactionIntelligence'; 
		   }
		   if(submenuname == 'adverseTransactions'){
			   submenuname = 'adverseTransactions';
			   window.location.href= EHUB_FE_API +'#/'+ submenuname; 
		   }
		   if(submenuname == 'workspace' || submenuname == 'systemMonitoring' || submenuname == 'appManager' || submenuname == 'dataManagement' || submenuname == 'generalSettings' || submenuname == 'dataCuration'){
			   window.location.href= EHUB_FE_API +'#/'+ submenuname; 
		   }
		   if(submenuname == 'transactionMonitoring'){
			   if($routeParams.caseId != undefined){
				   window.location.href= EHUB_FE_API +'#/'+ submenuname + '/' + $routeParams.caseId; 
			   }else{
				   window.location.href= EHUB_FE_API +'#/'+ submenuname;  
			   }
		   }
		   if(submenuname == 'decisionScoring'){
			  $rootScope.riskScoreModal();
		   }
		   if(submenuname == 'orchestration'){
			  /* var activityUrl = ACTIVITI_FE_PATH;*/
			  var activityUrl = ACTIVITI_FE_PATH+'id='+encodeURIComponent(JSON.stringify(localStorage.getItem('ehubObject')));
			   window.open(activityUrl, '_self');
		   }

		   	if(submenuname == 'documentParsing') {
				window.location.href = EHUB_FE_API + 'docparser/#!/' + "landing";
			}
			if(submenuname == 'sourceManagement') {
				window.location.href = EHUB_FE_API + 'sourceManagement/';
			}
			if(submenuname == 'personalization') {
				window.location.href = EHUB_FE_API + 'leadGeneration/';
			}
	}
});