package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CounterPartyService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Counter party API" },description="Manages alerted transactions data of counter parties")
@RestController
@RequestMapping("/api/counterParty")
public class CounterPartyController extends BaseController {

	@Autowired
	private CounterPartyService countryPartyService;

	@Autowired
	private TxDataService txDataService;

	@ApiOperation("Get total transaction and alert amount/count/ratio of activity type customers")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getActivity/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCouterPartyCountries(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(countryPartyService.getActivity(fromDate, toDate, filterDto),
				HttpStatus.OK);

	}

	@ApiOperation("Get total transaction and alert amount/count/ratio of each activity type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getActivityAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCouterPartyAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.getCustomerRiskAggregates(fromDate, toDate, filterDto),
				HttpStatus.OK);
	}

	@ApiOperation("Get total transaction and alert amount/count/ratio of all countries")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getGeoGraphic/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeoGraphic(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto,@RequestParam boolean isBankCountry) throws ParseException {
		return new ResponseEntity<>(countryPartyService.getGeoGraphic(fromDate, toDate, filterDto,isBankCountry), HttpStatus.OK);
	}

	@ApiOperation("Get total transaction and alert amount/count/ratio of each country or bank location")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getGeoGraphicAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeoGraphicAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto, @RequestParam boolean isBankCountry) throws ParseException {
		return new ResponseEntity<>(
				countryPartyService.getGeoGraphicAggregates(fromDate, toDate, filterDto, isBankCountry), HttpStatus.OK);
	}

	@ApiOperation("Get total transaction and alert amount/count/ratio of all banks")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getBanks/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getBanks(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(countryPartyService.getBanks(fromDate, toDate, filterDto), HttpStatus.OK);
	}

	@ApiOperation("Get total transaction and alert amount/count/ratio of each bank")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RiskCountAndRatioDto.class, responseContainer="List",message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Please ensure date format is yyyy-MM-dd"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message ="Could not process your request") })
	@PostMapping(value = "/getBankAggregates/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getBankAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(countryPartyService.getBankAggregates(fromDate, toDate, filterDto), HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/getViewAll/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" },
			consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getViewAll(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token, @RequestParam String type,
			HttpServletRequest request, @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "recordsPerPage", required = false) Integer recordsPerPage,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {

		List<RiskCountAndRatioDto> list = countryPartyService.getViewAll(fromDate, toDate, type, pageNumber,
				recordsPerPage,filterDto);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = countryPartyService.getViewAllCount(fromDate, toDate, type);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", list);
		jsonObject.put("paginationInformation", information);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);

	}

	/*@GetMapping(value = "/topCounterParties/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTopCounterParties(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token) throws ParseException {
		return new ResponseEntity<>(countryPartyService.getTopCounterParties(fromDate, toDate), HttpStatus.OK);

	}*/

	/*@GetMapping(value = "/addBankNames", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addBankNames(@RequestParam("token") String token) throws ParseException {
		return new ResponseEntity<>(countryPartyService.addBankNames(), HttpStatus.OK);

	}*/

	/*@PostMapping(value = "/getActivityByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getActivityByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		filterDto.setActivityType(true);
		filterDto.setActivityType(type);
		;
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}

	@PostMapping(value = "/getGeoGraphicByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGeoGraphicByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto,
			@RequestParam boolean isBankCountry) throws ParseException {
		filterDto.setCounterpartyCountry(type);
		filterDto.setCounterPartyCountry(true);
		;
		if (isBankCountry)
			filterDto.setBank(true);
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}

	@PostMapping(value = "/getBankByType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" }, consumes = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getBankByType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String type, @RequestBody FilterDto filterDto) throws ParseException {
		filterDto.setBankName(type);
		filterDto.setBank(true);
		return new ResponseEntity<>(txDataService.getAmountAndCountByType(fromDate, toDate, type, filterDto),
				HttpStatus.OK);

	}*/

}
