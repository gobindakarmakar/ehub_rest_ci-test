package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Parsed document response")
public class DocumentResponseDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Document Paragraph object")
	@JsonProperty("ContentTypeParagraph")
	private DocumentParagraphDto documentParagraphDto;
	@ApiModelProperty(value = "Document Table object")
	@JsonProperty("ContentTypeTable")
	private ContentTypeTableDto contentTypeTableDto;

	public DocumentResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public DocumentResponseDto(DocumentParagraphDto documentParagraphDto, ContentTypeTableDto contentTypeTableDto) {
		super();
		this.documentParagraphDto = documentParagraphDto;
		this.contentTypeTableDto = contentTypeTableDto;
	}



	public DocumentParagraphDto getDocumentParagraphDto() {
		return documentParagraphDto;
	}

	public void setDocumentParagraphDto(DocumentParagraphDto documentParagraphDto) {
		this.documentParagraphDto = documentParagraphDto;
	}



	public ContentTypeTableDto getContentTypeTableDto() {
		return contentTypeTableDto;
	}



	public void setContentTypeTableDto(ContentTypeTableDto contentTypeTableDto) {
		this.contentTypeTableDto = contentTypeTableDto;
	}

	
	

}
