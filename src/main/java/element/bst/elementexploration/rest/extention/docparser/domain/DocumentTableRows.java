package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_TABLE_ROWS")
public class DocumentTableRows implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String rowName;
	private Date updatedRecord;
	private DocumentTableColumns documentTableColumns;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "ROW_NAME_ANSWER",columnDefinition="LONGTEXT")
	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}
	@Column(name = "ANSWER_UPDATED_TIME")
	@UpdateTimestamp
	public Date getUpdatedRecord() {
		return updatedRecord;
	}

	public void setUpdatedRecord(Date updatedRecord) {
		this.updatedRecord = updatedRecord;
	}
	
	@ManyToOne
	@JoinColumn(name="DOCUMENT_TABLE_COLUMN_ID")
	public DocumentTableColumns getDocumentTableColumns() {
		return documentTableColumns;
	}

	public void setDocumentTableColumns(DocumentTableColumns documentTableColumns) {
		this.documentTableColumns = documentTableColumns;
	}

	

}
