package element.bst.elementexploration.rest.extention.menuitem.daoImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.LastVisitedDomainDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.LastVisitedDomains;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("lastVisitedDomainDao")
@Transactional("transactionManager")
public class LastVisitedDomainDaoImpl extends GenericDaoImpl<LastVisitedDomains, Long> implements LastVisitedDomainDao {

	public LastVisitedDomainDaoImpl() {
		super(LastVisitedDomains.class);
	}

	@Override
	public LastVisitedDomains findLastVisitedDomainByUserId(Long userId) {
		LastVisitedDomains existingRecord = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select lv from LastVisitedDomains lv where lv.userId = :idOfUser");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("idOfUser", userId);
			existingRecord = (LastVisitedDomains) query.getSingleResult();
		} catch (Exception e) {
			return existingRecord;
		}
		return existingRecord;
	}
}
