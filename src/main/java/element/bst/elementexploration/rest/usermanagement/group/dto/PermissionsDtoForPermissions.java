package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class PermissionsDtoForPermissions implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Privilege ID")
	private Long permissionId;

	@ApiModelProperty(value = "Privilege Name")
	private String itemName;

	@ApiModelProperty(value = "Status of permission")
	private Boolean active;

	// @JsonIgnore
	@ApiModelProperty(value = "Parent Permission ID")
	private Long parentPermissionId;

	@ApiModelProperty(value = "Created On")
	private Date createdOn;

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getParentPermissionId() {
		return parentPermissionId;
	}

	public void setParentPermissionId(Long parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
