package element.bst.elementexploration.rest.exception;

public class PermissionDeniedException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    

	private  String message;
	
	public static final String ACCESS_DENIED_MSG="You do not have sufficient permission to perform this operation";
	
	public PermissionDeniedException(String message) {
		super();
		this.message = message;
	}



	public PermissionDeniedException() {

		super();
		message = ACCESS_DENIED_MSG;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
	    StringBuilder builder = new StringBuilder();
	    builder.append("PermissionDeniedException [message=");
	    builder.append(message);
	    builder.append("]");
	    return builder.toString();
	}

}
