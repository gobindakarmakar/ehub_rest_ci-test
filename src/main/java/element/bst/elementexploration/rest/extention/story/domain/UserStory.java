package element.bst.elementexploration.rest.extention.story.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bst_story")
public class UserStory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_story_id")
	private Long id;
	
	@Column(name = "fk_user_id")
	private Long userId;
	
	@Column(name = "story_id")
	private String storyId;
	
	@Column(name = "created_on")
	private Date createdDate;
	
	@Column(name = "modified_on")
	private Date modifiedDate;

	public UserStory() {
		super();
	}

	public UserStory(Long id, Long userId, String storyId, Date createdDate, Date modifiedDate) {
		super();
		this.id = id;
		this.userId = userId;
		this.storyId = storyId;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStoryId() {
		return storyId;
	}

	public void setStoryId(String storyId) {
		this.storyId = storyId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "Story [id=" + id + ", userId=" + userId + ", storyId=" + storyId + ", createdDate=" + createdDate
				+ ", modifiedDate=" + modifiedDate + "]";
	}

}
