package element.bst.elementexploration.rest.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class TestJsonFileRead {

	public static JSONObject getJsonFromMap(Map<String, Object> map) throws JSONException {
	    JSONObject jsonData = new JSONObject();
	    for (String key : map.keySet()) {
	        Object value = map.get(key);
	        if (value instanceof Map<?, ?>) {
	            value = getJsonFromMap((Map<String, Object>) value);
	        }
	        jsonData.put(key, value);
	    }
	    return jsonData;
	}
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		Map<String, Object> data = new HashMap<String, Object>();
	    data.put( "name", "Mars" );
	    data.put( "age", 32 );
	    data.put( "city", "NY" );
	    System.out.println(TestJsonFileRead.getJsonFromMap(data));

			// TODO Auto-generated method stub
		/*	ObjectMapper mapper = new ObjectMapper();

			SystemSettingsDto jsonRead = mapper.readValue(
					new File("/home/ahextech/PAUL/ELEMENT/element-backend/src/main/resources/settingsJson.json"),
					SystemSettingsDto.class);
			String prettyStaff1 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonRead);
			System.out.println(prettyStaff1);
			
			if(jsonRead!=null) {
				
			}
*/
		
		}

}
