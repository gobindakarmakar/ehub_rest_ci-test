package element.bst.elementexploration.rest.settings.listManagement.dto;

public class IconDto {


	private Long iconId;
	private String name;
	public Long getIconId() {
		return iconId;
	}
	public void setIconId(Long iconId) {
		this.iconId = iconId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
