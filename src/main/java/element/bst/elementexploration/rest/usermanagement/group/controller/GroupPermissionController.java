package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionDtoWithParentId;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionWithOutObjectsDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupPermissionsDtoForPermissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.RolesDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupPermissionService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "GroupPermission API" })
@RestController
@RequestMapping("/api/groupPermission")
public class GroupPermissionController extends BaseController {

	@Autowired
	GroupPermissionService groupPermissionService;

	@Autowired
	private UsersService usersService;
	
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private PrivilegesService privilegesService;
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	UsersDao userDao;

	@Autowired
	AuditLogDao auditLogDao;
	

	@SuppressWarnings("unused")
	@ApiOperation("Updates Group Permission")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = GenericReturnObject.class, message = ElementConstants.ROLE_ADDED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateGroupPermission", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdatePrivilege(@RequestBody @Valid List<GroupPermissionWithOutObjectsDto> grouppermissiondtoList,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		List<GroupPermission> resultantPermissions = new ArrayList<>();
		GenericReturnObject message = new GenericReturnObject();
		//boolean status = false;
		for (GroupPermissionWithOutObjectsDto grouppermissiondto : grouppermissiondtoList) {
			Groups group = null;
			Roles role = null;
			Permissions permission = null;
			if (grouppermissiondto != null) {
				if (grouppermissiondto.getGroupPrivilegeId() == null) {
					if (grouppermissiondto.getGroupId() != null) {
						group = groupsService.find(grouppermissiondto.getGroupId());
					}else {
						List<Groups> analystGroups=groupsService.findAll().stream().filter(o -> o.getName().equalsIgnoreCase("Analyst")).collect(Collectors.toList());
						if(analystGroups!=null && analystGroups.size()>0)
							group=analystGroups.get(0);
					}
					if (grouppermissiondto.getRoleId() != null) {
						role = rolesService.find(grouppermissiondto.getRoleId());
						if (role == null) {
							message.setResponseMessage(ElementConstants.ROLE_ID_NOT_FOUND);
							message.setStatus(ElementConstants.ERROR_STATUS);
							return new ResponseEntity<>(message, HttpStatus.OK);
						} else {
							if (grouppermissiondto.getPermissionId() != null) {
								permission = privilegesService.find(grouppermissiondto.getPermissionId());
							}
							/*	if (permission == null) {
									message.setResponseMessage(ElementConstants.PERMISSION_ID_NOT_FOUND);
									message.setStatus(ElementConstants.ERROR_STATUS);
									return new ResponseEntity<>(message, HttpStatus.OK);
<<<<<<< HEAD
								} else {
									if (grouppermissiondto.getPermissionId() != null) {
										permission = privilegesService.find(grouppermissiondto.getPermissionId());
										if (permission == null) {
											message.setResponseMessage(ElementConstants.PERMISSION_ID_NOT_FOUND);
											message.setStatus(ElementConstants.ERROR_STATUS);
											return new ResponseEntity<>(message, HttpStatus.OK);
										} else {
											if (grouppermissiondto.getPermissionLevel() != null) {
												GroupPermission newGroupPermission = new GroupPermission();
												newGroupPermission.setGroupId(group);
												newGroupPermission.setPermissionId(permission);
												newGroupPermission.setRoleId(role);
												newGroupPermission
														.setPermissionLevel(grouppermissiondto.getPermissionLevel());
												resultantPermissions
														.add(groupPermissionService.save(newGroupPermission));
												status = true;
												AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
														ElementConstants.GROUP_PERMISSION_TYPE, null,
														ElementConstants.GROUP_PERMISSION_ADD_ACTION);
												Users admin = null;
												if (getCurrentUserId() != null){
													admin = usersService.find(getCurrentUserId());
													admin = usersService.prepareUserFromFirebase(admin);
												}
												log.setDescription(admin.getFirstName() + " " + admin.getLastName()
														+ ElementConstants.GROUP_PERMISSION_ADD_ACTION
														+ newGroupPermission.getPermissionLevel());
												log.setUserId(admin.getUserId());
												log.setName(ElementConstants.GROUP_PERMISSION_TYPE);
												auditLogService.save(log);
											}
										}
									} else {
										message.setResponseMessage(ElementConstants.PERMISSION_ID_EMPTY);
										message.setStatus(ElementConstants.ERROR_STATUS);
										return new ResponseEntity<>(message, HttpStatus.OK);
									}
								}
							} else {
								message.setResponseMessage(ElementConstants.ROLE_ID_EMPTY);
=======
								} else {*/
									//if (grouppermissiondto.getPermissionLevel() != null) {
										GroupPermission newGroupPermission = new GroupPermission();
										/*if (group != null)
											newGroupPermission.setGroupId(group);*/
										if(permission!=null)
										newGroupPermission.setPermissionId(permission);
										newGroupPermission.setRoleId(role);
										if (null != grouppermissiondto.getPermissionLevel())
											newGroupPermission
													.setPermissionLevel(grouppermissiondto.getPermissionLevel());
										else
											newGroupPermission.setPermissionLevel(0);
										resultantPermissions.add(groupPermissionService.save(newGroupPermission));
										//status = true;
										AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
												ElementConstants.GROUP_PERMISSION_TYPE, null,
												ElementConstants.GROUP_PERMISSION_ADD_ACTION);
										Users admin = null;
										if (getCurrentUserId() != null)
											admin = usersService.find(getCurrentUserId());

										log.setDescription(admin.getFirstName() + " " + admin.getLastName()
												+ ElementConstants.GROUP_PERMISSION_ADD_ACTION
												+ newGroupPermission.getPermissionLevel());
										log.setUserId(admin.getUserId());
										log.setName(ElementConstants.GROUP_PERMISSION_TYPE);
										auditLogService.save(log);
									//}
								//}
							/*} else {
								message.setResponseMessage(ElementConstants.PERMISSION_ID_EMPTY);
>>>>>>> 80197c40d68f534f20ecc547a6fc8550b3c2883e
								message.setStatus(ElementConstants.ERROR_STATUS);
								return new ResponseEntity<>(message, HttpStatus.OK);
							}*/
						}
					} else {
						message.setResponseMessage(ElementConstants.ROLE_ID_EMPTY);
						message.setStatus(ElementConstants.ERROR_STATUS);
						return new ResponseEntity<>(message, HttpStatus.OK);
					}
				} else {
					GroupPermission existingGroupPermission = groupPermissionService
							.find(grouppermissiondto.getGroupPrivilegeId());
					if (existingGroupPermission != null) {
						if (grouppermissiondto.getGroupId() != null) {
							group = groupsService.find(grouppermissiondto.getGroupId());
						}else {
							List<Groups> analystGroups=groupsService.findAll().stream().filter(o -> o.getName().equalsIgnoreCase("Analyst")).collect(Collectors.toList());
							if(analystGroups!=null && analystGroups.size()>0)
								group=analystGroups.get(0);
						}
						if (grouppermissiondto.getRoleId() != null) {
							role = rolesService.find(grouppermissiondto.getRoleId());
							if (role == null) {
								return new ResponseEntity<>(new ResponseMessage(ElementConstants.ROLE_ID_NOT_FOUND),
										HttpStatus.OK);
							} else {
								if (grouppermissiondto.getPermissionId() != null) {
									permission = privilegesService.find(grouppermissiondto.getPermissionId());
								}
									/*if (permission == null) {
										return new ResponseEntity<>(
												new ResponseMessage(ElementConstants.PERMISSION_ID_NOT_FOUND),
												HttpStatus.OK);
									} else {*/
										//if (grouppermissiondto.getPermissionLevel() != null) {
											/*if (group != null)
												existingGroupPermission.setGroupId(group);*/
											if(permission!=null)
											existingGroupPermission.setPermissionId(permission);
											existingGroupPermission.setRoleId(role);
											if (null != grouppermissiondto.getPermissionLevel())
												existingGroupPermission
														.setPermissionLevel(grouppermissiondto.getPermissionLevel());
											else
												existingGroupPermission.setPermissionLevel(0);
											groupPermissionService.saveOrUpdate(existingGroupPermission);
											resultantPermissions.add(existingGroupPermission);
											//status = true;
											AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
													ElementConstants.GROUP_PERMISSION_TYPE, null,
													ElementConstants.GROUP_PERMISSION_ADD_ACTION);
											Users admin = null;
											if (getCurrentUserId() != null)
												admin = usersService.find(getCurrentUserId());

											log.setDescription(admin.getFirstName() + " " + admin.getLastName()
													+ ElementConstants.GROUP_PERMISSION_ADD_ACTION
													+ existingGroupPermission.getPermissionLevel());
											log.setUserId(admin.getUserId());
											log.setName(ElementConstants.GROUP_PERMISSION_TYPE);
											auditLogService.save(log);
										//}
									//}
								/*} else {
									message.setResponseMessage(ElementConstants.PERMISSION_ID_EMPTY);
									message.setStatus(ElementConstants.ERROR_STATUS);
									return new ResponseEntity<>(message, HttpStatus.OK);
								}*/
							}
						} else {
							message.setResponseMessage(ElementConstants.ROLE_ID_EMPTY);
							message.setStatus(ElementConstants.ERROR_STATUS);
							return new ResponseEntity<>(message, HttpStatus.OK);
						}

					} else {
						message.setResponseMessage(ElementConstants.GROUP_PERMISSION_ID_NOT_FOUND);
						message.setStatus(ElementConstants.ERROR_STATUS);
						return new ResponseEntity<>(message, HttpStatus.OK);
					}

				}
			}
		}
		JSONArray result = new JSONArray(resultantPermissions);
		if (grouppermissiondtoList.size() == resultantPermissions.size()) {
			message.setResponseMessage(ElementConstants.GROUP_PERMISSION_UPDATE_SUCCESFUL);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			message.setData(result.toString());
			return new ResponseEntity<>(message, HttpStatus.OK);

		}
		if (resultantPermissions.size() == 0) {
			message.setResponseMessage(ElementConstants.GROUP_PERMISSION_UPDATE_FAIL);
			message.setStatus(ElementConstants.ERROR_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		} else {
			message.setResponseMessage(ElementConstants.GROUP_PERMISSION_SOME_UPDATE_FAIL);
			message.setStatus(ElementConstants.ERROR_STATUS);
			message.setData(result.toString());
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
	}
	
	@ApiOperation("Gets Permissions for Roles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getRolePermissions", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllRoles(@RequestParam(required = false) String roleName,
			@RequestParam(required = false) Long roleId, @RequestParam String token) throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<GroupPermissionDtoWithParentId> rolePermissions = new ArrayList<GroupPermissionDtoWithParentId>();
		Long finalRoleId = null;
		if (roleName != null && roleId == null) {
			Roles roleByName = rolesService.findRoleByName(roleName);
			if (roleByName != null)
				finalRoleId = roleByName.getRoleId();
		} else {
			finalRoleId = roleId;
		}
		if (finalRoleId != null) {
			rolePermissions = groupPermissionService.getGroupPermissionByRoleIdNew(finalRoleId);
		}
		JSONArray jsonArray = new JSONArray(rolePermissions);
		message.setResponseMessage(ElementConstants.FETCH_ROLES_SUCCESSFUL);
		message.setData(jsonArray.toString());
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Removes Permission from role")
	@ApiResponses(value = { @ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/removeRolePermissions", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> removeRolePermissions(@RequestParam(required = true) Long permissionId,
			@RequestParam(required = true) Long roleId, @RequestParam String token) throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		GroupPermission existingPermission = groupPermissionService.findGroupPermissionByRoleIdAndPermissionId(roleId,
				permissionId, getCurrentUserId());
		Users currentUser=usersService.find(getCurrentUserId());
		currentUser = usersService.prepareUserFromFirebase(currentUser);
		RolesDto role=rolesService.getRoleByIdOrName(roleId, null);
		Long deletedId=null;
		if (existingPermission != null && existingPermission.getGroupPrivilegeId() != null && currentUser != null) {
			deletedId = existingPermission.getGroupPrivilegeId();
			PermissionsWithIdDto permisionIdDto = privilegesService
					.findPermissionById(existingPermission.getPermissionId().getPermissionId());
			//groupPermissionService.delete(existingPermission);
			groupPermissionService.deleteGroupPermissionById(deletedId);
			message.setResponseMessage(ElementConstants.DELETE_GROUP_PERMISSIONS_SUCCESSFUL);
			message.setData(permissionId);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.GROUP_PERMISSION_UNASSIGN_ACTION,
					null, ElementConstants.GROUP_PERMISSION_TYPE);
			log.setDescription(currentUser.getFirstName() + " " + currentUser.getLastName() + " "
					+ ElementConstants.UNASSINED_GROUP_PERMISSION + " " + permisionIdDto.getItemName()
					+ " from the role " + role.getRoleName());
			log.setUserId(currentUser.getUserId());
			log.setTypeId(deletedId);
			log.setName(ElementConstants.GROUP_PERMISSION_UNASSIGN_ACTION);
			auditLogService.save(log);
		} else {
			message.setResponseMessage(ElementConstants.GROUP_PERMISSION_NOT_FOUND);
			message.setData(permissionId);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}

		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Gets Permissions for Roles List")
	@ApiResponses(value = { @ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getPermissionsByRolesList", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getPermissionsByRolesList(@RequestBody List<Long> roleIds, @RequestParam String token)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<GroupPermissionsDtoForPermissions> permissionsArray = new ArrayList<>();
		if (roleIds != null && roleIds.size() > 0)
			permissionsArray = groupPermissionService.getPermissionsByRolesList(roleIds);
		message.setResponseMessage(ElementConstants.FETCH_ROLES_SUCCESSFUL);
		message.setData(permissionsArray);
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	/*
	 * Replaced with the API with same name in RoleGroupController
	 * 
	 * */
	@Deprecated
	@ApiOperation("API to get Roles of a group")
	@ApiResponses(value = { @ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getRolesByGroup", produces = {"application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getRolesByGroup(@RequestBody List<Long> groupIds, @RequestParam String token)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<GroupPermissionsDtoForPermissions> permissionsList = groupPermissionService.getRolesByGroup(groupIds);
		List<GroupPermissionsDtoForPermissions> filteredList=null;
		if(permissionsList!=null && permissionsList.size()>0) {
			Map<Long,GroupPermissionsDtoForPermissions> filteredData= new HashMap<>();
			for(GroupPermissionsDtoForPermissions eachObj:permissionsList) {
				filteredData.put(eachObj.getRoleId().getRoleId(), eachObj);
			}
			if(filteredData!=null&&filteredData.size()>0) {
				filteredList=new ArrayList<>(filteredData.values());
			}
		}
		JSONArray groupsArray = new JSONArray();
		if (filteredList != null && filteredList.size()>0) {
			for (GroupPermissionsDtoForPermissions eachGroup : filteredList) {
				Map<Long, Long> count = userRolesService.getUsersCountByRole(eachGroup.getRoleId().getRoleId());
				JSONObject groupJson = new JSONObject(eachGroup);
				JSONObject roleJson = null;
				if (groupJson != null && groupJson.has("roleId")) {
					roleJson = groupJson.getJSONObject("roleId");
				}
				if (count != null && count.size() > 0 && count.containsKey(eachGroup.getRoleId().getRoleId())) {
					roleJson.put("noOfUsers", count.get(eachGroup.getRoleId().getRoleId()));
				}
				groupsArray.put(groupJson);

			}
		}
		message.setResponseMessage(ElementConstants.FETCH_ROLES_SUCCESSFUL);
		message.setData(groupsArray.toString());
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
}
