package element.bst.elementexploration.rest.document.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Document user and case")
public class DocumentUserAndCaseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("Document details")
	private DocumentVaultDTO documentVault;
	
	@ApiModelProperty("List of user linked with document")
	private List<DocumentUserMappingDTO> docUserList;
	
	@ApiModelProperty("List of cases linked with document")
	private List<CaseDto> caseList;

	public DocumentVaultDTO getDocumentVault() {
		return documentVault;
	}

	public void setDocumentVault(DocumentVaultDTO documentVault) {
		this.documentVault = documentVault;
	}

	public List<DocumentUserMappingDTO> getDocUserList() {
		return docUserList;
	}

	public void setDocUserList(List<DocumentUserMappingDTO> docUserList) {
		this.docUserList = docUserList;
	}

	public List<CaseDto> getCaseList() {
		return caseList;
	}

	public void setCaseList(List<CaseDto> caseList) {
		this.caseList = caseList;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
