package element.bst.elementexploration.rest.settings.serviceimpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.questionnairebuilder.dto.QuestionnaireDto;
import element.bst.elementexploration.rest.extention.questionnairebuilder.service.QuestionnaireBuilderService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.dao.AttributesDao;
import element.bst.elementexploration.rest.settings.dao.SettingsDao;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.dto.AttributeDto;
import element.bst.elementexploration.rest.settings.dto.SelectedObject;
import element.bst.elementexploration.rest.settings.dto.SettingsDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsRequestDto;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.AttributesService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.EntityConverter;
import element.bst.elementexploration.rest.util.SystemSettingTypes;

/**
 * @author suresh
 *
 */
@Service("settingsService")
public class SettingsServiceImpl extends GenericServiceImpl<Settings, Long> implements SettingsService {

	@Autowired
	public SettingsServiceImpl(GenericDao<Settings, Long> genericDao) {
		super(genericDao);
	}

	public SettingsServiceImpl() {
	}

	@Autowired
	SettingsService settingsService;

	@Autowired
	AttributesService attributeService;

	@Autowired
	AttributesDao attributesDao;

	@Autowired
	SettingsDao settingsDao;

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private QuestionnaireBuilderService builderService;

	@Value("${questionarie_request_endpoint}")
	private String QUESTIONARIE_REQUEST_ENDPOINT;

	@Autowired
	ListItemService listItemService;

	/*private Settings settings;*/
	
	@Value("${is_qb_enabled}")
		private Boolean QB_ENABLE;
	
	@Override
	@Transactional("transactionManager")
	public SystemSettingsDto fetchSystemSettings(String searchKey) {
		SystemSettingsDto systemSettingDto = new SystemSettingsDto();
		try {
			List<Settings> settingsList = null;
			if (searchKey != null) {
				settingsList = settingsDao.fetchAllSettings(searchKey);
			} else {
				settingsList = settingsService.findAll();
			}
			List<SettingsDto> generalSettingsDtoList = new ArrayList<SettingsDto>();
			List<SettingsDto> mailSettingsDtoList = new ArrayList<SettingsDto>();
			List<SettingsDto> listManagementSettingsDtoList = new ArrayList<SettingsDto>();
			List<SettingsDto> alertManagementSettingsDtoList = new ArrayList<SettingsDto>();
			List<SettingsDto> userManagementSettinsList = new ArrayList<SettingsDto>();
			for (Settings systemSettings : settingsList) {
				if (systemSettings.getSystemSettingType().equals("GENERAL_SETTING")) {
					SettingsDto settingsDto = new SettingsDto();
					settingsDto.setSettingId(systemSettings.getId());
					settingsDto.setName(systemSettings.getName());
					settingsDto.setSection(systemSettings.getSection());
					settingsDto.setType(systemSettings.getType());
					if (systemSettings.getCustomerLogoImage() != null)
						settingsDto.setCustomerLogoImage(systemSettings.getCustomerLogoImage());
					settingsDto.setSystemSettingType(systemSettings.getSystemSettingType());
					
						if (settingsDto.getName().equalsIgnoreCase("Questionnaries")
								|| settingsDto.getName().equalsIgnoreCase("Onboarding Questionnaire")) {
							if (QB_ENABLE) {
							List<SelectedObject> selectedObjectsList = new ArrayList<SelectedObject>();
							List<QuestionnaireDto> questionnaireDtos = builderService.getActiveQuestionnaire();
							for (QuestionnaireDto questionarie : questionnaireDtos) {
								SelectedObject selectedObjectOne = new SelectedObject();
								String url = QUESTIONARIE_REQUEST_ENDPOINT + questionarie.getQuestionnaireId()
										+ "?newtest=Y&lang=en";
								selectedObjectOne.setAttributeId(Long.valueOf(questionarie.getQuestionnaireId()));
								selectedObjectOne.setAttributeValue(url);
								selectedObjectOne.setAttributeName(questionarie.getQuestionnaireTitle());
								// selectedObjectOne.set
								if (!StringUtils.isEmpty(systemSettings.getDefaultValue()) && systemSettings
										.getDefaultValue().equalsIgnoreCase(questionarie.getQuestionnaireId()))
									selectedObjectOne.setSelected(true);
								else
									selectedObjectOne.setSelected(false);
								selectedObjectsList.add(selectedObjectOne);
								settingsDto.setOptions(selectedObjectsList);
							}
						}
					} else {
						List<Attributes> attributesList = systemSettings.getAttributesList();
						List<SelectedObject> selectedObjectsList = new ArrayList<SelectedObject>();
						for (Attributes attributes : attributesList) {
							SelectedObject selectedObjectOne = new SelectedObject();
							selectedObjectOne.setAttributeName(attributes.getAttributeName());
							selectedObjectOne.setSelected(attributes.isSelected());
							if (attributes.isSelected() == true)
								settingsDto.setSelectedValue(systemSettings.getDefaultValue());
							selectedObjectOne.setAttributeValue(attributes.getAttributeValue());
							selectedObjectOne.setAttributeId(attributes.getId());
							selectedObjectsList.add(selectedObjectOne);
							settingsDto.setOptions(selectedObjectsList);
						}
					}
					if(settingsDto.getName().equalsIgnoreCase("Languages")) {
						settingsDto.setOptions(null);
					}
					if (settingsDto.getSelectedValue() == null || settingsDto.getSelectedValue() == "")
					{
						settingsDto.setSelectedValue(systemSettings.getDefaultValue());
						settingsDto.setDefaultValue(systemSettings.getDefaultValue());
					}
					if (settingsDto.getName().equals("Light Theme") || settingsDto.getName().equals("Dark Theme")) {
						List<Settings> generalSettings=	settingsDao.getSystemSettingsByType("GENERAL_SETTING");
						generalSettings= generalSettings.stream().filter(s -> ((s.getName().equalsIgnoreCase("Dark Theme")||s.getName().equalsIgnoreCase("Light Theme")))).filter(s -> s.getDefaultValue()!=null).collect(Collectors.toList());
						if(generalSettings!=null && generalSettings.size()>0)
						settingsDto.setDefaultValue(generalSettings.get(0).getDefaultValue());
						settingsDto.setSelectedValue(generalSettings.get(0).getDefaultValue());
					}
					generalSettingsDtoList.add(settingsDto);
				} // general settings if close

				if (systemSettings.getSystemSettingType().equals("MAIL_SETTING")) {
					SettingsDto settingsDto = new SettingsDto();
					settingsDto.setSettingId(systemSettings.getId());
					settingsDto.setName(systemSettings.getName());
					settingsDto.setSection(systemSettings.getSection());
					settingsDto.setType(systemSettings.getType());
					if (systemSettings.getMaximumLength() != null)
						settingsDto.setMaximumLength(systemSettings.getMaximumLength());
					settingsDto.setRequired(systemSettings.isRequired());
					settingsDto.setSelectedValue(systemSettings.getDefaultValue());
					settingsDto.setValidation(systemSettings.getValidation());
					settingsDto.setSystemSettingType(systemSettings.getSystemSettingType());
					mailSettingsDtoList.add(settingsDto);
				} // mail settings if close
				
				//	alert management settings
				if (systemSettings.getSystemSettingType().equals("ALERT_MANAGEMENT")) {
					
					if(systemSettings.getName().equalsIgnoreCase("Predefined answers for :")){
						SettingsDto settingsDto = new SettingsDto();
						settingsDto.setSettingId(systemSettings.getId());
						settingsDto.setName(systemSettings.getName());
						settingsDto.setSection(systemSettings.getSection());
						settingsDto.setType(systemSettings.getType());
						if (systemSettings.getMaximumLength() != null)
							settingsDto.setMaximumLength(systemSettings.getMaximumLength());
						settingsDto.setRequired(systemSettings.isRequired());
						settingsDto.setSelectedValue(systemSettings.getDefaultValue());
						settingsDto.setValidation(systemSettings.getValidation());
						settingsDto.setSystemSettingType(systemSettings.getSystemSettingType());
						
						List<ListItemDto>  listAlertStatusesDto = listItemService.getListItemByListType("Alert Status");				
						//List<Attributes> attributesListAlertSettings = new ArrayList<Attributes>();
						
						List<SelectedObject> selectedObjectsList = new ArrayList<SelectedObject>();
						//if this flag is true means default value is null or empty
						boolean isdefaultEmpty = false;
						for(ListItemDto dto : listAlertStatusesDto){
							SelectedObject selectedObjectOne = new SelectedObject();
							selectedObjectOne.setAttributeName(dto.getCode());
							if(dto.getCode().equalsIgnoreCase(systemSettings.getDefaultValue())){
								selectedObjectOne.setSelected(true);
							}else if(StringUtils.isBlank(systemSettings.getDefaultValue()) && isdefaultEmpty == false){
								selectedObjectOne.setSelected(true);
								isdefaultEmpty = true;
							}else{
								selectedObjectOne.setSelected(false);
							}
							settingsDto.setSelectedValue(systemSettings.getDefaultValue());
							selectedObjectOne.setAttributeValue(dto.getCode());
							///selectedObjectOne.setAttributeId(dto.getListItemId());
							selectedObjectsList.add(selectedObjectOne);
							settingsDto.setOptions(selectedObjectsList);
						}
						alertManagementSettingsDtoList.add(settingsDto);
						alertManagementSettingsDtoList.sort(Comparator.comparing(SettingsDto::getName));
					}else{
						SettingsDto settingsDto = new SettingsDto();
						settingsDto.setSettingId(systemSettings.getId());
						settingsDto.setName(systemSettings.getName());
						settingsDto.setSection(systemSettings.getSection());
						settingsDto.setType(systemSettings.getType());
						if (systemSettings.getMaximumLength() != null)
							settingsDto.setMaximumLength(systemSettings.getMaximumLength());
						settingsDto.setRequired(systemSettings.isRequired());
						settingsDto.setSelectedValue(systemSettings.getDefaultValue());
						settingsDto.setValidation(systemSettings.getValidation());
						settingsDto.setSystemSettingType(systemSettings.getSystemSettingType());
						settingsDto.setSelectedValueMax(systemSettings.getSelectedValueMax());
						settingsDto.setSliderFrom(systemSettings.getSliderFrom());
						settingsDto.setSliderTo(systemSettings.getSliderTo());
						
						List<Attributes> attributesList = systemSettings.getAttributesList();
						List<SelectedObject> selectedObjectsList = new ArrayList<SelectedObject>();
						for (Attributes attributes : attributesList) {
							SelectedObject selectedObjectOne = new SelectedObject();
							selectedObjectOne.setAttributeName(attributes.getAttributeName());
							selectedObjectOne.setSelected(attributes.isSelected());
							if (attributes.isSelected() == true)
								settingsDto.setSelectedValue(systemSettings.getDefaultValue());
							selectedObjectOne.setAttributeValue(attributes.getAttributeValue());
							selectedObjectOne.setAttributeId(attributes.getId());
							selectedObjectsList.add(selectedObjectOne);
							settingsDto.setOptions(selectedObjectsList);
						}
						alertManagementSettingsDtoList.add(settingsDto);
						alertManagementSettingsDtoList.sort(Comparator.comparing(SettingsDto::getName));
					}
				}// alert management settings close
				
				if (systemSettings.getSystemSettingType().equals("LIST_MANAGEMENT")) {
					SettingsDto settingsDto = new SettingsDto();
					settingsDto.setSettingId(systemSettings.getId());
					settingsDto.setName(systemSettings.getName());
					settingsDto.setSection(systemSettings.getSection());
					settingsDto.setType(systemSettings.getType());
					if (systemSettings.getMaximumLength() != null)
						settingsDto.setMaximumLength(systemSettings.getMaximumLength());
					settingsDto.setRequired(systemSettings.isRequired());
					settingsDto.setSelectedValue(systemSettings.getDefaultValue());
					settingsDto.setValidation(systemSettings.getValidation());
					settingsDto.setSystemSettingType(systemSettings.getSystemSettingType());
					listManagementSettingsDtoList.add(settingsDto);
					
					List<Attributes> attributesList = systemSettings.getAttributesList();
					List<SelectedObject> selectedObjectsList = new ArrayList<SelectedObject>();
					for (Attributes attributes : attributesList) {
						SelectedObject selectedObjectOne = new SelectedObject();
						selectedObjectOne.setAttributeName(attributes.getAttributeName());
						selectedObjectOne.setSelected(attributes.isSelected());
						if (attributes.isSelected() == true)
							settingsDto.setSelectedValue(systemSettings.getDefaultValue());
						selectedObjectOne.setAttributeValue(attributes.getAttributeValue());
						selectedObjectOne.setAttributeId(attributes.getId());
						selectedObjectsList.add(selectedObjectOne);
						settingsDto.setOptions(selectedObjectsList);
					}
				} // List settings if close
				
				if (systemSettings.getSystemSettingType().equals(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString())) {
					SettingsDto settingsDto = new SettingsDto();
					settingsDto.setSettingId(systemSettings.getId());
					settingsDto.setName(systemSettings.getName());
					if(null!=systemSettings.getSection())
					settingsDto.setSection(systemSettings.getSection());
					settingsDto.setType(systemSettings.getType());
					if (systemSettings.getMaximumLength() != null)
						settingsDto.setMaximumLength(systemSettings.getMaximumLength());
					settingsDto.setRequired(systemSettings.isRequired());
					settingsDto.setSelectedValue(systemSettings.getDefaultValue());
					settingsDto.setValidation(systemSettings.getValidation());
					settingsDto.setSystemSettingType(systemSettings.getSystemSettingType());
					if(systemSettings.getName().equals("Enable Authentication")|| systemSettings.getName().equals("Pending users deactivation after")||systemSettings.getName().equals(ElementConstants.TWO_WAY_AUTHENTICATION_STRING))
						settingsDto.setDifferentiator("Main Key");
					userManagementSettinsList.add(settingsDto);
					
					List<Attributes> attributesList = systemSettings.getAttributesList();
					List<SelectedObject> selectedObjectsList = new ArrayList<SelectedObject>();
					for (Attributes attributes : attributesList) {
						SelectedObject selectedObjectOne = new SelectedObject();
						selectedObjectOne.setAttributeName(attributes.getAttributeName());
						selectedObjectOne.setSelected(attributes.isSelected());
						if (attributes.isSelected() == true)
							settingsDto.setSelectedValue(systemSettings.getDefaultValue());
						selectedObjectOne.setAttributeValue(attributes.getAttributeValue());
						selectedObjectOne.setAttributeId(attributes.getId());
						selectedObjectsList.add(selectedObjectOne);
						settingsDto.setOptions(selectedObjectsList);
					}
				}
				
				systemSettingDto.setListManagementSettingsList(listManagementSettingsDtoList);
				systemSettingDto.setGeneralSettingsList(generalSettingsDtoList);
				systemSettingDto.setMailSettingsList(mailSettingsDtoList);
				systemSettingDto.setAlertManagementSettingsList(alertManagementSettingsDtoList);
				systemSettingDto.setUserManagementSettingsList(userManagementSettinsList);

			} // main for close
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return systemSettingDto;
	}

	@Override
	@Transactional("transactionManager")
	public boolean updateSystemSettings(SystemSettingsRequestDto dto) {
		try {
			if (dto.getSystemSettingsType().equals("GENERAL_SETTING")) {
				if (dto.getAttributeId() != null && (dto.getSelectedValue() == null || dto.getSelectedValue() == "")) {
					Attributes attributes = attributeService.find(dto.getAttributeId());
					List<Attributes> attributesList = attributesDao
							.fetchSelectedAttributes(attributes.getSettings().getId());
					for (Attributes attributesSave : attributesList) {
						attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					Attributes attributesupdate = attributeService.find(dto.getAttributeId());
					attributesupdate.setSelected(true);
					attributeService.update(attributesupdate);
				} else if ((dto.getSelectedValue() != null || dto.getSelectedValue() != "")
						&& dto.getAttributeId() != null) {
					Settings settings = settingsService.find(dto.getSettingId());
					List<Attributes> attributesList = attributesDao.fetchSelectedAttributes(dto.getSettingId());
					for (Attributes attributesSave : attributesList) {
						if (dto.getAttributeId() == attributesSave.getId())
							attributesSave.setSelected(true);
						else
							attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					settings.setDefaultValue(dto.getSelectedValue());
					settingsService.update(settings);
				} else if (dto.getAttributeId() == null) {
					if (dto.getSettingId() == null) {
						return false;
					}
					Settings settings = settingsService.find(dto.getSettingId());
					settings.setDefaultValue(dto.getSelectedValue());
					settings.setCustomerLogoImage(dto.getCustomerLogoImage());
					if ((dto.getSelectedValue() != null)
							&& (settings.getName().equals("Light Theme") || settings.getName().equals("Dark Theme"))) {
						if (settings.getName().equals("Light Theme")) {
							Settings darkThemeSetting = settingsDao.fetchSettingByName("Dark Theme");
							if (darkThemeSetting != null) {
								darkThemeSetting.setDefaultValue(null);
								settingsService.update(darkThemeSetting);
							}
						} else {
							Settings lightThemeSetting = settingsDao.fetchSettingByName("Light Theme");
							if (lightThemeSetting != null) {
								lightThemeSetting.setDefaultValue(null);
								settingsService.update(lightThemeSetting);
							}
						}
					}
					settingsService.update(settings);
				}
			} // system settings
			if (dto.getSystemSettingsType().equals("MAIL_SETTING")) {
				Settings settings = settingsService.find(dto.getSettingId());
				settings.setDefaultValue(dto.getSelectedValue());
				settingsService.update(settings);
				JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) appContext.getBean("javaMailSender");
				Map<String, String> mailSettings = new HashMap<>();
				mailSettings = settingsService.getMailSettings();
				if (mailSettings != null) {
					if (mailSettings.get("Outbound SMTP Server") != null)
						javaMailSender.setHost(mailSettings.get("Outbound SMTP Server"));
					if (mailSettings.get("Port") != null)
						javaMailSender.setPort(Integer.valueOf(mailSettings.get("Port")));
					if (mailSettings.get("From") != null)
						javaMailSender.setUsername(mailSettings.get("From"));
					if (mailSettings.get("Password") != null)
						javaMailSender.setPassword(mailSettings.get("Password"));
					javaMailSender.setProtocol("smtp");
					javaMailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "true");
					if ((mailSettings.get("Enable Authentication") != null)
							&& ("ON".equalsIgnoreCase(mailSettings.get("Enable Authentication"))))
						javaMailSender.getJavaMailProperties().setProperty("mail.smtp.auth", "true");
				}
			}
			
			if (dto.getSystemSettingsType().equals("LIST_MANAGEMENT")) {
				if (dto.getAttributeId() != null && (dto.getSelectedValue() == null || dto.getSelectedValue() == "")) {
					Attributes attributes = attributeService.find(dto.getAttributeId());
					List<Attributes> attributesList = attributesDao
							.fetchSelectedAttributes(attributes.getSettings().getId());
					for (Attributes attributesSave : attributesList) {
						attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					Attributes attributesupdate = attributeService.find(dto.getAttributeId());
					attributesupdate.setSelected(true);
					attributeService.update(attributesupdate);
				} else if ((dto.getSelectedValue() != null || dto.getSelectedValue() != "")
						&& dto.getAttributeId() != null) {
					Settings settings = settingsService.find(dto.getSettingId());
					List<Attributes> attributesList = attributesDao.fetchSelectedAttributes(dto.getSettingId());
					for (Attributes attributesSave : attributesList) {
						if (dto.getAttributeId() == attributesSave.getId())
							attributesSave.setSelected(true);
						else
							attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					settings.setDefaultValue(dto.getSelectedValue());
					settingsService.update(settings);
				} else if (dto.getAttributeId() == null) {
					if (dto.getSettingId() == null) {
						return false;
					}
					Settings settings = settingsService.find(dto.getSettingId());
					settings.setDefaultValue(dto.getSelectedValue());
					settings.setCustomerLogoImage(dto.getCustomerLogoImage());
					settingsService.update(settings);
				}
			}
			
			if (dto.getSystemSettingsType().equals("ALERT_MANAGEMENT")) {
				if (dto.getAttributeId() != null && (dto.getSelectedValue() == null || dto.getSelectedValue() == "")) {
					Attributes attributes = attributeService.find(dto.getAttributeId());
					List<Attributes> attributesList = attributesDao
							.fetchSelectedAttributes(attributes.getSettings().getId());
					for (Attributes attributesSave : attributesList) {
						attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					Attributes attributesupdate = attributeService.find(dto.getAttributeId());
					attributesupdate.setSelected(true);
					attributeService.update(attributesupdate);
				} else if ((dto.getSelectedValue() != null || dto.getSelectedValue() != "")
						&& dto.getAttributeId() != null) {
					Settings settings = settingsService.find(dto.getSettingId());
					List<Attributes> attributesList = attributesDao.fetchSelectedAttributes(dto.getSettingId());
					for (Attributes attributesSave : attributesList) {
						if (dto.getAttributeId() == attributesSave.getId())
							attributesSave.setSelected(true);
						else
							attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					settings.setDefaultValue(dto.getSelectedValue());
					settings.setSelectedValueMax(dto.getSelectedValueMax());
					settings.setSliderFrom(dto.getSliderFrom());
					settings.setSliderTo(dto.getSliderTo());
					settingsService.update(settings);
				} else if (dto.getAttributeId() == null) {
					if (dto.getSettingId() == null) {
						return false;
					}
					Settings settings = settingsService.find(dto.getSettingId());
					settings.setDefaultValue(dto.getSelectedValue());
					settings.setSelectedValueMax(dto.getSelectedValueMax());
					settings.setSliderFrom(dto.getSliderFrom());
					settings.setSliderTo(dto.getSliderTo());
					settings.setCustomerLogoImage(dto.getCustomerLogoImage());
					settingsService.update(settings);
				}
			}
			if(dto.getSystemSettingsType().equals(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString())) {

				if (dto.getAttributeId() != null && (dto.getSelectedValue() == null || dto.getSelectedValue() == "")) {
					Attributes attributes = attributeService.find(dto.getAttributeId());
					List<Attributes> attributesList = attributesDao
							.fetchSelectedAttributes(attributes.getSettings().getId());
					for (Attributes attributesSave : attributesList) {
						attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					Attributes attributesupdate = attributeService.find(dto.getAttributeId());
					attributesupdate.setSelected(true);
					attributeService.update(attributesupdate);
				} else if ((dto.getSelectedValue() != null || dto.getSelectedValue() != "")
						&& dto.getAttributeId() != null) {
					Settings settings = settingsService.find(dto.getSettingId());
					List<Attributes> attributesList = attributesDao.fetchSelectedAttributes(dto.getSettingId());
					for (Attributes attributesSave : attributesList) {
						if (dto.getAttributeId() == attributesSave.getId())
							attributesSave.setSelected(true);
						else
							attributesSave.setSelected(false);
						attributeService.update(attributesSave);
					}
					settings.setDefaultValue(dto.getSelectedValue());
					settingsService.update(settings);
				} else if (dto.getAttributeId() == null) {
					if (dto.getSettingId() == null) {
						return false;
					}
					Settings settings = settingsService.find(dto.getSettingId());
					settings.setDefaultValue(dto.getSelectedValue());
					settings.setCustomerLogoImage(dto.getCustomerLogoImage());
					settingsService.update(settings);
				}
			
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	@Transactional("transactionManager")
	public String getDeaultUrl() {
		String url = "";
		Settings settings = settingsDao.getDeaultUrl();
		if (settings != null) {
			List<Attributes> attributesList = attributesDao.fetchSelectedAttributes(settings.getId());
			for (Attributes attributes : attributesList) {
				if (attributes.isSelected() == true)
					return url = attributes.getAttributeValue();
			}
		}
		return url;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, String> getMailSettings() {
		String type = "MAIL_SETTING";
		List<Settings> settings = settingsDao.getMailSettings(type);
		Map<String, String> map = settings.stream()
				.collect(Collectors.toMap(Settings::getName, Settings::getDefaultValue));
		return map;
	}
	
	@Override
	@Transactional("transactionManager")
	public List<Settings> findAllWithAttributes() {
	List<Settings> settings = settingsDao.findAll();
	settings.stream().forEach(setting -> Hibernate.initialize(setting.getAttributesList()));
	return settings;
	}

	@Override
	@Transactional("transactionManager")
	public List<SettingsDto> fetchSystemSettingsByType(String settingType) {
		List<Settings> settingsList = settingsDao.getSystemSettingsByType(settingType);
		List<SettingsDto> settingsDtoList = new ArrayList<SettingsDto>();
		settingsDtoList = settingsList.stream()
				.map((settingsDto) -> new EntityConverter<Settings, SettingsDto>(
						Settings.class, SettingsDto.class).toT2(settingsDto,
								new SettingsDto()))
				.collect(Collectors.toList());
		return settingsDtoList;
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean addAttributes(AttributeDto attributeDto) throws Exception {
		Settings settings = settingsDao.find(attributeDto.getSettings().getId());
		boolean found = false;
		List<Settings> languageSettings=settingsDao.fetchAllSettings("Languages");
		Settings existingSetting= null;
		List<Attributes> existingAttributes = null;
		if(languageSettings!=null && languageSettings.size()>0) {
			existingSetting=languageSettings.get(0);
			existingAttributes=existingSetting.getAttributesList();
		}
		Attributes attributes = new Attributes();
		if (existingAttributes != null) {
			for (Attributes attribute : existingAttributes) {
				if (attribute.getAttributeName().equalsIgnoreCase(attributeDto.getAttributeName())) {
					found = true;
				}
			}
		}
		if ((settings != null) && (!found)) {
			BeanUtils.copyProperties(attributeDto, attributes);
			attributes.setSettings(settings);
			attributes.setLanguageName(attributeDto.getAttributeName());
			attributeService.saveOrUpdate(attributes);
			// saves or updates Language in list item

			if (attributeDto.getSettings() != null && (settings.getId() == attributeDto.getSettings().getId())) {
				if (attributeDto.getSettings() != null) {
					ListItemDto languageItem = listItemService.getListItemByListTypeAndDisplayName("Languages",
							attributeDto.getAttributeName());
					if (languageItem != null) {
						languageItem.setDisplayName(attributeDto.getAttributeName());
						languageItem.setIsSelected(attributeDto.isSelected());
						listItemService.saveOrUpdateListItem(languageItem);
					} else {
						ListItemDto newItem = new ListItemDto();
						newItem.setAllowDelete(true);
						newItem.setDisplayName(attributeDto.getAttributeName());
						newItem.setCode(attributeDto.getAttributeValue());
						newItem.setColorCode("657f8b");
						newItem.setIcon("ban");
						newItem.setListType("Languages");
						listItemService.saveOrUpdateListItem(newItem);
					}

					return true;
				}
			}
		} else {
			return false;
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public String findSettingToggleValue(String settingType, String settingName) {
		Settings foundSetting=settingsDao.findSettingByTypeAndName(settingType,settingName);
		if(foundSetting!=null && foundSetting.getType().equalsIgnoreCase("Toggle On/Off")) {
			return foundSetting.getDefaultValue();
		}
		return null;
	}

	@Override
	@Transactional("transactionManager")
	public List<Settings> getBasicSettings() {
		List<Settings> generalSettings = settingsDao.getMailSettings(ElementConstants.GENERAL_SETTING);
		List<Settings> filteredSettings = new ArrayList<>();
		try {
			for (Settings settings : generalSettings) {
				if (settings.getName().equals("Company Logo") || settings.getName().equals("Languages")
						|| settings.getName().equals("Dark Theme") || settings.getName().equals("Light Theme")) {
					filteredSettings.add(settings);
				}
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		Settings twoWaySetting=settingsDao.findSettingByTypeAndName(ElementConstants.USER_MANAGEMENT_REGULATION_STRING,ElementConstants.TWO_WAY_AUTHENTICATION_STRING);
		if(twoWaySetting!=null)
			filteredSettings.add(twoWaySetting);
		return filteredSettings;
	}

}

