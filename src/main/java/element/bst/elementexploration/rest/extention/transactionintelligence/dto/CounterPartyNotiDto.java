package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Counter party notification")
public class CounterPartyNotiDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Count of counter parties")
	private long count;
	@ApiModelProperty(value="Customer id")
	private long customerId;
	@ApiModelProperty(value="Counstomer name")
	private String customerName;
	@ApiModelProperty(value="Id")
	private long id;
	@ApiModelProperty(value="Amount")
	private Double amount;
	@ApiModelProperty(value="Iso 2 code of country")
	private String iso2Code;
	@ApiModelProperty(value="Country")
	private CountryDto country;
	@ApiModelProperty(value="Risk")
	private String risk;
	@ApiModelProperty(value="Colour")
	private String colour;

	public CounterPartyNotiDto() {
	}

	public CounterPartyNotiDto(String customerName, Double amount) {
		super();
		if(customerName!=null)
		{
		this.customerName = customerName;
		}
		this.amount = amount;
	}
	
	
	public CounterPartyNotiDto(Long customerId, Double amount) {
		super();
		if(customerId!=null)
		{
		this.customerId = customerId;
		}
		this.amount = amount;
	}

	public CounterPartyNotiDto(long id, String iso2Code, Double amount) {
		super();
		this.amount = amount;
		this.iso2Code = iso2Code;
		this.id = id;
	}

	public CounterPartyNotiDto(long id, String iso2Code, Double amount, long count) {
		super();
		this.amount = amount;
		this.iso2Code = iso2Code;
		this.id = id;
		this.count = count;
	}
	
	

	public CounterPartyNotiDto(String customerName,Long customerId, Double amount) {
		super();
		if(customerId!=null)
		{
			this.customerId = customerId;
		}
		this.customerName = customerName;
		this.amount = amount;
	}
	
	public CounterPartyNotiDto(Long customerId, Double amount,Long id) {
		super();
		if(customerId!=null)
		{
			this.customerId = customerId;
		}
		this.amount = amount;
		this.id=id;
	}
	//amlTopGeographyAndTransfers
	public CounterPartyNotiDto( Double amount,Long customerId,Long count) {
		super();
		if(customerId!=null)
		{
			this.customerId = customerId;
		}
		this.amount = amount;
		this.count=count;
	}
	
	

	public CountryDto getCountry() {
		return country;
	}

	public void setCountry(CountryDto country) {
		this.country = country;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIso2Code() {
		return iso2Code;
	}

	public void setIso2Code(String iso2Code) {
		this.iso2Code = iso2Code;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

}
