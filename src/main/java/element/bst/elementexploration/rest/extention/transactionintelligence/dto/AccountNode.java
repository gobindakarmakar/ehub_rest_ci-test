package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountNode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("open_date")
	private Date accountOpenDate;
	
	private String currency;
	
	private String oid;
	
	private String id;
	
	@JsonProperty("search-name")
	private String searchName;
	
	private String location;
	
	private String labelV;

	
	public AccountNode(Date accountOpenDate, String currency, String oid, String id, String searchName, String location,
			String labelV) {
		super();
		this.accountOpenDate = accountOpenDate;
		this.currency = currency;
		this.oid = oid;
		this.id = id;
		this.searchName = searchName;
		this.location = location;
		this.labelV = labelV;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLabelV() {
		return labelV;
	}

	public void setLabelV(String labelV) {
		this.labelV = labelV;
	}

}
