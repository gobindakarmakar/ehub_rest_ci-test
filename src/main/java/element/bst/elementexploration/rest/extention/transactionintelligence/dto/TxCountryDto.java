package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TxCountryDto implements Serializable
{

	
	private static final long serialVersionUID = 1L;
	
	private String country;
	@JsonProperty("iso2_code")
	private String iso2Code;
	@JsonProperty("iso3_code")
	private String is03Code;
	private String latitude;
	private String longitude;
	private float risk;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getIso2Code() {
		return iso2Code;
	}
	public void setIso2Code(String iso2Code) {
		this.iso2Code = iso2Code;
	}
	public String getIs03Code() {
		return is03Code;
	}
	public void setIs03Code(String is03Code) {
		this.is03Code = is03Code;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public float getRisk() {
		return risk;
	}
	public void setRisk(float risk) {
		this.risk = risk;
	}
	
	
	
}

