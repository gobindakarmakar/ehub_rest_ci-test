package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@ApiModel("Case Update")
public class CaseUpdateDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "The ID of the case")
	private Long caseId;
	@ApiModelProperty(value = "The name of the case")
	private String name;
	@ApiModelProperty(value = "The description of the case")
	private String description;
	@ApiModelProperty(value = "The ID of the case")
	private String remarks;
	@ApiModelProperty(value = "The remarks of the case")
	private PriorityEnums priority;
	@ApiModelProperty(value = "The health of the case")
	private Double health;
	@ApiModelProperty(value = "The direct risk of the case")
	private Double directRisk;

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PriorityEnums getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnums priority) {
		this.priority = priority;
	}

	public Double getHealth() {
		return health;
	}

	public void setHealth(Double health) {
		this.health = health;
	}

	public Double getDirectRisk() {
		return directRisk;
	}

	public void setDirectRisk(Double directRisk) {
		this.directRisk = directRisk;
	}

	public Double getIndirectRisk() {
		return indirectRisk;
	}

	public void setIndirectRisk(Double indirectRisk) {
		this.indirectRisk = indirectRisk;
	}

	public Double getTransactionalRisk() {
		return transactionalRisk;
	}

	public void setTransactionalRisk(Double transactionalRisk) {
		this.transactionalRisk = transactionalRisk;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ApiModelProperty(value = "The indirect risk of the case")
	private Double indirectRisk;
	@ApiModelProperty(value = "The transactional risk of the case")
	private Double transactionalRisk;
	@ApiModelProperty(value = "Type of the case")
	private String type;

}
