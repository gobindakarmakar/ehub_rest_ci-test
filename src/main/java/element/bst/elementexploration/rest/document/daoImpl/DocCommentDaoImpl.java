package element.bst.elementexploration.rest.document.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.document.dao.DocCommentDao;
import element.bst.elementexploration.rest.document.domain.DocComment;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("docCommentDao")
public class DocCommentDaoImpl extends GenericDaoImpl<DocComment, Long> implements DocCommentDao{

	public DocCommentDaoImpl() {
		super(DocComment.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocCommentDTO> listDocComment(Long userId, Long docId, Integer pageNumber, Integer recordsPerPage,
			String orderIn) {
		List<DocCommentDTO> list = new ArrayList<>();
		StringBuilder hql = new StringBuilder();
		hql.append(
				"select new element.bst.elementexploration.rest.document.dto.DocCommentDTO(dc.commentId,dc.title,dc.description,dc.createdOn,dc.modifiedOn,dc.modificationReason) from DocumentVault dv, DocComment dc ");
		hql.append(
				"where dv.docId = dc.document.docId and dc.user.userId = :userId and dv.docId = :docId order by dc.createdOn ");
		hql.append(orderIn);
		try {
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setParameter("docId", docId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			list = (List<DocCommentDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e,
					DocCommentDaoImpl.class);
			throw new FailedToExecuteQueryException(e.getMessage());
		} 
		return list;
	}

	@Override
	public long countDocComment(Long userId, Long docId) {
		StringBuilder hql = new StringBuilder();
		hql.append("select count(*) from DocumentVault dv, DocComment dc ");
		hql.append("where dv.docId = dc.document.docId and dc.user.userId = :userId and dv.docId = :docId");
		try {
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setParameter("docId", docId);
			return ((Number)query.getSingleResult()).longValue();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e,
					DocCommentDaoImpl.class);
			throw new FailedToExecuteQueryException(e.getMessage());
		} 
	}	
}
