package element.bst.elementexploration.rest.extention.source.addToPage.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author hanuman
 *
 */
public class SourceAddToPageDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("Source Add To Page id")
	private Long pageId;

	public SourceAddToPageDto() {
		super();
	}

	@ApiModelProperty(value = "Entity ID if the Entity")
	private String entityId;

	@ApiModelProperty(value = "Doc ID if the Source")
	private Long docId;

	@ApiModelProperty(value = "Screen Shot URL if the Entity")
	private String screenShotUrl;

	@ApiModelProperty(value = "file name if the File Has")
	private String fileName;

	@ApiModelProperty(value = "Source if the Entity")
	private String sourceName;

	@ApiModelProperty(value = "FLAG if the Source")
	private Boolean isAddToPage;

	@ApiModelProperty(value = "UPDATE_TIME if the Source")
	private Date updatedTime;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getScreenShotUrl() {
		return screenShotUrl;
	}

	public void setScreenShotUrl(String screenShotUrl) {
		this.screenShotUrl = screenShotUrl;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public Boolean getIsAddToPage() {
		return isAddToPage;
	}

	public void setIsAddToPage(Boolean isAddToPage) {
		this.isAddToPage = isAddToPage;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {	
		this.fileName = fileName;
	}

}
