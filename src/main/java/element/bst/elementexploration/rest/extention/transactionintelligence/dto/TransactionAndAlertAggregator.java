package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Transaction and alert aggregates")
public class TransactionAndAlertAggregator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="List of transactions aggregator")	
	private List<TransactionAmountAndAlertCountDto> transactionAggregator;
	@ApiModelProperty(value="List of alerts aggregator")	
	private List<TransactionAmountAndAlertCountDto> alertAggregator;

	public TransactionAndAlertAggregator() {
		super();
	}

	public TransactionAndAlertAggregator(List<TransactionAmountAndAlertCountDto> transactionAggregator,
			List<TransactionAmountAndAlertCountDto> alertAggregator) {
		super();
		this.transactionAggregator = transactionAggregator;
		this.alertAggregator = alertAggregator;
	}

	public List<TransactionAmountAndAlertCountDto> getTransactionAggregator() {
		return transactionAggregator;
	}

	public void setTransactionAggregator(List<TransactionAmountAndAlertCountDto> transactionAggregator) {
		this.transactionAggregator = transactionAggregator;
	}

	public List<TransactionAmountAndAlertCountDto> getAlertAggregator() {
		return alertAggregator;
	}

	public void setAlertAggregator(List<TransactionAmountAndAlertCountDto> alertAggregator) {
		this.alertAggregator = alertAggregator;
	}

	@Override
	public String toString() {
		return "TransactionAndAlertAggregator [transactionAggregator=" + transactionAggregator + ", alertAggregator="
				+ alertAggregator + "]";
	}


}
