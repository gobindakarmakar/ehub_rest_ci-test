package element.bst.elementexploration.rest.usermanagement.security.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;

/**
 * 
 * @author Prateek Maurya
 *
 */
public interface LoginHistoryService extends GenericService<LoginHistory, Long>{

	public boolean isAccountLocked(String username);
	
	public boolean releaseAccountLocked(String username, String ipAddress);
	
	public void processFailedAuthentication(String emailAddress, String screenName, String ipAddress);

	public LoginHistory getFailedLoginByUsername(String username);
	
	int getCurrentLoginAttempt(String username, String screenName, String ipAddress);

	public boolean releaseAccountLockedNew(String emailAddress, String ipAddress, HttpServletRequest request,
			String loginSource, UsersDto usersDto);

	public void processFailedAuthenticationNew(String emailAddress, String screenName, String ipAddress,
			HttpServletRequest request, String loginSource, UsersDto usersDto);

	public List<LoginHistory> getlogonsByUsernames(List<String> usernamesList, Date fromDate, Date toDate);

	public Map<Long, List<LoginHistory>> getFailedlogonsByUserIdList(List<Long> userIdList);
}
