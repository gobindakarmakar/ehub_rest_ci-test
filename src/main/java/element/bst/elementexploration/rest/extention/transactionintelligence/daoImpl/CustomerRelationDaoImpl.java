package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerRelationDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerRelationShipDetails;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("customerRelationDao")
public class CustomerRelationDaoImpl extends GenericDaoImpl<CustomerRelationShipDetails, Long> implements CustomerRelationDao {

	public CustomerRelationDaoImpl() {
		super(CustomerRelationShipDetails.class);
	}

	@Override
	public List<CustomerRelationShipDetails> findByCustomerNumber(String customerNumber) {
		List<CustomerRelationShipDetails> list = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<CustomerRelationShipDetails> query = builder.createQuery(CustomerRelationShipDetails.class);
			Root<CustomerRelationShipDetails> relation = query.from(CustomerRelationShipDetails.class);
			query.select(relation);
			query.where(builder.equal(relation.get("customerNumber"), customerNumber));
			list = (ArrayList<CustomerRelationShipDetails>) this.getCurrentSession().createQuery(query).getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch accounts. Reason : " + e.getMessage());
		} 
		return list;
	}

	@Override
	public CustomerRelationShipDetails findCustomerRelation(String customerNumber, String customerRelationNumber) {
		CustomerRelationShipDetails customerRelationShipDetails=new CustomerRelationShipDetails();
		try{
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" from CustomerRelationShipDetails where customerNumber=:customerNumber and relatedCustomerNumber=:customerRelationNumber");
		Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
		query.setParameter("customerNumber", customerNumber);
		query.setParameter("customerRelationNumber", customerRelationNumber);
		 customerRelationShipDetails=(CustomerRelationShipDetails) query.getSingleResult();	
		 }
		catch(Exception e ){
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return null;
		}
		return customerRelationShipDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCustomerRelationnamesList(Long beneficiaryCustomerId) {
		List<String> namesList=new ArrayList<String>();
		try{
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select cd.displayName from CustomerRelationShipDetails cr,CustomerDetails cd where cd.customerNumber=cr.relatedCustomerNumber and"
					+ " cr.customerNumber IN (select distinct c.customerNumber from TransactionsData t,CustomerDetails c"
					+ " where t.beneficiaryCutomerId=c.id and t.beneficiaryCutomerId=:beneficiaryCustomerId)");

			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("beneficiaryCustomerId", beneficiaryCustomerId);
			namesList=(List<String>) query.getResultList();	
			 }
			catch(Exception e ){
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				return namesList;
			}
		return namesList;
	}
	
	

}
