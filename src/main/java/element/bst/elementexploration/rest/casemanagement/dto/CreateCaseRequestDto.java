package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@ApiModel("Case")
public class CreateCaseRequestDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "name of the case", required = true)
	@NotEmpty(message = "Case name can not be blank.")
	@Size(max = 75, message = "Case name should not be more than 75 characters.")
	private String name;
	
	@ApiModelProperty(value = "description of the case")
	private String description;
	
	@ApiModelProperty(value = "remarks on the case")
	private String remarks;
	
	@ApiModelProperty(value = "priority of the case")
	private PriorityEnums priority;
	
	@ApiModelProperty(value = "health of the case")
	private Double health;
	
	@ApiModelProperty(value = "direct risk of the case")
	private Double directRisk;
	
	@ApiModelProperty(value = "indirect risk of the case")
	private Double indirectRisk;
	
	@ApiModelProperty(value = "transactional risk of the case")
	private Double transactionalRisk;
	
	@ApiModelProperty(value = "type of the case")
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PriorityEnums getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnums priority) {
		this.priority = priority;
	}

	public Double getHealth() {
		return health;
	}

	public void setHealth(Double health) {
		this.health = health;
	}

	public Double getDirectRisk() {
		return directRisk;
	}

	public void setDirectRisk(Double directRisk) {
		this.directRisk = directRisk;
	}

	public Double getIndirectRisk() {
		return indirectRisk;
	}

	public void setIndirectRisk(Double indirectRisk) {
		this.indirectRisk = indirectRisk;
	}

	public Double getTransactionalRisk() {
		return transactionalRisk;
	}

	public void setTransactionalRisk(Double transactionalRisk) {
		this.transactionalRisk = transactionalRisk;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
