package element.bst.elementexploration.rest.settings.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.settings.dto.AttributeDto;

/**
 * @author suresh
 *
 */
public interface AttributesDao extends  GenericDao<Attributes, Long>
{
	
	List<Attributes> fetchSelectedAttributes(long id);
	boolean addAttributes(AttributeDto attributeDto);

}
