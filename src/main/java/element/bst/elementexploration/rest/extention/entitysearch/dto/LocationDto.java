package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;

public class LocationDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String original;
	private int latitude;
	private int longitude;
	private String admin_1_code;
	private String admin_2_code;
	private String country_code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public int getLongitude() {
		return longitude;
	}

	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}

	public String getAdmin_1_code() {
		return admin_1_code;
	}

	public void setAdmin_1_code(String admin_1_code) {
		this.admin_1_code = admin_1_code;
	}

	public String getAdmin_2_code() {
		return admin_2_code;
	}

	public void setAdmin_2_code(String admin_2_code) {
		this.admin_2_code = admin_2_code;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	

}
