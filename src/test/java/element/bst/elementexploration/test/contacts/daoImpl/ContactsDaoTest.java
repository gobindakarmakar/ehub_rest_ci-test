package element.bst.elementexploration.test.contacts.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.contacts.daoImpl.ContactsDaoImpl;
import element.bst.elementexploration.rest.contacts.domain.Contact;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ContactsDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<Contact> root;

	@InjectMocks
	ContactsDaoImpl contactsDao;

	List<ContactDTO> contacts;

	Contact contact;

	@Before
	public void setUp() {
		contact = new Contact(1L, 2L);
		contacts = new ArrayList<ContactDTO>();
		ContactDTO contactDTO = new ContactDTO(1L, "analyst", "user", "analyst@gmail.com");
		contacts.add(contactDTO);

	}

	@Test
	public void getUserContactList() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(contacts);
		List<ContactDTO> contactsFromDB = contactsDao.getUserContactList(1L, 1, 10, "name", "asc", "oracle");
		assertEquals(contactsFromDB.size(), contacts.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserContactListExceptionThrown() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<ContactDTO> contactsFromDB = contactsDao.getUserContactList(1L, 1, 10, "name", "asc", "oracle");
		assertEquals(contactsFromDB.size(), 0);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void removeContact() {
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Contact.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenThrow(FailedToExecuteQueryException.class);
		contactsDao.removeContact(1L, 2L);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserContact() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Contact.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(contact);
		Contact contactFromDB = contactsDao.getUserContact(1L, 2L);
		assertEquals(contactFromDB != null, contact != null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUserContactNoResultExceptionThrown() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Contact.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		Contact contactFromDB = contactsDao.getUserContact(1L, 2L);
		assertEquals(contactFromDB, null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getUserContactExceptionThrown() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Contact.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		contactsDao.getUserContact(1L, 2L);
	}

}
