package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Profile")
public class ProfileDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "The ID of the user profile requested")
	private Long userId;
	
	@ApiModelProperty(value = "Email Address of the user profile requested", required = true)
	@NotEmpty(message = "Email can not be blank.")
	@Email(message="Invalid email address.")
	private String emailAddress;
	
	@ApiModelProperty(value = "First name of the user profile requested", required = true)
	@NotEmpty(message = "FirstName can not be blank.")
	private String firstName;
	
	@ApiModelProperty(value = "Last name of the user profile requested", required = true)
	@NotEmpty(message = "LastName can not be blank.")
	private String lastName;
	
	@ApiModelProperty(value = "Middle name of the user profile requested",required = false)
	private String middleName;
	
	@ApiModelProperty(value = "Screen name of the user profile requested", required = true)
	@NotEmpty(message = "Screen name can not be blank.")
	private String screenName;
	
	@ApiModelProperty(value = "Date of Birth of the user profile requested", required = true)
	@NotNull(message = "Date of birth can not be blank.")
	private Date dob;

	@ApiModelProperty(value = "Country of the user profile requested", required = true)
	@NotEmpty(message = "Country can not be blank.")
    private String country;
	
	@ApiModelProperty(value = "Status of the user profile requested")
	@JsonIgnore
	private String status;
	
	@ApiModelProperty(value = "Last login date of the user profile requested")
	@JsonIgnore
	private Date lastLoginDate;
	
	@ApiModelProperty(value = "Old password of the user profile requested")
	private String oldPassword;
	
	@ApiModelProperty(value = "New password of the user profile requested")
	private String newPassword;
	
	@ApiModelProperty(value = "Retype password of the user profile requested")
	private String retypePassword;
	
	public ProfileDto() {
		super();
	}

	public ProfileDto(Long userId, String emailAddress, String firstName, String lastName, String middleName,
			String screenName, Date dob, String country, String status, Date lastLoginDate, String oldPassword,
			String newPassword, String retypePassword) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.screenName = screenName;
		this.dob = dob;
		this.country = country;
		this.status = status;
		this.lastLoginDate = lastLoginDate;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
		this.retypePassword = retypePassword;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getLastLoginDate() {
		return lastLoginDate;
	}


	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}


	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRetypePassword() {
		return retypePassword;
	}

	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
