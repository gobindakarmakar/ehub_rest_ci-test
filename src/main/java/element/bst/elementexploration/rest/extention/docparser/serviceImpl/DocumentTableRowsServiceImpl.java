package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableRows;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTableRowsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("documentTableRowsService")
public class DocumentTableRowsServiceImpl extends GenericServiceImpl<DocumentTableRows, Long>
		implements DocumentTableRowsService {

	@Autowired
	public DocumentTableRowsServiceImpl(GenericDao<DocumentTableRows, Long> genericDao) {
		super(genericDao);
	}

}
