package element.bst.elementexploration.rest.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedGroupSevice;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedManagementService;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.extention.entity.domain.EntityRequirements;
import element.bst.elementexploration.rest.extention.entity.domain.EntityType;
import element.bst.elementexploration.rest.extention.entity.service.EntityRequirementsService;
import element.bst.elementexploration.rest.extention.entity.service.EntityTypeService;
import element.bst.elementexploration.rest.extention.entitysearch.domain.Classification;
import element.bst.elementexploration.rest.extention.entitysearch.domain.Keywords;
import element.bst.elementexploration.rest.extention.entitysearch.service.ClassificationService;
import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;
import element.bst.elementexploration.rest.extention.geoencoder.service.CountryMasterDataService;
import element.bst.elementexploration.rest.extention.gridview.service.GridviewService;
import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.extention.menuitem.domain.LastVisitedDomains;
import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.extention.menuitem.domain.UserMenu;
import element.bst.elementexploration.rest.extention.menuitem.enumType.MenuSizeEnum;
import element.bst.elementexploration.rest.extention.menuitem.service.DomainsService;
import element.bst.elementexploration.rest.extention.menuitem.service.LastVisitedDomainService;
import element.bst.elementexploration.rest.extention.menuitem.service.MenuItemService;
import element.bst.elementexploration.rest.extention.menuitem.service.ModulesGroupService;
import element.bst.elementexploration.rest.extention.menuitem.service.ModulesService;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.DataAttributes;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceCategoryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceDomainDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceIndustryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceJurisdictionDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceMediaDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.ClassificationsService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.DataAttributesService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCategoryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCredibilityService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceDomainService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceIndustryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceMediaService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SubClassificationsService;
import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.service.ComplianceWidgetService;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.fileupload.service.FileService;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.listManagement.domain.Color;
import element.bst.elementexploration.rest.settings.listManagement.domain.Icon;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.domain.Role;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.dto.RoleDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ColorService;
import element.bst.elementexploration.rest.settings.listManagement.service.IconService;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.listManagement.service.RoleService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupAlertService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupPermissionService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.version.domain.ProductVersion;
import element.bst.elementexploration.rest.version.service.VersionService;

@Component
public class DatabaseInitializer {

	@Autowired
	UsersService usersService;

	@Autowired
	GroupsService groupsService;
	
	@Autowired
	UserGroupsService userGroupsService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	CountryMasterDataService countryMasterDataService;

	@Autowired
	SettingsService settingsService;

	@Autowired
	AlertManagementService alertService;
	
	@Autowired
	VersionService versionService;

	@Value("${product_version}")
	private String productVersion;

	@Autowired
	private ClassificationService classificationService;

	@Autowired
	ClassificationsService classificationsService;

	@Autowired
	SourcesService sourcesService;

	@Autowired
	SourceMediaService sourceMediaSourceService;

	@Autowired
	SourceCategoryService sourceCategoryService;

	@Autowired
	SourceDomainService sourceDomainService;

	@Autowired
	SourceIndustryService sourceIndustryService;

	@Autowired
	SourceJurisdictionService sourceJurisdictionService;

	@Autowired
	ComplianceWidgetService complianceWidgetService;

	@Autowired
	SubClassificationsService subClassificationsService;

	@Autowired
	DataAttributesService dataAttributesService;

	@Autowired
	EntityTypeService entityTypeService;

	@Autowired
	EntityRequirementsService entityRequirementsService;

	@Autowired
	ScheduleSourcesUtility scheduleSourcesUtility;

	@Autowired
	ListItemService listItemService;

	@Autowired
	ColorService colorService;

	@Autowired
	SourceCredibilityService sourceCredibilityService;

	@Autowired
	RoleService roleService;

	@Autowired
	IconService iconService;

	@Autowired
	ObjectMapper mapper;

	@Autowired
	ModulesService modulesService;

	@Autowired
	PrivilegesService privilegesService;

	@Autowired
	ModulesGroupService modulesGroupService;

	@Autowired
	DomainsService domainsService;
	
	@Autowired
	MenuItemService menuItemService;
	
	@Autowired
	FileService fileService;
	
	@Value("${compliance_report_version}")
	private String ComplianceReportVersion;
	
	@Autowired	
	RolesService rolesService;	
		
	@Autowired	
	UserRolesService userRolesService;	
		
	@Autowired	
	GroupPermissionService groupPermissionService;
	
	@Autowired
	LastVisitedDomainService lastVisitedDomainService;
	
	@Autowired
	GroupAlertService groupAlertService;

	@Autowired
	FeedManagementService feedService;
	
	@Autowired
	FeedGroupSevice feedGoupService;
	
	@Autowired
	GridviewService gridViewService;
	
	@Value("${mysql.url}")
	private String dbUrl;
	
	@PostConstruct
	public void initDatabase() {
		loadData();
	}

	private void loadData() {
		try {
			// User admin = null;
			// User analyst = null;
			Users admin = null;
			Users analyst = null;
			ListItem countryItem = new ListItem();
			ListItem dummyStatus = new ListItem();
			ListItem activeItem = new ListItem();
			ListItem deactiveItem = new ListItem();
			Roles analystRole=null; 	
			Roles adminRole=null;	
			Roles userRole= null;
			ListItem israelCountryItem=null;
			Groups adminGroup=null;
			Groups analystGroup=null;
			
			//sourceJurisdictionService.updateTables(dbUrl);

			List<CountryMasterData> countryMasterDataList = countryMasterDataService.findAll();
			if (countryMasterDataList.isEmpty()) {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				URL resource = classLoader.getResource("countries-code.json");
				File file = new File(resource.toURI());
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(file));
				JSONArray jsonObject = new JSONArray(object.toString());
				for (int i = 1; i < jsonObject.length(); i++) {
					JSONObject normalObject = jsonObject.getJSONObject(i);
					CountryMasterData countryMasterData = new CountryMasterData();
					countryMasterData.setName(normalObject.get("name").toString());
					countryMasterData.setLongitude(normalObject.get("longitude").toString());
					countryMasterData.setLatitude(normalObject.get("latitude").toString());
					countryMasterData.setIsoCode(normalObject.get("iso2Code").toString());
					countryMasterDataService.save(countryMasterData);
				}
			}
			
			// System Settings and Mail Settings
			if (settingsService.findAll().isEmpty()) {
				List<Settings> listSettings = new ArrayList<Settings>();
				Settings settings1 = new Settings();
				settings1.setName("Company View");
				settings1.setSection("defaults");
				settings1.setType("Dropdown");
				settings1.setDefaultValue("default");
				settings1.setSystemSettingType("GENERAL_SETTING");
				List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();
				Attributes attributesSettings1 = new Attributes();
				attributesSettings1.setAttributeName("default");
				attributesSettings1.setAttributeValue("");
				attributesSettings1.setSelected(true);
				attributesSettings1.setSettings(settings1);

				Attributes attributesSettings12 = new Attributes();
				attributesSettings12.setAttributeName("underwriting");
				attributesSettings12.setAttributeValue("");
				attributesSettings12.setSettings(settings1);

				Attributes attributesSettings13 = new Attributes();
				attributesSettings13.setAttributeName("compliance");
				attributesSettings13.setAttributeValue("");
				attributesSettings13.setSettings(settings1);

				attributesListSettings1.add(attributesSettings1);
				attributesListSettings1.add(attributesSettings12);
				attributesListSettings1.add(attributesSettings13);
				settings1.setAttributesList(attributesListSettings1);

				Settings settings2 = new Settings();
				settings2.setName("Case View");
				settings2.setSection("defaults");
				settings2.setType("Dropdown");
				settings2.setDefaultValue("default");
				settings2.setSystemSettingType("GENERAL_SETTING");

				List<Attributes> attributesListSettings2 = new ArrayList<Attributes>();
				Attributes attributesSettings2 = new Attributes();
				attributesSettings2.setAttributeName("default");
				attributesSettings2.setAttributeValue("");
				attributesSettings2.setSelected(true);
				attributesSettings2.setSettings(settings2);

				Attributes attributesSettings21 = new Attributes();
				attributesSettings21.setAttributeName("underwriting");
				attributesSettings21.setAttributeValue("");
				attributesSettings21.setSettings(settings2);

				attributesListSettings2.add(attributesSettings2);
				attributesListSettings2.add(attributesSettings21);
				settings2.setAttributesList(attributesListSettings2);

				Settings settings3 = new Settings();
				settings3.setName("Dashboard View");
				settings3.setSection("defaults");
				settings3.setType("Dropdown");
				settings3.setDefaultValue("default");
				settings3.setSystemSettingType("GENERAL_SETTING");

				List<Attributes> attributesListSettings3 = new ArrayList<Attributes>();
				Attributes attributesSettings3 = new Attributes();
				attributesSettings3.setAttributeName("default");
				attributesSettings3.setAttributeValue("");
				attributesSettings3.setSelected(true);
				attributesSettings3.setSettings(settings3);

				Attributes attributesSettings31 = new Attributes();
				attributesSettings31.setAttributeName("underwriting");
				attributesSettings31.setAttributeValue("");
				attributesSettings31.setSettings(settings3);

				attributesListSettings3.add(attributesSettings3);
				attributesListSettings3.add(attributesSettings31);

				settings3.setAttributesList(attributesListSettings3);

				Settings settings4 = new Settings();
				settings4.setName("Default Module");
				settings4.setSection("defaults");
				settings4.setType("Dropdown");
				settings4.setDefaultValue("None");
				settings4.setSystemSettingType("GENERAL_SETTING");

				Settings settings16 = new Settings();
				settings16.setName("List Type");
				settings16.setSection("List Management");
				settings16.setType("Dropdown");
				settings16.setDefaultValue("None");
				settings16.setSystemSettingType("LIST_MANAGEMENT");

				List<Attributes> attributesListSettings6 = new ArrayList<Attributes>();
				Attributes attributes6_1 = new Attributes();
				attributes6_1.setAttributeName("Roles");
				attributes6_1.setAttributeValue("Roles");
				attributes6_1.setSettings(settings16);

				Attributes attributes6_2 = new Attributes();
				attributes6_2.setAttributeName("Feed Classification");
				attributes6_2.setAttributeValue("Feed Classification");
				attributes6_2.setSettings(settings16);

				Attributes attributes6_3 = new Attributes();
				attributes6_3.setAttributeName("Feed Source");
				attributes6_3.setAttributeValue("Feed Source");
				attributes6_3.setSettings(settings16);

				Attributes attributes6_4 = new Attributes();
				attributes6_4.setAttributeName("Group Level");
				attributes6_4.setAttributeValue("Group Level");
				attributes6_4.setSettings(settings16);

				Attributes attributes6_5 = new Attributes();
				attributes6_5.setAttributeName("Alert Status");
				attributes6_5.setAttributeValue("Alert Status");
				attributes6_5.setSettings(settings16);

				Attributes attributes6_6 = new Attributes();
				attributes6_6.setAttributeName("Jurisdictions");
				attributes6_6.setAttributeValue("Jurisdictions");
				attributes6_6.setSettings(settings16);

				Attributes attributes6_7 = new Attributes();
				attributes6_7.setAttributeName("Industry");
				attributes6_7.setAttributeValue("Industry");
				attributes6_7.setSettings(settings16);

				Attributes attributes6_8 = new Attributes();
				attributes6_8.setAttributeName("Classifications");
				attributes6_8.setAttributeValue("Classifications");
				attributes6_8.setSettings(settings16);

				Attributes attributes6_9 = new Attributes();
				attributes6_9.setAttributeName("Domains");
				attributes6_9.setAttributeValue("Domains");
				attributes6_9.setSettings(settings16);

				Attributes attributes6_10 = new Attributes();
				attributes6_10.setAttributeName("Media");
				attributes6_10.setAttributeValue("Media");
				attributes6_10.setSettings(settings16);
				
				Attributes attributes6_11 = new Attributes();
				attributes6_11.setAttributeName(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
				attributes6_11.setAttributeValue(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
				attributes6_11.setSettings(settings16);
				
				Attributes attributes6_12 = new Attributes();
				attributes6_12.setAttributeName("Languages");
				attributes6_12.setAttributeValue("Languages");
				attributes6_12.setSettings(settings16);


				attributesListSettings6.add(attributes6_1);
				attributesListSettings6.add(attributes6_2);
				attributesListSettings6.add(attributes6_3);
				attributesListSettings6.add(attributes6_4);
				attributesListSettings6.add(attributes6_5);
				attributesListSettings6.add(attributes6_6);
				attributesListSettings6.add(attributes6_7);
				attributesListSettings6.add(attributes6_8);
				attributesListSettings6.add(attributes6_9);
				attributesListSettings6.add(attributes6_10);
				attributesListSettings6.add(attributes6_11);
				attributesListSettings6.add(attributes6_12);


				settings16.setAttributesList(attributesListSettings6);

				List<Attributes> attributesListSettings5 = new ArrayList<Attributes>();
				Attributes attributes1 = new Attributes();
				attributes1.setAttributeName("Advanced Search");
				attributes1.setAttributeValue("#/enrich");
				attributes1.setSettings(settings4);

				Attributes attributes2 = new Attributes();
				attributes2.setAttributeName("Dashboard");
				attributes2.setAttributeValue("#/discover");
				attributes2.setSettings(settings4);

				Attributes attributes3 = new Attributes();
				attributes3.setAttributeName("Lead Generation");
				attributes3.setAttributeValue("#/leadGeneration/#!/landing");
				attributes3.setSettings(settings4);

				Attributes attributes4 = new Attributes();
				attributes4.setAttributeName("Transaction Intelligence");
				attributes4.setAttributeValue("#/transactionIntelligence/#!/alertDashboard");
				attributes4.setSettings(settings4);

				Attributes attributes5 = new Attributes();
				attributes5.setAttributeName("Underwriting");
				attributes5.setAttributeValue("#/uploadDocuments/#!/landing");
				attributes5.setSettings(settings4);

				Attributes attributes6 = new Attributes();
				attributes6.setAttributeName("Search");
				attributes6.setAttributeValue("#/entitySearch/#!/entitySearchLanding");
				attributes6.setSettings(settings4);

				Attributes attributes7 = new Attributes();
				attributes7.setAttributeName("Onboarding");
				attributes7.setAttributeValue("#/enrich/onBoard");
				attributes7.setSettings(settings4);

				Attributes attributes8 = new Attributes();
				attributes8.setAttributeName("Policy Enforcement");
				attributes8.setAttributeValue("#/Policy Enforcement");
				attributes8.setSettings(settings4);

				Attributes attributes9 = new Attributes();
				attributes9.setAttributeName("Orchestration");
				attributes9.setAttributeValue("#/elementbpm/#/");
				attributes9.setSettings(settings4);

				Attributes attributes10 = new Attributes();
				attributes10.setAttributeName("Data Curation");
				attributes10.setAttributeValue("#/dataCuration");
				attributes10.setSettings(settings4);

				Attributes attributes12 = new Attributes();
				attributes12.setAttributeName("Fraud");
				attributes12.setAttributeValue("#/transactionIntelligence/#!/alertDashboard");
				attributes12.setSettings(settings4);

				Attributes attributes13 = new Attributes();
				attributes13.setAttributeName("Big Data Science Platform");
				attributes13.setAttributeValue("#/workflow/#!/");
				attributes13.setSettings(settings4);

				Attributes attributes14 = new Attributes();
				attributes14.setAttributeName("Questionnaire Builder");
				attributes14.setAttributeValue("#/Questionnaire Builder");
				attributes14.setSettings(settings4);

				Attributes attributes15 = new Attributes();
				attributes15.setAttributeName("None");
				attributes15.setAttributeValue("/");
				attributes15.setSettings(settings4);
				attributes15.setSelected(true);

				attributesListSettings5.add(attributes1);
				attributesListSettings5.add(attributes2);
				attributesListSettings5.add(attributes3);
				attributesListSettings5.add(attributes4);
				attributesListSettings5.add(attributes5);
				attributesListSettings5.add(attributes6);
				attributesListSettings5.add(attributes7);
				attributesListSettings5.add(attributes8);
				attributesListSettings5.add(attributes9);
				attributesListSettings5.add(attributes10);
				attributesListSettings5.add(attributes12);
				attributesListSettings5.add(attributes13);
				attributesListSettings5.add(attributes14);
				attributesListSettings5.add(attributes15);

				settings4.setAttributesList(attributesListSettings5);

				Settings settings5 = new Settings();
				settings5.setName("Open link in a new tab");
				settings5.setSection("User Interaction");
				settings5.setType("Toggle On/Off");
				settings5.setDefaultValue("On");
				settings5.setSystemSettingType("GENERAL_SETTING");

				Settings settings51 = new Settings();
				settings51.setName("Allow checklist");
				settings51.setSection("defaults");
				settings51.setType("Toggle On/Off");
				settings51.setDefaultValue("On");
				settings51.setSystemSettingType("GENERAL_SETTING");

				Settings settings61 = new Settings();
				settings61.setName("Questionnaries");
				settings61.setSection("defaults");
				settings61.setType("dropdown");
				settings61.setDefaultValue("");
				settings61.setSystemSettingType("GENERAL_SETTING");

				listSettings.add(settings1);
				listSettings.add(settings2);
				listSettings.add(settings3);
				listSettings.add(settings4);
				listSettings.add(settings5);
				listSettings.add(settings51);
				listSettings.add(settings61);
				listSettings.add(settings16);

				// mail settings
				Settings settings6 = new Settings();
				settings6.setName("Outbound SMTP Server");
				settings6.setSection("Account Settings");
				settings6.setType("Text");
				settings6.setRequired(true);
				settings6.setMaximumLength(100);
				settings6.setDefaultValue("");
				settings6.setValidation("Server/IP validator");
				settings6.setSystemSettingType("MAIL_SETTING");

				Settings settings7 = new Settings();
				settings7.setName("Port");
				settings7.setSection("Account Settings");
				settings7.setType("Text");
				settings7.setRequired(true);
				settings7.setMaximumLength(4);
				settings7.setDefaultValue("25");
				settings7.setValidation("Numeric");
				settings7.setSystemSettingType("MAIL_SETTING");

				Settings settings8 = new Settings();
				settings8.setName("SSL");
				settings8.setSection("Account Settings");
				settings8.setType("Toggle On/Off");
				settings8.setRequired(false);
				settings8.setDefaultValue("On");
				settings8.setSystemSettingType("MAIL_SETTING");

				Settings settings9 = new Settings();
				settings9.setName("Enable Authentication");
				settings9.setSection("Authentication");
				settings9.setType("Toggle On/Off");
				settings9.setRequired(false);
				settings9.setDefaultValue("On");
				settings9.setSystemSettingType("MAIL_SETTING");

				Settings settings10 = new Settings();
				settings10.setName("User");
				settings10.setSection("Authentication");
				settings10.setType("Text");
				settings10.setRequired(true);
				settings10.setMaximumLength(80);
				settings10.setDefaultValue("");
				settings10.setValidation("");
				settings10.setSystemSettingType("MAIL_SETTING");

				Settings settings11 = new Settings();
				settings11.setName("Password");
				settings11.setSection("Authentication");
				settings11.setType("Password");
				settings11.setRequired(true);
				settings11.setMaximumLength(30);
				settings11.setDefaultValue("");
				settings11.setValidation("Password rules as specified below");
				settings11.setSystemSettingType("MAIL_SETTING");

				Settings settings12 = new Settings();
				settings12.setName("Retype Password");
				settings12.setSection("Authentication");
				settings12.setType("Password");
				settings12.setRequired(true);
				settings12.setMaximumLength(30);
				settings12.setDefaultValue("");
				settings12.setValidation("Must much to Password field incase Password field is valid");
				settings12.setSystemSettingType("MAIL_SETTING");

				Settings settings13 = new Settings();
				settings13.setName("From");
				settings13.setSection("General");
				settings13.setType("Text");
				settings13.setRequired(true);
				settings13.setMaximumLength(50);
				settings13.setDefaultValue("{companyName@CompanyDomain}");
				settings13.setValidation("Email address validator");
				settings13.setSystemSettingType("MAIL_SETTING");

				listSettings.add(settings6);
				listSettings.add(settings7);
				listSettings.add(settings8);
				listSettings.add(settings9);
				listSettings.add(settings10);
				listSettings.add(settings11);
				listSettings.add(settings12);
				listSettings.add(settings13);

				for (Settings settings : listSettings) {
					settingsService.save(settings);
				}
			}

			List<Settings> settingsList = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow checklist")).collect(Collectors.toList());
			if (settingsList.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Allow checklist");
				settings51.setSection("defaults");
				settings51.setType("Toggle On/Off");
				settings51.setDefaultValue("On");
				settings51.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settings51);
			}

			List<Settings> settingsListQuestioanrie = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Questionnaries")).collect(Collectors.toList());
			if (settingsListQuestioanrie.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Questionnaries");
				settings51.setSection("defaults");
				settings51.setType("dropdown");
				settings51.setDefaultValue("");
				settings51.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settings51);
			}

			//on boarding questionnaire
			List<Settings> settingsListOnboardingQuestionnaires = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Onboarding Questionnaire"))
					.collect(Collectors.toList());
			if (settingsListOnboardingQuestionnaires.isEmpty()) {
				Settings settings52 = new Settings();
				settings52.setName("Onboarding Questionnaire");
				settings52.setSection("defaults");
				settings52.setType("dropdown");
				settings52.setDefaultValue("");
				settings52.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settings52);
			}
			// customer image logo
			List<Settings> customerLogsettingsList = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Company Logo")).collect(Collectors.toList());
			if (customerLogsettingsList.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Company Logo");
				settings51.setSection("Branding");
				settings51.setType("");
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				File file = new File(classLoader.getResource("CustomerLogoByteArray.txt").getFile());
				String content = new String(Files.readAllBytes(file.toPath()));
				settings51.setCustomerLogoImage(content);
				settings51.setDefaultValue(content);
				settings51.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settings51);
			}

			// new settings for compliance
			List<Settings> settingsComplianceReport = settingsService.findAllWithAttributes().stream()
					.filter(setting -> setting.getName().equals("Compliance Report")).collect(Collectors.toList());
			if (settingsComplianceReport.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Compliance Report");
				settings51.setSection("defaults");
				settings51.setType("dropdown");
				settings51.setDefaultValue("default");
				settings51.setSystemSettingType("GENERAL_SETTING");
				List<Attributes> attributesListSettings5 = new ArrayList<Attributes>();
				Attributes attributes1 = new Attributes();
				attributes1.setAttributeName("default");
				attributes1.setAttributeValue("");
				attributes1.setSelected(true);
				attributes1.setSettings(settings51);
				/*Attributes attributes2 = new Attributes();
				attributes2.setAttributeName("Genpact");
				attributes2.setAttributeValue("GP Compliance");
				attributes2.setSettings(settings51);
				Attributes attributes3 = new Attributes();
				attributes3.setAttributeName("Genpact V2");
				attributes3.setAttributeValue("GP Compliance V2");
				attributes3.setSettings(settings51);*/

				attributesListSettings5.add(attributes1);
				//attributesListSettings5.add(attributes2);
				//attributesListSettings5.add(attributes3);
				settings51.setAttributesList(attributesListSettings5);
				settingsService.save(settings51);
			}

			// add a new attribute Genpact V2 if not in compliance report
			/*if (settingsComplianceReport.stream()
					.filter(setting -> setting.getAttributesList().stream()
							.anyMatch(s -> "Genpact V2".equals(s.getAttributeName())))
					.collect(Collectors.toList()).size() == 0) {

				for (Settings setting : settingsComplianceReport) {

					List<Attributes> attributesListSettings5 = setting.getAttributesList();

					Attributes attributes3 = new Attributes();
					attributes3.setAttributeName("Genpact V2");
					attributes3.setAttributeValue("GP Compliance V2");
					attributes3.setSettings(setting);
					attributesListSettings5.add(attributes3);
					setting.setAttributesList(attributesListSettings5);
					settingsService.update(setting);

				}

			}*/

			// Compliance Checklist
			List<Settings> settingsListCompliance = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Report Comments")).collect(Collectors.toList());
			if (settingsListCompliance.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Report Comments");
				settings51.setSection("defaults");
				settings51.setType("dropdown");
				settings51.setDefaultValue("Showing the latest Comments");
				settings51.setSystemSettingType("GENERAL_SETTING");

				List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();
				Attributes attributesSettings1 = new Attributes();
				attributesSettings1.setAttributeName("Showing the latest Comments");
				attributesSettings1.setAttributeValue("");
				attributesSettings1.setSelected(true);
				attributesSettings1.setSettings(settings51);

				Attributes attributesSettings12 = new Attributes();
				attributesSettings12.setAttributeName("Showing all Comments");
				attributesSettings12.setAttributeValue("");
				attributesSettings12.setSettings(settings51);
				attributesListSettings1.add(attributesSettings1);
				attributesListSettings1.add(attributesSettings12);
				settings51.setAttributesList(attributesListSettings1);
				settingsService.save(settings51);
			}
			// alert management setting -> consolidated hits toggle
			List<Settings> settingsAlertHits = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Show consolidated hits")).collect(Collectors.toList());
			if (settingsAlertHits.isEmpty()) {
				Settings settings17 = new Settings();
				settings17.setName("Show consolidated hits");
				settings17.setSection("User Interaction");
				settings17.setType("Toggle On/Off");
				settings17.setDefaultValue("Off");
				settings17.setSystemSettingType("ALERT_MANAGEMENT");
				settingsService.save(settings17);
			}

			// alert management setting -> PEP Risk Factor toggle
			List<Settings> settingsAlertPepToggle = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("PEP Risk Factor")).collect(Collectors.toList());
			if (settingsAlertPepToggle.isEmpty()) {
				Settings settings18 = new Settings();
				settings18.setName("PEP Risk Factor");
				settings18.setSection("Alert Indicators");
				settings18.setType("Toggle On/Off");
				settings18.setDefaultValue("Off");
				settings18.setSystemSettingType("ALERT_MANAGEMENT");
				settingsService.save(settings18);
			}

			// alert management setting -> PEP Risk Factor toggle
			List<Settings> settingsAlertSanctionToggle = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Sanction Risk Factor")).collect(Collectors.toList());
			if (settingsAlertSanctionToggle.isEmpty()) {
				Settings settings19 = new Settings();
				settings19.setName("Sanction Risk Factor");
				settings19.setSection("Alert Indicators");
				settings19.setType("Toggle On/Off");
				settings19.setDefaultValue("Off");
				settings19.setSystemSettingType("ALERT_MANAGEMENT");
				settingsService.save(settings19);
			}

			// alert management setting -> Time in Status toggle
			List<Settings> settingsAlertTimeStatusToggle = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Time In Status")).collect(Collectors.toList());
			if (settingsAlertTimeStatusToggle.isEmpty()) {
				Settings settings23 = new Settings();
				settings23.setName("Time In Status");
				settings23.setSection("Alert Indicators");
				settings23.setType("Toggle On/Off");
				settings23.setDefaultValue("Off");
				settings23.setSystemSettingType("ALERT_MANAGEMENT");
				settingsService.save(settings23);
			}
			// predefined answers drop down
			List<Settings> settingsAlertPredefinedAnswers = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Predefined answers for :"))
					.collect(Collectors.toList());
			if (settingsAlertPredefinedAnswers.isEmpty()) {
				Settings settings28 = new Settings();
				settings28.setName("Predefined answers for :");
				settings28.setSection("Predefined Answers");
				settings28.setType("Dropdown");
				settings28.setDefaultValue("");
				settings28.setSystemSettingType("ALERT_MANAGEMENT");
				
				settingsService.save(settings28);
			}
			
			// System settings- Language Selection
						List<Settings> settingsLanguageList = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Languages")).collect(Collectors.toList());
						if (settingsLanguageList.isEmpty()) {
							Settings settingsLanguage = new Settings();
							settingsLanguage.setName("Languages");
							settingsLanguage.setSection("Regional Settings");
							settingsLanguage.setType("Autocomplete");
							settingsLanguage.setDefaultValue("English");
							settingsLanguage.setSystemSettingType("GENERAL_SETTING");
							
			               
							List<Attributes> attributesListSettingsLanguage = new ArrayList<Attributes>();
							Attributes attributesLanguageSettings = new Attributes();
							attributesLanguageSettings.setAttributeName("English");
							attributesLanguageSettings.setAttributeValue("English");
							attributesLanguageSettings.setSelected(true);
							attributesLanguageSettings.setSettings(settingsLanguage);

							Attributes attributesLanguageSettings1 = new Attributes();
							attributesLanguageSettings1.setAttributeName("German");
							attributesLanguageSettings1.setAttributeValue("German");
							attributesLanguageSettings1.setSettings(settingsLanguage);

							attributesListSettingsLanguage.add(attributesLanguageSettings);
							attributesListSettingsLanguage.add(attributesLanguageSettings1);

							settingsLanguage.setAttributesList(attributesListSettingsLanguage);
							settingsService.save(settingsLanguage);

						}



//----start--------------- Alert Settings Risk Indicator modification-----------------//
			
			// alert management setting -> PEP High Period drop down
						List<Settings> settingsAlertPepHighPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("PEP High Period")).collect(Collectors.toList());
						if (settingsAlertPepHighPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("PEP High Period");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> PEP High Slider
						List<Settings> settingsAlertPepHighSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("PEP High Slider")).collect(Collectors.toList());
						if (settingsAlertPepHighSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("PEP High Slider");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						// alert management setting -> PEP Medium Period drop down
						List<Settings> settingsAlertPepMediumPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("PEP Medium Period")).collect(Collectors.toList());
						if (settingsAlertPepMediumPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("PEP Medium Period");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> PEP Medium Slider
						List<Settings> settingsAlertPepMediumSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("PEP Medium Slider")).collect(Collectors.toList());
						if (settingsAlertPepMediumSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("PEP Medium Slider");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						// alert management setting -> PEP Period drop down
						List<Settings> settingsAlertPEPLowPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("PEP Low Period")).collect(Collectors.toList());
						if (settingsAlertPEPLowPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("PEP Low Period");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> PEP Low Slider
						List<Settings> settingsAlertPepLowSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("PEP Low Slider")).collect(Collectors.toList());
						if (settingsAlertPepLowSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("PEP Low Slider");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Sanction High Period drop down
						List<Settings> settingsAlertSanctionHighPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Sanction High Period")).collect(Collectors.toList());
						if (settingsAlertSanctionHighPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Sanction High Period");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Sanction High Slider
						List<Settings> settingsAlertSanctionHighSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Sanction High Slider")).collect(Collectors.toList());
						if (settingsAlertSanctionHighSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Sanction High Slider");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Sanction Medium Period drop down
						List<Settings> settingsAlertSanctionMediumPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Sanction Medium Period")).collect(Collectors.toList());
						if (settingsAlertSanctionMediumPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Sanction Medium Period");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Sanction Medium Slider
						List<Settings> settingsAlertSanctionMediumSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Sanction Medium Slider")).collect(Collectors.toList());
						if (settingsAlertSanctionMediumSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Sanction Medium Slider");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Sanction Period drop down
						List<Settings> settingsAlertSanctionLowPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Sanction Low Period")).collect(Collectors.toList());
						if (settingsAlertSanctionLowPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Sanction Low Period");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Sanction Low Slider
						List<Settings> settingsAlertSanctionLowSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Sanction Low Slider")).collect(Collectors.toList());
						if (settingsAlertSanctionLowSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Sanction Low Slider");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						
						// alert management setting -> Time Status High Period (Short Term) drop down
						List<Settings> settingsAlertTimeStatusHighPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Time Status High Period (Short Term)")).collect(Collectors.toList());
						if (settingsAlertTimeStatusHighPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Time Status High Period (Short Term)");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting ->Time Status High Slider (Short Term)
						List<Settings> settingsAlertTimeStatusHighSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Time Status High Slider (Short Term)")).collect(Collectors.toList());
						if (settingsAlertTimeStatusHighSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Time Status High Slider (Short Term)");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Time Status Medium Period (Long Term) drop down
						List<Settings> settingsAlertTimeStatusMediumPeriod = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Time Status Medium Period (Long Term)")).collect(Collectors.toList());
						if (settingsAlertTimeStatusMediumPeriod.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Time Status Medium Period (Long Term)");
							settings20.setSection("Alert Indicators");
							settings20.setType("Dropdown");
							settings20.setDefaultValue("Days");
							settings20.setSelectedValueMax(60);
							settings20.setSystemSettingType("ALERT_MANAGEMENT");

							List<Attributes> attributesListSettings1 = new ArrayList<Attributes>();

							Attributes attributesSettings1 = new Attributes();
							attributesSettings1.setAttributeName("Years");
							attributesSettings1.setAttributeValue("20");
							attributesSettings1.setSettings(settings20);

							Attributes attributesSettings2 = new Attributes();
							attributesSettings2.setAttributeName("Months");
							attributesSettings2.setAttributeValue("24");
							attributesSettings2.setSettings(settings20);

							Attributes attributesSettings3 = new Attributes();
							attributesSettings3.setAttributeName("Weeks");
							attributesSettings3.setAttributeValue("52");
							attributesSettings3.setSettings(settings20);

							Attributes attributesSettings4 = new Attributes();
							attributesSettings4.setAttributeName("Days");
							attributesSettings4.setAttributeValue("60");
							attributesSettings4.setSelected(true);
							attributesSettings4.setSettings(settings20);

							attributesListSettings1.add(attributesSettings1);
							attributesListSettings1.add(attributesSettings2);
							attributesListSettings1.add(attributesSettings3);
							attributesListSettings1.add(attributesSettings4);

							settings20.setAttributesList(attributesListSettings1);
							settingsService.save(settings20);
						}
						
						// alert management setting -> Time Status Medium Slider (Long Term)
						List<Settings> settingsAlertTimeStatusMediumSlider = settingsService.findAll().stream()
								.filter(setting -> setting.getName().equals("Time Status Medium Slider (Long Term)")).collect(Collectors.toList());
						if (settingsAlertTimeStatusMediumSlider.isEmpty()) {
							Settings settings20 = new Settings();
							settings20.setName("Time Status Medium Slider (Long Term)");
							settings20.setSection("Alert Indicators");
							settings20.setType("Slider");
							settings20.setDefaultValue("Days");
							settings20.setSystemSettingType("ALERT_MANAGEMENT");
							settings20.setSelectedValueMax(60);
							settings20.setSliderFrom(5);
							settings20.setSliderTo(10);
							settingsService.save(settings20);
						}
						
			//----end--------------- Alert Settings Risk Indicator modification-----------------//
			
			// Maximum file size allowed
			List<Settings> settingsListFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Maximum file size allowed"))
					.collect(Collectors.toList());
			if (settingsListFile.isEmpty()) {
				Settings settingsFile = new Settings();
				settingsFile.setName("Maximum file size allowed");
				settingsFile.setSection("File Settings");
				settingsFile.setType("Dropdown");
				settingsFile.setDefaultValue("20");
				settingsFile.setSystemSettingType("GENERAL_SETTING");

				List<Attributes> attributesListSettingsFile1 = new ArrayList<Attributes>();
				Attributes attributesFileSettings1 = new Attributes();
				attributesFileSettings1.setAttributeName("20MB");
				attributesFileSettings1.setAttributeValue("20MB");
				attributesFileSettings1.setSelected(true);
				attributesFileSettings1.setSettings(settingsFile);

				Attributes attributesFileSettings12 = new Attributes();
				attributesFileSettings12.setAttributeName("5MB");
				attributesFileSettings12.setAttributeValue("5MB");
				attributesFileSettings12.setSettings(settingsFile);

				Attributes attributesFileSettings13 = new Attributes();
				attributesFileSettings13.setAttributeName("10MB");
				attributesFileSettings13.setAttributeValue("10MB");
				attributesFileSettings13.setSettings(settingsFile);

				Attributes attributesFileSettings14 = new Attributes();
				attributesFileSettings14.setAttributeName("15MB");
				attributesFileSettings14.setAttributeValue("15MB");
				attributesFileSettings14.setSettings(settingsFile);

				Attributes attributesFileSettings15 = new Attributes();
				attributesFileSettings15.setAttributeName("25MB");
				attributesFileSettings15.setAttributeValue("25MB");
				attributesFileSettings15.setSettings(settingsFile);

				Attributes attributesFileSettings16 = new Attributes();
				attributesFileSettings16.setAttributeName("30MB");
				attributesFileSettings16.setAttributeValue("30MB");
				attributesFileSettings16.setSettings(settingsFile);

				Attributes attributesFileSettings17 = new Attributes();
				attributesFileSettings17.setAttributeName("50MB");
				attributesFileSettings17.setAttributeValue("50MB");
				attributesFileSettings17.setSettings(settingsFile);

				Attributes attributesFileSettings18 = new Attributes();
				attributesFileSettings18.setAttributeName("100MB");
				attributesFileSettings18.setAttributeValue("100MB");
				attributesFileSettings18.setSettings(settingsFile);

				attributesListSettingsFile1.add(attributesFileSettings1);
				attributesListSettingsFile1.add(attributesFileSettings12);
				attributesListSettingsFile1.add(attributesFileSettings13);
				attributesListSettingsFile1.add(attributesFileSettings14);
				attributesListSettingsFile1.add(attributesFileSettings15);
				attributesListSettingsFile1.add(attributesFileSettings16);
				attributesListSettingsFile1.add(attributesFileSettings17);
				attributesListSettingsFile1.add(attributesFileSettings18);

				settingsFile.setAttributesList(attributesListSettingsFile1);
				settingsService.save(settingsFile);
			}

			// pdf
			List<Settings> settingsListPDFFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("pdf")).collect(Collectors.toList());
			if (settingsListPDFFile.isEmpty()) {

				Settings settingsPdf = new Settings();
				settingsPdf.setName("pdf");
				settingsPdf.setSection("File Settings");
				settingsPdf.setType("Toggle On/Off");
				settingsPdf.setDefaultValue("On");
				settingsPdf.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsPdf);
			}
			// xlsx
			List<Settings> settingsListXlsxFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("xlsx")).collect(Collectors.toList());
			if (settingsListXlsxFile.isEmpty()) {

				Settings settingsXlsx = new Settings();
				settingsXlsx.setName("xlsx");
				settingsXlsx.setSection("File Settings");
				settingsXlsx.setType("Toggle On/Off");
				settingsXlsx.setDefaultValue("On");
				settingsXlsx.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsXlsx);
			}

			// txt
			List<Settings> settingsListTxtFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("txt")).collect(Collectors.toList());
			if (settingsListTxtFile.isEmpty()) {

				Settings settingsTxt = new Settings();
				settingsTxt.setName("txt");
				settingsTxt.setSection("File Settings");
				settingsTxt.setType("Toggle On/Off");
				settingsTxt.setDefaultValue("On");
				settingsTxt.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsTxt);
			}

			// jpg
			List<Settings> settingsListJpgFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("jpg")).collect(Collectors.toList());
			if (settingsListJpgFile.isEmpty()) {

				Settings settingsJpg = new Settings();
				settingsJpg.setName("jpg");
				settingsJpg.setSection("File Settings");
				settingsJpg.setType("Toggle On/Off");
				settingsJpg.setDefaultValue("On");
				settingsJpg.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsJpg);
			}

			// png
			List<Settings> settingsListPngFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("png")).collect(Collectors.toList());
			if (settingsListPngFile.isEmpty()) {

				Settings settingsPng = new Settings();
				settingsPng.setName("png");
				settingsPng.setSection("File Settings");
				settingsPng.setType("Toggle On/Off");
				settingsPng.setDefaultValue("On");
				settingsPng.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsPng);
			}

			// docx
			List<Settings> settingsListDocxFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("docx")).collect(Collectors.toList());
			if (settingsListDocxFile.isEmpty()) {

				Settings settingsDocx = new Settings();
				settingsDocx.setName("docx");
				settingsDocx.setSection("File Settings");
				settingsDocx.setType("Toggle On/Off");
				settingsDocx.setDefaultValue("On");
				settingsDocx.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsDocx);
			}

			// csv
			List<Settings> settingsListCsvFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("csv")).collect(Collectors.toList());
			if (settingsListCsvFile.isEmpty()) {

				Settings settingsCsv = new Settings();
				settingsCsv.setName("csv");
				settingsCsv.setSection("File Settings");
				settingsCsv.setType("Toggle On/Off");
				settingsCsv.setDefaultValue("On");
				settingsCsv.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsCsv);
			}

			// gif
			List<Settings> settingsListGifFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("gif")).collect(Collectors.toList());
			if (settingsListGifFile.isEmpty()) {

				Settings settingsGif = new Settings();
				settingsGif.setName("gif");
				settingsGif.setSection("File Settings");
				settingsGif.setType("Toggle On/Off");
				settingsGif.setDefaultValue("On");
				settingsGif.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsGif);
			}

			// rtf
			List<Settings> settingsListRtfFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("rtf")).collect(Collectors.toList());
			if (settingsListRtfFile.isEmpty()) {

				Settings settingsRtf = new Settings();
				settingsRtf.setName("rtf");
				settingsRtf.setSection("File Settings");
				settingsRtf.setType("Toggle On/Off");
				settingsRtf.setDefaultValue("On");
				settingsRtf.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsRtf);
			}

			// tiff
			List<Settings> settingsListTiffFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("tiff")).collect(Collectors.toList());
			if (settingsListTiffFile.isEmpty()) {

				Settings settingsTiff = new Settings();
				settingsTiff.setName("tiff");
				settingsTiff.setSection("File Settings");
				settingsTiff.setType("Toggle On/Off");
				settingsTiff.setDefaultValue("On");
				settingsTiff.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsTiff);
			}

			// snt(Sticky Note)
			List<Settings> settingsListSNTFile = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("snt")).collect(Collectors.toList());
			if (settingsListSNTFile.isEmpty()) {

				Settings settingsSNT = new Settings();
				settingsSNT.setName("snt");
				settingsSNT.setSection("File Settings");
				settingsSNT.setType("Toggle On/Off");
				settingsSNT.setDefaultValue("On");
				settingsSNT.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settingsSNT);
			}

			// only mail settings
			List<Settings> mailList = settingsService.findAll().stream()
					.filter(setting -> setting.getSystemSettingType().equals("MAIL_SETTING"))
					.collect(Collectors.toList());
			if (mailList.isEmpty()) {
				List<Settings> listSettings = new ArrayList<Settings>();
				// mail settings
				Settings settings6 = new Settings();
				settings6.setName("Outbound SMTP Server");
				settings6.setSection("Account Settings");
				settings6.setType("Text");
				settings6.setRequired(true);
				settings6.setMaximumLength(100);
				settings6.setDefaultValue("");
				settings6.setValidation("Server/IP validator");
				settings6.setSystemSettingType("MAIL_SETTING");

				Settings settings7 = new Settings();
				settings7.setName("Port");
				settings7.setSection("Account Settings");
				settings7.setType("Text");
				settings7.setRequired(true);
				settings7.setMaximumLength(4);
				settings7.setDefaultValue("25");
				settings7.setValidation("Numeric");
				settings7.setSystemSettingType("MAIL_SETTING");

				Settings settings8 = new Settings();
				settings8.setName("SSL");
				settings8.setSection("Account Settings");
				settings8.setType("Toggle On/Off");
				settings8.setRequired(false);
				settings8.setValidation("");
				settings8.setDefaultValue("On");
				settings8.setSystemSettingType("MAIL_SETTING");

				Settings settings9 = new Settings();
				settings9.setName("Enable Authentication");
				settings9.setSection("Authentication");
				settings9.setType("Toggle On/Off");
				settings9.setRequired(false);
				settings9.setDefaultValue("On");
				settings9.setValidation("");
				settings9.setSystemSettingType("MAIL_SETTING");

				Settings settings10 = new Settings();
				settings10.setName("User");
				settings10.setSection("Authentication");
				settings10.setType("Text");
				settings10.setRequired(true);
				settings10.setMaximumLength(80);
				settings10.setDefaultValue("");
				settings10.setValidation("");
				settings10.setSystemSettingType("MAIL_SETTING");

				Settings settings11 = new Settings();
				settings11.setName("Password");
				settings11.setSection("Authentication");
				settings11.setType("Password");
				settings11.setRequired(true);
				settings11.setMaximumLength(30);
				settings11.setDefaultValue("");
				settings11.setValidation("Password rules as specified below");
				settings11.setSystemSettingType("MAIL_SETTING");

				Settings settings12 = new Settings();
				settings12.setName("Retype Password");
				settings12.setSection("Authentication");
				settings12.setType("Password");
				settings12.setRequired(true);
				settings12.setMaximumLength(30);
				settings12.setDefaultValue("");
				settings12.setValidation("Must much to Password field incase Password field is valid");
				settings12.setSystemSettingType("MAIL_SETTING");

				Settings settings13 = new Settings();
				settings13.setName("From");
				settings13.setSection("General");
				settings13.setType("Email");
				settings13.setRequired(true);
				settings13.setMaximumLength(50);
				settings13.setDefaultValue("info@ahex.co.in");
				settings13.setValidation("Email address validator");
				settings13.setSystemSettingType("MAIL_SETTING");

				listSettings.add(settings6);
				listSettings.add(settings7);
				listSettings.add(settings8);
				listSettings.add(settings9);
				listSettings.add(settings10);
				listSettings.add(settings11);
				listSettings.add(settings12);
				listSettings.add(settings13);

				for (Settings settings : listSettings) {
					settingsService.save(settings);
				}
			}

			if (versionService.findAll().isEmpty()) {
				ProductVersion newProductVersion = new ProductVersion();
				newProductVersion.setReleaseDate(new Date());
				newProductVersion.setVersion(productVersion);
				newProductVersion.setDescription("Latest Version");
				versionService.save(newProductVersion);
			} else {
				ProductVersion currentVersion = versionService.getLatestVersion();
				if (!currentVersion.getVersion().equals(productVersion)) {
					ProductVersion newProductVersion = new ProductVersion();
					newProductVersion.setReleaseDate(new Date());
					newProductVersion.setVersion(productVersion);
					newProductVersion.setDescription("Latest Version");
					versionService.save(newProductVersion);
				}
			}

			// for classification and keywords auditing
			List<Classification> classificationsList = classificationService.findAll();
			if (classificationsList.isEmpty()) {
				List<Classification> classificationsListToSave = new ArrayList<Classification>();
				Classification classification1 = new Classification();
				classification1.setName("Corruption");
				List<Keywords> keywordsList1 = new ArrayList<>();
				Keywords keywords11 = new Keywords();
				keywords11.setKeyword("bribery");
				Keywords keywords12 = new Keywords();
				keywords12.setKeyword("bribe");
				Keywords keywords13 = new Keywords();
				keywords13.setKeyword("abuse");
				Keywords keywords14 = new Keywords();
				keywords14.setKeyword("corruption");
				keywordsList1.add(keywords11);
				keywordsList1.add(keywords12);
				keywordsList1.add(keywords13);
				keywordsList1.add(keywords14);
				for (Keywords keywords : keywordsList1) {
					keywords.setClassification(classification1);
				}
				classification1.setKeyWords(keywordsList1);

				Classification classification2 = new Classification();
				classification2.setName("Fraud");
				List<Keywords> keywordsList2 = new ArrayList<>();
				Keywords keywords21 = new Keywords();
				keywords21.setKeyword("accounting scheme");
				Keywords keywords22 = new Keywords();
				keywords22.setKeyword("falsified documentation");
				Keywords keywords23 = new Keywords();
				keywords23.setKeyword("fraud");
				keywordsList2.add(keywords21);
				keywordsList2.add(keywords22);
				keywordsList2.add(keywords23);
				for (Keywords keywords : keywordsList2) {
					keywords.setClassification(classification2);
				}
				classification2.setKeyWords(keywordsList2);

				Classification classification3 = new Classification();
				classification3.setName("Financial");
				List<Keywords> keywordsList3 = new ArrayList<>();
				Keywords keywords31 = new Keywords();
				keywords31.setKeyword("bankrupt");
				Keywords keywords32 = new Keywords();
				keywords32.setKeyword("insolvency");
				Keywords keywords33 = new Keywords();
				keywords33.setKeyword("liquidation");
				keywordsList3.add(keywords31);
				keywordsList3.add(keywords32);
				keywordsList3.add(keywords33);
				for (Keywords keywords : keywordsList3) {
					keywords.setClassification(classification3);
				}
				classification3.setKeyWords(keywordsList3);

				Classification classification4 = new Classification();
				classification4.setName("Terrorism");
				List<Keywords> keywordsList4 = new ArrayList<>();
				Keywords keywords41 = new Keywords();
				keywords41.setKeyword("terrorist organization");

				Keywords keywords42 = new Keywords();
				keywords42.setKeyword("terrorist");

				Keywords keywords43 = new Keywords();
				keywords43.setKeyword("financed terrorism");
				Keywords keywords44 = new Keywords();
				keywords44.setKeyword("terrorist attack");
				Keywords keywords45 = new Keywords();
				keywords45.setKeyword("terrorism");
				keywordsList4.add(keywords41);
				keywordsList4.add(keywords42);
				keywordsList4.add(keywords43);
				keywordsList4.add(keywords44);
				keywordsList4.add(keywords45);
				for (Keywords keywords : keywordsList4) {
					keywords.setClassification(classification4);
				}
				classification4.setKeyWords(keywordsList4);

				Classification classification5 = new Classification();
				classification5.setName("Adverse media narcotics");
				List<Keywords> keywordsList5 = new ArrayList<>();
				Keywords keywords51 = new Keywords();
				keywords51.setKeyword("drug dealer");
				Keywords keywords52 = new Keywords();
				keywords52.setKeyword("illicit drug trafficking");
				Keywords keywords53 = new Keywords();
				keywords53.setKeyword("drug cartel");
				keywordsList5.add(keywords51);
				keywordsList5.add(keywords52);
				keywordsList5.add(keywords53);
				for (Keywords keywords : keywordsList5) {
					keywords.setClassification(classification5);
				}
				classification5.setKeyWords(keywordsList5);

				Classification classification6 = new Classification();
				classification6.setName("Organized crime");
				List<Keywords> keywordsList6 = new ArrayList<>();
				Keywords keywords61 = new Keywords();
				keywords61.setKeyword("arms trafficking");
				Keywords keywords62 = new Keywords();
				keywords62.setKeyword("human trafficking");
				Keywords keywords63 = new Keywords();
				keywords63.setKeyword("organ trafficking");
				keywordsList6.add(keywords61);
				keywordsList6.add(keywords62);
				keywordsList6.add(keywords63);
				for (Keywords keywords : keywordsList6) {
					keywords.setClassification(classification6);
				}
				classification6.setKeyWords(keywordsList6);

				Classification classification7 = new Classification();
				classification7.setName("Sexual Crime");
				List<Keywords> keywordsList7 = new ArrayList<>();
				Keywords keywords71 = new Keywords();
				keywords71.setKeyword("sexual abuse");
				Keywords keywords72 = new Keywords();
				keywords72.setKeyword("rapist");
				Keywords keywords73 = new Keywords();
				keywords73.setKeyword("sexual assault");
				keywordsList7.add(keywords71);
				keywordsList7.add(keywords72);
				keywordsList7.add(keywords73);
				for (Keywords keywords : keywordsList7) {
					keywords.setClassification(classification7);
				}
				classification7.setKeyWords(keywordsList7);

				Classification classification8 = new Classification();
				classification8.setName("Adverse media competitive");
				List<Keywords> keywordsList8 = new ArrayList<>();
				Keywords keywords81 = new Keywords();
				keywords81.setKeyword("anticompetitive");
				Keywords keywords82 = new Keywords();
				keywords82.setKeyword("Information Rights/Copyright/Patent Issues");
				Keywords keywords83 = new Keywords();
				keywords83.setKeyword("antiturst");
				keywordsList8.add(keywords81);
				keywordsList8.add(keywords82);
				keywordsList8.add(keywords83);
				for (Keywords keywords : keywordsList8) {
					keywords.setClassification(classification8);
				}
				classification8.setKeyWords(keywordsList8);

				Classification classification9 = new Classification();
				classification9.setName("Tax-evasion");
				List<Keywords> keywordsList9 = new ArrayList<>();
				Keywords keywords91 = new Keywords();
				keywords91.setKeyword("tax fraud");
				Keywords keywords92 = new Keywords();
				keywords92.setKeyword("VAT fraud");
				Keywords keywords93 = new Keywords();
				keywords93.setKeyword("phantom firms");
				keywordsList9.add(keywords91);
				keywordsList9.add(keywords92);
				keywordsList9.add(keywords93);
				for (Keywords keywords : keywordsList9) {
					keywords.setClassification(classification9);
				}
				classification9.setKeyWords(keywordsList9);

				Classification classification10 = new Classification();
				classification10.setName("Fines");
				List<Keywords> keywordsList10 = new ArrayList<>();
				Keywords keywords101 = new Keywords();
				keywords101.setKeyword("criminal fine");
				Keywords keywords102 = new Keywords();
				keywords102.setKeyword("penalties");
				keywordsList10.add(keywords101);
				keywordsList10.add(keywords102);
				for (Keywords keywords : keywordsList10) {
					keywords.setClassification(classification10);
				}
				classification10.setKeyWords(keywordsList10);

				Classification classification11 = new Classification();
				classification11.setName("Money Laundering ");
				List<Keywords> keywordsList11 = new ArrayList<>();
				Keywords keywords111 = new Keywords();
				keywords111.setKeyword("illicit money");
				Keywords keywords112 = new Keywords();
				keywords112.setKeyword("suspicious transactions");
				Keywords keywords113 = new Keywords();
				keywords113.setKeyword("money trade");
				Keywords keywords114 = new Keywords();
				keywords114.setKeyword("dirty money");
				Keywords keywords115 = new Keywords();
				keywords115.setKeyword("money laundering");
				keywordsList11.add(keywords111);
				keywordsList11.add(keywords112);
				keywordsList11.add(keywords113);
				keywordsList11.add(keywords114);
				keywordsList11.add(keywords115);
				for (Keywords keywords : keywordsList11) {
					keywords.setClassification(classification11);
				}
				classification11.setKeyWords(keywordsList11);

				classificationsListToSave.add(classification1);
				classificationsListToSave.add(classification2);
				classificationsListToSave.add(classification3);
				classificationsListToSave.add(classification4);
				classificationsListToSave.add(classification5);
				classificationsListToSave.add(classification6);
				classificationsListToSave.add(classification7);
				classificationsListToSave.add(classification8);
				classificationsListToSave.add(classification9);
				classificationsListToSave.add(classification10);
				classificationsListToSave.add(classification11);

				for (Classification classificationTOSave : classificationsListToSave) {
					classificationService.save(classificationTOSave);
				}
			}

			// adding media information
			if (sourceMediaSourceService.findAll().isEmpty()) {
				List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
				SourceMedia media1 = new SourceMedia("Doc");
				SourceMedia media2 = new SourceMedia("Excel");
				SourceMedia media3 = new SourceMedia("PPT");
				SourceMedia media4 = new SourceMedia("PDF");
				SourceMedia media5 = new SourceMedia("Movies");
				SourceMedia media6 = new SourceMedia("Audio");
				SourceMedia media7 = new SourceMedia("All");
				mediaList.add(media1);
				mediaList.add(media2);
				mediaList.add(media3);
				mediaList.add(media4);
				mediaList.add(media5);
				mediaList.add(media6);
				mediaList.add(media7);
				for (SourceMedia sourceMedia : mediaList) {
					sourceMediaSourceService.save(sourceMedia);
				}
			}

			// adding new stock
			List<SourceMedia> sourceMediaList = sourceMediaSourceService.findAll().stream()
					.filter(media -> media.getMediaName().equals("Screenshot")).collect(Collectors.toList());
			if (sourceMediaList.isEmpty()) {
				SourceMedia media = new SourceMedia("Screenshot");
				sourceMediaSourceService.save(media);
			}

			// adding domain information
			if (sourceDomainService.findAll().isEmpty()) {
				List<SourceDomain> domainList = new ArrayList<SourceDomain>();
				SourceDomain domain1 = new SourceDomain("General");
				SourceDomain domain2 = new SourceDomain("Financial Crime");
				SourceDomain domain3 = new SourceDomain("Cyber Security");
				SourceDomain domain4 = new SourceDomain("Compliance");
				SourceDomain domain5 = new SourceDomain("Market Sonar");
				domainList.add(domain1);
				domainList.add(domain2);
				domainList.add(domain3);
				domainList.add(domain4);
				domainList.add(domain5);
				for (SourceDomain sourceDomain : domainList) {
					sourceDomainService.save(sourceDomain);
				}
			}

			// adding industry information
			if (sourceIndustryService.findAll().isEmpty()) {
				List<String[]> allIndustries = null;
				List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
				File file = ResourceUtils.getFile("classpath:SourceCredibilityIndustry.csv");
				try (FileReader reader = new FileReader(file);) {
					FileReader filereader = new FileReader(file);
					CSVReader csvReader = new CSVReaderBuilder(filereader).build();
					allIndustries = csvReader.readAll();
				}
				for (String sourceIndustry : allIndustries.get(0)) {
					SourceIndustry industry = new SourceIndustry();
					industry.setIndustryName(sourceIndustry);
					industryList.add(industry);
				}
				for (SourceIndustry sourceIndustry : industryList) {
					sourceIndustryService.save(sourceIndustry);
				}
			}

			// adding jurisdiction information
			if (sourceJurisdictionService.findAll().isEmpty()) {

				List<SourceJurisdiction> srcJDList = new ArrayList<SourceJurisdiction>();
				srcJDList.add(new SourceJurisdiction("All", "All", true));
				srcJDList.add(new SourceJurisdiction("GB", "United Kingdom", false));
				srcJDList.add(new SourceJurisdiction("RU", "Russia", false));
				srcJDList.add(new SourceJurisdiction("DE", "Germany", false));
				srcJDList.add(new SourceJurisdiction("FR", "France", false));
				srcJDList.add(new SourceJurisdiction("BE", "Belgium", false));
				srcJDList.add(new SourceJurisdiction("NL", "Netherlands", false));
				sourceJurisdictionService.saveAll(srcJDList);

			}

			// adding new jurisdictions
			if (!sourceJurisdictionService.findAll().isEmpty()) {
				
				Map<String, String> mapToInsert = new HashMap<String, String>();
				// ObjectMapper mapper = new ObjectMapper();
				List<NewJurisdictionDto> jurisdictionsList = new ArrayList<>();
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				URL resource = classLoader.getResource("newJurisdictions.json");
				File file = new File(resource.toURI());
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(file));
				JSONObject jsonObject = new JSONObject(object.toString());
				if (jsonObject.has("data")) {
					JSONArray dataArray = jsonObject.getJSONArray("data");
					for (int i = 0; i < dataArray.length(); i++) {
						NewJurisdictionDto jurisdictionObj = mapper.readValue(dataArray.get(i).toString(),
								NewJurisdictionDto.class);
						jurisdictionsList.add(jurisdictionObj);
					}
				}
				if (jurisdictionsList != null && jurisdictionsList.size() > 0) {
					for (NewJurisdictionDto jurisdiction : jurisdictionsList) {
						mapToInsert.put(jurisdiction.getCountries(), jurisdiction.getCountryCode());
					}
				}
				/////////////////
				List<SourceJurisdiction> jurisdictions = sourceJurisdictionService.findAll();
				jurisdictions.stream().filter(f -> null != f.getJurisdictionName());
				jurisdictions.stream().filter(f -> null != f.getJurisdictionOriginalName());
				
				Map<String, String> keysFromDB = jurisdictions.stream()
						.collect(Collectors.toMap(SourceJurisdiction::getJurisdictionName,
								SourceJurisdiction::getJurisdictionOriginalName, (key1, key2) -> key1));
				for (Entry<String, String> map : mapToInsert.entrySet()) {
					if (keysFromDB.containsKey(map.getValue())) {
						SourceJurisdiction jurisdictionNL = sourceJurisdictionService.fetchJurisdiction(map.getValue());
						if (jurisdictionNL != null && jurisdictionNL.getJurisdictionOriginalName() != null
								&& jurisdictionNL.getJurisdictionOriginalName().equals("")) {
							jurisdictionNL.setJurisdictionOriginalName(map.getKey());
							sourceJurisdictionService.saveOrUpdate(jurisdictionNL);
						}
						// do nothing
					} else {
						SourceJurisdiction jurisdictionNL = new SourceJurisdiction(map.getValue(), map.getKey(), false);
						sourceJurisdictionService.save(jurisdictionNL);
					}
				}
			}

			// load Jurisdictions into List Management
			List<SourceJurisdiction> jurisDictionList = sourceJurisdictionService.findAll();
			List<ListItemDto> jurisDictionsListItemDto = listItemService.getListItemByListType(ElementConstants.JURISDICTIONS_STRING);
			if (jurisDictionsListItemDto != null && jurisDictionsListItemDto.size() == 0){
				for (SourceJurisdiction jurisDiction : jurisDictionList) {
					ListItem newListItem = new ListItem();
					newListItem.setDisplayName(jurisDiction.getJurisdictionOriginalName());
					newListItem.setCode(jurisDiction.getJurisdictionName());
					newListItem.setListType(ElementConstants.JURISDICTIONS_STRING);
					newListItem.setAllowDelete(true);
					newListItem.setColorCode("657f8b");
					newListItem.setIcon("ban");
					newListItem = listItemService.save(newListItem);
					
					jurisDiction.setListItemId(newListItem.getListItemId());
					if(newListItem.getCode().equalsIgnoreCase(jurisDiction.getJurisdictionName())){
						sourceJurisdictionService.update(jurisDiction);
					}
				}
			}else if(jurisDictionsListItemDto != null && jurisDictionsListItemDto.size() > 0){
				for (SourceJurisdiction jurisDiction : jurisDictionList) {
					for (ListItemDto listItem : jurisDictionsListItemDto) {
						if ((null != listItem.getCode() && null != jurisDiction.getJurisdictionName() 
								&&  listItem.getCode().equalsIgnoreCase(jurisDiction.getJurisdictionName())) 
								|| (null != listItem.getDisplayName() && null != jurisDiction.getJurisdictionOriginalName()
								&&listItem.getDisplayName().equalsIgnoreCase(jurisDiction.getJurisdictionOriginalName()))) {
							if(null != listItem.getListItemId()){
								jurisDiction.setListItemId(listItem.getListItemId());
								if(listItem.getCode().equalsIgnoreCase(jurisDiction.getJurisdictionName())){
									sourceJurisdictionService.update(jurisDiction);
								}
								
							}
						}
					}
					
				}
				List<ListItemDto> jurisDictionsListItemDtoUpdated = listItemService.getListItemByListType(ElementConstants.JURISDICTIONS_STRING);
				Map<String, String> listJurisdictionMap = jurisDictionsListItemDtoUpdated.stream()
						.collect(Collectors.toMap(ListItemDto::getCode,
								ListItemDto::getDisplayName, (key1, key2) -> key1));
				Set<String> listItemSet = listJurisdictionMap.keySet();
				
				List<SourceJurisdiction> jurisDictionListUpdated = sourceJurisdictionService.findAll();
				Map<String, String> scJurisdictionMap = jurisDictionListUpdated.stream()
						.collect(Collectors.toMap(SourceJurisdiction::getJurisdictionName,
								SourceJurisdiction::getJurisdictionOriginalName, (key1, key2) -> key1));
				Set<String> scJurisdictionSet = scJurisdictionMap.keySet();
				
				Set<String> DifferListSet = Sets.difference(listItemSet, scJurisdictionSet);
				for (ListItemDto listItem : jurisDictionsListItemDtoUpdated) {
					if(DifferListSet.contains(listItem.getCode())){
						SourceJurisdiction jurisdictionNew = new SourceJurisdiction();
						jurisdictionNew.setSelected(false);
						jurisdictionNew.setJurisdictionName(listItem.getCode());
						jurisdictionNew.setJurisdictionOriginalName(listItem.getDisplayName());
						jurisdictionNew.setListItemId(listItem.getListItemId());
						sourceJurisdictionService.create(jurisdictionNew);
					}
					
				}
				List<ListItemDto> jurisDictionsListItemDtoUpdated1 = listItemService.getListItemByListType(ElementConstants.JURISDICTIONS_STRING);
				Map<String, String> listJurisdictionMap1 = jurisDictionsListItemDtoUpdated1.stream()
						.collect(Collectors.toMap(ListItemDto::getCode,
								ListItemDto::getDisplayName, (key1, key2) -> key1));
				Set<String> listItemSet1 = listJurisdictionMap1.keySet();
				
				List<SourceJurisdiction> jurisDictionListUpdated1 = sourceJurisdictionService.findAll();
				Map<String, String> scJurisdictionMap1 = jurisDictionListUpdated1.stream()
						.collect(Collectors.toMap(SourceJurisdiction::getJurisdictionName,
								SourceJurisdiction::getJurisdictionOriginalName, (key1, key2) -> key1));
				Set<String> scJurisdictionSet1 = scJurisdictionMap1.keySet();
				
				Set<String> differScJurisdictionSet = Sets.difference(scJurisdictionSet1, listItemSet1);
				
				for (SourceJurisdiction jurisDiction : jurisDictionListUpdated1) {
					if(differScJurisdictionSet.contains(jurisDiction.getJurisdictionName())){
						ListItem newListItem = new ListItem();
						newListItem.setDisplayName(jurisDiction.getJurisdictionOriginalName());
						newListItem.setCode(jurisDiction.getJurisdictionName());
						newListItem.setListType(ElementConstants.JURISDICTIONS_STRING);
						newListItem.setAllowDelete(true);
						newListItem.setColorCode("657f8b");
						newListItem.setIcon("ban");
						newListItem = listItemService.save(newListItem);
						
						if(newListItem.getCode().equalsIgnoreCase(jurisDiction.getJurisdictionName())){
							jurisDiction.setListItemId(newListItem.getListItemId());
							sourceJurisdictionService.update(jurisDiction);
						}
					}
				}
				/*List<SourceJurisdiction> jurisDictionListUpdated1 = sourceJurisdictionService.findAll();
				List<ListItemDto> jurisDictionsListItemDtoUpdated1 = listItemService.getListItemByListType(ElementConstants.JURISDICTIONS_STRING);
				Map<String, String> scJurisdictionMap = jurisDictionListUpdated1.stream()
						.collect(Collectors.toMap(SourceJurisdiction::getJurisdictionName,
								SourceJurisdiction::getJurisdictionOriginalName, (key1, key2) -> key1));
				for (ListItemDto listItem : jurisDictionsListItemDtoUpdated1) {
					if(!scJurisdictionMap.containsKey(listItem.getCode())){
						SourceJurisdiction existingJD = sourceJurisdictionService.findByListItemId(listItem.getListItemId());
						if(null == existingJD){
							SourceJurisdiction jurisdictionNew = new SourceJurisdiction();
							jurisdictionNew.setSelected(false);
							jurisdictionNew.setJurisdictionName(listItem.getCode());
							jurisdictionNew.setJurisdictionOriginalName(listItem.getDisplayName());
							jurisdictionNew.setListItemId(listItem.getListItemId());
							sourceJurisdictionService.create(jurisdictionNew);
						}
					}
				}*/
				
			}

			//load Alert Status into List Management
			List<ListItemDto> alertStatusListItemDto =listItemService.getListItemByListType("Alert Status");
			if(alertStatusListItemDto!=null && alertStatusListItemDto.size()==0) {
					/*ListItemDto listItemDto1 = new ListItemDto();
					listItemDto1.setDisplayName("Open");
					listItemDto1.setCode("Open");
					listItemDto1.setListType("Alert Status");
					listItemDto1.setAllowDelete(false);
					listItemDto1.setColorCode("f28618");
					listItemDto1.setIcon("exclamation-triangle");*/
					
					/*ListItemDto listItemDto2 = new ListItemDto();
					listItemDto2.setDisplayName("Investigation");
					listItemDto2.setCode("Investigation");
					listItemDto2.setListType("Alert Status");
					listItemDto2.setAllowDelete(false);
					listItemDto2.setColorCode("3eb6ff");
					listItemDto2.setIcon("play-circle");*/
					
					/*ListItemDto listItemDto3 = new ListItemDto();
					listItemDto3.setDisplayName("Awaiting additional Info");
					listItemDto3.setCode("Awaiting additional Info");
					listItemDto3.setListType("Alert Status");
					listItemDto3.setAllowDelete(false);
					listItemDto3.setColorCode("febd12");
					listItemDto3.setIcon("pause-circle");*/
					
					/*ListItemDto listItemDto4 = new ListItemDto();
					listItemDto4.setDisplayName("False Positive");
					listItemDto4.setCode("False Positive");
					listItemDto4.setListType("Alert Status");
					listItemDto4.setAllowDelete(false);
					listItemDto4.setColorCode("747474");
					listItemDto4.setIcon("exclamation-triangle");*/
					
					/*ListItemDto listItemDto5 = new ListItemDto();
					listItemDto5.setDisplayName("True Match");
					listItemDto5.setCode("True Match");
					listItemDto5.setListType("Alert Status");
					listItemDto5.setAllowDelete(false);
					listItemDto5.setColorCode("ef5350");
					listItemDto5.setIcon("exclamation-triangle");*/
					
					/*ListItemDto listItemDto6 = new ListItemDto();
					listItemDto6.setDisplayName("Customer Decline");
					listItemDto6.setCode("Customer Decline");
					listItemDto6.setListType("Alert Status");
					listItemDto6.setAllowDelete(false);
					listItemDto6.setColorCode("545454");
					listItemDto6.setIcon("times-circle");*/
					
					ListItemDto listItemDto7 = new ListItemDto();
					listItemDto7.setDisplayName(ElementConstants.ALERT_NEW_STATUS);
					listItemDto7.setCode(ElementConstants.ALERT_NEW_STATUS);
					listItemDto7.setListType(ElementConstants.ALERT_STATUS_TYPE);
					listItemDto7.setAllowDelete(false);
					listItemDto7.setColorCode("545454");
					listItemDto7.setIcon("times-circle");
					
					//listItemService.saveOrUpdateListItem(listItemDto1);
					//listItemService.saveOrUpdateListItem(listItemDto2);
					//listItemService.saveOrUpdateListItem(listItemDto3);
					//listItemService.saveOrUpdateListItem(listItemDto4);
					//listItemService.saveOrUpdateListItem(listItemDto5);
					//listItemService.saveOrUpdateListItem(listItemDto6);
					listItemService.saveOrUpdateListItem(listItemDto7);
				}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.ALERT_NEW_STATUS.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.ALERT_NEW_STATUS);
				listItemDto.setCode(ElementConstants.ALERT_NEW_STATUS);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("545454");
				listItemDto.setIcon("times-circle");
				listItemService.save(listItemDto);
			}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.IDENTITY_APPROVED_STATUS.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.IDENTITY_APPROVED_STATUS);
				listItemDto.setCode(ElementConstants.IDENTITY_APPROVED_STATUS);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("00796b");
				listItemDto.setIcon("check");
				listItemService.save(listItemDto);
			}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.IDENTITY_REJECTED_STATUS.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.IDENTITY_REJECTED_STATUS);
				listItemDto.setCode(ElementConstants.IDENTITY_REJECTED_STATUS);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("db1818");
				listItemDto.setIcon("times");
				listItemService.save(listItemDto);
			}
			
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.ENTITY_APPROVED_NEEDED_REVIEW.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.ENTITY_APPROVED_NEEDED_REVIEW);
				listItemDto.setCode(ElementConstants.ENTITY_APPROVED_NEEDED_REVIEW);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("00796b");
				listItemDto.setIcon("check");
				listItemService.save(listItemDto);
			}
			
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.ENTITY_REJECTED_NEEDED_REVIEW.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.ENTITY_REJECTED_NEEDED_REVIEW);
				listItemDto.setCode(ElementConstants.ENTITY_REJECTED_NEEDED_REVIEW);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("db1818");
				listItemDto.setIcon("times");
				listItemService.save(listItemDto);
			}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.ALERT_APPROVED_STATUS.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.ALERT_APPROVED_STATUS);
				listItemDto.setCode(ElementConstants.ALERT_APPROVED_STATUS);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("ef5350");
				listItemDto.setIcon("exclamation-triangle");
				listItemService.save(listItemDto);
			}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.APPROVED_NEEDED_REVIEW.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.APPROVED_NEEDED_REVIEW);
				listItemDto.setCode(ElementConstants.APPROVED_NEEDED_REVIEW);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("ef5350");
				listItemDto.setIcon("exclamation-triangle");
				listItemService.save(listItemDto);
			}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.ALERT_REJECTED_STATUS.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.ALERT_REJECTED_STATUS);
				listItemDto.setCode(ElementConstants.ALERT_REJECTED_STATUS);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("747474");
				listItemDto.setIcon("exclamation-triangle");
				listItemService.save(listItemDto);
			}
			if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.REJECTED_NEEDED_REVIEW.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.REJECTED_NEEDED_REVIEW);
				listItemDto.setCode(ElementConstants.REJECTED_NEEDED_REVIEW);
				listItemDto.setListType(ElementConstants.ALERT_STATUS_TYPE);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("747474");
				listItemDto.setIcon("exclamation-triangle");
				listItemService.save(listItemDto);
			}
			
			Groups entityIdentification = groupsService.getGroup(ElementConstants.ENTITY_IDENTIFICATION);
			if (entityIdentification == null) {

				Groups entityIdentificationgroup = new Groups();
				entityIdentificationgroup.setActive(true);
				entityIdentificationgroup.setCreatedDate(new Date());
				entityIdentificationgroup.setDescription(ElementConstants.ENTITY_IDENTIFICATION);
				entityIdentificationgroup.setName(ElementConstants.ENTITY_IDENTIFICATION);
				entityIdentificationgroup.setRemarks(ElementConstants.ENTITY_IDENTIFICATION);
				if (admin != null)
					entityIdentificationgroup.setCreatedBy(admin.getUserId());
				else {
					admin = usersService.getUserObjectByEmailOrUsername("admin");
					if (admin != null && admin.getUserId() != null)
						entityIdentificationgroup.setCreatedBy(admin.getUserId());
					// }
					entityIdentification = groupsService.save(entityIdentificationgroup);

					UserGroups user = userGroupsService.save(new UserGroups(entityIdentificationgroup, admin));

				}	
			}
			
			/*if(!listItemService.findAll().stream().anyMatch(l -> ElementConstants.ENTITY_IDENTIFICATION.equalsIgnoreCase(l.getDisplayName()))) {
				ListItem listItemDto = new ListItem();
				listItemDto.setDisplayName(ElementConstants.ENTITY_IDENTIFICATION);
				listItemDto.setCode(ElementConstants.ENTITY_IDENTIFICATION);
				listItemDto.setListType(ElementConstants.FEED_CLASSIFICATION_STRING);
				listItemDto.setAllowDelete(false);
				listItemDto.setColorCode("db1818");
				listItemDto.setIcon("times");
				listItemService.save(listItemDto);
			}
			
			if(!feedService.findAll().stream().anyMatch(f -> ElementConstants.ENTITY_IDENTIFICATION.equalsIgnoreCase(f.getFeedName()))) {
				
				List<ListItem> list = listItemService.findAll().stream()
						.filter(listItem -> "New".equals(listItem.getDisplayName())).collect(Collectors.toList());
				
				FeedManagement feed = new FeedManagement();
				feed.setColor("blue");
				feed.setFeedName(ElementConstants.ENTITY_IDENTIFICATION);
				feed.setSource(0);
				feed.setType(0);
				feed.setIsReviewerRequired(false);
				//feed.setReviewer(list);
				feed = feedService.save(feed);
				
				Groups entityIdentification = groupsService.getGroup(ElementConstants.ENTITY_IDENTIFICATION);
				if (entityIdentification == null) {

					Groups entityIdentificationgroup = new Groups();
					entityIdentificationgroup.setActive(true);
					entityIdentificationgroup.setCreatedDate(new Date());
					entityIdentificationgroup.setDescription(ElementConstants.ENTITY_IDENTIFICATION);
					entityIdentificationgroup.setName(ElementConstants.ENTITY_IDENTIFICATION);
					entityIdentificationgroup.setRemarks(ElementConstants.ENTITY_IDENTIFICATION);
					if (admin != null)
						entityIdentificationgroup.setCreatedBy(admin.getUserId());
					else {
						admin = usersService.getUserObjectByEmailOrUsername("admin");
						if (admin != null && admin.getUserId() != null)
							entityIdentificationgroup.setCreatedBy(admin.getUserId());
					//}
						entityIdentification = groupsService.save(entityIdentificationgroup);

						UserGroups	user = userGroupsService.save(new UserGroups(entityIdentificationgroup, admin));

				}
				
				FeedGroups feedGroup = new FeedGroups();
				feedGroup.setFeedId(feed);
				feedGroup.setRank(0);
				feedGroup.setGroupId(entityIdentification);
				
				feedGoupService.saveOrUpdate(feedGroup);
			}
			*/
			// load Domains into List Management
			List<SourceDomainDto> sourceDomainDtos = sourceDomainService.getSourceDomain();
			List<ListItemDto> domainsListItemDto = listItemService.getListItemByListType("Domains");
			if (domainsListItemDto != null && domainsListItemDto.size() == 0)
				for (SourceDomainDto domain : sourceDomainDtos) {
					ListItemDto listItemDto = new ListItemDto();
					listItemDto.setDisplayName(domain.getDomainName());
					listItemDto.setCode(domain.getDomainName());
					listItemDto.setListType("Domains");
					listItemDto.setAllowDelete(true);
					listItemDto.setColorCode("657f8b");
					listItemDto.setIcon("ban");
					listItemService.saveOrUpdateListItem(listItemDto);
				}

			// load Industry into List Management
			List<SourceIndustryDto> industryDtos = sourceIndustryService.getSourceIndustry();
			List<ListItemDto> industrysListItemDto = listItemService.getListItemByListType("Industry");
			if (industrysListItemDto != null && industrysListItemDto.size() == 0)
				for (SourceIndustryDto industry : industryDtos) {
					ListItemDto listItemDto = new ListItemDto();
					listItemDto.setDisplayName(industry.getIndustryName());
					listItemDto.setListType("Industry");
					listItemDto.setCode(industry.getIndustryName());
					listItemDto.setAllowDelete(true);
					listItemDto.setColorCode("657f8b");
					listItemDto.setIcon("ban");
					listItemService.saveOrUpdateListItem(listItemDto);
				}

			// load Colors into Colors Table
			if (colorService.getAllColors().isEmpty()) {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				URL resource = classLoader.getResource("colors.json");
				File file = new File(resource.toURI());
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(file));
				JSONArray jsonObject = new JSONArray(object.toString());
				for (int i = 1; i < jsonObject.length(); i++) {
					JSONObject normalObject = jsonObject.getJSONObject(i);
					Color color = new Color();
					color.setColor(normalObject.getString("color"));
					color.setColorCode(normalObject.getString("colorCode"));
					colorService.saveOrUpdate(color);
				}

			}

			// load Roles into Role Table
			if (roleService.getRoles().isEmpty()) {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				URL resource = classLoader.getResource("roles.json");
				File file = new File(resource.toURI());
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(file));
				JSONArray jsonObject = new JSONArray(object.toString());
				for (int i = 1; i < jsonObject.length(); i++) {
					JSONObject normalObject = jsonObject.getJSONObject(i);
					Role role = new Role();
					role.setName(normalObject.getString("name"));
					roleService.saveOrUpdate(role);
				}

			}

			// load icons into Icons Table
			if (iconService.getAllIcons().isEmpty()) {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				URL resource = classLoader.getResource("icons.json");
				File file = new File(resource.toURI());
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(file));
				JSONArray jsonObject = new JSONArray(object.toString());
				for (int i = 1; i < jsonObject.length(); i++) {
					JSONObject normalObject = jsonObject.getJSONObject(i);
					Icon icon = new Icon();
					icon.setName(normalObject.getString("name"));
					iconService.saveOrUpdate(icon);
				}

			}

			// load Roles into List Management
			List<ListItemDto> roleListItemDto = listItemService.getListItemByListType("Roles");
			List<RoleDto> roleDtos = roleService.getRoles();
			if (roleListItemDto != null && roleListItemDto.size() == 0)
				for (RoleDto roleDto : roleDtos) {
					ListItemDto listItemDto = new ListItemDto();
					listItemDto.setDisplayName(roleDto.getName());
					listItemDto.setCode(roleDto.getName());
					listItemDto.setListType("Roles");
					listItemDto.setAllowDelete(true);
					listItemDto.setColorCode("657f8b");
					listItemDto.setIcon("ban");
					listItemService.saveOrUpdateListItem(listItemDto);
				}

			// load Industry into List Management
			List<ListItemDto> classificationListItemDto = listItemService.getListItemByListType("Classifications");
			if (classificationListItemDto != null && classificationListItemDto.size() == 0) {
				ListItemDto listItemDto1 = new ListItemDto();
				listItemDto1.setDisplayName("GENERAL");
				listItemDto1.setCode("GENERAL");
				listItemDto1.setListType("Classifications");
				listItemDto1.setAllowDelete(true);
				listItemDto1.setColorCode("657f8b");
				listItemDto1.setIcon("ban");

				ListItemDto listItemDto2 = new ListItemDto();
				listItemDto2.setDisplayName("INDEX");
				listItemDto2.setCode("INDEX");
				listItemDto2.setListType("Classifications");
				listItemDto2.setAllowDelete(true);
				listItemDto2.setColorCode("657f8b");
				listItemDto2.setIcon("ban");

				ListItemDto listItemDto3 = new ListItemDto();
				listItemDto3.setDisplayName("NEWS");
				listItemDto3.setCode("NEWS");
				listItemDto3.setListType("Classifications");
				listItemDto3.setAllowDelete(true);
				listItemDto3.setColorCode("657f8b");
				listItemDto3.setIcon("ban");

				listItemService.saveOrUpdateListItem(listItemDto1);
				listItemService.saveOrUpdateListItem(listItemDto2);
				listItemService.saveOrUpdateListItem(listItemDto3);
			}
			
			 
			
			// load Media into List Management
			List<SourceMediaDto> mediaDtos = sourceMediaSourceService.getSourceMedia();
			List<ListItemDto> mediaListItemDto = listItemService.getListItemByListType("Media");
			if (mediaListItemDto != null && mediaListItemDto.size() == 0)
				for (SourceMediaDto media : mediaDtos) {
					ListItemDto listItemDto = new ListItemDto();
					listItemDto.setDisplayName(media.getMediaName());
					listItemDto.setCode(media.getMediaName());
					listItemDto.setListType("Media");
					listItemDto.setAllowDelete(true);
					listItemDto.setColorCode("657f8b");
					listItemDto.setIcon("ban");
					listItemService.saveOrUpdateListItem(listItemDto);
				}

			// load Categories into List Management
			List<SourceCategoryDto> categoryDtos = sourceCategoryService.getSourceCategories();
			List<ListItemDto> categoryListItemDto = listItemService.getListItemByListType("Categories");
			if (categoryListItemDto != null && categoryListItemDto.size() == 0)
				for (SourceCategoryDto category : categoryDtos) {
					ListItemDto listItemDto = new ListItemDto();
					listItemDto.setDisplayName(category.getCategoryName());
					listItemDto.setCode(category.getCategoryName());
					listItemDto.setListType("Categories");
					listItemDto.setAllowDelete(true);
					listItemDto.setColorCode("657f8b");
					listItemDto.setIcon("ban");
					listItemService.saveOrUpdateListItem(listItemDto);
				}
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			List<ListItemDto> userStatusListNew = listItemService.getListItemByListType("User Status");
			if (userStatusListNew == null || userStatusListNew.size() == 0) {
				
				for(UserStatus status: UserStatus.values()) {
					ListItem userStatus = new ListItem();
					ListItemDto copyDto = new ListItemDto();
					userStatus.setDisplayName(status.toString());
					userStatus.setIcon(status.getIcon());
					userStatus.setColorCode(status.getColor());
					userStatus.setCode(status.toString());
					userStatus.setListType("User Status");
					userStatus.setAllowDelete(false);
					BeanUtils.copyProperties(userStatus, copyDto);
					listItemService.saveOrUpdateListItem(copyDto);
				}
			}
			else {
				for(UserStatus status: UserStatus.values()) {
					boolean found=false;
					for(ListItemDto eachStatus:userStatusListNew) {
						if(eachStatus.getDisplayName().equalsIgnoreCase(status.toString())) {
							found=true;
						}
					}
					if(!found) {
						ListItem userStatus = new ListItem();
						ListItemDto copyDto = new ListItemDto();
						userStatus.setDisplayName(status.toString());
						userStatus.setCode(status.toString());
						userStatus.setColorCode(status.getColor());
						userStatus.setCode(status.toString());
						userStatus.setListType("User Status");
						userStatus.setAllowDelete(false);
						BeanUtils.copyProperties(userStatus, copyDto);
						listItemService.saveOrUpdateListItem(copyDto);
					}
				}
			}
			// adds list management type if it does not exist
			List<Settings> listTypeSettings = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("List Type")).collect(Collectors.toList());
			if (listTypeSettings.isEmpty()) {
				Settings settings16 = new Settings();
				settings16.setName("List Type");
				settings16.setSection("List Management");
				settings16.setType("Dropdown");
				settings16.setDefaultValue("None");
				settings16.setSystemSettingType("LIST_MANAGEMENT");

				List<Attributes> attributesListSettings6 = new ArrayList<Attributes>();
				Attributes attributes6_1 = new Attributes();
				attributes6_1.setAttributeName("Roles");
				attributes6_1.setAttributeValue("Roles");
				attributes6_1.setSettings(settings16);

				Attributes attributes6_2 = new Attributes();
				attributes6_2.setAttributeName("Feed Classification");
				attributes6_2.setAttributeValue("Feed Classification");
				attributes6_2.setSettings(settings16);

				Attributes attributes6_3 = new Attributes();
				attributes6_3.setAttributeName("Feed Source");
				attributes6_3.setAttributeValue("Feed Source");
				attributes6_3.setSettings(settings16);

				Attributes attributes6_4 = new Attributes();
				attributes6_4.setAttributeName("Group Level");
				attributes6_4.setAttributeValue("Group Level");
				attributes6_4.setSettings(settings16);

				Attributes attributes6_5 = new Attributes();
				attributes6_5.setAttributeName("Alert Status");
				attributes6_5.setAttributeValue("Alert Status");
				attributes6_5.setSettings(settings16);

				Attributes attributes6_6 = new Attributes();
				attributes6_6.setAttributeName("Jurisdictions");
				attributes6_6.setAttributeValue("Jurisdictions");
				attributes6_6.setSettings(settings16);

				Attributes attributes6_7 = new Attributes();
				attributes6_7.setAttributeName("Industry");
				attributes6_7.setAttributeValue("Industry");
				attributes6_7.setSettings(settings16);

				Attributes attributes6_8 = new Attributes();
				attributes6_8.setAttributeName("Classifications");
				attributes6_8.setAttributeValue("Classifications");
				attributes6_8.setSettings(settings16);

				Attributes attributes6_9 = new Attributes();
				attributes6_9.setAttributeName("Domains");
				attributes6_9.setAttributeValue("Domains");
				attributes6_9.setSettings(settings16);

				Attributes attributes6_10 = new Attributes();
				attributes6_10.setAttributeName("Media");
				attributes6_10.setAttributeValue("Media");
				attributes6_10.setSettings(settings16);
				
				Attributes attributes6_11 = new Attributes();
				attributes6_11.setAttributeName("Languages");
				attributes6_11.setAttributeValue("Languages");
				attributes6_11.setSettings(settings16);
				
				attributesListSettings6.add(attributes6_1);
				attributesListSettings6.add(attributes6_2);
				attributesListSettings6.add(attributes6_3);
				attributesListSettings6.add(attributes6_4);
				attributesListSettings6.add(attributes6_5);
				attributesListSettings6.add(attributes6_6);
				attributesListSettings6.add(attributes6_7);
				attributesListSettings6.add(attributes6_8);
				attributesListSettings6.add(attributes6_9);
				attributesListSettings6.add(attributes6_10);
				attributesListSettings6.add(attributes6_11);
				settings16.setAttributesList(attributesListSettings6);	
				settingsService.save(settings16);
			}
			
			List<Settings> allSettings=settingsService.findAllWithAttributes();
			if(allSettings!=null && allSettings.size()>0) {
				
				List<Settings> listSettings=allSettings.stream().filter(o -> o.getSystemSettingType().equalsIgnoreCase("LIST_MANAGEMENT")).collect(Collectors.toList());
				if(listSettings!=null & listSettings.size()>0) {
					//listSettings.stream().forEach(o -> Hibernate.initialize(o.getAttributesList()));
					Settings listSetting= listSettings.get(0);
					if(listSetting!=null) {
						List<Attributes> listAttributes= listSetting.getAttributesList().stream().filter(j -> j.getAttributeName().equalsIgnoreCase("User Status")).collect(Collectors.toList());
						if(listAttributes==null || listAttributes.size()==0) {
							listAttributes= new ArrayList<>();
							Attributes userStatusAttribute= new Attributes();
							userStatusAttribute.setAttributeName("User Status");
							userStatusAttribute.setAttributeValue("User Status");
							userStatusAttribute.setSettings(listSettings.get(0));
							listAttributes.add(userStatusAttribute);
							listSettings.get(0).setAttributesList(listAttributes);
							settingsService.saveOrUpdate(listSettings.get(0));
						}
					}
				}
			}
			if(allSettings!=null && allSettings.size()>0) {
				
				List<Settings> listSettings=allSettings.stream().filter(o -> o.getSystemSettingType().equalsIgnoreCase("LIST_MANAGEMENT")).collect(Collectors.toList());
				if(listSettings!=null & listSettings.size()>0) {
					//listSettings.stream().forEach(o -> Hibernate.initialize(o.getAttributesList()));
					Settings listSetting= listSettings.get(0);
					if(listSetting!=null) {
						List<Attributes> listAttributes= listSetting.getAttributesList().stream().filter(j -> j.getAttributeName().equalsIgnoreCase(ElementConstants.ALERT_REVIEW_STATUS_TYPE)).collect(Collectors.toList());
						if(listAttributes==null || listAttributes.size()==0) {
							listAttributes= new ArrayList<>();
							Attributes userStatusAttribute= new Attributes();
							userStatusAttribute.setAttributeName(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
							userStatusAttribute.setAttributeValue(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
							userStatusAttribute.setSettings(listSettings.get(0));
							listAttributes.add(userStatusAttribute);
							listSettings.get(0).setAttributesList(listAttributes);
							settingsService.saveOrUpdate(listSettings.get(0));
						}
					}
				}
			}	
			//load Alert Status into List Management
			List<ListItemDto> alertReviewStatusListItemDto =listItemService.getListItemByListType(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
			if(alertReviewStatusListItemDto!=null && alertReviewStatusListItemDto.size()==0) {
					ListItemDto listItemDto1 = new ListItemDto();
					listItemDto1.setDisplayName("Unconfirmed");
					listItemDto1.setCode("Unconfirmed");
					listItemDto1.setListType(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
					listItemDto1.setAllowDelete(false);
					listItemDto1.setColorCode("657f8b");
					listItemDto1.setIcon("ban");
					
					ListItemDto listItemDto2 = new ListItemDto();
					listItemDto2.setDisplayName("Confirmed");
					listItemDto2.setCode("Confirmed");
					listItemDto2.setListType(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
					listItemDto2.setAllowDelete(false);
					listItemDto2.setColorCode("657f8b");
					listItemDto2.setIcon("ban");
					
					ListItemDto listItemDto3 = new ListItemDto();
					listItemDto3.setDisplayName("Declined");
					listItemDto3.setCode("Declined");
					listItemDto3.setListType(ElementConstants.ALERT_REVIEW_STATUS_TYPE);
					listItemDto3.setAllowDelete(false);
					listItemDto3.setColorCode("657f8b");
					listItemDto3.setIcon("ban");
					
					listItemService.saveOrUpdateListItem(listItemDto1);
					listItemService.saveOrUpdateListItem(listItemDto2);
					listItemService.saveOrUpdateListItem(listItemDto3);
				}
			//add alert management module
			if (allSettings != null && allSettings.size() > 0) {
				List<Settings> listSettings = allSettings.stream()
						.filter(o -> o.getName().equalsIgnoreCase("Default Module")).collect(Collectors.toList());
				if (listSettings != null & listSettings.size() > 0) {
					Settings listSetting = listSettings.get(0);
					if (listSetting != null) {
						List<Attributes> listAttributes = listSetting.getAttributesList().stream()
								.filter(j -> j.getAttributeName().equalsIgnoreCase("Alert Management"))
								.collect(Collectors.toList());
						if (listAttributes == null || listAttributes.size() == 0) {
							listAttributes = new ArrayList<>();
							Attributes alertManagementAttribute = new Attributes();
							alertManagementAttribute.setAttributeName("Alert Management");
							alertManagementAttribute
									.setAttributeValue("/element-new/dist/new/#/element/alert-management/alertsList");
							alertManagementAttribute.setSettings(listSettings.get(0));
							listAttributes.add(alertManagementAttribute);
							listSettings.get(0).setAttributesList(listAttributes);
							settingsService.saveOrUpdate(listSettings.get(0));
						}
					}
				}
			}
			// adds source monitoring module
			if (allSettings != null && allSettings.size() > 0) {
				List<Settings> listSettings = allSettings.stream()
						.filter(o -> o.getName().equalsIgnoreCase("Default Module")).collect(Collectors.toList());
				if (listSettings != null & listSettings.size() > 0) {
					Settings listSetting = listSettings.get(0);
					if (listSetting != null) {
						List<Attributes> listAttributes = listSetting.getAttributesList().stream()
								.filter(j -> j.getAttributeName().equalsIgnoreCase("Source Monitoring"))
								.collect(Collectors.toList());
						if (listAttributes == null || listAttributes.size() == 0) {
							listAttributes = new ArrayList<>();
							Attributes sourceMonitoringAttribute = new Attributes();
							sourceMonitoringAttribute.setAttributeName("Source Monitoring");
							sourceMonitoringAttribute
									.setAttributeValue("/element-new/dist/new/#/element/system-monitoring/sources");
							sourceMonitoringAttribute.setSettings(listSettings.get(0));
							listAttributes.add(sourceMonitoringAttribute);
							listSettings.get(0).setAttributesList(listAttributes);
							settingsService.saveOrUpdate(listSettings.get(0));
						}
					}
				}
			}
			if (usersService.findAll().size() == 0) {
				// admin = new User();
				ListItemDto activeStatus = new ListItemDto();
				ListItemDto deactiveStatus = new ListItemDto();
				ListItemDto countryDto = new ListItemDto();
				List<ListItemDto> countryDtos = listItemService.getListItemByListType("Jurisdictions");
				if (countryDtos.size() > 0)
					countryDto = countryDtos.get(0);
				BeanUtils.copyProperties(countryDto, countryItem);
				
				/*List<ListItemDto> userStatusList = listItemService.getListItemByListType("User Status");
				if (userStatusList == null || userStatusList.size() == 0) {
					
					List<Settings> allSettings=settingsService.findAll();
					if(allSettings!=null && allSettings.size()>0) {
						
						List<Settings> listSettings=allSettings.stream().filter(o -> o.getSystemSettingType().equalsIgnoreCase("LIST_MANAGEMENT")).collect(Collectors.toList());
						if(listSettings!=null & listSettings.size()>0) {
							Settings listSetting= listSettings.get(0);
							if(listSetting!=null) {
								List<Attributes> listAttributes= listSetting.getAttributesList().stream().filter(j -> j.getAttributeName().equalsIgnoreCase("User Status")).collect(Collectors.toList());
								if(listAttributes==null || listAttributes.size()==0) {
									Attributes userStatusAttribute= new Attributes();
									userStatusAttribute.setAttributeName("User Status");
									userStatusAttribute.setAttributeValue("User Status");
									userStatusAttribute.setSettings(listSettings.get(0));
								}
							}
						}
					}
					
					activeStatus.setDisplayName("Active");
					activeStatus.setCode("Active");
					activeStatus.setListType("User Status");
					activeStatus.setAllowDelete(true);
					listItemService.saveOrUpdateListItem(activeStatus);
					BeanUtils.copyProperties(activeStatus, activeItem);
					
					
					deactiveStatus.setDisplayName("InActive");
					deactiveStatus.setCode("InActive");
					deactiveStatus.setListType("User Status");
					deactiveStatus.setAllowDelete(true);
					listItemService.saveOrUpdateListItem(deactiveStatus);
					BeanUtils.copyProperties(deactiveStatus, deactiveItem);
				}*/
/*				dummyStatus.setAllowDelete(true);
				dummyStatus.setCode("Active");
				dummyStatus.setDisplayName("Active");
				dummyStatus.setCode("DE");
				dummyStatus.setListType("User Status");
				listItemService.saveOrUpdate(dummyStatus);*/

				List<ListItem> allListItems=listItemService.findAll();
				List<ListItem> allListItemsAnother=new ArrayList<>();
				allListItemsAnother.addAll(allListItems);
				admin = new Users();
				if(allListItemsAnother!=null && allListItemsAnother.size()>0) {
					List<ListItem> filteredCountryItems=allListItemsAnother.stream().filter(a -> a.getListType().equalsIgnoreCase("Jurisdictions")).filter(a -> a.getDisplayName().equalsIgnoreCase("Israel")).collect(Collectors.toList());
					if(filteredCountryItems!=null && filteredCountryItems.size()>0) {
						israelCountryItem=filteredCountryItems.get(0);
						if(israelCountryItem!=null)
						admin.setCountryId(israelCountryItem);
					}
				}
				
				admin.setCreatedDate(new Date());
				admin.setDob(new Date());
				admin.setEmailAddress("admin@ehub.com");
				admin.setEmailAddressVerified(1);
				admin.setFirstName("Admin");
				admin.setLastName("User");
				admin.setPassword(passwordEncoder.encode("bst@56LKX#"));
				admin.setScreenName("admin");
				admin.setIsModifiable(false);
				if (allListItems != null && allListItems.size() > 0) {
					List<ListItem> filteredItems = allListItems.stream()
							.filter(a -> a.getCode().equalsIgnoreCase("active")).collect(Collectors.toList());
					if (filteredItems != null && filteredItems.size() > 0) {
						admin.setStatusId(filteredItems.get(0));
					}
				}

				admin = usersService.save(admin);

				// analyst = new User();
				analyst = new Users();
				if(israelCountryItem!=null)
				analyst.setCountryId(israelCountryItem);
				else {
					if(allListItemsAnother!=null && allListItemsAnother.size()>0) {
						List<ListItem> filteredCountryItems=allListItemsAnother.stream().filter(a -> a.getListType().equalsIgnoreCase("Jurisdictions")).filter(a -> a.getDisplayName().equalsIgnoreCase("Israel")).collect(Collectors.toList());
						if(filteredCountryItems!=null && filteredCountryItems.size()>0) {
							israelCountryItem=filteredCountryItems.get(0);
							if(israelCountryItem!=null)
								analyst.setCountryId(israelCountryItem);
						}
					}
				}
				analyst.setCreatedDate(new Date());
				analyst.setDob(new Date());
				analyst.setEmailAddress("analyst@ehub.com");
				analyst.setEmailAddressVerified(1);
				analyst.setFirstName("Analyst");
				analyst.setLastName("User");
				analyst.setPassword(passwordEncoder.encode("Xdrt#1765"));
				analyst.setScreenName("analyst");
				analyst.setIsModifiable(false);
				if (allListItems != null && allListItems.size() > 0) {
					List<ListItem> filteredItems = allListItems.stream()
							.filter(a -> a.getCode().equalsIgnoreCase("active")).collect(Collectors.toList());
					if (filteredItems != null && filteredItems.size() > 0) {
						analyst.setStatusId(filteredItems.get(0));
					}
				}
				analyst = usersService.save(analyst);
			}
			List<ListItem> allListItemsAnother=listItemService.findAll();
			try{
				if(allListItemsAnother!=null && allListItemsAnother.size()>0) {
					List<ListItem> filteredCountryItems=allListItemsAnother.stream()
							.filter(j -> null != j.getListType())
							.filter(a -> a.getListType().equalsIgnoreCase("Jurisdictions"))
							.filter(jd -> null != jd.getDisplayName())
							.filter(a -> a.getDisplayName().equalsIgnoreCase("Israel"))
							.collect(Collectors.toList());
					if(filteredCountryItems!=null && filteredCountryItems.size()>0) {
						israelCountryItem=filteredCountryItems.get(0);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//adds default roles to the system
			if(admin==null) {
				UsersDto adminDto=usersService.getUserByEmailOrUsername("admin");
				if(adminDto!=null && adminDto.getUserId()!=null)
				admin=usersService.find(adminDto.getUserId());
			}
			if(analyst==null) {
				UsersDto analystDto=usersService.getUserByEmailOrUsername("analyst");
				if(analystDto!=null && analystDto.getUserId()!=null)
					analyst=usersService.find(analystDto.getUserId());
			}
			if (rolesService.findAll().size() == 0 && admin!=null && analyst!=null) {
				 adminRole = new Roles();
				adminRole.setCreatedOn(new Date());
				adminRole.setDescription("Can control the system configuration");
				adminRole.setNotes("Administrator");
				adminRole.setRoleName(ElementConstants.ROLE_NAME_ADMIN);
				adminRole.setSource(UserCreationSource.INTERNAL.toString());
				adminRole.setIsModifiable(false);
				adminRole = rolesService.save(adminRole);

				analystRole = new Roles();
				analystRole.setCreatedOn(new Date());
				analystRole.setDescription("Default analyst role.");
				analystRole.setNotes("Analyst");
				analystRole.setRoleName(ElementConstants.ROLE_NAME_ANALYST);
				analystRole.setSource(UserCreationSource.INTERNAL.toString());
				analystRole.setIsModifiable(false);
				analystRole = rolesService.save(analystRole);
				
				userRole = new Roles();
				userRole.setCreatedOn(new Date());
				userRole.setDescription("Default user role.");
				userRole.setNotes("User");
				userRole.setRoleName(ElementConstants.ROLE_NAME_USER);
				userRole.setSource(UserCreationSource.INTERNAL.toString());
				userRole.setIsModifiable(false);
				userRole = rolesService.save(userRole);
				
				userRolesService.save(new UserRoles(adminRole, admin));
				userRolesService.save(new UserRoles(analystRole, analyst));

			}
			//if (groupsService.findAll().size() == 0) {
				Groups adminGroupDefault = groupsService.getGroup("Admin");
				if(adminGroupDefault==null) {
				adminGroup = new Groups();
				adminGroup.setActive(true);
				adminGroup.setCreatedDate(new Date());
				adminGroup.setDescription("Default admin group.");
				adminGroup.setName("Admin");
				adminGroup.setRemarks("This is a system generated group.");
				//adminGroup.setIsModifiable(false);
				if (admin != null)
					adminGroup.setCreatedBy(admin.getUserId());
				else {
					admin = usersService.getUserObjectByEmailOrUsername("admin");
					if (admin != null && admin.getUserId() != null)
						adminGroup.setCreatedBy(admin.getUserId());
				}
				adminGroup = groupsService.save(adminGroup);
				}
				Groups analystGroupDefault = groupsService.getGroup("Analyst");
				if(analystGroupDefault==null) {
			    analystGroup = new Groups();
				analystGroup.setActive(true);
				analystGroup.setCreatedDate(new Date());
				analystGroup.setDescription("Default analyst group.");
				analystGroup.setName("Analyst");
				analystGroup.setRemarks("This is a system generated group.");
				if (admin != null)
					analystGroup.setCreatedBy(admin.getUserId());
				else {
					admin = usersService.getUserObjectByEmailOrUsername("admin");
					if (admin != null && admin.getUserId() != null)
						analystGroup.setCreatedBy(admin.getUserId());
				}
				analystGroup = groupsService.save(analystGroup);
			}
				if(admin==null)
					admin = usersService.getUserObjectByEmailOrUsername("Admin");
				if(analyst==null)
					analyst = usersService.getUserObjectByEmailOrUsername("Analyst");
				if(adminGroup!=null && admin!=null)
				userGroupsService.saveOrUpdate(new UserGroups(adminGroup, admin));
				if(analystGroup!=null && analyst!=null)
				userGroupsService.saveOrUpdate(new UserGroups(analystGroup, analyst));
				
			Groups dataEntry = groupsService.getGroup("DataEntry");
			if (dataEntry == null) {
				Users dataEntryClerk = new Users();
				if(israelCountryItem!=null)
				dataEntryClerk.setCountryId(israelCountryItem);
				else {
					List<ListItem> allListItemsAnotherNew=listItemService.findAll();
					if(allListItemsAnotherNew!=null && allListItemsAnotherNew.size()>0) {
						List<ListItem> filteredCountryItems=allListItemsAnotherNew.stream().filter(a -> a.getListType().equalsIgnoreCase("Jurisdictions")).filter(a -> a.getDisplayName().equalsIgnoreCase("Israel")).collect(Collectors.toList());
						if(filteredCountryItems!=null && filteredCountryItems.size()>0) {
							israelCountryItem=filteredCountryItems.get(0);
							if(israelCountryItem!=null)
								dataEntryClerk.setCountryId(israelCountryItem);
						}
					}
				}
				dataEntryClerk.setCreatedDate(new Date());
				dataEntryClerk.setDob(new Date());
				dataEntryClerk.setEmailAddress("dataentry@ehub.com");
				dataEntryClerk.setEmailAddressVerified(1);
				dataEntryClerk.setFirstName("DataEntry");
				dataEntryClerk.setLastName("User");
				dataEntryClerk.setPassword(passwordEncoder.encode("dataentry#123"));
				dataEntryClerk.setScreenName("dataEntry");
				dataEntryClerk.setIsModifiable(false);
				if(activeItem!=null && activeItem.getListItemId()!=null)
				dataEntryClerk.setStatusId(activeItem);
				else {
					List<ListItem> allListItems=listItemService.findAll();
					if (allListItems != null && allListItems.size() > 0) {
						List<ListItem> filteredItems = allListItems.stream()
								.filter(a -> a.getCode().equalsIgnoreCase("active")).collect(Collectors.toList());
						if (filteredItems != null && filteredItems.size() > 0) {
							dataEntryClerk.setStatusId(filteredItems.get(0));
						}
					}
				}
					
				dataEntryClerk = usersService.save(dataEntryClerk);

				Groups dataEntryGroup = new Groups();
				dataEntryGroup.setActive(true);
				dataEntryGroup.setCreatedDate(new Date());
				dataEntryGroup.setDescription("Default DataEntry Group group.");
				dataEntryGroup.setName("DataEntry");
				dataEntryGroup.setRemarks("This is a system generated group.");
				//dataEntryGroup.setIsModifiable(false);
				if (admin != null)
					dataEntryGroup.setCreatedBy(admin.getUserId());
				else {
					admin = usersService.getUserObjectByEmailOrUsername("admin");
					if (admin != null && admin.getUserId() != null)
						dataEntryGroup.setCreatedBy(admin.getUserId());
				}
				dataEntryGroup = groupsService.save(dataEntryGroup);

				userGroupsService.save(new UserGroups(dataEntryGroup, dataEntryClerk));

			}
			
			if(adminGroup==null)
			 adminGroup=groupsService.getGroup("Admin");
			if(adminGroup!=null) {
				List<GroupAlert> existingSetting=groupAlertService.findGroupAlertsByGroup(adminGroup.getId());
				if(existingSetting!=null)
					 groupAlertService.loadZeroGroupAlertSettings(adminGroup.getId());
			}
			if(analystGroup==null)
			 analystGroup=groupsService.getGroup("Analyst");
			if(analystGroup!=null) {
				List<GroupAlert> existingSetting=groupAlertService.findGroupAlertsByGroup(analystGroup.getId());
				if(existingSetting!=null)
					 groupAlertService.loadZeroGroupAlertSettings(analystGroup.getId());
			}
			Groups dataEntryGroup=groupsService.getGroup("DataEntry");
			if(dataEntryGroup!=null) {
				List<GroupAlert> existingSetting=groupAlertService.findGroupAlertsByGroup(dataEntryGroup.getId());
				if(existingSetting!=null)
					 groupAlertService.loadZeroGroupAlertSettings(dataEntryGroup.getId());
			}
			// load Languages into List Management 
						List<ListItemDto> LanguageListItemDto  = listItemService.getListItemByListType("Languages"); 
						if (LanguageListItemDto != null && LanguageListItemDto.size() == 0) {
							  ListItemDto listItemDto1 = new ListItemDto();
					 		  listItemDto1.setDisplayName("English");
							  listItemDto1.setCode("English"); 
							  listItemDto1.setListType("Languages");
							  listItemDto1.setAllowDelete(false);
							  listItemDto1.setColorCode("657f8b");
							  listItemDto1.setIcon("ban");
							  ClassLoader classLoader = getClass().getClassLoader();
								File file = new File(classLoader.getResource("en.json").getFile());
								UsersDto user=usersService.getUserByEmailOrUsername("admin");
								if(user!=null) {
									MultipartFile multipartFile =null;
								FileItem fileItem = new DiskFileItem(file.getName(), Files.probeContentType(file.toPath()), false, file.getName(), (int) file.length(), file.getParentFile());
								InputStream input = new FileInputStream(file);
								OutputStream os = fileItem.getOutputStream();
								IOUtils.copy(input, os);
								multipartFile = new CommonsMultipartFile(fileItem);
								boolean status=fileService.uploadFile(multipartFile, user.getUserId(), "English");
								FileBst fileUploaded=fileService.findFileByName("English");
								listItemDto1.setFile_id(fileUploaded.getId());
								}
							 /* ListItemDto listItemDto2 = new ListItemDto();
							  listItemDto2.setDisplayName("German");
							  listItemDto2.setCode("German");
							  listItemDto2.setListType("Languages");
							  listItemDto2.setAllowDelete(true);
							  listItemDto2.setColorCode("657f8b");
							  listItemDto2.setIcon("ban");*/
							  
							  listItemService.saveOrUpdateListItem(listItemDto1);
							 // listItemService.saveOrUpdateListItem(listItemDto2); 
							  }
						ListItemDto LanguageListItemDtos  =listItemService.getListItemByListTypeAndDisplayName("Languages", "German");

						if(LanguageListItemDtos==null) {

							ListItemDto listItemDto2 = new ListItemDto();
							listItemDto2.setDisplayName("German");
							listItemDto2.setCode("German");
							listItemDto2.setListType("Languages");
							listItemDto2.setAllowDelete(true);
							listItemDto2.setColorCode("657f8b");
							listItemDto2.setIcon("ban");

							ClassLoader classLoader = getClass().getClassLoader();
							File file = new File(classLoader.getResource("ge.json").getFile());
							UsersDto user=usersService.getUserByEmailOrUsername("admin");
							if(user!=null) {
								MultipartFile multipartFile =null;
								FileItem fileItem = new DiskFileItem(file.getName(), Files.probeContentType(file.toPath()), false, file.getName(), (int) file.length(), file.getParentFile());
								InputStream input = new FileInputStream(file);
								OutputStream os = fileItem.getOutputStream();
								IOUtils.copy(input, os);
								multipartFile = new CommonsMultipartFile(fileItem);
								boolean status=fileService.uploadFile(multipartFile, user.getUserId(), "German");
								FileBst fileUploaded=fileService.findFileByName("German");
								listItemDto2.setFile_id(fileUploaded.getId());
								listItemService.saveOrUpdateListItem(listItemDto2); 
							}
						}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Source Credibility adding
			if (classificationsService.findAll().isEmpty()) {
				sourcesService.saveMasterDataClassifications();
			}

			// adds 'Key Management' Subclassification
			List<Classifications> classifications = classificationsService.findAllWithSubclassifications();
			if (classifications.stream()
					.filter(subClassifications -> subClassifications.getSubClassifications().stream()
							.anyMatch(sc -> "Key Management".equals(sc.getSubClassifcationName())))
					.collect(Collectors.toList()).size() == 0) {
				for (Classifications classification : classifications) {
					if (classification.getClassifcationName().equals("GENERAL")) {
						List<SubClassifications> subClassificationsList = classification.getSubClassifications();
						SubClassifications subClassificationsOne = new SubClassifications();
						subClassificationsOne.setClassifications(classification);
						subClassificationsOne.setSubClassifcationName("Key Management");
						subClassificationsList.add(subClassificationsOne);
						classificationsService.update(classification);
					}
				}

			}

			// Add default credibility for existing sources for the key management sub
			// classification.
			List<Sources> sources = sourcesService.fetchClassifctaionSourcesWithCredibility("GENERAL");
			for (Classifications classification : classifications) {
				if (classification.getClassifcationName().equalsIgnoreCase("GENERAL")) {
					List<SubClassifications> subClassificationsList = classification.getSubClassifications();
					for (SubClassifications sub : subClassificationsList) {
						Long subCalssificationId = sub.getSubClassificationId();
						if (sub.getSubClassifcationName().equalsIgnoreCase("Key Management")) {
							for (Sources source : sources) {
								// List<SourceCredibility> sourceCredibilityList =
								// source.getSourceCredibilityList();
								Set<SubClassifications> credSet = source.getSourceCredibilityList().stream()
										.map(sourc -> sourc.getSubClassifications()).collect(Collectors.toSet());

								SourceCredibility credibiliti = new SourceCredibility();
								boolean flag = false;
								for (SubClassifications s : credSet) {
									if ((s.getSubClassificationId()).equals(subCalssificationId)) {
										flag = true;
									}
								}
								if (flag == false) {
									credibiliti.setCredibility(CredibilityEnums.NONE);
									credibiliti.setSources(source);
									credibiliti.setSubClassifications(sub);
									sourceCredibilityService.save(credibiliti);
								}
							}
						}
					}

				}
			}

			// adding new Attributes
			SubClassifications subClassification = subClassificationsService
					.fetchSubClassification("Company Information");
			if (subClassification != null && (subClassification.getDataAttributes() != null)) {
				Map<String, String> keysToAdd = new LinkedHashMap<String, String>();
				keysToAdd.put("main_exchange", "stock exchange");
				keysToAdd.put("hasActivityStatus", "Company Status");
				keysToAdd.put("bst:aka", "Alias Name");
				keysToAdd.put("fullAddress", "Registered Address");
				keysToAdd.put("zip", "Postal Code");
				keysToAdd.put("country", "Country");
				keysToAdd.put("lei:legalForm", "Legal Type");
				keysToAdd.put("isIncorporatedIn", "Date of Incorporation");
				keysToAdd.put("tr-org:hasRegisteredPhoneNumber", "Registered Phone Number");
				keysToAdd.put("hasRegisteredFaxNumber", "Registered Fax Number");
				keysToAdd.put("tr-org:hasHeadquartersPhoneNumber", "Headquater Phone Number");
				keysToAdd.put("tr-org:hasHeadquartersFaxNumber", "Headquarters Fax Number");
				keysToAdd.put("vat_tax_number", "VAT/TIN");
				keysToAdd.put("trade_register_number", "Trade/Commerce");
				keysToAdd.put("ticket_symbol", "Stock Ticker");
				keysToAdd.put("giin", "Global Intermediary Identification Number");
				keysToAdd.put("cik_number", "Central Index Key");
				keysToAdd.put("fdic_certificate_number", "FDIC Certificate number");
				keysToAdd.put("rssd_id", "RSSD ID");
				keysToAdd.put("swift_code", "BIC / Swift Codes");
				keysToAdd.put("international_securities_identifier", "International Securities Identification Number");
				List<DataAttributes> attributes = subClassification.getDataAttributes();
				Map<String, String> keysFromDB = attributes.stream().collect(Collectors
						.toMap(DataAttributes::getSourceAttributeSchema, DataAttributes::getSourceAttributeName));
				List<DataAttributes> attributesList = new ArrayList<DataAttributes>();
				for (Entry<String, String> map : keysToAdd.entrySet()) {
					if (keysFromDB.containsKey(map.getKey())) {
						// do nothing
					} else {
						DataAttributes attributesAdd = new DataAttributes();
						attributesAdd.setSourceAttributeName(map.getValue());
						attributesAdd.setSourceAttributeSchema(map.getKey());
						attributesAdd.setSourceAttributeSchemaGroup("Overview");
						attributesAdd.setSubClassifications(subClassification);
						attributesList.add(attributesAdd);
					}
				}
				for (DataAttributes dataAttributesSave : attributesList) {
					dataAttributesService.save(dataAttributesSave);
				}
			}

			if (complianceWidgetService.findAll().isEmpty()) {
				ComplianceWidget comapnyInformation = new ComplianceWidget("COMPANY INFORMATION");
				complianceWidgetService.save(comapnyInformation);
				ComplianceWidget comapnyDetails = new ComplianceWidget("COMPANY DETAILS");
				complianceWidgetService.save(comapnyDetails);
				ComplianceWidget screeningResults = new ComplianceWidget("SCREENING RESULTS");
				complianceWidgetService.save(screeningResults);
				ComplianceWidget ownershipStructure = new ComplianceWidget("OWNERSHIP STRUCTURE");
				complianceWidgetService.save(ownershipStructure);
				ComplianceWidget countriesOfOperation = new ComplianceWidget("COUNTRIES OF OPERATION");
				complianceWidgetService.save(countriesOfOperation);
				ComplianceWidget associatedDocuments = new ComplianceWidget("ASSOCIATED DOCUMENTS");
				complianceWidgetService.save(associatedDocuments);
				ComplianceWidget conpanyIdentifier = new ComplianceWidget("COMPANY IDENTIFIERS");
				complianceWidgetService.save(conpanyIdentifier);
			}

			if (entityTypeService.findAll().isEmpty()) {
				EntityType standard = new EntityType("Standard");
				List<EntityRequirements> requirementsStandard = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirements = new EntityRequirements();
				registryRequirements.setEntityRequirements("Registry");
				registryRequirements.setType(standard);
				requirementsStandard.add(registryRequirements);
				EntityRequirements reportRequirements = new EntityRequirements();
				reportRequirements.setEntityRequirements("Annual Report");
				reportRequirements.setType(standard);
				requirementsStandard.add(reportRequirements);
				EntityRequirements regulatorRequirements = new EntityRequirements();
				regulatorRequirements.setEntityRequirements("Regulator");
				regulatorRequirements.setType(standard);
				requirementsStandard.add(regulatorRequirements);
				EntityRequirements otherRequirements = new EntityRequirements();
				otherRequirements.setEntityRequirements("Other");
				otherRequirements.setType(standard);
				requirementsStandard.add(otherRequirements);
				EntityRequirements nomineeRequirements = new EntityRequirements();
				nomineeRequirements.setEntityRequirements("Nominee");
				nomineeRequirements.setType(standard);
				requirementsStandard.add(nomineeRequirements);
				standard.setRequirementsList(requirementsStandard);
				entityTypeService.save(standard);

				EntityType FI = new EntityType("Financial Institution");
				List<EntityRequirements> requirementsFI = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsFI = new EntityRequirements();
				registryRequirementsFI.setEntityRequirements("Registry");
				registryRequirementsFI.setType(FI);
				requirementsFI.add(registryRequirementsFI);
				EntityRequirements reportRequirementsFI = new EntityRequirements();
				reportRequirementsFI.setEntityRequirements("Annual Report");
				reportRequirementsFI.setType(FI);
				requirementsFI.add(reportRequirementsFI);
				EntityRequirements regulatorRequirementsFI = new EntityRequirements();
				regulatorRequirementsFI.setEntityRequirements("Regulator");
				regulatorRequirementsFI.setType(FI);
				requirementsFI.add(regulatorRequirementsFI);
				EntityRequirements otherRequirementsFI = new EntityRequirements();
				otherRequirementsFI.setEntityRequirements("Other");
				otherRequirementsFI.setType(FI);
				requirementsFI.add(otherRequirementsFI);
				EntityRequirements nomineeRequirementsFI = new EntityRequirements();
				nomineeRequirementsFI.setEntityRequirements("Nominee");
				nomineeRequirementsFI.setType(standard);
				requirementsFI.add(nomineeRequirementsFI);
				FI.setRequirementsList(requirementsFI);
				entityTypeService.save(FI);

				EntityType trust = new EntityType("Trust");
				List<EntityRequirements> requirementsTrust = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsTrust = new EntityRequirements();
				registryRequirementsTrust.setEntityRequirements("Registry");
				registryRequirementsTrust.setType(trust);
				requirementsTrust.add(registryRequirementsTrust);
				EntityRequirements reportRequirementsTrust = new EntityRequirements();
				reportRequirementsTrust.setEntityRequirements("Annual Report");
				reportRequirementsTrust.setType(trust);
				requirementsTrust.add(reportRequirementsTrust);
				EntityRequirements regulatorRequirementsTrust = new EntityRequirements();
				regulatorRequirementsTrust.setEntityRequirements("Regulator");
				regulatorRequirementsTrust.setType(trust);
				requirementsTrust.add(regulatorRequirementsTrust);
				EntityRequirements otherRequirementsTrust = new EntityRequirements();
				otherRequirementsTrust.setEntityRequirements("Other");
				otherRequirementsTrust.setType(trust);
				requirementsTrust.add(otherRequirementsTrust);
				EntityRequirements trustDeedRequirementsTrust = new EntityRequirements();
				trustDeedRequirementsTrust.setEntityRequirements("Trust Deed");
				trustDeedRequirementsTrust.setType(trust);
				requirementsTrust.add(trustDeedRequirementsTrust);
				trust.setRequirementsList(requirementsTrust);
				entityTypeService.save(trust);

				EntityType fund = new EntityType("Fund");
				List<EntityRequirements> requirementsFund = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsFund = new EntityRequirements();
				registryRequirementsFund.setEntityRequirements("Registry");
				registryRequirementsFund.setType(fund);
				requirementsTrust.add(registryRequirementsFund);
				EntityRequirements reportRequirementsFund = new EntityRequirements();
				reportRequirementsFund.setEntityRequirements("Annual Report");
				reportRequirementsFund.setType(fund);
				requirementsTrust.add(reportRequirementsFund);
				EntityRequirements regulatorRequirementsFund = new EntityRequirements();
				regulatorRequirementsFund.setEntityRequirements("Regulator");
				regulatorRequirementsFund.setType(fund);
				requirementsTrust.add(regulatorRequirementsFund);
				EntityRequirements otherRequirementsFund = new EntityRequirements();
				otherRequirementsFund.setEntityRequirements("Other");
				otherRequirementsFund.setType(fund);
				requirementsTrust.add(otherRequirementsFund);
				EntityRequirements prospectusRequirementsFund = new EntityRequirements();
				prospectusRequirementsFund.setEntityRequirements("Prospectus");
				prospectusRequirementsFund.setType(fund);
				requirementsTrust.add(prospectusRequirementsFund);
				EntityRequirements AMLRequirementsFund = new EntityRequirements();
				AMLRequirementsFund.setEntityRequirements("AML Letter");
				AMLRequirementsFund.setType(fund);
				requirementsTrust.add(AMLRequirementsFund);
				fund.setRequirementsList(requirementsFund);
				entityTypeService.save(fund);

				EntityType LP = new EntityType("LP");
				List<EntityRequirements> requirementsLP = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsLP = new EntityRequirements();
				registryRequirementsLP.setEntityRequirements("Registry");
				registryRequirementsLP.setType(LP);
				requirementsLP.add(registryRequirementsLP);
				EntityRequirements reportRequirementsLP = new EntityRequirements();
				reportRequirementsLP.setEntityRequirements("Annual Report");
				reportRequirementsLP.setType(LP);
				requirementsLP.add(reportRequirementsLP);
				EntityRequirements regulatorRequirementsLP = new EntityRequirements();
				regulatorRequirementsLP.setEntityRequirements("Regulator");
				regulatorRequirementsLP.setType(LP);
				requirementsLP.add(regulatorRequirementsLP);
				EntityRequirements otherRequirementsLP = new EntityRequirements();
				otherRequirementsLP.setEntityRequirements("Other");
				otherRequirementsLP.setType(LP);
				requirementsLP.add(otherRequirementsLP);
				EntityRequirements AgreementDeedRequirementsLP = new EntityRequirements();
				AgreementDeedRequirementsLP.setEntityRequirements("Limited Partnership agreement");
				AgreementDeedRequirementsLP.setType(LP);
				requirementsLP.add(AgreementDeedRequirementsLP);
				LP.setRequirementsList(requirementsLP);
				entityTypeService.save(LP);

				EntityType LLP = new EntityType("LLP");
				List<EntityRequirements> requirementsLLP = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsLLP = new EntityRequirements();
				registryRequirementsLLP.setEntityRequirements("Registry");
				registryRequirementsLLP.setType(LLP);
				requirementsLLP.add(registryRequirementsLLP);
				EntityRequirements reportRequirementsLLP = new EntityRequirements();
				reportRequirementsLLP.setEntityRequirements("Annual Report");
				reportRequirementsLLP.setType(LLP);
				requirementsLLP.add(reportRequirementsLLP);
				EntityRequirements regulatorRequirementsLLP = new EntityRequirements();
				regulatorRequirementsLLP.setEntityRequirements("Regulator");
				regulatorRequirementsLLP.setType(LLP);
				requirementsLLP.add(regulatorRequirementsLLP);
				EntityRequirements otherRequirementsLLP = new EntityRequirements();
				otherRequirementsLLP.setEntityRequirements("Other");
				otherRequirementsLLP.setType(LLP);
				requirementsLLP.add(otherRequirementsLLP);
				EntityRequirements AgreementDeedRequirementsLLP = new EntityRequirements();
				AgreementDeedRequirementsLLP.setEntityRequirements("Limited Partnership agreement");
				AgreementDeedRequirementsLLP.setType(LLP);
				requirementsLLP.add(AgreementDeedRequirementsLLP);
				LLP.setRequirementsList(requirementsLLP);
				entityTypeService.save(LLP);

				EntityType foundation = new EntityType("Foundation");
				List<EntityRequirements> requirementsFoundation = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsFoundation = new EntityRequirements();
				registryRequirementsFoundation.setEntityRequirements("Registry");
				registryRequirementsFoundation.setType(foundation);
				requirementsFoundation.add(registryRequirementsFoundation);
				EntityRequirements reportRequirementsFoundation = new EntityRequirements();
				reportRequirementsFoundation.setEntityRequirements("Annual Report");
				reportRequirementsFoundation.setType(foundation);
				requirementsFoundation.add(reportRequirementsFoundation);
				EntityRequirements regulatorRequirementsFoundation = new EntityRequirements();
				regulatorRequirementsFoundation.setEntityRequirements("Regulator");
				regulatorRequirementsFoundation.setType(foundation);
				requirementsFoundation.add(regulatorRequirementsFoundation);
				EntityRequirements otherRequirementsFoundation = new EntityRequirements();
				otherRequirementsFoundation.setEntityRequirements("Other");
				otherRequirementsFoundation.setType(foundation);
				requirementsFoundation.add(otherRequirementsFoundation);
				EntityRequirements foundationDeedRequirementsFoundation = new EntityRequirements();
				foundationDeedRequirementsFoundation.setEntityRequirements("Foundation deed");
				foundationDeedRequirementsFoundation.setType(foundation);
				requirementsFoundation.add(foundationDeedRequirementsFoundation);
				foundation.setRequirementsList(requirementsFoundation);
				entityTypeService.save(foundation);

				EntityType STAK = new EntityType("STAK");
				List<EntityRequirements> requirementsSTAK = new ArrayList<EntityRequirements>();
				EntityRequirements registryRequirementsSTAK = new EntityRequirements();
				registryRequirementsSTAK.setEntityRequirements("Registry");
				registryRequirementsSTAK.setType(STAK);
				requirementsSTAK.add(registryRequirementsSTAK);
				EntityRequirements reportRequirementsSTAK = new EntityRequirements();
				reportRequirementsSTAK.setEntityRequirements("Annual Report");
				reportRequirementsSTAK.setType(STAK);
				requirementsSTAK.add(reportRequirementsSTAK);
				EntityRequirements regulatorRequirementsSTAK = new EntityRequirements();
				regulatorRequirementsSTAK.setEntityRequirements("Regulator");
				regulatorRequirementsSTAK.setType(STAK);
				requirementsSTAK.add(regulatorRequirementsSTAK);
				EntityRequirements otherRequirementsSTAK = new EntityRequirements();
				otherRequirementsSTAK.setEntityRequirements("Other");
				otherRequirementsSTAK.setType(STAK);
				requirementsSTAK.add(otherRequirementsSTAK);
				EntityRequirements formationRequirementsSTAK = new EntityRequirements();
				formationRequirementsSTAK.setEntityRequirements("Certificate of formation");
				formationRequirementsSTAK.setType(STAK);
				requirementsSTAK.add(formationRequirementsSTAK);
				EntityRequirements incumbencyRequirementsSTAK = new EntityRequirements();
				incumbencyRequirementsSTAK.setEntityRequirements("Certificate of incumbency");
				incumbencyRequirementsSTAK.setType(STAK);
				requirementsSTAK.add(incumbencyRequirementsSTAK);
				STAK.setRequirementsList(requirementsSTAK);
				entityTypeService.save(STAK);

			}

			// Privileges
			/*if (privilegesService.findAll().isEmpty()) {
				// domains permissions
				Permissions riskPermission = new Permissions("d.risk");
				Permissions amlPermission = new Permissions("d.aml");
				Permissions miPermission = new Permissions("d.market_intelligence");
				Permissions fcPermission = new Permissions("d.financial_crime");
				Permissions ccPermission = new Permissions("d.credit_cards");
				Permissions slPermission = new Permissions("d.social_listening");
				Permissions insurancePermission = new Permissions("d.insurance");
				System.out.println("Inside Permissions");
				privilegesService.save(riskPermission);
				privilegesService.save(amlPermission);
				privilegesService.save(miPermission);
				privilegesService.save(fcPermission);
				privilegesService.save(ccPermission);
				privilegesService.save(slPermission);
				privilegesService.save(insurancePermission);

				// Module Groups Permissions
				Permissions enrichPermission = new Permissions("g.enrich");
				Permissions discoverPermission = new Permissions("g.discover");
				Permissions actPermission = new Permissions("g.act");
				Permissions predictPermission = new Permissions("g.predict");
				Permissions managePermission = new Permissions("g.manage");
				privilegesService.save(enrichPermission);
				privilegesService.save(discoverPermission);
				privilegesService.save(actPermission);
				privilegesService.save(predictPermission);
				privilegesService.save(managePermission);

				// modules permissions
				List<Permissions> domainsPermissions = privilegesService.findAll();
				List<Permissions> domainsPermissionsExceptInsurance = new ArrayList<>();
				domainsPermissionsExceptInsurance = domainsPermissions.stream()
						.filter(d -> !d.getItemName().equalsIgnoreCase("d.insurance")).collect(Collectors.toList());

				Permissions dashBoardPermissions = new Permissions("m.dashboard");
				dashBoardPermissions.setPermissions(domainsPermissionsExceptInsurance);
				// dashBoardPermissions.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				dashBoardPermissions.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(dashBoardPermissions);
				Permissions advancedSearchPermission = new Permissions("m.advanced_search");
				advancedSearchPermission.setPermissions(domainsPermissionsExceptInsurance);
				// advancedSearchPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				advancedSearchPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.enrich"));
				privilegesService.save(advancedSearchPermission);
				Permissions onboardingPermission = new Permissions("m.onboarding");
				onboardingPermission.setPermissions(domainsPermissionsExceptInsurance);
				// onboardingPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				onboardingPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.enrich"));
				privilegesService.save(onboardingPermission);
				Permissions WorkspacePermission = new Permissions("m.workspace");
				WorkspacePermission.setPermissions(domainsPermissionsExceptInsurance);
				// WorkspacePermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				WorkspacePermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(WorkspacePermission);
				Permissions investigationconsolePermission = new Permissions("m.investigation_console");
				// investigationconsolePermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				investigationconsolePermission.setPermissions(domainsPermissionsExceptInsurance);
				investigationconsolePermission
						.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(investigationconsolePermission);
				Permissions TransactionMonitoring = new Permissions("m.transaction_monitoring");
				TransactionMonitoring.setPermissions(domainsPermissionsExceptInsurance);
				// TransactionMonitoring.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				TransactionMonitoring.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(TransactionMonitoring);
				Permissions MarketIntelligence = new Permissions("m.market_intelligence");
				MarketIntelligence.setPermissions(domainsPermissionsExceptInsurance);
				// MarketIntelligence.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				MarketIntelligence.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(MarketIntelligence);
				Permissions AdverseTransactionsPermission = new Permissions("m.adverse_transactions");
				AdverseTransactionsPermission.setPermissions(domainsPermissionsExceptInsurance);
				// AdverseTransactionsPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				AdverseTransactionsPermission
						.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(AdverseTransactionsPermission);
				Permissions FraudPermission = new Permissions("m.fraud");
				FraudPermission.setPermissions(domainsPermissionsExceptInsurance);
				// FraudPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				FraudPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(FraudPermission);
				Permissions TransactionIntelligencePermission = new Permissions("m.transaction_intelligence");
				TransactionIntelligencePermission.setPermissions(domainsPermissionsExceptInsurance);
				// TransactionIntelligencePermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				TransactionIntelligencePermission
						.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(TransactionIntelligencePermission);
				Permissions eDiscoveryPermission = new Permissions("m.ediscovery");
				eDiscoveryPermission.setPermissions(domainsPermissionsExceptInsurance);
				// eDiscoveryPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				eDiscoveryPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				privilegesService.save(eDiscoveryPermission);
				Permissions EntityPermission = new Permissions("m.entity");
				EntityPermission.setPermissions(domainsPermissionsExceptInsurance);
				// EntityPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				EntityPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.act"));
				privilegesService.save(EntityPermission);
				Permissions CasesPermission = new Permissions("m.cases");
				CasesPermission.setPermissions(domainsPermissionsExceptInsurance);
				// CasesPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				CasesPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.act"));
				privilegesService.save(CasesPermission);
				Permissions LinkAnalysisPermission = new Permissions("m.link_analysis");
				LinkAnalysisPermission.setPermissions(domainsPermissionsExceptInsurance);
				// LinkAnalysisPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				LinkAnalysisPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.act"));
				privilegesService.save(LinkAnalysisPermission);
				Permissions UnderwritingPermission = new Permissions("m.underwriting");
				// UnderwritingPermission.setDomains(domainsService.findAll());
				UnderwritingPermission.setPermissions(domainsPermissions);
				UnderwritingPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.act"));
				privilegesService.save(UnderwritingPermission);
				Permissions AuditTrailPermission = new Permissions("m.audit_trail");
				// AuditTrailPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				AuditTrailPermission.setPermissions(domainsPermissionsExceptInsurance);
				AuditTrailPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.act"));
				privilegesService.save(AuditTrailPermission);
				Permissions OrchestrationPermission = new Permissions("m.orchestration");
				OrchestrationPermission.setPermissions(domainsPermissionsExceptInsurance);
				// OrchestrationPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				OrchestrationPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(OrchestrationPermission);
				Permissions AppManagerPermission = new Permissions("m.app_manager");
				AppManagerPermission.setPermissions(domainsPermissionsExceptInsurance);
				// AppManagerPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				AppManagerPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(AppManagerPermission);
				Permissions DataManagementPermission = new Permissions("m.data_management");
				DataManagementPermission.setPermissions(domainsPermissionsExceptInsurance);
				// DataManagementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				DataManagementPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(DataManagementPermission);
				Permissions SystemMonitoringPermission = new Permissions("m.system_monitoring");
				SystemMonitoringPermission.setPermissions(domainsPermissionsExceptInsurance);
				// SystemMonitoringPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				SystemMonitoringPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(SystemMonitoringPermission);
				Permissions BigDataSciencePlatformPermission = new Permissions("m.big_data_science_platform");
				BigDataSciencePlatformPermission.setPermissions(domainsPermissionsExceptInsurance);
				// BigDataSciencePlatformPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				BigDataSciencePlatformPermission
						.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(BigDataSciencePlatformPermission);
				Permissions SystemSettingsPermission = new Permissions("m.system_settings");
				SystemSettingsPermission.setPermissions(domainsPermissionsExceptInsurance);
				// SystemSettingsPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				SystemSettingsPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(SystemSettingsPermission);
				Permissions PolicyEnforcementPermission = new Permissions("m.policy_enforcement");
				PolicyEnforcementPermission.setPermissions(domainsPermissionsExceptInsurance);
				// PolicyEnforcementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				PolicyEnforcementPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(PolicyEnforcementPermission);
				Permissions QuestionnaireBuilderPermission = new Permissions("m.questionnaire_builder");
				QuestionnaireBuilderPermission.setPermissions(domainsPermissionsExceptInsurance);
				// QuestionnaireBuilderPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				QuestionnaireBuilderPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(QuestionnaireBuilderPermission);
				Permissions DataCurationPermission = new Permissions("m.data_curation");
				DataCurationPermission.setPermissions(domainsPermissionsExceptInsurance);
				// DataCurationPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				DataCurationPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(DataCurationPermission);
				Permissions DocumentParsingPermission = new Permissions("m.document_parsing");
				DocumentParsingPermission.setPermissions(domainsPermissionsExceptInsurance);
				// DocumentParsingPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				DocumentParsingPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(DocumentParsingPermission);
				Permissions DecisionScoringPermission = new Permissions("m.decision_scoring");
				DecisionScoringPermission.setPermissions(domainsPermissionsExceptInsurance);
				// DecisionScoringPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				DecisionScoringPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(DecisionScoringPermission);
				Permissions SourceManagementPermission = new Permissions("m.source_management");
				// SourceManagementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				SourceManagementPermission.setPermissions(domainsPermissionsExceptInsurance);
				SourceManagementPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				privilegesService.save(SourceManagementPermission);
				Permissions LeadGenerationPermission = new Permissions("m.personalization");
				// LeadGenerationPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				LeadGenerationPermission.setPermissions(domainsPermissionsExceptInsurance);
				LeadGenerationPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.predict"));
				privilegesService.save(LeadGenerationPermission);

			}*/
			admin = usersService.getUserObjectByEmailOrUsername("admin");
			privilegesService.loadDefaultPreviliges("role-permisions.constants.json");
			gridViewService.loadDefaultGridView("gridview.json",admin.getUserId());
			

			// Domains
			// List<Modules> allModules=modulesService.findAll();
			List<Permissions> allPermissions = privilegesService.findAll();
			// List<Modules> allModulesExceptUnderWriting= allModules.stream().filter(m ->
			// !m.getModuleName().equalsIgnoreCase("Underwriting")).collect(Collectors.toList());
			if (domainsService.findAll().isEmpty()) {
				Domains riskDomain = new Domains();
				riskDomain.setDomainName("Risk");
				riskDomain.setDomainTitle("Element of Insurance");
				riskDomain.setDomainBody("Cognitive System for complex underwriting and dynamic risk.");
				// riskDomain.setModules(allModulesExceptUnderWriting);
				riskDomain.setDomainCode("d.risk");
				/*List<Permissions> riskPermissions = new ArrayList<>();
				riskPermissions = allPermissions.stream().filter(p -> p.getItemName().equalsIgnoreCase("d.risk"))
						.collect(Collectors.toList());
				if (riskPermissions.size() > 0)
					riskDomain.setPermissionId(riskPermissions.get(0));*/

				Domains amlDomain = new Domains();
				amlDomain.setDomainName("AML");
				amlDomain.setDomainTitle("Element of Compliance");
				amlDomain.setDomainBody("Cognitive system for all your compliance requirements.");
				// amlDomain.setModules(allModulesExceptUnderWriting);
				amlDomain.setDomainCode("d.aml");
				/*List<Permissions> amlPermissions = new ArrayList<>();
				amlPermissions = allPermissions.stream().filter(p -> p.getItemName().equalsIgnoreCase("d.aml"))
						.collect(Collectors.toList());
				if (amlPermissions.size() > 0)
					amlDomain.setPermissionId(amlPermissions.get(0));*/

				Domains miDomain = new Domains();
				miDomain.setDomainName("Market Intelligence");
				miDomain.setDomainTitle("Element of markets");
				miDomain.setDomainBody(
						"Cognitive system to proactively search, crawl and identify the global hidden market of non public companies.");
				// miDomain.setModules(allModulesExceptUnderWriting);
				miDomain.setDomainCode("d.market_intelligence");
				/*List<Permissions> miPermissions = new ArrayList<>();
				miPermissions = allPermissions.stream()
						.filter(p -> p.getItemName().equalsIgnoreCase("d.market_intelligence"))
						.collect(Collectors.toList());
				if (miPermissions.size() > 0)
					miDomain.setPermissionId(miPermissions.get(0));*/

				Domains fcDomain = new Domains();
				fcDomain.setDomainName("Financial Crime");
				fcDomain.setDomainTitle("Element of Investigation");
				fcDomain.setDomainBody("Cognitive system for deep and complex investigations.");
				// fcDomain.setModules(allModulesExceptUnderWriting);
				fcDomain.setDomainCode("d.financial_crime");
				/*List<Permissions> fcPermissions = new ArrayList<>();
				fcPermissions = allPermissions.stream()
						.filter(p -> p.getItemName().equalsIgnoreCase("d.financial_crime"))
						.collect(Collectors.toList());
				if (fcPermissions.size() > 0)
					fcDomain.setPermissionId(fcPermissions.get(0));*/

				Domains ccDomain = new Domains();
				ccDomain.setDomainName("Credit Cards");
				ccDomain.setDomainTitle("Element of Financial Terrain");
				ccDomain.setDomainBody(
						"AI enabled applications to the Credit Cards market identifying risk while revealing opportunities.");
				// ccDomain.setModules(allModulesExceptUnderWriting);
				ccDomain.setDomainCode("d.credit_cards");
				/*List<Permissions> ccPermissions = new ArrayList<>();
				ccPermissions = allPermissions.stream().filter(p -> p.getItemName().equalsIgnoreCase("d.credit_cards"))
						.collect(Collectors.toList());
				if (ccPermissions.size() > 0)
					ccDomain.setPermissionId(ccPermissions.get(0));*/

				Domains slDomain = new Domains();
				slDomain.setDomainName("Social Listening");
				slDomain.setDomainTitle("Social Listening");
				slDomain.setDomainBody(
						"Contextual searching and monitoring of social media, news portals and other open source outlets");
				// slDomain.setModules(allModulesExceptUnderWriting);
				slDomain.setDomainCode("d.social_listening");
				/*List<Permissions> slPermissions = new ArrayList<>();
				slPermissions = allPermissions.stream()
						.filter(p -> p.getItemName().equalsIgnoreCase("d.social_listening"))
						.collect(Collectors.toList());
				if (slPermissions.size() > 0)
					slDomain.setPermissionId(slPermissions.get(0));*/

				Domains iDomain = new Domains();
				iDomain.setDomainName("Insurance");
				iDomain.setDomainTitle("Insurace");
				iDomain.setDomainBody("Onboard clients, upload claims and monitor fraud");
				// iDomain.setModules(allModules);
				iDomain.setDomainCode("d.insurance");
				/*List<Permissions> iPermissions = new ArrayList<>();
				iPermissions = allPermissions.stream().filter(p -> p.getItemName().equalsIgnoreCase("d.insurance"))
						.collect(Collectors.toList());
				if (iPermissions.size() > 0)
					iDomain.setPermissionId(iPermissions.get(0));*/
				System.out.println("Inside Domains");
				domainsService.save(riskDomain);
				domainsService.save(amlDomain);
				domainsService.save(miDomain);
				domainsService.save(fcDomain);
				domainsService.save(ccDomain);
				domainsService.save(slDomain);
				domainsService.save(iDomain);
			}

			// Module Groups
			List<ModulesGroup> modulesGroups = new ArrayList<>();
			if (modulesGroupService.findAll().isEmpty()) {
				ModulesGroup group1 = new ModulesGroup();
				group1.setModuleGroupName("Enrich");
				group1.setModuleGroupIcon("fa fa-cubes");
				group1.setModuleGroupColor("#ef5350");
				group1.setModuleGroupClass("enrich-tile");
				group1.setMenuGroupCode("g.enrich");
				//group1.setPermissionId(privilegesService.getPermissionOfItem("g.enrich"));
				modulesGroupService.save(group1);

				ModulesGroup group2 = new ModulesGroup();
				group2.setModuleGroupName("Discover");
				group2.setModuleGroupIcon("fa fa-binoculars");
				group2.setModuleGroupColor("#8661c6");
				group2.setModuleGroupClass("discover-tile");
				group2.setMenuGroupCode("g.discover");
				//group2.setPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				modulesGroupService.save(group2);

				ModulesGroup group3 = new ModulesGroup();
				group3.setModuleGroupName("Act");
				group3.setModuleGroupIcon("fa fa-paper-plane");
				group3.setModuleGroupColor("#f7a248");
				group3.setModuleGroupClass("act-tile");
				group3.setMenuGroupCode("g.act");
				//group3.setPermissionId(privilegesService.getPermissionOfItem("g.act"));
				modulesGroupService.save(group3);

				ModulesGroup group4 = new ModulesGroup();
				group4.setModuleGroupName("Predict");
				group4.setModuleGroupIcon("fa fa-flask");
				group4.setModuleGroupColor("#4e75ff");
				group4.setModuleGroupClass("predict-tile");
				group4.setMenuGroupCode("g.predict");
				//group4.setPermissionId(privilegesService.getPermissionOfItem("g.predict"));
				modulesGroupService.save(group4);

				ModulesGroup group5 = new ModulesGroup();
				group5.setModuleGroupName("Manage");
				group5.setModuleGroupIcon("fa fa-server");
				group5.setModuleGroupColor("#009f59");
				group5.setModuleGroupClass("manage-tile");
				group5.setMenuGroupCode("g.manage");
				//group5.setPermissionId(privilegesService.getPermissionOfItem("g.manage"));
				modulesGroupService.save(group5);
				System.out.println("Inside Groups");
			}

			// modules
			modulesGroups = modulesGroupService.findAll();
			// List<Domains> allDomainsList=domainsService.findAll();
			if (modulesService.findAll().isEmpty()) {
				// List<Permissions> filterPermission= new ArrayList<>();
				// List<Domains> domainsFiltered= new ArrayList<>();
				// Enrich
				Modules module1 = new Modules();
				module1.setModuleName("Advanced Search");
				module1.setModuleIcon("fa fa-search");
				module1.setMenuCode("m.advanced_search");
				//module1.setPermissionId(privilegesService.getPermissionOfItem("m.advanced_search"));
				// module1.setDomains(allDomainsList);
				Modules module2 = new Modules();
				module2.setModuleName("Onboarding");
				module2.setModuleIcon("fa fa-slideshare");
				module2.setMenuCode("m.onboarding");
				// module2.setDomains(allDomainsList);
				// module2.setDisabled(true);
				//module2.setPermissionId(privilegesService.getPermissionOfItem("m.onboarding"));
				List<ModulesGroup> enrichModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Enrich")).collect(Collectors.toList());
				if (enrichModule != null && enrichModule.size() > 0) {
					module1.setModuleGroup(enrichModule.get(0));
					module2.setModuleGroup(enrichModule.get(0));
				}
				modulesService.save(module1);
				modulesService.save(module2);

				// Discover
				Modules dashboard = new Modules();
				dashboard.setModuleName("Dashboard");
				dashboard.setModuleIcon("fa fa-tachometer");
				dashboard.setMenuCode("m.dashboard");
				//dashboard.setPermissionId(privilegesService.getPermissionOfItem("m.dashboard"));
				// dashboard.setDomains(allDomainsList);
				Modules workspace = new Modules();
				workspace.setModuleName("Workspace");
				workspace.setModuleIcon("fa fa-th-large");
				workspace.setMenuCode("m.workspace");
				//workspace.setPermissionId(privilegesService.getPermissionOfItem("m.workspace"));
				// workspace.setDisabled(true);
				Modules investigationConsole = new Modules();
				investigationConsole.setModuleName("Investigation Console");
				investigationConsole.setModuleIcon("fa fa-eye");
				investigationConsole.setMenuCode("m.investigation_console");
				// investigationConsole.setDomains(allDomainsList);
				//investigationConsole.setPermissionId(privilegesService.getPermissionOfItem("m.investigation_console"));
				// investigationConsole.setDisabled(true);
				Modules transactionMonitoring = new Modules();
				transactionMonitoring.setModuleName("Transaction Monitoring");
				transactionMonitoring.setModuleIcon("fa fa-money");
				transactionMonitoring.setMenuCode("m.transaction_monitoring");
				// transactionMonitoring.setDomains(allDomainsList);
				/*transactionMonitoring
						.setPermissionId(privilegesService.getPermissionOfItem("m.transaction_monitoring"));*/
				// transactionMonitoring.setDisabled(true);
				Modules marketIntelligence = new Modules();
				marketIntelligence.setModuleName("Market Intelligence");
				marketIntelligence.setModuleIcon("fa fa-line-chart");
				marketIntelligence.setMenuCode("m.market_intelligence");
				// marketIntelligence.setDomains(allDomainsList);
				//marketIntelligence.setPermissionId(privilegesService.getPermissionOfItem("m.market_intelligence"));
				// marketIntelligence.setDisabled(true);
				Modules adverseTransactions = new Modules();
				adverseTransactions.setModuleName("Adverse Transactions");
				adverseTransactions.setModuleIcon("fa fa-shirtsinbulk");
				adverseTransactions.setMenuCode("m.adverse_transactions");
				// adverseTransactions.setDomains(allDomainsList);
				//adverseTransactions.setPermissionId(privilegesService.getPermissionOfItem("m.adverse_transactions"));
				// adverseTransactions.setDisabled(true);
				Modules fraud = new Modules();
				fraud.setModuleName("Fraud");
				fraud.setModuleIcon("fa fa-lightbulb-o");
				fraud.setMenuCode("m.fraud");
				// fraud.setDomains(allDomainsList);
				//fraud.setPermissionId(privilegesService.getPermissionOfItem("m.fraud"));
				Modules transactionIntelligence = new Modules();
				transactionIntelligence.setModuleName("Transaction Intelligence");
				transactionIntelligence.setModuleIcon("fa fa-lightbulb-o");
				transactionIntelligence.setMenuCode("m.transaction_intelligence");
				// transactionIntelligence.setDomains(allDomainsList);
				/*transactionIntelligence
						.setPermissionId(privilegesService.getPermissionOfItem("m.transaction_intelligence"));*/
				Modules eDiscovery = new Modules();
				eDiscovery.setModuleName("eDiscovery");
				eDiscovery.setModuleIcon("fa fa-file-o-s");
				eDiscovery.setMenuCode("m.ediscovery");
				// eDiscovery.setDomains(allDomainsList);
				//eDiscovery.setPermissionId(privilegesService.getPermissionOfItem("m.ediscovery"));
				// eDiscovery.setDisabled(true);
				List<ModulesGroup> discoverModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Discover")).collect(Collectors.toList());
				if (discoverModule != null && discoverModule.size() > 0) {
					dashboard.setModuleGroup(discoverModule.get(0));
					workspace.setModuleGroup(discoverModule.get(0));
					investigationConsole.setModuleGroup(discoverModule.get(0));
					transactionMonitoring.setModuleGroup(discoverModule.get(0));
					marketIntelligence.setModuleGroup(discoverModule.get(0));
					adverseTransactions.setModuleGroup(discoverModule.get(0));
					fraud.setModuleGroup(discoverModule.get(0));
					transactionIntelligence.setModuleGroup(discoverModule.get(0));
					eDiscovery.setModuleGroup(discoverModule.get(0));
				}
				modulesService.save(dashboard);
				modulesService.save(workspace);
				modulesService.save(investigationConsole);
				modulesService.save(transactionMonitoring);
				modulesService.save(marketIntelligence);
				modulesService.save(adverseTransactions);
				modulesService.save(fraud);
				modulesService.save(transactionIntelligence);
				modulesService.save(eDiscovery);
				System.out.println("Inside Modules");
				// Act
				Modules entity = new Modules();
				entity.setModuleName("Entity");
				entity.setModuleIcon("fa fa-spinner");
				entity.setMenuCode("m.entity");
				// entity.setDomains(allDomainsList);
				//entity.setPermissionId(privilegesService.getPermissionOfItem("m.entity"));
				// entity.setDisabled(true);
				Modules cases = new Modules();
				cases.setModuleName("Cases");
				cases.setModuleIcon("fa fa-suitcase");
				cases.setMenuCode("m.cases");
				// cases.setDomains(allDomainsList);
				//cases.setPermissionId(privilegesService.getPermissionOfItem("m.cases"));
				// cases.setDisabled(true);
				Modules linkAnalysis = new Modules();
				linkAnalysis.setModuleName("Link Analysis");
				linkAnalysis.setModuleIcon("fa fa-joomla");
				linkAnalysis.setMenuCode("m.link_analysis");
				// linkAnalysis.setDomains(allDomainsList);
				//linkAnalysis.setPermissionId(privilegesService.getPermissionOfItem("m.link_analysis"));
				// linkAnalysis.setDisabled(true);
				Modules underwriting = new Modules();
				underwriting.setModuleName("Underwriting");
				underwriting.setModuleIcon("fa fa-check-square-o");
				underwriting.setMenuCode("m.underwriting");
				//underwriting.setPermissionId(privilegesService.getPermissionOfItem("m.underwriting"));
				Modules auditTrailmodule = new Modules();
				auditTrailmodule.setModuleName("Audit Trail");
				auditTrailmodule.setModuleIcon("fa fa-stack-overflow");
				auditTrailmodule.setMenuCode("m.audit_trail");
				// auditTrailmodule.setDomains(allDomainsList);
				//auditTrailmodule.setPermissionId(privilegesService.getPermissionOfItem("m.audit_trail"));
				List<ModulesGroup> actModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Act")).collect(Collectors.toList());
				if (actModule != null && actModule.size() > 0) {
					entity.setModuleGroup(actModule.get(0));
					cases.setModuleGroup(actModule.get(0));
					linkAnalysis.setModuleGroup(actModule.get(0));
					underwriting.setModuleGroup(actModule.get(0));
					auditTrailmodule.setModuleGroup(actModule.get(0));
				}
				modulesService.save(entity);
				modulesService.save(cases);
				modulesService.save(linkAnalysis);
				modulesService.save(underwriting);
				modulesService.save(auditTrailmodule);

				// Predict
				Modules predict = new Modules();
				predict.setModuleName("Personalization");
				predict.setModuleIcon("fa fa-credit-card");
				predict.setMenuCode("m.personalization");
				//predict.setPermissionId(privilegesService.getPermissionOfItem("m.personalization"));
				List<ModulesGroup> predictModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Predict")).collect(Collectors.toList());
				if (predictModule != null && predictModule.size() > 0) {
					predict.setModuleGroup(predictModule.get(0));
				}
				modulesService.save(predict);

				// Manage
				Modules orchestration = new Modules();
				orchestration.setModuleName("Orchestration");
				orchestration.setModuleIcon("fa fa-user");
				orchestration.setMenuCode("m.orchestration");
				//orchestration.setPermissionId(privilegesService.getPermissionOfItem("m.orchestration"));
				Modules appManager = new Modules();
				appManager.setModuleName("App Manager");
				appManager.setModuleIcon("fa fa-qrcode");
				appManager.setMenuCode("m.app_manager");
				//appManager.setPermissionId(privilegesService.getPermissionOfItem("m.app_manager"));
				// appManager.setDisabled(true);
				Modules dataManagement = new Modules();
				dataManagement.setModuleName("Data Management");
				dataManagement.setModuleIcon("fa fa-life-ring");
				dataManagement.setMenuCode("m.data_management");
				//dataManagement.setPermissionId(privilegesService.getPermissionOfItem("m.data_management"));
				// dataManagement.setDisabled(true);
				Modules systemMonitoring = new Modules();
				systemMonitoring.setModuleName("System Monitoring");
				systemMonitoring.setModuleIcon("fa fa-desktop");
				systemMonitoring.setMenuCode("m.system_monitoring");
				//systemMonitoring.setPermissionId(privilegesService.getPermissionOfItem("m.system_monitoring"));
				// systemMonitoring.setDisabled(true);
				Modules bigDataSciencePlatform = new Modules();
				bigDataSciencePlatform.setModuleName("Big Data Science Platform");
				bigDataSciencePlatform.setModuleIcon("fa fa-flask");
				bigDataSciencePlatform.setMenuCode("m.big_data_science_platform");
				/*bigDataSciencePlatform
						.setPermissionId(privilegesService.getPermissionOfItem("m.big_data_science_platform"));*/
				// bigDataSciencePlatform.setDisabled(true);
				Modules systemSettings = new Modules();
				systemSettings.setModuleName("System Settings");
				systemSettings.setModuleIcon("fa fa-cog");
				systemSettings.setMenuCode("m.system_settings");
				//systemSettings.setPermissionId(privilegesService.getPermissionOfItem("m.system_settings"));
				// systemSettings.setDisabled(true);
				Modules policyEnforcement = new Modules();
				policyEnforcement.setModuleName("Policy Enforcement");
				policyEnforcement.setModuleIcon("fa fa-leanpub");
				policyEnforcement.setMenuCode("m.policy_enforcement");
				//policyEnforcement.setPermissionId(privilegesService.getPermissionOfItem("m.policy_enforcement"));
				// policyEnforcement.setDisabled(true);
				Modules questionnaireBuilder = new Modules();
				questionnaireBuilder.setModuleName("Questionnaire Builder");
				questionnaireBuilder.setModuleIcon("fa fa-newspaper-o");
				questionnaireBuilder.setMenuCode("m.questionnaire_builder");
				//questionnaireBuilder.setPermissionId(privilegesService.getPermissionOfItem("m.questionnaire_builder"));
				// questionnaireBuilder.setDisabled(true);
				Modules dataCuration = new Modules();
				dataCuration.setModuleName("Data Curation");
				dataCuration.setModuleIcon("fa fa-th");
				dataCuration.setMenuCode("m.data_curation");
				//dataCuration.setPermissionId(privilegesService.getPermissionOfItem("m.data_curation"));
				Modules decisionScoring = new Modules();
				decisionScoring.setModuleName("Decision Scoring");
				decisionScoring.setModuleIcon("fa fa-star");
				decisionScoring.setMenuCode("m.decision_scoring");
				//decisionScoring.setPermissionId(privilegesService.getPermissionOfItem("m.decision_scoring"));
				Modules documentParsing = new Modules();
				documentParsing.setModuleName("Document Parsing");
				documentParsing.setModuleIcon("fa fa-file");
				documentParsing.setMenuCode("m.document_parsing");
				//documentParsing.setPermissionId(privilegesService.getPermissionOfItem("m.document_parsing"));
				Modules sourceManagement = new Modules();
				sourceManagement.setModuleName("Source Management");
				sourceManagement.setModuleIcon("fa fa-link");
				sourceManagement.setMenuCode("m.source_management");
				//sourceManagement.setPermissionId(privilegesService.getPermissionOfItem("m.source_management"));
				// sourceManagement.setDisabled(true);

				List<ModulesGroup> manageModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Manage")).collect(Collectors.toList());
				if (manageModule != null && manageModule.size() > 0) {
					orchestration.setModuleGroup(manageModule.get(0));
					appManager.setModuleGroup(manageModule.get(0));
					dataManagement.setModuleGroup(manageModule.get(0));
					systemMonitoring.setModuleGroup(manageModule.get(0));
					bigDataSciencePlatform.setModuleGroup(manageModule.get(0));
					systemSettings.setModuleGroup(manageModule.get(0));
					policyEnforcement.setModuleGroup(manageModule.get(0));
					questionnaireBuilder.setModuleGroup(manageModule.get(0));
					dataCuration.setModuleGroup(manageModule.get(0));
					decisionScoring.setModuleGroup(manageModule.get(0));
					documentParsing.setModuleGroup(manageModule.get(0));
					sourceManagement.setModuleGroup(manageModule.get(0));

				}
				modulesService.save(orchestration);
				modulesService.save(appManager);
				modulesService.save(dataManagement);
				modulesService.save(systemMonitoring);
				modulesService.save(bigDataSciencePlatform);
				modulesService.save(systemSettings);
				modulesService.save(policyEnforcement);
				modulesService.save(questionnaireBuilder);
				modulesService.save(dataCuration);
				modulesService.save(decisionScoring);
				modulesService.save(documentParsing);
				modulesService.save(sourceManagement);

				// adds modules to Domains
				List<Modules> allModulesList = modulesService.findAll();
				List<Domains> allDomainsList = domainsService.findAll();
				for (Domains domain : allDomainsList) {
					domain.setModules(allModulesList);
					domainsService.save(domain);
				}
			}
			/*
			 * //adds modules to Domains List<Modules> allModulesList=
			 * modulesService.findAll(); List<Domains>
			 * allDomainsList=domainsService.findAll(); for(Domains domain: allDomainsList)
			 * { domain.setModules(allModulesList); domainsService.save(domain); }
			 */
			
			List<Users> allUsers=usersService.findAll();
			List<Modules> allModulesList = modulesService.findAll();
			//List<Domains> allDomainsList = domainsService.findAll();
			List<UserMenu> allMenu=menuItemService.findAll();
			
			//////////
			Permissions alertPermissionSaved=null;
			List<Permissions> domainsPermissions = privilegesService.findAll();
			List<Permissions> domainsPermissionsExceptInsurance = new ArrayList<>();
			domainsPermissionsExceptInsurance = domainsPermissions.stream()
					.filter(d -> !d.getItemName().equalsIgnoreCase("d.insurance")).collect(Collectors.toList());
			Permissions alertPermission=privilegesService.getPermissionOfItem("m.alert_management");
			if(null==alertPermission.getItemName()) {
				Permissions newAlertPermission= new Permissions("m.alert_management");
				newAlertPermission.setPermissions(domainsPermissionsExceptInsurance);
				//newAlertPermission.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
				alertPermissionSaved=privilegesService.save(newAlertPermission);
			}
			List<Permissions> allPermissionsNew = privilegesService.findAll();
			if(allPermissionsNew!=null && allPermissionsNew.size()>0) {
				List<Permissions> filteredPermissionsNew=allPermissionsNew.stream().filter(a -> a.getItemName().equals("m.alert_management")).collect(Collectors.toList());
				if(filteredPermissionsNew!=null && filteredPermissionsNew.size()>0) {
					Permissions alertPermissionsNew=filteredPermissionsNew.get(0);
					if(alertPermissionsNew!=null && null==alertPermissionsNew.getParentPermissionId()) {
						alertPermissionsNew.setParentPermissionId(privilegesService.getPermissionOfItem("g.discover"));
						privilegesService.saveOrUpdate(alertPermissionsNew);
					}
				}
			}
			
			modulesGroups= modulesGroupService.findAll();
			List<Modules> alertModulesList=allModulesList.stream().filter(module -> module.getMenuCode().equals("m.alert_management")).collect(Collectors.toList());
			if (alertModulesList == null || alertModulesList.size()==0) {
				Modules alertModule = new Modules();
				alertModule.setModuleName("Alert Management");
				alertModule.setModuleIcon("fa fa-lg fa-bell");
				alertModule.setMenuCode("m.alert_management");
				/*if (alertPermissionSaved != null)
					alertModule.setPermissionId(alertPermissionSaved);
				else
					alertModule.setPermissionId(privilegesService.getPermissionOfItem("m.alert_management"));*/
				List<ModulesGroup> discoverModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Discover")).collect(Collectors.toList());
				if (discoverModule != null && discoverModule.size() > 0) {
					alertModule.setModuleGroup(discoverModule.get(0));
					modulesService.save(alertModule);
				}
				
			}
			List<Modules> userModulesList=allModulesList.stream().filter(module -> module.getMenuCode().equals("m.user_management")).collect(Collectors.toList());
			if (userModulesList == null || userModulesList.size()==0) {
				Modules userManagementModule = new Modules();
				userManagementModule.setModuleName("User Management");
				userManagementModule.setModuleIcon("fa fa-user icon");
				userManagementModule.setMenuCode("m.user_management");
				List<ModulesGroup> manageModule = modulesGroups.stream()
						.filter(group -> group.getModuleGroupName().equals("Manage")).collect(Collectors.toList());
				if (manageModule != null && manageModule.size() > 0) {
					userManagementModule.setModuleGroup(manageModule.get(0));
					modulesService.save(userManagementModule);
				}
				
			}
			allModulesList = modulesService.findAll();
			 allMenu=menuItemService.findAll();
			 
			if (allUsers != null && allUsers.size() > 0) {
				if (allModulesList != null && allModulesList.size() > 0) {
					for (Modules module : allModulesList) {
						//if(module.getModuleName().equals("User Management"))
						for (Users user : allUsers) {
							List<UserMenu> singleUserMenu = allMenu.stream()
									.filter(menu -> menu.getUserId().equals(user.getUserId()))
									.filter(menu -> menu.getModule().getModuleName().equals(module.getModuleName()))
									.collect(Collectors.toList());
							if (singleUserMenu == null || singleUserMenu.size() == 0) {
								UserMenu newMenu = new UserMenu();
								newMenu.setClicksCount(0);
								newMenu.setMenuItemSize(MenuSizeEnum.SMALL.toString());
								newMenu.setModule(module);
								newMenu.setModuleGroup(module.getModuleGroup());
								newMenu.setUserId(user.getUserId());
								menuItemService.save(newMenu);
							}
						}
					}
				}
			}
			//adds default last visited domain for empty db
			List<Users> users=usersService.findAll();
			Long amlPermissionId=0L;
			List<PermissionsWithIdDto> allPermissionsDto=privilegesService.findAllPermissions();
			if(allPermissionsDto!=null && allPermissionsDto.size()>0) {
				List<PermissionsWithIdDto> filteredPermissions=allPermissionsDto.stream().filter(o -> o.getItemName().equalsIgnoreCase("d.aml")).collect(Collectors.toList());
				if(filteredPermissions!=null && filteredPermissions.size()>0) {
					amlPermissionId=filteredPermissions.get(0).getPermissionId();
				}
			}
			if(!amlPermissionId.equals(0L))
				if (users != null && users.size() > 0) {
					for (Users user : users) {
						LastVisitedDomains lastVisited = lastVisitedDomainService
								.findLastVisitedDomainByUserId(user.getUserId());
						if (lastVisited == null) {
							LastVisitedDomains defaultDomain = new LastVisitedDomains();
							defaultDomain.setDomainName("aml");
							defaultDomain.setPermissionId(amlPermissionId);
							defaultDomain.setUserId(user.getUserId());
							lastVisitedDomainService.save(defaultDomain);
						}
					}

				}
			
			
			/*if(!(privilegesService.findAll().stream().anyMatch(p -> "um.user_management".equalsIgnoreCase(p.getItemName())))){
				
				Permissions UserManagementPermission = new Permissions("um.user_management");
				privilegesService.save(UserManagementPermission);
				
				Permissions groupsPermission = new Permissions("um.groups");
				// SourceManagementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				//groupstPermission.setPermissions(domainsPermissionsExceptInsurance);
				groupsPermission.setParentPermissionId(privilegesService.getPermissionOfItem("um.user_management"));
				privilegesService.save(groupsPermission);
				
				Permissions permissionsPermission = new Permissions("um.permissions");
				// SourceManagementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				//groupstPermission.setPermissions(domainsPermissionsExceptInsurance);
				permissionsPermission.setParentPermissionId(privilegesService.getPermissionOfItem("um.user_management"));
				privilegesService.save(permissionsPermission);
				
				Permissions rolesPermission = new Permissions("um.roles");
				// SourceManagementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				//groupstPermission.setPermissions(domainsPermissionsExceptInsurance);
				rolesPermission.setParentPermissionId(privilegesService.getPermissionOfItem("um.user_management"));
				privilegesService.save(rolesPermission);
				
				Permissions usersPermission = new Permissions("um.users");
				// SourceManagementPermission.setDomains(domainsService.getDomainsListWithoutItem("Insurance"));
				//groupstPermission.setPermissions(domainsPermissionsExceptInsurance);
				usersPermission.setParentPermissionId(privilegesService.getPermissionOfItem("um.user_management"));
				privilegesService.save(usersPermission);
				
			}*/


			//Assign Zero permissions to default roles	
			if(adminRole==null)
				adminRole=rolesService.findRoleByName("Admin");
			if(analystRole==null)
				analystRole=rolesService.findRoleByName("Analyst");
			if(userRole==null)
				userRole=rolesService.findRoleByName("User");
			if (groupPermissionService.findAll().isEmpty()) {
				if (adminRole != null)
					groupPermissionService.loadZeroPermissionsForRole(adminRole);
				if (analystRole != null)
					groupPermissionService.loadZeroPermissionsForRole(analystRole);
				if (userRole != null)
					groupPermissionService.loadZeroPermissionsForRole(userRole);
			}
			
			//System settings- User Management Regulation---------------------------------------Start
			List<Settings> enableAuthenticationSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Enable Authentication")).collect(Collectors.toList());
			if (enableAuthenticationSetting.isEmpty() || ((enableAuthenticationSetting.size() > 0)
					&& (!enableAuthenticationSetting.stream().filter(s ->s.getSystemSettingType()
							.equalsIgnoreCase(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString())).findFirst()
							.isPresent()))) {
				Settings enableAuthentication = new Settings();
				enableAuthentication.setName("Enable Authentication");
				// settings17.setSection("User Interaction");
				enableAuthentication.setType("Toggle On/Off");
				enableAuthentication.setDefaultValue("Off");
				enableAuthentication.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(enableAuthentication);
			}
			
			// twoWayAuthenticationSetting
			List<Settings> twoWayAuthentication = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("2-way Authentication")).collect(Collectors.toList());
			if (twoWayAuthentication.isEmpty()) {
				Settings twoWayAuthenticationSetting = new Settings();
				twoWayAuthenticationSetting.setName("2-way Authentication");
				// settings17.setSection("User Interaction");
				twoWayAuthenticationSetting.setType("Toggle On/Off");
				twoWayAuthenticationSetting.setDefaultValue("Off");
				twoWayAuthenticationSetting.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(twoWayAuthenticationSetting);
			}
			
			List<Settings> pendingUsersDeactivationSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Pending users deactivation after")).collect(Collectors.toList());
			if (pendingUsersDeactivationSetting.isEmpty()) {
				Settings pendingDeactivation = new Settings();
				pendingDeactivation.setName("Pending users deactivation after");
				//settings17.setSection("User Interaction");
				pendingDeactivation.setType("Text");
				pendingDeactivation.setDefaultValue("10");
				pendingDeactivation.setMaximumLength(10);
				pendingDeactivation.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(pendingDeactivation);
			}
			
			List<Settings> createUserSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow to create manually-Users")).collect(Collectors.toList());
			if (createUserSetting.isEmpty()) {
				Settings createUser = new Settings();
				createUser.setName("Allow to create manually-Users");
				//settings17.setSection("User Interaction");
				createUser.setType("Toggle On/Off");
				createUser.setDefaultValue("Off");
				createUser.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(createUser);
			}
			
			List<Settings> createRoleSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow to create manually-Roles")).collect(Collectors.toList());
			if (createRoleSetting.isEmpty()) {
				Settings createRole = new Settings();
				createRole.setName("Allow to create manually-Roles");
				//settings17.setSection("User Interaction");
				createRole.setType("Toggle On/Off");
				createRole.setDefaultValue("Off");
				createRole.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(createRole);
			}
			
			List<Settings> createGroupSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow to create manually-Groups")).collect(Collectors.toList());
			if (createGroupSetting.isEmpty()) {
				Settings createGroup = new Settings();
				createGroup.setName("Allow to create manually-Groups");
				//settings17.setSection("User Interaction");
				createGroup.setType("Toggle On/Off");
				createGroup.setDefaultValue("Off");
				createGroup.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(createGroup);
			}
			
			List<Settings> updateUserSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow update manually-Users")).collect(Collectors.toList());
			if (updateUserSetting.isEmpty()) {
				Settings updateUser = new Settings();
				updateUser.setName("Allow update manually-Users");
				//settings17.setSection("User Interaction");
				updateUser.setType("Toggle On/Off");
				updateUser.setDefaultValue("Off");
				updateUser.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(updateUser);
			}
			
			List<Settings> updateRoleSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow update manually-Roles")).collect(Collectors.toList());
			if (updateRoleSetting.isEmpty()) {
				Settings updateRole = new Settings();
				updateRole.setName("Allow update manually-Roles");
				//settings17.setSection("User Interaction");
				updateRole.setType("Toggle On/Off");
				updateRole.setDefaultValue("Off");
				updateRole.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(updateRole);
			}
			
			List<Settings> updateGroupSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow update manually-Groups")).collect(Collectors.toList());
			if (updateGroupSetting.isEmpty()) {
				Settings updateGroup = new Settings();
				updateGroup.setName("Allow update manually-Groups");
				//settings17.setSection("User Interaction");
				updateGroup.setType("Toggle On/Off");
				updateGroup.setDefaultValue("Off");
				updateGroup.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(updateGroup);
			}
			
			List<Settings> thirdPartUserSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow 3rd Party synchronization-Users")).collect(Collectors.toList());
			if (thirdPartUserSetting.isEmpty()) {
				Settings thirdPartUser = new Settings();
				thirdPartUser.setName("Allow 3rd Party synchronization-Users");
				//settings17.setSection("User Interaction");
				thirdPartUser.setType("Toggle On/Off");
				thirdPartUser.setDefaultValue("Off");
				thirdPartUser.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(thirdPartUser);
			}
			
			List<Settings> thirdPartRoleSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow 3rd Party synchronization-Roles")).collect(Collectors.toList());
			if (thirdPartRoleSetting.isEmpty()) {
				Settings thirdPartRole = new Settings();
				thirdPartRole.setName("Allow 3rd Party synchronization-Roles");
				//settings17.setSection("User Interaction");
				thirdPartRole.setType("Toggle On/Off");
				thirdPartRole.setDefaultValue("Off");
				thirdPartRole.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(thirdPartRole);
			}
			
			List<Settings> thirdPartGroupSetting = settingsService.findAll().stream()
					.filter(setting -> setting.getName().equals("Allow 3rd Party synchronization-Groups")).collect(Collectors.toList());
			if (thirdPartGroupSetting.isEmpty()) {
				Settings thirdPartGroup = new Settings();
				thirdPartGroup.setName("Allow 3rd Party synchronization-Groups");
				//settings17.setSection("User Interaction");
				thirdPartGroup.setType("Toggle On/Off");
				thirdPartGroup.setDefaultValue("Off");
				thirdPartGroup.setSystemSettingType(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString());
				settingsService.save(thirdPartGroup);
			}
			//System settings- User Management Regulation---------------------------------------End
			List<Settings> allSettingsNew=settingsService.findAllWithAttributes();
			if(allSettingsNew!=null && allSettingsNew.size()>0) {
				
				List<Settings> listSettings=allSettingsNew.stream().filter(o -> o.getSystemSettingType().equalsIgnoreCase("LIST_MANAGEMENT")).collect(Collectors.toList());
				if(listSettings!=null & listSettings.size()>0) {
					Settings listSetting= listSettings.get(0);
					if(listSetting!=null) {
						List<Attributes> listAttributes= listSetting.getAttributesList().stream().filter(j -> j.getAttributeName().equalsIgnoreCase("Languages")).collect(Collectors.toList());
						if(listAttributes==null || listAttributes.size()==0) {
							listAttributes= new ArrayList<>();
							Attributes userStatusAttribute= new Attributes();
							userStatusAttribute.setAttributeName("Languages");
							userStatusAttribute.setAttributeValue("Languages");
							userStatusAttribute.setSettings(listSettings.get(0));
							listAttributes.add(userStatusAttribute);
							listSettings.get(0).setAttributesList(listAttributes);
							settingsService.saveOrUpdate(listSettings.get(0));
						}
					}
				}
				
			}

			// Theme settings
			List<Settings> darkThemeSettingsList = settingsService.findAllWithAttributes().stream()
					.filter(setting -> setting.getName().equals("Dark Theme")).collect(Collectors.toList());
			if (darkThemeSettingsList.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Dark Theme");
				settings51.setSection("Theme");
				settings51.setType("selection");
				settings51.setDefaultValue("Dark Theme");
				settings51.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settings51);
			}
			List<Settings> lightThemeSettingsList = settingsService.findAllWithAttributes().stream()
					.filter(setting -> setting.getName().equals("Light Theme")).collect(Collectors.toList());
			if (lightThemeSettingsList.isEmpty()) {
				Settings settings51 = new Settings();
				settings51.setName("Light Theme");
				settings51.setSection("Theme");
				settings51.setType("selection");
				// settings51.setDefaultValue("Dark Theme");
				settings51.setSystemSettingType("GENERAL_SETTING");
				settingsService.save(settings51);
			}
			try{
				alertService.createNewAlerts(null);			
			}catch(Exception e){
				e.printStackTrace();
			}
			
			scheduleSourcesUtility.demoServiceMethod();

		} catch (Exception e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
	}

	
////////////////////////////////////////////////////////////////////////////////////////////////



}