package element.bst.elementexploration.rest.extention.sourcemonitoring.service;

import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.ErrorBubbleChartDto;
import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.SourceMonitoringFinalDto;
import element.bst.elementexploration.rest.extention.sourcemonitoring.dto.SourceMonitoringMetadataDto;

public interface SourceMonitoringService {

	SourceMonitoringFinalDto  getMetaData(String dateLimit, String source_id) throws Exception;

	List<SourceMonitoringMetadataDto> getSparklineDataById(String dateLimit, String source_id) throws Exception;

	List<ErrorBubbleChartDto> getErrorData(String dateLimit) throws Exception;

}
