package element.bst.elementexploration.rest.version.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.version.domain.ProductVersion;

public interface VersionService extends GenericService<ProductVersion, Long> {

	ProductVersion getLatestVersion();

	List<ProductVersion> getAllVersions();
}
