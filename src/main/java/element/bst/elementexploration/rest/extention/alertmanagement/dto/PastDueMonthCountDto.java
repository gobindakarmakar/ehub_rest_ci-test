package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;

public class PastDueMonthCountDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer Year;

	private Integer month;

	private Integer count;

	public Integer getYear() {
		return Year;
	}

	public void setYear(Integer year) {
		Year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
