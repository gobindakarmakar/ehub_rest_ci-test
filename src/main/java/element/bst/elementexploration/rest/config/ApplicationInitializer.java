package element.bst.elementexploration.rest.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import element.bst.elementexploration.activiti.app.conf.ApplicationConfiguration;
import element.bst.elementexploration.activiti.app.servlet.ApiDispatcherServletConfiguration;
import element.bst.elementexploration.activiti.app.servlet.AppDispatcherServletConfiguration;

/**
 * 
 * @author Amit Patel
 *
 */
public class ApplicationInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) {

		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(WebConfig.class);
		rootContext.register(ApplicationConfiguration.class);// Added for activiti
		rootContext.setServletContext(container);
		rootContext.refresh();

		container.addListener(new ContextLoaderListener(rootContext));

		Dynamic springSecurityFilterChain = container.addFilter("springSecurityFilterChain",
				new DelegatingFilterProxy());
		springSecurityFilterChain.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD),
				false, "/*");
		springSecurityFilterChain.setAsyncSupported(true);

		ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(rootContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
		dispatcher.setAsyncSupported(true);

		/* Activiti Configuration Start */

		/* Activiti Rest ApiConfiguration */
		AnnotationConfigWebApplicationContext apiDispatcherServletConfiguration = new AnnotationConfigWebApplicationContext();
		apiDispatcherServletConfiguration.setParent(rootContext);
		apiDispatcherServletConfiguration.register(ApiDispatcherServletConfiguration.class,
				ApplicationConfiguration.class);

		ServletRegistration.Dynamic apiDispatcherServlet = container.addServlet("apiDispatcher",
				new DispatcherServlet(apiDispatcherServletConfiguration));
		apiDispatcherServlet.addMapping("/rest/*");
		apiDispatcherServlet.setLoadOnStartup(1);
		apiDispatcherServlet.setAsyncSupported(true);

		/* Activiti Application Configuration */
		AnnotationConfigWebApplicationContext appDispatcherServletConfiguration = new AnnotationConfigWebApplicationContext();
		appDispatcherServletConfiguration.setParent(apiDispatcherServletConfiguration);
		appDispatcherServletConfiguration.register(AppDispatcherServletConfiguration.class);

		ServletRegistration.Dynamic appDispatcherServlet = container.addServlet("appDispatcher",
				new DispatcherServlet(appDispatcherServletConfiguration));
		appDispatcherServlet.addMapping("/app/*");
		appDispatcherServlet.setLoadOnStartup(1);
		appDispatcherServlet.setAsyncSupported(true);

		/* Activiti Configuration End */
	}
}