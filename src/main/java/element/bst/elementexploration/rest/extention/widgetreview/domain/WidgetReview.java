package element.bst.elementexploration.rest.extention.widgetreview.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Rambabu
 *
 */
@ApiModel("widget")
@Entity
@Table(name = "compliance_widget_review")
public class WidgetReview implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "The ID of entity section")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "widget_id")
	private Long Id;

	@ApiModelProperty(value = "approval of the widget")
	@Column(name = "is_approved")
	private Boolean isApproved = false;

	@CreationTimestamp
	@Column(name = "approved_date")
	private Date approvedDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private Users user;

	@ApiModelProperty(value = "entityId of the widget", required = true)
	@Column(name = "entity_id")
	@NotEmpty(message = "entityId can not be blank.")
	private String entityId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compliance_widget_id")
	private ComplianceWidget widget;

	public WidgetReview() {
		super();
	}

	public WidgetReview(Boolean isApproved, Date approvedDate, Users user, String entityId, ComplianceWidget widget) {
		super();
		this.isApproved = isApproved;
		this.approvedDate = approvedDate;
		this.user = user;
		this.entityId = entityId;
		this.widget = widget;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Boolean getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public ComplianceWidget getWidget() {
		return widget;
	}

	public void setWidget(ComplianceWidget widget) {
		this.widget = widget;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
