package element.bst.elementexploration.rest.settings.listManagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Icon Management")
@Entity
@Table(name = "LM_ICONS")
public class Icon {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long iconId;

	@ApiModelProperty(value = "color")
	@Column(name = "name")
	private String name;
	
	public Icon() {
	}

	public Long getIconId() {
		return iconId;
	}

	public void setIconId(Long iconId) {
		this.iconId = iconId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
