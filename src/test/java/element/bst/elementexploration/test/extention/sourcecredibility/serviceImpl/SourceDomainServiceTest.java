package element.bst.elementexploration.test.extention.sourcecredibility.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceDomainDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceDomainDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl.SourceDomainServiceImpl;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SourceDomainServiceTest {
	
	
	@Mock
	SourceDomainDao sourceDomainDao;
	
	@InjectMocks
	SourceDomainServiceImpl sourceDomainService;
	
	List<SourceDomain> domianList = new ArrayList<SourceDomain>();
	SourceDomain sourceDomainOne = null;
	SourceDomainDto sourceDomainDto = null;
	
	@Before
    public void setupMock() {
		sourceDomainOne = new SourceDomain("Financial Crime");
		sourceDomainDto = new SourceDomainDto("Market Sonar");
		SourceDomain sourceDomainTwo = new SourceDomain("Compliance");
		domianList.add(sourceDomainOne);
		domianList.add(sourceDomainTwo);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getClassifications() {
		Mockito.when(sourceDomainDao.findAll()).thenReturn(domianList);
		List<SourceDomainDto> domainsCheck =  sourceDomainService.getSourceDomain();
		Assert.assertEquals(domainsCheck.size(),domianList.size());
	}
	
	@Test
	public void saveSourceDomain() throws Exception {
		Mockito.when(sourceDomainDao.create(Mockito.anyObject())).thenReturn(sourceDomainOne);
		Mockito.when(sourceDomainDao.findAll()).thenReturn(domianList);
		List<SourceDomainDto> domainsCheck =  sourceDomainService.saveSourceDomain(sourceDomainDto);
		Assert.assertEquals(domainsCheck.size(),domianList.size());
	}
	

}
