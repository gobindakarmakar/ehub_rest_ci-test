package element.bst.elementexploration.rest.extention.alert.feedmanagement.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
/**
 * @author prateek
 *
 */
public class FeedGroupsDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long feedGroupsId;
	private Integer rank;
	private FeedManagement feedId;
	private Groups groupId;
	public Long getFeedGroupsId() {
		return feedGroupsId;
	}
	public Integer getRank() {
		return rank;
	}
	public FeedManagement getFeedId() {
		return feedId;
	}
	public Groups getGroupId() {
		return groupId;
	}
	public void setFeedGroupsId(Long feedGroupsId) {
		this.feedGroupsId = feedGroupsId;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public void setFeedId(FeedManagement feedId) {
		this.feedId = feedId;
	}
	public void setGroupId(Groups groupId) {
		this.groupId = groupId;
	}
	
}
