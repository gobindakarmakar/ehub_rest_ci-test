package element.bst.elementexploration.rest.contacts.dao;

import java.util.List;

import element.bst.elementexploration.rest.contacts.domain.Contact;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Amit Patel
 * 
 * */
public interface ContactsDao extends GenericDao<Contact, Long>{
	
	List<ContactDTO> getUserContactList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn, String keyword);
	
	Long countUserContactList(Long userId, String keyword);
	
	void removeContact(Long contactId, Long userId);
	
	Contact getUserContact(Long contactId,Long userId);
}
