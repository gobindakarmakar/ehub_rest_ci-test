'use strict';
activitiApp.controller('UserEventsController', userEventsController);

		userEventsController.$inject = [
			'$scope',
			'$rootScope',
			'$window',
			'TopPanelApiService'
		];
	    function userEventsController(
	    		$scope, 
	    		$rootScope,
	    		$window,
	    		TopPanelApiService){
	    	
//			$scope.token = $rootScope.ehubObject.token;
			var params = {
		    	token: $scope.token		    	
		    };
			
			var monthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
	    	var date = new Date();
	    	/*$scope.day = date.getDate();
	    	$scope.month = monthNames[date.getMonth()];
	    	$scope.year = date.getFullYear();*/
	    	$scope.currentEvents = [];
	    	$scope.current = [];
	    	$scope.allEvents = [];
	    	$scope.next = [];
	    	$scope.data = [];
			/*
		     * @purpose: get user events
		     * @created: 13 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
	    	/*TopPanelApiService.getUserEvents(params).then(function(response){
				var monthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    	    	var date = new Date();
    	    	$scope.day = date.getDate();
    	    	$scope.month = monthNames[date.getMonth()];
    	    	$scope.year = date.getFullYear();
    	    	$scope.currentEvents = [];
    	    	$scope.current = response.data['currentEvents'];
    	    	$scope.allEvents = response.data['allEvents'];
    	    	$scope.next = response.data['nextEvents'];*/
    	    	
    	    	/*shows only the first current event - not sure on how to show multiple events on the same day*/
	    	/*$scope.data = response.data['currentEvents'][0];
			}, function(error){
				console.log(error);
			});*/
			/*$rootScope.dateSelected;*/
			var caseId =  getParameterByName('caseId');
			if(caseId != undefined && caseId != null && caseId != ''){
	    		$scope.showModal = function(date){
	    			params = {
	 		        	"caseId": caseId,
	 		        	"token": $scope.token
	 		        };
	    			var dateSelected = new Date(date);
	    			$rootScope.dateSelected = dateSelected;
	    			TopPanelApiService.getNotificationAlert(params).then(function(response){
	    				if (response.data != null && response.data.length > 0) {
	    		    		var i = 0, dataLength = response.data.length;
	    		    		function loop() {
	    		    			var data = response.data[i];
	    		    			var dataDate = new Date(data.createdOn);
	    		    			
	    			    	 	if(dateSelected.toDateString() === dataDate.toDateString()) {
	    			    	 		showSuccessAlert("INFO", data.message);
	    			    	 		i++;
	    			    		    if( i < dataLength ){
	    			    		        setTimeout(loop, 2000);
	    			    		    }
	    			    	 	} else {
	    			    	 		i++;
	    			    	 		if( i < dataLength ){
	    			    		        setTimeout(loop, 0);
	    			    		    }
	    			    	 	}
	    		    		}
	    		    		loop();
	    		    	}
	    			}, function(error){
	    			});
    	    	};
			}
	    		
    		function getParameterByName(name, url) {
    		    if (!url) {
    		      url = window.location.href;
    		    }
    		    name = name.replace(/[\[\]]/g, "\\$&");
    		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    		        results = regex.exec(url);
    		    if (!results) return null;
    		    if (!results[2]) return '';
    		    return decodeURIComponent(results[2].replace(/\+/g, " "));
    		}
	    };