package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.app.service.exception.NotFoundException;
import org.hibernate.Hibernate;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedGroupSevice;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedManagementService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupsDao;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserGroupsDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupAlertSettingDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UpdateGroupDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupAlertService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.RoleGroupService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author Paul Jalagari
 *
 */
@Service("groupsService")
public class GroupsServiceImpl extends GenericServiceImpl<Groups, Long> implements GroupsService {

	@Autowired
	private GroupsDao groupsDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private UserGroupsDao userGroupsDao;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private UserGroupsService userGroupsService;

	@Autowired
	FeedManagementService feedManagementService;

	@Autowired
	UsersService usersService;

	@Autowired
	RoleGroupService roleGroupService;

	@Autowired
	GroupAlertService groupAlertService;

	@Autowired
	FeedGroupSevice feedGroupService;

	@Autowired
	public GroupsServiceImpl(GenericDao<Groups, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public Groups getGroup(String name) {
		Groups group = groupsDao.getGroup(name);
		return group;
	}

	@Override
	@Transactional("transactionManager")
	public List<Groups> getUserGroups(Boolean status, String keyword, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		return groupsDao.getUserGroups(status, keyword,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public boolean isActiveGroup(Long groupId) {

		Groups group = groupsDao.find(groupId);
		boolean retVal = false;

		if (null != group) {
			if (group.getActive()) {
				retVal = true;
			}
		}

		return true;
	}

	@Override
	@Transactional("transactionManager")
	public List<Groups> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy,
			String orderIn) {

		pageNumber = (pageNumber == null) ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber;
		recordsPerPage = (recordsPerPage == null) ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;

		return groupsDao.getGroupsOfUser(userId, pageNumber, recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserGroups(Boolean status, String keyword) {
		return groupsDao.countUserGroups(status, keyword);
	}

	@Override
	@Transactional("transactionManager")
	public Long countGroupsOfUser(Long userId) {
		return groupsDao.countGroupsOfUser(userId);
	}

	@Override
	@Transactional("transactionManager")
	public Groups checkGroupExist(String name, Long groupId) {
		return groupsDao.checkGroupExist(name, groupId);
	}

	@Override
	@Transactional("transactionManager")
	public Boolean doesUserbelongtoGroup(String groupName, Long userId) {
		Users bstuser = usersDao.find(userId);
		bstuser = usersService.prepareUserFromFirebase(bstuser);
		if (bstuser == null)
			throw new NoDataFoundException(ElementConstants.USER_NOT_FOUND_MSG);

		return groupsDao.doesUserbelongtoGroup(groupName, userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<Users> getUsersByGroup(Long groupId) {
		return groupsDao.getUsersByGroup(groupId);
	}

	@Override
	@Transactional("transactionManager")
	public List<Groups> getGroupsOfUser(Long userId) {
		return groupsDao.getGroupsOfUser(userId);
	}

	@Override
	@Transactional("transactionManager")
	public List<Groups> getGroupsOfUserWithRoles(Long userId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {

		pageNumber = (pageNumber == null) ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber;
		recordsPerPage = (recordsPerPage == null) ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;

		return groupsDao.getGroupsOfUserWithRoles(userId, pageNumber, recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public boolean addUsersListToGroup(long adminId, List<Long> userIds, Long groupId) {
		int successCount = 0;
		if (userIds != null && userIds.size() > 0) {
			for (Long userId : userIds) {
				Users user = usersDao.find(userId);
				if (user != null) {
					Groups group = groupsDao.find(groupId);
					if (group != null) {
						if (userGroupsDao.getUserGroup(userId, groupId) == null) {
							UserGroups userGroup = new UserGroups();
							userGroup.setGroupId(group);
							userGroup.setUserId(user);
							userGroupsDao.create(userGroup);
							successCount = successCount + 1;
							AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
									ElementConstants.STRING_TYPE_USER_GROUP, null,
									ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
							Users admin = null;
							admin = usersDao.find(adminId);
							log.setDescription(admin.getFirstName() + " " + admin.getLastName()
									+ ElementConstants.USER_ADDED_IN_GROUP_SUCCESSFULLY_MSG);
							log.setUserId(userId);
							auditLogService.save(log);
						}

					}
				}
			}

		}
		if (userIds != null && successCount == userIds.size()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@Transactional("transactionManager")
	public Object getStatuswiseUsersByGroupId(Long groupId) {
		return groupsDao.getStatuswiseUsersByGroupId(groupId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getJsonFromMap(Map<String, Long> statusObjMap) {
		JSONObject jsonData = new JSONObject();
		for (String key : statusObjMap.keySet()) {
			Object value = statusObjMap.get(key);
			if (value instanceof Map<?, ?>) {
				value = getJsonFromMap((Map<String, Long>) value);
			}
			jsonData.put(key, value);
		}
		return jsonData;
	}

	@Override
	@Transactional("transactionManager")
	public Groups updateGroupById(Long groupId, UpdateGroupDto updatedGroup, Long adminId) {
		Groups group = find(groupId);
		group.setName(updatedGroup.getName());
		if (updatedGroup.getDescription() != null)
			group.setDescription(updatedGroup.getDescription());
		if (updatedGroup.getRemarks() != null)
			group.setRemarks(updatedGroup.getRemarks());
		if (null != updatedGroup.getActive() && updatedGroup.getActive())
			group.setActive(true);
		if (null != updatedGroup.getActive() && !updatedGroup.getActive())
			group.setActive(false);
		if (updatedGroup.getColor() != null)
			group.setColor(updatedGroup.getColor());
		if (updatedGroup.getIcon() != null)
			group.setIcon(updatedGroup.getIcon());
		if (updatedGroup.getSource() != null)
			group.setSource(updatedGroup.getSource());
		group.setModifiedDate(new Date());
		saveOrUpdate(group);
		return group;
	}

	@Override
	@Transactional("transactionManager")
	public Groups checkIfGroupExist(String name, Long groupId) {
		return groupsDao.checkIfGroupExist(name, groupId);
	}

	@Override
	public boolean addGroupAlertSetting(List<GroupAlertSettingDto> groupAlertSettingDto, Long currentUserId) {
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteGroup(Long groupId, Long currentUserId) {
		Groups group = null;
		boolean isDeleted = false;
		if (groupId != null) {
			group = groupsDao.find(groupId);
			if (group != null) {
				groupsDao.delete(group);
				isDeleted = true;
			} else {
				throw new NotFoundException("Role not found!");
			}
		} else {
			throw new NotFoundException("group ID can't be null or empty");
		}
		Users user = usersDao.find(currentUserId);
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.GROUP_MANAGEMENT_TYPE, null,
				ElementConstants.GROUP_MANAGEMENT_TYPE);
		log.setDescription(user.getFirstName() + " " + user.getLastName() + " deleted a group ");
		log.setName(ElementConstants.GROUP_MANAGEMENT_TYPE);
		log.setUserId(currentUserId);
		log.setType(ElementConstants.GROUP_MANAGEMENT_TYPE);
		log.setTypeId(groupId);
		auditLogService.save(log);
		return isDeleted;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteGroups(Long groupsId, Long currentUserId) throws Exception {
		Groups groups = null;
		boolean isDeleted = false;
		if (groupsId != null) {
			groups = find(groupsId);
			if (groups != null) {
				List<Long> groupIds = new ArrayList<>();
				groupIds.add(groupsId);
				// remove existing roles from group
				/*
				 * List<RoleGroup> existingRoleGroups =
				 * roleGroupService.getRolesByGroup(groupIds); if (existingRoleGroups != null &&
				 * existingRoleGroups.size() > 0)
				 * roleGroupService.deleteExistingRolesForGroup(groupsId, currentUserId);
				 * //unassign users from group List<Users> usersList=getUsersByGroup(groupsId);
				 * if(usersList!=null && usersList.size()>0) { for(Users eachUser:usersList) {
				 * userGroupsService.removeUserGroups(eachUser,groupIds,currentUserId,"delete");
				 * } }
				 */
				List<FeedGroups> existingFeedGroups = feedManagementService.getFeedGroupsByGroupId(groupIds);
				if (existingFeedGroups != null && existingFeedGroups.size() > 0) {
					List<FeedManagement> feedIdList = existingFeedGroups.stream().map(f -> f.getFeedId())
							.collect(Collectors.toList());

					for (FeedManagement feed : feedIdList) {
						List<FeedGroups> fGroups = feed.getGroupLevels();
						FeedManagementDto feedMDto = new FeedManagementDto();
						feedMDto.setFeed_management_id(feed.getFeed_management_id());

						if (fGroups != null && fGroups.size() > 0) {
							for (int i = 0; i < fGroups.size(); i++) {
								FeedGroups eachFeed = fGroups.get(i);
								if (eachFeed.getGroupId().getId().equals(groupsId))
									fGroups.remove(eachFeed);
							}
						}
						feedMDto.setGroupLevels(fGroups);
						// feedManagementService.saveOrUpdateFeedItem(feedMDto, currentUserId);
						List<FeedGroups> dbgroups = new ArrayList<>();
						dbgroups = feedGroupService.getFeedGroupsByFeedId(feedMDto.getFeed_management_id());
						feedGroupService.deleteFeedgroupList(dbgroups, null);
						feedManagementService.updateFeedGroupsSupportingFeed(feedMDto.getFeed_management_id(), fGroups);

					}
				}
				Hibernate.initialize(groups.getFeedGroups());
				List<FeedGroups> feedGroups = groups.getFeedGroups();
				if (feedGroups != null && feedGroups.size() > 0) {
					for (int i = 0; i < feedGroups.size(); i++) {
						FeedGroups eachFeedGroup = feedGroups.get(i);
						FeedManagement feedManagementMapped = eachFeedGroup.getFeedId();
						List<FeedGroups> groupLevels = feedManagementMapped.getGroupLevels();
						for (int j = 0; j < groupLevels.size(); j++) {
							FeedGroups eachLevel = groupLevels.get(j);
							if (eachLevel.getGroupId().getId().equals(groupsId))
								groupLevels.remove(eachLevel);
						}
						if (eachFeedGroup.getGroupId().getId().equals(groupsId)) {
							feedGroups.remove(eachFeedGroup);
							feedGroupService.delete(eachFeedGroup);
						}
					}
				}
				Hibernate.initialize(groups.getUserGroups());
				List<UserGroups> userGroupsList = groups.getUserGroups();
				if (userGroupsList != null && userGroupsList.size() > 0) {
					for (int k = 0; k < userGroupsList.size(); k++) {
						UserGroups eachUserGroup = userGroupsList.get(k);
						if (eachUserGroup.getGroupId().getId().equals(groupsId)) {
							userGroupsList.remove(eachUserGroup);
							userGroupsService.delete(eachUserGroup);
						}
					}
				}
				Hibernate.initialize(groups.getGroupAlerts());
				List<GroupAlert> groupAlertsList = groups.getGroupAlerts();
				if (groupAlertsList != null && groupAlertsList.size() > 0) {
					for (int l = 0; l < groupAlertsList.size(); l++) {
						GroupAlert eachGroupAlert = groupAlertsList.get(l);
						if (eachGroupAlert.getGroupId().getId().equals(groupsId)) {
							groupAlertsList.remove(eachGroupAlert);
							groupAlertService.delete(eachGroupAlert);
						}
					}
				}
				Hibernate.initialize(groups.getRoleGroups());
				List<RoleGroup> roleGroupsList = groups.getRoleGroups();
				if (roleGroupsList != null && roleGroupsList.size() > 0) {
					for (int z = 0; z < roleGroupsList.size(); z++) {
						RoleGroup eachRoleGroup = roleGroupsList.get(z);
						if (eachRoleGroup.getGroupId().getId().equals(groupsId)) {
							roleGroupsList.remove(eachRoleGroup);
							roleGroupService.delete(eachRoleGroup);
						}
					}
				}

				// delete associations from GroupAlert table
				/*
				 * List<GroupAlert>
				 * existingGroupAlerts=groupAlertService.getGroupAlertsByGroupIdList(groupIds);
				 * if(existingGroupAlerts!=null && existingGroupAlerts.size()>0) {
				 * groupAlertService.removeExistingGroupAlertSettings(groupsId,
				 * ElementConstants.JURISDICTION_TYPE_STRING);
				 * groupAlertService.removeExistingGroupAlertSettings(groupsId,
				 * ElementConstants.FEED_CLASSIFICATION_TYPE); }
				 */

				try {
					groups = find(groupsId);
					if (groups != null) {
						update(groups);
						delete(groups);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				isDeleted = true;
			} else {
				throw new NotFoundException("Groups not found!");
			}
		} else {
			throw new NotFoundException("groups ID can't be null or empty");
		}
		Users user = usersDao.find(currentUserId);
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.GROUPS_MANAGEMENT_TYPE, null,
				ElementConstants.GROUPS_MANAGEMENT_TYPE);
		log.setDescription(user.getFirstName() + " " + user.getLastName() + " deleted a groups ");
		log.setName(ElementConstants.GROUPS_MANAGEMENT_TYPE);
		log.setUserId(currentUserId);
		log.setType(ElementConstants.GROUPS_MANAGEMENT_TYPE);
		log.setTypeId(groupsId);
		auditLogService.save(log);
		return isDeleted;
	}

}
