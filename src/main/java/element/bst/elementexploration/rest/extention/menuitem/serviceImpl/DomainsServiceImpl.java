package element.bst.elementexploration.rest.extention.menuitem.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.extention.menuitem.service.DomainsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Jalagari Paul
 *
 */
@Service("domainsService")
@Transactional("transactionManager")
public class DomainsServiceImpl extends GenericServiceImpl<Domains, Long> implements DomainsService {

	public DomainsServiceImpl() {
	}

	@Autowired
	public DomainsServiceImpl(GenericDao<Domains, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public List<Domains> getDomainsListWithoutItem(String item) throws Exception {
		List<Domains> domains=findAll().stream().filter(d -> !d.getDomainName().equalsIgnoreCase(item)).collect(Collectors.toList());
		return domains;
	}

}
