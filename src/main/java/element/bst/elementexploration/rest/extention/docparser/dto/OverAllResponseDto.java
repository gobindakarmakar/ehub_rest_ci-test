package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Overall response")
public class OverAllResponseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "List of Document contents")
	private List<DocumentContentDto> documentContentDto;

	@ApiModelProperty(value = "List of iso categories")
	private List<Categories> categories;

	@ApiModelProperty(value = "List of iso categories")
	private Long templateId;

	public List<DocumentContentDto> getDocumentContentDto() {
		return documentContentDto;
	}

	public void setDocumentContentDto(List<DocumentContentDto> documentContentDto) {
		this.documentContentDto = documentContentDto;
	}

	public List<Categories> getCategories() {
		return categories;
	}

	public void setCategories(List<Categories> categories) {
		this.categories = categories;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	@Override
	public String toString() {
		return "OverAllResponseDto [documentContentDto=" + documentContentDto + "]";
	}

}
