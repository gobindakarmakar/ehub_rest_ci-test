package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourceCredibilityDao extends GenericDao<SourceCredibility, Long> {

	SourceCredibility findSubClassificationCredibility(Long sourceId, Long subClassificationId, Long attributeId);

}
