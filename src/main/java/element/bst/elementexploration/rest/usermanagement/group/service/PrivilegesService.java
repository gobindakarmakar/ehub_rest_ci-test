package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;

/**
 * @author Paul Jalagari
 *
 */
public interface PrivilegesService extends GenericService<Permissions, Long> {

	public Permissions getPermissionOfItem(String item) throws Exception;

	public Permissions findPermission(Permissions permissions) throws Exception;

	public List<PermissionsWithIdDto> findAllPermissions()throws Exception;
	
	PermissionsWithIdDto findPermissionById(Long permissionId) throws Exception;
	
	public void loadDefaultPreviliges(String permissionsFileName)throws Exception;

}
