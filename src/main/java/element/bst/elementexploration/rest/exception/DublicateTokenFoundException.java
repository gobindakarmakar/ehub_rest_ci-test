package element.bst.elementexploration.rest.exception;

/**
 * 
 * @author Amit Patel
 *
 */
public class DublicateTokenFoundException extends RuntimeException{
	 /**
	 * 
	 */
	private static final long serialVersionUID = -1775932834991554116L;

	
		
		private  String message;
		
		public static final String DUBLICATE_TOKEN_FOUND="Dublicate token found";
		
		public DublicateTokenFoundException(String message) {
			super();
			this.message = message;
		}



		public DublicateTokenFoundException() {

			super();
			message = DUBLICATE_TOKEN_FOUND;
		}


		/**
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "DublicateTokenFound [message=" + message + "]";
		}
		
}
