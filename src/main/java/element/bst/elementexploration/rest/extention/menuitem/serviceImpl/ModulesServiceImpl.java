package element.bst.elementexploration.rest.extention.menuitem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.menuitem.dao.ModulesDao;
import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.extention.menuitem.service.ModulesService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Jalagari Paul
 *
 */
@Service("modulesService")
@Transactional("transactionManager")
public class ModulesServiceImpl extends GenericServiceImpl<Modules, Long> implements ModulesService {

	@Autowired
	ModulesDao modulesDao;

	public ModulesServiceImpl() {

	}

	@Autowired
	public ModulesServiceImpl(GenericDao<Modules, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public List<Modules> findAllWithModuleGroups() throws Exception {
		List<Modules> modules = modulesDao.findAll();
		modules.stream().forEach(mod -> Hibernate.initialize(mod.getModuleGroup()));
		return modules;
	}

	@Override
	public List<Modules> getDefaultModules() throws Exception {
		/*List<Modules> modulesList= new ArrayList<>();
		Modules dashboardmodule= new Modules();
		dashboardmodule.setModuleName("Dashboard");
		dashboardmodule.set*/
		return null;
	}
}
