package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.RolesDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupPermissionService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.PrivilegesService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Roles API" })
@RestController
@RequestMapping("/api/roles")
public class RolesController extends BaseController {

	@Autowired
	RolesService rolesService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private SettingsService settingsService;
	
	@Autowired
	private PrivilegesService privilegesService;
	
	@Autowired
	private GroupPermissionService groupPermissionService;
	
	@Autowired
	private GroupsService groupsService;

	@ApiOperation("Saves/updates Role")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.ROLE_ADDED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateRole", consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateRole(@RequestBody @Valid RolesDto rolesDto, BindingResult results,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		GenericReturnObject  message= new GenericReturnObject();
		Long userId = getCurrentUserId();
		boolean isAnythingUpdated=false;
		boolean isUpdated=false;
		if (!results.hasErrors()) {
			if (!StringUtils.isBlank(rolesDto.getRoleName())) {
				if (rolesDto.getRoleId() == null) {
					Roles newRole = new Roles();
					BeanUtils.copyProperties(rolesDto, newRole);
					Roles existingRole = rolesService.findRole(newRole);
					if (existingRole.getRoleName() == null || null==existingRole.getRoleId()) {
						if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow to create manually-Roles")
								.equalsIgnoreCase("On")) {
							newRole.setCreatedOn(new Date());
							newRole.setModifiedOn(new Date());
							if(null==newRole.getIsModifiable())
								newRole.setIsModifiable(true);
							Roles savedRole = rolesService.save(newRole);
							isUpdated = true;
							List<Permissions> allPermissions=privilegesService.findAll();
							Groups analystGroup=groupsService.getGroup(ElementConstants.ANALYST_GROUP_NAME);
							if (allPermissions != null && allPermissions.size() > 0 && null !=analystGroup && savedRole!=null) {
								for (Permissions eachPermission : allPermissions) {
									GroupPermission permission = new GroupPermission();
									//permission.setGroupId(analystGroup);
									permission.setPermissionId(eachPermission);
									permission.setPermissionLevel(2);
									permission.setRoleId(savedRole);
									groupPermissionService.save(permission);
								}
							}
							Users currentUser = usersService.find(userId);
							currentUser = usersService.prepareUserFromFirebase(currentUser);
							AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ROLES_ADD_ACTION,
									null, ElementConstants.ROLES_ADDED);
							Users author = null;
							if (getCurrentUserId() != null){
								author = usersService.find(getCurrentUserId());
								author = usersService.prepareUserFromFirebase(author);
							}
								
							log.setDescription(author.getFirstName() + " " + author.getLastName()
									+ ElementConstants.ROLES_ADDED + newRole.getRoleName());
							log.setUserId(userId);
							log.setName(newRole.getRoleName());
							auditLogService.save(log);
							message.setResponseMessage(ElementConstants.ROLES_ADDED_SUCCESSFULLY);
							message.setData(newRole);
							message.setStatus(ElementConstants.SUCCESS_STATUS);
						/*	if (currentUser != null) {
								List<Long> roleIds = new ArrayList<>();
								roleIds.add(savedRole.getRoleId());
								userRolesService.saveUserRoles(currentUser, roleIds, currentUser.getUserId(),
										ElementConstants.USER_ROLE_ADDED);
							}*/
							return new ResponseEntity<>(message, HttpStatus.OK);
						}else {
							message.setResponseMessage(ElementConstants.ROLE_CREATION_DISABLED);
							message.setStatus(ElementConstants.ERROR_STATUS);
							return new ResponseEntity<>(message, HttpStatus.OK);
						}
					} else {
						message.setResponseMessage("ROLE NAME ALREADY EXISTS");
						message.setStatus(ElementConstants.ERROR_STATUS);
						return new ResponseEntity<>(message, HttpStatus.OK);
					}
				} else {
					boolean isDescriptionChanged = false;
					boolean isNoteChanged = false;
					boolean isRoleNameChanged = false;
					boolean isColorChanged=false;
					boolean isIconChanged=false;
					boolean isSourceChanged=false;
					boolean isModifiabilityChanged=false;
					Roles newRole = new Roles();
					BeanUtils.copyProperties(rolesDto, newRole);
					Roles existingRole = rolesService.find(rolesDto.getRoleId());
					BeanUtils.copyProperties(existingRole, newRole);
					/*
					 * if (existingRole == null) { newRole.setCreatedOn(new Date());
					 * rolesService.saveOrUpdate(newRole); AuditLog log = new AuditLog(new Date(),
					 * LogLevels.INFO, ElementConstants.STRING_TYPE_ROLE, null,
					 * ElementConstants.ROLES_ADDED); Users author = null; if (getCurrentUserId() !=
					 * null) author = usersService.find(getCurrentUserId());
					 * log.setDescription(author.getFirstName() + " " + author.getLastName() +
					 * ElementConstants.ROLES_ADDED + newRole.getRoleName()); log.setUserId(userId);
					 * log.setName(newRole.getRoleName()); auditLogService.save(log);
					 * message.setResponseMessage(ElementConstants.ROLES_ADDED);
					 * message.setData(newRole); message.setStatus(ElementConstants.SUCCESS_STATUS);
					 * return new ResponseEntity<>(message, HttpStatus.OK); }
					 */
					if (existingRole != null) {
						if (settingsService
								.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										"Allow update manually-Roles")
								.equalsIgnoreCase("On")) {
							existingRole.setModifiedOn(new Date());
							if ((null!=newRole.getDescription())&&(!newRole.getDescription().equals(rolesDto.getDescription()))) {
								existingRole.setDescription(rolesDto.getDescription());
								isDescriptionChanged = true;
								isAnythingUpdated = true;
							}
							if ((null!=newRole.getNotes())&&(!newRole.getNotes().equals(rolesDto.getNotes()))) {
								existingRole.setNotes(rolesDto.getNotes());
								isNoteChanged = true;
								isAnythingUpdated = true;
							}
							if((null!=rolesDto.getColor()) &&(!rolesDto.getColor().equals(newRole.getColor()))) {
								existingRole.setColor(rolesDto.getColor());
								isColorChanged=true;
								isAnythingUpdated=true;
							}
							if((null!=rolesDto.getSource()) &&(!rolesDto.getSource().equals(newRole.getSource()))) {
								existingRole.setSource(rolesDto.getSource());
								isSourceChanged=true;
								isAnythingUpdated=true;
							}
							if((null!=rolesDto.getIcon()) &&(!rolesDto.getIcon().equals(newRole.getIcon()))) {
								existingRole.setIcon(rolesDto.getIcon());
								isIconChanged=true;
								isAnythingUpdated=true;
							}
							if(Boolean.compare(rolesDto.getIsModifiable(), newRole.getIsModifiable())!=0) {
								existingRole.setIsModifiable(rolesDto.getIsModifiable());
								isModifiabilityChanged=true;
								isAnythingUpdated=true;
							}
							if (!newRole.getRoleName().equals(rolesDto.getRoleName())) {
								Roles roleWithName = rolesService.findRoleByName(newRole.getRoleName());
								if (roleWithName != null) {
									existingRole.setRoleName(rolesDto.getRoleName());
									rolesService.saveOrUpdate(existingRole);
									isRoleNameChanged = true;
									isUpdated = true;
									isAnythingUpdated = true;
								} else {
									message.setResponseMessage(ElementConstants.ROLE_WITH_NAME_ID_DEOESNT_EXIST);
									message.setStatus(ElementConstants.ERROR_STATUS);
									return new ResponseEntity<>(message, HttpStatus.OK);
								}
							} else {
								existingRole.setRoleName(rolesDto.getRoleName());
								rolesService.saveOrUpdate(existingRole);
								isUpdated = true;
								isRoleNameChanged = true;
								isAnythingUpdated = true;
							}
							
							Users author = null;
							if (getCurrentUserId() != null){
								author = usersService.find(getCurrentUserId());
								author = usersService.prepareUserFromFirebase(author);
							}
								
							String finalAuditString = author.getFirstName() + " " + author.getLastName() + " changed "
									+ newRole.getRoleName() + " role's";
							if (isRoleNameChanged)
								finalAuditString = finalAuditString + " name from " + newRole.getRoleName() + " to "
										+ rolesDto.getRoleName() + ",";
							if (isNoteChanged)
								finalAuditString = finalAuditString + " note from " + newRole.getNotes() + " to "
										+ rolesDto.getNotes() + ",";
							if (isDescriptionChanged)
								finalAuditString = finalAuditString + " description from " + newRole.getDescription()
										+ " to " + rolesDto.getDescription() + ", ";
							if (isColorChanged)
								finalAuditString = finalAuditString + " color from " + newRole.getColor()
										+ " to " + rolesDto.getColor() + " ,";
							if (isIconChanged)
								finalAuditString = finalAuditString + " icon from " + newRole.getIcon()
										+ " to " + rolesDto.getIcon() + ",";
							if (isSourceChanged)
								finalAuditString = finalAuditString + " source from " + newRole.getSource()
										+ " to " + rolesDto.getSource() + " ";
							if(isModifiabilityChanged)
								finalAuditString = finalAuditString + " modifiability from " + newRole.getIsModifiable()
								+ " to " + rolesDto.getIsModifiable() + " ";
							String finalString = "";
							if (!finalAuditString.isEmpty()) {
								String auditDesc = finalAuditString.trim();
								if (auditDesc.endsWith(",")) {
									finalString = auditDesc.substring(0, auditDesc.lastIndexOf(","));
								}
								if (isUpdated && isAnythingUpdated) {
									AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
											ElementConstants.ROLES_UPDATE_ACTION, null,
											ElementConstants.STRING_TYPE_ROLE);
									/*
									 * log.setDescription(author.getFirstName() + " " + author.getLastName() +
									 * ElementConstants.ROLES_UPDATED + newRole.getRoleName());
									 */
									log.setDescription(finalString);
									log.setUserId(userId);
									if (rolesDto.getRoleId() != null)
										log.setTypeId(rolesDto.getRoleId());
									log.setName(newRole.getRoleName());
									auditLogService.save(log);
								}
								message.setResponseMessage(ElementConstants.ROLES_UPDATED_SUCCESSFULLY);
								message.setData(existingRole);
								message.setStatus(ElementConstants.SUCCESS_STATUS);
								return new ResponseEntity<>(message, HttpStatus.OK);
							}
						} else {
							message.setResponseMessage(ElementConstants.ROLE_UPDATION_DISABLED);
							message.setStatus(ElementConstants.ERROR_STATUS);
							return new ResponseEntity<>(message, HttpStatus.OK);
						}
					} else {
						message.setResponseMessage(ElementConstants.ROLE_WITH_ID_SPECIFIED_DOESNT_EXIST);
						message.setStatus(ElementConstants.ERROR_STATUS);
						return new ResponseEntity<>(message, HttpStatus.OK);
					}
				}

			}
			message.setResponseMessage(ElementConstants.ROLE_NAME_EMPTY);
			message.setStatus(ElementConstants.ERROR_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		message.setResponseMessage(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		message.setStatus(ElementConstants.ERROR_STATUS);
		return new ResponseEntity<>(message, HttpStatus.OK);

	}
	
	@ApiOperation("Gets All Roles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllRoles", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllRoles(@RequestParam String token) throws Exception {
		GenericReturnObject  message= new GenericReturnObject();
		List<RolesDto> roles = new ArrayList<RolesDto>();
		//roles = rolesService.findAll();
		roles = rolesService.getAllRoles();
		message.setResponseMessage(ElementConstants.FETCH_ROLES_SUCCESSFUL);
		message.setData(roles);
		message.setStatus(ElementConstants.SUCCESS_STATUS);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Delete the Role by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/deleteRole", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteRole(@RequestParam(required=true) Long roleId, @RequestParam("token") String token,
			HttpServletRequest request) {
		GenericReturnObject  message= new GenericReturnObject();
		
		try{
			if(roleId != null){
				boolean isRoleDeleted=rolesService.deleteRole(roleId, getCurrentUserId());
				if(isRoleDeleted){
					message.setResponseMessage(ElementConstants.DELETE_ROLE_SUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}else{
					message.setResponseMessage(ElementConstants.DELETE_ROLE_UNSUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.ERROR_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
			}else{
				message.setResponseMessage(ElementConstants.EMPTY_ROLEID);
				message.setData(false);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			message.setResponseMessage(ElementConstants.ROLE_NOT_FOUND);
			message.setData(false);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		
		
		//return new ResponseEntity<>(isRoleDeleted,HttpStatus.OK);
	}
	
	@ApiOperation("Gets a Role by Id or Name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getRoleByIdOrName", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getRoleByIdOrName(@RequestParam String token, @RequestParam(required=false) Long roleId,
			@RequestParam(required=false) String roleName) throws Exception {
		RolesDto role = new RolesDto();
		if(roleId != null || roleName != null){
			role = rolesService.getRoleByIdOrName(roleId, roleName);
			return new ResponseEntity<>(role, HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.EMPTY_ROLEID_OR_ROLENAME), HttpStatus.OK);
		}
	}
	
	@ApiOperation("Gets a Role by Id or Name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getStatuswiseUsersByRoleId", produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getStatuswiseUsersByRoleId(@RequestParam String token,
			@RequestParam(required=false) Long roleId) throws Exception {
		if(roleId != null ){
			return new ResponseEntity<>(rolesService.getStatuswiseUsersByRoleId(roleId), HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.EMPTY_ROLEID), HttpStatus.OK);
		}
	}
	
	@ApiOperation("Gets a Role by Id or Name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = List.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/assignUsersToRole", consumes = {"application/json; charset=UTF-8" }, produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> assignUsersToRole(@RequestParam String token,
			@RequestParam(required=false) Long roleId, @RequestBody(required=true) List<Long> userList) throws Exception {
		GenericReturnObject  message= new GenericReturnObject();
		if(roleId != null ){
			if(userList != null && !userList.isEmpty()){
				boolean isAssigned = rolesService.assignUsersToRole(roleId, userList);
				if(isAssigned){
					message.setResponseMessage(ElementConstants.ROLE_ASSIGN_USER_SUCCESSFUL);
					message.setData(isAssigned);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}else{
					message.setResponseMessage(ElementConstants.ROLE_ASSIGN_USER_FAILED);
					message.setData(isAssigned);
					message.setStatus(ElementConstants.ERROR_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
				
			}else{
				message.setResponseMessage(ElementConstants.EMPTY_USER_IDs);
				message.setData(false);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		}else{
			message.setResponseMessage(ElementConstants.EMPTY_ROLEID);
			message.setData(false);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
	}
	
	@ApiOperation("Delete the Role by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/unassignUserFromRole", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> unassignUserFromRole(@RequestParam(required=true) Long roleId,@RequestParam(required=true) Long userId, @RequestParam("token") String token,
			HttpServletRequest request) {
		GenericReturnObject  message= new GenericReturnObject();
		
		try{
			if(roleId != null && userId != null){
				boolean isRoleDeleted=userRolesService.unassignUserFromRole(roleId, userId, getCurrentUserId());
				if(isRoleDeleted){
					message.setResponseMessage(ElementConstants.REMOVE_USER_FROM_ROLE_SUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}else{
					message.setResponseMessage(ElementConstants.REMOVE_USER_FROM_ROLE_UNSUCCESSFUL);
					message.setData(isRoleDeleted);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
			}else{
				message.setResponseMessage(ElementConstants.EMPTY_ROLEID_USERID);
				message.setData(false);
				message.setStatus(ElementConstants.SUCCESS_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			message.setResponseMessage(ElementConstants.ROLE_USER_NOT_FOUND);
			message.setData(false);
			message.setStatus(ElementConstants.ERROR_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		
		
		//return new ResponseEntity<>(isRoleDeleted,HttpStatus.OK);
	}
}
