package element.bst.elementexploration.rest.util;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.entitysearch.service.ClassificationService;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.DataObjectDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCategoryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCredibilityService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceDomainService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceIndustryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceMediaService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesHideStatusService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;

/**
 * @author suresh
 *
 */
@Component
@Transactional("transactionManager")
public class ScheduleSourcesUtility {

	@Autowired
	SourcesService sourcesService;

	@Autowired
	private ClassificationService classificationService;

	@Autowired
	SourceCredibilityService sourceCredibilityService;

	@Autowired
	SourceDomainService sourceDomainService;

	@Autowired
	SourceIndustryService sourceIndustryService;

	@Autowired
	SourceCategoryService sourceCategoryService;
	
	@Autowired
	SourceMediaService sourceMediaService;

	@Autowired
	SourceJurisdictionService sourceJurisdictionService;

	@Autowired
	ListItemService listItemService;
	
	@Autowired
	ObjectMapper mapper;

	@Autowired
	SourcesHideStatusService sourcesHideStatusService;

	@Value("${source_management_general_url_id}")
	private String SOURCE_URL_ID;

	@Value("${source_management_general_url_profiles}")
	private String SOURCE_URL_PROFILES;

	//@Scheduled(cron = "0 0 23 * * SUN")
	public void demoServiceMethod() throws Exception {
			
	Set<String> sourcesSet = sourcesService.fetchClassifctaionSources("GENERAL").stream()
    .map(source -> source.getSourceName()).collect(Collectors.toSet());
		
		Map<String, String> map = new HashMap<String,String>();
		
		Classifications classifications = classificationService.fetchClassification("GENERAL");
		
		List<Sources> listSources = sourcesService.fetchClassifctaionSources("GENERAL");
		String response = "";
		/*Map<String, String> jurisdictionKey = new HashMap<String, String>();
		jurisdictionKey.put("GB", "United Kingdom");
		jurisdictionKey.put("RU", "Russia");
		jurisdictionKey.put("DE", "Germany");
		jurisdictionKey.put("DK", "Denmark");
		jurisdictionKey.put("FR", "France");
		jurisdictionKey.put("ES", "Spain");
		jurisdictionKey.put("US", "United State");
		jurisdictionKey.put("IL", "Israel");
		jurisdictionKey.put("BE", "Belgium");*/
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File fileJSon = new File(classLoader.getResource("newJurisdictions.json").getFile());
		Object fileJsonData = parser.parse(new FileReader(fileJSon));
		JSONObject newJurisdictionsJson= new JSONObject(fileJsonData.toString());
		JSONArray countryJson= newJurisdictionsJson.getJSONArray("data");
		//org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
		//Map<String, String> jurisdictionKey = new HashMap<String, String>();
		for (int k = 0; k < countryJson.length(); k++) {
			//org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
			JSONObject countryObj = countryJson.getJSONObject(k);
			map.put(countryObj.get("Country Code").toString(), countryObj.get("Countries").toString());
		}
	
		for (int k = 1; k <= 2; k++) {
			if (k == 1) {
				//serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeout(SOURCE_URL_ID);
				File fileJson = new File(classLoader.getResource("id_index.json").getFile());
				Object fileJsonDataIdIndex = parser.parse(new FileReader(fileJson));
				response=fileJsonDataIdIndex.toString();
			}
				
			if (k == 2) {
				//	serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeout(SOURCE_URL_PROFILES);
				File fileJson = new File(classLoader.getResource("profile_index.json").getFile());
				Object fileJsonDataIdIndex = parser.parse(new FileReader(fileJson));
				response=fileJsonDataIdIndex.toString();
			}
				
			//if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			//	response = serverResponse[1];
				if (!StringUtils.isEmpty(response) ) {
					try {
						JSONObject json = new JSONObject(response);
						if (json.has("results")) {
							JSONArray resultsArray = json.getJSONArray("results");
							if (resultsArray != null && resultsArray.length() > 0) {
								JSONArray dataArray = resultsArray;
								if (dataArray != null && dataArray.length() > 0) {
									for (int i = 0; i < dataArray.length(); i++) {
										DataObjectDto dataObjectDto = mapper
												.readValue(dataArray.getJSONObject(i).toString(), DataObjectDto.class);

										if (!StringUtils.isBlank(dataObjectDto.getCategory())) {
											String dtoCategory = dataObjectDto.getCategory();
											SourceCategory category = new SourceCategory();
											category.setCategoryName(dtoCategory);
											boolean isCategoryExist = sourceCategoryService
													.checkCategoryExists(dtoCategory);
											if (!isCategoryExist) {
												sourceCategoryService.save(category);
											}
										}

										String sourceName = dataObjectDto.getSourceName();
										/*	String[] urlSplit = dataObjectDto.getApiBaseUrl().split("source=");
										String url = "";
										int m = 1;
										
									 	if (urlSplit.length > 0) {
											for (String urlSave : urlSplit) {
												if (m == 2)
													url = "https://" + urlSave;
												m++;
											}
										} else
										
											url = dataObjectDto.getSourceName();
											*/

										if(sourcesSet != null){
										if (!sourcesSet.contains(sourceName)) {
											Sources sources = new Sources(sourceName, null,
													dataObjectDto.getDisplayName(), "", "", dataObjectDto.getCategory());
											List<Classifications> classificationsList = new ArrayList<Classifications>();
											classificationsList.add(classifications);
											sources.setClassifications(classificationsList);
											List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
											SourceDomain domain = sourceDomainService.fetchDomain("General");
											if (domain != null) {
												domainsList.add(domain);
												sources.setSourceDomain(domainsList);
											}

											List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
											SourceIndustry industry = sourceIndustryService.fetchIndustry("All");
											if (industry != null) {
												industryList.add(industry);
												sources.setSourceIndustry(industryList);
											}

											List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
											SourceMedia media = sourceMediaService.fetchMedia("All");
											if (media != null) {
												mediaList.add(media);
												sources.setSourceMedia(mediaList);
											}

											List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
											List<String> jurisdictionsStringList = dataObjectDto.getJurisdictions();
											for (String jurisdictionString : jurisdictionsStringList) {
												SourceJurisdiction jurisdictionFromDB = sourceJurisdictionService
														.fetchJurisdiction(jurisdictionString);
												if (jurisdictionFromDB != null) {
													jurisdictionList.add(jurisdictionFromDB);
												} else {
													SourceJurisdiction jurisdictionNew = new SourceJurisdiction();
													if (map.containsKey(jurisdictionString)) {
														jurisdictionNew.setSelected(false);
														jurisdictionNew.setJurisdictionName(jurisdictionString);
														jurisdictionNew.setJurisdictionOriginalName(
																map.get(jurisdictionString));
													}else {
														/*jurisdictionNew.setJurisdictionOriginalName(
																map.get(jurisdictionString));*/
														/**If the jurisdictionCode not presented in the newJuridctionList.json we are not adding into the
														 * Jurisdiction and LM_LIST_ITEM tables. 
														 */
														continue;
													}
													//create the jurisdiction in the listItem table also
													ListItemDto listItemDto = new ListItemDto();
													listItemDto.setDisplayName(jurisdictionString);
													listItemDto.setCode(jurisdictionString);
													listItemDto.setListType(ElementConstants.JURISDICTIONS_STRING);
													listItemDto.setAllowDelete(true);
													listItemDto.setColorCode("657f8b");
													listItemDto.setIcon("ban");
													listItemDto = listItemService.saveOrUpdateListItem(listItemDto);

													//add listItemId to sourceJurisdicion table for sync
													jurisdictionNew.setListItemId(listItemDto.getListItemId());
													SourceJurisdiction jurisdictionNewSaved = sourceJurisdictionService
															.create(jurisdictionNew);
													jurisdictionList.add(jurisdictionNewSaved);
												}
											}
											sources.setSourceJurisdiction(jurisdictionList);
											Sources sourcesSaved = sourcesService.save(sources);
											SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false,
													false, false, sourcesSaved.getSourceId(),
													classifications.getClassificationId());
											hideStatus.setHideMedia(false);
											sourcesHideStatusService.save(hideStatus);
											for (SubClassifications subClassifications : classifications
													.getSubClassifications()) {
												SourceCredibility credibility = new SourceCredibility();
												credibility.setCredibility(calculateCredibility(sourceName));
												if (sourceName.equals("BST") && subClassifications
														.getSubClassifcationName().equals("Corporate Structure"))
													credibility.setCredibility(CredibilityEnums.MEDIUM);
												if (sourceName.equals("companyhouse.co.uk") && subClassifications
														.getSubClassifcationName().equals("Corporate Structure"))
													credibility.setCredibility(CredibilityEnums.HIGH);
												credibility.setSources(sourcesSaved);
												credibility.setSubClassifications(subClassifications);
												sourceCredibilityService.save(credibility);
											}
										} // if close for map
										else {
											for(Sources source : listSources) {
												if(source.getSourceName().equals(sourceName) && StringUtils.isBlank(source.getCategory())) {
													source.setCategory(dataObjectDto.getCategory());
													sourcesService.save(source);
													
												}
											}
										}
									}
									}
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
				}
			/*} else {
				throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			}*/

		}
	}
	

	CredibilityEnums calculateCredibility(String sourceName) {
		if (sourceName.contains(".gov"))
			return CredibilityEnums.HIGH;
		else if (sourceName.contains(".edu") || sourceName.contains(".ac") || sourceName.contains(".org"))
			return CredibilityEnums.MEDIUM;
		else if (sourceName.contains(".com.co"))
			return CredibilityEnums.LOW;
		else
			return CredibilityEnums.NONE;
	}
}
