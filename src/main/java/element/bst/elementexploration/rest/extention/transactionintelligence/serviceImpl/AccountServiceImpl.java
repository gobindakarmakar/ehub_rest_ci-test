package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AccountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountOwnershipType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountType;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AccountService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerDetailsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("accountService")
@Transactional("transactionManager")
public class AccountServiceImpl extends GenericServiceImpl<Account, Long> implements AccountService {

	@Autowired
	CustomerDetailsService customerDetailsService;

	private Random random = null;

	public AccountServiceImpl() {
		super();
		this.random = new Random();
	}

	@Autowired
	public AccountServiceImpl(GenericDao<Account, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	AccountDao accountDao;

	@Autowired
	CustomerDetailsDao customerDetailsDao;

	Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

	@Override
	public Boolean fetchAccountDetails(MultipartFile file)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException, Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
		String line = null;
		int i = 1;

		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if (line.contains(","))
					data = line.split(",");
				if (line.contains(";"))
					data = line.split(";");
				if (data == null || data.length == 0)
					break;
				Account account = null;
				boolean isExists = false;
				account = accountDao.fetchAccount(data[0]);
				if (account != null) {
					isExists = true;
				} else {
					account = new Account();
				}
				if (data != null && data[0] != null)
					account.setAccountNumber(data[0]);
				account.setAccountOwnershipType(AccountOwnershipType.valueOf(data[1]));
				account.setPrimaryCustomerIdentifier(data[2]);
				Date date = null;
				String dateTo = data[3];
				if (dateTo.contains("-")) {
					DateFormat formatter = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
					date = formatter.parse(dateTo);
				}
				if (dateTo.contains("/")) {
					DateFormat formatte1 = new SimpleDateFormat("dd/mm/yyyy hh:mm");
					date = formatte1.parse(dateTo);
				}
				account.setAccountOpenDate(date);
				account.setJurisdiction(data[4]);
				account.setSourceSystem(data[5]);
				account.setAccountType(AccountType.valueOf(data[6]));
				account.setAccountName(data[7]);
				account.setSegmentId(data[8]);
				account.setAccountCurrency(data[9]);
				if (data[10].equals("") || data[10] == null)
					account.setMaximumAmount(0.0);
				else
					account.setMaximumAmount(Double.valueOf(data[10]));
				account.setBankName(data[11]);
				account.setRowStatus(false);
				CustomerDetails customerDetails = null;
				if(data[2]!=null || data[2]!="")
				{
					customerDetails = customerDetailsDao.fetchCustomerDetails(data[2]);
				}
				if(customerDetails!=null)
				{
					account.setCustomerDetails(customerDetails);
				}
				try {
					accountDao.saveOrUpdate(account);
				} catch (Exception e) 
				{
					logger.info("Account already exists,So skipping record");
				}
			}
			i++;
		}

		return true;
	}

	@Override
	public boolean checkExistenceOfAccount(List<TxDto> accountIds) {

		return accountDao.checkExistenceOfAccount(accountIds);
	}

	@Override
	public boolean addBankAccount() {
		List<String> bankNames = new ArrayList<String>();
		bankNames.add("Edge Bank Group");
		bankNames.add("First Choice Financial Corp.");
		bankNames.add("Golden Gates Trust Corp.");
		bankNames.add("Grand Market Trust Corp.");
		bankNames.add("Aspire Holdings Inc.");
		bankNames.add("Absolute Bank Group");
		bankNames.add("Apex Holdings");
		bankNames.add("Goldleaf Banks Inc.");

		List<Account> accounts = accountDao.findAll();
		for (Account account : accounts) {
			account.setBankName(getRandomBank(bankNames));
			accountDao.saveOrUpdate(account);
		}

		return false;
	}

	private String getRandomBank(List<String> bank) {
		String bankname = null;
		int index = this.random.nextInt(bank.size());
		bankname = bank.get(index);
		return bankname;
	}

	@Override
	public void updateAccount(AccountDto accountDto) throws Exception {

		if (accountDto != null && accountDto.getId() != 0) {
			Account account = accountDao.find(accountDto.getId());
			if (null != account) {
				if (null != accountDto.getAccountNumber()) {
					account.setAccountNumber(accountDto.getAccountNumber());
				}
				if (null != accountDto.getAccountOwnershipType()) {
					account.setAccountOwnershipType(accountDto.getAccountOwnershipType());
				}
				if (null != accountDto.getPrimaryCustomerIdentifier()) {
					account.setPrimaryCustomerIdentifier(accountDto.getPrimaryCustomerIdentifier());
				}
				if (null != accountDto.getAccountOpenDate()) {
					account.setAccountOpenDate(accountDto.getAccountOpenDate());
				}
				if (null != accountDto.getJurisdiction()) {
					account.setJurisdiction(accountDto.getJurisdiction());
				}
				if (null != accountDto.getSourceSystem()) {
					account.setSourceSystem(accountDto.getSourceSystem());
				}
				if (null != accountDto.getAccountType()) {
					account.setAccountType(accountDto.getAccountType());
				}
				if (null != accountDto.getAccountName()) {
					account.setAccountName(accountDto.getAccountName());
				}
				if (null != accountDto.getSegmentId()) {
					account.setSegmentId(accountDto.getSegmentId());
				}
				if (null != accountDto.getAccountCurrency()) {
					account.setAccountCurrency(accountDto.getAccountCurrency());
				}

				if (accountDto.getCustomerId() != 0) {
					CustomerDetails customerDetails = customerDetailsService.find(accountDto.getCustomerId());
					if (null != customerDetails) {
						account.setCustomerDetails(customerDetails);
					} else {
						throw new Exception("Customer Id is invalid");
					}
				}
				if (null != accountDto.getBankName()) {
					account.setBankName(accountDto.getBankName());
				}
				accountDao.update(account);
			}
		} else {
			throw new Exception("Account Id cannot be empty");
		}

	}

}
