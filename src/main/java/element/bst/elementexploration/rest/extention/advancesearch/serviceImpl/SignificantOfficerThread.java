package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import org.json.JSONArray;
import  org.json.JSONObject;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantnewsService;

public class SignificantOfficerThread extends Thread{
	
private org.json.simple.JSONObject object;
	
	private SignificantnewsService significantnewsService;

	public SignificantOfficerThread(org.json.simple.JSONObject object,SignificantnewsService significantnewsService) {
		this.object=object;
		this.significantnewsService=significantnewsService;
	}
	
	@Override
	public void run() {
		getSignificantOfficers(object,significantnewsService);
	}

	private void getSignificantOfficers(org.json.simple.JSONObject object, SignificantnewsService significantnewsService) {
		
		org.json.simple.JSONArray officership=(org.json.simple.JSONArray) object.get("officership");
		if(officership!=null){
			JSONArray officer_arry=new JSONArray();
		for (int j = 0; j < officership.size(); j++) {
			JSONObject officer_json=new JSONObject();
			org.json.simple.JSONObject officership_temp=(org.json.simple.JSONObject) officership.get(j);
			officer_json.put("role", officership_temp.get("role").toString());
			officer_json.put("keyword", officership_temp.get("keyword").toString());
				org.json.simple.JSONArray profileResponseDto=(org.json.simple.JSONArray) officership_temp.get("profileResponseDto");
				JSONArray profileResponseDto_arry=new JSONArray();
				for (int l = 0; l < profileResponseDto.size(); l++) {
					JSONObject profileResponseDto_json_temp=new JSONObject();
					org.json.simple.JSONObject profileResponseDto_json=(org.json.simple.JSONObject) profileResponseDto.get(l);
					String basic_string=(String) profileResponseDto_json.get("basic");
					JSONObject basic=new JSONObject(basic_string);
					profileResponseDto_json_temp.put("basic", basic.toString());
					profileResponseDto_json_temp.put("watchlist", profileResponseDto_json.get("watchlist"));
					String keyword=basic.getString("vcard:hasName");
					String officer_identifier=(String) basic.get("@identifier");
					String news=(String) profileResponseDto_json.get("news");
					JSONArray news_array=new JSONArray(news);
					JSONArray news_array_temp=new JSONArray();

					for (int m = 0; m < news_array.length(); m++) {
						JSONObject temp_news = news_array.getJSONObject(m);
						String classType=null;
						String published=null;
						String title=null;
						String url=null;
						if(temp_news.get("class")!=null)
							 classType=temp_news.get("class").toString();
						if(temp_news.get("published")!=null)
							published=temp_news.get("published").toString();
						if(temp_news.get("title")!=null)
							title=temp_news.get("title").toString();
						if(temp_news.get("url")!=null)
							url=temp_news.get("url").toString();
						SignificantNews significantNews=significantnewsService.getSignificantNews(officer_identifier, keyword,
								published, title,url,classType);
						if(significantNews!=null){
							temp_news.put("isSignificant", true);
						}
						news_array_temp.put(temp_news);
					}
					profileResponseDto_json_temp.put("news", news_array_temp.toString());
					profileResponseDto_arry.put(profileResponseDto_json_temp);
				}
				officer_json.put("profileResponseDto", profileResponseDto_arry);
				officer_arry.put(officer_json);
		}
		if(officer_arry.length()>0)
			officer_arry.getJSONObject(officer_arry.length()-1).put("flag", "done");

		object.put("officership", officer_arry);
	}
	}

}
