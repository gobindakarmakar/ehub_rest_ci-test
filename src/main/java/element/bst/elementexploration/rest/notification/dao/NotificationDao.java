package element.bst.elementexploration.rest.notification.dao;

import java.time.LocalDateTime;
import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;

/**
 * @author Viswanath
 *
 */
public interface NotificationDao extends GenericDao<NotificationAlert, Long>{

	List<NotificationAlertDTO> retrieveCaseNotificationAlertBasedOnLastCheckIn(Long caseId, Long userId,
			LocalDateTime lastCheckIn, Integer type, Integer level, LocalDateTime dateTo);

	List<NotificationAlertDTO> getUserNotificationList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt,
			Long caseId, Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy,
			LocalDateTime lastCheckIn, String orderIn);

	NotificationAlertDTO retrieveNotificationDetail(Long notifId, Long userId);

	Long countUserNotificationList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId,
			Integer type, LocalDateTime lastCheckIn);

	boolean isEventOwner(Long eventId, Long userId);

	List<EventDTO> getUserEventList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	Long countUserEventList(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId, Integer type);

	List<EventDTO> getEventListByCreator(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	Long countEventListByCreator(Long userId, LocalDateTime dateFromDt, LocalDateTime dateToDt, Long caseId, Integer type);

	EventDetailDTO retrieveEventDetail(Long eventId, Long userId);

}
