package element.bst.elementexploration.rest.extention.docparser.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocumentUploadResponse;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.dto.DocumentDto;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.extention.docparser.dto.CheckPdfTypeDto;
import element.bst.elementexploration.rest.extention.docparser.dto.CoordinatesResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocParserSaveDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentAnswerDto;
import element.bst.elementexploration.rest.extention.docparser.dto.Level1CodeDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TemplatesRepsonseDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentAnswersService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentContentsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentISOCodeService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import element.bst.elementexploration.rest.extention.docparser.service.IsoCategoryService;
import element.bst.elementexploration.rest.extention.docparser.service.PossibleAnswersService;
import element.bst.elementexploration.rest.util.JSONResponseUtil;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "Document Parser API" })
@RestController
@RequestMapping("/api/documentParser")
public class DocumentParserController extends BaseController {
	
	@Autowired
	private DocumentService documentService;

	@Autowired
	DocumentContentsService documentContentsService;

	@Autowired
	private DocumentParserService documentParserService;

	@Autowired
	DocumentAnswersService documentAnswersService;

	@Autowired
	DocumentQuestionsService documentQuestionsService;

	@SuppressWarnings("unused")
	@Autowired
	private DocumentISOCodeService documentISOCodeService;

	@Autowired
	private DocumentTemplatesService documentTemplatesService;

	@Autowired
	private IsoCategoryService isoCategoryService;

	@Autowired
	private PossibleAnswersService possibleAnswersService;

	/*
	 * @ApiOperation("Generates a Json response of the document")
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, response = DOCResponseDto.class, responseContainer =
	 * "List", message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * "Document not found"),
	 * 
	 * @ApiResponse(code = 401, response = ResponseMessage.class, message =
	 * "Permission is denied"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	 * 
	 * @GetMapping(value = "/generateJsonResponse", produces = {
	 * "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	 * public ResponseEntity<?> generateJsonResponse(HttpServletResponse
	 * response, @RequestParam("docId") Long docId,
	 * 
	 * @RequestParam("token") String userId, HttpServletRequest request) throws
	 * DocNotFoundException, Exception { long userIdTemp = getCurrentUserId();
	 * DocumentVault documentVault = documentService.find(docId); if (documentVault
	 * != null) { byte[] fileData = documentService.downloadDocument(docId,
	 * userIdTemp, response); List<DOCResponseDto> responseBody =
	 * documentParserService.exploreOptions(fileData, docId); return new
	 * ResponseEntity<>(responseBody, HttpStatus.OK); } else { throw new
	 * DocNotFoundException(); } }
	 */

	/*
	 * @GetMapping(value = "/saveIntoDatabase", produces = {
	 * "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	 * public ResponseEntity<?> saveIntoDatabase(HttpServletResponse
	 * response, @RequestParam("docId") Long docId,
	 * 
	 * @RequestParam("token") String userId, HttpServletRequest request) throws
	 * DocNotFoundException, Exception { long userIdTemp = getCurrentUserId();
	 * DocumentVault documentVault = documentService.find(docId); if(documentVault
	 * != null) { byte[] fileData = documentService.downloadDocument(docId,
	 * userIdTemp, response); boolean responseBody =
	 * documentParserService.saveIntoDatabase(fileData,docId); return new
	 * ResponseEntity<>(responseBody, HttpStatus.OK); } else{ throw new
	 * DocNotFoundException(); } }
	 */

	/*
	 * @ApiOperation("Audits the document content into database")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, response = boolean.class,
	 * message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * "Document not found"),
	 * 
	 * @ApiResponse(code = 401, response = ResponseMessage.class, message =
	 * "Permission is denied"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	 * 
	 * @GetMapping(value = "/auditDatabase", produces = {
	 * "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	 * public ResponseEntity<?> auditDatabase(HttpServletResponse
	 * response, @RequestParam("docId") Long docId,
	 * 
	 * @RequestParam("token") String userId, HttpServletRequest request) throws
	 * DocNotFoundException, Exception { long userIdTemp = getCurrentUserId();
	 * DocumentVault documentVault = documentService.find(docId); if (documentVault
	 * != null) { byte[] fileData = documentService.downloadDocument(docId,
	 * userIdTemp, response); boolean responseBody =
	 * documentParserService.auditDatabase(fileData, docId); return new
	 * ResponseEntity<>(responseBody, HttpStatus.OK); } else { throw new
	 * DocNotFoundException(); } }
	 */

	/*
	 * @ApiOperation("Testing api for document parsing")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, response = boolean.class,
	 * message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * "Document not found"),
	 * 
	 * @ApiResponse(code = 401, response = ResponseMessage.class, message =
	 * "Permission is denied"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	 * 
	 * @GetMapping(value = "/auditGroups", produces = {
	 * "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	 * public ResponseEntity<?> auditGroups(HttpServletResponse
	 * response, @RequestParam("docId") Long docId,
	 * 
	 * @RequestParam("token") String userId, HttpServletRequest request) throws
	 * DocNotFoundException, Exception { long userIdTemp = getCurrentUserId();
	 * DocumentVault documentVault = documentService.find(docId); if (documentVault
	 * != null) { byte[] fileData = documentService.downloadDocument(docId,
	 * userIdTemp, response); boolean responseBody =
	 * documentParserService.auditGroups(fileData, docId); return new
	 * ResponseEntity<>(responseBody, HttpStatus.OK); } else { throw new
	 * DocNotFoundException(); } }
	 */

	/*
	 * @ApiOperation("Gets Json response of the document that has been uploaded" )
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, response = DocumentResponseDto.class,
	 * responseContainer = "List", message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * "Document not found"),
	 * 
	 * @ApiResponse(code = 401, response = ResponseMessage.class, message =
	 * "Permission is denied"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	 * 
	 * @SuppressWarnings("unused")
	 * 
	 * @GetMapping(value = "/getDocumentJsonResponse", produces = {
	 * "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	 * public ResponseEntity<?> getDocumentJsonResponse(HttpServletResponse
	 * response, @RequestParam("docId") Long docId,
	 * 
	 * @RequestParam("token") String userId, HttpServletRequest request) throws
	 * DocNotFoundException, Exception { long userIdTemp = getCurrentUserId();
	 * DocumentVault documentVault = documentService.find(docId); if (documentVault
	 * != null) { List<DocumentResponseDto> responseBody =
	 * documentParserService.getDocumentContentById(docId); return new
	 * ResponseEntity<>(responseBody, HttpStatus.OK); } else { throw new
	 * DocNotFoundException(); } }
	 */

	@ApiOperation("Gets parsed document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getDocumentParseResponse", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> getDocumentParseResponse(HttpServletResponse response, @RequestParam("docId") Long docId,
			@RequestParam("token") String token, HttpServletRequest request) throws DocNotFoundException, Exception {

		if (docId != null) {
			DocumentVault documentVault = documentService.find(docId);
			if (documentVault != null) {
				List<OverAllResponseDto> responseBody = documentParserService.getDocumentParseData(docId);
				return new ResponseEntity<>(responseBody, HttpStatus.OK);
			} else {
				throw new DocNotFoundException();
			}
		} else {
			throw new BadRequestException(ElementConstants.DOCUMENT_ID_NULL);
		}
	}

	@ApiOperation("Gets parsed document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getTemplateQandA", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> getTemplateQandA(HttpServletResponse response, @RequestParam("tempId") Long tempId,
			@RequestParam("token") String token, HttpServletRequest request) throws DocNotFoundException, Exception {

		DocumentTemplates documentTemplates = documentTemplatesService.find(tempId);
		if (documentTemplates != null) {
			List<OverAllResponseDto> responseBody = documentParserService.getTemplateQandA(tempId);
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}

	@ApiOperation("Updates the document content")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = JSONResponseUtil.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateAnswer", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateAnswer(@Valid @RequestBody DocumentAnswerDto documentAnswers,
			@RequestParam("token") String userId, HttpServletRequest request) throws NoDataFoundException, Exception {
		long userIdTemp = getCurrentUserId();
		DocumentAnswers updatedDocumentAnswer = documentAnswersService.saveNewDocumentAnswer(documentAnswers,
				userIdTemp);

		if (updatedDocumentAnswer != null) {
			JSONResponseUtil json = new JSONResponseUtil();
			json.setId(updatedDocumentAnswer.getId());
			return new ResponseEntity<>(json, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage("Document answer not updated"), HttpStatus.NOT_FOUND);
		}

	}

	/*
	 * @ApiOperation("Saves the ISO codes")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, response = boolean.class,
	 * message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * "Document not found"),
	 * 
	 * @ApiResponse(code = 401, response = ResponseMessage.class, message =
	 * "Permission is denied"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	 * 
	 * @GetMapping(value = "/saveIsoCodes", produces = {
	 * "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	 * public ResponseEntity<?> saveIsoCodes() {
	 * documentISOCodeService.saveIsoCodes(); return new ResponseEntity<>(true,
	 * HttpStatus.OK); }
	 */

	@PostMapping(value = "/uploadDocumentTemplate", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadDocumentTemplate(@RequestParam MultipartFile uploadFile,
			@RequestParam("fileExtensionOfTemplate") String fileExtensionOfTemplate,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {

		return new ResponseEntity<>(documentParserService.saveDocumentTemplates(uploadFile, fileExtensionOfTemplate),
				HttpStatus.OK);

	}

	/*
	 * @PostMapping(value = "/saveDocumentAnswers", consumes = {
	 * "application/json; charset=UTF-8" }, produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * saveDocumentAnswers(HttpServletResponse response, @RequestParam("docId") Long
	 * docId,
	 * 
	 * @RequestParam("templateId") Long templateId,@RequestParam("token") String
	 * userId, HttpServletRequest request) throws DocNotFoundException, Exception {
	 * long userIdTemp = getCurrentUserId(); DocumentVault documentVault =
	 * documentService.find(docId); if (documentVault != null) { if
	 * (documentVault.getDocumentParsingStatus().equals("uploaded")) { byte[]
	 * fileData = documentService.downloadDocument(docId, userIdTemp, response);
	 * documentParserService.auditDatabase(fileData, docId);
	 * documentParserService.setRiskScoreToQuestions(docId);
	 * //documentParserService.saveDocumetnAnswers(fileData,userIdTemp, templateId);
	 * DocumentDto documentDto = new DocumentDto();
	 * documentDto.setDocumentParsingStatus("completed");
	 * documentService.updateDocumentById(documentDto, documentVault.getDocId(),
	 * getCurrentUserId()); } //List<OverAllResponseDto> responseBody =
	 * documentParserService.getDocumentParseData(docId); //return new
	 * ResponseEntity<>(responseBody, HttpStatus.OK); return new
	 * ResponseEntity<>("Answers saved into database", HttpStatus.OK); } else {
	 * throw new DocNotFoundException(); } }
	 */
	@ApiOperation("Updates the document answer evaluation")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Answer not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateEvaluation", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateEvaluation(@RequestParam("answerId") Long answerId,
			@RequestParam("Evaluation") String evaluation, @RequestParam("token") String token,
			HttpServletRequest request, HttpServletResponse response) {
		DocumentAnswers documentAnswer = documentAnswersService.find(answerId);
		long userIdTemp = getCurrentUserId();
		if (documentAnswer != null) {
			int status = documentAnswersService.updateEvaluation(documentAnswer, evaluation, userIdTemp);
			if (status == 201) {
				Level1CodeDto level1CodeDto = isoCategoryService.updateIsoCategoryEvaluationByAnswer(documentAnswer,
						userIdTemp);
				return new ResponseEntity<>(level1CodeDto, HttpStatus.OK);
			} else
				return new ResponseEntity<>(new ResponseMessage("Evaluation value not updated"),
						HttpStatus.INTERNAL_SERVER_ERROR);
		} else
			return new ResponseEntity<>(new ResponseMessage("Answer not found"), HttpStatus.NOT_FOUND);
	}

	@ApiOperation("Updates the document answer ranking")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Answer not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateAnswerRank", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateAnswerRank(@RequestParam("answerId") Long answerId,
			@RequestParam("Ranking") Integer rank, @RequestParam("token") String token, HttpServletRequest request,
			HttpServletResponse response) {
		DocumentAnswers documentAnswer = documentAnswersService.find(answerId);
		if (documentAnswer != null) {
			long userIdTemp = getCurrentUserId();
			int status = documentAnswersService.updateAnswerRanking(documentAnswer, rank, userIdTemp);
			if (status == 201) {
				return new ResponseEntity<>(new ResponseMessage("Answer ranking updated successfully"), HttpStatus.OK);
			} else
				return new ResponseEntity<>(new ResponseMessage("Ranking value not updated"),
						HttpStatus.INTERNAL_SERVER_ERROR);
		} else
			return new ResponseEntity<>(new ResponseMessage("Answer not found"), HttpStatus.NOT_FOUND);
	}

	@ApiOperation("Gets updated iso codes of parsed document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = OverAllResponseDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUpdatedISODocumentParseResponse", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUpdatedISODocumentParseResponse(HttpServletResponse response,
			@RequestParam("docId") Long docId, @RequestParam("token") String token, HttpServletRequest request)
			throws DocNotFoundException, Exception {
		DocumentVault documentVault = documentService.find(docId);
		if (documentVault != null) {
			boolean status = documentParserService.getUpdatedISODocumentParseData(docId);
			if (status)
				return new ResponseEntity<>(documentParserService.getDocumentParseData(docId), HttpStatus.OK);
			else
				return new ResponseEntity<>(new ResponseMessage("Error refreshing data"),
						HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			throw new DocNotFoundException();
		}
	}

	@ApiOperation("Updates the document iso category evaluation")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Answer not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateIsoCategoryEvaluation", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateIsoCategoryEvaluation(@RequestParam("categoryId") Long isoCategoryId,
			@RequestParam("Evaluation") String evaluation, @RequestParam("token") String token,
			HttpServletRequest request, HttpServletResponse response) {
		IsoCategory isoCategory = isoCategoryService.find(isoCategoryId);
		long userIdTemp = getCurrentUserId();
		if (isoCategory != null) {
			int status = isoCategoryService.updateIsoCategoryEvaluation(isoCategory, evaluation, userIdTemp);
			if (status == 201) {
				isoCategoryService.storeIsoCategoryEvaluation(isoCategory.getDocId());
				return new ResponseEntity<>(new ResponseMessage("IsoCategory evaluation updated successfully"),
						HttpStatus.OK);
			} else
				return new ResponseEntity<>(new ResponseMessage("Evaluation value not updated"),
						HttpStatus.INTERNAL_SERVER_ERROR);
		} else
			return new ResponseEntity<>(new ResponseMessage("IsoCategory not found"), HttpStatus.NOT_FOUND);
	}

	@ApiOperation("Updates has iso code in document questions")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateHasIsoCodeOfQuestions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateHasIsoCode(@RequestParam("token") String token, HttpServletRequest request,
			HttpServletResponse response) {
		List<DocumentQuestions> questionsList = documentQuestionsService.findAll();
		for (DocumentQuestions documentQuestions : questionsList) {
			if (documentQuestions.getPrimaryIsoCode() != null)
				documentQuestions.setHasIsoCode(true);
			else
				documentQuestions.setHasIsoCode(false);
			documentQuestionsService.saveOrUpdate(documentQuestions);
		}
		return new ResponseEntity<>(new ResponseMessage("Has iso code flag updated"), HttpStatus.OK);
	}

	@PostMapping(value = "/getQuestionAndAnswerNewTemplate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getQuestionAndAnswerNewTemplate(@RequestBody CoordinatesResponseDto coordinatesDto,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		byte[] fileData = documentService.downloadDocumentDocParer(coordinatesDto.getDocId(), getCurrentUserId(), response);
		return new ResponseEntity<>(documentTemplatesService.getQuestionAndAnswerNewTemplate(coordinatesDto, fileData),
				HttpStatus.OK);
	}

	@PostMapping(value = "/updateTemplateQuestions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateTemplateQuestions(@RequestBody OverAllResponseDto repsonseDto,
			@RequestParam("token") String token, @RequestParam(required = true) Long tempId, HttpServletRequest request,
			HttpServletResponse response) throws DocNotFoundException, Exception {
		boolean value = documentTemplatesService.updateTemplateQuestions(repsonseDto, tempId, getCurrentUserId());
		if (value) {
			return new ResponseEntity<>(documentParserService.getTemplateQandA(tempId), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage("Failed to update the template"), HttpStatus.OK);
		}

	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/getTemplates", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getTemplates(@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer pageNumber,
			HttpServletRequest request, @RequestParam(required = false) String searchKey, HttpServletResponse response)
			throws DocNotFoundException, Exception {

		List<TemplatesRepsonseDto> templatesRepsonseDtos = documentTemplatesService
				.getTemplatesPagination(recordsPerPage, pageNumber, true, searchKey);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = documentTemplatesService.getTemplates(false, searchKey);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(templatesRepsonseDtos != null ? templatesRepsonseDtos.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("templatesRepsonseDtos", templatesRepsonseDtos);
		jsonObject.put("information", information);

		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}

	@PostMapping(value = "/updateTemplateName", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateTemplateName(@RequestBody TemplatesRepsonseDto repsonseDto,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		return new ResponseEntity<>(documentTemplatesService.updateTemplateName(repsonseDto, getCurrentUserId()),
				HttpStatus.OK);
	}

	@PostMapping(value = "/createNewTemplate", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createNewTemplate(@RequestBody DocParserSaveDto repsonseDto,
			@RequestParam("token") String token, @RequestParam(required = true) Long docId,
			@RequestParam(required = true) Integer docFlag, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		byte[] byteArray = documentService.downloadDocumentDocParer(docId, getCurrentUserId(), response);
		if (byteArray != null) {
			long tempId = documentTemplatesService.saveNewPdfFillableTemplate(repsonseDto, docId, getCurrentUserId(),
					docFlag, byteArray);
			List<OverAllResponseDto> responseBody = documentParserService.getTemplateQandA(tempId);
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage("Document not found"), HttpStatus.OK);
		}
	}

	@GetMapping(value = "/deleteTemplate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteTemplate(@RequestParam(required = true) Long templateId,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		long userIdTemp = getCurrentUserId();
		return new ResponseEntity<>(documentTemplatesService.deleteTemplate(templateId, userIdTemp), HttpStatus.OK);
	}

	@GetMapping(value = "/editTemplate", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> editTemplate(@RequestParam(required = true) Long templateId,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		return new ResponseEntity<>(documentParserService.getTemplateQandA(templateId), HttpStatus.OK);
	}

	@GetMapping(value = "/deleteQuestion", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteQuestion(@RequestParam(required = true) Long questionId,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		long userIdTemp = getCurrentUserId();
		return new ResponseEntity<>(documentQuestionsService.deleteQuestion(questionId, userIdTemp), HttpStatus.OK);
	}

	@GetMapping(value = "/deleteAnswerOptions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteAnswerOptions(@RequestParam(required = true) Long possibleAnswerId,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {
		long userIdTemp = getCurrentUserId();
		return new ResponseEntity<>(possibleAnswersService.deleteAnswerOptions(possibleAnswerId, userIdTemp),
				HttpStatus.OK);
	}

	@PostMapping(value = "/existTemplate", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> existTemplate(@RequestParam MultipartFile uploadFile, @RequestParam("token") String token,
			HttpServletRequest request, HttpServletResponse response) throws DocNotFoundException, Exception {
		DocumentTemplates documentTemplates = documentParserService.compareFileWithDocumentTemplate(uploadFile, null,
				getCurrentUserId(), null);
		if (documentTemplates != null) {
			// return new ResponseEntity<>(new ResponseMessage("Template already exists for
			// this uploaded file"), HttpStatus.OK);
			CaseDocumentUploadResponse caseDocumentUploadResponse = new CaseDocumentUploadResponse();
			if (documentTemplates.getInitialDocId() != null)
				caseDocumentUploadResponse.setDocId(documentTemplates.getInitialDocId());
			caseDocumentUploadResponse.setTemplateId(documentTemplates.getId());
			caseDocumentUploadResponse.setTemplateName(documentTemplates.getTemplateName());
			return new ResponseEntity<>(caseDocumentUploadResponse, HttpStatus.OK);
		} else {
			Long docId = documentService.uploadDocument(uploadFile, "", "", getCurrentUserId(), 10, null,null,null,null,null,null);
			DocumentDto documentDto = new DocumentDto();
			CheckPdfTypeDto pdfType = new CheckPdfTypeDto();
			byte[] byteArray = documentService.downloadDocumentDocParer(docId, getCurrentUserId(), response);
			if (byteArray != null)
				pdfType = documentParserService.checkPdfType(byteArray);
			if (pdfType.isFillablePdf() == true)
				documentDto.setDocStructureType(1);// for fillable pdf
			else if (pdfType.isNormalPdf() == true)
				documentDto.setDocStructureType(2);// for normal pdf
			else if (pdfType.isScannedPdf() == true)
				documentDto.setDocStructureType(3);// for scanned pdf
			documentService.updateDocumentById(documentDto, docId, getCurrentUserId());
			CaseDocumentUploadResponse caseDocumentUploadResponse = new CaseDocumentUploadResponse();
			caseDocumentUploadResponse.setDocId(docId);
			return new ResponseEntity<>(caseDocumentUploadResponse, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/checkPdfType", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> checkPdfType(@RequestParam(required = true) Long docId,
			@RequestParam("token") String token, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		byte[] byteArray = documentService.downloadDocumentDocParer(docId, getCurrentUserId(), response);
		if (byteArray != null)
			return new ResponseEntity<>(documentParserService.checkPdfType(byteArray), HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("Document no found"), HttpStatus.OK);
	}

	@ApiOperation("Download Document From DocParser")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	@CrossOrigin(origins = "*", methods = RequestMethod.GET)
	@GetMapping(value = "/downloadDocumentDocParser", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<StreamingResponseBody> downloadDocumentDocParser(HttpServletResponse response,
			@RequestParam("docId") Long docId, @RequestParam("token") String userId, HttpServletRequest request)
			throws DocNotFoundException, Exception {
		long userIdTemp = getCurrentUserId();
		DocumentVault documentVault = documentService.find(docId);
		if (documentVault != null) {
			byte[] fileData = documentParserService.downloadDocumentDocParser(docId, userIdTemp, response);
			ServiceCallHelper.setDownloadResponse(response, documentVault.getDocName(), documentVault.getType(),
					documentVault.getSize() != null ? documentVault.getSize().intValue() : 0);
			StreamingResponseBody responseBody = new StreamingResponseBody() {
				@Override
				public void writeTo(OutputStream out) throws IOException {
					out.write(fileData);
					out.flush();
				}
			};
			request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}

	@ApiOperation("Get documnet details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = DocumentVaultDTO.class, message = "Operation Successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getDocumentDetailsDocParser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDocumentDetailsDocParser(@RequestParam Long docId, @RequestParam("token") String userId,
			HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {

		long userIdTemp = getCurrentUserId();
		DocumentVaultDTO documentDetails = documentParserService.getDocumentDetailsDocParser(docId, userIdTemp);
		if (documentDetails.getDocumentParsingStatus().equals("completed")) {
			documentDetails.setNoOfQuestions(documentQuestionsService.findQuestions(documentDetails.getDocId()));
			documentDetails.setHasConflicts(false);// static
		}
		return new ResponseEntity<>(documentDetails, HttpStatus.OK);
	}

	/*
	 * @PostMapping(value = "/saveTemplateFile", produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * saveTemplateFile(@RequestParam(required = true) Long docId,
	 * 
	 * @RequestParam("token") String token, HttpServletRequest request,
	 * HttpServletResponse response) throws DocNotFoundException, Exception { long
	 * userIdTemp = getCurrentUserId(); if (docId != null && docId != 0) {
	 * DocumentVault documentVault = documentService.find(docId); if (documentVault
	 * != null) { String path =
	 * documentTemplatesService.saveTemplateFile(documentVault,
	 * userIdTemp,response); if (path != null) return new ResponseEntity<>(new
	 * ResponseMessage(path), HttpStatus.OK); } else { return new
	 * ResponseEntity<>(new ResponseMessage("Document not found"),
	 * HttpStatus.NOT_FOUND); } } else { return new ResponseEntity<>(new
	 * ResponseMessage("Invalid document Id"), HttpStatus.BAD_REQUEST); }
	 * 
	 * return new ResponseEntity<>(new ResponseMessage(""), HttpStatus.OK); }
	 * 
	 * @DeleteMapping(value = "/deleteTemplateFile", produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * deleteTemplateFile(@RequestParam(required = true) Long docId,
	 * 
	 * @RequestParam("token") String token, HttpServletRequest request,
	 * HttpServletResponse response) throws DocNotFoundException, Exception { if
	 * (docId != null && docId != 0) { DocumentVault documentVault =
	 * documentService.find(docId); if (documentVault != null) { boolean status =
	 * documentTemplatesService.deleteTemplateFile(documentVault); if (status)
	 * return new ResponseEntity<>(new ResponseMessage("File deleted successfully"),
	 * HttpStatus.OK); else return new ResponseEntity<>(new
	 * ResponseMessage("File not deleted"), HttpStatus.BAD_REQUEST); } else { return
	 * new ResponseEntity<>(new ResponseMessage("Document not found"),
	 * HttpStatus.NOT_FOUND); } } else { return new ResponseEntity<>(new
	 * ResponseMessage("Invalid document Id"), HttpStatus.BAD_REQUEST); }
	 * 
	 * }
	 */
}
