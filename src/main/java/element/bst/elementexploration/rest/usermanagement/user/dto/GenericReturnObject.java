package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;

public class GenericReturnObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String responseMessage;

	private String status;

	private Object data;

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
