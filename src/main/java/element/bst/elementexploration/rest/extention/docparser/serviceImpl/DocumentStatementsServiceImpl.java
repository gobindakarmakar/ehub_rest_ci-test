package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentStatements;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentStatementsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("documentStatementsService")
public class DocumentStatementsServiceImpl extends GenericServiceImpl<DocumentStatements, Long>
		implements DocumentStatementsService {
	@Autowired
	public DocumentStatementsServiceImpl(GenericDao<DocumentStatements, Long> genericDao) {
		super(genericDao);
	}

}
