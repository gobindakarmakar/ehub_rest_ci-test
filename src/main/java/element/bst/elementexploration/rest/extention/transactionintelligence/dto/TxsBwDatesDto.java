package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class TxsBwDatesDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long alertsGenerated;
	private long alertRatio;

	public TxsBwDatesDto() {
	}


	public TxsBwDatesDto(long alertsGenerated, long alertRatio) {
		super();
		this.alertsGenerated = alertsGenerated;
		this.alertRatio = alertRatio;
	}


	public long getAlertsGenerated() {
		return alertsGenerated;
	}

	public void setAlertsGenerated(long alertsGenerated) {
		this.alertsGenerated = alertsGenerated;
	}

	public long getAlertRatio() {
		return alertRatio;
	}

	public void setAlertRatio(long alertRatio) {
		this.alertRatio = alertRatio;
	}

}
