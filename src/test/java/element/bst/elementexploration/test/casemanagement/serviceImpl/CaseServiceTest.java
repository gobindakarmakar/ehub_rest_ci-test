package element.bst.elementexploration.test.casemanagement.serviceImpl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;

import element.bst.elementexploration.rest.casemanagement.bigdata.domain.CaseSource;
import element.bst.elementexploration.rest.casemanagement.bigdata.domain.Source;
import element.bst.elementexploration.rest.casemanagement.dao.CaseAnalystMappingDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.Answer;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.serviceImpl.CaseServiceImpl;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;



/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CaseServiceTest {
	
	@Mock
	CaseDao caseDaoMock;
	
	@Mock
	UsersDao userDaoMock;
	
	@Mock
	DocumentDao documentDaoMock;
	
	@Mock
	UsersDao userDao;
	
	@Mock
	CaseAnalystMappingDao caseAnalystMappingDao;
	
	@InjectMocks
	CaseServiceImpl caseService;
	
	public List<CaseDto> caseDtoList = new ArrayList<CaseDto>();
	
	public CaseSource caseSource;
	
	public Users user;	
	
	JsonObject caseAnalysis = null;
	
	CaseVo caseVo = new CaseVo();
	
	public List<CaseAggregator> caseAggregatorList;
	
	public List<Answer> answersList;
	
	public List<Long> caseIdsList;
	
	public CaseMapDocIdDto caseMapDocIdDto;
	
	List<CaseUnderwritingDto> caseUnderwritingDtoList;
	
	public List<Users> userList;
	
	public CaseAggregatorDto caseAggregatorDto;
	
	public Case caseSeed = new Case(); 
	
	public List<CaseAnalystMapping> caseAnalystMappingList;
	
	public CaseAnalystMapping mapping;
	
	public String[] serverResponse = new String[2];
	
	public DocumentVault documentVault;
	
	@Before
    public void setupMock() {
		CaseDto caseDto = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDto);
		CaseDto caseDtonNew = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDtonNew);
		ArrayList<Source> websites = new ArrayList<Source>();
		Source source = new Source("10.10.10.10","testHost");
		Source source_new = new Source("10.10.10.15","test Host");
		websites.add(source);
		websites.add(source_new);
		caseSource = new CaseSource();
		caseSource.setWebsites(websites);
		user=new Users("analyst","Analyst","Analyst",new Date(),null);
		caseAnalysis = new JsonObject();
		caseAnalysis.addProperty(StatusEnum.FRESH.getName(), 3);
		caseAnalysis.addProperty(StatusEnum.SUBMITTED.getName(), 4);
		caseAnalysis.addProperty(StatusEnum.ACKNOWLEDGE.getName(), 2);
		caseAnalysis.addProperty(StatusEnum.ACCEPTED.getName(), 4);
		caseAggregatorList = new ArrayList<CaseAggregator>();
		CaseAggregator caseAggregator = new CaseAggregator("testing period", 2L, 3L, 1L, 3L,1L, 2L, 0L, 0L);
		caseAggregatorList.add(caseAggregator);
		CaseAggregator caseAggregatorTest = new CaseAggregator("testing period", 2L, 3L, 1L, 3L,1L, 2L, 0L, 0L);
		caseAggregatorList.add(caseAggregatorTest);
		answersList = new ArrayList<Answer>();
		Answer answer= new Answer();
		answersList.add(answer);
		Answer answer_new = new Answer();
		answersList.add(answer_new);
		caseIdsList = new ArrayList<Long>();
		caseIdsList.add(123L);
		caseIdsList.add(124L);
		caseIdsList.add(125L);
		caseMapDocIdDto = new CaseMapDocIdDto();
		caseMapDocIdDto.setDocId(123L);
		caseUnderwritingDtoList = new ArrayList<CaseUnderwritingDto>();
		CaseUnderwritingDto caseUnderwritingDto = new CaseUnderwritingDto(123L,"test case",12L,4);
		caseUnderwritingDtoList.add(caseUnderwritingDto);
		CaseUnderwritingDto caseUnderwritingDtoNew = new CaseUnderwritingDto(124L,"test case",13L,3);
		caseUnderwritingDtoList.add(caseUnderwritingDtoNew);
		userList = new ArrayList<Users>();
		userList.add(user);
		userList.add(new Users("admin","Admin","Admin",new Date(),null));
		caseVo.setKeyword("test");
		caseAggregatorDto = new CaseAggregatorDto();
		caseAggregatorDto.setGranularity("test");
		caseAnalystMappingList = new ArrayList<CaseAnalystMapping>();
		caseAnalystMappingList.add(new CaseAnalystMapping(1L, 2, new Date(),new Date(), "testing",new Date(), null,user));
		caseAnalystMappingList.add(new CaseAnalystMapping(1L, 2, new Date(),new Date(), "testing-case",new Date(), null,user));
		caseAnalystMappingList.add(new CaseAnalystMapping(1L, 3, new Date(),new Date(), "junit",new Date(), null,user));
		caseAnalystMappingList.add(new CaseAnalystMapping(1L, 4, new Date(),new Date(), "case test",new Date(), null,user));
		serverResponse[0] = "200";
		serverResponse[1] = "response from server";
		documentVault = new DocumentVault(1L, "Junit", "Junit test case", "", "Document", 5L,new Date());
		mapping = new CaseAnalystMapping(1L, 2, new Date(),new Date(), "testing-case",new Date(), null,user);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void isCaseAccessibleOrOwnerTest() {
		Mockito.when(caseDaoMock.isCaseAccessibleOrOwner(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		boolean status = caseService.isCaseAccessibleOrOwner(1L,1L);
		assertEquals(true,status);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = InsufficientDataException.class)
	public void isCaseAccessibleOrOwner_WhenCaseIdOrUserIdNullTest() {
		Mockito.when(caseDaoMock.isCaseAccessibleOrOwner(isNull(Long.class),isNull(Long.class))).thenThrow(InsufficientDataException.class);
		 caseService.isCaseAccessibleOrOwner(null,null);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PermissionDeniedException.class)
	public void isCaseAccessibleOrOwner_WhenExceptionThrowTest() {
		Mockito.when(caseDaoMock.isCaseAccessibleOrOwner(Mockito.anyLong(),Mockito.anyLong())).thenThrow(PermissionDeniedException.class);
		 caseService.isCaseAccessibleOrOwner(1L,1L);
	}	
	
	@Test
	public void fullTextSearchForCaseSeedTest() throws ParseException{
		Mockito.when(caseDaoMock.fullTextSearchForCaseSeed(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.fullTextSearchForCaseSeed("test",1,10,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),1L,"Individual-KYC",3,1,"name","desc");
		assertEquals(2,caseDtoListResult.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForCaseSeed_WhenCreationDateFormat_ExceptionThrowTest() throws ParseException{
		Mockito.when(caseDaoMock.fullTextSearchForCaseSeed(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenThrow(DateFormatException.class);
		caseService.fullTextSearchForCaseSeed("test",1,10,new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),1L,"Individual-KYC",3,1,"name","desc");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForCaseSeed_WhenModificationDateFormat_ExceptionThrowTest() throws ParseException{
		Mockito.when(caseDaoMock.fullTextSearchForCaseSeed(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenThrow(DateFormatException.class);
		caseService.fullTextSearchForCaseSeed("test",1,10,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString(),1L,"Individual-KYC",3,1,"name","desc");
	}
	
	@Test
	public void countFullTextSearchForCaseSeedTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForCaseSeed(Mockito.anyString(),Mockito.anyLong(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(2L);
		long count = caseService.countFullTextSearchForCaseSeed("test",1L,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"Individual-KYC",3,1);
		assertEquals(2L,count);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countFullTextSearchForCaseSeed_WhenCreationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForCaseSeed(Mockito.anyString(),Mockito.anyLong(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenThrow(DateFormatException.class);
		caseService.countFullTextSearchForCaseSeed("test",1L,new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"Individual-KYC",3,1);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countFullTextSearchForCaseSeed_WhenModificationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForCaseSeed(Mockito.anyString(),Mockito.anyLong(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenThrow(DateFormatException.class);
		caseService.countFullTextSearchForCaseSeed("test",1L,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString(),"Individual-KYC",3,1);
		
	}
	
	@Test
	public void getAllCasesByUserTest() {
		Mockito.when(caseDaoMock.getAllCasesByUser(Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.getAllCasesByUser(1L,1,5,"Individual-KYC",4,1,"name","asc");
		assertEquals(2,caseDtoListResult.size());
	}
	
	@Test(expected = InsufficientDataException.class)
	public void mapUserWithCase_WhenUploadFileOrbstSeedIdNull_ExceptionThrowTest() throws Exception {		
		caseService.mapUserWithCase(null,null,1L,1L);		
	}
	
	@Test(expected = PermissionDeniedException.class)
	public void mapUserWithCase_WhenCaseIsNotAccessable_ExceptionThrowTest() throws Exception {		
		MultipartFile multipartFile = new MockMultipartFile("data", "other-file-name.data", "text/plain", "some other type".getBytes());
		Mockito.when(caseDaoMock.isCaseAccessibleOrOwner(Mockito.anyLong(),Mockito.anyLong())).thenReturn(false);
		caseService.mapUserWithCase(multipartFile,2L,1L,1L);		
	}
	
	@Test(expected = NoDataFoundException.class)
	public void mapUserWithCase_WhenCaseIsNull_ExceptionThrowTest() throws Exception {		
		MultipartFile multipartFile = new MockMultipartFile("data", "other-file-name.data", "text/plain", "some other type".getBytes());
		Mockito.when(caseDaoMock.isCaseAccessibleOrOwner(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(null);
		caseService.mapUserWithCase(multipartFile,2L,1L,1L);		
	}
	
	/*@Test(expected = FailedToExecuteQueryException.class)
	public void mapUserWithCase_WhenFailedToExecuteQuery_ExceptionThrowTest() throws Exception  {		
		MultipartFile multipartFile = new MockMultipartFile("data", "other-file-name.data", "text/plain", "some other type".getBytes());
		Mockito.when(caseDaoMock.isCaseAccessibleOrOwner(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(userDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(null);
		Mockito.when(caseDaoMock.isAnalystRejectedCaseBefore(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		Mockito.when(caseService.getNextAnalyst(Mockito.anyLong())).thenReturn(null);
		caseService.mapUserWithCase(multipartFile,2L,1L,1L);		
	}*/	
	
	
	@Test
	public void getAllIncomingTrayBasedOnUserIdTest() {
		Mockito.when(caseDaoMock.getAllIncomingtrayBasedOnUserId(Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.getAllIncomingTrayBasedOnUserId(1L,1,10,"name","desc");
		assertEquals(2,caseDtoListResult.size());
	}
	
	@Test
	public void countgetAllIncomingTrayTest()  {
		Mockito.when(caseDaoMock.countgetAllIncomingTray(Mockito.anyLong())).thenReturn(2L);
		long count = caseService.countgetAllIncomingTray(Mockito.anyLong());
		assertEquals(2L,count);
	}
	
	@Test
	public void fullTextSearchForIncomingtrayTest() throws ParseException {
		Mockito.when(caseDaoMock.fullTextSearchForIncomingtray(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.fullTextSearchForIncomingtray(1L,"test",1,9,new SimpleDateFormat("dd-MM-yyyy").format(new Date()) ,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"name","desc");
		assertEquals(2,caseDtoListResult.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForIncomingtray_WhenCreationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.fullTextSearchForIncomingtray(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenThrow(DateFormatException.class);
		caseService.fullTextSearchForIncomingtray(1L,"test",1,9,new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"name","desc");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForIncomingtray_WhenModificationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.fullTextSearchForIncomingtray(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenThrow(DateFormatException.class);
		caseService.fullTextSearchForIncomingtray(1L,"test",1,9,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString(),"name","desc");
	}
	
	@Test
	public void countFullTextSearchForIncomingtrayTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForIncomingtray(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class),Mockito.any(Date.class))).thenReturn(2L);
		long count = caseService.countFullTextSearchForIncomingtray(1L,"test",new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2L,count);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countFullTextSearchForIncomingtray_WhenCreationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForIncomingtray(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class),Mockito.any(Date.class))).thenThrow(DateFormatException.class);
		caseService.countFullTextSearchForIncomingtray(1L,"test",new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countFullTextSearchForIncomingtray_WhenModificationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForIncomingtray(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class),Mockito.any(Date.class))).thenThrow(DateFormatException.class);
		caseService.countFullTextSearchForIncomingtray(1L,"test",new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString());
		
	}
	
	@Test
	public void getAllMyCasesBasedOnUserIdTest()  {
		Mockito.when(caseDaoMock.getAllMyCasesBasedOnUserId(Mockito.anyLong(),Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.getAllMyCasesBasedOnUserId(1L,"test","desc",1,10);
		assertEquals(2,caseDtoListResult.size());
	}
	
	
	@Test
	public void countgetAllMyCasesTest()  {
		Mockito.when(caseDaoMock.countgetAllMyCases(Mockito.anyLong())).thenReturn(2L);
		Long count = caseService.countgetAllMyCases(1L);
		assertEquals(new Long("2"),count);
	}
	
	@Test
	public void fullTextSearchForMyCasesTest() throws ParseException {
		Mockito.when(caseDaoMock.fullTextSearchForMyCases(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.fullTextSearchForMyCases(1L,"test",1,15,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"name","desc");
		assertEquals(2,caseDtoListResult.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForMyCases_whenCreationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.fullTextSearchForMyCases(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenThrow(DateFormatException.class);
		caseService.fullTextSearchForMyCases(1L,"test",1,15,new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"name","desc");
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForMyCases_whenModificationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.fullTextSearchForMyCases(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenThrow(DateFormatException.class);
		caseService.fullTextSearchForMyCases(1L,"test",1,15,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString(),"name","desc");
		
	}
	
	@Test
	public void countFullTextSearchForMyCasesTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForMyCases(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class),Mockito.any(Date.class))).thenReturn(2L);
		long count = caseService.countFullTextSearchForMyCases(1L,"test",new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2L,count);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countFullTextSearchForMyCases_WhenCreationDateForamt_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForMyCases(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class),Mockito.any(Date.class))).thenThrow(DateFormatException.class);
		caseService.countFullTextSearchForMyCases(1L,"test",new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countFullTextSearchForMyCases_WhenModificationDateForamt_ExceptionThrowTest() throws ParseException {
		Mockito.when(caseDaoMock.countFullTextSearchForMyCases(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class),Mockito.any(Date.class))).thenThrow(DateFormatException.class);
		caseService.countFullTextSearchForMyCases(1L,"test",new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString());
	}
	
	
	/*@Test
	public void getRelatedCasesByIdTest(){
		Mockito.when(caseDaoMock.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.mock(ServiceCallHelper.class);
		Mockito.when(ServiceCallHelper.getDataFromServer(Mockito.anyString())).thenReturn(serverResponse);
		String response = caseService.getRelatedCasesById(1L);
		Assert.assertNotNull(response);
		
	}*/
	
	@Test
	public void countAllCasesByUserTest() {
		Mockito.when(caseDaoMock.countAllCasesByUser(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(2L);
		long count = caseService.countAllCasesByUser(1L,"Individual-KYC",4,1);
		assertEquals(2L,count);
	}
	
	/*@Test
	public void getNextAnalystTest()  {
		Mockito.when(userDaoMock.getAllAnalystWhoDidNotRejectedCaseBefore(Mockito.anyLong())).thenReturn(userList);
		User nextAnalyst = caseService.getNextAnalyst(3L);
		Assert.assertNotNull(nextAnalyst.getScreenName());
	}*/
	
	/*@Test
	public void getNextAnalystNullTest()  {
		List<Users> analystUserList = new ArrayList<Users>();
		Mockito.when(userDaoMock.getAllAnalystWhoDidNotRejectedCaseBefore(Mockito.anyLong())).thenReturn(analystUserList);
		Users nextAnalyst = caseService.getNextAnalyst(3L);
		assertEquals(null,nextAnalyst);
	}*/
	
	@Test
	public void getAnalystMetricsTest() throws ParseException{
		Mockito.when(caseAnalystMappingDao.getAnalystMetrics(Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyBoolean(),Mockito.anyLong())).thenReturn(caseAnalystMappingList);
		JsonObject caseMetrics = caseService.getAnalystMetrics(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "INDIVIDUAL", true, 1L);
		Assert.assertNotNull(caseMetrics);
	}
	
	@Test(expected = DateFormatException.class)
	public void getAnalystMetrics_WhenDateFromExceptionThrowTest() throws ParseException{
		Mockito.when(caseAnalystMappingDao.getAnalystMetrics(Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyBoolean(),Mockito.anyLong())).thenReturn(caseAnalystMappingList);
		caseService.getAnalystMetrics(new Date().toString(),new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "INDIVIDUAL", true, 1L);
	}
	
	@Test(expected = DateFormatException.class)
	public void getAnalystMetrics_WhenDateToExceptionThrowTest() throws ParseException{
		Mockito.when(caseAnalystMappingDao.getAnalystMetrics(Mockito.any(Date.class),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyBoolean(),Mockito.anyLong())).thenReturn(caseAnalystMappingList);
		caseService.getAnalystMetrics(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new Date().toString(), "INDIVIDUAL", true, 1L);
	}
	
	
	@Test
	public void getCaseListTest() throws Exception {
		Mockito.when(caseDaoMock.getCaseList(Mockito.any(CaseVo.class),Mockito.anyLong())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseService.getCaseList(caseVo,1L);
		assertEquals(2,caseDtoListResult.size());
	}
	
	@Test
	public void caseListCountTest() throws Exception {
		Mockito.when(caseDaoMock.getCaseListCount(Mockito.any(CaseVo.class),Mockito.anyLong())).thenReturn(2L);
		Long count = caseService.caseListCount(caseVo,1L);
		assertEquals(new Long("2"),count);
	}
	
	@Test
	public void caseAggregatorTest() {
		Mockito.when(caseDaoMock.caseAggregator(Mockito.anyLong(),Mockito.any(CaseAggregatorDto.class))).thenReturn(caseAggregatorList);
		List<CaseAggregator> caseAggregatorListTest = caseService.caseAggregator(1L,caseAggregatorDto);
		assertEquals(2,caseAggregatorListTest.size());
	}
	
	@Test
	public void getCasesByUserTest()  {
		Mockito.when(caseDaoMock.getCasesByUser(Mockito.anyLong())).thenReturn(caseIdsList);
		List<Long> responseList = caseService.getCasesByUser(1L);
		assertEquals(3,responseList.size());
	}
	
	@Test
	public void getMyCaseIdsTest()  {
		Mockito.when(caseDaoMock.getMyCaseIds(Mockito.anyLong(),Mockito.any(CaseVo.class),Mockito.anyInt())).thenReturn(caseIdsList);
		List<Long> responseList = caseService.getMyCaseIds(1L,caseVo,10);
		assertEquals(3,responseList.size());
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void getCaseListForUnderwritingTest(){
		Mockito.when(documentDaoMock.searchUnderwritingDocumentsIdsByName(Mockito.anyString())).thenReturn(caseIdsList);
		Mockito.when(caseDaoMock.getCaseListForUnderwriting(Mockito.anyInt(),Mockito.anyInt(),Mockito.anyList())).thenReturn(caseUnderwritingDtoList);
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(caseAnalystMappingDao.findByCaseId(Mockito.anyLong())).thenReturn(mapping);
		List<CaseUnderwritingDto> caseUnderWritingList = caseService.getCaseListForUnderwriting(15,1,"test");
		assertEquals(2,caseUnderWritingList.size());		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getCaseListForUnderwritingEmptyKeywordTest(){
		Mockito.when(documentDaoMock.searchUnderwritingDocumentsIdsByName(Mockito.anyString())).thenReturn(caseIdsList);
		Mockito.when(caseDaoMock.getCaseListForUnderwriting(Mockito.anyInt(),Mockito.anyInt(),Mockito.anyList())).thenReturn(caseUnderwritingDtoList);
		Mockito.when(documentDaoMock.find(Mockito.anyLong())).thenReturn(documentVault);
		Mockito.when(caseAnalystMappingDao.findByCaseId(Mockito.anyLong())).thenReturn(mapping);
		List<CaseUnderwritingDto> caseUnderWritingList = caseService.getCaseListForUnderwriting(15,1,"");
		assertEquals(2,caseUnderWritingList.size());		
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void caseListCountForUnderwritingTest()  {
		Mockito.when(documentDaoMock.searchUnderwritingDocumentsIdsByName(Mockito.anyString())).thenReturn(caseIdsList);
		Mockito.when(caseDaoMock.caseListCountForUnderwriting(Mockito.anyList())).thenReturn(3L);
		Long count = caseService.caseListCountForUnderwriting("test");
		assertEquals(new Long("3"),count);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void caseListCountForUnderwriting_WhenKeywordIsEMptyTest()  {		
		Mockito.when(caseDaoMock.caseListCountForUnderwriting(Mockito.anyList())).thenReturn(0L);
		Long count = caseService.caseListCountForUnderwriting("");
		assertEquals(new Long("0"),count);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void caseListCountForUnderwriting_WhenEmptyArrayListTest()  {
		Mockito.when(documentDaoMock.searchUnderwritingDocumentsIdsByName(Mockito.anyString())).thenReturn(new ArrayList<Long>());
		Mockito.when(caseDaoMock.caseListCountForUnderwriting(Mockito.anyList())).thenReturn(0L);
		Long count = caseService.caseListCountForUnderwriting("test");
		assertEquals(new Long("0"),count);
	}
	
	@Test
	public void searchCaseListCountTest()  {
		Mockito.when(caseDaoMock.searchCaseListCount(Mockito.anyString())).thenReturn(2L);
		Long count = caseService.searchCaseListCount("test");
		assertEquals(new Long("2"),count);
	}
	
	@Test
	public void assignCaseToAnalystTest()  {
		Case testCase = new Case();
		Mockito.when(caseDaoMock.assignCaseToAnalyst(Mockito.any(Users.class),Mockito.any(Case.class))).thenReturn(2L);
		Long id = caseService.assignCaseToAnalyst(user,testCase);
		assertEquals(new Long("2"),id);
	}
	
	
	
	
	
	
	
	
	
	/*@Test
	public void getRelatedCasesByIdTest()  {
		Mockito.when(caseServiceMock.getRelatedCasesById(Mockito.anyLong())).thenReturn("Test case");
		String relatedcase = caseServiceMock.getRelatedCasesById(Mockito.anyLong());
		Assert.assertNotNull(relatedcase);
	}
	
	@Test
	public void getHotTopicsByCaseIdTest()  {
		Mockito.when(caseServiceMock.getHotTopicsByCaseId(Mockito.anyLong())).thenReturn("Test case hot topic");
		String hotTopic = caseServiceMock.getHotTopicsByCaseId(Mockito.anyLong());
		Assert.assertNotNull(hotTopic);
	}
	
	@Test
	public void getCaseSourceByIdTest()  {
		Mockito.when(caseServiceMock.getCaseSourceById(Mockito.anyLong(),Mockito.anyInt())).thenReturn(caseSource);
		CaseSource caseSourceObj = caseServiceMock.getCaseSourceById(Mockito.anyLong(),Mockito.anyInt());
		assertEquals(2,caseSourceObj.getWebsites().size());
	}
	@Test
	public void getCaseSummaryByIdTest() throws ParseException {
		Mockito.when(caseServiceMock.getCaseSummaryById(Mockito.anyLong())).thenReturn("Test case summary");
		String summary = caseServiceMock.getCaseSummaryById(Mockito.anyLong());
		Assert.assertNotNull(summary);
	}
	
	
	
	@Test
	public void getNextAnalystTest()  {
		Mockito.when(caseServiceMock.getNextAnalyst(Mockito.anyLong())).thenReturn(user);
		User nextAnalyst = caseServiceMock.getNextAnalyst(Mockito.anyLong());
		assertEquals("analyst",nextAnalyst.getScreenName());
	}
	
	@Test
	public void getAnalystMetricsTest() throws ParseException  {
		Mockito.when(caseServiceMock.getAnalystMetrics(Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyBoolean(),Mockito.anyLong())).thenReturn(caseAnalysis);
		JsonObject  allCaseAnalysis = caseServiceMock.getAnalystMetrics(Mockito.anyString(),Mockito.anyString(),Mockito.anyString(),Mockito.anyBoolean(),Mockito.anyLong());
		Assert.assertNotNull(allCaseAnalysis);
	}
	
	@Test
	public void getCaseListTest() throws Exception {
		Mockito.when(caseServiceMock.getCaseList(Mockito.any(CaseVo.class),Mockito.anyLong())).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseServiceMock.getCaseList(Mockito.any(CaseVo.class),Mockito.anyLong());
		assertEquals(2,caseDtoListResult.size());
	}
	
	@Test
	public void getRiskScoresTest() {
		Mockito.when(caseServiceMock.getRiskScores(Mockito.anyListOf(CaseDto.class))).thenReturn(caseDtoList);
		List<CaseDto> caseDtoListResult = caseServiceMock.getRiskScores(Mockito.anyListOf(CaseDto.class));
		assertEquals(2,caseDtoListResult.size());
	}
	
	@Test
	public void caseListCountTest() {
		Mockito.when(caseServiceMock.caseListCount(Mockito.any(CaseVo.class),Mockito.anyLong())).thenReturn(3L);
		long count = caseServiceMock.caseListCount(Mockito.any(CaseVo.class),Mockito.anyLong());
		assertEquals(3L,count);
	}
	
	@Test
	public void caseAggregatorTest() {
		Mockito.when(caseServiceMock.caseAggregator(Mockito.anyLong(),Mockito.any(CaseAggregatorDto.class))).thenReturn(caseAggregatorList);
		List<CaseAggregator> caseAggregatorListTest = caseServiceMock.caseAggregator(Mockito.anyLong(),Mockito.any(CaseAggregatorDto.class));
		assertEquals(2,caseAggregatorListTest.size());
	}
	
	@Test
	public void calculateRiskTest() {
		Mockito.when(caseServiceMock.calculateRisk(Mockito.anyListOf(Answer.class),Mockito.any(Answer.class))).thenReturn(answersList);
		List<Answer> answerListTest = caseServiceMock.calculateRisk(Mockito.anyListOf(Answer.class),Mockito.any(Answer.class));
		assertEquals(2,answerListTest.size());
	}
	
	@Test
	public void readDataFromCsvFileTest() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Mockito.when(caseServiceMock.readDataFromCsvFile(Mockito.any(MultipartFile.class))).thenReturn(answersList);
		List<Answer> answerListTest = caseServiceMock.readDataFromCsvFile(Mockito.any(MultipartFile.class));
		assertEquals(2,answerListTest.size());
	}
	
	@Test
	public void generateSeedJsonDataTest()  {
		Mockito.when(caseServiceMock.generateSeedJsonData(Mockito.anyListOf(Answer.class))).thenReturn("Test seed json data");
		String response = caseServiceMock.generateSeedJsonData(Mockito.anyListOf(Answer.class));
		Assert.assertNotNull(response);
	}
	
	@Test
	public void getCasesByUserTest()  {
		Mockito.when(caseServiceMock.getCasesByUser(Mockito.anyLong())).thenReturn(caseIdsList);
		List<Long> responseList = caseServiceMock.getCasesByUser(Mockito.anyLong());
		assertEquals(3,responseList.size());
	}
	
	@Test
	public void getMyCaseIdsTest()  {
		Mockito.when(caseServiceMock.getMyCaseIds(Mockito.anyLong(),Mockito.any(CaseVo.class),Mockito.anyInt())).thenReturn(caseIdsList);
		List<Long> responseList = caseServiceMock.getMyCaseIds(Mockito.anyLong(),Mockito.any(CaseVo.class),Mockito.anyInt());
		assertEquals(3,responseList.size());
	}
	
	@Test
	public void createCaseFromQuestionnaireFileTest() throws IllegalStateException, InvalidFormatException, IOException, TesseractException  {
		Mockito.when(caseServiceMock.createCaseFromQuestionnaireFile(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.any(byte[].class))).thenReturn(caseMapDocIdDto);
		CaseMapDocIdDto caseMapDocIdDtoTest = caseServiceMock.createCaseFromQuestionnaireFile(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.any(byte[].class));
		assertEquals(123L,caseMapDocIdDtoTest.getDocId());
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void generateSeedJsonDataFieldsTest()  {
		Mockito.when(caseServiceMock.generateSeedJsonData(Mockito.anyMap())).thenReturn("Test seed json data");
		String response = caseServiceMock.generateSeedJsonData(Mockito.anyMap());
		Assert.assertNotNull(response);
	}
	
	@Test
	public void getCaseListForUnderwritingTest()  {
		Mockito.when(caseServiceMock.getCaseListForUnderwriting(Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString())).thenReturn(caseUnderwritingDtoList);
		List<CaseUnderwritingDto> responseList = caseServiceMock.getCaseListForUnderwriting(Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString());
		assertEquals(2,responseList.size());
	}
	
	@Test
	public void caseListCountForUnderwritingTest()  {
		Mockito.when(caseServiceMock.caseListCountForUnderwriting(Mockito.anyString())).thenReturn(2L);
		Long count = caseServiceMock.caseListCountForUnderwriting(Mockito.anyString());
		Assert.assertNotNull(count);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void createEntityTest() throws org.json.simple.parser.ParseException  {
		Mockito.when(caseServiceMock.createEntity(Mockito.anyMap(),Mockito.anyString())).thenReturn("AUX1L35KUL");
		String response = caseServiceMock.createEntity(Mockito.anyMap(),Mockito.anyString());
		Assert.assertNotNull(response);
	}
	
	@Test
	public void searchCaseListTest()  {
		Mockito.when(caseServiceMock.searchCaseList(Mockito.anyString())).thenReturn(caseUnderwritingDtoList);
		List<CaseUnderwritingDto> responseList = caseServiceMock.searchCaseList(Mockito.anyString());
		assertEquals(2,responseList.size());
	}
	
	@Test
	public void searchCaseListCountTest()  {
		Mockito.when(caseServiceMock.searchCaseListCount(Mockito.anyString())).thenReturn(2L);
		Long count = caseServiceMock.searchCaseListCount(Mockito.anyString());
		Assert.assertNotNull(count);
	}
	
	@Test
	public void getIndustryTest() throws JsonParseException, JsonMappingException, org.json.simple.parser.ParseException, URISyntaxException, IOException  {
		Mockito.when(caseServiceMock.getIndustry(Mockito.anyString())).thenReturn("test case industry");
		String response = caseServiceMock.getIndustry(Mockito.anyString());
		Assert.assertNotNull(response);
	}
	*/
	

}
