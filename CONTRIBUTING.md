#Introduction
This file contains guidelines that should be followed while making changes in the repository.

#Program documentation
All projects within the repository should be well documented to the extent that the instructions can be reproduced by the developers, stake holders and end users so as to access, upload, download, re-package, re-build, deploy, test and make use of the programs in the repository either in source code and/or in binary format.

##General instructions
Following files should not be mixed with the source code and must be placed in the root directory of the repository i.e. they should be visible as soon as a user or accesses the top level directory containing a program source or binary.

##README
File named in all upper case. It contains general information about the program including it’s name, compiled name, purpose, summary of major known bugs, development status, a summary of it’s installation requirements, operating instructions and references to supporting documents and external links if any. It should also include notes regarding finding additional information relevant to the program.

##CHANGELOG
File named in all upper case. This document maintains the sufficiently detailed logs of notable changes sorted in descending order (most recent change on top) from one minor/major release to another. If old logs are not provided then references to them should be provided. Description should include date of release, impact (MAJOR, MINOR, CRITICAL, SECURITY), new features added with respect to the last release, bugfixes with respect to the last release, known issues not fixed and other specific changes with respect to the last release important for the developers need to know. The document must also include a list of major program files added and modified with respect to the last release.

##INSTALL
File named in all upper case. This file contains instructions which can be followed reproducibly while installing the program and successfully running it either from source or binary on a new machine. Instructions should include descriptions of minimum system configuration, operation system configuration, deployment environment configuration, dependencies and their configuration both internal and to be sourced from a third party and need to be installed separately. The file should include notes describing the exact sequence of steps to be followed and what should be expected after completion of each step. The instruction should also provide steps required for validation of a successful installation.

##BUGS
File named in all upper case. The file lists all the major and minor known bugs, issues and instructions for procedure of finding and reporting new bugs, issues.

##CONTRIBUTING
File named in all upper case (**this file**). It contains general directive to the developers including setting up of the development environment, adding/modifying code base, reporting issues, procedure for testing and deployment.

##NEWS
File named in all upper case. This one is a basic change log intended for end users and not developers.

##Configuration files
Configuration files required by the automated package manager, builder and IDE will be provided. Bare minimum dependencies such as scripts, database schema etc which are required during setting up of the development environment and deployment of the binaries should be provided as well.

#Package documentation
Packages are subdirectories under the root directory of a source code repository which organize source code files into namespaces and provide a clear view of the call stack, code organization as well as provide clear separation between functional sub-modules.

##General instructions
Package documentation should provide clear and accurate top level information about the functionalities provided by the source files in the package.

##Package info
Exact convention is specific to the development environment and development language (e.g. package-info.java for Java). This file must contain a summary of files, interfaces and functionalities publicly exposed by the specific package to other parts of the program. The information should also provide a rationale for such an arrangement. The comment should include references to documentations of external libraries used in binary form. A relevant documentation utility (e.g. javadoc for Java, scaladoc for Scala) should be able to extract contents of this file.

#Source file documentation
Source code documentation should provide clear overview of the program, code blocks and logical units in terms of what they do, and if required, a nonrigorous proof of correctness. Every public class, method and field must be documented without exception.

Exact convention can be extracted from the instructions and best practices relevant to specific programming languages. For example, java provides single line, multi line and documentation comments while HTML provides comment tag.

##General instructions
All comments should be restricted to 80 characters per line including the white spaces and be converted into multi-line comments if the limit is exceeded. This limit should be maintained to ensure readability of the source code. All comments should be provided in US english and should contain full dictionary words. Multi line comments should follow a symantics of general-to-specific details ending up in notes and references if any.

A programming language and platform specific code documentation tool (e.g. javadoc for Java, scaladoc for Scala) should be able to extract and collate the documentation comments from all the files into a single multi-part documentation with annotations, cross-references and bookmarks for easy navigation through the documentation.

##Top level documentation
A top level documentation comment which preceeds everything else summarizes the utility and purpose of the file. Comment may include creation date, brief explanation about what does the the program within the source file implement.

##Public class and interface documentation
Each public declaration of a class, interface or other top level container in specific programming language should start imemdiately after a documentation comment describing the intent behind creation of that class, interface or container. These comments will be extracted by an auto doc tool.

##Public and protected method documentation
Each public and protected method/function should contain block comment before declaration describing it’s purpose, it’s input parameters, return type, constraints on input and output and exceptional conditions and error codes generated by specific method/function if any. Commenting of private method and function is optional however highly encouraged. These comments will be extracted by an automated code documentation tool.

##Public and protected field documentation
Each public and protected field should immediately follow a documentation comment summarizing it’s purpose and access patterns deemed safe in context. Providing comments for private members is optional however highly encouraged. These comments will be extracted by an auto doc tool.

##Code block comments
These comments are required by both developers and code reviewers to have a clear understanding of the logic and invariant in use inside methods (particularly loops). The comments in this block are not intended to be extracted by the automated code documentation tools. The comment should explain both “why” and “what” however should not exceed 200 words for the sake of code readability.

#Program naming conventions
These naming conventions have been proposed so that source and binaries can be deployed and handled across multiple platforms and by multiple teams.

##Version number
A version number is constructed as **MAJOR.MINOR.RELEASE**. All fields are non negative integers. README file should mention the complete version number.

##Major version number
Packages with different **MAJOR** version numbers should not be assumed to be binary compatible under any circumstance. External libraries and dependencies which work with one particular **MAJOR** version should not be assumed to be binary compatible with a package having another MAJOR version and hence be thoroughly tested before deployment.

##Minor version number
Packages with different **MINOR** version number but same **MAJOR** version numbers should be binary compatible. All libraries and external dependencies binary compatible with the packages having older MINOR numbers should be binary compatible with the newer releases as long as **MAJOR** version numbers match however the converse may not be true.

##Release version number
**RELEASE** version number is upgraded post application of a quick fix. All packages with same **MAJOR** and **MINOR** version number should be binary compatible with one another and with the external libraries or dependencies already in use since the last upgrade.

##Upgrading the version number
There is no special convention regarding the steps in which the version numbers are upgraded however following mechanism should be utilized in relation to one another. When **MAJOR** version number is upgraded then both **MINOR** and **RELEASE** should be reset to zero. When MINOR version number is upgraded then RELEASE should be reset to zero.

##Package name
Package name should be a noun and be in sync with the name of the module or hub it belongs to. Package name should be in all lowercase ASCII text and internal words can be separated with dash or underscore.

##Qualified name
A package name can be suffixed (particularly during archiving and sharing) to include additional information like it’s code name (if any), release status (alpha, beta, stable) and version number. Fully qualified name should be placed in the README file of the program as well. An archive of the program source or binary should contain the fully qualified name which can be modified at the time of deployment.

#Package naming conventions
These naming conventions have been proposed so as to be clearly readable to the developers and have no conflicts with the default or external libraries. Another purpose is to allow a code reviewer or a developer to make good guesses regarding the call stack and code organization.

##Package name
Each package represented by a directory in the repository and each sub package nested within it should be named using lower case ASCII letters. Package names should provide clear hints about source code organization and hierarchy.

Package name should be unique and indicate an ownership by Blackswan technologies and should not conflict with the package naming convention of the default libraries and external libraries which can create potential security issues (particularly in context of Java environment)

##Hierarchy
Package hierarchy should be clear from the naming convention and the leaf package and any package immediately containing source code should justify it’s purpose through it’s name.

#Source file naming convention
These naming conventions have been proposed so as to all the developers and reviewers to make good assumptions about the content and purpose of a source code file. In general names should be formed using ASCII character and digits and should always start with ASCII character.

##Source file name
Source file should be named according to the necessitites and conventions imposed by the underlying programming language. For example Java source files should have the same name as the public class they contain. Unless required by specifications or conventions of the specific programming language or framework the names use lowercase ASCII characters, be nouns and use of digits or special symbols should be discouraged.

##Class and interface name
Should start with a Capitalized ASCII character and each internal word should be capitalized. Class and Interface names should be nouns and use of acronyms or abbreviations should be avoided.

##Method or function name
Should start with a lower case ASCII letter and each internal word should be capitalized (camelCase). Method names should be verbs (except the constructors).

##Field or variable name
Field or variable names should be mnemonics, i.e. their names should provide a good indicator of the intent of their use. One character name is allowed for temporary variables containing basic data types. Common names for temporary variables are i, j, k for integral values, d,e, f for real values,  m,n,p for characters and w,x,y,z for strings. Only private fields and variables are allowed to start or end with an underscore(_). Public static variables should be named using all uppercase ASCII characters, dash/underscore and numbers always starting with a character.

#Database naming convention
These instructions are primarily relevant for relational databases however they can also be utlized in context of other data stores.

##Database name
Should be a noun formed using lowercase ASCII characters, internal words can be separated using underscores and may optionally contain numeric digits however cannot start with one.

##Table name
Table names are nouns which should be mnemonics and be formed using lowercase ASCII characters only, internal words can be separated using underscores. All table names created by the developers and not imported from elsewhere (to support external libraries) should be prefixed with “bst”.

##Column name
Column names should be nouns and be recommended to be mnemonics however be short (not longer than 12 characters unless absolutely necessary). Column names are formed using lowercase ASCII characters and numeric digits; internal words can be separated using underscores. Foreign keys can optionally contain “fk” prefix, Primary keys can optionally contain “pk” prefix and unique keys can optionally contain “u” prefix so as to clearly identify them while reading the schema.

##Primary key
Should contain "id" in suffix. Primary keys should always be of numeric long type (at least 64-bits). Exceptions are permitted however after proper deliberation and aproval.

#Miscellaneous directories
This section describes the top level directories which should be provided inside program’s root directory.

##The “doc” directory
Javadoc and equivalent programs (Scaladoc for Scala) should place their output inside this directory. Manually generated documentation should also be placed here.

##The “tools” directory
This directory must be provided to store scripts, and templates for configuration, log and settings files which only contain placeholders for the final names. Each file should contain instructions for their their proper use in final form.

##The “schema” directory
This directory contains **schema** for the data stores or databases which are immediate dependencies for the program.

##The “tests” directory
This directory provides temporary or permanent space for all unit test routines, control data and instructions except external libraries utilized for the purpose of running unit tests. This directory is self-contained to the extent that it only calls publicly exposed interfaces, methods and filelds by other packages and can be removed without effecting the proper functioning of the program.

##The “build” directory
Optional,a placeholder foer the root directory where an automated build should be initiated according to the instructions provided in the INSTALL file.

#Code organization

##Package
The program will be divided into packages with each package constituted by several source code files and sub packages. Each package represents a clearly distinguishable functional unit/module of the program and provides access to specific functionality through publicly exposed classes, methods and fields. 

Only the bare minimum set of functionalities must be exposed to the code external to the package. The general intent is the same as the packages in java. Packages should be self contained to the extent allowable by the software framework. Cross-dependencies between packages is prohibited.

Client or use-case specific extensions should be contained within well defined packages and should not have inward dependencies from external packages.

##Source file
A source file should contains just a single publicly accessible class, interface or equivalent and multiple private classes which are strictly accessed locally and never outside the source file.

A line of code should not exceed 80 characters including white spaces and must be continued over multiple lines if the constraint is violated.

##Encoding
All source files are encoded using UTF-8. When possible, character set in use should be explicitly declared in the header of the source files (e.g. CSS3 header).

##Coding styles
* Java: google coding style guidelines
* Scala: official scala documentation
* Python: google coding style guidelines

#Procedures
Following sections provide guidelines primarily to prevent introduction of new bugs, code bloat and resource leaks.

##Automated Resource Management
Utilize language specific ARM facilities (or build one as set of libraries) for handling of external resource lifecycles and for preventing system resource leaks (database connection, file descriptor etc). Create utility classes and function for handling commonly used resources (web services, databases, log files, documents etc)

##Reentrancy
All procedures (method/function calls or equivalent) should be reinterant, i.e. they can be safely invoked again before previous invocations complete execution.

##Thread Safety
Class level thread safety should be guaranteed at the minimum, which means that two objects of the same class can be utilized by two separate threads by virtue of not sharing a common state or variable (e.g. a private static field).

##Synchronization
Avoid synchronization directives. Most of the core libraries already contain proper synchronization mechanisms and do not require one be provided by the application developers. Use of such directives at application level generally indicate bad design and coding practice.

##Modularization
Group by use and purpose, e.g. all data source connectors belong to a single package.
Develop reusable components including but not limited to data source source and sink managers, loggers, error handlers, Themes library, Charting library.

##Initialization
A single initialization class/routine should be created which gets executed at start-up and sets the program in a consistent state before the process becomes accessible to the external users or programs. Such class can expose internal fields which should be constants and prefferably static.

##Configuration
Externalize all configuration files which can be potentially modified during deployment. All resources should be accessed via names and not via full path (application loads the full paths from external configuration files, environment variables or server settings).

##Logging and error handling
Implement logging and exceptions as cross cutting concerns utilizing the AOP facilities provided by specific languages. All access attempts to or from internal or external interfaces should be logged (can omit parameter and return values) and full stack trace should be logged after an exception. Provide procedure for accessing and reading the log files in the INSTALL document.

##Extensions
Specialized extended features or client specific features should be maintained in self-contained packages which can be removed without impacting the core functionalities of the program.

##Encoding
All character files and streams used for input or output by the top level externally facing interfaces should be UTF-8 encoded unless otherwise specified or required.

##Libraries
Use those libraries which are well integrated into the framework and don’t consider the alternatives unless the conventional libraries are inadequate or dysfunctional. e.g. use Jackson json parser with Jersey instead of Gson.

##Serialization and deserialization
While interfacing with external APIs all inputs should be deserialized as soon as possible before processing begins and be serialized only before a final return call and no sooner than that.

##Error and Exception propagation
An exception or error should be caught, logged and rethrown progressively propagating towards the top of the call stack where it is finally consumed (caught), logged and reported to the end user. The top level interface must provide a human readable message explaining the error or exception to the end user however only a system generated log accessible by the priviledged users should contain the full stack trace.

Error and exception logs should be recorded in a centralized and secure repository which should allow drill-down and diagnosis facilities.

#Web services

##Pattern
All web services should be RESTful.

All Web services are APIs while not all APIs are web services. While deploying web services strictly stick with the service-specific conventions instead of general conventions.

##Methods
Use the REST methods strictly according to their intent, e.g. a GET method should not produce side effects. GET (read a resource), POST (create new subordinate resource), PUT(update or replace an existing resporce), DELETE(delete a resource), PATCH (partially update a resource).

##Return codes
Most commonly used return codes are 200, 201, 204, 400, 403, 404, 405, 409 and 500. They should strictly retain their meaning and intent.

##Authentication
All REST APIs unless otherwise specified or intended for use in properly firewalled local network should provide token based authentication mechanism. The token should contain at least 32 BASE 16 characters to be considered secure. All APIs should follow the bearer authentication scheme. Tokens are valid either for a limited time period or until the next login attempt based on the use cases.

##Unit test blocks
Unit test blocks should be disabled or deleted before a final binary release or production deployment to prevent data leaks and other security related issues.

Build tool should be configured for removal of debugging information and unnecessary code from a production ready binary.

##Stress tests
All web services should be load tested to handle at least 500 authentications/second (new token creation), at least  2000 requests/second (hardware requirement must be standardized before or while running the tests) except when allowed otherwise to be certified as production ready.

##Load/Store tests
All data store bound web services must be tested to perform at least 18,000 randomized load/stores per second on a single node before they are certified as production ready.

##Redirection
All web service end points and clients must be configured to produce and handle 3xx redirection instructions gracefully.

##TLS support
ALL RESTFUL interfaces should support seamless and issueless migration to TLS either via direct server side configuration or via TLS termination.

#Testing
##General guidelines
QA is not a separate process in Agile devop and testing of software components is integral to the software development effort. Most of the software related issues should be captured and handled within the IDEs and the builds should be allowed to fail as early as possible so as to stop prapagation of bad code into the repository.

Build automation tools like maven should be configured to run static and dynamic code analysis tools in their pipeline and builds should be allowed to fail early on encountering issues.

Continuous code quality analysis tool integrated with the CI/CD pipeline must be utilized for improving "code coverage" and for reporting both newly introduced as well as existing vulnerabilities.

Following test sequence should be followed in strict order.

##Sanity Test
Test the interfaces with a small set of sample inputs and output pairs to produce both normal and error conditions and confirm that basic functionalities are in place. This test should be performed by the developer on his workdesk.

##Peer review (optional)
Get your code reviewed by a peer. This test should be performed by the developer on his workdesk.

##Static code analysis
Configure the IDE with an acceptable coding style and use it’s functionality to automatically indent and format the source code. After formatting, run the source codes through a static code analysis tool and fix the reported issues. These steps should be performed before committing the code into the repository. This step should be performed by the developer on-site and before a staged deployment.

##Unit test
Create automated unit test procedures and run them to test all the units (methods, classes, procedures) against a range of valid and invalid inputs. Unit tests can be run as part of CI/CD procedure however developers are required to run all the unit tests locally before committing the code into the repository. This test should be performed by the developer on his workdesk.

##Single user performance testing
Developer should test the UI components for browser support and responsiveness before committing the code into repository. All errors reported by the browser console should be fixed at this stage and not be allowed to propagate to the repository. This test should be performed by the developer on his workdesk.

##Integration test
Part of CI/CD procedure. All internal and external interfaces should be tested for defects resulting from improper integration of modules or hubs. These tests should be performed on a pre-release deployment.

##System test
Part of CI/CD procedure, however the frontend requires manual testing. These tests should be preformed on a pre-release deployment.

##Acceptence test
This one is a black box test procedure to confirm that all functional and use case requirements are satisfied. This test should be performed on a clean pre-release deployment.

#Archives
All program archives (containing source files and/or binaries) and inactive log files should be time stamped (DD-MM-YYYY_hh-mm-ss) and optionally be suffixed with a string containing BASE 16 encoded characters to avoid naming conflict during storage. Only lowercase characters should be used and dash should be used as separator. A SHA-256 checksum of the archive should be provided.

##JAR and WAR archives
JAR files should be hardened before deployment to prevent class injection and other malicious modifications.




