package element.bst.elementexploration.rest.usermanagement.group.service;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;

public interface UserGroupService extends GenericService<UserGroup, Long> {

	UserGroup getUserGroup(Long userId, Long groupId);

}
