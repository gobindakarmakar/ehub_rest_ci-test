package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AbstractDomainObject;

@Entity
@Table(name = "COUNTRY")
public class Country extends AbstractDomainObject {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String country;
	private String iso2Code;
	private String is03Code;
	private String latitude;
	private String longitude;
	private Float fatfRisk;
	private Float taskJusticeNetworkHighRisk_FinancialSecracyIndex;
	private Float internationalNarcoticsControlRisk;
	private Float transparencyInternationalRisk;
	private Float worldBankRisk;
	private Float freedomHouseRisk;
	private Float worldJusticeRiskWorld_CriminalJusticeRiskRating;
	private Float wefRisk;
	private Float politicalRiskServicesInternationalCountryRisk_PRS;
	private Double baselAMLIndex2017;
	private String currency;
	private Float fatfCreditCardAmountLimit;
	private Float taxJusticeNetworkCreditCardAmountLimit;
	private Float baselAMLIndex2017CreditCardAmountLimit;
	private Float transparancyInternationalCountriesCreditCardAmountLimit;
	private Float worldBankCreditCardAmountLimit;
	private Float wefCreditCardAmountLimit;
	private Float freedomHouseHighRiskCountriesCreditCardAmountLimit;
	private Float worldJusticeProjectHighRiskCountriesCreditCardAmountLimit;
	private Integer fatfCreditCardTransactionsLimit;
	private Integer taxJusticeNetworkCreditCardTransactionsLimit;
	private Integer baselAMLIndex2017CreditCardTransactionsLimit;
	private Integer transparancyInternationalCountriesCreditCardTransactionsLimit;
	private Integer worldBankCreditCardTransactionsLimit;
	private Integer wefCreditCardTransactionsLimit;
	private Integer freedomHouseHighRiskCountriesCreditCardTransactionsLimit;
	private Integer worldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit;
	

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "COUNTRY")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name = "CURRENCY")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "IS02_CODE")
	public String getIso2Code() {
		return iso2Code;
	}

	public void setIso2Code(String iso2Code) {
		this.iso2Code = iso2Code;
	}

	@Column(name = "IS03_CODE")
	public String getIs03Code() {
		return is03Code;
	}

	public void setIs03Code(String is03Code) {
		this.is03Code = is03Code;
	}

	@Column(name = "LATITUDE")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "LONGITUDE")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "FATF_RISK")
	public Float getFatfRisk() {
		return fatfRisk;
	}

	public void setFatfRisk(Float fatfRisk) {
		this.fatfRisk = fatfRisk;
	}

	@Column(name = "TASK_JUSTICE_NETWORK_HIGH_RISK_FINANCIALSECRACYINDEX")
	public Float getTaskJusticeNetworkHighRisk_FinancialSecracyIndex() {
		return taskJusticeNetworkHighRisk_FinancialSecracyIndex;
	}

	public void setTaskJusticeNetworkHighRisk_FinancialSecracyIndex(
			Float taskJusticeNetworkHighRisk_FinancialSecracyIndex) {
		this.taskJusticeNetworkHighRisk_FinancialSecracyIndex = taskJusticeNetworkHighRisk_FinancialSecracyIndex;
	}

	@Column(name = "INTERNATIONAL_NARCOTICS_CONTROL_RISK")
	public Float getInternationalNarcoticsControlRisk() {
		return internationalNarcoticsControlRisk;
	}

	public void setInternationalNarcoticsControlRisk(Float internationalNarcoticsControlRisk) {
		this.internationalNarcoticsControlRisk = internationalNarcoticsControlRisk;
	}

	@Column(name = "TRANSPARENCY_INTERNATIONAL_RISK")
	public Float getTransparencyInternationalRisk() {
		return transparencyInternationalRisk;
	}

	public void setTransparencyInternationalRisk(Float transparencyInternationalRisk) {
		this.transparencyInternationalRisk = transparencyInternationalRisk;
	}

	@Column(name = "WORLD_BANK_RISK")
	public Float getWorldBankRisk() {
		return worldBankRisk;
	}

	public void setWorldBankRisk(Float worldBankRisk) {
		this.worldBankRisk = worldBankRisk;
	}

	@Column(name = "FREEDOM_HOUSE_RISK")
	public Float getFreedomHouseRisk() {
		return freedomHouseRisk;
	}

	public void setFreedomHouseRisk(Float freedomHouseRisk) {
		this.freedomHouseRisk = freedomHouseRisk;
	}

	@Column(name = "WORLD_JUSTICE_RISK_CRIMINAL_JUSTICE_RISK_RATING")
	public Float getWorldJusticeRiskWorld_CriminalJusticeRiskRating() {
		return worldJusticeRiskWorld_CriminalJusticeRiskRating;
	}

	public void setWorldJusticeRiskWorld_CriminalJusticeRiskRating(
			Float worldJusticeRiskWorld_CriminalJusticeRiskRating) {
		this.worldJusticeRiskWorld_CriminalJusticeRiskRating = worldJusticeRiskWorld_CriminalJusticeRiskRating;
	}

	@Column(name = "WEF_RISK")
	public Float getWefRisk() {
		return wefRisk;
	}

	public void setWefRisk(Float wefRisk) {
		this.wefRisk = wefRisk;
	}

	@Column(name = "POLITICAL_RISK_SERVICES_INTERNATIONAL_COUNTRY_RISK_PRS")
	public Float getPoliticalRiskServicesInternationalCountryRisk_PRS() {
		return politicalRiskServicesInternationalCountryRisk_PRS;
	}

	public void setPoliticalRiskServicesInternationalCountryRisk_PRS(
			Float politicalRiskServicesInternationalCountryRisk_PRS) {
		this.politicalRiskServicesInternationalCountryRisk_PRS = politicalRiskServicesInternationalCountryRisk_PRS;
	}

	@Column(name = "BASEL_AML_INDEX_2017")
	public double getBaselAMLIndex2017() {
		return baselAMLIndex2017;
	}

	public void setBaselAMLIndex2017(double baselAMLIndex2017) {
		this.baselAMLIndex2017 = baselAMLIndex2017;
	}

	@Column(name = "FATF_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getFatfCreditCardAmountLimit() {
		return fatfCreditCardAmountLimit;
	}

	public void setFatfCreditCardAmountLimit(Float fatfCreditCardAmountLimit) {
		this.fatfCreditCardAmountLimit = fatfCreditCardAmountLimit;
	}
	
	@Column(name = "TAX_JUSTICE_NETWORK_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getTaxJusticeNetworkCreditCardAmountLimit() {
		return taxJusticeNetworkCreditCardAmountLimit;
	}

	public void setTaxJusticeNetworkCreditCardAmountLimit(Float taxJusticeNetworkCreditCardAmountLimit) {
		this.taxJusticeNetworkCreditCardAmountLimit = taxJusticeNetworkCreditCardAmountLimit;
	}
	
	@Column(name = "BASEL_AML_INDEX_2017_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getBaselAMLIndex2017CreditCardAmountLimit() {
		return baselAMLIndex2017CreditCardAmountLimit;
	}

	public void setBaselAMLIndex2017CreditCardAmountLimit(Float baselAMLIndex2017CreditCardAmountLimit) {
		this.baselAMLIndex2017CreditCardAmountLimit = baselAMLIndex2017CreditCardAmountLimit;
	}

	@Column(name = "TRANSPARANCY_INTERNATIONAL_COUNTRIES_CREDIT_CARD_AMOUNT_LIMIT")
	public Float gettransparancyInternationalCountriesCreditCardAmountLimit() {
		return transparancyInternationalCountriesCreditCardAmountLimit;
	}

	public void settransparancyInternationalCountriesCreditCardAmountLimit(
			Float transparancyInternationalCountriesCreditCardAmountLimit) {
		this.transparancyInternationalCountriesCreditCardAmountLimit = transparancyInternationalCountriesCreditCardAmountLimit;
	}
	
	@Column(name = "WORLD_BANK_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getWorldBankCreditCardAmountLimit() {
		return worldBankCreditCardAmountLimit;
	}

	public void setWorldBankCreditCardAmountLimit(Float worldBankCreditCardAmountLimit) {
		this.worldBankCreditCardAmountLimit = worldBankCreditCardAmountLimit;
	}

	@Column(name = "WEF_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getWefCreditCardAmountLimit() {
		return wefCreditCardAmountLimit;
	}

	public void setWefCreditCardAmountLimit(Float wefCreditCardAmountLimit) {
		this.wefCreditCardAmountLimit = wefCreditCardAmountLimit;
	}

	@Column(name = "FREEDOM_HOUSE_HIGH_RISK_COUNTRIES_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getFreedomHouseHighRiskCountriesCreditCardAmountLimit() {
		return freedomHouseHighRiskCountriesCreditCardAmountLimit;
	}

	public void setFreedomHouseHighRiskCountriesCreditCardAmountLimit(
			Float freedomHouseHighRiskCountriesCreditCardAmountLimit) {
		this.freedomHouseHighRiskCountriesCreditCardAmountLimit = freedomHouseHighRiskCountriesCreditCardAmountLimit;
	}

	@Column(name = "WORLDJUSTICEPROJECT_HIGHRISKCOUNTRIES_CREDIT_CARD_AMOUNT_LIMIT")
	public Float getWorldJusticeProjectHighRiskCountriesCreditCardAmountLimit() {
		return worldJusticeProjectHighRiskCountriesCreditCardAmountLimit;
	}

	public void setWorldJusticeProjectHighRiskCountriesCreditCardAmountLimit(
			Float worldJusticeProjectHighRiskCountriesCreditCardAmountLimit) {
		this.worldJusticeProjectHighRiskCountriesCreditCardAmountLimit = worldJusticeProjectHighRiskCountriesCreditCardAmountLimit;
	}

	@Column(name = "FATF_CREDIT_CARD_TRANSACTION_LIMIT")
	public Integer getFatfCreditCardTransactionsLimit() {
		return fatfCreditCardTransactionsLimit;
	}

	public void setFatfCreditCardTransactionsLimit(Integer fatfCreditCardTransactionsLimit) {
		this.fatfCreditCardTransactionsLimit = fatfCreditCardTransactionsLimit;
	}

	@Column(name = "TAX_JUSTICE_NETWORK_CREDIT_CARD_TRANSACTIONS_LIMIT")
	public Integer getTaxJusticeNetworkCreditCardTransactionsLimit() {
		return taxJusticeNetworkCreditCardTransactionsLimit;
	}

	public void setTaxJusticeNetworkCreditCardTransactionsLimit(Integer taxJusticeNetworkCreditCardTransactionsLimit) {
		this.taxJusticeNetworkCreditCardTransactionsLimit = taxJusticeNetworkCreditCardTransactionsLimit;
	}

	@Column(name = "BASEL_AML_INDEX_2017_CREDIT_CARD_TRANSACTIONS_LIMIT")
	public Integer getBaselAMLIndex2017CreditCardTransactionsLimit() {
		return baselAMLIndex2017CreditCardTransactionsLimit;
	}

	public void setBaselAMLIndex2017CreditCardTransactionsLimit(Integer baselAMLIndex2017CreditCardTransactionsLimit) {
		this.baselAMLIndex2017CreditCardTransactionsLimit = baselAMLIndex2017CreditCardTransactionsLimit;
	}

	@Column(name = "TRANSPARANCY_INTERNATIONAL_COUNTRIES_CREDIT_CARD_TRANS_LIMIT")
	public Integer gettransparancyInternationalCountriesCreditCardTransactionsLimit() {
		return transparancyInternationalCountriesCreditCardTransactionsLimit;
	}

	public void settransparancyInternationalCountriesCreditCardTransactionsLimit(
			Integer transparancyInternationalCountriesCreditCardTransactionsLimit) {
		this.transparancyInternationalCountriesCreditCardTransactionsLimit = transparancyInternationalCountriesCreditCardTransactionsLimit;
	}

	@Column(name = "WORLD_BANK_CREDIT_CARD_TRANSACTIONS_LIMIT")
	public Integer getWorldBankCreditCardTransactionsLimit() {
		return worldBankCreditCardTransactionsLimit;
	}

	public void setWorldBankCreditCardTransactionsLimit(Integer worldBankCreditCardTransactionsLimit) {
		this.worldBankCreditCardTransactionsLimit = worldBankCreditCardTransactionsLimit;
	}

	@Column(name = "WEF_CREDIT_CARD_TRANSACTIONS_LIMIT")
	public Integer getWefCreditCardTransactionsLimit() {
		return wefCreditCardTransactionsLimit;
	}

	public void setWefCreditCardTransactionsLimit(Integer wefCreditCardTransactionsLimit) {
		this.wefCreditCardTransactionsLimit = wefCreditCardTransactionsLimit;
	}

	@Column(name = "FREEDOM_HOUSE_HIGH_RISK_COUNTRIES_CREDIT_CARD_TRANS_LIMIT")
	public Integer getFreedomHouseHighRiskCountriesCreditCardTransactionsLimit() {
		return freedomHouseHighRiskCountriesCreditCardTransactionsLimit;
	}

	public void setFreedomHouseHighRiskCountriesCreditCardTransactionsLimit(
			Integer freedomHouseHighRiskCountriesCreditCardTransactionsLimit) {
		this.freedomHouseHighRiskCountriesCreditCardTransactionsLimit = freedomHouseHighRiskCountriesCreditCardTransactionsLimit;
	}

	@Column(name = "WORLDJUSTICEPROJECT_HIGHRISKCOUNTRIES_CREDIT_CARD_TRANS_LIMIT")
	public Integer getWorldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit() {
		return worldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit;
	}

	public void setWorldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit(
			Integer worldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit) {
		this.worldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit = worldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit;
	}
	
	

}
