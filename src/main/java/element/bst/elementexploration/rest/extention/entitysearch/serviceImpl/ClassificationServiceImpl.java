package element.bst.elementexploration.rest.extention.entitysearch.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.entitysearch.domain.Classification;
import element.bst.elementexploration.rest.extention.entitysearch.service.ClassificationService;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.ClassificationsDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("classificationService")
public class ClassificationServiceImpl extends GenericServiceImpl<Classification, Long> implements ClassificationService {

	@Autowired
	public ClassificationServiceImpl(GenericDao<Classification, Long> genericDao) {
		super(genericDao);
	}
	
	

	@Autowired
	ClassificationsDao classificationsDao;
	
	
	@Override
	@Transactional("transactionManager")
	public Classifications fetchClassification(String classsifcationName) {
		return classificationsDao.fetchClassification(classsifcationName);
	}

}
