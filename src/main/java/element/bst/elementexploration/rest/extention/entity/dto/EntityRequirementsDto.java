package element.bst.elementexploration.rest.extention.entity.dto;

import java.io.Serializable;

public class EntityRequirementsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long requirement_id;

	private String entityRequirements;

	public long getRequirement_id() {
		return requirement_id;
	}

	public void setRequirement_id(long requirement_id) {
		this.requirement_id = requirement_id;
	}

	public String getEntityRequirements() {
		return entityRequirements;
	}

	public void setEntityRequirements(String entityRequirements) {
		this.entityRequirements = entityRequirements;
	}

}
