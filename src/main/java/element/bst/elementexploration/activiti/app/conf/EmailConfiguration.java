/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package element.bst.elementexploration.activiti.app.conf;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

import element.bst.elementexploration.rest.settings.service.SettingsService;

@Configuration
public class EmailConfiguration {

	
	@Autowired
	private SettingsService settingsService;
	
	
	@Bean("javaMailSender")
	public JavaMailSender getJavaMailSender() {/*
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		
		boolean isEmailEnabled = env.getProperty("email.enabled", Boolean.class, false);
		if (isEmailEnabled) {
				javaMailSender.setHost(env.getRequiredProperty("email.host"));			
				javaMailSender.setPort(env.getRequiredProperty("email.port", Integer.class));
		}

		Boolean useCredentials = env.getProperty("email.useCredentials", Boolean.class);
		if (Boolean.TRUE.equals(useCredentials)) {
				javaMailSender.setUsername(env.getRequiredProperty("email.username"));
				javaMailSender.setPassword(env.getRequiredProperty("email.password"));
		}

		Boolean emailTLS = env.getProperty("email.tls", Boolean.class);
		if (emailTLS != null) {
			javaMailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", emailTLS.toString());
			javaMailSender.getJavaMailProperties().setProperty("mail.transport.protocol", "smtp");
			javaMailSender.getJavaMailProperties().setProperty("mail.debug", "true");
			javaMailSender.getJavaMailProperties().setProperty("mail.smtp.auth", "true");
			javaMailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
		}*/
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		Map<String,String> mailSettings=new HashMap<>();
		mailSettings=settingsService.getMailSettings();
		if(mailSettings!=null){
		if(mailSettings.get("Outbound SMTP Server")!=null)
		javaMailSender.setHost(mailSettings.get("Outbound SMTP Server"));
		if(mailSettings.get("Port")!=null)
		javaMailSender.setPort(Integer.valueOf(mailSettings.get("Port")));
		if(mailSettings.get("From")!=null)
		javaMailSender.setUsername(mailSettings.get("From"));
		if(mailSettings.get("Password")!=null)
		javaMailSender.setPassword(mailSettings.get("Password"));
		javaMailSender.setProtocol("smtp");
		javaMailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "true");
		if((mailSettings.get("Enable Authentication")!=null)&&("ON".equalsIgnoreCase(mailSettings.get("Enable Authentication"))))
		javaMailSender.getJavaMailProperties().setProperty("mail.smtp.auth", "true");
		}
		return javaMailSender;
	}

	@Bean
	public FreeMarkerConfigurationFactoryBean freeMarkerConfig() {
		Properties props = new Properties();
		props.setProperty("number_format", "0.##");
		props.setProperty("locale", "en-US");

		FreeMarkerConfigurationFactoryBean factory = new FreeMarkerConfigurationFactoryBean();
		factory.setFreemarkerSettings(props);
		factory.setTemplateLoaderPath("classpath:/templates/");
		return factory;
	}
}
