package element.bst.elementexploration.rest.generic.service;

import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.util.FilterModelDto;


/**
 * 
 * @author Amit Patel
 *
 */
public interface GenericService<E , I> {
	
	public List<E> findAll();
	public E find(I id);
	public E save(E e);
	public Iterable<E> saveAll(Iterable<E> e);
	public void update(E e);
	public void saveOrUpdate(E e);
	public void delete(E e);
	public void flush();
	public Map<List<E>, Long> filterData(E e, FilterModelDto dto, Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy);
}
