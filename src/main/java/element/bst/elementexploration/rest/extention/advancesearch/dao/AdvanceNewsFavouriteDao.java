package element.bst.elementexploration.rest.extention.advancesearch.dao;

import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface AdvanceNewsFavouriteDao extends GenericDao<AdvanceNewsFavourite, Long> {
	
	
	public int deleteAdvanceNewsFavourite(String entityId);

	public AdvanceNewsFavourite fetchadvanceNewsFavourite(String entityId, String entityName, Long userId);

}
