package element.bst.elementexploration.rest.extention.workflow.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.workflow.service.HdfsService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("hdfsService")
public class HdfsServiceImpl implements HdfsService {

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENTION_API;

	@Override
	public String deletedFiles(String profileId, String folderPath) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/hdfs/api/deletefiles";
		StringBuilder builder = new StringBuilder(url);
		Boolean flag = false;
		if (profileId != null) {
			builder.append("?profileId=" + encode(profileId));
			flag = true;
		}
		if (folderPath != null && flag) {
			builder.append("&folderPath=" + encode(folderPath));
		} else if (folderPath != null) {
			builder.append("?folderPath=" + encode(folderPath));
		}

		return getDataFromServer(builder.toString());	
	}

	@Override
	public String ListOfDirectories(String profileId, String folderPath) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/hdfs/api/directories?profileId=" + profileId + "&folderPath="
				+ folderPath;
		return getDataFromServer(url);	
	}

	@Override
	public String makeDirectory(String profileId, String folderPath) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/hdfs/api/makedir";
		StringBuilder builder = new StringBuilder(url);
		Boolean flag = false;
		if (profileId != null) {
			builder.append("?profileId=" + encode(profileId));
			flag = true;
		}
		if (folderPath != null && flag) {
			builder.append("&folderPath=" + encode(folderPath));
		} else if (folderPath != null) {
			builder.append("?folderPath=" + encode(folderPath));
		}

		return getDataFromServer(builder.toString());	
	}

	@Override
	public String getSampleDataOfHdfs(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/hdfs/api/sampledata/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String getSchemaOfHdfs(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/hdfs/api/schema/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	private String getDataFromServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}
}
