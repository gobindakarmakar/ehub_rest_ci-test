package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerAddressDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerAddress;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDetailsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AddressType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerTitle;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.Gender;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerDetailsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("customerDetailsService")
public class CustomerDetailsServiceImpl extends GenericServiceImpl<CustomerDetails, Long>
		implements CustomerDetailsService {

	private Random random = null;

	public CustomerDetailsServiceImpl() {
		random = new Random();
	}

	@Autowired
	public CustomerDetailsServiceImpl(GenericDao<CustomerDetails, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	CustomerDetailsDao customerDao;

	@Autowired
	CustomerAddressDao customerAddressDao;

	Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

	@Override
	@Transactional("transactionManager")
	public Boolean uploadCustomerDetailsFile(MultipartFile multipartFile) throws IOException, ParseException, NoResultException {
		BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
		String line = null;
		int i = 1;

		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if (line.contains(","))
					data = line.split(",");
				if (line.contains(";"))
					data = line.split(";");

				CustomerDetails customerDetails = new CustomerDetails();
				customerDetails.setCustomerNumber(data[0]);
				if (data[1].equals("-")) {
					customerDetails.setCustomerType(CustomerType.NONE);
				} else {
					customerDetails.setCustomerType(CustomerType.valueOf(data[1]));
				}
				if (data[2].equals("-") || data[2].equals("")) {
					customerDetails.setCustomerTitle(CustomerTitle.NONE); // customer
																			// title
				} else {
					// System.out.println(data[2] + "Customer title");
					customerDetails.setCustomerTitle(CustomerTitle.valueOf(data[2]));
				}
				customerDetails.setFirstName(data[3]);
				customerDetails.setMiddleName(data[4]);
				customerDetails.setInstitutionName(data[5]);
				customerDetails.setDisplayName(data[6]);
				if (data[7].equals("-")) {
					customerDetails.setGender(Gender.NONE);// ----gender
				} else {
					customerDetails.setGender(Gender.valueOf(data[7]));
				}

				Date date1 = null;// DOB
				if (data[8].equals("-") || data[8].equals("")) {
					// do nothing
				} else {
					String dateTo = data[8];
					if (dateTo.contains("-")) {
						DateFormat formatter = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
						date1 = formatter.parse(dateTo);
					}
					if (dateTo.contains("/")) {
						DateFormat formatte1 = new SimpleDateFormat("dd/mm/yyyy hh:mm");
						date1 = formatte1.parse(dateTo);
					}
				}
				customerDetails.setDateOfBirth(date1);

				customerDetails.setResidentCountry(data[9]);
				customerDetails.setTaxReportingCountry(data[10]);
				customerDetails.setTaxIdentificationNumber(data[11]);

				String date4 = data[12];
				Date date2 = null;
				if (date4.contains("-")) {
					DateFormat formatter = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
					date2 = formatter.parse(date4);
				}
				if (date4.contains("/")) {
					DateFormat formatte1 = new SimpleDateFormat("dd/mm/yyyy hh:mm");
					date2 = formatte1.parse(date4);
				}
				customerDetails.setCustomerCreationDate(date2);
				customerDetails.setIndustry(data[13]);
				customerDetails.setOccupation(data[14]);
				customerDetails.setPrimaryCitizenShipCountry(data[15]);
				customerDetails.setEmployerName(data[16]);
				customerDetails.setDomiciledOrganisation(data[17]);
				customerDetails.setJurisdiction(data[18]);
				customerDetails.setCountryOfTaxation(data[19]);
				customerDetails.setJobTitle(data[20]);

				Date date3 = null;
				if (data[21].equals("-") || data[21].equals("")) {
					// do nothing
				} else {
					String dateInc = data[21];
					if (dateInc.contains("-")) {
						DateFormat formatter = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
						date3 = formatter.parse(dateInc);
					}
					if (dateInc.contains("/")) {
						DateFormat formatte1 = new SimpleDateFormat("dd/mm/yyyy hh:mm");
						date3 = formatte1.parse(dateInc);
					}

				}
				customerDetails.setDateOfIncorporation(date3);
				customerDetails.setEstimatedTurnoverAmount(Double.valueOf(data[22]));
				customerDetails.setCorporateStructure(data[23]);
				customerDetails.setRowStatus(false);
				try {
					customerDetails = customerDao.fetchCustomerDetails(data[0]);
				}
				catch (NoResultException e) {
					customerDao.create(customerDetails);
			        continue;
				}
				catch (Exception e) {
					logger.info("Customer already exists,So skiiping record");
				}
				
			}
			i++;
		}

		return true;
	}

	@Override
	@Transactional("transactionManager")
	public Boolean uploadCustomerAddressFile(MultipartFile multipartFile) throws IOException, ParseException {
		BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
		String line = null;
		int i = 1;

		while ((line = br.readLine()) != null) {

			if (i != 1) {
				String[] data = null;
				if (line.contains(","))
					data = line.split(",");
				if (line.contains(";"))
					data = line.split(";");
				CustomerDetails customerDetails = new CustomerDetails();
				if (data != null && data[0] != null)
					customerDetails = customerDao.find(Long.valueOf(data[0]));
				// for old csv file
				CustomerAddress address = new CustomerAddress();
				address.setAddressType(AddressType.valueOf(data[1]));
				address.setAddressLine(data[2]);
				address.setCity(data[3]);
				address.setState(data[4]);
				address.setPostalCode(data[5]);
				address.setCountry(data[6]);
				address.setStreet(data[7]);
				address.setCustomerDetails(customerDetails);
				customerAddressDao.create(address);
			}
			i++;
		}

		return true;
	}

	@Override
	@Transactional("transactionManager")
	public Boolean addCorporateStructureAndIndustry() {
		List<String> industry = new ArrayList<String>();
		List<String> corpoarteStructure = new ArrayList<String>();
		industry.add("Politically Exposed Persons (PEP)");
		industry.add("Entities Associated with PEPs");
		industry.add(" Casinos and online gambling");
		industry.add("Asset and wealth management corporation");
		industry.add("Brokers/dealers in securities");
		industry.add(" Offshore corporations and banks");
		industry.add(" Leather goods stores");
		industry.add("Currency exchange houses");
		industry.add("Money remitters");
		industry.add("Check cashers");
		industry.add("Car, boat and plane dealers and manufacturers");
		industry.add("Travel agencies");
		industry.add(" Import/ export companies");
		industry.add("Cash-intensive businesses");
		industry.add("Credit corporation");
		industry.add("Insurance corporation");
		industry.add("Accounting corporation");
		industry.add("Law agency");
		industry.add("Leather goods store");
		industry.add("Currency exchange house");
		industry.add("Investing corporation");
		industry.add("Real estate agency");
		industry.add("Sale of works of art");
		industry.add("Charity");
		industry.add("Check casher");
		corpoarteStructure.add("Private corporation");
		corpoarteStructure.add("Fund");
		corpoarteStructure.add("Charity");
		corpoarteStructure.add("Public company");
		corpoarteStructure.add("Branch of forign/ international corporation");
		corpoarteStructure.add("Trade union");
		corpoarteStructure.add("Trust\n");
		corpoarteStructure.add("State corporation");
		corpoarteStructure.add("Local regulatory authority");
		corpoarteStructure.add("Offshore company");
		corpoarteStructure.add("Charity");
		corpoarteStructure.add("Limited liability company");

		List<CustomerDetails> customerDetails = customerDao.fetchAllCustomers();
		for (CustomerDetails customer : customerDetails) {
			customer.setIndustry(getRandomIndustry(industry));
			customer.setCorporateStructure(getRandomCorpoarteStruc(corpoarteStructure));
			customer.setEstimatedTurnoverAmount((getRandomNo()));
			customerDao.saveOrUpdate(customer);

		}
		return false;
	}

	private String getRandomIndustry(List<String> industry) {

		String indu = null;
		int index = this.random.nextInt(industry.size());
		indu = industry.get(index);
		return indu;
	}

	private String getRandomCorpoarteStruc(List<String> corpoarteStructure) {

		String corp = null;
		int index = this.random.nextInt(corpoarteStructure.size());
		corp = corpoarteStructure.get(index);
		return corp;
	}

	private Double getRandomNo() {
		int Low = 1;
		int High = 10000000;
		int result = this.random.nextInt(High - Low) + Low;
		return (double) (result);
	}

	@Override
	@Async
	@Transactional("transactionManager")
	public Boolean updateCustomer(MultipartFile multipartFile) throws IOException, ParseException {
		BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
		String line = null;
		int i = 1;
		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if (line.contains(","))
					data = line.split(",");
				if (line.contains(";"))
					data = line.split(";");
				CustomerDetails customerDetails = new CustomerDetails();
				if (data != null && data[0] != null)
					customerDetails = customerDao.fetchCustomerDetails(data[0]);
				if (customerDetails != null) {
					customerDetails.setDisplayName(data[6]);
				}
				customerDao.saveOrUpdate(customerDetails);

			} // if close
			i++;
		} // while close

		return true;
	}

	@Override
	@Transactional("transactionManager")
	public CustomerDto customerInfo(Long customerId) {
		return customerDao.customerInfo(customerId);
	}

	@Override
	@Transactional("transactionManager")
	public Boolean updateEstimateAverageAmt() {
		List<CustomerDetails> customerDetailsList = customerDao.fetchAllCustomers();
		for (CustomerDetails customerDetails : customerDetailsList) {
			customerDetails.setEstimatedAverageTurnoverAmount(customerDetails.getEstimatedTurnoverAmount());
			customerDao.saveOrUpdate(customerDetails);
		}
		return null;
	}

	@Override
	@Transactional("transactionManager")
	public CustomerDetails findCustomerById(long customerId) {
		CustomerDetails customerDetails = customerDao.find(customerId);
		return customerDetails;
	}

	@Override
	@Transactional("transactionManager")
	public void updateCustomerDetails(CustomerDetailsDto customerDetailsDto) throws Exception {

		if (customerDetailsDto != null && customerDetailsDto.getId() != 0) {
			CustomerDetails customerDetails = customerDao.find(customerDetailsDto.getId());
			if (null != customerDetails) {
				if (null != customerDetailsDto.getCustomerNumber()) {
					customerDetails.setCustomerNumber(customerDetailsDto.getCustomerNumber());
				}
				if (null != customerDetailsDto.getCustomerType()) {
					customerDetails.setCustomerType(customerDetailsDto.getCustomerType());
				}
				if (null != customerDetailsDto.getCustomerTitle()) {
					customerDetails.setCustomerTitle(customerDetailsDto.getCustomerTitle());
				}
				if (null != customerDetailsDto.getGender()) {
					customerDetails.setGender(customerDetailsDto.getGender());
				}
				if (null != customerDetailsDto.getCustomerTitle()) {
					customerDetails.setCustomerTitle(customerDetailsDto.getCustomerTitle());
				}
				if (null != customerDetailsDto.getDateOfBirth()) {
					customerDetails.setDateOfBirth(customerDetailsDto.getDateOfBirth());
				}
				if (null != customerDetailsDto.getResidentCountry()) {
					customerDetails.setResidentCountry(customerDetailsDto.getResidentCountry());
				}
				if (null != customerDetailsDto.getTaxReportingCountry()) {
					customerDetails.setTaxReportingCountry(customerDetailsDto.getTaxReportingCountry());
				}
				if (null != customerDetailsDto.getTaxIdentificationNumber()) {
					customerDetails.setTaxIdentificationNumber(customerDetailsDto.getTaxIdentificationNumber());
				}
				if (null != customerDetailsDto.getCustomerCreationDate()) {
					customerDetails.setCustomerCreationDate(customerDetailsDto.getCustomerCreationDate());
				}
				if (null != customerDetailsDto.getIndustry()) {
					customerDetails.setIndustry(customerDetailsDto.getIndustry());
				}
				if (null != customerDetailsDto.getOccupation()) {
					customerDetails.setOccupation(customerDetailsDto.getOccupation());
				}
				if (null != customerDetailsDto.getPrimaryCitizenShipCountry()) {
					customerDetails.setPrimaryCitizenShipCountry(customerDetailsDto.getPrimaryCitizenShipCountry());
				}
				if (null != customerDetailsDto.getFirstName()) {
					customerDetails.setFirstName(customerDetailsDto.getFirstName());
				}
				if (null != customerDetailsDto.getMiddleName()) {
					customerDetails.setMiddleName(customerDetailsDto.getMiddleName());
				}
				if (null != customerDetailsDto.getInstitutionName()) {
					customerDetails.setInstitutionName(customerDetailsDto.getInstitutionName());
				}
				if (null != customerDetailsDto.getDisplayName()) {
					customerDetails.setDisplayName(customerDetailsDto.getDisplayName());
				}
				if (null != customerDetailsDto.getEmployerName()) {
					customerDetails.setEmployerName(customerDetailsDto.getEmployerName());
				}
				if (null != customerDetailsDto.getDomiciledOrganisation()) {
					customerDetails.setDomiciledOrganisation(customerDetailsDto.getDomiciledOrganisation());
				}
				if (null != customerDetailsDto.getJurisdiction()) {
					customerDetails.setJurisdiction(customerDetailsDto.getJurisdiction());
				}
				if (null != customerDetailsDto.getCountryOfTaxation()) {
					customerDetails.setCountryOfTaxation(customerDetailsDto.getCountryOfTaxation());
				}
				if (null != customerDetailsDto.getJobTitle()) {
					customerDetails.setJobTitle(customerDetailsDto.getJobTitle());
				}
				if (null != customerDetailsDto.getDateOfIncorporation()) {
					customerDetails.setDateOfIncorporation(customerDetailsDto.getDateOfIncorporation());
				}
				if (null != customerDetailsDto.getEstimatedAverageTurnoverAmount()) {
					customerDetails
							.setEstimatedAverageTurnoverAmount(customerDetailsDto.getEstimatedAverageTurnoverAmount());
				}
				if (null != customerDetailsDto.getEstimatedTurnoverAmount()) {
					customerDetails.setEstimatedTurnoverAmount(customerDetailsDto.getEstimatedTurnoverAmount());
				}
				if (null != customerDetailsDto.getAmlFraud()) {
					customerDetails.setAmlFraud(customerDetailsDto.getAmlFraud());
				}
				if (null != customerDetailsDto.getSnit()) {
					customerDetails.setSnit(customerDetailsDto.getSnit());
				}
				if (null != customerDetailsDto.getWatchList()) {
					customerDetails.setWatchList(customerDetailsDto.getWatchList());
				}
				if (null != customerDetailsDto.getSar()) {
					customerDetails.setSar(customerDetailsDto.getSar());
				}
				if (null != customerDetailsDto.getBusinessesActivityCountry()) {
					customerDetails.setBusinessesActivityCountry(customerDetailsDto.getBusinessesActivityCountry());
				}
				if (null != customerDetailsDto.getBusinesses()) {
					customerDetails.setBusinesses(customerDetailsDto.getBusinesses());
				}
				if (null != customerDetailsDto.getProductType()) {
					customerDetails.setProductType(customerDetailsDto.getProductType());
				}
				if (null != customerDetailsDto.getAmlRisk()) {
					customerDetails.setAmlRisk(customerDetailsDto.getAmlRisk());
				}
				if (null != customerDetailsDto.getCorporateStructure()) {
					{
						customerDetails.setCorporateStructure(customerDetailsDto.getCorporateStructure());
					}
					customerDao.update(customerDetails);
				} else {
					throw new Exception("Customer Details are invalid");
				}
			}
		} else {
			throw new Exception("Customer Id cannot be empty");
		}

	}

}
