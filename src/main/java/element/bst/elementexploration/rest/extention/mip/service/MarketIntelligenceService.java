package element.bst.elementexploration.rest.extention.mip.service;

import element.bst.elementexploration.rest.extention.mip.domain.OrgWithFacts;

public interface MarketIntelligenceService {

	String getOrgWithFacts(OrgWithFacts orgWithFacts) throws Exception;

	//String searchEntity(String entity) throws Exception;

}
