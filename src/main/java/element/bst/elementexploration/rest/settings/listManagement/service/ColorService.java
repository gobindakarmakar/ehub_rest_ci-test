package element.bst.elementexploration.rest.settings.listManagement.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.settings.listManagement.domain.Color;
import element.bst.elementexploration.rest.settings.listManagement.dto.ColorDto;

public interface ColorService extends GenericService<Color, Long> {

	List<ColorDto> getAllColors();
}
