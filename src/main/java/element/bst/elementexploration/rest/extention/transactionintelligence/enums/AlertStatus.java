package element.bst.elementexploration.rest.extention.transactionintelligence.enums;

/**
 * @author suresh
 *
 */
public enum AlertStatus {
	
	OPEN ,CLOSE , UNDER_REVIEW , ESCALATED ,REPORTED

}
