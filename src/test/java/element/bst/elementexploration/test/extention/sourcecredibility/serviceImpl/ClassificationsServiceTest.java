package element.bst.elementexploration.test.extention.sourcecredibility.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.ClassificationsDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl.ClassificationsServiceImpl;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ClassificationsServiceTest {
	
	
	@Mock
	ClassificationsDao classificationsDaoMock;
	
	@InjectMocks
	ClassificationsServiceImpl classificationsService;
	
	List<Classifications> classifications = new ArrayList<Classifications>();
	Classifications classificationsOne = null;
	
	@Before
    public void setupMock() {
		classificationsOne = new Classifications(1L,"GENERAL");
		Classifications classificationsTwo = new Classifications(2L,"NEWS");
		classifications.add(classificationsOne);
		classifications.add(classificationsTwo);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getClassifications() {
		Mockito.when(classificationsDaoMock.findAll()).thenReturn(classifications);
		List<ClassificationsDto> classificationsCheck =  classificationsService.getClassifications();
		Assert.assertEquals(classificationsCheck.size(),classifications.size());
	}
	
	@Test
	public void fetchClassification() {
		Mockito.when(classificationsDaoMock.fetchClassification(Mockito.anyString())).thenReturn(classificationsOne);
		Classifications classificationsCheck =  classificationsService.fetchClassification("general");
		Assert.assertEquals(classificationsCheck.getClassifcationName(),classificationsOne.getClassifcationName());
	}

}
