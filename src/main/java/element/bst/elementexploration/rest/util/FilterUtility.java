package element.bst.elementexploration.rest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;

@Component
public class FilterUtility {


	public StringBuilder filterColumns(FilterDto dto) {

		StringBuilder filter = new StringBuilder();
		
		if (dto.isAlertsByPeriodgreaterthan30Days())
			filter.append(" and DATE(t.alertBusinessDate) >=:fromDate and DATE(t.alertBusinessDate) <DATE_ADD(:toDate,-30,DAY) ");
		
		if (dto.isAlertsByPeriod10to20Days())
		filter.append(" and (DATE(t.alertBusinessDate) >DATE_ADD(:toDate,-20,DAY) and DATE(t.alertBusinessDate) <DATE_ADD(:toDate,-10,DAY))");
		
		if (dto.isAlertsByPeriod20to30Days())
			filter.append(" and (DATE(t.alertBusinessDate) >DATE_ADD(:toDate,-10,DAY) and DATE(t.alertBusinessDate) >DATE_ADD(:toDate,0,DAY))");

		if (dto.isAlertByStatusGreaterThan30Days())
			filter.append(" and DATE(t.alertBusinessDate) >=:fromDate and DATE(t.alertBusinessDate) <DATE_ADD(:toDate,-30,DAY)");

		if (dto.getTransactionType() != null || dto.getProductRiskType() != null) 
		{
			filter.append(" and t.tranactionProductType is not null and t.tranactionProductType IN (:transProductType)");
		}
		if (dto.isCorporateStructure())
			filter.append(" and t.benificiaryCorporateStructure is not null and t.benificiaryCustomerType='CORP' ");
		if (dto.isShareholder())
			filter.append(" and t.shareHolderName is not null and t.benificiaryCustomerType = 'CORP' ");
		if (dto.getCorporateStructureType() != null || dto.getShareholderType() != null)
		{
			if(dto.getCorporateStructureType() != null)
				filter.append(" and t.benificiaryCorporateStructure IN(:corporateStructureType)");
			if(dto.getShareholderType() != null)
				filter.append(" and t.benificiaryCorporateStructure IN(:shareholderType)");
		}
		if (dto.getCustomerName() != null)
			filter.append(" and t.benificiaryCustomerName is not null and t.originatorCustomerName is not null and (t.alertCustomerDisplayName IN(:displayName)) ");
		
		if (dto.getCounterPartyNames() != null)
			filter.append(" and t.benificiaryCustomerName is not null  and t.benificiaryCustomerName IN(:counterPartyNames)");
		
		if (dto.getScenario() != null)
			filter.append(" and t.alertScenario is not null and t.alertScenario IN(:scenario)");

		if (dto.getCountry() != null && dto.isInputCountry())
			filter.append(" and t.benificiaryCountry is not null  and t.benificiaryCountry IN(:country)");
		
		if (dto.getCountry() != null && dto.isOutputCountry())
			filter.append(" and t.originatorCountry is not null  and t.originatorCountry IN(:country)");
		
		
		if (dto.getCountry() != null && (!dto.isInputCountry() && !dto.isOutputCountry()) || dto.getPoliticalRiskType() !=null 
				|| dto.getCorruptionRiskType() != null )
			filter.append(" and t.benificiaryCountry is not null  and t.benificiaryCountry IN(:country)");
		
		
		/*if (dto.isAlertsByPeriodgreaterthan30Days())
			filter.append(
					" and (t.alertBusinessDate >=:fromDate and t.alertBusinessDate <DATE_ADD(str_to_date(:toDate,'%Y-%m-%d') , INTERVAL -30 DAY)) ");
		if (dto.isAlertsByPeriod10to20Days())
			filter.append(
					" and (t.alertBusinessDate >DATE_ADD(str_to_date(:toDate,'%Y-%m-%d') , INTERVAL -20 DAY) and t.alertBusinessDate <DATE_ADD(str_to_date(:toDate,'%Y-%m-%d') , INTERVAL -10 DAY))");
		if (dto.isAlertsByPeriod20to30Days())
			filter.append(
					" and (t.alertBusinessDate >DATE_ADD(str_to_date(:toDate,'%Y-%m-%d') , INTERVAL -10 DAY) and t.alertBusinessDate >DATE_ADD(str_to_date(:toDate,'%Y-%m-%d') , INTERVAL 0 DAY))");

		if (dto.isAlertByStatusGreaterThan30Days())
			filter.append(
					" and (t.alertBusinessDate >=:fromDate and t.alertBusinessDate <DATE_ADD(str_to_date(:toDate,'%Y-%m-%d') , INTERVAL -30 DAY))");
*/
		if(dto.isAboveTenMillionUSD())
			filter.append(" and t.benificiaryEstimatedAmount >10000000");
		if(dto.isBelowOneMillionUSD())
			filter.append(" and t.benificiaryEstimatedAmount <1000000");
		if(dto.isBetweenFiveToTenMillionUSD())
			filter.append(" and t.benificiaryEstimatedAmount >5000000 and t.benificiaryEstimatedAmount<10000000");
		if(dto.isBetweenOneToFiveMillionUSD())
			filter.append(" and t.benificiaryEstimatedAmount >1000000 and t.benificiaryEstimatedAmount<5000000");
		if(dto.getAlertStatus()!=null)
			filter.append(" and t.alertStatus=:alertStatus ");
		if (dto.isInput())
			filter.append(" and t.benificiaryCustomerId is not null");
		if (dto.isOutput())
			filter.append(" and t.originatorCustomerId is not null");

		if (dto.isCorporateGeography() || dto.isCounterPartyCountry())
			filter.append(" and t.benificiaryCountry is not null");
		
		if (dto.getCorporateGeographyType() != null || dto.getBankLocations() != null || dto.getCounterpartyCountry() != null)
			filter.append(" and t.benificiaryCountry is not null and t.benificiaryCountry IN(:location)");

		if (dto.getBankName() != null)
			filter.append(" and t.benificiaryBankName is not null and t.benificiaryBankName IN(:bankName)");

		if(dto.isBank())
			filter.append(" and t.benificiaryBankName is not null");
		
		if (dto.isCustomerRiskType() || dto.isActivityType())
			filter.append(" and t.benificiaryIndustry is not null");
		
		if (dto.isProductRisk())
			filter.append(" and t.tranactionProductType is not null");
		
		if (dto.isGeographyRiskType()) {
			String queryforCondition = " and (t.fatfRisk>0 or t.taskJusticeRisk>0 or t.internationalNarcoticsRisk>0)";
			filter.append(queryforCondition);
		}
		
		if (dto.getCustomerRisk() != null || dto.getActivityCategory() != null)
			filter.append(" and t.benificiaryIndustry is not null and t.benificiaryIndustry in (:type)");
		
		if (dto.getGeoRiskType() != null) {
			if (dto.getGeoRiskType().equals("FATF High Risk Countries")) {
				filter.append(" and t.fatfRisk>0");

			} else if (dto.getGeoRiskType()
					.equals("Tax Justice Network High Risk Countries (Finacial Secracy Index)")) {
				filter.append(" and t.taskJusticeRisk > 0");

			} else if (dto.getGeoRiskType().equals("International Narcotics Control High Risk Countries")) {
				filter.append(" and t.internationalNarcoticsRisk >0");

			}
		}
		/*if (dto.getCorruptionRiskType() != null) {
			
			
			if (dto.getCorruptionRiskType().equals("Transparancy International High Risk Countries")) {
				filter.append(" and t.transparencyRisk>0");

			} else if (dto.getCorruptionRiskType().equals("World Bank High Risk Countries")) {
				filter.append(" and t.worldBankRisk > 0");

			} else if (dto.getCorruptionRiskType().equals("WEF Global Competitiveness High Risk Countries")) {
				filter.append(" and t.wefRisk >0");

			}
		}*/
		/*if (dto.getPoliticalRiskType() != null) {
			if (dto.getPoliticalRiskType().equals("Freedom House - Freedom in the World High Risk Countries")) {
				filter.append(" and t.freedomHouserisk>0");

			} else if (dto.getPoliticalRiskType().equals("World Justice Project")) {
				filter.append(" and t.worldJusticeRisk > 0");

			} else if (dto.getPoliticalRiskType().equals("WEF Global Competitiveness High Risk Countries")) {
				filter.append(" and t.wefRisk >0");

			}
		}*/

		return filter;
	}

	public Query<?> queryReturn(Query<?> query, FilterDto dto, String fromDate, String toDate) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Query<?> queryToSent = query;

		if (dto.getTransactionType() != null )
			queryToSent.setParameter("transProductType", getList(dto.getTransactionType()));
		if(dto.getProductRiskType() != null)
			queryToSent.setParameter("transProductType", getList(dto.getProductRiskType()));

		if (dto.getCorporateStructureType() != null)
			queryToSent.setParameter("corporateStructureType", getList(dto.getCorporateStructureType()));

		if (dto.getCustomerName() != null)
			queryToSent.setParameter("displayName", getList(dto.getCustomerName()));

		if (dto.getScenario() != null)
			queryToSent.setParameter("scenario", getList(dto.getScenario()));

		if (dto.getCountry() != null)
			queryToSent.setParameter("country", getList(dto.getCountry()));
		
		if (dto.getCorruptionRiskType() != null)
			queryToSent.setParameter("country", getList(dto.getCorruptionRiskType()));
		if (dto.getPoliticalRiskType() != null)
			queryToSent.setParameter("country", getList(dto.getPoliticalRiskType()));

		if (dto.getShareholderType() != null)
			queryToSent.setParameter("shareholderType", getList(dto.getShareholderType()));

		if (dto.isAlertsByPeriodgreaterthan30Days()) {
			queryToSent.setParameter("fromDate", formatter.parse(fromDate));
			queryToSent.setParameter("toDate", formatter.parse(toDate));
		}
		if (dto.isAlertsByPeriod10to20Days())
				queryToSent.setParameter("toDate", formatter.parse(toDate));
		if (dto.isAlertsByPeriod20to30Days())
				queryToSent.setParameter("toDate", formatter.parse(toDate));

		if (dto.isAlertByStatusGreaterThan30Days()) {
			queryToSent.setParameter("fromDate", formatter.parse(fromDate));
			queryToSent.setParameter("toDate", formatter.parse(toDate));
		}

		if (dto.getCorporateGeographyType() != null || dto.getBankLocations() != null
				|| dto.getCounterpartyCountry() != null) {
			if (dto.getCorporateGeographyType() != null)
				queryToSent.setParameter("location", getList(dto.getCorporateGeographyType()));
			if (dto.getBankLocations() != null)
				queryToSent.setParameter("location", getList(dto.getBankLocations()));
			if (dto.getCounterpartyCountry() != null)
				queryToSent.setParameter("location", getList(dto.getCounterpartyCountry()));
		}
		if (dto.getBankName() != null)
			queryToSent.setParameter("bankName", getList(dto.getBankName()));

		if (dto.getCustomerRisk() != null || dto.getActivityCategory() != null) {
			if (dto.getCustomerRisk() != null)
				queryToSent.setParameter("type", getList(dto.getCustomerRisk()));
			if (dto.getActivityCategory() != null)
				queryToSent.setParameter("type", getList(dto.getActivityCategory()));
		}
		
		if (dto.getCounterPartyNames() != null)
			queryToSent.setParameter("counterPartyNames",getList(dto.getCounterPartyNames()));
		
		if(dto.getAlertStatus()!=null)
			queryToSent.setParameter("alertStatus",getList(dto.getAlertStatus()));

		return queryToSent;

	}

	public List<String> getList(String type) {
		String givenType = type;
		List<String> listTypes = new ArrayList<String>();
		if (givenType != null && givenType != "")
			listTypes = Arrays.asList(givenType.split(","));
		return listTypes;
	}

}
