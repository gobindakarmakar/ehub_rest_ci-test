package element.bst.elementexploration.rest.document.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dao.DocumentCaseMppingDao;
import element.bst.elementexploration.rest.document.domain.DocumentVaultCaseMapping;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("documentCaseMppingDao")
public class DocumentCaseMppingDaoImpl extends GenericDaoImpl<DocumentVaultCaseMapping, Long>
		implements DocumentCaseMppingDao {

	public DocumentCaseMppingDaoImpl() {
		super(DocumentVaultCaseMapping.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseDto> getSeedByDocumentId(Long docId) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select new element.bst.elementexploration.rest.casemanagement.dto.CaseDto(c.caseId, c.name,c.description, c.remarks, c.priority, c.currentStatus, c.health, c.directRisk, c.indirectRisk, c.transactionalRisk, c.createdOn, c.createdBy.userId, c.modifiedOn, c.modifiedBy, c.type) ");
		sb.append("from Case c,DocumentVaultCaseMapping cm ");
		sb.append("where cm.docId=:docId ");
		sb.append("and cm.caseId=c.caseId ");
		sb.append("and cm.isAssociated=true");
		List<CaseDto> seedList = new ArrayList<CaseDto>();
		try {
			Query<?> query = this.getCurrentSession().createQuery(sb.toString());
			query.setParameter("docId", docId);
			seedList = (List<CaseDto>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e,
					this.getClass());
		} 
		return seedList;
	}

	@Override
	public DocumentVaultCaseMapping findByCaseIdAndDocId(Long docId, Long caseseedId) {
		DocumentVaultCaseMapping vaultCaseMapping = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<DocumentVaultCaseMapping> query = builder.createQuery(DocumentVaultCaseMapping.class);
			Root<DocumentVaultCaseMapping> doccasemapping = query.from(DocumentVaultCaseMapping.class);
			query.select(doccasemapping);
			query.where(builder.and(builder.equal(doccasemapping.get("docId"), docId),
					builder.equal(doccasemapping.get("caseId"), caseseedId)));
			 vaultCaseMapping = this.getCurrentSession().createQuery(query).getSingleResult();			
		} catch (NoResultException e) {
			return  vaultCaseMapping;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					DocumentCaseMppingDaoImpl.class);
		}
		return vaultCaseMapping;
	}
}
