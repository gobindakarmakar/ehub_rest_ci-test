package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface CustomerDetailsDao extends GenericDao<CustomerDetails, Long> {
	CustomerDetails fetchCustomerDetails(String customerNumber) throws NoResultException;

	List<CustomerDetails> fetchAllCustomers();

	List<CustomerDetails> fetchMulCustomerDetails(String entityId);

	CustomerDetails getCustomerDetails(String customerNumber, String locations) throws NoResultException;

	List<CustomerDetails> findByCustomerNumberInList(List<String> customerIds);
	
	
	List<CustomerDetails> findByCustomerIdInList(Set<Long> customerIds);
	List<CustomerDetails> findCorporateCustomers();

	CustomerDto customerInfo(Long customerId);
 
}
