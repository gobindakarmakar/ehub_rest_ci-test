package element.bst.elementexploration.rest.extention.docparser.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import net.sourceforge.tess4j.TesseractException;

public class DocumentParserServiceTest {
	
	@Mock
	DocumentParserService documentParserServiceMock;
	
	public List<OverAllResponseDto> overAllResponseDtoList;
	
	public DocumentTemplates documentTemplates;
	
	public CaseMapDocIdDto caseMapDocIdDto;
	
	@Before
    public void setupMock() {
		overAllResponseDtoList = new ArrayList<OverAllResponseDto>();
		OverAllResponseDto overAllResponseDto1 = new OverAllResponseDto();
		OverAllResponseDto overAllResponseDto2 = new OverAllResponseDto();
		overAllResponseDtoList.add(overAllResponseDto1);
		overAllResponseDtoList.add(overAllResponseDto2);
		documentTemplates = new DocumentTemplates();
		caseMapDocIdDto = new CaseMapDocIdDto();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void findQuestionsTest() {	
		Mockito.when(documentParserServiceMock.getDocumentParseData(Mockito.anyLong())).thenReturn(overAllResponseDtoList);
		List<OverAllResponseDto> newOverAllResponseDtoList = documentParserServiceMock.getDocumentParseData(Mockito.anyLong());
		assertEquals(2,newOverAllResponseDtoList.size());
	}
	
	@Test
	public void setRiskScoreToQuestionsTest() {	
		Mockito.when(documentParserServiceMock.setRiskScoreToQuestions(Mockito.anyLong())).thenReturn(true);
		boolean status = documentParserServiceMock.setRiskScoreToQuestions(Mockito.anyLong());
		assertEquals(true,status);
	}
	
	@Test
	public void compareFileWithJsonTemplateTest() throws IllegalStateException, InvalidFormatException, IOException, URISyntaxException, ParseException {	
		Mockito.when(documentParserServiceMock.compareFileWithJsonTemplate(Mockito.any(MultipartFile.class),Mockito.anyString())).thenReturn(true);
		boolean status = documentParserServiceMock.compareFileWithJsonTemplate(Mockito.any(MultipartFile.class),Mockito.anyString());
		assertEquals(true,status);
	}
	
	@Test
	public void saveDocumentTemplatesTest() throws IllegalStateException, InvalidFormatException, IOException {	
		Mockito.when(documentParserServiceMock.saveDocumentTemplates(Mockito.any(MultipartFile.class),Mockito.anyString())).thenReturn(documentTemplates);
		DocumentTemplates newDocumentTemplates = documentParserServiceMock.saveDocumentTemplates(Mockito.any(MultipartFile.class),Mockito.anyString());
		Assert.assertNotNull(newDocumentTemplates);
	}
	
	@Test
	public void saveDocumetnAnswersTest() throws IllegalStateException, InvalidFormatException, IOException {	
		Mockito.when(documentParserServiceMock.saveDocumetnAnswers(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.anyLong())).thenReturn(caseMapDocIdDto);
		CaseMapDocIdDto newCaseMapDocIdDto = documentParserServiceMock.saveDocumetnAnswers(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.anyLong());
		Assert.assertNotNull(newCaseMapDocIdDto);
	}
	
	@Test
	public void saveDocumetnAnswersCsvTest() throws IllegalStateException, InvalidFormatException, IOException {	
		Mockito.when(documentParserServiceMock.saveDocumetnAnswersCsv(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.anyLong())).thenReturn(caseMapDocIdDto);
		CaseMapDocIdDto newCaseMapDocIdDto = documentParserServiceMock.saveDocumetnAnswersCsv(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.anyLong());
		Assert.assertNotNull(newCaseMapDocIdDto);
	}
	
	@Test
	public void saveDocumetnAnswersPdfTest() throws IllegalStateException, InvalidFormatException, IOException, TesseractException {	
		Mockito.when(documentParserServiceMock.saveDocumetnAnswersPdf(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.any(DocumentTemplates.class),Mockito.any(byte[].class))).thenReturn(caseMapDocIdDto);
		CaseMapDocIdDto newCaseMapDocIdDto = documentParserServiceMock.saveDocumetnAnswersPdf(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.any(DocumentTemplates.class),Mockito.any(byte[].class));
		Assert.assertNotNull(newCaseMapDocIdDto);
	}
	

	@Test
	public void compareFileWithDocumentTemplateTest() throws IllegalStateException, InvalidFormatException, IOException, TesseractException {	
		Mockito.when(documentParserServiceMock.compareFileWithDocumentTemplate(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.any(byte[].class))).thenReturn(documentTemplates);
		DocumentTemplates newDocumentTemplates = documentParserServiceMock.compareFileWithDocumentTemplate(Mockito.any(MultipartFile.class),Mockito.anyLong(),Mockito.anyLong(),Mockito.any(byte[].class));
		Assert.assertNotNull(newDocumentTemplates);
	}
	
	@Test
	public void getUpdatedISODocumentParseDataTest() {	
		Mockito.when(documentParserServiceMock.getUpdatedISODocumentParseData(Mockito.anyLong())).thenReturn(true);
		boolean status = documentParserServiceMock.getUpdatedISODocumentParseData(Mockito.anyLong());
		assertEquals(true,status);
	}
	
	@Test
	public void getTemplateQandATest() {	
		Mockito.when(documentParserServiceMock.getTemplateQandA(Mockito.anyLong())).thenReturn(overAllResponseDtoList);
		List<OverAllResponseDto> newOverAllResponseDtoList = documentParserServiceMock.getTemplateQandA(Mockito.anyLong());
		assertEquals(2,newOverAllResponseDtoList.size());
	}
	
}
