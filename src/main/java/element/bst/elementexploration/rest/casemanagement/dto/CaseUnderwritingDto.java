package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("CaseUnderwriting")
public class CaseUnderwritingDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "The ID of the case")
	private Long caseId;

	@ApiModelProperty(value = "The name of the case")
	private String name;

	@ApiModelProperty(value = "Document ID")
	private Long docId;

	@ApiModelProperty(value = "Document file name")
	private String fileName;

	@ApiModelProperty(value = "Industry")
	private String industry;

	@ApiModelProperty(value = "Broker")
	private String broker;

	@ApiModelProperty(value = "The current status of the case")
	private Integer currentStatus;

	@ApiModelProperty(value = "Document type")
	private String docType;

	@ApiModelProperty(value = "Document size")
	private Long size;

	@ApiModelProperty(value = "Case assigned to")
	private String assignedTo;
	
	@ApiModelProperty(value = "Case assigned to")
	private Long assigneeId;
	
	private Date createdOn;

	public CaseUnderwritingDto(Long caseId, String name, Long docId, Integer currentStatus) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.docId = docId;
		this.currentStatus = currentStatus;
	}
	
	public CaseUnderwritingDto(Long caseId, String name, Long docId, Integer currentStatus,Date createdOn) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.docId = docId;
		this.currentStatus = currentStatus;
		this.createdOn=createdOn;
	}

	public CaseUnderwritingDto(Long caseId, String name, Long docId, Integer currentStatus, Date createdOn,
			String industry) {
		super();
		this.caseId = caseId;
		this.name = name;
		this.docId = docId;
		this.currentStatus = currentStatus;
		this.createdOn = createdOn;
		this.industry = industry;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public Integer getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Integer currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
