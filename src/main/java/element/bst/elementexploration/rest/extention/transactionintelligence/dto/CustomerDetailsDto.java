package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerTitle;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.Gender;

 public class CustomerDetailsDto  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String customerNumber;
	private CustomerType customerType;
	private CustomerTitle customerTitle;
	private Gender gender;
	private Date dateOfBirth;
	private String residentCountry;
	private String taxReportingCountry;
	private String taxIdentificationNumber;
	private Date customerCreationDate;
	private String industry;
	private String occupation;
	private String primaryCitizenShipCountry;
	private String firstName;
	private String middleName;
	private String institutionName;
	private String displayName;
	private String employerName;
	private String domiciledOrganisation;
	private String jurisdiction;
	private String countryOfTaxation;
	private String jobTitle;
	private Date   dateOfIncorporation;
	private Double estimatedAverageTurnoverAmount;
	private Double estimatedTurnoverAmount;
	private String amlFraud;
	private String snit;
	private String watchList;
	private String sar;
	private String businessesActivityCountry;
	private String businesses;
	private String productType;
	private Double amlRisk;
	private String corporateStructure;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public CustomerType getCustomerType() {
		return customerType;
	}
	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}
	public CustomerTitle getCustomerTitle() {
		return customerTitle;
	}
	public void setCustomerTitle(CustomerTitle customerTitle) {
		this.customerTitle = customerTitle;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getResidentCountry() {
		return residentCountry;
	}
	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}
	public String getTaxReportingCountry() {
		return taxReportingCountry;
	}
	public void setTaxReportingCountry(String taxReportingCountry) {
		this.taxReportingCountry = taxReportingCountry;
	}
	public String getTaxIdentificationNumber() {
		return taxIdentificationNumber;
	}
	public void setTaxIdentificationNumber(String taxIdentificationNumber) {
		this.taxIdentificationNumber = taxIdentificationNumber;
	}
	public Date getCustomerCreationDate() {
		return customerCreationDate;
	}
	public void setCustomerCreationDate(Date customerCreationDate) {
		this.customerCreationDate = customerCreationDate;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getPrimaryCitizenShipCountry() {
		return primaryCitizenShipCountry;
	}
	public void setPrimaryCitizenShipCountry(String primaryCitizenShipCountry) {
		this.primaryCitizenShipCountry = primaryCitizenShipCountry;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;	
	}
	public String getEmployerName() {
       return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	public String getDomiciledOrganisation() {
		return domiciledOrganisation;
	}
	public void setDomiciledOrganisation(String domiciledOrganisation) {
		this.domiciledOrganisation = domiciledOrganisation;
	}
	public String getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
	public String getCountryOfTaxation() {
		return countryOfTaxation;
	}
	public void setCountryOfTaxation(String countryOfTaxation) {
		this.countryOfTaxation = countryOfTaxation;
	}
	public String getJobTitle() {		
		return jobTitle;
	}
   public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Date getDateOfIncorporation() {
		return dateOfIncorporation;
	}
	public void setDateOfIncorporation(Date dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}
	public Double getEstimatedAverageTurnoverAmount() {
		return estimatedAverageTurnoverAmount;
	}
	public void setEstimatedAverageTurnoverAmount(Double estimatedAverageTurnoverAmount) {
		this.estimatedAverageTurnoverAmount = estimatedAverageTurnoverAmount;
	}
	public Double getEstimatedTurnoverAmount() {
		return estimatedTurnoverAmount;
	}
	public void setEstimatedTurnoverAmount(Double estimatedTurnoverAmount) {
		this.estimatedTurnoverAmount = estimatedTurnoverAmount;
	}
	public String getAmlFraud() {
		return amlFraud;
	}
	public void setAmlFraud(String amlFraud) {
		this.amlFraud = amlFraud;
	}
	public String getSnit() {
		return snit;
	}
	public void setSnit(String snit) {
		this.snit = snit;
	}
	public String getWatchList() {
		return watchList;
	}	
	public void setWatchList(String watchList) {
		this.watchList = watchList;
    }
	public String getSar() {
		return sar;
	}
	public void setSar(String sar) {
		this.sar = sar;
	}
	public String getBusinessesActivityCountry() {
		return businessesActivityCountry;
    }
	public void setBusinessesActivityCountry(String businessesActivityCountry) {
		this.businessesActivityCountry = businessesActivityCountry;
	}
	public String getBusinesses() {
		return businesses;
	}
	public void setBusinesses(String businesses) {
		this.businesses = businesses;
	}
	public String getProductType() {
          return productType;
    }
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public Double getAmlRisk() {
		return amlRisk;
	}
	public void setAmlRisk(Double amlRisk) {
		this.amlRisk = amlRisk;
	}
	public String getCorporateStructure() {
		return corporateStructure;
	}
	public void setCorporateStructure(String corporateStructure) {
		this.corporateStructure = corporateStructure;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		

}