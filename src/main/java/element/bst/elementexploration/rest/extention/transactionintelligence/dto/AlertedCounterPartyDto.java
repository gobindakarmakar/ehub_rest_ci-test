package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

public class AlertedCounterPartyDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Double amount;
	private String country;
	private String location;
	private String transactionType;
	private Date date;
	private String counterParty;
	private String counterPartyCountry;
	private String counterPartyBank;
	private String bankLocation;
	private String customerName;
	@SuppressWarnings("unused")
	private Long count;
	private Double turnOver;
	private String customerActivity;
	private String addressLine;
	private Long counterPartyId;
	private Long counterPartyCountryId;
	private String latitude;
	private String longitude;
	
	
	public AlertedCounterPartyDto() {
	}

	public AlertedCounterPartyDto(Long id,Date date, String transactionType, String counterParty, String counterPartyBank,String country, Double amount
			,Long counterPartyId,Long counterPartyCountryId,String latitude,String longitude) {
		super();
		this.id=id;
		this.date = date;
		this.transactionType = transactionType;
		this.counterParty = counterParty;
		this.counterPartyBank=counterPartyBank;
		this.country = country;
		this.amount = amount;
		this.counterPartyId=counterPartyId;
		this.counterPartyCountryId=counterPartyCountryId;
		this.latitude=latitude;
		this.longitude=longitude;
	}
	

	public AlertedCounterPartyDto(Long id,Date date, String transactionType, String counterParty, String country, Double amount,
			String counterPartyBank, String bankLocation) {
		super();
		this.id = id;
		this.date = date;
		this.transactionType = transactionType;
		this.counterParty = counterParty;
		this.country = country;
		this.amount = amount;
		this.counterPartyBank = counterPartyBank;
		this.bankLocation = bankLocation;
	}

	public Double getTurnOver() {
		return turnOver;
	}

	public void setTurnOver(Double turnOver) {
		this.turnOver = turnOver;
	}

	public String getCustomerActivity() {
		return customerActivity;
	}

	public void setCustomerActivity(String customerActivity) {
		this.customerActivity = customerActivity;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getId() {
		return id;
	}

	public AlertedCounterPartyDto(Long id, Double amount, String country, String location, String transactionType,
			Date date, String counterParty, String counterPartyCountry, String counterPartyBank, String bankLocation,
			Double turnover, String customerActivity, String addressLine) {
		super();
		this.id = id;
		this.amount = amount;
		this.country = country;
		this.location = location;
		this.transactionType = transactionType;
		this.date = date;
		this.counterParty = counterParty;
		this.counterPartyCountry = counterPartyCountry;
		this.counterPartyBank = counterPartyBank;
		this.bankLocation = bankLocation;
		this.turnOver = turnover;
		this.customerActivity = customerActivity;
		this.addressLine = addressLine;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(String counterParty) {
		this.counterParty = counterParty;
	}

	public String getCounterPartyCountry() {
		return counterPartyCountry;
	}

	public void setCounterPartyCountry(String counterPartyCountry) {
		this.counterPartyCountry = counterPartyCountry;
	}

	public String getCounterPartyBank() {
		return counterPartyBank;
	}

	public void setCounterPartyBank(String counterPartyBank) {
		this.counterPartyBank = counterPartyBank;
	}

	public String getBankLocation() {
		return bankLocation;
	}

	public void setBankLocation(String bankLocation) {
		this.bankLocation = bankLocation;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getCounterPartyId() {
		return counterPartyId;
	}

	public void setCounterPartyId(Long counterPartyId) {
		this.counterPartyId = counterPartyId;
	}

	public Long getCounterPartyCountryId() {
		return counterPartyCountryId;
	}

	public void setCounterPartyCountryId(Long counterPartyCountryId) {
		this.counterPartyCountryId = counterPartyCountryId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
