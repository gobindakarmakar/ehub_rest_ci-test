package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CountryDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("counrtryDao")
public class CountryDaoImpl extends GenericDaoImpl<Country, Long> implements CountryDao {

	public CountryDaoImpl() {
		super(Country.class);
	}
	
	@Override
	public Country findByName(String countryCode) {
		Country country=null;
			try{
				country=(Country) this.getCurrentSession().createQuery("from Country where iso2Code='"+countryCode+"'").getSingleResult();
				return country;
			}catch (NoResultException e) {
				return country;
			} 
	}

	@Override
	public Country getCountry(String location) throws NoResultException {
		Country countryObj=null;
		try
		{
		StringBuilder hql = new StringBuilder();
		hql.append("select c from Country c where c.country=:location");
		Query<?>query=getCurrentSession().createQuery(hql.toString());
		query.setParameter("location", location);
		countryObj=(Country) query.getSingleResult();
		}
		catch (NoResultException e) 
		{
			return countryObj;
		}
		catch (Exception  e) 
		{
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return countryObj;
	}
}
