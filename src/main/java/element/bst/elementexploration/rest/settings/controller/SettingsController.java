package element.bst.elementexploration.rest.settings.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.fileupload.service.FileService;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.dto.AttributeDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsRequestDto;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "System Settings API" })
@RestController
@RequestMapping("/api/systemSettings")
public class SettingsController extends BaseController {

	@Autowired
	SettingsService settingsService;
	
	@Autowired
	private FileService fileService;

	@ApiOperation("Get System Settings list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getSystemSettings", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSystemSettings(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = false) String searchKey) throws Exception {
		return new ResponseEntity<>(settingsService.fetchSystemSettings(searchKey), HttpStatus.OK);
	}

	@ApiOperation("System Settings update")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateSystemSettings", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateSystemSettings(HttpServletRequest request, @RequestParam String token,
			@RequestBody SystemSettingsRequestDto requestDto) throws Exception {
		boolean value = settingsService.updateSystemSettings(requestDto);
		return new ResponseEntity<>(value, HttpStatus.OK);
	}

	@ApiOperation("System Settings attributes")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/addAttributes", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> addAttributes(HttpServletRequest request, @RequestParam String token,
			@RequestBody AttributeDto attributeDto) throws Exception {
		boolean value = settingsService.addAttributes(attributeDto);
		return new ResponseEntity<>(value, HttpStatus.OK);
	}

	@ApiOperation("Get System Settings list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getSystemSettingsByType", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSystemSettingsByType(HttpServletRequest request, @RequestParam String token, @RequestParam(required=true) String settingType) throws Exception {
			return new ResponseEntity<>(settingsService.fetchSystemSettingsByType(settingType),HttpStatus.OK);
	}
	
	@ApiOperation("Get Basic Settings")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getBasicSettings", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getBasicSettings(HttpServletRequest request) throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<Settings>settings = settingsService.getBasicSettings();
		if (settings != null) {
			JSONArray settingJson=new JSONArray(settingsService.getBasicSettings());
			message.setData(settingJson.toString());
			message.setResponseMessage(ElementConstants.FETCH_BASIC_SETTINGS_SUCCESSFUL);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
		} else {
			message.setResponseMessage(ElementConstants.NO_DATA_FOUND_MSG);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}

		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@ApiOperation("Download Json File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	@GetMapping(value = "/downloadFileJsonFromJson", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> downloadFileJsonFromJson(HttpServletResponse response, @RequestParam String fileName,
			 HttpServletRequest request) throws DocNotFoundException, Exception {
		FileBst files = fileService.findFileByName(fileName);
		if (files != null) {
			byte[] fileData = files.getFile();
			String fileDataString = new String(fileData,"UTF-8");
			JSONObject fileJson= new JSONObject(fileDataString);
			return new ResponseEntity<>(fileJson.toString(), HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}
}
