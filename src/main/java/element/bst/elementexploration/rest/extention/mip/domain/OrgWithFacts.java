package element.bst.elementexploration.rest.extention.mip.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrgWithFacts implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String q;
	
	private Long size;	
	
	private Long from;
	
	private String sort;
	
	private TimeFil timefilter;
	
	public String getQ() {
		return q;
	}
	
	public void setQ(String q) {
		this.q = q;
	}
	
	public Long getSize() {
		return size;
	}
	
	public void setSize(Long size) {
		this.size = size;
	}
	
	public Long getFrom() {
		return from;
	}
	
	public void setFrom(Long from) {
		this.from = from;
	}
	
	public String getSort() {
		return sort;
	}
	
	public void setSort(String sort) {
		this.sort = sort;
	}
	
	public TimeFil getTimefilter() {
		return timefilter;
	}
	
	public void setTimefilter(TimeFil timefilter) {
		this.timefilter = timefilter;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
