package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScenariosResponseDto {
	
	@JsonProperty("1")
	private  List<RiskCountAndRatioDto> riskCountAndRatioDtos;
	
	@JsonProperty("2")
	private  List<RiskCountAndRatioDto> scenariosList;

	public List<RiskCountAndRatioDto> getRiskCountAndRatioDtos() {
		return riskCountAndRatioDtos;
	}

	public void setRiskCountAndRatioDtos(List<RiskCountAndRatioDto> riskCountAndRatioDtos) {
		this.riskCountAndRatioDtos = riskCountAndRatioDtos;
	}

	public List<RiskCountAndRatioDto> getScenariosList() {
		return scenariosList;
	}

	public void setScenariosList(List<RiskCountAndRatioDto> scenariosList) {
		this.scenariosList = scenariosList;
	}

}
