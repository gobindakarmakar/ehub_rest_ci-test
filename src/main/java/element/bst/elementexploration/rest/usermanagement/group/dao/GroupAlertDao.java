package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;

/**
 * @author Paul Jalagari
 *
 */
public interface GroupAlertDao extends GenericDao<GroupAlert, Long> {

	void removeExistingGroupAlertSettings(Long groupId, String type);

	List<GroupAlert> findGroupAlertsByGroup(Long groupId);

	List<GroupAlert> getGroupAlertsByGroupIdList(List<Long> groupIds);

}
