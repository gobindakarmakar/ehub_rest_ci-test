activitiApp.controller('submenuDropdownController', ['$scope', '$http', 'HostPathService', '$timeout', '$location', 'CommonService', 'EHUB_FE_API', function ($scope, $http, HostPathService, $timeout, $location, CommonService, EHUB_FE_API) {
	
		$scope.submenu = {
			activeDropdown:0,
			dashboardName:'manage',
			onclicksubMenuDropdown:onclicksubMenuDropdown,
			onhoversubMenuDropdown:onhoversubMenuDropdown,
			getSubMenu:getSubMenu,
			hoverdashboardName: '',
			submenuWidth: 0,
			subMenuContent: 0,
			addClass:addClass,
			iconColorClass: ['light','medium','dark'],
			checkSubmenuDisable: checkSubmenuDisable,
			checkMainmenuDisable: checkMainmenuDisable
		}
        /*
	     * @purpose: Get Submenu Data
	     * @created: 27 mar 2018
	     * @author: swathi
	    */
		var url;
        $scope.showMyTab = 'Manage';
		var moveArray = function (old_index, new_index, newArray) {
		    if (new_index >= newArray.length) {
		        var k = new_index - newArray.length;
		        while ((k--) + 1) {
		        	newArray.push(undefined);
		        }
		    }
		    newArray.splice(new_index, 0, newArray.splice(old_index, 1)[0]);
		    return newArray; // for testing purposes
		};
		
		function getSubmenu(){
			if(!window.location.hash.includes("#!/"))
	    		url = 'scripts/common/data/submenu.json';
			else
	   			url = '/scripts/common/data/submenu.json';
				$http.get(url).then(function(response){	
					HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);
					HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
					$scope.submenu.dashboarDropDownMenuItems = jQuery.extend(true, [], HostPathService.getdashboarDropDownMenuItems());
					HostPathService.setdashboardDisableBydomains(response.data.DisableBydomains);				
					moveArray(4, 0, $scope.submenu.dashboarDropDownMenuItems);
					
					/*get drop down menu items for submenu*/
					var dropDownDisableBydomains = HostPathService.getdashboardDisableBydomains();
					var current_domain = localStorage.getItem("domain");
					if(current_domain && dropDownDisableBydomains[current_domain]){
						angular.forEach($scope.submenu.dashboarDropDownMenuItems,function(val){
							angular.forEach(val.content,function(v){
								if (dropDownDisableBydomains[current_domain].indexOf(v['name']) != -1){
									v['disabled'] ="yes";
								}
							});
						});
					}
				},function(error){
				});
		}
		getSubmenu();
		
		 /*
	     * @purpose: Active submenu
	     * @created: 21 sep 2017
	     * @params: submenuIndex(string)
	     * @return: no
	     * @author: swathi
	    */
        function onhoversubMenuDropdown(submenuIndex, dashboardName, $event, disabled){
        	$scope.showMyTab = dashboardName;
	   		 if(disabled == 'no'){
	   			$scope.submenu.activeDropdown = submenuIndex;   
	   			$scope.submenu.hoverdashboardName = dashboardName;
	   			$timeout(function(){
	   				 var maindivElement = $event.currentTarget.parentElement.parentElement;
	   				 for(var i = 0; i < maindivElement.lastElementChild.childElementCount; i++){
	   					 for(var j = 0; j < $scope.submenu.dashboarDropDownMenuItems.length; j++){
	   						 if(maindivElement.lastElementChild.children[i].classList.contains('bg-' + $scope.submenu.dashboarDropDownMenuItems[j].menu.toLowerCase())){
	   							 maindivElement.lastElementChild.children[i].classList.remove('bg-' + $scope.submenu.dashboarDropDownMenuItems[j].menu.toLowerCase());
	   						 }
	   					 }
	   				 }
	   				 var divElement  = maindivElement.querySelectorAll('.tab-content .active');
	   				 divElement[0].classList.add('bg-'+dashboardName.toLowerCase());
	   			 },0);
	   		 }
   	 	}
        /*
         * @purpose: Document Ready
         * @created: 27 mar 2018
         * @params: none
         * @return: no
         * @author: swathi
        */
    	angular.element(document).ready(function () {
    		$scope.submenu.submenuWidth = angular.element('.utility-group').width();
    		var dropdown = angular.element('.utility-group .dropdown-menu').width();
    		$scope.submenu.subMenuContent = dropdown - $scope.submenu.submenuWidth;
    		angular.element('.utility-group').children().find('.tab-content').css('width',$scope.submenu.subMenuContent);
    		$scope.submenu.workDiaryWidth = angular.element('#work-diary-dropdown-button').width() + 'px';
        });
    	/*
         * @purpose: class
         * @created: 27 mar 2018
         * @params: null
         * @author: swathi
        */  
    	function addClass(index){
    		if(index==0 || index%3==0){
    			return $scope.submenu.iconColorClass[0];
    		}
    		else if(index==1 || (index-1)%3==0){
    			return $scope.submenu.iconColorClass[1];
    		}
    		else if(index==2 || (index-2)%3==0){
    			return $scope.submenu.iconColorClass[2];
    		}
    	}
    	 /*
         * @purpose: disable submenus
         * @created: 27 mar 2018
         * @params: value
         * @author: swathi
        */  
    	function checkSubmenuDisable(value){
    		return CommonService.checkSubmenuDisable(value);

    	}
    	/*
         * @purpose: disable main menu
         * @created: 27 mar 2018
         * @params: value
         * @return: checkMainmenuDisable value
         * @author: swathi
        */  
    	function checkMainmenuDisable(value){
    		return CommonService.checkMainmenuDisable(value);
    	}
		 
		 /*
	     * @purpose: Click submenu
	     * @created: 27 mar 2018
	     * @params: submenuName(string)
	     * @return: no
	     * @author: swathi
	    */  
		 var startUrl;
		 function onclicksubMenuDropdown(submenuname){
			 var submenuname = submenuname.toLowerCase();
			 if(submenuname != 'predict'){
				 startUrl = EHUB_FE_API + '#/' + submenuname;
				 window.open(startUrl, '_self');
			 }
		 }
		 
		 function getSubMenu(submenuname,  content, dashboardItemNames, caseSeedDetails){	
			 return CommonService.getSubMenuData(submenuname,  content, dashboardItemNames);
		 }
}]);