package element.bst.elementexploration.rest.settings.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class SelectedObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private long attributeId;
	private String attributeName;
	private String attributeValue;
	private boolean selected;

	public long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
