package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Counter aprty graph")
public class CounterPartyGraphDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Input counter parties list")	
	private List<CounterPartyNotiDto> inputCounterpartyList;
	@ApiModelProperty(value="Output counter parties list")	
	private List<CounterPartyNotiDto> outputCounterpartyList;

	public List<CounterPartyNotiDto> getInputCounterpartyList() {
		return inputCounterpartyList;
	}

	public void setInputCounterpartyList(List<CounterPartyNotiDto> inputCounterpartyList) {
		this.inputCounterpartyList = inputCounterpartyList;
	}

	public List<CounterPartyNotiDto> getOutputCounterpartyList() {
		return outputCounterpartyList;
	}

	public void setOutputCounterpartyList(List<CounterPartyNotiDto> outputCounterpartyList) {
		this.outputCounterpartyList = outputCounterpartyList;
	}

}
