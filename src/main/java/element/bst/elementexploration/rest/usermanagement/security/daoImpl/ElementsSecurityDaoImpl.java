package element.bst.elementexploration.rest.usermanagement.security.daoImpl;

import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.ElementsSecurityDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("elementsSecurityDao")
public class ElementsSecurityDaoImpl extends GenericDaoImpl<UsersToken, Long> implements ElementsSecurityDao{

	public ElementsSecurityDaoImpl(){
        super(UsersToken.class);
    }

	@Override
	public boolean isValidToken(String token) {
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Long> query = builder.createQuery(Long.class);
			Root<UsersToken> tokenObject = query.from(UsersToken.class);
			query.select(builder.count(tokenObject));
			query.where(builder.and(builder.equal(tokenObject.get("token"), token), builder.greaterThan(tokenObject.get("expireOn"), new Date())));
			return this.getCurrentSession().createQuery(query).getSingleResult() > 0;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Token validation", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not validate token - " + e.getMessage());
		}
	}

	@Override
	public UsersToken getUserToken(String token) {
		UsersToken userToken = null;
		try{
			String hql="from UsersToken where token = :token";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("token", token);
			userToken = (UsersToken) query.getSingleResult();
			if(userToken!=null)
				Hibernate.initialize(userToken.getUserId());
			return userToken;
		} catch (NoResultException e){
			return null;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Fetch token object ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not fetch token object - "+e.getMessage());
		}
	}

	@Override
	public UsersToken isTokenExist(long userId) {
		try{
			String hql="from UsersToken where userId.userId = :userId";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("userId", userId);
			return (UsersToken) query.getSingleResult();
		} catch (NoResultException e){
			return null;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Token Exist ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not validate token - "+e.getMessage());
		}
	}

	@Override
	public boolean isTokenExist(String token) {
		try{
			String hql="select count(*) from UsersToken where token = :token";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("token", token);
			return (((Number)query.getSingleResult()).longValue()) >0;
		}catch (NoResultException e){
			return false;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Token Exist ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not validate token - "+e.getMessage());
		}
	}

	@Override
	public UsersToken getUsersToken(String token) {
		UsersToken userToken = null;
		try{
			String hql="from UsersToken where token = :token";
			Query<?> query = this.getCurrentSession().createQuery(hql).setParameter("token", token);
			userToken = (UsersToken) query.getSingleResult();
			if(userToken!=null)
				Hibernate.initialize(userToken.getUserId());
			return userToken;
		} catch (NoResultException e){
			return null;
		}catch(HibernateException e){
			ElementLogger.log(ElementLoggerLevel.ERROR, "Fetch token object ", e, ElementSecurityDaoImpl.class);
			throw new FailedToExecuteQueryException("Could not fetch token object - "+e.getMessage());
		}
	}

}
