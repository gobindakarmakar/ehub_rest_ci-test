package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerNode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("residence_country")
	private String residentCountry;
	
	private String address;
	
	private String jurisdiction;
	
	private String industry;
	
	private String name;

	private String city;
	
	@JsonProperty("search-name")
	private String searchName;
	
	private String type;
	
	@JsonProperty("address_type")
	private String addressType;
	
	private String title;
	
	private String oid;
	
	@JsonProperty("customer_number")
	private String customerNumber;
	
	private String id;
	
	private String labelV;
	
	private Location location;
	    
    @JsonProperty("reporting_country")
	private String reportingCountry;
    
    private boolean generated = false;
    
    private Date addedOnDate;

    
	public CustomerNode(String residentCountry, String address, String jurisdiction, String industry, String name,
			String city, String searchName, String type, String addressType, String title, String oid,
			String customerNumber, String id, String labelV, Location location, String reportingCountry,Date addedOnDate) {
		super();
		this.residentCountry = residentCountry;
		this.address = address;
		this.jurisdiction = jurisdiction;
		this.industry = industry;
		this.name = name;
		this.city = city;
		this.searchName = searchName;
		this.type = type;
		this.addressType = addressType;
		this.title = title;
		this.oid = oid;
		this.customerNumber = customerNumber;
		this.id = id;
		this.labelV = labelV;
		this.location = location;
		this.reportingCountry = reportingCountry;
		this.addedOnDate = addedOnDate;
	}

	public String getResidentCountry() {
		return residentCountry;
	}

	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabelV() {
		return labelV;
	}

	public void setLabelV(String labelV) {
		this.labelV = labelV;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getReportingCountry() {
		return reportingCountry;
	}

	public void setReportingCountry(String reportingCountry) {
		this.reportingCountry = reportingCountry;
	}

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	public Date getAddedOnDate() {
		return addedOnDate;
	}

	public void setAddedOnDate(Date addedOnDate) {
		this.addedOnDate = addedOnDate;
	}
	
	
	
	
    
}
