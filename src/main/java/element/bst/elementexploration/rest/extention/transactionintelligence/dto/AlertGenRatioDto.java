package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

public class AlertGenRatioDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private int alertsGenerated;
	private double alertRatio;

	public AlertGenRatioDto() {
	}

	public AlertGenRatioDto(int alertsGenerated, long alertRatio) {
		super();
		this.alertsGenerated = alertsGenerated;
		this.alertRatio = alertRatio;
	}

	public int getAlertsGenerated() {
		return alertsGenerated;
	}

	public void setAlertsGenerated(int alertsGenerated) {
		this.alertsGenerated = alertsGenerated;
	}

	public double getAlertRatio() {
		return alertRatio;
	}

	public void setAlertRatio(double alertRatio) {
		this.alertRatio = alertRatio;
	}
	
	

}
