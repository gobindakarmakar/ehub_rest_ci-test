package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;


@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService{

	@Autowired
	UsersService usersService;
	
	@Override
	public UserDetails loadUserByUsername(String email){
		Users user = usersService.getUserObjectByEmailOrUsername(email);
			if(user == null){
				throw new UsernameNotFoundException("The user " + email + " was not found");
			}		
			Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));		
			return new org.springframework.security.core.userdetails.User(user.getEmailAddress(), user.getPassword(), 
	                 true, true, true, true, authorities);
	}

}
