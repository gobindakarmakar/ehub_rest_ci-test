package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourceMediaDao extends GenericDao<SourceMedia, Long> {

	SourceMedia fetchMedia(String name);

	List<SourceMedia> fetchMediaListExceptAll();

}
