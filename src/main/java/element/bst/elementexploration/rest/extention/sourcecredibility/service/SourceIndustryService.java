package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceIndustryDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourceIndustryService extends GenericService<SourceIndustry, Long> {

	List<SourceIndustryDto> saveSourceIndustry(SourceIndustryDto industryDto) throws Exception;

	List<SourceIndustryDto> getSourceIndustry();

	SourceIndustry fetchIndustry(String string);

}
