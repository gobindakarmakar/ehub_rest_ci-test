package element.bst.elementexploration.rest.casemanagement.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface CaseAnalystMappingDao extends GenericDao<CaseAnalystMapping, Long> {

	CaseAnalystMapping getAnalystMappingBasedOnUserId(Long seedId, Long userId, int status);

	void createMapping(CaseAnalystMapping analystMapping);

	CaseAnalystMapping getCaseInStatusList(Long seedId, Long userId, List<Integer> statuses);

	boolean deleteCaseFromFocusById(Long seedId, Long userId);

	List<CaseDto> getAllCasesInFocusById(Long userId, String orderBy, String orderIn, Integer pageNumber,
			Integer recordsPerPage);

	long countAllCasesInFocusById(Long userId);

	List<CaseDto> fullTextSearchForCasesInFocus(Long userId, String caseSearchKeyword, Integer pageNumber,
			Integer recordsPerPage, Date creationDate, String orderBy, String orderIn) throws ParseException;

	long countfullTextSearchForCaseInFocus(Long userId, String caseSearchKeyword, Date creationDate);
	
	CaseCommentNotificationDto getCaseCommentNotificationById(Long userId,Long caseId);
	
	List<CaseAnalystMapping> getAnalystMetrics(Date dateFrom, Date dateTo, String type, Boolean currentOnly, Long analystId);

	CaseAnalystMapping findByCaseId(Long caseId, Long currentUserId);

	  
	CaseAnalystMapping findByCaseId(Long caseId);
}
