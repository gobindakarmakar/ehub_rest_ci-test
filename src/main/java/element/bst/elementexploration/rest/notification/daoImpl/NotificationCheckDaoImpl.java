package element.bst.elementexploration.rest.notification.daoImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.notification.dao.NotificationCheckDao;
import element.bst.elementexploration.rest.notification.domain.NotificationCheck;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Viswanath
 *
 */
@Repository("notificationCheckDao")
public class NotificationCheckDaoImpl extends GenericDaoImpl<NotificationCheck, Long> implements NotificationCheckDao {

	public NotificationCheckDaoImpl() {
		super(NotificationCheck.class);
	}

	@Override
	public LocalDateTime checkInNotification(Long userId) {

		LocalDateTime checkIn = LocalDateTime.now();
		NotificationCheck notificationCheck = checkInNotificationFull(userId);
		try {
			NotificationCheck check;
			if (notificationCheck != null)
				check = notificationCheck;
			else
				check = new NotificationCheck();
			check.setTimestamp(checkIn);
			check.setUserId(userId);
			this.getCurrentSession().saveOrUpdate(check);
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, NotificationCheckDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return checkIn;
	}

	@Override
	public NotificationCheck checkInNotificationFull(Long userId) {

		NotificationCheck notificationCheck = null;
		List<NotificationCheck> notificationChecksList = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<NotificationCheck> query = builder.createQuery(NotificationCheck.class);
			Root<NotificationCheck> check = query.from(NotificationCheck.class);
			query.select(check);
			query.where(builder.equal(check.get("userId"), userId));
			query.orderBy(builder.desc(check.get("timestamp")));
			notificationChecksList = this.getCurrentSession().createQuery(query).getResultList();
			if (!notificationChecksList.isEmpty())
				notificationCheck = notificationChecksList.get(0);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get last checkin");
		}
		return notificationCheck;
	}

	@Override
	public LocalDateTime getLastCheckIn(Long userId) {

		LocalDateTime lastCheckIn = null;
		List<NotificationCheck> notificationChecksList = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<NotificationCheck> query = builder.createQuery(NotificationCheck.class);
			Root<NotificationCheck> check = query.from(NotificationCheck.class);
			query.select(check);
			query.where(builder.equal(check.get("userId"), userId));
			query.orderBy(builder.desc(check.get("timestamp")));
			notificationChecksList = this.getCurrentSession().createQuery(query).getResultList();
			if (!notificationChecksList.isEmpty())
				lastCheckIn = notificationChecksList.get(0).getTimestamp();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get last checkin");
		}
		return lastCheckIn;
	}
}
