package element.bst.elementexploration.rest.extention.alertmanagement.dto;

public class EntriesDto {
	String watchlist_id;
	String attribute_name;
	String entry_id;
	public String getWatchlist_id() {
		return watchlist_id;
	}
	public void setWatchlist_id(String watchlist_id) {
		this.watchlist_id = watchlist_id;
	}
	public String getAttribute_name() {
		return attribute_name;
	}
	public void setAttribute_name(String attribute_name) {
		this.attribute_name = attribute_name;
	}
	public String getEntry_id() {
		return entry_id;
	}
	public void setEntry_id(String entry_id) {
		this.entry_id = entry_id;
	}
	
	

}
