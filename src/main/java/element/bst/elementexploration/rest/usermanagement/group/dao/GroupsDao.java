package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author Paul Jalagari
 *
 */
public interface GroupsDao extends GenericDao<Groups, Long>{

	
	public Groups getGroup(String groupName);

	public List<Groups> getUserGroups(Boolean status, String keyword, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	public Long countUserGroups(Boolean status, String keyword);

	public List<Groups> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	Long countGroupsOfUser(Long userId);
	
	Groups checkGroupExist(String name, Long groupId);
	
	public Boolean doesUserbelongtoGroup(String groupName, Long userId);

	public List<Users> getUsersByGroup(Long groupId);

	public List<Groups> getGroupsOfUser(Long userId);
	
	public List<Groups> getGroupsOfUserWithRoles(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	public Object getStatuswiseUsersByGroupId(Long groupId);
	
	Groups checkIfGroupExist(String name, Long groupId);
}
