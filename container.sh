#!/bin/bash

ROOT=$PWD
#TAG=899719272550.dkr.ecr.eu-west-1.amazonaws.com/be-ui/explorer:1.7.1.6
TAG=registry.bst.ai:5000/bst-ehub-core-combined
TAG=rest_container
NAME=ehub-core-combined
## PRINT Codes

GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf "${GREEN}Stopping Running Containers${NC}\n"
# Check if a container with the same image tag, already exists
CONTAINER=$(docker ps -aq -f name=$NAME)
if [ ! -z "$CONTAINER" ]; then
  # Check if the container is currently running
  STATUS=$(docker inspect -f {{.State.Status}} $CONTAINER)
  if [ "$STATUS" == "running" ]; then
    docker stop $CONTAINER
  fi

  # Remove the old container
  docker rm --force $CONTAINER
fi
printf "${GREEN}Starting the Container${NC}\n"

#https://gaq2s3r5r1.execute-api.eu-west-1.amazonaws.com

docker run  --network bst  --name $NAME -it --hostname $NAME -d -p 8080:8080 -p 3308:3306  \
-e "container=docker" \
-e "BIG_DATA_CASE_URL=https://zya4qga7xl.execute-api.eu-west-2.amazonaws.com/sdk" \
-e "BIG_DATA_DOC_URL=http://3.9.171.152:9091" \
-e "BIG_DATA_RISK_SCORE_URL=https://zubyfn1eo1.execute-api.eu-west-1.amazonaws.com/development" \
-e "BE_ENTITY_URL=https://qdsbsreh31.execute-api.eu-west-1.amazonaws.com/development" \
-e "SOURCE_MONITORING_URL=https://myq8n2w3d9.execute-api.eu-west-1.amazonaws.com/Prod" \
-e "BE_ENTITY_SCREENSHOT_URL=https://ghobux4tte.execute-api.eu-west-1.amazonaws.com/Prod" \
-e "BE_ENTITY_API_KEY=lh78eJUEz66PJClxghxSq4k8gx7keT1y9Z3OMn4G" \
-e "BIG_DATA_SENTIMENT_URL=https://34z5khu9c7.execute-api.eu-west-1.amazonaws.com/Prod" \
-e "BIG_DATA_DOMAIN_URL=https://f6lqai1wta.execute-api.eu-west-1.amazonaws.com/Prod" \
-e "CLIENT_ID=element" \
-e "ELEMENT_API_REST_URL=http://3.248.195.168:8080" \
-e "DB_ELEMENT_NAME=release_sparta" \
-e "DB_ELEMENT_PORT=3306" \
-e "DB_ELEMENT_HOST=element-questionnaire-mysqldb" \
-e "DB_ELEMENT_USER_NAME=root" \
-e "DB_ELEMENT_PASSWORD=bst-element" \
-e "DB_QB_NAME=questionnaire" \
-e "DB_QB_HOST=element-questionnaire-mysqldb" \
-e "DB_QB_USER_NAME=root" \
-e "DB_QB_PASSWORD=password" \
-e "QUESTIONNAIRE_URL=http://3.248.195.168:90" \
-e "ELEMENT_MAIN_URL=http://3.248.195.168" \
-e "ELEMENT_REST_DOMAIN=http://3.248.195.168:8080" \
-e "SM_GENERAL_ID_URL=https://pb0rytz224.execute-api.eu-west-1.amazonaws.com/Prod" \
-e "SM_PROFILES_URL=https://inz2wwsjo4.execute-api.eu-west-1.amazonaws.com/Prod" \
-e "DB_BPM_NAME=bst_activiti" \
-e "DB_BPM_PORT=3306" \
-e "DB_BPM_HOST=element-questionnaire-mysqldb" \
-e "DB_BPM_USER_NAME=root" \
-e "DB_BPM_PASSWORD=bst-element" \
-e "POLICY_ENFORCEMENT_URL=http://3.248.195.168:7070" \
-e "ACTIVITI_URL=http://3.248.195.168:8080" \
-e "FIREBASE_VERIFY_API=https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword" \
-e "FIREBASE_VERIFY_TOKEN_API=https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo" \
-e "FIREBASE_API_KEY=AIzaSyArz8AZ9ThyWMIRvh69rDBgrvEe8fbFGW0" \
-e "SENDGRID_API_KEY=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" \
-e "SENDGRID_SENDER_MAIL=elementservices@blackswan-technologies.com" \
-e "SSB_ALERTS_API_URL=http://35.236.254.59:8200/v1/screen" \
-e "SSB_SCREENING_API_URL=http://35.245.236.83:8300/v1/screening" \
-e "AMS_ALERTS_API_URL=http://ams-api.sparta.xara.ai" \
-e "IS_QB_ENABLED=false" \
-v $ROOT/data/mysql:/var/lib/mysql \
-v $ROOT/data/tesserct-ocr:/tesserct-ocr \
-v $ROOT/data/noisereduction:/noisereduction \
--privileged=true  \
--restart=always \
--net bst  $TAG tail -f /dev/null
printf "${GREEN}Starting Success, you can access the docker container by {docker exec -it $NAME /bin/bash}${NC}\n"


