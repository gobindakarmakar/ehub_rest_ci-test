package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.AccountDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountOwnershipType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountType;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	AccountDaoImpl accountDao;

	Account account;

	List<Account> accountsList = new ArrayList<Account>();

	@Before
	public void setupMock() {
		account = new Account(1L, "12345", AccountOwnershipType.IND, "paswordNo", new Date(), "GB", "test",
				AccountType.SAV, "test", "321", "USD", 20000D, new CustomerDetails(), "GB");
		accountsList.add(account);
		accountsList.add(new Account());
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void checkExistenceOfAccountTest() {
		List<TxDto> accountIds = new ArrayList<TxDto>();
		TxDto txDto = new TxDto();
		txDto.setPartyIdentifier("123");
		accountIds.add(txDto);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(account);
		boolean status = accountDao.checkExistenceOfAccount(accountIds);
		assertEquals(true, status);
	}

	@Test
	public void fetchAccountsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(accountsList);
		List<Account> accountsListNew = accountDao.fetchAccounts("123456");
		assertEquals(2, accountsListNew.size());
	}

	@Test
	public void fetchMulAccountsTest() {
		List<String> accountNumbers = new ArrayList<String>();
		accountNumbers.add("123456");
		accountNumbers.add("789654");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(accountsList);
		List<Account> accountsListNew = accountDao.fetchMulAccounts(accountNumbers);
		assertEquals(2, accountsListNew.size());
	}

	@Test
	public void fetchAccountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(account);
		Account resultedAccount = accountDao.fetchAccount("123456");
		assertEquals(new Long("1"), resultedAccount.getId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAccount_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(NoResultException.class);
		Account resultedAccount = accountDao.fetchAccount("123456");
		assertEquals(null, resultedAccount);
	}

	@Test
	public void fetchLuxuryAccountsTest() {
		List<String> accountNumbers = new ArrayList<String>();
		accountNumbers.add("123456");
		accountNumbers.add("789654");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(accountsList);
		List<Account> accountsListNew = accountDao.fetchLuxuryAccounts(accountNumbers);
		assertEquals(2, accountsListNew.size());
	}

	@Test
	public void fetchLuxuryAccountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(account);
		Account resultedAccount = accountDao.fetchLuxuryAccount("123456");
		assertEquals(new Long("1"), resultedAccount.getId());
	}

}
