'use strict';
activitiApp.controller('TopPanelController', topPanelController);

	    topPanelController.$inject = [
			'$scope',
			'$rootScope',
			'$window',
			'$location',
			'TopPanelApiService',
			'EHUB_FE_API'
		];
	    function topPanelController(
	    		$scope, 
	    		$rootScope,
	    		$window,
	    		$location,
	    		TopPanelApiService,
	    		EHUB_FE_API){
	    	
//		$scope.fullName = $rootScope.ehubObject.email ? $rootScope.ehubObject.email : '';
//		$scope.token = $rootScope.ehubObject.token;
		$scope.isCollapsed = false;
		/*
		 * @purpose: logout
		 * @created: 13 sep 2017
		 * @params: null
		 * @returns: no
		 * @author: swathi
		 */
		$scope.logout = function(){
			TopPanelApiService.getLogout().then(function(resposne){
				window.location.href = EHUB_FE_API + 'login';
				 window.localStorage.removeItem('ehubObject');
			}, function(error){
			});
		};
		
		$rootScope.showMyClipboard = false;
		$rootScope.showUserEvents = false;
		$rootScope.showNotification = false;
		$rootScope.showLogout = false;
		/*
		 * @purpose: toggle the tabs drop down menu
		 * @created: 12 sep 2017
		 * @params: type(string)
		 * @returns: no
		 * @author: swathi
		 */
		$scope.toggleTabsDropdown = function(type){
			if(type === 'clipboard'){
				$rootScope.addContextOnNotes();
				$rootScope.showUserEvents = false;
				$rootScope.showNotification = false;
				$rootScope.showLogout = false;
				$rootScope.showMyClipboard = !$rootScope.showMyClipboard;
			} else if(type === 'userEvents') {
				$rootScope.showNotification = false;
				$rootScope.showMyClipboard = false;
				$rootScope.showLogout = false;
				$rootScope.showUserEvents = !$rootScope.showUserEvents;
			} else if(type === 'notification') {
				$rootScope.showUserEvents = false;
				$rootScope.showMyClipboard = false;
				$rootScope.showLogout = false;
				$rootScope.showNotification = !$rootScope.showNotification;
			} else if(type === 'logout') {
				$rootScope.showMyClipboard = false;
				$rootScope.showUserEvents = false;
				$rootScope.showNotification = false;
				$rootScope.showLogout = !$rootScope.showLogout;
			}				
		};
		
		$scope.advancedSearch = function(){
			var url = EHUB_FE_API + '#/enrich';
			window.open(url, '_self');
		};
    };
 
activitiApp.controller('logonUserCtrl', ['$scope', function ($scope) {
	
}]);

activitiApp.controller('notificationsController', ['$scope', function ($scope) {

}]);


