package element.bst.elementexploration.test.extention.transactionintelligence.service;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.ShareholderDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureTopFiveDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.CorporateStructureServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CorporateStructureServiceTest {

	@Mock
	ShareholderDao shareholderDaoMock;

	@InjectMocks
	CorporateStructureServiceImpl corporateStructureServiceImpl;

	public CorporateStructureDto corporateStructureDto;

	public List<CorporateStructureDto> list;

	public CorporateStructureTopFiveDto corporateStructureTopFiveDto;
	
	public FilterDto filterDto;
	
	public List<String> scenariosList;

	@Before
	public void setupMock() {
		corporateStructureDto = new CorporateStructureDto(new Double(1000), 10L, new Double(10), 100L);
		corporateStructureDto.setAlertAmount(new Double(100));

		list = new ArrayList<>();
		CorporateStructureDto corporateStructureDto1 = new CorporateStructureDto("Charity", new Double(1000), 10L);//corporateStructureDto1.setType("type");
		CorporateStructureDto corporateStructureDto2 = new CorporateStructureDto("Trust", new Double(100), 5L);
		list.add(corporateStructureDto1);
		list.add(corporateStructureDto2);

		corporateStructureTopFiveDto = new CorporateStructureTopFiveDto();
		corporateStructureTopFiveDto.setTopCorporateStructures(list);
		filterDto = new FilterDto();
		filterDto.setTransactionType("transactionType");
		filterDto.setCountry("india");
		filterDto.setBank(true);
		filterDto.setActivityCategory("activityCategory");
		
		scenariosList = new ArrayList<String>();
		scenariosList.add("scenarios1");
		scenariosList.add("scenarios2");

		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void getCorporateStructureTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getCorporateStructure(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(corporateStructureDto);

		CorporateStructureDto result = corporateStructureServiceImpl.getCorporateStructure(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(corporateStructureDto, result);
	}

	@Test
	public void getCorporateStructureAggregatesTest() {

		Mockito.when(shareholderDaoMock.getCorporateStructureAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);

		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCorporateStructureAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(list.size(), result.size());

	}
	
	@Test
	public void getCorporateStructuteByTypeTest() {

		Mockito.when(shareholderDaoMock.getCorporateStructuteByType(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyListOf(String.class))).thenReturn(list);

		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCorporateStructuteByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type", scenariosList);
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void getAlertEntitiesBusinessTypeTest() {

		Mockito.when(shareholderDaoMock.getAlertEntitiesBusinessType(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getAlertEntitiesBusinessType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void getScenariosByCountryTest() {

		Mockito.when(shareholderDaoMock.getCountryAggregates(Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class)))
				.thenReturn(list);
		Mockito.when(shareholderDaoMock.getCountryByType(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class)))
		.thenReturn(scenariosList);
		
		CorporateStructureDto result = corporateStructureServiceImpl.getScenariosByCountry(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		Assert.assertNotNull(result);
	}
	
	@Test
	public void getCustomerByCountryScenarioTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getCustomerByCountryScenario(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class)))
				.thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCustomerByCountryScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "country", scenariosList);
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void getCustomerByBusinessScenarioTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getCustomerByBusinessScenario(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class)))
				.thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCustomerByBusinessScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "industry", scenariosList);
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void getCounterPartyByCountryCustomerTest() throws ParseException {
		Mockito.when(shareholderDaoMock.getAlertEntitiesFilter(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyListOf(String.class), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyLong(), Mockito.anyString())).thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCounterPartyByCountryCustomer(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "country", 1L,scenariosList);
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void getCounterPartyByBusinessCustomerTest() throws ParseException {
		Mockito.when(shareholderDaoMock.getAlertEntitiesFilter(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyListOf(String.class), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyLong(), Mockito.anyString())).thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCounterPartyByBusinessCustomer(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "country", 1L, scenariosList);
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void insertShareHoldersTest() {
		Mockito.doNothing().when(shareholderDaoMock).insertShareHolders();
		boolean result = corporateStructureServiceImpl.insertShareHolders();
		assertEquals(true, result);
	}
	
	@Test
	public void getCorporateScenariosByTypeTest() {
		Mockito.when(shareholderDaoMock.getCorporateStructureAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);
		Mockito.when(shareholderDaoMock.getCorporateByType(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyListOf(String.class))).thenReturn(scenariosList);
		CorporateStructureDto result = corporateStructureServiceImpl.getCorporateScenariosByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "type");
		Assert.assertNotNull(result);
	}
	
	@Test
	public void getCorporateStructuteByScenariosTest() {
		Mockito.when(shareholderDaoMock.getCorporateStructuteByScenarios(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getCorporateStructuteByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(list.size(), result.size());
	}
	
	@Test
	public void getAlertEntitiesGeographyTest() {
		Mockito.when(shareholderDaoMock.getGeographyAlerts(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(corporateStructureDto);
		CorporateStructureDto result = corporateStructureServiceImpl.getAlertEntitiesGeography(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(corporateStructureDto, result);
	}
	
	@Test
	public void getShareholderCorporateStructureTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getCorporateStructureAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);
		List<CorporateStructureDto> result = corporateStructureServiceImpl.getShareholderCorporateStructure(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(list.size(), result.size());

	}

	@Test
	public void getShareholderAggregatesTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getShareHolder(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(corporateStructureDto);
		CorporateStructureDto result = corporateStructureServiceImpl.getShareholderAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(corporateStructureDto, result);
	}

	@Test
	public void getGeographyAggregatesTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getGeographyAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(list);

		List<CorporateStructureDto> result = corporateStructureServiceImpl.getGeographyAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(list.size(), result.size());

	}
	
	@Test
	public void getTopFiveAlertsTest() throws ParseException {

		Mockito.when(shareholderDaoMock.getCorporateStructureAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);
		Mockito.when(shareholderDaoMock.getCorporateStructureAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);
		Mockito.when(shareholderDaoMock.getGeographyAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(list);
		CorporateStructureTopFiveDto result = corporateStructureServiceImpl.getTopFiveAlerts(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		Assert.assertNotNull(result);
	}
}
