package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class TotalDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("avg")
	private double average;
	private long count;
	@JsonProperty("max")
	private double maximum;
	@JsonProperty("min")
	private double minimum;
	private double sum;
	private String point;

	public TotalDto() {
		super();
	}

	public TotalDto(double average, long count, double maximum, double minimum, double sum) {
		super();
		this.average = average;
		this.count = count;
		this.maximum = maximum;
		this.minimum = minimum;
		this.sum = sum;
	}

	public TotalDto(double average, long count, double maximum, double minimum, double sum, String point) {
		super();
		this.average = average;
		this.count = count;
		this.maximum = maximum;
		this.minimum = minimum;
		this.sum = sum;
		this.point = point;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public double getMaximum() {
		return maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public double getMinimum() {
		return minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

}
