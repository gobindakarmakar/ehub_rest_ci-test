package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Categories implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Iso category id")
	private long categoryId;
	@ApiModelProperty(value = "Level1 iso code")
	private String level1Code;
	@ApiModelProperty(value = "Level1 title")
	private String levelTitle;
	@ApiModelProperty(value = "Evaluation")
	private String evaluation;
	
	public Categories() {
		super();
		
	}

	public Categories(long categoryId, String level1Code, String levelTitle, String evaluation) {
		super();
		this.categoryId = categoryId;
		this.level1Code = level1Code;
		this.levelTitle = levelTitle;
		this.evaluation = evaluation;
	}
	@JsonProperty("categoryId")
	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	@JsonProperty("isoCode")
	public String getLevel1Code() {
		return level1Code;
	}

	public void setLevel1Code(String level1Code) {
		this.level1Code = level1Code;
	}
	@JsonProperty("title")
	public String getLevelTitle() {
		return levelTitle;
	}

	public void setLevelTitle(String levelTitle) {
		this.levelTitle = levelTitle;
	}
	@JsonProperty("evaluation")
	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	
	
	
	
	

}
