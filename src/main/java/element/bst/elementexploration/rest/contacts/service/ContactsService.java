package element.bst.elementexploration.rest.contacts.service;

import java.util.List;

import element.bst.elementexploration.rest.contacts.domain.Contact;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Amit Patel
 * 
 * */
public interface ContactsService extends GenericService<Contact, Long>{
	
	List<ContactDTO> getUserContactList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn, String keyword);
	
	Long countUserContactList(Long userId, String keyword);
	
	void removeContact(Long contactId, Long userId);
	
	boolean addContact(Long contactId, Long userId);
}
