package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
/**
 * 
 * @author Rambabu
 *
 */

public interface DocumentTemplatesDao extends GenericDao<DocumentTemplates, Long>{
	
	public List<DocumentTemplates> getDocumentTemplatesByFileExtension(String fileExtension);
	
	public List<DocumentTemplates> getDocumentTemplatesList(Integer recordsPerPage, Integer pageNumber,boolean isPagination, String searchKey);
	
	

}
