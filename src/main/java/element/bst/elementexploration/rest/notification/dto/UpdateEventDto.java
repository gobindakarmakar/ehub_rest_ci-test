package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@ApiModel("Update event")
public class UpdateEventDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Notification Id of the notification ", required = true)
	private Long notificationId;

	@ApiModelProperty(value = "Subject of the notification ", required = true)
	private String subject;

	@ApiModelProperty(value = "Body of the notification ", required = true)
	private String body;

	@ApiModelProperty(value = "Created Date of the notification ")
	private LocalDateTime createdOn;

	@ApiModelProperty(value = "Scheduled From  of the notification ", required = true)
	private LocalDateTime scheduledFrom;

	@ApiModelProperty(value = "Scheduled To  of the notification ")
	private LocalDateTime scheduledTo;

	@ApiModelProperty(value = "Type of the notification ")
	private Integer type;

	@ApiModelProperty(value = "Level of the notification ")
	private Integer level;

	@ApiModelProperty(value = "Created UserId of the notification ")
	private Long createdBy;

	@ApiModelProperty(value = "CaseId  of the notification ")
	private Long caseId;

	@ApiModelProperty(value = "Flag of the notification ")
	private Integer flag;

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getScheduledFrom() {
		return scheduledFrom;
	}

	public void setScheduledFrom(LocalDateTime scheduledFrom) {
		this.scheduledFrom = scheduledFrom;
	}

	public LocalDateTime getScheduledTo() {
		return scheduledTo;
	}

	public void setScheduledTo(LocalDateTime scheduledTo) {
		this.scheduledTo = scheduledTo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

}
