package element.bst.elementexploration.rest.extention.insuranceassistant.service;

public interface InsuranceAssistantService {

	String createNewDialog(String dialogRequest) throws Exception;

	String proposalRequest(String proposalRequest, String dialogId) throws Exception;

	String respondMessageDialog(String dialogRequest, String dialogId) throws Exception;

}
