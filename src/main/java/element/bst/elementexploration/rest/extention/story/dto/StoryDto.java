package element.bst.elementexploration.rest.extention.story.dto;

public class StoryDto {
	
	private String storyId;
	
	private Boolean status;
	
	public StoryDto() {
		super();
	}

	public StoryDto(String storyId, Boolean status) {
		super();
		this.storyId = storyId;
		this.status = status;
	}

	public String getStoryId() {
		return storyId;
	}

	public void setStoryId(String storyId) {
		this.storyId = storyId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}
