package element.bst.elementexploration.rest.extention.mip.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.mip.dao.MipSearchHistoryDao;
import element.bst.elementexploration.rest.extention.mip.domain.MipSearchHistory;
import element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO;
import element.bst.elementexploration.rest.extention.mip.service.MipSearchHistoryService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("mipSearchHistoryService")
public class MipSearchHistoryServiceImpl extends GenericServiceImpl<MipSearchHistory, Long>
		implements MipSearchHistoryService {
	
	@Autowired
	MipSearchHistoryDao mipSearchHistoryDao;

	public MipSearchHistoryServiceImpl(GenericDao<MipSearchHistory, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public List<MipSearchHistoryDTO> getSearchHistoryList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		return mipSearchHistoryDao.getSearchHistoryList(userId, pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn);
	}

	@Override
	@Transactional("transactionManager")
	public boolean isSearchHistoryOwner(Long searchId, Long userId) {

		return mipSearchHistoryDao.isSearchHistoryOwner(searchId, userId);
	}

	@Override
	@Transactional("transactionManager")
	public Long countSearchHistoryList(Long userId) {
		return mipSearchHistoryDao.countSearchHistoryList(userId);
	}

}
