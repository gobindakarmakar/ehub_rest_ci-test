package element.bst.elementexploration.test.extention.story.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.story.daoImpl.UserStoryDaoImpl;
import element.bst.elementexploration.rest.extention.story.domain.UserStory;

@RunWith(MockitoJUnitRunner.class)
public class UserStoryDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<UserStory> root;

	@InjectMocks
	UserStoryDaoImpl userStoryDaoImpl;

	public UserStory userStory;

	@Before
	public void setupMock() {

		userStory = new UserStory(1L, 2L, "storyId", new Date(), new Date());

		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getFavouriteStoryByUserIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(UserStory.class)).thenReturn(root);
		Mockito.when(criteriaQuery.select(root)).thenReturn(criteriaQuery);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(userStory);
		UserStory result = userStoryDaoImpl.getFavouriteStoryByUserId(2L);
		assertEquals(userStory, result);
	}

}
