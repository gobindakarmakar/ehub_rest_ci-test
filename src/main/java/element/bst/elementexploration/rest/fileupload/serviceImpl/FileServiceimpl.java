package element.bst.elementexploration.rest.fileupload.serviceImpl;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.document.dao.DocumentMappingDao;
import element.bst.elementexploration.rest.fileupload.dao.FileDao;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.fileupload.service.FileService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Service("fileService")
@Transactional("transactionManager")
public class FileServiceimpl extends GenericServiceImpl<FileBst, Long> implements FileService {

	@Autowired
	UsersService usersService;

	@Autowired
	FileDao fileDao;

	@Autowired
	private DocumentMappingDao documentMappingDao;

	@Autowired
	public FileServiceimpl(GenericDao<FileBst, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public boolean uploadFile(MultipartFile uploadFile, Long userId, String fileName) throws Exception {
		FileBst fileUpload = new FileBst();
		Users user = usersService.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		File file = null;
		String ext = "";
		if (uploadFile != null && userId != null && fileName != null) {
			FileBst files = fileDao.findFileByName(fileName);
			if (files != null && files.getId()!=null) {
				String extension = FilenameUtils.getExtension(uploadFile.getName());
				files.setFileName(fileName);
				files.setFileType(extension);
				files.setFile(uploadFile.getBytes());
				files.setUploadedBy(user);
				files.setSize(uploadFile.getSize());
				files.setUploadedOn(new Date());
				files.setModifiedBy(userId);
				files.setModifiedOn(new Date());
				files.setFile(uploadFile.getBytes());
				saveOrUpdate(files);
				return true;
			} else {
				try {
					/*
					 * String originalFile = new String(uploadFile.getOriginalFilename()); String
					 * fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile); ext =
					 * FilenameUtils.getExtension(originalFile); file =
					 * File.createTempFile(fileNameWithoutExtension, ext);
					 */
					//uploadFile.transferTo(file);
					fileUpload.setFileName(fileName);
					String extension = FilenameUtils.getExtension(uploadFile.getName());
					fileUpload.setFileType(extension);
					fileUpload.setUploadedBy(user);
					fileUpload.setModifiedBy(userId);
					fileUpload.setModifiedOn(new Date());
					fileUpload.setSize(uploadFile.getSize());
					fileUpload.setUploadedOn(new Date());
					fileUpload.setFile(uploadFile.getBytes());
					save(fileUpload);
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return false;
	}

	@Override
	@Transactional("transactionManager")
	public byte[] downloadFile(Long id, long userId, HttpServletResponse response) throws Exception {
		return null;
	}

	@Override
	@Transactional("transactionManager")
	public boolean updateFile(MultipartFile uploadFile, Long userId, Long docId) {
		Users user = usersService.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		FileBst fileUpdate = find(docId);
		File file = null;
		String ext = "";
		if (uploadFile != null && fileUpdate != null) {
			try {
				String originalFile = new String(uploadFile.getOriginalFilename());
				String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
				ext = FilenameUtils.getExtension(originalFile);
				file = File.createTempFile(fileNameWithoutExtension, ext);
				fileUpdate.setFileName(originalFile);
				String extension = FilenameUtils.getExtension(originalFile);
				fileUpdate.setFileType(extension);
				fileUpdate.setModifiedBy(userId);
				fileUpdate.setModifiedOn(new Date());
				fileUpdate.setSize(uploadFile.getSize());
				fileUpdate.setUploadedOn(new Date());
				fileUpdate.setUploadedBy(user);
				fileUpdate.setFile(uploadFile.getBytes());
				saveOrUpdate(fileUpdate);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return false;

	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteFileById(Long id, long userId) {
		FileBst document = fileDao.find(id);
		if (id != null) {
			fileDao.delete(document);
		}
		return true;

	}

	@Override
	@Transactional("transactionManager")
	public FileBst findFileByName(String fileName) {
		return fileDao.findFileByName(fileName);
	}

}
