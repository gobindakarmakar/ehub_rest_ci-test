package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourcesDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceFilterDto;

/**
 * @author suresh
 *
 */
public class SourcesDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<Sources> root;

	@InjectMocks
	SourcesDaoImpl sourcesDao;

	Sources sources = null;
	Join<Object, Object> classifications = null;
	Join<Object, Object> sourceCredibility = null;
	List<Sources> sourcesList = new ArrayList<Sources>();
	List<SourceFilterDto> sourceFilter = new ArrayList<SourceFilterDto>();
	boolean sourceFound;
	boolean sourceExist;

	@Before
	public void setupMock() {
		sources = new Sources();
		sourceFound = true;
		sourceExist = false;
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void checkSourceLink() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sources);
		boolean sourcesSaved = sourcesDao.checkSourceName("BST", false, 5L);
		assertEquals(sourceFound, sourcesSaved);
	}

	@Test
	public void checkSourceExist() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sources);
		boolean sourcesSaved = sourcesDao.checkSourceExistWithSourceNameAndEntityId("15dwdw585wdw", "BST");
		assertEquals(sourceExist, sourcesSaved);
	}

}
