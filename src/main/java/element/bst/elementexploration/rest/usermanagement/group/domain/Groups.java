package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Paul Jalagari
 *
 */
@Entity
@Table(name = "bst_um_groups")
@Access(AccessType.FIELD)
public class Groups implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private Date createdDate;

	@Column
	private Date modifiedDate;

	@Column(name = "parentUserGroupId")
	private Integer parentGroupId;

	@Column(name = "name", unique = true)
	private String name;

	@ApiModelProperty(value = "Icon if the List Type has")
	@Column(name = "icon")
	private String icon;
	
	@ApiModelProperty(value = "Color for each role")
	@Column(name = "color")
	private String color;
	
	@Column(name = "remarks")
	private String remarks;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "createdBy")
	private Long createdBy;
	
	@Column(name = "source")
	private String source;
	

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "groupId",orphanRemoval=true)
	private List<FeedGroups> feedGroups;
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "groupId",orphanRemoval=true)
	private List<GroupAlert> groupAlerts;

	public List<UserGroups> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroups> userGroups) {
		this.userGroups = userGroups;
	}

	@Column(name = "description", columnDefinition = "LONGTEXT")
	private String description;

	/*@JsonIgnore
	@OneToMany(mappedBy = "groupId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY,orphanRemoval=true)
	private List<GroupPermission> groupPermissions;*/

	@JsonIgnore
	@OneToMany(mappedBy = "groupId", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY,orphanRemoval=true)
	private List<UserGroups> userGroups;
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "groupId",orphanRemoval=true)
	private List<RoleGroup> roleGroups;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(Integer parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public List<GroupPermission> getGroupPermissions() {
		return groupPermissions;
	}

	public void setGroupPermissions(List<GroupPermission> groupPermissions) {
		this.groupPermissions = groupPermissions;
	}*/

	public List<FeedGroups> getFeedGroups() {
		return feedGroups;
	}

	public void setFeedGroups(List<FeedGroups> feedGroups) {
		this.feedGroups = feedGroups;
	}
	
	

	public List<GroupAlert> getGroupAlerts() {
		return groupAlerts;
	}

	public void setGroupAlerts(List<GroupAlert> groupAlerts) {
		this.groupAlerts = groupAlerts;
	}

	public List<RoleGroup> getRoleGroups() {
		return roleGroups;
	}

	public void setRoleGroups(List<RoleGroup> roleGroups) {
		this.roleGroups = roleGroups;
	}

	public Groups() {
		super();
	}

	public Groups(Long id, Date createdDate, Date modifiedDate, Integer parentGroupId, String name, String remarks,
			Boolean active, Long createdBy, String description) {
		super();
		this.id = id;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.parentGroupId = parentGroupId;
		this.name = name;
		this.remarks = remarks;
		this.active = active;
		this.createdBy = createdBy;
		this.description = description;
	}
	
	public Groups(Long id, Date createdDate, Date modifiedDate, Integer parentGroupId, String name, String remarks,
			Boolean active, Long createdBy, String description,String source) {
		super();
		this.id = id;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.parentGroupId = parentGroupId;
		this.name = name;
		this.remarks = remarks;
		this.active = active;
		this.createdBy = createdBy;
		this.description = description;
		this.source=source;
	}

	public Groups(Long id, Date createdDate, Date modifiedDate, Integer parentGroupId, String name, String remarks,
			Boolean active, Long createdBy, String description, Collection<GroupPermission> groupPermissions) {
		super();
		this.id = id;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.parentGroupId = parentGroupId;
		this.name = name;
		this.remarks = remarks;
		this.active = active;
		this.createdBy = createdBy;
		this.description = description;
		//this.groupPermissions = (List<GroupPermission>) groupPermissions;
	}

	public Groups(Long id, Date createdDate, Date modifiedDate, Integer parentGroupId, String name, String remarks,
			Boolean active, Long createdBy, String description, Collection<GroupPermission> groupPermissions, String source) {
		super();
		this.id = id;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.parentGroupId = parentGroupId;
		this.name = name;
		this.remarks = remarks;
		this.active = active;
		this.createdBy = createdBy;
		this.description = description;
		//this.groupPermissions = (List<GroupPermission>) groupPermissions;
		this.source=source;
	}
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	

}
