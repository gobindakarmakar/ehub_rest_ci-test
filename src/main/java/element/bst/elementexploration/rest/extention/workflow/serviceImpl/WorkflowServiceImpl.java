package element.bst.elementexploration.rest.extention.workflow.serviceImpl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.workflow.service.WorkflowService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("workflowService")
public class WorkflowServiceImpl implements WorkflowService {
	
	@Autowired
	private CaseService caseService;

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENTION_API;
	
	@Value("${bigdata_create_seed_url}")
	private String BIG_DATA_CASE_MANAGEMENT_URL;

	@Override
	public String getProfile(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/wrapper/profile/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String getIndependentStage(String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/wrapper/stage/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String getStageByWorkflow(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/wrapper/stage/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String getWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/wrapper/workflow/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowStates() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates";
		return deleteDataInServer(url);	
	}

	@Override
	public String getWorkflowStates() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates";
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowStatesById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates/" + encode(workflowId);
		return deleteDataInServer(url);		
	}

	@Override
	public String getWorkflowStatesById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowStatesByIdAndNominaltime(String workflowId, String nominalTime) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates/" + encode(workflowId) + "/" + encode(nominalTime);
		return deleteDataInServer(url);	
	}

	@Override
	public String getWorkflowStatesByIdAndNominalTime(String workflowId, String nominalTime) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates/" + encode(workflowId) + "/" + encode(nominalTime);
		return getDataFromServer(url);	
	}

	@Override
	public String insertWorkflowStates(String workflowId, String nominalTime, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstates/" + encode(workflowId) + "/" + encode(nominalTime);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String deletePublishedModels() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published";
		return deleteDataInServer(url);	
	}

	@Override
	public String getPublishedModels() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published";
		return getDataFromServer(url);	
	}

	@Override
	public String createPublishedModels(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updatePublishedModels(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published";
		return updateDataInServer(url, jsonToBeSent);		
	}

	@Override
	public String deletePublishedModelsInWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getPublishedModelsById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String deletePublishedModelsInWorkflowByModelId(String workflowId, String modelId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published/" + encode(workflowId) + "/" + encode(modelId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getPublishedModelsInWorkflowByModelId(String workflowId, String modelId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published/" + encode(workflowId) + "/" + encode(modelId);
		return getDataFromServer(url);	
	}

	@Override
	public String deletePublishedModelsInWorkflowByVersion(String workflowId, String modelId, String version)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published/" + encode(workflowId) + "/" + encode(modelId) + "/" + encode(version);
		return deleteDataInServer(url);	
	}

	@Override
	public String getPublishedModelsInWorkflowByVersion(String workflowId, String modelId, String version)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/published/" + encode(workflowId) + "/" + encode(modelId) + "/" + encode(version);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteStagingModels() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging";
		return deleteDataInServer(url);	
	}

	@Override
	public String getStagingModels() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging";
		return getDataFromServer(url);	
	}

	@Override
	public String createStagingModels(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updateStagingModels(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging";
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String getStagingToPublishModelsByVersion(String workflowId, String modelId, String version)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/publish/" + encode(workflowId) + "/" + encode(modelId) + "/"
				+ encode(version);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteStagingModelsById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getStagingModelsById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteStagingModelsByIdAndModelId(String workflowId, String modelId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/" + encode(workflowId) + "/" + encode(modelId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getStagingModelsByIdAndModelId(String workflowId, String modelId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/" + encode(workflowId) + "/" + encode(modelId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteStagingModelsByVersion(String workflowId, String modelId, String version) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/" + encode(workflowId) + "/" + encode(modelId) + "/" + encode(version);
		return deleteDataInServer(url);	
	}

	@Override
	public String getStagingModelsByVersion(String workflowId, String modelId, String version) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/model/staging/" + encode(workflowId) + "/" + encode(modelId) + "/" + encode(version);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteTags() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/tag";
		return deleteDataInServer(url);	
	}

	@Override
	public String getTags() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/tag";
		return getDataFromServer(url);	
	}

	@Override
	public String createTags(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/tag";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updateTags(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/tag";
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String deleteTagById(String tagId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/tag/" + encode(tagId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getTagById(String tagId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/tag/" + encode(tagId);
		return getDataFromServer(url);	
	}

	@Override
	public String getGraphcache() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/graphcache";
		return getDataFromServer(url);	
	}

	@Override
	public String deleteGraphcache() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/graphcache";
		return deleteDataInServer(url);	
	}

	@Override
	public String deleteGraphcacheByQuery(String query) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/graphcache/" + encode(query);
		return deleteDataInServer(url);	
	}

	@Override
	public String getGraphcacheByQuery(String query) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/graphcache/" + encode(query);
		return getDataFromServer(url);	
	}

	@Override
	public String createGraphcache(String query, String caseId, String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/graphcache/" + encode(query) + "/" + encode(caseId);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String deleteCases() throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL;
		return deleteDataInServer(url);	
	}

	@Override
	public String getCases() throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL;
		return getDataFromServer(url);	
	}

	@Override
	public String createCase(String jsonToBeSent) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL;
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updateCase(String jsonToBeSent) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL;
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String deleteCaseById(String caseId) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getCaseById(String caseId) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId);
		return getDataFromServer(url);	
	}

	@Override
	public String clearDatabase() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/esdatabase/clear";
		return getDataFromServer(url);	
	}

	@Override
	public String deleteIndex(String indexName) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/esdatabase/clear" + encode(indexName);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteContingencyTable() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/contingencytable";
		return deleteDataInServer(url);	
	}

	@Override
	public String getContingencyTable() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/contingencytable";
		return getDataFromServer(url);	
	}

	@Override
	public String createContingencyTable(String jsonToBeSent, String contingencytableId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/contingencytable/" + encode(contingencytableId);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updateContingencyTable(String jsonToBeSent, String contingencytableId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/contingencytable/" + encode(contingencytableId);
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String deleteContingencyTableById(String contingencytableId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/contingencytable/" + encode(contingencytableId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getContingencyTableById(String contingencytableId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/contingencytable/" + encode(contingencytableId);
		return getDataFromServer(url);	
	}

	@Override
	public String listOfInices(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/index/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String createIndex(String jsonToBeSent, String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/index/" + encode(profileId);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String deleteIndexByIdAndIndexName(String profileId, String indexName) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/index/" + encode(profileId) + "/" + encode(indexName);
		return deleteDataInServer(url);	
	}

	@Override
	public String listOfTypes(String profileId, String indexName) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/index/" + encode(profileId) + "/" + encode(indexName);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteAllWorkflowStats() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats";
		return deleteDataInServer(url);	
	}

	@Override
	public String getAllWorkflowStats() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats";
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowStatsById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getWorkflowStatsById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowStatsByIdAndStageId(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId) + "/" + encode(stageId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getWorkflowStatsByIdAndStageId(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowStatsByIdAndStageIdAndTime(String workflowId, String stageId, String nominalTime)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId) + "/" + encode(stageId) + "/" + encode(nominalTime);
		return deleteDataInServer(url);	
	}

	@Override
	public String getWorkflowStatsByIdAndStageIdAndTime(String workflowId, String stageId, String nominalTime)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId) + "/" + encode(stageId) + "/" + encode(nominalTime);
		return getDataFromServer(url);	
	}

	@Override
	public String createWorkflowStats(String workflowId, String stageId, String nominalTime, String jsonToBeSent)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflowstats/" + encode(workflowId) + "/" + encode(stageId) + "/" + encode(nominalTime);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String deleteAllStagesInWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/stage/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getAllStagesInWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/stage/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String updateStageInWorkflow(String workflowId, String jsonToBeSent) throws Exception{
		String url = WORKFLOW_EXTENTION_API + "/es/api/stage/" + encode(workflowId);
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String createStageInWorkflow(String workflowId, String jsonToBeSent) throws Exception{
		String url = WORKFLOW_EXTENTION_API + "/es/api/stage/" + encode(workflowId);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String deleteAllStagesInWorkflowByStageId(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/stage/" + encode(workflowId) + "/" + encode(stageId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getAllStagesInWorkflowByStageId(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/stage/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String getPyspark(String type) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/pyspark/" + encode(type);
		return getDataFromServer(url);	
	}

	@Override
	public String getAllWorkflows() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow";
		return getDataFromServer(url);	
	}

	@Override
	public String createWorkflow(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updateWorkflow(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow";
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String parmeterizedStrtWorkflow(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/parameterized/start";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String setWorkflowStatus(String workflowId, String refreshInterval, String status) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/setexecutionstatus/" + encode(workflowId) + "/"
				+ encode(refreshInterval) + "/" + encode(status);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getworkflowByWorkflowId(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String startWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/" + encode(workflowId) + "/start";
		return getDataFromServer(url);	
	}

	@Override
	public String startWorkflowWithResource(String workflowId, String mermory, String cores) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/" + encode(workflowId) + "/start/" + encode(mermory) + "/" + encode(cores);
		return getDataFromServer(url);	
	}

	@Override
	public String getWorkflowStatus(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/" + encode(workflowId) + "/status";
		return getDataFromServer(url);	
	}

	@Override
	public String stopWorkflow(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/" + encode(workflowId) + "/stop";
		return getDataFromServer(url);	
	}

	@Override
	public String deleteAllSamples(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/sample/" + encode(workflowId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getAllSamples(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/sample/" + encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteAllSamplesByStageId(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/sample/" + encode(workflowId) + "/" + encode(stageId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getAllSamplesByStageId(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/sample/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String insertSamples(String workflowId, String stageId, String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/sample/" + encode(workflowId) + "/" + encode(stageId);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String deleteProfile() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/profile";
		return deleteDataInServer(url);	
	}

	@Override
	public String getProfiles() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/profile";
		return getDataFromServer(url);	
	}

	@Override
	public String createProfile(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/profile";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String updateProfile(String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/profile";
		return updateDataInServer(url, jsonToBeSent);	
	}

	@Override
	public String deleteProfileById(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/profile/" + encode(profileId);
		return deleteDataInServer(url);	
	}

	@Override
	public String getProfileById(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/profile/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String getSampleDataOfEs(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/sampledata/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String getSchemaDataOfEs(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/schema/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	private String getDataFromServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	private String postStringDataToServer(String url, String jsonString) throws Exception{
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	private String deleteDataInServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_DELETE_DATA_ON_SERVER_MSG);
		}
	}
	
	private String updateDataInServer(String url, String jsonToBeSent) throws Exception{
		String serverResponse[] = ServiceCallHelper.updateDataInServer(url, jsonToBeSent);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_UPDATE_DATA_ON_SERVER_MSG);
		}
	}
	
	
	public static String postFileToServer(String url, File file) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create()
					.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("file", file);
			httpPost.setEntity(entityBuilder.build());
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return serverResponse[1];
	}
	
	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	@Override
	public String getCaseAlerts(String caseId) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/alerts";
		return getDataFromServer(url);
	}

	@Override
	public String createCaseAlert(String jsonToBeSent, String caseId) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/alerts";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String getLatestCaseAlerts(String caseId, String count) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/alerts/" + encode(count);
		return getDataFromServer(url);
	}

	@Override
	public String getCaseTimeline(String caseId) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/timeline";
		return getDataFromServer(url);
	}

	@Override
	public String createCaseTimeline(String caseId, String jsonToBeSent) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/timeline";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String getCasesSummary() throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/summary";
		return getDataFromServer(url);
	}

	@Override
	public String createCasesSummary(String jsonToBeSent) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/summary";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String getCasesSummaryWithLimit(String limit) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/summary/" + encode(limit);
		return getDataFromServer(url);
	}

	@Override
	public String getCasesSummaryWithcaseId(String caseId) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/summary";
		return getDataFromServer(url);
	}

	@Override
	public String createCasesSummaryWithcaseId(String caseId, String jsonToBeSent) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/summary";
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String getSimilarCases(String caseId, Integer limit) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/similar/" + limit;
		return getDataFromServer(url);
	}

	@Override
	public String getCaseTimelineWithLimit(String caseId, Integer limit) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/timeline/" + limit;
		return getDataFromServer(url);
	}

	@Override
	public String getCaseTimelineFoundedWithLimit(String caseId, Integer limit) throws Exception {
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/" + encode(caseId) + "/timeline/founded/" + limit;
		return getDataFromServer(url);
	}

	@Override
	public String getCasesSumary(Long userId, CaseVo caseVo,Integer limit) throws Exception {
		List<Long> caseIds = caseService.getMyCaseIds(userId, caseVo,limit);
		List<String> caseIdList = Lists.transform(caseIds, caseId -> caseId.toString());
		String caseIdsCommaSeperated = caseIdList.stream().collect(Collectors.joining(","));		
		String url = BIG_DATA_CASE_MANAGEMENT_URL + "/casesSummary/" + caseIdsCommaSeperated;
		return getDataFromServer(url);
	}

	@Override
	public String getDatabasesByProfileId(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/mysql/api/databases/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String getDatabaseTablesByProfileId(String profileId, String database) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/mysql/api/tables/" + encode(database)+"/"+ encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String assignRiskScoreToGraph(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/graph/api/risk/score";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String createCassandraDatabase(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/cassandra/api/create_keyspace";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String createCassandraTable(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/cassandra/api/create_family";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getCassandraDatabasesList(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/cassandra/api/keyspaces/"+ encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String getSampleCassandraData(String workflowId, String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/cassandra/api/sampledata/" + encode(workflowId)+"/"+encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String getSampleCassandraSchema(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/cassandra/api/schema/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String getListCassandraDatabaseTables(String database, String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/cassandra/api/families/" + encode(database) + "/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String createNotebookForWorkflow(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/notebook";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getAllNotebooksByWorkflowId(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/notebook/list/"+ encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String getNotebookById(String id) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/notebook/"+ encode(id);
		return getDataFromServer(url);	
	}

	@Override
	public String deleteNotebook(String workflowId, String notebookName) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/notebook/"+ encode(workflowId)+"/"+encode(notebookName);
		return deleteDataInServer(url);
	}

	@Override
	public String getNotebookByWorkFlowIdAndNotebookName(String workflowId, String notebookName) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/notebook/"+ encode(workflowId)+"/"+encode(notebookName);
		return getDataFromServer(url);	
	}

	@Override
	public String runParentStagesForAStage(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow/"+ encode(workflowId)+"/"+encode(stageId)+"/start";
		return getDataFromServer(url);	
	}

	@Override
	public String updateNotebook(String workflowId, String notebookName,String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/notebook/"+ encode(workflowId)+"/"+encode(notebookName);
		return updateDataInServer(url,jsonToBeSent);	
	}


	@Override
	public String deleteFileByPath(String path) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/local/api/source/delete/"+ encode(path);
		return deleteDataInServer(url);
	}

	@Override
	public String getHdfsProfileForFileSource() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/local/api/source/profile";
		return getDataFromServer(url);	
	}

	@Override
	public String saveSourceFile(String stageId, String workflowId, MultipartFile uploadFile) throws Exception {
		File file = null;
		String url = WORKFLOW_EXTENTION_API + "/local/api/source/save_file/"+encode(workflowId)+"/"+encode(stageId);
		if (uploadFile != null && uploadFile.getSize() > 0) {
			String originalFile = new String(uploadFile.getOriginalFilename());
			String ext = FilenameUtils.getExtension(originalFile);
			String fileNameWithoutExtension = FilenameUtils.getBaseName(originalFile);
			
			file = File.createTempFile(fileNameWithoutExtension, ext);
			uploadFile.transferTo(file);
			
		
	}
		return postFileToServer(url, file);}

	@Override
	public String ListWorkFlowCasesWithData() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow-cases/list";
		return getDataFromServer(url);	
	}

	@Override
	public String deleteWorkflowCaseById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow-cases/"+ encode(workflowId);
		return deleteDataInServer(url);
	}

	@Override
	public String getWorkFlowCaseById(String workflowId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow-cases/"+ encode(workflowId);
		return getDataFromServer(url);	
	}

	@Override
	public String saveWorkflowCase(String workflowId, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow-cases/"+ encode(workflowId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String updateWorkflowCase(String workflowId, String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/es/api/workflow-cases/"+ encode(workflowId);
		return updateDataInServer(url,jsonToBeSent);	
	}
}
