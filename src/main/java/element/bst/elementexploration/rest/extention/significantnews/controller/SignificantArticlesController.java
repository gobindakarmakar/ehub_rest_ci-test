package element.bst.elementexploration.rest.extention.significantnews.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantArticles;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantArticlesService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Significant Articles API" })
@RestController
@RequestMapping("/api/significantArticles")
public class SignificantArticlesController extends BaseController {

	@Autowired
	private SignificantArticlesService significantArticlesService;

	@ApiOperation("Save significant Articles")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Significant news saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveSignificantArticles", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSignificantArticles(@Valid @RequestBody SignificantArticles significantArticles,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(significantArticlesService.saveSignificantArticles(significantArticles,getCurrentUserId()),
				HttpStatus.OK);

	}

	@ApiOperation("Save Article Sentiments")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Significant news saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveArticleSentiment", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveArticleSentiment(@Valid @RequestBody SignificantArticles significantArticles,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(significantArticlesService.saveArticleSentiment(significantArticles),
				HttpStatus.OK);
	}

	@ApiOperation("Delete the significant Articles")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Significant news deleted successfully.") })
	@DeleteMapping(value = "/saveArticleSentiment", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteSignificantNews(@RequestParam("uuid") String uuid,@RequestParam(required = false) String comment,
			@RequestParam("token") String token, HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		boolean result = significantArticlesService.deleteSignificantArticles(uuid,comment,userIdTemp);
		if (result) {
			return new ResponseEntity<>(new ResponseMessage("Significant news deleted successfully."), HttpStatus.OK);
		} else {
			throw new NoDataFoundException();
		}
	}
	
	
	@ApiOperation("get all significant News")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Significant news.") })
	@PostMapping(value = "/getAllSignificantNews", produces = { "application/json; charset=UTF-8" },consumes= {"application/json; charset=UTF-8"})
	public ResponseEntity<?> getAllSignificantNews(@RequestParam("entityId") String entityId,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		JSONArray array = significantArticlesService.getAllSignificantNews(entityId);
			return new ResponseEntity<>(array.toString(), HttpStatus.OK);
		}
}