package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;

public class Rules implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int matchScore=0;
	
	private String ruleName;	

	public int getMatchScore() {
		return matchScore;
	}

	public void setMatchScore(int matchScore) {
		this.matchScore = matchScore;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}	

}
