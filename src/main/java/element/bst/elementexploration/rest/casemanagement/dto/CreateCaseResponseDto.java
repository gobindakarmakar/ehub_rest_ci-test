package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;

/**
 * @author Jalagari Paul
 *
 */
public class CreateCaseResponseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
