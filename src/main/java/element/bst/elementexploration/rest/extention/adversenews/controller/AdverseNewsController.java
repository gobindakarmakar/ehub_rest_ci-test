package element.bst.elementexploration.rest.extention.adversenews.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.adversenews.service.AdverseNewsService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Adverse News API" })
@RestController
@RequestMapping("/api/adverseNews")
public class AdverseNewsController extends BaseController {

	@Autowired
	AdverseNewsService adverseNewsService;

	@ApiOperation("Gets entities list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/entities", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntities(@RequestParam("token") String token, HttpServletRequest request)
			throws Exception {
		return new ResponseEntity<>(adverseNewsService.getEntities(), HttpStatus.OK);
	}

	@ApiOperation("Gets companies list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "File can not be empty"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILES_TO_SERVER_MSG) })
	@PostMapping(value = "/transactions", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getcompanies(@RequestParam("tx-file") MultipartFile txFile,
			@RequestParam("ent-file") MultipartFile entFile, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {

		File[] files = new File[2];
		files[0] = new File(txFile.getOriginalFilename());
		files[1] = new File(entFile.getOriginalFilename());
		txFile.transferTo(files[0]);
		entFile.transferTo(files[1]);
		String data = adverseNewsService.postFiles(files);
		return new ResponseEntity<>(data, HttpStatus.OK);
	}

	@ApiOperation("Gets news summary")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/summary/{type}/{name}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSummary(@PathVariable("type") String type, @PathVariable("name") String name,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getSummary(type, name), HttpStatus.OK);
	}

	@ApiOperation("Gets adverse news graph by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/{type}/{name}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGraphById(@PathVariable("type") String type, @PathVariable("name") String name,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getGraphById(type, name), HttpStatus.OK);
	}

	/*@ApiOperation("Gets adverse news by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/news/{type}/{id}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAdversenNewsById(@PathVariable("type") String type, @PathVariable("id") String id,
			@RequestParam(value = "offset", required = false) Double offset,
			@RequestParam(value = "limit", required = false) Double limit, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getAdversenNewsById(type, id, offset, limit), HttpStatus.OK);
	}*/

	/*@ApiOperation("Gets search result list of a search request")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/news/search", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getSearchResultList(@RequestBody String newsSearchRequest,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getSearchResultList(newsSearchRequest), HttpStatus.OK);
	}*/

	@ApiOperation("Gets adverse profile by ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/profile/org/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAdverseProfileById(@PathVariable String identifier, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getAdverseProfileById(identifier), HttpStatus.OK);
	}

	@ApiOperation("searches multiresources graph data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/graph/search/multisources", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getSearchMultiResourcesGraph(@RequestBody String newsSearchRequest,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getSearchMultiResourcesGraph(newsSearchRequest), HttpStatus.OK);
	}

	@ApiOperation("Gets search result list of a event search request")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/events/search", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getSearchEventResultList(@RequestBody String newsSearchRequest,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getSearchEventResultList(newsSearchRequest), HttpStatus.OK);
	}

	@ApiOperation("Gets search result list of a event aggregate request")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/events/aggregate", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getSearchEventAggregate(@RequestBody String newsSearchRequest,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(adverseNewsService.getSearchEventAggregate(newsSearchRequest), HttpStatus.OK);
	}

}
