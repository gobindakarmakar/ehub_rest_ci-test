<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.util.ResourceBundle" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.*" %>
<%@page import="java.util.Properties" %>
<% 
//ResourceBundle resource = ResourceBundle.getBundle("ehub-ui");
/*String activiti_url=resource.getString("activiti.url");
String ehub_fe_api=resource.getString("ehub.fe.api");
String kyc_questionnaire_url=resource.getString("kyc.questionnaire.url");
String policy_enforcement_url=resource.getString("policy.enforcement.url");
String element_main_url=resource.getString("element.main.url");
String ehub_rest_url=resource.getString("element.api.base.url");
String ehub_rest_end_point=resource.getString("element.rest.endpoint");*/

FileInputStream fis = new FileInputStream("/opt/bst-ehub-rest/conf/ehub-ui.properties");
Properties prop = new Properties();

try {
   prop.load(fis);
}
catch (IOException e) {
    e.printStackTrace();
}

String activiti_url=prop.getProperty("ACTIVITI_URL");
String ehub_fe_api=prop.getProperty("ELEMENT_MAIN_URL");
String kyc_questionnaire_url=prop.getProperty("QUESTIONNAIRE_URL");
String policy_enforcement_url=prop.getProperty("POLICY_ENFORCEMENT_URL");
String element_main_url=prop.getProperty("ELEMENT_MAIN_URL");
String ehub_rest_url=prop.getProperty("ELEMENT_REST_DOMAIN");
String ehub_rest_end_point=prop.getProperty("ELEMENT_REST_DOMAIN");
%>



{"activiti_url" : "<%= activiti_url %>","ehub_fe_api":"<%= ehub_fe_api%>","kyc_questionnaire_url":"<%= kyc_questionnaire_url%>"
,"policy_enforcement_url":"<%=  policy_enforcement_url %>",
"element_main_url":"<%=  element_main_url %>",
"ehub_rest_url":"<%=  ehub_rest_url %>",
"ehub_rest_end_point":"<%=  ehub_rest_end_point %>"}

