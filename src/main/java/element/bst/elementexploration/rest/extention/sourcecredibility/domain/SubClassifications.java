package element.bst.elementexploration.rest.extention.sourcecredibility.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_sub_classifcations")
public class SubClassifications implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long subClassificationId;

	@Column(name = " subClassifcationName")
	private String subClassifcationName;

	@ManyToOne
	@JoinColumn(name = "classification_id")
	private Classifications classifications;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "subClassifications")
	private List<DataAttributes> dataAttributes;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "subClassifications")
	private List<SourceCredibility> sourceCredibilityList;

	public Long getSubClassificationId() {
		return subClassificationId;
	}

	public void setSubClassificationId(Long subClassificationId) {
		this.subClassificationId = subClassificationId;
	}

	public String getSubClassifcationName() {
		return subClassifcationName;
	}

	public void setSubClassifcationName(String subClassifcationName) {
		this.subClassifcationName = subClassifcationName;
	}

	public Classifications getClassifications() {
		return classifications;
	}

	public void setClassifications(Classifications classifications) {
		this.classifications = classifications;
	}

	public List<DataAttributes> getDataAttributes() {
		return dataAttributes;
	}

	public void setDataAttributes(List<DataAttributes> dataAttributes) {
		this.dataAttributes = dataAttributes;
	}

	public List<SourceCredibility> getSourceCredibilityList() {
		return sourceCredibilityList;
	}

	public void setSourceCredibilityList(List<SourceCredibility> sourceCredibilityList) {
		this.sourceCredibilityList = sourceCredibilityList;
	}

}
