/** * START --- Sticky Notes *** */
'use strict';
activitiApp.directive('stickyNotesComponent', ['$document', function($document) {
	   return {
			scope: {
				index: '=',
				visible: '=',
				editable: '=',
				saveable: '=',
				deleteable: '=',
				text: '=',
				onClickStickyNotesClose: '&',
				onClickStickyNotesEdit: '&',
				onClickStickyNotesSave: '&',
				onClickStickyNotesDelete: '&'
			},
		    restrict: 'E',
		    template: '<div class="sticky-notes" style="left: {{(-300)+(index*15)}}px; top: {{(100)+(index*25)}}px;">'
						+	'<div class="pull-right">'
							+	'<span><a href ng-click="onClickStickyNotesClose({ev: $event, index: index})"><i class="fa fa-times"></i></a></span>'
						+	'</div>'
						+	'<div class="pull-left">'
							+	'<span><a href title="Edit" ng-click="onClickStickyNotesEdit({ev: $event, index: index})" ><i id="sticky-notes-edit-button-{{index}}" class="fa fa-edit" aria-hidden="true"></i></a></span>'
							+	'<span><a href title="Save" ng-disabled="{{!saveable}}" ng-click="onClickStickyNotesSave({ev: $event, index: index})"><i id="sticky-notes-save-button-{{index}}" class="fa fa-save sticky-notes-icon-disabled" aria-hidden="true"></i></a></span>'
							+	'<span><a href title="Delete" ng-disabled="{{!deleteable}}" ng-click="onClickStickyNotesDelete({ev: $event, index: index})"><i id="sticky-notes-delete-button-{{index}}" class="fa fa-trash sticky-notes-icon-disabled" aria-hidden="true"></i></a></span>'
						+	'</div>'
						+	'<div class="sticky-notes-content">'
							+   '<textarea id="sticky-notes-content-data-{{index}}" ng-disabled="{{!editable}}">{{text}}'
							+	'</textarea>'
						+	'</div>'
				   +	'</div>',
		      link: function(scope, elm, attrs) {
		          var startX, startY, initialMouseX, initialMouseY;
		          elm.css({position: 'absolute'});
	
		          elm.on('mousedown', function($event) {
			            startX = elm.prop('offsetLeft');
			            startY = elm.prop('offsetTop');
			            initialMouseX = $event.clientX;
			            initialMouseY = $event.clientY;
			            $document.on('mousemove', mousemove);
			            $document.on('mouseup', mouseup);
		          });
	
		          function mousemove($event) {
			            var dx = $event.clientX - initialMouseX;
			            var dy = $event.clientY - initialMouseY;
			            elm.css({
			              top:  startY + dy + 'px',
			              left: startX + dx + 'px'
			            });
			            return false;
		          }
	
		          function mouseup() {
			            $document.off('mousemove', mousemove);
			            $document.off('mouseup', mouseup);
		          }
		        }
		  };
}]);

activitiApp.directive('stickyNotesMenuComponent', ['$document', function($document) {
	  return {
			scope: {
				onClickStickyNotesMenuClose: '&',
				toggleStickyNotesItemDisplay: '&',
				stickyNotesAdd: '&'
			},
		    restrict: 'E',
		    template: '<div class="sticky-notes-menu">'
						+	'<div class="pull-right">'
							+	'<span><a href ng-click="onClickStickyNotesMenuClose()"><i class="fa fa-times"></i></a></span>'
						+	'</div>'
						+	'<div class="pull-left">'
							+	'<i class="fa fa-sticky-note" aria-hidden="true"></i>'
							+	'<span class="text-uppercase">Sticky Notes</span>'
						+	'</div>'
						+	'<div class="sticky-notes-menu-content">'
							+	'<span><a href title="Add" ng-click="stickyNotesAdd()"><i class="fa fa-plus" aria-hidden="true"></i><b class="sticky-notes-actionName text-uppercase">Add</b></a></span>'
							+	'&nbsp;&nbsp;&nbsp;'
							+	'<span><a href title="Show/View" ng-click="toggleStickyNotesItemDisplay()"><i id="sticky-notes-show-hide-button" class="fa fa-toggle-on" aria-hidden="true"></i><b class="sticky-notes-actionName text-uppercase">Show/View</b></a></span>'
						+	'</div>'
		    		+  '</div>',
		    link: function(scope, elm, attrs) {
		          var startX, startY, initialMouseX, initialMouseY;
		          elm.css({position: 'absolute'});
		
		          elm.bind('mousedown', function($event) {
			            startX = elm.prop('offsetLeft');
			            startY = elm.prop('offsetTop');
			            initialMouseX = $event.clientX;
			            initialMouseY = $event.clientY;
			            $document.bind('mousemove', mousemove);
			            $document.bind('mouseup', mouseup);
		          });
		
		          function mousemove($event) {
			            var dx = $event.clientX - initialMouseX;
			            var dy = $event.clientY - initialMouseY;
			            elm.css({
			              top:  startY + dy + 'px',
			              left: startX + dx + 'px'
			            });
			            return false;
		          }
		
		          function mouseup() {
			            $document.unbind('mousemove', mousemove);
			            $document.unbind('mouseup', mouseup);
		          }
	        }
	  };
}]);
/** * END --- Sticky Notes ** */