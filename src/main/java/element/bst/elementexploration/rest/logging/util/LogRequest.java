package element.bst.elementexploration.rest.logging.util;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import element.bst.elementexploration.rest.logging.enumType.GranularityEnum;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import io.swagger.annotations.ApiModelProperty;

public class LogRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Log level of log requested")
	private LogLevels level;

	@ApiModelProperty(value = "Type of log requested")
	private String type;

	@ApiModelProperty(value = "Flag of log requested")
	private Integer flag;

	@ApiModelProperty(value = "UserId ")
	private Long userId;

	@ApiModelProperty(value = "caseId")
	private Long caseId;

	@ApiModelProperty(value = "docId")
	private Long docId;
	
	@ApiModelProperty(value = "typeId")
	private Long typeId;

	@ApiModelProperty(value = "EntityId")
	private Long entityId;
	
	@ApiModelProperty(value = "keyword for search")
	private String keyword;
	
	@ApiModelProperty(value = "Page number for Pagination")
	private Integer pageNumber;

	@ApiModelProperty(value = "Records per page  for  Pagination")
	private Integer recordsPerPage;

	@ApiModelProperty(value = "EntityId of log requested")
	private String orderBy;

	@ApiModelProperty(value = "EntityId of log requested")
	private String orderIn;
	
	@ApiModelProperty(value = "Date from",example="dd-MM-yyyy")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateFrom;
	
	@ApiModelProperty(value = "Date to",example="dd-MM-yyyy")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateTo;
	
	@ApiModelProperty(value = "Granularity",example="daily,weekly,monthly,yearly")
	@Enumerated(EnumType.STRING)
	private GranularityEnum granularity;

	public LogLevels getLevel() {
		return level;
	}

	public void setLevel(LogLevels level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getRecordsPerPage() {
		return recordsPerPage;
	}

	public void setRecordsPerPage(Integer recordsPerPage) {
		this.recordsPerPage = recordsPerPage;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderIn() {
		return orderIn;
	}

	public void setOrderIn(String orderIn) {
		this.orderIn = orderIn;
	}

	public LocalDateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDateTime getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDateTime dateTo) {
		this.dateTo = dateTo;
	}

	public GranularityEnum getGranularity() {
		return granularity;
	}

	public void setGranularity(GranularityEnum granularity) {
		this.granularity = granularity;
	}
	

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
