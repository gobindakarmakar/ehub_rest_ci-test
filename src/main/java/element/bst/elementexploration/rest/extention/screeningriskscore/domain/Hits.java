package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Hits implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "profile")
	private ScreeningProfile profile;
	
	@ApiModelProperty(value = "watch lists")
	@JsonProperty("watchlists")
	private List<Watchlists> watchLists;
	
	@ApiModelProperty(value = "matches")
	private Matches matches;
	
	@ApiModelProperty(value = "score")
	private int riskScore;
	
	@ApiModelProperty(value = "score")
	private String ruleName;

	public ScreeningProfile getProfile() {
		return profile;
	}

	public void setProfile(ScreeningProfile profile) {
		this.profile = profile;
	}

	public List<Watchlists> getWatchLists() {
		return watchLists;
	}

	public void setWatchLists(List<Watchlists> watchLists) {
		this.watchLists = watchLists;
	}

	public Matches getMatches() {
		return matches;
	}

	public void setMatches(Matches matches) {
		this.matches = matches;
	}	

	public int getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(int riskScore) {
		this.riskScore = riskScore;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
}
