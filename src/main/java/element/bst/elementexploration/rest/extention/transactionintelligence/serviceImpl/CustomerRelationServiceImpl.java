package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerRelationDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerRelationShipDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerRelationService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service
@Transactional("transactionManager")
public class CustomerRelationServiceImpl extends GenericServiceImpl<CustomerRelationShipDetails, Long>
		implements CustomerRelationService {

	@Autowired
	public CustomerRelationServiceImpl(GenericDao<CustomerRelationShipDetails, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	CustomerRelationDao customerRelationDao;

	@Override
	public boolean fetchCustomerRelationDetails(MultipartFile multipartFile) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
		String line = null;
		int i = 1;
		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if(line.contains(","))
					data = line.split(",");
				if(line.contains(";"))
					data = line.split(";");
				CustomerRelationShipDetails customerRelationShipDetails = new CustomerRelationShipDetails();
				if(data!=null && data[0]!=null)
					customerRelationShipDetails.setCustomerNumber(data[0]);
				customerRelationShipDetails.setRelatedCustomerNumber(data[1]);
				customerRelationShipDetails.setRelationShipDefinition(data[2]);
				customerRelationDao.create(customerRelationShipDetails);
			}
			i++;
		}

		return true;
	}

}
