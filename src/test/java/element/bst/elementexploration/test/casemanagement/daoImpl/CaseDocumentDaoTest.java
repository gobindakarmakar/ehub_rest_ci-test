package element.bst.elementexploration.test.casemanagement.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseDocumentDaoImpl;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CaseDocumentDaoTest {
	
	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@InjectMocks
	CaseDocumentDaoImpl caseDocumentDao;
	
	public List<CaseDocDetails> caseDocDetailsList = new ArrayList<CaseDocDetails>();
	
	public List<DocAggregatorDto> docAggregatorDtoList = new ArrayList<DocAggregatorDto>(); 
	
	@Before
	public void setupMock() {
		caseDocDetailsList.add(new CaseDocDetails());
		caseDocDetailsList.add(new CaseDocDetails());
		docAggregatorDtoList.add(new DocAggregatorDto(1, 1L));
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getAllDocumentsForCaseSeedTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDocDetailsList);
		List<CaseDocDetails> caseDocDetailsListNew = caseDocumentDao.getAllDocumentsForCaseSeed(1L, 1,"name", 1, 11);
		assertEquals(2,caseDocDetailsListNew.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllDocumentsForCaseSeed_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		List<CaseDocDetails> caseDocDetailsListNew = caseDocumentDao.getAllDocumentsForCaseSeed(1L, 1,"name", 1, 11);
		assertEquals(0,caseDocDetailsListNew.size());
	}
	
	@Test
	public void getCaseIdFromDocTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		Long docId = caseDocumentDao.getCaseIdFromDoc(1L);
		assertEquals(new Long("2"),docId);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCaseIdFromDoc_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		 caseDocumentDao.getCaseIdFromDoc(1L);
	}
	
	@Test
	public void countAllDocumentsForCaseSeedTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(caseDocDetailsList);
		Long count = caseDocumentDao.countAllDocumentsForCaseSeed(1L,3);
		assertEquals(new Long("2"),count);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void countAllDocumentsForCaseSeed_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		Long count = caseDocumentDao.countAllDocumentsForCaseSeed(1L,3);
		assertEquals(new Long("0"),count);
	}
	
	@Test
	public void docAggregatorTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(docAggregatorDtoList);
		List<DocAggregatorDto> docAggregatorDtoListnew = caseDocumentDao.docAggregator(1L, new Date(),new Date());
		assertEquals(1,docAggregatorDtoListnew.size());
	}
	
	
	@Test
	public void docAggregator_whenFromDateIsNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(docAggregatorDtoList);
		List<DocAggregatorDto> docAggregatorDtoListnew = caseDocumentDao.docAggregator(1L, null,new Date());
		assertEquals(1,docAggregatorDtoListnew.size());
	}
	
	@Test
	public void docAggregator_whenToDateIsNullTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(docAggregatorDtoList);
		List<DocAggregatorDto> docAggregatorDtoListnew = caseDocumentDao.docAggregator(1L,new Date(),null);
		assertEquals(1,docAggregatorDtoListnew.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void docAggregator_whenExceptionTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		List<DocAggregatorDto> docAggregatorDtoListnew = caseDocumentDao.docAggregator(1L,new Date(),null);
		assertEquals(0,docAggregatorDtoListnew.size());
	}
	
	
	
	

}
