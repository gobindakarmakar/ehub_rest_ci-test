package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class InputStatsResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("counterparty")
	private List<CounterPartyDto> counterPartyDto;
	@JsonProperty("total")
	private TotalDto totalDto;
	@JsonProperty("country")
	private List<CountryAggDto> country;

	//private EnrolAccTxInputReqDto accTxInputReqDto;

	public List<CounterPartyDto> getCounterPartyDto() {
		return counterPartyDto;
	}

	public void setCounterPartyDto(List<CounterPartyDto> counterPartyDto) {
		this.counterPartyDto = counterPartyDto;
	}

	public List<CountryAggDto> getCountry() {
		return country;
	}

	public void setCountry(List<CountryAggDto> country) {
		this.country = country;
	}

	/*public EnrolAccTxInputReqDto getAccTxInputReqDto() {
		return accTxInputReqDto;
	}

	public void setAccTxInputReqDto(EnrolAccTxInputReqDto accTxInputReqDto) {
		this.accTxInputReqDto = accTxInputReqDto;
	}*/

	public TotalDto getTotalDto() {
		return totalDto;
	}

	public void setTotalDto(TotalDto totalDto) {
		this.totalDto = totalDto;
	}

}
