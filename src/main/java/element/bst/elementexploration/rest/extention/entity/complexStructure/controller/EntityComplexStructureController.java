package element.bst.elementexploration.rest.extention.entity.complexStructure.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.entity.complexStructure.dto.EntityComplexStructureDto;
import element.bst.elementexploration.rest.extention.entity.complexStructure.service.EntityComplexStructureService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hanuman
 *
 */
@Api(tags = { "Entity Complex Structure API" })
@RestController
@RequestMapping("/api/entityComplexStructure")
public class EntityComplexStructureController extends BaseController{

	@Autowired
	EntityComplexStructureService entityComplexStructureService;

	@ApiOperation("Get All EntityComplex Structure")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getAllEntityComplexStructure", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllEntityComplexStructure(HttpServletRequest request, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(entityComplexStructureService.getAllEntityComplexStructure(),HttpStatus.OK);
	}


	@ApiOperation("Get EntityComplex Structure")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getEntityComplexStructure", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntityComplexStructure(HttpServletRequest request,@RequestParam("token") String token,@RequestParam(required = true) String entityId,@RequestParam(required = true) String entitySource,@RequestParam(required = true) String entityName) throws Exception {
		EntityComplexStructureDto complexStructureDto=entityComplexStructureService.getEntityComplexStructure(entityId, entitySource,entityName);
		if(complexStructureDto==null) {
			return new ResponseEntity<>(false, HttpStatus.OK);
		}else {

			return new ResponseEntity<>(complexStructureDto,HttpStatus.OK);
		}
	}

/*	@ApiOperation("Save Entity Complex Structure")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveEntityComplexStructure", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveEntityComplexStructure(HttpServletRequest request, @RequestParam String token,
			@RequestBody EntityComplexStructureDto entityComplexStructureDto) throws Exception {
		EntityComplexStructureDto entityComplexStructureDtos = entityComplexStructureService.saveEntityComplexStructure(entityComplexStructureDto);
		if(entityComplexStructureDtos!=null) {
			return new ResponseEntity<>(entityComplexStructureDtos,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(new ResponseMessage("Complex Structure already exist"), HttpStatus.NOT_FOUND);
		}
	}*/


	@ApiOperation("Save Entity Complex Structure")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateEntityComplexStructure", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateEntityComplexStructure(HttpServletRequest request, @RequestParam String token,
			@RequestParam(required = true) String entityName,@RequestParam(required = true) String entityId,@RequestParam(required = true) String entitySource,@RequestParam(required = true) boolean isComplexStructure) throws Exception {
		long userIdTemp = getCurrentUserId();
		
		EntityComplexStructureDto entityComplexStructureDto = entityComplexStructureService.saveOrUpdateEntityComplexStructure(entityId,entityName,entitySource,isComplexStructure,userIdTemp);
		if(entityComplexStructureDto==null) {
			return new ResponseEntity<>(new ResponseMessage("Complex Structure Not Updated"), HttpStatus.NOT_FOUND);
		}else {

			return new ResponseEntity<>(entityComplexStructureDto,HttpStatus.OK);
		}
	}

}