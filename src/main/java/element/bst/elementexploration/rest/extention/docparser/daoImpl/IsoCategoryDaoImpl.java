package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.IsoCategoryDao;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("isoCategoryDao")
public class IsoCategoryDaoImpl extends GenericDaoImpl<IsoCategory, Long> implements IsoCategoryDao{

	public IsoCategoryDaoImpl() {
		super(IsoCategory.class);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IsoCategory> getISOCategoriesByDocId(Long docId) {
		List<IsoCategory> isoCategoryList=this.getCurrentSession().createQuery("select isoc from IsoCategory isoc where isoc.docId=" +docId).getResultList();
		return isoCategoryList;
	}

	@Override
	public IsoCategory getISOCategoriesByCodeAndDocId(String level1Code, Long docId) {
		
		IsoCategory isoCategory = null;
		try{
		 Query query = this.getCurrentSession().createQuery("select isoc from IsoCategory as isoc where isoc.docId = "+docId+" and isoc.level1Code = '"+level1Code+"'");
				 isoCategory = (IsoCategory)query.getSingleResult();
		//IsoCategory isoCategory = (IsoCategory) this.getCurrentSession().createQuery("from IsoCategory isoc where isoc.docId=" +docId+" and isoc.level1Code ="+level1Code).getSingleResult();
				 return isoCategory;
		}catch (Exception e) {
			return isoCategory;
		}
		
	}


	
}
