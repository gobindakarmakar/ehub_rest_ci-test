package element.bst.elementexploration.rest.util;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.extention.docparser.dto.CoordinatesResponseDto;

@Component
public class PdfReaderNewTemplateUtil 
{
	public static String parsePdfFile(byte[] byteArrayFile,CoordinatesResponseDto coordinatesDto,boolean answerValue) throws IOException 
	{
		
		List<String> items = null;
		String builder = "";
		if(!answerValue)
		     items = Arrays.asList(coordinatesDto.getQuestionCoordinates().split("\\s*,\\s*"));
		else
			items = Arrays.asList(coordinatesDto.getAnswerCoordinates().split("\\s*,\\s*"));
		PDDocument document =  PDDocument.load(byteArrayFile); 
		  PDFTextStripperByArea textStripper = new	  PDFTextStripperByArea();
		  Rectangle2D rect = new	  java.awt.geom.Rectangle2D.Float(Float.valueOf(items.get(0)),Float.valueOf(
	  items.get(1)),Float.valueOf(items.get(2)),Float.valueOf(items.get(3)));
	  textStripper.addRegion("region", rect);
	  PDPage docPage =  document.getPage(coordinatesDto.getPageNumber()-1);
	  textStripper.extractRegions(docPage); 
	  builder =  textStripper.getTextForRegion("region");
		
		/*
		 * List<JSONObject> jsonObjectsList = new ArrayList<JSONObject>(); PDDocument
		 * document = null; StringBuilder builder = new StringBuilder(); try { document
		 * = PDDocument.load(byteArrayFile); PDFTextStripper stripper = new
		 * CustomTextStripperNewTemplate(byteArrayFile, jsonObjectsList);
		 * stripper.setSortByPosition(true);
		 * 
		 * for (int i = coordinatesDto.getPageNumber(); i <=
		 * coordinatesDto.getPageNumber(); i++)// change to page number {
		 * stripper.setStartPage(i); stripper.setEndPage(i); Writer dummy = new
		 * OutputStreamWriter(new ByteArrayOutputStream()); // getting page content as
		 * single character stripper.writeText(document, dummy); for (JSONObject s :
		 * jsonObjectsList) { String val = s.get("val").toString(); float x = (float)
		 * s.get("xVal"); float y = (float) s.get("yYal"); List<String> items = null;
		 * 
		 * if(!answerValue) items =
		 * Arrays.asList(coordinatesDto.getQuestionCoordinates().split("\\s*,\\s*"));
		 * else items =
		 * Arrays.asList(coordinatesDto.getAnswerCoordinates().split("\\s*,\\s*"));
		 * 
		 * if (((y >= Float.valueOf(items.get(1)) && y <=
		 * Float.valueOf(items.get(1))+Float.valueOf(items.get(3))) && (x >=
		 * Float.valueOf(items.get(0)) && x <=
		 * Float.valueOf(items.get(0))+Float.valueOf(items.get(2)) )))// change to x and
		 * y values { builder.append(val); } } } // page ending for loop close
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 */
		return builder.toString().trim();
	}

}