package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 
 * @author Rambabu
 *
 */
@ApiModel("AML alert aggregates")
public class AmlAlertsAggregationResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="List of aml alerts aggregation")	
	private List<AlertScenarioDto> amlAlertsAggregation;

	public AmlAlertsAggregationResponseDto() {
		super();
		
	}

	public List<AlertScenarioDto> getAmlAlertsAggregation() {
		return amlAlertsAggregation;
	}

	public void setAmlAlertsAggregation(List<AlertScenarioDto> amlAlertsAggregation) {
		this.amlAlertsAggregation = amlAlertsAggregation;
	}	
	

}
