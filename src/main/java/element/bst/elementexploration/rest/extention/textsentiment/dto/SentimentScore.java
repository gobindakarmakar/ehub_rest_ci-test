package element.bst.elementexploration.rest.extention.textsentiment.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class SentimentScore implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("Positive")
	private double positive;
	
	@JsonProperty("Negative")
	private double negative;
	
	@JsonProperty("Neutral")
	private double neutral;
	
	@JsonProperty("Mixed")
	private double mixed;

	public double getPositive() {
		return positive;
	}

	public void setPositive(double positive) {
		this.positive = positive;
	}

	public double getNegative() {
		return negative;
	}

	public void setNegative(double negative) {
		this.negative = negative;
	}

	public double getNeutral() {
		return neutral;
	}

	public void setNeutral(double neutral) {
		this.neutral = neutral;
	}

	public double getMixed() {
		return mixed;
	}

	public void setMixed(double mixed) {
		this.mixed = mixed;
	}

}
