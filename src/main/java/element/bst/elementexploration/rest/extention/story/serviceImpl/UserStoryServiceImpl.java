package element.bst.elementexploration.rest.extention.story.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.story.dao.UserStoryDao;
import element.bst.elementexploration.rest.extention.story.domain.UserStory;
import element.bst.elementexploration.rest.extention.story.dto.StoryDto;
import element.bst.elementexploration.rest.extention.story.service.UserStoryService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("storyService")
public class UserStoryServiceImpl extends GenericServiceImpl<UserStory, Long> implements UserStoryService {

	public UserStoryServiceImpl(GenericDao<UserStory, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	private UserStoryDao storyDao;

	@Override
	@Transactional("transactionManager")
	public void markAsFavourite(String storyId, Long userId) {
		UserStory storyOld = storyDao.getFavouriteStoryByUserId(userId);
		if (storyOld == null) {
			UserStory story = new UserStory();
			story.setUserId(userId);
			story.setStoryId(storyId);
			story.setCreatedDate(new Date());
			storyDao.create(story);
		} else {
			storyOld.setStoryId(storyId);
			storyOld.setModifiedDate(new Date());
			storyDao.saveOrUpdate(storyOld);
		}

	}

	@Override
	@Transactional("transactionManager")
	public StoryDto getFavouriteStory(Long userId) {
		StoryDto responseDto=new StoryDto();
		UserStory story = storyDao.getFavouriteStoryByUserId(userId);
		if(story!=null){
			responseDto.setStatus(true);
			responseDto.setStoryId(story.getStoryId());
		}else{
			responseDto.setStatus(false);
		}
		return responseDto;
	}

}
