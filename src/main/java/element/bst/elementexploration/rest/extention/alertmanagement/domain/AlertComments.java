package element.bst.elementexploration.rest.extention.alertmanagement.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek
 *
 */

@ApiModel("Alert Comments")
@Entity
@Table(name = "bst_am_alert_comments")
public class AlertComments implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long commentId;

	@ApiModelProperty(value = "Comment for Alert")
	@Column(name = "comments" , columnDefinition = "TEXT")
	private String comments;
	
	@ApiModelProperty(value = "associated id of Alert")
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "alertId")
	private Alerts alertId;
	
	@ApiModelProperty(value = "Created date for Alert comments")
	@Column(name = "created_date")
	private Date createdDate;

	@ApiModelProperty(value = "updated date for Alert comments")
	@Column(name = "updated_date")
	private Date updatedDate;
	
	@ApiModelProperty(value = "user comment for Alert")
	@Column(name = "updated_by")
	private Long updatedBy;
	
	public Long getCommentId() {
		return commentId;
	}

	public String getComments() {
		return comments;
	}

	public Alerts getAlertId() {
		return alertId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setAlertId(Alerts alertId) {
		this.alertId = alertId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
