package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDtoWithRolesObj;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserInfo;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "User Roles Controller" }, description = "Manages User Roles")
@RestController
@RequestMapping("/api/userRoles")
public class UserRolesController extends BaseController {

	@Autowired
	UsersService usersService;

	@Autowired
	UserRolesService userRolesService;

	@Autowired
	RolesService rolesService;

	@Autowired
	private AuditLogService auditLogService;

	@ApiOperation("Get Roles of User")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful.") })
	@GetMapping(value = "/getRolesOfUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRolesOfUser(@RequestParam Long userId, @RequestParam String token,
			HttpServletRequest request) throws Exception {
		Users user = usersService.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		if (null != user) {
			List<UserRolesDto> userRoles = userRolesService.getRolesOfUserDto(userId);
			if (null != userRoles)
				return new ResponseEntity<>(userRoles, HttpStatus.OK);

		}
		return new ResponseEntity<>(new ResponseMessage("No roles found for user"), HttpStatus.OK);

	}
	
	@ApiOperation("Get Roles of User with Role object")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserInfo.class, message = "Operation successful.") })
	@GetMapping(value = "/getRolesOfUserWithObj", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRolesOfUserWithObj(@RequestParam Long userId, @RequestParam String token,@RequestParam Boolean isGroupRolesRequired,
			HttpServletRequest request) throws Exception {
		Users user = usersService.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		if (null != user) {
			List<UserRolesDtoWithRolesObj> userRoles = userRolesService.getRolesOfUserDtoWithRoleObj(userId,isGroupRolesRequired);
			if (null != userRoles)
				return new ResponseEntity<>(userRoles, HttpStatus.OK);

		}
		return new ResponseEntity<>(new ResponseMessage("No roles found for user"), HttpStatus.OK);

	}

	@ApiOperation("Assign Role to user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.ROLE_ASSIGN_SUCCESSFUL),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.ROLE_ASSIGN_FAIL) })
	@PostMapping(value = "/saveOrUpdateRoleOfUser", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateRoleOfUser(@RequestBody @Valid UserRolesDto userRolesDto,
			BindingResult results, @RequestParam("token") String token, HttpServletRequest request) throws Exception {
		
		Long userId = getCurrentUserId();
		boolean status = false;
		Roles existingRole = null;
		if (!results.hasErrors()) {
			if (userRolesDto.getUserId() != null) {
				Users user = usersService.find(userRolesDto.getUserId());
				user = usersService.prepareUserFromFirebase(user);
				if (null != user) {
					if (userRolesDto.getRoleId() != null) {
						try {
							existingRole = rolesService.find(userRolesDto.getRoleId());
							// List<Roles> rolesList= new ArrayList<Roles>();
							// rolesList= rolesService.findAll().stream().filter(role ->
							// role.getRoleId().equals(userRolesDto.getRoleId())).collect(Collectors.toList());
							// if(rolesList!=null && rolesList.size()>0)
							// existingRole= rolesList.get(0);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (existingRole != null) {
							// existing user role
							if (userRolesDto.getUserRoleId() != null) {
								UserRoles existingUserRole = userRolesService.find(userRolesDto.getUserRoleId());
								if (existingUserRole != null) {
									if (userRolesDto.getDescription() != null)
										existingUserRole.setDescription(userRolesDto.getDescription());
									if (userRolesDto.getRoleId() != null)
										existingUserRole.setRoleId(existingRole);
									if (userRolesDto.getUserId() != null)
										existingUserRole.setUserId(user);
									userRolesService.saveOrUpdate(existingUserRole);
									status = true;
									AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.STRING_TYPE_USER_ROLE,null, 
											ElementConstants.USER_ROLE_UPDATED);
									Users admin = null;
									if(getCurrentUserId() != null){
										admin = usersService.find(getCurrentUserId());
										admin = usersService.prepareUserFromFirebase(admin);
									}
										
									log.setDescription(admin.getFirstName() + " " + admin.getLastName() + ElementConstants.USER_ROLE_UPDATED
											+ existingUserRole.getDescription());
									log.setUserId(userId);
									log.setName(existingUserRole.getDescription());
									auditLogService.save(log);
									
									
								}
								
							} else {
								UserRoles newUserRole = new UserRoles();
								if (userRolesDto.getDescription() != null)
									newUserRole.setDescription(userRolesDto.getDescription());
								if (userRolesDto.getRoleId() != null)
									newUserRole.setRoleId(existingRole);
								if (userRolesDto.getUserId() != null)
									newUserRole.setUserId(user);
								userRolesService.saveOrUpdate(newUserRole);
								status = true;
								AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.STRING_TYPE_USER_ROLE,null, 
										ElementConstants.USER_ROLE_ADDED);
								Users admin = null;
								if(getCurrentUserId() != null){
									admin = usersService.find(getCurrentUserId());
									admin = usersService.prepareUserFromFirebase(admin);
								}
									
								log.setDescription(admin.getFirstName() + " " + admin.getLastName() + ElementConstants.USER_ROLE_ADDED
										+ newUserRole.getDescription());
								log.setUserId(userId);
								log.setName(newUserRole.getDescription());
								auditLogService.save(log);
							}
						} else {
							return new ResponseEntity<>(new ResponseMessage(ElementConstants.ROLE_NOT_FOUND),
									HttpStatus.OK);
						}
					} else {
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.ROLE_NAME_EMPTY),
								HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND), HttpStatus.OK);
				}
			}
		}
		if (status)
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.ROLE_ASSIGN_SUCCESSFUL), HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.ROLE_ASSIGN_FAIL), HttpStatus.OK);

	}

}
