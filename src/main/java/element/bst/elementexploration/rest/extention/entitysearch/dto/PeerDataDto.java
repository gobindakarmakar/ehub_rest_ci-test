package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

public class PeerDataDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> industry;
	private List<TechnologiesDto> technologies;
	private LocationDto location;
	private String name;
	private double riskScore;
	private double dr;
	private double tr;
	private double ir;
	private String dr_factors;
	private String tr_factors;
	private String ir_factors;
	private String identifier;
	private String riskScoreData;

	

	public List<String> getIndustry() {
		return industry;
	}

	public void setIndustry(List<String> industry) {
		this.industry = industry;
	}

	public List<TechnologiesDto> getTechnologies() {
		return technologies;
	}

	public void setTechnologies(List<TechnologiesDto> technologies) {
		this.technologies = technologies;
	}

	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(double riskScore) {
		this.riskScore = riskScore;
	}

	public double getDr() {
		return dr;
	}

	public void setDr(double dr) {
		this.dr = dr;
	}

	public double getTr() {
		return tr;
	}

	public void setTr(double tr) {
		this.tr = tr;
	}

	public double getIr() {
		return ir;
	}

	public void setIr(double ir) {
		this.ir = ir;
	}

	public String getDr_factors() {
		return dr_factors;
	}

	public void setDr_factors(String dr_factors) {
		this.dr_factors = dr_factors;
	}

	public String getTr_factors() {
		return tr_factors;
	}

	public void setTr_factors(String tr_factors) {
		this.tr_factors = tr_factors;
	}

	public String getIr_factors() {
		return ir_factors;
	}

	public void setIr_factors(String ir_factors) {
		this.ir_factors = ir_factors;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getRiskScoreData() {
		return riskScoreData;
	}

	public void setRiskScoreData(String riskScoreData) {
		this.riskScoreData = riskScoreData;
	}
	
}
