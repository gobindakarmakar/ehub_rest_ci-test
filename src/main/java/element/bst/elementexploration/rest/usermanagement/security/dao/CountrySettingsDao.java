package element.bst.elementexploration.rest.usermanagement.security.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;


/**
 * @author Paul Jalagari
 *
 */
public interface CountrySettingsDao extends GenericDao<CountrySetting, Long>{
	
	CountrySetting getCountrySetting(String countryIso);
}
