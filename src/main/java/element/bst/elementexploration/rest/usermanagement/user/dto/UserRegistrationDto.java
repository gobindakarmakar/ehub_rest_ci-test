package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@ApiModel("User registration")
public class UserRegistrationDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Screen name of the user", required = true)
	private String screenName;

	@ApiModelProperty(value = "First name of the user", required = true)
	private String firstName;

	@ApiModelProperty(value = "Last name of the user", required = true)
	private String lastName;

	@ApiModelProperty(value = "Middle name of the user", required = false)
	private String middleName;

	@ApiModelProperty(value = "Date of birth of the user", required = true)
	private Date dob;

	@ApiModelProperty(value = "Country of the user", required = true)
	private String country;

	@ApiModelProperty(value = "Email of the user", required = true)
	private String emailAddress;

	@ApiModelProperty(value = "password of the user", required = true)
	private String password;

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
