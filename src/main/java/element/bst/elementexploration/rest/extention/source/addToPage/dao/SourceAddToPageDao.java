package element.bst.elementexploration.rest.extention.source.addToPage.dao;
import element.bst.elementexploration.rest.extention.source.addToPage.domain.SourceAddToPage;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author hanuman
 *
 */
public interface SourceAddToPageDao extends GenericDao<SourceAddToPage, Long> {
	SourceAddToPage getSourceAddToPage(String entityId, String entitySource);
	boolean deleteSourceAddToPage(String entityId,String sourceName);

}