package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourceDomainDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;

/**
 * @author suresh
 *
 */
public class SourceDomainDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourceDomainDaoImpl sourceDomainDao;

	SourceDomain sourceDomain = null;

	@Before
	public void setupMock() {
		sourceDomain = new SourceDomain();
		sourceDomain.setDomainId(1L);
		sourceDomain.setDomainName("Education");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchDomain() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sourceDomain);
		SourceDomain sourceDomainFind = sourceDomainDao.fetchDomain("Education");
		assertEquals(sourceDomain.getDomainName(), sourceDomainFind.getDomainName());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fetchDomain_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		sourceDomainDao.fetchDomain("Education");
	}

}
