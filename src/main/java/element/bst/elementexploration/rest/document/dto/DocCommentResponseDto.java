package element.bst.elementexploration.rest.document.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Document comment response")
public class DocCommentResponseDto {
	
	@ApiModelProperty("Id of comment")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
