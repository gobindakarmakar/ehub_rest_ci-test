#FROM ubuntu:latest
#FROM python:3.7.6
#FROM alpine:3.7
FROM python:3.6.7-alpine
MAINTAINER BST
USER root

RUN echo "22-09-2019"
RUN mkdir -p /opt/bst-ehub-rest


RUN apk update \
&& apk upgrade \
&& apk add --no-cache bash \
&& apk add --no-cache --virtual=build-dependencies unzip \
&& apk add --no-cache curl \
&& apk add --no-cache openjdk8

#RUN apt-get update
#RUN apt-get install sudo -y
#RUN apt-get install software-properties-common -y
#RUN add-apt-repository ppa:openjdk-r/ppa -y 
#RUN apt-get install python-software-properties
#RUN apt-get install -y software-properties-common -y
#RUN add-apt-repository ppa:openjdk-r/ppa -y
#RUN add-apt-repository ppa:webupd8team/java -y
#RUN apt-get update
#RUN apt-get update && apt-get install -y --no-install-recommends apt-utils -y




#RUN apt-get install openjdk-8-jdk -y

#RUN sudo apt-get install ssh -y
#RUN apt-get install nano -y

#RUN apt-get install net-tools -y
#RUN apt-get update -y
#RUN apt-get upgrade -y
#RUN apt-get install build-essential -y
#RUN apt-get install zip unzip -y
RUN echo "x"
RUN apk --no-cache add sudo
RUN sudo addgroup -S tomcat
RUN sudo adduser -S tomcat -G tomcat
WORKDIR /opt
#RUN wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.4/bin/apache-tomcat-9.0.4.tar.gz
RUN wget https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.45/bin/apache-tomcat-8.5.45.tar.gz
#RUN tar xvzf apache-tomcat-9.0.4.tar.gz -C /opt
#RUN mv /opt/apache-tomcat-9.0.4 /opt/tomcat

#RUN tar xvzf apache-tomcat-8.5.45.tar.gz -C /opt
RUN  tar xvzf apache-tomcat-8.5.45.tar.gz

RUN mv /opt/apache-tomcat-8.5.45 /opt/tomcat

#RUN mv /usr/local/tomcat /opt/tomcat
RUN sudo chgrp -R tomcat /opt/tomcat
RUN sudo chmod -R g+r /opt/tomcat/conf
RUN sudo chmod g+x /opt/tomcat/conf
RUN sudo chown tomcat:tomcat -R /opt/tomcat


#TOMCAT Installation
RUN echo "#TOMCAT VARIABLES START" >> ~/.bashrc
RUN echo "export JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk" >> ~/.bashrc
RUN echo "export JRE_HOME=/usr/lib/jvm/java-1.8-openjdk/jre" >> ~/.bashrc
RUN echo "export CATALINA_PID=/opt/tomcat/temp/tomcat.pid" >> ~/.bashrc
RUN echo "export CATALINA_HOME=/opt/tomcat" >> ~/.bashrc
RUN echo "export CATALINA_BASE=/opt/tomcat" >> ~/.bashrc

RUN echo '#TOMCAT VARIABLES END' >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"
ADD configs/tomcat-users.xml /opt/tomcat/conf/
ADD configs/context.xml /opt/tomcat/webapps/manager/META-INF/
ADD configs/setenv.sh /opt/tomcat/bin/

#Maven Installation
RUN wget http://www-eu.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
RUN tar xvzf apache-maven-3.5.4-bin.tar.gz -C /opt
RUN mv /opt/apache-maven-3.5.4 /opt/maven
RUN echo "export M2_HOME=/opt/maven" >> ~/.bashrc
RUN echo "export PATH=$PATH:/opt/maven/bin" >> ~/.bashrc

RUN apk --no-cache add iputils
RUN apk add --no-cache curl
RUN echo "x"

RUN echo "Re-Deploying"
#installing the ehub core application
RUN apk add --no-cache git 
RUN apk add --no-cache vim 
RUN echo "30-09-2019"

#ADD bst-ehub-rest /opt
#RUN cd /opt/ && git clone -b release_sparta --single-branch https://gobindak2:gobindachalithawick:Chalitha1@bitbucket.org/bst_element/element-backend.git bst-ehub-rest
#RUN cd /opt/ && git clone -b release_sparta --single-branch https://chalithawick:Chalitha1@bitbucket.org/bst_element/element-backend.git bst-ehub-rest

RUN echo "app.conf.dir=/opt/bst-ehub-rest/conf" >> /opt/tomcat/conf/catalina.properties
RUN mkdir -p /opt/bst-ehub-rest/conf
ADD configs/restconf/ehub-ui.properties /opt/bst-ehub-rest/conf
ADD configs/restconf /opt/bst-ehub-rest/conf
ADD configs/restconf /opt/bst-ehub-rest/src/main/resources


ADD configs/startup.sh /root/
#ADD configs/restconf/log_configuration.properties /opt/bst-ehub-rest/conf/
#RUN mkdir /var/log/bst && chmod 777 /var/log/bst

#RUN cd /opt/bst-ehub-rest/ && /opt/maven/bin/mvn clean install -DskipTests
#RUN cd /opt/bst-ehub-rest/target && ls -la && cp ehubrest.war /opt/tomcat/webapps
Add target/ehubrest.war /opt/tomcat/webapps
RUN echo "Building BPM"

#installing the element bpm
#RUN git clone -b release_dynamic_config --single-branch https://chalithawick:Chalitha1@bitbucket.org/bst_element/bpmn-engine-activiti.git /opt/bst-bpm-front

ADD configs/bpm/app-cfg.js /opt/bst-bpm-front/activiti-app/src/main/webapp/scripts/
ADD configs/bpm/app.constant.js /opt/bst-bpm-front/activiti-app/src/main/webapp/scripts/common/constants/

# Running the builds

#RUN cd /opt/bst-bpm-front/activiti-app/ && /opt/maven/bin/mvn clean install
#RUN cd /opt/bst-bpm-front/activiti-app/target && ls -la && mv activiti-app.war elementbpm.war && cp elementbpm.war /opt/tomcat/webapps
ADD target/elementbpm.war /opt/tomcat/webapps

### script for installing tesseract
#RUN apt-get install nginx libleptonica-dev autotools-dev automake zip unzip libtool -y 
RUN apk add --no-cache nginx leptonica-dev autoconf automake zip unzip libtool

####ADDED BY VIRENDRA
RUN apk update 
RUN apk add --no-cache tesseract-ocr 
RUN apk add --no-cache ghostscript 
WORKDIR /usr/share/tesseract-ocr/
RUN chmod 777 -R /usr/share/tesseract-ocr/
RUN mkdir /opt/tesseract-ocr
RUN mkdir /opt/tesseract-ocr/tessdata
RUN chmod 777 -R /opt/tesseract-ocr/


ADD https://github.com/tesseract-ocr/tessdata/raw/master/eng.traineddata /opt/tesseract-ocr/tessdata/
RUN mkdir /opt/noisereduction
RUN chmod 777 -R /opt/noisereduction/
RUN mkdir /opt/corporatestructurepath
RUN chmod 777 -R /opt/corporatestructurepath/

RUN apk add --no-cache python
ADD configs/set_config.py /root/

ENTRYPOINT /root/startup.sh && /bin/bash

#WORKDIR /root
#CMD ["startup.sh","/bin/bash"]
