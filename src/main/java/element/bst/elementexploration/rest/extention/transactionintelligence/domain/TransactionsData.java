package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "TRANSACTIONS")
public class TransactionsData {

	private Long id;
	private String originatorAccountId;
	private String beneficiaryAccountId;
	private String transProductType;
	private String transactionChannel;
	private Double amount;
	private String currency;
	private Date businessDate;
	private String customText;
	private Long originatorCutomerId;
	private Long beneficiaryCutomerId;
	private String benficiaryBankName;
	private String beneficiaryName;	
	private String originatorName;	
	private String countryOfBeneficiary;		
	private String countryOfTransaction;
	private String atmAddress;
	private String merchant;
	private String merchantWebsite;
	
	
	

	public TransactionsData() {
	}

	public TransactionsData(Long id, String originatorAccountId, String beneficiaryAccountId, String transProductType,
			String transactionChannel, Double amount, String currency, Date businessDate, String customText,
			Long originatorCutomerId, Long beneficiaryCutomerId, String benficiaryBankName, String beneficiaryName,
			String originatorName, String countryOfBeneficiary, String countryOfTransaction, String atmAddress,
			String merchant, String merchantWebsite, String transProductTypeDetails) {
		super();
		this.id = id;
		this.originatorAccountId = originatorAccountId;
		this.beneficiaryAccountId = beneficiaryAccountId;
		this.transProductType = transProductType;
		this.transactionChannel = transactionChannel;
		this.amount = amount;
		this.currency = currency;
		this.businessDate = businessDate;
		this.customText = customText;
		this.originatorCutomerId = originatorCutomerId;
		this.beneficiaryCutomerId = beneficiaryCutomerId;
		this.benficiaryBankName = benficiaryBankName;
		this.beneficiaryName = beneficiaryName;
		this.originatorName = originatorName;
		this.countryOfBeneficiary = countryOfBeneficiary;
		this.countryOfTransaction = countryOfTransaction;
		this.atmAddress = atmAddress;
		this.merchant = merchant;
		this.merchantWebsite = merchantWebsite;
		
	}



	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "ORIGINATOR_ACCOUNT_ID")
	public String getOriginatorAccountId() {
		return originatorAccountId;
	}

	public void setOriginatorAccountId(String originatorAccountId) {
		this.originatorAccountId = originatorAccountId;
	}

	@Column(name = "BENEFICIARY_ACCOUNT_ID")
	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	@Column(name = "TRANS_PRODUCT_TYPE")
	public String getTransProductType() {
		return transProductType;
	}

	public void setTransProductType(String transProductType) {
		this.transProductType = transProductType;
	}

	@Column(name = "TRANSACTION_CHANNEL")
	public String getTransactionChannel() {
		return transactionChannel;
	}

	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

	@Column(name = "AMOUNT")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "CURRENCY")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "BUSINESS_DATE")
	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	@Column(name = "CUSTOM_TEXT")
	public String getCustomText() {
		return customText;
	}

	public void setCustomText(String customText) {
		this.customText = customText;
	}

	public Long getOriginatorCutomerId() {
		return originatorCutomerId;
	}

	public void setOriginatorCutomerId(Long originatorCutomerId) {
		this.originatorCutomerId = originatorCutomerId;
	}

	public Long getBeneficiaryCutomerId() {
		return beneficiaryCutomerId;
	}

	public void setBeneficiaryCutomerId(Long beneficiaryCutomerId) {
		this.beneficiaryCutomerId = beneficiaryCutomerId;
	}

	@Column(name = "BENEFICIARY_BANK_NAME")
	public String getBenficiaryBankName() {
		return benficiaryBankName;
	}

	public void setBenficiaryBankName(String benficiaryBankName) {
		this.benficiaryBankName = benficiaryBankName;
	}	

	
	@Column(name = "COUNTRY_OF_TRANSACTION")
	public String getCountryOfTransaction() {
		return countryOfTransaction;
	}

	public void setCountryOfTransaction(String countryOfTransaction) {
		this.countryOfTransaction = countryOfTransaction;
	}
	
	@Column(name = "ATM_ADDRESS",columnDefinition="TEXT")
	public String getAtmAddress() {
		return atmAddress;
	}

	public void setAtmAddress(String atmAddress) {
		this.atmAddress = atmAddress;
	}

	@Column(name = "BENEFICIARY_NAME")
	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	
	@Column(name = "ORIGINATOR_NAME")
	public String getOriginatorName() {
		return originatorName;
	}

	public void setOriginatorName(String originatorName) {
		this.originatorName = originatorName;
	}

	@Column(name = "COUNTRY_OF_BENEFICIARY")
	public String getCountryOfBeneficiary() {
		return countryOfBeneficiary;
	}

	public void setCountryOfBeneficiary(String countryOfBeneficiary) {
		this.countryOfBeneficiary = countryOfBeneficiary;
	}

	@Column(name = "MERCHANT",length = 500)
	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	@Column(name = "MERCHANT_WEBSITE",length = 500)
	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

	
	
	

}
