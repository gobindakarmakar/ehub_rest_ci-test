package element.bst.elementexploration.rest.util;

import java.io.DataOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

public class ServiceCallHelper {

	public static String[] postStringDataToServer(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] postStringDataToServerWithoutTimeout(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				System.out.println(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] getDataFromServer(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(30 * 1000)
				.setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] getDataFromServerWithoutTimeout(String url) {
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] postFileToServer(String url, File file) {
		String[] serverResponse = new String[2];
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create()
					.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("file", file);
			httpPost.setEntity(entityBuilder.build());
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return serverResponse;
	}

	public static byte[] downloadFileFromServer(String url) {
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from server ", e,
					ServiceCallHelper.class);
		}
		return null;
	}
	
	
	public static String[] getdownloadLinkFromS3Server(String url) {
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();
		StringBuilder responseStrBuilder = new StringBuilder();
		String[] serverResponse = new String[2];
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[0] = responseStrBuilder.toString();
			}else {
				serverResponse[0] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from s3 server ", e,
					ServiceCallHelper.class);
		}
		return serverResponse;
	}
	
	public static byte[] downloadFileFromS3Server(String url) {
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(60 * 1000)
				.setConnectTimeout(60 * 1000).setConnectionRequestTimeout(60 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from s3 server ", e,
					ServiceCallHelper.class);
		}
		return null;
	}

	public static byte[] downloadFileFromServerWithoutTimeout(String url) {
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader(HttpHeaders.USER_AGENT,
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from server ", e,
					ServiceCallHelper.class);
		}
		return null;
	}

	public static void setDownloadResponse(HttpServletResponse response, String fileName, String fileExtension,
			int size) {
		if (fileExtension == null) {
			fileExtension = "application/octet-stream";
		}
		MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		String mimeType = mimeTypesMap.getContentType(fileName);
		response.setContentType(mimeType);
		// response.setContentType(fileExtension);
		response.setContentLength(size);

		// set headers for the response
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
	}

	public static String[] deleteDataInServerWithoutTimeout(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpDelete httpDelete = new HttpDelete(url);
			response = httpClient.execute(httpDelete);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not delete data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] updateDataInServer(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not update data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] updateDataInServer(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPut.setEntity(stringEntity);
			httpPut.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not update data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] postFileArrayToServer(String url, File[] files) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("tx-file", files[0]);
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("ent-file", files[1]);
			httpPost.setEntity(entityBuilder.build());
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		} finally {
			for (File file : files) {
				if (file != null && file.exists())
					file.delete();
			}
		}
		return serverResponse;
	}

	public static String[] postStringDataToServerWithoutTimeout(String url) {

		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;

	}

	public static String[] postFileArrayToServerWithoutTImeOut(String url, File[] files) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("tx-file", files[0]);
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("ent-file", files[1]);
			httpPost.setEntity(entityBuilder.build());
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		} finally {
			for (File file : files) {
				if (file != null && file.exists())
					file.delete();
			}
		}
		return serverResponse;
	}

	public static String[] getDataFromServerWithCustomHeader(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader("Ocp-Apim-Subscription-Key", "0dc79f7ac9964d849ff70599369976c1");
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static byte[] downloadFileFromServer(String url, String jsonToBeSent) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from server ", e,
					ServiceCallHelper.class);
		}
		return null;
	}

	public static String[] putFileToServer(String url, File file) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create()
					.setMode(HttpMultipartMode.BROWSER_COMPATIBLE).addBinaryBody("template", file);
			httpPut.setEntity(entityBuilder.build());
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return serverResponse;
	}

	public static String[] deleteDataFromServerWithoutTimeout(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpDelete httpDelete = new HttpDelete(url);
			response = httpClient.execute(httpDelete);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static String[] getDataFromServerWithBasicAuth(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			// String authHeader = "Basic " + new
			// String(java.net.URLEncoder.encode("hcmVP4nFP0FOmJ7g7KFiRT9srAJ4I2IQgJE_Pvfg",
			// "UTF-8").replaceAll("\\+", "%20"));
			httpGet.setHeader(HttpHeaders.AUTHORIZATION, "hcmVP4nFP0FOmJ7g7KFiRT9srAJ4I2IQgJE_Pvfg");
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}

	public static int getStatusCodeFromServer(String url, String jsonToBeSent) {
		int status = 0;

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			status = response.getStatusLine().getStatusCode();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);

		}
		return status;
	}

	public static String[] postStringDataToServerWithoutTimeoutWithScreeningKey(String url, String jsonString) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonString,"UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			//httpPost.setHeader("x-api-key","aZUrJPkyWf9leEJDlQyzo8eOqqee3RHZ1iXxV9Jv");
			httpPost.setHeader("x-api-key","aXaRKkSeMn33dAh0MzYVE4Us3CISHeSMKK9z1l2d");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				System.out.println(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static String[] getDataFromServerWithoutTimeoutWithApiKey(String url,String apiKey) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			if(apiKey!=null)
				httpGet.setHeader("x-api-key",apiKey);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not get data from server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static String[] updateDataInServerWithApiKey(String url, String jsonToBeSent,String apiKey) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPut.setEntity(stringEntity);
			httpPut.setHeader("Content-type", "application/json");
			if(apiKey!=null)
				httpPut.setHeader("x-api-key",apiKey);
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not update data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static byte[] downloadFileFromServerWithoutTimeoutWithApiKey(String url,String apiKey) {
		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader(HttpHeaders.USER_AGENT,
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			if(apiKey!=null)
				httpGet.setHeader("x-api-key",apiKey);
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				return EntityUtils.toByteArray(entity);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not download file from server ", e,
					ServiceCallHelper.class);
		}
		return null;
	}
	
	public static String[] putStringDataToServerWithoutTimeout(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent,"UTF-8");
			httpPut.setEntity(stringEntity);
			httpPut.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				System.out.println(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post data to server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	public static String[] getS3LinkForUploadDoc(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPut httpPut = new HttpPut(url);
			response = httpClient.execute(httpPut);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not update data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	
	
	public static String[] getCaseManagementS3LinkForUploadDoc(String url) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();

		try (CloseableHttpClient httpClient = HttpClients.createDefault();) {
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not update data on server ", e, ServiceCallHelper.class);
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	public static int putFileToS3Server(String url, File file) {
		int status =0;
		try {
			URL address = new URL(url);
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpURLConnection httpCon = (HttpURLConnection) address.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestProperty("Content-Type", "binary/octet-stream" );
			httpCon.setRequestMethod("PUT");
			//httpCon.setRequestMethod("HEAD");

			byte[] bytes = Files.readAllBytes(file.toPath());
			DataOutputStream out = new DataOutputStream(httpCon.getOutputStream());
			out.write(bytes);
			out.flush();
			out.close();

			status = httpCon.getResponseCode();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return status;
	}
	
	public static int postFileToS3Server(String url, File file) {
		int status =0;
		try {
			URL address = new URL(url);
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpURLConnection httpCon = (HttpURLConnection) address.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestProperty("Content-Type", "binary/octet-stream" );
			httpCon.setRequestMethod("POST");
			//httpCon.setRequestMethod("HEAD");

			byte[] bytes = Files.readAllBytes(file.toPath());
			DataOutputStream out = new DataOutputStream(httpCon.getOutputStream());
			out.write(bytes);
			out.flush();
			out.close();

			status = httpCon.getResponseCode();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return status;
	}
	
	public static int putCaseManagmentFileToS3Server(String url, File file) {
		int status =0;
		try {
			URL address = new URL(url);
			ElementLogger.log(ElementLoggerLevel.INFO," url: "+url, ServiceCallHelper.class);
			HttpURLConnection httpCon = (HttpURLConnection) address.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestProperty("Content-Type", "binary/octet-stream" );
			httpCon.setRequestMethod("PUT");
			//httpCon.setRequestMethod("HEAD");

			byte[] bytes = Files.readAllBytes(file.toPath());
			DataOutputStream out = new DataOutputStream(httpCon.getOutputStream());
			out.write(bytes);
			out.flush();
			out.close();

			status = httpCon.getResponseCode();

		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Could not post file to server ", e, ServiceCallHelper.class);
		} finally {
			if (file != null && file.exists())
				file.delete();
		}
		return status;
	}
	
}
