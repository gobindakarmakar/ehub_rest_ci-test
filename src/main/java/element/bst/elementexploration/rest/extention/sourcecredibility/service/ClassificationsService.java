package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface ClassificationsService extends GenericService<Classifications, Long> {

	List<ClassificationsDto> getClassifications();

	Classifications updateSubClassifcationMedia(ClassificationsDto classifcationsDto);

	Classifications fetchClassification(String name);

	List<Classifications> findAllWithSubclassifications();

}
