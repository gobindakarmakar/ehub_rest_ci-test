package element.bst.elementexploration.rest.document.dao;

import java.util.List;

import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.domain.DocumentsVault;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * 
 * @author Viswanath
 *
 */
public interface DocumentDao extends GenericDao<DocumentVault, Long>{

	boolean isDocShareable(long userId, long docId);

	List<DocumentVaultDTO> myDocuments(Long userId, Long caseseedId, Integer docFlag, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn,String entityId,String docSection,Boolean isAllRequired);

	long countMyDocuments(Long userId, Long caseseedId, Integer docFlag,String entityId,String docSection);

	List<DocumentVaultDTO> fullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,
			Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn,String entityId,String docSection);

	long countFullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,String entityId,String docSection);

	List<DocumentVaultDTO> getAllMySharedDocument(Long userId, Long caseseedId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn, Integer flagValue);

	Long countGetAllMySharedDocument(Long userId, Long caseseedId, Integer flagValue);

	List<DocumentVaultDTO> fullTextSearchDocument(Long userId, String searchKeyWord, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Long caseId, Integer flagValue);

	Long countFullTextSearchDocument(Long userId, String searchKeyWord, Long caseId, Integer flagValue);

	boolean isDocCommentable(long userId, long docId);
	List<Long> searchUnderwritingDocumentsIdsByName(String keyword);

	List<DocumentVault> getDocumentVaultByEntityIdAndDocflag(int docFlag, Long entityId);

}
