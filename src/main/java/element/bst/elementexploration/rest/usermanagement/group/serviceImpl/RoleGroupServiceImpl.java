package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.usermanagement.group.dao.RolesGroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.RoleGroupDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.RoleGroupService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author Paul Jalagari
 *
 */
@Service("roleGroupService")
@Transactional("transactionManager")
public class RoleGroupServiceImpl extends GenericServiceImpl<RoleGroup, Long> implements RoleGroupService {
	
	@Autowired
	RolesGroupDao rolesGroupDao;
	
	@Autowired
	UsersService usersService;
	
	@Autowired
	AuditLogService auditLogService;
	
	@Autowired
	GroupsService groupsService;
	
	@Autowired
	UserRolesService userRolesService;

	public RoleGroupServiceImpl() {
	}

	@Autowired
	public RoleGroupServiceImpl(GenericDao<RoleGroup, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public RoleGroup saveOrUpdateRoleGroup(RoleGroupDto groupPermissiondto, Long currentUserId) {
		return null;
	}

	@SuppressWarnings("unused")
	@Override
	public RoleGroup saveOrUpdateRoleGroup(Roles role, Groups group, Long roleGroupId, Long currentUserId) throws Exception {
		Users currentUser = usersService.find(currentUserId);
		Long typeId = null;
		boolean isAnyThingChanged = false;
		boolean groupUpdated = false;
		boolean roleUpdated = false;
		String groupName = null;
		String roleName = null;
		StringBuilder logDescription = new StringBuilder();
		RoleGroup roleGroupToReturn = null;
		if (roleGroupId != null) {
			/*RoleGroup existingRoleGroup = find(roleGroupId);
			if (existingRoleGroup != null) {
				groupName = existingRoleGroup.getGroupId().getName();
				roleName = existingRoleGroup.getRoleId().getRoleName();
				if (!existingRoleGroup.getGroupId().getId().equals(group.getId())) {
					existingRoleGroup.setGroupId(group);
					groupUpdated = true;
					isAnyThingChanged = true;
				}
				if (!existingRoleGroup.getRoleId().getRoleId().equals(role.getRoleId())) {
					existingRoleGroup.setRoleId(role);
					roleUpdated = true;
					isAnyThingChanged = true;
				}
				existingRoleGroup.setModifiedOn(new Date());
				;
				saveOrUpdate(existingRoleGroup);
				typeId = roleGroupId;
				if (isAnyThingChanged) {
					logDescription.append(
							currentUser.getFirstName() + " " + currentUser.getLastName() + " modified RoleGroup's");
					if (groupUpdated)
						logDescription.append(" group from " + groupName + " to " + group.getName() + " ");
					if (roleUpdated)
						logDescription.append(" role from " + roleName + " to " + role.getRoleName() + " ");
				}
				roleGroupToReturn = existingRoleGroup;
			}*/
		} else {
			
			RoleGroup newRoleGroup = new RoleGroup();
			newRoleGroup.setCreatedOn(new Date());
			newRoleGroup.setModifiedOn(new Date());
			newRoleGroup.setGroupId(group);
			newRoleGroup.setRoleId(role);
			RoleGroup savedRoleGroup = save(newRoleGroup);
			/*List<Users> usersList = usersService.findAll();
			if (usersList != null && usersList.size() > 0) {
				for (Users user : usersList) {
					List<Groups> userGroupsList = groupsService.getGroupsOfUser(user.getUserId());
					if (userGroupsList != null && userGroupsList.size() > 0) {
						for (Groups eachGroup : userGroupsList) {
							if (group.getId().equals(eachGroup.getId())) {
								List<Long> roleIds= new ArrayList<Long>();
								roleIds.add(role.getRoleId());
								userRolesService.saveUserRoles(user, roleIds, currentUserId, ElementConstants.ROLE_ASSIGN_ACTION);
							}
						}
					}
				}
			}*/
			isAnyThingChanged = true;
			typeId = savedRoleGroup.getRoleGroupId();
			logDescription.append(currentUser.getFirstName() + " " + currentUser.getLastName() + " added "
					+ role.getRoleName() + " role to group " + group.getName() + " ");
			roleGroupToReturn = savedRoleGroup;
		}
		if (isAnyThingChanged && logDescription != null) {
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ROLES_GROUP_ADD_ACTION, null,
					ElementConstants.ROLES_GROUP_ADDED);
			log.setDescription(logDescription.toString());
			log.setUserId(currentUser.getUserId());
			if (typeId != null)
				log.setTypeId(typeId);
			log.setName(group.getName());
			auditLogService.save(log);
		}
		return roleGroupToReturn;
	}

	@Override
	public List<RoleGroup> getRolesByGroup(List<Long> groupIds) {
		return rolesGroupDao.getRolesByGroup(groupIds);
	}

	@Override
	public void initializeGroups(List<RoleGroup> filteredList) {
		filteredList.stream().forEach(o -> Hibernate.initialize(o.getGroupId()));
	}

	@Override
	public boolean checkRoleExistsForGroup(Long groupId, Long roleId) {
		List<RoleGroup> existingRoleGroup = findAll().stream().filter(o -> o.getGroupId().getId().equals(groupId))
				.filter(b -> b.getRoleId().getRoleId().equals(roleId)).collect(Collectors.toList());
		if (existingRoleGroup != null && existingRoleGroup.size() > 0)
			return true;
		return false;
	}

	@Override
	public boolean deleteExistingRolesForGroup(Long groupId,Long currentUserId) throws Exception {
		
		/*List<Long> groupIds= new ArrayList<>();
		groupIds.add(groupId);
		List<RoleGroup> roleGroupsList=getRolesByGroup(groupIds);
		
		List<Users> usersList=usersService.findAll();
		if (roleGroupsList != null && roleGroupsList.size() > 0) {
			for (RoleGroup eachRoleGroup : roleGroupsList) {
				if (usersList != null && usersList.size() > 0) {
					for (Users user : usersList) {
						List<Groups> userGroupsList = groupsService.getGroupsOfUser(user.getUserId());
						if (userGroupsList != null && userGroupsList.size() > 0) {
							for (Groups eachGroup : userGroupsList) {
								if (groupId.equals(eachGroup.getId())) {
									List<Long> roleIds = new ArrayList<Long>();
									roleIds.add(eachRoleGroup.getRoleId().getRoleId());
									userRolesService.removeUserRoles(user, roleIds, currentUserId,
											ElementConstants.ROLE_UNASSIGN_ACTION);
								}
							}
						}
					}
				}
			}
		}*/
		rolesGroupDao.deleteExistingRolesForGroup(groupId);
		return true;
	}
}
