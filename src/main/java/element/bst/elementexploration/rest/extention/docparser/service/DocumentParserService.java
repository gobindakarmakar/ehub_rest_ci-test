package element.bst.elementexploration.rest.extention.docparser.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.simple.parser.ParseException;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.extention.docparser.dto.CheckPdfTypeDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DOCResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentContentDto;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentResponseDto;
import element.bst.elementexploration.rest.extention.docparser.dto.OverAllResponseDto;
import net.sourceforge.tess4j.TesseractException;

/**
 * @author suresh
 *
 */
public interface DocumentParserService 
{
	
	public List<DOCResponseDto>  exploreOptions(byte[] streamFile, Long docId) throws IllegalStateException, IOException, InvalidFormatException;
	public boolean saveIntoDatabase(byte[] streamFile, Long docId) throws IllegalStateException, IOException, InvalidFormatException;
	public List<DocumentResponseDto> getDocumentContentById(Long docId);
	public Boolean auditDatabase(byte[] streamFile, Long docId) throws IllegalStateException, IOException, InvalidFormatException;
	public boolean auditGroups(byte[] streamFile, Long docId) throws IllegalStateException, IOException, InvalidFormatException;
	public List<DocumentContentDto> getDocumentParseDataById(Long docId);

	public List<OverAllResponseDto> getDocumentParseData(Long docId);
	
	public boolean setRiskScoreToQuestions(Long docId);
	
	public boolean compareFileWithJsonTemplate(MultipartFile uploadedFile,String templateName) throws IllegalStateException, IOException, InvalidFormatException, URISyntaxException, ParseException;
	
	public DocumentTemplates saveDocumentTemplates(MultipartFile uploadedFile,String templateName) throws IllegalStateException, IOException, InvalidFormatException;

	//public void saveDocumetnAnswers(byte[] streamFile, Long docId,Long templateId) throws IllegalStateException, IOException, InvalidFormatException;
	
	public CaseMapDocIdDto saveDocumetnAnswers(MultipartFile uploadedFile,Long userId,Long docId,Long templateId) throws IllegalStateException, IOException, InvalidFormatException,InsufficientDataException;
	
	public CaseMapDocIdDto saveDocumetnAnswersPdf(MultipartFile uploadFile, Long userId, long docId1,DocumentTemplates documentTemplates, byte[] fileData) throws IOException, TesseractException;
	public DocumentTemplates compareFileWithDocumentTemplate(MultipartFile uploadedFile, Long docId, Long userId, byte[] fileData) throws IllegalStateException, IOException, InvalidFormatException,TesseractException;
	public CaseMapDocIdDto saveDocumetnAnswersCsv(MultipartFile uploadFile, Long userId, long docId, long id) throws IOException, EncryptedDocumentException, InvalidFormatException;
	public void setEntityToAnswers(List<DocumentAnswers> answersList,String entityId) throws IOException, Exception;	
	public boolean getUpdatedISODocumentParseData(Long docId);
	public List<OverAllResponseDto> getTemplateQandA(Long tempId);
	public CheckPdfTypeDto checkPdfType(byte[] byteArray) throws IllegalStateException, IOException, TesseractException;
	byte[] downloadDocumentDocParser(Long docId, Long userId, HttpServletResponse response) throws DocNotFoundException, Exception;
	DocumentVaultDTO getDocumentDetailsDocParser(Long docId, Long userId) throws IllegalAccessException, InvocationTargetException;
	
}
