package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCategory;
import element.bst.elementexploration.rest.extention.docparser.dto.Level1CodeDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface IsoCategoryService extends GenericService<IsoCategory, Long> {

	public int updateIsoCategoryEvaluation(IsoCategory isoCategory, String evaluation, long userIdTemp);
	
	public void storeIsoCategoryEvaluation(Long docId);

	public Level1CodeDto updateIsoCategoryEvaluationByAnswer(DocumentAnswers documentAnswer,long userIdTemp);

}
