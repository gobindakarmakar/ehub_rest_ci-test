package element.bst.elementexploration.activiti.app.conf;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.databind.ObjectMapper;


public class TestClass {

	public static void main(String[] args) {
		try {
			List<DataObj> objects= new ArrayList<DataObj>();
			ObjectMapper mapper = new ObjectMapper();
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("ingSources.json");
			File file = new File(resource.toURI());
			JSONParser parser = new JSONParser();
			Object object = parser.parse(new FileReader(file));
			JSONObject jsonObject = new JSONObject(object.toString());
			if(jsonObject.has("result")) {
				JSONArray resultArray= jsonObject.getJSONArray("result");
				for(int i=0;i<resultArray.length();i++) {
					DataObj obj= mapper.readValue(resultArray.get(i).toString(), DataObj.class);
					objects.add(obj);
					//System.out.println(obj.getSourceId()+" - "+obj.getSourceName()+" - "+obj.getSourceUrl());
					//System.out.println("\n");
				}
			}
			Collections.sort(objects);
			for(DataObj obj:objects) {
				System.out.println(obj.getSourceId()+" - "+obj.getSourceName()+" - "+obj.getSourceUrl());
				System.out.println("\n");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

