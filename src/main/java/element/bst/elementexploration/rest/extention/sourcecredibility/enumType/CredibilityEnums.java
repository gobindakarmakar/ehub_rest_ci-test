package element.bst.elementexploration.rest.extention.sourcecredibility.enumType;

/**
 * 
 * @author Suresh
 *
 */
public enum CredibilityEnums {

	NONE, LOW, MEDIUM, HIGH

}
