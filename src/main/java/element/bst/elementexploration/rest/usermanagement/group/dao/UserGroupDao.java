package element.bst.elementexploration.rest.usermanagement.group.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;

public interface UserGroupDao extends GenericDao<UserGroup, Long> {

	UserGroup getUserGroup(Long userId, Long groupId);

}
