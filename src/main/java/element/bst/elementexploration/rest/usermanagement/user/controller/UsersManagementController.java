package element.bst.elementexploration.rest.usermanagement.user.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersManagementService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Prateek Maurya
 *
 */

@Api(tags = { "Users Management API" }, description = "Manages all the user management APIs")
@RestController
@RequestMapping("/api/usersManagement")
public class UsersManagementController {

	@Autowired
	UsersManagementService usersManagementService;
	
	@ApiOperation("Gets main tiles data")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/mainTilesData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> mainTilesData(@RequestParam("token") String token, HttpServletRequest request) {

		return new ResponseEntity<>(usersManagementService.mainTilesData(), HttpStatus.OK);
	}
	
	@ApiOperation("Gets groups with the user count")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/groupsWithUserCount", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> groupsWithUserCount(@RequestParam("token") String token, HttpServletRequest request) {

		return new ResponseEntity<>(usersManagementService.groupsWithUserCount(), HttpStatus.OK);
	}
		@ApiOperation("Gets roles with the user count")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/rolesWithUserCount", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> rolesWithUserCount(@RequestParam("token") String token, HttpServletRequest request) {

		return new ResponseEntity<>(usersManagementService.rolesWithUserCount(), HttpStatus.OK);
	}
}
