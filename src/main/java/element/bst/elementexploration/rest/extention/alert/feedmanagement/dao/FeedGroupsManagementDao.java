package element.bst.elementexploration.rest.extention.alert.feedmanagement.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface FeedGroupsManagementDao extends GenericDao<FeedGroups, Long>{
	
	List<FeedGroups> getFeedGroupsByFeedId(Long feedId);

}
