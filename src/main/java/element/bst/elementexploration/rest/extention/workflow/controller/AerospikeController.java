package element.bst.elementexploration.rest.extention.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.workflow.service.AerospikeService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "Aerospike API" },description="Manages aerospike data")
@RestController
@RequestMapping("/api/workflow/aero")
public class AerospikeController extends BaseController{

	@Autowired
	private AerospikeService aerospikeService;

	@ApiOperation("Gets list of aerospike databases by profile ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/namespaces/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfAerospikeDatabases(@RequestParam String token, @PathVariable String profileId)
			throws Exception {
		return new ResponseEntity<>(aerospikeService.listOfAerospikeDatabases(profileId), HttpStatus.OK);

	}

	@ApiOperation("Gets list of aerospike tables by profile ID and namespaces")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/namespaces/{profileId}/{namespace}/sets", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfAerospikeTables(@RequestParam String token, @PathVariable String profileId,
			@PathVariable String namespace) throws Exception {
		return new ResponseEntity<>(aerospikeService.listOfAerospikeTables(profileId, namespace), HttpStatus.OK);

	}

	@ApiOperation("Gets sample data by workflow ID and stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/sampledata/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> sampledata(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(aerospikeService.sampledata(workflowId, stageId), HttpStatus.OK);

	}

	@ApiOperation("Gets schema by workflow ID and stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/schema/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> schema(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(aerospikeService.schema(workflowId, stageId), HttpStatus.OK);

	}

}
