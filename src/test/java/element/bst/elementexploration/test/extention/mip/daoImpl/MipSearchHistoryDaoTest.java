package element.bst.elementexploration.test.extention.mip.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.mip.daoImpl.MipSearchHistoryDaoImpl;
import element.bst.elementexploration.rest.extention.mip.domain.MipSearchHistory;
import element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO;

@RunWith(MockitoJUnitRunner.class)
public class MipSearchHistoryDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;
	
	@Mock
	CriteriaBuilder builder;
	
	@Mock
	CriteriaQuery<Object> criteriaQuery;
	
	@Mock
	Root<MipSearchHistory> root;
	
	@InjectMocks
	MipSearchHistoryDaoImpl MipSearchHistoryDaoImpl;
	
	public MipSearchHistoryDTO mipSearchHistoryDTO;
	
	List<MipSearchHistoryDTO> list = new ArrayList<MipSearchHistoryDTO>();
	
	@Before
	public void setupMock() {
		mipSearchHistoryDTO = new MipSearchHistoryDTO(2L, "name", "data", "sourcePage", new Date(), new Date());
		MipSearchHistoryDTO mipSearchHistoryDTO1 = new MipSearchHistoryDTO(1L, "analyst", "newData", "sourcePage", new Date(), new Date());
		list.add(mipSearchHistoryDTO);
		list.add(mipSearchHistoryDTO1);
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getSearchHistoryListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		List<MipSearchHistoryDTO> newList = MipSearchHistoryDaoImpl.getSearchHistoryList(1L, 1, 15, "name", "desc");
		assertEquals(2, newList.size());
	}
	
	@Test
	public void getSearchHistoryList_whenOrderByAndOredrInNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		List<MipSearchHistoryDTO> newList = MipSearchHistoryDaoImpl.getSearchHistoryList(1L, 1, 15, null, null);
		assertEquals(2, newList.size());
	}
	
	@Test
	public void getSearchHistoryList_whenOredrInIsNotAscOrDescTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		List<MipSearchHistoryDTO> newList = MipSearchHistoryDaoImpl.getSearchHistoryList(1L, 1, 15, "name", " ");
		assertEquals(2, newList.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getSearchHistoryList_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		MipSearchHistoryDaoImpl.getSearchHistoryList(1L, 1, 15, null, null);
	}
	
	@Test
	public void isSearchHistoryOwnerTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(MipSearchHistory.class)).thenReturn(root);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(false);
		boolean result = MipSearchHistoryDaoImpl.isSearchHistoryOwner(1L, 2L);
		assertEquals(false, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void isSearchHistoryOwner_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(MipSearchHistory.class)).thenReturn(root);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		MipSearchHistoryDaoImpl.isSearchHistoryOwner(1L, 2L);
	}
	
	@Test
	public void countSearchHistoryList() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		long result = MipSearchHistoryDaoImpl.countSearchHistoryList(1L);
		assertEquals(2L, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void countSearchHistory_whenExceptionThrowList() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		MipSearchHistoryDaoImpl.countSearchHistoryList(1L);
	}
	
}
