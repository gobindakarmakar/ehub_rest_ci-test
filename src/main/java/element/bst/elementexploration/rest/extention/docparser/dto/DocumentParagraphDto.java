package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

/**
 * @author rambabu
 *
 */
public class DocumentParagraphDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String contentType;
	private String question;
	private String answer;
	private String paragraphTitle;
	private boolean isQuestion;
	private String answerType;
	private String expectedAnswer;
	private String isoCode;
	//private String possibleAnswer;
	private String riskScore;
	private String statndardForm;
	
	

	public DocumentParagraphDto() {
		super();

	}

	

	public DocumentParagraphDto(String contentType, String question, String answer, String paragraphTitle,
			boolean isQuestion, String answerType, String expectedAnswer, String isoCode, String riskScore,
			String statndardForm) {
		super();
		this.contentType = contentType;
		this.question = question;
		this.answer = answer;
		this.paragraphTitle = paragraphTitle;
		this.isQuestion = isQuestion;
		this.answerType = answerType;
		this.expectedAnswer = expectedAnswer;
		this.isoCode = isoCode;
		this.riskScore = riskScore;
		this.statndardForm = statndardForm;
	}



	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getParagraphTitle() {
		return paragraphTitle;
	}

	public void setParagraphTitle(String paragraphTitle) {
		this.paragraphTitle = paragraphTitle;
	}

	public boolean isQuestion() {
		return isQuestion;
	}

	public void setQuestion(boolean isQuestion) {
		this.isQuestion = isQuestion;
	}

	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}

	public String getExpectedAnswer() {
		return expectedAnswer;
	}

	public void setExpectedAnswer(String expectedAnswer) {
		this.expectedAnswer = expectedAnswer;
	}



	public String getIsoCode() {
		return isoCode;
	}



	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}



	public String getRiskScore() {
		return riskScore;
	}



	public void setRiskScore(String riskScore) {
		this.riskScore = riskScore;
	}



	public String getStatndardForm() {
		return statndardForm;
	}



	public void setStatndardForm(String statndardForm) {
		this.statndardForm = statndardForm;
	}

	
}
