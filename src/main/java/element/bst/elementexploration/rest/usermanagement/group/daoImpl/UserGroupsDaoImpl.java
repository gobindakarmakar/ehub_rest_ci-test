package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserGroupsDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("userGroupsDao")
public class UserGroupsDaoImpl extends GenericDaoImpl<UserGroups, Long> implements UserGroupsDao {

	public UserGroupsDaoImpl() {
		super(UserGroups.class);
	}

	/**
	 * Get UserGroup
	 * 
	 * @param userId
	 * @param groupId
	 */
	@Override
	public UserGroups getUserGroup(Long userId, Long groupId) {
		UserGroups userGroup = null;

		try {
			String hql = "FROM UserGroups as ug WHERE ug.userId.userId = :userId and ug.groupId.id = :groupId";
			Query<?> query = this.getCurrentSession().createQuery(hql);
			query.setParameter("userId", userId);
			query.setParameter("groupId", groupId);
			userGroup = (UserGroups) query.getSingleResult();
			return userGroup;
		} catch (NoResultException e) {
			return userGroup;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Get UserGroups", e, this.getClass());
		}
		return userGroup;
	}

	@Override
	public Map<Long, Long> groupsWithUserCount() {
		List<Object[]> queryResult = new ArrayList<Object[]>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = (CriteriaQuery<Object[]>) builder.createQuery(Object[].class);
		Root<UserGroups> result = (Root<UserGroups>) criteriaQuery.from(UserGroups.class);
		criteriaQuery.multiselect(result.get("groupId"), builder.count(result));
		criteriaQuery.groupBy(result.get("groupId"));
		Query<Object[]> query = this.getCurrentSession().createQuery(criteriaQuery);
		
		queryResult = query.getResultList();
		Map<Long, Long> resultMap = new HashMap<Long, Long>();
		for(Object[] groupCount : queryResult){
			if(groupCount[0] != null &&  groupCount[1] != null){
				Groups group = (Groups) groupCount[0];
				resultMap.put(group.getId(), Long.valueOf(groupCount[1].toString()));
			}
		}
		return resultMap;
	}

}
