package element.bst.elementexploration.test.extention.advancesearch.daoimpl;

import static org.junit.Assert.assertEquals;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.advancesearch.daoimpl.AdvanceNewsFavouriteDaoImpl;
import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;

/**
 * @author suresh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AdvanceNewsFavouriteDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	AdvanceNewsFavourite advanceNewsFavourite;

	@InjectMocks
	AdvanceNewsFavouriteDaoImpl advanceNewsFavoutiteDao;

	@Before
	public void setUp() {
		advanceNewsFavourite = new AdvanceNewsFavourite(1L, "abdwdw6dwdwdwdwh", "oracle", 2L);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchadvanceNewsFavourite() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(advanceNewsFavourite);
		AdvanceNewsFavourite advanceNewsFavouriteFetch = advanceNewsFavoutiteDao.fetchadvanceNewsFavourite("oracle",
				"jwddwidwh643qbhqd5s", 1L);
		assertEquals(advanceNewsFavourite.getEntityName(), advanceNewsFavouriteFetch.getEntityName());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchadvanceNewsFavouriteExceptionThrown() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		AdvanceNewsFavourite advanceNewsFavouriteFetch = advanceNewsFavoutiteDao.fetchadvanceNewsFavourite("oracle",
				"jwddwidwh643qbhqd5s", 1L);
		assertEquals(null, advanceNewsFavouriteFetch);
	}

	@Test
	public void deleteAdvanceNewsFavourite() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenReturn(1);
		int value = advanceNewsFavoutiteDao.deleteAdvanceNewsFavourite("oracle");
		assertEquals(1, value);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void deleteAdvanceNewsFavouriteExceptionThrown() {

		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.executeUpdate()).thenThrow(Exception.class);
		int value = advanceNewsFavoutiteDao.deleteAdvanceNewsFavourite("oracle");
		assertEquals(0, value);
	}

}
