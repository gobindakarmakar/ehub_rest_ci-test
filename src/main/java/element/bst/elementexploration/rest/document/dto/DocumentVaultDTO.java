package element.bst.elementexploration.rest.document.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Document")
public class DocumentVaultDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("Document id")
	private Long docId;

	@ApiModelProperty("Document title")
	private String title;

	@ApiModelProperty("Document remark")
	private String remark;

	@ApiModelProperty("Uploaded document size")
	private Long size;

	@ApiModelProperty("Document type")
	private String type;

	@ApiModelProperty("Document uploaded date")
	private Date uploadedOn;

	@ApiModelProperty("Document uploaded user id")
	private Long uploadedBy;

	@ApiModelProperty("Document modified user id")
	private Long modifiedBy;

	@ApiModelProperty("Document modified date")
	private Date modifiedOn;

	@ApiModelProperty("Document name")
	private String docName;

	@ApiModelProperty("Document uploaded user name")
	private String uploadedUserName;

	@ApiModelProperty("Number of questions in uploaded document")
	private int noOfQuestions;

	@ApiModelProperty("Document parsing status")
	private String documentParsingStatus;

	private boolean hasConflicts;

	@ApiModelProperty("Document modified user name")
	private String modifiedByName;
	
	@ApiModelProperty("Entity Id")
	private String entityId;
	
	@ApiModelProperty("Entity Source")
	private String entitySource;
	
	@ApiModelProperty("Document Section")
	private String docSection;
	
	@ApiModelProperty("Document Flag")
	private Integer docFlag;

	public DocumentVaultDTO() {
	}

	public DocumentVaultDTO(Long docId, String title, String remark, Long size, String type, Date uploadedOn,
			Long uploadedBy, Long modifiedBy, Date modifiedOn, String docName, String uploadedUserName) {
		super();
		this.docId = docId;
		this.title = title;
		this.remark = remark;
		this.size = size;
		this.type = type;
		this.uploadedOn = uploadedOn;
		this.uploadedBy = uploadedBy;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.docName = docName;
		this.uploadedUserName = uploadedUserName;
	}

	
	public DocumentVaultDTO(Long docId, String title, String remark, Long size, String type, Date uploadedOn,
			Long uploadedBy, Long modifiedBy, Date modifiedOn, String docName, String uploadedUserName, String documentParsingStatus) {
		super();
		this.docId = docId;
		this.title = title;
		this.remark = remark;
		this.size = size;
		this.type = type;
		this.uploadedOn = uploadedOn;
		this.uploadedBy = uploadedBy;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.docName = docName;
		this.uploadedUserName = uploadedUserName;
		this.documentParsingStatus = documentParsingStatus;
	}
	public DocumentVaultDTO(Long docId, String title, String remark, Long size, String type, Date uploadedOn,
			Long uploadedBy, Long modifiedBy, Date modifiedOn, String docName, String uploadedUserName, String documentParsingStatus,String docSection,String entityId,Integer docFlag,String entitySource) {
		super();
		this.docId = docId;
		this.title = title;
		this.remark = remark;
		this.size = size;
		this.type = type;
		this.uploadedOn = uploadedOn;
		this.uploadedBy = uploadedBy;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.docName = docName;
		this.uploadedUserName = uploadedUserName;
		this.documentParsingStatus = documentParsingStatus;
		this.docSection=docSection;
		this.entityId=entityId;
		this.docFlag=docFlag;
		this.entitySource=entitySource;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public Long getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(Long uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getUploadedUserName() {
		return uploadedUserName;
	}

	public void setUploadedUserName(String uploadedUserName) {
		this.uploadedUserName = uploadedUserName;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public int getNoOfQuestions() {
		return noOfQuestions;
	}

	public void setNoOfQuestions(int noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public boolean isHasConflicts() {
		return hasConflicts;
	}

	public void setHasConflicts(boolean hasConflicts) {
		this.hasConflicts = hasConflicts;
	}

	public String getModifiedByName() {
		return modifiedByName;
	}

	public void setModifiedByName(String modifiedByName) {
		this.modifiedByName = modifiedByName;
	}

	public String getDocumentParsingStatus() {
		return documentParsingStatus;
	}

	public void setDocumentParsingStatus(String documentParsingStatus) {
		this.documentParsingStatus = documentParsingStatus;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getDocSection() {
		return docSection;
	}

	public void setDocSection(String docSection) {
		this.docSection = docSection;
	}

	public Integer getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(Integer docFlag) {
		this.docFlag = docFlag;
	}

	public String getEntitySource() {
		return entitySource;
	}

	public void setEntitySource(String entitySource) {
		this.entitySource = entitySource;
	}

}
