package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SubClassificationsDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("subClassificationsDao")
public class SubClassificationsDaoImpl extends GenericDaoImpl<SubClassifications, Long>
		implements SubClassificationsDao {

	public SubClassificationsDaoImpl() {
		super(SubClassifications.class);
	}

	@Override
	public SubClassifications fetchSubClassification(String subClasssifcationName) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from SubClassifications s where s.subClassifcationName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", subClasssifcationName);
			SubClassifications subClassifications = (SubClassifications) query.getSingleResult();
			if (subClassifications != null)
			{
				subClassifications.getDataAttributes().get(0);
								return subClassifications;
			}
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, SourceIndustryDaoImpl.class);
			e.printStackTrace();
			throw new FailedToExecuteQueryException();
		}
		return null;
	}

}
