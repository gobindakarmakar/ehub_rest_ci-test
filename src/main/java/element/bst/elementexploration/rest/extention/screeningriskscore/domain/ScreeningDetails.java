package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;
import java.util.List;

public class ScreeningDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String type;
	
	private List<ScreenigHit> hits;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ScreenigHit> getHits() {
		return hits;
	}

	public void setHits(List<ScreenigHit> hits) {
		this.hits = hits;
	}

}