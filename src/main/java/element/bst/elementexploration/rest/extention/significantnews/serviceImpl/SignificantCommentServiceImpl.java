package element.bst.elementexploration.rest.extention.significantnews.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantCommentDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantCommentDto;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantCommentService;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;

@Service("significantCommentService")
public class SignificantCommentServiceImpl extends GenericServiceImpl<SignificantComment, Long>
		implements SignificantCommentService {

	@Autowired
	private UserDao userDao;

	@Autowired
	SignificantCommentDao significantCommentDao;

	@Override
	@Transactional("transactionManager")
	public List<SignificantCommentDto> getSignificantComments(String classification, Long significantId) {
		List<SignificantCommentDto> significantCommentDtoList = new ArrayList<SignificantCommentDto>();
		List<SignificantComment> significantCommentList = significantCommentDao.getSignificantComments(classification,
				significantId);
		for (SignificantComment significantComment : significantCommentList) {
			SignificantCommentDto significantCommentDto = new SignificantCommentDto();
			BeanUtils.copyProperties(significantComment, significantCommentDto);
			significantCommentDto.setIsSignificant(significantComment.getIsSignificant());
			User user = userDao.find(significantComment.getUserId());
			if (user != null)
				significantCommentDto.setUserName(user.getFirstName() + " " + user.getLastName());
			significantCommentDtoList.add(significantCommentDto);
		}
		return significantCommentDtoList;
	}

}
