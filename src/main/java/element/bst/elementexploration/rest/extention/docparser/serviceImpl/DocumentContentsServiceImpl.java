package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentContents;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentContentsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("documentContentsService")
public class DocumentContentsServiceImpl extends GenericServiceImpl<DocumentContents, Long> implements DocumentContentsService {

	@Autowired
	public DocumentContentsServiceImpl(GenericDao<DocumentContents, Long> genericDao) {
		super(genericDao);
	}

}
