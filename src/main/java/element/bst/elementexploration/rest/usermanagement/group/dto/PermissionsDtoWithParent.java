package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.extention.menuitem.domain.Domains;
import element.bst.elementexploration.rest.extention.menuitem.domain.Modules;
import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "PermissionsDtoWithParent")
public class PermissionsDtoWithParent implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Privilege ID")

	private Long permissionId;

	@ApiModelProperty(value = "Privilege Name")
	private String itemName;

	@ApiModelProperty(value = "Status of permission")
	private Boolean active;

	@ApiModelProperty(value = "Parent Permission ID")

	private Long parentPermissionId;

	@ApiModelProperty(value = "Created On")
	private Date createdOn;

	@ApiModelProperty(value = "Permission List")
	private List<Permissions> permissions;

	@ApiModelProperty(value = "Permission List")
	private List<GroupPermission> groupPermission;

	@ApiModelProperty(value = "Domains List")
	private List<Domains> domains;

	@ApiModelProperty(value = "Modules List")
	private List<Modules> modules;

	@ApiModelProperty(value = "Modules Group List")
	private List<ModulesGroup> modulesGroups;

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Long getParentPermissionId() {
		return parentPermissionId;
	}

	public void setParentPermissionId(Long parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}

	public List<Permissions> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permissions> permissions) {
		this.permissions = permissions;
	}

	public List<GroupPermission> getGroupPermission() {
		return groupPermission;
	}

	public void setGroupPermission(List<GroupPermission> groupPermission) {
		this.groupPermission = groupPermission;
	}

	public List<Domains> getDomains() {
		return domains;
	}

	public void setDomains(List<Domains> domains) {
		this.domains = domains;
	}

	public List<Modules> getModules() {
		return modules;
	}

	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}

	public List<ModulesGroup> getModulesGroups() {
		return modulesGroups;
	}

	public void setModulesGroups(List<ModulesGroup> modulesGroups) {
		this.modulesGroups = modulesGroups;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
