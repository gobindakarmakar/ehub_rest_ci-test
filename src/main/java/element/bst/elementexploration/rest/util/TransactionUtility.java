package element.bst.elementexploration.rest.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;

@Component
public class TransactionUtility 
{
	
	@Autowired
	TxDataService txDataService;
	
	@SuppressWarnings("unused")
	@Async
	public Boolean saveFotFotpData(MultipartFile multipartFile1, MultipartFile multipartFile2)
			throws IOException, ParseException {


		BufferedReader br1 = new BufferedReader(new InputStreamReader(multipartFile1.getInputStream()));

		String line1 = null;
		String line2 = null;

		int i = 1;
		int j = 1;

		while ((line1 = br1.readLine()) != null)// first fot
		{
			if (i != 1) {
				String[] data1 = null;
				if(line1.contains(","))
					data1 = line1.split(",");
				if(line1.contains(";"))
					data1 = line1.split(";");

				List<TxDto> listTx = new ArrayList<TxDto>();
				BufferedReader br2 = new BufferedReader(new InputStreamReader(multipartFile2.getInputStream()));
				while ((line2 = br2.readLine()) != null) // Fotp
				{
					if (j != 1) {
						String[] data2 = line2.split(";");

						if (data1!=null && data1[0].equals(data2[0]))// comparing fot eventId with fotp eventId
						{
							TxDto dto = new TxDto();
							dto.setAmount(data1[3]);
							dto.setEventId(data1[0]);
							dto.setPartyRole(data2[1]);
							dto.setPartyIdentifier(data2[4]);
							dto.setTransactionChannel(data1[2]);
							dto.setTransProductType(data1[1]);
							dto.setCurrency(data1[9]);
							DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
							Date date = dateFormat.parse(data1[12]);
							dto.setBusinessDate(date);// fot business date
							dto.setCustomText(data1[5]);
							listTx.add(dto);
						}

					} // 2nd if close
					j++;
				} // 2nd while close

				Boolean  value =true;
				if (!listTx.isEmpty())
					value = txDataService.generateAlert(listTx);

			} // 1st if
			i++;

		} // first while close
		return true;
	}

}
