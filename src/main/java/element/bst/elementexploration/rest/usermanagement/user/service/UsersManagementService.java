package element.bst.elementexploration.rest.usermanagement.user.service;

import java.util.Map;

/**
 * @author Prateek Maurya
 *
 */
public interface UsersManagementService {

	Map<String, Long> mainTilesData();

	Map<String, Long> groupsWithUserCount();
	Map<String, Long> rolesWithUserCount();

}
