package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Alert Comparision Object")
public class AlertComparisonReturnObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Present alert comparison")	
	private AlertComparisonDto present;
	@ApiModelProperty(value="Past alert comparison")	
	private AlertComparisonDto past;

	public AlertComparisonDto getPresent() {
		return present;
	}

	public void setPresent(AlertComparisonDto present) {
		this.present = present;
	}

	public AlertComparisonDto getPast() {
		return past;
	}

	public void setPast(AlertComparisonDto past) {
		this.past = past;
	}

}
