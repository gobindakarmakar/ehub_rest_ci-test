package element.bst.elementexploration.rest.extention.docparser.dao;

import element.bst.elementexploration.rest.extention.docparser.domain.IsoCodes;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface ISOCodeDao extends GenericDao<IsoCodes, Long>{

}
