package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
public class AdverseNewsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("news")
	private List<AdverseNews> adverseNews;

	public List<AdverseNews> getAdverseNews() {
		return adverseNews;
	}

	public void setAdverseNews(List<AdverseNews> adverseNews) {
		this.adverseNews = adverseNews;
	}

}
