package element.bst.elementexploration.rest.document.service;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.domain.DocumentsVault;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.document.dto.DocCommentVo;
import element.bst.elementexploration.rest.document.dto.DocumentDto;
import element.bst.elementexploration.rest.document.dto.DocumentUserAndCaseDto;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.generic.service.GenericService;
import net.sourceforge.tess4j.TesseractException;

/**
 * 
 * @author Viswanath
 *
 */
public interface DocumentService extends GenericService<DocumentVault, Long> {

	Long uploadDocument(MultipartFile uploadFile, String fileTitle, String remarks, Long userIdTemp, Integer docFlag,Long templateId,String entityId,String entityName,String entitySource,String docSection,File annualPdf) throws IOException, IllegalStateException,InvalidFormatException, TesseractException, Exception;

	void updateDocumentById(DocumentDto documentDto, Long docId, Long userId);
	
	Long uploadDocumentByScreenShotURL(String url, String fileTitle, String remarks, Long userIdTemp, Integer docFlag,Long templateId,String entityId,String entityName,String entitySource,String docSection,File annualPdf,Long docID) throws IOException, IllegalStateException,InvalidFormatException, TesseractException, Exception;

	boolean isPermissionWrite(Long userId, Long docId);

	void updateDocumentContent(MultipartFile docFile, Long userId, Long docId) throws IOException, Exception;

	Boolean isReadOrWritePermission(Long userId, Long docId);

	void softDeleteDocumentById(Long docId, Long userId);

	void shareDocumentWithCaseseed(Long userId, Long docId, Long caseseedId);

	void unshareDocumentFromCaseseed(Long userId, Long docId, Long caseseedId);

	void shareDocWithUser(long ownUserId, long shareUserId, long docId, int permission);

	void unShareDocFromUser(long ownUserId, long shareUserId, long docId, String comments);

	List<DocumentVaultDTO> myDocuments(Long userId, Long caseseedId, Integer docFlag, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn,String entityId,String docSection,Boolean isAllRequired);

	long countMyDocuments(Long userId, Long caseseedId, Integer docFlag,String entityId,String docSection);

	List<DocumentVaultDTO> fullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,
			String orderBy, String orderIn, Integer pageNumber, Integer recordsPerPage,String entityId,String docSection);

	long countFullTextSearchMyDocuments(Long userId, Long caseseedId, String keyword, Integer docFlag,String entityId,String docSection);

	List<DocumentVaultDTO> getAllMySharedDocument(Long userId, Long caseseedId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Integer docFlag);

	Long countGetAllMySharedDocument(Long userId, Long caseseedId, Integer docFlag);

	List<DocumentVaultDTO> fullTextSearchDocument(Long userId, String searchKeyWord, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn, Long caseId, Integer docFlag);

	Long countFullTextSearchDocument(Long userId, String searchKeyWord, Long caseId, Integer docFlag);

	DocumentUserAndCaseDto getCaseseedFromDocument(Long userId, Long docId)
			throws IllegalAccessException, InvocationTargetException;

	byte[] downloadDocument(Long docId, Long userId, HttpServletResponse response) throws DocNotFoundException, Exception;

	long countDocComment(Long userIdTemp, Long docId);

	List<DocCommentDTO> listDocComment(Long userId, Long docId, Integer pageNumber, Integer recordsPerPage,
			String orderIn);

	boolean isDocCommentable(long userId, Long docId);

	void documentDissemination(Long docId, Long userId, Long seedId);

	DocumentVaultDTO getDocumentDetails(Long docId, Long userId) throws IllegalAccessException, InvocationTargetException;
	
	Long commentDocument(Long userId, DocCommentVo docCommentDto);

	byte[] downloadDocumentDocParer(Long docId, Long userId, HttpServletResponse response) throws Exception;
	
	String getS3DownloadLinkFromServer(Long docId, String entityId) throws Exception;

	List<DocumentVaultDTO> getDocumentVaultByEntityIdAndDocflag(int docFlag, Long entityId);
}