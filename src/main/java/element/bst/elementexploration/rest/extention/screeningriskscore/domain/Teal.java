package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Teal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	@ApiModelProperty(value = "query profile")
	@JsonProperty("query-profile")
	private QueryProfile queryProfile;
	@ApiModelProperty(value = "hits")
	@JsonProperty("hits")
	private List<Hits> hits;
	public QueryProfile getQueryProfile() {
		return queryProfile;
	}
	public void setQueryProfile(QueryProfile queryProfile) {
		this.queryProfile = queryProfile;
	}
	public List<Hits> getHits() {
		return hits;
	}
	public void setHits(List<Hits> hits) {
		this.hits = hits;
	}

}
