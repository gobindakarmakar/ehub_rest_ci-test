package element.bst.elementexploration.rest.extention.menuitem.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Jalagari Paul
 *
 */
public class MenuFinalDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<GroupFinalDto> moduleGroups;

	public List<GroupFinalDto> getModuleGroups() {
		return moduleGroups;
	}

	public void setModuleGroups(List<GroupFinalDto> moduleGroups) {
		this.moduleGroups = moduleGroups;
	}

}
