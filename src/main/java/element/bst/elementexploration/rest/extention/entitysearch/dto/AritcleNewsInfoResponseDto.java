package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import element.bst.elementexploration.rest.extention.textsentiment.dto.SentimentDto;

public class AritcleNewsInfoResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String source;

	private String published;

	private String text;

	private String title;

	private String url;

	private List<EntityDto> entities;

	private SentimentDto sentiment;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<EntityDto> getEntities() {
		return entities;
	}

	public void setEntities(List<EntityDto> entities) {
		this.entities = entities;
	}

	public SentimentDto getSentiment() {
		return sentiment;
	}

	public void setSentiment(SentimentDto sentiment) {
		this.sentiment = sentiment;
	}

}
