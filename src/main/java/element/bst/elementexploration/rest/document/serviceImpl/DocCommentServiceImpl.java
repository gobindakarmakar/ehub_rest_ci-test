package element.bst.elementexploration.rest.document.serviceImpl;

import java.util.Date;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.document.dao.DocCommentDao;
import element.bst.elementexploration.rest.document.domain.DocComment;
import element.bst.elementexploration.rest.document.dto.DocCommentVo;
import element.bst.elementexploration.rest.document.service.DocCommentService;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Service("docCommentService")
public class DocCommentServiceImpl extends GenericServiceImpl<DocComment, Long> implements DocCommentService {
	@Autowired
	private DocCommentDao docCommentDao;
	
	@Autowired
	private AuditLogDao auditLogDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	
	
	@Autowired
	public DocCommentServiceImpl(GenericDao<DocComment, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public void editDocmentComment(Long commentId, long userId, DocCommentVo docCommentDto) {

		DocComment docComment = docCommentDao.find(commentId);
		Hibernate.initialize(docComment.getUser());
		Hibernate.initialize(docComment.getDocument());
		if (docComment != null && docComment.getUser().getUserId().equals(userId)) {
			docComment.setModificationReason(docCommentDto.getModificationReason());
			docComment.setTitle(docCommentDto.getTitle());
			docComment.setDescription(docCommentDto.getDescription());
			docComment.setModifiedOn(new Date());
			docCommentDao.saveOrUpdate(docComment);
			Users user = usersDao.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Comment edited");
			log.setDescription(user.getFirstName() + " " + user.getLastName() + " edited comment on document " + docComment.getDocument().getTitle());
			log.setUserId(userId);
			log.setDocumentId(docComment.getDocument().getDocId());
			log.setName(docComment.getDocument().getTitle());
			auditLogDao.create(log);
		}else{
			throw new NoDataFoundException("Comment not found.");
		}
	}
}
