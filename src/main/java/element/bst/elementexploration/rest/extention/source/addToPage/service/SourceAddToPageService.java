package element.bst.elementexploration.rest.extention.source.addToPage.service;

import java.io.File;
import java.util.List;

import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.source.addToPage.domain.SourceAddToPage;
import element.bst.elementexploration.rest.extention.source.addToPage.dto.SourceAddToPageDto;
import element.bst.elementexploration.rest.generic.service.GenericService;
/**
 * @author hanuman
 *
 */
public interface SourceAddToPageService extends GenericService<SourceAddToPage, Long> {

	List<SourceAddToPageDto> getAllSourceAddToPage();

	SourceAddToPageDto getSourceAddToPage(String entityId,String sorceEntity) throws Exception;

	List<SourceAddToPageDto> saveSourcesAddToPage(List<SourceAddToPageDto> addToPageDtos,Long userIdTemp)
			throws Exception;

	JSONObject uploadAddToPageDocument(MultipartFile uploadDoc, String fileTitle, Long userId, Integer docFlag,
			String entityId, String sourceName,File annualPdf,boolean isAddToPage)
					throws Exception;
	
	boolean deleteSourceAddToPage(String entityId,String sourceName,Long userIdTemp) throws Exception;

}
