package element.bst.elementexploration.rest.util;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.listManagement.dao.ListItemDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Component
@Transactional("transactionManager")
public class PendingUsersDeactivationUtility {

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private UsersService usersService;

	@Autowired
	ListItemDao listItemDao;

	@Transactional("transactionManager")
	@Scheduled(cron = "0 0 2 * * ?")
	public void deactivatePendingUsers() throws Exception {
		int pendingDaysOfUser = 0;
		ListItem pendingStatusItem = listItemDao.getListItemByListTypeAndDisplayName(ElementConstants.USER_STATUS_TYPE,
				UserStatus.Pending.getStatus());
		ListItem deactivatedStatusItem = listItemDao.getListItemByListTypeAndDisplayName(
				ElementConstants.USER_STATUS_TYPE, UserStatus.Deactivated.getStatus());
		Optional<Settings> pendingSetting = settingsService.findAll().stream()
				.filter(s -> s.getName().equalsIgnoreCase("Pending users deactivation after")).findFirst();
		Settings pendingSettingObject = null;
		if (pendingSetting.isPresent()) {
			pendingSettingObject = pendingSetting.get();
		}
		if (pendingStatusItem != null && pendingStatusItem.getListItemId() != null && pendingSetting != null) {
			List<Users> pendingUsers = usersService.findAll().stream()
					.filter(user -> user.getStatusId().getListItemId().equals(pendingStatusItem.getListItemId()))
					.collect(Collectors.toList());
			if (pendingUsers != null && pendingUsers.size() > 0) {
				for (Users user : pendingUsers) {
					Long days = (new Date().getTime() - user.getCreatedDate().getTime()) / 86400000;
					pendingDaysOfUser = Math.abs(days.intValue());
					if(null != pendingSettingObject.getDefaultValue())
					{
					if ((pendingDaysOfUser - Integer.valueOf(pendingSettingObject.getDefaultValue())) > 0) {
						user.setStatusId(deactivatedStatusItem);
						if (settingsService
								.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
								.equalsIgnoreCase("Off")) {
							usersService.saveOrUpdate(user);
						}
						
					}
					} 
					else
					{
						throw new Exception("Default value in SYS_SETTINGS for Pending users deactivation after cannot be null");
					}
				}
			}
		}
	}
}
