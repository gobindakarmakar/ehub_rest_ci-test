package element.bst.elementexploration.rest.extention.entitysearch.dto;

public class ArticleDto {
	
	private String articlePath;

	public String getArticlePath() {
		return articlePath;
	}

	public void setArticlePath(String articlePath) {
		this.articlePath = articlePath;
	}

}
