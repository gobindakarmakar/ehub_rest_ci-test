package element.bst.elementexploration.rest.casemanagement.bigdata.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Source implements Serializable{

	private static final long serialVersionUID = 7996279036602331372L;
	
	private String host;
	
	private String name;
		
	public Source() {
		super();
	}
	
	public Source(String host, String name) {
		super();
		this.host = host;
		this.name = name;
	}

	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}	
}
