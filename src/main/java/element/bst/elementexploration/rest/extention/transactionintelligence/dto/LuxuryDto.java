package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LuxuryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String hostname;
	
	private String status;
	@JsonProperty("website_class")
	private List<WebsiteClass> websiteClasses;

	public LuxuryDto() {
		super();
	}

	public LuxuryDto(String hostname, String status, List<WebsiteClass> websiteClasses) {
		super();
		this.hostname = hostname;
		this.status = status;
		this.websiteClasses = websiteClasses;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<WebsiteClass> getWebsiteClasses() {
		return websiteClasses;
	}

	public void setWebsiteClasses(List<WebsiteClass> websiteClasses) {
		this.websiteClasses = websiteClasses;
	}

	
}
