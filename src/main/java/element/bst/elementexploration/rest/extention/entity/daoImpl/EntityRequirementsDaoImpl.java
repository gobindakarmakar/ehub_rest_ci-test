package element.bst.elementexploration.rest.extention.entity.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.entity.dao.EntityRequirementsDao;
import element.bst.elementexploration.rest.extention.entity.domain.EntityRequirements;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("entityRequirementsDao")
public class EntityRequirementsDaoImpl extends GenericDaoImpl<EntityRequirements, Long>
		implements EntityRequirementsDao {

	public EntityRequirementsDaoImpl() {
		super(EntityRequirements.class);
	}

}
