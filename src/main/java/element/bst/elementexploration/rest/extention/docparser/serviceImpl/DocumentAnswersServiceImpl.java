package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.document.dao.DocumentDao;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentAnswersDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentQuestionsDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentAnswers;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentAnswerDto;
import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentAnswersService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * @author suresh
 *
 */
@Service("documentAnswersService")
public class DocumentAnswersServiceImpl extends GenericServiceImpl<DocumentAnswers, Long>
		implements DocumentAnswersService {
	
	@Autowired
	DocumentAnswersDao documentAnswersDao;
	
	@Autowired
	DocumentQuestionsDao documentQuestionsDao;
	
	@Autowired
	DocumentDao documentDao;
	
	@Autowired
	CaseDao caseDao;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Value("${isocode_question_url}")
	private String ISOCODE_QUESTION_URL;
	
	
	@Autowired
	public DocumentAnswersServiceImpl(GenericDao<DocumentAnswers, Long> genericDao) {
		super(genericDao);
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public DocumentAnswers saveNewDocumentAnswer(DocumentAnswerDto documentAnswersDto,Long userId) throws Exception {
		DocumentAnswers documentAnswers = new DocumentAnswers(); 
		if(documentAnswersDto.getId() == null || documentAnswersDto.getId() == 0){
			DocumentAnswers newDocumentAnswers = new DocumentAnswers(); 
			DocumentQuestions documentQuestions=documentQuestionsDao.find(documentAnswersDto.getDocumentQuestionId());			
			if(documentQuestions != null){
				DocumentAnswers newDocumentAnswer=new DocumentAnswers();
				if(documentAnswersDto.getDocId() !=null &&  documentAnswersDto.getDocId()!=0){
					DocumentVault documentVault = documentDao.find(documentAnswersDto.getDocId());
					if (documentVault != null) {
						Case caseSeed = caseDao.find(documentVault.getCaseId());
						if (caseSeed != null)
							newDocumentAnswer.setEntityId(caseSeed.getEntityId());
						else
							throw new NoDataFoundException("case not found");
					}
					else{
						throw new NoDataFoundException("Document not found");
					}
				}
				newDocumentAnswer.setDocumentQuestions(documentQuestions);
				newDocumentAnswer.setAnswer(documentAnswersDto.getAnswer());
				newDocumentAnswer.setUpdatedTime(new Date());
				newDocumentAnswer.setModefiedBy(userId);
				newDocumentAnswer.setDocumetnId(documentAnswersDto.getDocId());
				String evaluation = getEvaluation(documentQuestions.getName(),
						newDocumentAnswer.getAnswer(), documentQuestions.getPrimaryIsoCode(),newDocumentAnswer.getEntityId(),documentQuestions.isOSINT());
				if (evaluation != null) {
					JSONObject evaluationObject = new JSONObject(evaluation);
					if (evaluationObject.has("evaluation")) {
						if (!evaluationObject.getString("evaluation").equalsIgnoreCase("error"))
							newDocumentAnswer.setEvaluation(evaluationObject.getString("evaluation"));
					}
					if(evaluationObject.has("ranking"))
						newDocumentAnswer.setAnswerRank(Integer.parseInt(evaluationObject.getString("ranking")));

				}
				documentAnswers=documentAnswersDao.create(newDocumentAnswer);				
				eventPublisher.publishEvent(new LoggingEvent(documentAnswers.getId(), LoggingEventType.UPDATE_ANSWER, userId, new Date(), null, documentAnswers.getAnswer()));
				
			}else{
				throw new NoDataFoundException("Document question not found");
			}
		}
		else{
			DocumentAnswers existedDocumentAnswers = documentAnswersDao.find(documentAnswersDto.getId());
			DocumentQuestions documentQuestions = new DocumentQuestions();
			if(existedDocumentAnswers!=null)
				 documentQuestions = documentQuestionsDao.find(existedDocumentAnswers.getDocumentQuestions().getId());
			DocumentAnswers updateDocumentAnswers = new DocumentAnswers();
			if (existedDocumentAnswers != null) {
				updateDocumentAnswers.setAnswer(documentAnswersDto.getAnswer());
				updateDocumentAnswers.setUpdatedTime(new Date());
				updateDocumentAnswers.setModefiedBy(userId);
				updateDocumentAnswers.setActualAnswer(existedDocumentAnswers.getActualAnswer());
				updateDocumentAnswers.setDocumentQuestions(documentQuestions);
				updateDocumentAnswers.setDocumetnId(existedDocumentAnswers.getDocumetnId());
				updateDocumentAnswers.setEntityId(existedDocumentAnswers.getEntityId());
				updateDocumentAnswers.setTableDataPosition(existedDocumentAnswers.getTableDataPosition());
				if (!existedDocumentAnswers.getDocumentQuestions().getAnswerType().equals(AnswerType.TABLE)) {
					String evaluation = getEvaluation(documentQuestions.getName(), updateDocumentAnswers.getAnswer(),
							documentQuestions.getPrimaryIsoCode(), updateDocumentAnswers.getEntityId(),
							documentQuestions.isOSINT());
					if (evaluation != null) {
						JSONObject evaluationObject = new JSONObject(evaluation);
						if (evaluationObject.has("evaluation")) {
							if (!evaluationObject.getString("evaluation").equalsIgnoreCase("error"))
								updateDocumentAnswers.setEvaluation(evaluationObject.getString("evaluation"));
						}
						if (evaluationObject.has("ranking"))
							updateDocumentAnswers
									.setAnswerRank(Integer.parseInt(evaluationObject.getString("ranking")));

					}
				}
				documentAnswers = documentAnswersDao.create(updateDocumentAnswers);				
				eventPublisher.publishEvent(new LoggingEvent(documentAnswers.getId(), LoggingEventType.UPDATE_ANSWER, userId, new Date(), existedDocumentAnswers.getAnswer(), documentAnswers.getAnswer()));
			}
			else{
				throw new NoDataFoundException("Document answer not found");
			}
		}
		
		
		
	/*	if (documentQuestions != null) {
			if (documentQuestions.getRiskType() != null) {

				if (documentQuestions.getRiskType().equalsIgnoreCase("HIGH")) {
					if (newDocumentAnswers.getAnswer().equalsIgnoreCase("yes"))
						documentQuestions.setRiskScore(getRandomNumberLess100());
					if (newDocumentAnswers.getAnswer().equalsIgnoreCase("no"))
						documentQuestions.setRiskScore(getRandomNumberLess70());
					if (newDocumentAnswers.getAnswer().equalsIgnoreCase("empty"))
						documentQuestions.setRiskScore(getRandomNumberLess40());
					documentQuestionsDao.saveOrUpdate(documentQuestions);
				}
				if (documentQuestions.getRiskType().equalsIgnoreCase("MEDIUM")) {
					if (newDocumentAnswers.getAnswer().equalsIgnoreCase("yes"))
						documentQuestions.setRiskScore(getRandomNumberLess70());
					if (newDocumentAnswers.getAnswer().equalsIgnoreCase("no"))
						documentQuestions.setRiskScore(getRandomNumberLess40());

					documentQuestionsDao.saveOrUpdate(documentQuestions);
				}
				if (documentQuestions.getRiskType().equalsIgnoreCase("LOW")) {
					if (newDocumentAnswers.getAnswer().equalsIgnoreCase("yes"))
						documentQuestions.setRiskScore(getRandomNumberLess40());
					documentQuestionsDao.saveOrUpdate(documentQuestions);
				}
			}
			return newDocumentAnswers;
		}*/
			return documentAnswers;
		
	}
	
	private String getEvaluation(String question,String answer,String isoCode,String orgId,boolean isOSINT) throws Exception {
		JSONObject jsonObject = new JSONObject();
		String url = ISOCODE_QUESTION_URL;
		if(isOSINT){
		url=url+"osint/evaluate";
		jsonObject.put("org_id", orgId);
		jsonObject.put("session_id", UUID.randomUUID());
		jsonObject.put("ISO_code", isoCode);
		}
		else{
			url=url+"questionnaire/evaluate";
		jsonObject.put("org_id", orgId);
		jsonObject.put("session_id", UUID.randomUUID());
		jsonObject.put("question", question);
		jsonObject.put("answer", answer);
		jsonObject.put("ISO_code", isoCode);
		}
		 
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonObject.toString());
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}
	
	/*private int getRandomNumberLess40() {
		Random r = new Random();
		int Low = 1;
		int High = 40;
		int result = r.nextInt(High - Low) + Low;
		return (int) (result);
	}
	
	private int getRandomNumberLess70() {
		Random r = new Random();
		int Low = 41;
		int High = 70;
		int result = r.nextInt(High - Low) + Low;
		return (int) (result);
	}
	private int getRandomNumberLess100() {
		Random r = new Random();
		int Low = 71;
		int High = 100;
		int result = r.nextInt(High - Low) + Low;
		return (int) (result);
	}*/

	@Override
	@Transactional("transactionManager")
	public int updateEvaluation(DocumentAnswers documentAnswers,String evaluation,Long userId) {
		int status=0;
		
		if (evaluation != null) {
			String oldValue=documentAnswers.getEvaluation();
			DocumentQuestions documentQuestion = documentQuestionsDao
					.find(documentAnswers.getDocumentQuestions().getId());
			String url = ISOCODE_QUESTION_URL;
			Map<String,String> isoCodes=getBstIsocode();			
			JSONObject jsonObject = new JSONObject();
			if (documentQuestion.isOSINT()&&(isoCodes.containsKey(documentQuestion.getPrimaryIsoCode()))) {
				jsonObject.put("org_id", documentAnswers.getEntityId());
				jsonObject.put("session_id", UUID.randomUUID());
				jsonObject.put("question", documentQuestion.getName());
				jsonObject.put("ISO_code", documentQuestion.getPrimaryIsoCode());
				jsonObject.put("evaluation", evaluation);
				if (documentAnswers.getAnswerRank() == null)
					jsonObject.put("ranking", "3");
				else
					jsonObject.put("ranking", documentAnswers.getAnswerRank().toString());
				url = ISOCODE_QUESTION_URL + "osint/store";
			}

			else {
				url = ISOCODE_QUESTION_URL + "questionnaire/store";
				if(documentAnswers.getEntityId() == null ){
					Case caseSeed = caseDao.getCaseByDocId(documentAnswers.getDocumetnId());
					if(caseSeed != null)
						jsonObject.put("org_id", caseSeed.getEntityId());
				}else
					jsonObject.put("org_id", documentAnswers.getEntityId());
				
				jsonObject.put("session_id", UUID.randomUUID());
				jsonObject.put("question", documentQuestion.getName());
				jsonObject.put("answer", documentAnswers.getAnswer());
				jsonObject.put("ISO_code", documentQuestion.getPrimaryIsoCode());
				jsonObject.put("evaluation", evaluation);
				if (documentAnswers.getAnswerRank() == null)
					jsonObject.put("ranking", "3");
				else
					jsonObject.put("ranking", documentAnswers.getAnswerRank().toString());
			}
			status = ServiceCallHelper.getStatusCodeFromServer(url, jsonObject.toString());
			if (status == 201) {
				documentAnswers.setEvaluation(evaluation);
				documentAnswersDao.saveOrUpdate(documentAnswers);
				eventPublisher.publishEvent(new LoggingEvent(documentAnswers.getId(), LoggingEventType.UPDATE_EVALUATION,
						userId, new Date(),oldValue,evaluation));	
			}
		}
		
		return status;
	}

	@Override
	@Transactional("transactionManager")
	public int updateAnswerRanking(DocumentAnswers documentAnswers, Integer rank,Long tempUserId) {
		int status=0;
		if(rank !=null ){
			String oldValue = documentAnswers.getAnswerRank().toString();
			DocumentQuestions documentQuestion=documentQuestionsDao.find(documentAnswers.getDocumentQuestions().getId());
			String url = ISOCODE_QUESTION_URL;
			Map<String,String> isoCodes=getBstIsocode();			
			JSONObject jsonObject = new JSONObject();
			if(documentQuestion.isOSINT()&&(isoCodes.containsKey(documentQuestion.getPrimaryIsoCode()))){
				jsonObject.put("org_id", documentAnswers.getEntityId());
				jsonObject.put("session_id", UUID.randomUUID());
				jsonObject.put("question", documentQuestion.getName());				
				jsonObject.put("ISO_code", documentQuestion.getPrimaryIsoCode());
				if(!documentAnswers.getEvaluation().equalsIgnoreCase("undefined"))
					jsonObject.put("evaluation", documentAnswers.getEvaluation());			
				jsonObject.put("ranking", rank.toString());
				url = ISOCODE_QUESTION_URL + "osint/store";
			}
			else{
				jsonObject.put("org_id", documentAnswers.getEntityId());
				jsonObject.put("session_id", UUID.randomUUID());
				jsonObject.put("question", documentQuestion.getName());	
				jsonObject.put("answer", documentAnswers.getAnswer());
				jsonObject.put("ISO_code", documentQuestion.getPrimaryIsoCode());
				if(!documentAnswers.getEvaluation().equalsIgnoreCase("undefined"))
					jsonObject.put("evaluation", documentAnswers.getEvaluation());			
				jsonObject.put("ranking", rank.toString());
				url = ISOCODE_QUESTION_URL + "questionnaire/store";
			}
			status=ServiceCallHelper.getStatusCodeFromServer(url, jsonObject.toString());
			if(status==201){
			documentAnswers.setAnswerRank(rank);
			documentAnswers.setRankUpdatedBy(tempUserId);
			documentAnswers.setRankUpdatedTime(new Date());
			documentAnswersDao.saveOrUpdate(documentAnswers);
			eventPublisher.publishEvent(new LoggingEvent(documentAnswers.getId(), LoggingEventType.UPDATE_RANKING,
					tempUserId, new Date(),oldValue,documentAnswers.getAnswerRank().toString()));	
			
			}
		}
		
		return status;
	}
	
	public Map<String,String> getBstIsocode(){
		Map<String,String> isoCodes=new HashMap<String,String>();
		isoCodes.put("BST.1.3", "BST.1.3");
		isoCodes.put("BST.2.1", "BST.2.1");
		isoCodes.put("BST.2.2", "BST.2.2");
		isoCodes.put("BST.2.3", "BST.2.3");
		return isoCodes;
	}


	

}
