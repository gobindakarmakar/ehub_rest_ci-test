package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class BingEntityDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<EntityDto> entities;

	public List<EntityDto> getEntities() {
		return entities;
	}

	public void setEntities(List<EntityDto> entities) {
		this.entities = entities;
	}

}
