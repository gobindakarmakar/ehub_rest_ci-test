package element.bst.elementexploration.test.search.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.search.daoImpl.SearchHistoryDaoImpl;
import element.bst.elementexploration.rest.search.domain.SearchHistory;

@RunWith(MockitoJUnitRunner.class)
public class SearchHistoryDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<SearchHistory> root;

	@InjectMocks
	SearchHistoryDaoImpl searchHistoryDao;

	SearchHistory searchHistory;

	List<SearchHistory> searchHistoryList = new ArrayList<SearchHistory>();

	@Before
	public void setupMock() {
		searchHistory = new SearchHistory();
		searchHistory.setUserId(2L);
		searchHistory.setSearchId(1L);
		searchHistory.setFavourite(true);
		searchHistory.setCreatedOn(new Date());
		searchHistory.setUpdatedOn(new Date());
		searchHistory.setSearchData("searchData");
		searchHistory.setSearchFlag("searchFlag");
		searchHistoryList.add(searchHistory);
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findMostRecentSearchTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(SearchHistory.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(searchHistoryList);
		SearchHistory result = searchHistoryDao.findMostRecentSearch(2L);
		assertEquals(new Long(2l), result.getUserId());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findMostRecentSearch_whenReturnNullTest() {
		searchHistoryList.clear();
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(SearchHistory.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(searchHistoryList);
		SearchHistory result = searchHistoryDao.findMostRecentSearch(2L);
		assertEquals(null, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void findMostRecentSearch_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(SearchHistory.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		searchHistoryDao.findMostRecentSearch(2L);
	}
	
	@Test 
	public void getRecentSearchesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(searchHistoryList);
		List<SearchHistory> list = searchHistoryDao.getRecentSearches(2L, new Date(), new Date(), true, "flag", 1, 10);
		assertEquals(1, list.size());
	}
	
	@Test 
	public void getRecentSearches_whenToDateIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(searchHistoryList);
		List<SearchHistory> list = searchHistoryDao.getRecentSearches(2L, new Date(), null, true, "flag", 1, 10);
		assertEquals(1, list.size());
	}
	
	@Test 
	public void getRecentSearches_whenFromDateIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(searchHistoryList);
		List<SearchHistory> list = searchHistoryDao.getRecentSearches(2L, null, new Date(), true, "flag", 1, 10);
		assertEquals(1, list.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test 
	public void getRecentSearches_wheWhenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(HibernateException.class);
		searchHistoryDao.getRecentSearches(2L, new Date(), new Date(), true, "flag", 1, 10);
	}
	
	@Test
	public void countRecentSearchesTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = searchHistoryDao.countRecentSearches(2L, new Date(), new Date(), true, "flag");
		assertEquals(2L, result);
	}
	
	@Test
	public void countRecentSearches_whenToDateIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = searchHistoryDao.countRecentSearches(2L, new Date(), null, true, "flag");
		assertEquals(2L, result);
	}
	
	@Test
	public void countRecentSearches_whenFromDateIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(2L);
		long result = searchHistoryDao.countRecentSearches(2L, null, new Date(), true, "flag");
		assertEquals(2L, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void countRecentSearches_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(HibernateException.class);
		searchHistoryDao.countRecentSearches(2L, null, new Date(), true, "flag");
	}
	
}
