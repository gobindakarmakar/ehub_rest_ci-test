package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerAddress;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface CustomerAddressDao extends GenericDao<CustomerAddress, Long>{

}
