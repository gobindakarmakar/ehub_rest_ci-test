package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CountryDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CountryService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Service("countryService")
public class CountryServiceImpl extends GenericServiceImpl<Country, Long> implements CountryService {

	@Autowired
	public CountryServiceImpl(GenericDao<Country, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	CountryDao counrtryDao;

	@Override
	@Transactional("transactionManager")
	@Async
	public boolean saveCountryData(MultipartFile file) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
		String line = "";
		int i = 1;
		while ((line = bufferedReader.readLine()) != null) {
			if (i != 1) {
				String[] data = line.split(",");

				Country country = new Country();
				country.setCountry(data[0]);
				country.setIso2Code(data[1]);
				country.setIs03Code(data[2]);
				country.setLatitude(data[3]);
				country.setLongitude(data[4]);
				/*
				 * if (data[5].equals("#N/A")) { } else { //
				 * country.setRisk(Float.valueOf(data[5])); }
				 */
				country.setRowStatus(false);
				counrtryDao.create(country);
			} // if close
			i++;
		} // while close

		return true;
	}

	@Override
	@Transactional("transactionManager")
	@Async
	public Boolean saveCountryDataxl(File file) throws Exception {
		XSSFWorkbook wb = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			wb = new XSSFWorkbook(fis);
			XSSFSheet ws = wb.getSheetAt(0);
			ws.setForceFormulaRecalculation(true);

			int rowNum = ws.getLastRowNum() + 1;
			@SuppressWarnings("unused")
			int colNum = ws.getRow(0).getLastCellNum();
			int coun = -1, iso2 = -1, iso3 = -1, lati = -1, longi = -1, fatf = -1, tjn = -1, inc = -1, tir = -1,
					wbr = -1, fhr = -1, wjr = -1, wr = 12, pr = 13, bai = 14;

			// Read the headers first. Locate the ones you need
			XSSFRow rowHeader = ws.getRow(0);
			for (int j = 0; j < 12; j++) {
				XSSFCell cell = rowHeader.getCell(j);
				String cellValue = cell.toString();
				if ("COUNTRY".equalsIgnoreCase(cellValue)) {
					coun = j;
				}
				if ("ISO2_CODE".equalsIgnoreCase(cellValue)) {
					iso2 = j;
				}
				if ("ISO3_CODE".equalsIgnoreCase(cellValue)) {
					iso3 = j;
				}
				if ("LATITUDE".equalsIgnoreCase(cellValue)) {
					lati = j;
				}
				if ("LONGITUDE".equalsIgnoreCase(cellValue)) {
					longi = j;
				}
				if ("FATF Risk".equalsIgnoreCase(cellValue)) {
					fatf = j;
				}
				if ("Task Justice Network High Risk - Financial Secracy Index".equalsIgnoreCase(cellValue)) {
					tjn = j;
				}
				if ("International Narcotics Control Risk".equalsIgnoreCase(cellValue)) {
					inc = j;
				}
				if ("Transparency International Risk".equalsIgnoreCase(cellValue)) {
					tir = j;
				}
				if ("World Bank Risk".equalsIgnoreCase(cellValue)) {
					wbr = j;
				}
				if ("Freedom House Risk".equalsIgnoreCase(cellValue)) {
					fhr = j;
				}
				if ("World Justice Risk - Criminal Justice Risk Rating".equalsIgnoreCase(cellValue)) {
					wjr = j;
				}
				if ("WEF Risk".equalsIgnoreCase(cellValue)) {
					wr = j;
				}
				if ("Political Risk Services International Country Risk  (PRS)".equals(cellValue)) {
					pr = j;
				}
				if ("Basel AML Index 2017".equalsIgnoreCase(cellValue)) {
					bai = j;
				}

			}

			@SuppressWarnings("unused")
			Map<String, String> map = new HashMap<>();

			for (int i = 0; i < rowNum; i++) {
				if (i != 0) {
					XSSFRow row = ws.getRow(i);
					Country country = new Country();
					country.setCountry(row.getCell(coun).toString());
					country.setIso2Code(row.getCell(iso2).toString());
					country.setIs03Code(row.getCell(iso3).toString());
					if (row.getCell(lati).toString() != null)
						country.setLatitude(row.getCell(lati).toString());
					if (row.getCell(longi).toString() != null)
						country.setLongitude(row.getCell(longi).toString());
					if (row.getCell(fatf).toString() != null)
						country.setFatfRisk(Float.valueOf(row.getCell(fatf).toString()));
					if (row.getCell(tjn).toString() != null)
						country.setTaskJusticeNetworkHighRisk_FinancialSecracyIndex(
								Float.valueOf(row.getCell(tjn).toString()));
					if (row.getCell(inc).toString() != null)
						country.setInternationalNarcoticsControlRisk(Float.valueOf(row.getCell(inc).toString()));
					if (row.getCell(tir).toString() != null)
						country.setTransparencyInternationalRisk(Float.valueOf(row.getCell(tir).toString()));
					if (row.getCell(wbr).toString() != null)
						country.setWorldBankRisk(Float.valueOf(row.getCell(wbr).toString()));
					if (row.getCell(fhr).toString() != null)
						country.setFreedomHouseRisk(Float.valueOf(row.getCell(fhr).toString()));
					if (row.getCell(wjr).toString() != null)
						country.setWorldJusticeRiskWorld_CriminalJusticeRiskRating(
								Float.valueOf(row.getCell(wjr).toString()));
					country.setWefRisk(Float.valueOf(row.getCell(wr).toString()));
					country.setPoliticalRiskServicesInternationalCountryRisk_PRS(
							Float.valueOf(row.getCell(pr).toString()));
					if (row.getCell(bai).toString() != null && (!row.getCell(bai).toString().equals("#N/A")))
						country.setBaselAMLIndex2017(Double.valueOf(row.getCell(bai).toString()));
					country.setRowStatus(false);
					counrtryDao.create(country);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		} finally {
			wb.close();
		}

		return false;
	}

	@Override
	@Transactional("transactionManager")
	public Boolean auditCountryData() throws IOException, Exception {
		if(findAll().isEmpty()) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource("CountryData.xlsx");
		File file = new File(resource.toURI());
		XSSFWorkbook wb = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			wb = new XSSFWorkbook(fis);
			XSSFSheet ws = wb.getSheetAt(0);
			ws.setForceFormulaRecalculation(true);

			int rowNum = ws.getLastRowNum() + 1;
			@SuppressWarnings("unused")
			int colNum = ws.getRow(0).getLastCellNum();
			int coun = -1, iso2 = -1, iso3 = -1, lati = -1, longi = -1, fatf = -1, tjn = -1, inc = -1, tir = -1,
					wbr = -1, fhr = -1, wjr = -1, wr = 12, pr = 13, bai = 14;

			// Read the headers first. Locate the ones you need
			XSSFRow rowHeader = ws.getRow(0);
			for (int j = 0; j < 12; j++) {
				XSSFCell cell = rowHeader.getCell(j);
				String cellValue = cell.toString();
				if ("COUNTRY".equalsIgnoreCase(cellValue)) {
					coun = j;
				}
				if ("ISO2_CODE".equalsIgnoreCase(cellValue)) {
					iso2 = j;
				}
				if ("ISO3_CODE".equalsIgnoreCase(cellValue)) {
					iso3 = j;
				}
				if ("LATITUDE".equalsIgnoreCase(cellValue)) {
					lati = j;
				}
				if ("LONGITUDE".equalsIgnoreCase(cellValue)) {
					longi = j;
				}
				if ("FATF Risk".equalsIgnoreCase(cellValue)) {
					fatf = j;
				}
				if ("Task Justice Network High Risk - Financial Secracy Index".equalsIgnoreCase(cellValue)) {
					tjn = j;
				}
				if ("International Narcotics Control Risk".equalsIgnoreCase(cellValue)) {
					inc = j;
				}
				if ("Transparency International Risk".equalsIgnoreCase(cellValue)) {
					tir = j;
				}
				if ("World Bank Risk".equalsIgnoreCase(cellValue)) {
					wbr = j;
				}
				if ("Freedom House Risk".equalsIgnoreCase(cellValue)) {
					fhr = j;
				}
				if ("World Justice Risk - Criminal Justice Risk Rating".equalsIgnoreCase(cellValue)) {
					wjr = j;
				}
				if ("WEF Risk".equalsIgnoreCase(cellValue)) {
					wr = j;
				}
				if ("Political Risk Services International Country Risk  (PRS)".equals(cellValue)) {
					pr = j;
				}
				if ("Basel AML Index 2017".equalsIgnoreCase(cellValue)) {
					bai = j;
				}

			}

			@SuppressWarnings("unused")
			Map<String, String> map = new HashMap<>();

			for (int i = 0; i < rowNum; i++) {
				if (i != 0) {
					XSSFRow row = ws.getRow(i);
					Country country = new Country();
					country.setCountry(row.getCell(coun).toString());
					country.setIso2Code(row.getCell(iso2).toString());
					country.setIs03Code(row.getCell(iso3).toString());
					if (row.getCell(lati).toString() != null)
						country.setLatitude(row.getCell(lati).toString());
					if (row.getCell(longi).toString() != null)
						country.setLongitude(row.getCell(longi).toString());
					if (row.getCell(fatf).toString() != null)
						country.setFatfRisk(Float.valueOf(row.getCell(fatf).toString()));
					if (row.getCell(tjn).toString() != null)
						country.setTaskJusticeNetworkHighRisk_FinancialSecracyIndex(
								Float.valueOf(row.getCell(tjn).toString()));
					if (row.getCell(inc).toString() != null)
						country.setInternationalNarcoticsControlRisk(Float.valueOf(row.getCell(inc).toString()));
					if (row.getCell(tir).toString() != null)
						country.setTransparencyInternationalRisk(Float.valueOf(row.getCell(tir).toString()));
					if (row.getCell(wbr).toString() != null)
						country.setWorldBankRisk(Float.valueOf(row.getCell(wbr).toString()));
					if (row.getCell(fhr).toString() != null)
						country.setFreedomHouseRisk(Float.valueOf(row.getCell(fhr).toString()));
					if (row.getCell(wjr).toString() != null)
						country.setWorldJusticeRiskWorld_CriminalJusticeRiskRating(
								Float.valueOf(row.getCell(wjr).toString()));
					country.setWefRisk(Float.valueOf(row.getCell(wr).toString()));
					country.setPoliticalRiskServicesInternationalCountryRisk_PRS(
							Float.valueOf(row.getCell(pr).toString()));
					if (row.getCell(bai).toString() != null && (!row.getCell(bai).toString().equals("#N/A")))
						country.setBaselAMLIndex2017(Double.valueOf(row.getCell(bai).toString()));
					country.setRowStatus(false);
					counrtryDao.create(country);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		} finally {
			wb.close();
		}
	}
		return true;
	}

}
