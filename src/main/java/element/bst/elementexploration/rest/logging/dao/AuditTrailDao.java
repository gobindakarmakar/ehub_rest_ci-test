package element.bst.elementexploration.rest.logging.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.logging.domain.AuditTrail;

/**
 * 
 * @author Amit Patel
 *
 */
public interface AuditTrailDao extends GenericDao<AuditTrail, Long>{

}
