package element.bst.elementexploration.rest.notification.domain;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;


@Entity
@Table(name = "bst_notification_mapping")
@Access(AccessType.FIELD)
public class NotificationMapping implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7385694767611867559L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long mappingId;
	
	@Column(name = "notification_id")
	private Long notifId;
	
	@Column(name = "user_id")
	private Long userId;	
	
	@Column(name = "type")
	private Integer type;	
	
	@Column(name = "flag")
	private Integer flag;	
	
	@Column(name = "status")
	private Integer status;	
	
	/**
	 * 
	 */
	public NotificationMapping() {
		super();
	}

	public NotificationMapping(Long notifId, Long userId) {
		super();
		this.notifId = notifId;
		this.userId = userId;
	}

	public Long getMappingId() {
		return mappingId;
	}

	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}

	public Long getNotifId() {
		return notifId;
	}

	public void setNotifId(Long notifId) {
		this.notifId = notifId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
