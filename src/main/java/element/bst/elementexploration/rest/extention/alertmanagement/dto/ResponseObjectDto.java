package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseObjectDto {
	
	@JsonProperty("RESPONSE")
	private Object response;
	
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	
}
