package element.bst.elementexploration.test.usermanagement.security.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.usermanagement.security.daoImpl.FailedLoginDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.domain.FailedLogin;


@RunWith(MockitoJUnitRunner.class)
public class FailedLoginDaoTest {
	
	@Mock
	 SessionFactory sessionFactory;
	
	@Mock
	Session session;
	
	@SuppressWarnings("rawtypes")
	@Mock
	Query query;
	
	@SuppressWarnings("rawtypes")
	@Mock
	NativeQuery nativeQuery;
	
	@Mock
	CriteriaBuilder builder;
	
	@Mock
	CriteriaQuery<Object> criteriaQuery;
	
	@Mock
	Root<FailedLogin> root;
	
	@InjectMocks
	FailedLoginDaoImpl failedLoginDao;
	
	public List<FailedLogin> list = new ArrayList<FailedLogin>();
	
	@Before
	public void doSetup() {
		
		FailedLogin failedLogin = new FailedLogin(1L, "analyst@test.com", "analyst", 1, "analyst", new Date(), "", new Date());
		list.add(failedLogin);
		
		MockitoAnnotations.initMocks(this);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getFailedLoginByUsernameTest() {
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(FailedLogin.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		FailedLogin newResult = failedLoginDao.getFailedLoginByUsername("Analyst");
		assertEquals("analyst", newResult.getScreenName());
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getFailedLoginByUsername_whenFailedLoginIsNullTest() {
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(FailedLogin.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(null);
		FailedLogin newResult = failedLoginDao.getFailedLoginByUsername("Analyst");
		assertEquals(null, newResult);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getFailedLoginByUsername_whenExceptionThrowTest() {
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(FailedLogin.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		failedLoginDao.getFailedLoginByUsername("Analyst");
	}

}
