package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class EventDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long notificationId;
	
	private String subject;
	
	private String body;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createdOn;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledFrom;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledTo;
	
	private Integer type;
	
	private Integer level;
	
	private Long createdBy;	
	
	private Long caseId;
	
	private Integer flag;

	public EventDTO() {
		super();
	}

	public EventDTO(Long notificationId, String subject, String body, LocalDateTime createdOn,
			LocalDateTime scheduledFrom, LocalDateTime scheduledTo, Integer type, Integer level, Long createdBy,
			Long caseId, Integer flag) {
		super();
		this.notificationId = notificationId;
		this.subject = subject;
		this.body = body;
		this.createdOn = createdOn;
		this.scheduledFrom = scheduledFrom;
		this.scheduledTo = scheduledTo;
		this.type = type;
		this.level = level;
		this.createdBy = createdBy;
		this.caseId = caseId;
		this.flag = flag;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getScheduledFrom() {
		return scheduledFrom;
	}

	public void setScheduledFrom(LocalDateTime scheduledFrom) {
		this.scheduledFrom = scheduledFrom;
	}

	public LocalDateTime getScheduledTo() {
		return scheduledTo;
	}

	public void setScheduledTo(LocalDateTime scheduledTo) {
		this.scheduledTo = scheduledTo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
		
}
