package element.bst.elementexploration.rest.extention.alertmanagement.dto;

import java.io.Serializable;

/**
 * @author Paul Jalagari
 *
 */
public class StatusCountDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String status;

	private Integer count;
	
	private Long groupId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	

}
