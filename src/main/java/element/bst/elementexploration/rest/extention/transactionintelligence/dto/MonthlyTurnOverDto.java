package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Monthly turnover")
public class MonthlyTurnOverDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="Between 1 to 5 million USD")	
	private long BwOneToFiveMillionUSD;
	@ApiModelProperty(value="Between 5 to 10 million USD")
	private long BwFiveToTenMillionUSD;
	@ApiModelProperty(value="Above 10 million USD")
	private long AboveTenMillionUSD;
	@ApiModelProperty(value="Below 1 million USD")
	private long belowOneMillionUSD;
	
	@JsonProperty("below-one-million-USD")
	public long getBelowOneMillionUSD() {
		return belowOneMillionUSD;
	}

	public void setBelowOneMillionUSD(long belowOneMillionUSD) {
		this.belowOneMillionUSD = belowOneMillionUSD;
	}
	@JsonProperty("between-one-to-five-million-USD")
	public long getBwOneToFiveMillionUSD() {
		return BwOneToFiveMillionUSD;
	}

	public void setBwOneToFiveMillionUSD(long bwOneToFiveMillionUSD) {
		BwOneToFiveMillionUSD = bwOneToFiveMillionUSD;
	}
	@JsonProperty("between-five-to-ten-million-USD")
	public long getBwFiveToTenMillionUSD() {
		return BwFiveToTenMillionUSD;
	}

	public void setBwFiveToTenMillionUSD(long bwFiveToTenMillionUSD) {
		BwFiveToTenMillionUSD = bwFiveToTenMillionUSD;
	}
	@JsonProperty("above-ten-million-USD")
	public long getAboveTenMillionUSD() {
		return AboveTenMillionUSD;
	}

	public void setAboveTenMillionUSD(long aboveTenMillionUSD) {
		AboveTenMillionUSD = aboveTenMillionUSD;
	}


}
