package element.bst.elementexploration.rest.generic.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import element.bst.elementexploration.rest.util.FilterColumnDto;
import element.bst.elementexploration.rest.util.FilterModelDto;
import element.bst.elementexploration.rest.util.RequestFilterDto;

/**
 * 
 * @author Amit Patel
 *
 */
public interface GenericDao <E, I>{

	public Session getCurrentSession();
	public List<E> findAll();
	public E find(I id);
	public E create(E e);
	public List<E> saveAll(Iterable<E> e);
	public void update(E e);
	public void saveOrUpdate(E e);
	public void delete(E e);
	public void flush();
	public Map<List<E>, Long> filterData(E e, List<FilterColumnDto> columnsList, Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy);	
}
