package element.bst.elementexploration.rest.extention.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.workflow.service.HdfsService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "HDFS API" }, description = "Manages Hadoop file data")
@RestController
@RequestMapping("/api/workflow/hdfs")
public class HdfsController extends BaseController {

	@Autowired
	private HdfsService hdfsService;

	@ApiOperation("Deletes the files")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/deletefiles", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deletedFiles(@RequestParam String token,
			@RequestParam(value = "profileId", required = false) String profileId,
			@RequestParam(value = "folderPath", required = false) String folderPath) throws Exception {
		return new ResponseEntity<>(hdfsService.deletedFiles(profileId, folderPath), HttpStatus.OK);
	}

	@ApiOperation("Gets the list of directories")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/directories", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> ListOfDirectories(@RequestParam String token,
			@RequestParam(value = "profileId") String profileId, @RequestParam(value = "folderPath") String folderPath)
			throws Exception {
		return new ResponseEntity<>(hdfsService.ListOfDirectories(profileId, folderPath), HttpStatus.OK);
	}

	@ApiOperation("Makes a directory in the file system")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/makedir", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> makeDirectory(@RequestParam String token,
			@RequestParam(value = "profileId", required = false) String profileId,
			@RequestParam(value = "folderPath", required = false) String folderPath) throws Exception {
		return new ResponseEntity<>(hdfsService.makeDirectory(profileId, folderPath), HttpStatus.OK);
	}

	@ApiOperation("Gets sample data of HDFS by stage")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/sampledata/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSampleDataOfHdfs(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(hdfsService.getSampleDataOfHdfs(workflowId, stageId), HttpStatus.OK);
	}

	@ApiOperation("Gets schema of HDFS by stage ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/schema/{workflowId}/{stageId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSchemaOfHdfs(@RequestParam String token, @PathVariable String workflowId,
			@PathVariable String stageId) throws Exception {
		return new ResponseEntity<>(hdfsService.getSchemaOfHdfs(workflowId, stageId), HttpStatus.OK);
	}

}
