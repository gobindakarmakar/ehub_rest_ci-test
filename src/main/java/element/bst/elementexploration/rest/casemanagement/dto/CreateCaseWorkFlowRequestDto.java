package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Rambabu
 *
 */
@ApiModel("Case")
public class CreateCaseWorkFlowRequestDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "name of the case", required = true)
	@NotEmpty(message = "Case name can not be blank.")
	@Size(max = 75, message = "Case name should not be more than 75 characters.")
	private String name;
	
	@ApiModelProperty(value = "type of the case")
	private String type;
	
	@ApiModelProperty(value = "sub type of the case")
	private String subType;
	
	@ApiModelProperty(value = "work flow id")
	private String workflowId;
	
	@ApiModelProperty(value = "work flow name")
	private String workflowName;
		
	@ApiModelProperty(value = "case creation time")
	private Date timestamp;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@JsonProperty("sub-type")
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
	@JsonProperty("workflow-id")
	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}
	@JsonProperty("workflow-name")
	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	
	
	
	


}
