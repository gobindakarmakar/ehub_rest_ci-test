package element.bst.elementexploration.rest.usermanagement.admin.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.activiti.app.conf.Bootstrapper;
import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDtoWithRolesAndGroups;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersRegistrationDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import element.bst.elementexploration.rest.util.UserCreationSource;
import element.bst.elementexploration.rest.util.UserStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Administration API" },description= "Manages admin privileges")
@RestController
@RequestMapping("/api/adminNew")
public class AdministrationsController extends BaseController {

	@Autowired
	private UsersService usersService;
	
	@Autowired
	GroupsService groupsService;
	
/*	@Autowired
	private PasswordEncoder passwordEncoder;*/
	
	/*@Autowired
    private ApplicationEventPublisher eventPublisher;*/
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private UserGroupsService userGroupsService;
	
	@Autowired
	ListItemService listItemService;
	
	@Autowired
	AuditLogService auditLogService;
	
	@Autowired
	SettingsService settingsService;
	
	@Autowired
	Bootstrapper bootstrapper;
	
	

	@SuppressWarnings("unused")
	@ApiOperation("Register a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Username/email already exists or "+ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/register", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> handleRegistration(HttpServletRequest request, @RequestParam("token") String adminToken,
			@Valid @RequestBody UsersRegistrationDto userRegistrationDto, BindingResult results) throws Exception {
		GenericReturnObject  message= new GenericReturnObject();
		if (null == userRegistrationDto.getUserId()) {
			if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
					"Allow to create manually-Users").equalsIgnoreCase("On")) {
				Users user = new Users();
				List<Long> roleIdsToBeAdded = null;
				List<Long> groupIdsToBeAdded = null;
				List<Long> roleIdsToBeRemoved = null;
				List<Long> groupIdsToBeRemoved = null;
				boolean statusAdded = false;
				BeanUtils.copyProperties(userRegistrationDto, user);
				if (null != userRegistrationDto.getRolesToBeAdded()
						&& userRegistrationDto.getRolesToBeAdded().size() > 0) {
					roleIdsToBeAdded = userRegistrationDto.getRolesToBeAdded();
				}
				if (null != userRegistrationDto.getGroupsToBeAdded()
						&& userRegistrationDto.getGroupsToBeAdded().size() > 0) {
					groupIdsToBeAdded = userRegistrationDto.getGroupsToBeAdded();
				}
				if (null != userRegistrationDto.getRolesToBeRemoved()
						&& userRegistrationDto.getRolesToBeRemoved().size() > 0) {
					roleIdsToBeRemoved = userRegistrationDto.getRolesToBeRemoved();
				}
				if (null != userRegistrationDto.getGroupsToBeRemoved()
						&& userRegistrationDto.getGroupsToBeRemoved().size() > 0) {
					groupIdsToBeRemoved = userRegistrationDto.getGroupsToBeRemoved();
				}
				ListItem country = null;
				if (null != userRegistrationDto.getCountry())
					country = listItemService.find(userRegistrationDto.getCountry());
				if (country != null)
					user.setCountryId(country);
				Long adminUserId = getCurrentUserId();
				if (!results.hasErrors()) {
					UsersDto checkUserByUserName = usersService.getUserByEmailOrUsername(user.getScreenName());
					if (checkUserByUserName == null) {
						UsersDto checkUserByEmailAddress = usersService
								.getUserByEmailOrUsername(user.getEmailAddress());
						if (checkUserByEmailAddress == null) {
							// Check first if user is an Administrator
							// if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME,
							// adminUserId)) {
							// user.setPassword(passwordEncoder.encode(user.getPassword()));
							user.setPassword(null);
							user.setCreatedDate(new Date());
							user.setModifiedDate(null);
							List<ListItem> pendingStatusList = listItemService.findAll().stream()
									.filter(o -> o.getDisplayName().equalsIgnoreCase(UserStatus.Pending.toString()))
									.collect(Collectors.toList());
							if (pendingStatusList != null && pendingStatusList.size() > 0) {
								user.setStatusId(pendingStatusList.get(0));
								statusAdded = true;
							}
							user.setCreatedBy(adminUserId);
							user.setSource(UserCreationSource.INTERNAL.toString().toLowerCase());
							user = usersService.save(user);
							// Assign Roles and Groups
							if (null != roleIdsToBeAdded && roleIdsToBeAdded.size() > 0) {
								boolean isRolesSaved = userRolesService.saveUserRoles(user, roleIdsToBeAdded,
										getCurrentUserId(), ElementConstants.USER_REGISTRATION_TYPE);
							}
							if (null != groupIdsToBeAdded && groupIdsToBeAdded.size() > 0) {
								boolean isGroupsSaved = userGroupsService.saveUserGroups(user, groupIdsToBeAdded,
										getCurrentUserId(), ElementConstants.USER_REGISTRATION_TYPE);
							}
							if (null != roleIdsToBeRemoved && roleIdsToBeRemoved.size() > 0) {
								boolean isRolesRemoved = userRolesService.removeUserRoles(user, roleIdsToBeRemoved,
										getCurrentUserId(), ElementConstants.USER_REGISTRATION_TYPE);
							}
							if (null != groupIdsToBeRemoved && groupIdsToBeRemoved.size() > 0) {
								boolean isGroupsRemoved = userGroupsService.removeUserGroups(user, groupIdsToBeRemoved,
										getCurrentUserId(), ElementConstants.USER_REGISTRATION_TYPE);
							}
							UsersDto userAfterChange = usersService.getUserByEmailOrUsername(user.getEmailAddress());
							/*
							 * String url = new String(request.getScheme() + "://" + request.getLocalName()
							 * + ":" + request.getLocalPort() + "/");
							 */
							//checks if enable authentication is ON
							boolean isEmailSent=false;
							if (settingsService.findSettingToggleValue(SystemSettingTypes.MAIL_SETTING.toString(),
									"Enable Authentication").equalsIgnoreCase("On")) {
							 isEmailSent = usersService.sendEmailForVerification(userAfterChange, request);
							}
							// Users updatedUser=usersService.find(user.getUserId());
							UsersDtoWithRolesAndGroups usersDtoWithRolesAndGroups = usersService
									.getUserProfileById(user.getUserId(), getCurrentUserId());
							/*
							 * eventPublisher.publishEvent(new LoggingEvent(user,
							 * LoggingEventType.USER_REGISTRATION_BY_ADMIN, adminUserId, new Date()));
							 */
							// Audit operation
							AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
									ElementConstants.USER_REGISTRATION_TYPE, null,
									ElementConstants.USER_MANAGEMENT_TYPE);
							Users admin = usersService.find(getCurrentUserId());
							admin = usersService.prepareUserFromFirebase(admin);
							log.setDescription(admin.getFirstName() + " " + admin.getLastName() + " "
									+ ElementConstants.USER_REGISTRATION + " "
									+ usersDtoWithRolesAndGroups.getFirstName() + " "
									+ usersDtoWithRolesAndGroups.getLastName());
							log.setUserId(admin.getUserId());
							log.setTypeId(usersDtoWithRolesAndGroups.getUserId());
							log.setSubType(UserStatus.Pending.toString());
							log.setName(ElementConstants.USER_REGISTRATION_TYPE);
							auditLogService.save(log);
							
								if (isEmailSent) {
									message.setResponseMessage(ElementConstants.USER_CREATION_SUCCESSFUL + " "
											+ ElementConstants.EMAIL_SENT);
									message.setData(usersDtoWithRolesAndGroups);
									message.setStatus(ElementConstants.SUCCESS_STATUS);
									return new ResponseEntity<>(message, HttpStatus.OK);
								} else {
									message.setResponseMessage(ElementConstants.USER_CREATION_SUCCESSFUL);
									message.setData(usersDtoWithRolesAndGroups);
									message.setStatus(ElementConstants.SUCCESS_STATUS);
									return new ResponseEntity<>(message, HttpStatus.OK);
								}
							/*
							 * } else { message.setResponseMessage(ElementConstants.USER_NOT_ADMIN);
							 * message.setStatus(ElementConstants.ERROR_STATUS); return new
							 * ResponseEntity<>(message, HttpStatus.OK); }
							 */
						} else {
							message.setResponseMessage(
									ElementConstants.EMAILADDRESS_ALREADY_EXISTS + user.getEmailAddress());
							message.setStatus(ElementConstants.ERROR_STATUS);
							return new ResponseEntity<>(message, HttpStatus.OK);
						}

					} else {
						message.setResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + user.getScreenName());
						message.setStatus(ElementConstants.ERROR_STATUS);
						return new ResponseEntity<>(message, HttpStatus.OK);
					}
				} else {
					throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
				}
			}
			else {
				message.setResponseMessage(ElementConstants.USER_CREATION_DISABLED);
				message.setStatus(ElementConstants.ERROR_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		} else {
			Users user = usersService.find(userRegistrationDto.getUserId());
			user = usersService.prepareUserFromFirebase(user);
			//if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, getCurrentUserId())) {
			if (user != null) {
				if (settingsService.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
						"Allow update manually-Users").equalsIgnoreCase("On")) {
					boolean isUpdated = usersService.updateUser(user, userRegistrationDto, getCurrentUserId());
					// Users updatedUser=usersService.find(user.getUserId());
					UsersDtoWithRolesAndGroups usersDtoWithRolesAndGroups = usersService
							.getUserProfileById(user.getUserId(), getCurrentUserId());
					if (isUpdated) {
						message.setResponseMessage(ElementConstants.USER_CREATION_SUCCESSFUL);
						message.setStatus(ElementConstants.SUCCESS_STATUS);
						message.setData(usersDtoWithRolesAndGroups);
						return new ResponseEntity<>(message, HttpStatus.OK);
					} else {
						message.setResponseMessage(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
						message.setStatus(ElementConstants.ERROR_STATUS);
						return new ResponseEntity<>(message, HttpStatus.OK);
					}
				}else {
					message.setResponseMessage(ElementConstants.USER_UPDATION_DISABLED);
					message.setStatus(ElementConstants.ERROR_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
			} else {
					message.setResponseMessage(ElementConstants.USER_NOT_FOUND);
					message.setStatus(ElementConstants.ERROR_STATUS);
					return new ResponseEntity<>(message, HttpStatus.OK);
				}
			/*}else {
				message.setResponseMessage(ElementConstants.USER_NOT_ADMIN);
				message.setStatus(ElementConstants.ERROR_STATUS);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}*/
		}
	}

	@ApiOperation("Activate a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_ACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/activateUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> activateUser(@RequestParam("token") String adminUserToken,
			@RequestParam("userId") Long userId, HttpServletRequest request) {
		Long adminUserId = getCurrentUserId();
		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminUserId)) {
			Users userBeforeChange = usersService.find(userId);
			userBeforeChange = usersService.prepareUserFromFirebase(userBeforeChange);
			if(userBeforeChange != null){
				usersService.activateUser(userBeforeChange);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_ACTIVATED_SUCCESSFULLY_MSG), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Deactivate a user by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_DEACTIVATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/deactivateUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deactivateUser(@RequestParam("token") String adminUserToken,
			@RequestParam("userId") Long userId, HttpServletRequest request) {
		Long adminUserId = getCurrentUserId();
		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminUserId)) {
			Users userBeforeChange = usersService.find(userId);
			userBeforeChange = usersService.prepareUserFromFirebase(userBeforeChange);
			if(userBeforeChange != null){
				usersService.deactivateUser(userBeforeChange);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_DEACTIVATED_SUCCESSFULLY_MSG), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}
	@ApiOperation("Get a user details by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UsersDto.class, message = "Operation Succesfull"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserDetails", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserDetails(@RequestParam("token") String adminToken,
			@RequestParam("userId") Long userId, HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {

		UsersDto userDto = new UsersDto();

		Long adminId = getCurrentUserId();

		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {
			Users user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			if(user != null){
				BeanUtils.copyProperties(user, userDto);
				return new ResponseEntity<>(userDto, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND_MSG), HttpStatus.NOT_FOUND);
			}
		} else{
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation("Update user details by admin")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UsersDto.class, message = ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.USER_NOT_FOUND),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.EMAILADDRESS_ALREADY_EXISTS+" or "+ElementConstants.USERNAME_ALREADY_EXISTS),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateUser", produces = { "application/json; charset=UTF-8" },
			consumes = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleUpdateUserByAdmin(@RequestParam("userId") Long userId, 
			@RequestParam("token") String adminToken, @Valid @RequestBody UsersDto updatedUser, 
			BindingResult results, HttpServletRequest request) throws Exception {

		Long adminId = getCurrentUserId();

		if (groupsService.doesUserbelongtoGroup(ElementConstants.ADMIN_GROUP_NAME, adminId)) {

			Users user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			if (user == null) {
				throw new NoDataFoundException(ElementConstants.USER_NOT_FOUND);
			}

			UsersDto checkUserByUserName = usersService.checkUserNameOrEmailExists(updatedUser.getScreenName(), updatedUser.getUserId());
			if (checkUserByUserName == null) {
				UsersDto checkUserByEmailAddress = usersService.checkUserNameOrEmailExists(updatedUser.getEmailAddress(), updatedUser.getUserId());
				if (checkUserByEmailAddress == null) {

					// Set user updated record
					user.setEmailAddress(updatedUser.getEmailAddress());
					user.setFirstName(updatedUser.getFirstName());
					user.setLastName(updatedUser.getLastName());
					user.setMiddleName(updatedUser.getMiddleName());
					user.setScreenName(updatedUser.getScreenName());
					user.setDob(updatedUser.getDob());
					//user.setCountry(updatedUser.getCountry());	
					user.setModifiedDate(new Date());
					if (settingsService
							.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
									ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
							.equalsIgnoreCase("Off")) {
					usersService.saveOrUpdate(user);
					}

					return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG), HttpStatus.OK);
				} else {
					return new ResponseEntity<>(new ResponseMessage(ElementConstants.EMAILADDRESS_ALREADY_EXISTS + updatedUser.getEmailAddress()),
							HttpStatus.CONFLICT);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + updatedUser.getScreenName()),
						HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_ADMIN), HttpStatus.UNAUTHORIZED);
		}
		
	}
}
