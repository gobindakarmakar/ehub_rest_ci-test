package element.bst.elementexploration.rest.extention.isracard.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.isracard.service.IsraCardService;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("israCardService")
public class IsraCardServiceImpl implements IsraCardService {

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENTION_API;

	@Value("${bigdata_isracard_uc3_url}")
	private String BIGDATA_ISRACARD_UC3_URL;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String deleteDataInServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_DELETE_DATA_ON_SERVER_MSG);
		}
	}

	@Override
	public String getAgeDestributionCulster(String clusterId, String jsonToBeSent) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/age_distribution/" + encode(clusterId);
		return postStringDataToServer(url, jsonToBeSent);
	}

	@Override
	public String getAsAndBsDistribution(String clusterId, String buckets, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/as_bs_distribution/" + encode(clusterId) + "/"
				+ encode(buckets);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getAverageExpenditureTrends(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/average_expenditure_trends/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getAverageExpenditureForCluster(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/2_3/average_expenditure_for_cluster/"
				+ encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getAvgExpenditureRangesWithClusterIds(String buckets) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/avg_expendi_ranges/" + encode(buckets);
		return getDataFromServer(url);
	}

	@Override
	public String bankingCardDistribution(String clusterId, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/banking_card_distribution/" + encode(clusterId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getCityDistribution(String clusterId, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/city_distribution/" + encode(clusterId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getClusterSize() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/cluster_stats";
		return getDataFromServer(url);
	}

	@Override
	public String getClusterFallingOnGivenCriteria(String customerCountStart, String customerCountEnd,
			String avgExpendiStart, String avgExpendiEnd) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/cluster/" + encode(customerCountStart) + "/"
				+ encode(customerCountEnd) + "/" + encode(avgExpendiStart) + "/" + encode(avgExpendiEnd);
		return getDataFromServer(url);
	}

	@Override
	public String getClusterRangesWithCustomerCount(String buckets) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/cluster_size_ranges/" + encode(buckets);
		return getDataFromServer(url);
	}

	@Override
	public String getCustomerCount(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/customers_count/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getOverallExpenditureAvg() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/overall_avg_expenditure";
		return getDataFromServer(url);
	}

	@Override
	public String getSeniorityDistribution(String clusterId, String buckets) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/seniority_distribution/" + encode(clusterId) + "/"
				+ encode(buckets);
		return getDataFromServer(url);
	}

	@Override
	public String issuedByDistribution(String clusterId, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/issued_by_distribution/" + encode(clusterId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getOverAllExpenditureAvgAndSum() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/overall_stats";
		return getDataFromServer(url);
	}

	@Override
	public String getDataBasedOnFilters(String filters, String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/data/" + encode(clusterId);
		return postStringDataToServer(url, filters);
	}

	@Override
	public String getGenderDistributionForCluster(String clusterId, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/gender_distribution/" + encode(clusterId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getCustomerDetails(String customerId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/customer_details/" + encode(customerId);
		return getDataFromServer(url);
	}

	@Override
	public String getCustomerPercentageForCluster(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/2_3/customer_percentage_for_cluster/"
				+ encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getClusterFallingOnCriteria(String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/cluster_stats";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String cardDurationDistribution(String clusterId, String jsonString) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_3/card_duration_distribution/" + encode(clusterId);
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getClusterSizeById(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_2/cluster_stats/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getAverageExpPerCardForCustomer(String customerId, String cardId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/avg_expenditure_per_card/" + encode(customerId)
				+ "/" + encode(cardId);
		return getDataFromServer(url);
	}

	@Override
	public String getAverageExpPerMerchantForCustomer(String customerId, String merchantId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/avg_expenditure_per_merchant/" + encode(customerId)
				+ "/" + encode(merchantId);
		return getDataFromServer(url);
	}

	@Override
	public String getAvgExpPerMerchantTypeForSpecificCustomer(String customerId, String merchantType) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/avg_expenditure_per_merchant_type/"
				+ encode(customerId) + "/" + encode(merchantType);
		return getDataFromServer(url);
	}

	@Override
	public String getEventAcitivityCodeDistribution(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/event_acitivity_code/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getGroupLevelDistribution(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/group_level_distribution/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getTrackTwoLevel(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/track_2_mark_distribution/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getGraphDataByCustomerIdForFirst1000Transactions(String customerId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/graph/" + encode(customerId);
		return getDataFromServer(url);
	}

	@Override
	public String getGraphDataByCustomerIdForLimit(String customerId, Integer limit) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/1_4/graph/" + encode(customerId);
		return getDataFromServer(url);
	}

	@Override
	public String clubStatsWithDistributionForCluster(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc1/2_3/club_stats/" + encode(clusterId);
		return getDataFromServer(url);
	}

	// uc2
	@Override
	public String getClusterSizeForUC2() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_2/cluster_stats";
		return getDataFromServer(url);
	}

	@Override
	public String getClusterFallingOnCriteriaForUC2(String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_2/cluster_stats";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getClusterSizeByIdForUC2(String clusterId,String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_2/cluster_stats/" + encode(clusterId);
		return postStringDataToServer(url,criteria);
	}

	@Override
	public String getOverAllExpenditureAvgAndSumForUC2() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_2/overall_stats";
		return getDataFromServer(url);
	}

	@Override
	public String getAgeDestributionCulsterForUC2(String clusterId, String criteria, String buckets) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/age_distribution/" + encode(clusterId) + "/"
				+ encode(buckets);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getAsAndBsDistributionForUC2(String clusterId, String bucketCountAs, String criteria,
			String bucketCountBs) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/as_bs_distribution/" + encode(clusterId) + "/"
				+ encode(bucketCountAs) + "/" + encode(bucketCountBs);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getAverageExpenditureTrendsForUC2(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/average_expenditure_trends/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String bankingCardDistributionForUC2(String clusterId, String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/banking_card_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String cardDurationDistributionForUC2(String clusterId, String criteria, String buckets) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/card_duration_distribution/" + encode(clusterId)
				+ "/" + encode(buckets);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getCityDistributionForUC2(String clusterId, String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/city_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getGenderDistributionForClusterForUC2(String clusterId, String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/gender_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String issuedByDistributionForUC2(String clusterId, String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/issued_by_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getAverageExpPerCardForCustomerForUC2(String customerId, String cardId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/avg_expenditure_per_card/" + encode(customerId)
				+ "/" + encode(cardId);
		return getDataFromServer(url);
	}

	@Override
	public String getCustomerDetailsForUC2(String customerId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/customer_details/" + encode(customerId);
		return getDataFromServer(url);
	}

	@Override
	public String clubStatsWithDistributionForClusterForUC2(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_3/club_stats/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getEventAcitivityCodeDistributionForUC2(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/event_acitivity_code/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getTrackTwoLevelForUC2(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/track_2_mark_distribution/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getGroupLevelDistributionForUC2(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/group_level_distribution/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getClubConnectionStatsForUC2(String clusterId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/club_connection_stats/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getClubConnectionsForUC2(String clusterId, String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/club_connections/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getStatsOfClubConnectionForUC2(String clusterId, String firstClubName, String secondClubName,
			String criteria) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/club_connection_stats/" + encode(clusterId) + "/"
				+ encode(firstClubName) + "/" + encode(secondClubName);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getGraphDataByCardIdForUC2(String clusterId, String clubName, String graphType) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/isracard/api/uc2/2_4/graph/" + encode(clusterId) + "/"
				+ encode(clubName) + "/" + encode(graphType);
		return getDataFromServer(url);
	}

	// uc3
	@Override
	public String getClusterSizeForUC3() throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/cluster_stats";
		return getDataFromServer(url);
	}

	@Override
	public String getClusterFallingOnCriteriaForUC3(String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/cluster_stats";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getClusterSizeByIdForUC3(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/cluster_stats/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getOverAllExpenditureAvgAndSumForUC3() throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/overall_stats";
		return getDataFromServer(url);
	}

	@Override
	public String getAgeDestributionCulsterForUC3(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/age_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getAsAndBsDistributionForUC3(String clusterId, String buckets, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/as_bs_distribution/" + encode(clusterId) + "/"
				+ encode(buckets);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getAverageExpenditureTrendsForUC3(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/average_expenditure_trends/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String bankingCardDistributionForUC3(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/banking_card_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String cardDurationDistributionForUC3(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/card_duration_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getCityDistributionForUC3(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/city_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String clubStatsWithDistributionForClusterForUC3(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/club_stats/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getGenderDistributionForClusterForUC3(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/gender_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String issuedByDistributionForUC3(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_3/issued_by_distribution/" + encode(clusterId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getStatsOfClubConnectionForUC3(String clusterId, String firstClubName, String secondClubName,
			String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/club_connection_stats/" + encode(clusterId) + "/"
				+ encode(firstClubName) + "/" + encode(secondClubName);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getClubConnectionsForUC3(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/club_connections/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getCustomerDetailsForUC3(String customerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/customer_details/" + encode(customerId);
		return getDataFromServer(url);
	}

	@Override
	public String getEventAcitivityCodeDistributionForUC3(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/event_acitivity_code/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getVLAGraphForUC3(String clusterId, String clubName, String graphType) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/graph/" + encode(clusterId) + "/"
				+ encode(clubName) + "/" + encode(graphType);
		return getDataFromServer(url);
	}

	@Override
	public String getGroupLevelDistributionForUC3(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/group_level_distribution/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getTrackThreeLevelForUC2(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/track_3_mark_distribution/" + encode(clusterId);
		return getDataFromServer(url);
	}

	@Override
	public String getStories() throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_2/stories";
		return getDataFromServer(url);
	}

	@Override
	public String getClusterForUc1WithStory(String criteria, String storyId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_2/clusters/" + storyId;
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getDistribution(String jsonToSent, String storyId, String clusterId, String attributeName,
			String attributeType, String distributionCount) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_2/distributions/" + storyId + "/"
				+ clusterId + "/" + attributeName + "/" + attributeType + "/" + distributionCount;
		return postStringDataToServer(url, jsonToSent);
	}

	@Override
	public String getCustomersForUc1WithStory(String criteria, String storyId, String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_2/customers/" + storyId + "/"
				+ clusterId;
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getVLAByCustomerId(String customerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_4/graph/" + customerId;
		return getDataFromServer(url);
	}

	@Override
	public String getCustomerDetailsById(String customerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_4/customer_details/" + customerId;
		return getDataFromServer(url);
	}

	@Override
	public String getClusterByStoryIdAndClusterId(String storyId, String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_2/cluster/" + storyId + "/" + clusterId;
		return getDataFromServer(url);
	}

	@Override
	public String getCustomerStoryClustersById(String customerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_4/customer_stories_clusters/"
				+ customerId;
		return getDataFromServer(url);
	}

	@Override
	public String getUC3ClustersById(String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/cluster/" + clusterId;
		return getDataFromServer(url);
	}

	@Override
	public String getUC3ClustersByQuery(String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "/isracard/api/uc3/3_2/clusters";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getUC3CustomersByClusterId(String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/customers/" + clusterId;
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getUC3DistributionById(String clusterId, String attributeName, String attributeType,
			String distributionCount, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_2/distributions/" + clusterId + "/" + attributeName
				+ "/" + attributeType + "/" + distributionCount;
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getCustomerStoryClusterForUC3(String customerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/customer_stories_clusters/" + customerId;
		return getDataFromServer(url);
	}

	@Override
	public String getVLAGraphByIDForUC3(String customerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/graph/" + customerId;
		return getDataFromServer(url);
	}

	@Override
	public String getNamingById(String storyId, String clusterId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/naming/getClusterName/" + storyId + "/" + clusterId;
		return getDataFromServer(url);
	}

	@Override
	public String getStoryNameById(String storyId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/naming/getStoryName/" + storyId;
		return getDataFromServer(url);
	}

	@Override
	public String setclusterNamingById(String storyId, String clusterId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/naming/setClusterName/" + storyId + "/" + clusterId;
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String setStoryNameById(String storyId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/naming/setStoryName/" + storyId;
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getAllStories() throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/stories";
		return getDataFromServer(url);
	}

	@Override
	public String addANewStory(String criteria, String sotryName, Long userId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/stories";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, criteria);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				eventPublisher.publishEvent(
						new LoggingEvent(response, LoggingEventType.ADD_STORY, userId, new Date(), null, sotryName));
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
		// return postStringDataToServer(url, criteria);
	}

	@Override
	public String deleteAllStories() throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/stories";
		return deleteDataInServer(url);
	}

	@Override
	public String getStoriesById(String storyId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/stories/" + encode(storyId);
		return getDataFromServer(url);
	}

	@Override
	public String UpdateStoryById(String storyId, String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/stories/" + encode(storyId);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String deleteStoryById(String storyId, String sotryName, Long userId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/stories/" + encode(storyId);
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				eventPublisher.publishEvent(
						new LoggingEvent(response, LoggingEventType.DELETE_STORY, userId, new Date(), null, sotryName));
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
		// return deleteDataInServer(url);
	}

	@Override
	public String getAvaliableAttributesForUC4() throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc4/available_attributes";
		return getDataFromServer(url);
	}

	@Override
	public String getUC4CustomersByFilters(String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc4/customers";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getDistributionsForUC4(String attributeName, String attributeType, String distributionCount,
			String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc4/distributions/" + encode(attributeName) + "/"
				+ encode(attributeType) + "/" + encode(distributionCount);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getCustomerCountForUC4(String jsonString) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc4/customers_count";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getUC4PopularMerchantTypes(String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc4/popular_merchant_types";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getUC4TopEarlyAdaptors(String criteria) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc4/top_early_adapters";
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String getClubConnectionsForUC2ByNameAndId(String clusterId, String criteria, String clubName) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc2/2_4/club_connection_stats/" + encode(clusterId) + "/"
				+ encode(clubName);
		return postStringDataToServer(url, criteria);
	}

	@Override
	public String searchCustomerByIdAndStoryId(String storyId,String clusterId, String id) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc1_with_story/1_4/search_customer/"+encode(storyId)+"/"+encode(clusterId)+"/"+encode(id);
		return getDataFromServer(url);
	}

	@Override
	public String getUC3CustomerById(String clusterId,String ownerId) throws Exception {
		String url = BIGDATA_ISRACARD_UC3_URL + "isracard/api/uc3/3_4/search_customer/"+encode(clusterId)+"/"+encode(ownerId);
		return getDataFromServer(url);
	}

}
