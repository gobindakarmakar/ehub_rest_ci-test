package element.bst.elementexploration.rest.extention.sourcecredibility.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataObjectDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("source_name")
	private String sourceName;

	@JsonProperty("jurisdictions")
	private List<String> jurisdictions;

	@JsonProperty("api_base_url")
	private String apiBaseUrl;

	@JsonProperty("category")
	private String category;

	@JsonProperty("display_name")
	private String displayName;

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public List<String> getJurisdictions() {
		return jurisdictions;
	}

	public void setJurisdictions(List<String> jurisdictions) {
		this.jurisdictions = jurisdictions;
	}

	public String getApiBaseUrl() {
		return apiBaseUrl;
	}

	public void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
