package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregateVo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonReturnObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertResponseSendDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatisticsRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.GraphResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface AlertService extends GenericService<Alert, Long>{
	
	List<AlertResponseSendDto> fetchAlerts(MultipartFile file,long userId) throws IOException, ParseException,IllegalAccessException, InvocationTargetException  ;

	List<AlertResponseSendDto> fetchNonResolvedAlerts(Integer pageNumber,Integer recordsPerPage);
	
	boolean resolveAlert(Long alertId,String comment);
	
	AlertResponseSendDto fetchAlertSummary(long alertId);
	
	AlertStatisticsRespDto alertStatistics(Long entityId)  throws IllegalAccessException, InvocationTargetException ;
	
	int countTotalAlerts(Long userId);
	
	public List<AlertResponseSendDto> fetchfilterName(String name, String datefrom, String dateto, Integer from,Integer size) throws ParseException;
	
	public GraphResponseDto generateGraph(Long alertId,String transactionType,String locations,String minAmount);

	void createAlert(List<TransactionsData> transactionsList, long userId);
	
	void createAlert(TransactionsData transactionsData,long userId);
	public int fetchfilterNameCount(String name, String dateFrom, String dateTo, Integer pageNumber,Integer recordsPerPage);
	
	

	public GraphResponseDto expandCustomerNode(String customerId, Long alertId,String transactionType,String locations,String minAmount);
		
	public List<?> fetchAlertsBwDatesOrderBy(String name, String dateFrom, String dateTo,
			Integer pageNumber, Integer recordsPerPage, String orderBy) throws ParseException;
	
	
	public List<AlertStatusDto> fetchGroupByAlertStatus(String fromDate,String toDate,boolean periodBy,FilterDto filterDto) throws ParseException;
	
	public List<Alert> calculateAlertsGenRatio(List<Long> txIds,String fromDate,String toDate) throws ParseException ;
	//public List<?> fetchAlertsBwDatesList(String datefrom, String dateto) throws ParseException;
	
	public MonthlyTurnOverDto getMonthlyTurnOver(String fromDate,String toDate,FilterDto filterDto) throws ParseException ;
	public Map<String,AlertComparisonReturnObject> alertComparisonNotification(String fromDate,String toDate,FilterDto filterDto)throws ParseException;
	public List<AlertScenarioDto> amlTopScenarios(String fromDate,String toDate,FilterDto filterDto)  throws ParseException ;
	public AlertByPeriodDto amlAlertByPeriod(String fromDate,String toDate,FilterDto filterDto) throws ParseException;
	
	public AlertScenarioDto getAlertScenarioById(long id);

	List<AlertResponseSendDto> getAlertsForCustomer(String fromDate, String toDate, String customerNumber);

	List<AlertsDto> searchAlerts(String keyword);

	Long searchAlertsCount(String keyword);

	List<AlertAggregator> alertsAggregates(AlertAggregateVo alertAggregateVo, Long userId);
	
	void deleteAlerts(Long id) throws Exception ;
}
