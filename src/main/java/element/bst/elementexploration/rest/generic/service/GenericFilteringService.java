package element.bst.elementexploration.rest.generic.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.dto.GenericFilterDto;

public interface GenericFilteringService {
	
	List<?> getFilterDataForList(GenericFilterDto dto);
	
	<E> void getSingle(GenericFilterDto dto);

}
