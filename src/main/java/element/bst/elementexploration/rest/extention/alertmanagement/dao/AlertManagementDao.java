package element.bst.elementexploration.rest.extention.alertmanagement.dao;

import java.util.List;
import java.util.Set;

import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.PastDueMonthCountDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.StatusCountDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Prateek
 *
 */

public interface AlertManagementDao extends GenericDao<Alerts, Long>{

	List<Alerts> getUserAlertByUserId(Long userId);

	List<Alerts> getAlerts(Integer pageNumber, Integer recordsPerPage, String orderIn, String orderBy, Long feedId);

	Long getAlertsCount();

	List<StatusCountDto> getMyAlerts(Long userId);

	List<StatusCountDto> getAlertsOfGroups(Set<Long> groupIds);

	List<PastDueMonthCountDto> getAlertsGroupByMonth(List<Long> filteredAlerts);

	List<Alerts> getAlertsByFeed(Long feedID);

	boolean deleteComment(Long alertId, Long commentId, long userId);

	List<Alerts> findAllWithRisk();
	
	List<Alerts> findAlertsByCustomerId(String customeId);

	Alerts findBydetailId(Long detailId);

	List<Long> getAlertIdsByFeedIds(List<Long> comingIdList);

	List<Alerts> getAlertsByAlertIdList(List<Long> alertIdList);

}
