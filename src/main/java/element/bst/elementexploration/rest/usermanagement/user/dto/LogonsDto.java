package element.bst.elementexploration.rest.usermanagement.user.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserRolesDtoWithRolesObj;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Prateek Maurya
 *
 */

@ApiModel("Logons Model")
public class LogonsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "user id", required = false)
	private Long userId;
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@ApiModelProperty(value = "user full name", required = false)
	private String fullName;
	
	@ApiModelProperty(value = "user status", required = false)
	private ListItem status;
	
	@ApiModelProperty(value = "user roles", required = false)
	private List<UserRolesDtoWithRolesObj> roles;
	
	@ApiModelProperty(value = "Last Login", required = false)
	private Date loginTime;
	
	@ApiModelProperty(value = "no of logons", required = false)
	private Long logonsCount;
	
	@ApiModelProperty(value = "no of logon failures", required = false)
	private Long logonFailuresCount;
	
	@ApiModelProperty(value = "logon failure list", required = false)
	private List<LoginHistory> failedList;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public ListItem getStatus() {
		return status;
	}

	public void setStatus(ListItem status) {
		this.status = status;
	}

	public Long getLogonsCount() {
		return logonsCount;
	}

	public void setLogonsCount(Long logonsCount) {
		this.logonsCount = logonsCount;
	}

	public Long getLogonFailuresCount() {
		return logonFailuresCount;
	}

	public void setLogonFailuresCount(Long logonFailuresCount) {
		this.logonFailuresCount = logonFailuresCount;
	}

	public List<LoginHistory> getFailedList() {
		return failedList;
	}

	public void setFailedList(List<LoginHistory> failedList) {
		this.failedList = failedList;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public List<UserRolesDtoWithRolesObj> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRolesDtoWithRolesObj> roles) {
		this.roles = roles;
	}
	
	
}
