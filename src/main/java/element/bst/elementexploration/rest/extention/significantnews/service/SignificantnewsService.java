package element.bst.elementexploration.rest.extention.significantnews.service;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantnewsDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface SignificantnewsService extends GenericService<SignificantNews, Long> {
	
	public SignificantNews getSignificantNews(String entityId,String entityName,String publishedDate,String title,String url,String newsClass);
	
	public boolean deleteSignificantNews(SignificantnewsDto significantNewsDto,Long userId);
	
	public SignificantnewsDto saveSignificantNews(SignificantnewsDto significantnewsDto);

}
