package element.bst.elementexploration.rest.document.dao;

import java.util.List;

import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.document.domain.DocumentVaultCaseMapping;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * 
 * @author Viswanath
 *
 */
public interface DocumentCaseMppingDao extends GenericDao<DocumentVaultCaseMapping, Long>{
	
	List<CaseDto> getSeedByDocumentId(Long docId);

	DocumentVaultCaseMapping findByCaseIdAndDocId(Long docId, Long caseseedId);
}
