package element.bst.elementexploration.rest.extention.geoencoder.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.geoencoder.dto.GoogleResponse;
import element.bst.elementexploration.rest.extention.geoencoder.service.GeoEncoderService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "Geo Encoder API" })
@RestController
@RequestMapping("/api/geoEncoder")
public class GeoEncoderController extends BaseController {

	@Autowired
	GeoEncoderService geoEncoderService;

	@ApiOperation("Get latitude and longitude based on location")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getLatitudeAndLongitude", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getLatitudeAndLongitude(@RequestParam(required = true) String address,
			HttpServletRequest request, @RequestParam String token) throws Exception {

		List<GoogleResponse> googleResponses = geoEncoderService.getLatitudeAndLongitude(address);
		if (googleResponses.isEmpty()) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.INTERNET_CONNECTION), HttpStatus.OK);
		} /*
			 * else if (googleResponses.get(0).getLatitude().equals(ElementConstants.
			 * GET_LATITUDE_LONGITUDE_NOT_FOUND_MSG.toString())) { return new
			 * ResponseEntity<>(new
			 * ResponseMessage(ElementConstants.GET_LATITUDE_LONGITUDE_NOT_FOUND_MSG),
			 * HttpStatus.OK); }
			 */else {
			return new ResponseEntity<>(googleResponses, HttpStatus.OK);
		}
	}

	/*@ApiOperation("Save Countries master Data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveCountiesMasterData",consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> saveCountiesMasterData(@RequestParam MultipartFile jsonFile, HttpServletRequest request,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(geoEncoderService.saveCountiesMasterData(jsonFile), HttpStatus.OK);
	}*/
}
