package element.bst.elementexploration.test.logging.daoImpl;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.logging.daoImpl.AuditLogDaoImpl;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.enumType.GranularityEnum;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.util.LogRequest;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuditLogDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	AuditLogDaoImpl auditLogDao;

	List<AuditLog> logsList = new ArrayList<>();

	LogRequest logRequest = new LogRequest();

	List<LogAggregator> logAggregatorList = new ArrayList<LogAggregator>();

	@Before
	public void setupMock() {
		logsList.add(new AuditLog());
		logsList.add(new AuditLog());
		logRequest.setCaseId(1L);
		logRequest.setDateFrom(LocalDateTime.now());
		logRequest.setDateTo(LocalDateTime.now());
		logRequest.setDocId(1L);
		logRequest.setEntityId(1L);
		logRequest.setFlag(1);
		logRequest.setGranularity(GranularityEnum.daily);
		logRequest.setKeyword("test");
		logRequest.setLevel(LogLevels.INFO);
		logRequest.setOrderBy("createdOn");
		logRequest.setOrderIn("desc");
		logRequest.setUserId(1L);
		logRequest.setPageNumber(1);
		logRequest.setRecordsPerPage(10);
		logRequest.setType("test");
		logAggregatorList.add(new LogAggregator("yyyy-mm-dd", 2l));
		logAggregatorList.add(new LogAggregator("yyyy-dd-mm", 2l));
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getLogsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logsList);
		List<AuditLog> auditLogsList = auditLogDao.getLogs(logRequest);
		assertEquals(2, auditLogsList.size());
	}

	@Test
	public void getLogs_whenDatetoIsNullTest() {
		logRequest.setDateTo(null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logsList);
		List<AuditLog> auditLogsList = auditLogDao.getLogs(logRequest);
		assertEquals(2, auditLogsList.size());
	}

	@Test
	public void getLogs_whenDateFromIsNullTest() {
		logRequest.setDateFrom(null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logsList);
		List<AuditLog> auditLogsList = auditLogDao.getLogs(logRequest);
		assertEquals(2, auditLogsList.size());
	}

	@Test
	public void getLogs_whenOrderInIsNullTest() {
		logRequest.setOrderIn(null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logsList);
		List<AuditLog> auditLogsList = auditLogDao.getLogs(logRequest);
		assertEquals(2, auditLogsList.size());
	}

	@Test
	public void getLogs_whenOrderInIsNotInDescOrAscTest() {
		logRequest.setOrderIn("test");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logsList);
		List<AuditLog> auditLogsList = auditLogDao.getLogs(logRequest);
		assertEquals(2, auditLogsList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getLogs_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<AuditLog> auditLogsList = auditLogDao.getLogs(logRequest);
		assertEquals(0, auditLogsList.size());
	}

	@Test
	public void countLogsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logsList);
		Long count = auditLogDao.countLogs(logRequest);
		assertEquals(new Long("2"), count);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void countLogs_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		Long count = auditLogDao.countLogs(logRequest);
		assertEquals(new Long("0"), count);
	}

	@Test
	public void logAggregatorTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logAggregatorList);
		List<LogAggregator> logAggregatorListNew = auditLogDao.logAggregator(logRequest);
		assertEquals(2, logAggregatorListNew.size());
	}

	@Test
	public void logAggregator_whenGranularityIsMonthlyTest() {
		logRequest.setGranularity(GranularityEnum.monthly);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logAggregatorList);
		List<LogAggregator> logAggregatorListNew = auditLogDao.logAggregator(logRequest);
		assertEquals(2, logAggregatorListNew.size());
	}

	@Test
	public void logAggregator_whenGranularityIsWeeklyTest() {
		logRequest.setGranularity(GranularityEnum.weekly);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logAggregatorList);
		List<LogAggregator> logAggregatorListNew = auditLogDao.logAggregator(logRequest);
		assertEquals(2, logAggregatorListNew.size());
	}

	@Test
	public void logAggregator_whenGranularityIsYearlyTest() {
		logRequest.setGranularity(GranularityEnum.yearly);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logAggregatorList);
		List<LogAggregator> logAggregatorListNew = auditLogDao.logAggregator(logRequest);
		assertEquals(2, logAggregatorListNew.size());
	}

	@Test
	public void logAggregator_whenGranularityIsNullTest() {
		logRequest.setGranularity(null);
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(logAggregatorList);
		List<LogAggregator> logAggregatorListNew = auditLogDao.logAggregator(logRequest);
		assertEquals(2, logAggregatorListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void logAggregator_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		auditLogDao.logAggregator(logRequest);
	}

}
