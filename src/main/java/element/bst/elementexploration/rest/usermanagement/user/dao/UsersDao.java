package element.bst.elementexploration.rest.usermanagement.user.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.DateCountStatusDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.LoginUserCountDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.util.FilterColumnDto;

/**
 * @author Paul Jalagari
 *
 */
public interface UsersDao extends GenericDao<Users, Long> {

	List<UsersDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	List<UsersDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countUserList(Long groupId, String status);

	Long countUserListFullTextSearch(String keyword, String status, Long groupId);
	
	Users getUserByEmailOrUsername(String email);

	List<UsersDto> listUsersByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countUserByGroup(Long groupId, Integer userStatus);
	
	Users checkUserNameOrEmailExists(String email, Long id);

	List<Users> getUsersInList(List<Long> participants);
	
	public Users getAnalystByUserId(long analustId);

	public List<Users> getAllAnalystWhoDidNotRejectedCaseBefore(Long caseId);

	List<UsersDto> listUsersByGroupWithoutPagination(Long eachGroupId);

	List<Long> quickSearch(String searchKey, boolean isScreeNameRequired) throws Exception;

	List<Long> getUserIdByRolesList(List<Long> comingIdList);

	List<Long> getUserIdByGroupsList(List<Long> comingIdList);

	List<DateCountStatusDto> getCountByStatusWise() throws Exception;
	
	Long activeUsersCount();

	Map<Object, Long> getLogonsByHourCount(String period, Date fromDate, Date toDate);

	Map<String, Long> getTopTenLogonFailuresUsers(String period, Date fromDate, Date toDate);

	Map<Object, Long> getFailedLogonsCount(String period, Date fromDate, Date toDate);

	List<Users> getUsersByUsernames(List<String> usernamesList);

	List<Users> getUsersByIds(List<Long> userList);
	Map<List<LoginUserCountDto>, Long> userFilterData(Users userClass, List<FilterColumnDto> filterColumnDto, Integer pageNumber,
			Integer recordsPerPage, Long totalResults, String orderIn, String orderBy);

	Users getFirebaseUserBySourceId(String sourceId);

}
