package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceJurisdictionDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceJurisdictionDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.AlertServiceImpl;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.EntityConverter;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Service("sourceJurisdictionService")
public class SourceJurisdictionServiceImpl extends GenericServiceImpl<SourceJurisdiction, Long>
		implements SourceJurisdictionService {

	@Autowired
	public SourceJurisdictionServiceImpl(GenericDao<SourceJurisdiction, Long> genericDao) {
		super(genericDao);
	}

	public SourceJurisdictionServiceImpl() {
	}

	@Autowired
	SourceJurisdictionDao sourceJurisdictionDao;

	@Override
	@Transactional("transactionManager")
	public List<SourceJurisdictionDto> saveSourceJurisdiction(SourceJurisdictionDto jurisdictionDto) throws Exception {
		SourceJurisdiction sourceJurisdictionFromDB = sourceJurisdictionDao
				.fetchJurisdiction(jurisdictionDto.getJurisdictionName());
		if (sourceJurisdictionFromDB == null) {
			SourceJurisdiction sourceJurisdiction = new SourceJurisdiction();
			sourceJurisdiction.setJurisdictionName(jurisdictionDto.getJurisdictionName());
			sourceJurisdiction.setJurisdictionOriginalName(jurisdictionDto.getJurisdictionOriginalName());
			sourceJurisdiction.setSelected(jurisdictionDto.getSelected());
			sourceJurisdictionDao.create(sourceJurisdiction);
			return getSourceJurisdiction();
		} else
			throw new Exception(ElementConstants.JURISDICTION_ADD_FAILED);
	}

	@Override
	@Transactional("transactionManager")
	public List<SourceJurisdictionDto> getSourceJurisdiction() {
		List<SourceJurisdiction> jurisdictionList = sourceJurisdictionDao.findAll();
		List<SourceJurisdictionDto> sourcesDtoList = new ArrayList<SourceJurisdictionDto>();
		if (jurisdictionList != null) {
			Set<SourceJurisdiction> jurisdictionSet = jurisdictionList.stream()
	                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(SourceJurisdiction::getJurisdictionName))));
			
			sourcesDtoList = jurisdictionSet.stream().distinct()
					.map((sourcesDto) -> new EntityConverter<SourceJurisdiction, SourceJurisdictionDto>(
							SourceJurisdiction.class, SourceJurisdictionDto.class).toT2(sourcesDto,
									new SourceJurisdictionDto())).filter(s-> s.getJurisdictionOriginalName()!=null)
					.collect(Collectors.toList());
		}
		return sourcesDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public SourceJurisdiction fetchJurisdiction(String name) {
		return sourceJurisdictionDao.fetchJurisdiction(name);
	}

	@Override
	@Transactional("transactionManager")
	public SourceJurisdiction create(SourceJurisdiction jurisdictionNew) {
		return sourceJurisdictionDao.create(jurisdictionNew);
	}

	@Override
	@Transactional("transactionManager")
	public SourceJurisdiction findByListItemId(Long listItemId) {
		return sourceJurisdictionDao.findByListItemId(listItemId);
	}
	
	@Override
	public int updateDBCollation(String schemaName) {
		return sourceJurisdictionDao.updateDBCollation(schemaName);
	}
	
	@Override
	public List<String> getNonUTF8Tables(String schemaName) {
		return sourceJurisdictionDao.getNonUTF8Tables(schemaName);
	}
	
	@Override
	public int updateTableCollation(String tableName) {
		return sourceJurisdictionDao.updateTableCollation(tableName);
	}
	
	@Override
	public int updateTables(String dbUrl) {
		int updatedCount = 0;
		try {
		dbUrl = dbUrl.substring(0,dbUrl.indexOf('?'));
		String schemaName = dbUrl.substring(dbUrl.lastIndexOf('/')+1);
			updateDBCollation(schemaName);
			List<String> tableList = getNonUTF8Tables(schemaName);
			ElementLogger.log(ElementLoggerLevel.INFO,"Non UTF8 tables :"+tableList.size()+" for db:"+schemaName, SourceJurisdictionServiceImpl.class);
			
			if(tableList!=null && tableList.size()!=0 ) {
				Iterator<String> itr = tableList.iterator();
				while(itr.hasNext()) {
					int count = updateTableCollation(itr.next());
					updatedCount= updatedCount+count;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return updatedCount;
	}
	
}
