package element.bst.elementexploration.rest.extention.significantnews.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author rambabu
 *
 */
@Entity
@Table(name = "bst_significant_comment")
public class SignificantComment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "comment", columnDefinition = "LONGTEXT")
	@NotEmpty(message = "comment can not be blank.")
	private String comment;

	@CreationTimestamp
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "classification")
	@NotEmpty(message = "classification can not be blank.")
	private String classification;

	@Column(name = "isSignificant")
	private Boolean isSignificant;

	@Column(name = "significant_id")
	private Long significantId;

	public SignificantComment() {
		super();
	}

	public SignificantComment(Long id, String comment, Date createdDate, Long userId, String classification,
			Boolean isSignificant, Long significantId) {
		super();
		this.id = id;
		this.comment = comment;
		this.createdDate = createdDate;
		this.userId = userId;
		this.classification = classification;
		this.isSignificant = isSignificant;
		this.significantId = significantId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public Boolean getIsSignificant() {
		return isSignificant;
	}

	public void setIsSignificant(Boolean isSignificant) {
		this.isSignificant = isSignificant;
	}

	public Long getSignificantId() {
		return significantId;
	}

	public void setSignificantId(Long significantId) {
		this.significantId = significantId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
