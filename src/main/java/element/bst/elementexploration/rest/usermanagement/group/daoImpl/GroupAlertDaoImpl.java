package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupAlertDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("groupAlertDao")
public class GroupAlertDaoImpl extends GenericDaoImpl<GroupAlert, Long> implements GroupAlertDao {

	public GroupAlertDaoImpl() {
		super(GroupAlert.class);
	}

	@Override
	public void removeExistingGroupAlertSettings(Long groupId, String type) {
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("delete from GroupAlert ga where ga.groupId.id = :groupId");
			if ((type != null) && (type.equalsIgnoreCase(ElementConstants.FEED_CLASSIFICATION_TYPE)
					|| type.equalsIgnoreCase(ElementConstants.JURISDICTION_TYPE_STRING)))
				queryBuilder.append(" and ga.type = :type");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("groupId", groupId);
			if ((type != null) && (type.equalsIgnoreCase(ElementConstants.FEED_CLASSIFICATION_TYPE)
					|| type.equalsIgnoreCase(ElementConstants.JURISDICTION_TYPE_STRING)))
				query.setParameter("type", type);
			query.executeUpdate();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupAlert> findGroupAlertsByGroup(Long groupId) {
		List<GroupAlert> groupAlertsList = new ArrayList<>();
		try {
			String hql = "FROM GroupAlert as ga WHERE ga.groupId.id = :groupId";
			Query<?> query = this.getCurrentSession().createQuery(hql);
			query.setParameter("groupId", groupId);
			groupAlertsList = (List<GroupAlert>) query.getResultList();
		} catch (NoResultException e) {
			groupAlertsList = Collections.<GroupAlert>emptyList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Get UserGroup", e, this.getClass());

		}
		return groupAlertsList;
	}

	@Override
	public List<GroupAlert> getGroupAlertsByGroupIdList(List<Long> groupIds) {
		List<GroupAlert> groupAlertsList = new ArrayList<>();
		try {
			String hql = "FROM GroupAlert as ga WHERE ga.groupId.id in(:groupId)";
			Query<?> query = this.getCurrentSession().createQuery(hql);
			query.setParameter("groupId", groupIds);
			groupAlertsList = (List<GroupAlert>) query.getResultList();
		} catch (NoResultException e) {
			groupAlertsList = Collections.<GroupAlert>emptyList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Get UserGroup", e, this.getClass());

		}
		return groupAlertsList;

	}
}
