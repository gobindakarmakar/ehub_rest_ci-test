package element.bst.elementexploration.rest.extention.significantnews.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface SignificantCommentDao extends GenericDao<SignificantComment, Long>{

	List<SignificantComment> getSignificantComments(String classification, Long significantId);

}
