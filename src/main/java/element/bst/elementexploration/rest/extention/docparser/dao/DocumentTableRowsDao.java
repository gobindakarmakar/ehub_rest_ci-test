package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTableRows;
import element.bst.elementexploration.rest.extention.docparser.dto.DocumentTableRowsDto;
import element.bst.elementexploration.rest.extention.docparser.dto.TableRowDataDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentTableRowsDao extends GenericDao<DocumentTableRows, Long>{

	public List<TableRowDataDto> getAllRowsFromTable(Long tableId);

	public List<DocumentTableRowsDto> getAllRowsDataByColumnId(Long columnId);
}
