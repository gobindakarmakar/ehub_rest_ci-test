package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.RolesDto;

/**
 * @author Paul Jalagari
 *
 */
public interface RolesService extends GenericService<Roles, Long>{

	Roles findRole(Roles role) throws Exception;

	List<Roles> getRolesOfUser(Long userId) throws Exception;

	Long getRolesCount();

	boolean deleteRole(Long roleId, Long userId);

	List<RolesDto> getAllRoles() throws Exception;

	Roles findRoleByName(String roleName)throws Exception;

	RolesDto getRoleByIdOrName(Long roleId, String roleName);

	Object getStatuswiseUsersByRoleId(Long roleId);

	boolean assignUsersToRole(Long roleId, List<Long> userList) ;
	
    void deleteGroupPermissionByRoleId(Long roleId);
	

}
