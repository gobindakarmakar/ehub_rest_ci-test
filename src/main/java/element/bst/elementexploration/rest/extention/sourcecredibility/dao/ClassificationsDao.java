package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface ClassificationsDao extends GenericDao<Classifications, Long> {

	Classifications fetchClassification(String name);

}
