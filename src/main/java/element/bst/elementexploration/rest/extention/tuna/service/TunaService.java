package element.bst.elementexploration.rest.extention.tuna.service;

public interface TunaService {

	String getCompaniesHouse(String id) throws Exception;

	String getFilingHistory(String company_number) throws Exception;

	String getCompanyOfficers(String company_number) throws Exception;

	String searchCompanies(String q) throws Exception;

	/*String getOrgSanctions(String identifier) throws Exception;*/

	String getInfoOps(String identifier, String orgOrPerson, String filter) throws Exception;

	String getFinanceStatement(String source, String query, String exchange, String apiKey) throws Exception;

	String getSignificantPersonsWithControl(String company_number) throws Exception;

}
