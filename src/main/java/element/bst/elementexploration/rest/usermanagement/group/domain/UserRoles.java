package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_um_user_roles")
public class UserRoles implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "User Role ID")
	@Id
	@Column(name = "userRoleId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userRoleId;

	@ApiModelProperty(value = "Role ID")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId")
	private Roles roleId;

	@ApiModelProperty(value = "User Role Description")
	@Column(name = "description", columnDefinition = "LONGTEXT")
	private String description;

	@ApiModelProperty(value = "Role ID")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private Users userId;

	
	
	public UserRoles() {
		super();
	}

	public UserRoles(Roles roleId, Users userId) {
		super();
		this.roleId = roleId;
		this.userId = userId;
	}

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public Users getUserId() {
		return userId;
	}

	public void setUserId(Users userId) {
		this.userId = userId;
	}

	public Roles getRoleId() {
		return roleId;
	}

	public void setRoleId(Roles roleId) {
		this.roleId = roleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
