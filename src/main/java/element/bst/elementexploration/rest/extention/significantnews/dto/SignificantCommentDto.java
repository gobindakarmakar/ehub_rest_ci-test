package element.bst.elementexploration.rest.extention.significantnews.dto;

import java.io.Serializable;
import java.util.Date;

public class SignificantCommentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String comment;
	private Date createdDate;
	private Long userId;
	private String classification;
	private Boolean isSignificant;
	private Long significantId;
	private String userName;

	public SignificantCommentDto() {
		super();
	}

	public SignificantCommentDto(Long id, String comment, Date createdDate, Long userId, String classification,
			Boolean isSignificant, Long significantId, String userName) {
		super();
		this.id = id;
		this.comment = comment;
		this.createdDate = createdDate;
		this.userId = userId;
		this.classification = classification;
		this.isSignificant = isSignificant;
		this.significantId = significantId;
		this.userName = userName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public Boolean getIsSignificant() {
		return isSignificant;
	}

	public void setIsSignificant(Boolean isSignificant) {
		this.isSignificant = isSignificant;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getSignificantId() {
		return significantId;
	}

	public void setSignificantId(Long significantId) {
		this.significantId = significantId;
	}

}
