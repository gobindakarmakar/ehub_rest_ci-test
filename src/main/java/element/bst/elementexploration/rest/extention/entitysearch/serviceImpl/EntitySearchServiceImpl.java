package element.bst.elementexploration.rest.extention.entitysearch.serviceImpl;

import java.io.File;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.adversenews.service.AdverseNewsService;
import element.bst.elementexploration.rest.extention.entitysearch.domain.Classification;
import element.bst.elementexploration.rest.extention.entitysearch.domain.Keywords;
import element.bst.elementexploration.rest.extention.entitysearch.dto.AdverseNews;
import element.bst.elementexploration.rest.extention.entitysearch.dto.AnnualReturnResponseDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.ArticleDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.BingEntityDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.BingNews;
import element.bst.elementexploration.rest.extention.entitysearch.dto.BingSearchNewsDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.EntityDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.EntitySearchAggregatesDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.HitsDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.LocationDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.PeerDataDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.PeerResult;
import element.bst.elementexploration.rest.extention.entitysearch.dto.ShareholdingDto;
import element.bst.elementexploration.rest.extention.entitysearch.dto.TechnologiesDto;
import element.bst.elementexploration.rest.extention.entitysearch.service.ClassificationService;
import element.bst.elementexploration.rest.extention.entitysearch.service.EntitySearchService;
import element.bst.elementexploration.rest.extention.entityvisualizer.service.EntityVisualizerService;
import element.bst.elementexploration.rest.extention.riskscore.service.RiskScoreService;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantArticles;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantArticlesService;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantnewsService;
import element.bst.elementexploration.rest.extention.textsentiment.domain.TextSentiment;
import element.bst.elementexploration.rest.extention.textsentiment.service.TextSentimentService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;

/**
 * @author Paul Jalagari
 *
 */
@Service("entitySearchService")
public class EntitySearchServiceImpl implements EntitySearchService {

	@Value("${bigdata_org_url}")
	private String BIG_DATA__ORG_URL;

	@Autowired
	TextSentimentService textSentimentService;

	@Value("${entity_socket_url}")
	private String ENTITY_SOCKET_URL;

	@Value("${microsoft_cognitive_url}")
	private String MICROSOFT_COGNITIVE_URL;

	@Value("${bigdata_industry_url}")
	private String BIGDATA_INDUSTRY_URL;

	@Value("${tesseract_language}")
	private String TESSRACT_LANGUAGE;

	@Value("${imageAllocationArea}")
	private String TESSRACT_IMAGE_ALLOCATION;

	@Autowired
	private SignificantnewsService significantnewsService;

	@Autowired
	private AdverseNewsService adverseNewsService;
	/*
	 * @Autowired private CaseService caseService;
	 * 
	 * @Autowired private SearchHistoryService searchHistoryService;
	 * 
	 * private SearchHistoryService searchHistoryService;
	 */

	@Autowired
	private EntityVisualizerService entityVisualizerService;

	@Autowired
	private ClassificationService classificationService;

	@Autowired
	private RiskScoreService riskScoreService;

	@Lazy
	@Autowired
	private AdvanceSearchService advanceSearchService;

	@Autowired
	private DocumentService documentService;

	@Autowired
	private SignificantArticlesService significantArticlesService;

	@Override
	public String searchOrg(String jsonString)
			throws Exception {/*
								 * 
								 * String url = BIG_DATA__ORG_URL +
								 * "entity/org/search"; boolean flag = false; if
								 * (query != null) { url = url + "?q=" +
								 * URLEncoder.encode(query,
								 * "UTF-8").replaceAll("\\+", "%20"); flag =
								 * true; } if (size != null && flag) { url = url
								 * + "&size=" + size;
								 * 
								 * } else if (size != null) { url = url +
								 * "?size=" + size; flag = true; } if (from !=
								 * null && flag) { url = url + "&from=" + from;
								 * } else if (from != null && !flag) { url = url
								 * + "?from=" + from; } if (sort != null &&
								 * flag) { url = url + "&sort=" + sort; } else
								 * if (sort != null && !flag) { url = url +
								 * "?sort=" + sort; } return
								 * getDataFromServer(url);
								 */

		String url = BIG_DATA__ORG_URL + "entity/org/search";

		return postStringDataToServer(url, jsonString);

	}

	@Override
	public String searchPerson(String query, Long size, Long from) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/search";
		boolean flag = false;
		if (query != null) {
			url = url + "?q=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		if (size != null && flag) {
			url = url + "&size=" + size;

		} else if (size != null) {
			url = url + "?size=" + size;
			flag = true;
		}
		if (from != null && flag) {
			url = url + "&from=" + from;
		} else if (from != null && !flag) {
			url = url + "?from=" + from;
		}
		return getDataFromServer(url);
	}

	/*
	 * @Override public String entityLists(String entityType, String entityId)
	 * throws Exception { String url = BIG_DATA_ENTITY_LIST_URL + "lists/" +
	 * entityType + "/" + entityId; return getDataFromServer(url); }
	 */

	@Override
	public String getOrganizationOrCreateOrganizations(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/org/get-or-create";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getPersonOrCreatePerson(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/get-or-create";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getOrganizationBySuggestedName(String query) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/org/suggest-name";
		if (query != null)
			url = url + "?q=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		return getDataFromServer(url);
	}

	private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getPersonBySuggestedName(String query) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/suggest-name";
		if (query != null)
			url = url + "?q=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		return getDataFromServer(url);
	}

	@Override
	public String storePersonAttributeInEntity(String identifier, String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/" + identifier + "/attr";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getOrganizationByIdentifier(String identifier) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/org/" + identifier;
		return getDataFromServer(url);
	}

	@Override
	public String getPersonByIdentifier(String identifier) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/" + identifier;
		return getDataFromServer(url);
	}

	@Override
	public String storeOrganizationAttributeInEntity(String identifier, String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/org/" + identifier + "/attr";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String searchPerson(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/search";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getCognitiveMicrosoft(String q, String count, String offset, String mkt, String safeSearch)
			throws Exception {
		String url = MICROSOFT_COGNITIVE_URL;
		boolean flag = false;
		if (q != null) {
			url = url + "?q=" + URLEncoder.encode(q, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		if (count != null && flag) {
			url = url + "&count=" + URLEncoder.encode(count, "UTF-8").replaceAll("\\+", "%20");

		} else if (count != null) {
			url = url + "?count=" + URLEncoder.encode(count, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		if (offset != null && flag) {
			url = url + "&offset=" + URLEncoder.encode(offset, "UTF-8").replaceAll("\\+", "%20");
		} else if (offset != null && !flag) {
			url = url + "?offset=" + URLEncoder.encode(offset, "UTF-8").replaceAll("\\+", "%20");
		}
		if (mkt != null && flag) {
			url = url + "&mkt=" + URLEncoder.encode(mkt, "UTF-8").replaceAll("\\+", "%20");
		} else if (mkt != null && !flag) {
			url = url + "?mkt=" + URLEncoder.encode(mkt, "UTF-8").replaceAll("\\+", "%20");
		}
		if (safeSearch != null && flag) {
			url = url + "&safeSearch=" + URLEncoder.encode(safeSearch, "UTF-8").replaceAll("\\+", "%20");
		} else if (safeSearch != null && !flag) {
			url = url + "?safeSearch=" + URLEncoder.encode(safeSearch, "UTF-8").replaceAll("\\+", "%20");
		}

		String serverResponse[] = ServiceCallHelper.getDataFromServerWithCustomHeader(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getOrgNamesSuggestions(String query, String locationCode) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/org/suggest-name-loc";
		boolean flag = false;
		if (query != null) {
			url = url + "?q=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		if (locationCode != null && flag) {
			url = url + "&location-code=" + URLEncoder.encode(locationCode, "UTF-8").replaceAll("\\+", "%20");
		} else if (locationCode != null && !flag) {
			url = url + "?location-code=" + URLEncoder.encode(locationCode, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		return getDataFromServer(url);
	}

	@Override
	public String generateReport(String jsonString) throws Exception {
		// TODO Auto-generated method stub
		return jsonString;
	}

	@Override
	public AnnualReturnResponseDto getAnnualReports(String url, String identifier, String requestOrganisation,
			Long userId) throws Exception {
		AnnualReturnResponseDto annualReturnResponseDto = new AnnualReturnResponseDto();
		List<ShareholdingDto> shareholdingDtoList = new ArrayList<ShareholdingDto>();
		byte[] fileData = ServiceCallHelper.downloadFileFromServerWithoutTimeout(url);
		if (fileData != null) {
			try {
				File filePdf = new File(TESSRACT_IMAGE_ALLOCATION + "/Annualreport.pdf");
				FileUtils.writeByteArrayToFile(filePdf, fileData);
				ITesseract tesseract = new Tesseract();
				tesseract.setDatapath(TESSRACT_LANGUAGE);
				String text = tesseract.doOCR(filePdf);
				System.out.println(text);
				String[] array = text.split("\n");
				for (int i = 0; i < array.length; i++) {
					if (array[i].contains("Total number")) {
						String[] splitValue = array[i].split("Currency GBP Total number");
						// System.out.println("Total number of shares: " +
						// splitValue[1] + "\n" + "\n");
						String totalNumber ="";
						if (splitValue.length > 1 && splitValue[1] != null)
							 totalNumber = splitValue[1].replaceAll("\\s", "");
						if(!StringUtils.isEmpty(totalNumber))
						annualReturnResponseDto.setTotalNumberOfShares(Double.valueOf(totalNumber));
						else
							annualReturnResponseDto.setTotalNumberOfShares(0.0);
						// System.out.println(array[i]
						// +"\n"+array[i+1]+"\n"+"\n");
					}
					if (array[i].contains("Shareholding")) {
						ShareholdingDto dto = new ShareholdingDto();
						// for no of shares
						Character splitWith = null;
						String[] splitString = null;
						System.out.println(array[i]);
						if (array[i].contains("-")) {
							splitString = array[i].split("-");
						} else if (array[i].contains(";")) {
							splitString = array[i].split(";");
						} else {
							// splitString=array[i].split(";");
							// if(splitString.length==0 &&
							// array[i].contains("-")) {
							// splitString=array[i].split("-");
							// }else if(splitString.length==0) {
							Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
							Matcher m = p.matcher(array[i]);
							while (m.find()) {
								splitWith = array[i].charAt(m.start());
								break;
							}
							splitString = array[i].split(splitWith.toString());
						}
						// }
						System.out.println(splitString[0]);
						String value = splitString[1];
						int k = value.indexOf(" ", value.indexOf(" ") + 2);
						String res = value.substring(0, k);
						dto.setNoOfShares(Double.valueOf(res.trim()));
						// for name
						if (array[i + 1].contains(".")) {
							// System.out.println("Inside . value befor
							// splitting "+array[i+1]);
							String[] nameSplit = array[i + 1].split("\\.");
							//String nameSplit[] = array[i + 1].split("\\.", 2);

							String name="";
							if(nameSplit!=null){
								int m=0;
								for (String string : nameSplit) {
									if(m!=0){
										name=name+string;
									}
									m++;
								}
							}
							//dto.setShareHolderName(nameSplit[1]);
							dto.setShareHolderName(name);
						}
						if (array[i + 1].contains(":")) {
							String[] nameSplit = array[i + 1].split(":");
							dto.setShareHolderName(nameSplit[1]);
						}
						if ((!array[i + 1].contains("Name")) && (!array[i + 1].contains("N ame"))) {
							if (array[i + 2].contains(".")) {
								String[] nameSplit = array[i + 2].split("\\.");
								dto.setShareHolderName(nameSplit[1]);
							}
							if (array[i + 2].contains(":")) {
								String[] nameSplit = array[i + 2].split(":");
								dto.setShareHolderName(nameSplit[1]);
							}
						}

						Pattern pt = Pattern.compile("[^a-zA-Z]");
						String trimmedName = dto.getShareHolderName();
						Matcher match = pt.matcher(trimmedName);
						while (match.find()) {
							String s = match.group();
							trimmedName = trimmedName.replaceAll("\\" + s, " ");
							dto.setShareHolderName(trimmedName.trim());
						}
						Double initialShare = dto.getNoOfShares();
						Double totalShare = annualReturnResponseDto.getTotalNumberOfShares();
						Double valueper = (initialShare / totalShare) * 100;
						dto.setTotalShareValuePer(valueper);
						shareholdingDtoList.add(dto);
						annualReturnResponseDto.setShareholdingDto(shareholdingDtoList);
					} // shareholding if close

				} // for each close
				if (filePdf != null) {
					/*documentService.uploadDocument(null, "Annual Report", null, userId, null, null, identifier,
							requestOrganisation,null, filePdf);*/
					List<DocumentVault> documentVault = documentService.findAll()
							.stream()
							.filter(xe -> xe.getAnnualUrl().equalsIgnoreCase(url))
							.collect(Collectors.toList());
					if(documentVault.isEmpty())
					{
					documentService.uploadDocument(null, "Annual Report", url, userId, null, null,null, identifier,
							requestOrganisation,null, filePdf);
					}
					filePdf.delete();
				}
			} // try close
			catch (NullPointerException ex) {
				return annualReturnResponseDto;
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			}
		} else {
			return annualReturnResponseDto;
		}
		// mapping identifier
		List<ShareholdingDto> shareholdingDtos = annualReturnResponseDto.getShareholdingDto();
		if (shareholdingDtos != null) {
			for (ShareholdingDto shareholdingDto : shareholdingDtos) {
				String response1 = getOrgNamesSuggestions(shareholdingDto.getShareHolderName(), null);
				if (response1 != null) {
					JSONObject json = new JSONObject(response1);
					if (json.has("hits")) {
						JSONArray array = json.getJSONArray("hits");
						if (array.length() > 0) {
							JSONObject json1 = array.getJSONObject(0);
							if (json1.has("@identifier")) {
								shareholdingDto.setIdentifier(json1.getString("@identifier"));
							}
						}
					}

				}
			}
		}
		return annualReturnResponseDto;
	}

	
	@Override
	public String getIndustryStandardCodeIsic(String standard, String code) throws Exception {
		String url = BIGDATA_INDUSTRY_URL + "/api/industry/" + standard + "/" + code + "/isic";
		return getDataFromServer(url);
	}

	@SuppressWarnings("unused")
	@Transactional("transactionManager")
	@Override
	public PeerResult getPeerGroupData(String jsonString, Long userId) throws Exception {

		double average = 0.0;
		int largeValueIndex = 0;
		int total = 0;

		PeerResult peerResult = new PeerResult();
		List<PeerDataDto> peerListData = new ArrayList<>();
		List<CaseUnderwritingDto> caseList = new ArrayList<>();
		String eventsAggregatesResponse = adverseNewsService.getSearchEventResultList(jsonString);
		if (eventsAggregatesResponse != null) {
			JSONObject json = new JSONObject(eventsAggregatesResponse);
			if (json.has("hits")) {
				JSONArray hitsArray = json.getJSONArray("hits");
				if (hitsArray.length() > 0) {
					for (int i = 0; i < hitsArray.length(); i++) {
						double riskScore = 0;
						double ir = 0;
						double dr = 0;
						double tr = 0;
						String tr_factors = null;
						String ir_factors = null;
						String dr_factors = null;
						String identifier = null;
						String name = null;
						String jurisdiction = null;
						// get the name of the entity
						if ((hitsArray.getJSONObject(i).has("actor-a"))
								&& (!hitsArray.getJSONObject(i).get("actor-a").equals(null))) {
							PeerDataDto peerDataDto = new PeerDataDto();
							List<String> industries = new ArrayList<>();
							if ((hitsArray.getJSONObject(i).getJSONObject("actor-a").has("identifier"))
									&& (hitsArray.getJSONObject(i).getJSONObject("actor-a").has("type"))
									&& (!hitsArray.getJSONObject(i).getJSONObject("actor-a").get("type").equals(null))
									&& (hitsArray.getJSONObject(i).getJSONObject("actor-a").getString("type")
											.equalsIgnoreCase("organization"))) {
								if (!hitsArray.getJSONObject(i).getJSONObject("actor-a").get("identifier").equals(null))
									identifier = hitsArray.getJSONObject(i).getJSONObject("actor-a")
											.getString("identifier");
								if (identifier != null) {
									name = getOrganizationNameByIdentifier(identifier);
								} else {
									name = hitsArray.getJSONObject(i).getJSONObject("actor-a").getString("name");
									// get jurisdiction
									if ((!hitsArray.getJSONObject(i).get("location").equals(null))
											&& (hitsArray.getJSONObject(i).has("location"))) {
										JSONObject locationJson = (JSONObject) hitsArray.getJSONObject(i)
												.get("location");
										if ((locationJson.has("country-code"))
												&& (!locationJson.get("country-code").equals(null)))
											jurisdiction = locationJson.getString("country-code");
										// String multisourceData =null;
										String multisourceData = advanceSearchService.getMultisourceData(name,
												jurisdiction, null, userId);
										if (multisourceData != null) {
											JSONObject jsonData = new JSONObject(multisourceData);
											if (jsonData.has("data")) {
												JSONObject data = jsonData.getJSONObject("data");
												JSONObject finlaDataReturn = new JSONObject();
												if (data != null && data.has("results")) {
													JSONArray resultsArray = data.getJSONArray("results");
													if (resultsArray.length() > 0) {
														JSONObject resultObject = resultsArray.getJSONObject(0);
														if ((resultObject.has("identifier"))
																&& (!resultObject.get("identifier").equals(null)))
															identifier = resultObject.getString("identifier");
													}
												}
											}
										}
									}
								}

								/*
								 * if (hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").getString("type")
								 * .equalsIgnoreCase("organization")) { name =
								 * getOrganizationNameByIdentifier(hitsArray.
								 * getJSONObject(i)
								 * .getJSONObject("actor-a").getString(
								 * "identifier"));
								 * 
								 * }
								 */
							} /*
								 * else { if
								 * ((hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").has("identifier")) &&
								 * (hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").has("type")) &&
								 * (!hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").get("type") .equals(null)) &&
								 * (hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").getString("type")
								 * .equalsIgnoreCase("organization"))) { if
								 * ((name == null) &&
								 * (hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").has("name") &&
								 * (!hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").get("name") .equals(null)))) {
								 * name =
								 * hitsArray.getJSONObject(i).getJSONObject(
								 * "actor-a").getString("name"); }
								 * 
								 * }
								 * 
								 * }
								 */

							/*
							 * if (name != null) { caseList =
							 * caseService.searchCaseList(name); if (null !=
							 * caseList && caseList.size() > 0) { for
							 * (CaseUnderwritingDto caseInfo : caseList) { Long
							 * caseId = caseInfo.getCaseId(); // hit the big
							 * data graph api // for // ir, dr, //
							 * ir_factors,dr_factors values String caseData =
							 * searchHistoryService.getCaseGraphData(String.
							 * valueOf(caseId)); JSONObject caseDataJson = new
							 * JSONObject(caseData); JSONArray verticesArray =
							 * caseDataJson.getJSONArray("vertices"); if
							 * (verticesArray.length() > 0) { for (int j = 0; j
							 * < verticesArray.length(); j++) { if
							 * (verticesArray.get(j) != null) { if
							 * ((verticesArray.getJSONObject(j).has("riskScore")
							 * ) &&
							 * (verticesArray.getJSONObject(j).has("labelV"))) {
							 * if (verticesArray.getJSONObject(j).getString(
							 * "labelV") .equalsIgnoreCase("company")) { if
							 * (verticesArray.getJSONObject(j)
							 * .getDouble("riskScore") > riskScore) { riskScore
							 * = verticesArray.getJSONObject(j)
							 * .getDouble("riskScore"); if
							 * ((verticesArray.getJSONObject(j).has("ir")) &&
							 * (!verticesArray.getJSONObject(j).get("ir")
							 * .equals(null))) ir =
							 * verticesArray.getJSONObject(j).getDouble("ir");
							 * if ((verticesArray.getJSONObject(j).has("dr")) &&
							 * (!verticesArray.getJSONObject(j).get("dr")
							 * .equals(null))) dr =
							 * verticesArray.getJSONObject(j).getDouble("dr");
							 * if
							 * ((verticesArray.getJSONObject(j).has("ir_factors"
							 * )) && (!verticesArray.getJSONObject(j)
							 * .get("ir_factors").equals(null))) ir_factors =
							 * verticesArray.getJSONObject(j)
							 * .getString("ir_factors"); if
							 * ((verticesArray.getJSONObject(j).has("dr_factors"
							 * )) && (!verticesArray.getJSONObject(j)
							 * .get("dr_factors").equals(null))) dr_factors =
							 * verticesArray.getJSONObject(j)
							 * .getString("dr_factors"); if
							 * ((verticesArray.getJSONObject(j).has("tr_factors"
							 * )) && (!verticesArray.getJSONObject(j)
							 * .get("tr_factors").equals(null))) tr_factors =
							 * verticesArray.getJSONObject(j)
							 * .getString("tr_factors"); if
							 * ((verticesArray.getJSONObject(j).has("tr")) &&
							 * (!verticesArray.getJSONObject(j).get("tr")
							 * .equals(null))) tr =
							 * verticesArray.getJSONObject(j).getDouble("tr"); }
							 * } } } } }
							 * 
							 * } } else { // call akthar api for caseID //
							 * create JsonObject for request // body to // the
							 * // api JSONObject requestBody = new JSONObject();
							 * JSONArray fetchers = new JSONArray();
							 * fetchers.put(0, "1008"); fetchers.put(1, "1006");
							 * fetchers.put(2, "1013"); fetchers.put(3, "1021");
							 * requestBody.put("fetchers", fetchers); if (name
							 * != null) requestBody.put("keyword", name);
							 * requestBody.put("searchType", "Company");
							 * requestBody.put("lightWeight", true);
							 * requestBody.put("limit", 1);
							 * requestBody.put("iterations", 1);
							 * requestBody.put("alias", "decisionScoring");
							 * requestBody.put("create_new_graph", false);
							 * requestBody.put("entity_resolution", true);
							 * String responseWithCaseIds =
							 * searchHistoryService.getSearchData(null,
							 * requestBody.toString(), "graph"); JSONObject
							 * jsonResponseWithCaseIds = new
							 * JSONObject(responseWithCaseIds); String caseID =
							 * jsonResponseWithCaseIds.getString("caseId");
							 * String caseData = null; if (caseID.trim() !=
							 * null) caseData =
							 * searchHistoryService.getCaseGraphData(String.
							 * valueOf(caseID)); if (caseData != null) {
							 * JSONObject caseDataJson = new
							 * JSONObject(caseData); JSONArray verticesArray =
							 * caseDataJson.getJSONArray("vertices"); if
							 * (verticesArray.length() > 0) { for (int j = 0; j
							 * < verticesArray.length(); j++) { if
							 * (verticesArray.get(j) != null) { if
							 * ((verticesArray.getJSONObject(j).has("riskScore")
							 * ) &&
							 * (!verticesArray.getJSONObject(j).get("riskScore")
							 * .equals(null)) &&
							 * (verticesArray.getJSONObject(j).has("labelV")) &&
							 * (!verticesArray.getJSONObject(j).get("labelV")
							 * .equals(null))) { if
							 * (verticesArray.getJSONObject(j).getString(
							 * "labelV") .equalsIgnoreCase("company")) { if
							 * (verticesArray.getJSONObject(j)
							 * .getDouble("riskScore") > riskScore) { riskScore
							 * = verticesArray.getJSONObject(j)
							 * .getDouble("riskScore"); if
							 * ((verticesArray.getJSONObject(j).has("ir")) &&
							 * (!verticesArray.getJSONObject(j).get("ir")
							 * .equals(null))) ir =
							 * verticesArray.getJSONObject(j).getDouble("ir");
							 * if ((verticesArray.getJSONObject(j).has("dr")) &&
							 * (!verticesArray.getJSONObject(j).get("dr")
							 * .equals(null))) dr =
							 * verticesArray.getJSONObject(j).getDouble("dr");
							 * if
							 * ((verticesArray.getJSONObject(j).has("ir_factors"
							 * )) && (!verticesArray.getJSONObject(j)
							 * .get("ir_factors").equals(null))) ir_factors =
							 * verticesArray.getJSONObject(j)
							 * .getString("ir_factors"); if
							 * ((verticesArray.getJSONObject(j).has("dr_factors"
							 * )) && (!verticesArray.getJSONObject(j)
							 * .get("dr_factors").equals(null))) dr_factors =
							 * verticesArray.getJSONObject(j)
							 * .getString("dr_factors"); if
							 * ((verticesArray.getJSONObject(j).has("tr_factors"
							 * )) && (!verticesArray.getJSONObject(j)
							 * .get("tr_factors").equals(null))) tr_factors =
							 * verticesArray.getJSONObject(j)
							 * .getString("tr_factors"); if
							 * ((verticesArray.getJSONObject(j).has("tr")) &&
							 * (!verticesArray.getJSONObject(j).get("tr")
							 * .equals(null))) tr =
							 * verticesArray.getJSONObject(j).getDouble("tr"); }
							 * } } } } } } }
							 * 
							 * }
							 */

							// modification according to the new Risk score API
							if (identifier != null && name != null) {
								String riskScoreData = riskScoreService.getRiskScoreByEntityId(identifier);
								if (riskScoreData != null) {
									JSONObject riskScoreDataJson = new JSONObject(riskScoreData);
									if ((riskScoreDataJson.has("latest"))
											&& (!riskScoreDataJson.get("latest").equals(null))) {
										JSONObject latestJsonObj = riskScoreDataJson.getJSONObject("latest");
										if ((latestJsonObj.has("entityRiskModel"))
												&& (!latestJsonObj.get("entityRiskModel").equals(null))) {
											JSONObject entityRiskModelObj = latestJsonObj
													.getJSONObject("entityRiskModel");
											if ((entityRiskModelObj.has("overall-score"))
													&& (!entityRiskModelObj.get("overall-score").equals(null)))
												riskScore = entityRiskModelObj.getDouble("overall-score");
											peerDataDto.setRiskScoreData(riskScoreData);
										}
									}
								}
							}

							peerDataDto.setDr(dr);
							peerDataDto.setDr_factors(dr_factors);
							peerDataDto.setIr(ir);
							peerDataDto.setIr_factors(ir_factors);
							peerDataDto.setTr(tr);
							peerDataDto.setTr_factors(tr_factors);
							peerDataDto.setName(name);
							peerDataDto.setRiskScore(riskScore);
							peerDataDto.setIdentifier(identifier);

							if ((!hitsArray.getJSONObject(i).get("industry").equals(null))
									&& (hitsArray.getJSONObject(i).has("industry"))) {
								if (hitsArray.getJSONObject(i).get("industry") instanceof String) {
									industries.add(hitsArray.getJSONObject(i).getString("industry"));
								}

								else {
									JSONArray industryArray = hitsArray.getJSONObject(i).getJSONArray("industry");
									for (int in = 0; in < industryArray.length(); in++) {
										industries.add(industryArray.getString(in));
									}
								}
								peerDataDto.setIndustry(industries);
							}

							if ((!hitsArray.getJSONObject(i).get("location").equals(null))
									&& (hitsArray.getJSONObject(i).has("location"))) {
								// System.out.println(hitsArray.getJSONObject(i));
								// System.out.println(hitsArray.getJSONObject(i).get("location"));
								LocationDto locationDto = new LocationDto();
								JSONObject location = (JSONObject) hitsArray.getJSONObject(i).get("location");
								if ((location.has("name")) && (!location.get("name").equals(null)))
									locationDto.setName(location.getString("name"));
								if ((location.has("original")) && (!location.get("original").equals(null)))
									locationDto.setOriginal(location.getString("original"));
								if ((location.has("latitude")) && (!location.get("latitude").equals(null)))
									locationDto.setLatitude(location.getInt("latitude"));
								if ((location.has("longitude")) && (!location.get("longitude").equals(null)))
									locationDto.setLongitude(location.getInt("longitude"));
								if ((location.has("admin-1-code")) && (!location.get("admin-1-code").equals(null)))
									locationDto.setAdmin_1_code(location.getString("admin-1-code"));
								if ((location.has("admin-2-code")) && (!location.get("admin-2-code").equals(null)))
									locationDto.setAdmin_2_code(location.getString("admin-2-code"));
								if ((location.has("country-code")) && (!location.get("country-code").equals(null)))
									locationDto.setCountry_code(location.getString("country-code"));
								peerDataDto.setLocation(locationDto);
							}
							if ((!hitsArray.getJSONObject(i).get("technologies").equals(null))
									&& (hitsArray.getJSONObject(i).has("technologies"))) {
								TechnologiesDto technologiesDto = new TechnologiesDto();
								List<TechnologiesDto> listTechnologies = new ArrayList<>();
								JSONArray technologiesArray = hitsArray.getJSONObject(i).getJSONArray("technologies");
								for (int k = 0; k < technologiesArray.length(); k++) {
									if ((technologiesArray.getJSONObject(k).has("text"))
											&& (!technologiesArray.getJSONObject(k).get("text").equals(null)))
										technologiesDto.setText(technologiesArray.getJSONObject(k).getString("text"));
									if ((technologiesArray.getJSONObject(k).has("type"))
											&& (!technologiesArray.getJSONObject(k).get("type").equals(null)))
										technologiesDto.setType(technologiesArray.getJSONObject(k).getString("type"));
									listTechnologies.add(technologiesDto);
								}
								peerDataDto.setTechnologies(listTechnologies);

							}
							peerListData.add(peerDataDto);
							if ((json.has("total")) && (!json.get("total").equals(null))) {
								total = json.getInt("total");
							}
							peerResult.setPeerDataDto(peerListData);
							peerResult.setTotalResults(peerListData.size());
						}
					}
				}

			}

			return peerResult;
		} else {
			throw new InternalError("Failed to get aggregate news data");
		}
	}

	// unused
	@SuppressWarnings("unused")
	public List<EntitySearchAggregatesDto> setJsonReponseToDto(JSONObject json) {
		List<EntitySearchAggregatesDto> searchAggregateList = new ArrayList<EntitySearchAggregatesDto>();
		EntitySearchAggregatesDto aggregate = new EntitySearchAggregatesDto();
		if (json.has("hits")) {
			JSONArray hitsArray = json.getJSONArray("hits");
			if (hitsArray != null && hitsArray.length() > 0) {
				for (int i = 0; i < hitsArray.length(); i++) {
					HitsDto hitsDto = new HitsDto();
					/*
					 * if(hitsArray.getJSONObject(i).has("actor-a")){ JSONObject
					 * actor_a_Json=hitsArray.getJSONObject(i).getJSONObject(
					 * "actor-a"); ActorDto actorDto= new ActorDto();
					 * if(actor_a_Json.has("identifier")){
					 * actorDto.setIdentifier(actor_a_Json.getString(
					 * "identifier")); } if(actor_a_Json.has("name")){
					 * actorDto.setName(actor_a_Json.getString("name")); }
					 * if(actor_a_Json.has("type")){
					 * actorDto.setType(actor_a_Json.getString("type")); }
					 * hitsDto.setActor_a(actorDto);
					 * 
					 * }
					 */
					if (hitsArray.getJSONObject(i).has("industry")) {
						JSONArray industryArray = hitsArray.getJSONObject(i).getJSONArray("industry");
						if (industryArray != null && industryArray.length() > 0) {
							List<String> industryObj = new ArrayList<String>();
							for (int j = 0; j < industryArray.length(); j++) {
								industryObj.add(industryArray.getString(j));
							}
							hitsDto.setIndustry(industryObj);
						}
					}

					if (hitsArray.getJSONObject(i).has("technologies")) {
						JSONArray technologiesArray = hitsArray.getJSONObject(i).getJSONArray("technologies");
						if (technologiesArray != null && technologiesArray.length() > 0) {
							List<TechnologiesDto> technologiesObj = new ArrayList<TechnologiesDto>();
							for (int k = 0; k < technologiesArray.length(); k++) {
								TechnologiesDto technologiesDto = new TechnologiesDto();
								if (technologiesArray.getJSONObject(k).has("text"))
									technologiesDto.setText(technologiesArray.getJSONObject(k).getString("text"));
								if (technologiesArray.getJSONObject(k).has("type"))
									technologiesDto.setType(technologiesArray.getJSONObject(k).getString("type"));
								technologiesObj.add(technologiesDto);
							}
							hitsDto.setTechnologies(technologiesObj);
						}
					}

					if (hitsArray.getJSONObject(i).has("location")) {
						LocationDto locationDto = new LocationDto();
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("name"))
							locationDto
									.setName(hitsArray.getJSONObject(i).getJSONObject(("location")).getString("name"));
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("original"))
							locationDto.setOriginal(
									hitsArray.getJSONObject(i).getJSONObject(("location")).getString("original"));
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("country-code"))
							locationDto.setCountry_code(
									hitsArray.getJSONObject(i).getJSONObject(("location")).getString("country-code"));
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("latitude"))
							locationDto.setLatitude(
									hitsArray.getJSONObject(i).getJSONObject(("location")).getInt("latitude"));
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("longitude"))
							locationDto.setLongitude(
									hitsArray.getJSONObject(i).getJSONObject(("location")).getInt("longitude"));
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("admin-1-code"))
							locationDto.setAdmin_1_code(
									hitsArray.getJSONObject(i).getJSONObject(("location")).getString("admin-1-code"));
						if (hitsArray.getJSONObject(i).getJSONObject(("location")).has("admin-2-code"))
							locationDto.setAdmin_2_code(
									hitsArray.getJSONObject(i).getJSONObject(("location")).getString("admin-2-code"));
						hitsDto.setLocation(locationDto);
					}

					// Set the name,ir,tr,dr, and other values
				}
			}
		}
		return searchAggregateList;
	}

	public String getOrganizationNameByIdentifier(String identifier) throws Exception {
		String organizationName = null;
		String profileResponse = adverseNewsService.getAdverseProfileById(identifier);
		JSONObject profileJson = new JSONObject(profileResponse);
		if (profileJson.has("basic")) {
			if (profileJson.getJSONObject("basic").has("vcard:organization-name")) {
				organizationName = profileJson.getJSONObject("basic").getString("vcard:organization-name");
			}
		}
		return organizationName;
	}

	@Override
	public List<AdverseNews> getCognitiveMicrosoftWithAdvanceSearch(String q, String count, String offset, String mkt,
			String safeSearch) throws Exception {

		String url = MICROSOFT_COGNITIVE_URL;
		boolean flag = false;
		if (q != null) {
			url = url + "?q=" + URLEncoder.encode(q, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		if (count != null && flag) {
			url = url + "&count=" + URLEncoder.encode(count, "UTF-8").replaceAll("\\+", "%20");

		} else if (count != null) {
			url = url + "?count=" + URLEncoder.encode(count, "UTF-8").replaceAll("\\+", "%20");
			flag = true;
		}
		if (offset != null && flag) {
			url = url + "&offset=" + URLEncoder.encode(offset, "UTF-8").replaceAll("\\+", "%20");
		} else if (offset != null && !flag) {
			url = url + "?offset=" + URLEncoder.encode(offset, "UTF-8").replaceAll("\\+", "%20");
		}
		if (mkt != null && flag) {
			url = url + "&mkt=" + URLEncoder.encode(mkt, "UTF-8").replaceAll("\\+", "%20");
		} else if (mkt != null && !flag) {
			url = url + "?mkt=" + URLEncoder.encode(mkt, "UTF-8").replaceAll("\\+", "%20");
		}
		if (safeSearch != null && flag) {
			url = url + "&safeSearch=" + URLEncoder.encode(safeSearch, "UTF-8").replaceAll("\\+", "%20");
		} else if (safeSearch != null && !flag) {
			url = url + "?safeSearch=" + URLEncoder.encode(safeSearch, "UTF-8").replaceAll("\\+", "%20");
		}
		// filtering based on date
		// url = url + "&sortBy=" + URLEncoder.encode("Date",
		// "UTF-8").replaceAll("\\+",
		// "%20");

		String response = "";
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithCustomHeader(url);
		List<AdverseNews> adverseNewsList = new ArrayList<AdverseNews>();
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				ObjectMapper mapper = new ObjectMapper();
				BingSearchNewsDto bingSearchNewsDto = mapper.readValue(response, BingSearchNewsDto.class);
				if (bingSearchNewsDto != null) {
					List<BingNews> bingNews = bingSearchNewsDto.getValue();
					for (BingNews bingNewsSend : bingNews) {
						AdverseNews adverseNews = new AdverseNews();
						List<EntityDto> listEntityToSent = new ArrayList<EntityDto>();
						adverseNews.setClassBing(bingNewsSend.getCategory());
						adverseNews.setPublished(bingNewsSend.getDatePublished());
						adverseNews.setTitle(bingNewsSend.getName());
						adverseNews.setUrl(bingNewsSend.getUrl());
						adverseNews.setText(bingNewsSend.getDescription());
						if (bingNewsSend.getUrl() != null) {
							URL urlToSent = new URL(bingNewsSend.getUrl());
							adverseNews.setSource(urlToSent.getProtocol() + "://" + urlToSent.getHost());
						}
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("text", bingNewsSend.getDescription());
						String entityResponse = entityVisualizerService.getEntData(jsonObject.toString());
						BingEntityDto entityListDto = mapper.readValue(entityResponse, BingEntityDto.class);
						List<EntityDto> entityDtos = entityListDto.getEntities();
						for (EntityDto entityDto : entityDtos) {
							EntityDto dto = new EntityDto();
							BeanUtils.copyProperties(entityDto, dto);
							listEntityToSent.add(entityDto);
						}
						adverseNews.setEntityDto(listEntityToSent);
						adverseNewsList.add(adverseNews);
					}
					// adverseDto.setAdverseNews(adverseNewsList);
				}

			}
		} else {
			return adverseNewsList;
			// throw new
			// Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}

		return adverseNewsList;

	}

	@Override
	public List<AdverseNews> getBingNews(String identifier) throws Exception {
		List<Classification> classificationsList = classificationService.findAll();
		List<AdverseNews> result = new ArrayList<>();
		for (Classification classification : classificationsList) {
			StringBuilder builder = new StringBuilder();
			builder.append(identifier + " AND (");
			List<Keywords> keywordsList = classification.getKeyWords();
			int j = 0;
			for (Keywords keyword : keywordsList) {
				j++;
				if (j == keywordsList.size())
					builder.append("\"" + keyword.getKeyword() + "\"");
				else
					builder.append("\"" + keyword.getKeyword() + "\"" + " OR ");
			}
			builder.append(")");
			List<AdverseNews> intermediateResult = getCognitiveMicrosoftWithAdvanceSearch(builder.toString(), "10", "0",
					"en-us", "Moderate");
			for (AdverseNews adverseNews : intermediateResult) {
				adverseNews.setClassBing(classification.getName());
			}
			result.addAll(intermediateResult);
		} // main for close
		return result;
	}

	@Override
	public String orgSearchWithMatchers(String jsonString) throws Exception {

		String url = BIG_DATA__ORG_URL + "entity/org/search-with-matchers";
		return postStringDataToServer(url, jsonString);
	}

	public String searchPersonWithMatchers(String jsonString) throws Exception {
		String url = BIG_DATA__ORG_URL + "entity/person/search-with-matchers";
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getArticleNewsList(ArticleDto articleDto) throws Exception {
		String response = getDataFromServer(articleDto.getArticlePath());
		if (response != null) {
			JSONObject json = new JSONObject(response);
			if (json != null && json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				for (int i = 0; i < results.length(); i++) {
					JSONObject result = results.getJSONObject(i);
					String articlePath = result.getString("article_path");
					String[] stringArray = articlePath.split("/");
					String uuid = stringArray[1].replaceAll(".json", "");
					SignificantArticles significantArticles = null;
					if (uuid != null)
						significantArticles = significantArticlesService.getSignificantArticles(uuid);
					if (significantArticles != null && significantArticles.isSignificantNews())
						result.accumulate("isSignificant", true);
					if (significantArticles != null && significantArticles.getSentiment() != null)
						result.accumulate("sentiment", significantArticles.getSentiment());
					if(significantArticles != null)
						result.accumulate("significantId", significantArticles.getId());
				}
			}
		}
		return response;
	}

	@Override
	public String getArticleNewsInfo(ArticleDto ArticleDto) throws Exception {
		String response = null;
		JSONObject json = null;
		try {
			response = getDataFromServer(ArticleDto.getArticlePath());
			if (response != null) {
				json = new JSONObject(response);
				String text = null;
				if (json.has("results")) {
					JSONObject results = json.getJSONObject("results");
					String uuid = null;
					if (results.has("uuid")) {
						uuid = results.getString("uuid");
					} else if (results.has("thread")) {
						JSONObject thread = results.getJSONObject("thread");
						if (thread.has("uuid")) {
							uuid = thread.getString("uuid");
						}
					}
					if (uuid != null) {
						SignificantArticles significantArticles = significantArticlesService
								.getSignificantArticles(uuid);
						if (significantArticles != null && significantArticles.isSignificantNews())
							results.accumulate("isSignificant", true);
						if (significantArticles != null && significantArticles.getSentiment() != null)
							results.accumulate("sentiment", significantArticles.getSentiment());
						if(significantArticles!=null){
							results.accumulate("significantId", significantArticles.getId());
						}
					}
					if (results.has("text")) {
						text = results.getString("text");
						if (text != null) {
							TextSentiment textSentiment = new TextSentiment();
							textSentiment.setLanguage("en");
							textSentiment.setText(text);
							String responseSentiment = textSentimentService.getTextSentiments(textSentiment);
							results.accumulate("sentimentNews", new JSONObject(responseSentiment));
						}
					}
				}
			}

			/*
			 * if (!StringUtils.isEmpty(response) && response.trim().length() >
			 * 0) { ObjectMapper mapper = new ObjectMapper();
			 * AritcleNewsInfoRequestDto articleNews =
			 * mapper.readValue(response, AritcleNewsInfoRequestDto.class); if
			 * (articleNews != null) { aritcleNewsInfoResponseDto = new
			 * EntityConverter<AritcleNewsInfoRequestDto,
			 * AritcleNewsInfoResponseDto>( AritcleNewsInfoRequestDto.class,
			 * AritcleNewsInfoResponseDto.class).toT2(articleNews, new
			 * AritcleNewsInfoResponseDto()); JSONObject jsonObject = new
			 * JSONObject(); jsonObject.put("text",
			 * aritcleNewsInfoResponseDto.getText()); String entityResponse =
			 * entityVisualizerService.getEntData(jsonObject.toString());
			 * BingEntityDto entityListDto = mapper.readValue(entityResponse,
			 * BingEntityDto.class); List<EntityDto> entityDtos =
			 * entityListDto.getEntities(); List<EntityDto> entityDtoToSend =
			 * new ArrayList<EntityDto>(); for (EntityDto entityDto :
			 * entityDtos) { EntityDto dto = new EntityDto();
			 * BeanUtils.copyProperties(entityDto, dto);
			 * entityDtoToSend.add(entityDto); }
			 * aritcleNewsInfoResponseDto.setEntities(entityDtoToSend);
			 * 
			 * // setting setiment news TextSentiment textSentiment = new
			 * TextSentiment(); textSentiment.setLanguage("en");
			 * textSentiment.setText(aritcleNewsInfoResponseDto.getText());
			 * String responseSentiment =
			 * textSentimentService.getTextSentiments(textSentiment);
			 * ObjectMapper mapperSentiment = new ObjectMapper();
			 * SentimentRequestDto sentimentDto =
			 * mapperSentiment.readValue(responseSentiment,
			 * SentimentRequestDto.class);
			 * aritcleNewsInfoResponseDto.setSentiment(sentimentDto.
			 * getSentimentDto()); } }
			 */
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.INTERNAL_SERVER_ERROR, e, this.getClass());
			throw new Exception(ElementConstants.INTERNAL_SERVER_ERROR);
		}
		if (json != null)
			return json.toString();
		else
			return response;
	}
}
