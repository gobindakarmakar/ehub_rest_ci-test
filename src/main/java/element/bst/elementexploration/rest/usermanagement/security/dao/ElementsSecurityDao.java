package element.bst.elementexploration.rest.usermanagement.security.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;

/**
 * @author Paul Jalagari
 *
 */
public interface ElementsSecurityDao extends GenericDao<UsersToken, Long> {

	boolean isValidToken(String token);
	
	UsersToken getUserToken(String token);
	
	UsersToken isTokenExist(long userId);
	
	boolean isTokenExist(String token);
	
	UsersToken getUsersToken(String token);
}
