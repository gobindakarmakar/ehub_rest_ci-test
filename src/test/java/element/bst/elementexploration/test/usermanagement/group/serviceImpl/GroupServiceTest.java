package element.bst.elementexploration.test.usermanagement.group.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;
import element.bst.elementexploration.rest.usermanagement.group.serviceImpl.GroupServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
	
	@Mock
	GroupDao groupDaoMock;
	
	@Mock
	UserDao userDaoMock;
	
	public User user;
	
	@InjectMocks
	GroupServiceImpl groupService;
	
	public Group group = new Group();
	
	public Group userGroup = new Group();
	
	public List<Group> gorupList = new ArrayList<Group>();
	
	@Before
    public void setupMock() {
		group.setActive(1);
		group.setCreatedBy(1L);
		group.setCreatedDate(new Date());
		group.setDescription("testing user gorups");
		group.setName("admin");
		group.setRemarks("unit test group");
		group.setUserGroupId(1L);
		
		userGroup.setActive(1);
		userGroup.setCreatedBy(1L);
		userGroup.setCreatedDate(new Date());
		userGroup.setDescription(" user gorups");
		group.setName("Users group");
		group.setRemarks("unit test users group");
		group.setUserGroupId(2L);
		
		gorupList.add(group);
		gorupList.add(userGroup);
		user=new User("analyst","Analyst","Analyst",new Date(),"1");
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getGroupTest(){
		Mockito.when(groupDaoMock.getGroup(Mockito.anyString())).thenReturn(group);
		Group fetchGroup = groupService.getGroup("admin");
		Assert.assertNotNull(fetchGroup.getName());
	}
	
	@Test
	public void getUserGroupsTest(){
		Mockito.when(groupDaoMock.getUserGroups(Mockito.anyInt(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(gorupList);
		List<Group> fetchGroupsList = groupService.getUserGroups(1,"admin",1,10,"name","desc");
		assertEquals(2,fetchGroupsList.size());
	}
	
	@Test
	public void isActiveGroupTest(){
		Mockito.when(groupDaoMock.find(Mockito.anyLong())).thenReturn(group);
		boolean status = groupService.isActiveGroup(1L);
		assertEquals(true,status);
	}
	
	@Test
	public void whenGroupIsNull_IsActiveGroupTest(){
		Mockito.when(groupDaoMock.find(Mockito.anyLong())).thenReturn(null);
		boolean status = groupService.isActiveGroup(1L);
		assertEquals(false,status);
	}
	
	@Test
	public void whenGroupIsNotActive_IsActiveGroupTest(){
		group.setActive(0);
		Mockito.when(groupDaoMock.find(Mockito.anyLong())).thenReturn(group);
		boolean status = groupService.isActiveGroup(1L);
		assertEquals(false,status);
	}
	
	@Test
	public void getGroupsOfUserTest(){
		Mockito.when(groupDaoMock.getGroupsOfUser(Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(gorupList);
		List<Group> fetchGroupsList = groupService.getGroupsOfUser(1L,1,15,"admin","desc");
		assertEquals(2,fetchGroupsList.size());
	}
	
	@Test
	public void countUserGroupsTest(){
		Mockito.when(groupDaoMock.countUserGroups(Mockito.anyInt(),Mockito.anyString())).thenReturn(2L);
		long userGroupCount = groupService.countUserGroups(1,"admin");
		assertEquals(2,userGroupCount);
	}
	
	@Test
	public void countGroupsOfUserTest(){
		Mockito.when(groupDaoMock.countGroupsOfUser(Mockito.anyLong())).thenReturn(2L);
		long userGroupCount = groupService.countGroupsOfUser(1L);
		assertEquals(2,userGroupCount);
	}
	
	@Test
	public void checkGroupExistTest(){
		Mockito.when(groupDaoMock.checkGroupExist(Mockito.anyString(),Mockito.anyLong())).thenReturn(group);
		Group existedGroup = groupService.checkGroupExist("admin",1L);
		Assert.assertNotNull(existedGroup.getUserGroupId());
	}
	
	@Test
	public void doesUserbelongtoGroupTest(){
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(groupDaoMock.doesUserbelongtoGroup(Mockito.anyString(),Mockito.anyLong())).thenReturn(true);
		boolean groupStatus = groupService.doesUserbelongtoGroup("admin",1L);
		assertEquals(true,groupStatus);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = NoDataFoundException.class)
	public void doesUserbelongtoGroup_whenExThrowTest(){
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenThrow(NoDataFoundException.class);
	    groupService.doesUserbelongtoGroup("admin",1L);
		
	}

}
