package element.bst.elementexploration.rest.extention.sourcecredibility.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_source_credibility")
public class SourceCredibility implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long sourceCredibilityId;

	@ManyToOne
	@JoinColumn(name = "source_id")
	private Sources sources;

	@ManyToOne
	@JoinColumn(name = "sub_classifications_id")
	private SubClassifications subClassifications;

	@ManyToOne
	@JoinColumn(name = "data_attribute_id")
	private DataAttributes dataAttributes;

	@Column(name = "credibility")
	@Enumerated(EnumType.ORDINAL)
	private CredibilityEnums credibility;

	public SourceCredibility() {
	}

	public SourceCredibility(Sources sources, SubClassifications subClassifications, DataAttributes dataAttributes,
			CredibilityEnums credibility) {
		super();
		this.sources = sources;
		this.subClassifications = subClassifications;
		this.dataAttributes = dataAttributes;
		this.credibility = credibility;
	}

	public Long getSourceCredibilityId() {
		return sourceCredibilityId;
	}

	public void setSourceCredibilityId(Long sourceCredibilityId) {
		this.sourceCredibilityId = sourceCredibilityId;
	}

	public Sources getSources() {
		return sources;
	}

	public void setSources(Sources sources) {
		this.sources = sources;
	}

	public SubClassifications getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(SubClassifications subClassifications) {
		this.subClassifications = subClassifications;
	}

	public DataAttributes getDataAttributes() {
		return dataAttributes;
	}

	public void setDataAttributes(DataAttributes dataAttributes) {
		this.dataAttributes = dataAttributes;
	}

	public CredibilityEnums getCredibility() {
		return credibility;
	}

	public void setCredibility(CredibilityEnums credibility) {
		this.credibility = credibility;
	}

}
