package element.bst.elementexploration.rest.casemanagement.dto;

public class TransitionsDto {
	
	private Integer key;
		
	private String name;

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
