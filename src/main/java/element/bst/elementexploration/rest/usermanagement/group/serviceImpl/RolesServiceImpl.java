package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.app.service.exception.NotFoundException;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.activiti.app.conf.Bootstrapper;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.usermanagement.group.dao.RolesDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupPermission;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.group.dto.RolesDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupPermissionService;
import element.bst.elementexploration.rest.usermanagement.group.service.RoleGroupService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author Paul Jalagari
 *
 */
@Service("rolesService")
@Transactional("transactionManager")
public class RolesServiceImpl extends GenericServiceImpl<Roles, Long> implements RolesService {

	@Autowired
	RolesDao rolesDao;

	@Autowired
	private AuditLogDao auditLogDao;

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private RoleGroupService roleGroupService;
	
	@Autowired
	private GroupPermissionService groupPermissionService;
	
	@Autowired
	Bootstrapper bootstrapper;
	
	@Autowired
	public RolesServiceImpl(GenericDao<Roles, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public Roles findRole(Roles role) throws Exception {
		Roles exixstingRoles=new Roles();
		exixstingRoles= rolesDao.findRole(role);
		return exixstingRoles;
			 
			 
		
		
	}
	//not used
	@Override
	public List<Roles> getRolesOfUser(Long userId) throws Exception {
		//List<UserRoles> userRoles=rolesDao.findAll().stream().filter(r -> r.getUserRoles());
		//List<Roles> roles
		return rolesDao.getRolesOfUser(userId);
	}

	@Override
	@Transactional("transactionManager")
	public Long getRolesCount() {
		
		return rolesDao.getRolesCount();
	}

	@Override
	public boolean deleteRole(Long roleId, Long userId) {
		Roles roles =null;
		boolean isDeleted = false;
		if (roleId != null) {
			roles =	rolesDao.find(roleId);
			if(roles!=null ) {
				
				Hibernate.initialize(roles.getUserRoles());
				List<UserRoles> userRolesList=roles.getUserRoles();
				if (userRolesList != null && userRolesList.size() > 0) {
					for (int k = 0; k < userRolesList.size(); k++) {
						UserRoles eachUserRole = userRolesList.get(k);
						if (eachUserRole.getRoleId().getRoleId().equals(roleId)) {
							userRolesList.remove(eachUserRole);
							userRolesService.delete(eachUserRole);
						}
					}
				}
				
				Hibernate.initialize(roles.getRoleGroups());
				List<RoleGroup> rolesGroupList=roles.getRoleGroups();
				if (rolesGroupList != null && rolesGroupList.size() > 0) {
					for (int k = 0; k < rolesGroupList.size(); k++) {
						RoleGroup eachRoleGroup = rolesGroupList.get(k);
						if (eachRoleGroup.getRoleId().getRoleId().equals(roleId)) {
							rolesGroupList.remove(eachRoleGroup);
							roleGroupService.delete(eachRoleGroup);
						}
					}
				}
				
				//Hibernate.initialize(roles.getGroupPermissions());
				//List<GroupPermission> groupPermissionList=roles.getGroupPermissions();
				//if (groupPermissionList != null && groupPermissionList.size() > 0) {
					//for (int k = 0; k < groupPermissionList.size(); k++) {
						//GroupPermission eachGroupPermission = groupPermissionList.get(k);
						//if (eachGroupPermission.getRoleId().getRoleId().equals(roleId)) {
							//groupPermissionList.remove(eachGroupPermission);
							try {
								groupPermissionService.deleteGroupPermissionByRoleId(roleId);
							} catch (Exception e) {
								e.printStackTrace();
							}
							//groupPermissionService.delete(eachGroupPermission);
						//}
					//}
				//}
				roles=find(roleId);
				if(roles!=null && roles.getRoleId()!=null) {
				update(roles);
				delete(roles);
				}
				isDeleted = true;
			}else {
				throw new NotFoundException("Role not found!");
			}
		}else {
			throw new NotFoundException("Role Id can't be null or empty");
		}
		Users user = usersDao.find(userId);
		user = usersService.prepareUserFromFirebase(user);
		//AlertComments comment=alertCommentsService.find(commentId);
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, ElementConstants.ROLE_MANAGEMENT_TYPE, null, ElementConstants.ROLE_MANAGEMENT_TYPE);
		log.setDescription(user.getFirstName() + " " + user.getLastName() + " deleted a role ");
		log.setName(ElementConstants.ROLE_MANAGEMENT_TYPE);
		log.setUserId(userId);
		log.setType(ElementConstants.ROLE_MANAGEMENT_TYPE);
		log.setTypeId(roleId);
		auditLogDao.create(log);
		return isDeleted;
	}

	@Override
	@Transactional("transactionManager")
	public List<RolesDto> getAllRoles() throws Exception {
		List<Roles> roles = new ArrayList<Roles>();
		roles = rolesDao.findAll();
		List<RolesDto> rolesDto = new ArrayList<RolesDto>();
		if(roles != null){
			rolesDto = roles.stream()
					.map((roleDto) -> new EntityConverter<Roles, RolesDto>(Roles.class, RolesDto.class)
							.toT2(roleDto, new RolesDto()))
					.collect(Collectors.toList());
		}
		Map<Long, Long> roleNActiveUserMap = userRolesService.getActiveUserNRoleCount();
		for(RolesDto role : rolesDto){
			if(roleNActiveUserMap.containsKey(role.getRoleId())){
				role.setActiveUsers(roleNActiveUserMap.get(role.getRoleId()));
			}
		}
		return rolesDto;
	}
	
	@Override
	@Transactional("transactionManager")
	public Roles findRoleByName(String roleName) throws Exception {
		return rolesDao.findRoleByName(roleName);
	}

	@Override
	@Transactional("transactionManager")
	public RolesDto getRoleByIdOrName(Long roleId, String roleName) {
		RolesDto roleDto = new RolesDto();
		try{
			Roles role = rolesDao.getRoleByIdOrName(roleId,roleName);
			BeanUtils.copyProperties(role, roleDto);
			Map<Long, Long> roleNActiveUserMap = userRolesService.getActiveUserNRoleCount();
			if(null != role.getRoleId()){
				roleDto.setActiveUsers(roleNActiveUserMap.get(role.getRoleId()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return roleDto;
	}


	@Override
	@Transactional("transactionManager")
	public Object getStatuswiseUsersByRoleId(Long roleId) {
		return userRolesService.getStatuswiseUsersByRoleId(roleId);
	}

	@Override
	@Transactional("transactionManager")
	public boolean assignUsersToRole(Long roleId, List<Long> userList){		
		boolean assignUsersToRoleStatus = false;
		Roles role = rolesDao.find(roleId);
		if(role != null && userList != null && !userList.isEmpty()){
			List<Users> users = usersService.getUsersByIds(userList);
			for(Long userId : userList)
			{				
				Users user = users.stream().filter((u) -> u.getUserId().equals(userId)).findAny().get();
				user = usersService.prepareUserFromFirebase(user);
				if(user != null)
				{
				   // check whether User is already assigned the Role or not	
					if(userRolesService.isUserRolesExist(userId, roleId) == false)
					{
						UserRoles userRole = new UserRoles();
						userRole.setUserId(user);
						userRole.setRoleId(role);
						userRole.setDescription(role.getDescription());
						
						userRolesService.save(userRole);
						
						if(userRole.getRoleId().getRoleName().equals("Admin") && !userRole.getUserId().getScreenName().equals("admin"))
						{
							bootstrapper.initializeNewActivityUser(userRole.getUserId().getPassword(), userRole.getUserId().getLastName(), userRole.getUserId().getScreenName());
						}
						assignUsersToRoleStatus = true;
					}
				}
			}
		}
		return assignUsersToRoleStatus;
	}

	@Override
	public void deleteGroupPermissionByRoleId(Long roleId) {
		rolesDao.deleteRoleById(roleId);
	}
}
