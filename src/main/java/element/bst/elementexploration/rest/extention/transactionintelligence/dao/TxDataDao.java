package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionMasterView;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataNotifDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByDate;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByLocationDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByType;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxProductTypeDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface TxDataDao extends GenericDao<TransactionsData, Long> {

	List<TransactionsData> fetchTransaction(String accountNumber);
	
	List<TransactionsData> getTransactionByAccountId(String originatorNumber, String beneficiaryNumber);
	
	/*List<TransactionsData> fetchTxsForCounterPartiesInputStats(String accountNumber);

	List<TransactionsData> fetchTxsForCounterPartiesOutputStats(String accountNumber);*/

	public List<TxByType> getTxByType(Long entityId);

	List<TxByDate> getTxByDate(Long entityId);

	public List<TxByLocationDto> getTxByLoc(List<String> partyCustomerIdentifiers)
			throws IllegalAccessException, InvocationTargetException;

	// Input Statistics
	List<CounterPartyDto> getCounterPartiesAggregate(List<String> entityId,String counterPartyMin,String counterPartyMax,String minTxs,List<String> location,String transactionType,String minAmount);

	List<CountryAggDto> getCountryAggregate(List<String> entityId,List<String> location,String minTxs,String transactionType,String minAmount);

	TotalDto getTotalAggregate(List<String> entityId,String minAmount,List<String> location,String transactionType);

	// Output statistics
	List<CounterPartyDto> getCounterPartiesAggregateForOutputStats(List<String> entityId,String counterPartyMin,String counterPartyMax,String minTxs,List<String> location,String transactionType,String minAmount);

	List<CountryAggDto> getCountryAggregateForOutputStats(List<String> entityId,List<String> location,String minTxs,String transactionType,String minAmount);

	TotalDto getTotalAggregateForOutputStats(List<String> entityId,String minAmount,List<String> location,String transactionType);
	
	List<TransactionsData> getAllTransactions();

	List<TransactionsData> getTransaction(String accountNumber, String transactionType, String minAmount,String minTxs);
	
	List<TransactionsData> getTransactionsByAccountIds(List<String> accountIds, Date date,String transactionType,String minAmount);

	List<TransactionsData> getTransaction(Integer pageNumber, Integer recordsPerPage);
	
	public List<TxProductTypeDto> fetchSumTxsProductType(String fromDate,String toDate,FilterDto filterDto) throws ParseException;
	
	public List<TransactionsData> fetchTxsBwDates(String fromDate, String toDate) throws ParseException;

	List<TransactionAmountAndAlertCountDto> getTotalAmountAndAlertCountByProductType(String fromDate, String toDate,Boolean groupBy,Integer recordsPerPage,Integer pageNumber,boolean input,boolean output,String type,FilterDto filterDto) throws ParseException;

	List<AlertDashBoardRespDto> getAlertsBetweenDates(String fromDate, String toDate,String name, Integer pageNumber,
			Integer recordsPerPage, String orderIn,FilterDto filterDto,boolean isTimeStamp);

	List<TransactionAmountAndAlertCountDto> getTransactionAggregates(String fromDate, String toDate,String granularity,Boolean isGroupByType,String transType,boolean input,boolean output,FilterDto filterDto) throws ParseException;

	List<TransactionAmountAndAlertCountDto> getTransactionAggregatesProductType(String fromDate, String toDate,String granularity,Boolean isGroupByType,String transType,boolean input,boolean output,FilterDto filterDto) throws ParseException;

	List<String> getTransProductTypes();

	Long getAlertCountBetweenDates(String fromDate, String toDate, String name,FilterDto filterDto,boolean isTimeStamp);

	
	List<CorporateStructureDto> fetchCorporateStructure(String fromDate,String toDate,FilterDto filterDto) throws ParseException;

	RiskCountAndRatioDto getCustomerRisk(String fromDate, String toDate,String filterType,FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> getCustomerRiskAggregates(String fromDate, String toDate,FilterDto filterDto);

	//RiskCountAndRatioDto getRiskAlertsAggregates(String fromDate, String toDate,String groupByCloumn,List<String> scenarios);


	//RiskCountAndRatioDto getCorruptionRiskAggregates(String fromDate, String toDate,String groupByCloumn,Boolean isGroupBy);

	//RiskCountAndRatioDto getPoliticalRiskAggregates(String fromDate, String toDate, String groupByCloumn,Boolean isGroupBy);

	RiskCountAndRatioDto getRiskAlerts(String fromDate, String toDate, String query,FilterDto filterDto);


	//RiskCountAndRatioDto getCorruptionRisk(String fromDate, String toDate, String object, boolean b);

	//RiskCountAndRatioDto getPoliticalRisk(String fromDate, String toDate, String object, boolean b);
	
	public List<TransactionsDataNotifDto> fetchTxsBwDatesCustomized(String fromDate,String toDate,Integer recordsPerPage,Integer pageNumber,String productType,boolean input,boolean output,FilterDto filterDto) throws ParseException;
	public List<CounterPartyNotiDto> fetchCounterParties(String fromDate, String toDate,FilterDto filterDto) throws ParseException;
	public long fetchTxsBwDatesCustomizedCount(String fromDate, String toDate,String productType,boolean input,boolean output,FilterDto filterDto) throws ParseException;
	public List<AssociatedEntityRespDto> fetchAssociatedEntity(String fromDate, String toDate,FilterDto filterDto) throws ParseException;
	public List<TxProductTypeDto> fetchAlertTransactionProductCount(String fromDate,String toDate,FilterDto filterDto) throws ParseException;
	public List<CounterPartyNotiDto> fetchTopFiveTransactions(String fromDate, String toDate,FilterDto filterDto ) throws ParseException;
	public List<AlertComparisonDto> alertComparisonTransactions(String fromDate, String toDate,Date previousDate,FilterDto filterDto,String originalToDate) throws ParseException;

	List<RiskCountAndRatioDto> getTopCustomerRisks(String fromDate, String toDate);
	public List<CounterPartyNotiDto> counterPartyLocationsPlot(String fromDate, String toDate,boolean input,boolean output,String queryCondition,FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> viewAll(String fromDate, String toDate, boolean isTransaction,
			Integer pageNumber, Integer recordsPerPage,FilterDto filterDto,String queryCondition);

	Long viewAllCount(String fromDate, String toDate, boolean isTransaction,FilterDto filterDto,String queryCondition);

	List<RiskCountAndRatioDto> getViewAll(String fromDate, String toDate, boolean isTransaction, Integer pageNumber,
			Integer recordsPerPage,String queryforCondition,FilterDto filterDto);

	Long getViewAllCount(String fromDate, String toDate, boolean isTransaction, String queryforCondition,FilterDto filterDto);

	List<RiskCountAndRatioDto> getScenarioAggregates(String fromDate, String toDate, String columnToCondition,
			List<String> scenarios, String type);

	List<TopAmlCustomersDto> amlTopCustomers(String fromDate,String  toDate,FilterDto filterDto)  throws ParseException ;
	List<AlertedCounterPartyDto> getAlertedTransaction(String fromDate, String toDate,List<String> customerNumbers) throws ParseException;

	List<RiskCountAndRatioDto> getBankAggregates(String fromDate, String toDate, boolean isForCountry,boolean isGroupBy,String type);

	List<RiskCountAndRatioDto> getViewAllForBanks(String fromDate, String toDate, boolean isTransaction, Integer pageNumber,
			Integer recordsPerPage);

	List<TransactionAmountAndAlertCountDto> getInputAndOutput(String fromDate, String toDate, boolean isGroupBy,String queryCondition);

	Long getViewAllCountBanks(String fromDate, String toDate, boolean isTransaction);

	List<RiskCountAndRatioDto> getViewAllCountry(String fromDate, String toDate, boolean isTransaction, Integer pageNumber,
			Integer recordsPerPage);

	Long getViewAllContry(String fromDate, String toDate, boolean isTransaction);

	List<CounterPartyInfo> counterPartyCountryAggregate(String fromDate, String toDate, Long customerId,boolean isCountry,FilterDto filterDto);

	List<AlertedCounterPartyDto> getCustomerTransactions(String fromDate, String toDate, Long customerId,Integer pageNumber,
			Integer recordsPerPage,boolean isDetected);

	Long getCustomerTransactionsCount(String fromDate, String toDate, Long customerId, boolean isDetected);

	RiskCountAndRatioDto getBankByType(String fromDate, String toDate, boolean isForCountry, String type);

	List<CounterPartyInfo> counterPartyCountryAggregateOut(String fromDate, String toDate, Long customerId, boolean isCountry);

	List<AlertedCounterPartyDto> getCustomerTransactions(String fromDate, String toDate, Long customerId,
		 boolean isDetected,boolean isOutTransaction,boolean isInTransaction,FilterDto filterDto);

	Long getCustomerTransactionsCount(String fromDate, String toDate, Long customerId, boolean isDetected,
			boolean isOutTransaction,boolean isInTransaction,
			FilterDto filterDto );

	//RiskCountAndRatioDto getCouterPartyCountries(String fromDate, String toDate);
	
	public int getCreditCardLuxuryTransactionsCount(String originatorAccountId,String luxuryShopeNames,Boolean shopOrWebsite,Date date) throws NoResultException;
	
	public int getCreditCardLuxuryTransactions(String originatorAccountId,String beneficaryAccountId);
	
	public int getCreditCardNewCountryATMTransaction(String originatorAccountId,String countryName,Date transactionDate) throws NoResultException; 
	
	
	public List<TransactionMasterView> getMasterDataFromView();

	RiskCountAndRatioDto getRiskAlertsAggregates(String fromDate, String toDate, String value, FilterDto filterDto);

	String queryForAlertAmount(String filterType);

	String queryForTransactionAmount();

	List<RiskCountAndRatioDto> getCounterPartyGeoAggregates(String fromDate, String toDate,FilterDto filterDto,boolean isBankCountry);

	List<RiskCountAndRatioDto> getBankAggregates(String fromDate, String toDate, FilterDto filterDto);
	
	public Double getCreditCardMontlyAverageOfThreeMonthsTransactions(String creditCardAccountNumber,Date date) throws NoResultException;
	
	public Double getCreditCardMontlyAverageOfTransactions(String creditCardAccountNumber,Date date) throws NoResultException;

	List<RiskCountAndRatioDto> getCorruptionRiskAggregates(String fromDate, String toDate, String value,
			FilterDto filterDto);

	List<AlertDashBoardRespDto> fetchAlertsByNumber( String customerNumber);	
	
	
	public List<Double> getCreditCardAtmWithdrawlLastSevenDaysAmount(String creditCardAccountNumber,String transactionCountry,Date transactionDate);
	
	public Double getCreditCardATMCashInAverage(String beneficiaryAccountId,Date transactionDate) throws NoResultException;
	
	public Double getCreditCardATMWithdrawalAverage(String originatorAccountId,Date transactionDate) throws NoResultException;
	
	public int getCreditCardATMWithdrawlsInShortTime(String originatorAccountId,Date transactionDate,String ATMAddress) throws NoResultException;

	List<TransactionsData> getPreviousTransactions(String originatorAccountNumber, String beneficieryAccountNumber);

	Double getCreditCardUnrelatedCounterPartyOneMonthAmount(String originatorAccountId, String beneficiaryAccountId,
			Date businessDate);
	
	Double getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmount(String originatorAccountId, String beneficiaryAccountId,
			Date businessDate);
	
	public List<TransactionsData> getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions(String beneficiaryAccountId,Date businessDate);

	Double getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorName(String originatorName,
			String beneficiaryAccountId, Date businessDate);

	Double getThreeMonthsTransSumWithMerchant(String beneficiaryAccountId, String merchant, Date businessDate);

	Double getOverAllCreditAmount(String beneficiaryAccountId,String merchant);

	Double getOverAllDebitAmount(String beneficiaryAccountId,String merchant);

	int getPreviousTransactionsCountWithMerchant(String beneficiaryAccountId, String merchant, Date businessDate);
	
	List<AlertDashBoardRespDto> fetchAlerts();

	List<AlertDashBoardRespDto> getEntityBasedAlerts(String fromDate, String toDate, Integer pageNumber, Integer recordsPerPage, String orderIn,String entityType);

	long getEntityBasedAlertsCount(String fromDate, String toDate, String name);
	
	public TransactionMasterView updateFetchAlerts(AlertDashBoardRespDto alertDashBoardRespDto);
	

}
