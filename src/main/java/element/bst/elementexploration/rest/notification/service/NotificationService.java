package element.bst.elementexploration.rest.notification.service;

import java.time.LocalDateTime;
import java.util.List;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;

/**
 * @author Viswanath
 *
 */
public interface NotificationService extends GenericService<NotificationAlert, Long> {

	Long createNotificationAlert(NotificationAlert notification);

	String checkInNotifications(Long userId);

	LocalDateTime getLastCheckIn(Long userId);

	List<NotificationAlertDTO> retrieveCaseNotificationAlertBasedOnLastCheckIn(Long caseId, Long userId,
			LocalDateTime lastCheckIn, Integer type, Integer level, LocalDateTime dateTo);

	List<NotificationAlertDTO> getUserNotificationList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo,
			Long caseId, Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy,
			LocalDateTime lastCheckIn, String orderIn);

	NotificationAlertDTO retrieveNotificationDetail(Long notifId, Long userId);

	Long countUserNotificationList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId, Integer type, LocalDateTime lastCheckIn);

	Long createEvent(NotificationAlert notification);

	boolean isEventOwner(Long eventId, Long userId);

	void addEventParticipants(Long eventId, List<Long> participants, Long userId);

	void removeEventParticipants(Long eventId, List<Long> participants, Long userId);

	void updateEvent(NotificationAlert event, NotificationAlert oldEvent);

	List<EventDTO> getUserEventList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	Long countUserEventList(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId, Integer type);

	List<EventDTO> getEventListByCreator(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId,
			Integer type, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	Long countEventListByCreator(Long userId, LocalDateTime dateFrom, LocalDateTime dateTo, Long caseId, Integer type);

	EventDetailDTO retrieveEventDetail(Long eventId, Long userId);

}
