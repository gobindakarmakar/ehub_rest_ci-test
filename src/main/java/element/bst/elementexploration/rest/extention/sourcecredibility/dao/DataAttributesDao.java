package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.DataAttributes;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface DataAttributesDao extends GenericDao<DataAttributes, Long> {

}
