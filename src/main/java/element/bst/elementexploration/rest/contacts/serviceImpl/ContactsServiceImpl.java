package element.bst.elementexploration.rest.contacts.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.contacts.dao.ContactsDao;
import element.bst.elementexploration.rest.contacts.domain.Contact;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.contacts.service.ContactsService;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;

/**
 * @author Amit Patel
 * 
 * */
@Service("contactsService")
public class ContactsServiceImpl extends GenericServiceImpl<Contact, Long> implements ContactsService{

	@Autowired
	public ContactsServiceImpl(GenericDao<Contact, Long> genericDao) {
		super(genericDao);
	}

	
	@Autowired
	private ContactsDao contactsDao;
	
	@Autowired
	private UserDao userDao;
	
	@Override
	@Transactional("transactionManager")
	public List<ContactDTO> getUserContactList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy
			, String orderIn, String keyword) {

		return contactsDao.getUserContactList(userId, pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber, 
    			recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy, orderIn , keyword);
	}

	@Override
	@Transactional("transactionManager")
	public void removeContact(Long contactId, Long userId) {
		contactsDao.removeContact(contactId, userId);
	}

	@Override
	@Transactional("transactionManager")
	public boolean addContact(Long contactId, Long userId) {
		boolean status = true;
		User user = userDao.find(contactId);
		if(user != null){
			if(contactsDao.getUserContact(contactId, userId) == null){
				contactsDao.create(new Contact(contactId, userId));
			}else{
				status = false;
			}
		}else{
			throw new NoDataFoundException(ElementConstants.CONTACT_NOT_FOUND_MSG);
		}
		return status;
	}

	@Override
	@Transactional("transactionManager")
	public Long countUserContactList(Long userId, String keyword) {
		return contactsDao.countUserContactList(userId, keyword);
	}
}
