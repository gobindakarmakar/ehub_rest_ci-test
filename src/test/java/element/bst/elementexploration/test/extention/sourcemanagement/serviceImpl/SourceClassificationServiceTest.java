/*package element.bst.elementexploration.test.extention.sourcemanagement.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.sourcemanagement.dao.CategoryDao;
import element.bst.elementexploration.rest.extention.sourcemanagement.dao.DomainDao;
import element.bst.elementexploration.rest.extention.sourcemanagement.dao.IndustryDao;
import element.bst.elementexploration.rest.extention.sourcemanagement.dao.JurisdictionDao;
import element.bst.elementexploration.rest.extention.sourcemanagement.dao.SourceAttributesDao;
import element.bst.elementexploration.rest.extention.sourcemanagement.dao.SourceClassificationDao;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.Category;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.Domain;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.Industry;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.Jurisdiction;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.SourceAttributes;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.SourceClassification;
import element.bst.elementexploration.rest.extention.sourcemanagement.dto.DomainDto;
import element.bst.elementexploration.rest.extention.sourcemanagement.dto.SourceClassificationDto;
import element.bst.elementexploration.rest.extention.sourcemanagement.serviceimpl.SourceClassificationServiceImpl;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;

*//**
 * @author suresh
 *
 *//*
@RunWith(MockitoJUnitRunner.class)
public class SourceClassificationServiceTest {

	@Mock
	SourceClassificationDao sourceClassificationDao;

	@Mock
	CategoryDao categoryDao;

	@Mock
	SourceAttributesDao sourceAttributesDao;

	@Mock
	IndustryDao industryDao;

	@Mock
	DomainDao domainDao;

	@Mock
	JurisdictionDao jurisdictionDao;

	@Mock
	ApplicationEventPublisher eventPublisher;

	@InjectMocks
	SourceClassificationServiceImpl classificationServiceImpl;

	private SourceClassification sourceClassification;
	
	private Category category;
	
	private Industry industry;
	
	private Domain domain;
	
	private Jurisdiction jurisdiction;
	
	private SourceAttributes sourceAttributes;
	
	private SourceClassificationDto sourceDto;
	
	List<DomainDto> domainDtoList;
	
	List<SourceClassification> sourceClassificationList;

	@Before
	public void setUp() {
		sourceClassificationList = new ArrayList<SourceClassification>();
		sourceClassification = new SourceClassification();
		sourceClassification = new SourceClassification(1L, "BST", "http:yahho.com", "Medium", "finance", "GENERAL",
				true, false, false, false, true, true, null, null, null, null);
		sourceClassificationList.add(sourceClassification);
		category = new Category();
		industry = new Industry();
		domain = new Domain();
		jurisdiction = new Jurisdiction();
		sourceAttributes = new SourceAttributes();
		DomainDto domainDto = new DomainDto(1L, "assert");
		domainDtoList = new ArrayList<DomainDto>();
		domainDtoList.add(domainDto);
		sourceDto = new SourceClassificationDto(1L, "BST", "http:yahho.com", "Medium", "finance", "GENERAL", true,
				false, false, false, true, true, domainDtoList, null, null, null);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void deleteSourceClassificationTest() {
		Mockito.when(sourceClassificationDao.find(Mockito.anyLong())).thenReturn(sourceClassification);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(sourceClassificationDao).delete(Mockito.any(SourceClassification.class));
		boolean status = classificationServiceImpl.deleteSource(1L, null, null, null, null, null, true, false, false,
				false, false, false, null);
		assertEquals(true, status);
	}

	@Test
	public void deleteSourceCategoryTest() {
		Mockito.when(categoryDao.find(Mockito.anyLong())).thenReturn(category);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(categoryDao).delete(Mockito.any(Category.class));
		boolean status = classificationServiceImpl.deleteSource(null, 1L, null, null, null, null, false, true, false,
				false, false, false, null);
		assertEquals(true, status);
	}

	@Test
	public void deleteSourceJurisdictionTest() {
		Mockito.when(jurisdictionDao.find(Mockito.anyLong())).thenReturn(jurisdiction);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(jurisdictionDao).delete(Mockito.any(Jurisdiction.class));
		boolean status = classificationServiceImpl.deleteSource(null, null, null, null, null, 1L, false, false, false,
				false, false, true, null);
		assertEquals(true, status);
	}

	@Test
	public void deleteSourceAttributeTest() {
		Mockito.when(sourceAttributesDao.find(Mockito.anyLong())).thenReturn(sourceAttributes);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(sourceAttributesDao).delete(Mockito.any(SourceAttributes.class));
		boolean status = classificationServiceImpl.deleteSource(null, null, 1L, null, null, null, false, false, true,
				false, false, false, null);
		assertEquals(true, status);
	}

	@Test
	public void deleteSourceDomainTest() {
		Mockito.when(domainDao.find(Mockito.anyLong())).thenReturn(domain);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(domainDao).delete(Mockito.any(Domain.class));
		boolean status = classificationServiceImpl.deleteSource(null, null, null, 1L, null, null, false, false, false,
				true, false, false, null);
		assertEquals(true, status);
	}

	@Test
	public void deleteSourceIndustryTest() {
		Mockito.when(industryDao.find(Mockito.anyLong())).thenReturn(industry);
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(industryDao).delete(Mockito.any(Industry.class));
		boolean status = classificationServiceImpl.deleteSource(null, null, null, null, 1L, null, false, false, false,
				false, true, false, null);
		assertEquals(true, status);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = InsufficientDataException.class)
	public void deleteSourceWhenExceptionThrowTest() {
		Mockito.when(industryDao.find(Mockito.anyLong())).thenThrow(InsufficientDataException.class);
		classificationServiceImpl.deleteSource(null, null, null, null, 1L, null, false, false, false, false, true,
				false, null);
	}

	/*@Test
	public void updateSourceTest() {
		domain.setDomainName("oracle");
		sourceDto.getDomain().get(0).setDomainName("oracle");
		Mockito.doNothing().when(eventPublisher).publishEvent(Mockito.any(LoggingEvent.class));
		Mockito.doNothing().when(sourceClassificationDao).saveOrUpdate(Mockito.any(SourceClassification.class));
		Mockito.when(sourceClassificationDao.find(Mockito.anyLong())).thenReturn(sourceClassification);
		Mockito.when(industryDao.find(Mockito.anyLong())).thenReturn(industry);
		Mockito.when(jurisdictionDao.find(Mockito.anyLong())).thenReturn(jurisdiction);
		Mockito.when(domainDao.find(Mockito.anyLong())).thenReturn(domain);
		Mockito.when(categoryDao.find(Mockito.anyLong())).thenReturn(category);
		boolean status = classificationServiceImpl.updateSource(sourceDto, 1L);
		assertEquals(true, status);
	}

	@Test
	public void getSourcesTest() throws IllegalAccessException, InvocationTargetException {
		Mockito.when(sourceClassificationDao.getSources(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyString(),
				Mockito.anyBoolean(), Mockito.anyString())).thenReturn(sourceClassificationList);
		List<SourceClassificationDto> list = classificationServiceImpl.getSources(1, 10, null, true, null);
		assertEquals(list.size(), sourceClassificationList.size());
	}

}
*/