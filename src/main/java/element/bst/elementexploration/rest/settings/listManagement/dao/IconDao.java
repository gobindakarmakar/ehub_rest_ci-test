package element.bst.elementexploration.rest.settings.listManagement.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.Icon;

public interface IconDao extends GenericDao<Icon, Long>{

}
