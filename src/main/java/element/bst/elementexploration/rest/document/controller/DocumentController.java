package element.bst.elementexploration.rest.document.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocumentUploadResponse;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.document.dto.DocCommentResponseDto;
import element.bst.elementexploration.rest.document.dto.DocCommentVo;
import element.bst.elementexploration.rest.document.dto.DocumentDto;
import element.bst.elementexploration.rest.document.dto.DocumentUserAndCaseDto;
import element.bst.elementexploration.rest.document.dto.DocumentVaultDTO;
import element.bst.elementexploration.rest.document.service.DocCommentService;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "Document API" })
@RestController
@RequestMapping("/api/documentStorage")
public class DocumentController extends BaseController {

	@Autowired
	private DocumentService documentService;

	@Autowired
	private DocCommentService docCommentService;

	@Autowired
	private DocumentQuestionsService documentQuestionsService;

	@ApiOperation("Upload Document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocumentUploadResponse.class, message = "File Uploaded Successfully"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.INVALID_DOC_FLAG_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/uploadDocument", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadDocumentDetails(@RequestParam MultipartFile uploadFile,
			@RequestParam String fileTitle, @RequestParam String remarks, @RequestParam("token") String token,
			@RequestParam(required = false) Integer docFlag, @RequestParam(required = false) Long templateId,
			@RequestParam(required = false) String entityId ,@RequestParam(required = false) String entityName,@RequestParam(required = false) String docSection,
			@RequestParam(required = false) String entitySource,HttpServletRequest request, HttpServletResponse response) throws DocNotFoundException, Exception {

		if (docFlag != null && (docFlag < 0 || docFlag > 255)) {
			throw new BadRequestException(ElementConstants.INVALID_DOC_FLAG_MSG);
		}
		long userIdTemp = getCurrentUserId();
		Long docId = documentService.uploadDocument(uploadFile, fileTitle, remarks, userIdTemp, docFlag, templateId,entityId,entityName,entitySource,docSection,null);
		CaseDocumentUploadResponse caseDocumentUploadResponse = new CaseDocumentUploadResponse();
		caseDocumentUploadResponse.setDocId(docId);
		/*
		 * JSONObject json = new JSONObject(); json.put("docId", docId);
		 */
		return new ResponseEntity<>(caseDocumentUploadResponse, HttpStatus.OK);
	}

	@ApiOperation("Update Document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/updateDocument", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateDocument(@Valid @RequestBody DocumentDto documentDto, BindingResult results,
			@RequestParam Long docId, @RequestParam("token") String token, HttpServletRequest request) {

		long userIdTemp = getCurrentUserId();
		if (!results.hasErrors()) {
			documentService.updateDocumentById(documentDto, docId, userIdTemp);
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_UPDATED_SUCCESSFULLY_MSG),
					HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Update DocumentContent")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "File can not be empty."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/updateDocumentContent", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
	"application/json; charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<?> updateDocumentContent(@RequestParam("uploadFile") MultipartFile uploadFile,
			@RequestParam("docId") Long docId, @RequestParam("token") String token, HttpServletRequest request)
					throws Exception {

		long userIdTemp = getCurrentUserId();
		documentService.updateDocumentContent(uploadFile, userIdTemp, docId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_UPDATED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("Soft Delete Document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/softDeleteDocument", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> softDeleteDocument(@RequestParam Long docId, @RequestParam("token") String token,
			HttpServletRequest request) {

		long userId = getCurrentUserId();
		documentService.softDeleteDocumentById(docId, userId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("Share Document With Case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_SHARED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found.") })
	@PostMapping(value = "/shareDocumentWithCase", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> shareDocumentWithCaseseed(@RequestParam String token, @RequestParam Long docId,
			@RequestParam Long caseId, HttpServletRequest request) {

		long userIdTemp = getCurrentUserId();
		documentService.shareDocumentWithCaseseed(userIdTemp, docId, caseId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_SHARED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("UnShare Document With Case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_UNSHARED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Case not found.") })
	@PostMapping(value = "/unshareDocumentWithCase", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> unshareDocumentFromCaseseed(@RequestParam String token, @RequestParam Long docId,
			@RequestParam Long caseId, HttpServletRequest request) {

		long userIdTemp = getCurrentUserId();
		documentService.unshareDocumentFromCaseseed(userIdTemp, docId, caseId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_UNSHARED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("Share Document With User")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_SHARED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Invalid permission value."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Invalid document id or document is deleted.") })
	@PostMapping(value = "/shareDocumentWithUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> shareDocumetWithUser(@RequestParam Long userId, @RequestParam String token,
			@RequestParam Long docId, @RequestParam Integer permission, HttpServletRequest request) {

		long shareByUserId = getCurrentUserId();
		documentService.shareDocWithUser(userId, shareByUserId, docId, permission);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_SHARED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("UnShare Document With User")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_UNSHARED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"), })
	@PostMapping(value = "/unshareDocumentWithUser", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> unShareDocumetFromUser(@RequestParam Long userId, @RequestParam String token,
			@RequestParam Long docId, @RequestParam String comment, HttpServletRequest request) {

		long unsharedByUseId = getCurrentUserId();
		documentService.unShareDocFromUser(userId, unsharedByUseId, docId, comment);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_UNSHARED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("My Documents")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/myDocuments", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> myDocuments(@RequestParam String token, @RequestParam(required = false) Long caseId,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer docFlag,@RequestParam(required = false) String entityId,@RequestParam(required = false) String docSection,@RequestParam(required = false) Boolean isAllRequired, HttpServletRequest request) {

		Long userId = getCurrentUserId();
		List<DocumentVaultDTO> list = documentService.myDocuments(userId, caseId, docFlag, pageNumber, recordsPerPage,
				orderBy, orderIn,entityId,docSection,isAllRequired);
		for (DocumentVaultDTO documentVaultDTO : list) {
			if (documentVaultDTO.getDocumentParsingStatus().equals("completed")) {
				documentVaultDTO.setNoOfQuestions(documentQuestionsService.findQuestions(documentVaultDTO.getDocId()));
				documentVaultDTO.setHasConflicts(false);// static
			}

		}
		long totalResults = documentService.countMyDocuments(userId, caseId, docFlag,entityId,docSection);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;

		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myDocumentResult = new JSONObject();
		 * myDocumentResult.put("result", list);
		 * myDocumentResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(list);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Full Text Search My Documents")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/fullTextSearchMyDocuments", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fullTextSearchMyDocuments(@RequestParam String token, @RequestParam String keyword,
			@RequestParam(required = false) Long caseId, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer docFlag,
			@RequestParam(required = false) String entityId,@RequestParam(required = false) String docSection,HttpServletRequest request) {

		long userId = getCurrentUserId();
		List<DocumentVaultDTO> list = documentService.fullTextSearchMyDocuments(userId, caseId, keyword, docFlag,
				orderBy, orderIn, pageNumber, recordsPerPage, entityId,docSection);
		long totalResults = documentService.countFullTextSearchMyDocuments(userId, caseId, keyword, docFlag,entityId,docSection);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result", list);
		 * jsonObject.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(list);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Get all my shared documents")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/sharedDocument", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllSharedDocument(@RequestParam String token,
			@RequestParam(required = false) Long caseId, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, @RequestParam(required = false) Integer docFlag,
			HttpServletRequest request) {

		long userId = getCurrentUserId();
		// JSONObject json = new JSONObject();
		List<DocumentVaultDTO> mappingDtos = documentService.getAllMySharedDocument(userId, caseId, pageNumber,
				recordsPerPage, orderBy, orderIn, docFlag);

		Long result = documentService.countGetAllMySharedDocument(userId, caseId, docFlag);
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(result);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(mappingDtos != null ? mappingDtos.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (result - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * json.put("result", mappingDtos); json.put("paginationInformation",
		 * information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(mappingDtos);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Full text search my shared documents")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation Successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/fullTextSearchDocument", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fullTextSearchDocument(@RequestParam String token, @RequestParam String searchKeyWord,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			@RequestParam(required = false) Long caseId, @RequestParam(required = false) Integer docFlag,
			HttpServletRequest request) {

		long userId = getCurrentUserId();
		List<DocumentVaultDTO> list = documentService.fullTextSearchDocument(userId, searchKeyWord, pageNumber,
				recordsPerPage, orderBy, orderIn, caseId, docFlag);

		// Pagination information
		Long totalResults = documentService.countFullTextSearchDocument(userId, searchKeyWord, caseId, docFlag);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * JSONObject myDocumentResult = new JSONObject();
		 * myDocumentResult.put("result", list);
		 * myDocumentResult.put("paginationInformation", information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(list);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Get case mapping")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = DocumentUserAndCaseDto.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getCaseMapping", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseSeedFromCommonDocument(@RequestParam String token, @RequestParam Long docId,
			HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {

		long userId = getCurrentUserId();
		DocumentUserAndCaseDto documentUserAndCaseDto = documentService.getCaseseedFromDocument(userId, docId);
		if (documentUserAndCaseDto.getDocumentVault() == null)
			return new ResponseEntity<>(new ResponseMessage("Permission is denied or Document not found"),
					HttpStatus.OK);
		else
			return new ResponseEntity<>(documentUserAndCaseDto, HttpStatus.OK);
	}

	@ApiOperation("Download Document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	@CrossOrigin(origins = "*", methods = RequestMethod.GET)
	@GetMapping(value = "/downloadDocument", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<StreamingResponseBody> downloadDocumentDetails(HttpServletResponse response,
			@RequestParam("docId") Long docId, @RequestParam("token") String userId,
			HttpServletRequest request)
					throws DocNotFoundException, Exception {
		Long userIdTemp = null;
		//long userIdTemp = getCurrentUserId();//ToDo::Enable when we need user permission check..
		DocumentVault documentVault = documentService.find(docId);
		if (documentVault != null) {
			byte[] fileData = documentService.downloadDocument(docId, userIdTemp, response);
			ServiceCallHelper.setDownloadResponse(response, documentVault.getDocName(), documentVault.getType(),
					documentVault.getSize() != null ? documentVault.getSize().intValue() : 0);
			StreamingResponseBody responseBody = new StreamingResponseBody() {
				@Override
				public void writeTo(OutputStream out) throws IOException {
					out.write(fileData);
					out.flush();
				}
			};
			request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}

	@ApiOperation("Get documnet details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = DocumentVaultDTO.class, message = "Operation Successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getDocumentDetails", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDocumentDetails(@RequestParam Long docId, @RequestParam("token") String userId,
			HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {

		long userIdTemp = getCurrentUserId();
		if (docId != null) {
			DocumentVaultDTO documentDetails = documentService.getDocumentDetails(docId, userIdTemp);
			if (documentDetails.getDocumentParsingStatus().equals("completed")) {
				documentDetails.setNoOfQuestions(documentQuestionsService.findQuestions(documentDetails.getDocId()));
				documentDetails.setHasConflicts(false);// static
			}
			return new ResponseEntity<>(documentDetails, HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Add comment to document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = DocCommentResponseDto.class, message = "Operation Successful."),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/addComment", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> commentDocument(@RequestParam String token, @Valid @RequestBody DocCommentVo docCommentDto,
			BindingResult results, @RequestParam Long docId, HttpServletRequest request) {

		long userId = getCurrentUserId();
		if (!results.hasErrors()) {
			docCommentDto.setDocId(docId);
			Long id = documentService.commentDocument(userId, docCommentDto);
			DocCommentResponseDto docCommentResponseDto = new DocCommentResponseDto();
			docCommentResponseDto.setId(id);
			/*
			 * JSONObject jsonObject = new JSONObject(); jsonObject.put("id", id);
			 */
			return new ResponseEntity<>(docCommentResponseDto, HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.DOCUMENT_ID_NULL);
		}

	}

	@ApiOperation("Edit document comment")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.COMMENT_UPDATED_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Comment not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/{commentId}/editComment", produces = { "application/json; charset=UTF-8" }, consumes = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> editComment(@PathVariable Long commentId, @RequestParam String token,
			@Valid @RequestBody DocCommentVo docCommentDto, BindingResult results, HttpServletRequest request) {

		long userId = getCurrentUserId();
		if (!results.hasErrors()) {
			docCommentService.editDocmentComment(commentId, userId, docCommentDto);
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.COMMENT_UPDATED_MSG), HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Get list of comments")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = DocumentVaultDTO.class, message = "Operation successful."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have permission to view comment on this document"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/listComments", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listDocComment(@RequestParam String token, @RequestParam Long docId,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderIn, HttpServletRequest request) {

		long userIdTemp = getCurrentUserId();
		// JSONObject json = new JSONObject();
		List<DocCommentDTO> mappingDtos = documentService.listDocComment(userIdTemp, docId, pageNumber, recordsPerPage,
				orderIn);
		long result = documentService.countDocComment(userIdTemp, docId);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(result);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(mappingDtos != null ? mappingDtos.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (result - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*
		 * json.put("result", mappingDtos); json.put("paginationInformation",
		 * information);
		 */
		ResponseUtil responseUtil = new ResponseUtil();
		responseUtil.setResult(mappingDtos);
		responseUtil.setPaginationInformation(information);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}

	@ApiOperation("Document Dissemination")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DISSEMINATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Either document or case is not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/documentDissemination", produces = { "application/json; charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<?> documentDissemination(@RequestParam Long docId, @RequestParam("token") String token,
			@RequestParam Long caseId, HttpServletRequest request) {

		long userIdTemp = getCurrentUserId();
		documentService.documentDissemination(docId, userIdTemp, caseId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_DISSEMINATED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}

	@ApiOperation("Upload Screen Shot Url")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocumentUploadResponse.class, message = "File Uploaded Successfully"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.INVALID_DOC_FLAG_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@GetMapping(value = "/uploadDocumentByScreenShotURL", produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadDocumentByScreenShotURL(@RequestParam(required = false) String url,
			@RequestParam String fileTitle, @RequestParam String remarks, @RequestParam("token") String token,
			@RequestParam(required = false) Integer docFlag, @RequestParam(required = false) Long templateId,
			@RequestParam(required = false) String entityId ,@RequestParam(required = false) String entityName,@RequestParam(required = false) String entitySource,
			@RequestParam(required = false) String docSection,@RequestParam(required = false) Long docID,
			HttpServletRequest request, HttpServletResponse response) throws DocNotFoundException, Exception {

		if (docFlag != null && (docFlag < 0 || docFlag > 255)) {
			throw new BadRequestException(ElementConstants.INVALID_DOC_FLAG_MSG);
		}
		long userIdTemp = getCurrentUserId();
		Long docId = documentService.uploadDocumentByScreenShotURL(url, fileTitle, remarks, userIdTemp, docFlag, templateId, entityId, entityName,entitySource, docSection, new File(""),docID);
		CaseDocumentUploadResponse caseDocumentUploadResponse = new CaseDocumentUploadResponse();
		caseDocumentUploadResponse.setDocId(docId);
		return new ResponseEntity<>(caseDocumentUploadResponse, HttpStatus.OK);
	}
}
