package element.bst.elementexploration.rest.extention.significantnews.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantCommentDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface SignificantCommentService extends GenericService<SignificantComment, Long> {

	public List<SignificantCommentDto> getSignificantComments(String classification, Long significantId);

}
