package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_TABLE_COLUMNS")
public class DocumentTableColumns implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String columnName;
	private long parentId;
	private DocumentContents documentContent;
	private List<DocumentTableRows> documentTableRows;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "DOCUMENT_CONTENT_ID")
	public DocumentContents getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(DocumentContents documentContent) {
		this.documentContent = documentContent;
	}

	@Column(name = "COLUMN_NAME")
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@Column(name = "PARENT_ID")
	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "documentTableColumns")
	public List<DocumentTableRows> getDocumentTableRows() {
		return documentTableRows;
	}

	public void setDocumentTableRows(List<DocumentTableRows> documentTableRows) {
		this.documentTableRows = documentTableRows;
	}
	
	

}
