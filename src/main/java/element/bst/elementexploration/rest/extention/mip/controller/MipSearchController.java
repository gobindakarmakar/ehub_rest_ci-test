package element.bst.elementexploration.rest.extention.mip.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.extention.mip.domain.MipSearchHistory;
import element.bst.elementexploration.rest.extention.mip.dto.MipResponseDto;
import element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO;
import element.bst.elementexploration.rest.extention.mip.service.MipSearchHistoryService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "MIP search API" }, description = "Manages MIP data")
@RestController
@RequestMapping("/api/mip/search")
public class MipSearchController extends BaseController {

	@Autowired
	private MipSearchHistoryService mipSearchHistoryService;

	@ApiOperation("Saves search history")
	@ApiResponses(value = { @ApiResponse(code = 200, response = MipResponseDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveSearchHistory", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSearchHistory(@RequestParam("token") String token,
			@Valid @RequestBody MipSearchHistory bstSearchHistory, BindingResult results, HttpServletRequest request) {
		bstSearchHistory.setCreatedOn(new Date());
		bstSearchHistory.setUserId(getCurrentUserId());
		MipSearchHistory newBstSearchHistory = mipSearchHistoryService.save(bstSearchHistory);
		MipResponseDto mipResponseDto = new MipResponseDto();
		mipResponseDto.setSearchId(newBstSearchHistory.getSearchId());
		/*
		 * JSONObject json = new JSONObject(); json.put("searchId",
		 * newBstSearchHistory.getSearchId());
		 */
		return new ResponseEntity<>(mipResponseDto, HttpStatus.OK);
	}

	@ApiOperation("Gets search history list")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseUtil.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getSearchHistoryList", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSearchHistoryList(@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {
		List<MipSearchHistoryDTO> searchHistoryList = mipSearchHistoryService.getSearchHistoryList(getCurrentUserId(),
				pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = mipSearchHistoryService.countSearchHistoryList(getCurrentUserId());
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(searchHistoryList != null ? searchHistoryList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		ResponseUtil resultantObject = new ResponseUtil();
		resultantObject.setResult(searchHistoryList);
		resultantObject.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * searchHistoryList); jsonObject.put("paginationInformation",
		 * information);
		 */
		return new ResponseEntity<>(resultantObject, HttpStatus.OK);
	}

	@ApiOperation("Updates search history")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.SEARCH_HISTORY_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateSearchHistory", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateSearchHistory(@RequestParam("searchId") Long searchId,
			@RequestParam("token") String token, @Valid @RequestBody MipSearchHistory bstSearchHistory,
			HttpServletRequest request) {
		MipSearchHistory bstSearchHistoryBeforeUpdate = mipSearchHistoryService.find(searchId);
		if (bstSearchHistoryBeforeUpdate != null) {
			if (mipSearchHistoryService.isSearchHistoryOwner(bstSearchHistory.getSearchId(), getCurrentUserId())) {
				bstSearchHistoryBeforeUpdate.setUpdatedOn(new Date());
				bstSearchHistoryBeforeUpdate.setName(bstSearchHistory.getName() != null ? bstSearchHistory.getName()
						: bstSearchHistoryBeforeUpdate.getName());
				bstSearchHistoryBeforeUpdate.setData(bstSearchHistory.getData() != null ? bstSearchHistory.getData()
						: bstSearchHistoryBeforeUpdate.getData());
				mipSearchHistoryService.saveOrUpdate(bstSearchHistoryBeforeUpdate);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_HISTORY_UPDATE_MSG),
						HttpStatus.OK);
			} else {
				throw new PermissionDeniedException();
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_HISTORY_NOT_FOUND_MSG),
					HttpStatus.NOT_FOUND);
		}

	}

	@ApiOperation("Updates search history")
	@ApiResponses(value = { @ApiResponse(code = 200, response = MipSearchHistoryDTO.class, message = "Operation successful"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/findSearchHistoryById", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSearchHistoryById(@RequestParam("searchId") Long searchId,
			@RequestParam("token") String token, HttpServletRequest request) {

		MipSearchHistory bstSearchHistory = mipSearchHistoryService.find(searchId);
		if (bstSearchHistory != null) {
			MipSearchHistoryDTO bstSearchHistoryDto = new MipSearchHistoryDTO();
			BeanUtils.copyProperties(bstSearchHistory, bstSearchHistoryDto);
			return new ResponseEntity<>(bstSearchHistoryDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_HISTORY_NOT_FOUND_MSG),
					HttpStatus.NOT_FOUND);
		}
	}
}
