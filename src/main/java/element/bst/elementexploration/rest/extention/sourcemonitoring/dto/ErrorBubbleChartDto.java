package element.bst.elementexploration.rest.extention.sourcemonitoring.dto;

import java.io.Serializable;

/**
 * @author Prateek Maurya
 */

public class ErrorBubbleChartDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int errorCount;
	private String date;
	
	public int getErrorCount() {
		return errorCount;
	}
	public void setErrorCount(int errorCount2) {
		this.errorCount = errorCount2;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	
}
