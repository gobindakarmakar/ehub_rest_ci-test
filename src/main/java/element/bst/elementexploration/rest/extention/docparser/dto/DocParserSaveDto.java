package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author suresh
 *
 */
public class DocParserSaveDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<DocParserContent> documentContentDto;
	
	public List<DocParserContent> getDocumentContentDto() {
		return documentContentDto;
	}
	public void setDocumentContentDto(List<DocParserContent> documentContentDto) {
		this.documentContentDto = documentContentDto;
	}

	

}
