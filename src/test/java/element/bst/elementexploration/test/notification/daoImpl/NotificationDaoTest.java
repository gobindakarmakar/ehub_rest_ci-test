package element.bst.elementexploration.test.notification.daoImpl;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.notification.daoImpl.NotificationDaoImpl;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.dto.EventDTO;
import element.bst.elementexploration.rest.notification.dto.EventDetailDTO;
import element.bst.elementexploration.rest.notification.dto.NotificationAlertDTO;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NotificationDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<NotificationAlert> root;

	@InjectMocks
	NotificationDaoImpl notificationDao;

	List<NotificationAlertDTO> notificationList = new ArrayList<NotificationAlertDTO>();

	NotificationAlertDTO notificationAlertDTO;

	NotificationAlert notificationAlert;

	List<EventDTO> eventList = new ArrayList<EventDTO>();

	EventDetailDTO eventDetailDTO;

	@Before
	public void setupMock() {
		notificationAlert = new NotificationAlert(1L, "subject", "body", 1L, new ArrayList<Long>());
		notificationAlertDTO = new NotificationAlertDTO(1L, "subject", "body", 1, LocalDateTime.now(),
				LocalDateTime.now(), 1, 1L, 1L, 1);
		notificationList.add(notificationAlertDTO);
		notificationList.add(new NotificationAlertDTO());
		eventList.add(new EventDTO());
		eventList.add(new EventDTO());
		eventDetailDTO = new EventDetailDTO(1L, "subject", "body", LocalDateTime.now(), LocalDateTime.now(),
				LocalDateTime.now(), 1, 1, 1L, 1L, 1);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void retrieveCaseNotificationAlertBasedOnLastCheckInTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao
				.retrieveCaseNotificationAlertBasedOnLastCheckIn(1L, 1L, LocalDateTime.now(), 1, 1,
						LocalDateTime.now());
		assertEquals(2, notificationDtoList.size());
	}

	@Test
	public void retrieveCaseNotificationAlertBasedOnLastCheckIn_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao
				.retrieveCaseNotificationAlertBasedOnLastCheckIn(1L, 1L, null, 1, 1, null);
		assertEquals(2, notificationDtoList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void retrieveCaseNotificationAlertBasedOnLastCheckIn_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.retrieveCaseNotificationAlertBasedOnLastCheckIn(1L, 1L, null, 1, 1, null);
	}

	@Test
	public void getUserNotificationListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao.getUserNotificationList(1L,
				LocalDateTime.now(), LocalDateTime.now(), 1L, 1, 1, 10, "desc", LocalDateTime.now(), null);
		assertEquals(2, notificationDtoList.size());
	}

	@Test
	public void getUserNotificationList_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao.getUserNotificationList(1L,
				LocalDateTime.now(), null, 1L, 1, 1, 10, "desc", LocalDateTime.now(), null);
		assertEquals(2, notificationDtoList.size());
	}

	@Test
	public void getUserNotificationList_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao.getUserNotificationList(1L, null,
				LocalDateTime.now(), 1L, 1, 1, 10, "desc", LocalDateTime.now(), null);
		assertEquals(2, notificationDtoList.size());
	}

	@Test
	public void getUserNotificationList_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao.getUserNotificationList(1L, null, null, 1L, 1,
				1, 10, "desc", LocalDateTime.now(), null);
		assertEquals(2, notificationDtoList.size());
	}

	@Test
	public void getUserNotificationList_whenCheckInDateIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<NotificationAlertDTO> notificationDtoList = notificationDao.getUserNotificationList(1L, null, null, 1L, 1,
				1, 10, "desc", null, "level");
		assertEquals(2, notificationDtoList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getUserNotificationList_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.getUserNotificationList(1L, null, null, 1L, 1, 1, 10, "desc", null, null);
	}

	@Test
	public void retrieveNotificationDetailTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(notificationAlertDTO);
		NotificationAlertDTO notificationDtoObj = notificationDao.retrieveNotificationDetail(1L, 1L);
		assertEquals(new Long("1"), notificationDtoObj.getCaseId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void retrieveNotificationDetail_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		NotificationAlertDTO notificationDtoObj = notificationDao.retrieveNotificationDetail(1L, 1L);
		assertEquals(null, notificationDtoObj);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void retrieveNotificationDetail_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		NotificationAlertDTO notificationDtoObj = notificationDao.retrieveNotificationDetail(1L, 1L);
		assertEquals(null, notificationDtoObj);
	}

	@Test
	public void countUserNotificationListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserNotificationList(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1,
				LocalDateTime.now());
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserNotificationList_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserNotificationList(1L, LocalDateTime.now(), null, 1L, 1,
				LocalDateTime.now());
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserNotificationList_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserNotificationList(1L, null, LocalDateTime.now(), 1L, 1,
				LocalDateTime.now());
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserNotificationList_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserNotificationList(1L, null, null, 1L, 1, LocalDateTime.now());
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserNotificationList_whenCheckInDateIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserNotificationList(1L, null, null, 1L, 1, null);
		assertEquals(new Long("2"), count);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countUserNotificationList_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.countUserNotificationList(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1,
				LocalDateTime.now());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void isEventOwnerTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationAlert.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(notificationAlert);
		boolean status = notificationDao.isEventOwner(1L, 1L);
		assertEquals(true, status);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void isEventOwner_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationAlert.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		boolean status = notificationDao.isEventOwner(1L, 1L);
		assertEquals(false, status);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void isEventOwner_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(NotificationAlert.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.isEventOwner(1L, 1L);
	}

	@Test
	public void getUserEventListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getUserEventList(1L, LocalDateTime.now(), LocalDateTime.now(), 1L,
				1, 1, 10, "level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@Test
	public void getUserEventList_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getUserEventList(1L, LocalDateTime.now(), null, 1L, 1, 1, 10,
				"level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@Test
	public void getUserEventList_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getUserEventList(1L, null, LocalDateTime.now(), 1L, 1, 1, 10,
				"level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@Test
	public void getUserEventList_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getUserEventList(1L, null, null, 1L, 1, 1, 10, "level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getUserEventList_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.getUserEventList(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1, 1, 10, "level", "desc");
	}

	@Test
	public void countUserEventListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserEventList(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserEventList_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserEventList(1L, LocalDateTime.now(), null, 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserEventList_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserEventList(1L, null, LocalDateTime.now(), 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countUserEventList_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countUserEventList(1L, null, null, 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countUserEventList_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.countUserEventList(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1);
	}

	@Test
	public void getEventListByCreatorTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getEventListByCreator(1L, LocalDateTime.now(),
				LocalDateTime.now(), 1L, 1, 1, 10, "level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@Test
	public void getEventListByCreator_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getEventListByCreator(1L, LocalDateTime.now(), null, 1L, 1, 1, 10,
				"level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@Test
	public void getEventListByCreator_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getEventListByCreator(1L, null, LocalDateTime.now(), 1L, 1, 1, 10,
				"level", "desc");
		assertEquals(2, eventDtoList.size());
	}

	@Test
	public void getEventListByCreator_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		List<EventDTO> eventDtoList = notificationDao.getEventListByCreator(1L, null, null, 1L, 1, 1, 10, "level",
				"desc");
		assertEquals(2, eventDtoList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getEventListByCreator_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.getEventListByCreator(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1, 1, 10, "level",
				"desc");
	}

	@Test
	public void countEventListByCreatorTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countEventListByCreator(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countEventListByCreator_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countEventListByCreator(1L, LocalDateTime.now(), null, 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countEventListByCreator_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countEventListByCreator(1L, null, LocalDateTime.now(), 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@Test
	public void countEventListByCreator_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(notificationList);
		Long count = notificationDao.countEventListByCreator(1L, null, null, 1L, 1);
		assertEquals(new Long("2"), count);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void countEventListByCreator_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		notificationDao.countEventListByCreator(1L, LocalDateTime.now(), LocalDateTime.now(), 1L, 1);
	}

	@Test
	public void retrieveEventDetailTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(eventDetailDTO);
		EventDetailDTO eventDetailDto = notificationDao.retrieveEventDetail(1L, 1L);
		assertEquals(new Long("1"), eventDetailDto.getNotificationId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void retrieveEventDetail_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class).thenReturn(eventDetailDTO);
		EventDetailDTO eventDetailDto = notificationDao.retrieveEventDetail(1L, 1L);
		assertEquals(new Long("1"), eventDetailDto.getNotificationId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void retrieveEventDetail_whenNoResultExceptionThrow_findEventByOwnerNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class).thenThrow(NoResultException.class);
		EventDetailDTO eventDetailDto = notificationDao.retrieveEventDetail(1L, 1L);
		assertEquals(null, eventDetailDto);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void retrieveEventDetail_whenNoResultExceptionThrow_findEventByOwnerFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class)
				.thenThrow(FailedToExecuteQueryException.class);
		notificationDao.retrieveEventDetail(1L, 1L);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void retrieveEventDetail_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		EventDetailDTO eventDetailDto = notificationDao.retrieveEventDetail(1L, 1L);
		assertEquals(null, eventDetailDto);
	}

}
