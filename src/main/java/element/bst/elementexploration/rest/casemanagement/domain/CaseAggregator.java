package element.bst.elementexploration.rest.casemanagement.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class CaseAggregator implements Serializable{

	/**
	 * @author viswanath
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("period")
	private String period;
		
	@ApiModelProperty("Number of case in submitted status")
	private Long submitted;
	
	@ApiModelProperty("Number of case in acknowledge status")
	private Long acknowledge;
	
	@ApiModelProperty("Number of case in paused status")
	private Long paused;

	@ApiModelProperty("Number of case in accepted status")
	private Long accepted;

	@ApiModelProperty("Number of case in rejected status")
	private Long rejected;

	@ApiModelProperty("Number of case in focus status")
	private Long focus;
	
	@ApiModelProperty("Number of case in resolved status")
	private Long resolved;
	
	@ApiModelProperty("Number of case in canceled status")
	private Long canceled;

	public CaseAggregator(String period, Long submitted, Long acknowledge, Long paused, Long accepted,
			Long rejected, Long focus, Long resolved, Long canceled) {
		super();
		this.period = period;
		this.submitted = submitted;
		this.acknowledge = acknowledge;
		this.paused = paused;
		this.accepted = accepted;
		this.rejected = rejected;
		this.focus = focus;
		this.resolved = resolved;
		this.canceled = canceled;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Long getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Long submitted) {
		this.submitted = submitted;
	}

	public Long getAcknowledge() {
		return acknowledge;
	}

	public void setAcknowledge(Long acknowledge) {
		this.acknowledge = acknowledge;
	}

	public Long getPaused() {
		return paused;
	}

	public void setPaused(Long paused) {
		this.paused = paused;
	}

	public Long getAccepted() {
		return accepted;
	}

	public void setAccepted(Long accepted) {
		this.accepted = accepted;
	}

	public Long getRejected() {
		return rejected;
	}

	public void setRejected(Long rejected) {
		this.rejected = rejected;
	}

	public Long getFocus() {
		return focus;
	}

	public void setFocus(Long focus) {
		this.focus = focus;
	}

	public Long getResolved() {
		return resolved;
	}

	public void setResolved(Long resolved) {
		this.resolved = resolved;
	}

	public Long getCanceled() {
		return canceled;
	}

	public void setCanceled(Long canceled) {
		this.canceled = canceled;
	}

	@Override
	public String toString() {
		return "CaseAggregator [period=" + period + ", submitted=" + submitted + ", acknowledge=" + acknowledge
				+ ", paused=" + paused + ", accepted=" + accepted + ", rejected=" + rejected + ", focus=" + focus
				+ ", resolved=" + resolved + ", canceled=" + canceled + "]";
	}
	
}
