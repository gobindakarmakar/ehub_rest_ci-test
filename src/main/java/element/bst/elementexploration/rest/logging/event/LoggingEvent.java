package element.bst.elementexploration.rest.logging.event;

import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationEvent;

import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;

public class LoggingEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	private LoggingEventType eventType;

	private Long authorId;

	private Date eventTime;

	private Long analystId;

	private String oldValue;

	private String newValue;

	private CredibilityEnums oldCredibilityEnums;

	private CredibilityEnums newCredibilityEnums;

	private List<LoggingEvent> loggingEventList;

	public LoggingEvent(Object source, CredibilityEnums oldCredibilityEnums, CredibilityEnums newCredibilityEnums,
			LoggingEventType eventType, Long authorId, Date eventTime) {
		super(source);
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
		this.oldCredibilityEnums = oldCredibilityEnums;
		this.newCredibilityEnums = newCredibilityEnums;
	}

	public LoggingEvent(Object source, String newValue, LoggingEventType eventType, Long authorId, Date eventTime) {
		super(source);
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
		this.newValue = newValue;
	}

	public LoggingEvent(Object source, String oldValue, String newValue, LoggingEventType eventType, Long authorId,
			Date eventTime) {
		super(source);
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
		this.newValue = newValue;
		this.oldValue = oldValue;
	}

	public LoggingEvent(Object source, LoggingEventType eventType, Long authorId, Date eventTime) {
		super(source);
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
	}

	public LoggingEvent(Object source, LoggingEventType eventType, Long authorId, Date eventTime, Long analystId) {
		super(source);
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
		this.analystId = analystId;
	}

	public LoggingEvent(Object source, LoggingEventType eventType, Long authorId, Date eventTime, String oldValue,
			String newValue) {
		super(source);
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public LoggingEvent(Object source, List<LoggingEvent> loggingEventList, LoggingEventType eventType, Long authorId,
			Date eventTime) {
		super(source);
		this.loggingEventList = loggingEventList;
		this.eventType = eventType;
		this.authorId = authorId;
		this.eventTime = eventTime;
	}

	public LoggingEventType getEventType() {
		return eventType;
	}

	public void setEventType(LoggingEventType eventType) {
		this.eventType = eventType;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public Long getAnalystId() {
		return analystId;
	}

	public void setAnalystId(Long analystId) {
		this.analystId = analystId;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public CredibilityEnums getOldCredibilityEnums() {
		return oldCredibilityEnums;
	}

	public void setOldCredibilityEnums(CredibilityEnums oldCredibilityEnums) {
		this.oldCredibilityEnums = oldCredibilityEnums;
	}

	public CredibilityEnums getNewCredibilityEnums() {
		return newCredibilityEnums;
	}

	public void setNewCredibilityEnums(CredibilityEnums newCredibilityEnums) {
		this.newCredibilityEnums = newCredibilityEnums;
	}

	public List<LoggingEvent> getLoggingEventList() {
		return loggingEventList;
	}

	public void setLoggingEventList(List<LoggingEvent> loggingEventList) {
		this.loggingEventList = loggingEventList;
	}

}
