package element.bst.elementexploration.rest.logging.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.domain.AuditTrail;
import element.bst.elementexploration.rest.logging.service.AuditTrailService;

/**
 * 
 * @author Amit Patel
 *
 */
@Service("auditTrailService")
public class AuditTrailServiceImpl extends GenericServiceImpl<AuditTrail, Long> implements AuditTrailService{

	@Autowired
	public AuditTrailServiceImpl(GenericDao<AuditTrail, Long> genericDao) {
		super(genericDao);
	}
}
