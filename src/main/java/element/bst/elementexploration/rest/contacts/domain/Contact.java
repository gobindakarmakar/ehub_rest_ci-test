package element.bst.elementexploration.rest.contacts.domain;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author Amit
 * 
 * */
@Entity
@Table(name = "bst_user_contacts")
@Access(AccessType.FIELD)
public class Contact implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private Long id;
	
	@Column(name = "contactId")
	private Long contactId;
	
	@Column(name = "userId")
	private Long userId;

	public Contact(Long id, Long contactId, Long userId) {
		super();
		this.id = id;
		this.contactId = contactId;
		this.userId = userId;
	}

	public Contact() {
		super();
	}

	public Contact(Long contactId, Long userId) {
		super();
		this.contactId = contactId;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
