package element.bst.elementexploration.rest.extention.riskscore.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.riskscore.service.RiskScoreService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("riskScoreService")
public class RiskScoreServiceImpl implements RiskScoreService {

	@Value("${bigdata_risk_url}")
	private String BIGDATA_RISK_URL;

	@Value("${bigdata_entity_riskscore_url}")
	private String BIGDATA_ENTITY_RISK_SCORE_URL;
	
	@Value("${client_id}")
	private String CLIENT_ID;

	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}

	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	private String putStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.updateDataInServer(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getAllRiskModels() throws Exception {
		String url = BIGDATA_RISK_URL + "/risk?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}

	@Override
	public String createRiskModel(String jsonString) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk?client_id="+CLIENT_ID;
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String setModelId(String jsonString) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk?client_id="+CLIENT_ID;
		return putStringDataToServer(url, jsonString);
	}

	@Override
	public String getRiskAttributes() throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/attributevalues?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}

	@Override
	public String insertRiskAttributes(String jsonString) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/attributevalues?client_id="+CLIENT_ID;
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getRiskRelation() throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/relationshipValues?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}

	@Override
	public String insertRiskrelation(String jsonString) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/relationshipValues?client_id="+CLIENT_ID;
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String deleteRiskModel(String modelId) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/" + encode(modelId)+"?client_id="+CLIENT_ID;
		return deleteDataFromServer(url);
	}

	private String deleteDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.deleteDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String getRiskModel(String modelId) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/" + encode(modelId)+"?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}

	@Override
	public String getDefaultRiskModel() throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/defaultModel?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}

	@Override
	public String createDefaultRiskModel(String jsonString) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/defaultModel?client_id="+CLIENT_ID;
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String checkNameExists(String jsonString) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/nameExists?client_id="+CLIENT_ID;
		return postStringDataToServer(url, jsonString);
	}

	@Override
	public String getSanctionsList() throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/sanctions-list?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}

	@Override
	public String getRiskAttributesInsights(String entityType) throws Exception {
		String url = BIGDATA_RISK_URL + "/risk/attributeinsights?client_id="+CLIENT_ID;
		if(entityType!=null)
			url = url+"/"+entityType;
		return getDataFromServer(url);
	}

	@Override
	public String getRiskScoreByEntityId(String entityId) throws Exception {
		String url = BIGDATA_ENTITY_RISK_SCORE_URL + encode(entityId)+"?client_id="+CLIENT_ID;
		return getDataFromServer(url);
	}
}
