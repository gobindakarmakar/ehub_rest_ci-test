package element.bst.elementexploration.rest.logging.util;

import java.util.ArrayList;
import java.util.List;

import element.bst.elementexploration.rest.logging.enumType.LogLevels;

public class ApiLogger {

	private String controllerName;
	
	private String methodName;
	
	private List<String> params = new ArrayList<>();
	
	private LogLevels level;
	
	private String title;
	
	private String descriptionFormat;

	public ApiLogger(String controllerName, String methodName, List<String> params, LogLevels level, String title,
			String descriptionFormat) {
		super();
		this.controllerName = controllerName;
		this.methodName = methodName;
		this.params = params;
		this.level = level;
		this.title = title;
		this.descriptionFormat = descriptionFormat;
	}

	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}

	public LogLevels getLevel() {
		return level;
	}

	public void setLevel(LogLevels level) {
		this.level = level;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescriptionFormat() {
		return descriptionFormat;
	}

	public void setDescriptionFormat(String descriptionFormat) {
		this.descriptionFormat = descriptionFormat;
	}	
}
