package element.bst.elementexploration.test.logging.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.serviceImpl.AuditLogServiceImpl;
import element.bst.elementexploration.rest.logging.util.LogRequest;


@RunWith(MockitoJUnitRunner.class)
public class AuditLogServiceTest {
	
	@Mock
	AuditLogDao auditLogDaoMock;
	
	@InjectMocks
	AuditLogServiceImpl auditLogServiceImpl;
	
	public List<AuditLog> auditLogList;
	
	public List<LogAggregator> logAggregatorList;
	
	public AuditLog auditLog1;
	
	public AuditLog auditLog2;
	
	public LogAggregator logAggregator1, logAggregator2;
	
	public LogRequest logRequestMock;

	@Before
    public void setupMock() {
		auditLogList = new ArrayList<AuditLog>();
		auditLog1 = new AuditLog(new Date(),LogLevels.INFO,"CASE",3,"Case creation");
		auditLog2 = new AuditLog(new Date(),LogLevels.INFO,"CASE",3,"Case creation");
		auditLogList.add(auditLog1);
		auditLogList.add(auditLog2);
		logAggregatorList = new ArrayList<LogAggregator>();
		logAggregator1 = new LogAggregator("09-25-2018",4l);
		logAggregator2 = new LogAggregator("09-24-2018",3l);
		logAggregatorList.add(logAggregator1);
		logAggregatorList.add(logAggregator2);		
		logRequestMock = new LogRequest();
		logRequestMock.setPageNumber(2);
		logRequestMock.setRecordsPerPage(10);
		
		
		MockitoAnnotations.initMocks(this);
	}	

	@Test
	public void getLogsTest() {
		Mockito.when(auditLogDaoMock.getLogs(Mockito.any(LogRequest.class))).thenReturn(auditLogList);
		 List<AuditLog> newAuditLogList = auditLogServiceImpl.getLogs(logRequestMock);
		assertEquals(2,newAuditLogList.size());
	}	
	
	@Test
	public void getLogsTest_whenPageNumberOrRecordsPerPageNullTest() {
		logRequestMock.setPageNumber(null);
		logRequestMock.setRecordsPerPage(null);
		Mockito.when(auditLogDaoMock.getLogs(Mockito.any(LogRequest.class))).thenReturn(auditLogList);
		List<AuditLog> newAuditLogList = auditLogServiceImpl.getLogs(logRequestMock);
		assertEquals(2,newAuditLogList.size());
	}
		
	@Test
	public void countLogsTest() {
		Mockito.when(auditLogDaoMock.countLogs(Mockito.any(LogRequest.class))).thenReturn(2l);
		long count=auditLogServiceImpl.countLogs(logRequestMock);
		assertEquals(2l,count);
	}
	
	@Test
	public void logAggregatorTest() {
		Mockito.when(auditLogDaoMock.logAggregator(Mockito.any(LogRequest.class))).thenReturn(logAggregatorList);
		List<LogAggregator> newLogAggregatorList =auditLogServiceImpl.logAggregator(logRequestMock);
		assertEquals(2,newLogAggregatorList.size());
	}
}
