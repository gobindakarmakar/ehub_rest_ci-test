package element.bst.elementexploration.rest.notification.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 * @author Viswanath
 *
 */

@Entity
@Table(name = "bst_notification")
@Access(AccessType.FIELD)
public class NotificationAlert implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7385694767611867559L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "notification_id")
	private Long notificationId;

	@Column(name = "subject", length = 64)
	@NotEmpty(message = "Subject can not be blank.")
	private String subject;

	@Column(name = "body", length = 256)
	@NotEmpty(message = "Body can not be blank.")
	private String body;

	@Column(name = "type")
	private Integer type;

	@Column(name = "createdOn")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime createdOn;

	@Column(name = "deliverOn")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime deliverOn;

	@Column(name = "level")
	private Integer level;

	@Column(name = "flag")
	private Integer flag;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "case_id")
	private Long caseId;

	@Transient
	private ArrayList<Long> recipients;

	@Column(name = "scheduledFrom")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledFrom;

	@Column(name = "scheduledTo")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledTo;

	/**
	 * Default constructor
	 */
	public NotificationAlert() {
		super();
	}

	public NotificationAlert(Long notificationId, String subject, String body, Integer type, LocalDateTime createdOn,
			LocalDateTime deliverOn, Integer level, Integer flag, Long createdBy, Long caseId,
			ArrayList<Long> recipients, LocalDateTime scheduledFrom, LocalDateTime scheduledTo) {
		super();
		this.notificationId = notificationId;
		this.subject = subject;
		this.body = body;
		this.type = type;
		this.createdOn = createdOn;
		this.deliverOn = deliverOn;
		this.level = level;
		this.flag = flag;
		this.createdBy = createdBy;
		this.caseId = caseId;
		this.recipients = recipients;
		this.scheduledFrom = scheduledFrom;
		this.scheduledTo = scheduledTo;
	}
	

	public NotificationAlert(Long notificationId, String subject, String body, Long createdBy,
			ArrayList<Long> recipients) {
		super();
		this.notificationId = notificationId;
		this.subject = subject;
		this.body = body;
		this.createdBy = createdBy;
		this.recipients = recipients;
	}

	/**
	 * @return the notificationId
	 */
	public Long getNotificationId() {
		return notificationId;
	}

	/**
	 * @param notificationId
	 *            the notificationId to set
	 */
	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getDeliverOn() {
		return deliverOn;
	}

	public void setDeliverOn(LocalDateTime deliverOn) {
		this.deliverOn = deliverOn;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public ArrayList<Long> getRecipients() {
		return recipients;
	}

	public void setRecipients(ArrayList<Long> recipients) {
		this.recipients = recipients;
	}

	public LocalDateTime getScheduledFrom() {
		return scheduledFrom;
	}

	public void setScheduledFrom(LocalDateTime scheduledFrom) {
		this.scheduledFrom = scheduledFrom;
	}

	public LocalDateTime getScheduledTo() {
		return scheduledTo;
	}

	public void setScheduledTo(LocalDateTime scheduledTo) {
		this.scheduledTo = scheduledTo;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
