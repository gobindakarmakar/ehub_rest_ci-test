package element.bst.elementexploration.rest.search.service;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.search.dao.SearchHistoryDao;
import element.bst.elementexploration.rest.search.domain.SearchHistory;
import element.bst.elementexploration.rest.search.serviceImpl.SearchHistoryServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;

@RunWith(MockitoJUnitRunner.class)
public class SearchHistoryServiceTest {

	@Mock
	SearchHistoryDao searchHistoryDaoMock;

	@Mock
	UserDao userDao;

	@Mock
	AuditLogDao auditLogDao;

	@InjectMocks
	SearchHistoryServiceImpl searchHistoryServiceImpl;

	public List<SearchHistory> list;

	public SearchHistory searchHistoryMock;

	public AuditLog auditLogMock;

	@Before
	public void setup() {
		list = new ArrayList<>();
		searchHistoryMock = new SearchHistory();
		searchHistoryMock.setSearchId(1L);
		searchHistoryMock.setUserId(2L);
		searchHistoryMock.setFavourite(true);
		searchHistoryMock.setCreatedOn(new Date());
		list.add(searchHistoryMock);
		auditLogMock = new AuditLog();
		MockitoAnnotations.initMocks(this);

	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void createSearchHistoryTestAndFindUserTest() throws Exception {
		Mockito.when(searchHistoryDaoMock.create(searchHistoryMock)).thenReturn(searchHistoryMock);
		Mockito.when(userDao.find(searchHistoryMock.getUserId())).thenReturn(new User());
		Mockito.when(auditLogDao.create(Mockito.any(AuditLog.class))).thenThrow(Exception.class);
		searchHistoryServiceImpl.createSearchHistory(2L, "test", "test case");
	}

	@Test
	public void setFavouriteSearchTest() {
		Mockito.when(searchHistoryDaoMock.find(Mockito.anyLong())).thenReturn(searchHistoryMock);
		Mockito.doNothing().when(searchHistoryDaoMock).saveOrUpdate(searchHistoryMock);
		boolean status = searchHistoryServiceImpl.setFavouriteSearch(2L, 1L);
		assertEquals(true, status);
	}

	@Test
	public void setFavouriteSearch_whenSearchIdIsNullTest() {
		Mockito.when(searchHistoryDaoMock.findMostRecentSearch(Mockito.anyLong())).thenReturn(searchHistoryMock);
		Mockito.doNothing().when(searchHistoryDaoMock).saveOrUpdate(searchHistoryMock);
		boolean status = searchHistoryServiceImpl.setFavouriteSearch(2L, null);
		assertEquals(true, status);
	}

	@Test
	public void getRecentSearchesTest() throws ParseException {
		Mockito.when(searchHistoryDaoMock.getRecentSearches(Mockito.anyLong(), Mockito.any(Date.class),
				Mockito.any(Date.class), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(list);
		List<SearchHistory> result = searchHistoryServiceImpl.getRecentSearches(2L,
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "", 1, 2);
		assertEquals(list.size(), result.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getRecentSearchesTest_WhenCreationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(searchHistoryDaoMock.getRecentSearches(Mockito.anyLong(), Mockito.any(Date.class),
				Mockito.any(Date.class), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
				.thenThrow(DateFormatException.class);
		searchHistoryServiceImpl.getRecentSearches(2L, new Date().toString(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "", 1, 2);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getRecentSearchesTest_WhenModificationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(searchHistoryDaoMock.getRecentSearches(Mockito.anyLong(), Mockito.any(Date.class),
				Mockito.any(Date.class), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
				.thenThrow(DateFormatException.class);
		searchHistoryServiceImpl.getRecentSearches(2L,new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new Date().toString(),
				 true, "", 1, 2);
	}

	@Test
	public void countRecentSearchesTest() throws ParseException {
		Mockito.when(searchHistoryDaoMock.countRecentSearches(Mockito.anyLong(), Mockito.any(Date.class),
				Mockito.any(Date.class), Mockito.anyBoolean(), Mockito.anyString())).thenReturn(2L);
		long result = searchHistoryServiceImpl.countRecentSearches(2L, new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "");
		assertEquals(2L, result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countRecentSearchesTest_WhenCreationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(searchHistoryDaoMock.countRecentSearches(Mockito.anyLong(), Mockito.any(Date.class),
				Mockito.any(Date.class), Mockito.anyBoolean(), Mockito.anyString())).thenThrow(DateFormatException.class);
		searchHistoryServiceImpl.countRecentSearches(2L, new Date().toString(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void countRecentSearchesTest_WhenModificationDateFormat_ExceptionThrowTest() throws ParseException {
		Mockito.when(searchHistoryDaoMock.countRecentSearches(Mockito.anyLong(), Mockito.any(Date.class),
				Mockito.any(Date.class), Mockito.anyBoolean(), Mockito.anyString())).thenThrow(DateFormatException.class);
		searchHistoryServiceImpl.countRecentSearches(2L, new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new Date().toString(), true, "");
	}

}
