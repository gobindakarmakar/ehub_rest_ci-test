package element.bst.elementexploration.rest.extention.widgetreview.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.widgetreview.domain.ComplianceWidget;
import element.bst.elementexploration.rest.extention.widgetreview.dto.ComplianceWidgetDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author rambabu
 *
 */
public interface ComplianceWidgetService extends GenericService<ComplianceWidget, Long> {
	
	public List<ComplianceWidgetDto> getAllComplianceWidget();

}
