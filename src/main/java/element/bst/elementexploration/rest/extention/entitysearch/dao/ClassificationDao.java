package element.bst.elementexploration.rest.extention.entitysearch.dao;

import element.bst.elementexploration.rest.extention.entitysearch.domain.Classification;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface ClassificationDao extends GenericDao<Classification, Long>{

}
