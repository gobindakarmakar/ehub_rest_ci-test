package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import element.bst.elementexploration.rest.usermanagement.group.dto.DefaultPermissionsDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.PermissionsWithIdDto;

/**
 * @author Jalagari Paul
 *
 */
public interface PrivilegesDao extends GenericDao<Permissions, Long>{

	List<PermissionsWithIdDto> findAllPermissions() throws Exception;
	
	PermissionsWithIdDto findPermissionById(Long permissionId) throws Exception;

	boolean savePrivilege(DefaultPermissionsDto eachPermission) throws Exception;


}
