package element.bst.elementexploration.rest.usermanagement.group.dao;

import java.util.Map;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;

/**
 * @author Paul Jalagari
 *
 */
public interface UserGroupsDao extends GenericDao<UserGroups, Long> {

	UserGroups getUserGroup(Long userId, Long groupId);

	Map<Long, Long> groupsWithUserCount();

}
