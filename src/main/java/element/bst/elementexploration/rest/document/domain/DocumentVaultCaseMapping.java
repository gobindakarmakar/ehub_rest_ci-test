package element.bst.elementexploration.rest.document.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

@Entity
@Table(name = "bst_doc_case_mapping")
public class DocumentVaultCaseMapping implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9130732073540713146L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_doc_caseId")
	private Long docCaseId;

	@Column(name = "fk_doc_id")
	private Long docId;

	@Column(name = "fk_case_id")
	private Long caseId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "associated_by")
	private Users associatedBy;

	@Column(name = "is_associated")
	private Boolean isAssociated;

	@Column(name = "associated_on")
	private Date associatedOn;

	@Column(name = "dissociated_on")
	private Date dissociatedOn;

	@Column(name = "dissociated_by")
	private Long dissociatedBy;

	public DocumentVaultCaseMapping() {
	}

	public Long getDocCaseId() {
		return docCaseId;
	}

	public void setDocCaseId(Long docCaseId) {
		this.docCaseId = docCaseId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Users getAssociatedBy() {
		return associatedBy;
	}

	public void setAssociatedBy(Users associatedBy) {
		this.associatedBy = associatedBy;
	}

	public Boolean getIsAssociated() {
		return isAssociated;
	}

	public void setIsAssociated(Boolean isAssociated) {
		this.isAssociated = isAssociated;
	}

	public Date getAssociatedOn() {
		return associatedOn;
	}

	public void setAssociatedOn(Date associatedOn) {
		this.associatedOn = associatedOn;
	}

	public Date getDissociatedOn() {
		return dissociatedOn;
	}

	public void setDissociatedOn(Date dissociatedOn) {
		this.dissociatedOn = dissociatedOn;
	}

	public Long getDissociatedBy() {
		return dissociatedBy;
	}

	public void setDissociatedBy(Long dissociatedBy) {
		this.dissociatedBy = dissociatedBy;
	}

	public DocumentVaultCaseMapping(Long docCaseId, Long docId, Long caseId, Users associatedBy, Boolean isAssociated,
			Date associatedOn, Date dissociatedOn, Long dissociatedBy) {
		super();
		this.docCaseId = docCaseId;
		this.docId = docId;
		this.caseId = caseId;
		this.associatedBy = associatedBy;
		this.isAssociated = isAssociated;
		this.associatedOn = associatedOn;
		this.dissociatedOn = dissociatedOn;
		this.dissociatedBy = dissociatedBy;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
