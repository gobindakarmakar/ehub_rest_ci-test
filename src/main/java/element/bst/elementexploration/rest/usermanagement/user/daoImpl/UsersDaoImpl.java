package element.bst.elementexploration.rest.usermanagement.user.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseDaoImpl;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.DateCountStatusDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.LoginUserCountDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.FilterColumnDto;
import element.bst.elementexploration.rest.util.RequestFilterDto;

/**
 * @author Paul Jalagari
 *
 */
@Repository("usersDao")
public class UsersDaoImpl extends GenericDaoImpl<Users, Long> implements UsersDao {

	public UsersDaoImpl() {
		super(Users.class);
	}
	
	@Autowired
	private SessionFactory sessionFactory;

	@Lazy
	@Autowired
	UsersService usersService;
	
	@Override
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsersDto> getUserList(Long groupId, String statusId, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		List<UsersDto> users = new ArrayList<UsersDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto(u.userId, u.emailAddress,"
							+ " u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.countryId, u.statusId, u.lastLoginDate,"
							+ " u.userImage, u.jobTitle, u.extension, u.phoneNumber, u.department, u.source) ");
			queryBuilder.append("from Users u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroups ug on u.userId = ug.userId.userId ");
				queryBuilder.append(" where (ug.groupId.id = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(statusId)) {
				if (isNotOptional) {
					queryBuilder.append(" and (u.statusId.listItemId = " + statusId + ") ");

				} else {
					queryBuilder.append(" where (u.statusId.listItemId = " + statusId + ") ");

				}
			}
			queryBuilder.append(" order by ").append("u.").append(resolveUserCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			users = (List<UsersDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsersDto> getUserListFullTextSearch(String keyword, String statusId, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn) {
		List<UsersDto> users = new ArrayList<UsersDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto(u.userId, u.emailAddress,"
							+ " u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.countryId, u.statusId, u.lastLoginDate) ");
			queryBuilder.append("from Users u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroups ug on u.userId = ug.userId.userId ");
				queryBuilder.append(" where (ug.groupId.id = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(keyword)) {
				String clause = " where ";
				if (isNotOptional)
					clause = " and ";
				queryBuilder.append(clause + " (u.userId like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.emailAddress like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.firstName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.lastName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.middleName like '%" + keyword.replace("\"", "") + "%')");
			}
			if (!StringUtils.isEmpty(statusId)) {
				queryBuilder.append(" and (u.statusId.listItemId = " + statusId + ") ");
			}
			queryBuilder.append(" order by ").append("u.").append(resolveUserCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			users = (List<UsersDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserList(Long groupId, String statusId) {
		List<UsersDto> users = new ArrayList<UsersDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto(u.userId, u.emailAddress, "
							+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.countryId, u.statusId, u.lastLoginDate) ");
			queryBuilder.append("from Users u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroups ug on u.userId = ug.userId.userId ");
				queryBuilder.append(" where (ug.groupId.id = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(statusId)) {
				if (isNotOptional) {
					queryBuilder.append(" and (u.statusId.listItemId = " + statusId + ") ");

				} else {
					queryBuilder.append(" where (u.statusId.listItemId = " + statusId + ") ");

				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			users = (List<UsersDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) users.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserListFullTextSearch(String keyword, String statusId, Long groupId) {
		List<UsersDto> users = new ArrayList<UsersDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder
					.append("select new element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto(u.userId, u.emailAddress, "
							+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.countryId, u.statusId, u.lastLoginDate) ");
			queryBuilder.append("from Users u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroups ug on u.userId = ug.userId.userId ");
				queryBuilder.append(" where (ug.groupId.id = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(keyword)) {
				String clause = " where ";
				if (isNotOptional)
					clause = " and ";
				queryBuilder.append(clause + " (u.userId like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.emailAddress like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.firstName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.lastName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.middleName like '%" + keyword.replace("\"", "") + "%')");
			}
			if (!StringUtils.isEmpty(statusId)) {
				queryBuilder.append(" and (u.statusId.listItemId = " + statusId + ") ");
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			users = (List<UsersDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) users.size();
	}

	private String resolveUserCaseColumnName(String userColumn) {
		if (userColumn == null)
			return "createdDate";

		switch (userColumn.toLowerCase()) {
		case "firstname":
			return "firstName";
		case "username":
			return "screenName";
		default:
			return "createdDate";
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsersDto> listUsersByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		List<UsersDto> userList = new ArrayList<>();
		List<Object[]> objects = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			/*
			 * hql.append( "select u.userId, u.emailAddress, " +
			 * "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.countryId, u.statusId, u.lastLoginDate "
			 * ) .append("from Users u ").
			 * append("left outer join UserGroups ug on u.userId = ug.userId.userId "
			 * ) .append("where ug.groupId.id= :groupId ");
			 */
			////////////////////////
			/*
			 * if (userStatus != null) { hql.append(" and u.statusId= " +
			 * userStatus); hql.append(" "); }
			 */
			// hql.append(" order by
			// ").append("u.").append(resolveUserCaseColumnName(orderBy));
			hql.append(
					"select u.userId, u.emailAddress, u.firstName, u.lastName, u.middleName, u.screenName, u.dateOfBirth, u.countryId, u.statusId, u.lastLoginDate from bst_um_user u left outer join bst_um_user_group ug on u.userId = ug.userId where ug.userGroupId =:groupId ");
			hql.append(" order by ").append("u.").append(resolveUserCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if ("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)) {
					hql.append(" ");
					hql.append(orderIn);
				}
			}

			// Query<?> query =
			// this.getCurrentSession().createQuery(hql.toString());
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("groupId", groupId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			// userList =(List<UsersDto>)query.getResultList();
			// objects=(List<Object[]>)(userList);
			objects = (List<Object[]>) query.getResultList();
			if (objects != null && objects.size() > 0) {
				for (Object[] result : objects) {
					UsersDto userDto = new UsersDto();
					if (null != result[0] && result[0].toString() != null)
						userDto.setUserId(Long.valueOf(result[0].toString()));
					if (null != result[1] && result[1].toString() != null)
						userDto.setEmailAddress(result[1].toString());
					if (null != result[2] && result[2].toString() != null)
						userDto.setFirstName(result[2].toString());
					if (null != result[3] && result[3].toString() != null)
						userDto.setLastName(result[3].toString());
					if (null!=result[4]) {
						if(null !=result[4].toString())
						userDto.setMiddleName(result[4].toString());
					} else {
						userDto.setMiddleName(null);
						
					}

					if (null!=result[5] && result[5].toString() != null)
						userDto.setScreenName(result[5].toString());

					 if(null!=result[6] && result[6].toString()!=null)
					 userDto.setDob((Date) (result[6]));

					// if(result[7].toString()!=null)
					userDto.setCountryId(null);
					// if(result[8].toString()!=null)
					userDto.setStatusId(null);
					if (null!=result[9]) {
						if(null !=result[9].toString())
						userDto.setLastLoginDate((Date) (result[9]));
					} else {
						userDto.setLastLoginDate(null);
					}

					userList.add(userDto);
				}
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userList;
	}

	@Override
	public Users getUserByEmailOrUsername(String email) {
		Users found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Users> query = builder.createQuery(Users.class);
			Root<Users> user = query.from(Users.class);
			query.select(user);
			query.where(builder.or(builder.equal(user.get("emailAddress"), email),
					builder.equal(user.get("screenName"), email)));
			found = (Users) this.getCurrentSession().createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

	@Override
	public Users checkUserNameOrEmailExists(String email, Long id) {
		Users found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Users> query = builder.createQuery(Users.class);
			Root<Users> user = query.from(Users.class);
			query.select(user);
			query.where(builder.and(builder.notEqual(user.get("userId"), id), builder
					.or(builder.equal(user.get("emailAddress"), email), builder.equal(user.get("screenName"), email))));
			found = (Users) this.getCurrentSession().createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserByGroup(Long groupId, Integer userStatus) {
		List<UsersDto> userList = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto(u.userId, u.emailAddress, "
							+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.countryId, u.statusId, u.lastLoginDate) ")
					.append("from Users u ").append("left outer join UserGroups ug on u.userId = ug.userId.userId ")
					.append("where ug.groupId.id= :groupId ");

			/*
			 * if (userStatus != null) { hql.append(" and u.statusId= " +
			 * userStatus); hql.append(" "); }
			 */

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("groupId", groupId);
			userList = (List<UsersDto>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return (long) userList.size();
	}

	@Override
	public List<Users> getUsersInList(List<Long> idList) {
		List<Users> list = new ArrayList<>();
		try {
			if (idList != null && !idList.isEmpty()) {
				CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<Users> query = builder.createQuery(Users.class);
				Root<Users> user = query.from(Users.class);
				query.select(user);
				query.where(user.get("userId").in(idList));
				list = (ArrayList<Users>) this.getCurrentSession().createQuery(query).getResultList();
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to add participants. Reason : " + e.getMessage());
		}
		return list;
	}

	@Override
	public Users getAnalystByUserId(long analystId) {
		try {
			String sql = "select u from Users u,UserGroups uug,Groups ug where u.userId=uug.userId.userId and u.userId = :userId and uug.groupId.id=ug.id and ug.name=:groupName";
			Query<?> query = getCurrentSession().createQuery(sql);
			query.setParameter("groupName", ElementConstants.ANALYST_GROUP_NAME);
			query.setParameter("userId", analystId);
			return (Users) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Users> getAllAnalystWhoDidNotRejectedCaseBefore(Long caseId) {
		List<Users> users = new ArrayList<>();
		String sql = "select u from Users u,UserGroups uug,Groups ug where u.userId not in (select cam.user.userId from CaseAnalystMapping cam where"
				+ " cam.caseSeed.caseId = :caseId and cam.currentStatus =:statusId) and u.userId=uug.userId.userId and uug.groupId.id=ug.id and ug.name=:groupName";
		try {
			Query<?> query = getCurrentSession().createQuery(sql);
			query.setParameter("groupName", ElementConstants.ANALYST_GROUP_NAME);
			query.setParameter("caseId", caseId);
			query.setParameter("statusId", StatusEnum.REJECTED.getIndex());
			users = (List<Users>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsersDto> listUsersByGroupWithoutPagination(Long eachGroupId) {
		List<UsersDto> userList = new ArrayList<>();
		List<Object[]> objects = new ArrayList<>();
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select u.userId, u.emailAddress, u.firstName, u.lastName, u.middleName, u.screenName, u.dateOfBirth, u.countryId, u.statusId, u.lastLoginDate from bst_um_user u left outer join bst_um_user_group ug on u.userId = ug.userId where ug.userGroupId =:groupId ");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("groupId", eachGroupId);
			objects = (List<Object[]>) query.getResultList();
			if (objects != null && objects.size() > 0) {
				for (Object[] result : objects) {
					UsersDto userDto = new UsersDto();
					if (result[0].toString() != null)
						userDto.setUserId(Long.valueOf(result[0].toString()));
					if (result[1].toString() != null)
						userDto.setEmailAddress(result[1].toString());
					if (result[2].toString() != null)
						userDto.setFirstName(result[2].toString());
					if (result[3].toString() != null)
						userDto.setLastName(result[3].toString());
					if (null != result[4]) {
						if (null != result[4].toString())
							userDto.setMiddleName(result[4].toString());
					} else {
						userDto.setMiddleName(null);
					}
					if (result[5].toString() != null)
						userDto.setScreenName(result[5].toString());
					if (result[6].toString() != null)
						userDto.setDob((Date) (result[6]));
					// if(result[7].toString()!=null)
					userDto.setCountryId(null);
					// if(result[8].toString()!=null)
					userDto.setStatusId(null);
					if (null != result[9]) {
						if (null != result[9].toString())
							userDto.setLastLoginDate((Date) (result[9]));
					} else {
						userDto.setLastLoginDate(null);
					}
					userList.add(userDto);
				}
			}
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userList;

	}

	@Override
	public List<Long> quickSearch(String searchKey,boolean isScreeNameRequired) throws Exception {
		List<Long> userIds = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Long> query = builder.createQuery(Long.class);
			Root<Users> user = query.from(Users.class);
			query.select(user.get("userId"));
			List<Predicate> allPredicates=new ArrayList<>();
			Predicate otherPredicate= builder.or(builder.like(user.get("firstName"), "%" +searchKey+"%"),
					builder.like(user.get("lastName"), "%" +searchKey+ "%"), (builder.like(user.get("middleName"),"%" + searchKey + "%")));
			Predicate screenPredicate= builder.like(user.get("screenName"), "%" +searchKey+ "%");
			/*query.where(builder.or(builder.like(user.get("firstName"), searchKey),
					builder.like(user.get("lastName"), "%" +searchKey+ "%"), (builder.like(user.get("middleName"),"%" + searchKey + "%")),
					(builder.like(user.get("screenName"), "%" +searchKey+ "%"))));*/
			allPredicates.add(otherPredicate);
			if(isScreeNameRequired) {
				allPredicates.add(screenPredicate);
			}
			query.where(builder.or(allPredicates.toArray(new Predicate[allPredicates.size()])));
			userIds = (List<Long>) this.getCurrentSession().createQuery(query).setMaxResults(8).getResultList();
		} catch (NoResultException e) {
			return userIds;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userIds;
	}

	@Override
	public List<Long> getUserIdByRolesList(List<Long> comingIdList) {
		
		List<Long> userIds = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Long> query = builder.createQuery(Long.class);
			Root<UserRoles> user = query.from(UserRoles.class);
			query.select(user.get("userId").get("userId"));
			query.where(builder.in(user.get("roleId").get("roleId")).value(comingIdList));
			userIds = (List<Long>) this.getCurrentSession().createQuery(query).getResultList();
		} catch (NoResultException e) {
			return userIds;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userIds;
		
	}

	@Override
	public List<Long> getUserIdByGroupsList(List<Long> comingIdList) {
		
		List<Long> userIds = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Long> query = builder.createQuery(Long.class);
			Root<UserGroups> user = query.from(UserGroups.class);
			query.select(user.get("userId").get("userId"));
			query.where(builder.in(user.get("groupId").get("id")).value(comingIdList));
			userIds = (List<Long>) this.getCurrentSession().createQuery(query).getResultList();
		} catch (NoResultException e) {
			return userIds;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userIds;
	}

	@Override
	public Map<Object, Long> getLogonsByHourCount(String period, Date from, Date to) {
		List<Object[]> resultant = new ArrayList<>();
		Map<Object, Long> resultMap = new HashMap<>();
		try {
			CriteriaBuilder builder1 = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Object[]> query1 = builder1.createQuery(Object[].class);
			Root<LoginHistory> login1 = query1.from(LoginHistory.class);
			if(period != null){
				if(period.equalsIgnoreCase("Hour")){
					query1.multiselect(login1.get("loginTime"), builder1.count(login1));
					query1.where(builder1.equal(builder1.function("DATE", Integer.class, login1.get("loginTime")), builder1.currentDate()));
					query1.groupBy(builder1.function("Hour", Integer.class, login1.get("loginTime")));
				}else if(period.equalsIgnoreCase("Day") && from != null && to != null){
					query1.multiselect(login1.get("loginTime"), builder1.count(login1));
					query1.where(builder1.and(
							builder1.greaterThanOrEqualTo(login1.get("loginTime"), from), 
							builder1.lessThanOrEqualTo(login1.get("loginTime"), to)));
					query1.groupBy(builder1.function("Day", Integer.class, login1.get("loginTime")));
				}else if(period.equalsIgnoreCase("Month") && from != null && to != null){
					query1.multiselect(login1.get("loginTime"), builder1.count(login1));
					query1.where(builder1.and(
							builder1.greaterThanOrEqualTo(login1.get("loginTime"), from), 
							builder1.lessThanOrEqualTo(login1.get("loginTime"), to)));
					query1.groupBy(builder1.function("Month", Integer.class, login1.get("loginTime")));
				}
			}else{
				query1.multiselect(login1.get("loginTime"), builder1.count(login1));
				query1.where(builder1.equal(builder1.function("DATE", Integer.class, login1.get("loginTime")), builder1.currentDate()));
				query1.groupBy(builder1.function("Hour", Integer.class, login1.get("loginTime")));
			}
			resultant =  this.getCurrentSession().createQuery(query1).getResultList();
			if (resultant != null && resultant.size() > 0) {
				for (Object[] result : resultant) {
					resultMap.put((result[0]), ((Long)result[1]).longValue());
				}
			}
			
		} catch (NoResultException e) {
			return resultMap;
		}
		return resultMap;

	}
	
	@Override
	public Map<Object, Long> getFailedLogonsCount(String period, Date from, Date to) {
		List<Object[]> resultant = new ArrayList<>();
		Map<Object, Long> resultMap = new HashMap<>();
		try {
			CriteriaBuilder builder1 = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Object[]> query1 = builder1.createQuery(Object[].class);
			Root<LoginHistory> login1 = query1.from(LoginHistory.class);
			if(period != null){
				if(period.equalsIgnoreCase("Hour")){
					query1.multiselect(login1.get("loginTime"), builder1.count(login1));
					query1.where(
							builder1.and(
							builder1.equal(login1.get("loginStatus"), ElementConstants.FAILED_STATUS),
							builder1.equal(builder1.function("DATE", Integer.class, login1.get("loginTime")), builder1.currentDate())));
					query1.groupBy(builder1.function("Hour", Integer.class, login1.get("loginTime")));
				}else if(period.equalsIgnoreCase("Day") && from != null && to != null){
					query1.multiselect(login1.get("loginTime"), builder1.count(login1));
					query1.where(
							builder1.and(
							builder1.equal(login1.get("loginStatus"), ElementConstants.FAILED_STATUS),
							builder1.and(
							builder1.greaterThanOrEqualTo(login1.get("loginTime"), from), 
							builder1.lessThanOrEqualTo(login1.get("loginTime"), to))));
					query1.groupBy(builder1.function("Day", Integer.class, login1.get("loginTime")));
				}else if(period.equalsIgnoreCase("Month") && from != null && to != null){
					query1.multiselect(login1.get("loginTime"), builder1.count(login1));
					query1.where(
							builder1.and(
									builder1.equal(login1.get("loginStatus"), ElementConstants.FAILED_STATUS),
							builder1.and(
							builder1.greaterThanOrEqualTo(login1.get("loginTime"), from), 
							builder1.lessThanOrEqualTo(login1.get("loginTime"), to))));
					query1.groupBy(builder1.function("Month", Integer.class, login1.get("loginTime")));
				}
			}else{
				query1.multiselect(login1.get("loginTime"), builder1.count(login1));
				query1.where(builder1.equal(builder1.function("DATE", Integer.class, login1.get("loginTime")), builder1.currentDate()));
				query1.groupBy(builder1.function("Hour", Integer.class, login1.get("loginTime")));
			}
			resultant =  this.getCurrentSession().createQuery(query1).getResultList();
			if (resultant != null && resultant.size() > 0) {
				for (Object[] result : resultant) {
					resultMap.put((result[0]), ((Long)result[1]).longValue());
				}
			}
			
		} catch (NoResultException e) {
			return resultMap;
		}
		return resultMap;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<DateCountStatusDto> getCountByStatusWise() throws Exception {
		List<DateCountStatusDto> resultList = new ArrayList<>();
		List<Object[]> objects = new ArrayList<>();
		String type=ElementConstants.USER_MANAGEMENT_TYPE;
		String registraTionType= ElementConstants.USER_REGISTRATION_TYPE;
		try {
			/*CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<DateCountStatusDto> query = builder.createQuery(DateCountStatusDto.class);
			Root<AuditLog> root = query.from(AuditLog.class);
			query.select(builder.construct(DateCountStatusDto.class, root.get("createdOn"),root.get("subType"),builder.count(root.get("subType")))).where(builder.greaterThanOrEqualTo(x, y));*/
			StringBuilder hql = new StringBuilder();
			hql.append("SELECT al.created_on,al.subType,count(al.subType) FROM bst_audit_log al where al.created_on >= date_sub(now(), interval 12 month)" + 
					" and (al.type =:type or al.type =:regType) group by month(al.created_on), al.subType ");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("type", type);
			query.setParameter("regType", registraTionType);
			objects = (List<Object[]>) query.getResultList();
			if (objects != null && objects.size() > 0) {
				for (Object[] result : objects) {
					DateCountStatusDto eachObject = new DateCountStatusDto();
					if (null != result[0]) {
						if (null != result[0].toString())
							eachObject.setDate((Date) (result[0]));
					} else {
						eachObject.setDate(null);
					}
					if (null != result[1]) {
						if (null != result[1].toString())
							eachObject.setStatus((result[1].toString()));
					} else {
						eachObject.setStatus(null);
					}
					if (null != result[2]) {
						if (null != result[2].toString())
							eachObject.setCount(Integer.valueOf((result[2].toString())));
					} else {
						eachObject.setCount(0);
					}
					resultList.add(eachObject);
				}
			}
		} catch (NoResultException e) {
			return resultList;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		
		return resultList;
	}
	
	@Override
	public Long activeUsersCount() {
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = (CriteriaQuery<Long>) builder.createQuery(Long.class);
		Root<Users> result = (Root<Users>) criteriaQuery.from(Users.class);
		
		Join<Users, ListItem> listJoin = result.join("statusId");
		
		criteriaQuery.select(builder.count(result));
		criteriaQuery.where(builder.equal(listJoin.get("displayName"), "Active"));
		Query<Long> query = this.getCurrentSession().createQuery(criteriaQuery);
		return query.getSingleResult();
	}

	@Override
	public Map<String, Long> getTopTenLogonFailuresUsers(String period, Date from, Date to) {
		List<Object[]> resultant = new ArrayList<>();
		Map<String, Long> resultMap = new HashMap<>();
		try {
			CriteriaBuilder builder1 = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Object[]> query1 = builder1.createQuery(Object[].class);
			Root<LoginHistory> login1 = query1.from(LoginHistory.class);
			query1.multiselect(login1.get("username"), builder1.count(login1));
			if(period != null){
				if(period.equalsIgnoreCase("Hour")){
					query1.where(
							builder1.and(
							builder1.equal(login1.get("loginStatus"), ElementConstants.FAILED_STATUS),
							builder1.equal(builder1.function("DATE", Integer.class, login1.get("loginTime")), builder1.currentDate())));
					query1.groupBy(login1.get("username"));
					query1.orderBy(builder1.desc(builder1.count(login1)));
				}else if((period.equalsIgnoreCase("Day") || period.equalsIgnoreCase("Month")) && from != null && to != null){
					query1.where(
							builder1.and(
							builder1.equal(login1.get("loginStatus"), ElementConstants.FAILED_STATUS),
							builder1.and(
							builder1.greaterThanOrEqualTo(login1.get("loginTime"), from), 
							builder1.lessThanOrEqualTo(login1.get("loginTime"), to))));
					query1.groupBy(login1.get("username"));
					query1.orderBy(builder1.desc(builder1.count(login1)));
				}
			}else{
				query1.where(
						builder1.and(
						builder1.equal(login1.get("loginStatus"), ElementConstants.FAILED_STATUS),
						builder1.equal(builder1.function("DATE", Integer.class, login1.get("loginTime")), builder1.currentDate())));
				query1.groupBy(login1.get("username"));
				query1.orderBy(builder1.desc(builder1.count(login1)));
			}
			resultant =  this.getCurrentSession().createQuery(query1).setMaxResults(10).getResultList();
			if (resultant != null && resultant.size() > 0) {
				for (Object[] result : resultant) {
					resultMap.put((result[0].toString()), ((Long)result[1]).longValue());
				}
			}
			
		} catch (NoResultException e) {
			return resultMap;
		}
		return resultMap;

	}

	@Override
	public List<Users> getUsersByUsernames(List<String> usernamesList) {
		List<Users> userList = new ArrayList<Users>();
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Users> query = builder.createQuery(Users.class);
			Root<Users> user = query.from(Users.class);
			query.select(user);
			query.where(user.get("screenName").in(usernamesList));
			userList = this.getCurrentSession().createQuery(query).getResultList();
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UsersDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userList;
	}

	@Override
	public List<Users> getUsersByIds(List<Long> comingUserList) {
		
		List<Users> userList = new ArrayList<Users>();
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Users> query = builder.createQuery(Users.class);
			Root<Users> user = query.from(Users.class);
			query.select(user);
			query.where(user.get("userId").in(comingUserList));
			userList = this.getCurrentSession().createQuery(query).getResultList();
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UsersDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return userList;
	}
	@SuppressWarnings({ "null", "unchecked" })
	@Override
	public Map<List<LoginUserCountDto>, Long> userFilterData(Users userClass, List<FilterColumnDto> columnsList,
			Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy) {
		
		Map<List<LoginUserCountDto>, Long> returnResult = new HashMap<List<LoginUserCountDto>, Long>();
		
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Object[]> criteriaQuery = builder.createQuery(Object[].class);
		Root<Users> result = criteriaQuery.from(Users.class);
		
		/*Join<Users, UserRoles> userRolesJoin = null;
		Join<UserRoles, Roles> roleJoin = null;*/
		
		Join<Users, Roles> roleJoin = null;
		
		Subquery<Long> subQueryfull = criteriaQuery.subquery(Long.class);
		Subquery<Long> subQueryfailed = criteriaQuery.subquery(Long.class);
		Root<LoginHistory> loginRoot1 = subQueryfull.from(LoginHistory.class);
		Root<LoginHistory> loginRoot2 = subQueryfailed.from(LoginHistory.class);
		subQueryfull.select(builder.count(loginRoot1.get("failedLoginId"))).alias("fullCount");
		subQueryfailed.select(builder.count(loginRoot2.get("failedLoginId"))).alias("failedCount");
		
		criteriaQuery.multiselect(result.get("userId"),subQueryfull.getSelection(),
				subQueryfailed.getSelection());
		
		/*CriteriaQuery<Long> totalQuery = builder.createQuery(Long.class);
		Root<Users> countResult =  totalQuery.from(Users.class);
		totalQuery.select(builder.count(countResult));*/
		
		List<Predicate> columnListPredicates = new ArrayList<Predicate>();
		List<Predicate> countListPredicates = new ArrayList<Predicate>();
		if ((columnsList != null || columnsList.isEmpty()) ){
			for(FilterColumnDto filterColumn : columnsList){
				String columnName = null;
				String operator = null;
				List<RequestFilterDto> conditionsList = new ArrayList<RequestFilterDto>();
				//filterConditions();
				if(!StringUtils.isBlank(filterColumn.getColumn()))
					columnName = filterColumn.getColumn();
				if(!StringUtils.isBlank(filterColumn.getOperator()))
					operator = filterColumn.getOperator();
				if(null != filterColumn.getCondition1())
					conditionsList.add(filterColumn.getCondition1());
				if(null != filterColumn.getCondition2() && !StringUtils.isEmpty(filterColumn.getOperator()))
					conditionsList.add(filterColumn.getCondition2());
				
				if(filterColumn.getColumn().equals("roles")){
					/*userRolesJoin = result.join("userId");
					roleJoin = userRolesJoin.join("roleId");*/
					roleJoin = result.join("userRoles").join("roleId");
				}
				
				List<Predicate> filterPredicatesList = new ArrayList<Predicate>();
				List<Predicate> filterCountPredicatesList = new ArrayList<Predicate>();
				for(RequestFilterDto condition : conditionsList){
					List<Predicate> conditionPredicatesList = new ArrayList<Predicate>();
					List<Predicate> countPredicatesList = new ArrayList<Predicate>();
					//if filter type is Text
					if (condition.getFilterType() != null && condition.getFilterType().equalsIgnoreCase("Text")) {
						String []keyword = null;
						if(condition.getKeyword() != null ){
							keyword = condition.getKeyword();
						}
						
						//if filter type is Text and subtype contains
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("contains")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.like(roleJoin.get("roleName"), "%" + keyword[0] + "%"));
							}else{
								conditionPredicatesList.add(builder.like(result.get(columnName), "%" + keyword[0] + "%"));
							}
						}
						//if filter type is Text and subtype not contains
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("notContains")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.notLike(roleJoin.get("roleName"), "%" + keyword[0] + "%"));
							}else{
								conditionPredicatesList.add(builder.notLike(result.get(columnName), "%" + keyword[0] + "%"));
							}
							
						}
						//if filter type is Text and subtype "equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("equals")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.equal(roleJoin.get("roleName"),  keyword[0] ));
							}else{
								conditionPredicatesList.add(builder.equal(result.get(columnName),  keyword[0] ));
							}
							
						}
						//if filter type is Text and subtype "not equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("notEquals")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.notEqual(roleJoin.get("roleName"), keyword[0] ));
							}else{
								conditionPredicatesList.add(builder.notEqual(result.get(columnName), keyword[0] ));
							}
							
						}
						//if filter type is Text and subtype "starts with"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("startsWith")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.like(roleJoin.get("roleName"), keyword[0]+ "%" ));
							}else{
								conditionPredicatesList.add(builder.like(result.get(columnName), keyword[0]+ "%" ));
							}
							
						}
						//if filter type is Text and subtype "ends with"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("endsWith")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.like(roleJoin.get("roleName"), "%" +keyword[0]));
							}else{
								conditionPredicatesList.add(builder.like(result.get(columnName), "%" +keyword[0]));
							}
							
						}
						//if filter type is Text and subtype "range"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("inRange")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								conditionPredicatesList.add(builder.between(roleJoin.get("roleName"), keyword[0], keyword[1]));
							}else{
								conditionPredicatesList.add(builder.between(result.get(columnName), keyword[0], keyword[1]));
							}
							
						}
						//if filter type is Text and subtype "multiple strings"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("multiple")) {
							if(condition.getEntityColumn().equalsIgnoreCase("roles")){
								for(int i = 0; i<keyword.length; i++){
									conditionPredicatesList.add(builder.like(roleJoin.get("roleName"), "%" + keyword[i] + "%"));
								}
							}else{
								for(int i = 0; i<keyword.length; i++){
									conditionPredicatesList.add(builder.like(result.get(columnName), "%" + keyword[i] + "%"));
								}
							}
						}
						//if filter type is Text and subtype "multiple select"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("multiSelect")) {
							if(condition.getEntityColumn().equalsIgnoreCase("fullName")){
								for (int i = 0; i < keyword.length; i++) {
									conditionPredicatesList.add(builder.equal(result.get("userId"), Long.parseLong(keyword[i])));
								}
							}else if(condition.getEntityColumn().equalsIgnoreCase("status")){
								for (int i = 0; i < keyword.length; i++) {
									conditionPredicatesList.add(builder.equal(result.get("statusId"), Long.parseLong(keyword[i])));
								}
							}/*else if(condition.getEntityColumn().equalsIgnoreCase("userRoles")){
								List<Long> comingIdList = new ArrayList<Long>();
								List<Long> userIds = new ArrayList<Long>();
								for(int i = 0; i<keyword.length; i++){
									comingIdList.add(Long.parseLong(keyword[i]));
								}
								userIds = usersService.getUserIdByRolesList(comingIdList);
								conditionPredicatesList.add(builder.in(result.get("userId")).value(userIds));
							}*/
							
						}
						
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("multiSelectString")) {
							for (int i = 0; i < keyword.length; i++) {
								conditionPredicatesList.add(builder.equal(result.get(columnName), keyword[i]));
							}
						}
					}
					// if filter type is Number
					if (condition.getFilterType() != null && condition.getFilterType().equalsIgnoreCase("Number")) {
						String []keyword = null;
						if(condition.getKeyword() != null ){
							keyword = condition.getKeyword();
						}
						// if filter type is Number and sub type is "equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("equals")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								conditionPredicatesList.add(builder.equal(subQueryfull.getSelection(), Integer.parseInt(keyword[0])));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.equal(subQueryfailed.getSelection(), Integer.parseInt(keyword[0])));
							}
							//conditionPredicatesList.add(builder.equal(result.get(columnName), Integer.parseInt(keyword[0])));
						}
						// if filter type is Number and sub type is " not equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("notEqual")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								conditionPredicatesList.add(builder.notEqual(subQueryfull.getSelection(),  Integer.parseInt(keyword[0])));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.notEqual(subQueryfailed.getSelection(),  Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is "less than"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("lessThan")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								criteriaQuery.having(builder.lessThan(subQueryfull.getSelection(), Long.valueOf((keyword[0]).toString())));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.lessThan(subQueryfailed.getSelection(), Long.valueOf((keyword[0]).toString())));
							}
						}
						// if filter type is Number and sub type is "less than or equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("lessThanOrEqual")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								conditionPredicatesList.add(builder.lessThanOrEqualTo(subQueryfull.getSelection(), Long.valueOf(keyword[0].toString())));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.lessThanOrEqualTo(subQueryfailed.getSelection(), Long.valueOf(keyword[0].toString())));
							}
							//conditionPredicatesList.add(builder.lessThanOrEqualTo(result.get(columnName), Integer.parseInt(keyword[0])));

						}
						// if filter type is Number and sub type is "greater than"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("greaterThan")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								conditionPredicatesList.add(builder.greaterThan(subQueryfull.getSelection(), Long.valueOf(keyword[0].toString())));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.greaterThan(subQueryfailed.getSelection(), Long.valueOf(keyword[0].toString())));
							}
							

						}
						// if filter type is Number and sub type is "greater than or equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("greaterThanOrEqual")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								conditionPredicatesList.add(builder.greaterThanOrEqualTo(subQueryfull.getSelection(), Long.valueOf(keyword[0].toString())));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.greaterThanOrEqualTo(subQueryfailed.getSelection(), Long.valueOf(keyword[0].toString())));
							}
							

						}
						// if filter type is Number and sub type is "between"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("inRange")) {
							if(condition.getEntityColumn().equalsIgnoreCase("logonsCount")){
								conditionPredicatesList.add(builder.between(subQueryfull.getSelection(), Long.valueOf(keyword[0].toString()), Long.valueOf(keyword[1].toString())));
							}else if(condition.getEntityColumn().equalsIgnoreCase("logonFailuresCount")){
								conditionPredicatesList.add(builder.between(subQueryfailed.getSelection(), Long.valueOf(keyword[0].toString()), Long.valueOf(keyword[1].toString())));
							}
						}
					}
					
					//create the predicate for condition wise
					Predicate conditionPredicate = builder
							.and(builder.or(conditionPredicatesList.toArray(new Predicate[conditionPredicatesList.size()])));
					filterPredicatesList.add(conditionPredicate);
					//for total counts
					Predicate countPredicate = builder
							.and(builder.or(countPredicatesList.toArray(new Predicate[countPredicatesList.size()])));
					filterCountPredicatesList.add(countPredicate);
				}
				// apply AND between the two conditions of single column
				if(!StringUtils.isEmpty(operator) && operator.equalsIgnoreCase("AND") && !filterPredicatesList.isEmpty() && !filterCountPredicatesList.isEmpty()){
					Predicate conditionAnd = builder
							.and(builder.and(filterPredicatesList.toArray(new Predicate[filterPredicatesList.size()])));
					columnListPredicates.add(conditionAnd);
					
					// for counts
					Predicate countConditionAnd = builder
							.and(builder.and(filterCountPredicatesList.toArray(new Predicate[filterCountPredicatesList.size()])));
					countListPredicates.add(countConditionAnd);
				}else if(!StringUtils.isEmpty(operator) && operator.equalsIgnoreCase("OR") && !filterPredicatesList.isEmpty() && !filterCountPredicatesList.isEmpty()){
					// apply OR between the two conditions of single column
					Predicate conditionOr = builder
							.and(builder.or(filterPredicatesList.toArray(new Predicate[filterPredicatesList.size()])));
					columnListPredicates.add(conditionOr);
					//for counts
					Predicate countConditionOr = builder
							.and(builder.or(filterCountPredicatesList.toArray(new Predicate[filterCountPredicatesList.size()])));
					countListPredicates.add(countConditionOr);
				}else{
					// just add the predicate to column predicate with single condition
					Predicate conditionBlank = builder
							.and(filterPredicatesList.toArray(new Predicate[filterPredicatesList.size()]));
					columnListPredicates.add(conditionBlank);
					//for counts
					Predicate countConditionBlank = builder
							.and(filterCountPredicatesList.toArray(new Predicate[filterCountPredicatesList.size()]));
					countListPredicates.add(countConditionBlank);
				}
			}
		}
		
		Predicate subQueryfullPredicate = builder.equal(result.get("screenName"), loginRoot1.get("username"));
		subQueryfull.where(subQueryfullPredicate);
		subQueryfull.groupBy(loginRoot1.get("username"));
		
		Predicate subQueryfailedPredicate = builder.and(builder.equal(result.get("screenName"), loginRoot2.get("username")),builder.equal(loginRoot2.get("loginStatus"), ElementConstants.FAILED_STATUS));
		subQueryfailed.where(subQueryfailedPredicate);
		subQueryfailed.groupBy(loginRoot2.get("username"));
		
		if(!columnListPredicates.isEmpty()){
		
			
			criteriaQuery.where(builder.and(columnListPredicates.toArray(new Predicate[columnListPredicates.size()])));
			
			/*if(countJoin != null){
				totalQuery.where(builder.and(countListPredicates.toArray(new Predicate[countListPredicates.size()])));
			}*/
			//totalQuery.where(builder.and(columnListPredicates.toArray(new Predicate[columnListPredicates.size()])));
		}
		criteriaQuery.groupBy(result.get("userId"));
		
		
		Query<?> query = this.getCurrentSession().createQuery(criteriaQuery);
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		List<Object[]> resultList = (List<Object[]>) query.getResultList();
		
		Query<?> query1 = this.getCurrentSession().createQuery(criteriaQuery);
		totalResults = Long.valueOf(((List<Object[]>) query1.getResultList()).size());
		List<LoginUserCountDto> mapKey = new ArrayList<LoginUserCountDto>();
		for(Object[] object : resultList){
			LoginUserCountDto dto = new LoginUserCountDto();
			dto.setUserId(Long.valueOf(object[0].toString()));
			if(object[1] != null)
				dto.setAllLoginCount(Long.valueOf(object[1].toString()));
			else
				dto.setAllLoginCount(0L);
			if(object[2] != null)
				dto.setFailedLoginCount(Long.valueOf(object[2].toString()));
			else
				dto.setFailedLoginCount(0L);
			mapKey.add(dto);
		}
		returnResult.put(mapKey, totalResults);
		return returnResult;
	}
	
	@Override
	public Users getFirebaseUserBySourceId(String sourceId) {
		Users found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Users> query = builder.createQuery(Users.class);
			Root<Users> user = query.from(Users.class);
			query.select(user);
			query.where(builder.equal(user.get("sourceId"), sourceId));
			found = (Users) this.getCurrentSession().createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}
}
