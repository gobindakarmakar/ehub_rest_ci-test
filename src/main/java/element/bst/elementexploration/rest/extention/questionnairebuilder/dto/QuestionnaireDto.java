package element.bst.elementexploration.rest.extention.questionnairebuilder.dto;

import java.io.Serializable;

/**
 * rambabu
 */
public class QuestionnaireDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String questionnaireId;
	private String questionnaireTitle;
	private String active;

	public QuestionnaireDto() {
		super();
	}

	public QuestionnaireDto(String questionnaireId, String questionnaireTitle, String active) {
		super();
		this.questionnaireId = questionnaireId;
		this.questionnaireTitle = questionnaireTitle;
		this.active = active;
	}

	public String getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(String questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public String getQuestionnaireTitle() {
		return questionnaireTitle;
	}

	public void setQuestionnaireTitle(String questionnaireTitle) {
		this.questionnaireTitle = questionnaireTitle;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

}
