package element.bst.elementexploration.rest.usermanagement.user.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseDaoImpl;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Amit Patel
 *
 */
@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User, Long> implements UserDao {

	public UserDaoImpl() {
		super(User.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDto> getUserList(Long groupId, String status, Integer pageNumber, Integer recordsPerPage,
			String orderBy, String orderIn) {
		List<UserDto> users = new ArrayList<UserDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.usermanagement.user.dto.UserDto(u.userId, u.emailAddress,"
					+ " u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.country, u.status, u.lastLoginDate) ");
			queryBuilder.append("from User u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroup ug on u.userId = ug.userId ");
				queryBuilder.append(" where (ug.groupId = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(status)) {
				if (isNotOptional) {
					queryBuilder.append(" and (u.status = " + status + ") ");

				} else {
					queryBuilder.append(" where (u.status = " + status + ") ");

				}
			}
			queryBuilder.append(" order by ")
			   .append("u.")
			   .append(resolveUserCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			users = (List<UserDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDto> getUserListFullTextSearch(String keyword, String status, Long groupId, Integer pageNumber,
			Integer recordsPerPage, String orderBy, String orderIn) {
		List<UserDto> users = new ArrayList<UserDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.usermanagement.user.dto.UserDto(u.userId, u.emailAddress,"
					+ " u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.country, u.status, u.lastLoginDate) ");
			queryBuilder.append("from User u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroup ug on u.userId = ug.userId ");
				queryBuilder.append(" where (ug.groupId = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(keyword)) {
				String clause = " where ";
				if (isNotOptional)
					clause = " and ";
				queryBuilder.append(clause + " (u.userId like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.emailAddress like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.firstName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.lastName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.middleName like '%" + keyword.replace("\"", "") + "%')");
			}
			if (!StringUtils.isEmpty(status)) {
				queryBuilder.append(" and (u.status = " + status + ") ");
			}
			queryBuilder.append(" order by ")
			   .append("u.")
			   .append(resolveUserCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			users = (List<UserDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserList(Long groupId, String status) {
		List<UserDto> users = new ArrayList<UserDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.usermanagement.user.dto.UserDto(u.userId, u.emailAddress, "
					+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.country, u.status, u.lastLoginDate) ");
			queryBuilder.append("from User u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroup ug on u.userId = ug.userId ");
				queryBuilder.append(" where (ug.groupId = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(status)) {
				if (isNotOptional) {
					queryBuilder.append(" and (u.status = " + status + ") ");

				} else {
					queryBuilder.append(" where (u.status = " + status + ") ");

				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			users = (List<UserDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) users.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserListFullTextSearch(String keyword, String status, Long groupId) {
		List<UserDto> users = new ArrayList<UserDto>();
		try {
			boolean isNotOptional = false;
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select new element.bst.elementexploration.rest.usermanagement.user.dto.UserDto(u.userId, u.emailAddress, "
					+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.country, u.status, u.lastLoginDate) ");
			queryBuilder.append("from User u ");

			if (null != groupId && 0 < groupId) {
				queryBuilder.append(" left outer join UserGroup ug on u.userId = ug.userId ");
				queryBuilder.append(" where (ug.groupId = " + groupId + ") ");
				isNotOptional = true;
			}
			if (!StringUtils.isEmpty(keyword)) {
				String clause = " where ";
				if (isNotOptional)
					clause = " and ";
				queryBuilder.append(clause + " (u.userId like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.emailAddress like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.firstName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.lastName like '%" + keyword.replace("\"", "") + "%'");
				queryBuilder.append(" or u.middleName like '%" + keyword.replace("\"", "") + "%')");
			}
			if (!StringUtils.isEmpty(status)) {
				queryBuilder.append(" and (u.status = " + status + ") ");
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			users = (List<UserDto>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return (long) users.size();
	}
	
	private String resolveUserCaseColumnName(String userColumn) {
		if (userColumn == null)
			return "createdDate";

		switch (userColumn.toLowerCase()) {
		case "firstname":
			return "firstName";
		case "username":
			return "screenName";
		default:
			return "createdDate";
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserDto> listUsersByGroup(Long groupId, Integer userStatus, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		List<UserDto> userList = new ArrayList<>();		
		try {			
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select new element.bst.elementexploration.rest.usermanagement.user.dto.UserDto(u.userId, u.emailAddress, "
					+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.country, u.status, u.lastLoginDate) ")
		       .append("from User u ")
		       .append("left outer join UserGroup ug on u.userId = ug.userId ")
		       .append("where ug.groupId= :groupId ");
			
			if(userStatus != null) {
				hql.append(" and u.status= " + userStatus);
				hql.append(" ");
			}
			hql.append(" order by ")
			   .append("u.")
			   .append(resolveUserCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hql.append(" ");
					hql.append(orderIn);
				}
			}
				
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("groupId", groupId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			userList = (List<UserDto>) query.getResultList();			
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}	
		return userList;
	}

	@Override
	public User getUserByEmailOrUsername(String email) {
		User found = null;
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<User> query = builder.createQuery(User.class);
			Root<User> user = query.from(User.class);
			query.select(user);
			query.where(builder.or(builder.equal(user.get("emailAddress"), email), builder.equal(user.get("screenName"), email)));
			found = (User) this.getCurrentSession().createQuery(query).getSingleResult();
		}catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

	@Override
	public User checkUserNameOrEmailExists(String email, Long id) {
		User found = null;
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<User> query = builder.createQuery(User.class);
			Root<User> user = query.from(User.class);
			query.select(user);
			query.where(builder.and(builder.notEqual(user.get("userId"), id), builder.or(builder.equal(user.get("emailAddress"), email), builder.equal(user.get("screenName"), email))));
			found = (User) this.getCurrentSession().createQuery(query).getSingleResult();
		}catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserByGroup(Long groupId, Integer userStatus) {
		List<UserDto> userList = new ArrayList<>();		
		try {			
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.user.dto.UserDto(u.userId, u.emailAddress, "
					+ "u.firstName, u.lastName, u.middleName, u.screenName, u.dob, u.country, u.status, u.lastLoginDate) ")
		       .append("from User u ")
		       .append("left outer join UserGroup ug on u.userId = ug.userId ")
		       .append("where ug.groupId= :groupId ");
			
			if(userStatus != null) {
				hql.append(" and u.status= " + userStatus);
				hql.append(" ");
			}

			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("groupId", groupId);
			userList = (List<UserDto>) query.getResultList();			
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}	
		return (long) userList.size();
	}	
	
	@Override
	public List<User> getUsersInList(List<Long> idList) {
		List<User> list = new ArrayList<>();
		try {
			if (idList != null && !idList.isEmpty()) {
				CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<User> query = builder.createQuery(User.class);
				Root<User> user = query.from(User.class);
				query.select(user);
				query.where(user.get("userId").in(idList));
				list = (ArrayList<User>) this.getCurrentSession().createQuery(query).getResultList();
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to add participants. Reason : " + e.getMessage());
		} 
		return list;
	}
	
	@Override
	public User getAnalystByUserId(long analystId) {
		try {
			String sql = "select u from User u,UserGroup uug,Group ug where u.userId=uug.userId and u.userId = :userId and uug.groupId=ug.userGroupId and ug.name=:groupName";
			Query<?> query = getCurrentSession().createQuery(sql);
			query.setParameter("groupName", ElementConstants.ANALYST_GROUP_NAME);
			query.setParameter("userId", analystId);
			return (User) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllAnalystWhoDidNotRejectedCaseBefore(Long caseId) {
		List<User> users = new ArrayList<>();
		String sql = "select u from User u,UserGroup uug,Group ug where u.userId not in (select cam.user.userId from CaseAnalystMapping cam where"
				+ " cam.caseSeed.caseId = :caseId and cam.currentStatus =:status) and u.userId=uug.userId and uug.groupId=ug.userGroupId and ug.name=:groupName";
		try {
			Query<?> query = getCurrentSession().createQuery(sql);
			query.setParameter("groupName", ElementConstants.ANALYST_GROUP_NAME);
			query.setParameter("caseId", caseId);
			query.setParameter("status", StatusEnum.REJECTED.getIndex());
			users = (List<User>) query.getResultList();		
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return users;
	}
}