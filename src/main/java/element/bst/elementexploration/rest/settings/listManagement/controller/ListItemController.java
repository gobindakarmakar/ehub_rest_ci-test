package element.bst.elementexploration.rest.settings.listManagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.settings.listManagement.dto.ColorDto;
import element.bst.elementexploration.rest.settings.listManagement.dto.IconDto;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.dto.RoleDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ColorService;
import element.bst.elementexploration.rest.settings.listManagement.service.IconService;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.listManagement.service.RoleService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hanuman
 *
 */

@Api(tags = { "List Management API" })
@RestController
@RequestMapping("/api/listManagement")
public class ListItemController extends BaseController{ 

	@Autowired
	ListItemService listItemService; 
	
	@Autowired
	ColorService colorService;
	
	
	@Autowired
	IconService iconService;
	
	@Autowired
	RoleService roleService;
	
	

	@ApiOperation("Get List Items By List Type")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getListItemsByListType", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getListItemsByListType(HttpServletRequest request,@RequestParam(required=true) String listType, @RequestParam String token) throws Exception {

		List<ListItemDto> listItemDtoList=listItemService.getListItemByListType(listType);
		if(listItemDtoList!=null)
			return new ResponseEntity<>(listItemDtoList,HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("List Items Can't be found!"),HttpStatus.OK);

	}
	@ApiOperation("Get List Items By List Type and display Name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getListItemsByListTypeAndDisplayName", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getListItemsByListTypeAndDisplayName(HttpServletRequest request,@RequestParam(required=true) String listType,
			@RequestParam(required=true) String displayName,@RequestParam String token) throws Exception {
		
		ListItemDto result = listItemService.getListItemByListTypeAndLikeDisplayName(listType, displayName);
		if(result!=null)
			return new ResponseEntity<>(result,HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("List Items Can't be found!"),HttpStatus.OK);

	}
	@ApiOperation("Get List Items By List Item ID")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getListItemByListId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getListItemByListId(HttpServletRequest request,@RequestParam(required=true) Long listId, @RequestParam String token) throws Exception {

		ListItemDto listItemDto=listItemService.getListItemByID(listId);
		if(listItemDto!=null)
			return new ResponseEntity<>(listItemDto,HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("List Item Can't be found!"),HttpStatus.OK);
	}



	@ApiOperation("Save List Item")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation 	successful"),@ApiResponse(code = 500, response = ResponseMessage.class, message = 	ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveOrUpdateListItem", produces = { "application/json; charset=UTF-8" }, consumes = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrUpdateListItem(HttpServletRequest request, @RequestParam String token,
			@RequestBody ListItemDto listItemDto) throws Exception {

		ListItemDto listItemDtos = 	listItemService.saveOrUpdateListItem(listItemDto);
		if(listItemDtos==null) {
			return new ResponseEntity<>(new ResponseMessage("List Item Not Updated"), HttpStatus.NOT_FOUND);
		}else {

			return new ResponseEntity<>(listItemDtos,HttpStatus.OK);
		}
	}

	@ApiOperation("Delete List Item")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/deleteListItem", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteListItem(@RequestParam(required=true) Long listItemId, @RequestParam("token") String token,
			HttpServletRequest request) {

		boolean isListItemDeleted=listItemService.deleteListItem(listItemId);
		return new ResponseEntity<>(isListItemDeleted,HttpStatus.OK);
	}

	@ApiOperation("Get Colors")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getColors", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getColors(HttpServletRequest request, @RequestParam String token) throws Exception {

		List<ColorDto> colorDtoList=colorService.getAllColors();
		return new ResponseEntity<>(colorDtoList,HttpStatus.OK);

	}
	
	@ApiOperation("Get Icons")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getIcons", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getIcons(HttpServletRequest request, @RequestParam String token) throws Exception {

		List<IconDto> iconDtoList=iconService.getAllIcons();
		return new ResponseEntity<>(iconDtoList,HttpStatus.OK);

	}
	
	@ApiOperation("Get Roles")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getRoles", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRoles(HttpServletRequest request, @RequestParam String token) throws Exception {

		List<RoleDto> iconDtoList=roleService.getRoles();
		return new ResponseEntity<>(iconDtoList,HttpStatus.OK);

	}
	
	@ApiOperation("Get List Items by type and search key")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getListItemByListTypeAndsearchKey", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getListItemByListTypeAndsearchKey(HttpServletRequest request, @RequestParam String token,
			@RequestParam String listType, @RequestParam String searchKey) throws Exception {
		return new ResponseEntity<>(listItemService.getListItemByListTypeAndsearchKey(listType, searchKey),
				HttpStatus.OK);

	}
}
