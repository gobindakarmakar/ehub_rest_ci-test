package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "GroupPermissionWithOutObjectsDto")

public class GroupPermissionWithOutObjectsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Id of the grouppermission")

	private Long groupPrivilegeId;

	@ApiModelProperty(value = "Id of the permission",required = false)
	private Long permissionId;

	@ApiModelProperty(value = "Level of the Group",required = false)
	private Integer permissionLevel;

	@ApiModelProperty(value = "Id of the role")
	private Long roleId;

	@ApiModelProperty(value = "Id of the Group",required = false)
	private Long groupId;

	public GroupPermissionWithOutObjectsDto(Long groupPrivilegeId, Long permissionId, Integer permissionLevel,
			Long roleId, Long groupId) {
		super();
		this.groupPrivilegeId = groupPrivilegeId;
		this.permissionId = permissionId;
		this.permissionLevel = permissionLevel;
		this.roleId = roleId;
		this.groupId = groupId;
	}

	public GroupPermissionWithOutObjectsDto() {

	}
	
	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(Integer permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getGroupPrivilegeId() {
		return groupPrivilegeId;
	}

	public void setGroupPrivilegeId(Long groupPrivilegeId) {
		this.groupPrivilegeId = groupPrivilegeId;
	}

}