'use strict';
activitiApp.factory('TopPanelApiService', topPanelApiService);

		topPanelApiService.$inject = [
			'$http',
			'$rootScope',
			'$q',
			'EHUB_API',
			'EHUB_FE_API',
			'Upload'
		];
		
		function topPanelApiService(
				$http,
				$rootScope,
				$q,
				EHUB_API,
				EHUB_FE_API,
				Upload){
			
				return {
					getElasticSearchSuggest: getElasticSearchSuggest,
					getElasticSearchData: getElasticSearchData,
					getUserEvents: getUserEvents,
					getNotificationAlert: getNotificationAlert,
					getLogout: getLogout,
					getAllDocuments: getAllDocuments,
					downloadDocument: downloadDocument,
					uploadDocument: uploadDocument,
					deleteDocument: deleteDocument,
					updateDocumentContent:updateDocumentContent,
					updateDocumentTitle:updateDocumentTitle,
					fullTextSearchDocument:fullTextSearchDocument,
					fullTextSearchSharedDocument:fullTextSearchSharedDocument,
					getUserListing:getUserListing,
					shareDocumentWithUser:shareDocumentWithUser,
					unshareDocumentWithUser:unshareDocumentWithUser,
					sharedDocument:sharedDocument,
					listCases:listCases,
					shareDocumentWithCase:shareDocumentWithCase,
					unshareDocumentWithCase:unshareDocumentWithCase,
					getCaseMapping:getCaseMapping,
					addComment:addComment,
					listDocComment:listDocComment,
					documentParser:documentParser
				};
				/*
			     * @purpose: get elastic search suggest
			     * @created: 12 sep 2017
			     * @params: params(object), data(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getElasticSearchSuggest(params, data){
					var apiUrl =  EHUB_API + 'elasticsearch/_suggest';
			        var request = $http({
			            method: "POST",
			            url: apiUrl,
			            params: params,
			            data: data
			        });
			        return(request
			                .then(getElasticSearchSuggestSuccess)
			                .catch(getElasticSearchSuggestError));
			
			        /*getElasticSearchSuggest error function*/
			        function getElasticSearchSuggestError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			        /*getElasticSearchSuggest success function*/
			        function getElasticSearchSuggestSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: get elastic search data
			     * @created: 12 sep 2017
			     * @params: params(object), data(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getElasticSearchData(params, data){
					var apiUrl = EHUB_API + 'elasticsearch/getData';
			        var request = $http({
			            method: "POST",
			            url: apiUrl,
			            params: params,
			            data: data
			        });
			        return(request
			                .then(getElasticSearchDataSuccess)
			                .catch(getElasticSearchDataError));
			
			        /*getElasticSearchData error function*/
			        function getElasticSearchDataError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			             /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			         /*getElasticSearchData success function*/
			        function getElasticSearchDataSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: get user events
			     * @created: 13 sep 2017
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getUserEvents(params){
					var apiUrl = EHUB_API + 'events/getUserEvents';
			        var request = $http({
			            method: "GET",
			            url: apiUrl,
			            params: params,
			            headers: {
			                'Content-Type': 'application/json'
			            }	
			        });
			        return(request
			                .then(getUserEventsSuccess)
			                .catch(getUserEventsError));
			
			        /*getUserEvents error function*/
			        function getUserEventsError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			        /*getUserEvents success function*/
			        function getUserEventsSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: get notification alert
			     * @created: 13 sep 2017
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getNotificationAlert(params){
					var apiUrl = EHUB_API + 'notification/getNotificationAlert';
			        var request = $http({
			            method: "GET",
			            url: apiUrl,
			            params: params
			        });
			        return(request
			                .then(getNotificationAlertSuccess)
			                .catch(getNotificationAlertError));
			
			        /*getNotificationAlert error function*/
			        function getNotificationAlertError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			             /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			        /*getNotificationAlert success function*/
			        function getNotificationAlertSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: get logout
			     * @created: 13 sep 2017
			     * @params: null
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getLogout(){
					var apiUrl = EHUB_FE_API + 'logout';
			        var request = $http({
			            method: 'GET',
			            url: apiUrl
			        });
			        return(request
			                .then(getLogoutSuccess)
			                .catch(getLogoutError));
			
			        /*getLogout error function*/
			        function getLogoutError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			        /*getLogout success function*/
			        function getLogoutSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: get list of documents
			     * @created: 20 sep 2017
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getAllDocuments(params){
					var apiUrl = EHUB_API + 'documentStorage/myDocuments';
			        var request = $http({
			            method: "GET",
			            url: apiUrl,
			            params: params
			        });
			        return(request
			                .then(getAllDocumentsSuccess)
			                .catch(getAllDocumentsError));

			        /*getAllDocuments error function*/
			        function getAllDocumentsError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			             /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			         /*getAllDocuments success function*/
			        function getAllDocumentsSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: download document
			     * @created: 20 sep 2017
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function downloadDocument(params){
					var apiUrl = EHUB_API + 'documentStorage/downloadDocument';
			        var request = $http({
			            method: "GET",
			            url: apiUrl,
			            params: params,
				        responseType: "arraybuffer"
			        });
			        return(request
			                .then(getAllDocumentsSuccess)
			                .catch(getAllDocumentsError));

			        /*getAllDocuments error function*/
			        function getAllDocumentsError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			             /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			         /*getAllDocuments success function*/
			        function getAllDocumentsSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: upload document
			     * @created: 20 sep 2017
			     * @params: params(object), File(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function uploadDocument(params, file){
					var apiUrl = EHUB_API + 'documentStorage/uploadDocument';
			        var request = Upload.upload({
			            method: "POST",
			            url: apiUrl,
			            params: params,
			            data: file
			        });
			        return(request
			                .then(uploadDocumentSuccess)
			                .catch(uploadDocumentError));

			        /*uploadDocument error function*/
			        function uploadDocumentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function uploadDocumentSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: update document
			     * @created: 20 sep 2017
			     * @params: params(object), File(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function updateDocumentContent(params, file){
					var apiUrl = EHUB_API + 'documentStorage/updateDocumentContent';
			        var request = Upload.upload({
			            method: "POST",
			            url: apiUrl,
			            params: params,
			            data: file
			        });
			        return(request
			                .then(updateDocumentContentSuccess)
			                .catch(updateDocumentContentError));

			        /*uploadDocument error function*/
			        function updateDocumentContentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function updateDocumentContentSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: update document title
			     * @created: 03 sep 2017
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function updateDocumentTitle(params){
			    	var apiUrl =EHUB_API+'documentStorage/updateDocument'+"?token="+params.token+"&docId="+params.docId;
	          		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: apiUrl ,
					    	data:{
					    		'docId':params.docId,
					    		'title':params.title,
					    		'remark':' '
					    	
					    	}
				        });
			        return(request
			                .then(updateDocumentTitleSuccess)
			                .catch(updateDocumentTitleError));

			        /*uploadDocument error function*/
			        function updateDocumentTitleError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function updateDocumentTitleSuccess(response) {
			            return(response);
			        }
				}

				
				
				/*
			     * @purpose: delete document
			     * @created: 12 mar 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function deleteDocument(params){
					var apiUrl = EHUB_API + 'documentStorage/softDeleteDocument';
			        var request = Upload.upload({
			            method: "DELETE",
			            url: apiUrl,
			            params: params
			        });
			        return(request
			                .then(deleteDocumentSuccess)
			                .catch(deleteDocumentError));

			        /*deleteDocument error function*/
			        function deleteDocumentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*deleteDocument success function*/
			        function deleteDocumentSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: Full test search
			     * @created: 03 April 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function fullTextSearchDocument(params){
					var apiUrl = EHUB_API + 'documentStorage/fullTextSearchMyDocuments';
			        var request = Upload.upload({
			            method: "GET",
			            url: apiUrl,
			            params: params
			        });
			        return(request
			                .then(fullTextSearchDocumentSuccess)
			                .catch(fullTextSearchDocumentError));

			        /*fullTextSearchDocument error function*/
			        function fullTextSearchDocumentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*fullTextSearchDocument success function*/
			        function fullTextSearchDocumentSuccess(response) {
			            return(response);
			        }
				}
				/*
			     * @purpose: Full text search shared
			     * @created: 03 April 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function fullTextSearchSharedDocument(params){
					var apiUrl = EHUB_API + 'documentStorage/fullTextSearchDocument';
			        var request = Upload.upload({
			            method: "GET",
			            url: apiUrl,
			            params: params
			        });
			        return(request
			                .then(fullTextSearchSharedDocumentSuccess)
			                .catch(fullTextSearchSharedDocumentError));

			        /*fullTextSearchSharedDocument error function*/
			        function fullTextSearchSharedDocumentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*fullTextSearchSharedDocument success function*/
			        function fullTextSearchSharedDocumentSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: Get all user detail
			     * @created: 04 April 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function getUserListing(params){
					var apiUrl = EHUB_API + 'user/getUserListing'+"?token="+params.token;
					  var request = $http({
				            method: "GET",
				            url: apiUrl
				        });
			        return(request
			                .then(getUserListingSuccess)
			                .catch(getUserListingError));

			        /*fullTextSearchDocument error function*/
			        function getUserListingError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*fullTextSearchDocument success function*/
			        function getUserListingSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: Get all case detail
			     * @created: 05 April 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function listCases(params){
					var apiUrl = EHUB_API + 'case/listCases'+"?token="+params.token;
					  var request = $http({
				            method: "GET",
				            url: apiUrl
				        });
			        return(request
			                .then(listCasesSuccess)
			                .catch(listCasesError));

			        /*fullTextSearchDocument error function*/
			        function listCasesError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*fullTextSearchDocument success function*/
			        function listCasesSuccess(response) {
			            return(response);
			        }
				}
				
				
				/*
			     * @purpose: share Document
			     * @created: 03 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function shareDocumentWithUser(params){
			    	var apiUrl =EHUB_API+'documentStorage/shareDocumentWithUser'+"?token="+params.token+"&userId="+params.userId +"&docId="+params.docId+"&permission="+params.permission;
	          		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: apiUrl ,
					    	data:{
					    		'docId':params.docId
					    	}
				        });
			        return(request
			                .then(shareDocumentWithUserSuccess)
			                .catch(shareDocumentWithUserError));

			        /*uploadDocument error function*/
			        function shareDocumentWithUserError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function shareDocumentWithUserSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: Unshare Document
			     * @created: 03 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function unshareDocumentWithUser(params){
			    	var apiUrl =EHUB_API+'documentStorage/unshareDocumentWithUser'+"?token="+params.token+"&userId="+params.userId +"&docId="+params.docId +"&comment="+params.comment;
	          		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: apiUrl ,
					    	data:{
					    		'docId':params.docId,
					    		'comment':params.comment
					    	}
				        });
			        return(request
			                .then(unshareDocumentWithUserSuccess)
			                .catch(unshareDocumentWithUserError));

			        /*uploadDocument error function*/
			        function unshareDocumentWithUserError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function unshareDocumentWithUserSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: share document list
			     * @created: 04  april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function sharedDocument(params){
			    	var apiUrl =EHUB_API+'documentStorage/sharedDocument';
			    	 var request = $http({
				            method: "GET",
				            url: apiUrl,
				            params: params
				        });
			        return(request
			                .then(sharedDocumentSuccess)
			                .catch(sharedDocumentError));

			        /*uploadDocument error function*/
			        function sharedDocumentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function sharedDocumentSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: link case to document
			     * @created: 03 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function shareDocumentWithCase(params){
			    	var apiUrl =EHUB_API+'documentStorage/shareDocumentWithCase'+"?token="+params.token+"&caseId="+params.caseId +"&docId="+params.docId;
	          		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: apiUrl ,
					    	data:{
					    		'docId':params.docId
					    	}
				        });
			        return(request
			                .then(shareDocumentWithCaseSuccess)
			                .catch(shareDocumentWithCaseError));

			        /*uploadDocument error function*/
			        function shareDocumentWithCaseError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function shareDocumentWithCaseSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: Unlink case to document
			     * @created: 06 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function unshareDocumentWithCase(params){
			    	var apiUrl =EHUB_API+'documentStorage/unshareDocumentWithCase'+"?token="+params.token+"&caseId="+params.caseId +"&docId="+params.docId;
	          		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: apiUrl ,
					    	data:{
					    		'docId':params.docId
					    	}
				        });
			        return(request
			                .then(unshareDocumentWithCaseSuccess)
			                .catch(unshareDocumentWithCaseError));

			        /*uploadDocument error function*/
			        function unshareDocumentWithCaseError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function unshareDocumentWithCaseSuccess(response) {
			            return(response);
			        }
				}
				
				
				/*
			     * @purpose: Document Associations
			     * @created: 05 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function getCaseMapping(params){
			    	var apiUrl =EHUB_API+'documentStorage/getCaseMapping'+"?token="+params.token+"&docId="+params.docId;
			    	 var request = $http({
				            method: "GET",
				            url: apiUrl
				        });
			        return(request
			                .then(getCaseMappingSuccess)
			                .catch(getCaseMappingError));

			        /*uploadDocument error function*/
			        function getCaseMappingError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function getCaseMappingSuccess(response) {
			            return(response);
			        }
				}

				/*
			     * @purpose: Add Comment
			     * @created: 06 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function addComment(params){
			    	var apiUrl =EHUB_API+'documentStorage/addComment'+"?token="+params.token+"&docId="+params.docId ;
	          		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: apiUrl ,
					    	data:{
					    		'docId':params.docId,
					    		'title':'testComment',
					    		'description':params.commentdesc
					    	}
				        });
			        return(request
			                .then(addCommentSuccess)
			                .catch(addCommentError));

			        /*uploadDocument error function*/
			        function addCommentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*uploadDocument success function*/
			        function addCommentSuccess(response) {
			            return(response);
			        }
				}
				
				
				
				/*
			     * @purpose: List Comments
			     * @created: 05 april 2018
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: varsha
			    */
				function listDocComment(params){
			    	var apiUrl =EHUB_API+'documentStorage/listComments'+"?token="+params.token+"&docId="+params.docId;
			    	 var request = $http({
				            method: "GET",
				            url: apiUrl
				        });
			        return(request
			                .then(listDocCommentSuccess)
			                .catch(listDocCommentError));
			        /*listDocComment error function*/
			        function listDocCommentError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }

			       /*listDocComment success function*/
			        function listDocCommentSuccess(response) {
			            return(response);
			        }
				}
				
				/*
			     * @purpose: documentParser
			     * @created: 16th april 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
				function documentParser(params){
					var apiUrl =  EHUB_API + 'documentParser/generateJsonResponse';
					var request = $http({
			            method: "GET",
			            url: apiUrl,
		                params: params
			        });
			        return(request
			                .then(documentParserSuccess)
			                .catch(documentParserError));
			
			        /*docAggregator error function*/
			        function documentParserError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			        /*docAggregator success function*/
			        function documentParserSuccess(response) {
			            return(response);
			        }
				}
	         

				
			
		};