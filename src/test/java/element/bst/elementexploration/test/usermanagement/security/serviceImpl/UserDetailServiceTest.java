package element.bst.elementexploration.test.usermanagement.security.serviceImpl;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.security.serviceImpl.UserDetailServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;


/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDetailServiceTest {
	
	@Mock
	UsersService userServiceMock;
	
	@Mock
	ListItem listItem;
	
	@InjectMocks
	UserDetailServiceImpl userDetailService; 
	
	public Users user;
	
	@Before
	public void doSetup() {
		user=new Users("analyst","Analyst","Analyst",new Date(),listItem);
		user.setEmailAddress("analyst@gmail.com");
		user.setPassword("analyst#123");
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void loadUserByUsernameTest(){
		Mockito.when(userServiceMock.getUserObjectByEmailOrUsername(Mockito.anyString())).thenReturn(user);
		UserDetails userDetails = userDetailService.loadUserByUsername("analyst@gmail.com");
		Assert.assertNotNull(userDetails);
	}
	
	
	@Test(expected = UsernameNotFoundException.class)
	public void whenUserIsNull_ThenExThrow(){
		Mockito.when(userServiceMock.getUserObjectByEmailOrUsername(Mockito.anyString())).thenReturn(null);
		 userDetailService.loadUserByUsername("test@gmail.com");		
	}

}
