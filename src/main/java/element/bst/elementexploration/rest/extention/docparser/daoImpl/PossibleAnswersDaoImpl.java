package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.PossibleAnswersDao;
import element.bst.elementexploration.rest.extention.docparser.domain.PossibleAnswers;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("possibleAnswersDao")
public class PossibleAnswersDaoImpl extends GenericDaoImpl<PossibleAnswers, Long> implements PossibleAnswersDao {

	public PossibleAnswersDaoImpl() {
		super(PossibleAnswers.class);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<PossibleAnswers> fetchPossibleAnswers(Long questionId)
	{
		List<PossibleAnswers> possibleAnswersList = null;
		try 
		{
			StringBuilder hql = new StringBuilder();
			hql.append("select pa from PossibleAnswers pa join pa.documentQuestions dq where pa.deleted=:status and dq.id ="+questionId+"");
			Query query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("status", false);
			possibleAnswersList = (List<PossibleAnswers>) query.getResultList();
		} 
		catch (Exception e) 
		{
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return possibleAnswersList;
		
	}


}
