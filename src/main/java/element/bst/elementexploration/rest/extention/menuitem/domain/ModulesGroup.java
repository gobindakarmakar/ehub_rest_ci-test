package element.bst.elementexploration.rest.extention.menuitem.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_menu_groups")
public class ModulesGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menuGroupId")
	private Long moduleGroupId;

	@ApiModelProperty(value = "Menu Group Name")
	@Column(name = "menuGroupName", columnDefinition = "LONGTEXT")
	@NotEmpty(message = "group cannot be blank.")
	private String moduleGroupName;

	@ApiModelProperty(value = "Menu Group Name")
	@Column(name = "menuGroupIcon")
	private String moduleGroupIcon;

	@ApiModelProperty(value = "Menu Group Color")
	@Column(name = "menuGroupColor")
	private String moduleGroupColor;
	
	@ApiModelProperty(value = "Menu Group Class")
	@Column(name = "menuGroupClass")
	private String moduleGroupClass;

	@ApiModelProperty(value = "Localization ID")
	@Column(name = "localizationID")
	private Integer localizationID;
	
	@ApiModelProperty(value = "Menu Group Code(for permissions)")
	@Column(name = "menuGroupCode")
	private String menuGroupCode;

	@OneToMany(mappedBy = "moduleGroup", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private List<Modules> modules;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "permissionId")
	private Permissions permissionId;
	

	public Long getModuleGroupId() {
		return moduleGroupId;
	}

	public void setModuleGroupId(Long moduleGroupId) {
		this.moduleGroupId = moduleGroupId;
	}

	public String getModuleGroupName() {
		return moduleGroupName;
	}

	public void setModuleGroupName(String moduleGroupName) {
		this.moduleGroupName = moduleGroupName;
	}

	public String getModuleGroupIcon() {
		return moduleGroupIcon;
	}

	public void setModuleGroupIcon(String moduleGroupIcon) {
		this.moduleGroupIcon = moduleGroupIcon;
	}

	public List<Modules> getModules() {
		return modules;
	}

	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}

	public String getModuleGroupColor() {
		return moduleGroupColor;
	}

	public void setModuleGroupColor(String moduleGroupColor) {
		this.moduleGroupColor = moduleGroupColor;
	}

	public Integer getLocalizationID() {
		return localizationID;
	}

	public void setLocalizationID(Integer localizationID) {
		this.localizationID = localizationID;
	}

	public String getModuleGroupClass() {
		return moduleGroupClass;
	}

	public void setModuleGroupClass(String moduleGroupClass) {
		this.moduleGroupClass = moduleGroupClass;
	}

	public String getMenuGroupCode() {
		return menuGroupCode;
	}

	public void setMenuGroupCode(String menuGroupCode) {
		this.menuGroupCode = menuGroupCode;
	}

	public Permissions getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Permissions permissionId) {
		this.permissionId = permissionId;
	}

	
	
}
