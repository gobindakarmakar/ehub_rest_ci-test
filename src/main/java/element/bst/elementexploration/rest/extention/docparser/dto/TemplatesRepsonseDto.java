package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author suresh
 *
 */
public class TemplatesRepsonseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String templateName;
	private String templateFileType;
	private boolean isScannedPdf;
	private boolean isFillablePdf;
	private int scannedValue;

	private int docsParsed;
	private int noOfElements;
	private Date createdOn;
	private Date modifiedOn;
	private String createdBy;
	private String modifiedBy;
	private Long initialDocId;
	private Long createdById;
	private Long modifiedById;

	public TemplatesRepsonseDto() {
	}

	public TemplatesRepsonseDto(long id, String templateName, String templateFileType, boolean isScannedPdf,
			boolean isFillablePdf, int scannedValue, int docsParsed, int noOfElements, Date createdOn, Date modifiedOn,
			String createdBy, String modifiedBy, Long initialDocId, Long createdById, Long modifiedById) {
		super();
		this.id = id;
		this.templateName = templateName;
		this.templateFileType = templateFileType;
		this.isScannedPdf = isScannedPdf;
		this.isFillablePdf = isFillablePdf;
		this.scannedValue = scannedValue;
		this.docsParsed = docsParsed;
		this.noOfElements = noOfElements;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.initialDocId = initialDocId;
		this.createdById = createdById;
		this.modifiedById = modifiedById;
	}

	public Long getInitialDocId() {
		return initialDocId;
	}

	public void setInitialDocId(Long initialDocId) {
		this.initialDocId = initialDocId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateFileType() {
		return templateFileType;
	}

	public void setTemplateFileType(String templateFileType) {
		this.templateFileType = templateFileType;
	}

	public boolean isScannedPdf() {
		return isScannedPdf;
	}

	public void setScannedPdf(boolean isScannedPdf) {
		this.isScannedPdf = isScannedPdf;
	}

	public int getScannedValue() {
		return scannedValue;
	}

	public void setScannedValue(int scannedValue) {
		this.scannedValue = scannedValue;
	}

	public boolean isFillablePdf() {
		return isFillablePdf;
	}

	public void setFillablePdf(boolean isFillablePdf) {
		this.isFillablePdf = isFillablePdf;
	}

	public int getDocsParsed() {
		return docsParsed;
	}

	public void setDocsParsed(int docsParsed) {
		this.docsParsed = docsParsed;
	}

	public int getNoOfElements() {
		return noOfElements;
	}

	public void setNoOfElements(int noOfElements) {
		this.noOfElements = noOfElements;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getCreatedById() {
		return createdById;
	}

	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}

	public Long getModifiedById() {
		return modifiedById;
	}

	public void setModifiedById(Long modifiedById) {
		this.modifiedById = modifiedById;
	}

}
