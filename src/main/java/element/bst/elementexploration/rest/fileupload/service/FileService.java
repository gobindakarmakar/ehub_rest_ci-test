package element.bst.elementexploration.rest.fileupload.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface FileService extends GenericService<FileBst, Long> {

	boolean uploadFile(MultipartFile uploadFile, Long userId,String fileName) throws Exception;

	byte[] downloadFile(Long id, long userId, HttpServletResponse response) throws Exception;

	boolean deleteFileById(Long id, long userId);

	boolean updateFile(MultipartFile uploadFile, Long userId, Long docId );

	FileBst findFileByName(String fileName);

}
