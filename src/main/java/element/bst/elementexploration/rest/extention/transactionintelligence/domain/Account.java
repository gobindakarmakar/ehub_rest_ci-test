package element.bst.elementexploration.rest.extention.transactionintelligence.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AbstractDomainObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountOwnershipType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountType;

/**
 * <h1>Account</h1>
 * <p>
 * This class consists customer account details
 * </p>
 * 
 * @author suresh
 */
@Entity
@Table(name = "ACCOUNT")
public class Account extends AbstractDomainObject {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String accountNumber;
	private AccountOwnershipType accountOwnershipType;
	private String primaryCustomerIdentifier;
	private Date accountOpenDate;
	private String jurisdiction;
	private String sourceSystem;
	private AccountType accountType;
	private String accountName;
	private String segmentId;
	private String accountCurrency;
	private Double maximumAmount;
	private CustomerDetails customerDetails;
	private String bankName;

	public Account() {
	}

	public Account(Long id, String accountNumber, AccountOwnershipType accountOwnershipType,
			String primaryCustomerIdentifier, Date accountOpenDate, String jurisdiction, String sourceSystem,
			AccountType accountType, String accountName, String segmentId, String accountCurrency, Double maximumAmount,
			CustomerDetails customerDetails, String bankName) {
		super();
		this.id = id;
		this.accountNumber = accountNumber;
		this.accountOwnershipType = accountOwnershipType;
		this.primaryCustomerIdentifier = primaryCustomerIdentifier;
		this.accountOpenDate = accountOpenDate;
		this.jurisdiction = jurisdiction;
		this.sourceSystem = sourceSystem;
		this.accountType = accountType;
		this.accountName = accountName;
		this.segmentId = segmentId;
		this.accountCurrency = accountCurrency;
		this.maximumAmount = maximumAmount;
		this.customerDetails = customerDetails;
		this.bankName = bankName;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "ACCOUNT_NUMBER",unique = true)
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ACCOUNT_OWNERSHIP_TYPE")
	public AccountOwnershipType getAccountOwnershipType() {
		return accountOwnershipType;
	}

	public void setAccountOwnershipType(AccountOwnershipType accountOwnershipType) {
		this.accountOwnershipType = accountOwnershipType;
	}

	@Column(name = "ACCOUNT_OPEN_DATE")
	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	@Column(name = "JURISDICTION")
	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ACCOUNT_TYPE")
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	@Column(name = "ACCOUNT_NAME")
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Column(name = "ACCOUNT_CURRENCY")
	public String getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	@Column(name = "PRIMARY_CUSTOMER_IDENTIFIER")
	public String getPrimaryCustomerIdentifier() {
		return primaryCustomerIdentifier;
	}

	public void setPrimaryCustomerIdentifier(String primaryCustomerIdentifier) {
		this.primaryCustomerIdentifier = primaryCustomerIdentifier;
	}

	@Column(name = "SOURCE_SYSTEM")
	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	@Column(name = "SEGMENT_ID")
	public String getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}

	@Column(name = "MAXIMUM_AMOUNT")
	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

	@ManyToOne
	@JoinColumn(name = "CUSTOMER_ID", nullable = true)
	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

	@Column(name = "BANK_NAME")
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accountCurrency == null) ? 0 : accountCurrency.hashCode());
		result = prime * result + ((accountName == null) ? 0 : accountName.hashCode());
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((accountOpenDate == null) ? 0 : accountOpenDate.hashCode());
		result = prime * result + ((accountOwnershipType == null) ? 0 : accountOwnershipType.hashCode());
		result = prime * result + ((accountType == null) ? 0 : accountType.hashCode());
		result = prime * result + ((bankName == null) ? 0 : bankName.hashCode());
		result = prime * result + ((customerDetails == null) ? 0 : customerDetails.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jurisdiction == null) ? 0 : jurisdiction.hashCode());
		result = prime * result + ((maximumAmount == null) ? 0 : maximumAmount.hashCode());
		result = prime * result + ((primaryCustomerIdentifier == null) ? 0 : primaryCustomerIdentifier.hashCode());
		result = prime * result + ((segmentId == null) ? 0 : segmentId.hashCode());
		result = prime * result + ((sourceSystem == null) ? 0 : sourceSystem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountCurrency == null) {
			if (other.accountCurrency != null)
				return false;
		} else if (!accountCurrency.equals(other.accountCurrency))
			return false;
		if (accountName == null) {
			if (other.accountName != null)
				return false;
		} else if (!accountName.equals(other.accountName))
			return false;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (accountOpenDate == null) {
			if (other.accountOpenDate != null)
				return false;
		} else if (!accountOpenDate.equals(other.accountOpenDate))
			return false;
		if (accountOwnershipType != other.accountOwnershipType)
			return false;
		if (accountType != other.accountType)
			return false;
		if (bankName == null) {
			if (other.bankName != null)
				return false;
		} else if (!bankName.equals(other.bankName))
			return false;
		if (customerDetails == null) {
			if (other.customerDetails != null)
				return false;
		} else if (!customerDetails.equals(other.customerDetails))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jurisdiction == null) {
			if (other.jurisdiction != null)
				return false;
		} else if (!jurisdiction.equals(other.jurisdiction))
			return false;
		if (maximumAmount == null) {
			if (other.maximumAmount != null)
				return false;
		} else if (!maximumAmount.equals(other.maximumAmount))
			return false;
		if (primaryCustomerIdentifier == null) {
			if (other.primaryCustomerIdentifier != null)
				return false;
		} else if (!primaryCustomerIdentifier.equals(other.primaryCustomerIdentifier))
			return false;
		if (segmentId == null) {
			if (other.segmentId != null)
				return false;
		} else if (!segmentId.equals(other.segmentId))
			return false;
		if (sourceSystem == null) {
			if (other.sourceSystem != null)
				return false;
		} else if (!sourceSystem.equals(other.sourceSystem))
			return false;
		return true;
	}

}
