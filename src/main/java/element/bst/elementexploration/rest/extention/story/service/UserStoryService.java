package element.bst.elementexploration.rest.extention.story.service;

import element.bst.elementexploration.rest.extention.story.domain.UserStory;
import element.bst.elementexploration.rest.extention.story.dto.StoryDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface UserStoryService extends GenericService<UserStory, Long> {

	void markAsFavourite(String storyId, Long userId);

	StoryDto getFavouriteStory(Long userId);

}
