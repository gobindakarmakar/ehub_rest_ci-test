package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Alert dash board response")
public class AlertDashBoardRespDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "Id")
	private Long id;
	@ApiModelProperty(value = "Customer name")
	private String customerName;
	@ApiModelProperty(value = "Alert business date")
	private Date alertBusinessDate;
	@ApiModelProperty(value = "Total amount of alert transactions")
	private Double totalAmount;
	@ApiModelProperty(value = "Customer id")
	private String focalEntityId;
	@ApiModelProperty(value = "Search name")
	private String searchName;
	@ApiModelProperty(value = "Customer type")
	private CustomerType customerType;
	@ApiModelProperty(value = "Alert scenario")
	private String scenario;
	@ApiModelProperty(value = "Alert created date")
	private Date alertCreatedDate;
	@ApiModelProperty(value = "Transaction id")
	private Long alertTransactionId;
	@ApiModelProperty(value = "Alert risk score")
	private Integer alertRiskScore;
	@ApiModelProperty(value = "Entity type")
	private String entityType;

	// private String customerType;

	public AlertDashBoardRespDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AlertDashBoardRespDto(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public AlertDashBoardRespDto(Long id, String customerName, Date alertBusinessDate, Double totalAmount,
			String focalEntityId, String searchName, CustomerType customerType, String scenario, Date alertCreatedDate,
			Long alertTransactionId) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
		this.focalEntityId = focalEntityId;
		this.searchName = searchName;
		this.customerType = customerType;
		this.scenario = scenario;
		this.alertCreatedDate = alertCreatedDate;
		this.alertTransactionId = alertTransactionId;
	}

	public AlertDashBoardRespDto(Long id, String customerName, Date alertBusinessDate, Double totalAmount,
			String focalEntityId, String searchName, CustomerType customerType, String scenario, Date alertCreatedDate,
			Long alertTransactionId, Integer alertRiskScore) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
		this.focalEntityId = focalEntityId;
		this.searchName = searchName;
		this.customerType = customerType;
		this.scenario = scenario;
		this.alertCreatedDate = alertCreatedDate;
		this.alertTransactionId = alertTransactionId;
		this.alertRiskScore = alertRiskScore;
	}

	public AlertDashBoardRespDto(Long id, String customerName, Date alertBusinessDate, Double totalAmount,
			String scenario, Long alertTransactionId) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
		this.scenario = scenario;
		this.alertTransactionId = alertTransactionId;
	}

	public AlertDashBoardRespDto(Long id, String customerName, Date alertBusinessDate, Double totalAmount,
			String focalEntityId, String searchName, CustomerType customerType) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
		this.focalEntityId = focalEntityId;
		this.searchName = searchName;
		this.customerType = customerType;
	}

	public AlertDashBoardRespDto(String customerName, Date alertBusinessDate, Double totalAmount) {
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
	}

	public AlertDashBoardRespDto(Long id, String customerName, Date alertBusinessDate, Double totalAmount) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
	}

	public AlertDashBoardRespDto(Long id, String customerName, Date alertBusinessDate, Double totalAmount,
			String focalEntityId, String searchName, CustomerType customerType, String scenario, Date alertCreatedDate,
			Long alertTransactionId, String entityType) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
		this.focalEntityId = focalEntityId;
		this.searchName = searchName;
		this.customerType = customerType;
		this.scenario = scenario;
		this.alertCreatedDate = alertCreatedDate;
		this.alertTransactionId = alertTransactionId;
		this.entityType = entityType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFocalEntityId() {
		return focalEntityId;
	}

	public void setFocalEntityId(String focalEntityId) {
		this.focalEntityId = focalEntityId;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public CustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public Date getAlertCreatedDate() {
		return alertCreatedDate;
	}

	public void setAlertCreatedDate(Date alertCreatedDate) {
		this.alertCreatedDate = alertCreatedDate;
	}

	public Long getAlertTransactionId() {
		return alertTransactionId;
	}

	public void setAlertTransactionId(Long alertTransactionId) {
		this.alertTransactionId = alertTransactionId;
	}

	public Integer getAlertRiskScore() {
		return alertRiskScore;
	}

	public void setAlertRiskScore(Integer alertRiskScore) {
		this.alertRiskScore = alertRiskScore;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

}
