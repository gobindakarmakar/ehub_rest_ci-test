package element.bst.elementexploration.rest.generic.daoImpl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.service.FeedManagementService;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertManagementService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.FilterColumnDto;
import element.bst.elementexploration.rest.util.RequestFilterDto;

/**
 * 
 * @author Amit Patel
 *
 */
@Repository("genericDao")
public class GenericDaoImpl<E, I extends Serializable> implements GenericDao<E, I> {
	private Class<E> entityClass;

	public GenericDaoImpl() {
	}

	@Autowired
	private SessionFactory sessionFactory;

	@Lazy
	@Autowired
	UsersService usersService;
	
	@Lazy
	@Autowired
	AlertManagementService alertService;
	
	public GenericDaoImpl(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<E> findAll() throws DataAccessException {
		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<E> query = builder.createQuery(entityClass);
		Root<E> root = query.from(entityClass);
		query.select(root);
		return getCurrentSession().createQuery(query).getResultList();
	}

	@Override
	public E find(I id) {
		return (E) getCurrentSession().get(entityClass, id);
	}

	@Override
	public E create(E e) {
		Serializable id = getCurrentSession().save(e);
		return (E) getCurrentSession().get(entityClass, id);
	}

	@Override
	public List<E> saveAll(Iterable<E> e) {
		List<E> result = new ArrayList<E>();
		for (E entity : e) {
			Serializable id = getCurrentSession().save(entity);
			result.add((E) getCurrentSession().get(entityClass, id));
		}

		return result;
	}

	@Override
	public void update(E e)  {
		getCurrentSession().update(e);
	}
	@Override
	public void saveOrUpdate(E e) {
		getCurrentSession().saveOrUpdate(e);
	}

	@Override
	public void delete(E e) {
		getCurrentSession().delete(e);
	}

	@Override
	public void flush() {
		getCurrentSession().flush();
	}

	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public Map<List<E>, Long> filterData(E e,List<FilterColumnDto> columnsList, Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy) {
		//List<E> resultList = new ArrayList<E>();
		Map<List<E>, Long> returnResult = new HashMap<List<E>, Long>();
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<E> criteriaQuery = (CriteriaQuery<E>) builder.createQuery(e.getClass());
		Root<E> result = (Root<E>) criteriaQuery.from(e.getClass());
		criteriaQuery.select(result);
		
		CriteriaQuery<Long> totalQuery = builder.createQuery(Long.class);
		Root<Long> countResult = (Root<Long>) totalQuery.from(e.getClass());
		totalQuery.select(builder.count(countResult));
		
		//List<Predicate> predicatesFinalList = new ArrayList<Predicate>();
		//Join<E, FeedManagement> feedJoin = null;
		//Join<E, FeedManagement> countJoin = null;
		
		Root<FeedManagement> feedRoot = null;
		Root<FeedManagement> feedRoot1 = null;
		Root<Groups> groupRoot = null;
		Root<Groups> groupRoot1 = null;
		Root<Users> assigneeRoot = null;
		Root<Users> assigneeRoot1 = null;
		Root<ListItem> statusRoot = null;
		Root<ListItem> statusRoot1 = null;
		
		Boolean isAlert=false;
		if(e.getClass().getName().equalsIgnoreCase("element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts")){
			isAlert = true;
		}
		
		//FOr order by
		if(isAlert){
			/*if(orderBy.equals("feed")){
				feedRoot = (Root<FeedManagement>) criteriaQuery.from(FeedManagement.class);
				feedRoot1 = (Root<FeedManagement>) totalQuery.from(FeedManagement.class);
				
			}*/
			if(orderBy.equals("groupLevel")){
				groupRoot = (Root<Groups>) criteriaQuery.from(Groups.class);
				groupRoot1 = (Root<Groups>) totalQuery.from(Groups.class);
				//groupJoin = result.join("groupLevel");
			}
			if(orderBy.equals("assignee") || orderBy.equals("asignee")){
				assigneeRoot = (Root<Users>) criteriaQuery.from(Users.class);
				assigneeRoot1 = (Root<Users>) totalQuery.from(Users.class);
				//assigneeJoin = result.join("assignee");
			}
			if(orderBy.equals("status") || orderBy.equals("statuse")){
				statusRoot = (Root<ListItem>) criteriaQuery.from(ListItem.class);
				statusRoot1 = (Root<ListItem>) totalQuery.from(ListItem.class);
				//statusJoin = result.join("status");
			}
		}
		
		List<Predicate> columnListPredicates = new ArrayList<Predicate>();
		List<Predicate> countListPredicates = new ArrayList<Predicate>();
		//get list of all fields in the Entity
		Field[] fields = e.getClass().getDeclaredFields();
		List<String> fieldList = new ArrayList<String>();
		Map<String, String> fieldTypeMap = new HashMap<String, String>();
		for(int i = 0; i<fields.length; i++){
			fieldList.add(fields[i].getName());
			fieldTypeMap.put(fields[i].getName(), fields[i].getType().getSimpleName());
		}
		//check if the filter is not null or empty
		if ((columnsList != null || columnsList.isEmpty()) ){
			//List<Predicate> columnPredicatesList = new ArrayList<Predicate>();
			// loop the filter dto list for applying the criteria on each column 
			//either single condition or multiple
			for(FilterColumnDto filterColumn : columnsList){
				String columnName = null;
				String operator = null;
				List<RequestFilterDto> conditionsList = new ArrayList<RequestFilterDto>();
				//filterConditions();
				if(!StringUtils.isBlank(filterColumn.getColumn()))
					columnName = filterColumn.getColumn();
				if(!StringUtils.isBlank(filterColumn.getOperator()))
					operator = filterColumn.getOperator();
				if(null != filterColumn.getCondition1())
					conditionsList.add(filterColumn.getCondition1());
				if(null != filterColumn.getCondition2() && !StringUtils.isEmpty(filterColumn.getOperator()))
					conditionsList.add(filterColumn.getCondition2());
				
				//check if it is Alerts table and the column coming is feed
				if(isAlert && filterColumn.getColumn().equalsIgnoreCase("feed")){
					//feedJoin = result.join("feed");
					//countJoin = countResult.join("feed");
				}
				
				List<Predicate> filterPredicatesList = new ArrayList<Predicate>();
				List<Predicate> filterCountPredicatesList = new ArrayList<Predicate>();
				for(RequestFilterDto condition : conditionsList){
					List<Predicate> conditionPredicatesList = new ArrayList<Predicate>();
					List<Predicate> countPredicatesList = new ArrayList<Predicate>();
					//if filter type is Text
					if (condition.getFilterType() != null && condition.getFilterType().equalsIgnoreCase("Text")) {
						String []keyword = null;
						if(condition.getKeyword() != null ){
							keyword = condition.getKeyword();
						}
						
						//if filter type is Text and subtype contains
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("contains")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.like(feedJoin.get("feedName"), "%" + keyword[0] + "%"));
									countPredicatesList.add(builder.like(countJoin.get("feedName"), "%" + keyword[0] + "%"));*/
								}else{
									conditionPredicatesList.add(builder.like(result.get(columnName), "%" + keyword[0] + "%"));
								}
							}
						}
						//if filter type is Text and subtype not contains
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("notContains")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.notLike(feedJoin.get("feedName"), "%" + keyword[0] + "%"));
									countPredicatesList.add(builder.notLike(countJoin.get("feedName"), "%" + keyword[0] + "%"));*/
								}else{
									conditionPredicatesList.add(builder.notLike(result.get(columnName), "%" + keyword[0] + "%"));
								}
							}
						}
						//if filter type is Text and subtype "equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("equals")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.equal(feedJoin.get("feedName"), keyword[0]));
									countPredicatesList.add(builder.equal(countJoin.get("feedName"), keyword[0]));*/
								}else{
									conditionPredicatesList.add(builder.equal(result.get(columnName),  keyword[0] ));
								}
							}
						}
						//if filter type is Text and subtype "not equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("notEquals")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.notEqual(feedJoin.get("feedName"), keyword[0]));
									countPredicatesList.add(builder.notEqual(countJoin.get("feedName"), keyword[0]));*/
								}else{
									conditionPredicatesList.add(builder.notEqual(result.get(columnName), keyword[0] ));
								}
							}
						}
						//if filter type is Text and subtype "starts with"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("startsWith")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.like(feedJoin.get("feedName"), keyword[0]+ "%"));
									countPredicatesList.add(builder.like(countJoin.get("feedName"), keyword[0]+ "%"));*/
								}else{
									conditionPredicatesList.add(builder.like(result.get(columnName), keyword[0]+ "%" ));
								}
							}
						}
						//if filter type is Text and subtype "ends with"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("endsWith")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.like(feedJoin.get("feedName"), "%" +keyword[0]));
									countPredicatesList.add(builder.like(countJoin.get("feedName"), "%" +keyword[0]));*/
								}else{
									conditionPredicatesList.add(builder.like(result.get(columnName), "%" +keyword[0]));
								}
								
							}
						}
						//if filter type is Text and subtype "range"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("inRange")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*conditionPredicatesList.add(builder.between(feedJoin.get("feedName"), keyword[0], keyword[1]));
									countPredicatesList.add(builder.between(countJoin.get("feedName"), keyword[0], keyword[1]));*/
								}else{
									conditionPredicatesList.add(builder.between(result.get(columnName), keyword[0], keyword[1]));
								}
							}
						}
						//if filter type is Text and subtype "multiple strings"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("multiple")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*for(int i = 0; i<keyword.length; i++){
										conditionPredicatesList.add(builder.like(feedJoin.get("feedName"), "%" + keyword[i] + "%"));
										countPredicatesList.add(builder.like(countJoin.get("feedName"), "%" + keyword[i] + "%"));
									}*/
								}else{
									for(int i = 0; i<keyword.length; i++){
										conditionPredicatesList.add(builder.like(result.get(columnName), "%" + keyword[i] + "%"));
									}
								}
							}
						}
						//if filter type is Text and subtype "multiple select"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("multiSelect")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								
								/*if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									for(int i = 0; i<keyword.length; i++){
										conditionPredicatesList.add(builder.equal(feedJoin.get("feedName"), keyword[i]));
										countPredicatesList.add(builder.equal(countJoin.get("feedName"), keyword[i]));
									}
								}else*/ if((condition.getEntityColumn().equalsIgnoreCase("userRoles")) 
										||(condition.getEntityColumn().equalsIgnoreCase("userGroups"))){
									List<Long> comingIdList = new ArrayList<Long>();
									List<Long> userIds = new ArrayList<Long>();
									for(int i = 0; i<keyword.length; i++){
										comingIdList.add(Long.parseLong(keyword[i]));
									}
									if(condition.getEntityColumn().equalsIgnoreCase("userRoles")){
										userIds = usersService.getUserIdByRolesList(comingIdList);
									}
									if(condition.getEntityColumn().equalsIgnoreCase("userGroups")){
										userIds = usersService.getUserIdByGroupsList(comingIdList);
									}
									if(!userIds.isEmpty() || userIds.size()>0) {
										conditionPredicatesList.add(builder.in(result.get("userId")).value(userIds));
									}else {
										userIds.add(-1L);
										conditionPredicatesList.add(builder.in(result.get("userId")).value(userIds));
									}
										
									//conditionPredicatesList.add(builder.in(result.get(columnName)).in(userIds));
									
								}else if (condition.getEntityColumn().equalsIgnoreCase("feed")){
									List<Long> comingIdList = new ArrayList<Long>();
									List<Long> alertIdList = new ArrayList<Long>();
									for(int i = 0; i<keyword.length; i++){
										comingIdList.add(Long.parseLong(keyword[i]));
									}
									alertIdList = alertService.getAlertIdsByFeedIds(comingIdList);
									if(!alertIdList.isEmpty() || alertIdList.size()>0) {
										conditionPredicatesList.add(builder.in(result.get("alertId")).value(alertIdList));
									}else {
										alertIdList.add(-1L);
										conditionPredicatesList.add(builder.in(result.get("alertId")).value(alertIdList));
									}
								}else{
									for (int i = 0; i < keyword.length; i++) {
										conditionPredicatesList.add(builder.equal(result.get(columnName), Long.parseLong(keyword[i])));
									}
								}
							}
						}
						
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("multiSelectString")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								
								if(condition.getEntityColumn().equalsIgnoreCase("feed")){
									/*for(int i = 0; i<keyword.length; i++){
										conditionPredicatesList.add(builder.equal(feedJoin.get("feedName"), keyword[i]));
										countPredicatesList.add(builder.equal(countJoin.get("feedName"), keyword[i]));
									}*/
								}else{
									for (int i = 0; i < keyword.length; i++) {
										conditionPredicatesList.add(builder.equal(result.get(columnName), keyword[i]));
									}
									
								}
							}
						}
					}
					// if filter type is Number
					if (condition.getFilterType() != null && condition.getFilterType().equalsIgnoreCase("Number")) {
						String []keyword = null;
						if(condition.getKeyword() != null ){
							keyword = condition.getKeyword();
						}
						// if filter type is Number and sub type is "equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("equals")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.equal(result.get(columnName), Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is " not equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("notEqual")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.notEqual(result.get(columnName),  Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is "less than"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("lessThan")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.lessThan(result.get(columnName), Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is "less than or equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("lessThanOrEqual")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.lessThanOrEqualTo(result.get(columnName), Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is "greater than"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("greaterThan")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.greaterThan(result.get(columnName), Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is "greater than or equals"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("greaterThanOrEqual")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.greaterThanOrEqualTo(result.get(columnName), Integer.parseInt(keyword[0])));
							}
						}
						// if filter type is Number and sub type is "between"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("inRange")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								conditionPredicatesList.add(builder.between(result.get(columnName), Integer.parseInt(keyword[0]), Integer.parseInt(keyword[1])));
							}
						}
						
					}
					// if filter type is Date
					if (condition.getFilterType() != null && condition.getFilterType().equalsIgnoreCase("Date")) {
						String []keyword = null;
						if(condition.getKeyword() != null ){
							keyword = condition.getKeyword();
						}
						// if filter type is Date and sub type is "custom"
						if (condition.getSubType() != null && condition.getSubType().equalsIgnoreCase("between date")) {
							if (fieldList.contains(condition.getEntityColumn())) {
								try{
									SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
									String date1 = keyword[0]+" 00:00:00";
									String date2 = keyword[1]+" 23:59:59";
									
									Date startDate=format.parse(date1);
									Date endDate=format.parse(date2);
									conditionPredicatesList.add(builder.between(result.get(columnName), startDate, endDate));							
									
								}catch(Exception ex){
									ex.printStackTrace();
								}
								}
						}
						
					}
					
					//create the predicate for condition wise
					Predicate conditionPredicate = builder
							.and(builder.or(conditionPredicatesList.toArray(new Predicate[conditionPredicatesList.size()])));
					filterPredicatesList.add(conditionPredicate);
					//for total counts
					Predicate countPredicate = builder
							.and(builder.or(countPredicatesList.toArray(new Predicate[countPredicatesList.size()])));
					filterCountPredicatesList.add(countPredicate);
				}
				// apply AND between the two conditions of single column
				if(!StringUtils.isEmpty(operator) && operator.equalsIgnoreCase("AND") && !filterPredicatesList.isEmpty() && !filterCountPredicatesList.isEmpty()){
					Predicate conditionAnd = builder
							.and(builder.and(filterPredicatesList.toArray(new Predicate[filterPredicatesList.size()])));
					columnListPredicates.add(conditionAnd);
					
					// for counts
					Predicate countConditionAnd = builder
							.and(builder.and(filterCountPredicatesList.toArray(new Predicate[filterCountPredicatesList.size()])));
					countListPredicates.add(countConditionAnd);
				}else if(!StringUtils.isEmpty(operator) && operator.equalsIgnoreCase("OR") && !filterPredicatesList.isEmpty() && !filterCountPredicatesList.isEmpty()){
					// apply OR between the two conditions of single column
					Predicate conditionOr = builder
							.and(builder.or(filterPredicatesList.toArray(new Predicate[filterPredicatesList.size()])));
					columnListPredicates.add(conditionOr);
					//for counts
					Predicate countConditionOr = builder
							.and(builder.or(filterCountPredicatesList.toArray(new Predicate[filterCountPredicatesList.size()])));
					countListPredicates.add(countConditionOr);
				}else{
					// just add the predicate to column predicate with single condition
					Predicate conditionBlank = builder
							.and(filterPredicatesList.toArray(new Predicate[filterPredicatesList.size()]));
					columnListPredicates.add(conditionBlank);
					//for counts
					Predicate countConditionBlank = builder
							.and(filterCountPredicatesList.toArray(new Predicate[filterCountPredicatesList.size()]));
					countListPredicates.add(countConditionBlank);
				}
			}
		}
		//Sorting
		if ((!StringUtils.isEmpty(orderIn)) && (!StringUtils.isEmpty(orderBy))) {
			if(isAlert && (orderBy.equals("feed") || orderBy.equals("groupLevel") 
					|| (orderBy.equals("assignee") || orderBy.equals("asignee")) 
					|| (orderBy.equals("status") || orderBy.equals("statuse")))){
				
				/*if (isAlert && orderBy.equals("feed") && "asc".equalsIgnoreCase(orderIn)) {
					//criteriaQuery.orderBy(builder.asc(feedJoin.get("feedName")));
					criteriaQuery.orderBy(builder.asc(feedRoot.get("feedName")));
					columnListPredicates.add(builder.equal(result.get("feed"), feedRoot.get("feed_management_id")));
					totalQuery.orderBy(builder.asc(feedRoot1.get("feedName")));
					countListPredicates.add(builder.equal(countResult.get("feed"), feedRoot1.get("feed_management_id")));
				}
				if (isAlert && orderBy.equals("feed") && "desc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.desc(feedRoot.get("feedName")));
					columnListPredicates.add(builder.equal(result.get("feed"), feedRoot.get("feed_management_id")));
					totalQuery.orderBy(builder.desc(feedRoot1.get("feedName")));
					countListPredicates.add(builder.equal(countResult.get("feed"), feedRoot1.get("feed_management_id")));
				}*/
				if (isAlert && orderBy.equals("groupLevel") && "asc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.asc(groupRoot.get("name")));
					columnListPredicates.add(builder.equal(result.get("groupLevel"), groupRoot.get("id")));
					totalQuery.orderBy(builder.asc(groupRoot1.get("name")));
					countListPredicates.add(builder.equal(countResult.get("groupLevel"), groupRoot1.get("id")));
				}
				if (isAlert && orderBy.equals("groupLevel") && "desc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.desc(groupRoot.get("name")));
					columnListPredicates.add(builder.equal(result.get("groupLevel"), groupRoot.get("id")));
					totalQuery.orderBy(builder.desc(groupRoot1.get("name")));
					countListPredicates.add(builder.equal(countResult.get("groupLevel"), groupRoot1.get("id")));
				}
				if (isAlert && (orderBy.equals("assignee") || orderBy.equals("asignee")) && "asc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.asc(assigneeRoot.get("screenName")));
					columnListPredicates.add(builder.equal(result.get("assignee"), assigneeRoot.get("userId")));
					totalQuery.orderBy(builder.asc(assigneeRoot1.get("screenName")));
					countListPredicates.add(builder.equal(countResult.get("assignee"), assigneeRoot1.get("userId")));
				}
				if (isAlert && (orderBy.equals("assignee") || orderBy.equals("asignee")) && "desc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.desc(assigneeRoot.get("screenName")));
					columnListPredicates.add(builder.equal(result.get("assignee"), assigneeRoot.get("userId")));
					totalQuery.orderBy(builder.desc(assigneeRoot1.get("screenName")));
					countListPredicates.add(builder.equal(countResult.get("assignee"), assigneeRoot1.get("userId")));
				}
				if (isAlert && (orderBy.equals("status") || orderBy.equals("statuse")) && "asc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.asc(statusRoot.get("displayName")));
					columnListPredicates.add(builder.equal(result.get("status"), statusRoot.get("listItemId")));
					totalQuery.orderBy(builder.asc(statusRoot1.get("displayName")));
					countListPredicates.add(builder.equal(countResult.get("status"), statusRoot1.get("listItemId")));
				}
				if (isAlert && (orderBy.equals("status") || orderBy.equals("statuse")) && "desc".equalsIgnoreCase(orderIn)) {
					criteriaQuery.orderBy(builder.desc(statusRoot.get("displayName")));
					columnListPredicates.add(builder.equal(result.get("status"), statusRoot.get("listItemId")));
					totalQuery.orderBy(builder.desc(statusRoot1.get("displayName")));
					countListPredicates.add(builder.equal(countResult.get("status"), statusRoot1.get("listItemId")));
				}
			}else{
				if("asc".equalsIgnoreCase(orderIn)){
					criteriaQuery.orderBy(builder.asc(result.get(orderBy)));
					totalQuery.orderBy(builder.asc(result.get(orderBy)));
				}
				if("desc".equalsIgnoreCase(orderIn)){
					criteriaQuery.orderBy(builder.desc(result.get(orderBy)));
					totalQuery.orderBy(builder.desc(result.get(orderBy)));
				}
			}
		}
		
		if(!columnListPredicates.isEmpty()){
			criteriaQuery.where(builder.and(columnListPredicates.toArray(new Predicate[columnListPredicates.size()])));
			/*if(countJoin != null){
				totalQuery.where(builder.and(countListPredicates.toArray(new Predicate[countListPredicates.size()])));
			}*/
			totalQuery.where(builder.and(columnListPredicates.toArray(new Predicate[columnListPredicates.size()])));
		}
		
		Query<?> query = this.getCurrentSession().createQuery(criteriaQuery);
		query.setFirstResult((pageNumber - 1) * (recordsPerPage));
		query.setMaxResults(recordsPerPage);
		List<E> resultList = (List<E>) query.getResultList();
		
		Query<?> query1 = this.getCurrentSession().createQuery(totalQuery);
		totalResults = (Long) query1.getSingleResult();
		
		returnResult.put(resultList, totalResults);
		
	return returnResult;
	}
	public String filterConditions() {
		
		
		return null;
		
	}
	
}