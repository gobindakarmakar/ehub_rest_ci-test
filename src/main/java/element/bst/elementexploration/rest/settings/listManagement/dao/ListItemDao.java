package element.bst.elementexploration.rest.settings.listManagement.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;

public interface ListItemDao extends GenericDao<ListItem, Long>{

	List<ListItem> getListItemByListType(String listType);
	
	ListItem getListItemByListTypeAndDisplayName(String listType,String displayName);
	
	ListItem getListItemByListTypeAndCode(String listType, String code);
	
	List<ListItem> getListItemByListTypeAndSearchField(String listType,String searchField);

	List<ListItem> getListItemByListTypeAndLikeDisplayName(String listType, String displayName);
}
