package element.bst.elementexploration.rest.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.json.JSONObject;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.service.CaseDocumentService;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.AuditTrail;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.logging.service.AuditTrailService;
import element.bst.elementexploration.rest.logging.util.ApiLogConfigurer;
import element.bst.elementexploration.rest.logging.util.ApiLogger;
import element.bst.elementexploration.rest.notification.domain.NotificationAlert;
import element.bst.elementexploration.rest.notification.service.NotificationService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.security.domain.CurrentUser;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Aspect
@Component("loggingHandler")
public class LoggingHandler {

	@Autowired
	private AuditTrailService auditTrailService;
	
	@Autowired
	private CaseService caseService;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private ApiLogConfigurer apiLogConfigurer;
	
	@Autowired
	NotificationService notificationService;
	
	@Autowired
	private GroupsService groupsService;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	SettingsService settingsService;
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	private CaseDocumentService caseDocumentService;
	
	@Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
	public void controller() {
	}

	@AfterReturning(pointcut = "controller() && args(..,request)", returning = "result")
	public void logAfter(JoinPoint joinPoint, Object result, HttpServletRequest request) {
		AuditTrail trail = generateTrail(request, "INFO", joinPoint.getSignature().getDeclaringTypeName(), null, result);
		ApiLogger apiLogger = null;
		String className = joinPoint.getSignature().getDeclaringTypeName();
		if("SearchHistoryController".equalsIgnoreCase(className.substring(className.lastIndexOf(".") + 1)) && "getData".equalsIgnoreCase(joinPoint.getSignature().getName())){
			trail.setResponseBody(null);
			auditTrailService.save(trail);
		}else{
			try{
			auditTrailService.save(trail);}
			catch(Exception e ){
				e.printStackTrace();
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			}
		}
		apiLogger = apiLogConfigurer.getApiLoggers().stream()
				.filter((log) -> log.getControllerName().equalsIgnoreCase(className.substring(className.lastIndexOf(".") + 1)))
				.filter((log) -> log.getMethodName().equalsIgnoreCase(joinPoint.getSignature().getName()))
				.findFirst().orElse(null);
		generateLog(apiLogger, className, joinPoint.getSignature().getName(), request);	
	}

	/*@AfterThrowing(pointcut = "controller() && args(..,request)", throwing = "exception")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable exception, HttpServletRequest request) {	
		auditTrailService.save(generateTrail(request, "ERROR", joinPoint.getSignature().getDeclaringTypeName(), exception.getClass().getName(), exception.getMessage()));
	}*/
	
	private void generateLog(ApiLogger apiLogger, String className, String methodName, HttpServletRequest request){
		if(apiLogger != null){
			AuditLog log = new AuditLog(new Date(), apiLogger.getLevel(), null, null, apiLogger.getTitle());
			Users user = null;
			if(getCurrentUserId() != null){
				user = usersService.find(getCurrentUserId());
				user = usersService.prepareUserFromFirebase(user);
			}
			List<String> params = apiLogger.getParams();
			String author = null;
			if(user != null)
				author = user.getFirstName() + " " + user.getLastName();
			String controllerName = className.substring(className.lastIndexOf(".") + 1);
			if(controllerName.equalsIgnoreCase("InvestigationController")){
				if("caseReassignment".equalsIgnoreCase(methodName) || "caseForwarding".equalsIgnoreCase(methodName)){
					Case caseSeed = caseService.find(Long.parseLong(request.getParameter("caseId")));
					Users found = usersService.find(Long.parseLong(request.getParameter("newUserId")));
					found = usersService.prepareUserFromFirebase(found);
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, caseSeed.getName(), found.getFirstName() + " " + found.getLastName()));
					log.setCaseId(caseSeed.getCaseId());
					log.setUserId(getCurrentUserId());
					log.setName(caseSeed.getName());
					log.setType("CASE");
				}else{
					Case caseSeed = caseService.find(Long.parseLong(request.getParameter(params.get(0))));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, caseSeed.getName()));
					log.setCaseId(caseSeed.getCaseId());
					log.setUserId(getCurrentUserId());
					log.setName(caseSeed.getName());
					log.setType("CASE");
				}			
			}
			
			if(controllerName.equalsIgnoreCase("ContactsController")){
				Users contact = usersService.find(Long.parseLong(request.getParameter(params.get(0))));
				contact = usersService.prepareUserFromFirebase(contact);
				log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, contact.getFirstName() + " " + contact.getLastName()));
				log.setUserId(getCurrentUserId());
				log.setName(contact.getFirstName() + " " + contact.getLastName());
			}
			
			if(controllerName.equalsIgnoreCase("NotificationController")){
				log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author));
				log.setUserId(getCurrentUserId());			
			}
			
			if(controllerName.equalsIgnoreCase("CalendarEventController")){
				NotificationAlert event = notificationService.find(Long.parseLong(request.getParameter(params.get(0))));
				log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, event.getSubject()));
				log.setUserId(getCurrentUserId());	
				log.setName(event.getSubject());
			}
			if(controllerName.equalsIgnoreCase("UserController")){
				if("handleUpdateUser".equalsIgnoreCase(methodName)){
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author));
					log.setUserId(getCurrentUserId());
					log.setName(author);
				}
			}
			
			if(controllerName.equalsIgnoreCase("AdminstrationController")){
				Users newUser = usersService.find(Long.parseLong(request.getParameter(params.get(0))));
				newUser = usersService.prepareUserFromFirebase(newUser);
				log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, newUser.getFirstName() + " " + newUser.getLastName()));
				log.setUserId(getCurrentUserId());
				log.setName(newUser.getFirstName() + " " + newUser.getLastName());
			}
			
			if(controllerName.equalsIgnoreCase("ElementAuthenticationsController")){
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				UsersDto loggedInUser = new UsersDto();
				if (settingsService
						.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
								ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
						.equalsIgnoreCase("On")) {
					String token = (String) servletContext.getAttribute(request.getParameter(params.get(0)));
					JSONObject firebaseObject = (JSONObject) servletContext.getAttribute(token);
					org.json.JSONArray userArray = firebaseObject.getJSONArray("users");
					String email = null;
					if(userArray.length()>0){
						JSONObject userObject = (JSONObject) userArray.get(0);
						if(userObject.has("email")){
							email = userObject.get("email").toString();
						}
					}
					loggedInUser = usersService.getUserByEmailOrUsername(email);
				}else{
					loggedInUser = usersService.getUserByEmailOrUsername(request.getParameter(params.get(0)));
				}
				log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), loggedInUser.getFirstName() + " " + loggedInUser.getLastName(), dateFormat.format(log.getCreatedOn())));
				log.setUserId(loggedInUser.getUserId());
				log.setName(loggedInUser.getFirstName() + " " + loggedInUser.getLastName());
			}
			
			if(controllerName.equalsIgnoreCase("UserGroupsController")){
				if("addUsersGroups".equalsIgnoreCase(methodName) || "removeUsersGroups".equalsIgnoreCase(methodName)){
					Groups group = groupsService.find(Long.parseLong(request.getParameter("groupId")));
					Users target = usersService.find(Long.parseLong(request.getParameter("userId")));
					target = usersService.prepareUserFromFirebase(target);
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, target.getFirstName() + " " + target.getLastName(), group.getName()));
					log.setUserId(getCurrentUserId());
					log.setName(group.getName());
				}else{
					Groups group = groupsService.find(Long.parseLong(request.getParameter(params.get(0))));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, group.getName()));
					log.setUserId(getCurrentUserId());
					log.setName(group.getName());
				}
			}
			
			if(controllerName.equalsIgnoreCase("DocumentController")){
				if("shareDocumentWithCaseseed".equalsIgnoreCase(methodName) || "unshareDocumentFromCaseseed".equalsIgnoreCase(methodName) ||
						"documentDissemination".equalsIgnoreCase(methodName)){
					Case caseSeed = caseService.find(Long.parseLong(request.getParameter("caseId")));
					DocumentVault documentVault = documentService.find(Long.parseLong(request.getParameter("docId")));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, documentVault.getTitle(), caseSeed.getName()));
					log.setUserId(getCurrentUserId());
					log.setName(documentVault.getTitle());
					log.setDocumentId(documentVault.getDocId());
					log.setCaseId(caseSeed.getCaseId());
				} else if("shareDocumetWithUser".equalsIgnoreCase(methodName) || "unShareDocumetFromUser".equalsIgnoreCase(methodName)){
					Users found = usersService.find(Long.parseLong(request.getParameter("userId")));
					found = usersService.prepareUserFromFirebase(found);
					DocumentVault documentVault = documentService.find(Long.parseLong(request.getParameter("docId")));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, documentVault.getTitle(), found.getFirstName() + " " + found.getLastName()));
					log.setUserId(getCurrentUserId());
					log.setName(documentVault.getTitle());
					log.setDocumentId(documentVault.getDocId());
				} else if("updateDocumentContent".equalsIgnoreCase(methodName) || "softDeleteDocument".equalsIgnoreCase(methodName) || 
						"updateDocument".equalsIgnoreCase(methodName)){
					DocumentVault documentVault = documentService.find(Long.parseLong(request.getParameter(params.get(0))));
					if(documentVault.getEntityId()!=null && documentVault.getDocFlag()==5 && "updateDocumentContent".equalsIgnoreCase(methodName))
						log.setDescription(author+" updated note "+documentVault.getTitle() + " for entity "+documentVault.getEntityName());
					else if(documentVault.getEntityId()!=null && documentVault.getDocFlag()==5  && "softDeleteDocument".equalsIgnoreCase(methodName))
						log.setDescription(author+" deleted note "+documentVault.getTitle() + " form entity "+documentVault.getEntityName());
					else if(documentVault.getEntityId()!=null && "updateDocumentContent".equalsIgnoreCase(methodName))
						log.setDescription(author+" updated document "+documentVault.getTitle() + " for entity "+documentVault.getEntityName());
					else if(documentVault.getEntityId()!=null && "softDeleteDocument".equalsIgnoreCase(methodName))
						log.setDescription(author+" deleted document "+documentVault.getTitle() + " form entity "+documentVault.getEntityName());
					else
						log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, documentVault.getTitle()));
					log.setUserId(getCurrentUserId());
					log.setName(documentVault.getTitle());
					log.setDocumentId(documentVault.getDocId());
				} else if("commentDocument".equalsIgnoreCase(methodName)){
					DocumentVault documentVault = documentService.find(Long.parseLong(request.getParameter("docId")));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, documentVault.getTitle()));
					log.setUserId(getCurrentUserId());
					log.setName(documentVault.getTitle());
					log.setDocumentId(documentVault.getDocId());
				}
			}
			
			if(controllerName.equalsIgnoreCase("CaseController")){
				if("updateBstCaseSeedById".equalsIgnoreCase(methodName)){
					Case caseSeed = caseService.find(Long.parseLong(request.getParameter(params.get(0))));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, caseSeed.getName()));
					log.setUserId(getCurrentUserId());
					log.setName(caseSeed.getName());
					log.setCaseId(caseSeed.getCaseId());
					log.setType("CASE");
				} else if("upDateDocumentDetails".equalsIgnoreCase(methodName)){
					CaseDocDetails docDetails = caseDocumentService.find(Long.parseLong(request.getParameter(params.get(0))));
					log.setDescription(this.formatDescription(apiLogger.getDescriptionFormat(), author, docDetails.getFileTitle(), docDetails.getCaseSeed().getName()));
					log.setUserId(getCurrentUserId());
					log.setName(docDetails.getCaseSeed().getName());
					log.setDocumentId(docDetails.getDocId());
					log.setCaseId(docDetails.getCaseSeed().getCaseId());
					log.setType("CASE");
				}
			}
			auditLogService.save(log);
		}
	}
	
	private Long getCurrentUserId(){
		try{
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
			return currentUser.getUserId();
		}catch (Exception e) {
			return null;
		}
	}
	
	private String resolveType(String className) {

		if(StringUtils.isNoneEmpty(className))
			className = className.substring(className.lastIndexOf(".") + 1);
		else
			return "UNKNOWN";
		
		switch (className) {
		case "UserController":
			return "USER";
		case "UserGroupController":
			return "GROUP";
		case "AdminstrationController":
			return "USER";
		case "CalendarEventController":
			return "EVENT";
		case "NotificationController":
			return "NOTIFICATION";
		case "ContactsController":
			return "CONTACT";
		default:
			return "UNKNOWN";
		}
	}
	
	private AuditTrail generateTrail(HttpServletRequest request, String level, String className,
			String exceptionClass, Object result){
		String ip = request.getHeader("CALLER-IP");
        if (ip == null || "".equals(ip) || "".equals(ip.trim())) {
            ip = request.getRemoteAddr();
        }
        
        int statusCode = getResponseCode(result);
        String responseBody = null;
        if("GET".equalsIgnoreCase(request.getMethod()) && statusCode == 200){
        	responseBody = null;
        }else{
        	responseBody = getResponseBody(result);
        }
        
        String requestBody = null;
        try{
        	requestBody = IOUtils.toString(request.getReader());
        }catch (Exception e) {
        	requestBody = null;
		}
		return new AuditTrail(new Date(), ip, request, requestBody, responseBody, statusCode, getCurrentUserId(), 
				level, resolveType(className), exceptionClass);
	}
	
	private String getResponseBody(Object result) {
		String returnValue = null;
		if (null != result) {
			if (result instanceof ResponseEntity) {
				try{
					returnValue = ((ResponseEntity<?>)result).getBody().toString();
				}catch (Exception e) {
					// TODO: handle exception
				}
			} else {
				returnValue = result.toString();
			}
		}
		return returnValue;
	}
	
	private int getResponseCode(Object result) {
		int returnValue = 0;
		if (null != result) {
			if (result instanceof ResponseEntity) {
				try{
					returnValue = ((ResponseEntity<?>)result).getStatusCodeValue();
				}catch (Exception e) {
					// TODO: handle exception
				}
			} 
		}
		return returnValue;
	}
	
	private String formatDescription(String format, Object... arguments) {
		FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
		return ft.getMessage();
	}

}
