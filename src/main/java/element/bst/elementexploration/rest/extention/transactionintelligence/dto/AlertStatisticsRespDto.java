package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlertStatisticsRespDto {

	@JsonProperty("tx-by-location")
	private List<TxByLocationDto> txLoc;
	@JsonProperty("tx-by-time")
	private List<TxByDate> txDate;
	@JsonProperty("tx-by-type")
	private List<TxByType> txType;

	public List<TxByLocationDto> getTxLoc() {
		return txLoc;
	}

	public void setTxLoc(List<TxByLocationDto> txLoc) {
		this.txLoc = txLoc;
	}

	public List<TxByDate> getTxDate() {
		return txDate;
	}

	public void setTxDate(List<TxByDate> txDate) {
		this.txDate = txDate;
	}

	public List<TxByType> getTxType() {
		return txType;
	}

	public void setTxType(List<TxByType> txType) {
		this.txType = txType;
	}

}
