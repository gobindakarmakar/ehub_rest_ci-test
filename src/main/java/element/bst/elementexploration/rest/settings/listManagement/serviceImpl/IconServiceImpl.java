package element.bst.elementexploration.rest.settings.listManagement.serviceImpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.listManagement.domain.Icon;
import element.bst.elementexploration.rest.settings.listManagement.dto.IconDto;
import element.bst.elementexploration.rest.settings.listManagement.service.IconService;
import element.bst.elementexploration.rest.util.EntityConverter;
@Service("iconService")
@Transactional("transactionManager")
public class IconServiceImpl extends GenericServiceImpl<Icon, Long> implements IconService {

	
	@Autowired
	IconService iconService;
	
	@Autowired
	public IconServiceImpl(GenericDao<Icon, Long> genericDao) {
		super(genericDao);
	}

	public IconServiceImpl() {

	}
	
	@Override
	public List<IconDto> getAllIcons() {
		List<Icon> iconList = iconService.findAll();
		List<IconDto> iconDtosList = new ArrayList<IconDto>();
		if (iconList != null) {
			iconDtosList = iconList.stream().map(
					(entityComplexStructureDto) -> new EntityConverter<Icon, IconDto>(Icon.class, IconDto.class)
					.toT2(entityComplexStructureDto, new IconDto()))
					.collect(Collectors.toList());
			
			iconDtosList.sort(Comparator.comparing(IconDto::getName));//sorting in asc order
		}
		return iconDtosList;
	}
}
