package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.usermanagement.group.domain.Permissions;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Permissions")
public class PermissionsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Privilege Name")
	private String itemName;

	@ApiModelProperty(value = "Status of permission")
	private Boolean active;

	@ApiModelProperty(value = "Parent Permission ")
	private Permissions parentPermissionId;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Permissions getParentPermissionId() {
		return parentPermissionId;
	}

	public void setParentPermissionId(Permissions parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}

}
