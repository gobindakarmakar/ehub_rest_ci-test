package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;
/**
 * @author rambabu
 *
 */
public class RowsDto  implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private List<ColumnsDto> columns;

	public RowsDto() {
		super();		
	}

	public List<ColumnsDto> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnsDto> columns) {
		this.columns = columns;
	}
	
	
	
	

}
