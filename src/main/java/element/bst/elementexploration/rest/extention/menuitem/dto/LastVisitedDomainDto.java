package element.bst.elementexploration.rest.extention.menuitem.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class LastVisitedDomainDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID of the Domain")
	private Long lastVisitedDomainId;

	@ApiModelProperty(value = "userId of the Domain")
	@NotNull(message="User Id cannot be null")
	private Long userId;

	@ApiModelProperty(value = "permissionId of the Domain")
	@NotNull(message="Permission ID cannot be null")
	private Long permissionId;

	@ApiModelProperty(value = "Name of the Domain")
	@NotNull(message="Domain Name Id cannot be null")
	private String domainName;

	public Long getLastVisitedDomainId() {
		return lastVisitedDomainId;
	}

	public void setLastVisitedDomainId(Long lastVisitedDomainId) {
		this.lastVisitedDomainId = lastVisitedDomainId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
