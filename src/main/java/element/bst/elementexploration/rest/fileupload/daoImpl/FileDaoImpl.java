package element.bst.elementexploration.rest.fileupload.daoImpl;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.fileupload.dao.FileDao;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("fileDao")
public class FileDaoImpl extends GenericDaoImpl<FileBst, Long> implements FileDao {

	public FileDaoImpl() {
		super(FileBst.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public FileBst findFileByName(String fileName) {
		FileBst fileBst = new FileBst();
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("from FileBst f where f.fileName=:fileName");
			fileBst = (FileBst)this.getCurrentSession().createQuery(builder.toString()).setParameter("fileName", fileName).getSingleResult();
			
		} catch (Exception e) {
			return fileBst;
		}
		return fileBst;
	}
}
