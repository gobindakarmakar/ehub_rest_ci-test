package element.bst.elementexploration.rest.extention.significantnews.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author rambabu
 *
 */
@Entity
@Table(name = "bst_significant_watchlist")
public class SignificantWatchList implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "main_entity_id")
	@NotEmpty(message = "main entity id can not be blank.")
	private String mainEntityId;

	@Column(name = "entity_id")
	private String identifier=null;

	@Column(name = "entity_name")
	@NotEmpty(message = "entity name can not be blank.")
	private String entityName;

	@Column(name = "value",columnDefinition = "LONGTEXT")
	@NotEmpty(message = "value can not be blank.")
	private String value;
	
	@Column(name = "name")
	@NotEmpty(message = "name can not be blank.")
	private String name;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "is_pep")
	private Boolean isPep;

	@Column(name = "is_sanction")
	private Boolean isSanction;

	public SignificantWatchList() {
		super();
	}

	public SignificantWatchList(Long id, String mainEntityId, String identifier, String entityName, String value,
			String name, Long userId, Boolean isPep, Boolean isSanction) {
		super();
		this.id = id;
		this.mainEntityId = mainEntityId;
		this.identifier = identifier;
		this.entityName = entityName;
		this.value = value;
		this.name = name;
		this.userId = userId;
		this.isPep = isPep;
		this.isSanction = isSanction;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMainEntityId() {
		return mainEntityId;
	}

	public void setMainEntityId(String mainEntityId) {
		this.mainEntityId = mainEntityId;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean isPep() {
		return isPep;
	}

	public void setPep(Boolean isPep) {
		this.isPep = isPep;
	}

	public Boolean isSanction() {
		return isSanction;
	}

	public void setSanction(Boolean isSanction) {
		this.isSanction = isSanction;
	}

}
