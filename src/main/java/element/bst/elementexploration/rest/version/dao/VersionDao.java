package element.bst.elementexploration.rest.version.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.version.domain.ProductVersion;

public interface VersionDao extends GenericDao<ProductVersion, Long> {

	ProductVersion getLatestVersion();

	List<ProductVersion> getAllVersions();
}
