package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import javax.persistence.NoResultException;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface CountryDao extends GenericDao<Country,Long>{
	
	public Country findByName(String countryCode);
	public Country getCountry(String location) throws NoResultException;
}