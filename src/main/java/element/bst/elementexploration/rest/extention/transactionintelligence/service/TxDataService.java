
package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionMasterView;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonReturnObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertedCounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyGraphDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyInfoDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.InputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.NotificationDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.OutputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ScenariosResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAndAlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxsListAggRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.UpdateTransactionsDataDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface TxDataService extends GenericService<TransactionsData, Long> {

	void saveTxData(MultipartFile multipartFile1, long userId) throws IOException, ParseException;

	public Boolean generateAlert(List<TxDto> listTx);

	ResponseDto fetchAllTxsBasedOnEntity(List<String> entityIds, String transactionType, String minAmount,
			String locations, String minTxs,long alertId,long alertTransactionId) throws IllegalAccessException, InvocationTargetException;

	public InputStatsResponseDto inputStats(List<String> entityIds, String counterPartyMin, String counterPartyMax,
			String minTxs, String minAmount, String location, String transactionType) throws IllegalAccessException, InvocationTargetException;

	public OutputStatsResponseDto outputStats(List<String> entityIds, String counterPartyMin, String counterPartyMax,
			String minTxs, String minAmount, String location, String transactionType);

	List<TransactionsData> getAllTransactions();

	public NotificationDto fetchTxsBwDates(String fromDate, String toDate, String productType,FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException;

	public List<TransactionAmountAndAlertCountDto> getTotalAmountAndAlertCountByProductType(String fromDate,
			String toDate, boolean groupBy,boolean input,boolean output,String type,FilterDto filterDto) throws ParseException;

	List<AlertDashBoardRespDto> getAlertsBetweenDates(String fromDate, String toDate, String name, Integer pageNumber,
			Integer recordsPerPage, String orderIn,FilterDto filterDto,boolean isTimeStamp);

	TransactionAndAlertAggregator getTransactionAndAlertAggregates(String fromDate, String toDate, String granularity,
			Boolean isGroupByType, boolean input, boolean output, String type,FilterDto filterDto) throws ParseException;

	Map<String, TransactionAndAlertAggregator> getTransactionAndAlertAggregatesByProducType(String fromDate,
			String toDate, String granularity, boolean b,boolean input,
			 boolean output,String type,FilterDto filterDto) throws ParseException;

	Long getAlertCountBetweenDates(String fromDate, String toDate, String name,FilterDto filterDto,boolean isTimeStamp);

	public TxsListAggRespDto fetchTxsTotalListBwDates(String fromDate, String toDate, String productType,
			Integer recordsPerPage, Integer pageNumber, Boolean input, Boolean output,FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException;

	public long fetchTxsBwDatesCustomizedCount(String fromDate, String toDate, String productType, boolean input,
			boolean output,FilterDto filterDto) throws ParseException;

	public TxsListAggRespDto fetchTxByTypeListViewAll(String fromDate, String toDate, String productType,
			Integer recordsPerPage, Integer pageNumber, boolean input, boolean output,FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException;

	public List<AssociatedEntityRespDto> fetchAssociatedEntity(String fromDate, String toDate,FilterDto filterDto) throws ParseException;

	RiskCountAndRatioDto getCustomerRisk(String fromDate, String toDate,FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> getCustomerRiskAggregates(String fromDate, String toDate,FilterDto filterDto);

	RiskCountAndRatioDto getGeoGraphicRisk(String fromDate, String toDate,FilterDto filterDto);

	List<RiskCountAndRatioDto> getGeoGraphicRiskAggregates(String fromDate, String toDate,FilterDto filterDto);

	List<RiskCountAndRatioDto> getCorruptionRiskAggregates(String fromDate, String toDate,FilterDto filterDto);

	List<RiskCountAndRatioDto> getPoliticalRiskAggregates(String fromDate, String toDate,FilterDto filterDto);

	RiskCountAndRatioDto getCorruptionRisk(String fromDate, String toDate,FilterDto filterDto);

	RiskCountAndRatioDto getPoliticalRisk(String fromDate, String toDate,FilterDto filterDto);

	ScenariosResponseDto getScenariosByType(String fromDate, String toDate, String type,String filterType,FilterDto filterDto)
			throws ParseException;

	Map<Integer, List<RiskCountAndRatioDto>> getCorruptionRiskByType(String fromDate, String toDate, String type,FilterDto filterDto) throws ParseException;

	public Map<String, AlertComparisonReturnObject> alertComparisonTransactions(String fromDate, String toDate,FilterDto filterDto)
			throws ParseException;

	Map<Integer, List<RiskCountAndRatioDto>> getGeoGraphicRiskByType(String fromDate, String toDate, String type,FilterDto filterDto) throws ParseException;

	Map<Integer, List<RiskCountAndRatioDto>> getPoliticalRsikByType(String fromDate, String toDate, String type,FilterDto filterDto) throws ParseException;

	RiskCountAndRatioDto getProductRisk(String fromDate, String toDate, FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> getProductRiskAggregates(String fromDate, String toDate,FilterDto filterDto);

	List<RiskCountAndRatioDto> getRiskRatioChart(String fromDate, String toDate,FilterDto filterDto) throws ParseException;

	public CounterPartyGraphDto counterPartyLocationsPlot(String fromDate, String toDate, String type,FilterDto filterDto)
			throws ParseException, IllegalAccessException, InvocationTargetException;

	public List<TransactionAmountAndAlertCountDto> amlAlertAggregation(String fromDate, String toDate,
			String productType, Integer recordsPerPage, Integer pageNumber, Boolean input, Boolean output,FilterDto filterDto)
			throws ParseException;

	Map<Integer, List<RiskCountAndRatioDto>> getProductRiskByType(String fromDate, String toDate, String type,FilterDto filterDto) throws ParseException;

	List<RiskCountAndRatioDto> getRiskCount(String fromDate, String toDate,FilterDto filterDto) throws ParseException;

	List<AlertDashBoardRespDto> getRiskAlertsByScenarios(String fromDate, String toDate, 
			FilterDto filterDto);

	List<AlertDashBoardRespDto> getProductByScenarios(String fromDate, String toDate,FilterDto filterDto);

	List<AlertDashBoardRespDto> getGeoGraphicByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);

	List<AlertDashBoardRespDto> getPoliticalByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);

	List<AlertDashBoardRespDto> getCorruptionByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);

	List<RiskCountAndRatioDto> viewAll(String fromDate, String toDate, Integer pageNumber,
			Integer recordsPerPage, String type,FilterDto filterDto);

	long viewAllCount(String fromDate, String toDate, String type,FilterDto filterDto);

	List<RiskCountAndRatioDto> getScenarioAggregates(String fromDate, String toDate, String type);

	List<TopAmlCustomersDto> amlTopCustomers(String fromDate, String toDate,FilterDto filterDto)  throws ParseException ;

	List<TransactionAmountAndAlertCountDto> amlTransferByMonth(FilterDto filterDto) throws ParseException;

	List<TransactionAmountAndAlertCountDto> getInputAndOutputCount(String fromDate, String toDate)
			throws ParseException;

	CounterPartyInfoDto counterPartyInfo(String fromDate, String toDate, Long customerId,FilterDto filterDto);

	List<AlertedCounterPartyDto> getCustomerTransactions(String fromDate, String toDate, Long customerId,
			Integer pageNumber, Integer recordsPerPage, boolean isDetected,FilterDto filterDto);

	Long getCustomerTransactionsCount(String fromDate, String toDate, Long customerId, boolean isDetected,FilterDto filterDto);

	Boolean updateTransactions();
	Boolean alterDatabse();
	Boolean auditColumns();
	Boolean updatePassport();
	Boolean processAllFiles(List<MultipartFile> multipartFilesList,Long userId)  throws IOException, ParseException,IllegalAccessException, InvocationTargetException , Exception  ;
	public List<TransactionMasterView> getMasterDataFromView();

	List<AlertDashBoardRespDto> fetchAlertsByNumber(String customerNumber);
	
	List<AlertDashBoardRespDto> fetchAlerts();

	List<AlertDashBoardRespDto> getEntityBasedAlerts(String fromDate, String toDate, Integer pageNumber,
			Integer recordsPerPage, String orderIn, String entityType);

	long getEntityBasedAlertsCount(String fromDate, String toDate, String name);
	
   // void updateTransactions(UpdateTransactionsDataDto transactionsDataDto) throws Exception;
	void updateTransactions(TxEntityRespDto txEntityRespDto) throws Exception; 
	
	void deleteTransaction(Long id) throws Exception;
	
	boolean updateFetchAlerts(AlertDashBoardRespDto alertDashBoardRespDto, long userId) throws Exception ;
}
