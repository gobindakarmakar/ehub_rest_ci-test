package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceMediaDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceMediaDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceMediaService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author suresh
 *
 */
@Service("sourceMediaService")
public class SourceMediaServiceImpl extends GenericServiceImpl<SourceMedia, Long> implements SourceMediaService {

	@Autowired
	public SourceMediaServiceImpl(GenericDao<SourceMedia, Long> genericDao) {
		super(genericDao);
	}

	public SourceMediaServiceImpl() {
	}

	@Autowired
	SourceMediaDao sourceMediaDao;

	@Override
	@Transactional("transactionManager")
	public List<SourceMediaDto> saveSourceMedia(SourceMediaDto mediaDto) throws Exception {
		SourceMedia mediaFromDB = sourceMediaDao.fetchMedia(mediaDto.getMediaName());
		if (mediaFromDB == null) {
			SourceMedia media = new SourceMedia();
			media.setMediaName(mediaDto.getMediaName());
			sourceMediaDao.create(media);
			return getSourceMedia();
		} else
			throw new Exception(ElementConstants.MEDIA_ADD_FAILED);
	}

	@Override
	@Transactional("transactionManager")
	public List<SourceMediaDto> getSourceMedia() {
		List<SourceMedia> mediaList = sourceMediaDao.findAll();
		List<SourceMediaDto> sourcesDtoList = new ArrayList<SourceMediaDto>();
		if (mediaList != null) {
			sourcesDtoList = mediaList.stream()
					.map((sourcesDto) -> new EntityConverter<SourceMedia, SourceMediaDto>(SourceMedia.class,
							SourceMediaDto.class).toT2(sourcesDto, new SourceMediaDto()))
					.collect(Collectors.toList());
		}
		return sourcesDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public SourceMedia fetchMedia(String media) {
		return sourceMediaDao.fetchMedia(media);
	}

}
