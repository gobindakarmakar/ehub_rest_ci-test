package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.List;

public class ListPermissionsDtoWithParent implements Serializable {

	private static final long serialVersionUID = 1L;

	List<DefaultPermissionsDto> permissionsList;

	public List<DefaultPermissionsDto> getPermissionsList() {
		return permissionsList;
	}

	public void setPermissionsList(List<DefaultPermissionsDto> permissionsList) {
		this.permissionsList = permissionsList;
	}

	

}
