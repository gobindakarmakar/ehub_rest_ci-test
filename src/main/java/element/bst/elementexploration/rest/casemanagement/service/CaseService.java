package element.bst.elementexploration.rest.casemanagement.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.JsonObject;

import element.bst.elementexploration.rest.casemanagement.bigdata.domain.CaseSource;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAggregator;
import element.bst.elementexploration.rest.casemanagement.dto.Answer;
import element.bst.elementexploration.rest.casemanagement.dto.CaseAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseVo;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import net.sourceforge.tess4j.TesseractException;

public interface CaseService extends GenericService<Case, Long> {
	
	boolean isCaseAccessibleOrOwner(Long caseId, Long userId);

	List<CaseDto> fullTextSearchForCaseSeed(String caseSearchKeyword, Integer pageNumber, Integer recordsPerPage,
			String creationDate, String modifiedDate, long userId, String type,Integer status,Integer priority,String orderBy,String orderIn) throws ParseException;
	
	long countFullTextSearchForCaseSeed(String caseSearchKeyword,long userId,String creationDate, String modifiedDate,String type,Integer status,Integer priority) throws ParseException;

	List<CaseDto> getAllCasesByUser(long userId, Integer pageNumber, Integer recordsPerPage,String type,Integer status,Integer priority,String orderBy,String orderIn);
	
	Long countAllCasesByUser(long userId,String type,Integer status,Integer priority);

	void mapUserWithCase(MultipartFile uploadFile, Long bstSeedId, Long userId, Long analystId) throws InsufficientDataException, IOException, ParseException, org.json.simple.parser.ParseException, Exception;
	
	List<CaseDto> getAllIncomingTrayBasedOnUserId(Long analystId,Integer pageNumber, Integer recordsPerPage, String orderBy,String orderIn);
	
	long countgetAllIncomingTray(Long userId);
	
	List<CaseDto> fullTextSearchForIncomingtray(Long userId,String caseSearchKeyword,Integer pageNumber,Integer recordsPerPage,String creationDate,String modifiedDate, String orderBy, String orderIn) throws ParseException;
	
	long countFullTextSearchForIncomingtray(Long userId,String caseSearchKeyword,String creationDate,String modifiedDate) throws ParseException;
	
	List<CaseDto> getAllMyCasesBasedOnUserId(Long userId,String orderBy,String orderIn,Integer pageNumber,Integer recordsPerPage);
	
	long countgetAllMyCases(Long userId);
	
	List<CaseDto> fullTextSearchForMyCases(Long userId,String caseSearchKeyword,Integer pageNumber,Integer recordsPerPage,String creationDate,String modifiedDate, String orderBy, String orderIn) throws ParseException;
	
	long countFullTextSearchForMyCases(Long userId,String caseSearchKeyword,String creationDate,String modifiedDate) throws ParseException;
	
	String getRelatedCasesById(Long caseId);
	
    String getHotTopicsByCaseId(Long caseId);
    
    CaseSource getCaseSourceById(Long caseId,Integer limit);
    
    String getCaseSummaryById(Long caseId);
    
    Users getNextAnalyst(Long caseId);	
    
    JsonObject getAnalystMetrics(String dateFrom, String dateTo, String type, Boolean currentOnly, Long analystId) throws ParseException;

	List<CaseDto> getCaseList(CaseVo caseVo,Long userId) throws Exception;

	Long caseListCount(CaseVo caseVo, Long userId);

	List<CaseAggregator> caseAggregator(Long userId, CaseAggregatorDto caseAggregatorDto);	

	public List<Answer> calculateRisk(List<Answer> answers,Answer rule);
	
	public void mapUserWithCase(String jsonString, Long bstSeedId, Long userId) throws Exception;
	
	List<Answer> readDataFromCsvFile(MultipartFile uploadFile) throws IOException, EncryptedDocumentException, InvalidFormatException;
	
	String generateSeedJsonData(List<Answer> answers);
	String generateSeedJsonData(Map<String, String> fields);

	List<Long> getCasesByUser(Long currentUserId);

	List<Long> getMyCaseIds(Long userId, CaseVo caseVo,Integer limit);

	void mapUserWithCaseTest(MultipartFile uploadFile, Long bstSeedId, Long userId, Long analystId) throws Exception;

	List<CaseUnderwritingDto> getCaseListForUnderwriting(Integer recordsPerPage, Integer pageNumber, String keyWord);

	Long caseListCountForUnderwriting(String keyWord);

	public CaseMapDocIdDto createCaseFromQuestionnaireFile(MultipartFile uploadFile,Long userId, Long docId, byte[] fileData) throws IOException, IllegalStateException, InvalidFormatException,TesseractException,InsufficientDataException, Exception;
	
	public String createEntity(Map<String, String> fields, String type) throws org.json.simple.parser.ParseException;

	List<CaseUnderwritingDto> searchCaseList(String keyWord);

	Long searchCaseListCount(String keyWord);

	String getIndustry(String entityId) throws org.json.simple.parser.ParseException, JsonParseException, JsonMappingException, URISyntaxException, IOException;
	
	List<CaseDto> getRiskScores(List<CaseDto> caseList);
	
	public Long assignCaseToAnalyst(Users user, Case bstCaseSeed);

	String getMultiSourceId(String caseName);

}
