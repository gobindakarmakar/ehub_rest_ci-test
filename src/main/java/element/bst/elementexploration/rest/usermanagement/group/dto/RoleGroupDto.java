package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Role Group Dto")
public class RoleGroupDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Role Group ID")
	private Long roleGroupId;

	@ApiModelProperty(value = "Role ID", required = true)
	@NotEmpty(message = "Role ID cannot be blank.")
	private Long roleId;

	@ApiModelProperty(value = "GroupId", required = true)
	@NotEmpty(message = "Group ID cannot be blank.")
	private Long groupId;

	public Long getRoleGroupId() {
		return roleGroupId;
	}

	public void setRoleGroupId(Long roleGroupId) {
		this.roleGroupId = roleGroupId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

}
