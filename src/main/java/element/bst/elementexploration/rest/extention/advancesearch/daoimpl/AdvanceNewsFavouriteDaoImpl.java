package element.bst.elementexploration.rest.extention.advancesearch.daoimpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.advancesearch.dao.AdvanceNewsFavouriteDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("AdvanceNewsFavouriteDao")
public class AdvanceNewsFavouriteDaoImpl extends GenericDaoImpl<AdvanceNewsFavourite, Long> implements  AdvanceNewsFavouriteDao{
	
	public AdvanceNewsFavouriteDaoImpl() {
		super(AdvanceNewsFavourite.class);
	}

	@Override
	public AdvanceNewsFavourite fetchadvanceNewsFavourite(String entityId, String entityName,Long userId) {
		AdvanceNewsFavourite advanceNewsFavouriteReturn = null;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("select sn from AdvanceNewsFavourite sn where sn.entityId= :entityId and sn.userId="+userId+" and  sn.entityName = :entityName");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId",entityId);
			query.setParameter("entityName",entityName);
			advanceNewsFavouriteReturn = (AdvanceNewsFavourite) query.getSingleResult();
		}catch (Exception e) {
			return advanceNewsFavouriteReturn;
		}
		return advanceNewsFavouriteReturn;
	}

	@Override
	public int deleteAdvanceNewsFavourite(String entityId) {
		int result=0;
		try{
			StringBuilder hql = new StringBuilder();
			hql.append("delete from AdvanceNewsFavourite  where entityId = :entityId");			
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			result = query.executeUpdate();			
		}catch (Exception e) {
			return result;
		}
		return result;
	}

	

}
