package element.bst.elementexploration.rest.base.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class date {
	
	public static boolean isValidDate(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");  
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        catch (Exception e) {
			e.printStackTrace();
		}
        return true;
    }

    public static void main(String[] args) {

       System.out.println(isValidDate("31-11-2014"));
       // System.out.println(isValidDate("11-04-2015 22:01:33:023"));

        //System.out.println(isValidDate("32476347656435"));
    	
    	/*
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");

        String stringToTest = "11-04-2015";

        try {
            LocalDateTime dateTime = LocalDateTime.parse(stringToTest, formatter);
            System.out.println("The string is a date and time: " + dateTime);
        } catch (DateTimeParseException dtpe) {
            System.out.println("The string is not a date and time: " + dtpe.getMessage());
        }*/
    }
}
