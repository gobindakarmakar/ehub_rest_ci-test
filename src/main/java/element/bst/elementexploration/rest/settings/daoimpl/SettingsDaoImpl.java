package element.bst.elementexploration.rest.settings.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.dao.SettingsDao;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("settingsDao")
public class SettingsDaoImpl extends GenericDaoImpl<Settings, Long> implements SettingsDao {

	public SettingsDaoImpl() {
		super(Settings.class);
	}

	@Override
	public Settings getDeaultUrl() {
		Settings settings = null;
		try {
			settings = (Settings) this.getCurrentSession().createQuery("from Settings s where s.name ='Default Module'")
					.getSingleResult();
		} catch (NoResultException ne) {
			settings = null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return settings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Settings> fetchAllSettings(String searchKey) {
		List<Settings> listSettings = null;
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("from Settings s ");
			if (!StringUtils.isEmpty(searchKey)) {
				builder.append("where" + " s.name like '%" + searchKey.trim() + "%'");
				listSettings = this.getCurrentSession().createQuery(builder.toString()).getResultList();
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return listSettings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Settings> getMailSettings(String type) {
		List<Settings> settings=new ArrayList<>();
		try{
			StringBuilder builder = new StringBuilder();
			builder.append("from Settings s where s.systemSettingType=:type");
			settings=this.getCurrentSession().createQuery(builder.toString()).setParameter("type", type).getResultList();
			
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return settings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Settings> getSystemSettingsByType(String settingType) {
		
		List<Settings> settings=new ArrayList<>();
		try{
			StringBuilder builder = new StringBuilder();
			builder.append("from Settings s where s.systemSettingType=:type");
			settings=this.getCurrentSession().createQuery(builder.toString()).setParameter("type", settingType).getResultList();
			
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return settings;
	}

	@Override
	public Settings findSettingByTypeAndName(String settingType, String settingName) {
		Settings settings = null;
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("from Settings s where s.name=:name and s.systemSettingType=:type");
			settings = (Settings) this.getCurrentSession().createQuery(builder.toString())
					.setParameter("name", settingName).setParameter("type", settingType).getSingleResult();
		} catch (NoResultException ne) {
			settings = null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return settings;
	}

	@Override
	public Settings fetchSettingByName(String name) {
		Settings settings=new Settings();
		
		try{
			StringBuilder builder = new StringBuilder();
			builder.append("from Settings s where s.name=:name");
			settings=(Settings) this.getCurrentSession().createQuery(builder.toString()).setParameter("name", name).getSingleResult();
			
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		}
		return settings;
	}

	@Override
	public String getCustomerLogo() {
		Settings settings = null;
		try {
			settings = (Settings) this.getCurrentSession().createQuery("from Settings s where s.name = 'Company Logo'")
					.getSingleResult();
		} catch (NoResultException ne) {
			settings = null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return settings.getCustomerLogoImage();
	}
}
