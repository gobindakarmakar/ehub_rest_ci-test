package element.bst.elementexploration.rest.usermanagement.security.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.CountrySetting;

/**
 * 
 * @author Amit Patel
 *
 */
public interface CountrySettingDao extends GenericDao<CountrySetting, Long>{
	
	CountrySetting getCountrySetting(String countryIso);
}
