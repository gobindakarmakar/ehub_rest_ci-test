package element.bst.elementexploration.rest.extention.alertmanagement.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.alertmanagement.domain.AlertComments;
import element.bst.elementexploration.rest.extention.alertmanagement.service.AlertCommentsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Prateek
 *
 */
@Service("alertCommentsService")
public class AlertCommentsServiceImpl extends GenericServiceImpl<AlertComments, Long> implements  AlertCommentsService{  
	
	public AlertCommentsServiceImpl() {
	}

	@Autowired
	public AlertCommentsServiceImpl(GenericDao<AlertComments, Long> genericDao){
		super(genericDao);
	}
}
