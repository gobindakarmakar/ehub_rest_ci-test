package element.bst.elementexploration.rest.usermanagement.security.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "bst_failed_login")
public class FailedLogin implements Serializable{

	private static final long serialVersionUID = 6849884732508858002L;
	
	/**
	 * Primary Key. Faild Login Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_failed_login_id")
	private Long failedLoginId;
	
	/**
	 * Username or email address or ip address
	 */
	@Column(name = "username")
	private String username;
	
	@Column(name = "screen_name")
	private String screenName;
	
	/**
	 * Failed Login Attempt
	 */
	@Column(name = "failed_login_attempt")
	private Integer failedLoginAttempt;
	
	/**
     * Last Failed Login IP Address
     */
	@Column(name = "last_failed_login_ip_address", length = 255)
	private String lastFailedLoginIpAddress;
	
	/**
     * Last Failed Login Time
     */
	@Column(name = "last_failed_login_time")
	private Date lastFailedLoginTime;
	
	/**
     * Last Successful Login IP Address
     */
	@Column(name = "last_successful_login_ip_address", length = 255)
	private String lastSuccessfulLoginIpAddress;
	
	/**
     * Last Successful Login Time
     */
	@Column(name = "last_successful_login_time")
	private Date lastSuccessfulLoginTime;
	
	/**
	 * Constructor
	 */
	public FailedLogin() {
		
	}	

	public FailedLogin(Long failedLoginId, String username, String screenName, Integer failedLoginAttempt,
			String lastFailedLoginIpAddress, Date lastFailedLoginTime, String lastSuccessfulLoginIpAddress,
			Date lastSuccessfulLoginTime) {
		super();
		this.failedLoginId = failedLoginId;
		this.username = username;
		this.screenName = screenName;
		this.failedLoginAttempt = failedLoginAttempt;
		this.lastFailedLoginIpAddress = lastFailedLoginIpAddress;
		this.lastFailedLoginTime = lastFailedLoginTime;
		this.lastSuccessfulLoginIpAddress = lastSuccessfulLoginIpAddress;
		this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
	}

	public Long getFailedLoginId() {
		return failedLoginId;
	}

	public void setFailedLoginId(Long failedLoginId) {
		this.failedLoginId = failedLoginId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Integer getFailedLoginAttempt() {
		return failedLoginAttempt;
	}

	public void setFailedLoginAttempt(Integer failedLoginAttempt) {
		this.failedLoginAttempt = failedLoginAttempt;
	}

	public String getLastFailedLoginIpAddress() {
		return lastFailedLoginIpAddress;
	}

	public void setLastFailedLoginIpAddress(String lastFailedLoginIpAddress) {
		this.lastFailedLoginIpAddress = lastFailedLoginIpAddress;
	}

	public Date getLastFailedLoginTime() {
		return lastFailedLoginTime;
	}

	public void setLastFailedLoginTime(Date lastFailedLoginTime) {
		this.lastFailedLoginTime = lastFailedLoginTime;
	}

	public String getLastSuccessfulLoginIpAddress() {
		return lastSuccessfulLoginIpAddress;
	}

	public void setLastSuccessfulLoginIpAddress(String lastSuccessfulLoginIpAddress) {
		this.lastSuccessfulLoginIpAddress = lastSuccessfulLoginIpAddress;
	}

	public Date getLastSuccessfulLoginTime() {
		return lastSuccessfulLoginTime;
	}

	public void setLastSuccessfulLoginTime(Date lastSuccessfulLoginTime) {
		this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
