package element.bst.elementexploration.rest.extention.story.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.story.dto.StoryDto;
import element.bst.elementexploration.rest.extention.story.service.UserStoryService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "User Story API" }, description = "User favourite story")
@RestController
@RequestMapping("/api/story")
public class UserStoryController extends BaseController {

	@Autowired
	private UserStoryService storyService;

	@ApiOperation("Mark story as favourite")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Marked as favourite story"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/markAsFavourite", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> markAsFavourite(@RequestParam("token") String token,
			@RequestParam("storyId") String storyId, HttpServletRequest request) {
		Long userId = this.getCurrentUserId();
		storyService.markAsFavourite(storyId, userId);
		return new ResponseEntity<>(new ResponseMessage("Marked as favourite story"), HttpStatus.OK);
	}

	@ApiOperation("Get user favourite story")
	@ApiResponses(value = { @ApiResponse(code = 200, response = StoryDto.class, message = "Operation successful."),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Story not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getFavouriteStory", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getFavouriteStory(@RequestParam("token") String token, HttpServletRequest request) {
		Long userId = this.getCurrentUserId();
		return new ResponseEntity<>(storyService.getFavouriteStory(userId), HttpStatus.OK);
	}

}
