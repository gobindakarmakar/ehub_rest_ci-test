package element.bst.elementexploration.rest.version.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.version.dao.VersionDao;
import element.bst.elementexploration.rest.version.domain.ProductVersion;

@Repository("versionDao")
public class VersionDaoImpl extends GenericDaoImpl<ProductVersion, Long> implements VersionDao {

	public VersionDaoImpl() {
		super(ProductVersion.class);
	}

	@Override
	public ProductVersion getLatestVersion() {
		List<ProductVersion> versions = new ArrayList<>();
		ProductVersion currentVersion = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<ProductVersion> query = builder.createQuery(ProductVersion.class);
			Root<ProductVersion> version = query.from(ProductVersion.class);
			query.select(version);
			query.orderBy(builder.desc(version.get("releaseDate")));
			versions = this.getCurrentSession().createQuery(query).getResultList();
			if (!versions.isEmpty())
				currentVersion = versions.get(0);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get last checkin");
		}
		return currentVersion;
	}

	@Override
	public List<ProductVersion> getAllVersions() {
		List<ProductVersion> versions = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<ProductVersion> query = builder.createQuery(ProductVersion.class);
			Root<ProductVersion> version = query.from(ProductVersion.class);
			query.select(version);
			query.orderBy(builder.desc(version.get("releaseDate")));
			versions = this.getCurrentSession().createQuery(query).getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to get last checkin");
		}
		return versions;
	}
}
