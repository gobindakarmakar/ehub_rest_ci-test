package element.bst.elementexploration.rest.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Amit Patel
 *
 */

public class LoopBackConstants {

	public static final List<String> LOOP_BACK = new ArrayList<String>(Arrays.asList("0:0:0:0:0:0:0:1","0.0.0.0"));
}
