package element.bst.elementexploration.rest.settings.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.settings.domain.Settings;

/**
 * @author suresh
 *
 */
public interface SettingsDao extends  GenericDao<Settings, Long>
{
	public Settings getDeaultUrl();
	List<Settings> fetchAllSettings(String searchKey);
	Settings fetchSettingByName(String name);
	public List<Settings> getMailSettings(String type);
	public List<Settings> getSystemSettingsByType(String settingType);
	public Settings findSettingByTypeAndName(String settingType, String settingName);
	String getCustomerLogo();
	
}
