package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

public class AlertNotiDto implements Serializable,Comparable<AlertNotiDto> {

	private static final long serialVersionUID = 1L;
	private Date alertBusinessDate;
	private double alertAmount;

	public AlertNotiDto(Date alertBusinessDate, double alertAmount) {
		super();
		this.alertBusinessDate = alertBusinessDate;
		this.alertAmount = alertAmount;
	}

	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	public double getAlertAmount() {
		return alertAmount;
	}

	public void setAlertAmount(double alertAmount) {
		this.alertAmount = alertAmount;
	}
	
	@Override
	public int compareTo(AlertNotiDto o) {
		return this.getAlertBusinessDate().compareTo(o.getAlertBusinessDate());
	}

}
