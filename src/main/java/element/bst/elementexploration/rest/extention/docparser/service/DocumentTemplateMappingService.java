package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.generic.service.GenericService;
/**
 * @author suresh
 *
 */
public interface DocumentTemplateMappingService extends GenericService<DocumentTemplateMapping, Long> {

}
