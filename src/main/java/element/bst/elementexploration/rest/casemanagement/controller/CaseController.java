package element.bst.elementexploration.rest.casemanagement.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.Answer;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocDetailsDTO;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocumentUploadResponse;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocumentsDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseResult;
import element.bst.elementexploration.rest.casemanagement.dto.CaseUpdateDto;
import element.bst.elementexploration.rest.casemanagement.dto.CreateCaseRequestDto;
import element.bst.elementexploration.rest.casemanagement.dto.CreateCaseResponseDto;
import element.bst.elementexploration.rest.casemanagement.dto.CreateCaseWorkFlowRequestDto;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseDocumentService;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.exception.FileFormatException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.extention.docparser.dto.CaseMapDocIdDto;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentParserService;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentTemplatesService;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupService;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Case API" })
@RestController
@RequestMapping("/api/case")
public class CaseController extends BaseController {

	@Autowired
	private CaseService caseService;

	@Autowired
	UsersService usersService;

	@Autowired
	private CaseDocumentService caseDocumentService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@SuppressWarnings("unused")
	@Autowired
	private DocumentTemplatesService documentTemplatesService;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private DocumentParserService documentParserService;
	
	@Autowired
	private ElementsSecurityService elementsSecurityService;
	
	@Autowired
	private GroupService groupService;

	@ApiOperation("Creates a case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CreateCaseResponseDto.class, message = "Case created successfully"),
			@ApiResponse(code = 400, response = String.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 401, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/createCase", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCaseSeed(@Valid @RequestBody CreateCaseRequestDto createCaseRequestDto, BindingResult results,
			@RequestParam("token") String token, HttpServletRequest request) {
		if (!results.hasErrors()) {
			Long userIdTemp = getCurrentUserId();
			Case bstCaseSeed=new Case();
			BeanUtils.copyProperties(createCaseRequestDto, bstCaseSeed);
			bstCaseSeed.setCreatedOn(new Date());
			Users userTemp = usersService.find(userIdTemp);
			bstCaseSeed.setCreatedBy(userTemp);
			bstCaseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());
			/*if (bstCaseSeed.getName() != null) {
				bstCaseSeed.setMultiSourceId(caseService.getMultiSourceId(bstCaseSeed.getName()));
			}*/
			Case caseSeed = caseService.save(bstCaseSeed);
			/*JSONObject json = new JSONObject();
			json.put("id", caseSeed.getCaseId());*/
			CreateCaseResponseDto createCaseDto = new CreateCaseResponseDto();
			createCaseDto.setId(caseSeed.getCaseId());
			eventPublisher.publishEvent(
					new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.CREATE_CASE, userIdTemp, new Date()));
			//return new ResponseEntity<>(json, HttpStatus.OK);
			return new ResponseEntity<>(createCaseDto, HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Uploads json file of the case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Json file uploaded successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not find analyst for given case or analyst has rejected the case"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.CASE_SEED_NOT_FOUND),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process your request, please provide mandatory data"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "No enough permissions") })
	@PostMapping(value = "/uploadSeed", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadJsonFileToDatabase(@RequestBody MultipartFile uploadFile,
			@RequestParam("caseId") Long caseId, @RequestParam(required = false) Long analystId,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {

		Long userIdTemp = getCurrentUserId();
		caseService.mapUserWithCaseTest(uploadFile, caseId, userIdTemp, analystId);
		return new ResponseEntity<>(new ResponseMessage("Uploaded json file and linked case to user."), HttpStatus.OK);
	}

	@ApiOperation("Gets case json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.CASE_SEED_NOT_FOUND),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getCaseJson/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<StreamingResponseBody> getCaseseedJson(@PathVariable Long caseId,
			@RequestParam("token") String token, HttpServletResponse response, HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
		Case caseSeed = caseService.find(caseId);
		if (caseSeed != null) {
			byte[] fileData = caseSeed.getCaseJsonFile();
			ServiceCallHelper.setDownloadResponse(response, "CaseJson_" + caseSeed.getCaseId() + ".json",
					"application/json", caseSeed.getCaseJsonFile() != null ? caseSeed.getCaseJsonFile().length : 0);
			StreamingResponseBody responseBody = new StreamingResponseBody() {
				@Override
				public void writeTo(OutputStream out) throws IOException {
					out.write(fileData);
					out.flush();
				}
			};
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} else {
			throw new NoDataFoundException(ElementConstants.CASE_SEED_NOT_FOUND);
		}
	}

	@ApiOperation("Updates case by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.CASE_SEED_UPDATE_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.CASE_SEED_NOT_FOUND),
			@ApiResponse(code = 400, response = String.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 401, response = String.class, message = "No enough permissions")})
	@PostMapping(value = "/updateCase", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateBstCaseSeedById(@Valid @RequestBody CaseUpdateDto caseUpdateDto, BindingResult results,
			@RequestParam("token") String token, @RequestParam("caseId") Long caseId, HttpServletRequest request) {
		if (!results.hasErrors()) {
			Long userIdTemp = getCurrentUserId();
			caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
			Case caseSeed = caseService.find(caseId);
			Case bstCaseSeed = new Case();
			BeanUtils.copyProperties(caseUpdateDto, bstCaseSeed);
			if (caseSeed != null) {
				caseSeed.setModifiedBy(userIdTemp);
				caseSeed.setModifiedOn(new Date());
				caseSeed.setName(bstCaseSeed.getName());
				caseSeed.setDescription(bstCaseSeed.getDescription());
				caseSeed.setRemarks(bstCaseSeed.getRemarks());
				caseSeed.setPriority(bstCaseSeed.getPriority());
				caseSeed.setHealth(bstCaseSeed.getHealth());
				caseSeed.setDirectRisk(bstCaseSeed.getDirectRisk());
				caseSeed.setIndirectRisk(bstCaseSeed.getIndirectRisk());
				caseSeed.setTransactionalRisk(bstCaseSeed.getTransactionalRisk());
				caseSeed.setType(bstCaseSeed.getType());
				caseService.saveOrUpdate(caseSeed);
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.CASE_SEED_UPDATE_MSG), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.CASE_SEED_NOT_FOUND),
						HttpStatus.NOT_FOUND);
			}
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

	@ApiOperation("Creates a case from questioner file")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "A new Case is created successfully from csv file"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Error creating case") })
	@PostMapping(value = "/createCaseFromQuestioner", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCaseFromExcel(@RequestBody(required = false) MultipartFile uploadFile,
			@RequestParam("token") String token, @RequestParam(required = false) Long caseId,@RequestParam(required = false) Long docId, HttpServletRequest request,HttpServletResponse response) throws Exception {
		DocumentVault documentValut = new DocumentVault();
		String fileExtension = "";
		if(docId != null){
			documentValut = documentService.find(docId);			
			fileExtension = documentValut.getType();
		}
		else{
			if( !uploadFile.isEmpty())
				fileExtension = FilenameUtils.getExtension(uploadFile.getOriginalFilename());
		}		
		if(fileExtension.equalsIgnoreCase("csv") || fileExtension.equalsIgnoreCase("pdf") || fileExtension.equalsIgnoreCase("docx")){
			if(uploadFile != null){
			int maxFileSize = 10 * 1024 * 1024;
			if(uploadFile.isEmpty()){
				throw new BadRequestException("The file empty");
			}
			if(uploadFile.getSize() > maxFileSize){
				throw new BadRequestException("The file size exceeds the limit allowed (10MB)");
			}
		}
		}else{
			throw new FileFormatException("Invalid File Type, allowed file types are: csv, word(.docx), pdf");
		}
		if (fileExtension.equalsIgnoreCase("csv")) {
			Long userId = getCurrentUserId();
			List<Answer> answers = caseService.readDataFromCsvFile(uploadFile);
			CaseMapDocIdDto caseMapDocIdDto = caseService.createCaseFromQuestionnaireFile(uploadFile, userId,docId,null);
			Map<String, String> fields = caseMapDocIdDto.getCaseFields();
			if(StringUtils.isEmpty(fields.get("name"))){
				if(!StringUtils.isEmpty(fields.get("website"))){								
						URI uri = new URI(fields.get("website"));
						String domain = uri.getHost();	
						fields.put("name", domain.startsWith("www.") ? domain.substring(4) : domain);	
					}
					else
					fields.put("name","unknown");
			}
			String entityId = null;
			String industry = null;
			Answer type = null;
			Answer name = null;
			Answer remark = null;
			Answer companyName = null;
			Answer description = null;
			Answer priority = null;
			Answer rule = null;
			for (Answer ans : answers) {
				if ("name".equalsIgnoreCase(ans.getQuestionId()))
					name = ans;
				if ("customerType".equalsIgnoreCase(ans.getQuestionId()))
					type = ans;
				if ("remark".equalsIgnoreCase(ans.getQuestionId()))
					remark = ans;
				if ("companyName".equalsIgnoreCase(ans.getQuestionId()))
					companyName = ans;
				if ("description".equalsIgnoreCase(ans.getQuestionId()))
					description = ans;
				if ("priority".equalsIgnoreCase(ans.getQuestionId()))
					priority = ans;
				if ("rule".equalsIgnoreCase(ans.getQuestionId()))
					rule = ans;
			}

			if (name != null || companyName != null) {
				Long userIdTemp = getCurrentUserId();
				Case caseSeed = caseId == null ? new Case() : caseService.find(caseId);
				caseSeed.setType(type.getResponse() + "-KYC");
				if (remark != null)
					caseSeed.setRemarks(remark.getResponse());
				if (description != null)
					caseSeed.setDescription(description.getResponse());
				if (priority != null)
					caseSeed.setPriority(PriorityEnums.values()[Integer.parseInt(priority.getResponse())]);
				if ("Individual".equalsIgnoreCase(type.getResponse())) {
					if(caseId == null)
						caseSeed.setName(name.getResponse());
					entityId = caseService.createEntity(fields, "Individual");
				} else if ("Corporate".equalsIgnoreCase(type.getResponse())) {
					if(caseId == null)
						caseSeed.setName(companyName.getResponse());
					entityId = caseService.createEntity(fields, "Corporate");
				}
				if(entityId!=null){
					industry=caseService.getIndustry(entityId);
				}
				double directRisk = 1;
				/*if (rule != null && rule.getResponse() != null) {
					List<Answer> resultAnswers = caseService.calculateRisk(answers, rule);
					for (Answer ans : resultAnswers) {
						if (ans.getRisk() != 0) {
							directRisk *= (1 - ans.getRisk() / 100.0);
						}
					}
				}
				directRisk = 1 - directRisk;
				caseSeed.setDirectRisk(directRisk);*/
				//caseSeed.setCreatedOn(new Date());
				caseSeed.setQuestionnaireDocId(caseMapDocIdDto.getDocId());
				/*Users userTemp = usersService.find(userIdTemp);
				caseSeed.setCreatedBy(userTemp);
				caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());*/
				caseSeed.setEntityId(entityId);
				caseSeed.setIndustry(industry);
				/*if (caseSeed.getName() != null) {
					caseSeed.setMultiSourceId(caseService.getMultiSourceId(caseSeed.getName()));
				}*/
				Case newCase = null;
				if(caseId == null){
					caseSeed.setCreatedOn(new Date());
					Users userTemp = usersService.find(userIdTemp);
					caseSeed.setCreatedBy(userTemp);
					caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());
					newCase = caseService.save(caseSeed);
				}else{
					caseService.saveOrUpdate(caseSeed);
					newCase = caseService.find(caseSeed.getCaseId());
				}
				eventPublisher.publishEvent(new LoggingEvent(newCase.getCaseId(), LoggingEventType.CREATE_CASE,
						getCurrentUserId(), new Date()));
				String jsonString = caseService.generateSeedJsonData(answers);
				caseService.mapUserWithCase(jsonString, newCase.getCaseId(), userIdTemp);
				DocumentVault documentVault = documentService.find(caseMapDocIdDto.getDocId());
				documentVault.setCaseId(newCase.getCaseId());
				documentService.saveOrUpdate(documentVault);
				documentParserService.setEntityToAnswers(caseMapDocIdDto.getDocumentAnswers(), entityId);
				CreateCaseResponseDto createCaseDto = new CreateCaseResponseDto();
				createCaseDto.setId(caseSeed.getCaseId());
				return new ResponseEntity<>(createCaseDto, HttpStatus.OK);	
			} else {
				return new ResponseEntity<>(new ResponseMessage("Error creating case."),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} else if(fileExtension.equalsIgnoreCase("docx")){
			Long userIdTemp = getCurrentUserId();
			CaseMapDocIdDto caseMapDocIdDto = caseService.createCaseFromQuestionnaireFile(uploadFile, userIdTemp,docId,null);	
				if(caseMapDocIdDto != null){
					Map<String, String> fields = caseMapDocIdDto.getCaseFields();
					if(StringUtils.isEmpty(fields.get("name"))){
						if(!StringUtils.isEmpty(fields.get("website"))){							
								URI uri = new URI(fields.get("website"));
								String domain = uri.getHost();	
								fields.put("name", domain.startsWith("www.") ? domain.substring(4) : domain);	
							}
							else
							fields.put("name","unknown");
					}
				String entityId = caseService.createEntity(fields, "Corporate");
				String industry = null;
				if(entityId!=null){
					industry=caseService.getIndustry(entityId);
				}
				Case caseSeed = caseId == null ? new Case() : caseService.find(caseId);
				if(caseId == null){
					//caseSeed.setName(fields.get("name"));
					caseSeed.setCreatedOn(new Date());
					Users userTemp = usersService.find(userIdTemp);
					caseSeed.setCreatedBy(userTemp);
					caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());
				}
				caseSeed.setName(fields.get("name"));
				caseSeed.setType("Corporate-KYC");
				//caseSeed.setName(fields.get("name"));
				caseSeed.setEntityId(entityId);
				caseSeed.setIndustry(industry);
				//caseSeed.setCreatedOn(new Date());
				/*Users userTemp = usersService.find(userIdTemp);
				caseSeed.setCreatedBy(userTemp);
				caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());*/
				caseSeed.setQuestionnaireDocId(caseMapDocIdDto.getDocId());
				/*if (caseSeed.getName() != null) {
					caseSeed.setMultiSourceId(caseService.getMultiSourceId(caseSeed.getName()));
				}*/
				Case newCase = null;
				if(caseId == null){
					newCase = caseService.save(caseSeed);
				}else{
					caseService.saveOrUpdate(caseSeed);
					newCase = caseService.find(caseSeed.getCaseId());
				}
				eventPublisher.publishEvent(new LoggingEvent(newCase.getCaseId(), LoggingEventType.CREATE_CASE,
						getCurrentUserId(), new Date()));				
				fields.put("customerType", "Corporate");
				//fields.put("companyName", "companyName");
				String jsonString = caseService.generateSeedJsonData(fields);
				caseService.mapUserWithCase(jsonString, newCase.getCaseId(), userIdTemp);
				DocumentVault documentVault = documentService.find(caseMapDocIdDto.getDocId());
				documentVault.setCaseId(newCase.getCaseId());
				documentService.saveOrUpdate(documentVault);
				documentParserService.setEntityToAnswers(caseMapDocIdDto.getDocumentAnswers(), entityId);
				CreateCaseResponseDto createCaseDto = new CreateCaseResponseDto();
				createCaseDto.setId(caseSeed.getCaseId());
				return new ResponseEntity<>(createCaseDto, HttpStatus.OK);			
			}else{
				return new ResponseEntity<>(new ResponseMessage("Error creating case."),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}					
		} 
		else if(fileExtension.equalsIgnoreCase("pdf"))
		{
			Long userIdTemp = getCurrentUserId();
			byte[] fileData = null;
			if(docId != null && (uploadFile == null)){
				fileData = documentService.downloadDocument(docId, getCurrentUserId(), response);
			}
			 
			CaseMapDocIdDto caseMapDocIdDto = caseService.createCaseFromQuestionnaireFile(uploadFile, userIdTemp,docId,fileData);
			if(caseMapDocIdDto!=null){
				Map<String, String> fields = caseMapDocIdDto.getCaseFields();
				if(StringUtils.isEmpty(fields.get("name"))){
					if(!StringUtils.isEmpty(fields.get("website"))){								
							URI uri = new URI(fields.get("website"));
							String domain = uri.getHost();	
							fields.put("name", domain.startsWith("www.") ? domain.substring(4) : domain);	
						}
						else
						fields.put("name","unknown");
				}
				String entityId = caseService.createEntity(fields, "Corporate");	
				String industry = null;
				if(entityId!=null){
					industry=caseService.getIndustry(entityId);
				}
				Case caseSeed = caseId == null ? new Case() : caseService.find(caseId);
				if(caseId == null){
					//caseSeed.setName(fields.get("name"));
					caseSeed.setCreatedOn(new Date());
					Users userTemp = usersService.find(userIdTemp);
					caseSeed.setCreatedBy(userTemp);
					caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());
				}
				caseSeed.setName(fields.get("name"));
				caseSeed.setType("Corporate-KYC");
				//caseSeed.setName(fields.get("name"));
				caseSeed.setEntityId(entityId);
				caseSeed.setIndustry(industry);
				//caseSeed.setCreatedOn(new Date());
				/*Users userTemp = usersService.find(userIdTemp);
				caseSeed.setCreatedBy(userTemp);
				caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());*/
				caseSeed.setQuestionnaireDocId(caseMapDocIdDto.getDocId());
				/*if (caseSeed.getName() != null) {
					caseSeed.setMultiSourceId(caseService.getMultiSourceId(caseSeed.getName()));
				}*/
				Case newCase = null;
				if(caseId == null){
					newCase = caseService.save(caseSeed);
				}else{
					caseService.saveOrUpdate(caseSeed);
					newCase = caseService.find(caseSeed.getCaseId());
				}
				eventPublisher.publishEvent(new LoggingEvent(newCase.getCaseId(), LoggingEventType.CREATE_CASE,
						getCurrentUserId(), new Date()));				
				fields.put("customerType", "Corporate");
				//fields.put("companyName", "companyName");
				String jsonString = caseService.generateSeedJsonData(fields);
				caseService.mapUserWithCase(jsonString, newCase.getCaseId(), userIdTemp);
				DocumentVault documentVault = documentService.find(caseMapDocIdDto.getDocId());
				documentVault.setCaseId(newCase.getCaseId());
				documentService.saveOrUpdate(documentVault);
				documentParserService.setEntityToAnswers(caseMapDocIdDto.getDocumentAnswers(), entityId);
				CreateCaseResponseDto createCaseDto = new CreateCaseResponseDto();
				createCaseDto.setId(caseSeed.getCaseId());
				return new ResponseEntity<>(createCaseDto, HttpStatus.OK);			
			}else{
				return new ResponseEntity<>(new ResponseMessage("Error creating case."),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		else
		{
			return new ResponseEntity<>(new ResponseMessage("Error creating case."),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@ApiOperation("Download Questioner")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code=404 ,response=ResponseMessage.class ,message="Document not found."),
			@ApiResponse(code=401 ,response=ResponseMessage.class ,message="Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG)})
	@GetMapping(value = "/downloadQuestioner", produces = { "application/json; charset=UTF-8", MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<StreamingResponseBody> downloadDocumentDetails(HttpServletResponse response, @RequestParam("caseId") Long caseId, 
			@RequestParam("token") String userId, HttpServletRequest request) throws DocNotFoundException, Exception {
		long userIdTemp = getCurrentUserId();
		caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
		Case caseSeed = caseService.find(caseId);
		if(caseSeed != null){
			Long docId = caseSeed.getQuestionnaireDocId();
			if(docId != null){
				DocumentVault documentVault = documentService.find(docId);
				if(documentVault != null){
					byte[] fileData = documentService.downloadDocument(docId, userIdTemp, response);
					ServiceCallHelper.setDownloadResponse(response, documentVault.getDocName(), documentVault.getType(),
							documentVault.getSize() != null ? documentVault.getSize().intValue() : 0);
					StreamingResponseBody responseBody = new StreamingResponseBody() {
						@Override
						public void writeTo(OutputStream out) throws IOException {
							 out.write(fileData);
							 out.flush();					
						}
			        };
			        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
			        return new ResponseEntity<>(responseBody, HttpStatus.OK);
				}else{
					throw new DocNotFoundException();
				}		
			}else{
				throw new DocNotFoundException();
			}				
		}else {
			throw new NoDataFoundException(ElementConstants.CASE_SEED_NOT_FOUND);
		}
		
	}
	/*@ApiOperation("Creates a case from Questionnaire file")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "A new Case is created successfully from questionnaire file"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Error creating case") })
	@PostMapping(value = "/createCaseFromQuestionnaireFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
	"application/json; charset=UTF-8" })
public ResponseEntity<?> createCaseFromQuestionnaireFile(@RequestBody MultipartFile uploadFile,
	@RequestParam("token") String token) throws Exception {
		
		List<DocumentTemplates> documentTemplatesList=documentTemplatesService.findAll();
		DocumentTemplates documentTemplates=documentTemplatesList.get(0);
		Long userIdTemp = getCurrentUserId();
		if(documentTemplates!=null){
			caseService.createCaseFromQuestionnaireFile(uploadFile, userIdTemp, documentTemplates);
			
		}else{
			return new ResponseEntity<>(new ResponseMessage("Error creating case."),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(new ResponseMessage("A new Case is created successfully from questionnaire file."),
				HttpStatus.OK);
		
		
		
		
	}
*/
	@ApiOperation("Creates a case from kyc survey")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Case is created from Kyc survey successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Error creating case") })
	@PostMapping(value = "/createCaseFromSurvey", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCaseFromSurvey(@RequestBody List<Answer> answers,
			@RequestParam("token") String token) throws Exception {
		Answer type = null;
		Answer name = null;
		Answer remark = null;
		Answer companyName = null;
		Answer description = null;
		Answer priority = null;
		Answer rule = null;
		for (Answer ans : answers) {
			if ("name".equalsIgnoreCase(ans.getQuestionId()))
				name = ans;
			if ("customerType".equalsIgnoreCase(ans.getQuestionId()))
				type = ans;
			if ("remark".equalsIgnoreCase(ans.getQuestionId()))
				remark = ans;
			if ("companyName".equalsIgnoreCase(ans.getQuestionId()))
				companyName = ans;
			if ("description".equalsIgnoreCase(ans.getQuestionId()))
				description = ans;
			if ("priority".equalsIgnoreCase(ans.getQuestionId()))
				priority = ans;
			if ("rule".equalsIgnoreCase(ans.getQuestionId()))
				rule = ans;
		}

		if (name != null || companyName != null) {
			Long userIdTemp = getCurrentUserId();
			Case caseSeed = new Case();
			caseSeed.setType(type.getResponse() + "-KYC");
			if (remark != null)
				caseSeed.setRemarks(remark.getResponse());
			if (description != null)
				caseSeed.setDescription(description.getResponse());
			if (priority != null)
				caseSeed.setPriority(PriorityEnums.values()[Integer.parseInt(priority.getResponse())]);
			if ("Individual".equalsIgnoreCase(type.getResponse())) {
				caseSeed.setName(name.getResponse());
			} else if ("Corporate".equalsIgnoreCase(type.getResponse())) {
				caseSeed.setName(companyName.getResponse());
			}
			double directRisk = 1;
			if (rule != null && rule.getResponse() != null) {
				List<Answer> resultAnswers = caseService.calculateRisk(answers, rule);
				for (Answer ans : resultAnswers) {
					if (ans.getRisk() != 0) {
						directRisk *= (1 - ans.getRisk() / 100.0);
					}
				}
			}
			directRisk = 1 - directRisk;
			caseSeed.setDirectRisk(directRisk);
			caseSeed.setCreatedOn(new Date());
			Users userTemp = usersService.find(userIdTemp);
			caseSeed.setCreatedBy(userTemp);
			caseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());
			/*if (caseSeed.getName() != null) {
				caseSeed.setMultiSourceId(caseService.getMultiSourceId(caseSeed.getName()));
			}*/
			Case newCase = caseService.save(caseSeed);
			eventPublisher.publishEvent(new LoggingEvent(newCase.getCaseId(), LoggingEventType.CREATE_CASE,
					getCurrentUserId(), new Date()));
			String jsonString = caseService.generateSeedJsonData(answers);
			caseService.mapUserWithCase(jsonString, newCase.getCaseId(), userIdTemp);
			String riskString = String.format("%.2f", directRisk * 100);
			return new ResponseEntity<>(new ResponseMessage("A new case with risk score : " + riskString + "% and ID: "
					+ newCase.getCaseId() + " is added to your incoming tray."), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage("Error creating case."), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation("Gets case seed list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseResult.class, message = "List action on case seeds is successfull"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Error in listing case seeds") })
	@GetMapping(value = "/listCases", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCaseseedList(@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String type, @RequestParam(required = false) Integer status,
			@RequestParam(required = false) Integer priority, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		List<CaseDto> bstCaseSeed = caseService.getAllCasesByUser(userIdTemp, pageNumber, recordsPerPage, type, status,
				priority, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = caseService.countAllCasesByUser(userIdTemp, type, status, priority);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(bstCaseSeed != null ? bstCaseSeed.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		CaseResult caseSeedResult = new CaseResult();
		caseSeedResult.setPaginationInformation(information);
		caseSeedResult.setResult(bstCaseSeed);
		return new ResponseEntity<>(caseSeedResult, HttpStatus.OK);

	}

	@ApiOperation("Performs full text search action on case seeds")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseResult.class, message = "Search action on case seed is successfull"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Error in searching case seeds") })
	@GetMapping(value = "/fullTextSearch", produces = { "application/json; charset=UTF-8" })
	public @ResponseBody ResponseEntity<?> fullTextSearchForCaseSeed(@RequestParam("token") String token,
			@RequestParam String caseSearchKeyword, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String creationDate,
			@RequestParam(required = false) String modifiedDate, @RequestParam(required = false) String type,
			@RequestParam(required = false) Integer status, @RequestParam(required = false) Integer priority,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) throws ParseException {

		if (StringUtils.isEmpty(caseSearchKeyword) || caseSearchKeyword.length() < 5) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_KEYWORD_VALIDATION_MSG),
					HttpStatus.OK);
		}

		Long tempUserId = getCurrentUserId();
		List<CaseDto> bstCaseSeeds = caseService.fullTextSearchForCaseSeed(caseSearchKeyword, pageNumber,
				recordsPerPage, creationDate, modifiedDate, tempUserId, type, status, priority, orderBy, orderIn);

		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = caseService.countFullTextSearchForCaseSeed(caseSearchKeyword, tempUserId, creationDate,
				modifiedDate, type, status, priority);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(bstCaseSeeds != null ? bstCaseSeeds.size() : 0);
		information.setIndex(index);
		int nextIndex = 0;
		if(bstCaseSeeds != null)
			nextIndex = (int) (bstCaseSeeds.size() - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		CaseResult caseSeedResult = new CaseResult();
		caseSeedResult.setPaginationInformation(information);
		caseSeedResult.setResult(bstCaseSeeds);
		return new ResponseEntity<>(caseSeedResult, HttpStatus.OK);
	}

	// Case document management end points

	@ApiOperation("Uploads the case related document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocumentUploadResponse.class, message = "Case document is uploaded successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.CASE_SEED_NOT_FOUND),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "No enough permissions") })
	@PostMapping(value = "/uploadDocument", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadDocumentDetails(@RequestBody MultipartFile uploadDoc, @RequestParam Long caseId,
			@RequestParam String fileTitle, @RequestParam String remarks, @RequestParam("token") String token,
			@RequestParam(required = false) Integer docFlag, HttpServletRequest request) throws Exception {

		if (docFlag != null && (docFlag < 0 || docFlag > 255)) {
			throw new BadRequestException(ElementConstants.INVALID_DOC_FLAG_MSG);
		}
		Long docId = null;
		Long userIdTemp = getCurrentUserId();
		caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
		CaseDocumentUploadResponse caseDocumentUploadResponse=new CaseDocumentUploadResponse();
		docId = caseDocumentService.uploadCaseDocument(uploadDoc, caseId, fileTitle, remarks, userIdTemp, docFlag);
		caseDocumentUploadResponse.setDocId(docId);
		/*JSONObject json = new JSONObject();
		json.put("docId", docId);*/
		return new ResponseEntity<>(caseDocumentUploadResponse, HttpStatus.OK);
	}

	@ApiOperation("Updates the case related document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_METADATA_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No permissions for updating case document"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "case document not found"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@PostMapping(value = "/updateDocument", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> upDateDocumentDetails(@RequestParam Long docId, @RequestParam String fileTitle,
			@RequestParam String remarks, @RequestParam("token") String token, HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		Long caseId = caseDocumentService.getCaseIdFromDoc(docId);
		if (caseId != null) {
			caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
			caseDocumentService.updateCaseDocument(fileTitle, docId, remarks, userIdTemp);
			return new ResponseEntity<>(
					new ResponseMessage(ElementConstants.DOCUMENT_METADATA_UPDATED_SUCCESSFULLY_MSG), HttpStatus.OK);
		} else {
			throw new PermissionDeniedException();
		}
	}

	@ApiOperation("Deletes the case related document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No permissions for deleting the case document") })
	@DeleteMapping(value = "/deleteDocument", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteCaseSeedDocument(@RequestParam Long docId, @RequestParam("token") String token,
			HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		Long caseId = caseDocumentService.getCaseIdFromDoc(docId);
		if (caseId != null) {
			caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
			caseDocumentService.deleteCaseDocumentById(docId, userIdTemp);
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
					HttpStatus.OK);
		} else {
			throw new PermissionDeniedException();
		}
	}

	@ApiOperation("Downloads the case related document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Case document is downloaded successfully"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@GetMapping(value = "/downloadDocument", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<StreamingResponseBody> downloadDocument(HttpServletResponse response,
			@RequestParam("docId") Long docId, @RequestParam("token") String token, HttpServletRequest request)
			throws Exception {

		Long userIdTemp = getCurrentUserId();
		Long caseId = caseDocumentService.getCaseIdFromDoc(docId);
		if (caseId != null) {
			caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
			CaseDocDetails docDetails = caseDocumentService.find(docId);
			if (docDetails != null) {
				byte[] fileData = caseDocumentService.downloadCaseDocument(docDetails.getDocGuuidFromServer());
				ServiceCallHelper.setDownloadResponse(response, docDetails.getFilePath(), docDetails.getFileType(),
						docDetails.getFileSize() != null ? docDetails.getFileSize().intValue() : 0);
				StreamingResponseBody responseBody = new StreamingResponseBody() {
					@Override
					public void writeTo(OutputStream out) throws IOException {
						out.write(fileData);
						out.flush();
					}
				};
				return new ResponseEntity<>(responseBody, HttpStatus.OK);
			} else {
				throw new DocNotFoundException();
			}
		} else {
			throw new PermissionDeniedException();
		}
	}

	@ApiOperation("Gets case related documents")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocumentsDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = CaseDocumentsDto.class, message = "Case document not found"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 500, response = CaseDocumentsDto.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Operation failed") })
	@GetMapping(value = "/getCaseDocuments", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAllDocumentsForCaseSeed(@RequestParam Long caseId,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber, @RequestParam("token") String token,
			@RequestParam(required = false) Integer docFlag, HttpServletRequest request) {

		Long userIdTemp = getCurrentUserId();
		caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
		docFlag = docFlag != null ? docFlag : 0;
		List<CaseDocDetailsDTO> docList = caseDocumentService.getAllDocumentsForCase(caseId, docFlag, orderBy,
				pageNumber, recordsPerPage);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = caseDocumentService.countAllDocumentsForCase(caseId, docFlag);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(docList != null ? docList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject allDocumentResult = new JSONObject();
		allDocumentResult.put("result", docList);
		allDocumentResult.put("paginationInformation", information);*/
		CaseDocumentsDto caseDocumentsDto= new CaseDocumentsDto();
		caseDocumentsDto.setResult(docList);
		caseDocumentsDto.setPaginationInformation(information);
		return new ResponseEntity<>(caseDocumentsDto, HttpStatus.OK);
	}

	@ApiOperation("Gets case document details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocDetailsDTO.class, message = "Operation successful"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Operation failed") })
	@GetMapping(value = "/getDocumentDetails", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDocumentDetails(@RequestParam Long docId, @RequestParam("token") String token,
			HttpServletRequest request) {
		Long userIdTemp = getCurrentUserId();
		Long caseId = caseDocumentService.getCaseIdFromDoc(docId);
		if (caseId != null) {
			caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
			CaseDocDetailsDTO bstDocDetails = caseDocumentService.getCaseDocumentDetailsById(docId);
			return new ResponseEntity<>(bstDocDetails, HttpStatus.OK);
		} else {
			throw new PermissionDeniedException();
		}
	}

	@ApiOperation("Gets document aggregates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = DocAggregatorDto.class, responseContainer = "List",message = "Operation successful"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Operation failed") })
	@GetMapping(value = "/docAggregator", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> docAggregator(@RequestParam Long caseId, @RequestParam("token") String token,
			@RequestParam(required = false) String fromDate, @RequestParam(required = false) String toDate,
			HttpServletRequest request) throws ParseException {
		Long userId = getCurrentUserId();
		if (caseId != null) {
			caseService.isCaseAccessibleOrOwner(caseId, userId);
			List<DocAggregatorDto> docAggregatorDtoList = caseDocumentService.docAggregator(caseId, fromDate, toDate);
			return new ResponseEntity<>(docAggregatorDtoList, HttpStatus.OK);
		} else {
			throw new PermissionDeniedException();
		}
	}

	@ApiOperation("Updates case document content")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Case document is updated successfully"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Error in updating case document") })
	@PostMapping(value = "/updateDocumentContent", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<?> updateDocumentContent(@RequestBody MultipartFile uploadDoc,
			@RequestParam("docId") Long docId, @RequestParam("token") String token, HttpServletRequest request)
			throws IOException {

		Long userIdTemp = getCurrentUserId();
		Long caseId = caseDocumentService.getCaseIdFromDoc(docId);
		if (caseId != null) {
			caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
			caseDocumentService.updateDocumentContent(uploadDoc, userIdTemp, docId);
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_UPDATED_SUCCESSFULLY_MSG),
					HttpStatus.OK);
		} else {
			throw new PermissionDeniedException();
		}
	}

	@ApiOperation("Finds case by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDto.class, message = "Operation successful"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "No enough permissions") })
	@GetMapping(value = "/getCaseById", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> findCaseById(@RequestParam Long caseId, @RequestParam("token") String token,
			HttpServletRequest request) {

		long userIdTemp = getCurrentUserId();
		caseService.isCaseAccessibleOrOwner(caseId, userIdTemp);
		Case caseSeed = caseService.find(caseId);
		CaseDto caseDto = new CaseDto();
		BeanUtils.copyProperties(caseSeed, caseDto);
		return new ResponseEntity<>(caseDto, HttpStatus.OK);
	}

	@ApiOperation("Uploads Json file to database(TEST api)")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.CASE_SEED_NOT_FOUND),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process your request, please provide mandatory data"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not find analyst for given case or analyst has rejected the case"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Operation failed") })
	@PostMapping(value = "/uploadSeed2", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadJsonFileToDatabaseTest(@RequestBody MultipartFile uploadFile,
			@RequestParam("caseId") Long caseId, @RequestParam(required = false) Long analystId,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {

		Long userIdTemp = getCurrentUserId();
		caseService.mapUserWithCaseTest(uploadFile, caseId, userIdTemp, analystId);
		return new ResponseEntity<>(new ResponseMessage("Uploaded json file and linked case to user."), HttpStatus.OK);
	}
	
	
	
	@ApiOperation("Downloads the Annual return document")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Annual return document is downloaded successfully"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Could not process request, caseId or userId cannot be null"),
			@ApiResponse(code = 403, response = ResponseMessage.class, message = "No enough permissions"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found"),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG)})
	@GetMapping(value = "/downloadAnnualReturnsDocument", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<StreamingResponseBody> downloadAnnualReturnsDocument(HttpServletResponse response,
			@RequestParam("url") String url, @RequestParam("token") String token, HttpServletRequest request)
			throws Exception {
				byte[] fileData = caseDocumentService.downloadAnnualReturnsDocument(url);
				StreamingResponseBody responseBody = new StreamingResponseBody() {
					@Override
					public void writeTo(OutputStream out) throws IOException {
						out.write(fileData);
						out.flush();
					}
				};
				request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
				return new ResponseEntity<>(responseBody, HttpStatus.OK);
	}
	
	@ApiOperation("Creates a case from workflow")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CreateCaseResponseDto.class, message = "Case created successfully"),
			@ApiResponse(code = 400, response = String.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/createCaseFromWorkFlow", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> createCaseFromWorkFlow(@RequestParam("token") String token,@Valid @RequestBody CaseDto createCaseWorkFlowRequestDto, BindingResult results,
			 HttpServletRequest request) throws Exception {
		if (!results.hasErrors()) {		
			Users user=elementsSecurityService.getUserUserFromToken(token);
			Case bstCaseSeed=new Case();
			BeanUtils.copyProperties(createCaseWorkFlowRequestDto, bstCaseSeed);	
			bstCaseSeed.setCreatedOn(new Date());
			bstCaseSeed.setIsLeadGeneration(true);
			//bstCaseSeed.setCreatedBy(userTemp);
			bstCaseSeed.setCurrentStatus(StatusEnum.FRESH.getIndex());
			Case caseSeed = caseService.save(bstCaseSeed);
			//Users user = usersService.getUserObjectByEmailOrUsername("admin");
			if(user != null)
				caseSeed.setCreatedBy(user);
			caseSeed.setCurrentStatus(StatusEnum.SUBMITTED.getIndex());
			caseService.saveOrUpdate(caseSeed);	
			/*Users analyst = null;
			if (user !=null && groupService.doesUserbelongtoGroup(ElementConstants.ANALYST_GROUP_NAME, user.getUserId())) {
				caseService.assignCaseToAnalyst(user, caseSeed);
			}else{
				analyst = caseService.getNextAnalyst(caseSeed.getCaseId());
				caseService.assignCaseToAnalyst(analyst, caseSeed);
			}*/
			JSONObject jsonString= new JSONObject(createCaseWorkFlowRequestDto);
			caseService.mapUserWithCase(jsonString.toString(), caseSeed.getCaseId(), user.getUserId());
			eventPublisher.publishEvent(
					new LoggingEvent(caseSeed.getCaseId(), LoggingEventType.CREATE_CASE, user.getUserId(), new Date()));
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}}