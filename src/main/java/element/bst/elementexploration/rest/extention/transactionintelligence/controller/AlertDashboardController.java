package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonReturnObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertResponseSendDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AssociatedEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyGraphDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FetchTransactionsByTypeListViewAllResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAndAlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsBetweenDatesResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsCountAndRatioResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataNotifDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxsListAggRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Alert dashboard API" },description="Manages all alerted transactions data")
@RestController
@RequestMapping(value = "/api/dashboard")
public class AlertDashboardController extends BaseController {

	@Autowired
	AlertService alertService;

	@Autowired
	TxDataService txDataService;

	/**
	 * api for groupBy specific things
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @param productType
	 * @return
	 */
	@ApiOperation("Gets the transactions between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = TransactionsBetweenDatesResponseDto.class, message = "Fetched transactions Successfully."),
			@ApiResponse(code = 417, response = ResponseMessage.class, message = "Server was unable to properly complete the request.")})	
	@RequestMapping(value = "/fetchTxsBwDates/{date-from}/{date-to}", method = RequestMethod.POST)
	public ResponseEntity<?> fetchTxsBwDates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String productType,
			@RequestBody(required=false) FilterDto filterDto) {
		try {
			/*JSONObject jsonObject = new JSONObject();
			jsonObject.put("txsAggRespDto", txDataService.fetchTxsBwDates(fromDate, toDate, productType,filterDto));*/
			TransactionsBetweenDatesResponseDto transactionsBetweenDatesResponseDto=new TransactionsBetweenDatesResponseDto();
			transactionsBetweenDatesResponseDto.setTxsAggRespDto(txDataService.fetchTxsBwDates(fromDate, toDate, productType,filterDto));
			return new ResponseEntity<>(transactionsBetweenDatesResponseDto, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Api for list of txs and alert count,ratio
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @param productType
	 * @param recordsPerPage
	 * @param pageNumber
	 * @param request
	 * @return
	 */
	@ApiOperation("Gets the transactions count and ration between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = TransactionsCountAndRatioResponseDto.class, message = "Operation successful."),
			@ApiResponse(code = 417, response = ResponseMessage.class, message = "Server was unable to properly complete the request.")})	
	@RequestMapping(value = "/fetchTxsCountAndRatio/{date-from}/{date-to}", method = RequestMethod.POST)
	public ResponseEntity<?> fetchTxsTotalListBwDates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) String productType, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) boolean input,
			@RequestBody(required=false) FilterDto filterDto,
			@RequestParam(required = false) boolean output, HttpServletRequest request) {
		try {
			//JSONObject jsonObject = new JSONObject();
			TxsListAggRespDto txListAgg = txDataService.fetchTxsTotalListBwDates(fromDate, toDate, productType,
					recordsPerPage, pageNumber, input, output,filterDto);
			List<TransactionsDataNotifDto> list = txListAgg.getTransactionsDataList();
			//jsonObject.put("transactionsListAlertCountRatioDto", txListAgg);
			// Pagination
			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = txDataService.fetchTxsBwDatesCustomizedCount(fromDate, toDate, null, input, output,filterDto);
			PaginationInformation information = new PaginationInformation();
			information.setTotalResults(totalResults);
			information.setTitle(request.getRequestURI());
			information.setKind("list");
			information.setCount(list != null ? list.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");
			//jsonObject.put("pagination", information);			
			//return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			TransactionsCountAndRatioResponseDto transactionsCountAndRatioResponseDto=new TransactionsCountAndRatioResponseDto();
			transactionsCountAndRatioResponseDto.setTransactionsListAlertCountRatioDto(txListAgg);
			transactionsCountAndRatioResponseDto.setPagination(information);
			return new ResponseEntity<>(transactionsCountAndRatioResponseDto, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * api for alerts (notifications tab)
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @param groupBy
	 * @param recordsPerPage
	 * @param pageNumber
	 * @param name
	 * @param orderIn
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the alerts between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Fetched alerts Successfully.")})	
	@PostMapping(value = "/fetchAlertsBwDates/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertsBetweenDates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, 
			@RequestParam("token") String token,
			@RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String name, 
			@RequestParam(required = false) String orderIn,
			@RequestBody(required=false) FilterDto filterDto,
			@RequestParam(required=false) boolean isTimeStamp,
			HttpServletRequest request) throws ParseException {

		List<AlertDashBoardRespDto> list = txDataService.getAlertsBetweenDates(fromDate, toDate, name, pageNumber,
				recordsPerPage, orderIn,filterDto,isTimeStamp);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = txDataService.getAlertCountBetweenDates(fromDate, toDate, name,filterDto,isTimeStamp);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", list);
		jsonObject.put("paginationInformation", information);*/
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setPaginationInformation(information);
		responseUtil.setResult(list);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);

	}
	
	@GetMapping(value = "/fetchAlerts", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fetchAlerts(HttpServletRequest request) throws ParseException {

		List<AlertDashBoardRespDto> list = txDataService.fetchAlerts();
		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setResult(list);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}
	
	
	
	@ApiOperation("Gets the alerts by customer number")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertDashBoardRespDto.class,responseContainer = "List", message = "Fetched alerts Successfully.")})	
	@GetMapping(value = "/fetchAlertsByNumber", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fetchAlertsByNumber(@RequestParam("token") String token,@RequestParam(required = false) String customerNumber,
			HttpServletRequest request){
		return new ResponseEntity<>(txDataService.fetchAlertsByNumber(customerNumber),HttpStatus.OK);
	}

	/**
	 * api for amount,alert aggregation based on productType
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the total amount and alert counts by product type of transactions between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = TransactionAmountAndAlertCountDto.class,responseContainer = "List", message = "Operation successful.")})	
	@PostMapping(value = "/getTotalAmountAndAlertCountByProductType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTotalAmountAndAlertCountByProductType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = false) boolean groupBy,
			@RequestParam(required = false) boolean input,
			@RequestParam(required = false) boolean output,@RequestParam(required = false) String type,@RequestBody(required=false) FilterDto filterDto)
					throws ParseException {

		return new ResponseEntity<>(txDataService.getTotalAmountAndAlertCountByProductType(fromDate, toDate, groupBy,input,output,type,filterDto),
				HttpStatus.OK);

	}

	/**
	 * api for transactions and alerts graph
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @param granularity
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the transactions and alerts aggregates between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = TransactionAndAlertAggregator.class, message = "Operation successful.")})	
	@PostMapping(value = "/getTransactionAndAlertAggregates/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTransactionAndAlertAggregates(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam("granularity") String granularity, @RequestParam(required = false) boolean input,
			@RequestParam(required = false) boolean output,@RequestBody(required=false) FilterDto filterDto,
			@RequestParam(required = false) String type)
			throws ParseException {
		return new ResponseEntity<>(txDataService.getTransactionAndAlertAggregates(fromDate, toDate, granularity, false,
				input, output, type,filterDto), HttpStatus.OK);

	}

	/**
	 * api for product type graph
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @param granularity
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the list of transactions mapped with their alerts aggregates by product type between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = TransactionAndAlertAggregator.class,responseContainer = "Map", message = "Operation successful.")})	
	@PostMapping(value = "/getTransactionAndAlertAggregatesByProductType/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTransactionAndAlertAggregatesByProducType(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam("granularity") String granularity, @RequestParam(required = false) boolean input,
			@RequestParam(required = false) boolean output, @RequestParam(required = false) String type,@RequestBody(required=false) FilterDto filterDto)
			throws ParseException {

		return new ResponseEntity<>(txDataService.getTransactionAndAlertAggregatesByProducType(fromDate, toDate,
				granularity, true, input, output, type,filterDto), HttpStatus.OK);
	}

	/**
	 * fetching monthly turn over
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @param granularity
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the monthly turn over of transactions between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = MonthlyTurnOverDto.class, message = "Operation successful.")})	
	@PostMapping(value = "/getMonthlyTurnOver/{date-from}/{date-to}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getMonthlyTurnOver(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestBody(required=false) FilterDto filterDto) throws ParseException {

		return new ResponseEntity<>(alertService.getMonthlyTurnOver(fromDate, toDate,filterDto), HttpStatus.OK);
	}

	@ApiOperation("Gets the list of transactions aggregator between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = FetchTransactionsByTypeListViewAllResponseDto.class, message = "Operation successful."),
			@ApiResponse(code = 417, response = ResponseMessage.class, message = "Server was unable to properly complete the request.")})	
	@RequestMapping(value = "/fetchTxByTypeListViewAll/{date-from}/{date-to}", method = RequestMethod.POST)
	public ResponseEntity<?> fetchTxByTypeListViewAll(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,
			@RequestParam(required = true) String productType, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) boolean input,
			@RequestParam(required = false) boolean output,@RequestBody(required=false) FilterDto filterDto
			,HttpServletRequest request) {
		try {
			//JSONObject jsonObject = new JSONObject();
			TxsListAggRespDto txListAgg = txDataService.fetchTxByTypeListViewAll(fromDate, toDate, productType,
					recordsPerPage, pageNumber, input, output,filterDto);
			List<TransactionsDataNotifDto> list = txListAgg.getTransactionsDataList();
			//jsonObject.put("fetchTxByTypeListViewAll", txListAgg);

			// Pagination
			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = txDataService.fetchTxsBwDatesCustomizedCount(fromDate, toDate, productType, input,
					output,filterDto);
			PaginationInformation information = new PaginationInformation();
			information.setTotalResults(totalResults);
			information.setTitle(request.getRequestURI());
			information.setKind("list");
			information.setCount(list != null ? list.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");
			//jsonObject.put("pagination", information);
			FetchTransactionsByTypeListViewAllResponseDto fetchTransactionsByTypeListViewAllResponseDto=new FetchTransactionsByTypeListViewAllResponseDto();
			fetchTransactionsByTypeListViewAllResponseDto.setFetchTxByTypeListViewAll(txListAgg);
			fetchTransactionsByTypeListViewAllResponseDto.setPagination(information);
			return new ResponseEntity<>(fetchTransactionsByTypeListViewAllResponseDto, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * api for fetching associated entities
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the list of associated entities between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AssociatedEntityRespDto.class,responseContainer = "List", message = "Operation successful.")})
	@PostMapping(value = "/fetchAssociatedEntity/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> fetchAssociatedEntity(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.fetchAssociatedEntity(fromDate, toDate,filterDto), HttpStatus.OK);
	}

	/**
	 * api for alert comparison in notifications tab
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the list of alerts comparison between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertComparisonReturnObject.class,responseContainer = "map", message = "Operation successful.")})
	@PostMapping(value = "/alertComparisonNotification/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> alertComparisonNotification(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(alertService.alertComparisonNotification(fromDate, toDate,filterDto), HttpStatus.OK);
	}

	/**
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the list of alerts comparison transactions between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertComparisonReturnObject.class,responseContainer = "Map", message = "Operation successful.")})
	@PostMapping(value = "/alertComparisonTransactions/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> alertComparisonTransactions(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(txDataService.alertComparisonTransactions(fromDate, toDate,filterDto), HttpStatus.OK);
	}

	/**
	 * api for alert comparison in transactions tab
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param token
	 * @return
	 * @throws ParseException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@ApiOperation("Gets the counter parties locations of transactions between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CounterPartyGraphDto.class, message = "Operation successful.")})
	@PostMapping(value = "/counterPartyLocationsPlot/{date-from}/{date-to}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> counterPartyLocationsPlot(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto,
			@RequestParam(value = "type", required = false) String type)
			throws ParseException, IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(txDataService.counterPartyLocationsPlot(fromDate, toDate, type,filterDto), HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation("Gets the alert scenario by alert id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertScenarioDto.class, message = "Fetched the alert scenario Successfully.")})
	@GetMapping(value = "/getAlertScenarioById", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertScenarioById(@RequestParam("alertId") long id, @RequestParam("token") String token)
			throws ParseException {
		return new ResponseEntity<>(alertService.getAlertScenarioById(id), HttpStatus.OK);
	}
	@ApiOperation("Gets the list of alerts by customer id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertResponseSendDto.class,responseContainer = "List", message = "Fetched alerts Successfully.")})
	@PostMapping(value = "/getAlertsForCustomer/{date-from}/{date-to}/{customerNumber}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getAlertsForCustomer(@PathVariable("date-from") String fromDate,
			@PathVariable("date-to") String toDate, @PathVariable("customerNumber") String customerNumber,
			@RequestParam("token") String token,@RequestBody(required=false) FilterDto filterDto) throws ParseException {
		return new ResponseEntity<>(alertService.getAlertsForCustomer(fromDate, toDate, customerNumber), HttpStatus.OK);
	}
	
	
	
	@ApiOperation("Gets the alerts between two dates")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseUtil.class, message = "Fetched alerts Successfully.")})	
	@GetMapping(value = "/getEntityBasedAlerts", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntityBasedAlerts(@RequestParam(required = false) String fromDate,
			@RequestParam(required = false) String toDate, 
			@RequestParam(required = false) String token,
			@RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) String orderIn,
			@RequestParam("entityType") String entityType,
			HttpServletRequest request) throws ParseException {

		List<AlertDashBoardRespDto> list = txDataService.getEntityBasedAlerts(fromDate, toDate,  pageNumber,
				recordsPerPage, orderIn,entityType);
		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = txDataService.getEntityBasedAlertsCount(fromDate, toDate,entityType);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(list != null ? list.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");

		ResponseUtil responseUtil=new ResponseUtil();
		responseUtil.setPaginationInformation(information);
		responseUtil.setResult(list);
		return new ResponseEntity<>(responseUtil, HttpStatus.OK);
	}
	

	@ApiOperation("Update Fetch Alerts")
	@ApiResponse(code = 200, response = AlertDashBoardRespDto.class, message = "Alerts updated Successfully.")
	@PostMapping(value = "/updateFetchAlerts", produces = { "application/json; charset=UTF-8" }, consumes = {
 			"application/json; charset=UTF-8" })
 	public ResponseEntity<?> updateFetchTransactions(@RequestBody AlertDashBoardRespDto alertDashBoardRespDto,
 			@RequestParam("token") String token) {
 		boolean isUpdated = false;
 		try {
			isUpdated = txDataService.updateFetchAlerts(alertDashBoardRespDto, getCurrentUserId());
 			return new ResponseEntity<>(isUpdated, HttpStatus.OK);
 		} catch (Exception e) {
 			return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
 		}
	}
	

}
