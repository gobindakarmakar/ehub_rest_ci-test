package element.bst.elementexploration.rest.extention.transactionintelligence.enums;

/**
 * @author suresh
 *
 */
public enum CustomerTitle {

	MR, MS, MRS ,NONE, MISS, MDM, DR

}
