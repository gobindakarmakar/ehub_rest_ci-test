package element.bst.elementexploration.rest.extention.alert.feedmanagement.service;

import java.util.List;

import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedGroupsDto;
import element.bst.elementexploration.rest.extention.alert.feedmanagement.dto.FeedManagementDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.FeedUsersDto;
import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.util.RequestFilterDto;

/**
 * @author hanuman
 *
 */
public interface FeedManagementService extends GenericService<FeedManagement, Long>{

	

	List<FeedManagementDto> getAllFeedItems(RequestFilterDto filterDto, Boolean isAllRequired, Integer pageNumber, Integer recordsPerPage, String orderIn, String orderBy);	
	FeedManagementDto getFeedItemByID(Long feedManagementId);
	
	FeedManagementDto getFeedManagementByFeedName(String feedName);
	
	FeedManagementDto saveOrUpdateFeedItem(FeedManagementDto feedManagmentDto,long userIdTemp) throws Exception;
	
	boolean deleteFeedItem(Long feedID, long userIdTemp);

	List<FeedGroupsDto> getGroupLevelByFeedId(Long feedManagementID);

	Long getFeedsCount();

	List<FeedUsersDto> getUsersByFeed(Long feedManagementID, boolean isAllRequired);
	
	FeedManagementDto getFeedByTypeAndSource(Long typeId,Long sourceId);
	
	List<FeedGroups> getFeedGroupsByGroupId(List<Long> groupIds);
	List<FeedManagement> getFeedsByTypeIds(List<Long> classificationIds);
	List<FeedManagementDto> getFeedsByUserId(Long userId);
	List<FeedManagement> getFeedsByFeedIds(List<Long> feedIds);
	void updateFeedGroupsSupportingFeed(Long feedManagementid , List<FeedGroups> feedGroupsList);

}
