package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.runtime.BatchExecutionCommandImpl;
import org.json.JSONObject;
import org.kie.api.KieServices;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CountryDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerRelationDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.LuxuryShopsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerAddress;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerRelationShipDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.LuxuryShops;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregateVo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonReturnObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDtoAscnd;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertNode;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertResponseSendDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatisticsRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerNode;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.GraphEdge;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.GraphResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.LuxuryDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionNode;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionsDataDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByDate;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByLocationDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxByType;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.WebsiteClass;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.search.service.SearchHistoryService;
import element.bst.elementexploration.rest.util.DaysCalculationUtility;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * @author suresh
 *
 */
@Service("alertService")
@Transactional("transactionManager")
public class AlertServiceImpl extends GenericServiceImpl<Alert, Long> implements AlertService {

	@Value("${site_info_add_keyword}")
	private String SITE_INFO_ADD_KEYWORD;

	@Autowired
	CustomerDetailsDao customerDetailsDao;

	@Autowired
	AccountDao accountDao;

	@Autowired
	TxDataDao txDataDao;

	@Autowired
	AlertDao alertDao;

	@Autowired
	CountryDao countryDao;

	@Autowired
	DaysCalculationUtility daysCalulationUtility;

	@Autowired
	CustomerRelationDao customerRelationDao;

	@Autowired
	SearchHistoryService searchHistoryService;

	@Autowired
	LuxuryShopsDao luxuryShopsDao;

	@Value("${kie_server_url}")
	private String KIE_SERVER_URL;

	@Value("${kie_server_username}")
	private String KIE_SERVER_USERNAME;

	@Value("${kie_server_password}")
	private String KIE_SERVER_PASSWORD;

	@Value("${alert_container_id}")
	private String ALERT_CONTAINER;

	@Autowired
	public AlertServiceImpl(GenericDao<Alert, Long> genericDao) {
		super(genericDao);
	}

	@Override
	public List<AlertResponseSendDto> fetchAlerts(MultipartFile file, long userId)
			throws IOException, ParseException, IllegalAccessException, InvocationTargetException {

		BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
		String line = null;
		int i = 1;
		List<AlertResponseSendDto> responseDtos = new ArrayList<AlertResponseSendDto>();

		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if (line.contains(","))
					data = line.split(",");
				if (line.contains(";"))
					data = line.split(";");
				Alert alert = new Alert();
				if (data != null && data[0] != null)
					alert.setAge(Integer.parseInt(data[0]));
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				Date date = formatter.parse(data[1]);
				alert.setAlertBusinessDate(date);
				alert.setAlertStatus(AlertStatus.valueOf(data[2]));
				alert.setComment(data[3]);
				alert.setFocalNtityDisplayId(data[4]);
				alert.setFocalNtityDisplayName(data[5]);
				alert.setJurisdiction(data[6]);
				alert.setScenario(data[7]);
				alert.setSearchName(data[8]);
				alert.setStatusCd(data[9]);
				alert.setTransactionId(Long.valueOf(data[10]));
				alert.setTransactionProductType(data[11]);
				alert.setRowStatus(false);
				alert.setUserId(userId);
				alert.setAlertType("external");
				alertDao.create(alert);
			}
			i++;
		}

		return responseDtos;

	}

	@Override
	public List<AlertResponseSendDto> fetchNonResolvedAlerts(Integer pageNumber, Integer recordsPerPage) {
		List<AlertResponseSendDto> responseDtos = new ArrayList<AlertResponseSendDto>();
		List<Alert> alerts = alertDao.fetchNonResolvedAlerts(
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
		for (Alert alert : alerts) {
			AlertResponseSendDto dto = new AlertResponseSendDto();
			try {
				CustomerDetails customerDetails = customerDetailsDao
						.fetchCustomerDetails(alert.getFocalNtityDisplayId());
				if (customerDetails != null) {
					String value = customerDetails.getCustomerType().toString();
					if (value.equals((CustomerType.CORP).name())) {
						dto.setAccountType("company");
					} else if (value.equals((CustomerType.IND).name())) {
						dto.setAccountType("single");
					} else {
						dto.setAccountType(customerDetails.getCustomerType().name());
					}
				}

				BeanUtils.copyProperties(dto, alert);
			} catch (IllegalAccessException | InvocationTargetException e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			}

			responseDtos.add(dto);

		}

		return responseDtos;
	}

	@Override
	public boolean resolveAlert(Long alertId, String comment) {
		try {
			// Alert alert = transactionDao.fetchAlertSummary(alertId);
			Alert alertToResolve = alertDao.loadAlertById(alertId);
			alertToResolve.setRowStatus(true);
			alertToResolve.setAlertStatus(AlertStatus.CLOSE);
			alertToResolve.setComment(comment);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public AlertResponseSendDto fetchAlertSummary(long alertId) {
		AlertResponseSendDto dto = new AlertResponseSendDto();
		Alert alert = alertDao.fetchAlertSummary(alertId);
		try {
			BeanUtils.copyProperties(dto, alert);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return dto;
	}

	@Override
	public AlertStatisticsRespDto alertStatistics(Long entityId)
			throws IllegalAccessException, InvocationTargetException {
		AlertStatisticsRespDto alertStatisticsRespDto = new AlertStatisticsRespDto();

		List<TxByType> byTypes = txDataDao.getTxByType(entityId);

		List<TxByDate> byDate = txDataDao.getTxByDate(entityId);

		// List<TxByLocationDto> byLocationDtos = txDataDao.getTxByLoc(entityId);

		List<String> originators = new ArrayList<String>();
		List<String> beneficiaries = new ArrayList<String>();

		Alert alert = alertDao.find(entityId);
		if (alert != null) {
			CustomerDetails customerDetails = customerDetailsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId());

			if (customerDetails != null) {
				List<Account> accountlist = accountDao.fetchAccounts(customerDetails.getCustomerNumber());

				if (!accountlist.isEmpty()) {
					for (Account account : accountlist) {
						List<TransactionsData> data = txDataDao.fetchTransaction(account.getAccountNumber());

						for (TransactionsData transactionsData : data) {
							if (transactionsData.getBeneficiaryAccountId() != null)
								beneficiaries.add(transactionsData.getBeneficiaryAccountId());
							if (transactionsData.getOriginatorAccountId() != null)
								originators.add(transactionsData.getOriginatorAccountId());

						} // copy foreach close
					} // accoun foreach close
				} // account list empty if close

			} // customer details if close
		}

		List<String> partyCustomerIdentifier = new ArrayList<String>();

		if (!originators.isEmpty()) {
			Set<String> hs = new HashSet<>();
			hs.addAll(originators);
			originators.clear();
			originators.addAll(hs);

			for (String accNo : originators) {
				Account account = accountDao.fetchAccount(accNo);
				if (account != null) {
					partyCustomerIdentifier.add(account.getPrimaryCustomerIdentifier());
				}
			}
		}

		if (!beneficiaries.isEmpty()) {
			Set<String> hs = new HashSet<>();
			hs.addAll(beneficiaries);
			beneficiaries.clear();
			beneficiaries.addAll(hs);

			for (String accNo : beneficiaries) {
				Account account = accountDao.fetchAccount(accNo);
				if (account != null) {
					partyCustomerIdentifier.add(account.getPrimaryCustomerIdentifier());
				}

			}

		}

		// removing duplicate from partyIdentifiers
		Set<String> hasSet = new HashSet<>();
		hasSet.addAll(partyCustomerIdentifier);
		partyCustomerIdentifier.clear();
		partyCustomerIdentifier.addAll(hasSet);

		List<TxByLocationDto> txByLocationDtos = txDataDao.getTxByLoc(partyCustomerIdentifier);

		alertStatisticsRespDto.setTxLoc(txByLocationDtos);
		alertStatisticsRespDto.setTxDate(byDate);
		alertStatisticsRespDto.setTxType(byTypes);

		return alertStatisticsRespDto;
	}

	@Override
	public int countTotalAlerts(Long userId) {
		List<Alert> alerts = alertDao.fetchCount(userId);
		return alerts.size();
	}

	@Override
	public int fetchfilterNameCount(String name, String dateFrom, String dateTo, Integer pageNumber,
			Integer recordsPerPage) {
		return alertDao.fetchfilterNameCount(name, dateFrom, dateTo,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);
	}

	@Override
	public List<AlertResponseSendDto> fetchfilterName(String name, String datefrom, String dateto, Integer pageNumber,
			Integer recordsPerPage) throws ParseException {

		List<Alert> alerts = alertDao.fetchfilterName(name, datefrom, dateto,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage);

		List<AlertResponseSendDto> responseDtos = new ArrayList<AlertResponseSendDto>();
		for (Alert alert : alerts) {
			AlertResponseSendDto dto = new AlertResponseSendDto();
			try {
				CustomerDetails customerDetails = customerDetailsDao
						.fetchCustomerDetails(alert.getFocalNtityDisplayId());
				if (customerDetails != null) {
					String value = customerDetails.getCustomerType().toString();
					if (value.equals((CustomerType.CORP).name())) {
						dto.setAccountType("company");
					} else if (value.equals((CustomerType.IND).name())) {
						dto.setAccountType("single");
					} else {
						dto.setAccountType(customerDetails.getCustomerType().name());
					}
				}

				BeanUtils.copyProperties(dto, alert);
			} catch (IllegalAccessException | InvocationTargetException e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			}

			responseDtos.add(dto);

		}

		return responseDtos;
	}

	/*
	 * @Override public GraphResponseDto generateGraph(Long alertId) {
	 * 
	 * Alert alert = alertDao.find(alertId); CustomerDetails customer =
	 * customerDetailsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId());
	 * GraphResponseDto graphResponseDto = new GraphResponseDto();
	 * generateAlertNode(alert, graphResponseDto); generateCustomerNode(customer,
	 * graphResponseDto, alertId); return graphResponseDto; }
	 */

	@Override
	public GraphResponseDto generateGraph(Long alertId, String transactionType, String locations, String minAmount) {
		Alert alert = alertDao.find(alertId);
		Date recordAge = getFromDateForRecords(alert.getAlertBusinessDate(), alert.getAge());
		CustomerDetails customer = customerDetailsDao.fetchCustomerDetails(alert.getFocalNtityDisplayId());
		GraphResponseDto graphResponseDto = new GraphResponseDto();
		generateAlertNode(alert, graphResponseDto);
		generateCustomerNode(customer, graphResponseDto, alertId, recordAge, transactionType, minAmount);
		return graphResponseDto;
	}

	@Override
	public GraphResponseDto expandCustomerNode(String customerId, Long alertId, String transactionType,
			String locations, String minAmount) {
		Alert alert = alertDao.find(alertId);
		Date recordAge = getFromDateForRecords(alert.getAlertBusinessDate(), alert.getAge());
		String id = customerId.substring(customerId.indexOf("-") + 1);
		CustomerDetails customer = customerDetailsDao.find(Long.parseLong(id));
		GraphResponseDto graphResponseDto = new GraphResponseDto();
		expandCustomerNode(customer, graphResponseDto, recordAge, transactionType, minAmount);
		return graphResponseDto;
	}

	private Date getFromDateForRecords(Date date, int age) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date != null ? date : new Date());
		cal.add(Calendar.DATE, -age != 0 ? age : 30);
		return cal.getTime();
	}

	public void generateAlertNode(Alert alert, GraphResponseDto graphResponseDto) {
		AlertNode alertNode = new AlertNode();
		alertNode.setDate(alert.getAlertBusinessDate());
		alertNode.setLabelV("alert");
		alertNode.setId("alert:" + alert.getId());
		alertNode.setName(alert.getAlertId());
		alertNode.setOid("alert-" + alert.getId());
		alertNode.setSearchName(alert.getSearchName());
		alertNode.setScenario(alert.getScenario());
		alertNode.setRiskScore(alert.getRiskScore());
		TransactionsData transactionsData = txDataDao.find(alert.getTransactionId());
		if (transactionsData != null) {
			alertNode.setLocation(transactionsData.getCountryOfTransaction());
			alertNode.setOriginatorName(transactionsData.getOriginatorName());
			alertNode.setBeneficiaryName(transactionsData.getBeneficiaryName());
			alertNode.setMerchant(transactionsData.getMerchant());
			alertNode.setMerchantWebsite(transactionsData.getMerchantWebsite());
			alertNode.setAmount(transactionsData.getAmount());
		}
		graphResponseDto.getVertices().add(alertNode);
	}

	public void generateCustomerNode(CustomerDetails customer, GraphResponseDto graphResponseDto, long alertId,
			Date recordAge, String transactionType, String minAmount) {
		CustomerAddress customerAddressNode = customer.getCustomerAddress() != null
				&& !customer.getCustomerAddress().isEmpty() ? customer.getCustomerAddress().get(0) : null;
		CustomerNode customerNode = new CustomerNode(customer.getResidentCountry(),
				customerAddressNode != null ? customerAddressNode.getAddressLine() : "", customer.getJurisdiction(),
				customer.getIndustry(), customer.getDisplayName(),
				customerAddressNode != null ? customerAddressNode.getCity() : "", customer.getDisplayName(),
				customer.getCustomerType().name(),
				customerAddressNode != null ? customerAddressNode.getAddressType().name() : "",
				customer.getCustomerTitle().name(), "cust-" + customer.getId(), customer.getCustomerNumber(),
				"cust:" + customer.getId(), "customer", null, null, customer.getCreatedOn());
		List<Account> accounts = accountDao.fetchAccounts(customer.getCustomerNumber());
		List<String> accountIds = accounts.stream().map(Account::getAccountNumber).collect(Collectors.toList());
		List<TransactionsData> accountTransactions = txDataDao.getTransactionsByAccountIds(accountIds, recordAge,
				transactionType, minAmount);
		graphResponseDto.getVertices().add(customerNode);
		graphResponseDto.getEdges().add(new GraphEdge(String.valueOf(customer.getId())+"a", "has_alert", "alert:" + alertId,
				"cust:" + customer.getId()));
		Map<Long, CustomerDetails> customerMap = new HashMap<>();
		Set<Long> customerIdList = new HashSet<>();
		int i=0;
		if (!accountTransactions.isEmpty()) {
			for (TransactionsData transaction : accountTransactions) {
				if (accountIds.contains(transaction.getBeneficiaryAccountId())) {
					TransactionNode transactionNode = new TransactionNode(transaction.getAmount(),
							transaction.getBusinessDate(), null, "tx-" + transaction.getId(),
							transaction.getTransProductType(), transaction.getTransactionChannel(), null, "credit",
							"acct-" + transaction.getBeneficiaryAccountId(), transaction.getCurrency(),
							transaction.getAmount());
					i=i+1;
					if (transaction.getOriginatorCutomerId() != null) {
						graphResponseDto.getEdges()
								.add(new GraphEdge(String.valueOf(customer.getId())+i, "tx_beneficiary",
										"cust:" + customer.getId(), "cust:" + transaction.getOriginatorCutomerId(),
										transactionNode));
						customerIdList.add(transaction.getOriginatorCutomerId());
					} else {
						String originatorCustomerAccountId = !StringUtils.isEmpty(transaction.getOriginatorAccountId())
								? transaction.getOriginatorAccountId()
								: UUID.randomUUID().toString();
								i=i+1;	
						graphResponseDto.getEdges().add(new GraphEdge(String.valueOf(customer.getId())+i, "tx_beneficiary",
								"cust:" + customer.getId(), "cust:" + originatorCustomerAccountId, transactionNode));
						CustomerNode customerDetailNode = new CustomerNode(null, null, null, null, null, null,
								transaction.getOriginatorName() != null ? transaction.getOriginatorName()
										: (transaction.getMerchant() != null ? transaction.getMerchant() : "Unknown"),
								null, null, null, "cust-" + originatorCustomerAccountId, null,
								"cust:" + originatorCustomerAccountId, "customer", null, null, null);
						customerDetailNode.setGenerated(true);
						graphResponseDto.getVertices().add(customerDetailNode);
					}
				}

				if (accountIds.contains(transaction.getOriginatorAccountId())) {
					TransactionNode transactionNode = new TransactionNode(transaction.getAmount(),
							transaction.getBusinessDate(), null, "tx-" + transaction.getId(),
							transaction.getTransProductType(), transaction.getTransactionChannel(), null, "debit",
							"acct-" + transaction.getOriginatorAccountId(), transaction.getCurrency(),
							transaction.getAmount());
					i=i+1;
					if (transaction.getBeneficiaryCutomerId() != null) {
						graphResponseDto.getEdges()
								.add(new GraphEdge(String.valueOf(customer.getId())+i, "tx_originator",
										"cust:" + transaction.getBeneficiaryCutomerId(), "cust:" + customer.getId(),
										transactionNode));
						customerIdList.add(transaction.getBeneficiaryCutomerId());
					} else {
						i=i+1;
						String beneficiaryCustomerAccountId = !StringUtils
								.isEmpty(transaction.getBeneficiaryAccountId()) ? transaction.getBeneficiaryAccountId()
										: UUID.randomUUID().toString();
						graphResponseDto.getEdges().add(new GraphEdge(String.valueOf(customer.getId())+i, "tx_originator",
								"cust:" + beneficiaryCustomerAccountId, "cust:" + customer.getId(), transactionNode));
						CustomerNode customerDetailNode = new CustomerNode(null, null, null, null, null, null,
								transaction.getOriginatorName() != null ? transaction.getOriginatorName()
										: (transaction.getMerchant() != null ? transaction.getMerchant() : "Unknown"),
								null, null, null, "cust-" + beneficiaryCustomerAccountId, null,
								"cust:" + beneficiaryCustomerAccountId, "customer", null, null, null);
						customerDetailNode.setGenerated(true);
						graphResponseDto.getVertices().add(customerDetailNode);

					}

				}
			}
		}
		List<CustomerDetails> counterPartyList = customerDetailsDao.findByCustomerIdInList(customerIdList);
		for (CustomerDetails customerDetails : counterPartyList) {
			customerMap.put(customerDetails.getId(), customerDetails);
		}
		// Remove main customer node as it is already added.
		if (customerMap.containsKey(customer.getId())) {
			customerMap.remove(customer.getId());
		}
		List<CustomerRelationShipDetails> relations = customerRelationDao
				.findByCustomerNumber(customer.getCustomerNumber());
		List<String> customerIds = relations.stream().map(CustomerRelationShipDetails::getRelatedCustomerNumber)
				.collect(Collectors.toList());
		List<CustomerDetails> relatives = customerDetailsDao.findByCustomerNumberInList(customerIds);
		for (CustomerDetails relative : relatives) {
			Map<String, String> relation = new HashMap<>();
			for (CustomerRelationShipDetails relationdetail : relations) {
				if (relative.getCustomerNumber().equalsIgnoreCase(relationdetail.getRelatedCustomerNumber())) {
					relation.put("relationShipDefinition", relationdetail.getRelationShipDefinition());
				}
			}
			i=i+1;
			graphResponseDto.getEdges().add(new GraphEdge(String.valueOf(customer.getId())+i, "customer_relation",
					"cust:" + relative.getId(), "cust:" + customer.getId(), relation));
			customerMap.put(relative.getId(), relative);
		}
		for (Map.Entry<Long, CustomerDetails> entry : customerMap.entrySet()) {
			CustomerDetails customerDetails = entry.getValue();
			CustomerAddress customerAddress = customerDetails.getCustomerAddress() != null
					&& !customerDetails.getCustomerAddress().isEmpty() ? customerDetails.getCustomerAddress().get(0)
							: null;
			CustomerNode customerDetailNode = new CustomerNode(customerDetails.getResidentCountry(),
					customerAddress != null ? customerAddress.getAddressLine() : "", customerDetails.getJurisdiction(),
					customerDetails.getIndustry(), customerDetails.getDisplayName(),
					customerAddress != null ? customerAddress.getCity() : "", customerDetails.getDisplayName(),
					customerDetails.getCustomerType().name(),
					customerAddress != null ? customerAddress.getAddressType().name() : "",
					customerDetails.getCustomerTitle().name(), "cust-" + customerDetails.getId(),
					customerDetails.getCustomerNumber(), "cust:" + customerDetails.getId(), "customer", null, null,
					customerDetails.getCreatedOn());
			graphResponseDto.getVertices().add(customerDetailNode);
		}

	}

	public void expandCustomerNode(CustomerDetails customer, GraphResponseDto graphResponseDto, Date recordAge,
			String transactionType, String minAmount) {
		List<Account> accounts = accountDao.fetchAccounts(customer.getCustomerNumber());
		List<String> accountIds = accounts.stream().map(Account::getAccountNumber).collect(Collectors.toList());
		List<TransactionsData> accountTransactions = txDataDao.getTransactionsByAccountIds(accountIds, recordAge,
				transactionType, minAmount);
		Map<Long, CustomerDetails> customerMap = new HashMap<>();
		Set<Long> customerIdList = new HashSet<>();
		customerMap.put(customer.getId(), customer);
		if (!accountTransactions.isEmpty()) {
			for (TransactionsData transaction : accountTransactions) {
				if (accountIds.contains(transaction.getBeneficiaryAccountId())) {
					TransactionNode transactionNode = new TransactionNode(transaction.getAmount(),
							transaction.getBusinessDate(), null, "tx-" + transaction.getId(),
							transaction.getTransProductType(), transaction.getTransactionChannel(), null, "credit",
							"acct-" + transaction.getBeneficiaryAccountId(), transaction.getCurrency(),
							transaction.getAmount());
					if (transaction.getOriginatorCutomerId() != null) {
						graphResponseDto.getEdges()
								.add(new GraphEdge(UUID.randomUUID().toString(), "tx_beneficiary",
										"cust:" + customer.getId(), "cust:" + transaction.getOriginatorCutomerId(),
										transactionNode));
						customerIdList.add(transaction.getOriginatorCutomerId());
					} else {
						String originatorCustomerAccountId = !StringUtils.isEmpty(transaction.getOriginatorAccountId())
								? transaction.getOriginatorAccountId()
								: UUID.randomUUID().toString();
						graphResponseDto.getEdges().add(new GraphEdge(UUID.randomUUID().toString(), "tx_beneficiary",
								"cust:" + customer.getId(), "cust:" + originatorCustomerAccountId, transactionNode));
						CustomerNode customerDetailNode = new CustomerNode(null, null, null, null, null, null,
								transaction.getOriginatorName() != null ? transaction.getOriginatorName()
										: (transaction.getMerchant() != null ? transaction.getMerchant() : "Unknown"),
								null, null, null, "cust-" + originatorCustomerAccountId, null,
								"cust:" + originatorCustomerAccountId, "customer", null, null, null);
						customerDetailNode.setGenerated(true);
						graphResponseDto.getVertices().add(customerDetailNode);
					}
				}

				if (accountIds.contains(transaction.getOriginatorAccountId())) {
					TransactionNode transactionNode = new TransactionNode(transaction.getAmount(),
							transaction.getBusinessDate(), null, "tx-" + transaction.getId(),
							transaction.getTransProductType(), transaction.getTransactionChannel(), null, "debit",
							"acct-" + transaction.getOriginatorAccountId(), transaction.getCurrency(),
							transaction.getAmount());
					if (transaction.getBeneficiaryCutomerId() != null) {
						graphResponseDto.getEdges()
								.add(new GraphEdge(UUID.randomUUID().toString(), "tx_originator",
										"cust:" + transaction.getBeneficiaryCutomerId(), "cust:" + customer.getId(),
										transactionNode));
						customerIdList.add(transaction.getBeneficiaryCutomerId());
					} else {

						String beneficiaryCustomerAccountId = !StringUtils
								.isEmpty(transaction.getBeneficiaryAccountId()) ? transaction.getBeneficiaryAccountId()
										: UUID.randomUUID().toString();
						graphResponseDto.getEdges().add(new GraphEdge(UUID.randomUUID().toString(), "tx_originator",
								"cust:" + beneficiaryCustomerAccountId, "cust:" + customer.getId(), transactionNode));
						CustomerNode customerDetailNode = new CustomerNode(null, null, null, null, null, null,
								transaction.getOriginatorName() != null ? transaction.getOriginatorName()
										: (transaction.getMerchant() != null ? transaction.getMerchant() : "Unknown"),
								null, null, null, "cust-" + beneficiaryCustomerAccountId, null,
								"cust:" + beneficiaryCustomerAccountId, "customer", null, null, null);
						customerDetailNode.setGenerated(true);
						graphResponseDto.getVertices().add(customerDetailNode);

					}
				}
			}
		}
		List<CustomerDetails> counterPartyList = customerDetailsDao.findByCustomerIdInList(customerIdList);
		for (CustomerDetails customerDetails : counterPartyList) {
			customerMap.put(customerDetails.getId(), customerDetails);
		}
		List<CustomerRelationShipDetails> relations = customerRelationDao
				.findByCustomerNumber(customer.getCustomerNumber());
		List<String> customerIds = relations.stream().map(CustomerRelationShipDetails::getRelatedCustomerNumber)
				.collect(Collectors.toList());
		List<CustomerDetails> relatives = customerDetailsDao.findByCustomerNumberInList(customerIds);
		for (CustomerDetails relative : relatives) {
			Map<String, String> relation = new HashMap<>();
			for (CustomerRelationShipDetails relationdetail : relations) {
				if (relative.getCustomerNumber().equalsIgnoreCase(relationdetail.getRelatedCustomerNumber())) {
					relation.put("relationShipDefinition", relationdetail.getRelationShipDefinition());
				}
			}
			graphResponseDto.getEdges().add(new GraphEdge(UUID.randomUUID().toString(), "customer_relation",
					"cust:" + relative.getId(), "cust:" + customer.getId(), relation));
			customerMap.put(relative.getId(), relative);
		}
		for (Map.Entry<Long, CustomerDetails> entry : customerMap.entrySet()) {
			CustomerDetails customerDetails = entry.getValue();
			CustomerAddress customerAddress = customerDetails.getCustomerAddress() != null
					&& !customerDetails.getCustomerAddress().isEmpty() ? customerDetails.getCustomerAddress().get(0)
							: null;
			CustomerNode customerDetailNode = new CustomerNode(customerDetails.getResidentCountry(),
					customerAddress != null ? customerAddress.getAddressLine() : "", customerDetails.getJurisdiction(),
					customerDetails.getIndustry(), customerDetails.getDisplayName(),
					customerAddress != null ? customerAddress.getCity() : "", customerDetails.getDisplayName(),
					customerDetails.getCustomerType().name(),
					customerAddress != null ? customerAddress.getAddressType().name() : "",
					customerDetails.getCustomerTitle().name(), "cust-" + customerDetails.getId(),
					customerDetails.getCustomerNumber(), "cust:" + customerDetails.getId(), "customer", null, null,
					customerDetails.getCreatedOn());
			graphResponseDto.getVertices().add(customerDetailNode);
		}
		List<Alert> alerts = alertDao.findByCustomerAndDate(customer.getCustomerNumber(), recordAge);
		for (Alert alert : alerts) {
			generateAlertNode(alert, graphResponseDto);
			graphResponseDto.getEdges().add(new GraphEdge(UUID.randomUUID().toString(), "has_alert",
					"alert:" + alert.getAlertId(), "cust:" + customer.getId()));
		}
	}

	@Transactional("transactionManager")
	public void createAlert(List<TransactionsData> transactionsList, long userId) {
		RuleServicesClient ruleServiceClient = getRuleServicesClient();
		try {

			for (TransactionsData transaction : transactionsList) {
				TransactionsDataDto transactionsDataDto = new TransactionsDataDto();
				BeanUtils.copyProperties(transactionsDataDto, transaction);
				Alert alert = new Alert();
				if (transaction.getOriginatorAccountId() != null) {
					Account orginatorAccount = accountDao.fetchAccount(transaction.getOriginatorAccountId());
					transactionsDataDto
							.setOriginatorAccountCurrency(orginatorAccount.getAccountCurrency() == null ? null
									: orginatorAccount.getAccountCurrency().toLowerCase());
					Country OrginatorCountry = countryDao
							.findByName(orginatorAccount.getCustomerDetails().getResidentCountry());
					transactionsDataDto.setOriginatorCountry(
							OrginatorCountry == null ? null : OrginatorCountry.getCountry().toLowerCase());
					alert.setJurisdiction(orginatorAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(orginatorAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(orginatorAccount.getCustomerDetails().getDisplayName());
					alert.setUserId(userId);

				}
				if (transaction.getBeneficiaryAccountId() != null) {
					Account beneficiaryAccount = accountDao.fetchAccount(transaction.getBeneficiaryAccountId());
					transactionsDataDto
							.setBeneficiaryAccountCurrency(beneficiaryAccount.getAccountCurrency() == null ? null
									: beneficiaryAccount.getAccountCurrency().toLowerCase());
					Country beneficiaryCountry = countryDao
							.findByName(beneficiaryAccount.getCustomerDetails().getResidentCountry());
					transactionsDataDto.setBeneficiaryCountry(
							beneficiaryCountry == null ? null : beneficiaryCountry.getCountry().toLowerCase());
					alert.setJurisdiction(beneficiaryAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(beneficiaryAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(beneficiaryAccount.getCustomerDetails().getDisplayName());
					alert.setUserId(userId);

				}
				List<GenericCommand<?>> commands = new ArrayList<GenericCommand<?>>();
				commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newInsert(transactionsDataDto,
						"transalert"));
				commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newFireAllRules("FireRules"));
				BatchExecutionCommandImpl batchCommand = new BatchExecutionCommandImpl(commands);
				batchCommand.setLookup("defaultKieSession");
				ServiceResponse<ExecutionResults> responseResult = ruleServiceClient
						.executeCommandsWithResults(ALERT_CONTAINER, batchCommand);
				TransactionsDataDto resultTransactionsDataDto = (TransactionsDataDto) responseResult.getResult()
						.getValue("transalert");

				if (resultTransactionsDataDto.getScenario() != null) {
					alert.setAlertBusinessDate(transaction.getBusinessDate());
					alert.setCreatedOn(new Date());
					alert.setModifiedOn(new Date());
					alert.setAge(20);
					alert.setScenario(resultTransactionsDataDto.getScenario());
					alert.setCreatedBy(userId);
					alert.setRowStatus(false);
					alertDao.create(alert);
				}
			}

		} catch (Exception e) {

		}

	}

	@SuppressWarnings("unused")
	@Override
	@Transactional("transactionManager")
	public void createAlert(TransactionsData transactionsData, long userId) {
		RuleServicesClient ruleServiceClient = getRuleServicesClient();
		try {
			TransactionsDataDto transactionsDataDto = new TransactionsDataDto();
			BeanUtils.copyProperties(transactionsDataDto, transactionsData);
			Account orginatorAccount = new Account();
			Account beneficiaryAccount = new Account();
			Alert alert = new Alert();
			if (transactionsData.getOriginatorAccountId() != null && transactionsData.getOriginatorAccountId() != ""
					&& transactionsData.getOriginatorAccountId().length() > 0) {
					orginatorAccount = accountDao.fetchAccount(transactionsData.getOriginatorAccountId());
				if (orginatorAccount != null) {
					Country OrginatorCountry = countryDao
							.findByName(orginatorAccount.getCustomerDetails().getResidentCountry());

					if (transactionsDataDto.getTransactionChannel().equalsIgnoreCase("CREDIT CARD")) {
						if (transactionsDataDto.getTransProductType().equalsIgnoreCase("POS PURCHASE")) {
							LuxuryShops luxuryShop = new LuxuryShops();

							// creditcard Luxry apis

							// transactionsDataDto.setMerchantWebsite("www.amazon.com");
							boolean isLuxury = false;
							try {
								if (transactionsDataDto.getMerchantWebsite() != null
										&& transactionsDataDto.getMerchantWebsite() != ""
										&& transactionsDataDto.getMerchantWebsite().length() != 0) {

									String hostJson = "{\"tasks\":[\"LUXURY\"],\"host\":" + "\""
											+ transactionsDataDto.getMerchantWebsite() + "\"" + "}";
									String addHostUrl = SITE_INFO_ADD_KEYWORD + "/v1/site-info/add-host";
									String responseAddHost = postStringDataToServer(addHostUrl, hostJson);
									String resultUrl = SITE_INFO_ADD_KEYWORD + "/v1/site-info/results";
									String resultJson = "{\"hosts\":[" + "\"" + transactionsDataDto.getMerchantWebsite()
											+ "\"" + "]}";
									String response = postStringDataToServer(resultUrl, resultJson);
									ObjectMapper objectMapper = new ObjectMapper();
									TypeFactory typeFactory = objectMapper.getTypeFactory();
									List<LuxuryDto> list = objectMapper.readValue(response,
											typeFactory.constructCollectionType(List.class, LuxuryDto.class));
									if (!list.isEmpty()) {
										List<String> websiteClasses = list.get(0).getWebsiteClasses().stream()
												.map(WebsiteClass::getWebsiteClassName).collect(Collectors.toList());
										if (websiteClasses != null && websiteClasses.contains("LUXURY")) {
											isLuxury = true;
										}
									}

								}
								if (transactionsDataDto.getMerchant() != null && transactionsDataDto.getMerchant() != ""
										&& transactionsDataDto.getMerchant().length() != 0) {
									List<LuxuryDto> luxuryReponse = new ArrayList<>();

									String json = "{\"keyword\":" + "\"" + transactionsDataDto.getMerchant() + "\""
											+ "}";
									String url = SITE_INFO_ADD_KEYWORD + "/v1/site-info/add-keyword";
									String response = postStringDataToServer(url, json);

									String url2 = SITE_INFO_ADD_KEYWORD + "/v1/site-info/results";
									String query = "{\"hosts\":" + response + "}";

									String response2 = postStringDataToServer(url2, query);

									ObjectMapper objectMapper = new ObjectMapper();
									TypeFactory typeFactory = objectMapper.getTypeFactory();
									List<LuxuryDto> list = objectMapper.readValue(response2,
											typeFactory.constructCollectionType(List.class, LuxuryDto.class));

									for (Iterator<LuxuryDto> iter = list.listIterator(); iter.hasNext();) {
										LuxuryDto luxryDto = iter.next();
										if (luxryDto.getWebsiteClasses() == null
												|| luxryDto.getWebsiteClasses().size() == 0)
											iter.remove();
									}
									for (LuxuryDto luxryDto : list) {
										List<String> websiteClasses = luxryDto.getWebsiteClasses().stream()
												.map(WebsiteClass::getWebsiteClassName).collect(Collectors.toList());
										if (websiteClasses.contains("LUXURY"))
											luxuryReponse.add(luxryDto);
									}

									Double ratio = (luxuryReponse.size() * 100.00) / list.size();
									if (ratio >= 20.00) {
										isLuxury = true;
									}
								}
							} catch (Exception e) {
								ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
							}
							// ended..

							if (transactionsDataDto.getMerchant() != null && transactionsDataDto.getMerchant() != ""
									&& transactionsDataDto.getMerchant().length() != 0) {

								// luxuryShop = luxuryShopsDao.getLuxuryShop(transactionsDataDto.getMerchant(),
								// true);
								// if (luxuryShop != null) {
								if (isLuxury) {
									if (transactionsDataDto.getAmount() >= 1000.0) {

										Double averageOfTransactions = txDataDao
												.getCreditCardMontlyAverageOfThreeMonthsTransactions(
														transactionsData.getOriginatorAccountId(),
														transactionsData.getBusinessDate());
										if (averageOfTransactions != null && averageOfTransactions == 0.0) {
											transactionsDataDto.setCreditCardLuxuryTransaction("yes");
											transactionsDataDto.setCreditcardTransAverageIncrement(0.0);
										}
										if (averageOfTransactions != null && averageOfTransactions > 0.0) {
											if (transactionsDataDto.getAmount() > averageOfTransactions) {

												Double incrementPercentage = ((transactionsDataDto.getAmount()
														- averageOfTransactions) / averageOfTransactions) * 100;
												transactionsDataDto
														.setCreditcardTransAverageIncrement(incrementPercentage);
												if (incrementPercentage >= 20)
													transactionsDataDto.setCreditCardLuxuryTransaction("yes");

											}
										}

										// List<String>
										// luxuryShopNames=luxuryShopsDao.getLuxuryShopNames();

										int creditCardLuxuryTransactionsShopCount = txDataDao
												.getCreditCardLuxuryTransactionsCount(
														transactionsDataDto.getOriginatorAccountId(),
														transactionsDataDto.getMerchant(), true,
														transactionsDataDto.getBusinessDate());
										if (creditCardLuxuryTransactionsShopCount == 0 && transactionsDataDto
												.getCreditCardLuxuryTransaction().equalsIgnoreCase("yes"))
											transactionsDataDto.setNewCreditcardLuxuryCounterpartyTransaction("yes");
									}
								}
							}

						}

						if (transactionsDataDto.getTransProductType().equalsIgnoreCase("ATM withdrawal")) {
							List<Float> amountLimit = new ArrayList<Float>();
							List<Integer> atmTransactionsLimit = new ArrayList<Integer>();
							boolean highRiskCountry = false;
							Country atmWithdrawlCountry = countryDao
									.findByName(transactionsDataDto.getCountryOfTransaction());
							transactionsDataDto.setCreditCardAtmWithdrwalCountryName(atmWithdrawlCountry.getCountry());
							if (atmWithdrawlCountry.getFatfCreditCardAmountLimit() != null
									&& atmWithdrawlCountry.getFatfCreditCardAmountLimit() != 0.0
									&& atmWithdrawlCountry.getFatfCreditCardAmountLimit() > 0.0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry.getFatfCreditCardAmountLimit());
								atmTransactionsLimit.add(atmWithdrawlCountry.getFatfCreditCardTransactionsLimit());

							}
							if (atmWithdrawlCountry.getTaxJusticeNetworkCreditCardAmountLimit() != null
									&& atmWithdrawlCountry.getTaxJusticeNetworkCreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry.getTaxJusticeNetworkCreditCardAmountLimit());
								atmTransactionsLimit
										.add(atmWithdrawlCountry.getTaxJusticeNetworkCreditCardTransactionsLimit());
							}
							if (atmWithdrawlCountry.getBaselAMLIndex2017CreditCardAmountLimit() != null
									&& atmWithdrawlCountry.getBaselAMLIndex2017CreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry.getBaselAMLIndex2017CreditCardAmountLimit());
								atmTransactionsLimit
										.add(atmWithdrawlCountry.getBaselAMLIndex2017CreditCardTransactionsLimit());
							}
							if (atmWithdrawlCountry.gettransparancyInternationalCountriesCreditCardAmountLimit() != null
									&& atmWithdrawlCountry
											.gettransparancyInternationalCountriesCreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry
										.gettransparancyInternationalCountriesCreditCardAmountLimit());
								atmTransactionsLimit.add(atmWithdrawlCountry
										.gettransparancyInternationalCountriesCreditCardTransactionsLimit());
							}
							if (atmWithdrawlCountry.getWorldBankCreditCardAmountLimit() != null
									&& atmWithdrawlCountry.getWorldBankCreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry.getWorldBankCreditCardAmountLimit());
								atmTransactionsLimit.add(atmWithdrawlCountry.getWorldBankCreditCardTransactionsLimit());

							}
							if (atmWithdrawlCountry.getWefCreditCardAmountLimit() != null
									&& atmWithdrawlCountry.getWefCreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry.getWefCreditCardAmountLimit());
								atmTransactionsLimit.add(atmWithdrawlCountry.getWefCreditCardTransactionsLimit());
							}
							if (atmWithdrawlCountry.getFreedomHouseHighRiskCountriesCreditCardAmountLimit() != null
									&& atmWithdrawlCountry
											.getFreedomHouseHighRiskCountriesCreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(
										atmWithdrawlCountry.getFreedomHouseHighRiskCountriesCreditCardAmountLimit());
								atmTransactionsLimit.add(atmWithdrawlCountry
										.getFreedomHouseHighRiskCountriesCreditCardTransactionsLimit());
							}
							if (atmWithdrawlCountry
									.getWorldJusticeProjectHighRiskCountriesCreditCardAmountLimit() != null
									&& atmWithdrawlCountry
											.getWorldJusticeProjectHighRiskCountriesCreditCardAmountLimit() != 0) {
								highRiskCountry = true;
								amountLimit.add(atmWithdrawlCountry
										.getWorldJusticeProjectHighRiskCountriesCreditCardAmountLimit());
								atmTransactionsLimit.add(atmWithdrawlCountry
										.getWorldJusticeProjectHighRiskCountriesCreditCardTransactionsLimit());
							}

							if (highRiskCountry) {

								int smallAmountIndex = amountLimit.indexOf(Collections.min(amountLimit));
								Double highRiskCountryMinimumAmountLimit = Double
										.valueOf(amountLimit.get(smallAmountIndex));
								List<Double> atmWithdrawlSevenDaysAmountList = new ArrayList<Double>();

								atmWithdrawlSevenDaysAmountList = txDataDao
										.getCreditCardAtmWithdrawlLastSevenDaysAmount(
												transactionsDataDto.getOriginatorAccountId(),
												transactionsDataDto.getCountryOfTransaction(),
												transactionsDataDto.getBusinessDate());
								Double totalAmount = 0.0;
								if (atmWithdrawlSevenDaysAmountList.size() > 0) {

									int transactionsCount = 0;
									for (Double amount : atmWithdrawlSevenDaysAmountList) {
										totalAmount = totalAmount + amount;
										transactionsCount = transactionsCount + 1;
									}
									if (totalAmount > highRiskCountryMinimumAmountLimit) {
										transactionsDataDto.setCreditCardAtmWithdrawlsInHighRiskCountry("yes");
									}
									if (totalAmount > highRiskCountryMinimumAmountLimit && transactionsCount > 1)
										transactionsDataDto.setCreditCardAtmWithdrwalTransactionMoreThanOne("yes");
								}

								if (totalAmount > highRiskCountryMinimumAmountLimit) {

									/*
									 * Double creditCardATMCashInAverage = txDataDao.getCreditCardATMCashInAverage(
									 * transactionsDataDto.getBeneficiaryAccountId(),
									 * transactionsDataDto.getBusinessDate()); Double creditCardATMWithdralAverage =
									 * txDataDao.getCreditCardATMWithdrawalAverage(
									 * transactionsDataDto.getOriginatorAccountId(),
									 * transactionsDataDto.getBusinessDate()); if ((creditCardATMWithdralAverage !=
									 * 0.0 && creditCardATMWithdralAverage > 0.0) && (creditCardATMWithdralAverage
									 * != 0.0 && creditCardATMWithdralAverage > 0.0)) { if
									 * (creditCardATMWithdralAverage > creditCardATMCashInAverage) {
									 * 
									 * Double creditCardATMHighVolumeWithdrawlAverage =
									 * (creditCardATMWithdralAverage / creditCardATMCashInAverage) * 100;
									 * 
									 * transactionsDataDto.setCreditCardAtmHighVolumeWithdrwal(
									 * creditCardATMHighVolumeWithdrawlAverage); } }
									 */
									String originatorRelatedCountries = orginatorAccount.getCustomerDetails()
											.getResidentCountry();
									if (orginatorAccount.getCustomerDetails().getBusinessesActivityCountry() != null)
										originatorRelatedCountries = originatorRelatedCountries + ","
												+ orginatorAccount.getCustomerDetails().getBusinessesActivityCountry();
									transactionsDataDto
											.setCreditCardAtmTransactionRelationCountries(originatorRelatedCountries);

									int creditCardATMShortTimeWithdrawlscount = txDataDao
											.getCreditCardATMWithdrawlsInShortTime(
													transactionsDataDto.getOriginatorAccountId(),
													transactionsDataDto.getBusinessDate(),
													transactionsDataDto.getAtmAddress());
									if (creditCardATMShortTimeWithdrawlscount > 1) {
										transactionsDataDto.setCreditCardAtmWithdrwalsInShortTime("yes");
									}
									int creditCardAtmWithdrawlsCount = txDataDao.getCreditCardNewCountryATMTransaction(
											transactionsDataDto.getOriginatorAccountId(),
											transactionsDataDto.getCountryOfTransaction(),
											transactionsDataDto.getBusinessDate());
									if (creditCardAtmWithdrawlsCount == 0) {
										transactionsDataDto.setCreditCardAtmTransactionInNewCountries("yes");
									}

								}
							}

						}

					}
					alert.setJurisdiction(orginatorAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(orginatorAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(orginatorAccount.getCustomerDetails().getDisplayName());

					transactionsDataDto
							.setOriginatorBusinesses(orginatorAccount.getCustomerDetails().getIndustry().toLowerCase());

					if (OrginatorCountry != null) {
						transactionsDataDto.setOriginatorCountry(OrginatorCountry.getCountry().toLowerCase());
						transactionsDataDto.setOriginatorFatfRisk(OrginatorCountry.getFatfRisk());
						transactionsDataDto.setOriginatorTaskJusticeNetworkHighRisk_FinancialSecracyIndex(
								OrginatorCountry.getTaskJusticeNetworkHighRisk_FinancialSecracyIndex());
						transactionsDataDto.setOriginatorInternationalNarcoticsControlRisk(
								OrginatorCountry.getInternationalNarcoticsControlRisk());
						transactionsDataDto.setOriginatorTransparencyInternationalRisk(
								OrginatorCountry.getTransparencyInternationalRisk());
						transactionsDataDto.setOriginatorWorldBankRisk(OrginatorCountry.getWorldBankRisk());
						transactionsDataDto.setOriginatorFreedomHouseRisk(OrginatorCountry.getFreedomHouseRisk());
						transactionsDataDto.setOriginatorWorldJusticeRiskWorld_CriminalJusticeRiskRating(
								OrginatorCountry.getWorldJusticeRiskWorld_CriminalJusticeRiskRating());
						transactionsDataDto.setOriginatorWefRisk(OrginatorCountry.getWefRisk());
						transactionsDataDto.setOriginatorPoliticalRiskServicesInternationalCountryRisk_PRS(
								OrginatorCountry.getPoliticalRiskServicesInternationalCountryRisk_PRS());
						transactionsDataDto.setOriginatorBaselAMLIndex2017(OrginatorCountry.getBaselAMLIndex2017());
					}
					alert.setJurisdiction(orginatorAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(orginatorAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(orginatorAccount.getCustomerDetails().getDisplayName());

					alert.setJurisdiction(orginatorAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(orginatorAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(orginatorAccount.getCustomerDetails().getDisplayName());

					alert.setUserId(userId);
					alert.setTransactionProductType(transactionsDataDto.getTransProductType());
					alert.setTransactionId(transactionsDataDto.getId());
				}

			}
			if (transactionsData.getBeneficiaryAccountId() != null && transactionsData.getBeneficiaryAccountId() != ""
					&& transactionsData.getBeneficiaryAccountId().length() > 0) {
				beneficiaryAccount = accountDao.fetchAccount(transactionsData.getBeneficiaryAccountId());

				if (beneficiaryAccount != null) {
					transactionsDataDto
							.setBeneficiaryAccountCurrency(beneficiaryAccount.getAccountCurrency() == null ? "NA"
									: beneficiaryAccount.getAccountCurrency().toLowerCase());

					// unRelatedCounterParties
					Double totalAmount = 0.0;
					Double monthlyAverage = 0.0;
					Double average = 0.0;
					if (transactionsDataDto.getTransactionChannel().equalsIgnoreCase("CREDIT CARD")
							|| transactionsDataDto.getTransProductType().equalsIgnoreCase("Credit Card Payment")) {
						if (transactionsDataDto.getTransProductType().equalsIgnoreCase("Credit Card Payment")) {
							CustomerRelationShipDetails customerRelationShipDetails = new CustomerRelationShipDetails();
							if (transactionsDataDto.getTransactionChannel().equalsIgnoreCase("Debit Card")
									|| transactionsDataDto.getTransactionChannel().equalsIgnoreCase("Cheque")
									|| transactionsDataDto.getTransactionChannel().equalsIgnoreCase("Cash")
									|| transactionsDataDto.getTransactionChannel().equalsIgnoreCase("Check")
									|| transactionsDataDto.getTransactionChannel().equalsIgnoreCase("Online Payment")) {
								Double lastSevenDaysAggregat = 0.0;
								Boolean status = false;
								List<String> beneficiaryCustomerRelationNamesList = new ArrayList<String>();
								if ((transactionsDataDto.getOriginatorAccountId() == null)
										|| (transactionsDataDto.getOriginatorAccountId().equals(""))) {
									if (transactionsDataDto.getOriginatorName() != null) {
										if (transactionsDataDto.getOriginatorName()
												.equals(beneficiaryAccount.getCustomerDetails().getDisplayName())) {
											status = true;
										} else {
											beneficiaryCustomerRelationNamesList = customerRelationDao
													.getCustomerRelationnamesList(
															transactionsDataDto.getBeneficiaryCutomerId());
											for (String name : beneficiaryCustomerRelationNamesList) {
												if (name.equals(transactionsDataDto.getOriginatorName()))
													status = true;
											}
										}
										if (!status)
											lastSevenDaysAggregat = txDataDao
													.getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmountWithOriginatorName(
															transactionsDataDto.getOriginatorName(),
															transactionsDataDto.getBeneficiaryAccountId(),
															transactionsDataDto.getBusinessDate());
									}
								} else {
									if (orginatorAccount.getCustomerDetails().getCustomerNumber() != null) {
										customerRelationShipDetails = customerRelationDao.findCustomerRelation(
												beneficiaryAccount.getCustomerDetails().getCustomerNumber(),
												orginatorAccount.getCustomerDetails().getCustomerNumber());
									}
									if (customerRelationShipDetails == null) {
										lastSevenDaysAggregat = txDataDao
												.getCreditCardUnrelatedCounterPartyLastSevenDaysAggregateAmount(
														transactionsDataDto.getOriginatorAccountId(),
														transactionsDataDto.getBeneficiaryAccountId(),
														transactionsDataDto.getBusinessDate());
									}
								}

								if (lastSevenDaysAggregat > 1000.0) {
									List<TransactionsData> transactionsList = txDataDao
											.getCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions(
													transactionsDataDto.getBeneficiaryAccountId(),
													transactionsDataDto.getBusinessDate());
									if (transactionsList.size() > 0) {
										totalAmount = sumOfCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions(
												transactionsList);
										monthlyAverage = txDataDao.getCreditCardMontlyAverageOfThreeMonthsTransactions(
												transactionsDataDto.getBeneficiaryAccountId(),
												transactionsDataDto.getBusinessDate());
										if (monthlyAverage == 0.0)
											transactionsDataDto.setCreditCardUnrelatedCounterPartiesTransactions("yes");

										if (monthlyAverage > 0.0) {
											average = (monthlyAverage / 2);
											if (totalAmount > average)
												transactionsDataDto
														.setCreditCardUnrelatedCounterPartiesTransactions("yes");
										}
									}
								}
							}

							Double oneMonthTransactionSumAmount = txDataDao.getCreditCardMontlyAverageOfTransactions(
									transactionsDataDto.getBeneficiaryAccountId(),
									transactionsDataDto.getBusinessDate());
							Double overpaymentIncrement = 0.0;
							if ((oneMonthTransactionSumAmount != 0)
									&& (transactionsDataDto.getAmount() > oneMonthTransactionSumAmount)) {

								overpaymentIncrement = (transactionsDataDto.getAmount() / oneMonthTransactionSumAmount)
										* 100;
								transactionsDataDto.setCreditCardOverPaymentPercentage(overpaymentIncrement);

							}

						}
						///// Refund/Overpayment
						if (transactionsDataDto.getTransProductType().equalsIgnoreCase("Merchant Refund")) {
							Double merchantDebitAmount = 0.0;
							Double merchantRefundAvg = 0.0;
							boolean flag = false;
							// checks if the refund is >=$1000
							if (transactionsDataDto.getAmount() >= 1000.0) {
								// checks the last 3 months transactions other than this transaction made to the
								// merchant
								merchantDebitAmount = txDataDao.getThreeMonthsTransSumWithMerchant(
										transactionsDataDto.getBeneficiaryAccountId(),
										transactionsDataDto.getMerchant(), transactionsDataDto.getBusinessDate());
								// handles the first transaction made to the merchant
								if (merchantDebitAmount == 0.0 || merchantDebitAmount == null) {
									transactionsDataDto.setCreditCardMerchantRefund("yes");
									flag = true;
								}
								// checks if the merchant refund is greater than the last three months
								// transactions made with the merchant and calculates percentage if true
								if (transactionsDataDto.getAmount() > merchantDebitAmount) {
									if (merchantDebitAmount > 0)
										merchantRefundAvg = (transactionsDataDto.getAmount() / merchantDebitAmount)
												* 100;
									if (merchantRefundAvg >= 120.0) {
										// transactionsDataDto. ("yes");
										transactionsDataDto.setCreditCardMerchantRefund("yes");
										flag = true;
									}
								}

								Double overAllCreditAmount = 0.0;
								Double overAllDebitAmount = 0.0;
								Double avgMonthlyTurnover = 0.0;
								Double overAllTransactionAvg = 0.0;
								int transactionsCount = 0;
								if (flag) {
									transactionsCount = txDataDao.getPreviousTransactionsCountWithMerchant(
											transactionsDataDto.getBeneficiaryAccountId(),
											transactionsDataDto.getMerchant(), transactionsDataDto.getBusinessDate());
									if (transactionsCount == 0)
										transactionsDataDto.setCreditCardNewMerchantRefund("yes");

								}

								// gets overall credit amount of the card from its beginning
								overAllCreditAmount = txDataDao.getOverAllCreditAmount(
										transactionsDataDto.getBeneficiaryAccountId(),
										transactionsDataDto.getMerchant());
								// gets overAll debit amount of the card from its beginning
								overAllDebitAmount = txDataDao.getOverAllDebitAmount(
										transactionsDataDto.getBeneficiaryAccountId(),
										transactionsDataDto.getMerchant());
								/* if(overAllCreditAmount>overAllDebitAmount){ */
								if ((overAllCreditAmount - overAllDebitAmount) >= 1000.0) {
									boolean generalRuleApplied = false;
									avgMonthlyTurnover = txDataDao.getCreditCardMontlyAverageOfThreeMonthsTransactions(
											transactionsDataDto.getBeneficiaryAccountId(),
											transactionsDataDto.getBusinessDate());
									if (avgMonthlyTurnover == 0.0) {
										transactionsDataDto.setCreditCardMerchantRefund("yes");
										generalRuleApplied = true;
									} else {
										if (avgMonthlyTurnover < (overAllCreditAmount - overAllDebitAmount)) {
											overAllTransactionAvg = ((overAllCreditAmount - overAllDebitAmount)
													/ avgMonthlyTurnover) * 100;
											if (overAllTransactionAvg >= 120.0) {
												transactionsDataDto.setCreditCardMerchantRefund("yes");
												generalRuleApplied = true;
											}

										}

									}
								}

							}
						}
					}

					alert.setJurisdiction(beneficiaryAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(beneficiaryAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(beneficiaryAccount.getCustomerDetails().getDisplayName());

					alert.setUserId(userId);
					alert.setTransactionProductType(transactionsDataDto.getTransProductType());
					alert.setTransactionId(transactionsDataDto.getId());
				}

			}

			if ((transactionsData.getOriginatorAccountId() != null
					&& transactionsData.getOriginatorAccountId().trim().length() > 0)
					&& (transactionsData.getBeneficiaryAccountId() != null
							&& transactionsData.getBeneficiaryAccountId().trim().length() > 0)) {
				if (orginatorAccount != null) {

					alert.setJurisdiction(orginatorAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(orginatorAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(orginatorAccount.getCustomerDetails().getDisplayName());

					alert.setUserId(userId);
					alert.setTransactionProductType(transactionsDataDto.getTransProductType());
					alert.setTransactionId(transactionsDataDto.getId());
				}

				if (transactionsDataDto.getTransProductType().equalsIgnoreCase("Credit Card Payment")
						|| transactionsDataDto.getTransProductType().equalsIgnoreCase("Merchant Refund")) {
					alert.setJurisdiction(beneficiaryAccount.getCustomerDetails().getJurisdiction());
					alert.setFocalNtityDisplayId(beneficiaryAccount.getCustomerDetails().getCustomerNumber());
					alert.setFocalNtityDisplayName(beneficiaryAccount.getCustomerDetails().getDisplayName());
				}
			}
			List<String> scenarioList = new ArrayList<String>();
			List<String> description = new ArrayList<String>();
			List<String> scenarioType = new ArrayList<String>();
			List<Integer> riskScore = new ArrayList<Integer>();

			List<GenericCommand<?>> commands = new ArrayList<GenericCommand<?>>();

			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newSetGlobal("scenarioOfAlert",
					scenarioList));
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newSetGlobal("descriptionOfAlert",
					description));
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newSetGlobal("scenarioTypeOfAlert",
					scenarioType));
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newSetGlobal("riskScoreOfAlert",
					riskScore));
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newInsert(transactionsDataDto,
					"Transalert"));
			commands.add((GenericCommand<?>) KieServices.Factory.get().getCommands().newFireAllRules("FireRules"));
			BatchExecutionCommandImpl batchCommand = new BatchExecutionCommandImpl(commands);
			batchCommand.setLookup("defaultKieSession");
			ServiceResponse<ExecutionResults> responseResult = ruleServiceClient
					.executeCommandsWithResults(ALERT_CONTAINER, batchCommand);
			TransactionsDataDto resultTransactionsDataDto = (TransactionsDataDto) responseResult.getResult()
					.getValue("Transalert");

			alert.setAlertBusinessDate(transactionsData.getBusinessDate());
			alert.setCreatedOn(new Date());
			alert.setModifiedOn(new Date());
			// alert.setScenario(resultTransactionsDataDto.getScenario());
			alert.setCreatedBy(userId);
			alert.setRowStatus(false);
			alert.setAlertStatus(AlertStatus.OPEN);
			alert.setAlertType("internal");

			if ((resultTransactionsDataDto.getScenarioOfAlert() != null
					&& resultTransactionsDataDto.getScenarioOfAlert().size() > 0)) {
				scenarioList.addAll(resultTransactionsDataDto.getScenarioOfAlert());
				description.addAll(resultTransactionsDataDto.getDescriptionOfAlert());
				scenarioType.addAll(resultTransactionsDataDto.getScenarioTypeOfAlert());
				riskScore.addAll(resultTransactionsDataDto.getRiskScoreOfAlert());
			} else {
				scenarioList = new ArrayList<String>();
				description = new ArrayList<String>();
				scenarioType = new ArrayList<String>();
				riskScore = new ArrayList<Integer>();

			}

			if (resultTransactionsDataDto.getCreditCardLuxuryTransactionScenario() != null) {

				scenarioList.add(resultTransactionsDataDto.getCreditCardLuxuryTransactionScenario());
				description.add(resultTransactionsDataDto.getCreditCardLuxuryTransactionDescription());
				scenarioType.add(resultTransactionsDataDto.getCreditCardLuxuryTransactionScenarioType());
				riskScore.add(resultTransactionsDataDto.getCreditCardLuxuryTransactionRiskScore());
			}

			if (resultTransactionsDataDto.getCreditCardAtmWithdrawlsInHighRiskCountryScenario() != null) {
				scenarioList.add(resultTransactionsDataDto.getCreditCardAtmWithdrawlsInHighRiskCountryScenario());
				description.add(resultTransactionsDataDto.getCreditCardAtmWithdrawlsInHighRiskCountryDescription());
				scenarioType.add(resultTransactionsDataDto.getCreditCardAtmWithdrawlsInHighRiskCountryScenarioType());
				riskScore.add(resultTransactionsDataDto.getCreditCardAtmWithdrawlsInHighRiskCountryRiskScore());
			}

			if (resultTransactionsDataDto.getCreditCardUnrelatedCounterPartiesScenario() != null) {
				scenarioList.add(resultTransactionsDataDto.getCreditCardUnrelatedCounterPartiesScenario());
				description.add(resultTransactionsDataDto.getCreditCardUnrelatedCounterPartiesDescription());
				scenarioType.add(resultTransactionsDataDto.getCreditCardUnrelatedCounterPartiesScenarioType());
				riskScore.add(resultTransactionsDataDto.getCreditCardUnrelatedCounterPartiesRiskScore());
			}

			if (resultTransactionsDataDto.getCreditCardMerchantRefundScenario() != null) {
				scenarioList.add(resultTransactionsDataDto.getCreditCardMerchantRefundScenario());
				description.add(resultTransactionsDataDto.getCreditCardMerchantRefundDescription());
				scenarioType.add(resultTransactionsDataDto.getCreditCardMerchantRefundScenarioType());
				riskScore.add(resultTransactionsDataDto.getCreditCardMerchantRefundRiskScore());
			}

			if (scenarioList.size() > 0) {
				for (int i = 0; i < scenarioList.size(); i++) {
					Alert newAlert = new Alert();
					BeanUtils.copyProperties(newAlert, alert);
					newAlert.setScenario(scenarioList.get(i));
					newAlert.setAlertDescription(description.get(i));
					newAlert.setAlertScenarioType(scenarioType.get(i));
					newAlert.setRiskScore(riskScore.get(i));
					newAlert.setEntityType(EntityType.TRANSACTION);
					alertDao.create(newAlert);
				}

			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

	}

	public String getPersonFraud(String personName, long userId) throws Exception {
		String query = "{\"fetchers\":[\"6\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"17\"],\"keyword\":" + personName
				+ ",\"searchType\":\"Person\"}";
		String response = searchHistoryService.getSearchData(userId, query, "search");
		JSONObject jsonResponse = new JSONObject(response);

		org.json.JSONArray results = jsonResponse.getJSONArray("results");
		for (int i = 0; i < results.length(); i++) {
			JSONObject resultObject = results.getJSONObject(i);
			org.json.JSONArray entities = resultObject.getJSONArray("entities");
			if (entities.length() != 0)
				return "yes";

		}
		return response;
	}

	public RuleServicesClient getRuleServicesClient() {
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(KIE_SERVER_URL, KIE_SERVER_USERNAME,
				KIE_SERVER_PASSWORD);
		Set<Class<?>> allClasses = new HashSet<Class<?>>();
		allClasses.add(TransactionsDataDto.class);
		config.addExtraClasses(allClasses);
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);
		RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
		return ruleClient;

	}

	public Double sumOfCreditCardAllUnrelatedCounterPartyLastSevenDaysTransactions(
			List<TransactionsData> transactionsList) {
		Double totalAmount = 0.0;
		Account beneficiaryAccount = accountDao.fetchAccount(transactionsList.get(0).getBeneficiaryAccountId());
		for (TransactionsData transactionsData : transactionsList) {
			if (transactionsData.getOriginatorAccountId() != null) {
				Account originatorAccount = accountDao.fetchAccount(transactionsData.getBeneficiaryAccountId());
				CustomerRelationShipDetails customerRelationShipDetails = customerRelationDao.findCustomerRelation(
						beneficiaryAccount.getCustomerDetails().getCustomerNumber(),
						originatorAccount.getCustomerDetails().getCustomerNumber());
				if (customerRelationShipDetails == null)
					totalAmount = totalAmount + transactionsData.getAmount();

			} else {
				totalAmount = totalAmount + transactionsData.getAmount();
			}
		}
		return totalAmount;

	}

	@Override
	public List<?> fetchAlertsBwDatesOrderBy(String name, String datefrom, String dateto, Integer pageNumber,
			Integer recordsPerPage, String orderBy) throws ParseException {
		List<AlertDashBoardRespDtoAscnd> list = alertDao.fetchAlertsBwDatesOrderBy(name, datefrom, dateto,
				pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
				recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, orderBy);
		Collections.sort(list);
		// filter for name
		if (name != null) {
			Iterator<AlertDashBoardRespDtoAscnd> iterator = list.iterator();
			while (iterator.hasNext()) {
				AlertDashBoardRespDtoAscnd ascnd = iterator.next();
				String nameOfCustomer = (ascnd.getCustomerName()).replaceAll("\\s", "");
				String nameToFilter = name.replaceAll("\\s", "");
				Pattern pattern = Pattern.compile(".*" + nameToFilter + ".*", Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(nameOfCustomer);
				boolean output = matcher.matches();
				if (output == false) {
					iterator.remove();
				}

			}

		}
		return list;

	}

	@Override
	public List<AlertStatusDto> fetchGroupByAlertStatus(String fromDate, String toDate, boolean periodBy,
			FilterDto filterDto) throws ParseException {
		return alertDao.fetchGroupByAlertStatus(fromDate, toDate, periodBy, filterDto);
	}

	@Override
	public List<Alert> calculateAlertsGenRatio(List<Long> txIds, String fromDate, String toDate) throws ParseException {
		return alertDao.calculateAlertsGenRatio(txIds, fromDate, toDate);
	}

	@Override
	public MonthlyTurnOverDto getMonthlyTurnOver(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		try {
			return alertDao.getMonthlyTurnOver(fromDate, toDate, filterDto);
		} catch (Exception exception) {
			exception.printStackTrace();
			return new MonthlyTurnOverDto();
		}
	}

	@Override
	public Map<String, AlertComparisonReturnObject> alertComparisonNotification(String fromDate, String toDate,
			FilterDto filterDto) throws ParseException {
		Map<String, AlertComparisonReturnObject> comparisonResponse = new HashMap<String, AlertComparisonReturnObject>();
		List<AlertComparisonDto> presentAlerts = new ArrayList<>();
		List<AlertComparisonDto> pastAlerts = new ArrayList<>();
		String originalToDate = toDate;
		Long differenceBetweenDates = daysCalulationUtility.getDifferenceBetweenDates(fromDate, toDate);
		Date previousAlertDate = daysCalulationUtility.getPreviousDate(fromDate, differenceBetweenDates);
		presentAlerts = alertDao.alertComparisonNotification(fromDate, toDate, null, filterDto, originalToDate);
		pastAlerts = alertDao.alertComparisonNotification(fromDate, null, previousAlertDate, filterDto, originalToDate);

		// getting all productTypes into List<String>
		Set<String> allProductTypes = new HashSet<String>();
		for (AlertComparisonDto presentAlertProductType : presentAlerts) {
			allProductTypes.add(presentAlertProductType.getProductType());
		}
		for (AlertComparisonDto presentAlertProductType : pastAlerts) {
			allProductTypes.add(presentAlertProductType.getProductType());
		}

		for (String stringProduct : allProductTypes) {
			AlertComparisonReturnObject alertComparisonReturnObject = new AlertComparisonReturnObject();
			comparisonResponse.put(stringProduct, null);
			for (AlertComparisonDto comparisonResponsePresent : presentAlerts) {

				if (stringProduct.equals(comparisonResponsePresent.getProductType()))
					alertComparisonReturnObject.setPresent(comparisonResponsePresent);
			}
			for (AlertComparisonDto comparisonResponsePast : pastAlerts) {
				if (stringProduct.equals(comparisonResponsePast.getProductType()))
					alertComparisonReturnObject.setPast(comparisonResponsePast);
			}
			if (comparisonResponse.containsKey(stringProduct))
				comparisonResponse.put(stringProduct, alertComparisonReturnObject);
		}
		return comparisonResponse;
	}

	@Override
	public List<AlertScenarioDto> amlTopScenarios(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		return alertDao.amlTopScenarios(fromDate, toDate, filterDto);
	}

	@Override
	public AlertByPeriodDto amlAlertByPeriod(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		return alertDao.amlAlertByPeriod(fromDate, toDate, filterDto);
	}

	@Override
	public AlertScenarioDto getAlertScenarioById(long id) {
		Alert alert = alertDao.find(id);
		return alertDao.getAlertScenarioById(id, alert.getAlertScenarioType());
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertResponseSendDto> getAlertsForCustomer(String fromDate, String toDate, String customerNumber) {
		List<AlertResponseSendDto> dto = new ArrayList<AlertResponseSendDto>();
		List<Alert> alertList = alertDao.getAlertsForCustomer(fromDate, toDate, customerNumber);
		try {
			for (Alert alert : alertList) {
				AlertResponseSendDto alertResponseSendDto = new AlertResponseSendDto();
				BeanUtils.copyProperties(alertResponseSendDto, alert);
				dto.add(alertResponseSendDto);
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}
		return dto;
	}

	private String postStringDataToServer(String url, String jsonString) throws Exception {
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<AlertsDto> searchAlerts(String keyword) {
		return alertDao.searchAlerts(keyword);
	}

	@Override
	@Transactional("transactionManager")
	public Long searchAlertsCount(String keyword) {
		// TODO Auto-generated method stub
		return alertDao.searchAlertsCount(keyword);
	}

	@Override
	public List<AlertAggregator> alertsAggregates(AlertAggregateVo alertAggregateVo, Long userId) {
		return alertDao.alertsAggregates(alertAggregateVo, userId);
	}

	@Override
	@Transactional("transactionManager")
	public void deleteAlerts(Long id) throws Exception {
		Alert alerts = alertDao.find(id);
		if (null != alerts) {
			alertDao.delete(alerts);
		} else {
			throw new Exception("Alert not found!");
		}
	}

}
