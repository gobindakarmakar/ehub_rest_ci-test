package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("User group")
public class UserGroupDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 957321153960166215L;

	@ApiModelProperty(value="ID of the group")
	private Long groupId;
    
	@ApiModelProperty(value="ID of the user")
    private Long userId;
    
	@ApiModelProperty(value="Group created by")
    private Integer createdBy;
    
	@ApiModelProperty(value="Description of the group")
    private String description;
    
	@ApiModelProperty(value="Name of the group")
    private String groupName;
	
    public UserGroupDto() {
    	super();
    }
    
    public UserGroupDto(Long groupId, Long userId){
    	super();
    	this.groupId = groupId;
    	this.userId = userId;
    }
    
    public UserGroupDto(Long userId){
    	super();
    	this.userId = userId;
    }
    
    public UserGroupDto(Integer createdBy, String description){
    	super();
    	this.createdBy = createdBy;
    	this.description = description;
    }
    
    public UserGroupDto(Long groupId, String groupName) {
    	super();
    	this.groupId = groupId;
    	this.groupName = groupName;
    }
    
    public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
