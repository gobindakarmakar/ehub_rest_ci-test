package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

public class EntityAggregatesDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private List<BucketsDto> buckets;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BucketsDto> getBuckets() {
		return buckets;
	}

	public void setBuckets(List<BucketsDto> buckets) {
		this.buckets = buckets;
	}

}
