package element.bst.elementexploration.test.extention.transactionintelligence.service;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonReturnObject;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertResponseSendDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.AlertServiceImpl;
import element.bst.elementexploration.rest.util.DaysCalculationUtility;

@RunWith(MockitoJUnitRunner.class)
public class AlertServiceTest {

	@Mock
	AlertDao alertDaoMock;

	@Mock
	DaysCalculationUtility daysCalulationUtility;

	@InjectMocks
	AlertServiceImpl alertService;

	private MonthlyTurnOverDto monthlyTurnOverDto;

	private Map<String, AlertComparisonReturnObject> map;

	private AlertScenarioDto alertScenarioDto;

	private List<AlertResponseSendDto> alertResponseDtoList;

	private List<AlertScenarioDto> alertScenarioList;

	private List<AlertStatusDto> alertStatusDtoList;

	private AlertByPeriodDto alertByPeriodDto;

	private List<AlertComparisonDto> alertComparisonDtoList = new ArrayList<AlertComparisonDto>();

	@Before
	public void setupMock() {
		monthlyTurnOverDto = new MonthlyTurnOverDto();
		monthlyTurnOverDto.setAboveTenMillionUSD(2L);
		monthlyTurnOverDto.setBelowOneMillionUSD(33L);
		monthlyTurnOverDto.setBwFiveToTenMillionUSD(55L);
		monthlyTurnOverDto.setBwOneToFiveMillionUSD(99L);

		map = new HashMap<>();
		AlertComparisonReturnObject alertComparisonReturnObject = new AlertComparisonReturnObject();
		AlertComparisonDto present = new AlertComparisonDto("CURRENCY", 2L);
		AlertComparisonDto past = new AlertComparisonDto("CURRENCY", 1L);
		alertComparisonReturnObject.setPast(past);
		alertComparisonReturnObject.setPresent(present);
		alertComparisonDtoList.add(past);
		alertComparisonDtoList.add(present);
		map.put("CURRENCY", alertComparisonReturnObject);

		alertScenarioDto = new AlertScenarioDto(1L, "Transaction amount is more than 25000", new Double(10000),
				"amount", "ransaction amount is more than 25000", new Double(0));

		alertResponseDtoList = new ArrayList<>();
		AlertResponseSendDto alertResponseSendDto = new AlertResponseSendDto(new Date(), 20, "jurisdiction",
				"searchName", true, "statusCd", "focalNtityDisplayId", "focalNtityDisplayName", "scenario", "alertId",
				"accountType", 10L, new Double(10000), "alertDescription", "alertScenarioType",
				"transactionProductType", 55L, "comment");
		alertResponseDtoList.add(alertResponseSendDto);

		alertScenarioList = new ArrayList<>();
		AlertScenarioDto alertScenarioDto = new AlertScenarioDto("Transaction amount is more than 25000",
				new Double(100000), 10L);
		AlertScenarioDto alertScenarioDtoNew = new AlertScenarioDto("Large reportable wired transaction",
				new Double(10000), 50L);
		alertScenarioList.add(alertScenarioDto);
		alertScenarioList.add(alertScenarioDtoNew);

		alertStatusDtoList = new ArrayList<>();
		AlertStatusDto alertStatusDto = new AlertStatusDto(AlertStatus.OPEN, 10L);
		AlertStatusDto alertStatusDtoNew = new AlertStatusDto(AlertStatus.CLOSE, 5L);
		alertStatusDtoList.add(alertStatusDto);
		alertStatusDtoList.add(alertStatusDtoNew);

		alertByPeriodDto = new AlertByPeriodDto();
		alertByPeriodDto.setGreaterThirtyDaysCount(10L);
		alertByPeriodDto.setTenToTwentyCount(50L);
		alertByPeriodDto.setTwentyToThirtyCount(15L);

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchGroupByAlertStatusTest() throws ParseException {
		Mockito.when(alertDaoMock.fetchGroupByAlertStatus(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyBoolean(), Mockito.any(FilterDto.class))).thenReturn(alertStatusDtoList);

		List<AlertStatusDto> alertStatusDtoList = alertService.fetchGroupByAlertStatus(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, new FilterDto());
		assertEquals(2, alertStatusDtoList.size());
	}

	@Test
	public void getMonthlyTurnOverTest() throws ParseException {
		Mockito.when(
				alertDaoMock.getMonthlyTurnOver(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(monthlyTurnOverDto);

		MonthlyTurnOverDto resultMonthlyTurnOverDto = alertService.getMonthlyTurnOver(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(2L, resultMonthlyTurnOverDto.getAboveTenMillionUSD());
	}

	@Test
	public void alertComparisonNotificationTest() throws ParseException {
		Mockito.when(daysCalulationUtility.getDifferenceBetweenDates(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(3L);
		Mockito.when(daysCalulationUtility.getPreviousDate(Mockito.anyString(), Mockito.anyLong()))
				.thenReturn(new Date());
		Mockito.when(alertDaoMock.alertComparisonNotification(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(Date.class), Mockito.any(FilterDto.class), Mockito.anyString()))
				.thenReturn(alertComparisonDtoList);
		Map<String, AlertComparisonReturnObject> resultList = alertService.alertComparisonNotification(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(1, resultList.size());
	}

	@Test
	public void amlTopScenariosTest() throws ParseException {
		Mockito.when(
				alertDaoMock.amlTopScenarios(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(alertScenarioList);

		List<AlertScenarioDto> alertScenarioDtoList = alertService.amlTopScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(2, alertScenarioDtoList.size());
	}

	@Test
	public void amlAlertByPeriodTest() throws ParseException {
		Mockito.when(
				alertDaoMock.amlAlertByPeriod(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(alertByPeriodDto);
		AlertByPeriodDto alertByPeriodDto = alertService.amlAlertByPeriod(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(10L, alertByPeriodDto.getGreaterThirtyDaysCount());
	}

	@Test
	public void getAlertScenarioByIdTest() {
		Alert alert = new Alert();
		Mockito.when(alertDaoMock.find(Mockito.anyLong())).thenReturn(alert);
		Mockito.when(alertDaoMock.getAlertScenarioById(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(alertScenarioDto);

		AlertScenarioDto resultAlertScenarioDto = alertService.getAlertScenarioById(1L);
		assertEquals(new Long("1"), resultAlertScenarioDto.getId());
	}

	@Test
	public void getAlertsForCustomerTest() {
		List<Alert> alertList = new ArrayList<Alert>();
		Alert alert = new Alert(1L, "Test", "1234", "SG", "testing", new Date(), "test", 1, "1", "3", 1L,
				"alert comment", "Internal", AlertStatus.OPEN, "Credit card", 1L, "test", "grater amount", 85, 15000D,
				EntityType.PERSON);
		alertList.add(alert);
		Mockito.when(alertDaoMock.getAlertsForCustomer(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
				.thenReturn(alertList);
		List<AlertResponseSendDto> alertResponseSendDtoList = alertService.getAlertsForCustomer(new Date().toString(),
				new Date().toString(), "12345");
		assertEquals(1, alertResponseSendDtoList.size());
	}

	@Test
	public void searchAlertsTest() {
		List<AlertsDto> alertsDtoList = new ArrayList<AlertsDto>();
		alertsDtoList.add(new AlertsDto("test", "1234", "test", new Date(), 1L, "test comment", "internal",
				AlertStatus.OPEN, "credit card", 2L, "test description", "Large amount", 90, 600000D));
		Mockito.when(alertDaoMock.searchAlerts(Mockito.anyString())).thenReturn(alertsDtoList);
		List<AlertsDto> alertDtoList = alertService.searchAlerts("test");
		assertEquals(1, alertDtoList.size());
	}

	@Test
	public void searchAlertsCountTest() {
		Mockito.when(alertDaoMock.searchAlertsCount(Mockito.anyString())).thenReturn(2L);
		Long count = alertService.searchAlertsCount("test");
		assertEquals(new Long("2"), count);

	}
}
