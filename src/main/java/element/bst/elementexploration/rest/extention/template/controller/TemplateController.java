package element.bst.elementexploration.rest.extention.template.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.template.service.TemplateService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Template API" },description="Manages templates")
@RestController
@RequestMapping("/api/templates")
public class TemplateController extends BaseController {
	@Autowired
	private TemplateService templateService;

	@ApiOperation("Gets template by name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = byte[].class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/{template-name}/generate", produces = MediaType.MULTIPART_FORM_DATA_VALUE, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getTemplateByName(@RequestBody String requestBody,
			@PathVariable("template-name") String templateName, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(templateService.getTemplateByName(requestBody, templateName), HttpStatus.OK);
	}

	@ApiOperation("Saves a template")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String[].class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/{template-name}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveTemplate(@RequestParam MultipartFile uploadFile,
			@PathVariable("template-name") String templateName, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(templateService.saveTemplate(uploadFile, templateName), HttpStatus.OK);
	}
}
