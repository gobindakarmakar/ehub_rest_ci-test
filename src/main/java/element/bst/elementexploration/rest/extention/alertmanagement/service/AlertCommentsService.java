package element.bst.elementexploration.rest.extention.alertmanagement.service;

import element.bst.elementexploration.rest.extention.alertmanagement.domain.AlertComments;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Prateek
 *
 */

public interface AlertCommentsService extends GenericService<AlertComments, Long>{

}
