package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Risks count and ratio")
public class RiskCountAndRatioDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("Type of transaction")
	private String type;
	
	@ApiModelProperty("Transaction amount")
	private Double transactionAmount;
	
	@ApiModelProperty("Alerted transaction amount")
	private Double alertAmount;
	
	@ApiModelProperty("Transaction count")
	private Long transactionCount;
	
	@ApiModelProperty("Alerted transaction count")
	private Long alertCount;
	
	@ApiModelProperty("Alerted transaction ratio")
	private Double alertRatio;
	
	@ApiModelProperty("List of scenarios")
	private Set<String> scenario;
	
	@ApiModelProperty("Transaction business date")
	private Date businessDate;
	
	@ApiModelProperty("Latitude of location")
	private String latitude;
	
	@ApiModelProperty("Longitude of location")
	private String longitude;;


	public RiskCountAndRatioDto() {
		super();
	}

	public RiskCountAndRatioDto(Double alertAmount, Long alertCount) {
		super();
		this.alertAmount = alertAmount;
		this.alertCount = alertCount;
	}
	public RiskCountAndRatioDto(String type,Double alertAmount, Long alertCount) {
		super();
		this.type = type;
		this.alertAmount = alertAmount;
		this.alertCount = alertCount;
	}
	

	public RiskCountAndRatioDto(Double transactionAmount, Long alertCount, Double alertRatio, Long transactionCount) {
		super();
		this.transactionAmount = transactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionCount = transactionCount;
	}

	public RiskCountAndRatioDto(String type, Double transactionAmount, Long alertCount, Double alertRatio,
			Long transactionCount) {
		super();
		this.type = type;
		this.transactionAmount = transactionAmount;
		this.alertCount = alertCount;
		this.alertRatio = alertRatio;
		this.transactionCount = transactionCount;
	}

	public RiskCountAndRatioDto(Date businessDate, Double transactionAmount) {
		super();
		this.businessDate = businessDate;
		this.transactionAmount = transactionAmount;
	}

	public RiskCountAndRatioDto(Double alertAmount, Date businessDate) {
		super();
		this.alertAmount = alertAmount;
		this.businessDate = businessDate;
	}

	public RiskCountAndRatioDto(String type, String latitude, String longitude, Double alertAmount, Long alertCount) {
		super();
		this.type = type;
		this.latitude = latitude;
		this.longitude = longitude;
		this.alertAmount = alertAmount;
		this.alertCount = alertCount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Double getAlertAmount() {
		return alertAmount;
	}

	public void setAlertAmount(Double alertAmount) {
		this.alertAmount = alertAmount;
	}

	public Long getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(Long transactionCount) {
		this.transactionCount = transactionCount;
	}

	public Long getAlertCount() {
		return alertCount;
	}

	public void setAlertCount(Long alertCount) {
		this.alertCount = alertCount;
	}

	public Double getAlertRatio() {
		return alertRatio;
	}

	public void setAlertRatio(Double alertRatio) {
		this.alertRatio = alertRatio;
	}

	public Set<String> getScenario() {
		return scenario;
	}

	public void setScenario(Set<String> scenario) {
		this.scenario = scenario;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "RiskCountAndRatioDto [type=" + type + ", transactionAmount=" + transactionAmount + ", alertAmount="
				+ alertAmount + ", transactionCount=" + transactionCount + ", alertCount=" + alertCount
				+ ", alertRatio=" + alertRatio + ", scenario=" + scenario + ", businessDate=" + businessDate
				+ ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
}
