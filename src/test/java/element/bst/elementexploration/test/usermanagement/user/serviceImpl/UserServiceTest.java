package element.bst.elementexploration.test.usermanagement.user.serviceImpl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.hibernate.HibernateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.usermanagement.user.dao.UserDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.serviceImpl.UserServiceImpl;
/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
//@TestPropertySource(locations = "classpath:user.properties")
//@PropertySources("classpath:user.properties")
public class UserServiceTest {
	
	@Mock
	UserDao userDaoMock;
	
	@Mock
	CloseableHttpClient httpClient;
	
	@InjectMocks
	UserServiceImpl userService;
	
	public User user;
	
	public List<UserDto> userDtoList;
	
	@Before
	public void doSetup() {
		user=new User("analyst","Analyst","Analyst",new Date(),"1");
		userDtoList = new ArrayList<UserDto>();
		UserDto firstUser = new UserDto(1L, "analyst@gmail.com", "analyst","analyst", "analyst",
				"analyst", new Date(), "UK", "1", new Date());
		UserDto secondUser = new UserDto(2L, "admin@gmail.com", "admin","admin", "admin",
				"admin", new Date(), "UK", "1", new Date());
		userDtoList.add(firstUser);
		userDtoList.add(secondUser);
		MockitoAnnotations.initMocks(this);
	}
	
		
	@SuppressWarnings("unchecked")
	@Test(expected = HibernateException.class)
	public void whenUserDtoIsNull_ThenExIsThrown(){		
		Mockito.when(userDaoMock.find(isNull(Long.class))).thenThrow(HibernateException.class);
		UserDto userDto = new UserDto();
		userService.setUserLastLoginDateAndIp(userDto,"1.1.1.1");	
		
	}
	
	/*@Test
	public void getPropertiesTest(){
		Properties properties = userService.getProperties();
		Assert.assertNotNull(properties);
	}*/
	
	
	@Test(expected = RuntimeException.class)
	public void sendEmailVerification() throws ClientProtocolException, IOException{
		Mockito.doNothing().doThrow(new RuntimeException()).when(httpClient).execute(Mockito.any(HttpPost.class));
		userService.sendEmailVerification(1L, "analyst", "http://localhost", "a1d2c3", "analyst@mail.com");
	}
	
	@Test
	public void activateUserTest() {
		Mockito.doNothing().when(userDaoMock).saveOrUpdate(Mockito.any(User.class));
		user.setStatus("0");
		assertEquals("0", user.getStatus());
		boolean status = userService.activateUser(user);
		assertEquals(true, status);
		assertEquals("1", user.getStatus());
	}
	
	@Test(expected = HibernateException.class)
	public void activateUser_withExTest() {
		Mockito.doThrow(HibernateException.class).when(userDaoMock).saveOrUpdate(Mockito.any(User.class));		
		userService.activateUser(user);
	}
	
	@Test
	public void deactivateUserTest() {
		Mockito.doNothing().when(userDaoMock).saveOrUpdate(Mockito.any(User.class));		
		boolean status = userService.deactivateUser(user);
		assertEquals(true, status);
		assertEquals("0", user.getStatus());
	}
	
	@Test(expected = HibernateException.class)
	public void deactivateUser_withExTest() {
		Mockito.doThrow(HibernateException.class).when(userDaoMock).saveOrUpdate(Mockito.any(User.class));		
		userService.deactivateUser(user);
	}
	
	@Test
	public void getUserListTest(){		
		Mockito.when(userDaoMock.getUserList(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(userDtoList);
		List<UserDto> newUserDtoList = userService.getUserList(1L, "", 1, 10,"name", "desc");
		assertEquals(2,newUserDtoList.size());
		
	}
	
	
	@Test
	public void isActiveUserTest(){		
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		boolean status = userService.isActiveUser(1L);
		assertEquals(true,status);
	}	
	
	@Test
	public void whenUserIsNull_isActiveUserTest(){		
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(null);
		boolean status =userService.isActiveUser(null);
		assertEquals(true,status);
	}
	
	@Test
	public void whenUserStatusIsNull_isActiveUserTest(){	
		user.setStatus(null);
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		boolean status =userService.isActiveUser(null);
		assertEquals(false,status);
	}
	
	@Test
	public void whenUserStatusIs0_isActiveUserTest(){	
		user.setStatus("0");
		Mockito.when(userDaoMock.find(Mockito.anyLong())).thenReturn(user);
		boolean status =userService.isActiveUser(null);
		assertEquals(false,status);
	}
	
	
	
	@Test
	public void listUserByGroupTest(){		
		Mockito.when(userDaoMock.listUsersByGroup(Mockito.anyLong(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(userDtoList);
		List<UserDto> newUserDtoList = userService.listUserByGroup(1L,1,1,5,"name","desc");
		assertEquals(2,newUserDtoList.size());
		
	}
	
	@Test
	public void getUserByEmailOrUsernameTest(){		
		Mockito.when(userDaoMock.getUserByEmailOrUsername(Mockito.anyString())).thenReturn(user);
		UserDto userByEmail = userService.getUserByEmailOrUsername("analyst@gmail.com");
		assertEquals("analyst",userByEmail.getScreenName());
	}
	
	@Test
	public void whenEmailOrUsernameIsNullTest(){		
		//Mockito.when(userDaoMock.getUserByEmailOrUsername(isNull(String.class))).thenThrow(InsufficientDataException.class);
		UserDto userByEmail = userService.getUserByEmailOrUsername(null);	
		assertEquals(null,userByEmail);
	}
	
	@Test
	public void whenUserIsNull_getUserByEmailOrUsernameTest(){		
		Mockito.when(userDaoMock.getUserByEmailOrUsername(Mockito.anyString())).thenReturn(null);
		UserDto userByEmail = userService.getUserByEmailOrUsername("analyst@gmail.com");
		assertEquals(null,userByEmail);
	}
	
	
	@Test
	public void getUserObjectByEmailOrUsernameTest(){		
		Mockito.when(userDaoMock.getUserByEmailOrUsername(Mockito.anyString())).thenReturn(user);
		User userByEmail = userService.getUserObjectByEmailOrUsername("analyst@gmail.com");
		assertEquals("analyst",userByEmail.getScreenName());
	}
	
	@Test
	public void getUserObjectByEmailOrUsernameNullTest(){		
		//Mockito.when(userDaoMock.getUserByEmailOrUsername(isNull(String.class))).thenThrow(InsufficientDataException.class);
		User userByEmail = userService.getUserObjectByEmailOrUsername(null);	
		assertEquals(null,userByEmail);
	}

	@Test
	public void countUserListTest(){		
		Mockito.when(userDaoMock.countUserList(Mockito.anyLong(),Mockito.anyString())).thenReturn(2L);
		Long userCount = userService.countUserList(1L,"1");
		Assert.assertNotNull(userCount);
	}
	
	@Test
	public void countUserListFullTextSearchTest(){		
		Mockito.when(userDaoMock.countUserListFullTextSearch(Mockito.anyString(),Mockito.anyString(),Mockito.anyLong())).thenReturn(2L);
		Long userCount = userService.countUserListFullTextSearch("analyst","1",2L);	
		Assert.assertNotNull(userCount);
	}
	
	@Test
	public void checkUserNameOrEmailExistsTest(){		
		Mockito.when(userDaoMock.checkUserNameOrEmailExists(Mockito.anyString(),Mockito.anyLong())).thenReturn(user);
		UserDto userByEmail = userService.checkUserNameOrEmailExists("analyst@gmail.com",2L);
		assertEquals("analyst",userByEmail.getScreenName());
	}
	
	@Test
	public void checkUserNameOrEmailNullExistsTest(){		
		//Mockito.when(userDaoMock.getUserByEmailOrUsername(isNull(String.class))).thenThrow(InsufficientDataException.class);
		UserDto userByEmail = userService.checkUserNameOrEmailExists(null,null);	
		assertEquals(null,userByEmail);
	}
	
	@Test
	public void whenUserIsNull_CheckUserNameOrEmailExistsTest(){		
		Mockito.when(userDaoMock.checkUserNameOrEmailExists(Mockito.anyString(),Mockito.anyLong())).thenReturn(null);
		UserDto userByEmail = userService.checkUserNameOrEmailExists("analyst@gmail.com",2L);
		assertEquals(null,userByEmail);
	}

	
	
	@Test
	public void countUserByGroupTest(){		
		Mockito.when(userDaoMock.countUserByGroup(Mockito.anyLong(),Mockito.anyInt())).thenReturn(2L);
		Long userCount = userService.countUserByGroup(2L,1);	
		Assert.assertNotNull(userCount);
	}
}
