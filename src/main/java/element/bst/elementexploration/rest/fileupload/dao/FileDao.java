package element.bst.elementexploration.rest.fileupload.dao;

import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface FileDao extends GenericDao<FileBst, Long>{

	 FileBst findFileByName(String fileName);

}
