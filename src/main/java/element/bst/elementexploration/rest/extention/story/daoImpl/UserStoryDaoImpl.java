package element.bst.elementexploration.rest.extention.story.daoImpl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.story.dao.UserStoryDao;
import element.bst.elementexploration.rest.extention.story.domain.UserStory;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

@Repository("storyDao")
public class UserStoryDaoImpl extends GenericDaoImpl<UserStory, Long> implements UserStoryDao {

	public UserStoryDaoImpl() {
		super(UserStory.class);
	}

	@Override
	public UserStory getFavouriteStoryByUserId(Long userId) {
		UserStory story = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<UserStory> query = builder.createQuery(UserStory.class);
			Root<UserStory> root = query.from(UserStory.class);
			query.select(root).where(builder.equal(root.get("userId"), userId));
			Query<UserStory> q = this.getCurrentSession().createQuery(query);
			story = q.getSingleResult();

		} catch (Exception e) {
			return story;
		}
		return story;
	}

}
