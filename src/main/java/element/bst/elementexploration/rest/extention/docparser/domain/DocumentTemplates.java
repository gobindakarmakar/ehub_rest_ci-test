package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Rambabu
 *
 */
@Entity
@Table(name = "DOCUMENT_TEMPLATES")
public class DocumentTemplates implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String templateName;
	private String templateFileType;
	private boolean isScannedPdf = false;
	private boolean isFillablePdf = false;
	private Date createdOn;
	private Date modifiedOn;
	private Long createdBy;
	private Long modifiedBy;
	private int scannedValue = 0;
	private Long initialDocId;
	private boolean deletedStatus = false;
	private boolean docParserTemplate = false;
	private boolean isNormalPdf = false;

	public DocumentTemplates() {
		super();

	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "TEMPLATE_NAME")
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	@Column(name = "TEMPLATE_FILE_TYPE")
	public String getTemplateFileType() {
		return templateFileType;
	}

	public void setTemplateFileType(String templateFileType) {
		this.templateFileType = templateFileType;
	}

	@Column(name = "SCANNED_PDF")
	public boolean isScannedPdf() {
		return isScannedPdf;
	}

	public void setScannedPdf(boolean isScannedPdf) {
		this.isScannedPdf = isScannedPdf;
	}

	public int getScannedValue() {
		return scannedValue;
	}

	public void setScannedValue(int scannedValue) {
		this.scannedValue = scannedValue;
	}

	@Column(name = "FILLABLE_PDF")
	public boolean isFillablePdf() {
		return isFillablePdf;
	}

	public void setFillablePdf(boolean isFillablePdf) {
		this.isFillablePdf = isFillablePdf;
	}

	@Column(name = "CREATED_ON")
	@CreationTimestamp
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "MODIFIED_ON")
	@UpdateTimestamp
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "CREATED_BY")
	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "MODIFIED_BY")
	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "INITIAL_DOC_ID")
	public Long getInitialDocId() {
		return initialDocId;
	}

	public void setInitialDocId(Long initialDocId) {
		this.initialDocId = initialDocId;
	}

	@Column(name = "DOC_PARSE_STATUS")
	public boolean isDocParserTemplate() {
		return docParserTemplate;
	}

	public void setDocParserTemplate(boolean docParserTemplate) {
		this.docParserTemplate = docParserTemplate;
	}

	@Column(name = "IS_DELETED")
	public boolean isDeletedStatus() {
		return deletedStatus;
	}

	public void setDeletedStatus(boolean deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	
	@Column(name = "NORMAL_PDF")
	public boolean isNormalPdf() {
		return isNormalPdf;
	}

	public void setNormalPdf(boolean isNormalPdf) {
		this.isNormalPdf = isNormalPdf;
	}

}
