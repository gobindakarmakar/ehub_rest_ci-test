package element.bst.elementexploration.rest.extention.geoencoder.dao;

import element.bst.elementexploration.rest.extention.geoencoder.dto.CountryMasterData;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface CountryMasterDataDao extends GenericDao<CountryMasterData, Long> {
	
	
	CountryMasterData findCountry(String coutryName);

}
