package element.bst.elementexploration.rest.extention.mip.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.mip.domain.OrgWithFacts;
import element.bst.elementexploration.rest.extention.mip.service.MarketIntelligenceService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Market intelligence API" }, description="Manages market intelligence data")
@RestController
@RequestMapping("/api/marketIntelligence")
public class MarketIntelligenceController extends BaseController{
	
	@Autowired
	private MarketIntelligenceService marketIntelligenceService;

	@ApiOperation("Gets organization data with facts")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/search/orgWithFacts", consumes = {"application/json; charset=UTF-8" }, 
			produces = {"application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrgWithFacts(@RequestBody OrgWithFacts orgWithFacts,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(marketIntelligenceService.getOrgWithFacts(orgWithFacts), HttpStatus.OK);
	}

	/*@ApiOperation("Searches for the queried entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/entity/search", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchEntity(@RequestParam("entity") String entity, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(marketIntelligenceService.searchEntity(entity), HttpStatus.OK);
	}*/
}
