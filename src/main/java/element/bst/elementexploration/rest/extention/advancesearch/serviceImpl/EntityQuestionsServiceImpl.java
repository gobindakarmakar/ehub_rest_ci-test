package element.bst.elementexploration.rest.extention.advancesearch.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.extention.advancesearch.dao.EntityQuestionsDao;
import element.bst.elementexploration.rest.extention.advancesearch.domain.EntityQuestions;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityQuestionsDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.EntityQuestionsService;
import element.bst.elementexploration.rest.extention.questionnairebuilder.service.QuestionnaireBuilderService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author rambabu
 *
 */
@Service("entityQuestionsService")
public class EntityQuestionsServiceImpl extends GenericServiceImpl<EntityQuestions, Long> implements EntityQuestionsService {


	@Autowired
	EntityQuestionsDao entityQuestionsDao;
	
	@Autowired
	QuestionnaireBuilderService questionnaireBuilderService;
	
	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;
	
	public EntityQuestionsServiceImpl(GenericDao<EntityQuestions, Long> genericDao) {
		super(genericDao);
	}

	@Override
	@Transactional("transactionManager")
	public boolean saveEntityQuestions(List<EntityQuestionsDto> entityQuestionDtoList, String surveyId,Long userId) {
		if(userId == null)
			throw new InsufficientDataException("User not found");
		try{
			String response = questionnaireBuilderService.getQuestionsBySurveyId(surveyId);
			if (response != null) {
				JSONObject data = new JSONObject(response);
				if (data.has("result")) {
					JSONArray results = data.getJSONArray("result");
					for (int i = 0; i < results.length(); i++) {
						JSONObject question = results.getJSONObject(i);
						if (question.has("question")) {
							out: for (int j = 0; j < entityQuestionDtoList.size(); j++) {
								if (question.getString("question")
										.equals(entityQuestionDtoList.get(j).getQuestionName())) {
									if (question.has("help")&& !question.get("help").equals(null)) {
										String help = question.getString("help");
										if (help != null && help.trim().length() != 0)
											entityQuestionDtoList.get(j).setSchemaAttribute(help);
										break out;
									}
								}
							}
						}
					}
				}
			}
			if (entityQuestionDtoList != null && entityQuestionDtoList.size() > 0) {
				entityQuestionsDao.deleteExistingEntityAnswersByUser(entityQuestionDtoList.get(0).getEntityId(),
						Long.parseLong(surveyId), userId);
			}
		for(EntityQuestionsDto entityQuestionsDto : entityQuestionDtoList){
		/*	EntityQuestions eixstedEntityQuestion = entityQuestionsDao.getEntityQuestionByNameAndEntityId(entityQuestionsDto.getEntityId(),entityQuestionsDto.getQuestionDivId(),userId,Long.parseLong(surveyId));
			if(eixstedEntityQuestion != null){
				entityQuestionsDao.delete(eixstedEntityQuestion);
			}*/
			
			
			 EntityQuestions entityQuestionsNew = new EntityQuestions();
			 BeanUtils.copyProperties(entityQuestionsDto, entityQuestionsNew);
			 entityQuestionsNew.setCreatedBy(userId);
			 entityQuestionsNew.setCreatedDate(new Date());
			 entityQuestionsNew.setSurveyId(Long.parseLong(surveyId));
			 entityQuestionsDao.create(entityQuestionsNew);
		}
		return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<EntityQuestionsDto> getEntityQuestionsByEntityId(String entityId,Long surveyId) {
		List<EntityQuestions> entityQuestionsList = entityQuestionsDao.getEntityQuestionByEntityId(entityId,surveyId);
		List<EntityQuestionsDto> entityQuestionsDtoList = new ArrayList<EntityQuestionsDto>();
		for(EntityQuestions entityQuestion : entityQuestionsList){
			EntityQuestionsDto entityQuestionsDto = new EntityQuestionsDto();
			BeanUtils.copyProperties(entityQuestion, entityQuestionsDto);
			if (entityQuestion.getCreatedBy() != null) {
				Users user = usersDao.find(entityQuestion.getCreatedBy());
				user = usersService.prepareUserFromFirebase(user);
				if (user != null) {
					entityQuestionsDto.setUserName(user.getFirstName()+" "+user.getLastName());
				}
			}
			entityQuestionsDtoList.add(entityQuestionsDto);
		}
		return entityQuestionsDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public List<EntityQuestionsDto> getEntityQuestionsByEntityIdAndUserId(String entityId, Long userId,Long surveyId) {
		Users user = null;
		List<EntityQuestionsDto> entityQuestionsDtoList = new ArrayList<EntityQuestionsDto>();
		if (userId != null) {
			user = usersDao.find(userId);
			user = usersService.prepareUserFromFirebase(user);
		}
		if (user == null) {
			throw new InsufficientDataException("User not found");
		} else {
			List<EntityQuestions> entityQuestionsList = entityQuestionsDao
					.getEntityQuestionByEntityIdAndUserId(entityId, userId,surveyId);
			if(entityQuestionsList.size()!=0){
			for (EntityQuestions entityQuestion : entityQuestionsList) {
				EntityQuestionsDto entityQuestionsDto = new EntityQuestionsDto();
				BeanUtils.copyProperties(entityQuestion, entityQuestionsDto);
				entityQuestionsDto.setUserName(user.getFirstName()+" "+user.getLastName());
				entityQuestionsDtoList.add(entityQuestionsDto);
			}
			}else{
				String response = questionnaireBuilderService.getQuestionsBySurveyId(surveyId.toString());
				if (response != null) {
					JSONObject data = new JSONObject(response);
					if (data.has("result")) {
						JSONArray results = data.getJSONArray("result");
						for (int i = 0; i < results.length(); i++) {
							JSONObject question = results.getJSONObject(i);
							if(question.has("help")){
								if(!question.get("help").equals(null)&&!question.getString("help").isEmpty()){
									EntityQuestionsDto entityQuestions= new EntityQuestionsDto();
									entityQuestions.setSchemaAttribute(question.getString("help"));
									if(question.has("type")&&question.has("qid")){
										if(!question.getString("type").isEmpty() && !question.getString("qid").isEmpty()){
											if(question.has("qid")){
												entityQuestions.setQuestionDivId("question"+question.getString("qid"));
											}
											JSONObject result = createDivId(question.getString("type"),question.getString("qid"));
											if(result.has("question_div_id")){
												entityQuestions.setQuestionOptions(result.getString("question_div_id"));
											}
											if(question.has("question")){
												if(!question.getString("question").isEmpty())
													entityQuestions.setQuestionName(question.getString("question"));	
											}
											entityQuestions.setCreatedBy(userId);
											entityQuestions.setCreatedDate(new Date());
											entityQuestions.setQuestionType(getQuestionType(question.getString("type")));
											entityQuestions.setEntityId(entityId);
										}
									}
									entityQuestionsDtoList.add(entityQuestions);
								}
							}
						}
					}
				}
			}
			return entityQuestionsDtoList;
		}
	}

	private String getQuestionType(String type) {
		String questionType=null;
		if(type != null){
			if(type.equalsIgnoreCase("S")|| type.equalsIgnoreCase("T"))
				questionType = "text";				
			if(type.equalsIgnoreCase("M"))
				questionType = "Multiple Choice Checkbox";
			if(type.equalsIgnoreCase("L"))
				questionType = "Radio Button";	
			if(type.equalsIgnoreCase("Y"))
				questionType = "Yes/No Radio Buttons";
			if(type.equalsIgnoreCase("P"))
				questionType = "Multiple Choice with comment";		
		}
		return questionType;
	}
	
	public JSONObject createDivId(String questionType,String questionId){
		String type=null;
		String qid=null;
		String gid=null;
		String sid=null;
		/*String id="";
		String text="";
		String selected="";*/
		JSONObject result= new JSONObject();
		
		JSONArray questionOptions= new JSONArray();
		String questionProperties= questionnaireBuilderService.getQuestionPropertiesById(questionId);
		JSONObject questionPropertiesJson= new JSONObject(questionProperties);
		if(questionPropertiesJson!=null){
			if((questionPropertiesJson.has("result"))&& (questionPropertiesJson.get("result") instanceof JSONObject)){
				JSONObject resultObject= questionPropertiesJson.getJSONObject("result");
				if(resultObject.has("type")){
					type=resultObject.getString("type");
				}
				if(resultObject.has("qid")){
					qid=resultObject.getString("qid");
				}
				if(resultObject.has("gid")){
					gid=resultObject.getString("gid");
				}
				if(resultObject.has("sid")){
					sid=resultObject.getString("sid");
				}
					if(type!=null && questionType.equals(type)){
						/*if(questionType.equals("T") || questionType.equals("S")){
							String questionDivId="question"+qid;
							result.put("question_div_id", questionDivId);
						}*/
						if(questionType.equals("L")){
							if(resultObject.has("answeroptions")){
								JSONObject answerOptionsJson= resultObject.getJSONObject("answeroptions");
								@SuppressWarnings("rawtypes")
								Iterator keys = answerOptionsJson.keys();
								while(keys.hasNext()){
									String text ="";
									String selected="";
									String id= "";
									String key = keys.next().toString();
									JSONObject option = answerOptionsJson.getJSONObject(key);
									if(option.has("answer"))
										text=option.getString("answer");
									 id = "#answer"+sid+"X"+gid+"X"+qid+"s"+key;
									JSONObject internalObject= new JSONObject();
									internalObject.put("id", id);
									internalObject.put("text", text);
									internalObject.put("selected", selected);
									questionOptions.put(internalObject);
								}
							}
							result.put("question_div_id",questionOptions.toString());
							
						}
						if(questionType.equals("M")||questionType.equals("P")){
							if(resultObject.has("available_answers")){
								JSONObject availableAnswersJson= resultObject.getJSONObject("available_answers");
								@SuppressWarnings("rawtypes")
								Iterator keys = availableAnswersJson.keys();
								while(keys.hasNext()){
									String keym = keys.next().toString();
									String text=availableAnswersJson.getString(keym);
									String selected="";
									String id="#answer"+sid+"X"+gid+"X"+qid+keym;
									JSONObject internalObject= new JSONObject();
									internalObject.put("id", id);
									internalObject.put("text", text);
									internalObject.put("selected", selected);
									questionOptions.put(internalObject);
								}
							}
							result.put("question_div_id",questionOptions.toString());
						}
						if(questionType.equals("Y")){
							JSONObject yesInternalObject= new JSONObject();
							String yesText="Yes";
							String yesId="#answer"+sid+"X"+gid+"X"+qid+"Y";
							String selected= "";
							String noText= "No";
							String noId= "#answer"+sid+"X"+gid+"X"+qid+"N";
							yesInternalObject.put("id", yesId);
							yesInternalObject.put("text", yesText);
							yesInternalObject.put("selected", selected);
							JSONObject noInternalObject= new JSONObject();
							noInternalObject.put("id", noId);
							noInternalObject.put("text", noText);
							noInternalObject.put("selected", selected);
							questionOptions.put(yesInternalObject);
							questionOptions.put(noInternalObject);
							result.put("question_div_id",questionOptions.toString());
						}
						/*if(questionType.equals("P")){
							
						}*/
					}
				
			}
		}
		return result;
	}
	

}
