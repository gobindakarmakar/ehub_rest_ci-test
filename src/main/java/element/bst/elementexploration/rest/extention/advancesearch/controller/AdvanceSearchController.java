package element.bst.elementexploration.rest.extention.advancesearch.controller;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.advancesearch.domain.AdvanceNewsFavourite;
import element.bst.elementexploration.rest.extention.advancesearch.dto.CustomOfficer;
import element.bst.elementexploration.rest.extention.advancesearch.dto.CustomShareHolder;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityListDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityOfficerInfoDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityQuestionsDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.EntityScreeningDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.IdentifiersRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ProfileDataDto;
import element.bst.elementexploration.rest.extention.advancesearch.dto.ShareHolderRequestDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceNewsFavouriteService;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.advancesearch.service.EntityOfficerInfoService;
import element.bst.elementexploration.rest.extention.advancesearch.service.EntityQuestionsService;
import element.bst.elementexploration.rest.extention.entity.domain.EntityAttributes;
import element.bst.elementexploration.rest.extention.entity.dto.EntityAttributeDto;
import element.bst.elementexploration.rest.extention.entity.service.EntityAttributeService;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.ClassificationsService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath Reddy G
 *
 */
@Api(tags = { "Advance Search API" })
@RestController
@RequestMapping("/api/advancesearch")
public class AdvanceSearchController extends BaseController {

	@Autowired
	AdvanceSearchService advanceSearchService;

	@Autowired
	AdvanceNewsFavouriteService advanceNewsFavouriteService;

	@Autowired
	private ClassificationsService classificationsService;

	@Autowired
	private SourcesService sourcesService;

	@Autowired
	EntityQuestionsService entityQuestionsService;

	@Autowired
	private EntityAttributeService entityAttributeService;
	
	@Autowired
	EntityOfficerInfoService entityOfficerInfoService;

	@ApiOperation("New search api with bst entity resolver")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/graph/search/entity", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getNewSearchResults(@RequestBody String newsSearchRequest,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getNewSearchResults(newsSearchRequest), HttpStatus.OK);
	}

	@ApiOperation("Finds entity by name")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/entity/{name}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntityByName(@RequestParam("token") String token, @PathVariable("name") String name,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getEntityByName(name), HttpStatus.OK);
	}

	@ApiOperation("Calls hierarchy url")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/hierarchyLink", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getHierarchyData(@RequestParam("token") String token, HttpServletRequest request,
			@RequestBody HierarchyDto hierarchyDto) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getHierarchyData(hierarchyDto, getCurrentUserId(),null),
				HttpStatus.OK);
	}

	/*
	 * @ApiOperation("Gets the organisational structure data of the entity")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, response = String.class,
	 * message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * ElementConstants.NO_DATA_FOUND_MSG),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	 * 
	 * @PostMapping(value = "/getOrgParentData", consumes = {
	 * "application/json; charset=UTF-8" }, produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * getOrgParentData(@RequestBody EntityRequestDto entityRequestDto,
	 * 
	 * @RequestParam("token") String token, HttpServletRequest request) throws
	 * Exception { return new
	 * ResponseEntity<>(advanceSearchService.getOrgParentData(entityRequestDto),
	 * HttpStatus.OK); }
	 */

	@ApiOperation("Gets the organisation data of the entity")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = EntityListDataDto.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/getOrgDataOfEntity", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrgDataOfEntity(@RequestBody EntityRequestDto entityRequestDto,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getOrgDataOfEntity(entityRequestDto), HttpStatus.OK);
	}

	@ApiOperation("Gets the profile data of the entity")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ProfileDataDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/getProfileData", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getProfileData(@RequestBody EntityRequestDto entityRequestDto,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getProfileData(entityRequestDto), HttpStatus.OK);
	}

	@ApiOperation("Gets the profile data of the entity")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ShareHolderRequestDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/getShareHoldersData", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareHoldersData(@RequestBody List<ShareHolderRequestDto> jsonString,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getShareHoldersData(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Gets the organization data of the identifier")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = IdentifiersRequestDto.class, responseContainer = "List", message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/getOrgData", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrgData(@RequestBody CopyOnWriteArrayList<IdentifiersRequestDto> identifiers,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getOrgData(identifiers), HttpStatus.OK);
	}

	@ApiOperation("Gets the profile data of the entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getShareHoloders", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getShareHoloders(@RequestParam String organizationId, @RequestParam Integer level,
			@RequestParam String fields, @RequestParam("token") String token, HttpServletRequest request)
					throws Exception {
		String response = advanceSearchService.getShareHoloders(organizationId, level, fields);
		if (response == null) {
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

		} else {
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
	}

	@ApiOperation("Get Screening Status")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getScreeningStatus", produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getScreeningStatus(@RequestParam("token") String token, HttpServletRequest request)
					throws Exception {
		String response = advanceSearchService.getScreeningStatus();
		if (response == null) {
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
	}
	
	/*
	 * @ApiOperation("Get financial information")
	 * 
	 * @ApiOperation("Get financial information")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, response = String.class,
	 * message = "Operation successful"),
	 * 
	 * @ApiResponse(code = 404, response = ResponseMessage.class, message =
	 * ElementConstants.NO_DATA_FOUND_MSG),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	 * 
	 * @PostMapping(value = "/getFinancialInfo", consumes = {
	 * "application/json; charset=UTF-8" }, produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * getFinancialInfo(@RequestBody String jsonString,
	 * 
	 * @RequestParam("token") String token, HttpServletRequest request) throws
	 * Exception { return new
	 * ResponseEntity<>(advanceSearchService.getFinancialInfo(jsonString,
	 * getCurrentUserId()), HttpStatus.OK); }
	 */

	@ApiOperation("Owenship Structure")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/ownershipStructure", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> ownershipStructure(@RequestBody HierarchyDto jsonString,
			@RequestParam(required = false) Integer maxSubsidiarielevels,
			@RequestParam(required = false) Integer lowRange, @RequestParam(required = false) Integer highRange,
			@RequestParam("token") String token, HttpServletRequest request, @RequestParam String identifier,
			@RequestParam(required = false) Integer noOfSubsidiaries, @RequestParam String organisationName,
			@RequestParam String juridiction,
			@RequestParam(value = "isSubsidiariesRequired", required = false) Boolean isSubsidiariesRequired,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "sourceType", required = true) String sourceType) throws Exception {
		maxSubsidiarielevels = maxSubsidiarielevels != null ? maxSubsidiarielevels : 5;
		lowRange = lowRange != null ? lowRange : 10;
		highRange = highRange != null ? highRange : 100;
		noOfSubsidiaries = noOfSubsidiaries != null ? noOfSubsidiaries : 5;
		Long classificationId=null;

		List<ClassificationsDto> classificationsDtos = classificationsService.getClassifications();

		for (ClassificationsDto classificationsDto : classificationsDtos) {
			if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
				classificationId = classificationsDto.getClassificationId();
			}
		}

		List<SourcesDto> sourcesDtos = sourcesService.getSources(0, 0, null, classificationId, null, null, null,
				null,false);
		//Long classificationId = null;
		//Long newsClassificationId = null;
		/*boolean isBstSource = true;
		CredibilityEnums companyHouseCredibility = CredibilityEnums.LOW;
		CredibilityEnums bstCredibility = CredibilityEnums.LOW;
		String annualReportLink = null;
		String documentUrl =null;
		List<ClassificationsDto> classificationsDtos = classificationsService.getClassifications();
		for (ClassificationsDto classificationsDto : classificationsDtos) {
			if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
				classificationId = classificationsDto.getClassificationId();
			} else if (classificationsDto.getClassifcationName().equalsIgnoreCase("NEWS")) {
				newsClassificationId = classificationsDto.getClassificationId();
			}
		}
		if (jsonString.getUrl() != "") {
			List<SourcesDto> sourcesDtos = sourcesService.getSources(0, 100, null, classificationId, null, null, null,
					null);
			List<SourcesDto> filterdSourceDtos = sourcesDtos.stream()
					.filter(sourcesDto -> (sourcesDto.getSourceDisplayName().equalsIgnoreCase("bst")
							|| sourcesDto.getSourceDisplayName().equalsIgnoreCase("company house")))
					.collect(Collectors.toList());
			for (SourcesDto sourcesDto : filterdSourceDtos) {
				if (sourcesDto.getSourceDisplayName().equalsIgnoreCase("Company House")
						|| sourcesDto.getSourceDisplayName().equalsIgnoreCase("bst")) {
					List<ClassificationsDto> classifications = sourcesDto.getClassifications();
					for (ClassificationsDto classificationsDto : classifications) {
						List<SubClassificationsDto> subClassifications = classificationsDto.getSubClassifications();
						for (SubClassificationsDto subClassificationsDto : subClassifications) {
							if (subClassificationsDto.getSubClassifcationName()
									.equalsIgnoreCase("Corporate Structure")) {
								if (sourcesDto.getSourceDisplayName().equalsIgnoreCase("Company House"))
									companyHouseCredibility = subClassificationsDto.getSubClassificationCredibility();
								else
									bstCredibility = subClassificationsDto.getSubClassificationCredibility();
							}
						}
					}
				}
			}
			if (companyHouseCredibility.equals(CredibilityEnums.HIGH)
					|| (companyHouseCredibility.equals(CredibilityEnums.MEDIUM)
							&& bstCredibility.equals(CredibilityEnums.MEDIUM))
					|| (companyHouseCredibility.equals(CredibilityEnums.MEDIUM)
							&& bstCredibility.equals(CredibilityEnums.LOW))
					|| (companyHouseCredibility.equals(CredibilityEnums.MEDIUM)
							&& bstCredibility.equals(CredibilityEnums.NONE))
					|| (companyHouseCredibility.equals(CredibilityEnums.LOW)
							&& bstCredibility.equals(CredibilityEnums.LOW))
					|| (companyHouseCredibility.equals(CredibilityEnums.LOW)
							&& bstCredibility.equals(CredibilityEnums.NONE))) {
				annualReportLink = advanceSearchService.buildMultiSourceUrl(identifier, "documents", null, null, null);
				documentUrl = advanceSearchService.getAnnualReturnsLink(annualReportLink);
				if (documentUrl != null) {
					isBstSource = false;
				}
			}
		} else {
			annualReportLink = advanceSearchService.buildMultiSourceUrl(identifier, "documents", null, null, null);
			isBstSource = false;
		}*/
		String path = "";
		String requestId = UUID.randomUUID().toString();
		//if (isBstSource) {
		path = identifier + "-" + (maxSubsidiarielevels.toString()) + "-" + (lowRange.toString()) + "-"
				+ (highRange.toString()) + "-" + (noOfSubsidiaries.toString()) + "-"+requestId;
		//} else {
		/*path = identifier + "-" + (maxSubsidiarielevels.toString()) + "-" + (lowRange.toString()) + "-"
					+ (highRange.toString()) + "-" + (noOfSubsidiaries.toString()) + "-" + "ch-" + requestId;*/
		//}
		/*
		 * if (!identifier.equalsIgnoreCase("6593007dz11o8hcxy84") &&
		 * !identifier.equalsIgnoreCase("724500pmk2a2m1sqq228") &&
		 * !identifier.equalsIgnoreCase("54930012qjwzmyhnjw95") &&
		 * !identifier.equalsIgnoreCase("5493007kcyw14yc7bi65")) {
		 */
		/*advanceSearchService.ownershipStructure(identifier, jsonString, maxSubsidiarielevels, lowRange, highRange, path,
				noOfSubsidiaries, 30, organisationName, juridiction, newsClassificationId, isBstSource,
				annualReportLink, getCurrentUserId(), isSubsidiariesRequired, startDate, endDate);*/
		advanceSearchService.ownershipStructure(identifier, jsonString, maxSubsidiarielevels, lowRange, highRange, path,
				noOfSubsidiaries, 30, organisationName, juridiction, getCurrentUserId(), isSubsidiariesRequired, startDate, endDate,false,sourcesDtos,source,sourceType);
		// }
		JSONObject json = new JSONObject();
		/*
		 * if (identifier.equalsIgnoreCase("6593007dz11o8hcxy84")) { path =
		 * "6593007dz11o8hcxy84"; } if
		 * (identifier.equalsIgnoreCase("724500pmk2a2m1sqq228")) { if (lowRange
		 * == 0 || lowRange == 1 || lowRange == 2 || lowRange == 3) path =
		 * "724500pmk2a2m1sqq228"; else if (lowRange == 4) path =
		 * "724500pmk2a2m1sqq228-4-100"; else path =
		 * "724500pmk2a2m1sqq228-10-100"; } if
		 * (identifier.equalsIgnoreCase("54930012qjwzmyhnjw95")) { if (lowRange
		 * == 0 || lowRange == 1) path = "54930012qjwzmyhnjw95-0-100"; else if
		 * (lowRange == 2) path = "54930012qjwzmyhnjw95-2-100"; else if
		 * (lowRange == 3) path = "54930012qjwzmyhnjw95-3-100"; else if
		 * (lowRange == 4) path = "54930012qjwzmyhnjw95-4-100"; else if
		 * (lowRange == 5 || lowRange == 6 || lowRange == 7) path =
		 * "54930012qjwzmyhnjw95-5-100"; else if (lowRange > 8 && lowRange <=
		 * 10) path = "54930012qjwzmyhnjw95-10-100"; else path =
		 * "54930012qjwzmyhnjw95-11-100"; } if
		 * (identifier.equalsIgnoreCase("5493007kcyw14yc7bi65")) { if (lowRange
		 * == 0) path = "5493007kcyw14yc7bi65"; else if (lowRange > 0) path =
		 * "5493007kcyw14yc7bi65-10-100"; }
		 */
		json.put("path", path);
		return new ResponseEntity<>(json.toString(), HttpStatus.OK);
	}

	@ApiOperation("Get owner structure from path")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getCorporateStructure", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOwnershipPath(@RequestParam String path, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {

		return new ResponseEntity<>(advanceSearchService.readCorporateStructure(path), HttpStatus.OK);
	}

	@ApiOperation("delete file from directory")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/deleteFiles", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteFiles(@RequestParam(required = false) String path,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.deleteFiles(path), HttpStatus.OK);
	}

	@ApiOperation("Save Advance new favourite")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Significant news saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveAdvanceNewsFavourite", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveAdvanceNewsFavourite(@Valid @RequestBody AdvanceNewsFavourite advanceNewsFavourite,
			@RequestParam String token) throws Exception {
		return new ResponseEntity<>(
				advanceNewsFavouriteService.saveAdvanceNewsFavourite(advanceNewsFavourite, getCurrentUserId()),
				HttpStatus.OK);
	}

	@ApiOperation("Deletes the AdvanceNewsFavourite")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "AdvanceNewsFavourite deleted successfully.") })
	@GetMapping(value = "/deleteAdvanceNewsFavourite", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteSignificantNews(@Valid @RequestParam String entityId,
			@RequestParam("token") String token, HttpServletRequest request) {
		int result = advanceNewsFavouriteService.deleteAdvanceNewsFavourite(entityId);
		if (result != 0) {
			return new ResponseEntity<>(new ResponseMessage("AdvanceNewsFavourite  deleted successfully."),
					HttpStatus.OK);
		} else {
			throw new NoDataFoundException();
		}
	}

	@ApiOperation("Get Multisource data of entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/entity/multisource", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getMultisourceData(@RequestParam("token") String token, @RequestParam String query,
			@RequestParam(required = false) String jurisdiction, @RequestParam(required = false) String website,
			HttpServletRequest request) throws Exception {

		return new ResponseEntity<>(
				advanceSearchService.getMultisourceData(query, jurisdiction, website, getCurrentUserId()),
				HttpStatus.OK);
	}

	@ApiOperation("Fetches data from links in multisource api")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/entity/fetchLinkData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fetchLinkData(@RequestParam("token") String token, @RequestParam String identifier, @RequestParam String jurisdiction,
			@RequestParam String fields, @RequestParam(required = false) String graph,@RequestParam(required = true) String sourceType, HttpServletRequest request)
					throws Exception {
		return new ResponseEntity<>(advanceSearchService.fetchLinkData(identifier, jurisdiction, fields, graph, getCurrentUserId(),sourceType),

				HttpStatus.OK);
	}

	@ApiOperation("Get Organizaton Screening")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getEntityScreening", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getScreening(@RequestParam("token") String token,
			@RequestParam(required = true) String name, @RequestParam(required = true) String country,
			@RequestParam(required = true) String entityType, @RequestParam(required = false) String webSite,
			@RequestParam(required = false) String dob, @RequestParam(required = false) String identifier,
			@RequestParam(required = false) String requestId, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(
				advanceSearchService.getEntityScreening(name, country, entityType, webSite, dob, identifier, requestId),
				HttpStatus.OK);
	}

	@ApiOperation("Save entity question")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Significant news saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveEntityQuestions", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveEntityQuestions(@Valid @RequestBody List<EntityQuestionsDto> entityQuestionDtoList,
			@RequestParam(required = true) String surveyId, @RequestParam String token, HttpServletRequest request)
					throws Exception {
		Long userId = getCurrentUserId();
		boolean status = entityQuestionsService.saveEntityQuestions(entityQuestionDtoList, surveyId, userId);
		if (status)
			return new ResponseEntity<>(status, HttpStatus.OK);
		else
			return new ResponseEntity<>(new ResponseMessage("Failed to create question"),
					HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation("Get entity questions")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getEntityQuestionsByEntityId", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntityQuestionsByEntityId(@RequestParam("token") String token,
			@RequestParam(required = true) String entityId, @RequestParam(required = true) boolean isUserBase,
			@RequestParam(required = true) Long surveyId, HttpServletRequest request) throws Exception {
		Long userId = getCurrentUserId();
		if (isUserBase) {
			//enable this to get userbased comments
			/*return new ResponseEntity<>(
					entityQuestionsService.getEntityQuestionsByEntityIdAndUserId(entityId, userId, surveyId),
					HttpStatus.OK);*/
			return new ResponseEntity<>(entityQuestionsService.getEntityQuestionsByEntityId(entityId,surveyId), HttpStatus.OK);

		} else {
			return new ResponseEntity<>(entityQuestionsService.getEntityQuestionsByEntityId(entityId,surveyId), HttpStatus.OK);
		}

	}

	@ApiOperation("Get screen shot")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getScreenShotData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getScreenShotData(@RequestParam("token") String token,
			@RequestParam(required = true) String entityId, @RequestParam(required = true) String url,
			@RequestParam(required = true) String sourceName, HttpServletRequest request) throws Exception {

		return new ResponseEntity<>(advanceSearchService.getScreenShotData(entityId, sourceName, url), HttpStatus.OK);
	}

	@ApiOperation("Save Entity Attributes")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Company Attributes saved successfully"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveEntityAttributes", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveEntityAttributes(@Valid @RequestBody EntityAttributeDto entityAttributes,
			@RequestParam String token) throws Exception {
		EntityAttributes savedEntityAttributes = entityAttributeService.saveEntityAttributes(entityAttributes,
				getCurrentUserId());
		return new ResponseEntity<>(savedEntityAttributes.getId(), HttpStatus.OK);

	}

	@ApiOperation("Select Entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/selectEntity", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> selectEntity(@RequestParam("token") String token,
			@RequestParam(required = true) String identifier,@RequestParam(required = true) String requestId,@RequestParam(required = true) String sourceType, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.selectEntity(identifier,requestId,sourceType), HttpStatus.OK);
	}
	
	/*@ApiOperation("Select Entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/selectEntityByUrl",consumes = { "application/json; charset=UTF-8" }, produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> selectEntity1(@RequestParam("token") String token,
			@RequestParam(required = true) String selectEntityUrl, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.selectEntityUrl(selectEntityUrl), HttpStatus.OK);
	}*/

	@ApiOperation("Get Request Id Of Screening")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/getRequestIdOfScreening", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> getRequestIdOfScreening(@Valid @RequestBody List<EntityScreeningDto> entityScreeningDtos,
			@RequestParam("token") String token, @RequestParam(required = false) String mainEntityId,
			@RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(
				advanceSearchService.getRequestIdOfScreening(entityScreeningDtos, mainEntityId, startDate, endDate),
				HttpStatus.OK);
	}

	@ApiOperation("Get Request Id Of Screening")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getEntitiesScreening", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntitiesScreening(@RequestParam("token") String token,
			@RequestParam(required = false) String mainEntityId, @RequestParam(required = false) String requestId,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getEntitisScreening(mainEntityId, requestId), HttpStatus.OK);
	}

	@ApiOperation("Add Custom Entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/addCustomShareHolder", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> addCustomShareHolder(@Valid @RequestBody List<CustomShareHolder> customShareHolder,
			@RequestParam("token") String token, @RequestParam String mainIdentifier,@RequestParam Boolean isSubsidiare, HttpServletRequest request)
					throws Exception {
		return new ResponseEntity<>(advanceSearchService.addCustomShareHolder(customShareHolder, mainIdentifier,
				getCurrentUserId(), isSubsidiare), HttpStatus.OK);
	}

	@ApiOperation("Delete Custom Entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/deleteCustomEntity/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteCustomEntity(@RequestParam String token,@RequestParam String field,@PathVariable String identifier) throws Exception {
		return new ResponseEntity<>(advanceSearchService.deleteCustomEntity(identifier,field), HttpStatus.OK);
	}
	
	@ApiOperation("Add Custom Entity")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/customKeyManager", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })	
	public ResponseEntity<?> customKeyManager(@Valid @RequestBody CustomOfficer customOfficer,
			@RequestParam("token") String token, HttpServletRequest request)
					throws Exception {
		return new ResponseEntity<>(advanceSearchService.customKeyManager(customOfficer, getCurrentUserId()), HttpStatus.OK);
	}
	
	@ApiOperation("save entity officer Info")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/saveEntityOfficerInfo", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOfficers(@Valid @RequestBody List<EntityOfficerInfoDto> entityOfficers,
			@RequestParam("token") String token, HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(entityOfficerInfoService.saveEntityOfficer(entityOfficers), HttpStatus.OK);
	}
	
	@ApiOperation("Get Company VLA Data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getCompanyVLAData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getCompanyVLAData(@RequestParam("token") String token,
			@RequestParam(required = true) String identifier, @RequestParam(required=true) String request_id,HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(advanceSearchService.getCompanyVLAData(identifier, request_id), HttpStatus.OK);
	}
	
}
