package element.bst.elementexploration.rest.extention.workflow.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.workflow.service.EntityResolveService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("entityResolveService")
public class EntityResolveServiceImpl implements EntityResolveService {

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENTION_API;

	@Override
	public String resolve(String dir) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/er/api/entity/resolve/" + encode(dir);
		return getDataFromServer(url);	
	}

	@Override
	public String resolveWithThreshold(String dir, Double threshold) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/er/api/entity/resolve/" + encode(dir) + "/" + threshold;
		return getDataFromServer(url);	
	}

	@Override
	public String resolveWithThresholdGenerateSchema(String dir, Double threshold, String generateSchema)
			throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/er/api/entity/resolve/" + encode(dir) + "/" + threshold + "/" + encode(generateSchema);
		return getDataFromServer(url);	
	}

	@Override
	public String testService() throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/er/api/entity/test";
		return getDataFromServer(url);	
	}

	private String getDataFromServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}
}
