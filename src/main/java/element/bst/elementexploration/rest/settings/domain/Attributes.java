package element.bst.elementexploration.rest.settings.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "SYS_ATTRIBUTES")
public class Attributes implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long id;
	@Column(name = "ATTRIBUTE_NAME")
	private String attributeName;
	@Column(name = "ATTRIBUTE_VALUE")
	private String attributeValue;
	@Column(name = "SELECTED")
	private boolean selected =false;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "settings_id")
	private Settings settings;
	
	@Column
	private String languageName;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "attributes")
	private List<ListItem> listItemList;

	public Attributes() {
	}

	public Attributes(long id, String attributeName, String attributeValue, boolean selected, Settings settings) {
		super();
		this.id = id;
		this.attributeName = attributeName;
		this.attributeValue = attributeValue;
		this.selected = selected;
		this.settings = settings;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	
}
