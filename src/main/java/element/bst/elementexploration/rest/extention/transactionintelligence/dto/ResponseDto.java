package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author suresh
 *
 */
public class ResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	List<TxEntityRespDto> listTxDtos;

	public List<TxEntityRespDto> getListTxDtos() {
		return listTxDtos;
	}

	public void setListTxDtos(List<TxEntityRespDto> listTxDtos) {
		this.listTxDtos = listTxDtos;
	}

}
