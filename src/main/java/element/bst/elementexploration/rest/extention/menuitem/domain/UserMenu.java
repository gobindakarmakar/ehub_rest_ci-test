package element.bst.elementexploration.rest.extention.menuitem.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Jalagari Paul
 *
 */
@Entity
@Table(name = "bst_user_menu")
public class UserMenu implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ApiModelProperty(value = "Number of Menu Item Clicks")
	@Column(name = "menuClicksCount")
	private Integer clicksCount;

	@ApiModelProperty(value = "Menu Item Size")
	@Column(name = "menuItemSize")
	private String menuItemSize;

	@ApiModelProperty(value = "Id of the user")
	private Long userId;

	@ApiModelProperty(value = "Module Group ID")
	@ManyToOne
	@JoinColumn(name = "moduleGroupId")
	private ModulesGroup moduleGroup;

	@ApiModelProperty(value = "Module ID")
	@ManyToOne
	@JoinColumn(name = "moduleId")
	private Modules module;
	
	@ApiModelProperty(value = "Status of the module")
	@Column(name = "moduleDisabled",columnDefinition = "boolean default false", nullable = false)
	private Boolean disabled=false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getClicksCount() {
		return clicksCount;
	}

	public void setClicksCount(Integer clicksCount) {
		this.clicksCount = clicksCount;
	}

	public String getMenuItemSize() {
		return menuItemSize;
	}

	public void setMenuItemSize(String menuItemSize) {
		this.menuItemSize = menuItemSize;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ModulesGroup getModuleGroup() {
		return moduleGroup;
	}

	public void setModuleGroup(ModulesGroup moduleGroup) {
		this.moduleGroup = moduleGroup;
	}

	public Modules getModule() {
		return module;
	}

	public void setModule(Modules module) {
		this.module = module;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	
	
}
