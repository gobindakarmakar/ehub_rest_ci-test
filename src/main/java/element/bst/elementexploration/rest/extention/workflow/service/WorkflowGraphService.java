package element.bst.elementexploration.rest.extention.workflow.service;

public interface WorkflowGraphService {

	String listOfOrientDatabases(String profileId) throws Exception;

	String listOfVertexAndEdges(String profileId, String database, String classType) throws Exception;

	String deleteTables() throws Exception;

	String getContingencyTable() throws Exception;

	String updateContingencyTable(String jsonString) throws Exception;

	String deleteContingencyTableById(String workflowId) throws Exception;

	String getContingencyTableById(String workflowId) throws Exception;

	String insertContingencyTableById(String jsonString, String workflowId) throws Exception;

	String graphdataByCaseId(String caseId) throws Exception;

	String graphdataByCaseIdAndLimit(String caseId, String limit) throws Exception;

	String findEntityByName(String name) throws Exception;

	String addEntity(String jsonString) throws Exception;

	String findEntityByIdentifier(String identifier) throws Exception;

	String fetcherList() throws Exception;

	String fetcherListById(String fetcherId) throws Exception;

	String search(String jsonString) throws Exception;

	String listOfOrientDatabases() throws Exception;

	String listOfOrientDatabaseByQuery(String query) throws Exception;

	String searchGraph(String jsonString) throws Exception;

	String searchHints(String searchType, String query) throws Exception;

	String searchResolve(String jsonString) throws Exception;

	String searchGraphSchema(String searchType, String query) throws Exception;

	String caseGraph(String jsonString) throws Exception;

	String getEntitesForCase(String caseId) throws Exception;

	String getRickScoreOfEntity(String entityId) throws Exception;

}
