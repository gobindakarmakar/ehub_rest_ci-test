package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Event details")
public class EventDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "List of notifications")
	private List<ParticipantDTO> recipients = new ArrayList<>();

	@ApiModelProperty(value = "ID of the notification")
	private Long notificationId;

	@ApiModelProperty(value = "Subject of the notification")
	private String subject;

	@ApiModelProperty(value = "Body of the notification")
	private String body;

	@ApiModelProperty(value = "Date of creation of the notification")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createdOn;

	@ApiModelProperty(value = "Schedule from date of the notification")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledFrom;

	@ApiModelProperty(value = "Schedule to date of the notification")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledTo;

	@ApiModelProperty(value = "Type of notification")
	private Integer type;

	@ApiModelProperty(value = "Level of notification")
	private Integer level;

	@ApiModelProperty(value = "Creator of the notification")
	private Long createdBy;

	@ApiModelProperty(value = "Case ID of the notification")
	private Long caseId;

	@ApiModelProperty(value = "Flag of notification")
	private Integer flag;

	public EventDetailDTO() {
		super();
	}

	public EventDetailDTO(Long notificationId, String subject, String body, LocalDateTime createdOn,
			LocalDateTime scheduledFrom, LocalDateTime scheduledTo, Integer type, Integer level, Long createdBy,
			Long caseId, Integer flag) {
		super();
		this.notificationId = notificationId;
		this.subject = subject;
		this.body = body;
		this.createdOn = createdOn;
		this.scheduledFrom = scheduledFrom;
		this.scheduledTo = scheduledTo;
		this.type = type;
		this.level = level;
		this.createdBy = createdBy;
		this.caseId = caseId;
		this.flag = flag;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getScheduledFrom() {
		return scheduledFrom;
	}

	public void setScheduledFrom(LocalDateTime scheduledFrom) {
		this.scheduledFrom = scheduledFrom;
	}

	public LocalDateTime getScheduledTo() {
		return scheduledTo;
	}

	public void setScheduledTo(LocalDateTime scheduledTo) {
		this.scheduledTo = scheduledTo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public List<ParticipantDTO> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<ParticipantDTO> recipients) {
		this.recipients = recipients;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
