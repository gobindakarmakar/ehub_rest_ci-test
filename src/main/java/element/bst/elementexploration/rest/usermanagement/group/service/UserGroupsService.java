package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;
import java.util.Map;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author Paul Jalagari
 *
 */
public interface UserGroupsService  extends GenericService<UserGroups, Long>{

	UserGroups getUserGroup(Long userId, Long groupId);

	boolean saveUserGroups(Users user, List<Long> groupIds, Long currentUserId,String action) throws Exception;

	Map<Long, Long> groupsWithUserCount();

	boolean removeUserGroups(Users user, List<Long> groupIdsToBeRemoved, Long currentUserId,String action) throws Exception;

	boolean addUserToGroup(Long groupId, Long userId, Long adminId) throws Exception;

}
