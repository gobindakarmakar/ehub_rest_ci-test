package element.bst.elementexploration.rest.casemanagement.dto;

import java.io.Serializable;

public class DocAggregatorDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer docFlag;

	private Long count;

	public DocAggregatorDto(Integer docFlag, Long count) {
		this.docFlag = docFlag;
		this.count = count;
	}

	public Integer getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(Integer docFlag) {
		this.docFlag = docFlag;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}
