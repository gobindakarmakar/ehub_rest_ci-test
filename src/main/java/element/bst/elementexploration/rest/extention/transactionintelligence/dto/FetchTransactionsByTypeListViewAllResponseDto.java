package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author Rambabu
 */
@ApiModel("Transactions by type")
public class FetchTransactionsByTypeListViewAllResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Transactions list by type")	
	private TxsListAggRespDto fetchTxByTypeListViewAll;
	@ApiModelProperty(value="pagination")	
	private PaginationInformation pagination;

	public FetchTransactionsByTypeListViewAllResponseDto() {
		super();
		
	}

	public TxsListAggRespDto getFetchTxByTypeListViewAll() {
		return fetchTxByTypeListViewAll;
	}

	public void setFetchTxByTypeListViewAll(TxsListAggRespDto fetchTxByTypeListViewAll) {
		this.fetchTxByTypeListViewAll = fetchTxByTypeListViewAll;
	}

	public PaginationInformation getPagination() {
		return pagination;
	}

	public void setPagination(PaginationInformation pagination) {
		this.pagination = pagination;
	}
	
	

}
