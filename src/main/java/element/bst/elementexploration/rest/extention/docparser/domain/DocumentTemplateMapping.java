package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Rambabu
 *
 */
@Entity
@Table(name = "DOCUMENT_TEMPLATE_MAPPING")
public class DocumentTemplateMapping implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private long documentId;
	private DocumentTemplates templateId;
	
	public DocumentTemplateMapping() {
		super();
		
	}

	
	public DocumentTemplateMapping(long id, long documentId, DocumentTemplates templateId) {
		super();
		this.id = id;
		this.documentId = documentId;
		this.templateId = templateId;
	}


	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "DOCUMENT_ID")
	public long getDocumentId() {
		return documentId;
	}
	
	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	
	@ManyToOne
	@JoinColumn(name = "DOCUMENT_TEMPLATE_ID")
	public DocumentTemplates getTemplateId() {
		return templateId;
	}


	public void setTemplateId(DocumentTemplates templateId) {
		this.templateId = templateId;
	}
	
	
	
	

}
