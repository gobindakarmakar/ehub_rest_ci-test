package element.bst.elementexploration.test.extention.transactionintelligence.service;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.CounterPartyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CounterPartyServiceTest {

	@Mock
	TxDataDao txDaoMock;

	@Mock
	AlertDao alertDaoMock;

	@Mock
	AccountDao accountDaoMock;

	@InjectMocks
	CounterPartyServiceImpl counterPartyServiceImpl;

	public RiskCountAndRatioDto riskCountAndRatioDto;

	public RiskCountAndRatioDto bankRisk;

	public RiskCountAndRatioDto geoGraphicRisk;

	public List<RiskCountAndRatioDto> list;

	public FilterDto filterDto;

	public Map<Integer, List<RiskCountAndRatioDto>> map;

	public CounterPartyNotiDto counterPartyNotiDto;

	public List<CounterPartyNotiDto> CounterPartyList;
	
	public List<String> listString;
	
	public List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
	
	public Account account = new Account();

	@Before
	public void setupMock() {

		riskCountAndRatioDto = new RiskCountAndRatioDto("ActivityType", new Double(1000), 10L, new Double(10), 100L);
		riskCountAndRatioDto.setAlertAmount(new Double(100));

		geoGraphicRisk = new RiskCountAndRatioDto("GeoGraphic", new Double(1000), 10L, new Double(10), 100L);
		geoGraphicRisk.setAlertAmount(new Double(100));

		bankRisk = new RiskCountAndRatioDto("Banks", new Double(1000), 10L, new Double(10), 100L);
		bankRisk.setAlertAmount(new Double(100));

		list = new ArrayList<>();
		RiskCountAndRatioDto riskCountAndRatioDto1 = new RiskCountAndRatioDto("Singapore", new Double(1000), 10L);
		riskCountAndRatioDto1.setBusinessDate(new Date());
		RiskCountAndRatioDto riskCountAndRatioDto2 = new RiskCountAndRatioDto("Malaysia", new Double(100), 5L);
		riskCountAndRatioDto2.setBusinessDate(new Date());
		list.add(riskCountAndRatioDto1);
		list.add(riskCountAndRatioDto2);

		filterDto = new FilterDto();
		filterDto.setTransactionType("transactionType");
		filterDto.setCountry("india");
		filterDto.setBank(true);
		filterDto.setActivityCategory("activityCategory");
		map = new HashMap<>();
		map.putIfAbsent(1, list);

		counterPartyNotiDto = new CounterPartyNotiDto("customerName", 1L, new Double(100));
		CounterPartyNotiDto counterPartyNotiDto1 = new CounterPartyNotiDto("customerName1", 2L, new Double(1000));
		CounterPartyList = new ArrayList<CounterPartyNotiDto>();
		CounterPartyList.add(counterPartyNotiDto);
		CounterPartyList.add(counterPartyNotiDto1);
		listString = new ArrayList<String>();
		listString.add("listString");
		listString.add("listString2");
		
		TransactionsData transactionsData1 = new TransactionsData();transactionsData1.setAmount(new Double(100));
		transactionsData1.setId(1L);transactionsData1.setBeneficiaryAccountId("AccountId");
		TransactionsData transactionsData2 = new TransactionsData();transactionsData2.setAmount(new Double(100));
		transactionsData2.setId(1L);transactionsData2.setBeneficiaryAccountId("AccountId");
		transactionsDataList.add(transactionsData1);
		transactionsDataList.add(transactionsData2);
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void getActivityTest() throws ParseException {
		Mockito.when(txDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(),Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(riskCountAndRatioDto);

		RiskCountAndRatioDto result = counterPartyServiceImpl.getActivity(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(riskCountAndRatioDto, result);
	}

	@Test
	public void getGeoGraphicAggregatesTest() throws ParseException {
		Mockito.when(txDaoMock.getCounterPartyGeoAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class), Mockito.anyBoolean())).thenReturn(list);
		List<RiskCountAndRatioDto> result = counterPartyServiceImpl.getGeoGraphicAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				filterDto, true);
		assertEquals(list.size(), result.size());
	}

	@Test
	public void getBanks() throws ParseException {
		Mockito.when(txDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(riskCountAndRatioDto);

		RiskCountAndRatioDto result = counterPartyServiceImpl.getActivity(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto);
		assertEquals(riskCountAndRatioDto, result);
	}

	@Test
	public void getBankAggregatesTest() throws ParseException {
		Mockito.when(
				txDaoMock.getBankAggregates(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(list);
		List<RiskCountAndRatioDto> result = counterPartyServiceImpl.getBankAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				filterDto);
		assertEquals(list.size(), result.size());
	}

	@Test
	public void getGeoGraphicTest() throws ParseException {
		Mockito.when(txDaoMock.getCustomerRisk(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),Mockito.any(FilterDto.class)))
				.thenReturn(riskCountAndRatioDto);
		RiskCountAndRatioDto result = counterPartyServiceImpl.getGeoGraphic(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), filterDto,
				true);
		assertEquals(riskCountAndRatioDto, result);
	}

	@Test
	public void getCouterPartyAggregatesTest() {
		Mockito.when(txDaoMock.getCustomerRiskAggregates(Mockito.anyString(), Mockito.anyString(),
				Mockito.any(FilterDto.class))).thenReturn(list);
		List<RiskCountAndRatioDto> result = counterPartyServiceImpl.getCouterPartyAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(list.size(), result.size());
	}

	@Test
	public void getBankByTypeTest() {
		Mockito.when(txDaoMock.getBankByType(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyString())).thenReturn(riskCountAndRatioDto);
		Mockito.when(
				txDaoMock.getBankAggregates(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(list);
		Map<Integer, List<RiskCountAndRatioDto>> result = counterPartyServiceImpl.getBankByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, "");
		assertEquals(2, result.size());
	}

	@Test
	public void getViewAllTest() {
		Mockito.when(txDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);
		Mockito.when(txDaoMock.viewAll(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.any(FilterDto.class), Mockito.anyString())).thenReturn(list);

		List<RiskCountAndRatioDto> result = counterPartyServiceImpl.getViewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "activity",
				1, 2, filterDto);
		assertEquals(list.size(), result.size());
	}

	@Test
	public void getViewAll_whenTypeIsBanksTest() {
		Mockito.when(txDaoMock.getViewAllForBanks(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt())).thenReturn(list);
		Mockito.when(txDaoMock.getViewAllForBanks(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt())).thenReturn(list);

		List<RiskCountAndRatioDto> result = counterPartyServiceImpl.getViewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Banks", 1,
				2, filterDto);
		assertEquals(list.size(), result.size());
	}

	@Test
	public void getViewAll_whenTypeIsCountryTest() {
		Mockito.when(txDaoMock.getViewAllCountry(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt())).thenReturn(list);
		Mockito.when(txDaoMock.getViewAllCountry(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(),
				Mockito.anyInt(), Mockito.anyInt())).thenReturn(list);

		List<RiskCountAndRatioDto> result = counterPartyServiceImpl.getViewAll(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				"BankLocations", 1, 2, filterDto);
		assertEquals(list.size(), result.size());
	}

	@Test
	public void getAlertByScenariosTest() {
		Mockito.when(alertDaoMock.getAlertByScenarios(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyListOf(String.class), Mockito.anyBoolean())).thenReturn(CounterPartyList);
		List<CounterPartyNotiDto> result = counterPartyServiceImpl.getAlertByScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "BankLocations", listString, true); 
		assertEquals(CounterPartyList.size(), result.size());
	}
	
	@Test
	public void getTopCounterPartiesTest() {
		Mockito.when(txDaoMock.getTopCustomerRisks(Mockito.anyString(), Mockito.anyString())).thenReturn(list);
		Mockito.when(alertDaoMock.getTopCounterParties(Mockito.anyString(), Mockito.anyString())).thenReturn(list);
		Mockito.when(alertDaoMock.getTopBanks(Mockito.anyString(), Mockito.anyString())).thenReturn(list);
		Map<String, List<RiskCountAndRatioDto>> result = counterPartyServiceImpl.getTopCounterParties(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(3, result.size());
	}
	
	@Test
	public void getViewAllCountTest() {
		Mockito.when(txDaoMock.getViewAllCountBanks(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean())).thenReturn(5l);
		long result = counterPartyServiceImpl.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "banks");
		assertEquals(5l, result);
	}
	
	@Test
	public void getViewAllCount_whenTypeIsBankLocationsTest() {
		Mockito.when(txDaoMock.getViewAllCountBanks(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean())).thenReturn(5l);
		long result = counterPartyServiceImpl.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "BankLocations");
		assertEquals(5l, result);
	}
	@Test
	public void getViewAllCount_whenTypeIsActivityTest() {
		//Mockito.when(txDaoMock.getViewAllCountBanks(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean())).thenReturn(5l);
		long result = counterPartyServiceImpl.getViewAllCount(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "BankLocations");
		assertEquals(0, result);
	}
	
	@Test
	public void addBankNamesTest() {
		Mockito.when(txDaoMock.findAll()).thenReturn(transactionsDataList);
		Mockito.when(accountDaoMock.fetchAccount(Mockito.anyString())).thenReturn(account);
		boolean result = counterPartyServiceImpl.addBankNames();
		assertEquals(true, result);
	}

}
