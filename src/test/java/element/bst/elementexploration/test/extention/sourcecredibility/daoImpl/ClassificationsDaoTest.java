package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.ClassificationsDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;

/**
 * @author suresh
 *
 */
public class ClassificationsDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	ClassificationsDaoImpl classificationsDao;

	Classifications classifications = null;

	@Before
	public void setupMock() {
		classifications = new Classifications();
		classifications.setClassificationId(1L);
		classifications.setClassifcationName("GENERAL");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchClassification() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(classifications);
		Classifications classificationsSaved = classificationsDao.fetchClassification("GENERAL");
		assertEquals(classifications.getClassificationId(), classificationsSaved.getClassificationId());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void fetchClassification_whenExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		classificationsDao.fetchClassification("GENERAL");
	}

}
