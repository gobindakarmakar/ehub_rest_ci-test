package element.bst.elementexploration.rest.extention.menuitem.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jalagari Paul
 *
 */
public class UserMenuFinalDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Id of the menu")
	private Long id;

	@ApiModelProperty(value = "Number of Menu Item Clicks")
	private Long clicksCount;

	@ApiModelProperty(value = "Menu Item Size")
	private String menuItemSize;

	@ApiModelProperty(value = "Id of the user")
	@JsonProperty(required = true)
	private Long userId;

	@ApiModelProperty(value = "Id of the module")
	@JsonProperty(required = true)
	private Long moduleId;

	@ApiModelProperty(value = "Id of the Group")
	@JsonProperty(required = true)
	private Long groupId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClicksCount() {
		return clicksCount;
	}

	public void setClicksCount(Long clicksCount) {
		this.clicksCount = clicksCount;
	}

	public String getMenuItemSize() {
		return menuItemSize;
	}

	public void setMenuItemSize(String menuItemSize) {
		this.menuItemSize = menuItemSize;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

}
