package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author suresh
 *
 */
@ApiModel("Document response")
public class DOCResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	/*
	 * @JsonProperty("content_type_paragraph") private String
	 * contentTypeParagraph;
	 * 
	 * @JsonProperty("content_type_table") private List<ValueDto>
	 * contentTypeTable;
	 */

	@ApiModelProperty(value = "Content type of paragraph")
	private ParagraphDto contentTypeParagraph;

	@ApiModelProperty(value = "Content type of table")
	private TableDto contentTypeTable;

	public ParagraphDto getContentTypeParagraph() {
		return contentTypeParagraph;
	}

	public void setContentTypeParagraph(ParagraphDto contentTypeParagraph) {
		this.contentTypeParagraph = contentTypeParagraph;
	}

	public TableDto getContentTypeTable() {
		return contentTypeTable;
	}

	public void setContentTypeTable(TableDto contentTypeTable) {
		this.contentTypeTable = contentTypeTable;
	}

	/*
	 * public String getContentTypeParagraph() { return contentTypeParagraph; }
	 * public void setContentTypeParagraph(String contentTypeParagraph) {
	 * this.contentTypeParagraph = contentTypeParagraph; } public List<ValueDto>
	 * getContentTypeTable() { return contentTypeTable; } public void
	 * setContentTypeTable(List<ValueDto> contentTypeTable) {
	 * this.contentTypeTable = contentTypeTable; }
	 */

}
