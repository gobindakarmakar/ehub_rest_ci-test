package element.bst.elementexploration.rest.recentsearch.dto;

import java.util.List;

import element.bst.elementexploration.rest.casemanagement.dto.CaseUnderwritingDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;

public class SearchDto {
	
	private List<AlertsDto> alertList;
	
	private List<CaseUnderwritingDto> caseList;
	
	private String network;

	private String orgNews;
	
	private String personNews;
	
	private String entityNews;

	private List<EntityDto> entities;
	
	private String id_details;

	public List<AlertsDto> getAlertList() {
		return alertList;
	}

	public void setAlertList(List<AlertsDto> alertList) {
		this.alertList = alertList;
	}

	public List<CaseUnderwritingDto> getCaseList() {
		return caseList;
	}

	public void setCaseList(List<CaseUnderwritingDto> caseList) {
		this.caseList = caseList;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public List<EntityDto> getEntities() {
		return entities;
	}

	public void setEntities(List<EntityDto> entities) {
		this.entities = entities;
	}

	public String getOrgNews() {
		return orgNews;
	}

	public void setOrgNews(String orgNews) {
		this.orgNews = orgNews;
	}

	public String getPersonNews() {
		return personNews;
	}

	public void setPersonNews(String personNews) {
		this.personNews = personNews;
	}

	public String getEntityNews() {
		return entityNews;
	}

	public void setEntityNews(String entityNews) {
		this.entityNews = entityNews;
	}

	public String getId_details() {
		return id_details;
	}

	public void setId_details(String id_details) {
		this.id_details = id_details;
	}



}
