package element.bst.elementexploration.rest.extention.transactionintelligence.enums;

/**
 * @author suresh
 *
 */
public enum Gender {

	M, F, TRANSGENDER,NONE

}
