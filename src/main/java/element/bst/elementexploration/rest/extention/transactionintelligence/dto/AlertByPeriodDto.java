package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Alert by period")
public class AlertByPeriodDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Greater than 30 days count")	
	private long greaterThirtyDaysCount;
	@ApiModelProperty(value="Between 20 to 30 days count")	
	private long twentyToThirtyCount;
	@ApiModelProperty(value="Between 10 to 20 days count")	
	private long tenToTwentyCount;
	public long getGreaterThirtyDaysCount() {
		return greaterThirtyDaysCount;
	}
	public void setGreaterThirtyDaysCount(long greaterThirtyDaysCount) {
		this.greaterThirtyDaysCount = greaterThirtyDaysCount;
	}
	public long getTwentyToThirtyCount() {
		return twentyToThirtyCount;
	}
	public void setTwentyToThirtyCount(long twentyToThirtyCount) {
		this.twentyToThirtyCount = twentyToThirtyCount;
	}
	public long getTenToTwentyCount() {
		return tenToTwentyCount;
	}
	public void setTenToTwentyCount(long tenToTwentyCount) {
		this.tenToTwentyCount = tenToTwentyCount;
	}

	

}
