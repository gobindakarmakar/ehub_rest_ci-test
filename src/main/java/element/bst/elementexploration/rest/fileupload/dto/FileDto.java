package element.bst.elementexploration.rest.fileupload.dto;

import java.io.Serializable;
import java.util.Date;

import element.bst.elementexploration.rest.usermanagement.user.domain.User;

public class FileDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String fileName;
	private String fileType;
	private Long size;
	private Date uploadedOn;
	private User uploadedBy;
	private Long modifiedBy;
	private Date modifiedOn;
	private byte[] file;

	public FileDto(Long id, String fileName, String fileType, Long size, Date uploadedOn, User uploadedBy,
			Long modifiedBy, Date modifiedOn, byte[] file) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileType = fileType;
		this.size = size;
		this.uploadedOn = uploadedOn;
		this.uploadedBy = uploadedBy;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.file = file;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public User getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(User uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

}
