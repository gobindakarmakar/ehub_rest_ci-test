package element.bst.elementexploration.rest.logging.serviceImpl;

import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.domain.LogAggregator;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.logging.util.LogRequest;

@Service("auditLogService")
public class AuditLogServiceImpl extends GenericServiceImpl<AuditLog, Long> implements AuditLogService {

	@Autowired
	public AuditLogServiceImpl(GenericDao<AuditLog, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	AuditLogDao auditLogDao;

	@Override
	@Transactional("transactionManager")
	public List<AuditLog> getLogs(LogRequest logRequest) {
		if (logRequest.getPageNumber() == null)
			logRequest.setPageNumber(ElementConstants.DEFAULT_PAGE_NUMBER);
		if (logRequest.getRecordsPerPage() == null)
			logRequest.setRecordsPerPage(ElementConstants.DEFAULT_PAGE_SIZE);
		return auditLogDao.getLogs(logRequest);
	}

	@Override
	@Transactional("transactionManager")
	public Long countLogs(LogRequest logRequest) {
		return auditLogDao.countLogs(logRequest);
	}

	@Override
	@Transactional("transactionManager")
	public List<LogAggregator> logAggregator(LogRequest logRequest) {
		return auditLogDao.logAggregator(logRequest);
	}

	@Override
	@Transactional(value = "transactionManager", propagation = Propagation.REQUIRES_NEW)
	public AuditLog mailLogging(DelegateExecution execution, Exception e) {

		AuditLog auditLog = new AuditLog();
		auditLog.setName((String) execution.getVariable("caseName"));
		auditLog.setCaseId(new Long((String) execution.getVariable("caseId")));
		auditLog.setUserId(new Long((String) execution.getVariable("userId")));
		auditLog.setCreatedOn(new Date());
		auditLog.setLevel(LogLevels.INFO);
		if (e == null) {
			auditLog.setTitle("Mail Success");
			auditLog.setDescription("mail sent successfully to " + (String) execution.getVariable("assignedUser"));
		} else {
			auditLog.setTitle("Mail Failure");
			auditLog.setDescription("failled to sent mail " + (String) execution.getVariable("assignedUser"));
		}
		return auditLogDao.create(auditLog);
	}

	@Override
	@Transactional("transactionManager")
	public List<String> getTypes() {
		return auditLogDao.getTypes();
	}

	@Override
	@Transactional("transactionManager")
	public AuditLog auditLogForGeneratedReport(String entityId, String entityName, long userId) {
		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Report Generated Successfully");

		log.setDescription("Report was generated for entity "+ entityName+" successfully ");
		log.setUserId(userId);
		log.setEntityId(entityId);
		log.setName(entityName);
		AuditLog logReturn=auditLogDao.create(log);

		return logReturn;

	}

	@Override
	@Transactional("transactionManager")
	public AuditLog auditLogForOverviewAPI(String entityId, String entityName, long userId) {

		AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, null, "Entity Page loaded with data");

		log.setDescription("Detailed information for entity "+ entityName+" was loaded successfully");
		log.setUserId(userId);
		log.setEntityId(entityId);
		log.setName(entityName);
		AuditLog logReturn=auditLogDao.create(log);

		return logReturn;
	}

	@Override
	@Transactional("transactionManager")
	public List<AuditLog> getLogsByTypeId(String type, Long typeId) {
		return auditLogDao.getLogsByTypeId(type, typeId);
	}

	@Override
	@Transactional("transactionManager")
	public Date getLatestStatusChangeOfAlert(Long alertId) {
		return auditLogDao.getLatestStatusChangeOfAlert(alertId);
	}

	@Override
	@Transactional("transactionManager")
	public List<AuditLog> getLogsByTypeAndSubtype(String type, String subType) {
		return auditLogDao.getLogsByTypeAndSubtype(type, subType);
	}

	@Override
	@Transactional("transactionManager")
	public List<AuditLog> getLogsByTitleAndTypeId(String title, Long typeId) {
		return auditLogDao.getLogsByTitleAndTypeId(title, typeId);
	}



	


}
