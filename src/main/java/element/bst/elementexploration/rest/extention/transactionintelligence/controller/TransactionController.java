package element.bst.elementexploration.rest.extention.transactionintelligence.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AccountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregateVo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertResponseSendDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatisticsRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CustomerDetailsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.InputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.OutputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ResolveDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.UpdateTransactionsDataDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AccountService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.AlertService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerDetailsService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CustomerRelationService;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.TxDataService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ResponseUtil;
import element.bst.elementexploration.rest.util.TransactionUtility;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "Transaction API" }, description = "Manages all transactions data")
@RestController
@RequestMapping(value = "/api/tx")
public class TransactionController extends BaseController {
	@Autowired
	AlertService alertService;

	@Autowired
	AccountService accountService;

	@Autowired
	CustomerRelationService customerRelationService;

	@Autowired
	CustomerDetailsService customerDetailsService;

	@Autowired
	TxDataService txDataService;

	@Autowired
	TransactionUtility serviceUtility;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fetchNonResolvedAlerts", method = RequestMethod.GET)
	public ResponseEntity<?> fetchNonResolvedAlerts(@RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) Integer pageNumber, @RequestParam("token") String token,
			@RequestParam(required = false) String name, @RequestParam(required = false) String resolved,
			@RequestParam(value = "date-from", required = false) String datefrom,
			@RequestParam(value = "date-to", required = false) String dateto, HttpServletRequest request) {
		try {
			if (name != null || datefrom != null || dateto != null) {
				List<AlertResponseSendDto> respDto = alertService.fetchfilterName(name, datefrom, dateto, pageNumber,
						recordsPerPage);
				// Pagination
				int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
				int index = pageNumber != null ? pageNumber : 1;
				long totalResults = alertService.fetchfilterNameCount(name, datefrom, dateto, pageNumber,
						recordsPerPage);
				PaginationInformation information = new PaginationInformation();
				information.setTotalResults(totalResults);
				information.setTitle(request.getRequestURI());
				information.setKind("list");
				information.setCount(respDto != null ? respDto.size() : 0);
				information.setIndex(index);
				int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
				information.setStartIndex(nextIndex);
				information.setInputEncoding("utf-8");
				information.setOutputEncoding("utf-8");

				JSONObject jsonObject = new JSONObject();
				jsonObject.put("listAlerts", respDto);
				jsonObject.put("information", information);
				return new ResponseEntity<>(jsonObject, HttpStatus.OK);
			}
			List<AlertResponseSendDto> respDto = alertService.fetchNonResolvedAlerts(pageNumber, recordsPerPage);
			// Pagination
			int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
			int index = pageNumber != null ? pageNumber : 1;
			long totalResults = alertService.countTotalAlerts(getCurrentUserId());
			PaginationInformation information = new PaginationInformation();
			information.setTotalResults(totalResults);
			information.setTitle(request.getRequestURI());
			information.setKind("list");
			information.setCount(respDto != null ? respDto.size() : 0);
			information.setIndex(index);
			int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
			information.setStartIndex(nextIndex);
			information.setInputEncoding("utf-8");
			information.setOutputEncoding("utf-8");

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("listAlerts", respDto);
			jsonObject.put("information", information);
			return new ResponseEntity<>(jsonObject, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed,Something went wrong"), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/alertStatistics/{entityId}", method = RequestMethod.GET)
	public ResponseEntity<?> alertStatistics(@PathVariable Long entityId, @RequestParam("token") String token) {

		try {
			AlertStatisticsRespDto statsDto = alertService.alertStatistics(entityId);
			return new ResponseEntity<>(statsDto, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed,Something went wrong"), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/resolveAlert", method = RequestMethod.POST)
	public ResponseEntity<?> resolveAlert(@RequestBody ResolveDto resolveDto, @RequestParam("token") String token) {

		try {
			@SuppressWarnings("unused")
			boolean status = alertService.resolveAlert(Long.valueOf(resolveDto.getAlertId()), resolveDto.getComment());
			return new ResponseEntity<>(new ResponseMessage("Successfully resolved alert"), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMessage("No such alertId exists"), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/fetchAlertSummary/{alertId}", method = RequestMethod.GET)
	public ResponseEntity<?> fetchAlertSummary(@PathVariable long alertId, @RequestParam("token") String token) {
		try {
			AlertResponseSendDto responseSendDto = alertService.fetchAlertSummary(alertId);
			return new ResponseEntity<>(responseSendDto, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMessage("Sorry,Alert already resolved"), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/fetchTxsFrSelectedEntitys", method = RequestMethod.POST)
	public ResponseEntity<?> fetchTransactions(@RequestBody List<String> entityIds, @RequestParam("token") String token,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String counterPartyMin,
			@RequestParam(required = false) String counterPartyMax, @RequestParam(required = false) String minAmount,
			@RequestParam(required = false) String minTxs, @RequestParam(required = false) String locations,
			@RequestParam(required = false) Long alertId, @RequestParam(required = false) Long alertTransactionId) {
		try {
			long value = (long) (alertTransactionId != null ? alertTransactionId : 0.0);
			ResponseDto response = txDataService.fetchAllTxsBasedOnEntity(entityIds, transactionType, minAmount,
					locations, minTxs, 0, value);
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed"), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/input/stats", method = RequestMethod.POST)
	public ResponseEntity<?> getInputStatus(@RequestParam("token") String token, @RequestBody List<String> entityIds,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String counterPartyMin,
			@RequestParam(required = false) String counterPartyMax, @RequestParam(required = false) String minAmount,
			@RequestParam(required = false) String minTxs, @RequestParam(required = false) String locations) {
		try {
			InputStatsResponseDto response = txDataService.inputStats(entityIds, counterPartyMin, counterPartyMax,
					minTxs, minAmount, locations, transactionType);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed"), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/output/stats", method = RequestMethod.POST)
	public ResponseEntity<?> getOutputstats(@RequestParam("token") String token, @RequestBody List<String> entityIds,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String counterPartyMin,
			@RequestParam(required = false) String counterPartyMax, @RequestParam(required = false) String minAmount,
			@RequestParam(required = false) String minTxs, @RequestParam(required = false) String locations) {
		try {
			OutputStatsResponseDto response = txDataService.outputStats(entityIds, counterPartyMin, counterPartyMax,
					minTxs, minAmount, locations, transactionType);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed"), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/graph/{alert-id}/alert", method = RequestMethod.GET)
	public ResponseEntity<?> getGraph(@RequestParam("token") String token, @PathVariable("alert-id") Long alertId,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String counterPartyMin,
			@RequestParam(required = false) String counterPartyMax, @RequestParam(required = false) String minAmount,
			@RequestParam(required = false) String minTxs, @RequestParam(required = false) String locations) {
		return new ResponseEntity<>(alertService.generateGraph(alertId, transactionType, locations, minAmount),
				HttpStatus.OK);

	}

	@RequestMapping(value = "/createAlerts", method = RequestMethod.GET)
	public ResponseEntity<?> createAlerts(@RequestParam("token") String token) {
		Long userIdTemp = getCurrentUserId();
		try {
			List<TransactionsData> transactionsList = txDataService.getAllTransactions();
			if (!transactionsList.isEmpty()) {
				alertService.createAlert(transactionsList, userIdTemp);
				return new ResponseEntity<>(new ResponseMessage("Transaction alerts created"), HttpStatus.OK);
			}
			return new ResponseEntity<>(new ResponseMessage("No transaction found "), HttpStatus.OK);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage("Failed to create the transaction"), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/graph/{vertex-id}/expand/{alert-id}", method = RequestMethod.GET)
	public ResponseEntity<?> expandGraph(@RequestParam("token") String token,
			@PathVariable("vertex-id") String customerId, @PathVariable("alert-id") Long alertId,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String counterPartyMin,
			@RequestParam(required = false) String counterPartyMax, @RequestParam(required = false) String minAmount,
			@RequestParam(required = false) String minTxs, @RequestParam(required = false) String locations) {
		return new ResponseEntity<>(
				alertService.expandCustomerNode(customerId, alertId, transactionType, locations, minAmount),
				HttpStatus.OK);

	}

	/*
	 * @ApiOperation("Search Alerts List")
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, response = ResponseUtil.class, message =
	 * "Operation Successful"),
	 * 
	 * @ApiResponse(code = 500, response = ResponseMessage.class, message =
	 * "Could not process your request") })
	 * 
	 * @GetMapping(value = "/searchAlerts", produces = {
	 * "application/json; charset=UTF-8" }) public ResponseEntity<?>
	 * searchCaseList(@RequestParam("token") String token,
	 * 
	 * @RequestParam(required = false) Integer
	 * recordsPerPage, @RequestParam(required = false) Integer pageNumber,
	 * 
	 * @RequestParam(required = false) String keyword, HttpServletRequest request) {
	 * 
	 * List<AlertsDto> alertList = alertService.searchAlerts(recordsPerPage,
	 * pageNumber, keyword); Long totalResults =
	 * alertService.searchAlertsCount(keyword);
	 * 
	 * int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE :
	 * recordsPerPage; int index = pageNumber != null ? pageNumber : 1;
	 * PaginationInformation information = new PaginationInformation();
	 * information.setTotalResults(totalResults);
	 * information.setTitle(request.getRequestURI()); information.setKind("list");
	 * information.setCount(alertList != null ? alertList.size() : 0);
	 * information.setIndex(index); int nextIndex = (int) (totalResults - (pageSize
	 * * index)) > 0 ? index + 1 : 0; information.setStartIndex(nextIndex);
	 * information.setInputEncoding("utf-8");
	 * information.setOutputEncoding("utf-8"); ResponseUtil responseUtil=new
	 * ResponseUtil(); responseUtil.setResult(alertList);
	 * responseUtil.setPaginationInformation(information); return new
	 * ResponseEntity<>(responseUtil, HttpStatus.OK); }
	 */

	/*//API for update Transaction Details 
	@RequestMapping(value = "/updateTransactions", method = RequestMethod.POST)
	public ResponseEntity<?> updateTransactions(@RequestBody UpdateTransactionsDataDto updatetransactionsDataDto, @RequestParam("token") String token)
	{
		try
		{
		if( updatetransactionsDataDto != null)
		{
		    txDataService.updateTransactions(updatetransactionsDataDto);
			return new ResponseEntity<>(new ResponseMessage("Transaction data updated"), HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<>(new ResponseMessage("No Transaction data found "), HttpStatus.OK);
		}
		}
		catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.OK);
		}
	}*/
	@ApiOperation("Alert Aggregator")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = AlertAggregator.class, responseContainer = "List", message = "Operation Successful"),
			@ApiResponse(code = 400, response = ResponseUtil.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = "Could not process your request") })
	@PostMapping(value = "/alertsAggregates", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> alertsAggregates(@RequestParam("token") String token,
			@Valid @RequestBody AlertAggregateVo alertAggregateVo, BindingResult results) {
		if (!results.hasErrors()) {
			return new ResponseEntity<>(alertService.alertsAggregates(alertAggregateVo, getCurrentUserId()),
					HttpStatus.OK);
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);

		}
	}

	// API for update Transaction Details
	@RequestMapping(value = "/updateTransactions", method = RequestMethod.POST)
	public ResponseEntity<?> updateTransactions(@RequestBody TxEntityRespDto txEntityRespDto, @RequestParam("token") String token) {
	   try {
		   if (txEntityRespDto != null) {
			   	txDataService.updateTransactions(txEntityRespDto);
	 		return new ResponseEntity<>(new ResponseMessage("Transaction data updated"), HttpStatus.OK);
		}
		else {
	 	    return new ResponseEntity<>(new ResponseMessage("No Transaction data found "), HttpStatus.OK);
	 	  }
		} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.OK);
	 	}
	  }
	
	  // API for delete Transaction Details
	    @ApiOperation("Deletes Transactions")
		@ApiResponses(value = {
				@ApiResponse(code = 200, response = ResponseMessage.class, message = "Transaction deleted successfully"),
				@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
				@ApiResponse(code = 404, response = ResponseMessage.class, message = "Transaction not found") })
		@RequestMapping(value = "/deleteTransactions", method = RequestMethod.DELETE)
		public ResponseEntity<?> deleteTransactions(@RequestParam(required = true) Long id,
				@RequestParam("token") String token, HttpServletRequest request) {
	
			try {
				txDataService.deleteTransaction(id);
				return new ResponseEntity<>(new ResponseMessage("Transaction deleted"), HttpStatus.OK);
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.OK);
			}
		}
	
	    // API for delete Alerts Details
		@ApiOperation("Deletes Alerts")
		@ApiResponses(value = {
		@ApiResponse(code = 200, response = ResponseMessage.class, message = "Alert deleted successfully"),
		@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
		@ApiResponse(code = 404, response = ResponseMessage.class, message = "Alert not found") })
		@RequestMapping(value = "/deleteAlerts", method = RequestMethod.DELETE)
		public ResponseEntity<?> deleteAlerts(@RequestParam(required = true) Long id, @RequestParam("token") String token,
				HttpServletRequest request) {
	
			try {
				alertService.deleteAlerts(id);
				return new ResponseEntity<>(new ResponseMessage("Alert deleted"), HttpStatus.OK);
			} catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.OK);
			}
	 	}
		
        //API for update Account Details 
		@RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
		@ApiResponses(value = {
		@ApiResponse(code = 200 , message = "Account updated  Successfully"),
		@ApiResponse(code = 400,  message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
		@ApiResponse(code = 500,  message = "Could not process your request") })
		public ResponseEntity<?> updateAccount(@RequestBody AccountDto accountDto, @RequestParam("token") String token)
		{
			try
			{
			 if(accountDto != null)
			   {
				accountService.updateAccount(accountDto);
				return new ResponseEntity<>(new ResponseMessage("Account data updated"), HttpStatus.OK);
				}
				else
				{
				return new ResponseEntity<>(new ResponseMessage("No Account data found"), HttpStatus.OK);
				}
			}
			catch (Exception e) {
				ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
				return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.OK);
			 }
			
		}		
			
			//API for update Customer Details 
			@RequestMapping(value = "/updateCustomerDetails", method = RequestMethod.POST)
			@ApiResponses(value = {
			@ApiResponse(code = 200 , message = "Customer Details updated  Successfully"),
			@ApiResponse(code = 400,  message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
		    @ApiResponse(code = 500,  message = "Could not process your request") })
			public ResponseEntity<?> updateCustomerDetails(@RequestBody CustomerDetailsDto customerDetailsDto, @RequestParam("token") String token)
			{
				try
				{
				if(customerDetailsDto != null)
				{
				    customerDetailsService.updateCustomerDetails(customerDetailsDto);
					return new ResponseEntity<>(new ResponseMessage("Customer Details updated"), HttpStatus.OK);
				}
				else		
				{
						return new ResponseEntity<>(new ResponseMessage("Customer Details data found "), HttpStatus.OK);
				}
				}
				catch (Exception e) {
					ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
					return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.OK);
			 	}
			
			}		
				

}
