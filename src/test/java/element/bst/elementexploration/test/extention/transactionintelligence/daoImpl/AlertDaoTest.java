package element.bst.elementexploration.test.extention.transactionintelligence.daoImpl;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl.AlertDaoImpl;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregateVo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDtoAscnd;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;
import element.bst.elementexploration.rest.util.DaysCalculationUtility;
import element.bst.elementexploration.rest.util.FilterUtility;

/**
 * @author rambabu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AlertDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@Mock
	CriteriaBuilder builder;

	@Mock
	CriteriaQuery<Object> criteriaQuery;

	@Mock
	Root<Alert> root;

	@Mock
	DaysCalculationUtility daysCalculationUtility;

	@Mock
	FilterUtility filterUtility;

	@Mock
	CustomerDetailsDao customerDeatilsDao;

	@SuppressWarnings("rawtypes")
	@Mock
	private NativeQuery nativeQuery;

	@InjectMocks
	AlertDaoImpl alertDao;

	List<Alert> alertsList = new ArrayList<Alert>();

	Alert alert;

	List<Object[]> objectList = new ArrayList<Object[]>();

	CustomerDetails customerDetails;

	List<String> scenarios = new ArrayList<String>();

	List<String> list = new ArrayList<String>();

	RiskCountAndRatioDto riskCountAndRatioDto = new RiskCountAndRatioDto();

	List<RiskCountAndRatioDto> riskCountAndRatioDtoList = new ArrayList<RiskCountAndRatioDto>();

	List<AlertDashBoardRespDto> alertDashBoardRespDtolist = new ArrayList<AlertDashBoardRespDto>();

	AlertAggregateVo alertAggregateVo = new AlertAggregateVo();
	List<AlertAggregator> alertAggregatorList = new ArrayList<AlertAggregator>();

	@Before
	public void setupMock() {
		alert = new Alert(1L, "test", "123", "SG", "Large repotable amount", new Date(), "test", 0, "123", "status", 1L,
				"test comment", "Internal", AlertStatus.OPEN, "Amount", 1L, "Test description", "Amount", 80, 60000D);
		alertsList.add(alert);
		alertsList.add(new Alert(1L, "test", "123", "SG", "Large repotable amount", new Date(), "test", 0, "123",
				"status", 1L, "test comment", "Internal", AlertStatus.OPEN, "Amount", 1L, "Test description", "Amount",
				80, 80000D));
		Object[] objectArrayNew = new Object[4];
		objectArrayNew[0] = AlertStatus.OPEN;
		objectArrayNew[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectArrayNew[2] = "60000";
		objectArrayNew[3] = "50000";
		objectList.add(objectArrayNew);
		Object[] objectArray = new Object[4];
		objectArray[0] = AlertStatus.OPEN;
		objectArray[1] = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		objectArray[2] = "60000";
		objectArray[3] = "50000";
		objectList.add(objectArray);
		customerDetails = new CustomerDetails();
		scenarios.add("Large reportable ammount wired");
		scenarios.add("Large reportable ammount wired");
		list.add("80");
		list.add("90");
		riskCountAndRatioDto.setAlertCount(10L);
		riskCountAndRatioDtoList.add(riskCountAndRatioDto);
		alertDashBoardRespDtolist.add(new AlertDashBoardRespDto());
		alertAggregateVo.setFromDate(new Date());
		alertAggregateVo.setToDate(new Date());
		alertAggregateVo.setEntityType(EntityType.TRANSACTION.toString());
		alertAggregatorList.add(new AlertAggregator("30", 30L));
		alertAggregatorList.add(new AlertAggregator("31", 30L));
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchNonResolvedAlertsTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchNonResolvedAlerts(2, 10);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchNonResolvedAlerts_whenListIsEmptyTest() {
		List<Alert> alertsListTest = new ArrayList<Alert>();
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsListTest);
		List<Alert> alertsList = alertDao.fetchNonResolvedAlerts(2, 10);
		assertEquals(0, alertsList.size());
	}

	@Test
	public void fetchAlertSummaryTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.find(Mockito.anyObject(), Mockito.anyObject())).thenReturn(alert);
		Alert newAlert = alertDao.fetchAlertSummary(1L);
		assertEquals(new Long("1"), newAlert.getId());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = NoDataFoundException.class)
	public void fetchAlertSummary_whenNoDataFoundExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.find(Mockito.anyObject(), Mockito.anyObject())).thenThrow(NoDataFoundException.class);
		alertDao.fetchAlertSummary(1L);
	}

	@Test
	public void loadAlertByIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.doNothing().when(session).load(Mockito.any(Alert.class), Mockito.anyObject());
		Alert newAlert = alertDao.loadAlertById(1L);
		assertEquals(null, newAlert);
	}
	/*
	 * 
	 * @Test(expected = NoDataFoundException.class) public void
	 * loadAlertById_whenNoDataFoundExceptionThrowTest(){
	 * Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	 * Mockito.doThrow(NoDataFoundException.class).when(session).load(Mockito.
	 * any(Alert.class),Mockito.anyObject()); alertDao.loadAlertById(1L); }
	 */

	@Test
	public void fetchCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchCount(1L);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterNameTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName("test",
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterName_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName("test", null, null, 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterName_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName("test",
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), null, 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterName_whenNameAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName(null,
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), null, 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterName_whenNameIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName(null,
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterName_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName("test", null,
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterName_whenNameAndDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertsListnew = alertDao.fetchfilterName(null, null, null, 1, 5);
		assertEquals(2, alertsListnew.size());
	}

	@Test
	public void fetchfilterNameCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount("test", new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5);
		assertEquals(2, count);
	}

	@Test
	public void fetchfilterNameCount_whenDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount("test", null, null, 1, 5);
		assertEquals(2, count);
	}

	@Test
	public void fetchfilterNameCount_whenNameIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount(null, new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5);
		assertEquals(2, count);
	}

	@Test
	public void fetchfilterNameCount_whenNameAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount(null, new SimpleDateFormat("dd-MM-yyyy").format(new Date()), null, 1,
				5);
		assertEquals(2, count);
	}

	@Test
	public void fetchfilterNameCount_whenDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount("test", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), null,
				1, 5);
		assertEquals(2, count);
	}

	@Test
	public void fetchfilterNameCount_whenNameAndDateFromAndDateToIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount(null, null, null, 1, 5);
		assertEquals(2, count);
	}

	@Test
	public void fetchfilterNameCount_whenDateFromIsNullTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		int count = alertDao.fetchfilterNameCount("test", null, new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				1, 5);
		assertEquals(2, count);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByCustomerAndDateTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Alert.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> list = alertDao.findByCustomerAndDate("test", new Date(System.currentTimeMillis() - 3600 * 1000));
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void findByCustomerAndDate_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(Alert.class)).thenReturn(root);
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.findByCustomerAndDate("test", new Date(System.currentTimeMillis() - 3600 * 1000));

	}

	@Test
	public void overAllAlertsBwDatesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> list = alertDao.overAllAlertsBwDates("test", new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "desc");
		assertEquals(2, list.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchGroupByAlertStatusTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(daysCalculationUtility.getDifferenceBetweenDates(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(31L);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AlertStatusDto> alertStatusDtolist = alertDao.fetchGroupByAlertStatus(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), true, new FilterDto());
		assertEquals(1, alertStatusDtolist.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchGroupByAlertStatus_whenPeriodByIsFalseTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(daysCalculationUtility.getDifferenceBetweenDates(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(31L);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AlertStatusDto> alertStatusDtolist = alertDao.fetchGroupByAlertStatus(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), false, new FilterDto());
		assertEquals(1, alertStatusDtolist.size());
	}

	@Test
	public void fetchAlertsBwDatesOrderByTest() throws ParseException {
		customerDetails.setDisplayName("test");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		Mockito.when(customerDeatilsDao.fetchCustomerDetails(Mockito.anyString())).thenReturn(customerDetails);
		Mockito.when(query.getSingleResult()).thenReturn(60000D);
		List<AlertDashBoardRespDtoAscnd> dashBoardList = alertDao.fetchAlertsBwDatesOrderBy("test",
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5, "desc");
		assertEquals(2, dashBoardList.size());
	}

	@Test
	public void fetchAlertsBwDatesOrderBy_whenTransAmountIsNullTest() throws ParseException {
		customerDetails.setDisplayName("test");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		Mockito.when(customerDeatilsDao.fetchCustomerDetails(Mockito.anyString())).thenReturn(customerDetails);
		Mockito.when(query.getSingleResult()).thenReturn(null);
		List<AlertDashBoardRespDtoAscnd> dashBoardList = alertDao.fetchAlertsBwDatesOrderBy("test",
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 1, 5, "desc");
		assertEquals(2, dashBoardList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "daily", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregates_whenGranularityIsWeeklyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "weekly", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregates_whenGranularityIsMonthlyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "monthly", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregates_whenGranularityIsYearlyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "yearly", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregates_whenGranularityIsHoursTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "hours", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregates_whenGranularityIsMinuteTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "minute", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesProductTypeTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "daily", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesProductType_whenGranularityIsWeeklyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "weekly", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesProductType_whenGranularityIsMonthlyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "monthly", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesProductType_whenGranularityIsYearlyTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "yearly", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesProductType_whenGranularityIsHoursTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "hours", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAlertAggregatesProductType_whenGranularityIsMinuteTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<TransactionAmountAndAlertCountDto> alertList = alertDao.getAlertAggregatesProductType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "minute", true, "wired", true, true,
				new FilterDto());
		assertEquals(1, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getMonthlyTurnOverTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		MonthlyTurnOverDto monthlyTurnOverDto = alertDao.getMonthlyTurnOver(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(2, monthlyTurnOverDto.getBwOneToFiveMillionUSD());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAlertsBwDatesCustomizedTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AlertNotiDto> alertNotiDtos = alertDao.fetchAlertsBwDatesCustomized(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "", true, true, new FilterDto());
		assertEquals(1, alertNotiDtos.size());
	}

	@Test
	public void getCustomerRiskByTypeTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		List<String> riskByTypeList = alertDao.getCustomerRiskByType(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired", scenarios);
		assertEquals(2, riskByTypeList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getCustomerRiskByType_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getCustomerRiskByType(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired", scenarios);
	}

	@Test
	public void getScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(scenarios);
		List<String> riskByTypeList = alertDao.getScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired");
		assertEquals(2, riskByTypeList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getScenarios_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		List<String> riskByTypeList = alertDao.getScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired");
		assertEquals(null, riskByTypeList);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getScenarios_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void alertComparisonNotificationTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AlertComparisonDto> transactionList = alertDao.alertComparisonNotification(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new Date(), new FilterDto(),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(1, transactionList.size());
	}

	@Test
	public void getProductRiskTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(riskCountAndRatioDto).thenReturn(60000D);
		RiskCountAndRatioDto riskCountAndRatioDtoNew = alertDao.getProductRisk(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(new Double("60000"), riskCountAndRatioDtoNew.getAlertAmount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getProductRisk_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		RiskCountAndRatioDto riskCountAndRatioDtoNew = alertDao.getProductRisk(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(null, riskCountAndRatioDtoNew);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getProductRisk_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getProductRisk(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getProductRiskAggregatesTest() throws ParseException {
		objectList.get(0)[0] = "5000";
		objectList.get(1)[0] = "5000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> riskCountAndRatioList = alertDao.getProductRiskAggregates(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired", new FilterDto());
		assertEquals(1, riskCountAndRatioList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getProductRiskAggregates_whenParseExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		alertDao.getProductRiskAggregates(new Date().toString(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				"wired", new FilterDto());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getProductRiskAggregates_whenFailedToExecuteQueryExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getProductRiskAggregates(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "wired", new FilterDto());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getTopProductRiskTest() throws ParseException {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(nativeQuery.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> riskCountAndRatioList = alertDao.getTopProductRisk(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2, riskCountAndRatioList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getTopProductRisk_whenNoResultExceptionTest() throws ParseException {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(nativeQuery.getResultList()).thenThrow(NoResultException.class);
		List<RiskCountAndRatioDto> riskCountAndRatioList = alertDao.getTopProductRisk(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(null, riskCountAndRatioList);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getTopProductRisk_whenFailedToExecuteQueryExceptionTest() throws ParseException {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(nativeQuery.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getTopProductRisk(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
	}

	@Test
	public void getRiskAlertsByScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(query.getResultList()).thenReturn(alertDashBoardRespDtolist);
		List<AlertDashBoardRespDto> alertDashBoardRespDtolistnew = alertDao.getRiskAlertsByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(1, alertDashBoardRespDtolistnew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = DateFormatException.class)
	public void getRiskAlertsByScenarios_whenDateFormatExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
		alertDao.getRiskAlertsByScenarios(new Date().toString(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new FilterDto());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getRiskAlertsByScenarios_whenFailedToExecuteQueryExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getRiskAlertsByScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
	}

	@Test
	public void getRiskByScenariosTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertDashBoardRespDtolist);
		List<AlertDashBoardRespDto> alertDashBoardRespDtolistnew = alertDao.getRiskByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenarios, "wired");
		assertEquals(1, alertDashBoardRespDtolistnew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getRiskByScenarios_whenFailedToExecuteQueryExceptionTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		List<AlertDashBoardRespDto> alertDashBoardRespDtolistnew = alertDao.getRiskByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), scenarios, "wired");
		assertEquals(1, alertDashBoardRespDtolistnew.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void amlTopScenariosTest() throws ParseException {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(objectList);
		List<AlertScenarioDto> alertScenarioDtos = alertDao.amlTopScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(2, alertScenarioDtos.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void amlTopScenarios_whenExceptionThrowTest() throws ParseException {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(Exception.class);
		List<AlertScenarioDto> alertScenarioDtos = alertDao.amlTopScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(0, alertScenarioDtos.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void amlAlertByPeriodTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(daysCalculationUtility.getDifferenceBetweenDates(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(31L);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3L);
		AlertByPeriodDto alertByPeriodDto = alertDao.amlAlertByPeriod(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(3L, alertByPeriodDto.getGreaterThirtyDaysCount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void amlAlertByPeriod_whenExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(daysCalculationUtility.getDifferenceBetweenDates(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(31L);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(Exception.class);
		AlertByPeriodDto alertByPeriodDto = alertDao.amlAlertByPeriod(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(0L, alertByPeriodDto.getGreaterThirtyDaysCount());
	}

	@Test
	public void getAlertByScenariosTest() {
		List<CounterPartyNotiDto> CounterPartyNotilist = new ArrayList<CounterPartyNotiDto>();
		CounterPartyNotilist.add(new CounterPartyNotiDto());
		CounterPartyNotilist.add(new CounterPartyNotiDto());
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(CounterPartyNotilist);
		List<CounterPartyNotiDto> CounterPartyNotiListNew = alertDao.getAlertByScenarios(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "CHeque", scenarios, true);
		assertEquals(2, CounterPartyNotiListNew.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertByScenarios_whenExceptionExceptionTest() {
		List<CounterPartyNotiDto> CounterPartyNotilist = new ArrayList<CounterPartyNotiDto>();
		CounterPartyNotilist.add(new CounterPartyNotiDto());
		CounterPartyNotilist.add(new CounterPartyNotiDto());
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getAlertByScenarios(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "CHeque", scenarios, true);

	}

	@Test
	public void getTopCounterPartiesTest() {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		objectList.get(0)[2] = "2";
		objectList.get(1)[2] = "3";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> riskCountAndRatioDtoList = alertDao.getTopCounterParties(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2, riskCountAndRatioDtoList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getTopCounterParties_whenExceptionThrowTest() {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		objectList.get(0)[2] = "2";
		objectList.get(1)[2] = "3";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getTopCounterParties(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

	}

	@Test
	public void getTopBanksTest() {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		objectList.get(0)[2] = "2";
		objectList.get(1)[2] = "3";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenReturn(objectList);
		List<RiskCountAndRatioDto> riskCountAndRatioDtoList = alertDao.getTopBanks(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2, riskCountAndRatioDtoList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getTopBanks_whenExceptionThrowTest() {
		objectList.get(0)[1] = "5000";
		objectList.get(1)[1] = "5000";
		objectList.get(0)[2] = "2";
		objectList.get(1)[2] = "3";
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getTopBanks(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

	}

	@Test
	public void fetchAlertSummaryListTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertList = alertDao.fetchAlertSummaryList(1L);
		assertEquals(2, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAlertSummaryList_whenNoResultExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(nativeQuery);
		Mockito.when(nativeQuery.getResultList()).thenThrow(NoResultException.class);
		List<Alert> alertList = alertDao.fetchAlertSummaryList(1L);
		assertEquals(0, alertList.size());

	}

	@SuppressWarnings("unchecked")
	@Test
	public void fetchAlertSummaryList_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		List<Alert> alertList = alertDao.fetchAlertSummaryList(1L);
		assertEquals(0, alertList.size());

	}

	@Test
	public void getAlertScenarioByIdTest() {
		AlertScenarioDto alertScenarioDto = new AlertScenarioDto();
		alertScenarioDto.setAmount(20000D);
		alertScenarioDto.setAlertDescription("Testing");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(alertScenarioDto);
		AlertScenarioDto alertScenarioDtoResult = alertDao.getAlertScenarioById(1L, "large reportable amount");
		assertEquals("Testing", alertScenarioDtoResult.getAlertDescription());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertScenarioById_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getAlertScenarioById(1L, "large reportable amount");
	}

	@Test
	public void getAlertsForCustomerTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		List<Alert> alertList = alertDao.getAlertsForCustomer(new Date().toString(), new Date().toString(), "123");
		assertEquals(2, alertList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getAlertsForCustomer_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getAlertsForCustomer(new Date().toString(), new Date().toString(), "123");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getGroupByScenarioTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(riskCountAndRatioDtoList);
		List<RiskCountAndRatioDto> riskCountAndRatioDtoListNew = alertDao.getGroupByScenario(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(1, riskCountAndRatioDtoListNew.size());
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Test(expected = DateFormatException.class) public void
	 * getGroupByScenario_whenDateFormatExceptionThrowTest() throws
	 * ParseException{
	 * Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	 * Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
	 * Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).
	 * thenReturn(new StringBuilder());
	 * Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class),Mockito.
	 * any(FilterDto.class),Mockito.anyString(),Mockito.anyString())).thenReturn
	 * (query);
	 * Mockito.when(query.getResultList()).thenThrow(DateFormatException.class);
	 * alertDao.getGroupByScenario(new SimpleDateFormat("dd-MM-yyyy").format(new
	 * Date()), new SimpleDateFormat("dd-MM-yyyy").format(new Date()),new
	 * FilterDto()); }
	 */

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getGroupByScenario_whenFailedToExecuteQueryExceptionThrowTest() throws ParseException {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(filterUtility.filterColumns(Mockito.any(FilterDto.class))).thenReturn(new StringBuilder());
		Mockito.when(filterUtility.queryReturn(Mockito.any(Query.class), Mockito.any(FilterDto.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.getGroupByScenario(new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());

	}

	@Test
	public void findAlertByAlertTransactionIdTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertsList);
		boolean status = alertDao.findAlertByAlertTransactionId(1L);
		assertEquals(true, status);
	}

	@Test
	public void findAlertByAlertTransactionId_whenListIsEmptyTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(new ArrayList<Alert>());
		boolean status = alertDao.findAlertByAlertTransactionId(1L);
		assertEquals(false, status);
	}

	@Test
	public void searchAlertsTest() {
		List<AlertsDto> alertDtoList = new ArrayList<AlertsDto>();
		alertDtoList.add(new AlertsDto("1234", "123456", "Large peportable amount", new Date(), 1L, "test", "internal",
				AlertStatus.OPEN, "Wired", 2L, "desciption", "scenario", 80, 80000D));
		alertDtoList.add(new AlertsDto("1234", "123456", "Large peportable amount", new Date(), 1L, "test", "internal",
				AlertStatus.OPEN, "Wired", 2L, "desciption", "scenario", 80, 80000D));
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertDtoList);
		List<AlertsDto> alertDtoResultList = alertDao.searchAlerts("test");
		assertEquals(2, alertDtoResultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void searchAlerts_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.searchAlerts("test");
	}

	@Test
	public void searchAlertsCountTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(3L);
		Long count = alertDao.searchAlertsCount("test");
		assertEquals(new Long("3"), count);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void searchAlertsCount_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.searchAlertsCount("test");
	}

	@Test
	public void alertsAggregates_whenGranularityIsDailyTest() {
		alertAggregateVo.setGranularity("daily");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertAggregatorList);
		List<AlertAggregator> alertAggregatorResultList = alertDao.alertsAggregates(alertAggregateVo, 1L);
		assertEquals(2, alertAggregatorResultList.size());
	}

	@Test
	public void alertsAggregates_whenGranularityIsWeeklyTest() {
		alertAggregateVo.setGranularity("weekly");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertAggregatorList);
		List<AlertAggregator> alertAggregatorResultList = alertDao.alertsAggregates(alertAggregateVo, 1L);
		assertEquals(2, alertAggregatorResultList.size());
	}

	@Test
	public void alertsAggregates_whenGranularityIsMonthlyTest() {
		alertAggregateVo.setGranularity("monthly");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertAggregatorList);
		List<AlertAggregator> alertAggregatorResultList = alertDao.alertsAggregates(alertAggregateVo, 1L);
		assertEquals(2, alertAggregatorResultList.size());
	}

	@Test
	public void alertsAggregates_whenGranularityIsYearlyTest() {
		alertAggregateVo.setGranularity("yearly");
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(alertAggregatorList);
		List<AlertAggregator> alertAggregatorResultList = alertDao.alertsAggregates(alertAggregateVo, 1L);
		assertEquals(2, alertAggregatorResultList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void alertsAggregates_whenFailedToExecuteQueryExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(FailedToExecuteQueryException.class);
		alertDao.searchAlertsCount("test");
	}

}
