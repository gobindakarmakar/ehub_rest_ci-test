package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/*
 * 
 * @author Prateek Maurya
 */

public interface SourceCategoryDao extends GenericDao<SourceCategory, Long> {

	boolean checkCategoryExists(String dtoCategory);

}
