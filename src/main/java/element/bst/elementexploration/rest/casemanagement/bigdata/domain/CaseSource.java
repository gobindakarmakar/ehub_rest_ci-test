package element.bst.elementexploration.rest.casemanagement.bigdata.domain;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CaseSource implements Serializable {
	
	private static final long serialVersionUID = 8050829496881397420L;
	
	private   ArrayList<Source> websites;

	public ArrayList<Source> getWebsites() {
		return websites;
	}

	public void setWebsites(ArrayList<Source> websites) {
		this.websites = websites;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
