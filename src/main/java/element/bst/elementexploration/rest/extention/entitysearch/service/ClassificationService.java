package element.bst.elementexploration.rest.extention.entitysearch.service;

import element.bst.elementexploration.rest.extention.entitysearch.domain.Classification;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface ClassificationService extends GenericService<Classification, Long>{
	
	public Classifications fetchClassification(String classsifcationName);

	
}
