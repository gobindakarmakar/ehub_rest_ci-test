package element.bst.elementexploration.rest.usermanagement.group.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "bst_user_group")
@Access(AccessType.FIELD)
public class Group implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "userGroupId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userGroupId;
	
	@Column
	private Date createdDate;
	
	@Column
	private Date modifiedDate;
	
	@Column(name = "parentUserGroupId")
	private Integer parentGroupId;
	
	@Column(name = "name",unique=true)
	private String name;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "active")
	private Integer active;
	
	@Column(name = "createdBy")
	private Long createdBy;

	@Column(name = "description")
	private String description;

	public Group() {
		super();
	}
			
	public Group(Long userGroupId, Date createdDate, Date modifiedDate, Integer parentGroupId, String name,
			String remarks, Integer active, Long createdBy, String description) {
		super();
		this.userGroupId = userGroupId;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.parentGroupId = parentGroupId;
		this.name = name;
		this.remarks = remarks;
		this.active = active;
		this.createdBy = createdBy;
		this.description = description;
	}

	public Long getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(Integer parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
