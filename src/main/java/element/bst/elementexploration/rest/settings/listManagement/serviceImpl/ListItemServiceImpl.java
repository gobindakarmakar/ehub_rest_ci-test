package element.bst.elementexploration.rest.settings.listManagement.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.activiti.app.service.exception.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceJurisdictionService;
import element.bst.elementexploration.rest.fileupload.dao.FileDao;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.dao.SettingsDao;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.listManagement.dao.ColorDao;
import element.bst.elementexploration.rest.settings.listManagement.dao.IconDao;
import element.bst.elementexploration.rest.settings.listManagement.dao.ListItemDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.AttributesService;

@Service("listItemService")
@Transactional("transactionManager")
public class ListItemServiceImpl extends GenericServiceImpl<ListItem, Long> implements ListItemService {

	
	@Autowired
	public ListItemServiceImpl(GenericDao<ListItem, Long> genericDao) {
		super(genericDao);
	}
	

	public ListItemServiceImpl() {

	}

	@Autowired
	ListItemDao listItemDao;

	@Autowired
	ColorDao colorDao;

	@Autowired
	IconDao iconDao;
	
	@Autowired
	SettingsDao settingsDao;
	
	@Autowired
	FileDao fileDao;
	
	@Autowired
	AttributesService attributeService;
	
	@Autowired
	SourceJurisdictionService sourceJurisdictionService;

	@Override
	@Transactional("transactionManager")
	public ListItemDto saveOrUpdateListItem(ListItemDto listItemDto) throws Exception {

		ListItem listItem =null;
		boolean isSaved=false;

		if(listItemDto.getDisplayName()==null || listItemDto.getDisplayName() == "" ) {
			throw new Exception("Display Name can't be null or empty!");
		}
		
		if(listItemDto.getCode()==null || listItemDto.getCode() == "" ) {
			throw new Exception("Code can't be null or empty!");
		}

		if (listItemDto != null && listItemDto.getListItemId()!=null) {
			listItem =	listItemDao.find(listItemDto.getListItemId());
			if(listItem ==null) {
				
				if(getListItemByListTypeAndDisplayName(listItemDto.getListType(), listItemDto.getDisplayName())!=null) {
					throw new Exception("Display Name already Exist for this List Type!");
				}
				
				
				if(getListItemByListTypeAndCode(listItemDto.getListType(), listItemDto.getCode())!=null) {
					throw new Exception("Code already Exist for this List Type!");
				}


				listItem = new ListItem();
				BeanUtils.copyProperties(listItemDto, listItem);
				listItem=listItemDao.create(listItem);
				BeanUtils.copyProperties(listItem, listItemDto);
				if(listItemDto.getListType().equalsIgnoreCase("Languages")) {
					saveLanguageTypeAttribute(listItemDto);
				}
				//create new object if 
				if(listItemDto.getListType().equalsIgnoreCase(ElementConstants.JURISDICTIONS_STRING)){
					SourceJurisdiction jurisdiction = null;
					if(listItemDto.getCode().equalsIgnoreCase("All")){
						jurisdiction = new SourceJurisdiction(listItemDto.getCode(), listItemDto.getDisplayName(), true);
					}else{
						jurisdiction = new SourceJurisdiction(listItemDto.getCode(), listItemDto.getDisplayName(), false);
					}
					jurisdiction.setListItemId(listItemDto.getListItemId());
					sourceJurisdictionService.save(jurisdiction);
				}
			}else {
				ListItemDto listItmDisplay=getListItemByListTypeAndDisplayName(listItemDto.getListType(), listItemDto.getDisplayName());
				
				ListItemDto listItmCode=getListItemByListTypeAndCode(listItemDto.getListType(), listItemDto.getCode());
				
				if(listItmDisplay!=null && !listItmDisplay.getListItemId().equals(listItemDto.getListItemId())) {
					throw new Exception("Display Name already Exist for this List Type!");
				}
				
				if(listItmCode!=null && !listItmCode.getListItemId().equals(listItemDto.getListItemId())) {
					throw new Exception("Code already Exist for this List Type!");
				}
				
				BeanUtils.copyProperties(listItemDto, listItem);
				listItemDao.saveOrUpdate(listItem);
				BeanUtils.copyProperties(listItem, listItemDto);
				if(listItemDto.getListType().equalsIgnoreCase("Languages")) {
					saveLanguageTypeAttribute(listItemDto);
				}
				
				if(listItemDto.getListType().equalsIgnoreCase(ElementConstants.JURISDICTIONS_STRING)){
					SourceJurisdiction jurisdiction = sourceJurisdictionService.findByListItemId(listItemDto.getListItemId());
					if(null != jurisdiction){
						jurisdiction.setJurisdictionName(listItemDto.getCode());
						jurisdiction.setJurisdictionOriginalName(listItemDto.getDisplayName());
						jurisdiction.setListItemId(listItemDto.getListItemId());
						sourceJurisdictionService.saveOrUpdate(jurisdiction);
					}
				}
			}
		} /*
			 * else {
			 * 
			 * if(listItemDto != null &&
			 * listItemDto.getListType().equalsIgnoreCase("Languages") &&
			 * getListItemByListTypeAndDisplayName(listItemDto.getListType(),
			 * listItemDto.getDisplayName())!=null) {
			 * 
			 * listItem = new ListItem(); ListItemDto
			 * listItemDTO=getListItemByListTypeAndDisplayName(listItemDto.getListType(),
			 * listItemDto.getDisplayName());
			 * listItemDto.setListItemId(listItemDTO.getListItemId());
			 * 
			 * BeanUtils.copyProperties(listItemDto, listItem);
			 * listItemDao.saveOrUpdate(listItem); BeanUtils.copyProperties(listItem,
			 * listItemDto); if(listItemDto.getListType().equalsIgnoreCase("Languages")) {
			 * saveLanguageTypeAttribute(listItemDto); } }else
			 * if(getListItemByListTypeAndDisplayName(listItemDto.getListType(),
			 * listItemDto.getDisplayName())!=null) { throw new
			 * Exception("Display Name :: "+listItemDto.getDisplayName()+
			 * " Type:: "+listItemDto.getListType() +"already Exist for this List Type!"); }
			 */
		else {

			if (listItemDto != null && listItemDto.getListType().equalsIgnoreCase("Languages")
					&& getListItemByListTypeAndDisplayName(listItemDto.getListType(),
							listItemDto.getDisplayName()) != null) {

				listItem = new ListItem();
				ListItem exisitingListItem = listItemDao.getListItemByListTypeAndDisplayName(listItemDto.getListType(),
						listItemDto.getDisplayName());
				// listItemDto.setListItemId(listItemDTO.getListItemId());
				if (exisitingListItem != null) {
					if (listItemDto.getAllowDelete() != null)
						exisitingListItem.setAllowDelete(listItemDto.getAllowDelete());
					if (listItemDto.getCode() != null)
						exisitingListItem.setCode(listItemDto.getCode());
					if (listItemDto.getColorCode() != null)
						exisitingListItem.setColorCode(listItemDto.getColorCode());
					if (listItemDto.getDisplayName() != null)
						exisitingListItem.setDisplayName(listItemDto.getDisplayName());
					if (listItemDto.getIcon()!= null)
						exisitingListItem.setIcon(listItemDto.getIcon());
					if (listItemDto.getListType() != null)
						exisitingListItem.setListType(listItemDto.getListType());
					if (listItemDto.getFlagName() != null)
						exisitingListItem.setFlagName(listItemDto.getFlagName());
					
					BeanUtils.copyProperties(listItemDto, listItem);
					listItemDao.saveOrUpdate(exisitingListItem);
					isSaved = true;
				} else {
					BeanUtils.copyProperties(listItemDto, listItem);
					listItemDao.saveOrUpdate(listItem);
					isSaved=true;
				}
				BeanUtils.copyProperties(listItem, listItemDto);
				if (listItemDto.getListType().equalsIgnoreCase("Languages") && isSaved) {
					saveLanguageTypeAttribute(listItemDto);
					
				}
				return listItemDto;
			} else if (getListItemByListTypeAndDisplayName(listItemDto.getListType(),
					listItemDto.getDisplayName()) != null) {
				throw new Exception("Display Name :: " + listItemDto.getDisplayName() + " Type:: "
						+ listItemDto.getListType() + "already Exist for this List Type!");
			}

			if (getListItemByListTypeAndCode(listItemDto.getListType(), listItemDto.getCode()) != null) {
				throw new Exception("Code already Exist for this List Type!");
			}

			listItem = new ListItem();
			BeanUtils.copyProperties(listItemDto, listItem);
			listItem = listItemDao.create(listItem);
			BeanUtils.copyProperties(listItem, listItemDto);
			
			if(listItemDto.getListType().equalsIgnoreCase(ElementConstants.JURISDICTIONS_STRING)){
				SourceJurisdiction jurisdiction = null;
				if(listItemDto.getCode().equalsIgnoreCase("All")){
					jurisdiction = new SourceJurisdiction(listItemDto.getCode(), listItemDto.getDisplayName(), true);
				}else{
					jurisdiction = new SourceJurisdiction(listItemDto.getCode(), listItemDto.getDisplayName(), false);
				}
				jurisdiction.setListItemId(listItemDto.getListItemId());
				sourceJurisdictionService.save(jurisdiction);
			}
			
			if (listItemDto.getListType().equalsIgnoreCase("Languages")) {
				saveLanguageTypeAttribute(listItemDto);
			}
		}
		return listItemDto;
	}
	
	public void saveLanguageTypeAttribute(ListItemDto listItemDto) {
		if(listItemDto.getListType().equalsIgnoreCase("Languages")) {
			boolean found = false;
			Settings languageSetting=null;
			List<Settings> languageSettings = settingsDao.fetchAllSettings(listItemDto.getListType());
			if(languageSettings!=null && languageSettings.size()>0) {
				languageSetting=languageSettings.get(0);
				if(languageSetting!=null) {
					List<Attributes> existingAttributes = languageSetting.getAttributesList();
					for (Attributes attribute : existingAttributes) {
						if (attribute.getAttributeName().equalsIgnoreCase(listItemDto.getDisplayName())) {
							attribute.setLanguageName(listItemDto.getDisplayName());
							if (listItemDto.isSelected())
								attribute.setSelected(listItemDto.isSelected());
							else
								attribute.setSelected(false);
							attributeService.saveOrUpdate(attribute);
							found = true;
						}
					}
				}
			}
			if(!found && languageSetting==null ) {
				Attributes newLanguageAttribute= new Attributes();
				newLanguageAttribute.setAttributeName(listItemDto.getDisplayName());
				newLanguageAttribute.setAttributeValue(listItemDto.getDisplayName());
				newLanguageAttribute.setLanguageName(listItemDto.getDisplayName());
				newLanguageAttribute.setSettings(languageSetting);
				attributeService.saveOrUpdate(newLanguageAttribute);
			}
		}
	}

	@Override
	@Transactional("transactionManager")
	public ListItemDto getListItemByID(Long listItemId) throws Exception {

		ListItem listItem =null;
		ListItemDto listItemDto=null;
		if (listItemId != null) {
			listItem =	listItemDao.find(listItemId);
			if(listItem !=null) {
				listItemDto = new ListItemDto();
				BeanUtils.copyProperties(listItem, listItemDto);
			}
		}
		return listItemDto;
	}

	/*
	 * @Override
	 * 
	 * @Transactional("transactionManager") public List<ListItemDto>
	 * getListItemByListType(String listType) throws Exception {
	 * 
	 * List<ListItem> listItem =null; List<ListItemDto> listItemDtoList= new
	 * ArrayList<ListItemDto>(); ListItemDto listItemDto=null; if (listType != null)
	 * {
	 * 
	 * 
	 * listItem = listItemDao.getListItemByListType(listType); for(ListItem listItm:
	 * listItem) { listItemDto = new ListItemDto();
	 * BeanUtils.copyProperties(listItm, listItemDto);
	 * listItemDtoList.add(listItemDto); } } return listItemDtoList; }
	 */
	
	
	@Override
	@Transactional("transactionManager")
	public List<ListItemDto> getListItemByListType(String listType) throws Exception {

		List<ListItem> listItem =null;
		List<ListItemDto> listItemDtoList= new ArrayList<ListItemDto>();
		ListItemDto listItemDto=null;
		Settings selectedLanguage=null;
		FileBst fileBst=null;
		if (listType != null) {

			listItem =	listItemDao.getListItemByListType(listType);
			for(ListItem listItm: listItem) {
				listItemDto = new ListItemDto();
				BeanUtils.copyProperties(listItm, listItemDto);

				if(listType.equalsIgnoreCase("Languages")) {
					selectedLanguage = 	settingsDao.fetchSettingByName("Languages");
					fileBst=fileDao.findFileByName(listItm.getDisplayName());
					listItemDto.setFile_id(fileBst.getId());
				}
				if(selectedLanguage!=null && selectedLanguage.getAttributesList()!=null && selectedLanguage.getAttributesList().size()!=0) {
					for(Attributes attribute :selectedLanguage.getAttributesList()) {
						if(attribute.getAttributeName().equalsIgnoreCase(listItm.getDisplayName())&&attribute.isSelected())
						{
							listItemDto.setIsSelected(true);
						}
					}

				}
				listItemDtoList.add(listItemDto);
			}
		}
		return listItemDtoList;
	}
	
	@Override
	@Transactional("transactionManager")
	public List<ListItemDto> getListItemByListTypeAndsearchKey(String listType,String searchKey) throws Exception {

		List<ListItem> listItem =null;
		List<ListItemDto> listItemDtoList= new ArrayList<ListItemDto>();
		ListItemDto listItemDto=null;
		if (listType != null) {
			listItem =	listItemDao.getListItemByListTypeAndSearchField(listType, searchKey);
			for(ListItem listItm: listItem) {
				listItemDto = new ListItemDto();
				BeanUtils.copyProperties(listItm, listItemDto);
				listItemDtoList.add(listItemDto);
			}
		}
		return listItemDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public ListItemDto getListItemByListTypeAndDisplayName(String listType,String displayName) throws Exception {

		ListItem listItem =null;
		ListItemDto listItemDto=null;
		if (listType != null && displayName!=null) {
			listItem =	listItemDao.getListItemByListTypeAndDisplayName(listType,displayName);
			if( listItem !=null) {
				listItemDto = new ListItemDto();
				BeanUtils.copyProperties(listItem, listItemDto);
			}
		}
		return listItemDto;
	}
	
	@Override
	@Transactional("transactionManager")
	public ListItemDto getListItemByListTypeAndCode(String listType,String code) throws Exception {

		ListItem listItem =null;
		ListItemDto listItemDto=null;
		if (listType != null && code!=null) {
			listItem =	listItemDao.getListItemByListTypeAndCode(listType,code);
			if( listItem !=null) {
				listItemDto = new ListItemDto();
				BeanUtils.copyProperties(listItem, listItemDto);
			}
		}
		return listItemDto;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteListItem(Long listItemId) {
		ListItem listItem = null;
		if (listItemId != null) {
			listItem = listItemDao.find(listItemId);
			if (listItem != null && listItem.getAllowDelete()) {
				if (listItem.getListType().equalsIgnoreCase("Languages")) {
					String listDisplayName = listItem.getDisplayName();
					if (listDisplayName != null) {
						List<Attributes> filteredAttributes = attributeService.findAll().stream()
								.filter(a -> a.getAttributeName().equalsIgnoreCase(listDisplayName))
								.collect(Collectors.toList());
						if(filteredAttributes.size()>0) {
							Settings setting = filteredAttributes.get(0).getSettings();
							if(setting.getDefaultValue().equalsIgnoreCase(listDisplayName)) {
								setting.setDefaultValue("");
								settingsDao.saveOrUpdate(setting);
							}

							if (filteredAttributes != null && filteredAttributes.size() > 0) {
								attributeService.delete(filteredAttributes.get(0));
							}
						}
				}
				}
				listItemDao.delete(listItem);
				
				if(listItem.getListType().equalsIgnoreCase(ElementConstants.JURISDICTIONS_STRING)){
					SourceJurisdiction jurisdiction = sourceJurisdictionService.findByListItemId(listItem.getListItemId());;
					if(null != jurisdiction)
						sourceJurisdictionService.delete(jurisdiction);
				}
				
				return true;
			} else {
				throw new NotFoundException("List Item not found!");
			}
		} else {
			throw new NotFoundException("List Item can't be null or empty");
		}
	}


	@Override
	public ListItemDto getListItemByListTypeAndLikeDisplayName(String listType, String displayName) {
		ListItem listItem =null;
		ListItemDto listItemDto=null;
		if (listType != null && displayName!=null) {
			List<ListItem> listItems =	listItemDao.getListItemByListTypeAndLikeDisplayName(listType,displayName);
			if( listItems !=null && listItems.size() > 0) {
				for(ListItem ls : listItems){
					String []names = ls.getDisplayName().split(",");
					for(String str : names){
						if(str.equalsIgnoreCase(displayName)){
							listItem = ls;
						}
					}
				}
				if(listItem != null){
					listItemDto = new ListItemDto();
					BeanUtils.copyProperties(listItem, listItemDto);
				}
			}
		}
		return listItemDto;
	}


}
