package element.bst.elementexploration.rest.extention.textsentiment.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.textsentiment.domain.TextSentiment;
import element.bst.elementexploration.rest.extention.textsentiment.service.TextSentimentService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("textSentimentService")
public class TextSentimentServiceImpl implements TextSentimentService {

	@Value("${bigdata_text_sentiment}")
	private String BIG_DATA_TEXT_SENTIMENT;

	@Override
	public String getTextSentiments(TextSentiment textSentiment) throws Exception {
		String url = BIG_DATA_TEXT_SENTIMENT;
		ObjectMapper mapper = new ObjectMapper();
		String replaceString = textSentiment.getText().substring(0, Math.min(textSentiment.getText().length(), 4500));
		textSentiment.setText(replaceString);
		String data = mapper.writeValueAsString(textSentiment);
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, data);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
}
