package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "document_top_events")
public class AnswerTopEvents implements Serializable{
	
	private static final long serialVersionUID = 768680922728836388L;
	
	private Long id;
	private DocumentAnswers documentAnswer;	
	private String locationName;
	private String title;
	private String text;
	private String url;
	private String technologies;
	private Date published;
	
	public AnswerTopEvents() {
		super();
		
	}

	public AnswerTopEvents(Long id, DocumentAnswers documentAnswer, String name, String locationName, String title,
			String text, String url, String technologies, Date published) {
		super();
		this.id = id;
		this.documentAnswer = documentAnswer;		
		this.locationName = locationName;
		this.title = title;
		this.text = text;
		this.url = url;
		this.technologies = technologies;
		this.published = published;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "document_answer_id")
	public DocumentAnswers getDocumentAnswer() {
		return documentAnswer;
	}

	public void setDocumentAnswer(DocumentAnswers documentAnswer) {
		this.documentAnswer = documentAnswer;
	}
	
	@Column(name = "location_name",columnDefinition = "LONGTEXT")
	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	@Column(name = "title",columnDefinition = "LONGTEXT")
	public String getTitle() {
		return title;
	}	

	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name = "text",columnDefinition = "LONGTEXT")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	@Column(name = "url",columnDefinition = "LONGTEXT")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	@Column(name = "technologies",columnDefinition = "LONGTEXT")
	public String getTechnologies() {
		return technologies;
	}

	public void setTechnologies(String technologies) {
		this.technologies = technologies;
	}
	@Column(name = "publishedOn")
	public Date getPublished() {
		return published;
	}

	public void setPublished(Date published) {
		this.published = published;
	}
	
		
}
