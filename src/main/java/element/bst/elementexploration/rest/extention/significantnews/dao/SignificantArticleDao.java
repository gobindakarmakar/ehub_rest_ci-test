package element.bst.elementexploration.rest.extention.significantnews.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantArticles;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface SignificantArticleDao extends GenericDao<SignificantArticles, Long>{

	SignificantArticles getSignificantArticles(String uuid);

	List<SignificantArticles> findArticlesByEntityId(String entityId);

}
