package element.bst.elementexploration.rest.extention.docparser.serviceImpl.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;

public class DocumentQuestionsServiceTest {
	
	@Mock
	DocumentQuestionsService documentQuestionsServiceMock;	
	
	public List<DocumentQuestions> documentQuestionsList;
	
	@Before
    public void setupMock() {
		DocumentQuestions question1= new DocumentQuestions();
		DocumentQuestions question2= new DocumentQuestions();
		documentQuestionsList = new ArrayList<DocumentQuestions>();
		documentQuestionsList.add(question1);
		documentQuestionsList.add(question2);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void findQuestionsTest() {	
		Mockito.when(documentQuestionsServiceMock.findQuestions(Mockito.anyLong())).thenReturn(10);
		int count = documentQuestionsServiceMock.findQuestions(Mockito.anyLong());
		assertEquals(10,count);
		
	}
	
	@Test
	public void getListOfControlQuestionTest() {	
		Mockito.when(documentQuestionsServiceMock.getListOfControlQuestion()).thenReturn(documentQuestionsList);
		List<DocumentQuestions> newDocumentQuestionsList = documentQuestionsServiceMock.getListOfControlQuestion();
		assertEquals(2,newDocumentQuestionsList.size());
		
	}
	
	@Test
	public void deleteQuestionsTest() {	
		Mockito.when(documentQuestionsServiceMock.deleteQuestion(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		boolean status = documentQuestionsServiceMock.deleteQuestion(Mockito.anyLong(),Mockito.anyLong());
		assertEquals(true,status);		
	}
	
	

}
