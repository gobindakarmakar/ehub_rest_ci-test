package element.bst.elementexploration.rest.extention.sourcecredibility.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface SourceIndustryDao extends GenericDao<SourceIndustry, Long> {

	SourceIndustry fetchIndustry(String name);
	List<SourceIndustry> fetchIndustryListExceptAll();

}
