package element.bst.elementexploration.rest.extention.docparser.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplatesDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplates;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
/**
 * 
 * @author Rambabu
 *
 */
@Repository("documentTemplatesDao")
public class DocumentTemplatesDaoImpl extends GenericDaoImpl<DocumentTemplates, Long> implements DocumentTemplatesDao {

	public DocumentTemplatesDaoImpl() {
		super(DocumentTemplates.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentTemplates> getDocumentTemplatesByFileExtension(String fileExtension) {
		List<DocumentTemplates> documentTemplatesList=new ArrayList<DocumentTemplates>();
		try{
			Query<?> query = this.getCurrentSession().createQuery("select dt from DocumentTemplates dt where dt.templateFileType = :fileExtension and dt.deletedStatus = :isDelete");
			query.setParameter("fileExtension", fileExtension);
			query.setParameter("isDelete", false);
			documentTemplatesList = (List<DocumentTemplates>) query.getResultList();
			return documentTemplatesList;
		}catch (Exception e) {
			return documentTemplatesList;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentTemplates> getDocumentTemplatesList(Integer recordsPerPage, Integer pageNumber,boolean isPagination,String searchKey ) {
		List<DocumentTemplates> documentTemplatesList=new ArrayList<DocumentTemplates>();
		try{
			Query<?> query  = null;
			if(!isPagination)
			{
				StringBuilder builder = new StringBuilder();
				builder.append("select dt from DocumentTemplates dt where dt.deletedStatus = :isDelete and dt.docParserTemplate = :docParsed");
				if (!StringUtils.isEmpty(searchKey)) 
						builder.append(" and " + " dt.templateName like '%" + searchKey.trim() + "%'");
				 query = this.getCurrentSession().createQuery(builder.toString());			
				 query.setParameter("isDelete", false);
				 query.setParameter("docParsed", true);
			}
			else
			{
				StringBuilder builder = new StringBuilder();
				builder.append("select dt from DocumentTemplates dt where dt.deletedStatus = :isDelete and dt.docParserTemplate = :docParsed");
				if (!StringUtils.isEmpty(searchKey)) 
						builder.append(" and " + " dt.templateName like '%" + searchKey.trim() + "%'");
				 query = this.getCurrentSession().createQuery(builder.toString());			
				query.setParameter("isDelete", false);
				query.setParameter("docParsed", true);
				query.setFirstResult((pageNumber - 1) * (recordsPerPage));
				query.setMaxResults(recordsPerPage);
			}
			documentTemplatesList = (List<DocumentTemplates>) query.getResultList();
			return documentTemplatesList;
		}catch (Exception e) {
			return documentTemplatesList;
		}
		
	}

	
	
	

}
