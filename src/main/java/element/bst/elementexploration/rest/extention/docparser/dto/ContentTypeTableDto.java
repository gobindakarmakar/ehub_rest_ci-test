package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
/**
 * @author rambabu
 *
 */
import java.util.List;

import io.swagger.annotations.ApiModel;

@ApiModel("Table contents and content type")
public class ContentTypeTableDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String contentType;
	private List<TableColumnsDto> content;

	public ContentTypeTableDto() {
		super();
	}

	public ContentTypeTableDto(String contentType, List<TableColumnsDto> content) {
		super();
		this.contentType = contentType;
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public List<TableColumnsDto> getContent() {
		return content;
	}

	public void setContent(List<TableColumnsDto> content) {
		this.content = content;
	}

}
