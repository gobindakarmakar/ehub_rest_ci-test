/*package element.bst.elementexploration.test.extention.sourcemanagement.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.sourcemanagement.daoimpl.SourceClassificationDaoImpl;
import element.bst.elementexploration.rest.extention.sourcemanagement.domain.SourceClassification;

*//**
 * @author suresh
 *
 *//*
@RunWith(MockitoJUnitRunner.class)
public class SourceClassificationDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourceClassificationDaoImpl sourceClassificationDao;

	private SourceClassification sourceClassification;
	
	List<SourceClassification> sourceClassificationList;

	@Before
	public void setUp() {
		sourceClassificationList = new ArrayList<SourceClassification>();
		sourceClassification = new SourceClassification(1L, "BST", "http:yahho.com", "Medium", "finance", "GENERAL",
				true, false, false, false, true, true, null, null, null, null);
		sourceClassificationList.add(sourceClassification);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getSources() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(sourceClassificationList);
		List<SourceClassification> sourceClassifications = sourceClassificationDao.getSources(1, 10, "High", true,
				"BST");
		assertEquals(sourceClassifications.size(), sourceClassificationList.size());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = InternalError.class)
	public void deleteSourceWhenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenThrow(InternalError.class);
		sourceClassificationDao.getSources(1, 10, "High", true, "BST");
	}

}
*/