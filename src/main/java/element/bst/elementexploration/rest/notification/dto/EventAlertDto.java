package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author Rambabu
 *
 */
@ApiModel("Event alert")
public class EventAlertDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@ApiModelProperty(value="Subject of the notification",required=true)	
	private String subject;

	@ApiModelProperty(value="Body of the notification",required=true)	
	private String body;
	

	@ApiModelProperty(value="Type of the notification")	
	private Integer type;	

	@ApiModelProperty(value="Level of the notification")	
	private Integer level;
	
	@ApiModelProperty(value="Flag of the notification")	
	private Integer flag;

	@ApiModelProperty(value="CaseId  of the notification")	
	private Long caseId;		
	
	@ApiModelProperty(value="Recipients of the notification",required=true)
	private ArrayList<Long> recipients;

	@ApiModelProperty(value="Scheduled From  of the notification")	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledFrom;

	@ApiModelProperty(value="Scheduled To  of the notification")	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime scheduledTo;

	public EventAlertDto() {
		super();		
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public ArrayList<Long> getRecipients() {
		return recipients;
	}

	public void setRecipients(ArrayList<Long> recipients) {
		this.recipients = recipients;
	}

	public LocalDateTime getScheduledFrom() {
		return scheduledFrom;
	}

	public void setScheduledFrom(LocalDateTime scheduledFrom) {
		this.scheduledFrom = scheduledFrom;
	}

	public LocalDateTime getScheduledTo() {
		return scheduledTo;
	}

	public void setScheduledTo(LocalDateTime scheduledTo) {
		this.scheduledTo = scheduledTo;
	}

	

}
