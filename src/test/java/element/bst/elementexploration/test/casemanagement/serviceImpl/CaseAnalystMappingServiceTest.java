package element.bst.elementexploration.test.casemanagement.serviceImpl;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.casemanagement.dao.CaseAnalystMappingDao;
import element.bst.elementexploration.rest.casemanagement.dao.CaseDao;
import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.casemanagement.dto.CaseCommentNotificationDto;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDto;
import element.bst.elementexploration.rest.casemanagement.enumType.PriorityEnums;
import element.bst.elementexploration.rest.casemanagement.serviceImpl.CaseAnalystMappingServiceImpl;
import element.bst.elementexploration.rest.exception.CaseSeedNotFoundException;
import element.bst.elementexploration.rest.exception.DateFormatException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.exception.PermissionDeniedException;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CaseAnalystMappingServiceTest {
	
	@Mock
	CaseAnalystMappingDao caseAnalystMappingDaoMock;
	
	@Mock
	UsersDao usersDao;
	
	@Mock
	ListItem listItem;
	
	@Mock
	UsersService usersService;
	
	@Mock
	CaseDao caseDao;
	
	@InjectMocks
	CaseAnalystMappingServiceImpl caseAnalystMappingService;
	
	public List<CaseDto> caseDtoList = new ArrayList<CaseDto>();
	
	public CaseCommentNotificationDto caseCommentNotificationDto;
	
	public CaseAnalystMapping caseAnalystMapping;
	
	public Case caseSeed;
	
	public Users user;
	
	@Before
    public void setupMock() {
		
		CaseDto caseDto1 = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDto1);
		CaseDto caseDto2 = new CaseDto(1L, "junit test case", "Testing unit test case", "", PriorityEnums.MEDIUM,2, 50.00, 45.00, 10.00, 10.00,new Date(),2L,new Date(),2L,"test");
		caseDtoList.add(caseDto2);
		caseCommentNotificationDto = new CaseCommentNotificationDto("Test case comment notification",new Date());
		caseAnalystMapping = new CaseAnalystMapping();
		caseSeed = new Case();
		caseSeed.setCaseId(1L);
		caseSeed.setName("Test case");
		caseSeed.setCurrentStatus(4);
		
		user=new Users("analyst","Analyst","Analyst",new Date(),listItem);
		caseAnalystMapping = new CaseAnalystMapping(1L, 4, new Date(), new Date(), "test case comment",new Date(),caseSeed, user);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected = DateFormatException.class)
	public void updateCaseStatus_whenDateExceptionThrownTest() throws ParseException {		
		caseAnalystMappingService.updateCaseStatus(1L,2L,"test case comment",new Date().toString(),4,5);
	}
	
	/*@Test(expected = CaseSeedNotFoundException.class)
	public void updateCaseStatus_whenCaseNotFoundExceptionThrownTest() throws ParseException {	
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getAnalystMappingBasedOnUserId(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(caseAnalystMapping);
		Mockito.doNothing().when(caseAnalystMappingDaoMock).createMapping(Mockito.any(CaseAnalystMapping.class));
		Mockito.doNothing().when(caseAnalystMappingDaoMock).saveOrUpdate(Mockito.any(CaseAnalystMapping.class));
		caseAnalystMappingService.updateCaseStatus(1L,2L,"test case comment",new SimpleDateFormat("dd-MM-yyyy").format(new Date()),4,6);
	}*/
	
	@Test(expected = CaseSeedNotFoundException.class)
	public void updateCaseStatus_whenCaseNotFoundExceptionThrownTest() throws ParseException {	
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(null);
		Mockito.when(usersService.prepareUserFromFirebase(Mockito.any(Users.class))).thenReturn(user);
		caseAnalystMappingService.updateCaseStatus(1L,2L,"test case comment",new SimpleDateFormat("dd-MM-yyyy").format(new Date()),4,5);
	}
	
	
	@Test(expected = NoDataFoundException.class)
	public void caseReassignment_whenUserOrCaseIsNullTest() {
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(null);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(null);
		caseAnalystMappingService.caseReassignment(1L,2L,3L,"test case comment");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PermissionDeniedException.class)
	public void caseReassignment_whenCaseAnalystMappingNullTest() {
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(null);
		caseAnalystMappingService.caseReassignment(1L,2L,3L,"test case comment");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PermissionDeniedException.class)
	public void caseReassignment_whenCurrentStatusNotInSubmittedOrAcknowledgeTest() {
		caseSeed.setCurrentStatus(8);
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(caseAnalystMapping);
		caseAnalystMappingService.caseReassignment(1L,2L,3L,"test case comment");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void caseReassignment_whenExceptionThrowTest() {
		caseSeed.setCurrentStatus(3);
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(usersDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(caseAnalystMapping);
		Mockito.doNothing().when(caseAnalystMappingDaoMock).createMapping(Mockito.any(CaseAnalystMapping.class));
		Mockito.doNothing().when(caseAnalystMappingDaoMock).saveOrUpdate(Mockito.any(CaseAnalystMapping.class));
		Mockito.when(caseAnalystMappingDaoMock.deleteCaseFromFocusById(Mockito.anyLong(),Mockito.anyLong())).thenThrow(Exception.class);
		caseAnalystMappingService.caseReassignment(1L,2L,3L,"test case comment");
	}
	
	@Test(expected = NoDataFoundException.class)
	public void caseForwarding_whenUserOrCaseIsNullTest() {
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(null);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(null);
		caseAnalystMappingService.caseForwarding(1L,2L,3L,"test case comment");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PermissionDeniedException.class)
	public void caseForwarding_whenCaseAnalystMappingNullTest() {
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(null);
		caseAnalystMappingService.caseForwarding(1L,2L,3L,"test case comment");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PermissionDeniedException.class)
	public void caseForwarding_whenCurrentStatusNotInSubmittedOrAcknowledgeTest() {
		caseSeed.setCurrentStatus(5);
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(caseAnalystMapping);
		caseAnalystMappingService.caseForwarding(1L,2L,3L,"test case comment");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void caseForwarding_whenExceptionThrowTest() {
		caseSeed.setCurrentStatus(3);
		Mockito.when(usersDao.getAnalystByUserId(Mockito.anyLong())).thenReturn(user);
		Mockito.when(usersDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(caseAnalystMapping);
		Mockito.doNothing().when(caseAnalystMappingDaoMock).createMapping(Mockito.any(CaseAnalystMapping.class));
		Mockito.doNothing().when(caseAnalystMappingDaoMock).saveOrUpdate(Mockito.any(CaseAnalystMapping.class));
		Mockito.when(caseAnalystMappingDaoMock.deleteCaseFromFocusById(Mockito.anyLong(),Mockito.anyLong())).thenThrow(Exception.class);
		caseAnalystMappingService.caseForwarding(1L,2L,3L,"test case comment");
	}
	
	
	
	//@SuppressWarnings("unchecked")
	@Test
	public void addStatusFocusTest() {
		Mockito.when(caseAnalystMappingDaoMock.getAnalystMappingBasedOnUserId(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(null);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.any())).thenReturn(caseAnalystMapping);
		Mockito.when(usersDao.find(Mockito.anyLong())).thenReturn(user);
		Mockito.when(usersService.prepareUserFromFirebase(Mockito.any(Users.class))).thenReturn(user);
		Mockito.when(caseDao.find(Mockito.anyLong())).thenReturn(caseSeed);
		Mockito.when(caseAnalystMappingDaoMock.create(Mockito.any(CaseAnalystMapping.class))).thenReturn(caseAnalystMapping);
		boolean status = caseAnalystMappingService.addStatusFocus(1L,2L);
		assertEquals(true,status);
	}
	
	
	@Test
	public void addStatusFocus_whenStatusIsFalseTest() {
		Mockito.when(caseAnalystMappingDaoMock.getAnalystMappingBasedOnUserId(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(caseAnalystMapping);
		boolean status = caseAnalystMappingService.addStatusFocus(1L,2L);
		assertEquals(false,status);
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Test(expected = PermissionDeniedException.class)
	public void addStatusFocus_WhenExceptionThrowTest() {
		Mockito.when(caseAnalystMappingDaoMock.getAnalystMappingBasedOnUserId(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyInt())).thenReturn(null);
		Mockito.when(caseAnalystMappingDaoMock.getCaseInStatusList(Mockito.anyLong(),Mockito.anyLong(),Mockito.anyList())).thenReturn(null);
		caseAnalystMappingService.addStatusFocus(1L,2L);
	}
	
	@Test
	public void deleteCaseFromFocusByIdTest() {
		Mockito.when(caseAnalystMappingDaoMock.deleteCaseFromFocusById(Mockito.anyLong(),Mockito.anyLong())).thenReturn(true);
		boolean status = caseAnalystMappingService.deleteCaseFromFocusById(1L,2L);
		assertEquals(true,status);
	}
	
	@Test
	public void getAllCasesInFocusByIdTest() {
		Mockito.when(caseAnalystMappingDaoMock.getAllCasesInFocusById(Mockito.anyLong(),Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(caseDtoList);
		List<CaseDto> resultCaseDtoList = caseAnalystMappingService.getAllCasesInFocusById(1L,"name","desc",1,12);
		assertEquals(2,resultCaseDtoList.size());
	}

	@Test
	public void countAllCasesInFocusByIdTest() {
		Mockito.when(caseAnalystMappingDaoMock.countAllCasesInFocusById(Mockito.anyLong())).thenReturn(2L);
		long count = caseAnalystMappingService.countAllCasesInFocusById(1L);
		assertEquals(2L,count);
	}
	
	@Test
	public void fullTextSearchForCasesInFocusTest() throws ParseException {
		Mockito.when(caseAnalystMappingDaoMock.fullTextSearchForCasesInFocus(Mockito.anyLong(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.any(Date.class),Mockito.anyString(),Mockito.anyString())).thenReturn(caseDtoList);
		List<CaseDto> resultCaseDtoList = caseAnalystMappingService.fullTextSearchForCasesInFocus(1L,"text",1,11,new SimpleDateFormat("dd-MM-yyyy").format(new Date()),"name","desc");
		assertEquals(2,resultCaseDtoList.size());
	}
	
	@Test(expected = DateFormatException.class)
	public void fullTextSearchForCasesInFocus_WhenExceptionTHrowTest() throws ParseException {
		caseAnalystMappingService.fullTextSearchForCasesInFocus(1L,"text",1,11,new Date().toString(),"name","desc");
	}
	
	@Test
	public void countfullTextSearchForCaseInFocusTest() throws ParseException {
		Mockito.when(caseAnalystMappingDaoMock.countfullTextSearchForCaseInFocus(Mockito.anyLong(),Mockito.anyString(),Mockito.any(Date.class))).thenReturn(2L);
		long count = caseAnalystMappingService.countfullTextSearchForCaseInFocus(1L,"test",new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		assertEquals(2L,count);
	}
	
	@Test(expected = DateFormatException.class)
	public void countfullTextSearchForCaseInFocus_WhenExceptionThrowTest() throws ParseException {
		caseAnalystMappingService.countfullTextSearchForCaseInFocus(1L,"test",new Date().toString());
		
	}
	
	
	
	@Test
	public void getCaseCommentNotificationByIdTest() {
		Mockito.when(caseAnalystMappingDaoMock.getCaseCommentNotificationById(Mockito.anyLong(),Mockito.anyLong())).thenReturn(caseCommentNotificationDto);
		CaseCommentNotificationDto caseCommentNotificationDtoNew = caseAnalystMappingService.getCaseCommentNotificationById(1L,2L);
		Assert.assertNotNull(caseCommentNotificationDtoNew.getComment());
	}
	
	@Test
	public void findByCaseIdTest() {
		Mockito.when(caseAnalystMappingDaoMock.findByCaseId(Mockito.anyLong())).thenReturn(caseAnalystMapping);
		CaseAnalystMapping newCaseAnalystMapping = caseAnalystMappingService.findByCaseId(1L);
		Assert.assertNotNull(newCaseAnalystMapping.getCurrentStatus());
	}

}
