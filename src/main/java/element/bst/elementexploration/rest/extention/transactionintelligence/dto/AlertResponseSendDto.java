package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Alert Response")
public class AlertResponseSendDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Alert business date")	
	@JsonProperty("date")
	private Date alertBusinessDate;
	@ApiModelProperty(value="Alert age")	
	private int age;
	@ApiModelProperty(value="Alert jurisdiction")	
	private String jurisdiction;
	@ApiModelProperty(value="Search name")	
	@JsonProperty("search-name")
	private String searchName;
	@ApiModelProperty(value="Row status")	
	@JsonProperty("resolved")
	private boolean rowStatus;
	@ApiModelProperty(value="status")	
	@JsonProperty("status")
	private String statusCd;
	@ApiModelProperty(value="Counstomer id")	
	@JsonProperty("focal-entity-id")
	private String focalNtityDisplayId;
	@ApiModelProperty(value="Customer name")	
	@JsonProperty("focal-entity")
	private String focalNtityDisplayName;
	@ApiModelProperty(value="Alert scenario")	
	private String scenario;
	@ApiModelProperty(value="Alert id")	
	private String alertId;
	@ApiModelProperty(value="Account type")	
	private String accountType;
	@ApiModelProperty(value="Id")	
	private Long id;
	@ApiModelProperty(value="Maximum amount")	
	private Double maximumAmount;
	@ApiModelProperty(value="Alert description")	
	private String alertDescription;
	@ApiModelProperty(value="Alert scenario type")	
	private String alertScenarioType;
	@ApiModelProperty(value="Transaction product type")	
	private String transactionProductType;
	@ApiModelProperty(value="Transaction id")	
	private Long transactionId;
	@ApiModelProperty(value="comment")	
	private String comment;

	public AlertResponseSendDto() {
		super();
	}

	public AlertResponseSendDto(Date alertBusinessDate, int age, String jurisdiction, String searchName,
			boolean rowStatus, String statusCd, String focalNtityDisplayId, String focalNtityDisplayName,
			String scenario, String alertId, String accountType, Long id, Double maximumAmount, String alertDescription,
			String alertScenarioType, String transactionProductType, Long transactionId, String comment) {
		super();
		this.alertBusinessDate = alertBusinessDate;
		this.age = age;
		this.jurisdiction = jurisdiction;
		this.searchName = searchName;
		this.rowStatus = rowStatus;
		this.statusCd = statusCd;
		this.focalNtityDisplayId = focalNtityDisplayId;
		this.focalNtityDisplayName = focalNtityDisplayName;
		this.scenario = scenario;
		this.alertId = alertId;
		this.accountType = accountType;
		this.id = id;
		this.maximumAmount = maximumAmount;
		this.alertDescription = alertDescription;
		this.alertScenarioType = alertScenarioType;
		this.transactionProductType = transactionProductType;
		this.transactionId = transactionId;
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public boolean isRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(boolean rowStatus) {
		this.rowStatus = rowStatus;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getFocalNtityDisplayId() {
		return focalNtityDisplayId;
	}

	public void setFocalNtityDisplayId(String focalNtityDisplayId) {
		this.focalNtityDisplayId = focalNtityDisplayId;
	}

	public String getFocalNtityDisplayName() {
		return focalNtityDisplayName;
	}

	public void setFocalNtityDisplayName(String focalNtityDisplayName) {
		this.focalNtityDisplayName = focalNtityDisplayName;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public String getAlertScenarioType() {
		return alertScenarioType;
	}

	public void setAlertScenarioType(String alertScenarioType) {
		this.alertScenarioType = alertScenarioType;
	}

	public String getTransactionProductType() {
		return transactionProductType;
	}

	public void setTransactionProductType(String transactionProductType) {
		this.transactionProductType = transactionProductType;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
