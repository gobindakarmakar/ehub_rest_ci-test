package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AccountDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface AccountService extends GenericService<Account, Long>{

	
	
	Boolean fetchAccountDetails(MultipartFile file) throws IOException, ParseException,IllegalAccessException, InvocationTargetException,Exception   ;
	public boolean checkExistenceOfAccount(List<TxDto> accountIds);
	public boolean addBankAccount();
	void updateAccount(AccountDto accountDto) throws Exception;
	
}
