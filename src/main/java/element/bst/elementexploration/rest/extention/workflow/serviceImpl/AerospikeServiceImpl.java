package element.bst.elementexploration.rest.extention.workflow.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.workflow.service.AerospikeService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("aerospikeService")
public class AerospikeServiceImpl implements AerospikeService {

	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENTION_API;

	@Override
	public String listOfAerospikeDatabases(String profileId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/aero/api/namespaces/" + encode(profileId);
		return getDataFromServer(url);	
	}

	@Override
	public String listOfAerospikeTables(String profileId, String namespace) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/aero/api/namespaces/" + encode(profileId) + "/" + encode(namespace) + "/sets";
		return getDataFromServer(url);	
	}

	@Override
	public String sampledata(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/aero/api/sampledata/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	@Override
	public String schema(String workflowId, String stageId) throws Exception {
		String url = WORKFLOW_EXTENTION_API + "/aero/api/schema/" + encode(workflowId) + "/" + encode(stageId);
		return getDataFromServer(url);	
	}

	private String getDataFromServer(String url) throws Exception{
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
	
	private String encode(String value) throws UnsupportedEncodingException {
		String encodedString = URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		return encodedString;

	}
}
