package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceCredibilityDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceDomainDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceIndustryDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceJurisdictionDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceMediaDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourcesDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourcesHideStatusDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.DataAttributes;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceDomain;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceIndustry;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceMedia;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourcesHideStatus;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SubClassifications;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.DataAttributesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.DataObjectDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceDomainDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceFilterDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceIndustryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceJurisdictionDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceMediaDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesHideStatusDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SubClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.enumType.CredibilityEnums;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.ClassificationsService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.DataAttributesService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCategoryService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCredibilityService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesHideStatusService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourcesService;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SubClassificationsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.EntityConverter;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

/**
 * @author suresh
 *
 */
@Service("sourcesService")
public class SourcesServiceImpl extends GenericServiceImpl<Sources, Long> implements SourcesService {

	@Autowired
	public SourcesServiceImpl(GenericDao<Sources, Long> genericDao) {
		super(genericDao);
	}

	public SourcesServiceImpl() {
	}

	@Autowired
	ClassificationsService classificationsService;

	@Autowired
	SourcesService sourcesService;

	@Autowired
	SourcesDao sourcesDao;

	@Autowired
	SourceCredibilityDao sourceCredibilityDao;
	
	@Autowired
	SourceCategoryService sourceCategoryService;

	@Autowired
	SourceCredibilityService sourceCredibilityService;

	@Value("${source_management_general_url_id}")
	private String SOURCE_URL_ID;

	@Value("${source_management_general_url_profiles}")
	private String SOURCE_URL_PROFILES;
	
	@Value("${big_data_domains_list_url}")
	private String BIG_DATA_DOMAINS_LIST_URL;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	SourceDomainDao sourceDomainDao;

	@Autowired
	SourceIndustryDao sourceIndustryDao;

	@Autowired
	SourceMediaDao sourceMediaDao;

	@Autowired
	SourceJurisdictionDao sourceJurisdictionDao;

	@Autowired
	SubClassificationsService subClassificationService;

	@Autowired
	DataAttributesService dataAttributesService;

	@Autowired
	ObjectMapper mapper;

	@Autowired
	SourcesHideStatusService sourcesHideStatusService;

	@Autowired
	SourcesHideStatusDao sourcesHideStatusDao;

	@Override
	@Transactional("transactionManager")
	public boolean saveMasterDataClassifications() throws Exception {
		Classifications classificationsOne = new Classifications();
		classificationsOne.setClassifcationName("GENERAL");
		Classifications classificationsTwo = new Classifications();
		classificationsTwo.setClassifcationName("NEWS");
		Classifications classificationsThree = new Classifications();
		classificationsThree.setClassifcationName("INDEX");
		List<SubClassifications> subClassificationsList = new ArrayList<SubClassifications>();
		SubClassifications subClassificationsOne = new SubClassifications();
		subClassificationsOne.setClassifications(classificationsOne);
		subClassificationsOne.setSubClassifcationName("Company Information");
		subClassificationsList.add(subClassificationsOne);
		SubClassifications subClassificationsTwo = new SubClassifications();
		subClassificationsTwo.setClassifications(classificationsOne);
		subClassificationsTwo.setSubClassifcationName("Financial");
		subClassificationsList.add(subClassificationsTwo);
		SubClassifications subClassificationsThree = new SubClassifications();
		subClassificationsThree.setClassifications(classificationsOne);
		subClassificationsThree.setSubClassifcationName("Corporate Structure");
		subClassificationsList.add(subClassificationsThree);
		SubClassifications subClassificationsFour = new SubClassifications();
		subClassificationsFour.setClassifications(classificationsOne);
		subClassificationsFour.setSubClassifcationName("Social Media");
		subClassificationsList.add(subClassificationsFour);
		SubClassifications subClassificationsFive = new SubClassifications();
		subClassificationsFive.setClassifications(classificationsOne);
		subClassificationsFive.setSubClassifcationName("Key Management");
		subClassificationsList.add(subClassificationsFive);
		for (SubClassifications subClassifications : subClassificationsList) {
			JSONParser parser = new JSONParser();
			File file = ResourceUtils.getFile("classpath:DataAttributes.json");
			Object jsonfiledata = parser.parse(new FileReader(file));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) jsonfiledata;
			if (subClassifications.getSubClassifcationName().equals("Company Information")) {
				if (file.exists() && file.length() > 0) {
					List<DataAttributes> attributesList = new ArrayList<DataAttributes>();
					org.json.simple.JSONArray jsonArray = (org.json.simple.JSONArray) jsonObject
							.get("Company Information");
					for (int i = 0; i < jsonArray.size(); i++) {
						org.json.simple.JSONObject jsonObjectSource = (org.json.simple.JSONObject) jsonArray.get(i);
						DataAttributes attributes = new DataAttributes();
						attributes.setSourceAttributeName(jsonObjectSource.get("sourceAttributeName").toString());
						attributes.setSourceAttributeSchema(jsonObjectSource.get("sourceAttributeSchema").toString());
						attributes.setSourceAttributeSchemaGroup("Overview");
						attributes.setSubClassifications(subClassifications);
						attributesList.add(attributes);
					}
					subClassifications.setDataAttributes(attributesList);
				}
			}
			if (subClassifications.getSubClassifcationName().equals("Financial")) {
				if (file.exists() && file.length() > 0) {
					List<DataAttributes> attributesList = new ArrayList<DataAttributes>();
					org.json.simple.JSONArray jsonArray = (org.json.simple.JSONArray) jsonObject.get("Financial");
					for (int i = 0; i < jsonArray.size(); i++) {
						org.json.simple.JSONObject jsonObjectSource = (org.json.simple.JSONObject) jsonArray.get(i);
						DataAttributes attributes = new DataAttributes();
						attributes.setSourceAttributeName(jsonObjectSource.get("sourceAttributeName").toString());
						attributes.setSourceAttributeSchema(jsonObjectSource.get("sourceAttributeSchema").toString());
						attributes.setSourceAttributeSchemaGroup("Finacial_Information");
						attributes.setSubClassifications(subClassifications);
						attributesList.add(attributes);
					}
					subClassifications.setDataAttributes(attributesList);
				}
			}
		}
		classificationsOne.setSubClassifications(subClassificationsList);

		List<SubClassifications> subClassificationsListNews = new ArrayList<SubClassifications>();
		SubClassifications subClassificationsNewsOne = new SubClassifications();
		subClassificationsNewsOne.setClassifications(classificationsTwo);
		subClassificationsNewsOne.setSubClassifcationName("General");
		SubClassifications subClassificationsNewsTwo = new SubClassifications();
		subClassificationsNewsTwo.setClassifications(classificationsTwo);
		subClassificationsNewsTwo.setSubClassifcationName("Press Release");
		SubClassifications subClassificationsNewsThree = new SubClassifications();
		subClassificationsNewsThree.setClassifications(classificationsTwo);
		subClassificationsNewsThree.setSubClassifcationName("Business and Financial");
		SubClassifications subClassificationsNewsFour = new SubClassifications();
		subClassificationsNewsFour.setClassifications(classificationsTwo);
		subClassificationsNewsFour.setSubClassifcationName("Education");
		SubClassifications subClassificationsNewsFive = new SubClassifications();
		subClassificationsNewsFive.setClassifications(classificationsTwo);
		subClassificationsNewsFive.setSubClassifcationName("Science and Technology");
		SubClassifications subClassificationsNewsSix = new SubClassifications();
		subClassificationsNewsSix.setClassifications(classificationsTwo);
		subClassificationsNewsSix.setSubClassifcationName("International");
		subClassificationsListNews.add(subClassificationsNewsOne);
		subClassificationsListNews.add(subClassificationsNewsTwo);
		subClassificationsListNews.add(subClassificationsNewsThree);
		subClassificationsListNews.add(subClassificationsNewsFour);
		subClassificationsListNews.add(subClassificationsNewsFive);
		subClassificationsListNews.add(subClassificationsNewsSix);
		classificationsTwo.setSubClassifications(subClassificationsListNews);
		Classifications classificationsSavedGeneral = classificationsService.save(classificationsOne);
		Classifications classificationsSavedNews = classificationsService.save(classificationsTwo);

		List<SubClassifications> subClassificationsListIndex = new ArrayList<SubClassifications>();
		SubClassifications subClassificationsIndexOne = new SubClassifications();
		subClassificationsIndexOne.setClassifications(classificationsThree);
		subClassificationsIndexOne.setSubClassifcationName("Sanctions");
		SubClassifications subClassificationsIndexTwo = new SubClassifications();
		subClassificationsIndexTwo.setClassifications(classificationsThree);
		subClassificationsIndexTwo.setSubClassifcationName("PEP");
		SubClassifications subClassificationsIndexThree = new SubClassifications();
		subClassificationsIndexThree.setClassifications(classificationsThree);
		subClassificationsIndexThree.setSubClassifcationName("Leaks");
		SubClassifications subClassificationsIndexFour = new SubClassifications();
		subClassificationsIndexFour.setClassifications(classificationsThree);
		subClassificationsIndexFour.setSubClassifcationName("Products");
		SubClassifications subClassificationsIndexFive = new SubClassifications();
		subClassificationsIndexFive.setClassifications(classificationsThree);
		subClassificationsIndexFive.setSubClassifcationName("Services");
		subClassificationsListIndex.add(subClassificationsIndexOne);
		subClassificationsListIndex.add(subClassificationsIndexTwo);
		subClassificationsListIndex.add(subClassificationsIndexThree);
		subClassificationsListIndex.add(subClassificationsIndexFour);
		subClassificationsListIndex.add(subClassificationsIndexFive);
		classificationsThree.setSubClassifications(subClassificationsListIndex);
		Classifications classificationsSavedIndex = classificationsService.save(classificationsThree);
		saveGeneralSource(classificationsSavedGeneral);
		saveNewsSource(classificationsSavedNews);
		saveIndexSource(classificationsSavedIndex);
		return true;
	}

	public boolean saveIndexSource(Classifications classifications) throws IOException {
		List<String[]> allData = null;
		boolean flag = true;
		boolean ownedByZigySaved = true;
		Long jurisdictionId = null;
		File file = ResourceUtils.getFile("classpath:Watchlists Sources in Element - Sheet1 _updated.csv");
		try (FileReader reader = new FileReader(file);) {
			FileReader filereader = new FileReader(file);
			CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build();
			allData = csvReader.readAll();
		}

		String name = "owned by Zygi";
		for (String[] data : allData) {
			if (ownedByZigySaved) {
				Sources sources = null;
				if (data[2].equals(name)) {
					sources = new Sources("http://opensanctions.org", "http://opensanctions.org",
							"http://opensanctions.org", "", "",null);
					ownedByZigySaved = false;
				} else
					sources = new Sources(data[0], data[0], data[0], "", "",null);
				List<Classifications> classificationsList = new ArrayList<Classifications>();
				classificationsList.add(classifications);
				sources.setClassifications(classificationsList);
				List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
				SourceDomain domain = sourceDomainDao.fetchDomain("General");
				if (domain != null) {
					domainsList.add(domain);
					sources.setSourceDomain(domainsList);
				}
				List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
				SourceIndustry industry = sourceIndustryDao.fetchIndustry("All");
				if (industry != null) {
					industryList.add(industry);
					sources.setSourceIndustry(industryList);
				}

				List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
				SourceMedia media = sourceMediaDao.fetchMedia("All");
				if (media != null) {
					mediaList.add(media);
					sources.setSourceMedia(mediaList);
				}

				if (data[0].contains(".uk")) {
					List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
					if (flag) {
						SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.fetchJurisdiction("GB");
						if (jurisdictionFromDB == null) {
							SourceJurisdiction jurisdiction = new SourceJurisdiction();
							jurisdiction.setJurisdictionName("GB");
							jurisdiction.setJurisdictionOriginalName("United Kingdom");
							jurisdiction.setSelected(true);
							SourceJurisdiction jurisdictionSaved = sourceJurisdictionDao.create(jurisdiction);
							jurisdictionList.add(jurisdictionSaved);
							sources.setSourceJurisdiction(jurisdictionList);
							jurisdictionId = jurisdictionSaved.getJurisdictionId();
							flag = false;
						} else {
							jurisdictionList.add(jurisdictionFromDB);
							sources.setSourceJurisdiction(jurisdictionList);
							jurisdictionId = jurisdictionFromDB.getJurisdictionId();
							flag = false;
						}
					} else {
						SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.find(jurisdictionId);
						if (jurisdictionFromDB != null) {
							jurisdictionList.add(jurisdictionFromDB);
							sources.setSourceJurisdiction(jurisdictionList);
							flag = false;
						}
					}
				} else {
					List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
					SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.fetchJurisdiction("All");
					if (jurisdictionFromDB != null) {
						jurisdictionList.add(jurisdictionFromDB);
						sources.setSourceJurisdiction(jurisdictionList);
					}
				}

				Sources sourcesSaved = sourcesService.save(sources);
				SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false, false, false,
						sourcesSaved.getSourceId(), classifications.getClassificationId());
				sourcesHideStatusService.save(hideStatus);
				for (SubClassifications subClassifications : classifications.getSubClassifications()) {
					SourceCredibility credibility = new SourceCredibility();
					credibility.setCredibility(calculateCredibility(data[0]));
					credibility.setSources(sourcesSaved);
					credibility.setSubClassifications(subClassifications);
					sourceCredibilityService.save(credibility);
				}
			} else {
				Sources sources = new Sources(data[0], data[0], data[0], "", "",null);
				List<Classifications> classificationsList = new ArrayList<Classifications>();
				classificationsList.add(classifications);
				sources.setClassifications(classificationsList);
				List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
				SourceDomain domain = sourceDomainDao.fetchDomain("General");
				if (domain != null) {
					domainsList.add(domain);
					sources.setSourceDomain(domainsList);
				}
				List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
				SourceIndustry industry = sourceIndustryDao.fetchIndustry("All");
				if (industry != null) {
					industryList.add(industry);
					sources.setSourceIndustry(industryList);
				}

				List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
				SourceMedia media = sourceMediaDao.fetchMedia("All");
				if (media != null) {
					mediaList.add(media);
					sources.setSourceMedia(mediaList);
				}

				if (data[0].contains(".uk")) {
					List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
					if (flag) {
						SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.fetchJurisdiction("GB");
						if (jurisdictionFromDB == null) {
							SourceJurisdiction jurisdiction = new SourceJurisdiction();
							jurisdiction.setJurisdictionName("GB");
							jurisdiction.setJurisdictionOriginalName("United Kingdom");
							jurisdiction.setSelected(true);
							SourceJurisdiction jurisdictionSaved = sourceJurisdictionDao.create(jurisdiction);
							jurisdictionList.add(jurisdictionSaved);
							sources.setSourceJurisdiction(jurisdictionList);
							jurisdictionId = jurisdictionSaved.getJurisdictionId();
							flag = false;
						} else {
							jurisdictionList.add(jurisdictionFromDB);
							sources.setSourceJurisdiction(jurisdictionList);
							jurisdictionId = jurisdictionFromDB.getJurisdictionId();
							flag = false;
						}
					} else {
						SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.find(jurisdictionId);
						if (jurisdictionFromDB != null) {
							jurisdictionList.add(jurisdictionFromDB);
							sources.setSourceJurisdiction(jurisdictionList);
							flag = false;
						}
					}
				} else {
					List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
					SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.fetchJurisdiction("All");
					if (jurisdictionFromDB != null) {
						jurisdictionList.add(jurisdictionFromDB);
						sources.setSourceJurisdiction(jurisdictionList);
					}
				}

				Sources sourcesSaved = sourcesService.save(sources);
				SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false, false, false,
						sourcesSaved.getSourceId(), classifications.getClassificationId());
				sourcesHideStatusService.save(hideStatus);
				for (SubClassifications subClassifications : classifications.getSubClassifications()) {
					SourceCredibility credibility = new SourceCredibility();
					credibility.setCredibility(calculateCredibility(data[0]));
					credibility.setSources(sourcesSaved);
					credibility.setSubClassifications(subClassifications);
					sourceCredibilityService.save(credibility);
				}

			}
		}

		return true;
	}

	public boolean saveNewsSource(Classifications classifications) throws IOException {
		List<String[]> allData = null;
		File file = ResourceUtils.getFile("classpath:News_Sources_2018_Dec_27.csv");
		try (FileReader reader = new FileReader(file);) {
			FileReader filereader = new FileReader(file);
			CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build();
			allData = csvReader.readAll();
		}
		for (String[] data : allData) {
			String name = getName(data[1]);
			Sources sources = new Sources(name, data[0], name, "", "",null);
			List<Classifications> classificationsList = new ArrayList<Classifications>();
			classificationsList.add(classifications);
			sources.setClassifications(classificationsList);
			List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
			Map<String, String> keyWords = new HashMap<String, String>();
			keyWords.put("world", "General");
			keyWords.put("crime", "Financial Crime");
			keyWords.put("market-sonar", "Market Sonar");
			keyWords.put("cyber-security", "Cyber Security");
			keyWords.put("cyber security", "Cyber Security");
			keyWords.put("compliance", "Compliance");
			if (keyWords.containsKey(data[2])) {
				SourceDomain domain = sourceDomainDao.fetchDomain(keyWords.get(data[2]));
				if (domain != null) {
					domainsList.add(domain);
					sources.setSourceDomain(domainsList);
				}
			} else {
				SourceDomain domain = sourceDomainDao.fetchDomain("General");
				if (domain != null) {
					domainsList.add(domain);
					sources.setSourceDomain(domainsList);
				}
			}
			List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
			SourceIndustry industry = sourceIndustryDao.fetchIndustry("All");
			if (industry != null) {
				industryList.add(industry);
				sources.setSourceIndustry(industryList);
			}

			List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
			SourceMedia media = sourceMediaDao.fetchMedia("All");
			if (media != null) {
				mediaList.add(media);
				sources.setSourceMedia(mediaList);
			}

			List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
			SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.fetchJurisdiction("All");
			if (jurisdictionFromDB != null) {
				jurisdictionList.add(jurisdictionFromDB);
				sources.setSourceJurisdiction(jurisdictionList);
			}

			Sources sourcesSaved = sourcesService.save(sources);
			SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false, false, false,
					sourcesSaved.getSourceId(), classifications.getClassificationId());
			sourcesHideStatusService.save(hideStatus);
			for (SubClassifications subClassifications : classifications.getSubClassifications()) {
				SourceCredibility credibility = new SourceCredibility();
				credibility.setCredibility(calculateCredibility(data[1]));
				credibility.setSources(sourcesSaved);
				credibility.setSubClassifications(subClassifications);
				sourceCredibilityService.save(credibility);
			}
		}
		return true;
	}

	public String getName(String hostName) {
		String domainName = "";
		if (hostName.startsWith("www.")) {
			String domain = hostName.substring(4, hostName.length());
			if (domain.contains(".")) {
				String[] firstDomain = domain.split("\\.", 2);
				domainName = firstDomain[0];
			}
		} else if (hostName.contains(".")) {
			String firstDomain[] = hostName.split("\\.");
			domainName = firstDomain[0];
		} else {
			domainName = hostName;
		}
		return domainName;
	}

	public boolean saveGeneralSource(Classifications classifications) throws Exception {
		String response = "";
		//String[] serverResponse = null;
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File fileJSon = new File(classLoader.getResource("newJurisdictions.json").getFile());
		Object fileJsonData = parser.parse(new FileReader(fileJSon));
		JSONObject newJurisdictionsJson= new JSONObject(fileJsonData.toString());
		JSONArray countryJson= newJurisdictionsJson.getJSONArray("data");
		Map<String, String> map = new HashMap<String, String>();
		for (int k = 0; k < countryJson.length(); k++) {
			JSONObject countryObj = countryJson.getJSONObject(k);
			map.put(countryObj.get("Country Code").toString(), countryObj.get("Countries").toString());
		}
		for (int k = 1; k <= 2; k++) {
			if (k == 1) {
				//serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeout(SOURCE_URL_ID);
				File fileJson = new File(classLoader.getResource("id_index.json").getFile());
				Object fileJsonDataIdIndex = parser.parse(new FileReader(fileJson));
				response=fileJsonDataIdIndex.toString();
			}
			if (k == 2) {
				//serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeout(SOURCE_URL_PROFILES);
				File fileJson = new File(classLoader.getResource("profile_index.json").getFile());
				Object fileJsonDataIdIndex = parser.parse(new FileReader(fileJson));
				response=fileJsonDataIdIndex.toString();
			}
			//if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			//response = serverResponse[1];
			if (!StringUtils.isEmpty(response) ) {
				//if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					try {
						JSONObject json = new JSONObject(response);
						if (json.has("results")) {
							JSONArray resultsArray = json.getJSONArray("results");
							if (resultsArray != null && resultsArray.length() > 0) {
								// JSONObject jsonObject = resultsArray.getJSONObject(0);
								// if (jsonObject.has("data")) {
								JSONArray dataArray = resultsArray;
								if (dataArray != null && dataArray.length() > 0) {
									for (int i = 0; i < dataArray.length(); i++) {
										DataObjectDto dataObjectDto = mapper
												.readValue(dataArray.getJSONObject(i).toString(), DataObjectDto.class);
										
										if(!StringUtils.isBlank(dataObjectDto.getCategory())){
											String dtoCategory = dataObjectDto.getCategory();
											SourceCategory category = new SourceCategory();
											category.setCategoryName(dtoCategory);
											boolean isCategoryExist = sourceCategoryService.checkCategoryExists(dtoCategory);
											if(!isCategoryExist){
												sourceCategoryService.save(category);
											}
										}
										
										String sourceName = dataObjectDto.getSourceName();
										String[] urlSplit = dataObjectDto.getApiBaseUrl().split("source=");
										String url = "";
										int m = 1;
										if (urlSplit.length > 0) {
											for (String urlSave : urlSplit) {
												if (m == 2)
													url = "https://" + urlSave;
												m++;
											}
										} else
											url = dataObjectDto.getSourceName();
										Sources sources = new Sources(sourceName, url, dataObjectDto.getDisplayName(),
												"", "",dataObjectDto.getCategory());
										List<Classifications> classificationsList = new ArrayList<Classifications>();
										classificationsList.add(classifications);
										sources.setClassifications(classificationsList);
										if (!sourcesDao.checkSourceName(sources.getSourceName(), false,
												sources.getClassifications().get(0).getClassificationId())) {
											List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
											SourceDomain domain = sourceDomainDao.fetchDomain("General");
											if (domain != null) {
												domainsList.add(domain);
												sources.setSourceDomain(domainsList);
											}

											List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
											SourceIndustry industry = sourceIndustryDao.fetchIndustry("All");
											if (industry != null) {
												industryList.add(industry);
												sources.setSourceIndustry(industryList);
											}

											List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
											SourceMedia media = sourceMediaDao.fetchMedia("All");
											if (media != null) {
												mediaList.add(media);
												sources.setSourceMedia(mediaList);
											}

											List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
											List<String> jurisdictionsStringList = dataObjectDto.getJurisdictions();
											for (String jurisdictionString : jurisdictionsStringList) {
												SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao
														.fetchJurisdiction(jurisdictionString);
												if (jurisdictionFromDB != null) {
													jurisdictionList.add(jurisdictionFromDB);
												} else {
													SourceJurisdiction jurisdictionNew = new SourceJurisdiction();
													if (map.containsKey(jurisdictionString)) {
														jurisdictionNew.setSelected(false);
													jurisdictionNew.setJurisdictionName(jurisdictionString);
														jurisdictionNew.setJurisdictionOriginalName(
																map.get(jurisdictionString));
													}else {
														//jurisdictionNew.setJurisdictionOriginalName("");
														/**If the jurisdictionCode not presented in the newJuridctionList.json we are not adding into the
														 * Jurisdiction and LM_LIST_ITEM tables. 
														 */
														continue;
													}
													SourceJurisdiction jurisdictionNewSaved = sourceJurisdictionDao
															.create(jurisdictionNew);
													jurisdictionList.add(jurisdictionNewSaved);
												}
											}
											sources.setSourceJurisdiction(jurisdictionList);
											Sources sourcesSaved = sourcesService.save(sources);
											SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false,
													false, false, sourcesSaved.getSourceId(),
													classifications.getClassificationId());
											hideStatus.setHideMedia(false);
											sourcesHideStatusService.save(hideStatus);
											for (SubClassifications subClassifications : classifications
													.getSubClassifications()) {
												SourceCredibility credibility = new SourceCredibility();
												credibility.setCredibility(calculateCredibility(sourceName));
												if (sourceName.equals("BST") && subClassifications
														.getSubClassifcationName().equals("Corporate Structure"))
													credibility.setCredibility(CredibilityEnums.MEDIUM);
												if (sourceName.equals("companyhouse.co.uk") && subClassifications
														.getSubClassifcationName().equals("Corporate Structure"))
													credibility.setCredibility(CredibilityEnums.HIGH);
												credibility.setSources(sourcesSaved);
												credibility.setSubClassifications(subClassifications);
												sourceCredibilityService.save(credibility);
											}
										} // if condition close
									} // for close
								}
								// }
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				/*} else {
					throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
				}*/
			} else {
				throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
			}

		}

		return true;
	}

	CredibilityEnums calculateCredibility(String sourceName) {
		if (sourceName.contains(".gov"))
			return CredibilityEnums.HIGH;
		else if (sourceName.contains(".edu") || sourceName.contains(".ac") || sourceName.contains(".org"))
			return CredibilityEnums.MEDIUM;
		else if (sourceName.contains(".com.co"))
			return CredibilityEnums.LOW;
		else
			return CredibilityEnums.NONE;
	}

	@Override
	@Transactional("transactionManager")
	public List<SourcesDto> getSources(Integer pageNumber, Integer recordsPerPage, Boolean visible,
			Long classificationId, String orderBy, String orderIn, List<SourceFilterDto> sourceFilterDto,
			Long subClassifcationId, Boolean isAllSourcesRequired) {

		List<Sources> sourcesList = new ArrayList<Sources>();
		List<SourcesDto> sourcesDtoList = new ArrayList<SourcesDto>();
		if (isAllSourcesRequired) {// returning all sources without classificationId
			sourcesDtoList = sourcesDao.getAllSources();
		}else if((pageNumber!= null && recordsPerPage != null) && (pageNumber== 0 && recordsPerPage == 0)) {//When not required Pagination for Specific ClassificationId
			sourcesList = sourcesDao.getSources(
					null,null, visible, classificationId,
					orderBy, orderIn, sourceFilterDto, subClassifcationId);
			sourcesDtoList = sourcesList.stream()
					.map((sourcesDto) -> new EntityConverter<Sources, SourcesDto>(Sources.class, SourcesDto.class)
							.toT2(sourcesDto, new SourcesDto()))
					.collect(Collectors.toList());
		}else{
			sourcesList = sourcesDao.getSources(
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
							recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, visible, classificationId,
									orderBy, orderIn, sourceFilterDto, subClassifcationId);
			sourcesDtoList = sourcesList.stream()
					.map((sourcesDto) -> new EntityConverter<Sources, SourcesDto>(Sources.class, SourcesDto.class)
							.toT2(sourcesDto, new SourcesDto()))
					.collect(Collectors.toList());
		}

		// adding status
		if(!isAllSourcesRequired) {
			List<SourcesHideStatus> hideStatusOverAllList = sourcesHideStatusDao.findAll();
			for (SourcesDto sourcesSingle : sourcesDtoList) {
				List<ClassificationsDto> classificationsList = sourcesSingle.getClassifications();
				for (ClassificationsDto classification : classificationsList) {
					Optional<SourcesHideStatus> matchingObject = hideStatusOverAllList.stream()
							.filter(hideStatus -> hideStatus.getSourceId().equals(sourcesSingle.getSourceId())
									&& hideStatus.getClassificationId().equals(classification.getClassificationId()))
							.findFirst();
					SourcesHideStatus hideStatus = matchingObject.orElse(null);
					if (hideStatus != null) {
						SourcesHideStatusDto hideStatusDto = new EntityConverter<SourcesHideStatus, SourcesHideStatusDto>(
								SourcesHideStatus.class, SourcesHideStatusDto.class).toT2(hideStatus,
										new SourcesHideStatusDto());
						classification.setHideStatusDto(hideStatusDto);
					}
				}
			}
			try {
				// for data attributes and sub classification credibility
				List<SourceCredibility> credibilitiesList = sourceCredibilityDao.findAll();
				for (SourcesDto sourceDtoToset : sourcesDtoList) {
					for (ClassificationsDto sourceClassificationDto : sourceDtoToset.getClassifications()) {
						for (SubClassificationsDto sourceSubClassificationDto : sourceClassificationDto
								.getSubClassifications()) {
							Optional<SourceCredibility> matchingObject = credibilitiesList.stream()
									.filter(sourceCredibilityOperator -> sourceCredibilityOperator.getSources()
											.getSourceId().equals(sourceDtoToset.getSourceId())
											&& sourceCredibilityOperator.getSubClassifications()
													.getSubClassificationId()
													.equals(sourceSubClassificationDto.getSubClassificationId())
											&& sourceCredibilityOperator.getDataAttributes() == null)
									.findFirst();
							SourceCredibility credibilityFind = matchingObject.orElse(null);
							if (credibilityFind != null) {
								sourceSubClassificationDto
										.setSubClassificationCredibility(credibilityFind.getCredibility());
							}
							for (DataAttributesDto attributesDto : sourceSubClassificationDto.getDataAttributes()) {
								SourceCredibility credibilityForAttributes = null;

								for (SourceCredibility sourceCredibility : credibilitiesList) {
									if (sourceCredibility.getDataAttributes() != null) {
										if (sourceCredibility.getSources().getSourceId()
												.equals(sourceDtoToset.getSourceId())
												&& sourceCredibility.getSubClassifications().getSubClassificationId()
														.equals(sourceSubClassificationDto.getSubClassificationId())
												&& sourceCredibility.getDataAttributes().getAttributeId()
														.equals(attributesDto.getAttributeId())) {
											credibilityForAttributes = sourceCredibility;
										}
									}

								}
								if (credibilityForAttributes != null) {
									attributesDto.setAttributeId(
											credibilityForAttributes.getDataAttributes().getAttributeId());
									attributesDto.setCredibilityValue(credibilityForAttributes.getCredibility());
									attributesDto.setAttributeSaved(true);
								} else {
									attributesDto.setCredibilityValue(credibilityFind.getCredibility());
									attributesDto.setAttributeId(attributesDto.getAttributeId());
								}
							}
						}
					}

				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return sourcesDtoList;
	}

	@Override
	@Transactional("transactionManager")
	public long getSourcesCount(Integer pageNumber, Integer recordsPerPage, Boolean visible, Long classificationId,
			String orderBy, String orderIn, List<SourceFilterDto> sourceFilterDto, Long subClassifcationId) {
		List<Sources> sourcesList = sourcesDao.getSources(null, null, visible, classificationId, orderBy, orderIn,
				sourceFilterDto, subClassifcationId);
		if (sourcesList != null && !sourcesList.isEmpty())
			return sourcesList.size();
		return 0;
	}

	@Override
	@Transactional("transactionManager")
	public boolean updateSource(SourcesDto sourceDto, Long currentUserId) throws Exception {
		try {
			List<LoggingEvent> loggingEventList = new ArrayList<LoggingEvent>();
			Sources sources = sourcesDao.find(sourceDto.getSourceId());
			if (sources != null) {
				if (sourceDto.getClassifications() != null) {
					for (ClassificationsDto classificationsDto : sourceDto.getClassifications()) {
						if (classificationsDto.getClassifcationName().equalsIgnoreCase("general")
								|| classificationsDto.getClassifcationName().equalsIgnoreCase("news")) {
							if (!sources.getSourceName().equals(sourceDto.getSourceName())) {
								if (sourcesDao.checkSourceName(sourceDto.getSourceName(), false,
										sourceDto.getClassifications().get(0).getClassificationId()))
									throw new InsufficientDataException("source name already exists");
							}
						}
						if (classificationsDto.getClassifcationName().equalsIgnoreCase("index")) {
							if (!sources.getSourceName().equals(sourceDto.getSourceName())) {
								if (sourcesDao.checkSourceName(sourceDto.getSourceName(), true,
										sourceDto.getClassifications().get(0).getClassificationId()))
									throw new InsufficientDataException("index source name already exists");
							}
						}
						if (classificationsDto.getHideStatusDto() != null) {
							SourcesHideStatusDto hideDto = classificationsDto.getHideStatusDto();
							SourcesHideStatus hideStatus = sourcesHideStatusService
									.find(classificationsDto.getHideStatusDto().getSourcesHideStatusId());
							
							if(hideDto.getHideDomain() != null){
								if(hideStatus.getHideDomain() == null){
									hideStatus.setHideDomain(false);
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideDomain().toString(), hideDto.getHideDomain().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_DOMAIN_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideDomain(hideDto.getHideDomain());
								}
								else if(!hideStatus.getHideDomain() == hideDto.getHideDomain()) {
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideDomain().toString(), hideDto.getHideDomain().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_DOMAIN_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideDomain(hideDto.getHideDomain());
								}
							}
							if(hideDto.getHideIndustry() != null){
								if(hideStatus.getHideIndustry() == null){
									hideStatus.setHideIndustry(false);
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideIndustry().toString(), hideDto.getHideIndustry().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_INDUSTRY_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideIndustry(hideDto.getHideIndustry());
								}else if(!hideStatus.getHideIndustry() == hideDto.getHideIndustry()) {
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideIndustry().toString(), hideDto.getHideIndustry().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_INDUSTRY_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideIndustry(hideDto.getHideIndustry());
								}
							}
							if(hideDto.getHideLink() != null){
								if(hideStatus.getHideLink() == null){
									hideStatus.setHideLink(false);
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideLink().toString(), hideDto.getHideLink().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_LINK_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideLink(hideDto.getHideLink());
								}else if (!hideStatus.getHideLink() == hideDto.getHideLink()) {
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideLink().toString(), hideDto.getHideLink().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_LINK_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideLink(hideDto.getHideLink());
								}
							}
							
							if(hideDto.getHideMedia() != null){
								if(hideStatus.getHideMedia() == null){
									hideStatus.setHideMedia(false);
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideMedia().toString(), hideDto.getHideMedia().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_MEDIA_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideMedia(hideDto.getHideMedia());
								}else if (!hideStatus.getHideMedia() == hideDto.getHideMedia()) {
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideMedia().toString(), hideDto.getHideMedia().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_MEDIA_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideMedia(hideDto.getHideMedia());
								}
							}
							if(hideDto.getHideJurisdiction() != null){
								if(hideStatus.getHideJurisdiction() == null){
									hideStatus.setHideJurisdiction(false);
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideJurisdiction().toString(),
											hideDto.getHideJurisdiction().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_JURISDICTION_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideJurisdiction(hideDto.getHideJurisdiction());
								}else if (!hideStatus.getHideJurisdiction() == hideDto.getHideJurisdiction()) {
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getHideJurisdiction().toString(),
											hideDto.getHideJurisdiction().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_JURISDICTION_HIDE, currentUserId,
											new Date()));
									hideStatus.setHideJurisdiction(hideDto.getHideJurisdiction());
								}
							}
							
							if(hideDto.getVisible() != null){
								if(hideStatus.getVisible() == null){
									hideStatus.setVisible(false);
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getVisible().toString(), hideDto.getVisible().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_VISIBILITY, currentUserId,
											new Date()));
									hideStatus.setVisible(hideDto.getVisible());
								}else if (!hideStatus.getVisible() == hideDto.getVisible()) {
									loggingEventList.add(new LoggingEvent(hideStatus.getSourcesHideStatusId(),
											hideStatus.getVisible().toString(), hideDto.getVisible().toString(),
											LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_VISIBILITY, currentUserId,
											new Date()));
									hideStatus.setVisible(hideDto.getVisible());
								}
							}
							
						}
					}
				}
				if(sourceDto.getSourceName() != null){
					if(sources.getSourceName() == null){
						sources.setSourceName("");
						loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceName(),
								sourceDto.getSourceName(), LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_NAME_CHANGE,
								currentUserId, new Date()));
						sources.setSourceName(sourceDto.getSourceName());
					}else if (!sources.getSourceName().equals(sourceDto.getSourceName())) {
						loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceName(),
								sourceDto.getSourceName(), LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_NAME_CHANGE,
								currentUserId, new Date()));
						sources.setSourceName(sourceDto.getSourceName());
					}
				}
				
				if(sourceDto.getSourceDisplayName() != null){
					if(sources.getSourceDisplayName() == null){
						sources.setSourceDisplayName("");
						loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceDisplayName(),
								sourceDto.getSourceDisplayName(),
								LoggingEventType.SOURCE_CLASSIFICATION_DISPLAY_NAME_CHANGE, currentUserId, new Date()));
						sources.setSourceDisplayName(sourceDto.getSourceDisplayName());
					}else if (!sources.getSourceDisplayName().equals(sourceDto.getSourceDisplayName())) {
						loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceDisplayName(),
								sourceDto.getSourceDisplayName(),
								LoggingEventType.SOURCE_CLASSIFICATION_DISPLAY_NAME_CHANGE, currentUserId, new Date()));
						sources.setSourceDisplayName(sourceDto.getSourceDisplayName());
					}
				}
				
				if(sourceDto.getSourceUrl() != null){
					if(sources.getSourceUrl() == null){
						sources.setSourceUrl("");
						loggingEventList.add(
								new LoggingEvent(sources.getSourceId(), sources.getSourceUrl(), sourceDto.getSourceUrl(),
										LoggingEventType.SOURCE_CLASSIFICATION_LINK_CHANGE, currentUserId, new Date()));
						sources.setSourceUrl(sourceDto.getSourceUrl());
					}
					else if(!sources.getSourceUrl().equals(sourceDto.getSourceUrl())){
						loggingEventList.add(
								new LoggingEvent(sources.getSourceId(), sources.getSourceUrl(), sourceDto.getSourceUrl(),
										LoggingEventType.SOURCE_CLASSIFICATION_LINK_CHANGE, currentUserId, new Date()));
						sources.setSourceUrl(sourceDto.getSourceUrl());
					}
					
				}
				
				// domain
				if (sources.getSourceDomain() != null && !sourceDto.getSourceDomain().isEmpty()) {
					List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
					List<Long> domainRemovedList = new ArrayList<Long>();
					List<Long> domainAddedList = new ArrayList<Long>();
					for (SourceDomain sourceDomain : sources.getSourceDomain()) {
						if (!sourceDto.getSourceDomain().stream()
								.anyMatch(ti -> ti.getDomainId().equals(sourceDomain.getDomainId()))) {
							domainRemovedList.add(sourceDomain.getDomainId());
							loggingEventList.add(new LoggingEvent(sourceDomain.getDomainId(),
									sourceDto.getSourceDisplayName(), sourceDomain.getDomainName(),
									LoggingEventType.DELETE_SOURCE_DOMAIN, currentUserId, new Date()));
						}
					}
					for (SourceDomainDto sourceDomainDto : sourceDto.getSourceDomain()) {
						if (!sources.getSourceDomain().stream()
								.anyMatch(ti -> ti.getDomainId().equals(sourceDomainDto.getDomainId()))) {
							domainAddedList.add(sourceDomainDto.getDomainId());
							loggingEventList.add(new LoggingEvent(sourceDomainDto.getDomainId(),
									sourceDto.getSourceDisplayName(), sourceDomainDto.getDomainName(),
									LoggingEventType.SOURCE_DOMAIN_NEW_ASSIGNED, currentUserId, new Date()));
						}
					}
					for (SourceDomainDto domainDto : sourceDto.getSourceDomain()) {
						// creating new category
						SourceDomain domain = sourceDomainDao.find(domainDto.getDomainId());
						domainsList.add(domain);
					}
					sources.setSourceDomain(domainsList);
				} // domain close
					// Jurisdiction
				if (sourceDto.getSourceJurisdiction() != null && !sourceDto.getSourceJurisdiction().isEmpty()) {
					List<SourceJurisdiction> jurisdictionsAll = sourceJurisdictionDao.fetchJurisdictionExceptAll();
					if (sourceDto.getSourceJurisdiction().size() == 1
							&& sourceDto.getSourceJurisdiction().get(0).getJurisdictionName().equalsIgnoreCase("All")) {
						for (SourceJurisdiction sourceJurisdiction : jurisdictionsAll) {
							if (!sources.getSourceJurisdiction().stream().anyMatch(
									ti -> ti.getJurisdictionId().equals(sourceJurisdiction.getJurisdictionId()))) {
								loggingEventList.add(new LoggingEvent(sourceJurisdiction.getJurisdictionId(),
										sourceDto.getSourceDisplayName(), sourceJurisdiction.getJurisdictionName(),
										LoggingEventType.SOURCE_JURISDICTION_NEW_ASSIGNED, currentUserId, new Date()));
							}
						}
						List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
						for (SourceJurisdictionDto jurisdictionDto : sourceDto.getSourceJurisdiction()) {
							SourceJurisdiction jurisdiction = sourceJurisdictionDao
									.find(jurisdictionDto.getJurisdictionId());
							jurisdictionList.add(jurisdiction);
						}
						sources.setSourceJurisdiction(jurisdictionList);
					} else {
						List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
						List<Long> jurisdictionRemovedList = new ArrayList<Long>();
						List<Long> jurisdictionAddedList = new ArrayList<Long>();

						if (sources.getSourceJurisdiction().size() == 1 && sources.getSourceJurisdiction().get(0)
								.getJurisdictionName().equalsIgnoreCase("All")) {
							for (SourceJurisdiction sourceJurisdiction : jurisdictionsAll) {
								if (!sourceDto.getSourceJurisdiction().stream().anyMatch(
										ti -> ti.getJurisdictionId().equals(sourceJurisdiction.getJurisdictionId()))) {
									jurisdictionRemovedList.add(sourceJurisdiction.getJurisdictionId());
									loggingEventList.add(new LoggingEvent(sourceJurisdiction.getJurisdictionId(),
											sourceDto.getSourceDisplayName(), sourceJurisdiction.getJurisdictionName(),
											LoggingEventType.DELETE_SOURCE_JURISDICTION, currentUserId, new Date()));
								}
							}
						} else {
							for (SourceJurisdiction sourceJurisdiction : sources.getSourceJurisdiction()) {
								if (!sourceDto.getSourceJurisdiction().stream().anyMatch(
										ti -> ti.getJurisdictionId().equals(sourceJurisdiction.getJurisdictionId()))) {
									jurisdictionRemovedList.add(sourceJurisdiction.getJurisdictionId());
									loggingEventList.add(new LoggingEvent(sourceJurisdiction.getJurisdictionId(),
											sourceDto.getSourceDisplayName(), sourceJurisdiction.getJurisdictionName(),
											LoggingEventType.DELETE_SOURCE_JURISDICTION, currentUserId, new Date()));
								}
							}
							for (SourceJurisdictionDto sourceJurisdictionDto : sourceDto.getSourceJurisdiction()) {
								if (!sources.getSourceJurisdiction().stream().anyMatch(ti -> ti.getJurisdictionId()
										.equals(sourceJurisdictionDto.getJurisdictionId()))) {
									jurisdictionAddedList.add(sourceJurisdictionDto.getJurisdictionId());
									loggingEventList.add(new LoggingEvent(sourceJurisdictionDto.getJurisdictionId(),
											sourceDto.getSourceDisplayName(),
											sourceJurisdictionDto.getJurisdictionName(),
											LoggingEventType.SOURCE_JURISDICTION_NEW_ASSIGNED, currentUserId,
											new Date()));
								}
							}
						}
						for (SourceJurisdictionDto jurisdictionDto : sourceDto.getSourceJurisdiction()) {
							// creating new category and respective source
							SourceJurisdiction jurisdiction = sourceJurisdictionDao
									.find(jurisdictionDto.getJurisdictionId());
							jurisdictionList.add(jurisdiction);
						}
						sources.setSourceJurisdiction(jurisdictionList);
					}
				} // jurisdiction close
					// Industry
				if (sourceDto.getSourceIndustry() != null && !sourceDto.getSourceIndustry().isEmpty()) {
					List<SourceIndustry> industryAll = sourceIndustryDao.fetchIndustryListExceptAll();
					if (sourceDto.getSourceIndustry().size() == 1
							&& sourceDto.getSourceIndustry().get(0).getIndustryName().equals("All")) {
						for (SourceIndustry sourceIndustry : industryAll) {
							if (!sources.getSourceIndustry().stream()
									.anyMatch(ti -> ti.getIndustryId().equals(sourceIndustry.getIndustryId()))) {
								loggingEventList.add(new LoggingEvent(sourceIndustry.getIndustryId(),
										sourceDto.getSourceDisplayName(), sourceIndustry.getIndustryName(),
										LoggingEventType.SOURCE_INDUSTRY_NEW_ASSIGNED, currentUserId, new Date()));
							}
						}
						List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
						for (SourceIndustryDto industryDto : sourceDto.getSourceIndustry()) {
							SourceIndustry industry = sourceIndustryDao.find(industryDto.getIndustryId());
							industryList.add(industry);
						}
						sources.setSourceIndustry(industryList);
					} else {
						List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
						List<Long> industryRemovedList = new ArrayList<Long>();
						List<Long> industryAddedList = new ArrayList<Long>();
						if (sources.getSourceIndustry().size() == 1
								&& sources.getSourceIndustry().get(0).getIndustryName().equalsIgnoreCase("All")) {
							for (SourceIndustry sourceIndustry : industryAll) {
								if (!sourceDto.getSourceIndustry().stream()
										.anyMatch(ti -> ti.getIndustryId().equals(sourceIndustry.getIndustryId()))) {
									industryRemovedList.add(sourceIndustry.getIndustryId());
									loggingEventList.add(new LoggingEvent(sourceIndustry.getIndustryId(),
											sourceDto.getSourceDisplayName(), sourceIndustry.getIndustryName(),
											LoggingEventType.DELETE_SOURCE_INDUSTRY, currentUserId, new Date()));
								}
							}
						} else {
							for (SourceIndustry sourceIndustry : sources.getSourceIndustry()) {
								if (!sourceDto.getSourceIndustry().stream()
										.anyMatch(ti -> ti.getIndustryId().equals(sourceIndustry.getIndustryId()))) {
									industryRemovedList.add(sourceIndustry.getIndustryId());
									loggingEventList.add(new LoggingEvent(sourceIndustry.getIndustryId(),
											sourceDto.getSourceDisplayName(), sourceIndustry.getIndustryName(),
											LoggingEventType.DELETE_SOURCE_INDUSTRY, currentUserId, new Date()));
								}
							}
							for (SourceIndustryDto sourceIndustryDto : sourceDto.getSourceIndustry()) {
								if (!sources.getSourceIndustry().stream()
										.anyMatch(ti -> ti.getIndustryId().equals(sourceIndustryDto.getIndustryId()))) {
									industryAddedList.add(sourceIndustryDto.getIndustryId());
									loggingEventList.add(new LoggingEvent(sourceIndustryDto.getIndustryId(),
											sourceDto.getSourceDisplayName(), sourceIndustryDto.getIndustryName(),
											LoggingEventType.SOURCE_INDUSTRY_NEW_ASSIGNED, currentUserId, new Date()));
								}
							}
						}
						for (SourceIndustryDto industryDto : sourceDto.getSourceIndustry()) {
							// creating new category and respective source
							SourceIndustry industry = sourceIndustryDao.find(industryDto.getIndustryId());
							industryList.add(industry);
						}
						sources.setSourceIndustry(industryList);
					}
				} // industry close
					// media
				if (sourceDto.getSourceMedia() != null && !sourceDto.getSourceMedia().isEmpty()) {
					List<SourceMedia> sourceMediaList = sourceMediaDao.fetchMediaListExceptAll();
					if (sourceDto.getSourceMedia().size() == 1
							&& sourceDto.getSourceMedia().get(0).getMediaName().equals("All")) {
						for (SourceMedia sourceMedia : sourceMediaList) {
							if (!sources.getSourceMedia().stream()
									.anyMatch(ti -> ti.getMediaId().equals(sourceMedia.getMediaId()))) {
								loggingEventList.add(new LoggingEvent(sourceMedia.getMediaId(),
										sourceDto.getSourceDisplayName(), sourceMedia.getMediaName(),
										LoggingEventType.SOURCE_MEDIA_NEW_ASSIGNED, currentUserId, new Date()));
							}
						}
						List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
						for (SourceMediaDto mediaDto : sourceDto.getSourceMedia()) {
							SourceMedia media = sourceMediaDao.find(mediaDto.getMediaId());
							mediaList.add(media);
						}
						sources.setSourceMedia(mediaList);
					} else {
						List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
						List<Long> sourceMediaRemoveList = new ArrayList<Long>();
						List<Long> sourceMediaAddedList = new ArrayList<Long>();
						if (sources.getSourceMedia().size() == 1
								&& sources.getSourceMedia().get(0).getMediaName().equalsIgnoreCase("All")) {
							for (SourceMedia sourceMedia : sourceMediaList) {
								if (!sourceDto.getSourceMedia().stream()
										.anyMatch(ti -> ti.getMediaId().equals(sourceMedia.getMediaId()))) {
									sourceMediaRemoveList.add(sourceMedia.getMediaId());
									loggingEventList.add(new LoggingEvent(sourceMedia.getMediaId(),
											sourceDto.getSourceDisplayName(), sourceMedia.getMediaName(),
											LoggingEventType.DELETE_SOURCE_MEDIA, currentUserId, new Date()));
								}
							}
						} else {
							for (SourceMedia sourceMedia : sources.getSourceMedia()) {
								if (!sourceDto.getSourceMedia().stream()
										.anyMatch(ti -> ti.getMediaId().equals(sourceMedia.getMediaId()))) {
									sourceMediaRemoveList.add(sourceMedia.getMediaId());
									loggingEventList.add(new LoggingEvent(sourceMedia.getMediaId(),
											sourceDto.getSourceDisplayName(), sourceMedia.getMediaName(),
											LoggingEventType.DELETE_SOURCE_MEDIA, currentUserId, new Date()));
								}
							}
							for (SourceMediaDto sourceMediaDto : sourceDto.getSourceMedia()) {
								if (!sources.getSourceMedia().stream()
										.anyMatch(ti -> ti.getMediaId().equals(sourceMediaDto.getMediaId()))) {
									sourceMediaAddedList.add(sourceMediaDto.getMediaId());
									loggingEventList.add(new LoggingEvent(sourceMediaDto.getMediaId(),
											sourceDto.getSourceDisplayName(), sourceMediaDto.getMediaName(),
											LoggingEventType.SOURCE_MEDIA_NEW_ASSIGNED, currentUserId, new Date()));
								}
							}
						}
						for (SourceMediaDto mediaDto : sourceDto.getSourceMedia()) {
							SourceMedia media = sourceMediaDao.find(mediaDto.getMediaId());
							mediaList.add(media);
						}
						sources.setSourceMedia(mediaList);
					}
				} // media close
				if (sourceDto.isDeleteMedia() && sourceDto.getSourceMedia().isEmpty()) {
					List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
					sources.setSourceMedia(mediaList);
				}
				// update category
				if(sourceDto.getCategory() != null && !sourceDto.getCategory().isEmpty()){
					sources.setCategory(sourceDto.getCategory());
				}
				// update data attributes
				if (sourceDto.getClassifications() != null) {
					List<ClassificationsDto> classificationsListSave = sourceDto.getClassifications();
					for (ClassificationsDto classificationsDtoSave : classificationsListSave) {
						List<SubClassificationsDto> subClassificationsDtoListSave = classificationsDtoSave
								.getSubClassifications();
						for (SubClassificationsDto subClassificationsDtoSave : subClassificationsDtoListSave) {
							SourceCredibility credibilityFind = sourceCredibilityDao.findSubClassificationCredibility(
									sources.getSourceId(), subClassificationsDtoSave.getSubClassificationId(), null);
							if (credibilityFind != null) {
								if (!credibilityFind.getCredibility()
										.equals(subClassificationsDtoSave.getSubClassificationCredibility())) {
									loggingEventList.add(new LoggingEvent(credibilityFind.getSourceCredibilityId(),
											credibilityFind.getCredibility(),
											subClassificationsDtoSave.getSubClassificationCredibility(),
											LoggingEventType.SUB_CLASSIFICATION_CREDIBILITY_CHANGE, currentUserId,
											new Date()));
									credibilityFind.setCredibility(
											subClassificationsDtoSave.getSubClassificationCredibility());
									sourceCredibilityDao.saveOrUpdate(credibilityFind);
								}
							}
							// updating data attributes
							List<DataAttributesDto> attributesDtoList = subClassificationsDtoSave.getDataAttributes();
							for (DataAttributesDto dataAttributesDto : attributesDtoList) {
								if (dataAttributesDto.isAttributeSaved()) {
									SourceCredibility credibilityAttributeUpdate = sourceCredibilityDao
											.findSubClassificationCredibility(sources.getSourceId(),
													subClassificationsDtoSave.getSubClassificationId(),
													dataAttributesDto.getAttributeId());
									if (credibilityAttributeUpdate != null) {
										if (!credibilityAttributeUpdate.getCredibility()
												.equals(dataAttributesDto.getCredibilityValue())) {
											loggingEventList.add(new LoggingEvent(
													credibilityAttributeUpdate.getSourceCredibilityId(),
													credibilityAttributeUpdate.getCredibility(),
													dataAttributesDto.getCredibilityValue(),
													LoggingEventType.ATTRIBUTE_CREDIBILITY_CHANGE, currentUserId,
													new Date()));
											credibilityAttributeUpdate
													.setCredibility(dataAttributesDto.getCredibilityValue());
											sourceCredibilityDao.saveOrUpdate(credibilityAttributeUpdate);
										}
									}
								} else {
									if (!subClassificationsDtoSave.getSubClassificationCredibility()
											.equals(dataAttributesDto.getCredibilityValue())) {
										SourceCredibility credibilityToSave = new SourceCredibility(sources,
												subClassificationService
														.find(subClassificationsDtoSave.getSubClassificationId()),
												dataAttributesService.find(dataAttributesDto.getAttributeId()),
												dataAttributesDto.getCredibilityValue());
										SourceCredibility credibilitySaved = sourceCredibilityDao
												.create(credibilityToSave);
										loggingEventList.add(new LoggingEvent(credibilitySaved.getSourceCredibilityId(),
												subClassificationsDtoSave.getSubClassificationCredibility(),
												credibilitySaved.getCredibility(),
												LoggingEventType.ATTRIBUTE_CREDIBILITY_ADD_NEW, currentUserId,
												new Date()));
									}
								}
							}
						}
					}
				}
				if (!loggingEventList.isEmpty())
					eventPublisher.publishEvent(new LoggingEvent(sources.getSourceId(), loggingEventList,
							LoggingEventType.UPDATE_SOURCE, currentUserId, new Date()));
				sourcesDao.saveOrUpdate(sources);
				return true;
			} else {
				throw new NoDataFoundException("Source Does not exist");
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new Exception(ElementConstants.UPDATE_FAILED);
		}
	}

	@Override
	@Transactional("transactionManager")
	public boolean saveSource(SourcesDto sourcesDto, Long userId) {
		Classifications classificationsSaved = new Classifications();
		List<Classifications> classificationsList = new ArrayList<Classifications>();
		if (sourcesDto.getClassifications() != null && !sourcesDto.getClassifications().isEmpty()) {
			if (sourcesDto.getClassifications().get(0).getClassifcationName().equalsIgnoreCase("index")) {
				if (sourcesDao.checkSourceName(sourcesDto.getSourceName(), true,
						sourcesDto.getClassifications().get(0).getClassificationId()))
					throw new InsufficientDataException("index source name already exists");
			}

			if (sourcesDto.getClassifications().get(0).getClassifcationName().equalsIgnoreCase("general")
					|| sourcesDto.getClassifications().get(0).getClassifcationName().equalsIgnoreCase("news"))

			{
				if (sourcesDao.checkSourceName(sourcesDto.getSourceName(), false,
						sourcesDto.getClassifications().get(0).getClassificationId()))
					throw new InsufficientDataException("source name already exists");
			}

			classificationsSaved = classificationsService
					.find(sourcesDto.getClassifications().get(0).getClassificationId());

		}
		Sources sources = new EntityConverter<SourcesDto, Sources>(SourcesDto.class, Sources.class).toT2(sourcesDto,
				new Sources());
		classificationsList.add(classificationsSaved);
		sources.setClassifications(classificationsList);
		sources.setSourceDisplayName(sourcesDto.getSourceName());
		Sources sourcesSaved = sourcesDao.create(sources);

		if (sourcesDto.getClassifications() != null) {
			for (ClassificationsDto classifications : sourcesDto.getClassifications()) {
				for (SubClassificationsDto subClassificationsDto : classifications.getSubClassifications()) {
					SourceCredibility credibility = new SourceCredibility(sourcesSaved,
							subClassificationService.find(subClassificationsDto.getSubClassificationId()), null,
							calculateCredibility(sourcesDto.getSourceName()));
					sourceCredibilityService.save(credibility);
				}
			}
			for (ClassificationsDto classifications : sourcesDto.getClassifications()) {
				SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false, false, false,
						sourcesSaved.getSourceId(), classifications.getClassificationId());
				hideStatus.setHideMedia(false);
				sourcesHideStatusService.save(hideStatus);
			}
		}
		eventPublisher.publishEvent(new LoggingEvent(sourcesSaved.getSourceId(), sources.getSourceName(),
				LoggingEventType.SOURCE_CREATED, userId, new Date()));
		return true;
	}

	@Override
	@Transactional("transactionManager")
	public boolean checkSourceExistWithSourceNameAndEntityId(String identifier, String sourceName) {
		return sourcesDao.checkSourceExistWithSourceNameAndEntityId(identifier, sourceName);
	}
	
	@Override
	@Transactional("transactionManager")
	public boolean checkSourceExist(String identifier, String url) {
		return sourcesDao.checkSourceExist(identifier, url);
	}

	@Override
	@Transactional("transactionManager")
	public void saveCompanyWebsiteSource(String identifiervalue, String url, String companyHouseObjecEntityName,
			String sourceType, Long userId) {
		List<Classifications> classificationsList = new ArrayList<Classifications>();
		Classifications classificationsSaved = classificationsService.fetchClassification("GENERAL");
		classificationsList.add(classificationsSaved);
		Sources sources = new Sources();
		sources.setEntityId(identifiervalue);
		sources.setSourceUrl(url);
		sources.setSourceType(sourceType);
		sources.setClassifications(classificationsList);
		sources.setSourceDisplayName(companyHouseObjecEntityName);
		sources.setSourceName(companyHouseObjecEntityName);
		//checkSourceLink
		if (!sourcesDao.checkSourceName(sources.getSourceName(), false,
				sources.getClassifications().get(0).getClassificationId())) {
			List<SourceDomain> domainsList = new ArrayList<SourceDomain>();
			SourceDomain domain = sourceDomainDao.fetchDomain("General");
			if (domain != null) {
				domainsList.add(domain);
				sources.setSourceDomain(domainsList);
			}
			List<SourceIndustry> industryList = new ArrayList<SourceIndustry>();
			SourceIndustry industry = sourceIndustryDao.fetchIndustry("All");
			if (industry != null) {
				industryList.add(industry);
				sources.setSourceIndustry(industryList);
			}
			List<SourceMedia> mediaList = new ArrayList<SourceMedia>();
			SourceMedia media = sourceMediaDao.fetchMedia("All");
			if (media != null) {
				mediaList.add(media);
				sources.setSourceMedia(mediaList);
			}

			List<SourceJurisdiction> jurisdictionList = new ArrayList<SourceJurisdiction>();
			SourceJurisdiction jurisdictionFromDB = sourceJurisdictionDao.fetchJurisdiction("All");
			if (jurisdictionFromDB != null) {
				jurisdictionList.add(jurisdictionFromDB);
				sources.setSourceJurisdiction(jurisdictionList);
			}
			Sources sourcesSaved = sourcesDao.create(sources);
			for (Classifications classifications : classificationsList) {
				for (SubClassifications subClassifications : classifications.getSubClassifications()) {
					SourceCredibility credibility = new SourceCredibility(sourcesSaved,
							subClassificationService.find(subClassifications.getSubClassificationId()), null,
							CredibilityEnums.HIGH);
					sourceCredibilityService.save(credibility);
				}
			}
			for (Classifications classifications : classificationsList) {
				SourcesHideStatus hideStatus = new SourcesHideStatus(true, false, false, false, false,
						sourcesSaved.getSourceId(), classifications.getClassificationId());
				hideStatus.setHideMedia(false);
				sourcesHideStatusService.save(hideStatus);
			}
			eventPublisher.publishEvent(new LoggingEvent(sourcesSaved.getSourceId(), sources.getSourceName(),
					LoggingEventType.SOURCE_CREATED, userId, new Date()));
		}
	}

	@Override
	@Transactional("transactionManager")
	public List<Sources> fetchClassifctaionSources(String classiifcationName) {
		return sourcesDao.fetchClassifctaionSources(classiifcationName);
	}
	
	@Override
	@Transactional("transactionManager")
	public List<Sources> fetchClassifctaionSourcesWithCredibility(String classiifcationName) {
		List<Sources> sourcesList = sourcesDao.fetchClassifctaionSources(classiifcationName);
		sourcesList.stream()
				.forEach(source -> Hibernate.initialize(source.getSourceCredibilityList()));
		return sourcesList;
	}

	@Override
	public String getInfoFromDomains(String domain, Integer count) throws Exception {
		count = count != null ? count : 20;
		String url = BIG_DATA_DOMAINS_LIST_URL;
		if (domain != null)
			url = BIG_DATA_DOMAINS_LIST_URL + "search-domain?count=" + count + "&domain=" + domain;
		return getDataFromServer(url);
	}
	
	private String getDataFromServer(String url) throws Exception {
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	@Transactional("transactionManager")
	public String getSourceCategoryBySourceName(String source) {
		String category = sourcesDao.getSourceCategoryBySourceName(source);
		return category;
	}
}
