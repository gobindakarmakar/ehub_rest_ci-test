package element.bst.elementexploration.rest.util;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.AlertServiceImpl;
import element.bst.elementexploration.rest.settings.listManagement.dto.ListItemDto;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

public class CsvToJsonUtil {
	
	public static String convertCsvToJson(List<CSVRecord> csvRecord,List<ListItemDto> jurisdictions, double confidence)
			throws Exception {
		JSONObject json = new JSONObject();
		json.put("confidence", confidence);
		json.put("entities", new JSONArray());
		JSONArray entityArray = new JSONArray();
		Iterator<CSVRecord> iterator=csvRecord.iterator();
		while (iterator.hasNext()) {
			JSONObject jsonEntity = new JSONObject();
			CSVRecord record = iterator.next();
			long recordNum = record.getRecordNumber();
			StringBuffer addMsg = new StringBuffer(", ");
			addMsg.append("record number : ");
			addMsg.append(recordNum);
			if (record.get("Entity Name").isEmpty()) 
				throw new NoDataFoundException(ElementConstants.ENTITY_NAME_NOT_FOUND+addMsg);
			
			jsonEntity.put("name", new String(record.get("Entity Name").getBytes(), StandardCharsets.UTF_8));
			
			if (record.get("Entity Type").isEmpty())
				throw new NoDataFoundException(ElementConstants.ENTITY_TYPE_ERROR+addMsg);
			
			if(!("person".equals(record.get("Entity Type")) || "organization".equals(record.get("Entity Type"))))
				throw new NoDataFoundException(ElementConstants.ENTITY_TYPE_ERROR+addMsg);
			
			jsonEntity.put("entity_type", record.get("Entity Type"));
			
			if (!record.get("Birth Date/Registration Date").isEmpty()) {
				try {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					dateFormat.setLenient(false);
					dateFormat.parse(record.get("Birth Date/Registration Date"));
				} catch (Exception e) {
					throw new NoDataFoundException(ElementConstants.DATE_FORMAT_ERROR+addMsg);
				}
			}
			
			List<ListItemDto> singleCountry = null;
			if(record.get("Birth Place/Country of Registration")!=null && !record.get("Birth Place/Country of Registration").trim().isEmpty()) {
				singleCountry = jurisdictions.stream()
				.filter(juri -> juri.getDisplayName().equalsIgnoreCase(record.get("Birth Place/Country of Registration").trim()))
				.collect(Collectors.toList());
			}
			
			if ("organization".equals(record.get("Entity Type"))) {
				if (record.get("Birth Place/Country of Registration").isEmpty())
					throw new NoDataFoundException(ElementConstants.COUNTRY_NOT_FOUND+addMsg);
				
				if (singleCountry == null || singleCountry.size() == 0) {
					jsonEntity.put("jurisdiction", ElementConstants.EMPTY_VALUE);
					jsonEntity.put("place_of_registration", ElementConstants.EMPTY_VALUE);
				}else {
					jsonEntity.put("jurisdiction", singleCountry.get(0).getCode());
					jsonEntity.put("place_of_registration", singleCountry.get(0).getCode());
				}
				jsonEntity.put("date_of_registration", record.get("Birth Date/Registration Date"));
                

			} else {
				if (singleCountry == null || singleCountry.size() == 0) 
					jsonEntity.put("place_of_birth", ElementConstants.EMPTY_VALUE);
				else	
					jsonEntity.put("place_of_birth", singleCountry.get(0).getCode());
				
				jsonEntity.put("date_of_birth", record.get("Birth Date/Registration Date"));
				
			}

			jsonEntity.put("customer_id", ElementConstants.EMPTY_VALUE);
			jsonEntity.put("entity_id", record.get("Entity ID"));
			
			entityArray.put(jsonEntity);
		}
		json.put("entities", entityArray);
		return json.toString();
	}
	
	public static File convertJsontoCsv(String jsonString) {
		File file = null;
		try {
			file = new File("/opt/corporatestructurepath/"+"sample.json");
			FileUtils.writeByteArrayToFile(file, jsonString.getBytes());	
		}catch(Exception e) {
			e.printStackTrace();
		}
        return file;
	}

}
