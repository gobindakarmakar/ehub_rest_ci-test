package element.bst.elementexploration.rest.extention.significantnews.serviceImpl;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantCommentDao;
import element.bst.elementexploration.rest.extention.significantnews.dao.SignificantNewsDao;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantComment;
import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantNews;
import element.bst.elementexploration.rest.extention.significantnews.dto.SignificantnewsDto;
import element.bst.elementexploration.rest.extention.significantnews.service.SignificantnewsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.dao.AuditLogDao;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.user.dao.UsersDao;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Service("significantnewsService")
public class SignificantnewsServiceImpl extends GenericServiceImpl<SignificantNews, Long>
		implements SignificantnewsService {

	@Autowired
	private SignificantNewsDao significantNewsDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	UsersService usersService;

	@Autowired
	private AuditLogDao auditLogDao;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	SignificantCommentDao significantCommentDao;

	@Autowired
	public SignificantnewsServiceImpl(GenericDao<SignificantNews, Long> genericDao) {
		super(genericDao);

	}

	@Override
	@Transactional("transactionManager")
	public SignificantNews getSignificantNews(String entityId, String entityName, String publishedDate, String title,
			String url, String newsClass) {
		SignificantNews significantNews = significantNewsDao.getSignificantNews(entityId, entityName, publishedDate,
				title, url, newsClass);
		return significantNews;
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteSignificantNews(SignificantnewsDto significantnewsDto, Long userId) {
		boolean found = true;
		SignificantNews existedSignificantNews = significantNewsDao.getSignificantNews(significantnewsDto.getEntityId(),
				significantnewsDto.getEntityName(), significantnewsDto.getPublishedDate(),
				significantnewsDto.getTitle(), significantnewsDto.getUrl(), significantnewsDto.getNewsClass());
		if (existedSignificantNews != null) {
			existedSignificantNews.setSignificantNews(false);
			significantNewsDao.saveOrUpdate(existedSignificantNews);
			if (significantnewsDto.getComment() != null) {
				SignificantComment significantComment = new SignificantComment();
				significantComment.setClassification("NEWS");
				significantComment.setComment(significantnewsDto.getComment());
				significantComment.setUserId(userId);
				significantComment.setIsSignificant(false);
				significantComment.setSignificantId(existedSignificantNews.getId());
				significantCommentDao.create(significantComment);
			}

		} else
			found = false;
		if (found) {
			AuditLog log = new AuditLog(new Date(), LogLevels.WARNING, "SIGNIFICANT NEWS", null,
					"Significant news deleted");
			Users author = usersDao.find(userId);
			author = usersService.prepareUserFromFirebase(author);
			log.setDescription(author.getFirstName() + " " + author.getLastName() + " deleted significant news "
					+ significantnewsDto.getTitle());
			log.setUserId(userId);
			log.setName(significantnewsDto.getTitle());
			auditLogDao.create(log);
		}
		return found;
	}

	@Override
	@Transactional("transactionManager")
	public SignificantnewsDto saveSignificantNews(SignificantnewsDto significantnewsDto) {
		SignificantNews resultSignificantNews = null;
		SignificantnewsDto resultSignificantnewsDto = new SignificantnewsDto();
		SignificantNews significantNews = new SignificantNews();
		BeanUtils.copyProperties(significantnewsDto, significantNews);
		significantNews.setSignificantNews(significantnewsDto.getIsSignificantNews());
		SignificantNews existedSignificantNews = significantNewsDao.getSignificantNews(
				significantNews.getEntityId(), significantNews.getEntityName(), significantNews.getPublishedDate(),
				significantNews.getTitle(), significantNews.getUrl(), significantNews.getNewsClass());
		if (existedSignificantNews != null) {
			if (significantNews.isSignificantNews()) {
				existedSignificantNews.setSignificantNews(true);
				significantNewsDao.saveOrUpdate(existedSignificantNews);
				eventPublisher.publishEvent(new LoggingEvent(existedSignificantNews.getId(),
						LoggingEventType.SIGNIFICANT_NEWS, significantNews.getUserId(), new Date()));
				resultSignificantNews = existedSignificantNews;
				
			}
		} else {
			if (significantNews.isSignificantNews()) {
				resultSignificantNews = significantNewsDao.create(significantNews);
				eventPublisher.publishEvent(new LoggingEvent(resultSignificantNews.getId(),
						LoggingEventType.SIGNIFICANT_NEWS, resultSignificantNews.getUserId(), new Date()));
			}
		}
		if(resultSignificantNews.getId()!=null){
		BeanUtils.copyProperties(resultSignificantNews, resultSignificantnewsDto);
		resultSignificantnewsDto.setIsSignificantNews(resultSignificantNews.isSignificantNews());
		resultSignificantnewsDto.setComment(significantnewsDto.getComment());
		}	
		
		return resultSignificantnewsDto;
	}

}
