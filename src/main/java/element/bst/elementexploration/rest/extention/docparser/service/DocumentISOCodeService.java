package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentISOCodes;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface DocumentISOCodeService extends GenericService<DocumentISOCodes, Long>{
	
	
	public boolean saveIsoCodes();
	
	
}
