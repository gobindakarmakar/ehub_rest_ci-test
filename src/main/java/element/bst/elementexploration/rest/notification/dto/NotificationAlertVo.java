package element.bst.elementexploration.rest.notification.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Notification alerts")
public class NotificationAlertVo {

	@ApiModelProperty(value="Subject of the notification requested",required=true)
	@Column(name = "subject", length = 64)
	@NotEmpty(message = "Subject can not be blank.")
	private String subject;

	@ApiModelProperty(value="Body of the notification requested",required=true)
	@Column(name = "body", length = 256)
	@NotEmpty(message = "Body can not be blank.")
	private String body;

	@ApiModelProperty(value="Type of the notification requested")
	@Column(name = "type")
	private Integer type;

	@ApiModelProperty(value="Deliver Date of the notification requested")
	@Column(name = "deliverOn")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime deliverOn;

	@ApiModelProperty(value="Level of the notification requested")
	@Column(name = "level")
	private Integer level;

	@ApiModelProperty(value="Flag of the notification requested")
	@Column(name = "flag")
	private Integer flag;

	@ApiModelProperty(value="CaseId  of the notification requested")
	@Column(name = "case_id")
	private Long caseId;

	@Transient
	@ApiModelProperty(value="Recipients of the notification requested",required=true)
	private ArrayList<Long> recipients;

	public NotificationAlertVo() {
		super();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public LocalDateTime getDeliverOn() {
		return deliverOn;
	}

	public void setDeliverOn(LocalDateTime deliverOn) {
		this.deliverOn = deliverOn;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public ArrayList<Long> getRecipients() {
		return recipients;
	}
}
