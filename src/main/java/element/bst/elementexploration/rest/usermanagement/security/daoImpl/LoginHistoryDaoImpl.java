package element.bst.elementexploration.rest.usermanagement.security.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.dao.LoginHistoryDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Prateek Maurya
 *
 */
@Repository("loginHistoryDao")
public class LoginHistoryDaoImpl extends GenericDaoImpl<LoginHistory, Long> implements LoginHistoryDao{

	public LoginHistoryDaoImpl() {
        super(LoginHistory.class);
    }

	@Override
	public LoginHistory getFailedLoginByUsername(String username) {
		LoginHistory failedLogin = null;
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<LoginHistory> query = builder.createQuery(LoginHistory.class);
			Root<LoginHistory> login = query.from(LoginHistory.class);
			query.select(login);
			query.where(builder.or(builder.equal(login.get("username"), username), builder.equal(login.get("screenName"), username)));
			query.orderBy(builder.desc(login.get("loginTime")));
			List<LoginHistory> failedLogins = this.getCurrentSession().createQuery(query).setMaxResults(1).getResultList();
			if(failedLogins != null && !failedLogins.isEmpty()){
				failedLogin =  failedLogins.get(0);
			}
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, LoginHistoryDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return failedLogin;
	}

	@Override
	public List<LoginHistory> getlogonsByUsernames(List<String> usernamesList, Date from, Date to) {
		List<LoginHistory> logonsList = new ArrayList<LoginHistory>();
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<LoginHistory> query = builder.createQuery(LoginHistory.class);
			Root<LoginHistory> login = query.from(LoginHistory.class);
			query.select(login);
			
			if(usernamesList != null && from == null && to == null){
				query.where(login.get("username").in(usernamesList));
			}else if(usernamesList != null && from != null && to != null){
				query.where(
						builder.and(
								login.get("username").in(usernamesList)),
						builder.and(
								builder.greaterThanOrEqualTo(login.get("loginTime"), from), 
								builder.lessThanOrEqualTo(login.get("loginTime"), to)));
			}
			else if(from != null && to != null){
				query.where(
						builder.and(
								builder.greaterThanOrEqualTo(login.get("loginTime"), from), 
								builder.lessThanOrEqualTo(login.get("loginTime"), to)));
			}
			logonsList = this.getCurrentSession().createQuery(query).getResultList();
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, LoginHistoryDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return logonsList;
	}

	@Override
	public Map<Long, List<LoginHistory>> getFailedlogonsByUserIdList(List<Long> userIdList) {
		Map<Long, List<LoginHistory>> finalResult = new HashMap<Long, List<LoginHistory>>();
		List<LoginHistory> resultList = new ArrayList<>();
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<LoginHistory> query = builder.createQuery(LoginHistory.class);
			Root<LoginHistory> login = query.from(LoginHistory.class);
			query.select(login);
			query.where(builder.and(builder.equal(login.get("loginStatus"), ElementConstants.FAILED_STATUS),
					builder.in(login.get("userId")).value(userIdList)));
			resultList = this.getCurrentSession().createQuery(query).getResultList();
			
			finalResult = resultList.stream().collect(Collectors.groupingBy(LoginHistory::getUserId, Collectors.toList()));
			
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, LoginHistoryDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return finalResult;
	}

}
