package element.bst.elementexploration.rest.casemanagement.dao;

import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.casemanagement.domain.CaseDocDetails;
import element.bst.elementexploration.rest.casemanagement.dto.DocAggregatorDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface CaseDocumentDao extends GenericDao<CaseDocDetails, Long> {
	
	List<CaseDocDetails> getAllDocumentsForCaseSeed(Long caseSeedId, Integer docFlag,String orderBy, Integer pageNumber, Integer recordsPerPage);
	
	Long getCaseIdFromDoc(long docId);
	
	Long countAllDocumentsForCaseSeed(Long caseSeedId,Integer docFlag);

	List<DocAggregatorDto> docAggregator(Long caseId,Date fromDate,Date toDate);
	
}
