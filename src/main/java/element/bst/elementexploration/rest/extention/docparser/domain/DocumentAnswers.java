package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_ANSWERS")
public class DocumentAnswers implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String answer;
	private String specifiedAnswer;
	private DocumentQuestions documentQuestions;
	private Date updatedTime;
	private long documetnId;
	private long modefiedBy;
	private String evaluation;	
	private String actualAnswer;
	private String entityId;
	private Integer answerRank;
	private Date rankUpdatedTime;
	private long rankUpdatedBy;
	private String sourceUrls;
	private String tableDataPosition;
	

	public DocumentAnswers() {
		super();
		
	}	

	

	public DocumentAnswers(long id, String answer, String specifiedAnswer, DocumentQuestions documentQuestions,
			Date updatedTime, long documetnId, long modefiedBy, String evaluation, String actualAnswer, String entityId,
			Integer answerRank, Date rankUpdatedTime, long rankUpdatedBy,String sourceUrls,String tableDataPosition) {
		super();
		this.id = id;
		this.answer = answer;
		this.specifiedAnswer = specifiedAnswer;
		this.documentQuestions = documentQuestions;
		this.updatedTime = updatedTime;
		this.documetnId = documetnId;
		this.modefiedBy = modefiedBy;
		this.evaluation = evaluation;
		this.actualAnswer = actualAnswer;
		this.entityId = entityId;
		this.answerRank = answerRank;
		this.rankUpdatedTime = rankUpdatedTime;
		this.rankUpdatedBy = rankUpdatedBy;
		this.sourceUrls = sourceUrls;
		this.tableDataPosition = tableDataPosition;
	}



	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "ANSWER",columnDefinition = "LONGTEXT")
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@ManyToOne
	@JoinColumn(name = "DOCUMENT_QUESTION_ID")
	public DocumentQuestions getDocumentQuestions() {
		return documentQuestions;
	}

	public void setDocumentQuestions(DocumentQuestions documentQuestions) {
		this.documentQuestions = documentQuestions;
	}

	@Column(name = "UPDATED_TIME")	
	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	@Column(name="SPECIFIED_ANSWER",columnDefinition = "LONGTEXT")
	public String getSpecifiedAnswer() {
		return specifiedAnswer;
	}

	public void setSpecifiedAnswer(String specifiedAnswer) {
		this.specifiedAnswer = specifiedAnswer;
	}
	@Column(name="DOCUMENT_ID")
	public long getDocumetnId() {
		return documetnId;
	}

	public void setDocumetnId(long documetnId) {
		this.documetnId = documetnId;
	}
	
	@Column(name="MODEFIED_BY")
	public long getModefiedBy() {
		return modefiedBy;
	}

	public void setModefiedBy(long modefiedBy) {
		this.modefiedBy = modefiedBy;
	}
	@Column(name="EVALUATION")
	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	@Column(name="ACTUAL_ANSWER",columnDefinition = "LONGTEXT")
	public String getActualAnswer() {
		return actualAnswer;
	}

	public void setActualAnswer(String actualAnswer) {
		this.actualAnswer = actualAnswer;
	}
	@Column(name="ENTITY_ID")
	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	@Column(name="ANSWER_RANK")
	public Integer getAnswerRank() {
		return answerRank;
	}

	public void setAnswerRank(Integer answerRank) {
		this.answerRank = answerRank;
	}
	
	@Column(name = "RANK_UPDATED_TIME")	
	public Date getRankUpdatedTime() {
		return rankUpdatedTime;
	}

	public void setRankUpdatedTime(Date rankUpdatedTime) {
		this.rankUpdatedTime = rankUpdatedTime;
	}
	@Column(name = "RANK_MODEFIED_BY")
	public long getRankUpdatedBy() {
		return rankUpdatedBy;
	}

	public void setRankUpdatedBy(long rankUpdatedBy) {
		this.rankUpdatedBy = rankUpdatedBy;
	}
	@Column(name="SOURCE_URLS",columnDefinition = "LONGTEXT")
	public String getSourceUrls() {
		return sourceUrls;
	}

	public void setSourceUrls(String sourceUrls) {
		this.sourceUrls = sourceUrls;
	}
	@Column(name="TABLE_DATA_POSITION")
	public String getTableDataPosition() {
		return tableDataPosition;
	}

	public void setTableDataPosition(String tableDataPosition) {
		this.tableDataPosition = tableDataPosition;
	}



	@Override
	public String toString() {
		return "DocumentAnswers [id=" + id + ", answer=" + answer + ", specifiedAnswer=" + specifiedAnswer
				+ ", documentQuestions=" + documentQuestions + ", updatedTime=" + updatedTime + ", documetnId="
				+ documetnId + ", modefiedBy=" + modefiedBy + ", evaluation=" + evaluation + ", actualAnswer="
				+ actualAnswer + ", entityId=" + entityId + ", answerRank=" + answerRank + ", rankUpdatedTime="
				+ rankUpdatedTime + ", rankUpdatedBy=" + rankUpdatedBy + ", sourceUrls=" + sourceUrls
				+ ", tableDataPosition=" + tableDataPosition + "]";
	}

		
}
