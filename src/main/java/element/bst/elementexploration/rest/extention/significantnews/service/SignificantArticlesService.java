package element.bst.elementexploration.rest.extention.significantnews.service;

import org.json.JSONArray;

import element.bst.elementexploration.rest.extention.significantnews.domain.SignificantArticles;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface SignificantArticlesService extends GenericService<SignificantArticles, Long>{

	SignificantArticles saveSignificantArticles(SignificantArticles significantArticles, Long userId);

	SignificantArticles getSignificantArticles(String uuid);

	SignificantArticles saveArticleSentiment(SignificantArticles significantArticles);

	boolean deleteSignificantArticles(String uuid,String comment,Long userId);

	JSONArray getAllSignificantNews(String entityId) throws Exception;

}
