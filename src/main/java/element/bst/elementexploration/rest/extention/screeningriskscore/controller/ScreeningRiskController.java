package element.bst.elementexploration.rest.extention.screeningriskscore.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.screeningriskscore.domain.ScreeningDetails;
import element.bst.elementexploration.rest.extention.screeningriskscore.service.ScreeningRiskScoreService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Screening risk API" },description="Manages screening risk")
@RestController
@RequestMapping("/api/screeningRiskScore")
public class ScreeningRiskController extends BaseController {

	@Autowired
	ScreeningRiskScoreService screeningRiskScoreService;
	
	@ApiOperation("Gets screening name risk score")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG)})			
	@PostMapping(value = "/getRiskScore", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRiskScore(@RequestBody ScreeningDetails teal,HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(screeningRiskScoreService.screeningRiskSocreCalculation(teal), HttpStatus.OK);
	}
	
	@ApiOperation("Gives screening information")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/Stage/screenings", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getScreeningInfo(@RequestBody String jsonString, @RequestParam String token) throws Exception {
		return new ResponseEntity<>(screeningRiskScoreService.getScreeningInfo(jsonString), HttpStatus.OK);
	}
	
}
