package element.bst.elementexploration.rest.usermanagement.security.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
/**
 * 
 * @author Amit Patel
 *
 */
public class UserSetting implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String country;
	
	private String isoCode;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
