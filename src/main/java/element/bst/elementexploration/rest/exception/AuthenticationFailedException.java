package element.bst.elementexploration.rest.exception;

/**
 * 
 * @author Amit Patel
 *
 */
public class AuthenticationFailedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6452908626192012961L;


	
	private  String message;
	
	public static final String AUTHENTICATION_FAILED_EXCEPTION="Authentication failed";
	
	public AuthenticationFailedException(String message) {
		super();
		this.message = message;
	}



	public AuthenticationFailedException() {

		super();
		message = AUTHENTICATION_FAILED_EXCEPTION;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuthenticationFailedException [message=" + message + "]";
	}
}
