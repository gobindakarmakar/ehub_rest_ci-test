package element.bst.elementexploration.rest.extention.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.workflow.service.WorkflowGraphService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "Workflow graph API" },description="Manages workflow graph data")
@RestController
@RequestMapping("/api/workflow/graph")
public class WorkflowGraphController extends BaseController{

	@Autowired
	private WorkflowGraphService workflowGraphService;

	//orient
	@ApiOperation("List orient databases")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/orient/databases/{profileId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfOrientDatabases(@RequestParam String token, @PathVariable String profileId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.listOfOrientDatabases(profileId), HttpStatus.OK);
	}

	@ApiOperation("Lists vertex and edge classes of selected database")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/orient/dbclasses/{profileId}/{database}/{classType}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfVertexAndEdges(@RequestParam String token, @PathVariable String profileId,
			@PathVariable String database, @PathVariable String classType) throws Exception {
		return new ResponseEntity<>(workflowGraphService.listOfVertexAndEdges(profileId, database, classType), HttpStatus.OK);
	}

	//graph
	@ApiOperation("Delete contingency tables")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/graph/contingencyTable", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteTables(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowGraphService.deleteTables(), HttpStatus.OK);
	}

	@ApiOperation("Get contingency tables")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/contingencyTable", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getContingencyTable(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowGraphService.getContingencyTable(), HttpStatus.OK);
	}

	@ApiOperation("Update contingency tables")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PutMapping(value = "/graph/contingencyTable", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> updateContingencyTable(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.updateContingencyTable(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Delete contingency table by id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@DeleteMapping(value = "/graph/contingencyTable/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteContingencyTableById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.deleteContingencyTableById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Get contingency table by id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/contingencyTable/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getContingencyTableById(@RequestParam String token, @PathVariable String workflowId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.getContingencyTableById(workflowId), HttpStatus.OK);
	}

	@ApiOperation("Save contingency table by id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/graph/contingencyTable/{workflowId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> insertContingencyTableById(@RequestParam String token, @RequestBody String jsonString,
			@PathVariable String workflowId) throws Exception {
		return new ResponseEntity<>(workflowGraphService.insertContingencyTableById(jsonString, workflowId), HttpStatus.OK);
	}


	@ApiOperation("Returns VLA graph data by case id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/data/{caseId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> graphdataByCaseId(@RequestParam String token, @PathVariable String caseId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.graphdataByCaseId(caseId), HttpStatus.OK);
	}

	@ApiOperation("Returns VLA graph data by case id and limits number of results")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/data/{caseId}/{limit}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> graphdataByCaseIdAndLimit(@RequestParam String token, @PathVariable String caseId,
			@PathVariable String limit) throws Exception {
		return new ResponseEntity<>(workflowGraphService.graphdataByCaseIdAndLimit(caseId, limit), HttpStatus.OK);
	}

	@ApiOperation("Finds Entity by name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/name/{name}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> findEntityByName(@RequestParam String token, @PathVariable String name) throws Exception {
		return new ResponseEntity<>(workflowGraphService.findEntityByName(name), HttpStatus.OK);
	}

	@ApiOperation("Adds Entity")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/graph/unified", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> addEntity(@RequestParam String token, @RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(workflowGraphService.addEntity(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Finds Entity by identifier")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/{identifier}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> findEntityByIdentifier(@RequestParam String token, @PathVariable String identifier)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.findEntityByIdentifier(identifier), HttpStatus.OK);
	}

	//fetcherlist 
	@ApiOperation("Lists all fetchers")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/fetcherlist", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fetcherList(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowGraphService.fetcherList(), HttpStatus.OK);
	}

	@ApiOperation("Lists details of a fetchers")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/fetcherlist/{fetcherId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> fetcherListById(@RequestParam String token, @PathVariable String fetcherId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.fetcherListById(fetcherId), HttpStatus.OK);
	}

	//search
	@ApiOperation("search")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/search", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> search(@RequestParam String token, @RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(workflowGraphService.search(jsonString), HttpStatus.OK);
	}

	@ApiOperation("List cas-ids for graph")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/search/caseids", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfOrientDatabases(@RequestParam String token) throws Exception {
		return new ResponseEntity<>(workflowGraphService.listOfOrientDatabases(), HttpStatus.OK);
	}

	@ApiOperation("List of orient databases by query")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/search/caseids/{query}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> listOfOrientDatabaseByQuery(@RequestParam String token, @PathVariable String query)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.listOfOrientDatabaseByQuery(query), HttpStatus.OK);
	}

	@ApiOperation("Search graph")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/search/graph", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchGraph(@RequestParam String token, @RequestBody String jsonString) throws Exception {
		return new ResponseEntity<>(workflowGraphService.searchGraph(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Search hints")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/search/hints/{searchType}/{query}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchHints(@RequestParam String token, @PathVariable String query,
			@PathVariable String searchType) throws Exception {
		return new ResponseEntity<>(workflowGraphService.searchHints(searchType, query), HttpStatus.OK);
	}

	@ApiOperation("Search resolve")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/search/resolve", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchResolve(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.searchResolve(jsonString), HttpStatus.OK);
	}

	@ApiOperation("Search graph schema")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/search/searchGraphSchema/{searchType}/{query}", produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> searchGraphSchema(@RequestParam String token, @PathVariable String query,
			@PathVariable String searchType) throws Exception {
		return new ResponseEntity<>(workflowGraphService.searchGraphSchema(searchType, query), HttpStatus.OK);
	}
	
	@ApiOperation("Builds case graph")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@PostMapping(value = "/case/graph", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> caseGraph(@RequestParam String token, @RequestBody String jsonString)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.caseGraph(jsonString), HttpStatus.OK);
	}
	
	@ApiOperation("Returns associated entities of a case")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/case/{caseId}/entities", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getEntitesForCase(@RequestParam String token, @PathVariable String caseId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.getEntitesForCase(caseId), HttpStatus.OK);
	}
	
	@ApiOperation("Returns risk score of an entity")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Operation Successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message =ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/graph/risk/{entityId}", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRickScoreOfEntity(@RequestParam String token, @PathVariable String entityId)
			throws Exception {
		return new ResponseEntity<>(workflowGraphService.getRickScoreOfEntity(entityId), HttpStatus.OK);
	}

}
