package element.bst.elementexploration.rest.usermanagement.user.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.domain.CaseAnalystMapping;
import element.bst.elementexploration.rest.document.domain.DocumentVault;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroups;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserRoles;

/**
 * @author Paul Jalagari
 *
 */
@Entity
@Table(name = "bst_um_user")
@Access(AccessType.FIELD)
public class Users implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;

	@Column(name = "emailAddress", length = 75)
	@NotEmpty(message = "Email can not be blank.")
	@Email(message = "Invalid email address.")
	private String emailAddress;

	@Column(name = "firstName", length = 75)
	@NotEmpty(message = "FirstName can not be blank.")
	private String firstName;

	@Column(name = "lastName", length = 75)
	@NotEmpty(message = "LastName can not be blank.")
	private String lastName;

	@Column(name = "middleName", length = 75)
	private String middleName;

	@Column(name = "dateOfBirth")
	Date dob;

	@Column(name = "password")
	//@NotEmpty(message = "Password can not be blank.")
	private String password;

	@Column(name = "screenName", length = 75)
	@NotEmpty(message = "Screen name can not be blank.")
	private String screenName;

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "statusID") private ListItem status;
	 */

	@Column(name = "emailAddressVerified", columnDefinition = "int default 0")
	private int emailAddressVerified;

	@Column
	private Long createdBy;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "modifiedDate")
	private Date modifiedDate;

	@Column(name = "lastLoginDate")
	private Date lastLoginDate;

	@Column(name = "lastLoginIP", length = 20)
	private String lastLoginIP;

	@Column(name = "temporaryPassword")
	private String temporaryPassword;

	@Column(name = "temporaryPassword_expire_on")
	private Date temporaryPasswordExpiresOn;

	@Lob
	@Column(name = "userImage", nullable = true, columnDefinition = "mediumblob")
	private byte[] userImage;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, targetEntity = CaseAnalystMapping.class, fetch = FetchType.LAZY)
	private List<CaseAnalystMapping> caseSeedAnalystMappingsList;

	@OneToMany(mappedBy = "uploadedBy", cascade = CascadeType.ALL)
	private List<DocumentVault> documentVaults;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "createdBy", cascade = CascadeType.ALL, targetEntity = Case.class)
	private List<Case> bstCaseSeedsList;
	@JsonIgnore
	@OneToMany(mappedBy = "userId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<UserRoles> userRoles;

	@JsonIgnore
	@OneToMany(mappedBy = "userId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<UserGroups> userGroups;

	@JsonIgnore
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name = "countryId")
	private ListItem countryId;

	@JsonIgnore
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name = "statusId")
	private ListItem statusId;
	
	@Column(name = "jobTitle")
	private String jobTitle;

	@Column(name = "extension")
	private String extension;

	@Column(name = "phoneNumber")
	private String phoneNumber;
	
	@Column(name = "department")
	private String department;
	
	@Column(name = "source")
	private String source;
	
	@Column(name = "sourceId", length = 255)
	private String sourceId;
	
	@Column(name = "isModifiable", columnDefinition = "boolean default true", nullable = false)
	private Boolean isModifiable=true;
	
	@OneToMany(mappedBy = "uploadedBy", cascade = CascadeType.ALL)	
	private List<FileBst> files;

	/**
	 * 
	 */
	public Users() {
		super();
	}

	/**
	 * @param userId
	 * @param uuid
	 * @param emailAddress
	 * @param firstName
	 * @param lastName
	 * @param middleName
	 * @param contactId
	 * @param caseSeedAnalystMappingsList
	 */
	public Users(Long userId, String uuid, String emailAddress, String firstName, String lastName, String middleName,
			List<CaseAnalystMapping> caseSeedAnalystMappingsList) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
	}

	public Users(String screenName, String firstName, String lastName, Date createdDate, ListItem statusId) {
		super();
		this.screenName = screenName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createdDate = createdDate;
		this.statusId = statusId;
	}

	public Users(Long userId, String emailAddress, String firstName, String lastName, String middleName, Date dob,
			ListItem country, String password, String screenName, ListItem statusId, int emailAddressVerified,
			Long createdBy, Date createdDate, Date modifiedDate, Date lastLoginDate, String lastLoginIP,
			String temporaryPassword, Date temporaryPasswordExpiresOn,
			List<CaseAnalystMapping> caseSeedAnalystMappingsList, List<DocumentVault> documentVaults,
			List<Case> bstCaseSeedsList) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.dob = dob;
		this.countryId = country;
		this.password = password;
		this.screenName = screenName;
		this.statusId = statusId;
		this.emailAddressVerified = emailAddressVerified;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.lastLoginDate = lastLoginDate;
		this.lastLoginIP = lastLoginIP;
		this.temporaryPassword = temporaryPassword;
		this.temporaryPasswordExpiresOn = temporaryPasswordExpiresOn;
		this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
		this.documentVaults = documentVaults;
		this.bstCaseSeedsList = bstCaseSeedsList;
	}

	public Users(Long userId, String emailAddress, String firstName, String lastName, String middleName, Date dob,
			String password, String screenName, int emailAddressVerified, Long createdBy, Date createdDate,
			Date modifiedDate, Date lastLoginDate, String lastLoginIP, String temporaryPassword,
			Date temporaryPasswordExpiresOn, byte[] userImage, List<CaseAnalystMapping> caseSeedAnalystMappingsList,
			List<DocumentVault> documentVaults, List<Case> bstCaseSeedsList, List<UserRoles> userRoles,
			List<UserGroups> userGroups, ListItem countryId, ListItem statusId, String jobTitle, String extension,
			String phoneNumber, String department, String source) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.dob = dob;
		this.password = password;
		this.screenName = screenName;
		this.emailAddressVerified = emailAddressVerified;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.lastLoginDate = lastLoginDate;
		this.lastLoginIP = lastLoginIP;
		this.temporaryPassword = temporaryPassword;
		this.temporaryPasswordExpiresOn = temporaryPasswordExpiresOn;
		this.userImage = userImage;
		this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
		this.documentVaults = documentVaults;
		this.bstCaseSeedsList = bstCaseSeedsList;
		this.userRoles = userRoles;
		this.userGroups = userGroups;
		this.countryId = countryId;
		this.statusId = statusId;
		this.jobTitle = jobTitle;
		this.extension = extension;
		this.phoneNumber = phoneNumber;
		this.department = department;
		this.source = source;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastLoginDate
	 */
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	/**
	 * @param lastLoginDate
	 *            the lastLoginDate to set
	 */
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	/**
	 * @return the lastLoginIP
	 */
	public String getLastLoginIP() {
		return lastLoginIP;
	}

	/**
	 * @param lastLoginIP
	 *            the lastLoginIP to set
	 */
	public void setLastLoginIP(String lastLoginIP) {
		this.lastLoginIP = lastLoginIP;
	}

	/**
	 * @return the caseSeedAnalystMappingsList
	 */
	/*public List<CaseAnalystMapping> getCaseSeedAnalystMappingsList() {
		return caseSeedAnalystMappingsList;
	}

	*//**
	 * @param caseSeedAnalystMappingsList
	 *            the caseSeedAnalystMappingsList to set
	 *//*
	public void setCaseSeedAnalystMappingsList(List<CaseAnalystMapping> caseSeedAnalystMappingsList) {
		this.caseSeedAnalystMappingsList = caseSeedAnalystMappingsList;
	}

	public List<DocumentVault> getDocumentVaults() {
		return documentVaults;
	}

	public void setDocumentVaults(List<DocumentVault> documentVaults) {
		this.documentVaults = documentVaults;
	}

	public List<Case> getBstCaseSeedsList() {
		return bstCaseSeedsList;
	}

	public void setBstCaseSeedsList(List<Case> bstCaseSeedsList) {
		this.bstCaseSeedsList = bstCaseSeedsList;
	}*/

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public int getEmailAddressVerified() {
		return emailAddressVerified;
	}

	public void setEmailAddressVerified(int emailAddressVerified) {
		this.emailAddressVerified = emailAddressVerified;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTemporaryPassword() {
		return temporaryPassword;
	}

	public void setTemporaryPassword(String temporaryPassword) {
		this.temporaryPassword = temporaryPassword;
	}

	public Date getTemporaryPasswordExpiresOn() {
		return temporaryPasswordExpiresOn;
	}

	public void setTemporaryPasswordExpiresOn(Date temporaryPasswordExpiresOn) {
		this.temporaryPasswordExpiresOn = temporaryPasswordExpiresOn;
	}

	public byte[] getUserImage() {
		return userImage;
	}

	public void setUserImage(byte[] userImage) {
		this.userImage = userImage;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	public List<UserGroups> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroups> userGroups) {
		this.userGroups = userGroups;
	}

	public ListItem getCountryId() {
		return countryId;
	}

	public void setCountryId(ListItem countryId) {
		this.countryId = countryId;
	}

	public ListItem getStatusId() {
		return statusId;
	}

	public void setStatusId(ListItem statusId) {
		this.statusId = statusId;
	}

	
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public Boolean getIsModifiable() {
		return isModifiable;
	}

	public void setIsModifiable(Boolean isModifiable) {
		this.isModifiable = isModifiable;
	}
	
	
	
}
