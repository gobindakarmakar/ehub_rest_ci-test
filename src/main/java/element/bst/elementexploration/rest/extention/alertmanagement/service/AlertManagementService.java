package element.bst.elementexploration.rest.extention.alertmanagement.service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedGroups;
import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertCommentsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.AssociatedAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.BatchScreeningResultDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.CreateAlertDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.MyAlertsDto;
import element.bst.elementexploration.rest.extention.alertmanagement.dto.PastDueAlertsDto;
import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.util.FilterModelDto;

/**
 * @author Prateek
 *
 */

public interface AlertManagementService extends GenericService<Alerts, Long>{

	AlertsDto saveOrUpdateAlerts(AlertsDto alertDto, long userId) throws Exception;

	boolean deleteAlert(Long alertId, long userId);

	Map<List<AlertsDto>, Long> getAlerts(FilterModelDto filterDto, Boolean isAllRequired, Integer pageNumber, Integer recordsPerPage, Long totalResults, String orderIn, String orderBy, Long feedId,Long currentUserId)  throws Exception;

	List<AlertsDto> getUserAlert(Long userId);

	AlertCommentsDto saveOrUpdateAlertComments(AlertCommentsDto alertCommentDto, long userId)throws Exception ;

	List<AlertCommentsDto> getAlertCommentsByAlertId(Long alertId);

	Long getAlertsCount();

	List<MyAlertsDto> getMyAlerts(Long userId);

	List<AssociatedAlertsDto> getAssociatedAlerts(Long userId);

	PastDueAlertsDto getPastDueAlerts(Long userId);

	List<Alerts> getAlertsByfeed(Long feedID);

	boolean deleteComment(Long alertId, Long commentId, long userId);
	
	ByteArrayOutputStream convertToCSV(List<AlertsDto> alertDtos);

	List<Alerts> findAllWithRisk();
	
	//String createAlerts(CreateAlertDto alerData);
	
	String getScreeningResultId(MultipartFile uploadFile, double confidence) throws Exception;
	
	BatchScreeningResultDto getScreeningResults(String id) throws Exception;
	public List<AlertsDto> timeStatusUpdater(List<AlertsDto> alertList) throws Exception;
	String getAlertsFromServer(String name, String givenName, String jurisdiction, String screeningType,
			String familyName, String birthDate, boolean isWrapper) throws Exception;
	
	Boolean createNewAlerts(String request) throws Exception;
	
	Alerts findBydetailId(Long detailId);

	String getODSScreeningResults(String input) throws Exception;

	void updateGroupLevelOnfeedChangeByRnak(Long feed_management_id, List<FeedGroups> feedGroupsListDTO);
	
	String postSSBData(String requestBody,String id) throws Exception;
	
	String getSSBScreeningData(String id) throws Exception;
	
	String getDataFromUrl(String url)throws Exception;
	
	List<Long> getAlertIdsByFeedIds(List<Long> comingIdList);
	List<Alerts> getAlertsByAlertIdList(List<Long> alertIdList);
	
	String getSubGraphVla(String entityId, int levels, String field, String parentId, String requestId) throws Exception;
	
	public boolean validateAlertStatusChange(Long existingStatus, Long statusTobeChanged) throws Exception;
}
