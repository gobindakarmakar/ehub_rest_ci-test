package element.bst.elementexploration.rest.settings.service;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.settings.domain.Attributes;

/**
 * @author suresh
 *
 */
public interface AttributesService extends GenericService<Attributes, Long>
{

}
