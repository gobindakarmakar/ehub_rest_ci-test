package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourceCredibilityService extends GenericService<SourceCredibility, Long> {

}
