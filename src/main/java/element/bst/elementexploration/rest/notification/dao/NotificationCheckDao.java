package element.bst.elementexploration.rest.notification.dao;

import java.time.LocalDateTime;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.notification.domain.NotificationCheck;


/**
 * @author Viswanath
 *
 */
public interface NotificationCheckDao extends GenericDao<NotificationCheck, Long>{

	LocalDateTime checkInNotification(Long userId);

	NotificationCheck checkInNotificationFull(Long userId);

	LocalDateTime getLastCheckIn(Long userId);

}
