package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

public class PeerResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<PeerDataDto> peerDataDto;
	int totalResults;

	public List<PeerDataDto> getPeerDataDto() {
		return peerDataDto;
	}

	public void setPeerDataDto(List<PeerDataDto> peerDataDto) {
		this.peerDataDto = peerDataDto;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

}
