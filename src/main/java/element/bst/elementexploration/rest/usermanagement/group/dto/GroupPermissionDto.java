package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "GroupPermission")

public class GroupPermissionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Id of the permission")
	private PermissionsWithOutObjectsDto permissionId;

	@ApiModelProperty(value = "Level of the Group")
	private Integer permissionLevel;

	@ApiModelProperty(value = "Id of the role")
	private RolesDto roleId;

	@ApiModelProperty(value = "Id of the Group")
	private GroupWithOutObjectsDto groupId;

	public PermissionsWithOutObjectsDto getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(PermissionsWithOutObjectsDto permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(Integer permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public RolesDto getRoleId() {
		return roleId;
	}

	public void setRoleId(RolesDto roleId) {
		this.roleId = roleId;
	}

	public GroupWithOutObjectsDto getGroupId() {
		return groupId;
	}

	public void setGroupId(GroupWithOutObjectsDto groupId) {
		this.groupId = groupId;
	}

	
	
	
}
