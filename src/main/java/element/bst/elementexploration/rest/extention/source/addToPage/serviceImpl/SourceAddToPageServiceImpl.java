package element.bst.elementexploration.rest.extention.source.addToPage.serviceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.document.service.DocumentService;
import element.bst.elementexploration.rest.extention.source.addToPage.dao.SourceAddToPageDao;
import element.bst.elementexploration.rest.extention.source.addToPage.domain.SourceAddToPage;
import element.bst.elementexploration.rest.extention.source.addToPage.dto.SourceAddToPageDto;
import element.bst.elementexploration.rest.extention.source.addToPage.service.SourceAddToPageService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

/**
 * @author hanuman
 *
 */
@Service("sourceAddToPageService")
@Transactional("transactionManager")
public class SourceAddToPageServiceImpl extends GenericServiceImpl<SourceAddToPage, Long> implements 
SourceAddToPageService {

	public SourceAddToPageServiceImpl() {

	}

	@Autowired
	SourceAddToPageDao sourceAddToPageDao;

	@Autowired
	DocumentService documentService;

	@Autowired
	public SourceAddToPageServiceImpl(GenericDao<SourceAddToPage, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	SourceAddToPageService sourceAddToPageService;

	@Override
	public List<SourceAddToPageDto> getAllSourceAddToPage() {
		List<SourceAddToPage> sourceaddToPageList = sourceAddToPageService.findAll();
		List<SourceAddToPageDto> sourceAddToPageDtosList = new ArrayList<SourceAddToPageDto>();
		if (sourceaddToPageList != null) {
			sourceAddToPageDtosList = sourceaddToPageList.stream().map(
					(entityComplexStructureDto) -> new EntityConverter<SourceAddToPage, SourceAddToPageDto>(SourceAddToPage.class, SourceAddToPageDto.class)
					.toT2(entityComplexStructureDto, new SourceAddToPageDto()))
					.collect(Collectors.toList());
		}
		return sourceAddToPageDtosList;
	}

	@Override
	public SourceAddToPageDto getSourceAddToPage(String entityId,String sourceName) throws Exception {
		SourceAddToPage sourceAddToPage = sourceAddToPageDao.getSourceAddToPage(entityId, sourceName);
		SourceAddToPageDto sourceAddToPageDto=null;
		String downloadLink=null;
		if (sourceAddToPage != null) {
			sourceAddToPageDto = new SourceAddToPageDto();
			if(sourceAddToPage.getDocId()!=null) {

				downloadLink=documentService.getS3DownloadLinkFromServer(sourceAddToPage.getDocId(), sourceAddToPage.getEntityId());
			}

			BeanUtils.copyProperties(sourceAddToPage, sourceAddToPageDto);
			sourceAddToPageDto.setScreenShotUrl(downloadLink);
		} 
		return sourceAddToPageDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<SourceAddToPageDto> saveSourcesAddToPage(List<SourceAddToPageDto> sourceAddToPageDtos,Long userIdTemp) throws Exception {
		SourceAddToPage sourceAddToPage =null;
		SourceAddToPageDto sourceAddToPageDto =null;
		List<SourceAddToPageDto> sourceAddToPageList = new ArrayList<SourceAddToPageDto>();

		if (sourceAddToPageDtos != null && sourceAddToPageDtos.size()>0) {

			for(SourceAddToPageDto addToPageDto: sourceAddToPageDtos) {
				
				SourceAddToPage pageDto= sourceAddToPageDao.getSourceAddToPage(addToPageDto.getEntityId(), addToPageDto.getSourceName());
				if(pageDto!=null) {
					pageDto.setEntityId(addToPageDto.getEntityId());
					pageDto.setIsAddToPage(addToPageDto.getIsAddToPage());
					pageDto.setSourceName(addToPageDto.getSourceName());
					sourceAddToPageDao.saveOrUpdate(pageDto);
					if(pageDto.getPageId()!=null) {
						SourceAddToPageDto sourcePageDto=getSourceAddToPage(pageDto.getEntityId(), pageDto.getSourceName());
						sourceAddToPageList.add(sourcePageDto);
					}

				}else {

					sourceAddToPage = new SourceAddToPage();
					sourceAddToPageDto = new SourceAddToPageDto();
					addToPageDto.setIsAddToPage(addToPageDto.getIsAddToPage());
					addToPageDto.setUpdatedTime(new Date());
					BeanUtils.copyProperties(addToPageDto, sourceAddToPage);
					sourceAddToPage=sourceAddToPageDao.create(sourceAddToPage);
					BeanUtils.copyProperties(sourceAddToPage, sourceAddToPageDto);
					sourceAddToPageList.add(sourceAddToPageDto);
				}
			}
		}
		return sourceAddToPageList;
	}

	@Override
	@Transactional("transactionManager")
	public JSONObject uploadAddToPageDocument(MultipartFile uploadDoc, String fileTitle, Long userId, Integer docFlag,
			String entityId, String sourceName, File annualPdf,boolean isAddToPage)
					throws Exception {

		JSONObject jsonObject =null;
		SourceAddToPage addToPage=null;
		String downloadLink=null;
		Long docId = documentService.uploadDocument(uploadDoc, fileTitle, "", userId, docFlag, null, entityId, null,sourceName, null, annualPdf);

		SourceAddToPage sourceAddToPage = sourceAddToPageDao.getSourceAddToPage(entityId, sourceName);
		if(sourceAddToPage!=null) {

			if(docId != null && uploadDoc!=null) {
				sourceAddToPage.setDocId(docId);
				sourceAddToPage.setIsAddToPage(isAddToPage);
				sourceAddToPage.setFileName(uploadDoc.getOriginalFilename());
				sourceAddToPage.setUpdatedTime(new Date());
				sourceAddToPageDao.saveOrUpdate(sourceAddToPage);
			}
		}else {
			addToPage = new SourceAddToPage();
			addToPage.setDocId(docId);
			addToPage.setEntityId(entityId);
			addToPage.setFileName(uploadDoc!=null?uploadDoc.getOriginalFilename():null);
			addToPage.setSourceName(sourceName);
			addToPage.setIsAddToPage(isAddToPage);
			addToPage.setUpdatedTime(new Date());
			addToPage=sourceAddToPageDao.create(addToPage);
		}
		
		if(docId!=null) {
			jsonObject = new JSONObject();
			downloadLink=documentService.getS3DownloadLinkFromServer(docId, entityId);
			jsonObject.put("docId", docId);
			jsonObject.put("downloadLink", downloadLink);
		}else {
			jsonObject = new JSONObject();
			jsonObject.put("docId", "");
			jsonObject.put("downloadLink", "");	
		}
		return jsonObject;

	}
	
	@Override
	@Transactional("transactionManager")
	public boolean deleteSourceAddToPage(String entityId,String sourceName,Long userId) throws Exception {

		SourceAddToPageDto addToPageDto=getSourceAddToPage(entityId, sourceName);
		if(addToPageDto!=null && addToPageDto.getDocId()!=null) {
			documentService.softDeleteDocumentById(addToPageDto.getDocId(), userId);
		}
		boolean isDeleted=sourceAddToPageDao.deleteSourceAddToPage(entityId, sourceName);
		return isDeleted;
	}



}
