package element.bst.elementexploration.rest.fileupload.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.simpleflatmapper.csv.CsvParser;
import org.simpleflatmapper.csv.CsvReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.casemanagement.dto.CaseDocumentUploadResponse;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.DocNotFoundException;
import element.bst.elementexploration.rest.fileupload.domain.FileBst;
import element.bst.elementexploration.rest.fileupload.service.FileService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Files API" })
@RestController
@RequestMapping("/api/fileStorage")
public class FileController extends BaseController {

	@Autowired
	private FileService fileService;

	@ApiOperation("Upload File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CaseDocumentUploadResponse.class, message = "File Uploaded Successfully"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.INVALID_DOC_FLAG_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> uploadFile(@RequestParam MultipartFile uploadFile, @RequestParam("token") String token,
			@RequestParam String fileName, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {

		Long userId = getCurrentUserId();

		return new ResponseEntity<>(fileService.uploadFile(uploadFile, userId, fileName), HttpStatus.OK);

	}

	@ApiOperation("Update File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_UPLOAD_FILE_TO_SERVER_MSG) })
	@PostMapping(value = "/updateFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> updateFile(@RequestParam MultipartFile uploadFile, @RequestParam("token") String token,
			@RequestParam Long docId, HttpServletRequest request, HttpServletResponse response)
			throws DocNotFoundException, Exception {

		Long userId = getCurrentUserId();

		return new ResponseEntity<>(fileService.updateFile(uploadFile, userId, docId), HttpStatus.OK);

	}

	@ApiOperation("Download File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	@GetMapping(value = "/downloadFile", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<StreamingResponseBody> downloadDocumentDetails(HttpServletResponse response,
			@RequestParam Long docId, @RequestParam("token") String token, HttpServletRequest request)
			throws DocNotFoundException, Exception {
		FileBst files = fileService.find(docId);
		if (files != null) {
			byte[] fileData = files.getFile();
			ServiceCallHelper.setDownloadResponse(response, files.getFileName(), files.getFileType(),
					files.getSize() != null ? files.getSize().intValue() : 0);
			StreamingResponseBody responseBody = new StreamingResponseBody() {
				@Override
				public void writeTo(OutputStream out) throws IOException {
					out.write(fileData);
					out.flush();
				}
			};
			return new ResponseEntity<>(responseBody, HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}
	
	@ApiOperation("Download Json File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	@GetMapping(value = "/downloadFileJson", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> downloadFileJson(HttpServletResponse response, @RequestParam String fileName,
			@RequestParam("token") String token, HttpServletRequest request) throws DocNotFoundException, Exception {
		FileBst files = fileService.findFileByName(fileName);
		if (files != null) {
			byte[] fileData = files.getFile();
			String fileDataString = new String(fileData,"UTF-8");
			CsvReader reader = CsvParser.reader(fileDataString);
			Iterator<String[]> iterator = reader.iterator();
			String[] headers = iterator.next();
			JSONObject json = new JSONObject();
			while (iterator.hasNext()) {
				String[] values = iterator.next();
				int nbCells = Math.min(values.length, headers.length);
				for (int i = 0; i < nbCells; i++) {
					json.put(headers[i], values[i]);
				}
			}
			return new ResponseEntity<>(json.toString(), HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}


	
	@ApiOperation("Delete File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "You do not have sufficient permission to perform this operation"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found.") })
	@DeleteMapping(value = "/deleteFile", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> deleteFile(@RequestParam Long id, @RequestParam("token") String token,
			HttpServletRequest request) {

		long userId = getCurrentUserId();
		fileService.deleteFileById(id, userId);
		return new ResponseEntity<>(new ResponseMessage(ElementConstants.DOCUMENT_DELETED_SUCCESSFULLY_MSG),
				HttpStatus.OK);
	}
	
	@ApiOperation("Download Json File")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = StreamingResponseBody.class, message = "Document Downloaded Successfully"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = "Document not found."),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = "Permission is denied or Document not found"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_DOWNLOAD_FILE_FROM_SERVER_MSG) })
	@GetMapping(value = "/downloadFileJsonFromJson", produces = { "application/json; charset=UTF-8",
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> downloadFileJsonFromJson(HttpServletResponse response, @RequestParam String fileName,
			@RequestParam("token") String token, HttpServletRequest request) throws DocNotFoundException, Exception {
		FileBst files = fileService.findFileByName(fileName);
		if (files != null) {
			byte[] fileData = files.getFile();
			String fileDataString = new String(fileData,"UTF-8");
			JSONObject fileJson= new JSONObject(fileDataString);
			return new ResponseEntity<>(fileJson.toString(), HttpStatus.OK);
		} else {
			throw new DocNotFoundException();
		}
	}

}
