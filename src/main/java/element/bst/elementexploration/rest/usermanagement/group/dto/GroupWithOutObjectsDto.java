package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel("NewGroupWithOutObjects")
public class GroupWithOutObjectsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 957321153960166215L;
	private Long userGroupId;
	private Date createdDate;
	private Date modifiedDate;
	private Integer parentGroupId;
	private String name;
	private String remarks;
	private Boolean active;
	private Long createdBy;
	private String description;

	public GroupWithOutObjectsDto() {

	}

	public GroupWithOutObjectsDto(Date createdDate, Date modifiedDate, Integer parentGroupId, String name,
			String remarks, Boolean active, Long createdBy, String description, Long userGroupId) {
		// super();
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.parentGroupId = parentGroupId;
		this.name = name;
		this.remarks = remarks;
		this.active = active;
		this.createdBy = createdBy;
		this.description = description;
		this.userGroupId = userGroupId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(Integer parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}

}
