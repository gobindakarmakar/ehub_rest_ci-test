package element.bst.elementexploration.test.extention.sourcecredibility.daoImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl.SourceJurisdictionDaoImpl;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceJurisdiction;

/**
 * @author suresh
 *
 */
public class SourceJurisdictionDaoTest {

	@Mock
	private SessionFactory sessionFactory;

	@Mock
	private Session session;

	@SuppressWarnings("rawtypes")
	@Mock
	private Query query;

	@InjectMocks
	SourceJurisdictionDaoImpl sourceJurisdictionDao;

	SourceJurisdiction sourceJurisdiction = null;
	List<SourceJurisdiction> JurisdictionList = new ArrayList<SourceJurisdiction>();

	@Before
	public void setupMock() {
		sourceJurisdiction = new SourceJurisdiction();
		sourceJurisdiction.setJurisdictionId(1L);
		sourceJurisdiction.setJurisdictionName("EU");
		SourceJurisdiction sourceJurisdictionTwo = new SourceJurisdiction();
		sourceJurisdictionTwo.setJurisdictionId(1L);
		sourceJurisdictionTwo.setJurisdictionName("IND");
		JurisdictionList.add(sourceJurisdiction);
		JurisdictionList.add(sourceJurisdictionTwo);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchJurisdiction() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(sourceJurisdiction);
		SourceJurisdiction sourcesJurisdictionSaved = sourceJurisdictionDao.fetchJurisdiction("EU");
		assertEquals(sourceJurisdiction.getJurisdictionName(), sourcesJurisdictionSaved.getJurisdictionName());
	}


	@Test
	public void fetchJurisdictionListExceptAll() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(JurisdictionList);
		List<SourceJurisdiction> JurisdictionListSaved = sourceJurisdictionDao.fetchJurisdictionExceptAll();
		assertEquals(JurisdictionListSaved.size(), JurisdictionList.size());
	}

}
