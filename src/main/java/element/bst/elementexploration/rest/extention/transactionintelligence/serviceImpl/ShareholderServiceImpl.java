package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.ShareholderDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Shareholders;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.ShareholderService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

@Service("shareholderService")
public class ShareholderServiceImpl extends GenericServiceImpl<Shareholders, Long> implements ShareholderService
{
	
	@Autowired
	public ShareholderServiceImpl(GenericDao<Shareholders, Long> genericDao) {
		super(genericDao);
	}
	
	@Autowired
	ShareholderDao shareholderDao;
	
	@Autowired
	CustomerDetailsDao customerDetailsDao;

	@Override
	@Transactional("transactionManager")
	public Boolean uoploadShareholders(MultipartFile multipartFile) throws IOException 
	{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
		String line = null;
		int i = 1;

		while ((line = br.readLine()) != null) {
			if (i != 1) {
				String[] data = null;
				if(line.contains(","))
					data = line.split(",");
				if(line.contains(";"))
					data = line.split(";");
				Shareholders shareholder = new Shareholders();
				CustomerDetails customerDetails = null;
				if(data!=null && data[1]!=null)
					customerDetails = customerDetailsDao.fetchCustomerDetails(data[1]);
				if(customerDetails!=null)
				{
				shareholder.setNumberOfShares(Integer.valueOf(data[0]));
				shareholder.setShareHolderName(data[2]);
				shareholder.setCustomerNumber((data[1]));
				shareholderDao.create(shareholder);
				}
			}
			i++;
		}
		return true;
	}

}
