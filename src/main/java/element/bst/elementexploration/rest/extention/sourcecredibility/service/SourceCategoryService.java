package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceCategoryDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface SourceCategoryService extends GenericService<SourceCategory, Long>{

	boolean checkCategoryExists(String dtoCategory);

	public List<SourceCategoryDto> getSourceCategories();
	
}
