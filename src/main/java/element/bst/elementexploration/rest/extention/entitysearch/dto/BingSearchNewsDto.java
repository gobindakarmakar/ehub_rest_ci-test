package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BingSearchNewsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("value")
	private List<BingNews> value;

	public List<BingNews> getValue() {
		return value;
	}

	public void setValue(List<BingNews> value) {
		this.value = value;
	}

}
