package element.bst.elementexploration.rest.extention.source.addToPage.daoImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseAnalystMappingDaoImpl;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.source.addToPage.dao.SourceAddToPageDao;
import element.bst.elementexploration.rest.extention.source.addToPage.domain.SourceAddToPage;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author hanuman
 *
 */
@Repository("sourceAddToPageDao")
@Transactional("transactionManager")
public class SourceAddToPageDaoImpl extends GenericDaoImpl<SourceAddToPage, Long> implements 
SourceAddToPageDao{

	public SourceAddToPageDaoImpl() {
		super(SourceAddToPage.class);
	}

	@Override
	public SourceAddToPage getSourceAddToPage(String entityId,String sourceName) {
		SourceAddToPage elementComplexStructure = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select sap from SourceAddToPage sap where sap.entityId = :entityId and sap.sourceName = :sourceName");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("entityId", entityId);
			query.setParameter("sourceName", sourceName);
			elementComplexStructure = (SourceAddToPage) query.getSingleResult();
		} catch (Exception e) {
			return elementComplexStructure;
		}
		return elementComplexStructure;
	}
	
	
	@Override
	public boolean deleteSourceAddToPage(String entityId,String sourceName) {
		try {
			String hql = "DELETE FROM SourceAddToPage sap WHERE sap.entityId = :entityId and sap.sourceName = :sourceName";
			Query<?> query = getCurrentSession().createQuery(hql);
			query.setParameter("entityId", entityId);
			query.setParameter("sourceName", sourceName);
			int result = query.executeUpdate();
			if (result > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					CaseAnalystMappingDaoImpl.class);
			return false;
		}
	}

}
