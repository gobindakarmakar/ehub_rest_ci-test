package element.bst.elementexploration.rest.util;

import java.io.Serializable;

/**
 * 
 * @author Prateek Maurya
 *
 */
public class FilterModelDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String filterModel;
	
	public String getFilterModel() {
		return filterModel;
	}

	public void setFilterModel(String filterModel) {
		this.filterModel = filterModel;
	}

}
