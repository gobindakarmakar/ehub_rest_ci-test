package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.domain.RoleGroup;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.usermanagement.group.dto.RoleGroupDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupsService;
import element.bst.elementexploration.rest.usermanagement.group.service.RoleGroupService;
import element.bst.elementexploration.rest.usermanagement.group.service.RolesService;
import element.bst.elementexploration.rest.usermanagement.group.service.UserRolesService;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Role Group API" }, description = "Manages application's role's groups")
@RestController
@RequestMapping("/api/roleGroup")
public class RoleGroupController extends BaseController {

	@Autowired
	private RoleGroupService roleGroupService;

	@Autowired
	private RolesService rolesService;

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private UserRolesService userRolesService;

	@ApiOperation("saves or updates group roles")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = GenericReturnObject.class, message = ElementConstants.ROLE_ADDED_UPDATED_TO_GROUP_SUCCESSFULLY_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.ROLE_ADDED_UPDATED_TO_GROUP_FAILED_MSG) })
	@PostMapping(value = "/saveOrRoleGroup", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrRoleGroup(@RequestBody @Valid List<RoleGroupDto> grouppermissiondtoList,
			@RequestParam("token") String token,@RequestParam Long groupId, BindingResult results, HttpServletRequest request) throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		if (!results.hasErrors()) {
			Long currentUserId = getCurrentUserId();
			List<RoleGroup> savedList = new ArrayList<>();
			if (grouppermissiondtoList != null && grouppermissiondtoList.size() > 0) {
				roleGroupService.deleteExistingRolesForGroup(groupId,getCurrentUserId());
				for (RoleGroupDto eachRoleGroup : grouppermissiondtoList) {
					if (eachRoleGroup.getGroupId() != null) {
						Groups group = groupsService.find(eachRoleGroup.getGroupId());
						if (group != null) {
							Roles role = rolesService.find(eachRoleGroup.getRoleId());
							if (role != null) {
								//roleGroupService.deleteExistingRolesForGroup(group.getId());
								if (eachRoleGroup.getRoleGroupId() != null)
									savedList.add(roleGroupService.saveOrUpdateRoleGroup(role, group,
											eachRoleGroup.getRoleGroupId(), currentUserId));
								else
									savedList.add(
											roleGroupService.saveOrUpdateRoleGroup(role, group, null, currentUserId));
							}
						}
					}
				}

				if (savedList != null && savedList.size() > 0) {
					message.setData(savedList);
					message.setStatus(ElementConstants.SUCCESS_STATUS);
					message.setResponseMessage(ElementConstants.ROLE_ADDED_UPDATED_TO_GROUP_SUCCESSFULLY_MSG);
				}
			}else {
				if(groupId!=null) {
					roleGroupService.deleteExistingRolesForGroup(groupId,getCurrentUserId());
				}
			}
		} else {
			message.setStatus(ElementConstants.ERROR_STATUS);
			message.setResponseMessage(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}

		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@ApiOperation("API to get Roles of a group")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/getRolesByGroup", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getRolesByGroup(@RequestBody List<Long> groupIds, @RequestParam String token)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<RoleGroup> roleGroupsList = roleGroupService.getRolesByGroup(groupIds);
		List<RoleGroup> filteredList = null;
		if (roleGroupsList != null && roleGroupsList.size() > 0) {
			Map<Long, RoleGroup> filteredData = new HashMap<>();
			for (RoleGroup eachRoleGroup : roleGroupsList) {
				filteredData.put(eachRoleGroup.getRoleId().getRoleId(), eachRoleGroup);
			}
			if (filteredData != null && filteredData.size() > 0) {
				filteredList = new ArrayList<>(filteredData.values());
			}
			JSONArray groupsArray = new JSONArray();
			if (filteredList != null && filteredList.size() > 0) {
				//roleGroupService.initializeGroups(filteredList);
				for (RoleGroup eachRoleGroup : filteredList) {
					Map<Long, Long> count = userRolesService.getUsersCountByRole(eachRoleGroup.getRoleId().getRoleId());
					JSONObject groupJson = new JSONObject(eachRoleGroup);
					JSONObject roleJson = null;
					if (groupJson != null && groupJson.has("roleId")) {
						roleJson = groupJson.getJSONObject("roleId");
					}
					if(groupJson.has("groupId"))
					groupJson.remove("groupId");
					if (count != null && count.size() > 0 && count.containsKey(eachRoleGroup.getRoleId().getRoleId())) {
						roleJson.put("noOfUsers", count.get(eachRoleGroup.getRoleId().getRoleId()));
					}else {
						roleJson.put("noOfUsers",0L);
					}
					groupsArray.put(groupJson);

				}
			}
			message.setResponseMessage(ElementConstants.FETCH_ROLES_SUCCESSFUL);
			message.setData(groupsArray.toString());
			message.setStatus(ElementConstants.SUCCESS_STATUS);
		} else {
			message.setResponseMessage(ElementConstants.FETCH_ROLES_UNSUCCESSFUL);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
