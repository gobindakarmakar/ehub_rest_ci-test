package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author rambabu
 *
 */
@ApiModel("Document table content")
public class ContentDataTableDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "Header of the table")
	private Boolean tableHeader;
	
	@ApiModelProperty(value = "List of header positions of the tables")
	private List<Integer> tableHeaderPostions;
	
	@ApiModelProperty(value = "Table row data")
	private List<DataTableRowsDto> dataTableRowsDto;

	public ContentDataTableDto() {
		super();
		
	}

	public ContentDataTableDto(List<DataTableRowsDto> dataTableRowsDto) {
		super();
		this.dataTableRowsDto = dataTableRowsDto;
	}

	public List<DataTableRowsDto> getDataTableRowsDto() {
		return dataTableRowsDto;
	}

	public void setDataTableRowsDto(List<DataTableRowsDto> dataTableRowsDto) {
		this.dataTableRowsDto = dataTableRowsDto;
	}

	public Boolean getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(Boolean tableHeader) {
		this.tableHeader = tableHeader;
	}

	public List<Integer> getTableHeaderPostions() {
		return tableHeaderPostions;
	}

	public void setTableHeaderPostions(List<Integer> tableHeaderPostions) {
		this.tableHeaderPostions = tableHeaderPostions;
	}

	@Override
	public String toString() {
		return "ContentDataTableDto [tableHeader=" + tableHeader + ", tableHeaderPostions=" + tableHeaderPostions
				+ ", dataTableRowsDto=" + dataTableRowsDto + "]";
	}
	
	
	
	

}
