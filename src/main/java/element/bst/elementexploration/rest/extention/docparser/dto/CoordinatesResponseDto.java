package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;

/**
 * @author Suresh
 *
 */
public class CoordinatesResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String contentType;
	private String question;
	private String questionCoordinates;
	private String answer;
	private String possibleAnswer;
	private String answerCoordinates;
	private String answerType;
	private int pageNumber;
	private Long docId;
	private String formField;
	private String subType;
	private String questionPositionOnThePage;
	private String answerPositionOnThePage;
	private String icon;
	private String questionUUID;
	private String answerUUID;
	private String elementName;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionCoordinates() {
		return questionCoordinates;
	}

	public void setQuestionCoordinates(String questionCoordinates) {
		this.questionCoordinates = questionCoordinates;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAnswerCoordinates() {
		return answerCoordinates;
	}

	public void setAnswerCoordinates(String answerCoordinates) {
		this.answerCoordinates = answerCoordinates;
	}

	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getPossibleAnswer() {
		return possibleAnswer;
	}

	public void setPossibleAnswer(String possibleAnswer) {
		this.possibleAnswer = possibleAnswer;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFormField() {
		return formField;
	}

	public void setFormField(String formField) {
		this.formField = formField;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getQuestionPositionOnThePage() {
		return questionPositionOnThePage;
	}

	public void setQuestionPositionOnThePage(String questionPositionOnThePage) {
		this.questionPositionOnThePage = questionPositionOnThePage;
	}

	public String getAnswerPositionOnThePage() {
		return answerPositionOnThePage;
	}

	public void setAnswerPositionOnThePage(String answerPositionOnThePage) {
		this.answerPositionOnThePage = answerPositionOnThePage;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getQuestionUUID() {
		return questionUUID;
	}

	public void setQuestionUUID(String questionUUID) {
		this.questionUUID = questionUUID;
	}

	public String getAnswerUUID() {
		return answerUUID;
	}

	public void setAnswerUUID(String answerUUID) {
		this.answerUUID = answerUUID;
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

}
