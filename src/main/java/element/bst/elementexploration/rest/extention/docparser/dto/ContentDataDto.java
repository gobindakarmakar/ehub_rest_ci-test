package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Document content")
public class ContentDataDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Question in the document")
	private String question;
	@ApiModelProperty(value = "Answer in th document")
	private String answer;
	@ApiModelProperty(value = "Normalized question in the document")
	private String normalizedQuestion;
	@ApiModelProperty(value = "ID of the question")
	private Long questionId;
	@ApiModelProperty(value = "ID if the answer")
	private Long answerId;
	@ApiModelProperty(value = "Text in the document")
	private String text;
	@ApiModelProperty(value = "Type of the answer")
	private AnswerType answerType;
	@ApiModelProperty(value = "Possible answers for the question")
	private String possibleAnswer;
	@ApiModelProperty(value = "Risk score of the question")
	private Integer riskScore;
	@ApiModelProperty(value = "Primary ISO codes for the question")
	private String primaryIsoCode;
	@ApiModelProperty(value = "Secondary ISO codes for the question")
	private String secondaryIsoCodes;
	@ApiModelProperty(value = "Topic")
	private String topic;
	@ApiModelProperty(value = "Evaluation")
	private String evaluation;
	@ApiModelProperty(value = "Level1 Iso code")
	private String level1Code;
	@ApiModelProperty(value = "Level1 title")
	private String level1Title;
	@ApiModelProperty(value = "Level3 title")
	private String level3Title;
	@ApiModelProperty(value = "Level3 description")
	private String level3Description;
	@ApiModelProperty(value = "Actual answer")
	private String actualAnswer;
	@ApiModelProperty(value = "Modefied by")
	private String modefiedBy;
	@ApiModelProperty(value = "Answer updated time")
	private Date answerUpdatedOn;
	@ApiModelProperty(value = "Answer ranking")
	private Integer answerRanking;
	@ApiModelProperty(value = "OSINT")
	private Boolean isOSINT;
	@ApiModelProperty(value = "Show in UI")
	private Boolean showINUI;
	@ApiModelProperty(value = "Top events")
	private List<TopEventsDto> topEvents;
	@ApiModelProperty(value = "Rank updated time")
	private Date rankUpdatedOn;
	@ApiModelProperty(value = "Rank modefied by")
	private String rankModefiedBy;

	@ApiModelProperty(value = "Source Urls Answer")
	private String sourceUrls;
	@ApiModelProperty(value = "Table")
	private TableDto table;
	@ApiModelProperty(value = "Has iso code")
	private boolean hasIsoCode;
	@ApiModelProperty(value = "question position")
	private String questionPosition;
	@ApiModelProperty(value = "answer position")
	private String answerPosition;
	@ApiModelProperty(value = "possible answers dto")
	private List<PossibleAnswersDto> possibleAnswersDto;

	@ApiModelProperty(value = "possible answers dto")
	private List<PossibleAnswersDto> specifiedPossibleAnswersDto;

	@ApiModelProperty(value = "Page Number ")
	private int pageNumber;
	@ApiModelProperty(value = "Question form field ")
	private String formField;
	@ApiModelProperty(value = "Answer sub type")
	private String subType;
	@ApiModelProperty(value = "Specified Question Position")
	private String specifiedQuestionPosition;
	@ApiModelProperty(value = "Specified Answer Position")
	private String specifiedAnswerPosition;
	@ApiModelProperty(value = "Specified Question")
	private String specifiedQuestion;
	@ApiModelProperty(value = "Specified Answer")
	private String specifiedAnswer;
	@ApiModelProperty(value = "Specified Answer Type")
	private AnswerType specifiedAnswerType;
	@ApiModelProperty(value = "Specified Question Id")
	private long specifiedQuestionId;
	@ApiModelProperty(value = "Specified Answer Id")
	private long specifiedAnswerId;
	@ApiModelProperty(value = "Question Position On Page")
	private String questionPositionOnThePage;
	@ApiModelProperty(value = "Answer Position On Page")
	private String answerPositionOnThePage;
	@ApiModelProperty(value = "Specified Question Position On Page")
	private String specifiedQuestionPositionOnThePage;
	@ApiModelProperty(value = "Specified Answer Position On Page")
	private String specifiedAnswerPositionOnThePage;
	@ApiModelProperty(value = "Question UUID")
	private String questionUUID;
	@ApiModelProperty(value = "Answer UUID")
	private String answerUUID;
	@ApiModelProperty(value = "Specified Question UUID")
	private String specifiedQuestionUUID;
	@ApiModelProperty(value = "Specified Answer UUID")
	private String specifiedAnswerUUID;
	@ApiModelProperty(value = "Alternative name for Question")
	private String elementName;
	@ApiModelProperty(value = "Delete Question")
	private Boolean questionStatus;

	public ContentDataDto() {
		super();
	}

	public ContentDataDto(String question, String answer, String normalizedQuestion, Long questionId, Long answerId,
			String text, AnswerType answerType, String possibleAnswer, Integer riskScore, String primaryIsoCode,
			String secondaryIsoCodes) {
		super();
		this.question = question;
		this.answer = answer;
		this.normalizedQuestion = normalizedQuestion;
		this.questionId = questionId;
		this.answerId = answerId;
		this.text = text;
		this.answerType = answerType;
		this.possibleAnswer = possibleAnswer;
		this.riskScore = riskScore;
		this.primaryIsoCode = primaryIsoCode;
		this.secondaryIsoCodes = secondaryIsoCodes;

	}

	public ContentDataDto(String question, String answer, String normalizedQuestion, Long questionId, Long answerId,
			String text, AnswerType answerType, String possibleAnswer, Integer riskScore, String primaryIsoCode,
			String secondaryIsoCodes, String topic, String evaluation, String level1Code, String level1Title,
			String level3Title, String level3Description, String actualAnswer, String modefiedBy, Date answerUpdatedOn,
			Integer answerRanking, Boolean isOSINT, Boolean showINUI, List<TopEventsDto> topEvents, Date rankUpdatedOn,
			String rankModefiedBy, String sourceUrls, TableDto table, boolean hasIsoCode, String questionPosition,
			String answerPosition, List<PossibleAnswersDto> possibleAnswersDto,
			List<PossibleAnswersDto> specifiedPossibleAnswersDto, int pageNumber, String formField, String subType,
			String specifiedQuestionPosition, String specifiedAnswerPosition, String specifiedQuestion,
			String specifiedAnswer, AnswerType specifiedAnswerType, long specifiedQuestionId, long specifiedAnswerId,
			String questionPositionOnThePage, String answerPositionOnThePage, String specifiedQuestionPositionOnThePage,
			String specifiedAnswerPositionOnThePage, String questionUUID, String answerUUID) {
		super();
		this.question = question;
		this.answer = answer;
		this.normalizedQuestion = normalizedQuestion;
		this.questionId = questionId;
		this.answerId = answerId;
		this.text = text;
		this.answerType = answerType;
		this.possibleAnswer = possibleAnswer;
		this.riskScore = riskScore;
		this.primaryIsoCode = primaryIsoCode;
		this.secondaryIsoCodes = secondaryIsoCodes;
		this.topic = topic;
		this.evaluation = evaluation;
		this.level1Code = level1Code;
		this.level1Title = level1Title;
		this.level3Title = level3Title;
		this.level3Description = level3Description;
		this.actualAnswer = actualAnswer;
		this.modefiedBy = modefiedBy;
		this.answerUpdatedOn = answerUpdatedOn;
		this.answerRanking = answerRanking;
		this.isOSINT = isOSINT;
		this.showINUI = showINUI;
		this.topEvents = topEvents;
		this.rankUpdatedOn = rankUpdatedOn;
		this.rankModefiedBy = rankModefiedBy;
		this.sourceUrls = sourceUrls;
		this.table = table;
		this.hasIsoCode = hasIsoCode;
		this.questionPosition = questionPosition;
		this.answerPosition = answerPosition;
		this.possibleAnswersDto = possibleAnswersDto;
		this.specifiedPossibleAnswersDto = specifiedPossibleAnswersDto;
		this.pageNumber = pageNumber;
		this.formField = formField;
		this.subType = subType;
		this.specifiedQuestionPosition = specifiedQuestionPosition;
		this.specifiedAnswerPosition = specifiedAnswerPosition;
		this.specifiedQuestion = specifiedQuestion;
		this.specifiedAnswer = specifiedAnswer;
		this.specifiedAnswerType = specifiedAnswerType;
		this.specifiedQuestionId = specifiedQuestionId;
		this.specifiedAnswerId = specifiedAnswerId;
		this.questionPositionOnThePage = questionPositionOnThePage;
		this.answerPositionOnThePage = answerPositionOnThePage;
		this.specifiedQuestionPositionOnThePage = specifiedQuestionPositionOnThePage;
		this.specifiedAnswerPositionOnThePage = specifiedAnswerPositionOnThePage;
		this.questionUUID = questionUUID;
		this.answerUUID = answerUUID;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getQuestionPosition() {
		return questionPosition;
	}

	public void setQuestionPosition(String questionPosition) {
		this.questionPosition = questionPosition;
	}

	public String getAnswerPosition() {
		return answerPosition;
	}

	public void setAnswerPosition(String answerPosition) {
		this.answerPosition = answerPosition;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getNormalizedQuestion() {
		return normalizedQuestion;
	}

	public void setNormalizedQuestion(String normalizedQuestion) {
		this.normalizedQuestion = normalizedQuestion;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public AnswerType getAnswerType() {
		return answerType;
	}

	public void setAnswerType(AnswerType answerType) {
		this.answerType = answerType;
	}

	public String getPossibleAnswer() {
		return possibleAnswer;
	}

	public void setPossibleAnswer(String possibleAnswer) {
		this.possibleAnswer = possibleAnswer;
	}

	public Integer getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}

	public String getPrimaryIsoCode() {
		return primaryIsoCode;
	}

	public void setPrimaryIsoCode(String primaryIsoCode) {
		this.primaryIsoCode = primaryIsoCode;
	}

	public String getSecondaryIsoCodes() {
		return secondaryIsoCodes;
	}

	public void setSecondaryIsoCodes(String secondaryIsoCodes) {
		this.secondaryIsoCodes = secondaryIsoCodes;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}

	public String getLevel1Code() {
		return level1Code;
	}

	public void setLevel1Code(String level1Code) {
		this.level1Code = level1Code;
	}

	public String getLevel1Title() {
		return level1Title;
	}

	public void setLevel1Title(String level1Title) {
		this.level1Title = level1Title;
	}

	public String getLevel3Title() {
		return level3Title;
	}

	public void setLevel3Title(String level3Title) {
		this.level3Title = level3Title;
	}

	public String getLevel3Description() {
		return level3Description;
	}

	public void setLevel3Description(String level3Description) {
		this.level3Description = level3Description;
	}

	public String getActualAnswer() {
		return actualAnswer;
	}

	public void setActualAnswer(String actualAnswer) {
		this.actualAnswer = actualAnswer;
	}

	public String getModefiedBy() {
		return modefiedBy;
	}

	public void setModefiedBy(String modefiedBy) {
		this.modefiedBy = modefiedBy;
	}

	public Date getAnswerUpdatedOn() {
		return answerUpdatedOn;
	}

	public void setAnswerUpdatedOn(Date answerUpdatedOn) {
		this.answerUpdatedOn = answerUpdatedOn;
	}

	public Integer getAnswerRanking() {
		return answerRanking;
	}

	public void setAnswerRanking(Integer answerRanking) {
		this.answerRanking = answerRanking;
	}

	public Boolean getIsOSINT() {
		return isOSINT;
	}

	public void setIsOSINT(Boolean isOSINT) {
		this.isOSINT = isOSINT;
	}

	public Boolean getShowINUI() {
		return showINUI;
	}

	public void setShowINUI(Boolean showINUI) {
		this.showINUI = showINUI;
	}

	public List<TopEventsDto> getTopEvents() {
		return topEvents;
	}

	public void setTopEvents(List<TopEventsDto> topEvents) {
		this.topEvents = topEvents;
	}

	public Date getRankUpdatedOn() {
		return rankUpdatedOn;
	}

	public void setRankUpdatedOn(Date rankUpdatedOn) {
		this.rankUpdatedOn = rankUpdatedOn;
	}

	public String getRankModefiedBy() {
		return rankModefiedBy;
	}

	public void setRankModefiedBy(String rankModefiedBy) {
		this.rankModefiedBy = rankModefiedBy;
	}

	public String getSpecifiedQuestion() {
		return specifiedQuestion;
	}

	public void setSpecifiedQuestion(String specifiedQuestion) {
		this.specifiedQuestion = specifiedQuestion;
	}

	public String getSpecifiedAnswer() {
		return specifiedAnswer;
	}

	public void setSpecifiedAnswer(String specifiedAnswer) {
		this.specifiedAnswer = specifiedAnswer;
	}

	public long getSpecifiedQuestionId() {
		return specifiedQuestionId;
	}

	public void setSpecifiedQuestionId(long specifiedQuestionId) {
		this.specifiedQuestionId = specifiedQuestionId;
	}

	public long getSpecifiedAnswerId() {
		return specifiedAnswerId;
	}

	public void setSpecifiedAnswerId(long specifiedAnswerId) {
		this.specifiedAnswerId = specifiedAnswerId;
	}

	public String getSourceUrls() {
		return sourceUrls;
	}

	public void setSourceUrls(String sourceUrls) {
		this.sourceUrls = sourceUrls;
	}

	public TableDto getTable() {
		return table;
	}

	public void setTable(TableDto table) {
		this.table = table;
	}

	public boolean isHasIsoCode() {
		return hasIsoCode;
	}

	public void setHasIsoCode(boolean hasIsoCode) {
		this.hasIsoCode = hasIsoCode;
	}

	public List<PossibleAnswersDto> getPossibleAnswersDto() {
		return possibleAnswersDto;
	}

	public void setPossibleAnswersDto(List<PossibleAnswersDto> possibleAnswersDto) {
		this.possibleAnswersDto = possibleAnswersDto;
	}

	public AnswerType getSpecifiedAnswerType() {
		return specifiedAnswerType;
	}

	public void setSpecifiedAnswerType(AnswerType specifiedAnswerType) {
		this.specifiedAnswerType = specifiedAnswerType;
	}

	public List<PossibleAnswersDto> getSpecifiedPossibleAnswersDto() {
		return specifiedPossibleAnswersDto;
	}

	public void setSpecifiedPossibleAnswersDto(List<PossibleAnswersDto> specifiedPossibleAnswersDto) {
		this.specifiedPossibleAnswersDto = specifiedPossibleAnswersDto;
	}

	public String getFormField() {
		return formField;
	}

	public void setFormField(String formField) {
		this.formField = formField;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getSpecifiedQuestionPosition() {
		return specifiedQuestionPosition;
	}

	public void setSpecifiedQuestionPosition(String specifiedQuestionPosition) {
		this.specifiedQuestionPosition = specifiedQuestionPosition;
	}

	public String getSpecifiedAnswerPosition() {
		return specifiedAnswerPosition;
	}

	public void setSpecifiedAnswerPosition(String specifiedAnswerPosition) {
		this.specifiedAnswerPosition = specifiedAnswerPosition;
	}

	public String getQuestionPositionOnThePage() {
		return questionPositionOnThePage;
	}

	public void setQuestionPositionOnThePage(String questionPositionOnThePage) {
		this.questionPositionOnThePage = questionPositionOnThePage;
	}

	public String getAnswerPositionOnThePage() {
		return answerPositionOnThePage;
	}

	public void setAnswerPositionOnThePage(String answerPositionOnThePage) {
		this.answerPositionOnThePage = answerPositionOnThePage;
	}

	public String getSpecifiedQuestionPositionOnThePage() {
		return specifiedQuestionPositionOnThePage;
	}

	public void setSpecifiedQuestionPositionOnThePage(String specifiedQuestionPositionOnThePage) {
		this.specifiedQuestionPositionOnThePage = specifiedQuestionPositionOnThePage;
	}

	public String getSpecifiedAnswerPositionOnThePage() {
		return specifiedAnswerPositionOnThePage;
	}

	public void setSpecifiedAnswerPositionOnThePage(String specifiedAnswerPositionOnThePage) {
		this.specifiedAnswerPositionOnThePage = specifiedAnswerPositionOnThePage;
	}

	public String getQuestionUUID() {
		return questionUUID;
	}

	public void setQuestionUUID(String questionUUID) {
		this.questionUUID = questionUUID;
	}

	public String getAnswerUUID() {
		return answerUUID;
	}

	public void setAnswerUUID(String answerUUID) {
		this.answerUUID = answerUUID;
	}

	public String getSpecifiedQuestionUUID() {
		return specifiedQuestionUUID;
	}

	public void setSpecifiedQuestionUUID(String specifiedQuestionUUID) {
		this.specifiedQuestionUUID = specifiedQuestionUUID;
	}

	public String getSpecifiedAnswerUUID() {
		return specifiedAnswerUUID;
	}

	public void setSpecifiedAnswerUUID(String specifiedAnswerUUID) {
		this.specifiedAnswerUUID = specifiedAnswerUUID;
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	public Boolean getQuestionStatus() {
		return questionStatus;
	}

	public void setQuestionStatus(Boolean questionStatus) {
		this.questionStatus = questionStatus;
	}

}