package element.bst.elementexploration.rest.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.context.annotation.Configuration;

@Configuration
public class ElementBasicUtil {

	public boolean validateCaseSeedSearchKeyword(String caseSearchKeyword){
		return caseSearchKeyword == null ? false : caseSearchKeyword.trim().length() > 4 ? true : false;
	}
	
	public boolean validateCaseSeedSearchKeywordForStatus(Integer status) {
		return status > 0;
	}
	public boolean validateSearchKeyword(String searchKeyword){
		return searchKeyword == null ? false : searchKeyword.trim().length() > 4 ? true : false;
	}
	
	public static Date convertToDateddMMyyyy(String date) throws ParseException {
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		if (date != null) {
			return format.parse(date);
		} else {
			throw new NullPointerException();
		}

	}
	
	public static Date convertToDateyyyyMMdd(String date) throws ParseException {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (date != null) {
			return format.parse(date);
		} else {
			throw new NullPointerException();
		}

	}
}