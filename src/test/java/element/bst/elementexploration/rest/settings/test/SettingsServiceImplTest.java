package element.bst.elementexploration.rest.settings.test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import element.bst.elementexploration.rest.settings.dto.SystemSettingsDto;
import element.bst.elementexploration.rest.settings.dto.SystemSettingsRequestDto;
import element.bst.elementexploration.rest.settings.service.SettingsService;


public class SettingsServiceImplTest {
	

	@Mock
	SettingsService settingsServiceMock;

	SystemSettingsDto systemSettingsDto;
	
	Map<String,String> settings;

	@Before
	public void setup() {
		systemSettingsDto = new SystemSettingsDto();
		settings = new HashMap<String,String>();
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void fetchSystemSettingsTest() {
		Mockito.when(settingsServiceMock.fetchSystemSettings(Mockito.anyString())).thenReturn(systemSettingsDto);
		SystemSettingsDto dtoResult = settingsServiceMock.fetchSystemSettings(Mockito.anyString());
		Assert.assertNotNull(dtoResult);		
	}
	
	@Test
	public void updateSystemSettingsTest() {
		Mockito.when(settingsServiceMock.updateSystemSettings(Mockito.any(SystemSettingsRequestDto.class))).thenReturn(true);
		boolean status = settingsServiceMock.updateSystemSettings(Mockito.any(SystemSettingsRequestDto.class));
		assertEquals(true, status);	
	}
	
	@Test
	public void getDeaultUrlTest() {
		Mockito.when(settingsServiceMock.getDeaultUrl()).thenReturn("test url");
		String url = settingsServiceMock.getDeaultUrl();
		Assert.assertNotNull(url);
	}
	
	@Test
	public void getMailSettingsTest() {
		Mockito.when(settingsServiceMock.getMailSettings()).thenReturn(settings);
		Map<String,String> newSettings = settingsServiceMock.getMailSettings();
		Assert.assertNotNull(newSettings);
	}
	
	

}
