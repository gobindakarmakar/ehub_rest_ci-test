package element.bst.elementexploration.rest.usermanagement.user.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.BadRequestException;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.dto.ProfileDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UserRegistrationDto;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersListDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.PaginationInformation;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Amit Patel
 *
 */
@Api(tags = { "User API" }, description = "Manages users of the application")
@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@ApiOperation("Register a user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_REGISTRATION_DISABLED),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Username or email already exists"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/registerByUser", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleRegistrationByUser(@Valid @RequestBody UserRegistrationDto userRegistrationDto,@RequestParam("token") String token
			,BindingResult results, HttpServletRequest request) {
		User user = new User();
		BeanUtils.copyProperties(userRegistrationDto, user);
		// Load properties file.
		Properties prop = userService.getProperties();

		// Check if registration by user is enabled.
		if (prop != null
				&& ElementConstants.ENABLED_USER_REGISTRATION == Integer.valueOf(prop.getProperty("enable", "0"))) {
			if (!results.hasErrors()) {
				UserDto existingUser = userService.getUserByEmailOrUsername(user.getScreenName());
				if (existingUser == null) {
					UserDto checkUserByEmailAddress = userService.getUserByEmailOrUsername(user.getEmailAddress());
					if (checkUserByEmailAddress == null) {
						user.setCreatedDate(new Date());
						user.setModifiedDate(null);
						user.setPassword(passwordEncoder.encode(user.getPassword()));
						user.setStatus(prop.getProperty("status", "0"));
						user = userService.save(user);

						String url = new String(request.getScheme() + "://" + request.getLocalName() + ":"
								+ request.getLocalPort() + "/");

						// Send email verification link.
						if (ElementConstants.ENABLED_USER_REGISTRATION == Integer
								.valueOf(prop.getProperty("emailVerification", "0"))) {
							userService.sendEmailVerification(user.getUserId(), user.getScreenName(), url, "token",
									user.getEmailAddress());
						}
						eventPublisher.publishEvent(new LoggingEvent(user, LoggingEventType.USER_REGISTRATION,
								user.getUserId(), new Date()));
						return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_CREATION_SUCCESSFUL),
								HttpStatus.OK);
					} else {
						return new ResponseEntity<>(
								new ResponseMessage(
										ElementConstants.EMAILADDRESS_ALREADY_EXISTS + user.getEmailAddress()),
								HttpStatus.BAD_REQUEST);
					}
				} else {
					return new ResponseEntity<>(
							new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + user.getScreenName()),
							HttpStatus.BAD_REQUEST);
				}
			} else {
				throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
			}
		} else {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_REGISTRATION_DISABLED),
					HttpStatus.UNAUTHORIZED);
		}
	}

	
	@ApiOperation("Register a List of users")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_CREATION_SUCCESSFUL),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_REGISTRATION_DISABLED),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = "Username or email already exists"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/registerByUsers", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleRegistrationByUsersList(@Valid @RequestBody ArrayList<UserRegistrationDto> userRegistrationDtos,
			BindingResult results, HttpServletRequest request) {
		User user = new User();


		if(userRegistrationDtos.size()<=0) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_NOT_FOUND),
					HttpStatus.OK);
		}

		for(UserRegistrationDto userRegistrationDto: userRegistrationDtos) {
			BeanUtils.copyProperties(userRegistrationDto, user);

			// Load properties file.
			Properties prop = userService.getProperties();

			// Check if registration by user is enabled.
			if (prop != null
					&& ElementConstants.ENABLED_USER_REGISTRATION == Integer.valueOf(prop.getProperty("enable", "0"))) {
				if (!results.hasErrors()) {
					UserDto existingUser = userService.getUserByEmailOrUsername(user.getScreenName());
					if (existingUser == null) {
						UserDto checkUserByEmailAddress = userService.getUserByEmailOrUsername(user.getEmailAddress());
						if (checkUserByEmailAddress == null) {
							user.setCreatedDate(new Date());
							user.setModifiedDate(null);
							user.setPassword(passwordEncoder.encode(user.getPassword()));
							user.setStatus(prop.getProperty("status", "0"));
							user = userService.save(user);

							String url = new String(request.getScheme() + "://" + request.getLocalName() + ":"
									+ request.getLocalPort() + "/");

							// Send email verification link.
							if (ElementConstants.ENABLED_USER_REGISTRATION == Integer
									.valueOf(prop.getProperty("emailVerification", "0"))) {
								userService.sendEmailVerification(user.getUserId(), user.getScreenName(), url, "token",
										user.getEmailAddress());
							}
							eventPublisher.publishEvent(new LoggingEvent(user, LoggingEventType.USER_REGISTRATION,
									user.getUserId(), new Date()));

						} else {
							return new ResponseEntity<>(
									new ResponseMessage(
											ElementConstants.EMAILADDRESS_ALREADY_EXISTS + user.getEmailAddress()),
									HttpStatus.BAD_REQUEST);
						}
					} else {
						return new ResponseEntity<>(
								new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + user.getScreenName()),
								HttpStatus.BAD_REQUEST);
					}
				} else {
					throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
				}
			} else {
				return new ResponseEntity<>(new ResponseMessage(ElementConstants.USER_REGISTRATION_DISABLED),
						HttpStatus.UNAUTHORIZED);
			}
		}
		return new ResponseEntity<>(new ResponseMessage("Total "+userRegistrationDtos.size()+" "+ElementConstants.USERS_CREATION_SUCCESSFUL),
				HttpStatus.OK);
	}
	
	@ApiOperation("Gets all Users")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersListDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserListing", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserList(@RequestParam("token") String token,
			@RequestParam(required = false) String status, @RequestParam(required = false) Long groupId,
			@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer recordsPerPage,
			@RequestParam(required = false) String orderBy, @RequestParam(required = false) String orderIn,
			HttpServletRequest request) {
		List<UserDto> userList = userService.getUserList(groupId, status, pageNumber, recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = userService.countUserList(groupId, status);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(userList != null ? userList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		UsersListDto userListDto = new UsersListDto();
		userListDto.setResult(userList);
		userListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * userList); jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userListDto, HttpStatus.OK);
	}

	@ApiOperation("Search all Users")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UsersListDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/fullTextSearch", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserListFullTextSearch(@RequestParam("token") String token,
			@RequestParam("keyword") String keyword, @RequestParam(required = false) String status,
			@RequestParam(required = false) Long groupId, @RequestParam(required = false) Integer pageNumber,
			@RequestParam(required = false) Integer recordsPerPage, @RequestParam(required = false) String orderBy,
			@RequestParam(required = false) String orderIn, HttpServletRequest request) {

		if (StringUtils.isEmpty(keyword) || keyword.length() < 3) {
			return new ResponseEntity<>(new ResponseMessage(ElementConstants.SEARCH_STRING_LENTH_INVALID),
					HttpStatus.OK);
		}
		List<UserDto> userList = userService.getUserListFullTextSearch(keyword, status, groupId, pageNumber,
				recordsPerPage, orderBy, orderIn);

		// Pagination
		int pageSize = recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage;
		int index = pageNumber != null ? pageNumber : 1;
		long totalResults = userService.countUserListFullTextSearch(keyword, status, groupId);
		PaginationInformation information = new PaginationInformation();
		information.setTotalResults(totalResults);
		information.setTitle(request.getRequestURI());
		information.setKind("list");
		information.setCount(userList != null ? userList.size() : 0);
		information.setIndex(index);
		int nextIndex = (int) (totalResults - (pageSize * index)) > 0 ? index + 1 : 0;
		information.setStartIndex(nextIndex);
		information.setInputEncoding("utf-8");
		information.setOutputEncoding("utf-8");
		UsersListDto userListDto = new UsersListDto();
		userListDto.setResult(userList);
		userListDto.setPaginationInformation(information);
		/*
		 * JSONObject jsonObject = new JSONObject(); jsonObject.put("result",
		 * userList); jsonObject.put("paginationInformation", information);
		 */
		return new ResponseEntity<>(userListDto, HttpStatus.OK);
	}

	@ApiOperation("Gets user profile")
	@ApiResponses(value = { @ApiResponse(code = 200, response = UserDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = String.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getUserProfile", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getUserProfile(@RequestParam("token") String token, HttpServletRequest request)
			throws IllegalAccessException, InvocationTargetException {

		UserDto userDto = new UserDto();
		Long userId = getCurrentUserId();
		User user = userService.find(userId);

		BeanUtils.copyProperties(user, userDto);
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}

	@ApiOperation("Updates user profile")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.CHANGE_PASSWORD_FAIL_MSG),
			@ApiResponse(code = 400, response = ResponseMessage.class, message = ElementConstants.MISSING_REQUIRED_FIELDS_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateProfile", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> handleUpdateUser(@RequestParam("token") String token,
			@Valid @RequestBody ProfileDto updatedUser, BindingResult results, HttpServletRequest request)
			throws Exception {

		if (!results.hasErrors()) {
			Long userId = getCurrentUserId();
			User user = userService.find(userId);
			UserDto checkUserByUserName = userService.checkUserNameOrEmailExists(updatedUser.getScreenName(),
					updatedUser.getUserId());
			if (checkUserByUserName == null) {
				UserDto checkUserByEmailAddress = userService.checkUserNameOrEmailExists(updatedUser.getEmailAddress(),
						updatedUser.getUserId());
				if (checkUserByEmailAddress == null) {
					// Set user updated record
					user.setEmailAddress(updatedUser.getEmailAddress());
					user.setFirstName(updatedUser.getFirstName());
					user.setLastName(updatedUser.getLastName());
					user.setMiddleName(updatedUser.getMiddleName());
					user.setScreenName(updatedUser.getScreenName());
					user.setDob(updatedUser.getDob());
					user.setCountry(updatedUser.getCountry());
					user.setModifiedDate(new Date());
					if (updatedUser.getOldPassword() != null && updatedUser.getNewPassword() != null
							&& updatedUser.getRetypePassword() != null) {
						if ((updatedUser.getNewPassword().equals(updatedUser.getRetypePassword()))
								&& (passwordEncoder.matches(updatedUser.getOldPassword(), user.getPassword()))) {
							user.setPassword(passwordEncoder.encode(updatedUser.getNewPassword()));
						} else {
							return new ResponseEntity<>(new ResponseMessage(ElementConstants.CHANGE_PASSWORD_FAIL_MSG),
									HttpStatus.CONFLICT);
						}
					}
					userService.saveOrUpdate(user);

					return new ResponseEntity<>(
							new ResponseMessage(ElementConstants.USER_PROFILE_UPDATED_SUCCESSFULLY_MSG), HttpStatus.OK);
				} else {
					return new ResponseEntity<>(
							new ResponseMessage(
									ElementConstants.EMAILADDRESS_ALREADY_EXISTS + updatedUser.getEmailAddress()),
							HttpStatus.CONFLICT);
				}
			} else {
				return new ResponseEntity<>(
						new ResponseMessage(ElementConstants.USERNAME_ALREADY_EXISTS + updatedUser.getScreenName()),
						HttpStatus.CONFLICT);
			}
		} else {
			throw new BadRequestException(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
		}
	}

}
