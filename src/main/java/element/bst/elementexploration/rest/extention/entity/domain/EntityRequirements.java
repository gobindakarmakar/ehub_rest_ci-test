package element.bst.elementexploration.rest.extention.entity.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "entity_requirements")
public class EntityRequirements implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long requirement_id;
	
	@Column(name = "requirements")
	private String entityRequirements;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "type_id")
	private EntityType type;

	public EntityRequirements() {
	}

	public EntityRequirements(long requirement_id, String entityRequirements, EntityType type) {
		super();
		this.requirement_id = requirement_id;
		this.entityRequirements = entityRequirements;
		this.type = type;
	}

	
	public long getRequirement_id() {
		return requirement_id;
	}

	public void setRequirement_id(long requirement_id) {
		this.requirement_id = requirement_id;
	}

	public String getEntityRequirements() {
		return entityRequirements;
	}

	public void setEntityRequirements(String entityRequirements) {
		this.entityRequirements = entityRequirements;
	}

	public EntityType getType() {
		return type;
	}

	public void setType(EntityType type) {
		this.type = type;
	}

}
