package element.bst.elementexploration.rest.extention.insuranceassistant.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.insuranceassistant.service.InsuranceAssistantService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("insuranceAsistantService")
public class InsuranceAssistantServiceImpl implements InsuranceAssistantService {

	@Value("${insuranceassist_url}")
	private String INSURANCEASSIT_API;

	@Override
	public String createNewDialog(String dialogRequest) throws Exception {
		String url = INSURANCEASSIT_API + "/dialog";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, dialogRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String proposalRequest(String proposalRequest, String dialogId) throws Exception {
		String idEncode = java.net.URLEncoder.encode(dialogId, "UTF-8").replaceAll("\\+", "%20");
		String url = INSURANCEASSIT_API + "/dialog/" + idEncode + "/proposal";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, proposalRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

	@Override
	public String respondMessageDialog(String dialogRequest, String dialogId) throws Exception {
		String idEncode = java.net.URLEncoder.encode(dialogId, "UTF-8").replaceAll("\\+", "%20");
		String url = INSURANCEASSIT_API + "/dialog/" + idEncode + "/message";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, dialogRequest);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}
}
