package element.bst.elementexploration.rest.extention.alert.feedmanagement.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.alertmanagement.domain.Alerts;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author hanuman
 *
 */
@ApiModel("Feed Management")
@Entity
@Table(name = "bst_am_feed_management")
public class FeedManagement implements Serializable {

	private static final long serialVersionUID = 8250206122259177304L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long feed_management_id;

	@ApiModelProperty(value = "name of the feed")
	@Column(name = "feed_name",unique = true)
	private String feedName;

	@ApiModelProperty(value = "color of the feed management")
	@Column(name = "color", columnDefinition = "VARCHAR(10)")
	private String color;

	@ApiModelProperty(value = "type of the feed management")
	@Column(name = "type")
	private Integer type;	

	@ApiModelProperty(value = "source of the feed management")
	@Column(name = "source")
	private Integer source;

	/*@ManyToOne
	@JoinColumn(name = "alert_id")
	private Alerts alertId;*/
	
	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "feedId")
	private List<FeedGroups> groupLevels;
	
	@Column(name = "is_reviewer_required")
	private Boolean isReviewerRequired;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "bst_am_feed_review_status", joinColumns = { @JoinColumn(name = "feed_management_id") }, inverseJoinColumns = {
			@JoinColumn(name = "status_id") })
	private List<ListItem> reviewer;
	/*@ApiModelProperty(value = "Assigned Alerts of the feed management")
	@Column(name = "assigned_alerts")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "alertId")
	private List<Alerts> assignedAlerts;*/

	/*@JsonIgnore
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "bst_am_feed_groups", joinColumns = { @JoinColumn(name = "feed_id") }, inverseJoinColumns = {
			@JoinColumn(name = "group_id") })
	private List<Groups> groupLevels;*/
	
	/*@ApiModelProperty(value = "history of the feed management")
	@Column(name = "history")
	private String history;*/

	public String getFeedName() {
		return feedName;
	}

	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}

	public Long getFeed_management_id() {
		return feed_management_id;
	}

	public void setFeed_management_id(Long feed_management_id) {
		this.feed_management_id = feed_management_id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public List<FeedGroups> getGroupLevels() {
		return groupLevels;
	}

	public void setGroupLevels(List<FeedGroups> groupLevels) {
		this.groupLevels = groupLevels;
	}

	public Boolean getIsReviewerRequired() {
		return isReviewerRequired;
	}

	public void setIsReviewerRequired(Boolean isReviewerRequired) {
		this.isReviewerRequired = isReviewerRequired;
	}

	public List<ListItem> getReviewer() {
		return reviewer;
	}

	public void setReviewer(List<ListItem> reviewer) {
		this.reviewer = reviewer;
	}

	/*public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}*/

	/*public List<Groups> getGroupLevels() {
		return groupLevels;
	}

	public void setGroupLevels(List<Groups> groupLevels) {
		this.groupLevels = groupLevels;
	}*/

}
