package element.bst.elementexploration.rest.document.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.document.dao.DocumentMappingDao;
import element.bst.elementexploration.rest.document.domain.DocumentVaultUserMapping;
import element.bst.elementexploration.rest.document.dto.DocumentUserMappingDTO;
import element.bst.elementexploration.rest.document.enumType.ReadWritePermissionEnum;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * 
 * @author Viswanath
 *
 */
@Repository("documentMappingDao")
public class DocumentMappingDaoImpl extends GenericDaoImpl<DocumentVaultUserMapping, Long>
		implements DocumentMappingDao {

	public DocumentMappingDaoImpl() {
		super(DocumentVaultUserMapping.class);
	}

	/**
	 * This method will return the true if user has the Read or Write permission
	 * else return the false
	 */
	@Override
	public Boolean isPermissionWrite(Long userId, Long docId) {
		Boolean flag = false;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("From DocumentVaultUserMapping dvm ");
			sb.append("where dvm.userId=:userId ");
			sb.append("and dvm.docId=:docId ");
			sb.append("and dvm.isShared=:isShared ");
			sb.append("and dvm.permission=:writePermission");
			Query<?> query = this.getCurrentSession().createQuery(sb.toString());
			query.setParameter("userId", userId);
			query.setParameter("docId", docId);
			query.setParameter("isShared", true);
			query.setParameter("writePermission", ReadWritePermissionEnum.WRITE);
			DocumentVaultUserMapping dvm = (DocumentVaultUserMapping) query.getSingleResult();
			if (dvm != null)
				flag = true;
		} catch (NoResultException ne) {
			return flag;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					DocumentMappingDaoImpl.class);
		}
		return flag;
	}

	/**
	 * This method will return the true if user has the Read or Write permission
	 * else return the false
	 */
	@Override
	public Boolean isReadOrWritePermission(Long userId, Long docId) {
		try {

			StringBuilder sb = new StringBuilder();
			sb.append("From DocumentVaultUserMapping dvm ");
			sb.append("where dvm.userId=:userId ");
			sb.append("and dvm.docId=:docId ");
			sb.append("and dvm.isShared=:isShared ");
			sb.append("and (dvm.permission=:writePermission or dvm.permission=:readPermission)");
			Query<?> query = this.getCurrentSession().createQuery(sb.toString());
			query.setParameter("userId", userId);
			query.setParameter("docId", docId);
			query.setParameter("isShared", true);
			query.setParameter("writePermission", ReadWritePermissionEnum.WRITE);
			query.setParameter("readPermission", ReadWritePermissionEnum.READ);
			DocumentVaultUserMapping dvm = (DocumentVaultUserMapping) query.getSingleResult();
			if (dvm != null)
				return true;
		} catch (NoResultException ne) {
			return false;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					DocumentMappingDaoImpl.class);
		}
		return false;
	}

	@Override
	public DocumentVaultUserMapping findByUserIdAndDocId(long userId, long docId) {
		DocumentVaultUserMapping vaultUserMapping = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<DocumentVaultUserMapping> query = builder.createQuery(DocumentVaultUserMapping.class);
			Root<DocumentVaultUserMapping> docusermapping = query.from(DocumentVaultUserMapping.class);
			query.select(docusermapping);
			query.where(builder.and(builder.equal(docusermapping.get("docId"), docId),
					builder.equal(docusermapping.get("userId"), userId)));
			vaultUserMapping = this.getCurrentSession().createQuery(query).getSingleResult();
			return vaultUserMapping;
		} catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					DocumentCaseMppingDaoImpl.class);
		}
		return vaultUserMapping;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentUserMappingDTO> getUserList(Long docId) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select new element.bst.elementexploration.rest.document.dto.DocumentUserMappingDTO(dm.userId, u.firstName, ");
		sb.append("dm.permission, dm.sharedOn) ");
		sb.append("from DocumentVaultUserMapping dm,User u ");
		sb.append("where dm.docId=:docId ");
		sb.append("and dm.userId=u.userId ");
		sb.append("and dm.isShared=true");
		List<DocumentUserMappingDTO> userList = new ArrayList<>();
		try {
			Query<?> query = this.getCurrentSession().createQuery(sb.toString());
			query.setParameter("docId", docId);
			userList = (List<DocumentUserMappingDTO>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception : ", e, DocumentMappingDaoImpl.class);
		}
		return userList;
	}
}
