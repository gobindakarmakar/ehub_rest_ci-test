package element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.ShareholderDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CorporateStructureTopFiveDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.service.CorporateStructureService;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author Paul Jalagari
 * 
 */
@Service("corporateStructureService")
public class CorporateStructureServiceImpl extends GenericServiceImpl<TransactionsData, Long>
		implements CorporateStructureService {

	@Autowired
	ShareholderDao shareholderDao;

	/// YES
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getCorporateStructure(String fromDate, String toDate, FilterDto filterDto)
			throws ParseException {
		/* return shareholderDao.getCorporateStructure(fromDate, toDate); */
		return shareholderDao.getCorporateStructure(fromDate, toDate, filterDto);
	}

	/// YES
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCorporateStructureAggregates(String fromDate, String toDate,
			FilterDto filterDto) {
		return shareholderDao.getCorporateStructureAggregates(fromDate, toDate, filterDto, "CorporateStructure");
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCorporateStructuteByType(String fromDate, String toDate, String type,
			List<String> scenarios) {
		return shareholderDao.getCorporateStructuteByType(fromDate, toDate, type, scenarios);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getAlertEntitiesBusinessType(String fromDate, String toDate) {

		return shareholderDao.getAlertEntitiesBusinessType(fromDate, toDate);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getScenariosByCountry(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = shareholderDao.getCountryAggregates(fromDate, toDate, null);
		CorporateStructureDto countAndRatioDto = new CorporateStructureDto();

		for (CorporateStructureDto corporateCountAndRatioDto : list) {
			if (corporateCountAndRatioDto.getCorporateStructure().equalsIgnoreCase(type))
				countAndRatioDto = corporateCountAndRatioDto;
		}
		countAndRatioDto.setScenario(shareholderDao.getCountryByType(fromDate, toDate, type, null));

		return countAndRatioDto;
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCustomerByCountryScenario(String fromDate, String toDate, String country,
			List<String> scenarios) {
		return shareholderDao.getCustomerByCountryScenario(fromDate, toDate, country, scenarios);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCustomerByBusinessScenario(String fromDate, String toDate, String industry,
			List<String> scenarios) {
		return shareholderDao.getCustomerByBusinessScenario(fromDate, toDate, industry, scenarios);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCounterPartyByCountryCustomer(String fromDate, String toDate, String country,
			Long customerId, List<String> scenarios) {
		return shareholderDao.getAlertEntitiesFilter(fromDate, toDate, "a.scenario", scenarios, "cd.residentCountry",
				country, "cd.id", customerId, "cd.Id");
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCounterPartyByBusinessCustomer(String fromDate, String toDate, String country,
			Long customerId, List<String> scenarios) {
		return shareholderDao.getAlertEntitiesFilter(fromDate, toDate, "a.scenario", scenarios, "cd.industry", country,
				"cd.id", customerId, "cd.Id");
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public Boolean insertShareHolders() {
		shareholderDao.insertShareHolders();
		return true;

	}

	// ok
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getCorporateScenariosByType(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = shareholderDao.getCorporateStructureAggregates(fromDate, toDate, null, null);
		CorporateStructureDto countAndRatioDto = new CorporateStructureDto();

		for (CorporateStructureDto corporateCountAndRatioDto : list) {
			if (corporateCountAndRatioDto.getCorporateStructure().equals(type))
				countAndRatioDto = corporateCountAndRatioDto;
		}
		countAndRatioDto.setScenario(shareholderDao.getCorporateByType(fromDate, toDate, type, null));

		return countAndRatioDto;
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCorporateStructuteByScenarios(String fromDate, String toDate,
			FilterDto filterDto) {
		return shareholderDao.getCorporateStructuteByScenarios(fromDate, toDate, filterDto, "CorporateStructure");
	}

	/// YES
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getAlertEntitiesGeography(String fromDate, String toDate, FilterDto filterDto) {

		return shareholderDao.getGeographyAlerts(fromDate, toDate, filterDto);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getScenariosByBusinessType(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = shareholderDao.getBusinessAggregates(fromDate, toDate, null);
		CorporateStructureDto countAndRatioDto = new CorporateStructureDto();

		for (CorporateStructureDto corporateCountAndRatioDto : list) {
			if (corporateCountAndRatioDto.getCorporateStructure().equals(type))
				countAndRatioDto = corporateCountAndRatioDto;
		}
		countAndRatioDto.setScenario(shareholderDao.getBusinessByType(fromDate, toDate, type, null));

		return countAndRatioDto;
	}

	/// YES
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getShareholderCorporateStructure(String fromDate, String toDate,
			FilterDto filterDto) {
		return shareholderDao.getCorporateStructureAggregates(fromDate, toDate, filterDto, "Shareholder");
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getShareholderCountry(String fromDate, String toDate) {
		return shareholderDao.getShareholderCountry(fromDate, toDate, "cd.residentCountry");
	}

	/// YES
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getShareholderAggregates(String fromDate, String toDate, FilterDto filterDto) {
		return shareholderDao.getShareHolder(fromDate, toDate, filterDto);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getCustomerByShareholderScenario(String fromDate, String toDate, String country,
			List<String> scenarios) {
		return shareholderDao.getCustomerByShareholderScenario(fromDate, toDate, country, scenarios);
	}

	// ok
	@Override
	@Transactional("transactionManager")
	public CorporateStructureDto getScenariosByShareholder(String fromDate, String toDate, String type) {
		List<CorporateStructureDto> list = shareholderDao.getShareholderAggregates(fromDate, toDate, null);
		CorporateStructureDto countAndRatioDto = new CorporateStructureDto();

		for (CorporateStructureDto corporateCountAndRatioDto : list) {
			if (corporateCountAndRatioDto.getType().equals(type))
				countAndRatioDto = corporateCountAndRatioDto;
		}
		countAndRatioDto.setScenario(shareholderDao.getShareholderByType(fromDate, toDate, type, null));

		return countAndRatioDto;
	}

	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getViewAllCorporateStructure(String fromDate, String toDate, Integer pageNumber,
			Integer recordsPerPage, String type, FilterDto filterDto) {

		List<CorporateStructureDto> transactionList = new ArrayList<>();
		List<CorporateStructureDto> alertList = new ArrayList<>();
		if ("CorporateStructure".equalsIgnoreCase(type)) {
			transactionList = shareholderDao.getViewAllCorporateStructure(fromDate, toDate, true,
					"cd.corporateStructure", pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto);
			alertList = shareholderDao.getViewAllCorporateStructure(fromDate, toDate, false, "cd.corporateStructure",
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto);
			for (CorporateStructureDto countAndRatioDto : alertList) {
				for (CorporateStructureDto transaction : transactionList) {
					int value = (countAndRatioDto.getDate()).compareTo(transaction.getDate());
					if (value == 0)
						transaction.setAlertAmount(countAndRatioDto.getAlertAmount());

				}

			}
		} else if ("Shareholders".equalsIgnoreCase(type)) {
			transactionList = shareholderDao.getViewAllCorporateStructure(fromDate, toDate, true,
					"cd.corporateStructure", pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto);
			alertList = shareholderDao.getViewAllCorporateStructure(fromDate, toDate, false, "cd.corporateStructure",
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto);
			for (CorporateStructureDto countAndRatioDto : alertList) {
				for (CorporateStructureDto transaction : transactionList) {
					int value = (countAndRatioDto.getDate()).compareTo(transaction.getDate());
					if (value == 0)
						transaction.setAlertAmount(countAndRatioDto.getAlertAmount());

				}

			}
		} else if ("Geography".equalsIgnoreCase(type)) {
			transactionList = shareholderDao.getViewAllCorporateStructure(fromDate, toDate, true,
					"cd.corporateStructure", pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto);
			alertList = shareholderDao.getViewAllCorporateStructure(fromDate, toDate, false, "cd.corporateStructure",
					pageNumber == null ? ElementConstants.DEFAULT_PAGE_NUMBER : pageNumber,
					recordsPerPage == null ? ElementConstants.DEFAULT_PAGE_SIZE : recordsPerPage, filterDto);
			for (CorporateStructureDto countAndRatioDto : alertList) {
				for (CorporateStructureDto transaction : transactionList) {
					int value = (countAndRatioDto.getDate()).compareTo(transaction.getDate());
					if (value == 0)
						transaction.setAlertAmount(countAndRatioDto.getAlertAmount());

				}

			}
		}
		return transactionList;
	}

	@Override
	@Transactional("transactionManager")
	public long getViewAllCorporateStructureCount(String fromDate, String toDate, String type) {
		long count = 0;

		if ("CorporateStructure".equalsIgnoreCase(type)) {

			count = shareholderDao.getViewAllCorporateStructureCount(fromDate, toDate, true);
		} else if ("Shareholder".equalsIgnoreCase(type)) {

			String queryforCondition = "Join Shareholders sh on cd.id=sh.customerId";
			count = shareholderDao.getViewAllCount(fromDate, toDate, true, queryforCondition);
		} else if ("Geography".equalsIgnoreCase(type)) {

			// String queryforCondition = "(c.freedomHouseRisk>0 or
			// c.worldJusticeRiskWorld_CriminalJusticeRiskRating>0 or
			// c.wefRisk>0)";
			// count = shareholderDao.getViewAllCount(fromDate, toDate, true,
			// queryforCondition);
			count = shareholderDao.getViewAllCorporateStructureCount(fromDate, toDate, true);
		}
		return count;
	}

	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getAssociatedAlerts(String fromDate, String toDate, FilterDto filterDto) {
		return shareholderDao.getAssociatedAlerts(fromDate, toDate, filterDto);
	}

	/// YES
	@Override
	@Transactional("transactionManager")
	public CorporateStructureTopFiveDto getTopFiveAlerts(String fromDate, String toDate,
			FilterDto filterDto) {
		Map<String, List<CorporateStructureDto>> map = new HashMap<String, List<CorporateStructureDto>>();
		List<CorporateStructureDto> topCorporateStructures = shareholderDao.getCorporateStructureAggregates(fromDate,
				toDate, filterDto, "CorporateStructure");
		List<CorporateStructureDto> topShareholders = shareholderDao.getCorporateStructureAggregates(fromDate, toDate,
				filterDto, "Shareholder");
		List<CorporateStructureDto> topgeography = shareholderDao.getGeographyAggregates(fromDate, toDate, filterDto);
		map.put("topCorporateStructures", topCorporateStructures);
		map.put("topShareholders", topShareholders);
		map.put("topgeography", topgeography);
		CorporateStructureTopFiveDto topStructuresData= new CorporateStructureTopFiveDto();
		topStructuresData.setTopCorporateStructures(topCorporateStructures);
		topStructuresData.setTopShareholders(topShareholders);
		topStructuresData.setTopgeography(topgeography);
		return topStructuresData;
	}

	/// YES
	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getGeographyAggregates(String fromDate, String toDate, FilterDto filterDto) {
		return shareholderDao.getGeographyAggregates(fromDate, toDate, filterDto);
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, List<CorporateStructureDto>> getCorporateStructureByType(String fromDate, String toDate,
			String type, FilterDto filterDto) {
		Map<String, List<CorporateStructureDto>> map = new HashMap<String, List<CorporateStructureDto>>();
		List<CorporateStructureDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		if (type != null)
			filterDto.setCorporateStructureType(type);
		CorporateStructureDto riskCountAndRatioDto = shareholderDao.getCorporateStructure(fromDate, toDate, filterDto);
		List<CorporateStructureDto> listScenarios = shareholderDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getCorporateStructureType());
		for (CorporateStructureDto dto : listScenarios) {
			scenarios.add(dto.getCorporateStructure());
		}
		riskCountAndRatioDto.setScenarios(scenarios);
		list.add(riskCountAndRatioDto);
		map.put("list", list);
		map.put("aggregates", listScenarios);
		return map;
	}

	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getGeographyByScenarios(String fromDate, String toDate, FilterDto filterDto) {
		return shareholderDao.getCorporateStructuteByScenarios(fromDate, toDate, filterDto, "Geography");
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, List<CorporateStructureDto>> getShareholderByType(String fromDate, String toDate, String type,
			FilterDto filterDto) {
		Map<String, List<CorporateStructureDto>> map = new HashMap<String, List<CorporateStructureDto>>();
		List<CorporateStructureDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		if (type != null)
			filterDto.setShareholderType(type);
		CorporateStructureDto riskCountAndRatioDto = shareholderDao.getCorporateStructure(fromDate, toDate, filterDto);
		List<CorporateStructureDto> listScenarios = shareholderDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getShareholderType());
		for (CorporateStructureDto dto : listScenarios) {
			scenarios.add(dto.getCorporateStructure());
		}
		riskCountAndRatioDto.setScenarios(scenarios);
		list.add(riskCountAndRatioDto);
		map.put("list", list);
		map.put("aggregates", listScenarios);
		return map;
	}

	@Override
	@Transactional("transactionManager")
	public Map<String, List<CorporateStructureDto>> getGeographyByType(String fromDate, String toDate, String type,
			FilterDto filterDto) {
		Map<String, List<CorporateStructureDto>> map = new HashMap<String, List<CorporateStructureDto>>();
		List<CorporateStructureDto> list = new ArrayList<>();
		Set<String> scenarios = new HashSet<>();
		if (type != null)
			filterDto.setCorporateGeographyType(type);
		CorporateStructureDto riskCountAndRatioDto = shareholderDao.getCorporateStructure(fromDate, toDate, filterDto);
		List<CorporateStructureDto> listScenarios = shareholderDao.getGroupByScenario(fromDate, toDate, filterDto);
		riskCountAndRatioDto.setType(filterDto.getCorporateGeographyType());
		for (CorporateStructureDto dto : listScenarios) {
			scenarios.add(dto.getCorporateStructure());
		}
		riskCountAndRatioDto.setScenarios(scenarios);
		list.add(riskCountAndRatioDto);
		map.put("list", list);
		map.put("aggregates", listScenarios);
		return map;
	}

	@Override
	@Transactional("transactionManager")
	public List<CorporateStructureDto> getShareholderByScenarios(String fromDate, String toDate, FilterDto filterDto) {
		return shareholderDao.getCorporateStructuteByScenarios(fromDate, toDate, filterDto, "Shareholder");
	}

}
