package element.bst.elementexploration.rest.util;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.activiti.app.conf.Bootstrapper;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.settings.domain.Settings;
import element.bst.elementexploration.rest.settings.listManagement.dao.ListItemDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;

@Component
@Transactional("transactionManager")
public class SchedulePendingUsersDeactivation {
	
	
	private final Logger log = LoggerFactory.getLogger(SchedulePendingUsersDeactivation.class);

	@Autowired
	ListItemDao listItemDao;
	
	@Autowired
	private SettingsService settingsService;
	
	@Autowired
	UsersService usersService;

	@Scheduled(cron = "0 0 1 * * ?")
	public void deactivatePendingUsers() throws Exception {
		log.info("Calling SchedulePendingUsersDeactivation >> deactivatePendingUsers");
		
		int pendingDaysOfUser = 0;
		ListItem pendingStatusItem = listItemDao.getListItemByListTypeAndDisplayName(ElementConstants.USER_STATUS_TYPE,
				UserStatus.Pending.getStatus());
		ListItem deactivatedStatusItem = listItemDao.getListItemByListTypeAndDisplayName(
				ElementConstants.USER_STATUS_TYPE, UserStatus.Deactivated.getStatus());
		Optional<Settings> pendingSetting = settingsService.findAll().stream()
				.filter(s -> s.getName().equalsIgnoreCase("Pending users deactivation after")).findFirst();
		Settings pendingSettingObject = null;
		if (pendingSetting.isPresent()) {
			pendingSettingObject = pendingSetting.get();
		}
		if (pendingStatusItem != null && pendingStatusItem.getListItemId() != null && pendingSetting != null) {
			List<Users> pendingUsers = usersService.findAll().stream()
					.filter(user -> user.getStatusId().getListItemId().equals(pendingStatusItem.getListItemId()))
					.collect(Collectors.toList());
			if (pendingUsers != null && pendingUsers.size() > 0) {
				log.info("pendingUsers count is : "+pendingUsers.size());
				for (Users user : pendingUsers) {
					Long days = (user.getCreatedDate().getTime() - new Date().getTime()) / 86400000;
					pendingDaysOfUser = Math.abs(days.intValue());
					if ((pendingDaysOfUser - Integer.valueOf(pendingSettingObject.getDefaultValue())) > 0) {
						user.setStatusId(deactivatedStatusItem);
						usersService.saveOrUpdate(user);
					}
				}
			}else {
				log.info("pendingUsers count is 0 ");
			}
		}
	}

}
