package element.bst.elementexploration.rest.extention.docparser.enums;

/**
 * @author suresh
 *
 */
public enum AnswerType
{
	
	CHECKBOX,LIST,TEXT,TEXT_AREA,SHEET,ATTACHMENT,TABLE,EMPTY,CHECKBOX_AND_TEXT_AREA,YES_NO_CHECKBOX
	

}
