package element.bst.elementexploration.rest.extention.sourcecredibility.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.ClassificationsService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author suresh
 *
 */
@Api(tags = { "Classification Management API" })
@RestController
@RequestMapping("/api/classification")
public class ClassificationController extends BaseController {

	@Autowired
	ClassificationsService classificationService;

	@ApiOperation("Gets the industries")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Sources Successfully.") })
	@GetMapping(value = "/getClassifications", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getSources(@RequestParam("token") String token, HttpServletRequest request) {
		return new ResponseEntity<>(classificationService.getClassifications(), HttpStatus.OK);
	}

	@ApiOperation("Save Jurisdiction  Source")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/updateSubClassifcationMedia", produces = { "application/json; charset=UTF-8" },consumes= {"application/json; charset=UTF-8"})
	public ResponseEntity<?> updateSubClassifcationMedia(HttpServletRequest request, @RequestParam String token,
			@RequestBody ClassificationsDto classifcationsDto) throws Exception {
		return new ResponseEntity<>(classificationService.updateSubClassifcationMedia(classifcationsDto),
				HttpStatus.OK);
	}

}
