package element.bst.elementexploration.rest.search.daoImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.search.dao.SearchHistoryDao;
import element.bst.elementexploration.rest.search.domain.SearchHistory;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("searchHistoryDao")
public class SearchHistoryDaoImpl extends GenericDaoImpl<SearchHistory, Long> implements SearchHistoryDao{

	public SearchHistoryDaoImpl() {
		super(SearchHistory.class);
	}

	@Override
	public SearchHistory findMostRecentSearch(Long userId) {
		List<SearchHistory> searches = new ArrayList<>();
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<SearchHistory> query = builder.createQuery(SearchHistory.class);
			Root<SearchHistory> search = query.from(SearchHistory.class);
			query.select(search);
			query.where(builder.equal(search.get("userId"), userId));
			query.orderBy(builder.desc(search.get("createdOn")));
			searches = this.getCurrentSession().createQuery(query).getResultList();
			if(!searches.isEmpty())
				return searches.get(0);
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to add participants. Reason : " + e.getMessage());
		} 	
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchHistory> getRecentSearches(Long userId, Date dateFrom, Date dateTo, Boolean favourite,
			String flag, Integer pageNumber, Integer recordsPerPage) {
		//DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
		List<SearchHistory> searchList = new ArrayList<>();
		try {			
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from SearchHistory search where search.userId =:userId ");
			if(favourite != null)
				queryBuilder.append(" and search.favourite =:favourite");
			if(flag != null)
				queryBuilder.append(" and search.searchFlag =:searchFlag");
			if (dateFrom != null && dateTo == null) {
				queryBuilder.append(" and search.createdOn >='");
				queryBuilder.append(sdf.format(dateFrom));
				queryBuilder.append("'");
			} else if (dateTo != null && dateFrom == null) {
				queryBuilder.append(" and search.createdOn <'");
				queryBuilder.append(sdf.format(dateTo));
				queryBuilder.append("'");
			} else if(dateFrom != null && dateTo != null){
				queryBuilder.append(" and search.createdOn BETWEEN ");
				queryBuilder.append("'" + sdf.format(dateFrom) + "'");
				queryBuilder.append(" and '" + sdf.format(dateTo) + "'");
			}
			queryBuilder.append(" order by search.createdOn desc");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if(userId != null)
				query.setParameter("userId", userId);
			if(favourite != null)
				query.setParameter("favourite", favourite);
			if(flag != null)
				query.setParameter("searchFlag", flag);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			searchList = (List<SearchHistory>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		} 		
		return searchList;
	}

	@Override
	public long countRecentSearches(Long userId, Date dateFrom, Date dateTo, Boolean favourite, String flag) {
		DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		long count = 0;
		try {			
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select count(*) from SearchHistory search where search.userId =:userId ");
			if(favourite != null)
				queryBuilder.append(" and search.favourite =:favourite");
			if(flag != null)
				queryBuilder.append(" and search.searchFlag =:searchFlag");
			if (dateFrom != null && dateTo == null) {
				queryBuilder.append(" and search.createdOn >='");
				queryBuilder.append(sdf.format(dateFrom));
				queryBuilder.append("'");
			} else if (dateTo != null && dateFrom == null) {
				queryBuilder.append(" and search.createdOn <'");
				queryBuilder.append(sdf.format(dateTo));
				queryBuilder.append("'");
			} else if(dateFrom != null && dateTo != null){
				queryBuilder.append(" and search.createdOn BETWEEN ");
				queryBuilder.append("'" + sdf.format(dateFrom) + "'");
				queryBuilder.append(" and '" + sdf.format(dateTo) + "'");
			}

			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if(userId != null)
				query.setParameter("userId", userId);
			if(favourite != null)
				query.setParameter("favourite", favourite);
			if(flag != null)
				query.setParameter("searchFlag", flag);
			count = ((Number)query.getSingleResult()).longValue();			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		} 
		return count;
	}

}
