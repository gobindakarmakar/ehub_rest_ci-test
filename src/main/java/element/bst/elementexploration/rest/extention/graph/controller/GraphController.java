package element.bst.elementexploration.rest.extention.graph.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.graph.service.GraphService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Graph API" },description="Renders graph data")
@RestController
@RequestMapping("/api/graph")
public class GraphController extends BaseController {

	@Autowired
	private GraphService graphService;

	@ApiOperation("Gets queried entity graph data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getDataFineData", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getDataFineData(@RequestParam String searchQuery,
			@RequestParam(required = false) Integer depth, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(graphService.getAllDataFineDataFromBigDataServer(searchQuery, depth),
				HttpStatus.OK);
	}

	@ApiOperation("Gets cyber security graph data")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/cyberSecurity", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> getCyberSecurity(@RequestBody String jsonString, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(graphService.getCyberSecurity(jsonString), HttpStatus.OK);
	}
}
