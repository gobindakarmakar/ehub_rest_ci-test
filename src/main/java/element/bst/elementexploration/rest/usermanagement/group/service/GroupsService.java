package element.bst.elementexploration.rest.usermanagement.group.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import element.bst.elementexploration.rest.generic.service.GenericService;
import element.bst.elementexploration.rest.usermanagement.group.domain.Groups;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupAlertSettingDto;
import element.bst.elementexploration.rest.usermanagement.group.dto.UpdateGroupDto;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

/**
 * @author Paul Jalagari
 *
 */
public interface GroupsService extends GenericService<Groups, Long>{

	Groups getGroup(String name);

	List<Groups> getUserGroups(Boolean status, String keyword, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	public Long countUserGroups(Boolean status, String keyword);

	boolean isActiveGroup(Long groupId);

	List<Groups> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);
	
	Long countGroupsOfUser(Long userId);
	
	Groups checkGroupExist(String name, Long groupId);
	
	public Boolean doesUserbelongtoGroup(String groupName, Long userId);
	
	public List<Users> getUsersByGroup(Long groupId);

	List<Groups> getGroupsOfUser(Long userId);
	
	List<Groups> getGroupsOfUserWithRoles(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn);

	boolean addUsersListToGroup(long adminId, List<Long> userIds,Long groupId);

	Object getStatuswiseUsersByGroupId(Long groupId);

	JSONObject getJsonFromMap(Map<String, Long> statusObjMap);

	Groups updateGroupById(Long groupId, UpdateGroupDto updatedGroup, Long adminId);
	
	Groups checkIfGroupExist(String name, Long groupId);

	boolean addGroupAlertSetting(List<GroupAlertSettingDto> groupAlertSettingDto, Long currentUserId);

	boolean deleteGroup(Long groupId, Long currentUserId);

	boolean deleteGroups(Long groupsId, Long currentUserId)throws Exception;

}
