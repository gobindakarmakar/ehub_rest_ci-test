package element.bst.elementexploration.rest.extention.docparser.dao;

import java.util.List;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
/**
 * 
 * @author Rambabu
 *
 */
public interface DocumentTemplateMappingDao extends GenericDao<DocumentTemplateMapping, Long>{
	
	public DocumentTemplateMapping getDocumentTemplateMappingByDocId(Long docId);

	public List<DocumentTemplateMapping> getDocumentTemplateMappingByTemplateId(long templateId);
	

}
