package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceCategoryDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCategory;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceCategoryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCategoryService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.util.EntityConverter;

@Service("sourceCategoryService")
public class SourceCategoryServiceImpl extends GenericServiceImpl<SourceCategory, Long> implements SourceCategoryService{

	@Autowired
	public SourceCategoryServiceImpl(GenericDao<SourceCategory, Long> genericDao){
		super(genericDao);
	}
	
	public SourceCategoryServiceImpl(){
		
	}
	
	@Autowired
	SourceCategoryDao sourceCategoryDao;

	@Override
	public boolean checkCategoryExists(String dtoCategory) {
		boolean isCategory = sourceCategoryDao.checkCategoryExists(dtoCategory);
		return isCategory;
	}

	@Override
	@Transactional("transactionManager")
	public List<SourceCategoryDto> getSourceCategories() {
		
		List<SourceCategory> categoryList = sourceCategoryDao.findAll();
		List<SourceCategoryDto> sourcesDtoList = new ArrayList<SourceCategoryDto>();
		if (categoryList != null) {
			sourcesDtoList = categoryList.stream()
					.map((sourcesDto) -> new EntityConverter<SourceCategory, SourceCategoryDto>(SourceCategory.class,
							SourceCategoryDto.class).toT2(sourcesDto, new SourceCategoryDto()))
					.collect(Collectors.toList());
		}
		return sourcesDtoList;
	}
}
