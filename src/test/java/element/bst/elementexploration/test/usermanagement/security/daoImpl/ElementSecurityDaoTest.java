package element.bst.elementexploration.test.usermanagement.security.daoImpl;

import static org.junit.Assert.assertEquals;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.usermanagement.security.daoImpl.ElementSecurityDaoImpl;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserToken;

/**
 * @author ramababu
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ElementSecurityDaoTest {
	
	@Mock
	 SessionFactory sessionFactory;
	
	@Mock
	Session session;
	
	@SuppressWarnings("rawtypes")
	@Mock
	Query query;
	
	@Mock
	CriteriaBuilder builder;
	
	@Mock
	CriteriaQuery<Object> criteriaQuery;
	
	@Mock
	Root<UserToken> root;
	
	
	@InjectMocks
	ElementSecurityDaoImpl elementSecurityDao;
	
	public UserToken userToken;
	
	@Before
	public void doSetup() {
		userToken = new UserToken();
		userToken.setId(1L);
		userToken.setToken("a1b2c");
		MockitoAnnotations.initMocks(this);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void isValidTokenTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(UserToken.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		boolean newResult = elementSecurityDao.isValidToken("token");
		assertEquals(true, newResult);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void isValidToken_whenExceptionThrowTest() {
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(Mockito.anyObject())).thenReturn(criteriaQuery);
		Mockito.when(criteriaQuery.from(UserToken.class)).thenReturn(root);		
		Mockito.when(session.createQuery(criteriaQuery)).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		elementSecurityDao.isValidToken("token");
	}
	
	
	@Test
	public void getUserTokenTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(userToken);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		UserToken userTokenNew = elementSecurityDao.getUserToken("analyst");
		assertEquals(new Long("1"),userTokenNew.getId());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserToken_whenNoResultExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		UserToken newUserToken = elementSecurityDao.getUserToken("analyst");
		assertEquals(null,newUserToken);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void getUserToken_whenFailedToExecuteQueryExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		elementSecurityDao.getUserToken("analyst");
	}
	

	@Test
	public void isTokenExistTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(userToken);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		UserToken userTokenNew = elementSecurityDao.isTokenExist(1L);
		assertEquals(new Long("1"),userTokenNew.getId());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void isTokenExist_whenNoResultExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		UserToken newUserToken = elementSecurityDao.isTokenExist(1L);
		assertEquals(null,newUserToken);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void isTokenExist_whenFailedToExecuteQueryExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		elementSecurityDao.isTokenExist(1L);
	}
	
	@Test
	public void isTokenExist_withTokenTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenReturn(1L);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		boolean status = elementSecurityDao.isTokenExist("analyst");
		assertEquals(true,status);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void isTokenExist_withToken_whenNoResultExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(NoResultException.class);
		boolean status = elementSecurityDao.isTokenExist("analyst");
		assertEquals(false,status);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = FailedToExecuteQueryException.class)
	public void isTokenExist_withToken_whenFailedToExecuteQueryExceptionThrowTest(){
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);
		Mockito.when(query.setParameter(Mockito.anyString(), Mockito.anyString())).thenReturn(query);
		Mockito.when(query.getSingleResult()).thenThrow(FailedToExecuteQueryException.class);
		elementSecurityDao.isTokenExist("analyst");
	}
	
	
	
	
	
	

}
