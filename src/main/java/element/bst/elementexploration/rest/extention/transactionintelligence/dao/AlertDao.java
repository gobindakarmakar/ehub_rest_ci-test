package element.bst.elementexploration.rest.extention.transactionintelligence.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregateVo;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertAggregator;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertByPeriodDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertComparisonDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDtoAscnd;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertScenarioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertStatusDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertsDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.MonthlyTurnOverDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.RiskCountAndRatioDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TransactionAmountAndAlertCountDto;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author suresh
 *
 */
public interface AlertDao extends GenericDao<Alert, Long> {

	public List<Alert> fetchNonResolvedAlerts(Integer pageNumber, Integer recordsPerPage);

	Alert fetchAlertSummary(long alertId);

	Alert loadAlertById(Long Id);

	List<Alert> fetchCount(Long userId);

	List<Alert> fetchfilterName(String name, String dateFrom, String dateTo, Integer pageNumber,Integer recordsPerPage);
	
	List<Alert> findByCustomerAndDate(String customerNumber, Date date);

	public int fetchfilterNameCount(String name, String dateFrom, String dateTo, Integer pageNumber,Integer recordsPerPage);
	
	public List<AlertDashBoardRespDtoAscnd> fetchAlertsBwDatesOrderBy(String name, String dateFrom, String dateTo,
			Integer pageNumber, Integer recordsPerPage, String orderBy) throws ParseException;
	
	public List<AlertStatusDto> fetchGroupByAlertStatus(String fromDate, String toDate,boolean periodBy,FilterDto filterDto) throws ParseException ;
	public List<Alert> calculateAlertsGenRatio(List<Long> transactionIds,String fromDate,String toDate) throws ParseException ;
	//public List<AlertDashBoardRespDto> fetchAlertsBwDatesList(String dateFrom, String dateTo) throws ParseException;

	public List<TransactionAmountAndAlertCountDto> getAlertAggregatesProductType(String fromDate, String toDate, String granularity,Boolean isGroupByType,String transType,boolean input,boolean output,FilterDto filterDto) throws ParseException;
	public List<TransactionAmountAndAlertCountDto> getAlertAggregates(String fromDate, String toDate, String granularity,Boolean isGroupByType,String transType,boolean input,boolean output,FilterDto filterDto) throws ParseException;
	public MonthlyTurnOverDto getMonthlyTurnOver(String fromDate, String toDate,FilterDto filterDto) throws ParseException ;
	public List<AlertNotiDto> fetchAlertsBwDatesCustomized(String fromDate,String toDate,String productType,boolean input ,boolean output,FilterDto filterDto )  throws ParseException;

	public List<String> getCustomerRiskByType(String fromDate, String toDate,String type,List<String> scenarios);

	public List<String> getScenarios(String fromDate, String toDate, String type);

	public RiskCountAndRatioDto getProductRisk(String fromDate, String toDate);

	public List<RiskCountAndRatioDto> getProductRiskAggregates(String fromDate, String toDate,String type,FilterDto filterDto);
	public List<AlertComparisonDto> alertComparisonNotification(String fromDate,String toDate,Date previousDate,FilterDto filterDto,String originalToDate) throws ParseException;
	List<AlertScenarioDto> amlTopScenarios(String fromDate,String toDate,FilterDto filterDto)throws ParseException ;

	List<RiskCountAndRatioDto> getTopProductRisk(String fromDate, String toDate);
	AlertByPeriodDto amlAlertByPeriod(String fromDate,String toDate,FilterDto filterDto) throws ParseException;

	public List<AlertDashBoardRespDto> getRiskAlertsByScenarios(String fromDate, String toDate,FilterDto filterDto);

	public List<AlertDashBoardRespDto> getRiskByScenarios(String fromDate, String toDate, List<String> scenarios,
			String type);
	
	List<CounterPartyNotiDto> getAlertByScenarios(String fromDate, String toDate,String type,List<String> scenarios,boolean isCountry);

	public List<RiskCountAndRatioDto> getTopCounterParties(String fromDate, String toDate);

	public List<RiskCountAndRatioDto> getTopBanks(String fromDate, String toDate);
	
	public List<Alert> fetchAlertSummaryList(Long transactionId);
	
	public AlertScenarioDto getAlertScenarioById(Long id,String scenarioType);

	public List<Alert> getAlertsForCustomer(String fromDate, String toDate, String customerNumber);

	public List<RiskCountAndRatioDto> getGroupByScenario(String fromDate, String toDate, FilterDto filterDto);
	
	
	boolean findAlertByAlertTransactionId(Long alertTransactionId);

	public List<AlertsDto> searchAlerts(String keyword);

	public Long searchAlertsCount(String keyword);

	List<AlertAggregator> alertsAggregates(AlertAggregateVo alertAggregateVo, Long userId);

}

