package element.bst.elementexploration.rest.casemanagement.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.usermanagement.user.domain.Users;

public class CaseAnalystMappings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 768680922728836388L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "analyst_mapping_id")
	private Long id;

	@Column(name = "current_status")
	private Integer currentStatus;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "status_comments")
	private String statusComments;

	@Column(name = "notification_date")
	private Date notificationDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_case_id")
	private Cases caseSeed;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_analyst_id")
	private Users user;

	@OneToOne
	@JoinColumn(name = "assigned_by")
	private Users assignedBy;

	/**
	 * 
	 */
	public CaseAnalystMappings() {
		super();
	}

	public CaseAnalystMappings(Long id, Integer currentStatus, Date createdOn, Date modifiedOn, String statusComments,
			Date notificationDate, Cases caseSeed, Users user) {
		super();
		this.id = id;
		this.currentStatus = currentStatus;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
		this.statusComments = statusComments;
		this.notificationDate = notificationDate;
		this.caseSeed = caseSeed;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Integer currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public Cases getCaseSeed() {
		return caseSeed;
	}

	public void setCaseSeed(Cases caseSeed) {
		this.caseSeed = caseSeed;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Users getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(Users assignedBy) {
		this.assignedBy = assignedBy;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
