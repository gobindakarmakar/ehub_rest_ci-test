package element.bst.elementexploration.rest.extention.docparser.service;

import element.bst.elementexploration.rest.extention.docparser.domain.IsoCodes;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Suresh
 *
 */
public interface ISOCodeService extends GenericService<IsoCodes, Long>{
	
	
}
