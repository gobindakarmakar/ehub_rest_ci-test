package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.GroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Group;
import element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupDto;
import element.bst.elementexploration.rest.usermanagement.user.daoImpl.UserDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;


/**
 * 
 * @author Viswanath
 *
 */
@Repository("groupDao")
public class GroupDaoImpl extends GenericDaoImpl<Group, Long> implements GroupDao {
	
	public GroupDaoImpl() {
		super(Group.class);
	}
	
	@Override
	public Group getGroup(String groupName) {		
		Group group = null;
		try {			
			StringBuilder queryBuilder = new StringBuilder();			
			queryBuilder.append("from Group g where g.name = :groupName");			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("groupName", groupName);			
			group = (Group)query.getSingleResult();
			return group;
		} catch (NoResultException e) {
			return null;			
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		} 		
		return group;
	}
	
	
	/**
	 * Method to resolve Usergroup column names
	 * 
	 * @param groupColumn
	 * @return
	 */
	private String resolveGroupCaseColumnName(String groupColumn) {
		if (groupColumn == null)
			return "createdDate";

		switch (groupColumn.toLowerCase()) {
		case "name":
			return "name";
		default:
			return "createdDate";
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Group> getUserGroups(Integer status, String keyword, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		List<Group> groups = new ArrayList<>();
		try {			
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from Group g ");
			boolean isOptional = true;
			if (!StringUtils.isEmpty(keyword)) {
				queryBuilder.append(" where g.name like '%" + keyword + "%'");
				queryBuilder.append(" or g.description like '%" + keyword + "%'");
				queryBuilder.append(" or g.remarks like '%" + keyword + "%'");
				isOptional = false;
			}
			if(status != null){
				if(!isOptional)
					queryBuilder.append(" and g.active =:status");
				else
					queryBuilder.append(" where g.active =:status");
			}
			queryBuilder.append(" order by ")
			   .append("g.")
			   .append(resolveGroupCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if(status != null)
				query.setParameter("status", status);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			groups = (List<Group>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		} 		
		return groups;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Group> getGroupsOfUser(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		List<Group> userGroupList = new ArrayList<>();		
		try {
			
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Group(g.userGroupId, g.createdDate, "
					+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description) ");
			hql.append("from Group g ")
			 .append("left outer join UserGroup ug on g.userGroupId = ug.groupId ")
		       .append("where ug.userId =:userId and g.active = 1");

			hql.append(" order by ")
			   .append("g.")
			   .append(resolveGroupCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					hql.append(" ");
					hql.append(orderIn);
				}
			}
			Query<?> query =  this .getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);
			userGroupList = (List<Group>) query.getResultList();
			
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		
		return userGroupList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserGroups(Integer status, String keyword) {
		List<Group> groups = new ArrayList<>();
		try {			
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("from Group g ");
			boolean isOptional = true;
			if (!StringUtils.isEmpty(keyword)) {
				queryBuilder.append(" where g.name like '%" + keyword + "%'");
				queryBuilder.append(" or g.description like '%" + keyword + "%'");
				queryBuilder.append(" or g.remarks like '%" + keyword + "%'");
				isOptional = false;
			}
			if(status != null){
				if(!isOptional)
					queryBuilder.append(" and g.active =:status");
				else
					queryBuilder.append(" where g.active =:status");
			}
			
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			if(status != null)
				query.setParameter("status", status);
			groups = (List<Group>) query.getResultList();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e, this.getClass());
		} 		
		return (long) groups.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countGroupsOfUser(Long userId) {
		List<Group> userGroupList = new ArrayList<>();		
		try {
			
			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Group(g.userGroupId, g.createdDate, "
					+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description) ");
			hql.append("from Group g ")
			 .append("left outer join UserGroup ug on g.userGroupId = ug.groupId ")
		       .append("where ug.userId =:userId and g.active = 1");
			
			Query<?> query =  this .getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			userGroupList = (List<Group>) query.getResultList();			
		}catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		
		return (long) userGroupList.size();
	}

	@Override
	public Group checkGroupExist(String name, Long groupId) {
		Group found = null;
		try{
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Group> query = builder.createQuery(Group.class);
			Root<Group> group = query.from(Group.class);
			query.select(group);
			query.where(builder.and(builder.notEqual(group.get("userGroupId"), groupId), builder.equal(group.get("name"), name)));
			found = (Group) this.getCurrentSession().createQuery(query).getSingleResult();
		}catch (NoResultException e) {
			return null;
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, UserDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return found;
	}

	@Override
	public Boolean doesUserbelongtoGroup(String groupName, Long userId) {
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select distinct new element.bst.elementexploration.rest.usermanagement.group.dto.UserGroupDto(ug.userId) ");
			queryBuilder.append("from UserGroup ug left outer join Group g on g.userGroupId = ug.groupId ");
			queryBuilder.append("where g.name = '" + groupName + "' AND ug.userId ='" + userId + "'");

			UserGroupDto userGroup = (UserGroupDto) this.getCurrentSession().createQuery(queryBuilder.toString())
					.getSingleResult();
			if (null != userGroup) {
				return true;
			}
		}catch (NoResultException e) {
			return false;
		}  catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "check user from admin group " + userId, e, this.getClass());
			throw new FailedToExecuteQueryException(
					userId + "Could not get user from admin group " + userId + " - " + e.getMessage());
		}
		return false;
	}
	 
}
