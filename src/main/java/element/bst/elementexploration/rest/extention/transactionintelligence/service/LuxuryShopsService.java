package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.LuxuryShops;
import element.bst.elementexploration.rest.generic.service.GenericService;

public interface LuxuryShopsService extends GenericService<LuxuryShops, Long>{
	
	Boolean uploadLuxuryShopDetails(MultipartFile file) throws IOException, ParseException,IllegalAccessException, InvocationTargetException  ;

}
