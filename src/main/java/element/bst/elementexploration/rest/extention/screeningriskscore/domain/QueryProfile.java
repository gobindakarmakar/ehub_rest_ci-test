package element.bst.elementexploration.rest.extention.screeningriskscore.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class QueryProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "vcard family-name")
	@JsonProperty("vcard:family-name")
	private String vcardFamilyName;
	
	@ApiModelProperty(value = "vcardgiven-name")
	@JsonProperty("vcard:given-name")
	private String vcardGivenName;	
	
	@ApiModelProperty(value = "Date of birth")
	@JsonProperty("DOB")
	private String dob;

	public String getVcardFamilyName() {
		return vcardFamilyName;
	}

	public void setVcardFamilyName(String vcardFamilyName) {
		this.vcardFamilyName = vcardFamilyName;
	}

	public String getVcardGivenName() {
		return vcardGivenName;
	}

	public void setVcardGivenName(String vcardGivenName) {
		this.vcardGivenName = vcardGivenName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	
	
}
