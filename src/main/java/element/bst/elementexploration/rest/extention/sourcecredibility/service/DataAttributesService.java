package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.DataAttributes;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface DataAttributesService extends GenericService<DataAttributes, Long> {

}
