package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Alert status")
public class AlertStatusDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Alert status")
	private AlertStatus alertStatus;
	@ApiModelProperty(value="Count of alert statuses")
	private long count;

	public AlertStatusDto() {
	}

	public AlertStatusDto(AlertStatus alertStatus, Long count) {
		this.alertStatus = alertStatus;
		this.count = count;
	}

	public AlertStatus getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(AlertStatus alertStatus) {
		this.alertStatus = alertStatus;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}


}
