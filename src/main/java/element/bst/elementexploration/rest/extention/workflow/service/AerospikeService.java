package element.bst.elementexploration.rest.extention.workflow.service;

public interface AerospikeService {

	String listOfAerospikeDatabases(String profileId) throws Exception;

	String listOfAerospikeTables(String profileId, String namespace) throws Exception;

	String sampledata(String workflowId, String stageId) throws Exception;

	String schema(String workflowId, String stageId) throws Exception;

}
