package element.bst.elementexploration.rest.extention.alertmanagement.dto;

/**
 * @author Prateek
 *
 */
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class AlertUpdateDto implements Serializable{

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "List of Alerts")
	private List<AlertsDto> result;
	
	@ApiModelProperty(value = "List of Entity Name")
	private List<String> failedUpdates;

	public List<AlertsDto> getResult() {
		return result;
	}

	public void setResult(List<AlertsDto> result) {
		this.result = result;
	}

	public List<String> getFailedUpdates() {
		return failedUpdates;
	}

	public void setFailedUpdates(List<String> failedUpdates) {
		this.failedUpdates = failedUpdates;
	}
	
	
}
