package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Group creation")
public class CreateGroupDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Name of the user group", required = true)
	@NotEmpty(message = "Group name can not be blank.")
	private String name;

	@ApiModelProperty(value = "Description of the user group")
	private String description;

	@ApiModelProperty(value = "Remarks on the user group")
	private String remarks;

	@ApiModelProperty(value = "Source of user group")
	private String source;

	@ApiModelProperty(value = "Color of user group")
	private String color;

	@ApiModelProperty(value = "Icon of user group")
	private String icon;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
