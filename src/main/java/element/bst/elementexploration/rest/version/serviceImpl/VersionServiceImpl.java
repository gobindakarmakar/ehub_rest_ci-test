package element.bst.elementexploration.rest.version.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.version.dao.VersionDao;
import element.bst.elementexploration.rest.version.domain.ProductVersion;
import element.bst.elementexploration.rest.version.service.VersionService;

@Service("versionService")
public class VersionServiceImpl extends GenericServiceImpl<ProductVersion, Long> implements VersionService {

	@Autowired
	public VersionServiceImpl(GenericDao<ProductVersion, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	VersionDao versionDao;

	@Override
	@Transactional("transactionManager")
	public ProductVersion getLatestVersion() {
		return versionDao.getLatestVersion();
	}

	@Override
	@Transactional("transactionManager")
	public List<ProductVersion> getAllVersions() {
		return versionDao.getAllVersions();
	}
}
