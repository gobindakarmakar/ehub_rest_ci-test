package element.bst.elementexploration.rest.usermanagement.group.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.RolesDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.Roles;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Paul Jalagari
 *
 */
@Repository("rolesDao")
public class RolesDaoImpl extends GenericDaoImpl<Roles, Long> implements RolesDao {
	
	public RolesDaoImpl() {
		super(Roles.class);
	}

	@Override
	public Roles findRole(Roles role) throws Exception {
		Roles existingRole = new Roles();
		try {

			StringBuilder hql = new StringBuilder();
			// hql.append(
			// "select distinct new
			// element.bst.elementexploration.rest.usermanagement.group.dto.RolesDto(r.roleName,r.description,r.notes)
			// ");
			if (role.getRoleId() == null) {
				hql.append("from Roles r ");
				if (role.getRoleName() != null) {
					hql.append("where r.roleName=:roleName ");
				}
				
				
			} else {

				hql.append("from Roles r ");
				if (role.getRoleName() != null) {
					hql.append("where r.roleName=:roleName ");
				}

				if (role.getRoleId() != null) {
					hql.append(" and r.roleId=:roleId ");
				}
			}
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			if (role.getRoleId() != null)
				query.setParameter("roleId", role.getRoleId());
			if (role.getRoleName() != null)
				query.setParameter("roleName", role.getRoleName());
			try{
			existingRole = (Roles) query.getSingleResult();
			}
			
			catch(NoResultException ex){
				
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, RolesDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		
		return existingRole;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Roles> getRolesOfUser(Long userId) throws Exception {

		List<Roles> rolesList = new ArrayList<>();
		try {

			StringBuilder hql = new StringBuilder();
			hql.append(
					"select distinct new element.bst.elementexploration.rest.usermanagement.group.domain.Roles(g.id, g.createdDate, "
							+ "g.modifiedDate, g.parentGroupId, g.name, g.remarks, g.active, g.createdBy, g.description, g.groupPermissions) ");
			hql.append("from Groups g ")
					.append("left outer join UserGroups ug on g.id = ug.groupId.id left outer join GroupPermission gp on g.id=gp.groupId ")
					.append("where ug.userId.userId =:userId and g.active = true");
			// .append("where ug.userId.userId =:userId");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("userId", userId);
			rolesList = (List<Roles>) query.getResultList();

		} catch (HibernateException e) {
			e.printStackTrace();
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, GroupDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}

		return rolesList;

	}

	@Override
	public Long getRolesCount() {
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = (CriteriaQuery<Long>) builder.createQuery(Long.class);
		Root<Roles> result = (Root<Roles>) criteriaQuery.from(Roles.class);
		criteriaQuery.select(builder.count(result));
		Query<Long> query = this.getCurrentSession().createQuery(criteriaQuery);
		return query.getSingleResult();
	}

	@Override
	public Roles findRoleByName(String roleName) throws Exception {
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Roles> criteriaQuery = (CriteriaQuery<Roles>) builder.createQuery(Roles.class);
		Root<Roles> result = (Root<Roles>) criteriaQuery.from(Roles.class);
		criteriaQuery.select(result);
		criteriaQuery.where(builder.equal(result.get("roleName"), roleName));
		Query<Roles> query = this.getCurrentSession().createQuery(criteriaQuery);
		return query.getSingleResult();
	}

	@Override
	public Roles getRoleByIdOrName(Long roleId, String roleName) {
		Roles roles = null;			
		try
		{
		CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Roles> criteriaQuery = (CriteriaQuery<Roles>) builder.createQuery(Roles.class);
		Root<Roles> result = (Root<Roles>) criteriaQuery.from(Roles.class);
		criteriaQuery.select(result);
		if(roleName != null)
			criteriaQuery.where(builder.equal(result.get("roleName"), roleName));
		else
			criteriaQuery.where(builder.equal(result.get("roleId"), roleId));
		
		roles = this.getCurrentSession().createQuery(criteriaQuery).getSingleResult();
		}
		catch (Exception e) {
			 ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
					this.getClass());
		}
		return roles;	
		
	}

	@Override
	public void deleteRoleById(Long roleId) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("delete from bst_um_role_permissions where roleId =:roleId");
			NativeQuery<?> query = this.getCurrentSession().createNativeQuery(hql.toString());
			query.setParameter("roleId", roleId);
			query.executeUpdate();
	    }catch (Exception e) {
		 ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,
				this.getClass());
	   }			
	}

	/*
	 * @Override public String findRoleName(List <String> roleName) throws
	 * Exception {
	 * 
	 * StringBuilder hql = new StringBuilder();
	 * // TODO Auto-generated method stub
	 * hql.append("select  ");
	 * 
	 * 
	 * return null; }
	 */

}
