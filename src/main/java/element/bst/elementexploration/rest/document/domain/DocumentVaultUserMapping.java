package element.bst.elementexploration.rest.document.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import element.bst.elementexploration.rest.document.enumType.ReadWritePermissionEnum;

@Entity
@Table(name = "bst_doc_user_mapping")
public class DocumentVaultUserMapping implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5920224103890522640L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "docuserId_pk")
	private Long docVaultUserId;

	@Column(name = "fk_doc_id")
	private Long docId;

	@Column(name = "fk_user_id")
	private Long userId;

	@Column(name = "permission")
	@Enumerated(EnumType.ORDINAL)
	private ReadWritePermissionEnum permission;

	@Column(name = "is_shared")
	private Boolean isShared;

	@Column(name = "comment")
	private String comment;

	@Column(name = "shared_on")
	private Date sharedOn;

	@Column(name = "unshared_on")
	private Date unsharedOn;

	@Column(name = "shared_by")
	private Long sharedBy;

	public Long getDocVaultUserId() {
		return docVaultUserId;
	}

	public void setDocVaultUserId(Long docVaultUserId) {
		this.docVaultUserId = docVaultUserId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ReadWritePermissionEnum getPermission() {
		return permission;
	}

	public void setPermission(ReadWritePermissionEnum permission) {
		this.permission = permission;
	}

	public Boolean getIsShared() {
		return isShared;
	}

	public void setIsShared(Boolean isShared) {
		this.isShared = isShared;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getSharedOn() {
		return sharedOn;
	}

	public void setSharedOn(Date sharedOn) {
		this.sharedOn = sharedOn;
	}

	public Date getUnsharedOn() {
		return unsharedOn;
	}

	public void setUnsharedOn(Date unsharedOn) {
		this.unsharedOn = unsharedOn;
	}

	public Long getSharedBy() {
		return sharedBy;
	}

	public void setSharedBy(Long sharedBy) {
		this.sharedBy = sharedBy;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
