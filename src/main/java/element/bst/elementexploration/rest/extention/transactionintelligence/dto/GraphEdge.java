package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.HashMap;

public class GraphEdge implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;	
	
	private String labelE;
	
	private String to;
	
	private String from;
	
	private Object properties = new HashMap<String, Object>();

	
	public GraphEdge(String id, String labelE, String to, String from) {
		super();
		this.id = id;
		this.labelE = labelE;
		this.to = to;
		this.from = from;
	}
	
	public GraphEdge(String id, String labelE, String to, String from, Object properties) {
		super();
		this.id = id;
		this.labelE = labelE;
		this.to = to;
		this.from = from;
		this.properties = properties;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabelE() {
		return labelE;
	}

	public void setLabelE(String labelE) {
		this.labelE = labelE;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Object getProperties() {
		return properties;
	}

	public void setProperties(Object properties) {
		this.properties = properties;
	}
	
}
