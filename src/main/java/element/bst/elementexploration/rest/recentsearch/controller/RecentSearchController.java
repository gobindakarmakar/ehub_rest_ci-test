package element.bst.elementexploration.rest.recentsearch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.recentsearch.domain.RecentSearchs;
import element.bst.elementexploration.rest.recentsearch.dto.RecentSearchDto;
import element.bst.elementexploration.rest.recentsearch.dto.SearchDto;
import element.bst.elementexploration.rest.recentsearch.service.RecentSearchService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Viswanath
 *
 */
@Api(tags = { "Recent Search Apis" }, description = "Manage recent searchs")
@RestController
@RequestMapping("/api/recentSearch")
public class RecentSearchController extends BaseController {

	@Autowired
	private RecentSearchService recentSearchService;

	@ApiOperation("Search all")
	@ApiResponses(value = { @ApiResponse(code = 200, response = SearchDto.class, message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/search", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> searchAll(@RequestParam("token") String token,
			@RequestParam String searchKeyword, HttpServletRequest request) throws Exception {
		SearchDto searchDto = recentSearchService.getSearchs(searchKeyword);

		return new ResponseEntity<>(searchDto, HttpStatus.OK);
	}

	@ApiOperation("save as recent search")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = "Saved as a recent search"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveAsRecentSearch", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveSearch(@RequestParam("token") String token, @RequestBody String searchQuery)
			throws Exception {
		RecentSearchs recentSearchs = recentSearchService.saveSearch(searchQuery,getCurrentUserId());
		if (recentSearchs != null) {
			return new ResponseEntity<>(recentSearchs, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Recent search not saved", HttpStatus.OK);

		}
	}

	@ApiOperation("get recent search")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = RecentSearchDto.class, responseContainer = "List", message = "Operation successful."),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getRecentSearchs", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getRecentSearch(@RequestParam("token") String token, HttpServletRequest request)
			throws Exception {
		Long userId=getCurrentUserId();
		List<RecentSearchDto> recentSearchDto = recentSearchService.getRecentSearchs(userId);
		return new ResponseEntity<>(recentSearchDto, HttpStatus.OK);
	}
}
