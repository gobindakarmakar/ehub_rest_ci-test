package element.bst.elementexploration.rest.notification.dao;

import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.notification.domain.NotificationMapping;

/**
 * @author Viswanath
 *
 */

public interface NotificationMappingDao extends GenericDao<NotificationMapping, Long> {

	List<Long> addEventParticipants(Long eventId, List<Long> participants);

	void removeEventParticipants(Long eventId, List<Long> participants);

	List<Long> getEventParticipants(Long eventId);

}
