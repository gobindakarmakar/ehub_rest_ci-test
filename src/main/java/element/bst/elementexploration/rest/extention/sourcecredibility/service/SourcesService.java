package element.bst.elementexploration.rest.extention.sourcecredibility.service;

import java.util.List;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Sources;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceCategoryDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourceFilterDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface SourcesService extends GenericService<Sources, Long> {

	public boolean saveMasterDataClassifications() throws Exception;

	public List<SourcesDto> getSources(Integer pageNumber, Integer recordsPerPage,Boolean visible,
			Long classificationId, String orderBy,String orderIn,List<SourceFilterDto> sourceFilterDto,Long subClassifcationId,Boolean isAllSourcesRequired);

	public long getSourcesCount(Integer pageNumber, Integer recordsPerPage, Boolean visible,
			Long classificationId, String orderBy,String orderIn,List<SourceFilterDto> sourceFilterDto,Long subClassifcationId);

	public boolean updateSource(SourcesDto sourceDto, Long currentUserId) throws Exception;

	boolean saveSource(SourcesDto sourcesDto, Long userId);

	boolean checkSourceExist(String identifier, String url);

	public void saveCompanyWebsiteSource(String identifiervalue, String url, String companyHouseObjecEntityName,
			String sourceType, Long userId);
	
	public List<Sources> fetchClassifctaionSources(String classiifcationName);

	public String getInfoFromDomains(String domain,Integer count) throws Exception;

	public String getSourceCategoryBySourceName(String string);

	List<Sources> fetchClassifctaionSourcesWithCredibility(String classiifcationName);

	boolean checkSourceExistWithSourceNameAndEntityId(String identifier, String sourceName);


}
