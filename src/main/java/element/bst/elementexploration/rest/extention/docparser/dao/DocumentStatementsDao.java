package element.bst.elementexploration.rest.extention.docparser.dao;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentStatements;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

/**
 * @author Suresh
 *
 */
public interface DocumentStatementsDao extends GenericDao<DocumentStatements, Long>{
	
	public DocumentStatements getDocumentStatementById(Long id);
	
}
