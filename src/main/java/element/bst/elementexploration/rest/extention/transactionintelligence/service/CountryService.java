package element.bst.elementexploration.rest.extention.transactionintelligence.service;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author suresh
 *
 */
public interface CountryService extends GenericService<Country,Long>{
	
	boolean saveCountryData(MultipartFile file) throws IOException ;
	Boolean saveCountryDataxl(File file) throws IOException,Exception;
	Boolean auditCountryData() throws IOException,Exception;
	
}
