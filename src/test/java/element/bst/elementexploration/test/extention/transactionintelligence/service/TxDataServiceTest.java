package element.bst.elementexploration.test.extention.transactionintelligence.service;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AccountDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.AlertDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CountryDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.CustomerDetailsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.TxDataDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Account;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Alert;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.Country;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerAddress;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.CustomerDetails;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.TransactionsData;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.AlertDashBoardRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyGraphDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CounterPartyNotiDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.CountryAggDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.FilterDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.InputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.OutputStatsResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.ResponseDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TopAmlCustomersDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TotalDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.dto.TxEntityRespDto;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountOwnershipType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AccountType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.AlertStatus;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerTitle;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.CustomerType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.EntityType;
import element.bst.elementexploration.rest.extention.transactionintelligence.enums.Gender;
import element.bst.elementexploration.rest.extention.transactionintelligence.serviceImpl.TxDataServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TxDataServiceTest {

	@Mock
	TxDataDao txDataDaoMock;

	@Mock
	CustomerDetailsDao customerDetailsDaoMock;

	@Mock
	CountryDao countryDaoMock;
	
	@Mock
	Random randomMock;

	@Mock
	AlertDao alertDaoMock;

	@Mock
	AccountDao accountDaoMock;

	@InjectMocks
	private TxDataServiceImpl txDataService;

	public ResponseDto responseDto;

	public List<TxEntityRespDto> listTxDtos;

	public InputStatsResponseDto inputStatsResponseDto;

	public List<CounterPartyDto> counterPartyDto;

	public List<TransactionsData> transactionsDataList;

	public OutputStatsResponseDto outputStatsResponseDto;

	public List<CounterPartyNotiDto> inputCounterpartyList;

	public CounterPartyGraphDto counterPartyGraphDto;

	public List<TopAmlCustomersDto> topAmlCustomersDtoList;

	public List<String> entities = new ArrayList<String>();

	public List<CounterPartyDto> counterDtosList = new ArrayList<CounterPartyDto>();

	public List<CountryAggDto> countryAggDtoList = new ArrayList<CountryAggDto>();

	List<AlertDashBoardRespDto> alertDashBoardRespDtoList;

	@Before
	public void setupMock() {
		responseDto = new ResponseDto();
		TxEntityRespDto txEntityRespDto1 = new TxEntityRespDto();
		TxEntityRespDto txEntityRespDto2 = new TxEntityRespDto();
		listTxDtos = new ArrayList<TxEntityRespDto>();
		listTxDtos.add(txEntityRespDto1);
		listTxDtos.add(txEntityRespDto2);
		responseDto.setListTxDtos(listTxDtos);
		inputStatsResponseDto = new InputStatsResponseDto();
		CustomerDetails customerDetails = new CustomerDetails();
		CounterPartyDto counterPartyDto1 = new CounterPartyDto("test1", customerDetails, 2l, 500, 100000, 5000000,
				250000);
		CounterPartyDto counterPartyDto2 = new CounterPartyDto("test2", customerDetails, 3l, 100, 10000, 5000000,
				250000);
		counterPartyDto = new ArrayList<CounterPartyDto>();
		counterPartyDto.add(counterPartyDto1);
		counterPartyDto.add(counterPartyDto2);
		inputStatsResponseDto.setCounterPartyDto(counterPartyDto);
		TotalDto totalDto = new TotalDto(3000, 10l, 25000, 1000, 50000);
		inputStatsResponseDto.setTotalDto(totalDto);
		transactionsDataList = new ArrayList<TransactionsData>();
		TransactionsData transactionsData = new TransactionsData();
		TransactionsData transactionsDataNew = new TransactionsData();
		transactionsDataList.add(transactionsData);
		transactionsDataList.add(transactionsDataNew);
		outputStatsResponseDto = new OutputStatsResponseDto();
		outputStatsResponseDto.setCounterPartyDto(counterPartyDto);
		outputStatsResponseDto.setTotalDto(totalDto);
		inputCounterpartyList = new ArrayList<CounterPartyNotiDto>();
		CounterPartyNotiDto counterPartyNotiDto = new CounterPartyNotiDto(1l, "US", 50000.00, 3l);
		CounterPartyNotiDto counterPartyNotiDtoNew = new CounterPartyNotiDto(2l, "UK", 100000.00, 5l);
		inputCounterpartyList.add(counterPartyNotiDto);
		inputCounterpartyList.add(counterPartyNotiDtoNew);
		counterPartyGraphDto = new CounterPartyGraphDto();
		counterPartyGraphDto.setInputCounterpartyList(inputCounterpartyList);
		counterPartyGraphDto.setOutputCounterpartyList(inputCounterpartyList);
		topAmlCustomersDtoList = new ArrayList<TopAmlCustomersDto>();
		TopAmlCustomersDto topAmlCustomersDto1 = new TopAmlCustomersDto("testcase1", 3l);
		TopAmlCustomersDto topAmlCustomersDto2 = new TopAmlCustomersDto("testcase2", 5l);
		topAmlCustomersDtoList.add(topAmlCustomersDto1);
		topAmlCustomersDtoList.add(topAmlCustomersDto2);
		entities.add("123");
		CounterPartyDto counterPartyDto = new CounterPartyDto();
		counterPartyDto.setCustomerNumer("test");
		counterDtosList.add(counterPartyDto);
		CountryAggDto countryAggDto = new CountryAggDto();
		countryAggDto.setCountryCode("IN");
		countryAggDtoList.add(countryAggDto);

		alertDashBoardRespDtoList = new ArrayList<>();
		AlertDashBoardRespDto alertDashBoardRespDto = new AlertDashBoardRespDto(1L, "test", new Date(),
				new Double(1000), "focal Entity id", "search name", CustomerType.IND, "scenario", new Date(), 33L, 20);
		AlertDashBoardRespDto alertDashBoardRespDtoNew = new AlertDashBoardRespDto(2L, "test", new Date(),
				new Double(1000), "focal Entity id", "search name", CustomerType.IND, "scenario", new Date(), 33L, 20);
		alertDashBoardRespDtoList.add(alertDashBoardRespDto);
		alertDashBoardRespDtoList.add(alertDashBoardRespDtoNew);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void fetchAllTxsBasedOnEntityTest() throws IllegalAccessException, InvocationTargetException {
		List<Account> accountlist = new ArrayList<Account>();
		CustomerDetails customerDetail = new CustomerDetails(1L, "123", CustomerType.IND, CustomerTitle.MR, Gender.M,
				new Date(), "UK", "UK", "123dx", new Date(), "Manufacturing", "Tech", "UK", "analyst", "analyst", "",
				"", "", "", "UK", "UK", " ", new Date(), new ArrayList<CustomerAddress>(), new ArrayList<Account>(),
				700000D, 800000D, "", "", "", "", "UK", "manufacturing", "card", 55d, "");
		Account account = new Account(1L, "123456", AccountOwnershipType.IND, "123", new Date(), "UK", " ",
				AccountType.CUR, "1589368", "12", "USD", 100000D, customerDetail, "US bank");
		accountlist.add(account);

		List<TransactionsData> transactionsDataList = new ArrayList<TransactionsData>();
		TransactionsData transactionData = new TransactionsData(1L, "123456", "789456", "wired", "swift", 10000D, "USD",
				new Date(), "", 1L, 1L, "Bank", "analyst", "INC", "US", "US", "", "", "", "test");
		transactionsDataList.add(transactionData);
		Country country = new Country();
		country.setIso2Code("US");
		Mockito.when(customerDetailsDaoMock.getCustomerDetails(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(customerDetail);
		Mockito.when(customerDetailsDaoMock.find(Mockito.anyLong())).thenReturn(customerDetail);
		Mockito.when(accountDaoMock.fetchAccounts(Mockito.anyString())).thenReturn(accountlist);
		Mockito.when(txDataDaoMock.getTransaction(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString())).thenReturn(transactionsDataList);
		Mockito.when(alertDaoMock.findAlertByAlertTransactionId(Mockito.anyLong())).thenReturn(true);
		Mockito.when(countryDaoMock.getCountry(Mockito.anyString())).thenReturn(country);
		List<String> entityIds = new ArrayList<String>();
		entityIds.add("1456");
		ResponseDto newResponseDto = txDataService.fetchAllTxsBasedOnEntity(entityIds, "internal", "1000", "us.uk.sg",
				"500", 1L, 1L);
		assertEquals(1, newResponseDto.getListTxDtos().size());
	}

	@Test
	public void inputStatsTest() throws IllegalAccessException, InvocationTargetException {
		Mockito.when(txDataDaoMock.getTotalAggregateForOutputStats(Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyListOf(String.class), Mockito.anyString())).thenReturn(new TotalDto());
		Mockito.when(txDataDaoMock.getCounterPartiesAggregate(Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyString())).thenReturn(counterDtosList);
		Mockito.when(txDataDaoMock.getCountryAggregate(Mockito.anyListOf(String.class), Mockito.anyListOf(String.class),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(countryAggDtoList);
		Mockito.when(customerDetailsDaoMock.fetchCustomerDetails(Mockito.anyString()))
				.thenReturn(new CustomerDetails());
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(new Country());
		InputStatsResponseDto newInputStatsResponseDto = txDataService.inputStats(entities, "test", "5", "10", "5000",
				"uk,in,usa", "internal");
		assertEquals(1, newInputStatsResponseDto.getCounterPartyDto().size());
	}

	@Test
	public void inputStats_whenUnknowTest() throws IllegalAccessException, InvocationTargetException {
		Mockito.when(txDataDaoMock.getTotalAggregateForOutputStats(Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyListOf(String.class), Mockito.anyString())).thenReturn(new TotalDto());
		counterDtosList.get(0).setCustomerNumer("Unknown");
		Mockito.when(txDataDaoMock.getCounterPartiesAggregate(Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyString())).thenReturn(counterDtosList);
		countryAggDtoList.get(0).setCountryCode("Unknown");
		Mockito.when(txDataDaoMock.getCountryAggregate(Mockito.anyListOf(String.class), Mockito.anyListOf(String.class),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(countryAggDtoList);
		Mockito.when(customerDetailsDaoMock.fetchCustomerDetails(Mockito.anyString()))
				.thenReturn(new CustomerDetails());
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(new Country());
		InputStatsResponseDto newInputStatsResponseDto = txDataService.inputStats(entities, "Unknown", "5", "10",
				"5000", "uk,in,usa", "internal");
		assertEquals(1, newInputStatsResponseDto.getCounterPartyDto().size());
	}

	@Test
	public void getAllTransactionsTest() {
		Mockito.when(txDataDaoMock.getAllTransactions()).thenReturn(transactionsDataList);
		List<TransactionsData> newTransactionsDataList = txDataService.getAllTransactions();
		assertEquals(2, newTransactionsDataList.size());
	}

	@Test
	public void outputStatsTest() throws IllegalAccessException, InvocationTargetException {
		Mockito.when(txDataDaoMock.getTotalAggregateForOutputStats(Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyListOf(String.class), Mockito.anyString())).thenReturn(new TotalDto());
		Mockito.when(txDataDaoMock.getCounterPartiesAggregateForOutputStats(Mockito.anyListOf(String.class),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(counterDtosList);
		Mockito.when(txDataDaoMock.getCountryAggregateForOutputStats(Mockito.anyListOf(String.class),
				Mockito.anyListOf(String.class), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
				.thenReturn(countryAggDtoList);
		Mockito.when(customerDetailsDaoMock.fetchCustomerDetails(Mockito.anyString()))
				.thenReturn(new CustomerDetails());
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(new Country());
		OutputStatsResponseDto newOutputStatsResponseDto = txDataService.outputStats(entities, "test", "5", "10",
				"5000", "uk,in,usa", "internal");
		assertEquals(1, newOutputStatsResponseDto.getCounterPartyDto().size());
	}

	@Test
	public void outputStats_whenUnkonwTest() throws IllegalAccessException, InvocationTargetException {
		Mockito.when(txDataDaoMock.getTotalAggregateForOutputStats(Mockito.anyListOf(String.class), Mockito.anyString(),
				Mockito.anyListOf(String.class), Mockito.anyString())).thenReturn(new TotalDto());
		Mockito.when(txDataDaoMock.getCounterPartiesAggregateForOutputStats(Mockito.anyListOf(String.class),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyListOf(String.class),
				Mockito.anyString(), Mockito.anyString())).thenReturn(counterDtosList);
		Mockito.when(txDataDaoMock.getCountryAggregateForOutputStats(Mockito.anyListOf(String.class),
				Mockito.anyListOf(String.class), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
				.thenReturn(countryAggDtoList);
		Mockito.when(customerDetailsDaoMock.fetchCustomerDetails(Mockito.anyString()))
				.thenReturn(new CustomerDetails());
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(new Country());
		OutputStatsResponseDto newOutputStatsResponseDto = txDataService.outputStats(entities, "Unknown", "5", "10",
				"5000", "uk,in,usa", "internal");
		assertEquals(1, newOutputStatsResponseDto.getCounterPartyDto().size());
	}

	@Test
	public void counterPartyLocationsPlot_whenBaselAMLIndex2017Is_lessThan40Test()
			throws IllegalAccessException, InvocationTargetException, ParseException {
		Country country = new Country();
		country.setBaselAMLIndex2017(30);
		Mockito.when(txDataDaoMock.counterPartyLocationsPlot(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(inputCounterpartyList);
		Mockito.when(countryDaoMock.find(Mockito.anyLong())).thenReturn(country);
		CounterPartyGraphDto newCounterPartyGraphDto = txDataService.counterPartyLocationsPlot(new Date().toString(),
				new Date().toString(), "internal", new FilterDto());
		assertEquals(2, newCounterPartyGraphDto.getInputCounterpartyList().size());
	}

	@Test
	public void counterPartyLocationsPlot_whenBaselAMLIndex2017Is_lessThan70Test()
			throws IllegalAccessException, InvocationTargetException, ParseException {
		Country country = new Country();
		country.setBaselAMLIndex2017(60);
		Mockito.when(txDataDaoMock.counterPartyLocationsPlot(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(inputCounterpartyList);
		Mockito.when(countryDaoMock.find(Mockito.anyLong())).thenReturn(country);
		CounterPartyGraphDto newCounterPartyGraphDto = txDataService.counterPartyLocationsPlot(new Date().toString(),
				new Date().toString(), "internal", new FilterDto());
		assertEquals(2, newCounterPartyGraphDto.getInputCounterpartyList().size());
	}

	@Test
	public void counterPartyLocationsPlot_whenBaselAMLIndex2017Is_greaterThan70Test()
			throws IllegalAccessException, InvocationTargetException, ParseException {
		Country country = new Country();
		country.setBaselAMLIndex2017(75);
		Mockito.when(txDataDaoMock.counterPartyLocationsPlot(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(inputCounterpartyList);
		Mockito.when(countryDaoMock.find(Mockito.anyLong())).thenReturn(country);
		CounterPartyGraphDto newCounterPartyGraphDto = txDataService.counterPartyLocationsPlot(new Date().toString(),
				new Date().toString(), "internal", new FilterDto());
		assertEquals(2, newCounterPartyGraphDto.getInputCounterpartyList().size());
	}

	@Test
	public void amlTopCustomersTest() throws IllegalAccessException, InvocationTargetException, ParseException {
		Mockito.when(
				txDataDaoMock.amlTopCustomers(Mockito.anyString(), Mockito.anyString(), Mockito.any(FilterDto.class)))
				.thenReturn(topAmlCustomersDtoList);
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setDisplayName("test");
		Mockito.when(customerDetailsDaoMock.fetchCustomerDetails(Mockito.anyString())).thenReturn(customerDetails);
		List<TopAmlCustomersDto> newTopAmlCustomersDtoList = txDataService.amlTopCustomers(
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
				new SimpleDateFormat("dd-MM-yyyy").format(new Date()), new FilterDto());
		assertEquals(2, newTopAmlCustomersDtoList.size());
	}

	@Test
	public void updateTransactionsTest() {
		Mockito.when(txDataDaoMock.findAll()).thenReturn(transactionsDataList);
		Mockito.doNothing().when(txDataDaoMock).saveOrUpdate(Mockito.any(TransactionsData.class));
		List<Alert> alertList = new ArrayList<Alert>();
		Alert alert = new Alert(1L, "Test", "1234", "SG", "testing", new Date(), "test", 1, "1", "3", 1L,
				"alert comment", "Internal", AlertStatus.OPEN, "Credit card", 1L, "test", "grater amount", 85, 15000D,
				EntityType.PERSON);
		alertList.add(alert);
		Mockito.when(alertDaoMock.fetchAlertSummaryList(Mockito.anyLong())).thenReturn(alertList);
		Mockito.when(alertDaoMock.find(Mockito.anyLong())).thenReturn(alert);
		Mockito.doNothing().when(alertDaoMock).saveOrUpdate(Mockito.any(Alert.class));
		boolean status = txDataService.updateTransactions();
		assertEquals(true, status);
	}

	@Test
	public void alterDatabseTest() {
		Mockito.when(txDataDaoMock.findAll()).thenReturn(transactionsDataList);
		Mockito.doNothing().when(txDataDaoMock).saveOrUpdate(Mockito.any(TransactionsData.class));
		boolean status = txDataService.alterDatabse();
		assertEquals(true, status);
	}

	@Test
	public void auditColumnstTest() {
		transactionsDataList.get(0).setTransProductType("Ach in");
		transactionsDataList.get(1).setTransProductType("Cash out");
		TransactionsData transactionsData = new TransactionsData();
		transactionsData.setTransProductType("Currency Exchange");
		transactionsDataList.add(transactionsData);
		Mockito.when(txDataDaoMock.findAll()).thenReturn(transactionsDataList);
		Mockito.doNothing().when(txDataDaoMock).saveOrUpdate(Mockito.any(TransactionsData.class));
		boolean status = txDataService.auditColumns();
		assertEquals(true, status);
	}

	@Test
	public void updatePassportTest() {
		Mockito.when(txDataDaoMock.findAll()).thenReturn(transactionsDataList);
		Mockito.doNothing().when(txDataDaoMock).saveOrUpdate(Mockito.any(TransactionsData.class));
		boolean status = txDataService.updatePassport();
		assertEquals(true, status);
	}

	@Test
	public void fetchAlertsByNumberTest() {
		Mockito.when(txDataDaoMock.fetchAlertsByNumber(Mockito.anyString())).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> alertDashBoardRespDtoList = txDataService.fetchAlertsByNumber("123567");
		assertEquals(2, alertDashBoardRespDtoList.size());
	}

	@Test
	public void fetchAlertsTest() {
		Mockito.when(txDataDaoMock.fetchAlerts()).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> alertDashBoardRespDtoList = txDataService.fetchAlerts();
		assertEquals(2, alertDashBoardRespDtoList.size());
	}

	@Test
	public void getEntityBasedAlertsTest() {
		Mockito.when(txDataDaoMock.getEntityBasedAlerts(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(alertDashBoardRespDtoList);
		List<AlertDashBoardRespDto> alertDashBoardRespDtoList = txDataService
				.getEntityBasedAlerts(new Date().toString(), new Date().toString(), 1, 12, "desc", "trasaction");
		assertEquals(2, alertDashBoardRespDtoList.size());
	}

	@Test
	public void getEntityBasedAlertsCountTest() {
		Mockito.when(
				txDataDaoMock.getEntityBasedAlertsCount(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
				.thenReturn(1L);
		Long count = txDataService.getEntityBasedAlertsCount(new Date().toString(), new Date().toString(), "2343");
		assertEquals(new Long("1"), count);
	}

}
