package element.bst.elementexploration.rest.usermanagement.group.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.usermanagement.group.domain.GroupAlert;
import element.bst.elementexploration.rest.usermanagement.group.dto.GroupAlertSettingDto;
import element.bst.elementexploration.rest.usermanagement.group.service.GroupAlertService;
import element.bst.elementexploration.rest.usermanagement.user.dto.GenericReturnObject;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Paul Jalagari
 *
 */
@Api(tags = { "Group Alert API" })
@RestController
@RequestMapping("/api/groupAlert")
public class GroupAlertController extends BaseController {

	@Autowired
	private GroupAlertService groupAlertService;

	@ApiOperation("Adds Group-Alert Settings")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = ResponseMessage.class, message = ElementConstants.GROUP_UPDATED_SUCCESSFULLY_MSG),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.GROUP_NOT_FOUND_MSG),
			@ApiResponse(code = 401, response = ResponseMessage.class, message = ElementConstants.USER_NOT_ADMIN),
			@ApiResponse(code = 409, response = ResponseMessage.class, message = ElementConstants.GROUP_NAME_EXIST),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/saveGroupAlertSetting", produces = { "application/json; charset=UTF-8" }, consumes = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> groupAlertSetting(@RequestParam(value = "groupId") Long groupId,
			@RequestParam("token") String adminToken,
			@Valid @RequestBody GroupAlertSettingDto groupAlertSettingDto, BindingResult results,
			HttpServletRequest request) {
		GenericReturnObject message = new GenericReturnObject();
		List<GroupAlert> status = new ArrayList<>();
		Long currentUserId = getCurrentUserId();
		if (!results.hasErrors()) {
			try {
			status = groupAlertService.saveGroupAlertSetting(groupAlertSettingDto, groupId, currentUserId);
			}catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			message.setResponseMessage(ElementConstants.MISSING_REQUIRED_FIELDS_MSG);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		if (status != null && status.size() != 0) {
			message.setResponseMessage(ElementConstants.ADD_ALERT_GROUP_SETTINGS_SUCCESSFUL);
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			message.setData(status);
		} else {
			message.setResponseMessage(ElementConstants.ADD_ALERT_GROUP_SETTINGS_FAILED);
			message.setStatus(ElementConstants.ERROR_STATUS);
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@ApiOperation("Gets Group Alerts Settings")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = GenericReturnObject.class, message = "Operation successful"),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/getGroupAlertSettings", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupAlertSettings(@RequestParam Long groupId, @RequestParam String token)
			throws Exception {
		GenericReturnObject message = new GenericReturnObject();
		List<GroupAlert> groupAlerts = groupAlertService.findGroupAlertsByGroup(groupId);
		JSONArray groupAlertsArray = null;
		if (groupAlerts != null && groupAlerts.size() > 0) {
			groupAlertsArray = new JSONArray();
			for (GroupAlert eachObj : groupAlerts) {
				JSONObject obj = new JSONObject(eachObj);
				if (obj.has("groupId"))
					obj.remove("groupId");
				groupAlertsArray.put(obj);
			}
			message.setResponseMessage(ElementConstants.FETCH_GROUP_ALERT_SETTINGS_SUCCESSFUL);
			message.setData(groupAlertsArray.toString());
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		else
		{
			message.setResponseMessage(ElementConstants.FETCH_GROUP_ALERT_SETTINGS_SUCCESSFUL);
			message.setData(groupAlerts.toString());
			message.setStatus(ElementConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}	
	}
}
