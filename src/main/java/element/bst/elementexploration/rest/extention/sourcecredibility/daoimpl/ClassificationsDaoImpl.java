package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.sourcecredibility.dao.ClassificationsDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.Classifications;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author suresh
 *
 */
@Repository("classificationsDao")
public class ClassificationsDaoImpl extends GenericDaoImpl<Classifications, Long> implements ClassificationsDao {

	public ClassificationsDaoImpl() {
		super(Classifications.class);
	}

	@Override
	public Classifications fetchClassification(String name) {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("from Classifications a where a.classifcationName =:name ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("name", name);
			Classifications classifications = (Classifications) query.getSingleResult();
			if (classifications != null)
				return classifications;
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, ClassificationsDaoImpl.class);
			throw new FailedToExecuteQueryException();
		}
		return null;
	}

}
