package element.bst.elementexploration.rest.extention.gridview.dao;

import element.bst.elementexploration.rest.extention.gridview.domain.GridView;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface GridViewDao extends GenericDao<GridView, Long>{

	boolean deleteGridView(Long gridViewId, Long userId) throws Exception;


}
