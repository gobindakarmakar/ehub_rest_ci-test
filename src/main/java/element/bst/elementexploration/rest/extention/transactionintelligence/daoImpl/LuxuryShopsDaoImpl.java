package element.bst.elementexploration.rest.extention.transactionintelligence.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.transactionintelligence.dao.LuxuryShopsDao;
import element.bst.elementexploration.rest.extention.transactionintelligence.domain.LuxuryShops;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("luxuryShopsDao")
public class LuxuryShopsDaoImpl  extends GenericDaoImpl<LuxuryShops, Long> implements LuxuryShopsDao{

	public LuxuryShopsDaoImpl() {
		super(LuxuryShops.class);
		
	}

	@Override
	public void getAllLuxuryShops() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public LuxuryShops getLuxuryShop(String shopName, Boolean shopType) {
		LuxuryShops luxuryShops=new LuxuryShops();
		StringBuilder queryBuilder = new StringBuilder();
		if(shopType){
			queryBuilder.append("from LuxuryShops ls");
			queryBuilder.append(" where ls.shopName= :shopName");
		}
		else{
			queryBuilder.append("from LuxuryShops ls");
			queryBuilder.append(" where ls.webPage= :shopName");
		}
		
		try {
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			query.setParameter("shopName", shopName);
			luxuryShops = (LuxuryShops) query.getSingleResult();
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
			
			
		}
		return luxuryShops;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getLuxuryShopNames() {
		List<String> luxuryShopNames=new ArrayList<String>();
		
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select ls.shopName from LuxuryShops ls");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			luxuryShopNames=(List<String>) query.getResultList();
			
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
		return luxuryShopNames;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getLuxuryShopWebsites() {
List<String> luxuryShopWebsites=new ArrayList<String>();
		
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("select ls.webPage from LuxuryShops ls");
			Query<?> query = this.getCurrentSession().createQuery(queryBuilder.toString());
			luxuryShopWebsites=(List<String>) query.getResultList();			
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			
			throw new FailedToExecuteQueryException("Failed to fetch transactions. Reason : " + e.getMessage());
		}
		return luxuryShopWebsites;
	}
	

	
}
