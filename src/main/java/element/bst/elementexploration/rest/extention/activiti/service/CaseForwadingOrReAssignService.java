package element.bst.elementexploration.rest.extention.activiti.service;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import element.bst.elementexploration.rest.casemanagement.enumType.StatusEnum;
import element.bst.elementexploration.rest.casemanagement.service.CaseAnalystMappingService;
import element.bst.elementexploration.rest.usermanagement.user.domain.User;
import element.bst.elementexploration.rest.usermanagement.user.service.UserService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Component("caseForwadingOrReAssignService")
public class CaseForwadingOrReAssignService implements JavaDelegate {

	@Autowired
	private CaseAnalystMappingService caseAnalystMappingService;

	@Autowired
	private UserService userService;

	@Override
	public void execute(DelegateExecution execution) {
		String caseId = (String) execution.getVariable("caseId");
		String currentUserId = (String) execution.getVariable("userId");
		String newUserId = (String) execution.getVariable("newUserId");
		String statusComment = (String) execution.getVariable("statusComment");
		String reassign = (String) execution.getVariable("reassign");
		String forward = (String) execution.getVariable("forward");
		String statusFrom = (String) execution.getVariable("statusFrom");
		String statusTo = (String) execution.getVariable("statusTo");
		try {
			if (reassign != null) {
				caseAnalystMappingService.caseReassignmentBpm(new Long(caseId), new Long(currentUserId),
						new Long(newUserId), statusComment);
				User newUser = userService.find(new Long(newUserId));
				execution.setVariable("userId", newUser.getUserId().toString());
				execution.setVariable("assignedUser", newUser.getEmailAddress());
				execution.setVariable("newUser", newUser.getFirstName());
			}
			if (forward != null) {
				caseAnalystMappingService.caseForwardingBpm(new Long(caseId), new Long(currentUserId),
						new Long(newUserId), statusComment);
				User newUser = userService.find(new Long(newUserId));
				execution.setVariable("userId", newUser.getUserId().toString());
				execution.setVariable("assignedUser", newUser.getEmailAddress());
				execution.setVariable("newUser", newUser.getFirstName());
			}
			if ("focus".equalsIgnoreCase(statusTo)) {
				caseAnalystMappingService.addStatusFocusBpm(new Long(caseId), new Long(currentUserId));
				execution.setVariable("statusFrom", StatusEnum.getStatusEnumByIndex(new Integer(statusFrom)).getName());
			}
		
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Exception", e, CaseForwadingOrReAssignService.class);
		}
	}

}
