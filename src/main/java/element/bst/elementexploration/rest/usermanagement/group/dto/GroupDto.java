package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Group")
public class GroupDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 957321153960166215L;

	@ApiModelProperty(value = "ID of the user group")
	private Long userGroupId;
	
	@ApiModelProperty(value = "Remarks on the user group")
	private String remarks;
	
	@ApiModelProperty(value = "Description of the user group")
	private String description;
	
	@ApiModelProperty(value = "Name of the user group",required=true)
	@NotEmpty(message = "Group name can not be blank.")
	private String name;

	public Long getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
