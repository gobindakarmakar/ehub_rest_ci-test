package element.bst.elementexploration.rest.extention.graph.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.NoDataFoundException;
import element.bst.elementexploration.rest.extention.graph.service.GraphService;
import element.bst.elementexploration.rest.util.ServiceCallHelper;

@Service("graphService")
public class GraphServiceImpl implements GraphService {
	
	@Value("${bigdata_graph_url}")
	private String BIG_DATA_GRAPH_URL;
	
	@Value("${workflow_extention_url}")
	private String WORKFLOW_EXTENSION_URL;

	@Override
	public String getAllDataFineDataFromBigDataServer(String searchQuery, Integer depth) throws Exception {

		if (depth == null || depth < 2)
			depth = 2;

		String url = null;
		if (searchQuery != null) {
			String searchQueryEncode = java.net.URLEncoder.encode(searchQuery, "UTF-8").replaceAll("\\+", "%20");
			url = BIG_DATA_GRAPH_URL + "?q=" + searchQueryEncode + "&n=" + depth;
		}
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}	
	}

	@Override
	public String getCyberSecurity(String jsonString) throws Exception {
		String url = WORKFLOW_EXTENSION_URL + "/graph/api/graph/cybersecurity";
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonString);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			String response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
				return response;
			} else {
				throw new NoDataFoundException(ElementConstants.NO_DATA_FOUND_MSG);
			}
		} else {
			throw new Exception(ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG);
		}
	}

}
