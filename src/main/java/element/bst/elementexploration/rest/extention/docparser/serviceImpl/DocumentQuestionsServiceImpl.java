package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.docparser.dao.DocumentQuestionsDao;
import element.bst.elementexploration.rest.extention.docparser.dao.DocumentTemplateMappingDao;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentQuestions;
import element.bst.elementexploration.rest.extention.docparser.domain.DocumentTemplateMapping;
import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentQuestionsService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.enumType.LoggingEventType;
import element.bst.elementexploration.rest.logging.event.LoggingEvent;

/**
 * @author suresh
 *
 */
@Service("documentQuestionsService")
public class DocumentQuestionsServiceImpl extends GenericServiceImpl<DocumentQuestions, Long>
		implements DocumentQuestionsService {
	@Autowired
	public DocumentQuestionsServiceImpl(GenericDao<DocumentQuestions, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	DocumentQuestionsDao documentQuestionsDao;
	
	@Autowired
	DocumentTemplateMappingDao templateMappingDao;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Override
	@Transactional("transactionManager")
	public int findQuestions(long docId) 
	{
		int count = 0;
		DocumentTemplateMapping documentTemplateMapping = templateMappingDao.getDocumentTemplateMappingByDocId(docId);
		if(documentTemplateMapping!=null)
			count = documentQuestionsDao.findQuestions(documentTemplateMapping.getTemplateId().getId());
		return count;
	}

	@Override
	public List<DocumentQuestions> getListOfControlQuestion() {

		List<DocumentQuestions> documentQuestionsList = new ArrayList<DocumentQuestions>();
		for (int i = 0; i < 4; i++) {
			DocumentQuestions documentQuestion = new DocumentQuestions();
			documentQuestion.setContentType("SUB_PARAGRAPH");
			documentQuestion.setLevel1Code("BST.1");
			documentQuestion.setLevel3Title("Adverse cyber events");
			documentQuestion.setLevel3Description(
					"Data from news monitoring collecting cyber events in the domain relevant to company activities:(location,industry,technology)");
			documentQuestion.setOSINT(true);
			documentQuestion.setAnswerType(AnswerType.TEXT);
			documentQuestion.setQuestionStatus(true);
			documentQuestion.setShowInUI(false);
			documentQuestion.setLevel1Title("Adverse cyber events");

			if (i == 0) {
				documentQuestion.setPrimaryIsoCode("BST.1.1");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Was company involved in cyber incidents");
				documentQuestionsList.add(documentQuestion);
			}
			if (i == 1) {
				documentQuestion.setPrimaryIsoCode("BST.1.2");
				documentQuestion.setHasIsoCode(true);
				documentQuestion
						.setName("How many attacks happened on the companies in the same industry in past 6 months");
				documentQuestionsList.add(documentQuestion);
			}
			if (i == 2) {
				documentQuestion.setPrimaryIsoCode("BST.1.3");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setShowInUI(true);
				documentQuestion
						.setName("How many attacks happened on the companies in the same location in past 6 months");
				documentQuestionsList.add(documentQuestion);
			}
			if (i == 3) {
				documentQuestion.setPrimaryIsoCode("BST.1.4");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("How many attacks happened on the companies using same tech in past 6 months");
				documentQuestionsList.add(documentQuestion);
			}

		}
		for (int j = 0; j < 3; j++) {
			DocumentQuestions documentQuestion = new DocumentQuestions();
			documentQuestion.setContentType("SUB_PARAGRAPH");
			documentQuestion.setLevel1Code("BST.2");
			documentQuestion.setLevel3Title("Company profile");
			documentQuestion.setLevel3Description(
					"Provide information about elements in company profile which expose it to cyber threats(technology,industry,location)");
			documentQuestion.setOSINT(true);
			documentQuestion.setAnswerType(AnswerType.TEXT);
			documentQuestion.setQuestionStatus(true);
			documentQuestion.setShowInUI(true);
			documentQuestion.setLevel1Title("Company profile");
			if (j == 0) {
				documentQuestion.setPrimaryIsoCode("BST.2.1");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company operating in location with high cyber risk");
				documentQuestionsList.add(documentQuestion);
			}
			if (j == 1) {
				documentQuestion.setPrimaryIsoCode("BST.2.2");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company operating in industry with high cyber risk");
				documentQuestionsList.add(documentQuestion);
			}
			if (j == 2) {
				documentQuestion.setPrimaryIsoCode("BST.2.3");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company using technology with high cyber risk");
				documentQuestionsList.add(documentQuestion);
			}

		}
		for (int k = 0; k < 4; k++) {
			DocumentQuestions documentQuestion = new DocumentQuestions();
			documentQuestion.setContentType("SUB_PARAGRAPH");
			documentQuestion.setLevel1Code("BST.3");
			documentQuestion.setLevel3Title("Company BI exposures");
			documentQuestion.setLevel3Description(
					"Given company's online footprint,what are the potential points of business interruption");
			documentQuestion.setOSINT(true);
			documentQuestion.setAnswerType(AnswerType.TEXT);
			documentQuestion.setQuestionStatus(true);
			documentQuestion.setShowInUI(true);
			documentQuestion.setLevel1Title("Company BI exposures");
			if (k == 0) {
				documentQuestion.setPrimaryIsoCode("BST.3.1");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company collecting PII");
				documentQuestionsList.add(documentQuestion);
			}
			if (k == 1) {
				documentQuestion.setPrimaryIsoCode("BST.3.2");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company running e-shop");
				documentQuestionsList.add(documentQuestion);
			}
			if (k == 2) {
				documentQuestion.setPrimaryIsoCode("BST.3.3");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company publishing content");
				documentQuestionsList.add(documentQuestion);
			}
			if (k == 3) {
				documentQuestion.setPrimaryIsoCode("BST.3.4");
				documentQuestion.setHasIsoCode(true);
				documentQuestion.setName("Is company allowing third party content(comments)");
				documentQuestionsList.add(documentQuestion);
			}

		}
		return documentQuestionsList;
	
	}

	@Override
	@Transactional("transactionManager")
	public boolean deleteQuestion(Long questionId,Long currentUserId) {
		DocumentQuestions documentQuestions = documentQuestionsDao.find(questionId);
		if(documentQuestions !=null)
		{
			documentQuestions.setDeleteStatus(true);
			documentQuestionsDao.saveOrUpdate(documentQuestions);
			eventPublisher.publishEvent(new LoggingEvent(documentQuestions.getId(), LoggingEventType.DELETE_QUESTION_TO_TEMPLATE, currentUserId, new Date(), null));
			eventPublisher.publishEvent(new LoggingEvent(documentQuestions.getTemplateId(), LoggingEventType.UPDATE_TEMPLATE, currentUserId, new Date(), null));
		}
			
		return true;
	}

	
	
	

}
