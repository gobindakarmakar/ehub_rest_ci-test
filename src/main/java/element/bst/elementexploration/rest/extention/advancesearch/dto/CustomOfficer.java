package element.bst.elementexploration.rest.extention.advancesearch.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomOfficer {
	
	@JsonProperty(required=true)
	private String mainIdentifier;
	
	private String source;

	private String officerId;
	
	@JsonProperty(required=true)
	private String officerName;
	
	private String dateOfBirth;
	
	private String officerRole;
	
	private String officerCountry;
	
	private String classification;
	
	private String totalPercentage;
	
	private String sourceUrl;
	
	private String from;
	

	public String getMainIdentifier() {
		return mainIdentifier;
	}

	public void setMainIdentifier(String mainIdentifier) {
		this.mainIdentifier = mainIdentifier;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getOfficerId() {
		return officerId;
	}

	public void setOfficerId(String officerId) {
		this.officerId = officerId;
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getOfficerRole() {
		return officerRole;
	}

	public void setOfficerRole(String officerRole) {
		this.officerRole = officerRole;
	}

	public String getOfficerCountry() {
		return officerCountry;
	}

	public void setOfficerCountry(String officerCountry) {
		this.officerCountry = officerCountry;
	}

	
	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getTotalPercentage() {
		return totalPercentage;
	}

	public void setTotalPercentage(String totalPercentage) {
		this.totalPercentage = totalPercentage;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	
}
