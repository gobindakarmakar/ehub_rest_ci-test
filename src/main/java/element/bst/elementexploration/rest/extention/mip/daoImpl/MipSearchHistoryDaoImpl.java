package element.bst.elementexploration.rest.extention.mip.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.mip.dao.MipSearchHistoryDao;
import element.bst.elementexploration.rest.extention.mip.domain.MipSearchHistory;
import element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("mipSearchHistoryDao")
public class MipSearchHistoryDaoImpl extends GenericDaoImpl<MipSearchHistory, Long> implements MipSearchHistoryDao {
	
	public MipSearchHistoryDaoImpl() {
		super(MipSearchHistory.class);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MipSearchHistoryDTO> getSearchHistoryList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn) {
		List<MipSearchHistoryDTO> bstSearchHistoryList = new ArrayList<>();
		try {
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select distinct new element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO(mshd.searchId, mshd.name, ");
			queryBuilder.append("mshd.data, mshd.sourcePage, mshd.createdOn, mshd.updatedOn ) ");
			queryBuilder.append("from MipSearchHistory mshd ");
			queryBuilder.append("where mshd.userId = :userId ");
			queryBuilder.append(" order by ");
			queryBuilder.append(" mshd.");
			queryBuilder.append(resolveCaseColumnName(orderBy));
			if (orderBy != null && orderIn != null) {
				if("asc".equalsIgnoreCase(orderIn) || "desc".equalsIgnoreCase(orderIn)){
					queryBuilder.append(" ");
					queryBuilder.append(orderIn);
				}else{
					queryBuilder.append(" ");
					queryBuilder.append("desc");
				}
			}else{
				queryBuilder.append(" ");
				queryBuilder.append("desc");
			}
			Query<?> queryResult = getCurrentSession().createQuery(queryBuilder.toString());
			queryResult.setParameter("userId", userId);
			queryResult.setFirstResult((pageNumber - 1) * (recordsPerPage));
			queryResult.setMaxResults(recordsPerPage);
			bstSearchHistoryList = (List<MipSearchHistoryDTO>) queryResult.getResultList();
		} catch (Exception ex) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, ex,
					MipSearchHistoryDaoImpl.class);
		}
		return bstSearchHistoryList;

	}

	@Override
	public boolean isSearchHistoryOwner(Long searchId, Long userId) {
		MipSearchHistory bstSearchHistory = null;
		try {
			CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<MipSearchHistory> query = builder.createQuery(MipSearchHistory.class);
			Root<MipSearchHistory> bstSearchHistoryList = query.from(MipSearchHistory.class);
			query.select(bstSearchHistoryList);
			query.where(builder.and(builder.equal(bstSearchHistoryList.get("searchId"), searchId),
					builder.equal(bstSearchHistoryList.get("userId"), userId)));
			bstSearchHistory = getCurrentSession().createQuery(query).getSingleResult();
			if (bstSearchHistory != null)
				return true;
		} catch (Exception ex) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, ex,
					MipSearchHistoryDaoImpl.class);
		}
		return false;
	}
	
	private static String resolveCaseColumnName(String userColumn) {
		if (userColumn == null)
			return "createdOn";

		switch (userColumn.toLowerCase()) {
		case "name":
			return "name";
		default:
			return "createdOn";

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countSearchHistoryList(Long userId) {
		List<MipSearchHistoryDTO> bstSearchHistoryList = new ArrayList<>();
		try {

			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"select distinct new element.bst.elementexploration.rest.extention.mip.dto.MipSearchHistoryDTO(mshd.searchId, mshd.name, ");
			queryBuilder.append("mshd.data, mshd.sourcePage, mshd.createdOn, mshd.updatedOn ) ");
			queryBuilder.append("from MipSearchHistory mshd ");
			queryBuilder.append("where mshd.userId = :userId ");
			Query<?> queryResult = getCurrentSession().createQuery(queryBuilder.toString());
			queryResult.setParameter("userId", userId);
			bstSearchHistoryList = (List<MipSearchHistoryDTO>) queryResult.getResultList();
		} catch (Exception ex) {
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, ex,
					MipSearchHistoryDaoImpl.class);
		}
		return (long) bstSearchHistoryList.size();

	}
}
