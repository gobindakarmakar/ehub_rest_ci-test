package element.bst.elementexploration.rest.notification.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class NotificationDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long notificationId;
	
	private String subject;
	
	private String body;
	
	private Integer type;
	
	private Date timestamp;
	
	private Integer level;
	
	private Long userId;	
	
	private Long seedId;
	
	public NotificationDTO(Long notificationId, String subject, String body, Integer type, Date timestamp, Integer level,
			Long userId, Long seedId) {
		super();
		this.notificationId = notificationId;
		this.subject = subject;
		this.body = body;
		this.type = type;
		this.timestamp = timestamp;
		this.level = level;
		this.userId = userId;
		this.seedId = seedId;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getSeedId() {
		return seedId;
	}

	public void setSeedId(Long seedId) {
		this.seedId = seedId;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
