package element.bst.elementexploration.rest.extention.advancesearch.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;


public class EntityOfficerInfoDto implements Serializable{

	/**
	 * @author Prateek Maurya
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(required = true)
	private String entityId;

	@JsonProperty(required = true)
	private String officerName;
	
	@JsonProperty(required = true)
	private String source;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}