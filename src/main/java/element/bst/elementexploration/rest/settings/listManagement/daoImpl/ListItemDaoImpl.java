package element.bst.elementexploration.rest.settings.listManagement.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.casemanagement.daoImpl.CaseDocumentDaoImpl;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.settings.listManagement.dao.ListItemDao;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

@Repository("listItemDao")
@Transactional("transactionManager")
public class ListItemDaoImpl extends GenericDaoImpl<ListItem, Long> implements 
ListItemDao {

	public ListItemDaoImpl() {
		super(ListItem.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ListItem> getListItemByListType(String listType) {
		List<ListItem> listItemList = new ArrayList<>();
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append(" select li FROM ListItem li WHERE li.listType=:listType");		

		try {		   
			Query<?> query = getCurrentSession().createQuery(sqlString.toString());
			query.setParameter("listType", listType);
			listItemList = (List<ListItem>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
					e, CaseDocumentDaoImpl.class);
		} 
		return listItemList;			
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ListItem> getListItemByListTypeAndSearchField(String listType,String searchField) {
		List<ListItem> listItemList = new ArrayList<>();
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append(" select li FROM ListItem li WHERE li.listType=:listType and li.displayName like '"+"%"+searchField+"%'");		

		try {		   
			Query<?> query = getCurrentSession().createQuery(sqlString.toString());
			query.setParameter("listType", listType);
			//query.setParameter("searchField",searchField);
			listItemList = (List<ListItem>) query.getResultList();
		} catch (HibernateException e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
					e, CaseDocumentDaoImpl.class);
		} 
		return listItemList;			
	}

	@Override
	public ListItem getListItemByListTypeAndDisplayName(String listType, String displayName) {
		ListItem listItem = null;
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append(" select li FROM ListItem li WHERE li.listType=:listType AND li.displayName=:displayName");		

		try {		   
			Query<?> query = getCurrentSession().createQuery(sqlString.toString());
			query.setParameter("listType", listType);
			query.setParameter("displayName", displayName);
			listItem = (ListItem) query.getSingleResult();
		} catch (NoResultException ne) {
			return listItem;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
					e, CaseDocumentDaoImpl.class);
		}
		return listItem;
	}
	
	@Override
	public ListItem getListItemByListTypeAndCode(String listType, String code) {
		ListItem listItem = null;
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append(" select li FROM ListItem li WHERE li.listType=:listType AND li.code=:code");		

		try {		   
			Query<?> query = getCurrentSession().createQuery(sqlString.toString());
			query.setParameter("listType", listType);
			query.setParameter("code", code);
			listItem = (ListItem) query.getSingleResult();
		} catch (NoResultException ne) {
			return listItem;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
					e, CaseDocumentDaoImpl.class);
		}
		return listItem;
	}

	@Override
	public List<ListItem> getListItemByListTypeAndLikeDisplayName(String listType, String displayName) {
		List<ListItem> listItem = null;
		StringBuilder sqlString = new StringBuilder();		
		sqlString.append(" select li FROM ListItem li WHERE li.listType=:listType AND li.displayName like :displayName");		

		try {		   
			Query<?> query = getCurrentSession().createQuery(sqlString.toString());
			query.setParameter("listType", listType);
			query.setParameter("displayName", "%"+displayName+"%");
			listItem = (List<ListItem>) query.getResultList();
		} catch (NoResultException ne) {
			return listItem;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, "Hibernate exception ",
					e, CaseDocumentDaoImpl.class);
		}
		return listItem;
	}
}
