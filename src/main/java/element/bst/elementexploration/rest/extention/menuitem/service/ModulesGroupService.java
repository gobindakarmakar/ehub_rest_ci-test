package element.bst.elementexploration.rest.extention.menuitem.service;

import element.bst.elementexploration.rest.extention.menuitem.domain.ModulesGroup;
import element.bst.elementexploration.rest.generic.service.GenericService;

/**
 * @author Jalagari Paul
 *
 */
public interface ModulesGroupService  extends GenericService<ModulesGroup, Long> {

}
