package element.bst.elementexploration.rest.usermanagement.security.dao;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.UserToken;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;

/**
 * 
 * @author Amit Patel
 *
 */
public interface ElementSecurityDao extends GenericDao<UserToken, Long>{

	boolean isValidToken(String token);
	
	UserToken getUserToken(String token);
	
	UserToken isTokenExist(long userId);
	
	boolean isTokenExist(String token);
	
	UsersToken getUsersToken(String token);
}
