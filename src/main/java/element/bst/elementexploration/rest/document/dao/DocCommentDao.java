package element.bst.elementexploration.rest.document.dao;

import java.util.List;

import element.bst.elementexploration.rest.document.domain.DocComment;
import element.bst.elementexploration.rest.document.dto.DocCommentDTO;
import element.bst.elementexploration.rest.generic.dao.GenericDao;

public interface DocCommentDao extends GenericDao<DocComment, Long>{

	List<DocCommentDTO> listDocComment(Long userId, Long docId, Integer pageNumber, Integer recordsPerPage,
			String orderIn);

	long countDocComment(Long userId, Long docId);

}
