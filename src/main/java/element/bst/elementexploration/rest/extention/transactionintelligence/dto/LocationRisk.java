package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;

public class LocationRisk implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double score;
	
	private String level;
	
	private String coordinates;

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}
	
	
}
