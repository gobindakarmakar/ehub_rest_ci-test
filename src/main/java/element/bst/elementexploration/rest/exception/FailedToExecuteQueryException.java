package element.bst.elementexploration.rest.exception;

/**
 * 
 * @author Amit Patel
 *
 */
public class FailedToExecuteQueryException extends RuntimeException{

    /**
     * 
     */
    private static final long serialVersionUID = -5725055899793801738L;
   
    private  String message;;
    
    public static final String CASE_SEED_NOT_FOUND_MSG="Could not process your request";
	
	public FailedToExecuteQueryException(String message) {
		super();
		this.message = message;
	}

	public FailedToExecuteQueryException() {

		super();
		message = CASE_SEED_NOT_FOUND_MSG;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return message;
	}

}
