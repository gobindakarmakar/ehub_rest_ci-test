package element.bst.elementexploration.rest.usermanagement.group.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Paul Jalagari
 *
 */
public class UsersListByGroupListDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<GroupUsersListDto> result;

	public List<GroupUsersListDto> getResult() {
		return result;
	}

	public void setResult(List<GroupUsersListDto> result) {
		this.result = result;
	}

}
