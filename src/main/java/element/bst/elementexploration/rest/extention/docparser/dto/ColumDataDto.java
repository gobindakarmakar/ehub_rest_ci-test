package element.bst.elementexploration.rest.extention.docparser.dto;

import java.io.Serializable;
/**
 * @author rambabu
 *
 */
public class ColumDataDto implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private boolean isHeader;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isHeader() {
		return isHeader;
	}
	public void setHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}
	
	
	
}
