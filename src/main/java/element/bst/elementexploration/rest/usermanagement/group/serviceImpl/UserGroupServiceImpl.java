package element.bst.elementexploration.rest.usermanagement.group.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.usermanagement.group.dao.UserGroupDao;
import element.bst.elementexploration.rest.usermanagement.group.domain.UserGroup;
import element.bst.elementexploration.rest.usermanagement.group.service.UserGroupService;

@Service("userGroupService")
public class UserGroupServiceImpl extends GenericServiceImpl<UserGroup, Long> implements UserGroupService {

	@Autowired
	private UserGroupDao userGroupDao;
	
	@Autowired
	public UserGroupServiceImpl(GenericDao<UserGroup, Long> genericDao) {
		super(genericDao);
	}
	
	@Override
	@Transactional("transactionManager")
	public UserGroup getUserGroup(Long userId, Long groupId) {		
		return userGroupDao.getUserGroup(userId, groupId);
	}

}
