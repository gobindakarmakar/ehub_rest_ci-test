package element.bst.elementexploration.rest.extention.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Suresh
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BingNews implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String url;
	private String description;
	private String datePublished;
	private String category;
	private List<EntityDto> entityDtos;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<EntityDto> getEntityDtos() {
		return entityDtos;
	}

	public void setEntityDtos(List<EntityDto> entityDtos) {
		this.entityDtos = entityDtos;
	}

}
