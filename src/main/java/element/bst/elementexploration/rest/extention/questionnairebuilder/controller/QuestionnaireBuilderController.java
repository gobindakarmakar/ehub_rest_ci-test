package element.bst.elementexploration.rest.extention.questionnairebuilder.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.questionnairebuilder.service.QuestionnaireBuilderService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * @author rambabu
 *
 */
@Api(tags = { "Questionnaire builder API" })
@RestController
@RequestMapping("/api/questionnaireBuilder")
public class QuestionnaireBuilderController extends BaseController{
	
	@Autowired
	QuestionnaireBuilderService questionnaireBuilderService;
	
	@ApiOperation("Get groups of questionnaire")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/groups", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getGroupsBySurveyId(@RequestParam("token") String token, @RequestParam(required = true) String surveyId,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(questionnaireBuilderService.getGroupsBySurveyId(surveyId), HttpStatus.OK);
	}
	
	@ApiOperation("Get groups of questionnaire")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/questions", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getQuestionsBySurveyId(@RequestParam("token") String token, @RequestParam(required = true) String surveyId,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(questionnaireBuilderService.getQuestionsBySurveyId(surveyId), HttpStatus.OK);
	}
	
	@ApiOperation("Get activi questionnaire")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.FAILED_TO_GET_DATA_FROM_SERVER_MSG) })
	@GetMapping(value = "/getActiveQuestionnaires", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getActiveQuestionnaires(@RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(questionnaireBuilderService.getActiveQuestionnaire(), HttpStatus.OK);
	}

}
