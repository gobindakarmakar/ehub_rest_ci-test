package element.bst.elementexploration.rest.extention.docparser.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;

import element.bst.elementexploration.rest.extention.docparser.enums.AnswerType;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "DOCUMENT_STATEMENTS")
public class DocumentStatements implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String contentQuestion;
	private String contentAnswer;
	private DocumentContents documentContent;
	private Date updatedRecord;
	private boolean isQuestion;
	private AnswerType answerType;
	private String expectedAnswer;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "CONTENT_QUESTION", columnDefinition = "LONGTEXT")
	public String getContentQuestion() {
		return contentQuestion;
	}

	public void setContentQuestion(String contentQuestion) {
		this.contentQuestion = contentQuestion;
	}

	@Column(name = "CONTENT_ANSWER", columnDefinition = "LONGTEXT")
	public String getContentAnswer() {
		return contentAnswer;
	}

	public void setContentAnswer(String contentAnswer) {
		this.contentAnswer = contentAnswer;
	}

	@ManyToOne
	@JoinColumn(name = "DOCUMENT_CONTENT_ID")
	public DocumentContents getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(DocumentContents documentContent) {
		this.documentContent = documentContent;
	}

	@Column(name = "ANSWER_UPDATED_TIME")
	@UpdateTimestamp
	public Date getUpdatedRecord() {
		return updatedRecord;
	}

	public void setUpdatedRecord(Date updatedRecord) {
		this.updatedRecord = updatedRecord;
	}

	@Column(name = "IS_QUESTION")
	public boolean isQuestion() {
		return isQuestion;
	}

	public void setQuestion(boolean isQuestion) {
		this.isQuestion = isQuestion;
	}

	@Column(name = "ANSWER_TYPE")
	@Enumerated(EnumType.STRING)
	public AnswerType getAnswerType() {
		return answerType;
	}

	public void setAnswerType(AnswerType answerType) {
		this.answerType = answerType;
	}

	@Column(name = "EXPECTED_ANSWER")
	public String getExpectedAnswer() {
		return expectedAnswer;
	}

	public void setExpectedAnswer(String expectedAnswer) {
		this.expectedAnswer = expectedAnswer;
	}

}
