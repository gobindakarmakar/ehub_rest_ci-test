package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

public class AlertDashBoardRespDtoAscnd implements Serializable,Comparable<AlertDashBoardRespDtoAscnd> {

	private static final long serialVersionUID = 1L;
	private String customerName;
	private Date alertBusinessDate;
	private Integer totalAmount;
	// private String customerType;

	public AlertDashBoardRespDtoAscnd() {
		super();
	}
	
	

	public AlertDashBoardRespDtoAscnd(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}



	public AlertDashBoardRespDtoAscnd(String customerName, Date alertBusinessDate, Integer totalAmount) {
		super();
		this.customerName = customerName;
		this.alertBusinessDate = alertBusinessDate;
		this.totalAmount = totalAmount;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getAlertBusinessDate() {
		return alertBusinessDate;
	}

	public void setAlertBusinessDate(Date alertBusinessDate) {
		this.alertBusinessDate = alertBusinessDate;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	
	@Override
    public int compareTo(AlertDashBoardRespDtoAscnd dto) 
	{
        int compareAmount=((AlertDashBoardRespDtoAscnd)dto).getTotalAmount();
        /* For Ascending order*/
       return this.getTotalAmount()-compareAmount;

        /* For Descending order do like this */
        //return compareAmount-this.getTotalAmount();
    }

}
