package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.SourceCredibilityService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("sourceCredibilityService")
public class SourceCredibilityServiceImpl extends GenericServiceImpl<SourceCredibility, Long>
		implements SourceCredibilityService {

	@Autowired
	public SourceCredibilityServiceImpl(GenericDao<SourceCredibility, Long> genericDao) {
		super(genericDao);
	}

	public SourceCredibilityServiceImpl() {
	}

}
