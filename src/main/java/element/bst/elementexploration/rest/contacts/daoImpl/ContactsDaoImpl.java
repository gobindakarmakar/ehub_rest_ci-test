package element.bst.elementexploration.rest.contacts.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.contacts.dao.ContactsDao;
import element.bst.elementexploration.rest.contacts.domain.Contact;
import element.bst.elementexploration.rest.contacts.dto.ContactDTO;
import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Amit Patel
 * 
 * */
@Repository("contactsDao")
public class ContactsDaoImpl extends GenericDaoImpl<Contact, Long> implements ContactsDao{
	
	public ContactsDaoImpl(){
        super(Contact.class);
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactDTO> getUserContactList(Long userId, Integer pageNumber, Integer recordsPerPage, String orderBy, String orderIn, String keyword) {
		List<ContactDTO> contacts = new ArrayList<ContactDTO>();
		StringBuilder hqlQuery = new StringBuilder();		
		try{
			hqlQuery.append("select distinct new element.bst.elementexploration.rest.contacts.dto.ContactDTO(user.userId, user.firstName, ");
			hqlQuery.append("user.lastName, user.emailAddress)");
			hqlQuery.append("from User user ");
			hqlQuery.append("left outer join Contact contact on contact.contactId = user.userId ");
			hqlQuery.append("where contact.userId = :userId ");
			
			if(!StringUtils.isEmpty(keyword)){
				hqlQuery.append("AND (user.emailAddress LIKE '%").append(keyword.replaceAll("\"", "")).append("%'")				
					.append(" OR user.firstName LIKE '%").append(keyword.replaceAll("\"", "")).append("%'")
					.append(" OR user.lastName LIKE '%").append(keyword.replaceAll("\"", "")).append("%')");				
			}
			
			if(orderBy != null && orderIn != null){
				hqlQuery.append(" ORDER BY ").append("user." + orderBy).append(" ").append(orderIn);				
			}
		
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("userId", userId);
			query.setFirstResult((pageNumber - 1) * (recordsPerPage));
			query.setMaxResults(recordsPerPage);			
			contacts = (List<ContactDTO>) query.getResultList();
		} catch(Exception e){
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,ContactsDaoImpl.class);
		} 
		
		return contacts;
	}

	@Override
	public void removeContact(Long contactId, Long userId) {
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaDelete<Contact> query = builder.createCriteriaDelete(Contact.class);
			Root<Contact> contact = query.from(Contact.class);
			query.where(builder.and(builder.equal(contact.get("contactId"), contactId), builder.equal(contact.get("userId"), userId)));
			this.getCurrentSession().createQuery(query).executeUpdate();
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to remove contact. Reason : " + e.getMessage());
		} 	
	}

	@Override
	public Contact getUserContact(Long contactId, Long userId) {
		Contact found = null;
		try {
			CriteriaBuilder builder = this.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<Contact> query = builder.createQuery(Contact.class);
			Root<Contact> contact = query.from(Contact.class);
			query.select(contact);
			query.where(builder.and(builder.equal(contact.get("contactId"), contactId), builder.equal(contact.get("userId"), userId)));
			found = this.getCurrentSession().createQuery(query).getSingleResult();
		} catch(NoResultException e) {
			found = null;
		}catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to execute request. Reason : " + e.getMessage());
		}	
		return found;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countUserContactList(Long userId, String keyword) {
		List<ContactDTO> contacts = new ArrayList<>();
		StringBuilder hqlQuery = new StringBuilder();		
		try{
			hqlQuery.append("select distinct new element.bst.elementexploration.rest.contacts.dto.ContactDTO(user.userId, user.firstName, ");
			hqlQuery.append("user.lastName, user.emailAddress)");
			hqlQuery.append("from User user ");
			hqlQuery.append("left outer join Contact contact on contact.contactId = user.userId ");
			hqlQuery.append("where contact.userId = :userId ");
			
			if(!StringUtils.isEmpty(keyword)){
				hqlQuery.append("AND (user.emailAddress LIKE '%").append(keyword.replaceAll("\"", "")).append("%'")				
					.append(" OR user.firstName LIKE '%").append(keyword.replaceAll("\"", "")).append("%'")
					.append(" OR user.lastName LIKE '%").append(keyword.replaceAll("\"", "")).append("%')");				
			}
		
			Query<?> query = this.getCurrentSession().createQuery(hqlQuery.toString());
			query.setParameter("userId", userId);		
			contacts = (List<ContactDTO>) query.getResultList();
		} catch(Exception e){
			ElementLogger.log(ElementLoggerLevel.ERROR, ElementConstants.HIBERNATE_EXCEPTION_MSG, e,ContactsDaoImpl.class);
		}		
		return contacts == null ? 0L : contacts.size();	
	}

}
