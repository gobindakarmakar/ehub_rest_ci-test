package element.bst.elementexploration.rest.extention.riskscore.service;

public interface RiskScoreService {

	String getAllRiskModels() throws Exception;

	String createRiskModel(String jsonString) throws Exception;

	String setModelId(String jsonString) throws Exception;

	String getRiskAttributes() throws Exception;

	String insertRiskAttributes(String jsonString) throws Exception;

	String getRiskRelation() throws Exception;

	String insertRiskrelation(String jsonString) throws Exception;

	String deleteRiskModel(String modelId) throws Exception;

	String getRiskModel(String modelId) throws Exception;

	String getDefaultRiskModel() throws Exception;

	String createDefaultRiskModel(String jsonString) throws Exception;

	String checkNameExists(String jsonString) throws Exception;

	String getSanctionsList() throws Exception;

	String getRiskAttributesInsights(String entityType) throws Exception;

	String getRiskScoreByEntityId(String entityId)throws Exception;

}
