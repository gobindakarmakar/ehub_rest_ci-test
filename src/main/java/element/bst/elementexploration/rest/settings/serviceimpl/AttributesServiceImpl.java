package element.bst.elementexploration.rest.settings.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.settings.domain.Attributes;
import element.bst.elementexploration.rest.settings.service.AttributesService;

/**
 * @author suresh
 *
 */
@Service("attributesService")
public class AttributesServiceImpl extends GenericServiceImpl<Attributes, Long> implements AttributesService {

	@Autowired
	public AttributesServiceImpl(GenericDao<Attributes, Long> genericDao) {
		super(genericDao);
	}

}
