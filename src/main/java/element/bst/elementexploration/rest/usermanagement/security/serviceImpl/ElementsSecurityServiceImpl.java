package element.bst.elementexploration.rest.usermanagement.security.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriUtils;

import com.google.gson.JsonObject;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import element.bst.elementexploration.rest.casemanagement.domain.Case;
import element.bst.elementexploration.rest.casemanagement.service.CaseService;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.exception.AuthenticationFailedException;
import element.bst.elementexploration.rest.exception.DublicateTokenFoundException;
import element.bst.elementexploration.rest.exception.InsufficientDataException;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;
import element.bst.elementexploration.rest.logging.domain.AuditLog;
import element.bst.elementexploration.rest.logging.enumType.LogLevels;
import element.bst.elementexploration.rest.logging.service.AuditLogService;
import element.bst.elementexploration.rest.settings.listManagement.domain.ListItem;
import element.bst.elementexploration.rest.settings.listManagement.service.ListItemService;
import element.bst.elementexploration.rest.settings.service.SettingsService;
import element.bst.elementexploration.rest.usermanagement.security.dao.ElementsSecurityDao;
import element.bst.elementexploration.rest.usermanagement.security.domain.LoginHistory;
import element.bst.elementexploration.rest.usermanagement.security.domain.UsersToken;
import element.bst.elementexploration.rest.usermanagement.security.service.ElementsSecurityService;
import element.bst.elementexploration.rest.usermanagement.security.service.FailedLoginService;
import element.bst.elementexploration.rest.usermanagement.security.service.LoginHistoryService;
import element.bst.elementexploration.rest.usermanagement.security.service.MailService;
import element.bst.elementexploration.rest.usermanagement.user.domain.Users;
import element.bst.elementexploration.rest.usermanagement.user.dto.UsersDto;
import element.bst.elementexploration.rest.usermanagement.user.service.UsersService;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;
import element.bst.elementexploration.rest.util.EmailTypes;
import element.bst.elementexploration.rest.util.Mail;
import element.bst.elementexploration.rest.util.ServiceCallHelper;
import element.bst.elementexploration.rest.util.SystemSettingTypes;
import element.bst.elementexploration.rest.util.UserStatus;

/**
 * @author Paul Jalagari
 *
 */
@Service("elementsSecurityService")
public class ElementsSecurityServiceImpl extends GenericServiceImpl<UsersToken, Long> implements ElementsSecurityService {

	@Autowired
	public ElementsSecurityServiceImpl(GenericDao<UsersToken, Long> genericDao) {
		super(genericDao);
	}

	@Autowired
	ElementsSecurityDao elementsSecurityDao;

	@Autowired
	AuditLogService auditLogService;
	
	@Lazy
	@Autowired
	ElementsSecurityService elementsSecurityService;

	@Autowired
	private UsersService usersService;

	@SuppressWarnings("unused")
	@Autowired
	private FailedLoginService failedLoginService;
	
	@Autowired
	private LoginHistoryService loginHistoryService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	public JavaMailSender javaMailSender;

	@Autowired
	Environment env;

	@Autowired
	private MailService mailService;
	
	@Autowired
	private CaseService caseService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	@Lazy
	private PasswordEncoder passwordEncoder;

	@Value("${token.validity.days}")
	private int TOKEN_VALIDITY_DAYS;

	@Value("${validation_api}")
	private String VALIDATION_API;

	@Value("${firebase_verify_api}")
	private String FIREBASE_VERIFY_API;
	
	@Value("${firebase_verify_token_api}")
	private String FIREBASE_VERIFY_TOKEN_API;
	
	@Value("${sendgrid_api_key}")
	private String SENDGRID_API_KEY;
	
	@Value("${sendgrid_sender_mail}")
	private String SENDGRID_SENDER_MAIL;
	
	@Value("${firebase_api_key}")
	private String FIREBASE_API_KEY;
	
	@Value("${fe_base_url}")
	private String FE_BASE_URL;

	private final int DEFAULT_TOKEN_VALIDITY_DAYS = 60;
	
	@Autowired
	private ListItemService listItemService;

	/**
	 * 
	 * @param token
	 * @return
	 */
	@Override
	@Transactional("transactionManager")
	public Users getUserUserFromToken(String token) {
		if (token == null)
			return null;
		else if (elementsSecurityDao.isValidToken(token)) {
			if (elementsSecurityDao.getUserToken(token) != null)
				return elementsSecurityDao.getUserToken(token).getUserId();
			else
				return null;
		} else
			return null;
	}

	@Override
	@Transactional("transactionManager")
	public UsersToken getUserToken(Long userId) {
		return elementsSecurityDao.isTokenExist(userId);
	}

	@Override
	public String validateEmailPassword(String emailAddress, String password, String ipAddress) throws Exception {
		String generatedToken = null;
		if (emailAddress == null || password == null || emailAddress.trim().length() == 0
				|| password.trim().length() == 0) {
			throw new InsufficientDataException("Username or Password cannot be empty");
		}

		if (loginHistoryService.isAccountLocked(emailAddress)) {
			throw new AuthenticationFailedException("Users Account is locked.");
		}

		int validationStatus = 0;
		Users user = usersService.getUserObjectByEmailOrUsername(emailAddress);
		if (user != null) {
			// if (null != user.getStatusId()) {
			//	if (Integer.parseInt(user.getStatus()) != 0) {
					UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
							emailAddress, password);
					try {
						Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
						SecurityContextHolder.getContext().setAuthentication(authentication);
						validationStatus = 1;
					} catch (Exception ex) {
						validationStatus = -1;
					}
				/*} else {
					validationStatus = 2;
				}*/
			/*} else {
				validationStatus = 2;
			}*/
		}

		try {
			if (validateLoginResponse(validationStatus)) {
				if(user!=null)
					generatedToken = elementsSecurityService.saveToken(user);
				loginHistoryService.releaseAccountLocked(emailAddress, ipAddress);
			}
		} catch (Exception e) {
			loginHistoryService.processFailedAuthentication(user.getEmailAddress(), user.getScreenName(), ipAddress);
			throw e;
		}

		return generateResponseMessage(generatedToken);
	}

	/**
	 * Generate proper response message
	 * 
	 * @param validationStatus
	 * @param generatedToken
	 * @return
	 */
	private String generateResponseMessage(String generatedToken) {
		JsonObject object = new JsonObject();
		object.addProperty("token", generatedToken);
		return object.toString();
	}

	/**
	 * 
	 * @param validationStatus
	 * @return true if login is successful
	 */
	private boolean validateLoginResponse(int validationStatus) {
		switch (validationStatus) {
		case 1:
			return true;
		case 0:
			throw new AuthenticationFailedException("Users not found.");
		case -1:
			throw new AuthenticationFailedException("Either email or password is incorrect.");
		case 2:
			throw new AuthenticationFailedException("Users account deactivated.");
		default:
			throw new AuthenticationFailedException("Could not process request.");
		}
	}

	/**
	 * 
	 * @param user
	 */
	@Override
	@Transactional("transactionManager")
	public String saveToken(Users user) {
		try {
		String generatedToken = null;
		UsersToken userToken = null;
		Long userId = user.getUserId();
		if (null != userId) {
			userToken = elementsSecurityDao.isTokenExist(userId);
		}

		if (userToken == null) {
			// generate new token
			generatedToken = genearteToken();
			if (generatedToken == null || elementsSecurityDao.isTokenExist(generatedToken)) {
				throw new DublicateTokenFoundException("Could not generate new token, try agian");
			}
			Date currentDate = new Date();
			UsersToken userTokenTemp = new UsersToken();
			userTokenTemp.setCreatedOn(currentDate);
			userTokenTemp.setExpireOn(calculateTokenExpireDate(currentDate));
			userTokenTemp.setUserId(user);
			userTokenTemp.setToken(generatedToken);
			elementsSecurityDao.saveOrUpdate(userTokenTemp);
		} else {
			if (validateTokenObject(userToken)) {
				generatedToken = userToken.getToken();
			} else {
				generatedToken = genearteToken();
				if (generatedToken == null || elementsSecurityDao.isTokenExist(generatedToken)) {
					throw new DublicateTokenFoundException("Could not generate new token, try agian");
				}
				Date currentDate = new Date();
				userToken.setCreatedOn(currentDate);
				userToken.setExpireOn(calculateTokenExpireDate(currentDate));
				userToken.setToken(generatedToken);
				elementsSecurityDao.saveOrUpdate(userToken);
			}
		}
		return generatedToken;
	}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 
	 * @param userToken
	 * @return return true if no expired token is found, else return false
	 */
	private boolean validateTokenObject(UsersToken userToken) {
		// Compare expiry date with current date
		Date expiryDate = userToken.getExpireOn();
		if (expiryDate.compareTo(new Date()) <= 0)
			return false;
		return true;
	}

	/**
	 * Generate unique token
	 * 
	 * @return
	 */
	private String genearteToken() {
		// Creating a random UUID (Universally unique identifier).
		return UUID.randomUUID().toString();
	}

	/**
	 * Add given day to current date
	 * 
	 * @param currentDate
	 * @return
	 */
	private Date calculateTokenExpireDate(Date currentDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.DATE, getTokenExpiryDay()); // minus number would
														// decrement the days
		return cal.getTime();
	}

	/**
	 * Check if expiry days is valid
	 * 
	 * @return
	 */
	private int getTokenExpiryDay() {
		return TOKEN_VALIDITY_DAYS > 0 ? TOKEN_VALIDITY_DAYS : DEFAULT_TOKEN_VALIDITY_DAYS;
	}

	@Override
	@Transactional("transactionManager")
	public boolean validateTemporaryPassword(Long userId,String type, String password) {
		Users user = null;
		if (userId != null){
			user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
		}
		if (null != user && password != null) {
			if (null != user.getTemporaryPasswordExpiresOn() && user.getTemporaryPassword() != null)
				if (((new Date()).compareTo(user.getTemporaryPasswordExpiresOn()) <= 0)
						&& (passwordEncoder.matches(password, user.getTemporaryPassword()))) {
					if(type.equalsIgnoreCase(EmailTypes.REGISTRATION.toString())) {
						List<ListItem> activeStatuses=listItemService.findAll().stream().filter(o -> o.getDisplayName().equalsIgnoreCase("Active")).collect(Collectors.toList());
						if(activeStatuses!=null && activeStatuses.size()>0)
						user.setStatusId(activeStatuses.get(0));
						if (settingsService
								.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
										ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
								.equalsIgnoreCase("Off")) {
							usersService.saveOrUpdate(user);
						}
						AuditLog log = new AuditLog(new Date(), LogLevels.INFO,
								ElementConstants.USER_STATUS_TYPE, null,
								ElementConstants.USER_MANAGEMENT_TYPE);
						log.setDescription(user.getFirstName() + " " + user.getLastName()+" "
								+ ElementConstants.CHANGED + " the status from " + UserStatus.Pending.toString()+" to "+ UserStatus.Active.toString());
						log.setUserId(user.getUserId());
						log.setTypeId(user.getUserId());
						log.setSubType(UserStatus.Pending.toString());
						log.setName(ElementConstants.USER_STATUS_TYPE);
						auditLogService.save(log);
					}
					return true;
				}
		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean sendEmailForPasswordReset(UsersDto user, HttpServletRequest request) throws Exception {
		String remoteAddr;
		if (null != user) {
			Users userData = usersService.find(user.getUserId());
			userData = usersService.prepareUserFromFirebase(userData);
			String tempPassword = RandomStringUtils.randomAlphanumeric(10);
			userData.setTemporaryPassword(passwordEncoder.encode(tempPassword));
			Date currentDate = new Date();
			Calendar calenderDate = Calendar.getInstance();
			calenderDate.setTime(currentDate);
			calenderDate.add(Calendar.DATE, 1);
			userData.setTemporaryPasswordExpiresOn(calenderDate.getTime());
			if (settingsService
					.findSettingToggleValue(SystemSettingTypes.USER_MANAGEMENT_REGULATION.toString(),
							ElementConstants.TWO_WAY_AUTHENTICATION_STRING)
					.equalsIgnoreCase("Off")) {
				usersService.saveOrUpdate(userData);
			}
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
			String validationUrl = new String(
					request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/");
			Map<String, String> mailSettings = new HashMap<>();
			Mail mail = new Mail();
			// get mail from system settings
			mailSettings = settingsService.getMailSettings();
			mail.setMailFrom(mailSettings.get("From"));
			mail.setMailTo(userData.getEmailAddress());
			mail.setMailSubject("Reset your password");

			String token = userData.getUserId() + "-" + tempPassword;
			String warName = new File(request.getServletContext().getRealPath("/")).getName();
			//String link = UriUtils.encodePath(FE_BASE_URL+"/login?"+ token+"/reset", "UTF-8");
			String link = FE_BASE_URL+"/login?"+ token+"/reset";
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("firstName", userData.getFirstName());
			model.put("lastName", userData.getLastName());
			model.put("location", link);
			mail.setModel(model);
			try {
				mailService.sendEmail(mail);
				return true;
			} catch (Exception e) {
				return false;
			}

		}
		return false;
	}

	@Override
	@Transactional("transactionManager")
	public boolean validateTemporaryPassword(String userIDAndTempPassword,String type, HttpServletRequest request)
			throws Exception {
		Long userId = null;
		String password = null;
		String[] userIdAndPass = userIDAndTempPassword.split("-");
		if (userIdAndPass.length > 0) {
			userId = Long.valueOf(userIdAndPass[0].trim());
			password = userIdAndPass[1];
			if (elementsSecurityService.validateTemporaryPassword(userId,type, password)) {
				return true;
			}
		}

		return false;

	}
	
	@Override
	@Transactional("transactionManager")
	public boolean sendStatusChangeEmail(Long userId, String statusFrom, String statusTo, Long newUserId, Long caseId,
			String forward, String reassign, String text) {
		try {
			Case caseobj = caseService.find(caseId);
			Users newUser = null;
			Users user = usersService.find(userId);
			user = usersService.prepareUserFromFirebase(user);
			if (newUserId != null){
				newUser = usersService.find(newUserId);
				newUser = usersService.prepareUserFromFirebase(newUser);
			}
			Mail mail = new Mail();
			mail.setMailFrom(env.getProperty("email.username"));
			mail.setMailTo(user.getEmailAddress());
			mail.setMailSubject(caseobj.getName());
			if (newUserId != null)
				mail.setMailCc(newUser.getEmailAddress());
			if (text == null) {
				if (statusFrom == null && statusTo == null)
					mail.setMailContent("New Case Added to you are case diary " + caseId);

				if (statusTo!=null && statusTo.equalsIgnoreCase("focus")) {
					mail.setMailContent("Case added to focus area");
				} else if (statusTo.equalsIgnoreCase("unfocus")) {
					mail.setMailContent("Case removed from focus area");
				} else if (reassign != null) {
					mail.setMailContent("Case assigned to " + newUser.getFirstName() + " " + newUser.getLastName()
							+ " by " + user.getFirstName());
				} else if (forward != null) {
					mail.setMailContent("Case forward to " + newUser.getFirstName() + " " + newUser.getLastName()
							+ " by " + user.getFirstName());
				} else {
					mail.setMailContent(
							"Status Changed " + statusFrom + " to " + statusTo + " by " + user.getFirstName());
				}
			}else{
				mail.setMailContent(text);
			}

			mailService.sendCaseEmail(mail);
			return true;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			return false;
		}
	}

	@Override
	public Users getUserUsersFromToken(String token) {
		if (token == null)
			return null;
		else if (elementsSecurityDao.isValidToken(token)) {
			if (elementsSecurityDao.getUserToken(token) != null)
				return elementsSecurityDao.getUsersToken(token).getUserId();
			else
				return null;
		} else
			return null;
	}

	@Override
	//@Transactional("transactionManager")
	public String validateEmailPasswordNew(String emailAddress, String password , String ipAddress, HttpServletRequest request,
			String loginSource, UsersDto usersDto) {String generatedToken = null;
			if (emailAddress == null || password == null || emailAddress.trim().length() == 0
					|| password.trim().length() == 0) {
				throw new InsufficientDataException("Username or Password cannot be empty");
			}

			if (loginHistoryService.isAccountLocked(emailAddress)) {
				throw new AuthenticationFailedException("Users Account is locked.");
			}

			int validationStatus = 0;
			Users user = usersService.getUserObjectByEmailOrUsername(emailAddress);
			if (user != null) {
				// if (null != user.getStatusId()) {
				//	if (Integer.parseInt(user.getStatus()) != 0) {
						UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
								emailAddress, password);
						try {
							Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
							SecurityContextHolder.getContext().setAuthentication(authentication);
							validationStatus = 1;
						} catch (Exception ex) {
							validationStatus = -1;
						}
					/*} else {
						validationStatus = 2;
					}*/
				/*} else {
					validationStatus = 2;
				}*/
			}

			//boolean isSuccess = false;
			//int currentLoginAttempt = 0;
			try {
				if (validateLoginResponse(validationStatus)) {
					if(user!=null)
						generatedToken = elementsSecurityService.saveToken(user);
					loginHistoryService.releaseAccountLockedNew(user.getEmailAddress(), ipAddress, request, loginSource, usersDto);
					//isSuccess = true;
				}
				
			} catch (Exception e) {
				loginHistoryService.processFailedAuthenticationNew(user.getEmailAddress(), user.getScreenName(), ipAddress,request, loginSource, usersDto );
				//isSuccess = false;
				throw e;
			}

			return generateResponseMessage(generatedToken);
	}

	@Override
	public void logUserLogins(UsersDto userDto, HttpServletRequest request, Boolean isLoginSuccess) {
	
		String ipAddress = request.getHeader("CALLER-IP");
		if (ipAddress == null || "".equals(ipAddress) || "".equals(ipAddress.trim())) {
			ipAddress = request.getRemoteAddr();
		}
		String loginSource = "Internal";
		
		LoginHistory loginHistoryNew = new LoginHistory();
		//loginHistoryNew.setFailedLoginAttempt(0);
		loginHistoryNew.setLastSuccessfulLoginIpAddress(ipAddress);
		loginHistoryNew.setLastSuccessfulLoginTime(new Date());
		//loginHistoryNew.setLoginStatus("Success");
		loginHistoryNew.setUsername(userDto.getScreenName());
		loginHistoryNew.setScreenName(userDto.getScreenName());
		loginHistoryNew.setLoginSource(loginSource);
		loginHistoryNew.setLoginClientMedia(request.getHeader("User-Agent"));
		loginHistoryNew.setLoginIpAddress(ipAddress);
		loginHistoryNew.setLoginTime(new Date());
		loginHistoryNew.setUserId(userDto.getUserId());
		//loginHistoryNew.setLoginDescription(ElementConstants.Active_USER_LOGIN);
		loginHistoryNew.setLoginClientLocation(null);
		
		if(isLoginSuccess){
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, ElementConstants.LOGIN_ACTION,
					"", userDto.getUserId(), userDto.getFirstName()+" " + userDto.getLastName());
			log.setDescription(userDto.getFirstName()+" "+ userDto.getLastName() +" logged in into the system successfully ");
			log.setType(ElementConstants.USER_MANAGEMENT_TYPE);
			log.setTypeId(userDto.getUserId());
			auditLogService.save(log);
			
			loginHistoryNew.setFailedLoginAttempt(0);
			loginHistoryNew.setLoginStatus(ElementConstants.SUCCESS_STATUS);
			loginHistoryNew.setLoginDescription(ElementConstants.Active_USER_LOGIN);
		}else{
			AuditLog log = new AuditLog(new Date(), LogLevels.INFO, null, ElementConstants.LOGIN_ACTION,
					"", userDto.getUserId(), userDto.getFirstName()+" " + userDto.getLastName());
			log.setDescription(userDto.getFirstName()+" "+userDto.getLastName()
				+" tried to login into the system with "+userDto.getStatusId().getDisplayName()+ " account");
			log.setType(ElementConstants.USER_MANAGEMENT_TYPE);
			log.setTypeId(userDto.getUserId());
			auditLogService.save(log);
			
			int currentLoginAttempt = 0;
			LoginHistory loginHistory = loginHistoryService.getFailedLoginByUsername(userDto.getScreenName());
			if (loginHistory != null) {
				if (loginHistory.getFailedLoginAttempt() != null) {
					currentLoginAttempt = loginHistory.getFailedLoginAttempt().intValue() + 1;				
				}
			}
			else {
				currentLoginAttempt = 1;
			}
			loginHistoryNew.setFailedLoginAttempt(currentLoginAttempt);
			loginHistoryNew.setLoginStatus(ElementConstants.FAILED_STATUS);
			loginHistoryNew.setLoginDescription(userDto.getFirstName()+" "+userDto.getLastName()
			+" tried to login into the system with "
			+userDto.getStatusId().getDisplayName()+ "account");
		}
		loginHistoryService.save(loginHistoryNew);
		
		
	}


	@Override
	public JSONObject getFirebaseAccountInfoByToken(String accesstoken) {
		String url  = FIREBASE_VERIFY_TOKEN_API+"?key="+FIREBASE_API_KEY;
		JSONObject jsonToBeSent = new JSONObject();
		jsonToBeSent.put("idToken", accesstoken);
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonToBeSent.toString());
		if(serverResponse[0].equalsIgnoreCase("200")){
			return new JSONObject(serverResponse[1]);
		}else{
			return null;
		}
	}

	@Override
	public Boolean sendVerificationCodeSMTP(String verificationCode, String email) {
		//Prepare Mail for sending verification code
		Map<String, String> mailSettings = new HashMap<>();
		Mail mail = new Mail();
		// get mail from system settings
		mailSettings = settingsService.getMailSettings();
		mail.setMailFrom(mailSettings.get("From"));
		mail.setMailTo(email);
		mail.setMailSubject("BST Verification Code : "+verificationCode);
		Map<String, Object> model = new HashMap<String, Object>();
		//model.put("firstName", verificationCode);
		
		mail.setModel(model);
		try {
			mailService.sendEmail(mail);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Boolean sendVerificationCodeSendgrid(String verificationCode, String email) throws IOException {
		Boolean result = false;
		Email from = new Email(SENDGRID_SENDER_MAIL);
		String subject = "Element verification code";
		Email to = new Email(email);
		Content content = new Content("text/plain", "Verification code : " + verificationCode);
		com.sendgrid.helpers.mail.Mail mail = new com.sendgrid.helpers.mail.Mail(from, subject, to, content);

		// SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
		SendGrid sg = new SendGrid(SENDGRID_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
			result = true;
		} catch (IOException ex) {
			result = false;
			//throw ex;
		}
		return result;
	}


}
