package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

public class CustomerCounterPartyDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String displayName;

	private String scenario;

	private Double transactionAmount;

	private String country;

	private String iso2Code;

	private String beneficiaryAccountId;

	private Date date;

	public CustomerCounterPartyDto(String displayName, String scenario, String country, Double transactionAmount,
			String iso2Code, String beneficiaryAccountId, Date date, String bank) {
		super();
		this.displayName = displayName;
		this.scenario = scenario;
		this.transactionAmount = transactionAmount;
		this.country = country;
		this.iso2Code = iso2Code;
		this.beneficiaryAccountId = beneficiaryAccountId;
		this.date = date;
		this.bank = bank;
	}

	public String getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}

	public void setBeneficiaryAccountId(String beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}

	private String bank;

	private String bankLocation;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIso2Code() {
		return iso2Code;
	}

	public void setIso2Code(String iso2Code) {
		this.iso2Code = iso2Code;
	}

	public String getBank() {
		return bank;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBankLocation() {
		return bankLocation;
	}

	public void setBankLocation(String bankLocation) {
		this.bankLocation = bankLocation;
	}

}
