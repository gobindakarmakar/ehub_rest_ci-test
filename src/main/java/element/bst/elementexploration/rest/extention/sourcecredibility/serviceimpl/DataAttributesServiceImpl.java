package element.bst.elementexploration.rest.extention.sourcecredibility.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import element.bst.elementexploration.rest.extention.sourcecredibility.domain.DataAttributes;
import element.bst.elementexploration.rest.extention.sourcecredibility.service.DataAttributesService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("dataAttributesService")
public class DataAttributesServiceImpl extends GenericServiceImpl<DataAttributes, Long>
		implements DataAttributesService {

	@Autowired
	public DataAttributesServiceImpl(GenericDao<DataAttributes, Long> genericDao) {
		super(genericDao);
	}

	public DataAttributesServiceImpl() {
	}

}
