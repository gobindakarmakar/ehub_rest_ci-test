package element.bst.elementexploration.rest.extention.staging.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import element.bst.elementexploration.rest.base.controller.BaseController;
import element.bst.elementexploration.rest.constants.ElementConstants;
import element.bst.elementexploration.rest.extention.staging.domain.OrgStaging;
import element.bst.elementexploration.rest.extention.staging.service.StagingService;
import element.bst.elementexploration.rest.util.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Jalagari Paul
 *
 */
@Api(tags = { "Staging API" },description="Manages staging data")
@RestController
@RequestMapping("/api/staging")
public class StagingController extends BaseController{

	@Autowired
	StagingService stagingService;

	@ApiOperation("Gets staging data of organization")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@GetMapping(value = "/org/staging", produces = { "application/json; charset=UTF-8" })
	public ResponseEntity<?> getOrgStaging(@RequestParam("status") String status, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(stagingService.getOrgStaging(status), HttpStatus.OK);
	}

	@ApiOperation("Saves staging data of organization")
	@ApiResponses(value = { @ApiResponse(code = 200, response = String.class, message = "Operation successful"),
			@ApiResponse(code = 404, response = ResponseMessage.class, message = ElementConstants.NO_DATA_FOUND_MSG),
			@ApiResponse(code = 500, response = ResponseMessage.class, message = ElementConstants.HIBERNATE_EXCEPTION_MSG) })
	@PostMapping(value = "/org/saveStaging", consumes = MediaType.APPLICATION_JSON_VALUE, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> saveOrgStaging(@RequestBody OrgStaging orgStaging, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {
		return new ResponseEntity<>(stagingService.saveOrgStaging(orgStaging), HttpStatus.OK);
	}
}
