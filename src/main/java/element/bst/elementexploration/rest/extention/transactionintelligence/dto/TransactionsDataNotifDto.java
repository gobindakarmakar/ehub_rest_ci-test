package element.bst.elementexploration.rest.extention.transactionintelligence.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("Transactions data notification")
public class TransactionsDataNotifDto implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="Transaction date")	
	private Date txDate;
	@ApiModelProperty(value="Transaction amount")	
	private double txAmount;
	@ApiModelProperty(value="Alert amount")	
	private double alertAmount;
	
	
	public TransactionsDataNotifDto(Date txDate, double txAmount) {
		super();
		this.txDate = txDate;
		this.txAmount = txAmount;
	}
	

	public Date getTxDate() {
		return txDate;
	}

	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}

	public double getTxAmount() {
		return txAmount;
	}

	public void setTxAmount(double txAmount) {
		this.txAmount = txAmount;
	}

	public double getAlertAmount() {
		return alertAmount;
	}

	public void setAlertAmount(double alertAmount) {
		this.alertAmount = alertAmount;
	}

}
