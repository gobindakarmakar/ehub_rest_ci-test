package element.bst.elementexploration.rest.casemanagement.enumType;

/**
 * enum class to set Status for a case
 * 
 *
 */
public enum StatusEnum {
	 FRESH(1, "fresh"),
	    SUBMITTED(2, "submitted"),
	    ACKNOWLEDGE(3, "acknowledge"),
	    ACCEPTED(4, "accepted"),
	    PAUSED(5, "paused"),
	    REJECTED(6, "rejected"),
	    FOCUS(7, "focus"),
	    RESOLVED(8,"resolved"),
	    CANCELED(9, "canceled");
	    
	    
	    private int index;
	    
	    private String name;
	    
		StatusEnum(int index, String name) {
	    	this.index = index;
	    	this.name = name;
	    }
	     
	    public int getIndex() {
	    	return this.index;
	    }
	    
	    public String getName() {
	    	return this.name;
	    }
	    
	    public static StatusEnum getStatusEnumByIndex(int index){
	    	StatusEnum[] array=StatusEnum.values();
	    	for (StatusEnum statusEnum : array) {
	    		if(statusEnum.getIndex()==index)
	    			return statusEnum;
			}
			return null;
	    }
	   

}
