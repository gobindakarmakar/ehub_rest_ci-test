package element.bst.elementexploration.rest.extention.sourcecredibility.daoimpl;

import javax.persistence.NoResultException;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import element.bst.elementexploration.rest.extention.sourcecredibility.dao.SourceCredibilityDao;
import element.bst.elementexploration.rest.extention.sourcecredibility.domain.SourceCredibility;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;

/**
 * @author suresh
 *
 */
@Repository("sourceCredibilityDao")
public class SourceCredibilityDaoImpl extends GenericDaoImpl<SourceCredibility, Long> implements SourceCredibilityDao {

	public SourceCredibilityDaoImpl() {
		super(SourceCredibility.class);
	}

	@Override
	public SourceCredibility findSubClassificationCredibility(Long sourceId, Long subClassificationId,
			Long attributeId) {
		SourceCredibility credibility = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append(
					"from SourceCredibility s  where s.sources.sourceId =:sourceId and  s.subClassifications.subClassificationId =:subClassificationId ");
			if (attributeId == null)
				hql.append(" and  s.dataAttributes.attributeId is NULL ");
			else
				hql.append(" and  s.dataAttributes.attributeId =:attributeId ");
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("sourceId", sourceId);
			query.setParameter("subClassificationId", subClassificationId);
			if (attributeId != null)
				query.setParameter("attributeId", attributeId);
			credibility = (SourceCredibility) query.getSingleResult();
			if (credibility != null)
				return credibility;
		} catch (NoResultException ne) {
			return credibility;
		} catch (Exception e) {
			return credibility;
		}
		return credibility;
	}

}
