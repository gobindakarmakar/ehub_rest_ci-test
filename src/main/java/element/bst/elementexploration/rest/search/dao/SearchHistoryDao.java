package element.bst.elementexploration.rest.search.dao;

import java.util.Date;
import java.util.List;

import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.search.domain.SearchHistory;

public interface SearchHistoryDao extends GenericDao<SearchHistory, Long>{
	
	SearchHistory findMostRecentSearch(Long userId);
	
	List<SearchHistory> getRecentSearches(Long userId, Date dateFrom, Date dateTo, Boolean favourite, String flag,
			Integer pageNumber, Integer recordsPerPage);
	
	long countRecentSearches(Long userId, Date dateFrom, Date dateTo, Boolean favourite, String flag);
}
