package element.bst.elementexploration.rest.extention.mip.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TimeFil implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String from;
	
	private String to;
	
	public String getFrom() {
		return from;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getTo() {
		return to;
	}
	
	public void setTo(String to) {
		this.to = to;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
