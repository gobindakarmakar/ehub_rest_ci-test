package element.bst.elementexploration.rest.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.json.simple.JSONObject;


public class CustomTextStripperNewTemplate extends PDFTextStripper {
	public CustomTextStripperNewTemplate(byte[] byteArrayFile, List<JSONObject> jsonObjectsList) throws IOException {
		super.setSortByPosition(true);
		this.jsonObjectsList = jsonObjectsList;
	}

	public  List<JSONObject> jsonObjectsList = new ArrayList<JSONObject>();
	
	@SuppressWarnings("unchecked")
	@Override
	protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
		for (TextPosition position : textPositions) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("val", position.getUnicode());
			jsonObject.put("xVal", position.getX());
			jsonObject.put("yYal", position.getY());
			jsonObjectsList.add(jsonObject);
		}

	}


}
