package element.bst.elementexploration.rest.extention.alertmanagement.dto;

/**
 * @author Prateek
 *
 */
import java.io.Serializable;
/**
 * @author Prateek
 *
 */
import java.util.List;

import element.bst.elementexploration.rest.util.PaginationInformation;
import io.swagger.annotations.ApiModelProperty;
public class AlertListDto implements Serializable{

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "List of Alerts")
	private List<AlertsDto> result;
	
	@ApiModelProperty(value = "Page information")
	private PaginationInformation paginationInformation;

	public List<AlertsDto> getResult() {
		return result;
	}

	public PaginationInformation getPaginationInformation() {
		return paginationInformation;
	}

	public void setResult(List<AlertsDto> result) {
		this.result = result;
	}

	public void setPaginationInformation(PaginationInformation paginationInformation) {
		this.paginationInformation = paginationInformation;
	}
	
	

}
