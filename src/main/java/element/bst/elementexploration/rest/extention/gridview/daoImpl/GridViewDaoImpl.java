package element.bst.elementexploration.rest.extention.gridview.daoImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.exception.FailedToExecuteQueryException;
import element.bst.elementexploration.rest.extention.gridview.dao.GridViewDao;
import element.bst.elementexploration.rest.extention.gridview.domain.GridView;
import element.bst.elementexploration.rest.generic.daoImpl.GenericDaoImpl;
import element.bst.elementexploration.rest.util.ElementLogger;
import element.bst.elementexploration.rest.util.ElementLogger.ElementLoggerLevel;

/**
 * @author Jalagari Paul
 *
 */
@Repository("gridViewDao")
@Transactional("transactionManager")
public class GridViewDaoImpl extends GenericDaoImpl<GridView, Long> implements GridViewDao {

	public GridViewDaoImpl() {
		super(GridView.class);
	}

	@Override
	public boolean deleteGridView(Long gridViewId, Long userId) throws Exception {
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("delete GridView gv where gv.gridViewId =:gridViewId and gv.createdBy=:userId");
			/*if (gridViewDto.getGridTableName() != null)
				hql.append(" and gv.gridTableName=:gridTableName");*/
			Query<?> query = this.getCurrentSession().createQuery(hql.toString());
			query.setParameter("gridViewId", gridViewId);
			query.setParameter("userId", userId);
			/*if (gridViewDto.getGridTableName() != null)
				query.setParameter("gridTableName", gridViewDto.getGridTableName());*/
			int result=query.executeUpdate();
			if (result != 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
			throw new FailedToExecuteQueryException("Failed to delete earlier records. Reason : " + e.getMessage());
		}
	}

}
