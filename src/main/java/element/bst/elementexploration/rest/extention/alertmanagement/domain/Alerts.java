package element.bst.elementexploration.rest.extention.alertmanagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import element.bst.elementexploration.rest.extention.alert.feedmanagement.domain.FeedManagement;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Prateek
 *
 */

@ApiModel("Alert Management")
@Entity
@Table(name = "bst_am_alerts")
public class Alerts implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long alertId;
	
	@ApiModelProperty(value = "Customer ID")
	@Column(name = "customerId" , columnDefinition = "TEXT")
	private String customerId;
	
	@ApiModelProperty(value = "WatchList")
	@Column(name = "watchList")
	private String watchList;
	
	@ApiModelProperty(value = "Entity Name")
	@Column(name = "entityName", columnDefinition = "TEXT")
	private String entityName;
	
	@ApiModelProperty(value = "Confidence level")
	@Column(name = "confidenceLevel")
	private Double confidenceLevel;
	
	@ApiModelProperty(value = "Created date for Alert")
	@CreationTimestamp
	@Column(name = "created_date")
	private Date createdDate;

	@ApiModelProperty(value = "updated date for Alert")
	@CreationTimestamp
	@Column(name = "updated_date")
	private Date updatedDate;
	
	@ApiModelProperty(value = "Feed From Feed Management")
	//@ManyToOne
	//@JoinColumn(name = "feed")
	//@OneToMany(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "alertId")
	@ManyToMany(cascade = CascadeType.DETACH)
	@JoinTable(name = "bst_am_alert_feed", joinColumns = { @JoinColumn(name = "alertId") }, inverseJoinColumns = {
			@JoinColumn(name = "feed_id") })
	private List<FeedManagement> feed;
	
	//All groups which are associated with the alert feed source (Mapping)
	@ApiModelProperty(value = "Group Level")
	@Column(name = "groupLevel")
	private Integer groupLevel;
	
	//Connected to user in the same groups the user associated with (Mapping)
	@ApiModelProperty(value = "Assignee")
	@Column(name = "assignee")
	private Long assignee;

	@ApiModelProperty(value = "Reviewer")
	@Column(name = "reviewer")
	private Long reviewer;
	
	@JsonIgnore
	@ApiModelProperty(value = "Review Status")
	//@ManyToOne(cascade=CascadeType.ALL)
	//@JoinColumn(name = "review_status_id")
	private Long reviewStatusId;
	
	@ApiModelProperty(value = "Status")
	@Column(name = "status")
	private Long status;
	
	@ApiModelProperty(value = "Risk Indicators")
	@Column(name = "riskIndicators")
	private String riskIndicators;

	// List of the comments from comments management
	
	@ApiModelProperty(value = "Comments")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "alertId")
	private List<AlertComments> comments;
		
	@ApiModelProperty(value = "Metadata of alert")
	@Column(name = "alert_metadata", columnDefinition = "LONGTEXT")
	private String alertMetaData;
	
	@ApiModelProperty(value = "Periodic Review for Alert")
	@Column(name = "periodic_review", nullable = false, columnDefinition="BIT DEFAULT 1", length = 1)
	private Boolean periodicReview;
	
	@Column(name="entiType")
	private String entiType;
	
	@Column(name="isEntityIdentified", nullable = false, columnDefinition="BIT DEFAULT 1", length = 1)
	private Boolean isEntityIdentified=false;
	
	@Column(name="identifiedEntityId")
	private String identifiedEntityId;
	
	@Column(name="jurisdiction")
	private Long jurisdiction;
	
	@Column(name="detailId")
	private Long detailId;
	
	@Column(name="userApproved")
	private Long userApproved;
	
	public Long getUserApproved() {
		return userApproved;
	}

	public void setUserApproved(Long userApproved) {
		this.userApproved = userApproved;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Double getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(Double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<FeedManagement> getFeed() {
		return feed;
	}

	public void setFeed(List<FeedManagement> feed) {
		this.feed = feed;
	}

	public Integer getGroupLevel() {
		return groupLevel;
	}

	public void setGroupLevel(Integer groupLevel) {
		this.groupLevel = groupLevel;
	}

	public Long getAssignee() {
		return assignee;
	}

	public void setAssignee(Long assignee) {
		this.assignee = assignee;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getRiskIndicators() {
		return riskIndicators;
	}

	public void setRiskIndicators(String riskIndicators) {
		this.riskIndicators = riskIndicators;
	}

	public String getWatchList() {
		return watchList;
	}

	public void setWatchList(String watchList) {
		this.watchList = watchList;
	}

	public List<AlertComments> getComments() {
		return comments;
	}

	public void setComments(List<AlertComments> comments) {
		this.comments = comments;
	}

	public String getAlertMetaData() {
		return alertMetaData;
	}

	public void setAlertMetaData(String alertMetaData) {
		this.alertMetaData = alertMetaData;
	}

	public Boolean getPeriodicReview() {
		return periodicReview;
	}

	public void setPeriodicReview(Boolean periodicReview) {
		this.periodicReview = periodicReview;
	}

	public Long getReviewer() {
		return reviewer;
	}

	public void setReviewer(Long reviewer) {
		this.reviewer = reviewer;
	}

	public Long getReviewStatusId() {
		return reviewStatusId;
	}

	public void setReviewStatusId(Long reviewStatusId) {
		this.reviewStatusId = reviewStatusId;
	}

	public String getEntiType() {
		return entiType;
	}

	public void setEntiType(String entiType) {
		this.entiType = entiType;
	}

	public Boolean getIsEntityIdentified() {
		return isEntityIdentified;
	}

	public void setIsEntityIdentified(Boolean isEntityIdentified) {
		this.isEntityIdentified = isEntityIdentified;
	}

	public String getIdentifiedEntityId() {
		return identifiedEntityId;
	}

	public void setIdentifiedEntityId(String identifiedEntityId) {
		this.identifiedEntityId = identifiedEntityId;
	}


	public Long getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Long jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}
	
}
