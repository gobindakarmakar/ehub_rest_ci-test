package element.bst.elementexploration.rest.extention.workflow.service;

public interface HdfsService {

	String deletedFiles(String profileId, String folderPath) throws Exception;

	String ListOfDirectories(String profileId, String folderPath) throws Exception;

	String makeDirectory(String profileId, String folderPath) throws Exception;

	String getSampleDataOfHdfs(String workflowId, String stageId) throws Exception;

	String getSchemaOfHdfs(String workflowId, String stageId) throws Exception;

}
