package element.bst.elementexploration.rest.extention.docparser.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import element.bst.elementexploration.rest.extention.docparser.domain.DocumentISOCodes;
import element.bst.elementexploration.rest.extention.docparser.domain.IsoCodes;
import element.bst.elementexploration.rest.extention.docparser.service.DocumentISOCodeService;
import element.bst.elementexploration.rest.extention.docparser.service.ISOCodeService;
import element.bst.elementexploration.rest.generic.dao.GenericDao;
import element.bst.elementexploration.rest.generic.serviceImpl.GenericServiceImpl;

/**
 * @author suresh
 *
 */
@Service("documentISOCodeService")
@Transactional("transactionManager")
public class DocumentISOCodeServiceImpl extends GenericServiceImpl<DocumentISOCodes, Long> implements DocumentISOCodeService {

	@Autowired
	public DocumentISOCodeServiceImpl(GenericDao<DocumentISOCodes, Long> genericDao) {
		super(genericDao);
	}
	
	
	@Autowired
	 DocumentISOCodeService documentISOCodeService;
	
	@Autowired
	ISOCodeService isoCodeService;

	@SuppressWarnings("unused")
	@Override
	public boolean saveIsoCodes() 
	{
		//1nd
		{
		List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
		DocumentISOCodes object1=new DocumentISOCodes();
		object1.setQuestion("Has the proposer designated a Chief Privacy Officer? ");
		object1.setNormalizedQuestion("Policies for Information Security");
		object1.setPrimaryIsoCode("A6.1.1");
		object1.setAnswerType("Yes/No");
		DocumentISOCodes  codes = documentISOCodeService.save(object1);
		List<IsoCodes> list1 = new ArrayList<IsoCodes>();
		list1.add(new IsoCodes("A6.1.1", codes));
		list1.add(new IsoCodes("A16.1.1", codes));
		for (IsoCodes isoCodes : list1) 
		{
			isoCodeService.save(isoCodes);
		}
		}
		
		//2rd object
		{
		List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
		DocumentISOCodes object1=new DocumentISOCodes();
		object1.setQuestion("Do you have a corporate wide privacy policy?");
		object1.setNormalizedQuestion("Policies for Information Security");
		object1.setPrimaryIsoCode("A5.1.1");
		object1.setAnswerType("Yes/No");
		DocumentISOCodes  codes = documentISOCodeService.save(object1);
		List<IsoCodes> list1 = new ArrayList<IsoCodes>();
		list1.add(new IsoCodes("A5.1.1", codes));		list1.add(new IsoCodes("A7.2.1", codes));		list1.add(new IsoCodes("A8.2.1 ", codes));
		list1.add(new IsoCodes("A8.2.2", codes));		list1.add(new IsoCodes("A11.2.6", codes));		list1.add(new IsoCodes("A11.2.7", codes));
		list1.add(new IsoCodes("A11.2.8", codes)); 		list1.add(new IsoCodes("A11.2.9", codes));        list1.add(new IsoCodes("A12.1.1", codes));
		list1.add(new IsoCodes("A13.2.1", codes));		list1.add(new IsoCodes("A13.2.2", codes));		list1.add(new IsoCodes("A13.2.3", codes));
		list1.add(new IsoCodes("A13.2.4", codes));		list1.add(new IsoCodes("A14.1.1", codes));		list1.add(new IsoCodes("A14.1.2", codes));
		list1.add(new IsoCodes("A15.1.1", codes)); 		list1.add(new IsoCodes("A15.1.2", codes));        list1.add(new IsoCodes("A16.1.1", codes));
		list1.add(new IsoCodes("A18.1.1", codes));		
		for (IsoCodes isoCodes : list1) 
		{
			isoCodeService.save(isoCodes);
		}
		}
		
		//3 object
		{
			List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
			DocumentISOCodes object1=new DocumentISOCodes();
			object1.setQuestion("Do you encrypt all account information on your databases?");
			object1.setNormalizedQuestion("Handling of Assets");
			object1.setPrimaryIsoCode("A8.2.3");
			object1.setAnswerType("Yes/No");
			DocumentISOCodes  codes = documentISOCodeService.save(object1);
			List<IsoCodes> list1 = new ArrayList<IsoCodes>();
			list1.add(new IsoCodes("A8.1.4", codes));		list1.add(new IsoCodes("A8.2.3", codes));		list1.add(new IsoCodes("A8.3.1 ", codes));
			list1.add(new IsoCodes("A8.3.3", codes));		list1.add(new IsoCodes("A11.2.7", codes));		list1.add(new IsoCodes("A12.4.2", codes));
			list1.add(new IsoCodes("A12.4.3", codes)); 		list1.add(new IsoCodes("A14.1.2", codes));        list1.add(new IsoCodes("A14.3.1", codes));
			list1.add(new IsoCodes("A17.2.1", codes));		list1.add(new IsoCodes("A18.1.3", codes));		list1.add(new IsoCodes("A18.1.5", codes));
			for (IsoCodes isoCodes : list1) 
			{
				isoCodeService.save(isoCodes);
			}
		}
		
		//4 object
				{
					List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
					DocumentISOCodes object1=new DocumentISOCodes();
					object1.setQuestion("Do you mask all but the last four digits of a card number when displaying or printing card holder data?");
					object1.setNormalizedQuestion("Protection of log information");
					object1.setPrimaryIsoCode("A12.4.2");
					object1.setAnswerType("Yes/No");
					DocumentISOCodes  codes = documentISOCodeService.save(object1);
					List<IsoCodes> list1 = new ArrayList<IsoCodes>();
					list1.add(new IsoCodes("A12.4.2", codes));		list1.add(new IsoCodes("A12.4.3", codes));		list1.add(new IsoCodes("A14.1.3 ", codes));
					list1.add(new IsoCodes("A18.1.1", codes));		list1.add(new IsoCodes("A18.1.3", codes));		
					for (IsoCodes isoCodes : list1) 
					{
						isoCodeService.save(isoCodes);
					}
				}
				
				//5 object
				{
					List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
					DocumentISOCodes object1=new DocumentISOCodes();
					object1.setQuestion("Do you ensure that card validation codes are not stored in any of your databases, log files or anywhere else within your network?");
					object1.setNormalizedQuestion("Protection of log information");
					object1.setPrimaryIsoCode("A12.4.2");
					object1.setAnswerType("Yes/No");
					DocumentISOCodes  codes = documentISOCodeService.save(object1);
					List<IsoCodes> list1 = new ArrayList<IsoCodes>();
					list1.add(new IsoCodes("A12.4.2", codes));		list1.add(new IsoCodes("A12.4.3", codes));		list1.add(new IsoCodes("A14.1.3 ", codes));
					list1.add(new IsoCodes("A18.1.1", codes));		list1.add(new IsoCodes("A18.1.3", codes));		
					for (IsoCodes isoCodes : list1) 
					{
						isoCodeService.save(isoCodes);
					}
				}
		
				//6 object
				{
					List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
					DocumentISOCodes object1=new DocumentISOCodes();
					object1.setQuestion("Have you implemented an identity theft prevention programme, i.e. FTC “Red Flags” Programme?");
					object1.setNormalizedQuestion("Clear desk and clear screen policy");
					object1.setPrimaryIsoCode("?");
					object1.setAnswerType("Yes/No");
					DocumentISOCodes  codes = documentISOCodeService.save(object1);
					List<IsoCodes> list1 = new ArrayList<IsoCodes>();
					list1.add(new IsoCodes("?", codes));
					for (IsoCodes isoCodes : list1) 
					{
						isoCodeService.save(isoCodes);
					}
				}
				
				//7 object
				{
					List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
					DocumentISOCodes object1=new DocumentISOCodes();
					object1.setQuestion("Has any third party audit of your privacy practices been undertaken in the last two years?");
					object1.setNormalizedQuestion("Management of systems audit controls");
					object1.setPrimaryIsoCode("A12.6.1");
					object1.setAnswerType("Yes/No");
					DocumentISOCodes  codes = documentISOCodeService.save(object1);
					List<IsoCodes> list1 = new ArrayList<IsoCodes>();
					list1.add(new IsoCodes("A11.1.4", codes));
					list1.add(new IsoCodes("A14.1.2", codes));
					list1.add(new IsoCodes("A14.2.8", codes));
					list1.add(new IsoCodes("A16.1.3", codes));
					for (IsoCodes isoCodes : list1) 
					{
						isoCodeService.save(isoCodes);
					}
				}
		
		
				//8 object
				{
					List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
					DocumentISOCodes object1=new DocumentISOCodes();
					object1.setQuestion("Does the proposer distribute information security policies to its employees?");
					object1.setNormalizedQuestion("Information security awareness, education and training");
					object1.setPrimaryIsoCode("A7.2.2");
					object1.setAnswerType("Yes/No");
					DocumentISOCodes  codes = documentISOCodeService.save(object1);
					List<IsoCodes> list1 = new ArrayList<IsoCodes>();
					list1.add(new IsoCodes("A7.2.2", codes));
					for (IsoCodes isoCodes : list1) 
					{
						isoCodeService.save(isoCodes);
					}
				}
		
				//9 object
				{
					List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
					DocumentISOCodes object1=new DocumentISOCodes();
					object1.setQuestion("Do you provide awareness training for employees in data privacy ad security including legal liability and social engineering issues?");
					object1.setNormalizedQuestion("Information security awareness, education and training");
					object1.setPrimaryIsoCode("A7.2.2");
					object1.setAnswerType("Yes/No");
					DocumentISOCodes  codes = documentISOCodeService.save(object1);
					List<IsoCodes> list1 = new ArrayList<IsoCodes>();
					list1.add(new IsoCodes("A7.2.2", codes));
					for (IsoCodes isoCodes : list1) 
					{
						isoCodeService.save(isoCodes);
					}
				}
				
		
					//10 object
					{
						List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
						DocumentISOCodes object1=new DocumentISOCodes();
						object1.setQuestion("Is write access to USB drives disabled for employees?\"");
						object1.setNormalizedQuestion("Access control policy");
						object1.setPrimaryIsoCode("A9.1.1");
						object1.setAnswerType("Yes/No");
						DocumentISOCodes  codes = documentISOCodeService.save(object1);
						List<IsoCodes> list1 = new ArrayList<IsoCodes>();
						list1.add(new IsoCodes("A13.2.1", codes));		list1.add(new IsoCodes("A13.2.3", codes));		list1.add(new IsoCodes("A13.2.4 ", codes));
						list1.add(new IsoCodes("A14.1.1", codes));		list1.add(new IsoCodes("A14.1.2", codes));		
						list1.add(new IsoCodes("A14.2.7", codes));		list1.add(new IsoCodes("A15.1.1", codes));		
						list1.add(new IsoCodes("A16.1.1", codes));		list1.add(new IsoCodes("A18.1.1", codes));		
						list1.add(new IsoCodes("A18.1.3", codes));		list1.add(new IsoCodes("A18.2.1", codes));
						list1.add(new IsoCodes("A18.2.2", codes));	
						for (IsoCodes isoCodes : list1) 
						{
							isoCodeService.save(isoCodes);
						}
					}
					
					//11 object
					{
						List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
						DocumentISOCodes object1=new DocumentISOCodes();
						object1.setQuestion("Do you have a procedure for the revocation of user accounts and the recovery of inventoried information assets following an employee’s departure from your organisation?");
						object1.setNormalizedQuestion("Termination or change of employment responsibilities");
						object1.setPrimaryIsoCode("A7.3.1");
						object1.setAnswerType("Yes/No");
						DocumentISOCodes  codes = documentISOCodeService.save(object1);
						List<IsoCodes> list1 = new ArrayList<IsoCodes>();
						list1.add(new IsoCodes("A7.3.1", codes));		list1.add(new IsoCodes("A8.1.4", codes));		list1.add(new IsoCodes("A8.2.3", codes));
						list1.add(new IsoCodes("A9.1.1", codes));		list1.add(new IsoCodes("A9.2.1", codes));		
						list1.add(new IsoCodes("A18.2.2", codes));	
						for (IsoCodes isoCodes : list1) 
						{
							isoCodeService.save(isoCodes);
						}
					}
					

					//12 object
					{
						List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
						DocumentISOCodes object1=new DocumentISOCodes();
						object1.setQuestion("Do you have written clearance procedures in place regarding use, licensing, and consent agreements for third party content used by you on your website or in your promotional materials?");
						object1.setNormalizedQuestion("Classification of information");
						object1.setPrimaryIsoCode("A8.2.1");
						object1.setAnswerType("Yes/No");
						DocumentISOCodes  codes = documentISOCodeService.save(object1);
						List<IsoCodes> list1 = new ArrayList<IsoCodes>();
						list1.add(new IsoCodes("A5.1.1", codes));		list1.add(new IsoCodes("A8.2.1", codes));		list1.add(new IsoCodes("A8.2.2", codes));
						list1.add(new IsoCodes("A11.2.6", codes));		list1.add(new IsoCodes("A11.2.7", codes));			list1.add(new IsoCodes("A11.2.9", codes));	
						list1.add(new IsoCodes("A12.1.1", codes));		list1.add(new IsoCodes("A13.2.1", codes));		list1.add(new IsoCodes("A13.2.2", codes));
						list1.add(new IsoCodes("A13.2.3", codes));		list1.add(new IsoCodes("A13.2.4", codes));		list1.add(new IsoCodes("A14.1.1", codes));
						list1.add(new IsoCodes("A14.1.2", codes));		list1.add(new IsoCodes("A14.2.7", codes));		list1.add(new IsoCodes("A15.1.1", codes));
						list1.add(new IsoCodes("A15.1.2", codes));		list1.add(new IsoCodes("A16.1.1", codes));		list1.add(new IsoCodes("A18.1.1", codes));
						list1.add(new IsoCodes("A18.2.1", codes));		list1.add(new IsoCodes("A18.2.2", codes));		
						
						for (IsoCodes isoCodes : list1) 
						{
							isoCodeService.save(isoCodes);
						}
					}
					
					//13 object
					{
						List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
						DocumentISOCodes object1=new DocumentISOCodes();
						object1.setQuestion("Does your website feature opt in/opt out procedures when collecting individual users’ information?");
						object1.setNormalizedQuestion("Indentification of applicable legislation and contractual requirements");
						object1.setPrimaryIsoCode("A18.1.1");
						object1.setAnswerType("Yes/No");
						DocumentISOCodes  codes = documentISOCodeService.save(object1);
						List<IsoCodes> list1 = new ArrayList<IsoCodes>();
						list1.add(new IsoCodes("A18.1.1", codes));		list1.add(new IsoCodes("A18.1.3", codes));		list1.add(new IsoCodes("A18.2.2", codes));
						
						for (IsoCodes isoCodes : list1) 
						{
							isoCodeService.save(isoCodes);
						}
					}
					
					//14 object
					{
						List<DocumentISOCodes> DocumentISOCodess=new ArrayList<DocumentISOCodes>();
						DocumentISOCodes object1=new DocumentISOCodes();
						object1.setQuestion("Do you or have you ever used flash cookies on your website to track visitors?");
						object1.setNormalizedQuestion("Controls against malware");
						object1.setPrimaryIsoCode("?");
						object1.setAnswerType("Yes/No");
						DocumentISOCodes  codes = documentISOCodeService.save(object1);
						List<IsoCodes> list1 = new ArrayList<IsoCodes>();
						list1.add(new IsoCodes("?", codes));
						for (IsoCodes isoCodes : list1) 
						{
							isoCodeService.save(isoCodes);
						}
					}
					
		return false;
	}
}
